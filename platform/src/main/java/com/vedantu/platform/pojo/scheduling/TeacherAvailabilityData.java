package com.vedantu.platform.pojo.scheduling;

import com.vedantu.listing.pojo.EsTeacherData;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.platform.pojo.AvailableTeacherExportInfo;
import com.vedantu.util.scheduling.CommonCalendarUtils;

import java.util.Arrays;
import java.util.BitSet;

public class TeacherAvailabilityData {
	private String userId;
	private EsTeacherData teacherData;
	private long startTime;
	private long endTime;
	private long[] availabilityBitSet;
	private long[] markedAvailabilityBitSet;

	public TeacherAvailabilityData() {
		super();
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public EsTeacherData getTeacherData() {
		return teacherData;
	}

	public void setTeacherData(EsTeacherData teacherData) {
		this.teacherData = teacherData;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long[] getAvailabilityBitSet() {
		return availabilityBitSet;
	}

	public void setAvailabilityBitSet(long[] availabilityBitSet) {
		this.availabilityBitSet = availabilityBitSet;
	}

	public long[] getMarkedAvailabilityBitSet() {
		return markedAvailabilityBitSet;
	}

	public void setMarkedAvailabilityBitSet(long[] markedAvailabilityBitSet) {
		this.markedAvailabilityBitSet = markedAvailabilityBitSet;
	}

	public void updateTeacherInfo(User teacher) {
		if (teacherData != null) {
			teacherData.setFirstName(teacher.getFirstName());
			teacherData.setLastName(teacher.getLastName());
			teacherData.setFullname(teacher.getFullName());
			teacherData.setGender(String.valueOf(teacher.getGender()));
			teacherData.setProfilePicUrl(teacher.getProfilePicUrl());
		}
		this.setTeacherData(teacherData);
	}


	public String fetchAvailabilityString(Long startTime, Long endTime) {
		int startIndex = getBitSetIndex(CommonCalendarUtils.getSlotStartTime(startTime));
		int endIndex = getBitSetIndex(CommonCalendarUtils.getSlotEndTime(endTime));
		BitSet markedAvailabilityBitSet = new BitSet();
		if (this.markedAvailabilityBitSet != null) {
			markedAvailabilityBitSet = BitSet.valueOf(this.markedAvailabilityBitSet);
		}

		if ((startTime < this.startTime) || (endTime > this.endTime)) {
			return AvailableTeacherExportInfo.UNAVAILABLE_STRING;
		}

		BitSet availabilityBitSet = BitSet.valueOf(this.availabilityBitSet);
		if (endIndex > availabilityBitSet.nextClearBit(startIndex)) {

			if (endIndex > markedAvailabilityBitSet.nextClearBit(startIndex)) {
				return AvailableTeacherExportInfo.UNAVAILABLE_STRING;
			} else {
				return AvailableTeacherExportInfo.AVAILABLE_BOOKED;
			}
		}

		return AvailableTeacherExportInfo.AVAILABLE_STRING;
	}

	public int getBitSetIndex(long time) {
		return Long.valueOf((time - this.startTime) / CommonCalendarUtils.SLOT_LENGTH).intValue();
	}

	@Override
	public String toString() {
		return "TeacherAvailabilityData [userId=" + userId + ", teacherData=" + teacherData + ", startTime=" + startTime
				+ ", endTime=" + endTime + ", availabilityBitSet=" + Arrays.toString(availabilityBitSet)
				+ ", markedAvailabilityBitSet=" + Arrays.toString(markedAvailabilityBitSet) + "]";
	}
}
