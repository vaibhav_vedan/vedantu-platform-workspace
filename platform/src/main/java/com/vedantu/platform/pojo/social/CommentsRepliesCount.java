/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.social;

/**
 *
 * @author ajith
 */
public class CommentsRepliesCount {

    private String parentId;
    private int replies;

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public int getReplies() {
        return replies;
    }

    public void setReplies(int replies) {
        this.replies = replies;
    }

    @Override
    public String toString() {
        return "CommentsRepliesCount{" + "commentId=" + parentId + ", replies=" + replies + '}';
    }

}
