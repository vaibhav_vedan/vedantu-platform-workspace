/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.user;

import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author somil
 */
public class SendUserEmailReq extends AbstractFrontEndReq {

	private Long userId;
	private CommunicationType emailType;

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public CommunicationType getEmailType() {

		return emailType;
	}

	public void setEmailType(CommunicationType emailType) {

		this.emailType = emailType;
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		if (null == userId) {
			errors.add("userId");
		}
		
		if (null == emailType) {
			errors.add("emailType");
		}
		
		return errors;
	}
}

