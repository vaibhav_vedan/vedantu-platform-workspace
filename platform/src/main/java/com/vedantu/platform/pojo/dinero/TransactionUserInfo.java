/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.dinero;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.pojo.ExtTransactionPojo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.subscription.pojo.Subscription;

/**
 *
 * @author somil
 */
public class TransactionUserInfo {
        
    private Transaction transaction;
    private ExtTransactionPojo extTransaction;
    private UserBasicInfo userBasicInfo;
    private Orders orders;
    private SessionPayout sessionPayout;
    private UserBasicInfo teacherBasicInfo;
    private Subscription subscription;

    public TransactionUserInfo(Transaction transaction, UserBasicInfo userBasicInfo) {
        this.transaction = transaction;
        this.userBasicInfo = userBasicInfo;
    }
    
    public TransactionUserInfo() {
        
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
    }

    public UserBasicInfo getUserBasicInfo() {
        return userBasicInfo;
    }

    public void setUserBasicInfo(UserBasicInfo userBasicInfo) {
        this.userBasicInfo = userBasicInfo;
    }

    public ExtTransactionPojo getExtTransaction() {
        return extTransaction;
    }

    public void setExtTransaction(ExtTransactionPojo extTransaction) {
        this.extTransaction = extTransaction;
    }

    public Orders getOrders() {
        return orders;
    }

    public void setOrders(Orders orders) {
        this.orders = orders;
    }

    public SessionPayout getSessionPayout() {
        return sessionPayout;
    }

    public void setSessionPayout(SessionPayout sessionPayout) {
        this.sessionPayout = sessionPayout;
    }

    public UserBasicInfo getTeacherBasicInfo() {
        return teacherBasicInfo;
    }

    public void setTeacherBasicInfo(UserBasicInfo teacherBasicInfo) {
        this.teacherBasicInfo = teacherBasicInfo;
    }

    public Subscription getSubscription() {
        return subscription;
    }

    public void setSubscription(Subscription subscription) {
        this.subscription = subscription;
    }
    
    
    
  
}
