/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.cms;

/**
 *
 * @author jeet
 */
public class WebinarUserAttendenceInterval {
    private String joinTime;
    private String leaveTime;

    public WebinarUserAttendenceInterval() {
    }

    public String getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(String joinTime) {
        this.joinTime = joinTime;
    }

    public String getLeaveTime() {
        return leaveTime;
    }

    public void setLeaveTime(String leaveTime) {
        this.leaveTime = leaveTime;
    }

    @Override
    public String toString() {
        return "WebinarUserAttendenceInterval{" + "joinTime=" + joinTime + ", leaveTime=" + leaveTime + '}';
    }
    
}
