package com.vedantu.platform.pojo.courseplan;

import com.vedantu.dinero.response.InstalmentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mano
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProcessInstallmentForCoursePlanPojo {
    private InstalmentInfo instalmentInfo;
    private Long instalmentPaidUserId;
}
