package com.vedantu.platform.pojo.dinero;

public class ApprovePriceWithTime {

	int min;
	int max;
	Long price;
	boolean approved;
	Long updateTime;

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public boolean isApproved() {
		return approved;
	}

	public void setApproved(boolean approved) {
		this.approved = approved;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public ApprovePriceWithTime(int min, int max, Long price, boolean approved, Long updateTime) {
		super();
		this.min = min;
		this.max = max;
		this.price = price;
		this.approved = approved;
		this.updateTime = updateTime;
	}

	public ApprovePriceWithTime() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ApprovePriceWithTime [min=" + min + ", max=" + max + ", price=" + price + ", approved=" + approved
				+ ", updateTime=" + updateTime + "]";
	}

}
