package com.vedantu.platform.pojo.askadoubt;

import com.vedantu.platform.request.AddDoubtRequest;
import java.util.List;

import com.vedantu.platform.enums.askadoubt.DoubtStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Doubt extends AbstractMongoStringIdEntity{

	private Long userId;
	private Integer grade;
	private String boardId;
	private String doubtText;
	private List<String> attachments;
	private DoubtStatus status;
	private Long assignedTo;
	private String startMessageId;
	private String endMessageId;
	private Long solutionSubmissionTime;
	private Long doubtAcceptanceTime;
	private Long broadcastedTime;
	
	public Doubt() {
		super();
	}
        
	public Doubt(Long creationTime, String createdBy, Long lastUpdated,
			String lastUpdatedBy) {
		super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
	}
	
	public Doubt(Long userId, Integer grade, String boardId, String doubtText,
			List<String> attachments, DoubtStatus status, Long assignedTo,
			String startMessageId, String endMessageId) {
		super();
		this.userId = userId;
		this.grade = grade;
		this.boardId = boardId;
		this.doubtText = doubtText;
		this.attachments = attachments;
		this.status = status;
		this.assignedTo = assignedTo;
		this.startMessageId = startMessageId;
		this.endMessageId = endMessageId;
	}
	
	public Doubt(AddDoubtRequest addDoubtRequest) {
		super(System.currentTimeMillis(), addDoubtRequest.getUserId().toString(),
                        System.currentTimeMillis(),addDoubtRequest.getUserId().toString());
		this.userId = addDoubtRequest.getUserId();
		this.boardId = addDoubtRequest.getBoardId();
		this.grade = addDoubtRequest.getGrade();
		this.doubtText = addDoubtRequest.getDoubtText();
		this.attachments = addDoubtRequest.getAttachments();
	}
	
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getDoubtText() {
		return doubtText;
	}
	public void setDoubtText(String doubtText) {
		this.doubtText = doubtText;
	}
	public List<String> getAttachments() {
		return attachments;
	}
	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}
	public DoubtStatus getStatus() {
		return status;
	}
	public void setStatus(DoubtStatus status) {
		this.status = status;
	}
	public Long getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(Long assignedTo) {
		this.assignedTo = assignedTo;
	}
	public String getStartMessageId() {
		return startMessageId;
	}
	public void setStartMessageId(String startMessageId) {
		this.startMessageId = startMessageId;
	}
	public String getEndMessageId() {
		return endMessageId;
	}
	public void setEndMessageId(String endMessageId) {
		this.endMessageId = endMessageId;
	}
	public Long getSolutionSubmissionTime() {
		return solutionSubmissionTime;
	}
	public void setSolutionSubmissionTime(Long solutionSubmissionTime) {
		this.solutionSubmissionTime = solutionSubmissionTime;
	}
	public Long getDoubtAcceptanceTime() {
		return doubtAcceptanceTime;
	}
	public void setDoubtAcceptanceTime(Long doubtAcceptanceTime) {
		this.doubtAcceptanceTime = doubtAcceptanceTime;
	}
	public Long getBroadcastedTime() {
		return broadcastedTime;
	}
	public void setBroadcastedTime(Long broadcastedTime) {
		this.broadcastedTime = broadcastedTime;
	}
	
	@Override
	public String toString() {
		return "Doubt [userId=" + userId + ", grade=" + grade + ", boardId="
				+ boardId + ", doubtText=" + doubtText + ", attachments="
				+ attachments + ", status=" + status + ", assignedTo="
				+ assignedTo + ", startMessageId=" + startMessageId
				+ ", endMessageId=" + endMessageId + "]";
	}
	
	public static class Constants {
		public static final String USER_ID = "userId";
		public static final String BOARD_ID = "boardId";
		public static final String GRADE = "grade";
		public static final String STATUS = "status";
		public static final String ASSIGNED_TO = "assignedTo";
	}

}
