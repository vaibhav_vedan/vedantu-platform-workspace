/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo;

import com.vedantu.User.UTMParams;

/**
 *
 * @author jeet
 */
public class ISLStudentData {

    private String schoolName;
    private String city;
    private String studentName;
    private String grade;
    private String parentEmail;
    private String parentPhoneNo;
    private String parentPhoneCode;
    private String email;
    private String agentName;
    private String agentEmail;
    private UTMParams utm = new UTMParams();

    public ISLStudentData() {
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getParentPhoneNo() {
        return parentPhoneNo;
    }

    public void setParentPhoneNo(String parentPhoneNo) {
        this.parentPhoneNo = parentPhoneNo;
    }

    public String getParentPhoneCode() {
        return parentPhoneCode;
    }

    public void setParentPhoneCode(String parentPhoneCode) {
        this.parentPhoneCode = parentPhoneCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAgentName() {
        return agentName;
    }

    public void setAgentName(String agentName) {
        this.agentName = agentName;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public String getParentEmail() {
        return parentEmail;
    }

    public void setParentEmail(String parentEmail) {
        this.parentEmail = parentEmail;
    }

    public UTMParams getUtm() {
        return utm;
    }

    public void setUtm(UTMParams utm) {
        this.utm = utm;
    }

    @Override
    public String toString() {
        return "ISLStudentData{" + "schoolName=" + schoolName + ", city=" + city + ", studentName=" + studentName + ", grade=" + grade + ", parentEmail=" + parentEmail + ", parentPhoneNo=" + parentPhoneNo + ", parentPhoneCode=" + parentPhoneCode + ", email=" + email + ", agentName=" + agentName + ", agentEmail=" + agentEmail + ", utm=" + utm + '}';
    }


}
