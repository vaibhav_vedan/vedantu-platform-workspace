/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.subscription;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.platform.pojo.scheduling.BasicRes;

import java.util.Map;

/**
 *
 * @author somil
 */
public class SubscriptionRequestPaymentRes extends BasicRes {
	private ErrorCode errorCode;
	private Long amount;
	private Boolean needRecharge;
	private String rechargeUrl;
	private String forwardHttpMethod;
        private PaymentGatewayName gatewayName;
	private Map<String, Object> postParams;
        private Boolean redirect;

	public SubscriptionRequestPaymentRes() {
		super();
	}

        public PaymentGatewayName getGatewayName() {
            return gatewayName;
        }

        public void setGatewayName(PaymentGatewayName gatewayName) {
            this.gatewayName = gatewayName;
        }
        
        

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Boolean getNeedRecharge() {
		return needRecharge;
	}

	public void setNeedRecharge(Boolean needRecharge) {
		this.needRecharge = needRecharge;
	}

	public String getRechargeUrl() {
		return rechargeUrl;
	}

	public void setRechargeUrl(String rechargeUrl) {
		this.rechargeUrl = rechargeUrl;
	}

	public String getForwardHttpMethod() {
		return forwardHttpMethod;
	}

	public void setForwardHttpMethod(String forwardHttpMethod) {
		this.forwardHttpMethod = forwardHttpMethod;
	}

	public Map<String, Object> getPostParams() {
		return postParams;
	}

	public void setPostParams(Map<String, Object> postParams) {
		this.postParams = postParams;
	}

	public void updateRechargeUrlParams(RechargeUrlInfo rechargeUrlInfo) {
		setSuccess(false);
		this.needRecharge = true;
		this.rechargeUrl = rechargeUrlInfo.getRechargeUrl();
		this.forwardHttpMethod = rechargeUrlInfo.getForwardHttpMethod();
                this.gatewayName = rechargeUrlInfo.getGatewayName();
		
		if (rechargeUrlInfo.getPostParams() != null && !rechargeUrlInfo.getPostParams().isEmpty()) {
			this.postParams = rechargeUrlInfo.getPostParams();
		}
	}

    public Boolean getRedirect() {
        return redirect;
    }

    public void setRedirect(Boolean redirect) {
        this.redirect = redirect;
    }
        
        

	@Override
	public String toString() {
		return "SubscriptionRequestPaymentRes [errorCode=" + errorCode + ", needRecharge=" + needRecharge
				+ ", rechargeUrl=" + rechargeUrl
                                + ", gatewayName=" + gatewayName
                                + ", forwardHttpMethod=" + forwardHttpMethod + ", postParams="
				+ postParams + "]";
	}
}

