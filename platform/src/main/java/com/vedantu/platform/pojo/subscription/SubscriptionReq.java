package com.vedantu.platform.pojo.subscription;

import com.vedantu.subscription.enums.SubModel;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SubscriptionReq extends AbstractFrontEndReq {

	private Long teacherId;
	private Long studentId;
	private String planId;
	private Long offeringId;
	private SessionModel model;
	private ScheduleType scheduleType;
	private Long totalHours;
	private String teacherCouponId;
	private Long teacherDiscountAmount;
	private Long teacherHourlyRate;
	private String subject;
	private String target;
	private Integer grade;
	private SessionSchedule schedule;
	private Long subscriptionId;
	private String note;
	private SubModel subModel;
	private SessionSource sessionSource;

	public SubscriptionReq() {
		super();
	}

	public SubscriptionReq(Long teacherId, Long studentId, String planId, Long offeringId, SessionModel model,
			ScheduleType scheduleType, Long totalHours, String teacherCouponId, Long teacherDiscountAmount,
			Long teacherHourlyRate, String subject, String target, Integer grade, SessionSchedule schedule,
			Long subscriptionId, String note, SubModel subModel, SessionSource sessionSource) {
		super();
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.planId = planId;
		this.offeringId = offeringId;
		this.model = model;
		this.scheduleType = scheduleType;
		this.totalHours = totalHours;
		this.teacherCouponId = teacherCouponId;
		this.teacherDiscountAmount = teacherDiscountAmount;
		this.teacherHourlyRate = teacherHourlyRate;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.schedule = schedule;
		this.subscriptionId = subscriptionId;
		this.note = note;
		this.subModel = subModel;
		this.sessionSource= sessionSource;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public ScheduleType getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(ScheduleType scheduleType) {
		this.scheduleType = scheduleType;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public String getTeacherCouponId() {
		return teacherCouponId;
	}

	public void setTeacherCouponId(String teacherCouponId) {
		this.teacherCouponId = teacherCouponId;
	}

	public Long getTeacherDiscountAmount() {
		return teacherDiscountAmount;
	}

	public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
		this.teacherDiscountAmount = teacherDiscountAmount;
	}

	public Long getTeacherHourlyRate() {
		return teacherHourlyRate;
	}

	public void setTeacherHourlyRate(Long teacherHourlyRate) {
		this.teacherHourlyRate = teacherHourlyRate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public SessionSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(SessionSchedule schedule) {
		this.schedule = schedule;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public SubModel getSubModel() {
		return subModel;
	}

	public void setSubModel(SubModel subModel) {
		this.subModel = subModel;
	}

	public SessionSource getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(SessionSource sessionSource) {
		this.sessionSource = sessionSource;
	}
}
