/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

import com.vedantu.platform.enums.feedback.FeedbackDynamicEntity;
import java.util.List;

/**
 *
 * @author parashar
 */
public class FeedbackFormDynamic {
    
    private List<FeedbackQuestion> questions;
    
    private FeedbackDynamicEntity feedbackDynamicEntity;

    @Override
    public String toString() {
        return "FeedbackFormDynamic{" + "questions=" + questions + ", feedbackDynamicEntity=" + feedbackDynamicEntity + '}';
    }

    /**
     * @return the questions
     */
    public List<FeedbackQuestion> getQuestions() {
        return questions;
    }

    /**
     * @param questions the questions to set
     */
    public void setQuestions(List<FeedbackQuestion> questions) {
        this.questions = questions;
    }

    /**
     * @return the feedbackDynamicEntity
     */
    public FeedbackDynamicEntity getFeedbackDynamicEntity() {
        return feedbackDynamicEntity;
    }

    /**
     * @param feedbackDynamicEntity the feedbackDynamicEntity to set
     */
    public void setFeedbackDynamicEntity(FeedbackDynamicEntity feedbackDynamicEntity) {
        this.feedbackDynamicEntity = feedbackDynamicEntity;
    }
    
}
