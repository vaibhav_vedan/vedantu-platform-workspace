package com.vedantu.platform.pojo.dinero;

import com.vedantu.platform.enums.SessionEntity;
import com.vedantu.util.dbentitybeans.mysql.AbstractSqlEntityBean;
import com.vedantu.util.enums.SessionModel;

public class SessionPayout extends AbstractSqlEntityBean {

	private Long sessionId;
	private Long subscriptionId;
	private Long billingDuration;
	private Long sessionDuration;
	private Long date;
	private SessionModel model;
	private int teacherPayout;
	private int teacherPromotionalPayout;
	private int teacherNonPromotionalPayout;

	private int cut;
	private int promotionalCut;
	private int nonPromotionalCut;

	private Long teacherId;
	private Long studentId;
	private SessionEntity entity;
	private String entityId;
	private Long hourlyRate;
	private Boolean isUpdated;

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getDate() {
		return date;
	}

	public void setDate(Long date) {
		this.date = date;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public int getTeacherPayout() {
		return teacherPayout;
	}

	public void setTeacherPayout(int teacherPayout) {
		this.teacherPayout = teacherPayout;
	}

	public int getCut() {
		return cut;
	}

	public void setCut(int cut) {
		this.cut = cut;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public SessionEntity getEntity() {
		return entity;
	}

	public void setEntity(SessionEntity entity) {
		this.entity = entity;
	}

	@Override
	public String toString() {
		return "SessionPayout{" +
				"sessionId=" + sessionId +
				", subscriptionId=" + subscriptionId +
				", billingDuration=" + billingDuration +
				", sessionDuration=" + sessionDuration +
				", date=" + date +
				", model=" + model +
				", teacherPayout=" + teacherPayout +
				", teacherPromotionalPayout=" + teacherPromotionalPayout +
				", teacherNonPromotionalPayout=" + teacherNonPromotionalPayout +
				", cut=" + cut +
				", promotionalCut=" + promotionalCut +
				", nonPromotionalCut=" + nonPromotionalCut +
				", teacherId=" + teacherId +
				", studentId=" + studentId +
				", entity=" + entity +
				", entityId='" + entityId + '\'' +
				", hourlyRate=" + hourlyRate +
				", isUpdated=" + isUpdated +
				"} " + super.toString();
	}

	public SessionPayout(Long id, Long creationTime, String createdBy, Long lastUpdatedTime, String lastUpdatedby,
						 Boolean enabled, String callingUserId) {
		super(id, creationTime, createdBy, lastUpdatedTime, lastUpdatedby, enabled, callingUserId);
		// TODO Auto-generated constructor stub
	}

	public SessionPayout(Long sessionId, Long subscriptionId, Long billingDuration, Long sessionDuration, Long date,
			SessionModel model, int teacherPayout, int teacherPromotionalPayout, int teacherNonPromotionalPayout,
			int cut, int promotionalCut, int nonPromotionalCut, Long teacherId, Long studentId, SessionEntity entity,
			String entityId, Long hourlyRate) {
		super();
		this.sessionId = sessionId;
		this.subscriptionId = subscriptionId;
		this.billingDuration = billingDuration;
		this.sessionDuration = sessionDuration;
		this.date = date;
		this.model = model;
		this.teacherPayout = teacherPayout;
		this.teacherPromotionalPayout = teacherPromotionalPayout;
		this.teacherNonPromotionalPayout = teacherNonPromotionalPayout;
		this.cut = cut;
		this.promotionalCut = promotionalCut;
		this.nonPromotionalCut = nonPromotionalCut;
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.entity = entity;
		this.entityId = entityId;
		this.hourlyRate = hourlyRate;
	}

	public int getTeacherPromotionalPayout() {
		return teacherPromotionalPayout;
	}

	public void setTeacherPromotionalPayout(int teacherPromotionalPayout) {
		this.teacherPromotionalPayout = teacherPromotionalPayout;
	}

	public int getTeacherNonPromotionalPayout() {
		return teacherNonPromotionalPayout;
	}

	public void setTeacherNonPromotionalPayout(int teacherNonPromotionalPayout) {
		this.teacherNonPromotionalPayout = teacherNonPromotionalPayout;
	}

	public int getPromotionalCut() {
		return promotionalCut;
	}

	public void setPromotionalCut(int promotionalCut) {
		this.promotionalCut = promotionalCut;
	}

	public int getNonPromotionalCut() {
		return nonPromotionalCut;
	}

	public void setNonPromotionalCut(int nonPromotionalCut) {
		this.nonPromotionalCut = nonPromotionalCut;
	}

	public Long getBillingDuration() {
		return billingDuration;
	}

	public void setBillingDuration(Long billingDuration) {
		this.billingDuration = billingDuration;
	}

	public Long getSessionDuration() {
		return sessionDuration;
	}

	public void setSessionDuration(Long sessionDuration) {
		this.sessionDuration = sessionDuration;
	}

	public Boolean getUpdated() {
		return isUpdated;
	}

	public void setUpdated(Boolean updated) {
		isUpdated = updated;
	}

	public SessionPayout() {
		super();
		// TODO Auto-generated constructor stub
	}

}
