package com.vedantu.platform.pojo.scheduling;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class ElasticSearchResponseParser {

	public static ElasticSearchResponseParser INSTANCE = new ElasticSearchResponseParser();
	
	public ElasticSearchResponseParser() {
	}

	public ESSearchResponse parse(String json) {

		ESSearchResponse response = new ESSearchResponse();
		
		JsonElement jelement = new JsonParser().parse(json);
	    JsonObject  jobject = jelement.getAsJsonObject();
	    jobject = jobject.getAsJsonObject("hits");
	    int totalHits = 0;
	    if(jobject.get("total")!=null){
	    totalHits = Integer.parseInt(jobject.get("total").getAsString());
	    }
	    response.setTotalHits(totalHits);
	    JsonArray jarray = jobject.getAsJsonArray("hits");
	    
	    List<EsTeacherInfo> teacherData = new ArrayList<>();
	    for(JsonElement element : jarray){
	    	JsonObject jsonObject = element.getAsJsonObject();	    		    	
	    	EsTeacherInfo result = new Gson().fromJson(jsonObject.getAsJsonObject("_source"), EsTeacherInfo.class);
                result.setRating(result.getRating());
                if(result.getSessionHours()!=null){
                    result.setSessionHours((long)Math.ceil(result.getSessionHours().floatValue()/3600000));
                }
                JsonElement _scoreJson = jsonObject.get("_score");
                if(_scoreJson!=null){
                    String _score = _scoreJson.toString();
                    result.set_score(Float.parseFloat(_score));                    
                }
                teacherData.add(result);
	    }
	    response.setTeacherData(teacherData);
	    return response;
	}
}