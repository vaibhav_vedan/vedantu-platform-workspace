/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.wave;

import java.util.Objects;

/**
 *
 * @author arjun
 */
public class NodeAddress {

    private String ip;
    private String port;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public NodeAddress(String ip, String port) {
        this.ip = ip;
        this.port = port;
    }

    public NodeAddress() {
        super();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NodeAddress other = (NodeAddress) obj;
        if (!Objects.equals(this.ip, other.ip)) {
            return false;
        }
        return Objects.equals(this.port, other.port);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.ip);
        hash = 97 * hash + Objects.hashCode(this.port);
        return hash;
    }

    @Override
    public String toString() {
        return "NodeAddress{" + "ip=" + ip + ", port=" + port + '}';
    }

}
