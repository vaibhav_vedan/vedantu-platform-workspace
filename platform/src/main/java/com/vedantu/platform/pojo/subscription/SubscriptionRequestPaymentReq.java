package com.vedantu.platform.pojo.subscription;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.List;


public class SubscriptionRequestPaymentReq extends AbstractFrontEndReq{
	private Long userId;
	private Long amount; // in paisa
	private Long subscriptionRequestId;
	private String paymentDetails;
        private String redirectUrl;

	public SubscriptionRequestPaymentReq() {
		super();
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public Long getSubscriptionRequestId() {
		return subscriptionRequestId;
	}

	public void setSubscriptionRequestId(Long subscriptionRequestId) {
		this.subscriptionRequestId = subscriptionRequestId;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public List<String> collectErrors() {
		List<String> errors = new ArrayList<String>();
		if (this.userId == null || this.userId <= 0l) {
			errors.add("userId");
		}
		if (this.subscriptionRequestId == null || this.subscriptionRequestId <= 0l) {
			errors.add("subscriptionRequestId");
		}

		return errors;
	}

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }
        
        

	@Override
	public String toString() {
		return "SubscriptionRequestPaymentReq [userId=" + userId + ", amount=" + amount + ", subscriptionRequestId="
				+ subscriptionRequestId + ", paymentDetails=" + paymentDetails + "]";
	}
}
