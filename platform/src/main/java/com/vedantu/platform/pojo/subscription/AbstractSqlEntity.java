package com.vedantu.platform.pojo.subscription;

import com.vedantu.util.BasicResponse;


//This pojo is not removed because of a usage which needs a version which extends BasicResponse
public class AbstractSqlEntity extends BasicResponse{

	public Long id;
	public Long creationTime;
	public String createdBy;
	public Long lastUpdatedTime;
	public String lastUpdatedby;
	private Boolean enabled;
	private String callingUser;

	public AbstractSqlEntity() {
		super();
	}

	public AbstractSqlEntity(Long id, Long creationTime, String createdBy, Long lastUpdatedTime, String lastUpdatedby,
			Boolean enabled, String callingUser) {
		super();
		this.id = id;
		this.creationTime = creationTime;
		this.createdBy = createdBy;
		this.lastUpdatedTime = lastUpdatedTime;
		this.lastUpdatedby = lastUpdatedby;
		this.enabled = enabled;
		this.callingUser = callingUser;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Long getLastUpdatedTime() {
		return lastUpdatedTime;
	}

	public void setLastUpdatedTime(Long lastUpdatedTime) {
		this.lastUpdatedTime = lastUpdatedTime;
	}

	public String getLastUpdatedby() {
		return lastUpdatedby;
	}

	public void setLastUpdatedby(String lastUpdatedby) {
		this.lastUpdatedby = lastUpdatedby;
	}
	
	 public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public String getCallingUser() {
		return callingUser;
	}

	public void setCallingUser(String callingUser) {
		this.callingUser = callingUser;
	}

	@Override
	public String toString() {
		return "AbstractSqlEntity [id=" + id + ", creationTime=" + creationTime + ", createdBy=" + createdBy
				+ ", lastUpdatedTime=" + lastUpdatedTime + ", lastUpdatedby=" + lastUpdatedby + ", enabled=" + enabled
				+ ", callingUser=" + callingUser + "]";
	}

	public static class Constants {
		public static final String ID = "id";
		public static final String CREATION_TIME = "creationTime";
		public static final String CREATED_BY = "createdBy";
		public static final String LAST_UPDATED_TIME = "lastUpdatedTime";
		public static final String LAST_UPDATED_BY = "lastUpdatedby";
		public static final String ENABLED = "enabled";
	}

}
