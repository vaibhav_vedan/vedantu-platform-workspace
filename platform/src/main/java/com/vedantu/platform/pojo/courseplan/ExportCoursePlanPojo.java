package com.vedantu.platform.pojo.courseplan;

import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.util.security.HttpSessionData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mano
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ExportCoursePlanPojo {
    private ExportCoursePlansReq exportCoursePlansReq;
    private HttpSessionData sessionData;
}
