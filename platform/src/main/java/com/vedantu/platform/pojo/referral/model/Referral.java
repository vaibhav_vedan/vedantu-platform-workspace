package com.vedantu.platform.pojo.referral.model;

import java.util.List;

import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Referral extends AbstractMongoStringIdEntity {

	private Long userId;
	private Long referrerUserId;
	private String referralCode;
	private Long referralPolicyId;
	private List<ReferralStep> stepsDone;
	private Long totalAmountToReferrer;
	private Long totalAmountToReferee;
	private String entityId;
//	private DeliverableEntityType entityType;

	public Referral() {
		super();
		this.totalAmountToReferee = 0l;
		this.totalAmountToReferrer = 0l;
	}

	public Referral(Long userId, Long referrerUserId, String referralCode,
			Long referralPolicyId, List<ReferralStep> stepsDone,
			Long totalAmountToReferrer, Long totalAmountToReferee) {
		super();
		this.userId = userId;
		this.referrerUserId = referrerUserId;
		this.referralCode = referralCode;
		this.referralPolicyId = referralPolicyId;
		this.stepsDone = stepsDone;
		this.totalAmountToReferrer = totalAmountToReferrer;
		this.totalAmountToReferee = totalAmountToReferee;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getReferrerUserId() {
		return referrerUserId;
	}

	public void setReferrerUserId(Long referrerUserId) {
		this.referrerUserId = referrerUserId;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public Long getReferralPolicyId() {
		return referralPolicyId;
	}

	public void setReferralPolicyId(Long referralPolicyId) {
		this.referralPolicyId = referralPolicyId;
	}

	public List<ReferralStep> getStepsDone() {
		return stepsDone;
	}

	public void setStepsDone(List<ReferralStep> stepsDone) {
		this.stepsDone = stepsDone;
	}

	public Long getTotalAmountToReferrer() {
		return totalAmountToReferrer;
	}

	public void setTotalAmountToReferrer(Long totalAmountToReferrer) {
		this.totalAmountToReferrer = totalAmountToReferrer;
	}

	public Long getTotalAmountToReferee() {
		return totalAmountToReferee;
	}

	public void setTotalAmountToReferee(Long totalAmountToReferee) {
		this.totalAmountToReferee = totalAmountToReferee;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}


	@Override
	public String toString() {
		return "Referral [userId=" + userId + ", referrerUserId="
				+ referrerUserId + ", referralCode=" + referralCode
				+ ", referralPolicyId=" + referralPolicyId + ", stepsDone="
				+ stepsDone + ", totalAmountToReferrer="
				+ totalAmountToReferrer + ", totalAmountToReferee="
				+ totalAmountToReferee + "]";
	}

}
