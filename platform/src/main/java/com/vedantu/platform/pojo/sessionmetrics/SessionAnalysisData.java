/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.sessionmetrics;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;
import java.util.Map;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author jeet
 */
@Document(collection = "SessionAnalysisData")
public class SessionAnalysisData extends AbstractMongoStringIdEntity {
    @Indexed(background = true)
    private String sessionId;
    private String sessionType;
    @Indexed(background = true)
    private String userId;
    private String otherUserId;
    private String role;
    private String name;
    private String email;
    private String otherUserEmail;
    private String firstName;
    private String lastName;
    private String state;
    private String displayState;
    private Long joinTime;
    @Indexed(background = true)
    private Long startTime;
    private Long endTime;
    private String subject;
    private String topic;
    private Long billingPeriod;
    private Long teacherRating;
    private Long techRating;
    private String techRatingReason;
    private Map sessionErrors;
    private List<String> publicIPs;
    private List<String> callServices;
    private Long totalCallDuration;
    private Long totalScreenShareDuration;
    private Long socketDisconnectedAudioActive;
    private List<Map> ispDetails;
    private String browserName;
    private String browserVersion;
    private Long multipleConnectionCount;
    private Map reconnectionTimeReq;
    private Map disconnectionMap;
    private Long offlineTime;
    private Long offlineTimeNormalized;
    private Long disconnections;
    private Long disconnectionsNormalized;
    private Long totalSessionInactiveTime;
    private Long totalSessionInactiveTimeNormalized;
    private Long audioDisconnectedTimes;
    private Long audioDisconnectedTimesNormalized;
    private Long audioDisconnectionTime;
    private Long audioDisconnectionTimeNormalized;
    private Map audioDisconnectedBuckets;
    private Long highPacketLossTime;
    private Long highPacketLossTimeNormalized;
    private Long poorAudioQualityTime; //seconds
    private Long poorAudioQualityTimeNormalized;
    private Long avgRtt;
    private Long avgJitter;
    private Long avgPacketLoss;
    private List<Map> poorAudioQualityList;
    private boolean sessionOnMobile;
    private Long timeOnMobile;
    private Long refreshCount;
    private Long phoneCallCount;
    private Long avgCallConnectionTime;
    private Long callsCount;
    private Long callsCountNormalized;
    private Long audioCallTries;
    private Long videoCallTries;
    private Long callRequestDeclined;
    private Map iceCandidates;
    private Long joinDelay;
    private Long timeTakenInitiateFirstCall;
    private Long avgCallAcceptTime;
    private Long avgDownloadSpeedAfterDisconnect;
    private Long avgUploadSpeedAfterDisconnect;
    private boolean wbActivityFound;
    private boolean analysable;
    private Long whiteboardPauses;
    private Long textchatCount;
    private Map turnStats;
    private String turnServerUsed;
    private String startTimeStr;
    private Long speedTestStartTime;
    private Long speedTestDuration;
    private Long speedTestFileCount;
    private Map downloadTest;
    private Map uploadTest;
    private Long exotelCallDuration;
    private Long exotelCallCount;
    private List<Map> exotelCallDetails;

    public SessionAnalysisData() {
        super();
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getSessionType() {
        return sessionType;
    }

    public void setSessionType(String sessionType) {
        this.sessionType = sessionType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOtherUserId() {
        return otherUserId;
    }

    public void setOtherUserId(String otherUserId) {
        this.otherUserId = otherUserId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getOtherUserEmail() {
        return otherUserEmail;
    }

    public void setOtherUserEmail(String otherUserEmail) {
        this.otherUserEmail = otherUserEmail;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDisplayState() {
        return displayState;
    }

    public void setDisplayState(String displayState) {
        this.displayState = displayState;
    }

    public Long getJoinTime() {
        return joinTime;
    }

    public void setJoinTime(Long joinTime) {
        this.joinTime = joinTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Long getBillingPeriod() {
        return billingPeriod;
    }

    public void setBillingPeriod(Long billingPeriod) {
        this.billingPeriod = billingPeriod;
    }

    public Long getTeacherRating() {
        return teacherRating;
    }

    public void setTeacherRating(Long teacherRating) {
        this.teacherRating = teacherRating;
    }

    public Long getTechRating() {
        return techRating;
    }

    public void setTechRating(Long techRating) {
        this.techRating = techRating;
    }

    public String getTechRatingReason() {
        return techRatingReason;
    }

    public void setTechRatingReason(String techRatingReason) {
        this.techRatingReason = techRatingReason;
    }

    public Map getSessionErrors() {
        return sessionErrors;
    }

    public void setSessionErrors(Map sessionErrors) {
        this.sessionErrors = sessionErrors;
    }

    public List<String> getPublicIPs() {
        return publicIPs;
    }

    public void setPublicIPs(List<String> publicIPs) {
        this.publicIPs = publicIPs;
    }

    public List<String> getCallServices() {
        return callServices;
    }

    public void setCallServices(List<String> callServices) {
        this.callServices = callServices;
    }

    public Long getTotalCallDuration() {
        return totalCallDuration;
    }

    public void setTotalCallDuration(Long totalCallDuration) {
        this.totalCallDuration = totalCallDuration;
    }

    public Long getTotalScreenShareDuration() {
        return totalScreenShareDuration;
    }

    public void setTotalScreenShareDuration(Long totalScreenShareDuration) {
        this.totalScreenShareDuration = totalScreenShareDuration;
    }

    public Long getSocketDisconnectedAudioActive() {
        return socketDisconnectedAudioActive;
    }

    public void setSocketDisconnectedAudioActive(Long socketDisconnectedAudioActive) {
        this.socketDisconnectedAudioActive = socketDisconnectedAudioActive;
    }

    public List<Map> getIspDetails() {
        return ispDetails;
    }

    public void setIspDetails(List<Map> ispDetails) {
        this.ispDetails = ispDetails;
    }

    public String getBrowserName() {
        return browserName;
    }

    public void setBrowserName(String browserName) {
        this.browserName = browserName;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public void setBrowserVersion(String browserVersion) {
        this.browserVersion = browserVersion;
    }

    public Long getMultipleConnectionCount() {
        return multipleConnectionCount;
    }

    public void setMultipleConnectionCount(Long multipleConnectionCount) {
        this.multipleConnectionCount = multipleConnectionCount;
    }

    public Map getReconnectionTimeReq() {
        return reconnectionTimeReq;
    }

    public void setReconnectionTimeReq(Map reconnectionTimeReq) {
        this.reconnectionTimeReq = reconnectionTimeReq;
    }

    public Map getDisconnectionMap() {
        return disconnectionMap;
    }

    public void setDisconnectionMap(Map disconnectionMap) {
        this.disconnectionMap = disconnectionMap;
    }

    public Long getOfflineTime() {
        return offlineTime;
    }

    public void setOfflineTime(Long offlineTime) {
        this.offlineTime = offlineTime;
    }

    public Long getOfflineTimeNormalized() {
        return offlineTimeNormalized;
    }

    public void setOfflineTimeNormalized(Long offlineTimeNormalized) {
        this.offlineTimeNormalized = offlineTimeNormalized;
    }

    public Long getDisconnections() {
        return disconnections;
    }

    public void setDisconnections(Long disconnections) {
        this.disconnections = disconnections;
    }

    public Long getDisconnectionsNormalized() {
        return disconnectionsNormalized;
    }

    public void setDisconnectionsNormalized(Long disconnectionsNormalized) {
        this.disconnectionsNormalized = disconnectionsNormalized;
    }

    public Long getTotalSessionInactiveTime() {
        return totalSessionInactiveTime;
    }

    public void setTotalSessionInactiveTime(Long totalSessionInactiveTime) {
        this.totalSessionInactiveTime = totalSessionInactiveTime;
    }

    public Long getTotalSessionInactiveTimeNormalized() {
        return totalSessionInactiveTimeNormalized;
    }

    public void setTotalSessionInactiveTimeNormalized(Long totalSessionInactiveTimeNormalized) {
        this.totalSessionInactiveTimeNormalized = totalSessionInactiveTimeNormalized;
    }

    public Long getAudioDisconnectedTimes() {
        return audioDisconnectedTimes;
    }

    public void setAudioDisconnectedTimes(Long audioDisconnectedTimes) {
        this.audioDisconnectedTimes = audioDisconnectedTimes;
    }

    public Long getAudioDisconnectedTimesNormalized() {
        return audioDisconnectedTimesNormalized;
    }

    public void setAudioDisconnectedTimesNormalized(Long audioDisconnectedTimesNormalized) {
        this.audioDisconnectedTimesNormalized = audioDisconnectedTimesNormalized;
    }

    public Long getAudioDisconnectionTime() {
        return audioDisconnectionTime;
    }

    public void setAudioDisconnectionTime(Long audioDisconnectionTime) {
        this.audioDisconnectionTime = audioDisconnectionTime;
    }

    public Long getAudioDisconnectionTimeNormalized() {
        return audioDisconnectionTimeNormalized;
    }

    public void setAudioDisconnectionTimeNormalized(Long audioDisconnectionTimeNormalized) {
        this.audioDisconnectionTimeNormalized = audioDisconnectionTimeNormalized;
    }

    public Map getAudioDisconnectedBuckets() {
        return audioDisconnectedBuckets;
    }

    public void setAudioDisconnectedBuckets(Map audioDisconnectedBuckets) {
        this.audioDisconnectedBuckets = audioDisconnectedBuckets;
    }

    public Long getHighPacketLossTime() {
        return highPacketLossTime;
    }

    public void setHighPacketLossTime(Long highPacketLossTime) {
        this.highPacketLossTime = highPacketLossTime;
    }

    public Long getHighPacketLossTimeNormalized() {
        return highPacketLossTimeNormalized;
    }

    public void setHighPacketLossTimeNormalized(Long highPacketLossTimeNormalized) {
        this.highPacketLossTimeNormalized = highPacketLossTimeNormalized;
    }

    public Long getPoorAudioQualityTime() {
        return poorAudioQualityTime;
    }

    public void setPoorAudioQualityTime(Long poorAudioQualityTime) {
        this.poorAudioQualityTime = poorAudioQualityTime;
    }

    public Long getPoorAudioQualityTimeNormalized() {
        return poorAudioQualityTimeNormalized;
    }

    public void setPoorAudioQualityTimeNormalized(Long poorAudioQualityTimeNormalized) {
        this.poorAudioQualityTimeNormalized = poorAudioQualityTimeNormalized;
    }

    public Long getAvgRtt() {
        return avgRtt;
    }

    public void setAvgRtt(Long avgRtt) {
        this.avgRtt = avgRtt;
    }

    public Long getAvgJitter() {
        return avgJitter;
    }

    public void setAvgJitter(Long avgJitter) {
        this.avgJitter = avgJitter;
    }

    public Long getAvgPacketLoss() {
        return avgPacketLoss;
    }

    public void setAvgPacketLoss(Long avgPacketLoss) {
        this.avgPacketLoss = avgPacketLoss;
    }

    public List<Map> getPoorAudioQualityList() {
        return poorAudioQualityList;
    }

    public void setPoorAudioQualityList(List<Map> poorAudioQualityList) {
        this.poorAudioQualityList = poorAudioQualityList;
    }

    public boolean isSessionOnMobile() {
        return sessionOnMobile;
    }

    public void setSessionOnMobile(boolean sessionOnMobile) {
        this.sessionOnMobile = sessionOnMobile;
    }

    public Long getTimeOnMobile() {
        return timeOnMobile;
    }

    public void setTimeOnMobile(Long timeOnMobile) {
        this.timeOnMobile = timeOnMobile;
    }

    public Long getRefreshCount() {
        return refreshCount;
    }

    public void setRefreshCount(Long refreshCount) {
        this.refreshCount = refreshCount;
    }

    public Long getPhoneCallCount() {
        return phoneCallCount;
    }

    public void setPhoneCallCount(Long phoneCallCount) {
        this.phoneCallCount = phoneCallCount;
    }

    public Long getAvgCallConnectionTime() {
        return avgCallConnectionTime;
    }

    public void setAvgCallConnectionTime(Long avgCallConnectionTime) {
        this.avgCallConnectionTime = avgCallConnectionTime;
    }

    public Long getCallsCount() {
        return callsCount;
    }

    public void setCallsCount(Long callsCount) {
        this.callsCount = callsCount;
    }

    public Long getCallsCountNormalized() {
        return callsCountNormalized;
    }

    public void setCallsCountNormalized(Long callsCountNormalized) {
        this.callsCountNormalized = callsCountNormalized;
    }

    public Long getAudioCallTries() {
        return audioCallTries;
    }

    public void setAudioCallTries(Long audioCallTries) {
        this.audioCallTries = audioCallTries;
    }

    public Long getVideoCallTries() {
        return videoCallTries;
    }

    public void setVideoCallTries(Long videoCallTries) {
        this.videoCallTries = videoCallTries;
    }

    public Long getCallRequestDeclined() {
        return callRequestDeclined;
    }

    public void setCallRequestDeclined(Long callRequestDeclined) {
        this.callRequestDeclined = callRequestDeclined;
    }

    public Map getIceCandidates() {
        return iceCandidates;
    }

    public void setIceCandidates(Map iceCandidates) {
        this.iceCandidates = iceCandidates;
    }

    public Long getJoinDelay() {
        return joinDelay;
    }

    public void setJoinDelay(Long joinDelay) {
        this.joinDelay = joinDelay;
    }

    public Long getTimeTakenInitiateFirstCall() {
        return timeTakenInitiateFirstCall;
    }

    public void setTimeTakenInitiateFirstCall(Long timeTakenInitiateFirstCall) {
        this.timeTakenInitiateFirstCall = timeTakenInitiateFirstCall;
    }

    public Long getAvgCallAcceptTime() {
        return avgCallAcceptTime;
    }

    public void setAvgCallAcceptTime(Long avgCallAcceptTime) {
        this.avgCallAcceptTime = avgCallAcceptTime;
    }

    public Long getAvgDownloadSpeedAfterDisconnect() {
        return avgDownloadSpeedAfterDisconnect;
    }

    public void setAvgDownloadSpeedAfterDisconnect(Long avgDownloadSpeedAfterDisconnect) {
        this.avgDownloadSpeedAfterDisconnect = avgDownloadSpeedAfterDisconnect;
    }

    public Long getAvgUploadSpeedAfterDisconnect() {
        return avgUploadSpeedAfterDisconnect;
    }

    public void setAvgUploadSpeedAfterDisconnect(Long avgUploadSpeedAfterDisconnect) {
        this.avgUploadSpeedAfterDisconnect = avgUploadSpeedAfterDisconnect;
    }

    public boolean isWbActivityFound() {
        return wbActivityFound;
    }

    public void setWbActivityFound(boolean wbActivityFound) {
        this.wbActivityFound = wbActivityFound;
    }

    public boolean isAnalysable() {
        return analysable;
    }

    public void setAnalysable(boolean analysable) {
        this.analysable = analysable;
    }

    public Long getWhiteboardPauses() {
        return whiteboardPauses;
    }

    public void setWhiteboardPauses(Long whiteboardPauses) {
        this.whiteboardPauses = whiteboardPauses;
    }

    public Long getTextchatCount() {
        return textchatCount;
    }

    public void setTextchatCount(Long textchatCount) {
        this.textchatCount = textchatCount;
    }

    public Map getTurnStats() {
        return turnStats;
    }

    public void setTurnStats(Map turnStats) {
        this.turnStats = turnStats;
    }

    public String getTurnServerUsed() {
        return turnServerUsed;
    }

    public void setTurnServerUsed(String turnServerUsed) {
        this.turnServerUsed = turnServerUsed;
    }
    
    public String getStartTimeStr() {
        return startTimeStr;
    }

    public void setStartTimeStr(String startTimeStr) {
        this.startTimeStr = startTimeStr;
    }

    public Long getSpeedTestStartTime() {
        return speedTestStartTime;
    }

    public void setSpeedTestStartTime(Long speedTestStartTime) {
        this.speedTestStartTime = speedTestStartTime;
    }

    public Long getSpeedTestDuration() {
        return speedTestDuration;
    }

    public void setSpeedTestDuration(Long speedTestDuration) {
        this.speedTestDuration = speedTestDuration;
    }

    public Long getSpeedTestFileCount() {
        return speedTestFileCount;
    }

    public void setSpeedTestFileCount(Long speedTestFileCount) {
        this.speedTestFileCount = speedTestFileCount;
    }

    public Map getDownloadTest() {
        return downloadTest;
    }

    public void setDownloadTest(Map downloadTest) {
        this.downloadTest = downloadTest;
    }

    public Map getUploadTest() {
        return uploadTest;
    }

    public void setUploadTest(Map uploadTest) {
        this.uploadTest = uploadTest;
    }

    public Long getExotelCallDuration() {
        return exotelCallDuration;
    }

    public void setExotelCallDuration(Long exotelCallDuration) {
        this.exotelCallDuration = exotelCallDuration;
    }

    public Long getExotelCallCount() {
        return exotelCallCount;
    }

    public void setExotelCallCount(Long exotelCallCount) {
        this.exotelCallCount = exotelCallCount;
    }

    public List<Map> getExotelCallDetails() {
        return exotelCallDetails;
    }

    public void setExotelCallDetails(List<Map> exotelCallDetails) {
        this.exotelCallDetails = exotelCallDetails;
    }

    @Override
    public String toString() {
        return "SessionAnalysisData{" + "sessionId=" + sessionId + ", sessionType=" + sessionType + ", userId=" + userId + ", otherUserId=" + otherUserId + ", role=" + role + ", name=" + name + ", email=" + email + ", otherUserEmail=" + otherUserEmail + ", firstName=" + firstName + ", lastName=" + lastName + ", state=" + state + ", displayState=" + displayState + ", joinTime=" + joinTime + ", startTime=" + startTime + ", endTime=" + endTime + ", subject=" + subject + ", topic=" + topic + ", billingPeriod=" + billingPeriod + ", teacherRating=" + teacherRating + ", techRating=" + techRating + ", techRatingReason=" + techRatingReason + ", sessionErrors=" + sessionErrors + ", publicIPs=" + publicIPs + ", callServices=" + callServices + ", totalCallDuration=" + totalCallDuration + ", totalScreenShareDuration=" + totalScreenShareDuration + ", socketDisconnectedAudioActive=" + socketDisconnectedAudioActive + ", ispDetails=" + ispDetails + ", browserName=" + browserName + ", browserVersion=" + browserVersion + ", multipleConnectionCount=" + multipleConnectionCount + ", reconnectionTimeReq=" + reconnectionTimeReq + ", disconnectionMap=" + disconnectionMap + ", offlineTime=" + offlineTime + ", offlineTimeNormalized=" + offlineTimeNormalized + ", disconnections=" + disconnections + ", disconnectionsNormalized=" + disconnectionsNormalized + ", totalSessionInactiveTime=" + totalSessionInactiveTime + ", totalSessionInactiveTimeNormalized=" + totalSessionInactiveTimeNormalized + ", audioDisconnectedTimes=" + audioDisconnectedTimes + ", audioDisconnectedTimesNormalized=" + audioDisconnectedTimesNormalized + ", audioDisconnectionTime=" + audioDisconnectionTime + ", audioDisconnectionTimeNormalized=" + audioDisconnectionTimeNormalized + ", audioDisconnectedBuckets=" + audioDisconnectedBuckets + ", highPacketLossTime=" + highPacketLossTime + ", highPacketLossTimeNormalized=" + highPacketLossTimeNormalized + ", poorAudioQualityTime=" + poorAudioQualityTime + ", poorAudioQualityTimeNormalized=" + poorAudioQualityTimeNormalized + ", avgRtt=" + avgRtt + ", avgJitter=" + avgJitter + ", avgPacketLoss=" + avgPacketLoss + ", poorAudioQualityList=" + poorAudioQualityList + ", sessionOnMobile=" + sessionOnMobile + ", timeOnMobile=" + timeOnMobile + ", refreshCount=" + refreshCount + ", phoneCallCount=" + phoneCallCount + ", avgCallConnectionTime=" + avgCallConnectionTime + ", callsCount=" + callsCount + ", callsCountNormalized=" + callsCountNormalized + ", audioCallTries=" + audioCallTries + ", videoCallTries=" + videoCallTries + ", callRequestDeclined=" + callRequestDeclined + ", iceCandidates=" + iceCandidates + ", joinDelay=" + joinDelay + ", timeTakenInitiateFirstCall=" + timeTakenInitiateFirstCall + ", avgCallAcceptTime=" + avgCallAcceptTime + ", avgDownloadSpeedAfterDisconnect=" + avgDownloadSpeedAfterDisconnect + ", avgUploadSpeedAfterDisconnect=" + avgUploadSpeedAfterDisconnect + ", wbActivityFound=" + wbActivityFound + ", analysable=" + analysable + ", whiteboardPauses=" + whiteboardPauses + ", textchatCount=" + textchatCount + ", turnStats=" + turnStats + ", turnServerUsed=" + turnServerUsed  + ", startTimeStr=" + startTimeStr + ", speedTestStartTime=" + speedTestStartTime + ", speedTestDuration=" + speedTestDuration + ", speedTestFileCount=" + speedTestFileCount + ", downloadTest=" + downloadTest + ", uploadTest=" + uploadTest + ", exotelCallDuration=" + exotelCallDuration + ", exotelCallCount=" + exotelCallCount + ", exotelCallDetails=" + exotelCallDetails + '}';
    }
    
    
}
