/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.scheduling;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author somil
 */
public class SendTrialReminderReq extends AbstractFrontEndReq {

    List<SessionParticipantsId> userIds;

    public SendTrialReminderReq() {
    }
    
    public SendTrialReminderReq(HttpServletRequest req) {
        StringBuilder jb = new StringBuilder();
        try {
            userIds = new ArrayList<>();
            
            String line = null;
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
            
            Logger.getLogger(SendTrialReminderReq.class.getName()).info("body "+jb.toString());
                JsonElement jelement = new JsonParser().parse(jb.toString());
                JsonArray jArray = jelement.getAsJsonArray();

                for (JsonElement element : jArray) {
                    JsonObject jobject = element.getAsJsonObject();
                    JsonObject data = jobject.getAsJsonObject("Data");
                    Long studentId = Long.parseLong(data.get("mx_Custom_1").getAsString().trim());
                    Long teacherId = Long.parseLong(data.get("mx_Custom_2").getAsString().trim());
                    userIds.add(new SessionParticipantsId(studentId, teacherId));
                }
         } catch (Exception e) {
            Logger.getLogger( SendTrialReminderReq.class.getName() ).severe(e.toString());
        }
        
    }

    public List<SessionParticipantsId> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<SessionParticipantsId> userId) {
        this.userIds = userId;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == userIds) {
            errors.add("userId");
        }

        return errors;
    }

}
