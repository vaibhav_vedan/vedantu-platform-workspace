package com.vedantu.platform.pojo.subscription;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.request.LockBalanceReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;

public class SubscriptionRequestPaymentDetails extends LockBalanceReq {

    public SubscriptionRequestPaymentDetails() {
        super();
    }

    public void updateDetails(SubscriptionRequestInfo subscriptionRequestInfo) {
        super.setStudentId(subscriptionRequestInfo.getStudentId());
        super.setTeacherId(subscriptionRequestInfo.getTeacherId());
        super.setEntityId(subscriptionRequestInfo.getPlanId());
        super.setEntityType(EntityType.PLAN);
        super.setTotalHours(subscriptionRequestInfo.getTotalHours());

        //if the paymentdetails do not contain, paymenttype
        if (super.getPaymentType() == null) {
            String paymentDetails = subscriptionRequestInfo.getPaymentDetails();
            JsonObject json = new Gson().fromJson(paymentDetails, JsonObject.class);
            PaymentType paymentType = PaymentType.BULK;

            if (json.get("paymentType") != null
                    && PaymentType.valueOf(json.get("paymentType").getAsString()) != null) {
                paymentType = PaymentType.valueOf(json.get("paymentType").getAsString());
            }
            super.setPaymentType(paymentType);
            if(json.get("vedantuDiscountCouponId")!=null){
                super.setVedantuDiscountCouponId(json.get("vedantuDiscountCouponId").getAsString());
            }
            if(json.get("teacherDiscountCouponId")!=null){
                super.setTeacherDiscountCouponId(json.get("teacherDiscountCouponId").getAsString());
            }            
        }
        SessionSchedule schedule = new Gson().fromJson(subscriptionRequestInfo.getSlots(), SessionSchedule.class);
        super.setSessionSchedule(schedule);
        super.setPurchaseFlowType(PurchaseFlowType.SUBSCRIPTION_REQUEST);
        super.setPurchaseFlowId(subscriptionRequestInfo.getId().toString());
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
