package com.vedantu.platform.pojo.scheduling;

import com.vedantu.listing.pojo.EsTeacherData;


public class EsTeacherInfo extends EsTeacherData{

	public Float _score;

	public EsTeacherInfo() {
		super();
	}

	public Float get_score() {
		return _score;
	}

	public void set_score(Float _score) {
		this._score = _score;
	}

}

