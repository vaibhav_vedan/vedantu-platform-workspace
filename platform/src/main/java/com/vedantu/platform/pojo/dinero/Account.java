package com.vedantu.platform.pojo.dinero;

import com.vedantu.platform.enums.HolderType;
import com.vedantu.util.dbentitybeans.mysql.AbstractSqlEntityBean;

public class Account extends AbstractSqlEntityBean {

	private String holderId;
	private String holderName;
	private HolderType holderType;
	private Integer balance;
	private Integer promotionalBalance;
	private Integer nonPromotionalBalance;
	private Integer lockedBalance;
	private Integer promotionalLockedBalance;
	private Integer nonPromotionalLockedBalance;

	public Account(String holderId, String holderName, HolderType holderType, Integer balance,
			Integer promotionalBalance, Integer nonPromotionalBalance, Integer lockedBalance,
			Integer promotionalLockedBalance, Integer nonPromotionalLockedBalance) {
		super();
		this.holderId = holderId;
		this.holderName = holderName;
		this.holderType = holderType;
		this.balance = balance;
		this.promotionalBalance = promotionalBalance;
		this.nonPromotionalBalance = nonPromotionalBalance;
		this.lockedBalance = lockedBalance;
		this.promotionalLockedBalance = promotionalLockedBalance;
		this.nonPromotionalLockedBalance = nonPromotionalLockedBalance;
	}

	public Integer getPromotionalLockedBalance() {
		return promotionalLockedBalance;
	}

	public void setPromotionalLockedBalance(Integer promotionalLockedBalance) {
		this.promotionalLockedBalance = promotionalLockedBalance;
	}

	public Integer getNonPromotionalLockedBalance() {
		return nonPromotionalLockedBalance;
	}

	public void setNonPromotionalLockedBalance(Integer nonPromotionalLockedBalance) {
		this.nonPromotionalLockedBalance = nonPromotionalLockedBalance;
	}

	public Integer getPromotionalBalance() {
		return promotionalBalance;
	}

	public void setPromotionalBalance(Integer promotionalBalance) {
		this.promotionalBalance = promotionalBalance;
	}

	public Integer getNonPromotionalBalance() {
		return nonPromotionalBalance;
	}

	public void setNonPromotionalBalance(Integer nonPromotionalBalance) {
		this.nonPromotionalBalance = nonPromotionalBalance;
	}

	public static String __getUserAccountHolderId(String userId) {

		return "user/" + userId;
	}

	public static String __getVedantuAccountHolderId(String id) {

		return "vedantu/" + id;

	}

	public String getHolderId() {
		return holderId;
	}

	public void setHolderId(String holderId) {
		this.holderId = holderId;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = holderName;
	}

	public HolderType getHolderType() {
		return holderType;
	}

	public void setHolderType(HolderType holderType) {
		this.holderType = holderType;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getLockedBalance() {
		return lockedBalance;
	}

	public void setLockedBalance(Integer lockedBalance) {
		this.lockedBalance = lockedBalance;
	}

	public Account() {
		super();
	}

	public Account(Long id, Long creationTime, String createdBy, Long lastUpdatedTime, String lastUpdatedby,
			Boolean enabled, String callingUser) {
		super(id, creationTime, createdBy, lastUpdatedTime, lastUpdatedby, enabled, callingUser);
	}

	public void lockAmount(int cost) {
		if (cost < this.balance) {
			return;
		}

		int promotionalCost = 0;
		int nonPromotionalCost = 0;
		if (cost < this.promotionalBalance) {
			promotionalCost = cost;
		} else {
			promotionalCost = promotionalBalance;
			nonPromotionalCost = cost - promotionalCost;
		}

		lockAmount(promotionalCost, nonPromotionalCost);
	}

	public void unLockAmount(int promotionalCost, int nonPromotionalCost) {
		if (this.promotionalLockedBalance < promotionalCost || this.nonPromotionalLockedBalance < nonPromotionalCost) {
			return;
		}
		unlockPromotionalBalance(promotionalCost);
		unlockNonPromotionalBalance(nonPromotionalCost);
	}


	public void lockAmount(int promotionalCost, int nonPromotionalCost) {
		lockPromotionalBalance(promotionalCost);
		lockNonPromotionalBalance(nonPromotionalCost);
	}

	public void deductPromotionalBalance(int cost) {
		this.promotionalBalance -= cost;
		this.balance -= cost;
	}

	public void lockPromotionalBalance(int cost) {
		this.promotionalLockedBalance += cost;
		this.lockedBalance += cost;
		deductPromotionalBalance(cost);
	}

	public void deductNonPromotionalBalance(int cost) {
		this.nonPromotionalBalance -= cost;
		this.balance -= cost;
	}

	public void lockNonPromotionalBalance(int cost) {
		this.nonPromotionalLockedBalance += cost;
		this.lockedBalance += cost;
		deductNonPromotionalBalance(cost);
	}

	public void deductPromotionalLockedBalance(int cost) {
		this.promotionalLockedBalance -= cost;
		this.lockedBalance -= cost;
	}

	public void unlockPromotionalBalance(int cost) {
		this.promotionalBalance += cost;
		this.balance += cost;
		deductPromotionalLockedBalance(cost);
	}

	public void deductNonPromotionalLockedBalance(int cost) {
		this.nonPromotionalLockedBalance -= cost;
		this.lockedBalance -= cost;
	}

	public void unlockNonPromotionalBalance(int cost) {
		this.nonPromotionalBalance += cost;
		this.balance += cost;
		deductNonPromotionalLockedBalance(cost);
	}

	@Override
	public String toString() {
		return "Account [holderId=" + holderId + ", holderName=" + holderName + ", holderType=" + holderType
				+ ", balance=" + balance + ", lockedBalance=" + lockedBalance + ", promotionalBalance="
				+ promotionalBalance + ", nonPromotionalBalance=" + nonPromotionalBalance + "]";
	}

}