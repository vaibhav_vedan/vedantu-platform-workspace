/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.leadsquared;

import java.util.List;

/**
 *
 * @author pranavm
 */
public class UpdateLeadActivitiesRes {

    private List<UpdateLeadActivityReq> updateReqs;
    private List<LeadActivityReq> createActivetityReqs;

    public List<UpdateLeadActivityReq> getUpdateReqs() {
        return updateReqs;
    }

    public void setUpdateReqs(List<UpdateLeadActivityReq> updateReqs) {
        this.updateReqs = updateReqs;
    }

    public List<LeadActivityReq> getCreateActivetityReqs() {
        return createActivetityReqs;
    }

    public void setCreateActivetityReqs(List<LeadActivityReq> createActivetityReqs) {
        this.createActivetityReqs = createActivetityReqs;
    }
}
