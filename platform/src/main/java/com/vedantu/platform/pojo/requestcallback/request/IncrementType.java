package com.vedantu.platform.pojo.requestcallback.request;

public enum IncrementType {
	INCREMENTAL, AVERAGE, FIXED;
}
