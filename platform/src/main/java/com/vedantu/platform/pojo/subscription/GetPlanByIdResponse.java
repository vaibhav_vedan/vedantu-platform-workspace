package com.vedantu.platform.pojo.subscription;

public class GetPlanByIdResponse {

	private String id;
	private String slabId;
	private Long teacherId;
	private Long price;
	private boolean active;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSlabId() {
		return slabId;
	}

	public void setSlabId(String slabId) {
		this.slabId = slabId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public GetPlanByIdResponse(String id, String slabId, Long teacherId, Long price, boolean active) {
		super();
		this.id = id;
		this.slabId = slabId;
		this.teacherId = teacherId;
		this.price = price;
		this.active = active;
	}

	public GetPlanByIdResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetPlanByIdResponse [id=" + id + ", slabId=" + slabId + ", teacherId=" + teacherId + ", price=" + price
				+ ", active=" + active + "]";
	}

}
