package com.vedantu.platform.pojo.requestcallback.request;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddRequestCallbackDetailsReq extends AbstractFrontEndReq {

    public Long studentId;
    public Long teacherId;
    public String message;
    public Long boardId;
    public Long grade;
    public String target;
    public boolean parent;

    public AddRequestCallbackDetailsReq() {
        super();
    }

    public AddRequestCallbackDetailsReq(Long callingUserId, Long studentId, Long teacherId, String message,
            Long boardId, Long grade, String target, boolean parent) {
        super();
        super.setCallingUserId(callingUserId);
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.message = message;
        this.boardId = boardId;
        this.grade = grade;
        this.target = target;
        this.parent = parent;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getGrade() {
        return grade;
    }

    public void setGrade(Long grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public boolean isParent() {
        return parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return "AddRequestCallbackDetailsReq [studentId=" + studentId + ", teacherId=" + teacherId + ", message="
                + message + ", boardId=" + boardId + ", grade=" + grade + ", target=" + target + ", parent=" + parent
                + "]";
    }

    @Override
    public void verify() throws BadRequestException {
        super.verify();
        if (getCallingUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "CallingUserId not present");
        }
    }

}
