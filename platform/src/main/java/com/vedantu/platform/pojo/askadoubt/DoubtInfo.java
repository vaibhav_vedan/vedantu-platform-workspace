package com.vedantu.platform.pojo.askadoubt;

import com.vedantu.User.UserBasicInfo;
import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.platform.enums.askadoubt.DoubtStatus;
import com.vedantu.platform.interfaces.broadcast.Broadcastable;

public class DoubtInfo implements Broadcastable{

	public String id;
	public UserBasicInfo user;
	public Integer grade;
	public Board subject;
	public String doubtText;
	public List<String> attachments;
	public DoubtStatus status;
	public UserBasicInfo assignedToUser;
	public String startMessageId;
	public String endMessageId;
	public Long creationTime;
	
	public DoubtInfo() {
		super();
	}

	public DoubtInfo(String id, UserBasicInfo user, Integer grade, Board subject,
			String doubtText, List<String> attachments, DoubtStatus status,
			UserBasicInfo assignedToUser, String startMessageId, String endMessageId,
			Long creationTime) {
		super();
		this.id = id;
		this.user = user;
		this.grade = grade;
		this.subject = subject;
		this.doubtText = doubtText;
		this.attachments = attachments;
		this.status = status;
		this.assignedToUser = assignedToUser;
		this.startMessageId = startMessageId;
		this.endMessageId = endMessageId;
		this.creationTime = creationTime;
	}



	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public UserBasicInfo getUser() {
		return user;
	}

	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Board getSubject() {
		return subject;
	}

	public void setSubject(Board subject) {
		this.subject = subject;
	}

	public String getDoubtText() {
		return doubtText;
	}

	public void setDoubtText(String doubtText) {
		this.doubtText = doubtText;
	}

	public List<String> getAttachments() {
		return attachments;
	}

	public void setAttachments(List<String> attachments) {
		this.attachments = attachments;
	}

	public DoubtStatus getStatus() {
		return status;
	}

	public void setStatus(DoubtStatus status) {
		this.status = status;
	}

	public UserBasicInfo getAssignedToUser() {
		return assignedToUser;
	}

	public void setAssignedToUser(UserBasicInfo assignedToUser) {
		this.assignedToUser = assignedToUser;
	}

	public String getStartMessageId() {
		return startMessageId;
	}

	public void setStartMessageId(String startMessageId) {
		this.startMessageId = startMessageId;
	}

	public String getEndMessageId() {
		return endMessageId;
	}

	public void setEndMessageId(String endMessageId) {
		this.endMessageId = endMessageId;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	@Override
	public String toString() {
		return "DoubtInfo [id=" + id + ", user=" + user + ", grade=" + grade
				+ ", subject=" + subject + ", doubtText=" + doubtText
				+ ", attachments=" + attachments + ", status=" + status
				+ ", assignedTo=" + assignedToUser + ", startMessageId="
				+ startMessageId + ", endMessageId=" + endMessageId
				+ ", creationTime=" + creationTime + "]";
	}

}
