package com.vedantu.platform.pojo.courseplan;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.subscription.response.CoursePlanInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mano
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BookCoursePlanSessionPojo {

    private CoursePlanInfo coursePlanInfo;
    private Long userId;
    private PaymentType paymentType;

}
