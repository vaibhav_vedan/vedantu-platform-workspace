/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.leadsquared;

import com.google.gson.annotations.SerializedName;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pranavm
 */
public class UpdateLeadActivityReq {


    @SerializedName("ProspectActivityId")
    private String prospectActivityId;

    @SerializedName("Fields")
    private List<SchemaValuePair> fields = new ArrayList<>();

    public List<SchemaValuePair> getFields() {
        return fields;
    }

    public void setFields(List<SchemaValuePair> fields) {
        this.fields = fields;
    }

    public String getProspectActivityId() {
        return prospectActivityId;
    }

    public void setProspectActivityId(String prospectActivityId) {
        this.prospectActivityId = prospectActivityId;
    }

}
