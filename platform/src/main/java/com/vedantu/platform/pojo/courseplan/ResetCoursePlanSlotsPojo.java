package com.vedantu.platform.pojo.courseplan;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author mano
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ResetCoursePlanSlotsPojo {
    private Long endTime;
    private String coursePlanId;
}
