package com.vedantu.platform.pojo.dinero;


public interface IEntity {

	public void preStore();

	public void postStore();
}
