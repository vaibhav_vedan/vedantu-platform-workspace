package com.vedantu.platform.pojo.scheduling;



public class BasicFilters {

	String grade;
	String boardId;
	String target;
	Long startTime;
	Long endTime;
	
	public BasicFilters() {
		super();
		
	}
	public BasicFilters(String grade, String boardId, String target, Long startTime, Long endTime) {
		super();
		this.grade = grade;
		this.boardId = boardId;
		this.target = target;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getBoardId() {
		return boardId;
	}
	public void setBoardId(String boardId) {
		this.boardId = boardId;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public Long getStartTime() {
		return startTime;
	}
	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}
	public Long getEndTime() {
		return endTime;
	}
	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
	
}
