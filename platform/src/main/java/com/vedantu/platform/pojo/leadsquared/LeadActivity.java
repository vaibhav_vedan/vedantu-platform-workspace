package com.vedantu.platform.pojo.leadsquared;

import com.vedantu.platform.utils.LeadSquaredUtils;
import com.vedantu.util.StringUtils;

import java.util.HashMap;
import java.util.Map;


public class LeadActivity {
	private String activityEvent;
	private String email;
	private String phone;
	private String activityTime;
	private String activityNote;
	private String prospectId;

	public LeadActivity() {
		super();
	}

	public LeadActivity(String activityEvent, String email, Long activityTime, String activityNote) {
		super();
		this.activityEvent = activityEvent;
		this.email = email;
		setActivityTime(activityTime);
		this.activityNote = activityNote;
	}

	public Map<String, String> getFieldMap() {
		Map<String, String> fields = new HashMap<String, String>();
		if(StringUtils.isNotEmpty(this.prospectId)){
			fields.put("RelatedProspectId", this.prospectId);
		}
		if (StringUtils.isNotEmpty(this.phone)) {
			fields.put("Phone", this.phone);
			if(StringUtils.isEmpty(this.prospectId) && StringUtils.isEmpty(this.email)){
				fields.put("SearchBy", "Phone");
			}
		}
		if (StringUtils.isNotEmpty(this.email)) {
			fields.put("EmailAddress", this.email);
		}
		fields.put("ActivityEvent", String.valueOf(this.activityEvent));
		fields.put("ActivityDateTime", this.activityTime);
		fields.put("ActivityNote", this.activityNote);

		return fields;
	}

	public String getActivityEvent() {
		return activityEvent;
	}

	public void setActivityEvent(String activityEvent) {
		this.activityEvent = activityEvent;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivityTime() {
		return activityTime;
	}

	public void setActivityTime(String activityTime) {
		this.activityTime = activityTime;
	}

	public void setActivityTime(Long activityTime) {
		this.activityTime = LeadSquaredUtils.getLeadSquaredDate(activityTime);
	}

	public String getActivityNote() {
		return activityNote;
	}

	public void setActivityNote(String activityNote) {
		this.activityNote = activityNote;
	}

	public static class Constants {
		public static final String ACTIVITY_NOTE = "ActivityNote";
	}

	public String getProspectId() {
		return prospectId;
	}

	public void setProspectId(String prospectId) {
		this.prospectId = prospectId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}
}
