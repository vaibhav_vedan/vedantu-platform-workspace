package com.vedantu.platform.pojo.subscription;

import com.vedantu.subscription.response.CancelSubscriptionResponse;

public class UnlockHoursResponse {

	private Long subscriptionId;
	private Long lockedHours;
	private Long remainingHours;
	private CancelSubscriptionResponse cancelSubscriptionResponse;
	
	public UnlockHoursResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UnlockHoursResponse(Long subscriptionId, Long lockedHours, Long remainingHours,
			CancelSubscriptionResponse cancelSubscriptionResponse) {
		super();
		this.subscriptionId = subscriptionId;
		this.lockedHours = lockedHours;
		this.remainingHours = remainingHours;
		this.cancelSubscriptionResponse = cancelSubscriptionResponse;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getLockedHours() {
		return lockedHours;
	}

	public void setLockedHours(Long lockedHours) {
		this.lockedHours = lockedHours;
	}

	public Long getRemainingHours() {
		return remainingHours;
	}

	public void setRemainingHours(Long remainingHours) {
		this.remainingHours = remainingHours;
	}

	public CancelSubscriptionResponse getCancelSubscriptionResponse() {
		return cancelSubscriptionResponse;
	}

	public void setCancelSubscriptionResponse(CancelSubscriptionResponse cancelSubscriptionResponse) {
		this.cancelSubscriptionResponse = cancelSubscriptionResponse;
	}

	@Override
	public String toString() {
		return "UnlockHoursResponse [subscriptionId=" + subscriptionId + ", lockedHours=" + lockedHours
				+ ", remainingHours=" + remainingHours + ", cancelSubscriptionResponse=" + cancelSubscriptionResponse
				+ "]";
	}
}
