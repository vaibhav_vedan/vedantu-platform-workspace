/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.cms;

/**
 *
 * @author jeet
 */
class GTWInterval {
    private String startTime;
    private String endTime;

    public GTWInterval() {
            super();
    }

    public String getStartTime() {
            return startTime;
    }

    public void setStartTime(String startTime) {
            this.startTime = startTime;
    }

    public String getEndTime() {
            return endTime;
    }

    public void setEndTime(String endTime) {
            this.endTime = endTime;
    }

    @Override
    public String toString() {
            return "GTTInterval [startTime=" + startTime + ", endTime=" + endTime + ", toString()=" + super.toString()
                            + "]";
    }
}
