package com.vedantu.platform.pojo;

/**
 * Created by somil on 24/01/17.
 */
public class ExportUserSessionListDetail extends ExportUserSession {

    public String studentEmail;
    public String studentPhoneNumber;
    public String teacherEmail;
    public String teacherPhoneNumber;
    public String getStudentEmail() {
        return studentEmail;
    }
    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }
    public String getStudentPhoneNumber() {
        return studentPhoneNumber;
    }
    public void setStudentPhoneNumber(String studentPhoneNumber) {
        this.studentPhoneNumber = studentPhoneNumber;
    }
    public String getTeacherEmail() {
        return teacherEmail;
    }
    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }
    public String getTeacherPhoneNumber() {
        return teacherPhoneNumber;
    }
    public void setTeacherPhoneNumber(String teacherPhoneNumber) {
        this.teacherPhoneNumber = teacherPhoneNumber;
    }


    @Override
    public String toString() {
        return "ExportUserSessionListDetail{" +
                "studentEmail='" + studentEmail + '\'' +
                ", studentPhoneNumber='" + studentPhoneNumber + '\'' +
                ", teacherEmail='" + teacherEmail + '\'' +
                ", teacherPhoneNumber='" + teacherPhoneNumber + '\'' +
                "} " + super.toString();
    }
}
