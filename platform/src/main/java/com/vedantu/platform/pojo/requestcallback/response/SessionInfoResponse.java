package com.vedantu.platform.pojo.requestcallback.response;

import com.vedantu.User.UserBasicInfo;
import java.util.List;

import com.vedantu.board.pojo.BoardInfoRes;
import com.vedantu.scheduling.pojo.session.SessionInfo;

public class SessionInfoResponse {

		private UserBasicInfo teacher;
		private UserBasicInfo student;
		private List<SessionInfo> sessionList;
		private BoardInfoRes board;

		public SessionInfoResponse() {
			super();
			// TODO Auto-generated constructor stub
		}

		public SessionInfoResponse(UserBasicInfo teacher, UserBasicInfo student, List<SessionInfo> sessionList, BoardInfoRes board) {
			super();
			this.teacher = teacher;
			this.student = student;
			this.sessionList = sessionList;
			this.board = board;
		}

		public UserBasicInfo getTeacher() {
			return teacher;
		}

		public void setTeacher(UserBasicInfo teacher) {
			this.teacher = teacher;
		}

		public UserBasicInfo getStudent() {
			return student;
		}

		public void setStudent(UserBasicInfo student) {
			this.student = student;
		}

		public List<SessionInfo> getSessionList() {
			return sessionList;
		}

		public void setSessionList(List<SessionInfo> sessionList) {
			this.sessionList = sessionList;
		}

		public BoardInfoRes getBoard() {
			return board;
		}

		public void setBoard(BoardInfoRes board) {
			this.board = board;
		}

		@Override
		public String toString() {
			return "SessionInfoResponse [teacher=" + teacher + ", student=" + student + ", sessionList=" + sessionList
					+ ", board=" + board + "]";
		}
}
