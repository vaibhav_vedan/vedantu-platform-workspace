package com.vedantu.platform.pojo.scheduling;

import java.util.List;

public class TrueAvailability {

	String user;
	List<IntervalSlot> intervalSlots;
	public TrueAvailability(String user, List<IntervalSlot> intervalSlots) {
		super();
		this.user = user;
		this.intervalSlots = intervalSlots;
	}
	public TrueAvailability() {
		super();
	}
	@Override
	public String toString() {
		return "TrueAvailability [user=" + user + ", intervalSlots=" + intervalSlots + "]";
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public List<IntervalSlot> getIntervalSlots() {
		return intervalSlots;
	}
	public void setIntervalSlots(List<IntervalSlot> intervalSlots) {
		this.intervalSlots = intervalSlots;
	}
	
}
