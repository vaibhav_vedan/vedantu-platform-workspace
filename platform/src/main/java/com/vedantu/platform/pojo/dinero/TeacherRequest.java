package com.vedantu.platform.pojo.dinero;

import com.vedantu.User.Role;
import java.util.List;

public class TeacherRequest {

	private List<ApprovePriceWithTime> requests;
	private Long userId;
        private String email;
	private String contactNumber;
	private String firstName;
	private String lastName;
	private String fullName;
	private Role role;
	private String profilePicUrl;
	private String grade;

	public List<ApprovePriceWithTime> getRequests() {
		return requests;
	}

	public void setRequests(List<ApprovePriceWithTime> requests) {
		this.requests = requests;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "TeacherRequest [requests=" + requests + ", userId=" + userId + "]";
	}

	public TeacherRequest(List<ApprovePriceWithTime> requests, Long userId) {
		super();
		this.requests = requests;
		this.userId = userId;
	}

	public TeacherRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getProfilePicUrl() {
        return profilePicUrl;
    }

    public void setProfilePicUrl(String profilePicUrl) {
        this.profilePicUrl = profilePicUrl;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
        
        

}
