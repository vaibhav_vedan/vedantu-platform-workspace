/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.user;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.notification.pojos.MessageUserRes;
import java.util.List;

/**
 *
 * @author jeet
 */
public class MessageUserInfo {

    /**
     * @return the messageId
     */
    public Long getMessageId() {
        return messageId;
    }

    /**
     * @param messageId the messageId to set
     */
    public void setMessageId(Long messageId) {
        this.messageId = messageId;
    }
    
    private Long messageId;
    private UserBasicInfo user;
    private UserBasicInfo toUser;
    private String contextType;
    private String message;
    private List<String> replies;

    public MessageUserInfo() {
        super();
    }

    public MessageUserInfo(UserBasicInfo user, UserBasicInfo toUser, String contextType, String message) {
        super();
        this.user = user;
        this.toUser = toUser;
        this.contextType = contextType;
        this.message = message;
    }

    public MessageUserInfo(MessageUserRes messageUser, UserBasicInfo user, UserBasicInfo toUser) {
        this.user = user;
        this.toUser = toUser;
        this.contextType = messageUser.getContextType();
        this.message = messageUser.getMessageString();
        this.messageId = messageUser.getId();
        this.replies = messageUser.getReplies();
    }

    public UserBasicInfo getUser() {
        return user;
    }

    public void setUser(UserBasicInfo user) {
        this.user = user;
    }

    public UserBasicInfo getToUser() {
        return toUser;
    }

    public void setToUser(UserBasicInfo toUser) {
        this.toUser = toUser;
    }

    public String getContextType() {
        return contextType;
    }

    public void setContextType(String contextType) {
        this.contextType = contextType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the replies
     */
    public List<String> getReplies() {
        return replies;
    }

    /**
     * @param replies the replies to set
     */
    public void setReplies(List<String> replies) {
        this.replies = replies;
    }
}
