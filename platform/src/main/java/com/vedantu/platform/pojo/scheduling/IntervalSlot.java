package com.vedantu.platform.pojo.scheduling;

import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;

public class IntervalSlot {

	private long startTime;
	private long endTime;
	private CalendarEntrySlotState slotState;

	public IntervalSlot(long startTime, long endTime, CalendarEntrySlotState slotState) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.slotState = slotState;
	}

	public IntervalSlot() {
		super();
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public CalendarEntrySlotState getSlotState() {
		return slotState;
	}

	public void setSlotState(CalendarEntrySlotState slotState) {
		this.slotState = slotState;
	}

	@Override
	public String toString() {
		return "IntervalSlot [startTime=" + startTime + ", endTime=" + endTime + ", slotState=" + slotState + "]";
	}

}
