/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

import com.vedantu.platform.enums.feedback.FeedbackQuestionType;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class FeedbackQuestion {
    private FeedbackQuestionType feedbackQuestionType;
    private String questionText;
    private String minLabel;
    private String maxLabel;
    private boolean mandatory = false;
    private List<String> options = new ArrayList<>();
    private List<String> responses = new ArrayList<>();

    /**
     * @return the feedbackQuestionType
     */
    public FeedbackQuestionType getFeedbackQuestionType() {
        return feedbackQuestionType;
    }

    /**
     * @param feedbackQuestionType the feedbackQuestionType to set
     */
    public void setFeedbackQuestionType(FeedbackQuestionType feedbackQuestionType) {
        this.feedbackQuestionType = feedbackQuestionType;
    }

    /**
     * @return the questionText
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * @param questionText the questionText to set
     */
    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    /**
     * @return the options
     */
    public List<String> getOptions() {
        return options;
    }

    /**
     * @param options the options to set
     */
    public void setOptions(List<String> options) {
        this.options = options;
    }

    /**
     * @return the responses
     */
    public List<String> getResponses() {
        return responses;
    }

    /**
     * @param responses the responses to set
     */
    public void setResponses(List<String> responses) {
        this.responses = responses;
    }

    /**
     * @return the mandatory
     */
    public boolean isMandatory() {
        return mandatory;
    }

    /**
     * @param mandatory the mandatory to set
     */
    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    /**
     * @return the minLabel
     */
    public String getMinLabel() {
        return minLabel;
    }

    /**
     * @param minLabel the minLabel to set
     */
    public void setMinLabel(String minLabel) {
        this.minLabel = minLabel;
    }

    /**
     * @return the maxLabel
     */
    public String getMaxLabel() {
        return maxLabel;
    }

    /**
     * @param maxLabel the maxLabel to set
     */
    public void setMaxLabel(String maxLabel) {
        this.maxLabel = maxLabel;
    }
    
    
}
