/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo;

/**
 *
 * @author ajith
 */
public class AdminAccessFeatureEntity {

    private String name;
    private String access;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public AdminAccessFeatureEntity() {
    }

    @Override
    public String toString() {
        return "AdminAccessFeatureEntity{" + "name=" + name + ", access=" + access + '}';
    }

}
