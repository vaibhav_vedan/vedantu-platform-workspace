/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.cms;

/**
 *
 * @author jeet
 */
public class GTWSessionInfo {

	private String webinarKey;
	private Integer registrantsAttended;
	private String webinarID;
	private String sessionKey;
	private String startTime;
	private String endTime;

	public GTWSessionInfo() {
		super();
	}

	public String getWebinarKey() {
		return webinarKey;
	}

	public void setWebinarKey(String webinarKey) {
		this.webinarKey = webinarKey;
	}

	public Integer getRegistrantsAttended() {
		return registrantsAttended;
	}

	public void setRegistrantsAttended(Integer registrantsAttended) {
		this.registrantsAttended = registrantsAttended;
	}

	public String getWebinarID() {
		return webinarID;
	}

	public void setWebinarID(String webinarID) {
		this.webinarID = webinarID;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	@Override
	public String toString() {
		return "GTWSessionInfo [webinarKey=" + webinarKey + ", registrantsAttended=" + registrantsAttended
				+ ", webinarID=" + webinarID + ", sessionKey=" + sessionKey + ", startTime=" + startTime + ", endTime="
				+ endTime + ", toString()=" + super.toString() + "]";
	}
}
