package com.vedantu.platform.pojo.referral.request;

import java.util.List;

import com.vedantu.platform.pojo.referral.model.ReferralStepBonus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddUpdateReferralStepsReq extends AbstractFrontEndReq {

	public List<ReferralStepBonus> referralStepBonus;
	

	public AddUpdateReferralStepsReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public List<ReferralStepBonus> getReferralStepBonus() {
		return referralStepBonus;
	}

	public void setReferralStepBonus(List<ReferralStepBonus> referralStepBonus) {
		this.referralStepBonus = referralStepBonus;
	}
}
