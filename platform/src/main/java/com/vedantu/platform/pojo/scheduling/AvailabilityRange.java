package com.vedantu.platform.pojo.scheduling;

public class AvailabilityRange {

	private int min;
	private int max;
	private double weight;
	private boolean active = true;

	public AvailabilityRange() {
		super();
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	@Override
	public String toString() {
		return "AvailabilityRange [min=" + min + ", max=" + max + ", weight=" + weight + ", active=" + active + "]";
	}

	public AvailabilityRange(int min, int max, double weight, boolean active) {
		super();
		this.min = min;
		this.max = max;
		this.weight = weight;
		this.active = active;
	}

}
