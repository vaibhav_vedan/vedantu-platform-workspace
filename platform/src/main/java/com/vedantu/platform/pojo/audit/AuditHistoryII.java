/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.audit;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author Darshit
 */
@Document(collection = "AuditHistoryII")
public class AuditHistoryII extends AbstractMongoStringIdEntity {

    String auditEntityType;
    String auditEntityId;
    Object entityObject;

    public String getAuditEntityType() {
        return auditEntityType;
    }

    public void setAuditEntityType(String auditEntityType) {
        this.auditEntityType = auditEntityType;
    }

    public String getAuditEntityId() {
        return auditEntityId;
    }

    public void setAuditEntityId(String auditEntityId) {
        this.auditEntityId = auditEntityId;
    }

    public Object getEntityObject() {
        return entityObject;
    }

    public void setEntityObject(Object entityObject) {
        this.entityObject = entityObject;
    }
}
