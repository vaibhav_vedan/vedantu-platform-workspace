package com.vedantu.platform.pojo.dinero;

import com.vedantu.dinero.enums.TransactionRefSubType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoEntityBean;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class Transaction extends AbstractMongoStringIdEntityBean {

    private int amount;// amount in paisa
    private int promotionalAmount;
    private int nonPromotionalAmount;
    private String currencyCode;
    private String debitFromAccount;
    private String creditToAccount;
    private String reasonType;
    private String reasonRefNo;
    private Integer debitFromClosingBalance;
    private Integer creditToClosingBalance;

    private TransactionRefType reasonRefType;
    private TransactionRefSubType reasonRefSubType;

    private String reasonNote;

    private String triggredBy;

    public Transaction(int amount, int promotionalAmount, int nonPromotionalAmount, String currencyCode,
            String debitFromAccount, String creditToAccount, String reasonType, String reasonRefNo,
            TransactionRefType reasonRefType, String reasonNote, String triggredBy) {
        super();
        this.amount = amount;
        this.promotionalAmount = promotionalAmount;
        this.nonPromotionalAmount = nonPromotionalAmount;
        this.currencyCode = currencyCode;
        this.debitFromAccount = debitFromAccount;
        this.creditToAccount = creditToAccount;
        this.reasonType = reasonType;
        this.reasonRefNo = reasonRefNo;
        this.reasonRefType = reasonRefType;
        this.reasonNote = reasonNote;
        this.triggredBy = triggredBy;
    }

    public int getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(int promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public int getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(int nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public Transaction() {

        super();
    }

    public static String _getTriggredByUser(Long userId) {

        return "user/" + userId;
    }

    public static String _getTriggredBySystem() {

        return "system";
    }

    public int getAmount() {

        return amount;
    }

    public void setAmount(int amount) {

        this.amount = amount;
    }

    public String getCurrencyCode() {

        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {

        this.currencyCode = currencyCode;
    }

    public String getDebitFromAccount() {

        return debitFromAccount;
    }

    public void setDebitFromAccount(String debitFromAccount) {

        this.debitFromAccount = debitFromAccount;
    }

    public String getCreditToAccount() {

        return creditToAccount;
    }

    public void setCreditToAccount(String creditToAccount) {

        this.creditToAccount = creditToAccount;
    }

    public String getReasonType() {

        return reasonType;
    }

    public void setReasonType(String reasonType) {

        this.reasonType = reasonType;
    }

    public String getReasonRefNo() {

        return reasonRefNo;
    }

    public void setReasonRefNo(String reasonRefNo) {

        this.reasonRefNo = reasonRefNo;
    }

    public TransactionRefType getReasonRefType() {

        return reasonRefType;
    }

    public void setReasonRefType(TransactionRefType reasonRefType) {

        this.reasonRefType = reasonRefType;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public String getTriggredBy() {
        return triggredBy;
    }

    public void setTriggredBy(String triggredBy) {
        this.triggredBy = triggredBy;
    }

    public Integer getDebitFromClosingBalance() {
        return debitFromClosingBalance;
    }

    public void setDebitFromClosingBalance(Integer debitFromClosingBalance) {
        this.debitFromClosingBalance = debitFromClosingBalance;
    }

    public Integer getCreditToClosingBalance() {
        return creditToClosingBalance;
    }

    public void setCreditToClosingBalance(Integer creditToClosingBalance) {
        this.creditToClosingBalance = creditToClosingBalance;
    }

    public TransactionRefSubType getReasonRefSubType() {
        return reasonRefSubType;
    }

    public void setReasonRefSubType(TransactionRefSubType reasonRefSubType) {
        this.reasonRefSubType = reasonRefSubType;
    }

    public static class Constants extends AbstractMongoEntityBean.Constants {

        public static final String CREDIT_TO_ACCOUNT = "creditToAccount";
        public static final String REASON_REF_TYPE = "reasonRefType";
        public static final String REASON_REF_NO = "reasonRefNo";
        public static final String DEFAULT_CURRENCY_CODE = "INR";
    }

}
