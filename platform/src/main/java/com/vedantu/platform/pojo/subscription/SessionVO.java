package com.vedantu.platform.pojo.subscription;

import com.vedantu.User.Role;
import java.util.List;

import com.vedantu.platform.enums.SessionRequestType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;

public class SessionVO {

    public SessionRequestType sessionRequestType;
    public EntityType entitytype; // subscription
    public Long entityId;// subscriptionid
    public String title;
    public List<SessionSlot> sessionSlots;
    public Long hours;
    public Long teacherId;
    public Long studentId;
    public Role requestorRole;
    public Long requestorId;
    public Long sessionId;
    public SessionModel model;
    public String reason;
    public SubModel subModel;
    public Long proposalId;
    private SessionSource sessionSource;

    public SessionVO() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SessionVO(SessionRequestType sessionRequestType,
            EntityType entitytype, Long entityId, String title,
            List<SessionSlot> sessionSlots, Long hours, Long teacherId,
            Long studentId, Role requestorRole, Long requestorId,
            Long sessionId, SessionModel model, String reason, SubModel subModel,
            SessionSource sessionSource) {
        super();
        this.sessionRequestType = sessionRequestType;
        this.entitytype = entitytype;
        this.entityId = entityId;
        this.title = title;
        this.sessionSlots = sessionSlots;
        this.hours = hours;
        this.teacherId = teacherId;
        this.studentId = studentId;
        this.requestorRole = requestorRole;
        this.requestorId = requestorId;
        this.sessionId = sessionId;
        this.model = model;
        this.reason = reason;
        this.subModel = subModel;
        this.sessionSource = sessionSource;
    }

    public SessionRequestType getSessionRequestType() {
        return sessionRequestType;
    }

    public void setSessionRequestType(SessionRequestType sessionRequestType) {
        this.sessionRequestType = sessionRequestType;
    }

    public EntityType getEntitytype() {
        return entitytype;
    }

    public void setEntitytype(EntityType entitytype) {
        this.entitytype = entitytype;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<com.vedantu.session.pojo.SessionSlot> getSessionSlots() {
        return sessionSlots;
    }

    public void setSessionSlots(List<SessionSlot> sessionSlots) {
        this.sessionSlots = sessionSlots;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Role getRequestorRole() {
        return requestorRole;
    }

    public void setRequestorRole(Role requestorRole) {
        this.requestorRole = requestorRole;
    }

    public Long getRequestorId() {
        return requestorId;
    }

    public void setRequestorId(Long requestorId) {
        this.requestorId = requestorId;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public SubModel getSubModel() {
        return subModel;
    }

    public void setSubModel(SubModel subModel) {
        this.subModel = subModel;
    }

    public Long getProposalId() {
        return proposalId;
    }

    public void setProposalId(Long proposalId) {
        this.proposalId = proposalId;
    }

    public SessionSource getSessionSource() {
        return sessionSource;
    }

    public void setSessionSource(SessionSource sessionSource) {
        this.sessionSource = sessionSource;
    }

    @Override
    public String toString() {
        return "SessionVO{" + "sessionRequestType=" + sessionRequestType + ", entitytype=" + entitytype + ", entityId=" + entityId + ", title=" + title + ", sessionSlots=" + sessionSlots + ", hours=" + hours + ", teacherId=" + teacherId + ", studentId=" + studentId + ", requestorRole=" + requestorRole + ", requestorId=" + requestorId + ", sessionId=" + sessionId + ", model=" + model + ", reason=" + reason + ", subModel=" + subModel + ", proposalId=" + proposalId + ", sessionSource=" + sessionSource + '}';
    }

}
