package com.vedantu.platform.pojo.scheduling;


public class GetAvailableTeachersReq {
	private String grade;
	private String subject;
	private String target;
	private Long startTime;
	private Long endTime;

	public GetAvailableTeachersReq() {
		super();
	}

	public GetAvailableTeachersReq(String grade, String subject, String target, Long startTime, Long endTime) {
		super();
		this.grade = grade;
		this.subject = subject;
		this.target = target;
		this.startTime = startTime;
		this.endTime = endTime;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
}
