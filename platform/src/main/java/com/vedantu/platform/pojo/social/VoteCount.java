/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.social;

/**
 *
 * @author ajith
 */
public class VoteCount {

    private String contextId;
    private int upVotes = 0;
    private int downVotes = 0;
    private int voteValue;

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public int getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(int upVotes) {
        this.upVotes = upVotes;
    }

    public int getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(int downVotes) {
        this.downVotes = downVotes;
    }

    @Override
    public String toString() {
        return "VoteCount{" + "contextId=" + contextId + ", upVotes=" + upVotes + ", downVotes=" + downVotes + '}';
    }

    /**
     * @return the voteValue
     */
    public int getVoteValue() {
        return voteValue;
    }

    /**
     * @param voteValue the voteValue to set
     */
    public void setVoteValue(int voteValue) {
        this.voteValue = voteValue;
    }

}
