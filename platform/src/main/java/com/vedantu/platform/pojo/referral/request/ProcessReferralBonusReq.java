package com.vedantu.platform.pojo.referral.request;

import com.vedantu.platform.enums.referral.ReferralStep;

public class ProcessReferralBonusReq {
	
	public ReferralStep referralStep;
	public Long userId;
	public Long referrerUserId;
	public String referralCode;
	public String entityId;
	
	public ProcessReferralBonusReq() {
		super();
	}

	public ProcessReferralBonusReq(ReferralStep referralStep, Long userId,
			Long referrerUserId, String referralCode) {
		super();
		this.referralStep = referralStep;
		this.userId = userId;
		this.referrerUserId = referrerUserId;
		this.referralCode = referralCode;
	}

	public ReferralStep getReferralStep() {
		return referralStep;
	}

	public void setReferralStep(ReferralStep referralStep) {
		this.referralStep = referralStep;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getReferrerUserId() {
		return referrerUserId;
	}

	public void setReferrerUserId(Long referrerUserId) {
		this.referrerUserId = referrerUserId;
	}

	public String getReferralCode() {
		return referralCode;
	}

	public void setReferralCode(String referralCode) {
		this.referralCode = referralCode;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public String toString() {
		return "ProcessReferralBonusReq [referralStep=" + referralStep
				+ ", userId=" + userId + ", referrerUserId=" + referrerUserId
				+ ", referralCode=" + referralCode + "]";
	}
	
}
