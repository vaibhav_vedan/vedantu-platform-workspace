package com.vedantu.platform.pojo.subscription;

import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SubscriptionReqVO extends AbstractFrontEndReq {

	private Long teacherId;
	private Long studentId;
	private String title;
	private String planId;
	private SessionModel model;
	private Long offeringId;
	private Long totalHours;
	private String subject;
	private String target;
	private Integer grade;
	private String paymentDetails;
	private Long boardId;
	private SessionSchedule schedule;
	private String reason; 
	private String note; 
	private Long hourlyRate;
	private Long subscriptionRequestId;

	public SubscriptionReqVO() {
		super();
	}

	public SubscriptionReqVO(Long teacherId, Long studentId, String title, String planId, SessionModel model,
			Long offeringId, Long totalHours, String subject, String target, Integer grade, String paymentDetails,
			Long boardId, SessionSchedule schedule, String reason, String note, Long hourlyRate) {
		super();
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.title = title;
		this.planId = planId;
		this.model = model;
		this.offeringId = offeringId;
		this.totalHours = totalHours;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.paymentDetails = paymentDetails;
		this.boardId = boardId;
		this.schedule = schedule;
		this.reason = reason;
		this.note = note;
		this.hourlyRate = hourlyRate;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public SessionSchedule getSchedule() {
		return schedule;
	}

	public void setSchedule(SessionSchedule schedule) {
		this.schedule = schedule;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public Long getSubscriptionRequestId() {
		return subscriptionRequestId;
	}

	public void setSubscriptionRequestId(Long subscriptionRequestId) {
		this.subscriptionRequestId = subscriptionRequestId;
	}
}
