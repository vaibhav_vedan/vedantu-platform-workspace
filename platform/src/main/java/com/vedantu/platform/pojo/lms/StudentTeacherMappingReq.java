package com.vedantu.platform.pojo.lms;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.listing.pojo.EngagementType;


public class StudentTeacherMappingReq {
	private UserBasicInfo studentInfo;
	private UserBasicInfo teacherInfo;
	private String subject;
	private EngagementType engagementType;
	

	public EngagementType getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(EngagementType engagementType) {
		this.engagementType = engagementType;
	}

	public StudentTeacherMappingReq() {
		super();
	}

	public UserBasicInfo getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(UserBasicInfo studentInfo) {
		this.studentInfo = studentInfo;
	}

	public UserBasicInfo getTeacherInfo() {
		return teacherInfo;
	}

	public void setTeacherInfo(UserBasicInfo teacherInfo) {
		this.teacherInfo = teacherInfo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "StudentTeacherMappingReq [studentInfo=" + studentInfo + ", teacherInfo=" + teacherInfo + ", subject="
				+ subject + "]";
	}
}
