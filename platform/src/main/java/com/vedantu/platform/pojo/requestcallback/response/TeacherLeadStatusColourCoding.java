package com.vedantu.platform.pojo.requestcallback.response;

public class TeacherLeadStatusColourCoding {

	public String textColour;
	public String backgroundColour;
	
	public TeacherLeadStatusColourCoding() {
		super();
	}

	public TeacherLeadStatusColourCoding(String textColour,
			String backgroundColour) {
		super();
		this.textColour = textColour;
		this.backgroundColour = backgroundColour;
	}

	public String getTextColour() {
		return textColour;
	}

	public void setTextColour(String textColour) {
		this.textColour = textColour;
	}

	public String getBackgroundColour() {
		return backgroundColour;
	}

	public void setBackgroundColour(String backgroundColour) {
		this.backgroundColour = backgroundColour;
	}

	@Override
	public String toString() {
		return "TeacherLeadStatusColourCoding [textColour=" + textColour
				+ ", backgroundColour=" + backgroundColour + "]";
	}
	
}
