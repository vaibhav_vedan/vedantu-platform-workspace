package com.vedantu.platform.pojo.requestcallback.response;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.BoardInfoRes;
import com.vedantu.exception.VException;
import com.vedantu.platform.enums.requestcallback.LeadAssignee;
import com.vedantu.platform.enums.requestcallback.TeacherLeadStatus;
import com.vedantu.platform.managers.requestcallback.RequestCallbackManager;
import com.vedantu.platform.pojo.requestcallback.model.RequestCallBackDetails;
import com.vedantu.platform.controllers.subscription.SubscriptionController;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.LogFactory;

public class RequestCallbackDetailsRes {
	
	@Autowired
	LogFactory logFactory;
	
	Logger logger = logFactory.getLogger(RequestCallbackDetailsRes.class);
	
	public String id;
	public UserBasicInfo student;
	public UserBasicInfo teacher;
	public String message;
	public BoardInfoRes board;
	public Long grade;
	public String target;
	public boolean parent;
	public LeadAssignee assignedTo;
	public boolean teacherActionTaken;
	public TeacherLeadStatus teacherLeadStatus;
	public SessionInfo session;
	public SubscriptionResponse subscription;
	public Long creationTime;
	public Long currentSystemTime;
	public String teacherLeadStatusString;
	public TeacherLeadStatusColourCoding teacherLeadStatusColourCoding;
	
	public RequestCallbackDetailsRes(String id, UserBasicInfo student, UserBasicInfo teacher,
			String message, BoardInfoRes board, Long grade, String target,
			boolean parent, LeadAssignee assignedTo,
			boolean teacherActionTaken, TeacherLeadStatus teacherLeadStatus,
			SessionInfo session, SubscriptionResponse subscription,
			Long creationTime, Long currentSystemTime) {
		super();
		this.id = id;
		this.student = student;
		this.teacher = teacher;
		this.message = message;
		this.board = board;
		this.grade = grade;
		this.target = target;
		this.parent = parent;
		this.assignedTo = assignedTo;
		this.teacherActionTaken = teacherActionTaken;
		this.teacherLeadStatus = teacherLeadStatus;
		this.session = session;
		this.subscription = subscription;
		this.creationTime = creationTime;
		this.currentSystemTime = currentSystemTime;
	}
	
	public RequestCallbackDetailsRes(RequestCallBackDetails requestCallBackDetails, SessionInfoResponse sessionInfoResponse) throws VException{
		super();
		this.id=requestCallBackDetails.getId();
		this.student = sessionInfoResponse.getStudent();
		this.teacher = sessionInfoResponse.getTeacher();
		this.message = requestCallBackDetails.getMessage();
		this.board = sessionInfoResponse.getBoard();
		this.grade = requestCallBackDetails.getGrade();
		this.target = requestCallBackDetails.getTarget();
		this.parent = requestCallBackDetails.isParent();
		this.assignedTo = requestCallBackDetails.getAssignedTo();
		this.teacherActionTaken = requestCallBackDetails.isTeacherActionTaken();
		this.teacherLeadStatus = requestCallBackDetails.getTeacherLeadStatus();
		if(requestCallBackDetails.getSessionId()!=null){
		if(sessionInfoResponse.getSessionList()!=null && !sessionInfoResponse.getSessionList().isEmpty()){
			logger.debug(sessionInfoResponse.getSessionList().toString());
			for(SessionInfo sessionInfo:sessionInfoResponse.getSessionList()){
				logger.debug(requestCallBackDetails.getSessionId());
				logger.debug(sessionInfo.getId());
				if(sessionInfo.getId().compareTo(requestCallBackDetails.getSessionId())==0){
					
					this.session = sessionInfo;
					break;
				}
			}
			 SubscriptionController subscriptionController = new SubscriptionController();
			 if(requestCallBackDetails.getSubscriptionId()!=null){
				 try{
			 SubscriptionResponse subscriptionResponse = (SubscriptionResponse) subscriptionController.getSubscription(requestCallBackDetails.getSubscriptionId());
			 this.subscription = subscriptionResponse;
				 }catch(Exception e){
					 logger.debug(e.getMessage());
					 this.subscription = null;
				 }
			 }
		}}else{
		this.session = null;
		this.subscription = null;
		}
		this.creationTime = requestCallBackDetails.getCreationTime();
		this.currentSystemTime = System.currentTimeMillis();
		if(this.teacherLeadStatus!=null){
		this.teacherLeadStatusString = TeacherLeadStatus.getStatusString(this.teacherLeadStatus);
		this.teacherLeadStatusColourCoding = RequestCallbackManager.colourCodingMap.get(this.teacherLeadStatus);
		}
	}

	public UserBasicInfo getStudent() {
		return student;
	}

	public void setStudent(UserBasicInfo student) {
		this.student = student;
	}

	public UserBasicInfo getTeacher() {
		return teacher;
	}

	public void setTeacher(UserBasicInfo teacher) {
		this.teacher = teacher;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BoardInfoRes getBoard() {
		return board;
	}

	public void setBoardId(BoardInfoRes board) {
		this.board = board;
	}

	public Long getGrade() {
		return grade;
	}

	public void setGrade(Long grade) {
		this.grade = grade;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public boolean isParent() {
		return parent;
	}

	public void setParent(boolean parent) {
		this.parent = parent;
	}

	public LeadAssignee getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(LeadAssignee assignedTo) {
		this.assignedTo = assignedTo;
	}

	public boolean isTeacherActionTaken() {
		return teacherActionTaken;
	}

	public void setTeacherActionTaken(boolean teacherActionTaken) {
		this.teacherActionTaken = teacherActionTaken;
	}

	public TeacherLeadStatus getTeacherLeadStatus() {
		return teacherLeadStatus;
	}

	public void setTeacherLeadStatus(TeacherLeadStatus teacherLeadStatus) {
		this.teacherLeadStatus = teacherLeadStatus;
	}

	public SessionInfo getSession() {
		return session;
	}

	public void setSession(SessionInfo session) {
		this.session = session;
	}

	public SubscriptionResponse getSubscription() {
		return subscription;
	}

	public void setSubscription(SubscriptionResponse subscription) {
		this.subscription = subscription;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public Long getCurrentSystemTime() {
		return currentSystemTime;
	}

	public void setCurrentSystemTime(Long currentSystemTime) {
		this.currentSystemTime = currentSystemTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setBoard(BoardInfoRes board) {
		this.board = board;
	}

	@Override
	public String toString() {
		return "RequestCallbackDetailsRes [id=" + id + ", student=" + student
				+ ", teacher=" + teacher + ", message=" + message + ", board="
				+ board + ", grade=" + grade + ", target=" + target
				+ ", parent=" + parent + ", assignedTo=" + assignedTo
				+ ", teacherActionTaken=" + teacherActionTaken
				+ ", teacherLeadStatus=" + teacherLeadStatus + ", session="
				+ session + ", subscription=" + subscription
				+ ", creationTime=" + creationTime + ", currentSystemTime="
				+ currentSystemTime + "]";
	}

}
