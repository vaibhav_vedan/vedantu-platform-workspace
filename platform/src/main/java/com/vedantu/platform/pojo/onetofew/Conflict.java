package com.vedantu.platform.pojo.onetofew;

public class Conflict {

	public Long startTime;
	public Long endTime;
	public String organiserId;

	public Conflict(Long startTime, Long endTime, String organiserId) {
		super();
		this.startTime = startTime;
		this.endTime = endTime;
		this.organiserId = organiserId;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public String getOrganiserId() {
		return organiserId;
	}

	public void setOrganiserId(String organiserId) {
		this.organiserId = organiserId;
	}

	@Override
	public String toString() {
		return "Conflict [startTime=" + startTime + ", endTime=" + endTime
				+ ", organiserId=" + organiserId + "]";
	}
	
}
