package com.vedantu.platform.pojo.wave;

import com.vedantu.User.Role;
import com.vedantu.platform.enums.wave.WavebookScope;
import com.vedantu.platform.request.wave.CreateWavebookReq;
import com.vedantu.session.pojo.SessionPagesMetadata;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import java.util.List;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Wavebook")
@CompoundIndexes({
    @CompoundIndex(name = "wavebookScope_userRole", def = "{'wavebookScope' : 1, 'userRole': 1}")
    ,
    @CompoundIndex(name = "userId_wavebookScope", def = "{'userId' : 1, 'wavebookScope': 1}"),})
public class Wavebook extends AbstractTargetTopicEntity {

    private Long userId;

    private Role userRole;

    private String title;

    private String pagesMetaData;

    private List<SessionPagesMetadata> pagesMetaDataNew;

    private WavebookScope wavebookScope;

    private List<String> grades;

    private List<String> subjects;

    private boolean deleted;

    private WavebookType wavebookType;

    private WavebookVersion wavebookVersion = WavebookVersion.DEFAULT_WHITEBOARD;

    private String parentWavebookId;

    public Wavebook() {
        super();
    }

    public Wavebook(CreateWavebookReq createWavebookReq) {
        this.title = createWavebookReq.getTitle();
        this.pagesMetaData = createWavebookReq.getPagesMetaData();
        this.wavebookScope = createWavebookReq.getWavebookScope();
        this.grades = createWavebookReq.getGrades();
        this.subjects = createWavebookReq.getSubjects();
        this.wavebookType = createWavebookReq.getWavebookType();
        this.wavebookVersion = createWavebookReq.getWavebookVersion();
        this.parentWavebookId = createWavebookReq.getParentWavebookId();
        this.pagesMetaDataNew = createWavebookReq.getPagesMetaDataNew();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPagesMetaData() {
        return pagesMetaData;
    }

    public void setPagesMetaData(String pagesMetaData) {
        this.pagesMetaData = pagesMetaData;
    }

    public WavebookScope getWavebookScope() {
        return wavebookScope;
    }

    public void setWavebookScope(WavebookScope wavebookScope) {
        this.wavebookScope = wavebookScope;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Role getUserRole() {
        return userRole;
    }

    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }

    public WavebookType getWavebookType() {
        return wavebookType;
    }

    public void setWavebookType(WavebookType wavebookType) {
        this.wavebookType = wavebookType;
    }

    public WavebookVersion getWavebookVersion() {
        return wavebookVersion;
    }

    public void setWavebookVersion(WavebookVersion wavebookVersion) {
        this.wavebookVersion = wavebookVersion;
    }

    public String getParentWavebookId() {
        return parentWavebookId;
    }

    public void setParentWavebookId(String parentWavebookId) {
        this.parentWavebookId = parentWavebookId;
    }

    public List<SessionPagesMetadata> getPagesMetaDataNew() {
        return pagesMetaDataNew;
    }

    public void setPagesMetaDataNew(List<SessionPagesMetadata> pagesMetaDataNew) {
        this.pagesMetaDataNew = pagesMetaDataNew;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String USERID = "userId";
        public static final String USERROLE = "userRole";
        public static final String WAVEBOOKSCOPE = "wavebookScope";
        public static final String DELETED = "deleted";
        public static final String WAVEBOOK_TYPE = "wavebookType";
        public static final String WAVEBOOK_VERSION = "wavebookVersion";
        public static final String PARENT_WAVEBOOK_ID = "parentWavebookId";
    }
}
