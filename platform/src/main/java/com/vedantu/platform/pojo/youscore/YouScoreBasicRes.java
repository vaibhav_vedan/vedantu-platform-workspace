/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.youscore;

import com.google.gson.annotations.SerializedName;

/**
 *
 * @author somil
 */
public class YouScoreBasicRes {

	@SerializedName("Status")
	private String status;

	public YouScoreBasicRes() {
		super();
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
