/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.wave;

import com.vedantu.platform.request.filemgmt.FileACL;
import com.vedantu.platform.request.filemgmt.UploadType;
import com.vedantu.platform.request.filemgmt.WhiteboardPdfInitReq;
import com.vedantu.platform.response.filemgmt.PdfToImageRes;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.UploadState;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jeet
 */
public class WhiteboardPDF extends AbstractMongoStringIdEntity{
    private String name;
    private String type;
    private Long size;
    private Long userId;
    private Long totalPageCount;
    private Long convertedPageCount;
    private UploadType uploadType;
    private FileACL acl;
    private String folder;
    private String s3BucketName;
    private String s3BucketPath;
    private float scale;
    private List<PdfToImageRes> images = new ArrayList<>();
    private UploadState state = UploadState.INIT;
    
    public WhiteboardPDF() {
        super();
    }

    public WhiteboardPDF(String name, String type, Long size, Long userId, UploadType uploadType, FileACL acl, String folder, String s3BucketName, float scale,List<PdfToImageRes> images) {
        this.name = name;
        this.type = type;
        this.size = size;
        this.userId = userId;
        this.uploadType = uploadType;
        this.acl = acl;
        this.folder = folder;
        this.s3BucketName = s3BucketName;
        this.scale = scale;
        this.images = images;
    }
    
    public WhiteboardPDF(WhiteboardPdfInitReq req){
        super();
        this.name = req.getName();
        this.type = req.getType();
        this.size = req.getSize();
        this.uploadType = req.getUploadType();
        this.acl = req.getAcl();
        this.folder = req.getFolder();
        this.scale = req.getScale();
        this.userId = req.getCallingUserId();
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public UploadType getUploadType() {
        return uploadType;
    }

    public void setUploadType(UploadType uploadType) {
        this.uploadType = uploadType;
    }

    public FileACL getAcl() {
        return acl;
    }

    public void setAcl(FileACL acl) {
        this.acl = acl;
    }

    public String getFolder() {
        return folder;
    }

    public void setFolder(String folder) {
        this.folder = folder;
    }

    public String getS3BucketName() {
        return s3BucketName;
    }

    public void setS3BucketName(String s3BucketName) {
        this.s3BucketName = s3BucketName;
    }

    public String getS3BucketPath() {
        return s3BucketPath;
    }

    public void setS3BucketPath(String s3BucketPath) {
        this.s3BucketPath = s3BucketPath;
    }
    
    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public List<PdfToImageRes> getImages() {
        return images;
    }

    public void setImages(List<PdfToImageRes> images) {
        this.images = images;
    }

    public UploadState getState() {
        return state;
    }

    public void setState(UploadState state) {
        this.state = state;
    }

    public Long getTotalPageCount() {
        return totalPageCount;
    }

    public void setTotalPageCount(Long totalPageCount) {
        this.totalPageCount = totalPageCount;
    }

    public Long getConvertedPageCount() {
        return convertedPageCount;
    }

    public void setConvertedPageCount(Long convertedPageCount) {
        this.convertedPageCount = convertedPageCount;
    }

    @Override
    public String toString() {
        return "WhiteboardPDF{" + "name=" + name + ", type=" + type + ", size=" + size + ", userId=" + userId + ", totalPageCount=" + totalPageCount + ", convertedPageCount=" + convertedPageCount + ", uploadType=" + uploadType + ", acl=" + acl + ", folder=" + folder + ", s3BucketName=" + s3BucketName + ", s3BucketPath=" + s3BucketPath + ", scale=" + scale + ", images=" + images + ", state=" + state + '}';
    }
    
}
