package com.vedantu.platform.pojo.requestcallback.request;

import com.vedantu.platform.enums.requestcallback.TeacherLeadStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UpdateTeacherStatusRequestCallbackReq extends AbstractFrontEndReq {

	public String id;
	public TeacherLeadStatus teacherLeadStatus;

	public UpdateTeacherStatusRequestCallbackReq() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public TeacherLeadStatus getTeacherLeadStatus() {
		return teacherLeadStatus;
	}

	public void setTeacherLeadStatus(TeacherLeadStatus teacherLeadStatus) {
		this.teacherLeadStatus = teacherLeadStatus;
	}
}
