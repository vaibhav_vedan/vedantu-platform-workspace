package com.vedantu.platform.pojo.scheduling;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Service
public class RouteScheduling {

	private Client client = Client.create();

	

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(RouteScheduling.class);

	public String getTeachersWithFilters(String boardId, String grade, String target, String startString,
			String sizeString, Boolean fetchAll) throws IllegalAccessException {

		String activeFilter = "{\"term\":{\"active\":true}}";
		int start;
		int size;
		List<String> queryParams = new ArrayList<String>();

		String elasticSearchUrl = ConfigUtils.INSTANCE.getStringValue("elasticsearch.baseurl.scheduling")
				+ "vedantu/teachers/_search";
		String boardIdQuery = ElasticSearchQueryFormatter.INSTANCE.getTermFilter("boardIds", boardId);
		String gradeQuery = ElasticSearchQueryFormatter.INSTANCE.getTermFilter("grade", grade);
		String targetQuery = ElasticSearchQueryFormatter.INSTANCE.getTermFilter("category", target);

		String termTeacherBoardQuery = null;
		String termTeacherBoardInitQuery = "{\"has_child\":{\"type\":\"teacherBoardMappings\",\"filter\":{\"bool\":{\"must\":[%s]}}}}";
		if (boardId != null && boardId != "") {
			termTeacherBoardQuery = boardIdQuery;
		} else {
		}
		if (grade != null && grade != "") {
			if (termTeacherBoardQuery == null) {
				termTeacherBoardQuery = gradeQuery;
			} else {
				termTeacherBoardQuery += "," + gradeQuery;
			}
		}
		if (target != null && target != "") {
			target = target.toLowerCase();
			if (termTeacherBoardQuery == null) {
				termTeacherBoardQuery = targetQuery;
			} else {
				termTeacherBoardQuery += "," + targetQuery;
			}
		}
		if (!StringUtils.isEmpty(termTeacherBoardQuery)) {
			termTeacherBoardInitQuery = String.format(termTeacherBoardInitQuery, termTeacherBoardQuery);
			queryParams.add(termTeacherBoardInitQuery);
		}
		List<String> termTeacherQueryBuilder = new ArrayList<String>();
		boolean useAvailability = false;
		String query = null;
		if (Boolean.TRUE.equals(fetchAll)) {
			query = "{\"query\":{\"filtered\":{\"query\":{\"match_all\":{}},\"filter\":{\"bool\":{\"must\":[%s]}}}}%s}";
		} else {
			if (useAvailability) {
				String queryWithAvailability = "{\"query\":{\"filtered\":{\"query\":{\"function_score\":{\"query\":{\"has_child\":{\"type\":\"availabilitySlots\",\"score_mode\":\"avg\",\"query\":{\"function_score\":{\"query\": {\"bool\" : {\"must\":[{\"range\":{\"availabilityPercentage\":{\"gte\":0}}},{\"range\":{\"tillTime\":{\"gte\":"
						+ (System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_HOUR)
						+ "}}},{\"range\":{\"fromTime\":{\"lte\":"
						+ (System.currentTimeMillis() + (3 * DateTimeUtils.MILLIS_PER_DAY))
						+ "}}}]}},\"script_score\":{\"script\":\"doc['availabilitySlots.availabilityPercentage'].value\"}}}}},\"script_score\":{\"script\":\"(( ((doc['rating'].value )  - (val + exp((100-(ceil(_score*1.0)))/availability_penalty))/ ((log(1+ ((doc['sessionHours'].value/3600000)*doc['sessions'].value/session_penalty))/2.303)+1))) + doc['scoreOffset'].value)/(_score+1)\",\"params\":{\"val\":1.5,\"session_penalty\":10,\"availability_penalty\":20}}}},\"filter\":{\"bool\":{\"must\":[%s]}}}}%s}";
				query = queryWithAvailability;

			} else {
				String queryWithoutAvailabilty = "{\"query\":{\"filtered\":{\"query\":{\"function_score\":{\"boost_mode\":\"replace\",\"query\":{\"match_all\":{}},\"script_score\":{\"script\":\"((((doc['rating'].value.round(1) )  - (val)/ ((log(1+ ((doc['sessionHours'].value/3600000)*doc['sessions'].value/session_penalty))/2.303)+1)))+ doc['scoreOffset'].value)\",\"params\":{\"val\":1.5,\"session_penalty\":100,\"availability_penalty\":20}}}},\"filter\":{\"bool\":{\"must\":[%s]}}}}%s}";
				query = queryWithoutAvailabilty;
			}
		}

		String termTeacherQuery = StringUtils.join(termTeacherQueryBuilder.toArray(), ",");

		if (!CollectionUtils.isEmpty(termTeacherQueryBuilder)) {
			queryParams.add(termTeacherQuery);
		}

		queryParams.add(activeFilter);

		String limitQuery = "";

		if (startString != null) {
			start = Integer.parseInt(startString);
			limitQuery += ",\"from\":" + start;
		}
		if (sizeString != null) {
			size = Integer.parseInt(sizeString);
			limitQuery += ",\"size\":" + size;
		}

		query = String.format(query, StringUtils.join(queryParams.toArray(), ","), limitQuery);
		String output = null;
		try {
			output = sendRequestToElasticSearch(elasticSearchUrl, "post", query);
		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
		return output;
	}
        
    public String getTeachersByIds(List<Long> teacherIds) throws IllegalAccessException {
        String elasticSearchUrl = ConfigUtils.INSTANCE.getStringValue("elasticsearch.baseurl.scheduling")
                + "vedantu/teachers/_search";
        if (ArrayUtils.isNotEmpty(teacherIds)) {
        	int size = teacherIds.size();
            String teacherIdsQueryString = StringUtils.join(
                    teacherIds.toArray(), ",");
            String queryForEs = "{ \"query\": { \"filtered\": { \"filter\": { \"ids\" : {\"values\" : [%s] } }, \"query\": { \"match_all\": {} } } }, \"from\" : 0 , \"size\":  "+ size+ "}";
            queryForEs = String.format(queryForEs,
                    teacherIdsQueryString);
            return sendRequestToElasticSearch(elasticSearchUrl, "post", queryForEs);
        }
        return null;
    }
        
        
        
        
        

	public String sendRequestToElasticSearch(String serverUrl, String requestType, String query)
			throws IllegalAccessException {

		logger.info("Sending request to :" + serverUrl + ". Query Data: " + query);

		// boolean authenticationEnabled =
		// Boolean.parseBoolean(this.ConfigUtils.INSTANCE.properties.getProperty("elasticsearch.authentication.enabled"));
		// if (authenticationEnabled) {
		// String user =
		// this.ConfigUtils.INSTANCE.properties.getProperty("elasticsearch.authentication.user");
		// String password =
		// this.ConfigUtils.INSTANCE.properties.getProperty("elasticsearch.authentication.password");
		// if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(password)) {
		// HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(user,
		// password);
		// client.addFilter(authFilter);
		// } else {
		// throw new IllegalAccessException(
		// "User Not authorized to call Elastic Search");
		// }
		// }

		WebResource webResource = client.resource(serverUrl);

		ClientResponse response = null;
		switch (requestType) {
		case "post":
			response = webResource.type("application/json").post(ClientResponse.class, query);
			break;
		case "put":
			response = webResource.type("application/json").put(ClientResponse.class, query);
			break;
		case "get":
			response = webResource.type("application/json").get(ClientResponse.class);
			break;
		case "delete":
			response = webResource.type("application/json").delete(ClientResponse.class, query);
			break;
		}

		String output = response.getEntity(String.class);
		logger.info("es output for avalbale teachers in Route Scheduling is " + output);
		if (response.getStatus() != 200 && response.getStatus() != 201) {
			logger.info(
					"sendRequestToElasticSearch" + new RuntimeException(response.getClientResponseStatus().toString()));
			throw new RuntimeException(output);
		} else {
			logger.info("sendRequestToElasticSearch : " + output);
		}
		return output;

	}
}
