package com.vedantu.platform.pojo;

import com.vedantu.session.pojo.ProposalRepeatOrder;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by somil on 25/01/17.
 */
public class AvailabilitySlotPojo extends SessionSlot {
    private ProposalRepeatOrder repeatOrder = ProposalRepeatOrder.DAILY;
    private Integer repeatCount;

    public AvailabilitySlotPojo() {
        super();
    }

    public AvailabilitySlotPojo(Long startTime, Long endTime, String topic, String title, String description,
                                Boolean isConflicted) {
        super(startTime, endTime, topic, title, description, isConflicted);
    }

    public ProposalRepeatOrder getRepeatOrder() {
        return repeatOrder;
    }

    public void setRepeatOrder(ProposalRepeatOrder repeatOrder) {
        this.repeatOrder = repeatOrder;
    }

    public Integer getRepeatCount() {
        return repeatCount;
    }

    public void setRepeatCount(Integer repeatCount) {
        this.repeatCount = repeatCount;
    }

    public List<SessionSlot> createSessionSlots() {
        List<SessionSlot> sessionSlots = new ArrayList<SessionSlot>();
        if (repeatCount == null || repeatCount <= 0l) {
            repeatCount = 1;
        }

        for (int i = 0; i < repeatCount; i++) {
            Long slotStartTime = startTime + (i * duration(this.repeatOrder));
            Long slotEndTime = slotStartTime + (endTime - startTime);

            sessionSlots.add(new SessionSlot(slotStartTime, slotEndTime, null, null, null, null));
        }
        return sessionSlots;
    }

    public long duration(ProposalRepeatOrder repeatOrder) {
        switch (repeatOrder) {
            case DAILY:
            case ONCE:
                return DateTimeUtils.MILLIS_PER_DAY;
            case WEEKLY:
                return DateTimeUtils.MILLIS_PER_WEEK;
        }
        return 0;
    }

    @Override
    public String toString() {
        return "AvailabilitySlotPojo [repeatOrder=" + repeatOrder + ", repeatCount=" + repeatCount + ", toString()="
                + super.toString() + "]";
    }

}
