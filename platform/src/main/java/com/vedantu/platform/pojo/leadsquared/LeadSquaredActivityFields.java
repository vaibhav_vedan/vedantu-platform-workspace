/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.leadsquared;

/**
 *
 * @author pranavm
 */
public class LeadSquaredActivityFields {

    private String mx_Custom_1;
    private String mx_Custom_2;
    private String mx_Custom_3;
    private String mx_Custom_4;
    private String mx_Custom_5;
    private String mx_Custom_6;
    private String mx_Custom_7;
    private String mx_Custom_8;
    private String mx_Custom_9;
    private String mx_Custom_10;
    private String mx_Custom_11;
    private String mx_Custom_12;

    public String getMx_Custom_1() {
        return mx_Custom_1;
    }

    public void setMx_Custom_1(String mx_Custom_1) {
        this.mx_Custom_1 = mx_Custom_1;
    }

    public String getMx_Custom_2() {
        return mx_Custom_2;
    }

    public void setMx_Custom_2(String mx_Custom_2) {
        this.mx_Custom_2 = mx_Custom_2;
    }

    public String getMx_Custom_3() {
        return mx_Custom_3;
    }

    public void setMx_Custom_3(String mx_Custom_3) {
        this.mx_Custom_3 = mx_Custom_3;
    }

    public String getMx_Custom_4() {
        return mx_Custom_4;
    }

    public void setMx_Custom_4(String mx_Custom_4) {
        this.mx_Custom_4 = mx_Custom_4;
    }

    public String getMx_Custom_5() {
        return mx_Custom_5;
    }

    public void setMx_Custom_5(String mx_Custom_5) {
        this.mx_Custom_5 = mx_Custom_5;
    }

    public String getMx_Custom_6() {
        return mx_Custom_6;
    }

    public void setMx_Custom_6(String mx_Custom_6) {
        this.mx_Custom_6 = mx_Custom_6;
    }

    public String getMx_Custom_7() {
        return mx_Custom_7;
    }

    public void setMx_Custom_7(String mx_Custom_7) {
        this.mx_Custom_7 = mx_Custom_7;
    }

    public String getMx_Custom_8() {
        return mx_Custom_8;
    }

    public void setMx_Custom_8(String mx_Custom_8) {
        this.mx_Custom_8 = mx_Custom_8;
    }

    public String getMx_Custom_9() {
        return mx_Custom_9;
    }

    public void setMx_Custom_9(String mx_Custom_9) {
        this.mx_Custom_9 = mx_Custom_9;
    }

    public String getMx_Custom_10() {
        return mx_Custom_10;
    }

    public void setMx_Custom_10(String mx_Custom_10) {
        this.mx_Custom_10 = mx_Custom_10;
    }

    public String getMx_Custom_11() {
        return mx_Custom_11;
    }

    public void setMx_Custom_11(String mx_Custom_11) {
        this.mx_Custom_11 = mx_Custom_11;
    }

    public String getMx_Custom_12() {
        return mx_Custom_12;
    }

    public void setMx_Custom_12(String mx_Custom_12) {
        this.mx_Custom_12 = mx_Custom_12;
    }

}
