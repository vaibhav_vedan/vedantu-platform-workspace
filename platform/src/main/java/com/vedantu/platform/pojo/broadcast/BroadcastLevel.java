package com.vedantu.platform.pojo.broadcast;

import java.util.List;

import com.vedantu.platform.enums.broadcast.BroadcastType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class BroadcastLevel extends AbstractMongoStringIdEntity {

	private BroadcastType broadcastType;
	private Integer level;
	private List<Long> userIds;
	private Long expiryTime;

	public BroadcastLevel() {
		super();
	}
        
	public BroadcastLevel(Long creationTime, String createdBy,
			Long lastUpdated, String lastUpdatedBy) {
		super(creationTime, createdBy, lastUpdated, lastUpdatedBy);
	}

	public BroadcastLevel(BroadcastType broadcastType, Integer level,
			List<Long> userIds, Long expiryTime) {
		super();
		this.broadcastType = broadcastType;
		this.level = level;
		this.userIds = userIds;
		this.expiryTime = expiryTime;
	}

	public BroadcastType getBroadcastType() {
		return broadcastType;
	}

	public void setBroadcastType(BroadcastType broadcastType) {
		this.broadcastType = broadcastType;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public List<Long> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<Long> userIds) {
		this.userIds = userIds;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	@Override
	public String toString() {
		return "BroadcastLevel [broadcastType=" + broadcastType + ", level="
				+ level + ", userIds=" + userIds + ", expiryTime=" + expiryTime
				+ "]";
	}
	
	public static class Constants {
		public static final String BROADCAST_TYPE = "broadcastType";
		public static final String LEVEL = "level";
		public static final String EXPIRY_TIME = "expiryTime";
		public static final String USER_IDS = "userIds";
	}

}
