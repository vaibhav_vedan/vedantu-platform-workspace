/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.feedback;

import com.vedantu.platform.enums.feedback.RuleSetType;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

/**
 *
 * @author parashar
 */
public class FeedbackShareMetadata {
   
    private String contextId;
    private RuleSetType ruleSetType;
    private Integer ruleSetValue;
    private Long expiresIn; 
    private Long validTill;
    private EntityState entityState;
    private Long sharedOn;

    /**
     * @return the contextId
     */
    public String getContextId() {
        return contextId;
    }

    /**
     * @param contextId the contextId to set
     */
    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    /**
     * @return the ruleSetType
     */
    public RuleSetType getRuleSetType() {
        return ruleSetType;
    }

    /**
     * @param ruleSetType the ruleSetType to set
     */
    public void setRuleSetType(RuleSetType ruleSetType) {
        this.ruleSetType = ruleSetType;
    }

    /**
     * @return the ruleSetValue
     */
    public Integer getRuleSetValue() {
        return ruleSetValue;
    }

    /**
     * @param ruleSetValue the ruleSetValue to set
     */
    public void setRuleSetValue(Integer ruleSetValue) {
        this.ruleSetValue = ruleSetValue;
    }

    /**
     * @return the expiresIn
     */
    public Long getExpiresIn() {
        return expiresIn;
    }

    /**
     * @param expiresIn the expiresIn to set
     */
    public void setExpiresIn(Long expiresIn) {
        this.expiresIn = expiresIn;
    }

    /**
     * @return the validTill
     */
    public Long getValidTill() {
        return validTill;
    }

    /**
     * @param validTill the validTill to set
     */
    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    /**
     * @return the entityState
     */
    public EntityState getEntityState() {
        return entityState;
    }

    /**
     * @param entityState the entityState to set
     */
    public void setEntityState(EntityState entityState) {
        this.entityState = entityState;
    }

    /**
     * @return the sharedOn
     */
    public Long getSharedOn() {
        return sharedOn;
    }

    /**
     * @param sharedOn the sharedOn to set
     */
    public void setSharedOn(Long sharedOn) {
        this.sharedOn = sharedOn;
    }
    
}
