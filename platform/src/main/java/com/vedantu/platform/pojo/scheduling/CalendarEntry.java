package com.vedantu.platform.pojo.scheduling;

import com.vedantu.scheduling.pojo.calendar.CalendarUpdateDetails;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import java.util.ArrayList;
import java.util.List;


public class CalendarEntry extends AbstractMongoStringIdEntityBean {

	private String userId;
	private Long dayStartTime;
	private List<Integer> availability;
	private List<Integer> booked;
	private List<Integer> sessionRequest;
	private List<CalendarUpdateDetails> updateHistory = new ArrayList<>();

	public CalendarEntry() {
		super();
	}

	public CalendarEntry(String userId, Long dayStartTime, List<Integer> availability, List<Integer> booked,
			List<Integer> sessionRequest) {
		super();
		this.userId = userId;
		this.dayStartTime = dayStartTime;
		this.availability = availability;
		this.booked = booked;
		this.sessionRequest = sessionRequest;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Long getDayStartTime() {
		return dayStartTime;
	}

	public void setDayStartTime(Long dayStartTime) {
		this.dayStartTime = dayStartTime;
	}

	public List<Integer> getAvailability() {
		return availability;
	}

	public void setAvailability(List<Integer> availability) {
		this.availability = availability;
	}

	public List<Integer> getBooked() {
		return booked;
	}

	public void setBooked(List<Integer> booked) {
		this.booked = booked;
	}

	public List<Integer> getSessionRequest() {
		return sessionRequest;
	}

	public void setSessionRequest(List<Integer> sessionRequest) {
		this.sessionRequest = sessionRequest;
	}

	public List<CalendarUpdateDetails> getUpdateHistory() {
		return updateHistory;
	}

	public void setUpdateHistory(List<CalendarUpdateDetails> updateHistory) {
		this.updateHistory = updateHistory;
	}

	@Override
	public String toString() {
		return "CalendarEntry [userId=" + userId + ", dayStartTime=" + dayStartTime + ", availability=" + availability
				+ ", booked=" + booked + ", sessionRequest=" + sessionRequest + "]";
	}

	@SuppressWarnings("unchecked")
	public List<Integer> getCalendarEntryString(String pojoName) {
		try {
			return (List<Integer>) CalendarEntry.class.getField(pojoName).get(this);
		} catch (Exception ex) {
			ex.printStackTrace();
			return new ArrayList<Integer>();
		}
	}
	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String USER_ID = "userId";
		public static final String DAY_START_TIME = "dayStartTime";
		public static final String AVAILABILITY = "availability";
		public static final String BOOKED = "booked";
		public static final String SESSION_REQUEST = "sessionRequest";

	}
}
