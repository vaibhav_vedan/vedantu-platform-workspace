package com.vedantu.platform.pojo;

import com.vedantu.platform.enums.WebinarSpotInstanceStatus;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class WebinarSpotInstanceFilterReq {

    private Set<String> webinarIds;
    private String webinarId;
    private WebinarSpotInstanceStatus instanceStatus;
    private List<WebinarSpotInstanceStatus> instanceStatusList;
    private Long webinarFromStartTime;
    private Long webinarToStartTime;
    private Integer size;
    private Integer start;

}
