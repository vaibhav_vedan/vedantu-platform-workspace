package com.vedantu.platform.pojo.broadcast;

import com.vedantu.platform.interfaces.broadcast.INotificationInfo;

public class SendAskADoubtNotificationInfo implements INotificationInfo{

	public Long studentId;
	public Long teacherId;
	public String teacherFirstName;
	public String teacherLastName;
	public String teacherProfilePic;
	public String studentName;
	public String broadcastId;
	public String doubtId;
	public Integer grade;
	public String subject;
	
	public SendAskADoubtNotificationInfo() {
		super();
	}

	public SendAskADoubtNotificationInfo(Long studentId, Long teacherId,
			String teacherFirstName, String teacherLastName,
			String teacherProfilePic, String studentName, String broadcastId,
			String doubtId, Integer grade, String subject) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.teacherFirstName = teacherFirstName;
		this.teacherLastName = teacherLastName;
		this.teacherProfilePic = teacherProfilePic;
		this.studentName = studentName;
		this.broadcastId = broadcastId;
		this.doubtId = doubtId;
		this.grade = grade;
		this.subject = subject;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getTeacherFirstName() {
		return teacherFirstName;
	}

	public void setTeacherFirstName(String teacherFirstName) {
		this.teacherFirstName = teacherFirstName;
	}

	public String getTeacherLastName() {
		return teacherLastName;
	}

	public void setTeacherLastName(String teacherLastName) {
		this.teacherLastName = teacherLastName;
	}

	public String getTeacherProfilePic() {
		return teacherProfilePic;
	}

	public void setTeacherProfilePic(String teacherProfilePic) {
		this.teacherProfilePic = teacherProfilePic;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getBroadcastId() {
		return broadcastId;
	}

	public void setBroadcastId(String broadcastId) {
		this.broadcastId = broadcastId;
	}

	public String getDoubtId() {
		return doubtId;
	}

	public void setDoubtId(String doubtId) {
		this.doubtId = doubtId;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "SendAskADoubtNotificationInfo [studentId=" + studentId
				+ ", teacherId=" + teacherId + ", teacherFirstName="
				+ teacherFirstName + ", teacherLastName=" + teacherLastName
				+ ", teacherProfilePic=" + teacherProfilePic + ", studentName="
				+ studentName + ", broadcastId=" + broadcastId + ", doubtId="
				+ doubtId + ", grade=" + grade + ", subject=" + subject + "]";
	}
	
	
}
