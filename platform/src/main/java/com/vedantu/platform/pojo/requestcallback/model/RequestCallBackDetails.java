package com.vedantu.platform.pojo.requestcallback.model;

import org.dozer.Mapper;

import com.vedantu.platform.enums.requestcallback.LeadAssignee;
import com.vedantu.platform.enums.requestcallback.TeacherLeadStatus;
import com.vedantu.platform.pojo.requestcallback.request.AddRequestCallbackDetailsReq;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class RequestCallBackDetails extends AbstractMongoStringIdEntity {

    private Long studentId;
    private Long teacherId;
    private String message;
    private Long boardId;
    private Long grade;
    private String target;
    private boolean parent;
    private LeadAssignee assignedTo = LeadAssignee.TEACHER;
    private boolean teacherActionTaken;
    private TeacherLeadStatus teacherLeadStatus;
    private Long sessionId;
    private Long subscriptionId;
    private Long callingTime;
    private Integer numberOfCalls;

    public RequestCallBackDetails() {
        super();
    }

    public RequestCallBackDetails(
            AddRequestCallbackDetailsReq addRequestCallbackDetailsReq, Mapper mapper) {
        mapper.map(addRequestCallbackDetailsReq, this);
        this.setTeacherLeadStatus(TeacherLeadStatus.NOT_UPDATED);
        //this.setCreatedBy(addRequestCallbackDetailsReq.getCallingUserId()
        //        .toString());
        this.setCreationTime(System.currentTimeMillis());
        this.setLastUpdated(this.getCreationTime());
        //this.setLastUpdatedBy(this.getCreatedBy());
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Long getGrade() {
        return grade;
    }

    public void setGrade(Long grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public boolean isParent() {
        return parent;
    }

    public void setParent(boolean parent) {
        this.parent = parent;
    }

    public LeadAssignee getAssignedTo() {
        return assignedTo;
    }

    public void setAssignedTo(LeadAssignee assignedTo) {
        this.assignedTo = assignedTo;
    }

    public boolean isTeacherActionTaken() {
        return teacherActionTaken;
    }

    public void setTeacherActionTaken(boolean teacherActionTaken) {
        this.teacherActionTaken = teacherActionTaken;
    }

    public TeacherLeadStatus getTeacherLeadStatus() {
        return teacherLeadStatus;
    }

    public void setTeacherLeadStatus(TeacherLeadStatus teacherLeadStatus) {
        this.teacherLeadStatus = teacherLeadStatus;
    }

    public Long getSessionId() {
        return sessionId;
    }

    public void setSessionId(Long sessionId) {
        this.sessionId = sessionId;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Long getCallingTime() {
        return callingTime;
    }

    public void setCallingTime(Long callingTime) {
        this.callingTime = callingTime;
    }

    public Integer getNumberOfCalls() {
        return numberOfCalls;
    }

    public void setNumberOfCalls(Integer numberOfCalls) {
        this.numberOfCalls = numberOfCalls;
    }

    @Override
    public String toString() {
        return "RequestCallBackDetails [id=" + super.getId() + " studentId=" + studentId
                + ", teacherId=" + teacherId + ", message=" + message
                + ", boardId=" + boardId + ", grade=" + grade + ", target="
                + target + ", parent=" + parent + ", assignedTo=" + assignedTo
                + ", teacherActionTaken=" + teacherActionTaken
                + ", teacherLeadStatus=" + teacherLeadStatus + ", sessionId="
                + sessionId + ", subscriptionId=" + subscriptionId
                + ", callingTime=" + callingTime + ", numberOfCalls="
                + numberOfCalls + "]";
    }

    public static class Constants {

        public static final String STUDENT_ID = "studentId";
        public static final String TEACHER_ID = "teacherId";
        public static final String BOARD_ID = "boardId";
        public static final String GRADE = "grade";
        public static final String TARGET = "target";
        public static final String PARENT = "parent";
        public static final String ASSIGNED_TO = "assignedTo";
        public static final String TEACHER_ACTION_TAKEN = "teacherActionTaken";
        public static final String TEACHER_LEAD_STATUS = "teacherLeadStatus";
    }

}
