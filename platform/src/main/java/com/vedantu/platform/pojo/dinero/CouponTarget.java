package com.vedantu.platform.pojo.dinero;

import com.vedantu.platform.enums.dinero.CouponTargetEntityType;

import java.util.List;

/**
 * Created by somil on 02/06/17.
 */
public class CouponTarget {
    private CouponTargetEntityType targetType;//eg Subject,target,grade
    //will go with strings for all entities, so comparison becomes easy
    private List<String> targetStrs;//eg<physics,maths><cbse,icse>,<11,12>

    public CouponTarget() {
    }

    public CouponTarget(CouponTargetEntityType targetType, List<String> targetStrs) {
        this.targetType = targetType;
        this.targetStrs = targetStrs;
    }

    public CouponTargetEntityType getTargetType() {
        return targetType;
    }

    public void setTargetType(CouponTargetEntityType targetType) {
        this.targetType = targetType;
    }

    public List<String> getTargetStrs() {
        return targetStrs;
    }

    public void setTargetStrs(List<String> targetStrs) {
        this.targetStrs = targetStrs;
    }

    @Override
    public String toString() {
        return "CouponTarget{" + "targetType=" + targetType + ", targetStrs=" + targetStrs + '}';
    }

}
