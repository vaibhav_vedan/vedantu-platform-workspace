package com.vedantu.platform.pojo.requestcallback.response;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetSessionInfoRequest extends AbstractFrontEndReq {

    public Long subscriptionId;
    public Long teacherId;
    public Long studentId;
    public Long boardId;

    public GetSessionInfoRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

}
