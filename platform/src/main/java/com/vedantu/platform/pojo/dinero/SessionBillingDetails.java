package com.vedantu.platform.pojo.dinero;

import com.vedantu.session.pojo.EntityType;
import org.springframework.util.CollectionUtils;

import com.vedantu.User.Role;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.UserSessionInfo;
import com.vedantu.util.enums.SessionModel;

public class SessionBillingDetails {

    private String sessionId;
    private Long sessionScheduledDuration; // Session endedtime - session starttime.
    private Long billingDuration; // For now picking up the teacher billing
    // duration. TODO : Need to fetch it
    // seperately for different users
    private Long teacherId;
    private SessionModel model;
    private Long studentId;
    private Long subscriptionId;
    private Long sessionStartTime; // Scheduled start time of the session
    private Long sessionEndTime; // Scheduled end time of the session
    private String snsKey;
    private EntityType contextType;
    private String contextId;

    public SessionBillingDetails() {
        super();
    }

    public SessionBillingDetails(SessionInfo sessionInfo) {
        super();
        this.sessionId = String.valueOf(sessionInfo.getId());
        this.sessionScheduledDuration = sessionInfo.getEndTime() - sessionInfo.getStartTime();
        if (!CollectionUtils.isEmpty(sessionInfo.getAttendees())) {
            for (UserSessionInfo attendee : sessionInfo.getAttendees()) {
                if (Role.TEACHER.equals(attendee.getRole())) {
                    if(sessionInfo.getAttendees().get(0).getBillingPeriod() != null){
                        this.billingDuration = Long.valueOf(sessionInfo.getAttendees().get(0).getBillingPeriod());
                    }
                }
            }
        }
        this.teacherId = sessionInfo.getTeacherId();

        // Assuming student id will be present.
        this.studentId = sessionInfo.getStudentIds().get(0);
        this.subscriptionId = sessionInfo.getSubscriptionId();
        this.model = sessionInfo.getModel();
        Long endTime = sessionInfo.getEndTime();
        Long startTime = sessionInfo.getStartTime();
//        if (sessionInfo.getEndedAt() != null) {
//            endTime = Math.min(sessionInfo.getEndedAt(), sessionInfo.getEndTime());
//        }
//        if (sessionInfo.getStartedAt() != null)
//            startTime = Math.max(sessionInfo.getStartedAt(), sessionInfo.getStartTime());
        this.sessionStartTime = startTime;
        this.sessionEndTime = endTime;
        this.contextType = sessionInfo.getContextType();
        this.contextId = sessionInfo.getContextId();

    }

    public SessionBillingDetails(String sessionId, Long sessionDuration, Long billingDuration, Long teacherId,
            SessionModel model, Long studentId, Long subscriptionId) {
        super();
        this.sessionId = sessionId;
        this.sessionScheduledDuration = sessionDuration;
        this.billingDuration = billingDuration;
        this.teacherId = teacherId;
        this.model = model;
        this.studentId = studentId;
        this.subscriptionId = subscriptionId;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public Long getSessionScheduledDuration() {
        return sessionScheduledDuration;
    }

    public void setSessionScheduledDuration(Long sessionScheduledDuration) {
        this.sessionScheduledDuration = sessionScheduledDuration;
    }

    public Long getBillingDuration() {
        return billingDuration;
    }

    public void setBillingDuration(Long billingDuration) {
        this.billingDuration = billingDuration;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getSessionStartTime() {
        return sessionStartTime;
    }

    public void setSessionStartTime(Long sessionStartTime) {
        this.sessionStartTime = sessionStartTime;
    }

    public Long getSessionEndTime() {
        return sessionEndTime;
    }

    public void setSessionEndTime(Long sessionEndTime) {
        this.sessionEndTime = sessionEndTime;
    }

    public String getSnsKey() {
        return snsKey;
    }

    public void setSnsKey(String snsKey) {
        this.snsKey = snsKey;
    }

    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    @Override
    public String toString() {
        return "SessionBillingDetails{" +
                "sessionId='" + sessionId + '\'' +
                ", sessionScheduledDuration=" + sessionScheduledDuration +
                ", billingDuration=" + billingDuration +
                ", teacherId=" + teacherId +
                ", model=" + model +
                ", studentId=" + studentId +
                ", subscriptionId=" + subscriptionId +
                ", sessionStartTime=" + sessionStartTime +
                ", sessionEndTime=" + sessionEndTime +
                ", snsKey='" + snsKey + '\'' +
                '}';
    }
}
