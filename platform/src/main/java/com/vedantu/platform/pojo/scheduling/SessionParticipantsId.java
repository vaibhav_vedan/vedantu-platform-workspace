/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.platform.pojo.scheduling;

/**
 *
 * @author somil
 */
public class SessionParticipantsId {
    Long studentId;
    Long teacherId;
    
    public SessionParticipantsId() {
    }

    public SessionParticipantsId(Long studentId, Long teacherId) {
        this.studentId = studentId;
        this.teacherId = teacherId;
    }
    
    

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    @Override
    public String toString() {
        return "SessionParticipantsId{" + "studentId=" + studentId + ", teacherId=" + teacherId + '}';
    }
    
    
    
    
    
}
