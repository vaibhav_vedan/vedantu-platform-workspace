package com.vedantu.platform.pojo.referral.model;

import com.vedantu.platform.enums.referral.ReferralStep;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class ReferralStepBonus extends AbstractMongoStringIdEntity {

	private Long referralPolicyId;
	private ReferralStep referralStep;
	private Long referrerBonusAmount; // in Paisa
	private Long referreeBonusAmount; //in Paisa
	private String entityId;
	private Integer maxReferrals;
	
	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public Integer getMaxReferrals() {
		return maxReferrals;
	}

	public void setMaxReferrals(Integer maxReferrals) {
		this.maxReferrals = maxReferrals;
	}

	public ReferralStepBonus() {
		super();
	}

	public ReferralStepBonus(Long referralPolicyId, ReferralStep referralStep,
			Long referrerBonusAmount, Long referreeBonusAmount) {
		super();
		this.referralPolicyId = referralPolicyId;
		this.referralStep = referralStep;
		this.referrerBonusAmount = referrerBonusAmount;
		this.referreeBonusAmount = referreeBonusAmount;
	}

	public ReferralStepBonus(Long creationTime, String createdBy,
			Long lastUpdated, String callingUserId, Long referralPolicyId,
			ReferralStep referralStep, Long referrerBonusAmount, Long referreeBonusAmount) {
		super(creationTime, createdBy, lastUpdated, callingUserId);
		this.referralPolicyId = referralPolicyId;
		this.referralStep = referralStep;
		this.referrerBonusAmount = referrerBonusAmount;
		this.referreeBonusAmount = referreeBonusAmount;
		
	}

	public Long getReferralPolicyId() {
		return referralPolicyId;
	}

	public void setReferralPolicyId(Long referralPolicyId) {
		this.referralPolicyId = referralPolicyId;
	}

	public ReferralStep getReferralStep() {
		return referralStep;
	}

	public void setReferralStep(ReferralStep referralStep) {
		this.referralStep = referralStep;
	}

	public Long getReferrerBonusAmount() {
		return referrerBonusAmount;
	}

	public void setReferrerBonusAmount(Long referrerBonusAmount) {
		this.referrerBonusAmount = referrerBonusAmount;
	}

	public Long getReferreeBonusAmount() {
		return referreeBonusAmount;
	}

	public void setReferreeBonusAmount(Long referreeBonusAmount) {
		this.referreeBonusAmount = referreeBonusAmount;
	}

	@Override
	public String toString() {
		return "ReferralStepBonus [referralPolicyId=" + referralPolicyId
				+ ", referralStep=" + referralStep + ", referrerBonusAmount="
				+ referrerBonusAmount + ", referreeBonusAmount="
				+ referreeBonusAmount + "]";
	}

	public static class Constants {
		public static final String ID = "id";
		public static final String CREATION_TIME = "creationTime";
		public static final String LAST_UPDATED = "lastUpdated";
		public static final String CREATED_BY = "createdBy";
		public static final String CALLING_USERID = "callingUserId";
		public static final String LAST_UPDATED_BY = "lastUpdatedBy";
		public static final String REFERRAL_POLICY_ID = "referralPolicyId";
		public static final String REFERRAL_STEP = "referralStep";
		public static final String ENTITY_ID = "entityId";
	}

}
