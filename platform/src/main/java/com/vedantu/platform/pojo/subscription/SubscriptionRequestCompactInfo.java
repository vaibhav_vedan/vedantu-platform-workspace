package com.vedantu.platform.pojo.subscription;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.subscription.SubscriptionRequestAction;

public class SubscriptionRequestCompactInfo extends SubscriptionRequestBasicInfo {

	private UserBasicInfo teacher;
	private UserBasicInfo student;
	private String subject;
	private Long totalHours;
	private Long noOfWeeks;
	private Long hourlyRate;
	private Long creationTime;
	private Long subscriptionId;
	private SubscriptionRequestAction action;

	public SubscriptionRequestCompactInfo() {
		super();
	}

	public SubscriptionRequestCompactInfo(SubscriptionRequest req) {
		super(req);
		this.subject = req.getSubject();
		this.totalHours = req.getTotalHours();
		this.noOfWeeks = req.getNoOfWeeks();
		this.hourlyRate = req.getHourlyRate();
		this.creationTime = req.getCreationTime();
		this.subscriptionId = req.getSubscriptionId();
		this.action = req.getAction();
	}

	public UserBasicInfo getTeacher() {
		return teacher;
	}

	public void setTeacher(UserBasicInfo teacher) {
		this.teacher = teacher;
	}

	public UserBasicInfo getStudent() {
		return student;
	}

	public void setStudent(UserBasicInfo student) {
		this.student = student;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public Long getNoOfWeeks() {
		return noOfWeeks;
	}

	public void setNoOfWeeks(Long noOfWeeks) {
		this.noOfWeeks = noOfWeeks;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public SubscriptionRequestAction getAction() {
		return action;
	}

	public void setAction(SubscriptionRequestAction action) {
		this.action = action;
	}

	@Override
	public String toString() {
		return "SubscriptionRequestCompactInfo [id=" + getId() + ", studentName=" + getStudentName() + ", title="
				+ getTitle() + ", startDate=" + getStartDate() + ", expiryTime=" + getExpiryTime() + ", amount="
				+ getAmount() + ", state=" + getState() + ", teacher=" + teacher + ", student=" + student + ", subject=" + subject
				+ ", totalHours=" + totalHours + ", noOfWeeks=" + noOfWeeks + ", hourlyRate=" + hourlyRate
				+ ", creationTime=" + creationTime + ", subscriptionId=" + subscriptionId + ", action=" + action + "]";
	}
}
