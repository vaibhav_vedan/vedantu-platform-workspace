package com.vedantu.platform.pojo.subscription;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.BasicResponse;

public class GetCompactSubscriptionRequestsResponse extends BasicResponse {

	private Integer count;
	private List<SubscriptionRequestCompactInfo> compactSubscriptionRequestsList;

	public GetCompactSubscriptionRequestsResponse() {
		super();
	}

	public GetCompactSubscriptionRequestsResponse(Integer count,
			List<SubscriptionRequestCompactInfo> compactSubscriptionRequestsList) {
		super();
		this.count = count;
		this.compactSubscriptionRequestsList = compactSubscriptionRequestsList;
	}

	public GetCompactSubscriptionRequestsResponse(GetSubscriptionRequestsResponse resp) {
		super();
		this.count = resp.getCount();
		this.compactSubscriptionRequestsList = new ArrayList<>();
		for (SubscriptionRequest s : resp.getSubscriptionRequestsList()) {
			this.compactSubscriptionRequestsList.add(new SubscriptionRequestCompactInfo(s));
		}
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<SubscriptionRequestCompactInfo> getCompactSubscriptionRequestsList() {
		return compactSubscriptionRequestsList;
	}

	public void setCompactSubscriptionRequestsList(List<SubscriptionRequestCompactInfo> compactSubscriptionRequestsList) {
		this.compactSubscriptionRequestsList = compactSubscriptionRequestsList;
	}

	@Override
	public String toString() {
		return "GetCompactSubscriptionRequestsResponse [count=" + count + ", compactSubscriptionRequestsList="
				+ compactSubscriptionRequestsList + "]";
	}
}
