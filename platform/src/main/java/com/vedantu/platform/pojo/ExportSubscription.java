package com.vedantu.platform.pojo;

import com.vedantu.util.enums.SessionModel;

/**
 * Created by somil on 24/01/17.
 */
public class ExportSubscription {

    public long offeringId;
    public String offeringType;
    public String offeringName;
    public long studentId;
    public String studentName;
    public String studentEmail;
    public String studentPhone;
    public long teacherId;
    public String teacherName;
    public String teacherEmail;
    public String teacherPhone;
    public long paidAmount;
    public String paymentTime;
    public String paymentDate;
    public String currencyCode;
    public String paymentStatus;
    public String state;
    public String startedAt;
    public String endedAt;
    public long transactionId;
    public String planId;
    public int planSchedule;
    public String scheduleType;
    public Float hourBalance;
    public Float lockedHourBalance;
    public Float totalHourConsumed;
    public String planType;
    public String creationDate;
    public String creationTime;
    public SessionModel model;
    public String active;

    public long getOfferingId() {
        return offeringId;
    }

    public void setOfferingId(long offeringId) {
        this.offeringId = offeringId;
    }

    public String getOfferingType() {
        return offeringType;
    }

    public void setOfferingType(String offeringType) {
        this.offeringType = offeringType;
    }

    public String getOfferingName() {
        return offeringName;
    }

    public void setOfferingName(String offeringName) {
        this.offeringName = offeringName;
    }

    public long getStudentId() {
        return studentId;
    }

    public void setStudentId(long studentId) {
        this.studentId = studentId;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getStudentPhone() {
        return studentPhone;
    }

    public void setStudentPhone(String studentPhone) {
        this.studentPhone = studentPhone;
    }

    public long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(long teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public String getTeacherPhone() {
        return teacherPhone;
    }

    public void setTeacherPhone(String teacherPhone) {
        this.teacherPhone = teacherPhone;
    }

    public long getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(long paidAmount) {
        this.paidAmount = paidAmount;
    }

    public String getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(String paymentTime) {
        this.paymentTime = paymentTime;
    }

    public String getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(String paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getEndedAt() {
        return endedAt;
    }

    public void setEndedAt(String endedAt) {
        this.endedAt = endedAt;
    }

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public int getPlanSchedule() {
        return planSchedule;
    }

    public void setPlanSchedule(int planSchedule) {
        this.planSchedule = planSchedule;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public Float getHourBalance() {
        return hourBalance;
    }

    public void setHourBalance(Float hourBalance) {
        this.hourBalance = hourBalance;
    }

    public Float getLockedHourBalance() {
        return lockedHourBalance;
    }

    public void setLockedHourBalance(Float lockedHourBalance) {
        this.lockedHourBalance = lockedHourBalance;
    }

    public Float getTotalHourConsumed() {
        return totalHourConsumed;
    }

    public void setTotalHourConsumed(Float totalHourConsumed) {
        this.totalHourConsumed = totalHourConsumed;
    }

    public String getPlanType() {
        return planType;
    }

    public void setPlanType(String planType) {
        this.planType = planType;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public SessionModel getModel() {
        return model;
    }

    public void setModel(SessionModel model) {
        this.model = model;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

}
