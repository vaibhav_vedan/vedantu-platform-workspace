package com.vedantu.platform.vsatAutomation.managers;


import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.platform.dao.redis.RedisDAO;
import com.vedantu.util.Constants;
import com.vedantu.util.LogFactory;
import com.vedantu.util.pojo.VsatEventDetailsPojo;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author mnpk
 */

@Service
public class VsatAutomationManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VsatAutomationManager.class);

    @Autowired
    private RedisDAO redisDAO;

    public void changeRedisVsatEventDetails(String message) throws InternalServerErrorException, BadRequestException {
        redisDAO.set(Constants.VSAT_EVENT_DETAILS, message);
    }

    public void clearRedisVsatEventDetails() throws BadRequestException, InternalServerErrorException {
        redisDAO.del(Constants.VSAT_EVENT_DETAILS);
    }

    public VsatEventDetailsPojo getVsatEventDetails() throws BadRequestException, InternalServerErrorException {
        String vsatEventDetailsStr = redisDAO.get(Constants.VSAT_EVENT_DETAILS);
        VsatEventDetailsPojo vsatEventDetailsPojo = new Gson().fromJson(vsatEventDetailsStr, VsatEventDetailsPojo.class);
        return vsatEventDetailsPojo;
    }
}
