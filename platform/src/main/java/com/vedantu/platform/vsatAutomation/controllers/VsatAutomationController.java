package com.vedantu.platform.vsatAutomation.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.VException;
import com.vedantu.platform.vsatAutomation.managers.VsatAutomationManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.pojo.VsatEventDetailsPojo;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;


/**
 *
 * @author mnpk
 */

@RestController
@RequestMapping("/vsat")
public class VsatAutomationController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VsatAutomationController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    VsatAutomationManager vsatAutomationManager;

    @RequestMapping(value = "/getVsatEventDetails",method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public VsatEventDetailsPojo getVsatEventDetails() throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return vsatAutomationManager.getVsatEventDetails();
    }

    @RequestMapping(value = "/clearRedisVsatEventDetails", method = RequestMethod.DELETE)
    @ResponseBody
    public PlatformBasicResponse clearRedisVsatEventDetails() throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        vsatAutomationManager.clearRedisVsatEventDetails();
        return new PlatformBasicResponse();
    }


    //it is calling from isl repo
    @RequestMapping(value = "/changeRedisVsatEventDetails", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE ,produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void changeRedisVsatEventDetails(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                         @RequestBody String req) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(req, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            if (StringUtils.isNotEmpty(subscriptionRequest.getSubject())) {
                String message = subscriptionRequest.getMessage();
                vsatAutomationManager.changeRedisVsatEventDetails(message);
            }
        }
    }
    
}
