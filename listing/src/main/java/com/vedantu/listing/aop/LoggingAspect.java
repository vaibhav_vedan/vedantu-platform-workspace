/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.listing.aop;

import com.vedantu.util.aop.AbstractAOPLayer;
import com.vedantu.util.logstash.LogstashLayout;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@Component
@Aspect
public class LoggingAspect extends AbstractAOPLayer{

    @Before("allCtrlMethods()")
    public void beforeExecutionForCtrlMethods(JoinPoint jp) {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes.getRequest() == null) {
            return;
        }

        HttpServletRequest request = servletRequestAttributes.getRequest();
        LogstashLayout.addToMDC(request);

    }

    //tracking dao methods calls
    @Pointcut("within(com.vedantu.listing.dao.serializers..*)")
    public void allDAOMethods() {
    }        
    public static void main(String[] args) throws IOException {
System.out.println("\nSending 'GET' request to URL : ");
        String url = "https://www1.nseindia.com/ChartApp/install/charts/data/GetDataAll.jsp?Instrument=FUTSTK&CDSymbol=HDFC&Segment=CM&Series=EQ&CDExpiryMonth=1&FOExpiryMonth=1&IRFExpiryMonth=31-12-2014&CDIntraExpiryMonth=28-07-2014&FOIntraExpiryMonth=31-07-2014&IRFIntraExpiryMonth=&CDDate1=&CDDate2=&PeriodType=1&Periodicity=1&ct0=g1%7C1%7C1&ct1=g2%7C2%7C1&ctcount=2&time=1579577772975&stored=false";

        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        // optional default is GET
        con.setRequestMethod("GET");

        // add request header
        
        con.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/79.0.3945.117 Safari/537.36");
        con.setRequestProperty("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9");
        con.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.9");
        con.setRequestProperty("Connection", "keep-alive");

        int responseCode = con.getResponseCode();
        System.out.println("\nSending 'GET' request to URL : " + url);
        System.out.println("Response Code : " + responseCode);

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        // print result
        System.out.println(response.toString());
         
    }
}
