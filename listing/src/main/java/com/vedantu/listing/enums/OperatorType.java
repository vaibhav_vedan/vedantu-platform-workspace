package com.vedantu.listing.enums;

public enum OperatorType {
	UNARY, BINARY
}