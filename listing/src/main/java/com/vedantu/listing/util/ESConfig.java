package com.vedantu.listing.util;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.ImmutableSettings;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.springframework.stereotype.Service;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.io.IOException;
import javax.annotation.PreDestroy;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.springframework.beans.factory.annotation.Autowired;

@Service
public class ESConfig {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(ESConfig.class);

    private static final String host = ConfigUtils.INSTANCE.getStringValue("elasticsearch.baseurl");
    private static final int port = Integer.parseInt(ConfigUtils.INSTANCE.getStringValue("elasticsearch.port"));
    private static final String index = ConfigUtils.INSTANCE.getStringValue("elasticsearch.baseIndex");
    private static final String type = ConfigUtils.INSTANCE.getStringValue("elasticsearch.teachers.typeName");
    private static final String cluster = ConfigUtils.INSTANCE.getStringValue("elasticsearch.clusterName");
    private static final String node = ConfigUtils.INSTANCE.getStringValue("elasticsearch.nodeName");
    private TransportClient client = null;

    @SuppressWarnings("resource")
    public ESConfig() {
        ImmutableSettings.Builder builder = ImmutableSettings.settingsBuilder();
        if (cluster != null) {
            builder.put("cluster.name", cluster);
        }
        if (node != null) {
            builder.put("node.name", node);
        }

        Settings settings = builder.build();
        client = new TransportClient(settings)
                .addTransportAddress(new InetSocketTransportAddress(host, port));
        logger.info("Transport Client: " + client.transportAddresses());
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getIndex() {
        return index;
    }

    public String getType() {
        return type;
    }

    public static String getCluster() {
        return cluster;
    }

    public static String getNode() {
        return node;
    }

    public Client getTransportClient() {
        return this.client;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing es connection ", e);
        }
    }

    public static void main(String[] args) throws IOException {
        System.out.println(">>>>>>");
        ESConfig eSConfig=new ESConfig();
        Client client = eSConfig.getTransportClient();
        SearchRequestBuilder srb = client.prepareSearch(eSConfig.getIndex()).setTypes(eSConfig.getType())
				.setTrackScores(true).setExplain(true);
		srb.setSize(8);
                srb.setFrom(0);
		System.out.println("size creation don>>>>>e");

		System.out.println("from creation done");

		System.out.println("ES execution");

		System.out.println("ES execution" + srb.toString());

		SearchResponse response = srb.execute().actionGet();
		System.out.println("ES response status:" + response.status());
		System.out.println("ES response:" + response.toString());        
        System.err.println(">>done");
//        Settings settings = ImmutableSettings.settingsBuilder()
//                .build();
//        Client client = new TransportClient(settings)
//                .addTransportAddress(new InetSocketTransportAddress("wave-search.vedantu.com", 9300));
        // XXX is my server's ip address
//        IndexResponse response = client.prepareIndex("twitter", "tweet")
//                .setSource(XContentFactory.jsonBuilder()
//                        .startObject()
//                        .field("productId", "2")
//                        .field("productName", "XXX").endObject()).execute().actionGet();

        //update
//        Map<String, Set<String>> ESSubjectTargetMap = new HashMap<>();
//        Set<String> str=new HashSet<>();
//        str.add("icse");
//        ESSubjectTargetMap.put("physics", str);
//        String json=new Gson().toJson(ESSubjectTargetMap);                       
//        Map<String, Object> jsonMap = new ObjectMapper().readValue(json, HashMap.class);
////        Map<String, Object> params = ImmutableMap.of("jsonMap", jsonMap);
//        
//        String script = "ctx._source.subjectTargetMap = jsonMap";
//        System.out.println("????? "+script);
//        UpdateRequestBuilder builder = client.prepareUpdate("vedantu", "teachers", "14")
//                .setScript(script, ScriptService.ScriptType.INLINE);
//        builder.addScriptParam("jsonMap", jsonMap);        
//        UpdateResponse response=builder.get();
//        
        //create
//        String json="{\"id\":44,\"userId\":14,\"category\":\"icse\",\"grade\":\"12\",\"boards\":[2]}";
//        IndexResponse response = client.prepareIndex("vedantu", "teacherBoardMappings", "10")
//                .setSource(json).setParent("14").get();        
//        
//        
//        System.out.println("Return : " + response.toString());
//        System.out.println(response.getIndex());
//        System.out.println(response.getType());
//        System.out.println(response.getVersion());

        /*
        //update using doc
        Double teacherOneHourRate = 250.0;
        Double teacherMinPrice = 210.0;
        String doc = "{\"rate\" :" + teacherOneHourRate
                + ", \"startPrice\":" + teacherMinPrice + "}";
        String json = new Gson().toJson(doc);
        System.err.println("setting " + json);
        Long teacherId = 16l;
        UpdateResponse response = client.prepareUpdate("vedantu", "teachers", Long.toString(teacherId))
                .setDoc(doc).get();
        System.err.println("Ret?changed???urn : " + response);
        printESUpdateResponse(response);
         */
//        client.close();
    }

    private static void printESUpdateResponse(UpdateResponse response) {
        if (response != null) {
            System.err.println("id: " + response.getId());
            System.err.println("index: " + response.getIndex());
            System.err.println("type: " + response.getType());
            System.err.println("version: " + response.getVersion());
        }
    }

}
