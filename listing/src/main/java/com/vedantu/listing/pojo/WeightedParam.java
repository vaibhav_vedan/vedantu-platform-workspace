package com.vedantu.listing.pojo;

public class WeightedParam {

	String ParamName;
	Double weight;
	
	
	public WeightedParam() {
		super();
	}
	public WeightedParam(String paramName, Double weight) {
		super();
		ParamName = paramName;
		this.weight = weight;
	}
	public String getParamName() {
		return ParamName;
	}
	public void setParamName(String paramName) {
		ParamName = paramName;
	}
	public Double getWeight() {
		return weight;
	}
	public void setWeight(Double weight) {
		this.weight = weight;
	}
	@Override
	public String toString() {
		return "WeightedParam [ParamName=" + ParamName + ", weight=" + weight + "]";
	}
	
}
