package com.vedantu.listing.pojo;

import java.util.List;

public class Sort {

	private List<String> params;

	public List<String> getParams() {
		return params;
	}

	public void setParams(List<String> params) {
		this.params = params;
	}

	public static class Constants {
		public static final String PARAMS = "params";
	}

	@Override
	public String toString() {
		String s = null;
		if(params != null){
			s = "Sort [ params=" + params.toString() + "]";
		} else {
			s = "Sort [ params=" + params + "]";
		}
		return s;
	}
}