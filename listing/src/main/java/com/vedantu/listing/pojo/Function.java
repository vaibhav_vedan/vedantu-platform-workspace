package com.vedantu.listing.pojo;

import java.util.ArrayList;
import java.util.List;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.index.query.functionscore.gauss.GaussDecayFunctionBuilder;

public class Function {

	private String type;
	private String field;
	private Float weight;
	private Object origin;
	private Object scale;
	private Filter filter;
	private Double decay;
	private Object offset;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}
	
	public Float getWeight() {
		return weight;
	}

	public void setWeight(Float weight) {
		this.weight = weight;
	}
	
	public Object getOrigin() {
		return origin;
	}

	public void setOrigin(Object origin) {
		this.origin = origin;
	}
	
	public Object getScale() {
		return scale;
	}

	public void setScale(Object scale) {
		this.scale = scale;
	}

	public Filter getFilter() {
		return filter;
	}

	public void setFilter(Filter filter) {
		this.filter = filter;
	}

	public Double getDecay() {
		return decay;
	}

	public void setDecay(Double decay) {
		this.decay = decay;
	}

	public Object getOffset() {
		return offset;
	}

	public void setOffset(Object offset) {
		this.offset = offset;
	}

	public ScoreFunctionBuilder getFunction(String script){
		
		ScoreFunctionBuilder f = null;
		switch(type){
		case "gauss":
			if(field != null && scale != null){
				f = ScoreFunctionBuilders.gaussDecayFunction(field, origin, scale);
				GaussDecayFunctionBuilder g = (GaussDecayFunctionBuilder)f;
				if(decay != null){
					g.setDecay(decay);
				}
				if(offset != null){
					g.setOffset(offset);
				}
				f = (ScoreFunctionBuilder)g;
			}				
			break;
		case "exp":
			if(field != null && scale != null){
				f = ScoreFunctionBuilders.exponentialDecayFunction(field, origin, scale);
			}
			break;
		case "linear":
			if(field != null && scale != null){
				f = ScoreFunctionBuilders.linearDecayFunction(field, origin, scale);
			}
			break;
		case "random":
			if(field != null && scale != null){
				f = ScoreFunctionBuilders.randomFunction(field);
			}
			break;
		case "script":
			if(script != null){
				f = ScoreFunctionBuilders.scriptFunction(script);
			}
			break;
		default:
			break;
		}
		
		if(f != null){
			if(weight != null){
				f.setWeight(weight);
			}
		}
		return f;
	}

	public List<String> validate() {
		List<String> errors = new ArrayList<String>();

		if (type == null) {
			errors.add(Constants.TYPE);
		}
		return errors;
	}

	public static class Constants {
		public static final String TYPE = "type";
		public static final String FIELD = "field";
		public static final String WEIGHT = "weight";
		public static final String ORIGIN = "origin";
		public static final String SCALE = "scale";
		public static final String FILTER = "filter";	
	}

	@Override
	public String toString() {
		return "Function [ type=" + type + ", field=" + field + "]";
	}
}