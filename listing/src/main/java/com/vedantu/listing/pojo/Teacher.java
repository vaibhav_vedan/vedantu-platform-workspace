package com.vedantu.listing.pojo;

import com.vedantu.User.LocationInfo;
import java.util.List;
import java.util.Map;

public class Teacher {
	private Long teacherId;
	private String firstName;
	private String lastName;
	private String gender;
	private LocationInfo locationInfo;
	private String latestEducation;
	private Integer onlineStatus;
	private String professionalCategory;
	private String primaryCallingNumber;
	private Integer extensionNumber;
	private String biggestStrength;
	private boolean active;
	private Long responseTime;
	private String experience;
	private Double sessionHours;
	private Long sessions;
	private Double rating;
	private Double rate;
	private Double score;
	private String fullname;
	private Long startPrice;
	private String profilePicUrl;
	private Map<String, List<Object>> subjectTargetMap;
	private List<String> languagePrefs;
	private List<Long> boards;
	private Float scoreOffset;
	private Long shownTillDate;
	private Long shownToday;

	public Teacher() {
	}

	public Teacher(String firstName, String lastName, String gender, LocationInfo locationInfo, String latestEducation,
			Integer onlineStatus, String professionalCategory, String primaryCallingNumber, Integer extensionNumber,
			String biggestStrength, boolean active, Long responseTime, String experience, Double sessionHours,
			Long sessions, Double rating, Double rate, Double score, Long teacherId, String fullname, Long startPrice,
			String profilePicUrl, Map<String, List<Object>> subjectTargetMap, List<String> languagePrefs, List<Long> boards,
			Float scoreOffset) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.locationInfo = locationInfo;
		this.latestEducation = latestEducation;
		this.onlineStatus = onlineStatus;
		this.professionalCategory = professionalCategory;
		this.primaryCallingNumber = primaryCallingNumber;
		this.extensionNumber = extensionNumber;
		this.biggestStrength = biggestStrength;
		this.active = active;
		this.responseTime = responseTime;
		this.experience = experience;
		this.sessionHours = sessionHours;
		this.sessions = sessions;
		this.rating = rating;
		this.rate = rate;
		this.score = score;
		this.teacherId = teacherId;
		this.fullname = fullname;
		this.startPrice = startPrice;
		this.profilePicUrl = profilePicUrl;
		this.subjectTargetMap = subjectTargetMap;
		this.languagePrefs = languagePrefs;
		this.boards = boards;
		this.scoreOffset = scoreOffset;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getLatestEducation() {
		return latestEducation;
	}

	public void setLatestEducation(String latestEducation) {
		this.latestEducation = latestEducation;
	}

	public Integer getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(Integer onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public String getProfessionalCategory() {
		return professionalCategory;
	}

	public void setProfessionalCategory(String professionalCategory) {
		this.professionalCategory = professionalCategory;
	}

	public String getPrimaryCallingNumber() {
		return primaryCallingNumber;
	}

	public void setPrimaryCallingNumber(String primaryCallingNumber) {
		this.primaryCallingNumber = primaryCallingNumber;
	}

	public Integer getExtensionNumber() {
		return extensionNumber;
	}

	public void setExtensionNumber(Integer extensionNumber) {
		this.extensionNumber = extensionNumber;
	}

	public String getBiggestStrength() {
		return biggestStrength;
	}

	public void setBiggestStrength(String biggestStrength) {
		this.biggestStrength = biggestStrength;
	}

	public boolean getActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getResponseTime() {
		return responseTime;
	}

	public void setResponseTime(Long responseTime) {
		this.responseTime = responseTime;
	}

	public String getExperience() {
		return experience;
	}

	public void setExperience(String experience) {
		this.experience = experience;
	}

	public Double getSessionHours() {
		return sessionHours;
	}

	public void setSessionHours(Double sessionHours) {
		this.sessionHours = sessionHours;
	}

	public Long getSessions() {
		return sessions;
	}

	public void setSessions(Long sessions) {
		this.sessions = sessions;
	}

	public Double getRating() {
		return rating;
	}

	public void setRating(Double rating) {
		this.rating = rating;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Long getStartPrice() {
		return startPrice;
	}

	public void setStartPrice(Long startPrice) {
		this.startPrice = startPrice;
	}

	public String getProfilePicUrl() {
		return profilePicUrl;
	}

	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}

	public LocationInfo getLocationInfo() {
		return locationInfo;
	}

	public void setLocationInfo(LocationInfo locationInfo) {
		this.locationInfo = locationInfo;
	}

	public Map<String, List<Object>> getSubjectTargetMap() {
		return subjectTargetMap;
	}

	public void setSubjectTargetMap(Map<String, List<Object>> subjectTargetMap) {
		this.subjectTargetMap = subjectTargetMap;
	}

	public List<String> getLanguagePrefs() {
		return languagePrefs;
	}

	public void setLanguagePrefs(List<String> languagePrefs) {
		this.languagePrefs = languagePrefs;
	}

	public List<Long> getBoards() {
		return boards;
	}

	public void setBoards(List<Long> boards) {
		this.boards = boards;
	}

	public Float getScoreOffset() {
		return scoreOffset;
	}

	public void setScoreOffset(Float scoreOffset) {
		this.scoreOffset = scoreOffset;
	}

	public Long getShownTillDate() {
		return shownTillDate;
	}

	public void setShownTillDate(Long shownTillDate) {
		this.shownTillDate = shownTillDate;
	}

	public Long getShownToday() {
		return shownToday;
	}

	public void setShownToday(Long shownToday) {
		this.shownToday = shownToday;
	}

	@Override
	public String toString() {
		String s = "{ teacherId:" + teacherId + ", firstName:" + firstName + ", lastName:" + lastName + ", gender:"
				+ gender + ",latestEducation:" + latestEducation + ",onlineStatus:" + onlineStatus
				+ ",professionalCategory:" + professionalCategory + ",primaryCallingNumber:" + primaryCallingNumber
				+ ",extensionNumber:" + extensionNumber + ",biggestStrength:" + biggestStrength + ",active:" + active
				+ ",responseTime:" + responseTime + ",experience:" + experience + ",sessionHours:" + sessionHours
				+ ",sessions:" + sessions + ",rating:" + rating + ",rate:" + rate + ",score:" + score + "fullname:"
				+ fullname + ",startPrice" + startPrice + ",profilePicUrl:" + profilePicUrl + ",scoreOffset:"+ scoreOffset;

		if (locationInfo != null) {
			s += ",locationInfo:" + locationInfo.toString();
		}
		if (subjectTargetMap != null) {
			s += ",subjectTargetMap:" + subjectTargetMap.toString();
		}
		if (languagePrefs != null) {
			s += ",languagePrefs:" + languagePrefs.toString();
		}
		if (boards != null) {
			s += ",boards:" + boards.toString();
		}
		s += "}";
		return s;
	}
}
