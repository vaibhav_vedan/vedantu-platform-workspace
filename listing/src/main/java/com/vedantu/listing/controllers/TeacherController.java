package com.vedantu.listing.controllers;

import java.lang.Exception;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.listing.manager.FeatureMappingManager;
import com.vedantu.listing.viewobject.request.GetTeacherReq;
import com.vedantu.listing.viewobject.response.GetTeacherResponse;
import com.vedantu.util.LogFactory;


@RestController
@RequestMapping("teachers")
public class TeacherController {

	@Autowired
	private FeatureMappingManager featureMappingManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(TeacherController.class);

	@RequestMapping(value = "getTeachers/{featureId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetTeacherResponse getTeachers(@RequestBody GetTeacherReq GTR, @PathVariable("featureId") String featureId)
			throws Exception {

		logger.info("Request " + GTR.toString() + " received for getting Teacher List for feature" + featureId);

		GetTeacherResponse resp = new GetTeacherResponse();
		resp = featureMappingManager.getTeachers(featureId, GTR);

		logger.info("Response:" + resp.toString());
		return resp;
	}
}
