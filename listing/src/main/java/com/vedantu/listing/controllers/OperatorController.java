package com.vedantu.listing.controllers;

import java.util.ArrayList;
import java.util.List;
import java.lang.Exception;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vedantu.listing.dao.entity.Operator;
import com.vedantu.listing.manager.FeatureMappingManager;
import com.vedantu.listing.manager.OperatorManager;
import com.vedantu.listing.viewobject.request.GetOperatorReq;
import com.vedantu.listing.viewobject.response.GetOperatorRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;


@RestController
@RequestMapping("operator")
public class OperatorController {

	@Autowired
	private OperatorManager operatorManager;

	@Autowired
	private FeatureMappingManager featureMappingManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(OperatorController.class);

	@RequestMapping(value = "/addOperator", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Operator addOperator(@RequestBody Operator operator) throws Exception {

		operator.setLastUpdated(System.currentTimeMillis());
		operator.setEntityState(EntityState.ACTIVE);
		logger.info("Request:" + operator.toString());
		List<String> errors = validate(operator);
		Operator operatorRes;

		if (errors != null && !errors.isEmpty()) {
			logger.error("Illegal Arguments with fields " + errors);
			throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
		} else {
			operatorRes = operatorManager.operator(operator);
		}

		logger.info("Response:" + operatorRes.toString());
		return operatorRes;
	}

	@RequestMapping(value = { "getAllOperators" }, method = RequestMethod.GET)
	@ResponseBody
	public GetOperatorRes getAllOperators(@RequestParam(value = "userId", required = false) Long userId,
			@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "size", required = false) Integer limit) throws Exception {

		logger.info("Request received for getting All Operator");
		GetOperatorReq req = new GetOperatorReq(start, limit);
		GetOperatorRes res = (GetOperatorRes) operatorManager.getOperators(req);
		logger.info("Response:" + res.toString());
		return res;
	}

	@RequestMapping(value = "getOperator/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Operator getOperatorById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for getting Operator for db_id: " + id);
		Operator operatorRes = operatorManager.getOperatorById(id);
		logger.info("Response:" + operatorRes.toString());
		return operatorRes;
	}

	@RequestMapping(value = "/updateOperator/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Operator updateOperator(@RequestBody Operator operator, @PathVariable("id") String id) throws Exception {
		operator.setLastUpdated(System.currentTimeMillis());
		operator.setId(id);
		logger.info("Request:" + operator.toString());
		Operator operatorRes = operatorManager.operator(operator);
		logger.info("Response:" + operatorRes.toString());
		return operatorRes;
	}

	@RequestMapping(value = "deleteOperator/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public Operator deleteOperatorById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for disabling Operator for id: " + id);
		Operator operatorRes = operatorManager.getOperatorById(id);
		if (operatorRes != null) {
                        operatorRes.setEntityState(EntityState.INACTIVE);
			featureMappingManager.deleteOperatorMappingsByOperatorId(id);
			operatorRes = operatorManager.operator(operatorRes);
			logger.info("Response:" + operatorRes.toString());
		}
		return operatorRes;
	}

	public List<String> validate(Operator operator) {
		List<String> errors = new ArrayList<String>();

		if (operator == null || operator.getSymbol() == null) {
			errors.add(Operator.Constants.SYMBOL);
		}

		String id = operator.getId();
		String symbol = operator.getSymbol();

		logger.info("ID: " + id + " ,symbol: " + symbol);
		return errors;
	}
}
