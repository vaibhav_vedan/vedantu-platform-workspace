package com.vedantu.listing.controllers;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vedantu.listing.dao.entity.SequenceId;
import com.vedantu.listing.manager.SequenceIdManager;
import com.vedantu.listing.viewobject.request.GetSequenceReq;
import com.vedantu.listing.viewobject.response.GetSequenceRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;


@RestController
@RequestMapping("sequence")
public class SequenceIdController {

	@Autowired
	private SequenceIdManager sequenceIdManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SequenceIdController.class);

	@RequestMapping(value = "/addSequence", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SequenceId addSequence(@RequestBody SequenceId sequenceId) throws Exception {

		sequenceId.setLastUpdated(System.currentTimeMillis());
		sequenceId.setEntityState(EntityState.ACTIVE);
		logger.info("Request:" + sequenceId.toString());
		List<String> errors = validate(sequenceId);
		SequenceId sequenceRes;

		if (errors != null && !errors.isEmpty()) {
			logger.error("Illegal Arguments with fields " + errors);
			throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
		} else {
			sequenceRes = sequenceIdManager.sequenceId(sequenceId);
		}

		logger.info("Response:" + sequenceRes.toString());
		return sequenceRes;
	}

	@RequestMapping(value = "/updateSequence", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SequenceId updateSequence(@RequestBody SequenceId sequenceId) throws Exception {

		sequenceId.setLastUpdated(System.currentTimeMillis());
		logger.info("Request:" + sequenceId.toString());
		List<String> errors = validate(sequenceId);
		SequenceId sequenceRes;

		if (errors != null && !errors.isEmpty()) {
			logger.error("Illegal Arguments with fields " + errors);
			throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
		} else {
			sequenceRes = sequenceIdManager.sequenceId(sequenceId);
		}

		logger.info("Response:" + sequenceRes.toString());
		return sequenceRes;
	}

	@RequestMapping(value = "getSequence/{id}", method = RequestMethod.GET)
	@ResponseBody
	public SequenceId getSequenceById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for getting SequenceId for db_id: " + id);
		SequenceId sequenceRes = sequenceIdManager.getSequenceById(id);
		logger.info("Response:" + sequenceRes.toString());
		return sequenceRes;
	}

	@RequestMapping(value = { "getAllSequences" }, method = RequestMethod.GET)
	@ResponseBody
	public GetSequenceRes getAllSequences(@RequestParam(value = "start", required = false) Integer start,
			@RequestParam(value = "size", required = false) Integer limit) throws Exception {

		logger.info("Request received for getting All SequenceId");
		GetSequenceReq req = new GetSequenceReq(start, limit);
		List<SequenceId> sequenceIds = sequenceIdManager.getSequences(req);
		GetSequenceRes res = new GetSequenceRes(sequenceIds, sequenceIds.size());
		logger.info("Response:" + res.toString());
		return res;
	}

	@RequestMapping(value = "deleteSequenceById/{id}", method = RequestMethod.POST)
	@ResponseBody
	public SequenceId deleteSequenceById(@PathVariable("id") String id) throws Exception {
		logger.info("Request received for getting SequenceId for db_id: " + id);
		SequenceId sequenceRes = sequenceIdManager.getSequenceById(id);
		logger.info("Response:" + sequenceRes.toString());
		return sequenceRes;
	}

	public List<String> validate(SequenceId sequenceId) {
		List<String> errors = new ArrayList<String>();

		if (sequenceId == null || sequenceId.getName() == null) {
			errors.add(SequenceId.Constants.NAME);
		}

		String id = sequenceId.getId();
		String name = sequenceId.getName();

		logger.info("ID: " + id + " ,name: " + name);
		return errors;
	}
}
