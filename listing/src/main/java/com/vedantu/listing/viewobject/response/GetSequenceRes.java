package com.vedantu.listing.viewobject.response;

import java.util.List;

import com.vedantu.listing.dao.entity.SequenceId;

public class GetSequenceRes extends AbstractListRes<SequenceId> {
	private int totalCount;

	public GetSequenceRes() {
		super();
	}

	public GetSequenceRes(List<SequenceId> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
