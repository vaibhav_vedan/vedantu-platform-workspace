package com.vedantu.listing.viewobject.response;

import java.util.List;

import com.vedantu.listing.dao.entity.FeatureMapping;

public class GetFeatureMappingRes extends AbstractListRes<FeatureMapping> {
	private int totalCount;

	public GetFeatureMappingRes() {
		super();
	}

	public GetFeatureMappingRes(List<FeatureMapping> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
