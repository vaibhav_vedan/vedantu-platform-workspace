package com.vedantu.listing.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetFeatureReq extends AbstractFrontEndListReq {

	public GetFeatureReq() {
		super();
	}

	public GetFeatureReq(Integer start, Integer limit) {
		super(start, limit);
	}

}
