package com.vedantu.listing.viewobject.response;

import java.util.List;

import com.vedantu.listing.dao.entity.Feature;

public class GetFeatureRes extends AbstractListRes<Feature> {
	private int totalCount;

	public GetFeatureRes() {
		super();
	}

	public GetFeatureRes(List<Feature> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
