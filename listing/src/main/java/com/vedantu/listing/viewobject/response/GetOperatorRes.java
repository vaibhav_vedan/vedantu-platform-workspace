package com.vedantu.listing.viewobject.response;

import java.util.List;

import com.vedantu.listing.dao.entity.Operator;

public class GetOperatorRes extends AbstractListRes<Operator> {
	private int totalCount;

	public GetOperatorRes() {
		super();
	}

	public GetOperatorRes(List<Operator> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
