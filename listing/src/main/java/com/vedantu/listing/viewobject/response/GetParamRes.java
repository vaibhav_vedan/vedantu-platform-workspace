package com.vedantu.listing.viewobject.response;

import java.util.List;

import com.vedantu.listing.dao.entity.Param;

public class GetParamRes extends AbstractListRes<Param> {
	private int totalCount;

	public GetParamRes() {
		super();
	}

	public GetParamRes(List<Param> list, int totalCount) {
		super();
		this.setList(list);
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
