package com.vedantu.listing.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;


public class GetSequenceReq extends AbstractFrontEndListReq {

	public GetSequenceReq() {
		super();
	}

	public GetSequenceReq(Integer start, Integer limit) {
		super(start, limit);
	}

}
