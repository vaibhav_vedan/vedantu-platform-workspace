package com.vedantu.listing.viewobject.response;

import java.util.List;

import com.vedantu.listing.pojo.Teacher;

public class GetTeacherResponse {

	long hits;
	List<Teacher> teachers;
	
	public long getHits() {
		return hits;
	}
	public void setHits(long hits) {
		this.hits = hits;
	}
	public List<Teacher> getTeachers() {
		return teachers;
	}
	public void setTeachers(List<Teacher> teachers) {
		this.teachers = teachers;
	}
	
}
