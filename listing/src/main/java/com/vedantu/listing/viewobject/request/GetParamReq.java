package com.vedantu.listing.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetParamReq extends AbstractFrontEndListReq {

	public GetParamReq() {
		super();
	}

	public GetParamReq(Integer start, Integer limit) {
		super(start, limit);
	}

}
