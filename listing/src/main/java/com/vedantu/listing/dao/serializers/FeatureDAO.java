package com.vedantu.listing.dao.serializers;

import java.util.List;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.vedantu.listing.dao.entity.Feature;
import com.vedantu.listing.mongo.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class FeatureDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public FeatureDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }

	public void create(Feature p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void updateAll(List<Feature> p) {
		try {
			if (p != null && !p.isEmpty()) {
				insertAllEntities(p, Feature.class.getSimpleName());
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public Feature getById(String id) {
		Feature feature = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				feature = (Feature) getEntityById(id, Feature.class);
			}
		} catch (Exception ex) {
			// log Exception
			feature = null;
		}
		return feature;
	}

	public void update(Query q, Update u) {
		try {
			updateFirst(q, u,Feature.class);
		} catch (Exception ex) {
		}
	}

	public int deleteById(Long id) {
		int result = 0;
		try {
			result = deleteEntityById(id, Feature.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
