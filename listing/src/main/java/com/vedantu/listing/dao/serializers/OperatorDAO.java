package com.vedantu.listing.dao.serializers;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.vedantu.listing.dao.entity.Operator;
import com.vedantu.listing.mongo.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class OperatorDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public OperatorDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }

	public void create(Operator p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}


	public Operator getById(String id) {
		Operator feature = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				feature = (Operator) getEntityById(id, Operator.class);
			}
		} catch (Exception ex) {
			// log Exception
			feature = null;
		}
		return feature;
	}

        public void update(Query q, Update u) {
            try {
                updateFirst(q, u, Operator.class);
            } catch (Exception ex) {
            }
        }

	public int deleteById(Long id) {
		int result = 0;
		try {
			result = deleteEntityById(id, Operator.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}

}
