package com.vedantu.listing.dao.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class SequenceId extends AbstractMongoStringIdEntity {

	private String name;
	private long seq;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSeq() {
		return seq;
	}

	public void setSeq(long seq) {
		this.seq = seq;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String NAME = "name";
		public static final String SEQ = "seq";
	}

}