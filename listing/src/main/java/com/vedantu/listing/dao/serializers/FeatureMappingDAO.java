package com.vedantu.listing.dao.serializers;

import java.util.List;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.vedantu.listing.dao.entity.FeatureMapping;
import com.vedantu.listing.mongo.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class FeatureMappingDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public FeatureMappingDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
        
	public void create(FeatureMapping featureMapping) {
		try {
			if (featureMapping != null) {
				saveEntity(featureMapping);
			}
		} catch (Exception ex) {
		}
	}

	public FeatureMapping getById(String id) {
		FeatureMapping featureMapping = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				featureMapping = (FeatureMapping) getEntityById(id, FeatureMapping.class);
			}
		} catch (Exception ex) {
			// log Exception
			featureMapping = null;
		}
		return featureMapping;
	}

	public void update(Query q, Update u) {
		try {
			updateFirst(q, u,FeatureMapping.class);
		} catch (Exception ex) {
		}
	}

	public void updateAll(List<FeatureMapping> p) {
		try {
			if (p != null) {
				insertAllEntities(p, FeatureMapping.class.getSimpleName());
			}
		} catch (Exception ex) {
			// throw Exception;
		}
	}

	public int deleteById(Long id) {
		int result = 0;
		try {
			result = deleteEntityById(id, FeatureMapping.class);
		} catch (Exception ex) {
			// throw Exception;
		}

		return result;
	}
}
