package com.vedantu.listing.dao.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vedantu.listing.enums.ParamType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Param extends AbstractMongoStringIdEntity {
	@JsonProperty
	private String name;
	private ParamType type;
	private Double value;

	public Param() {
		super();
	}

	public Param(String id, String name) {
		super();
		super.setId(id);
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ParamType getType() {
		return type;
	}

	public void setType(ParamType type) {
		this.type = type;
	}

	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String NAME = "name";
	}
}
