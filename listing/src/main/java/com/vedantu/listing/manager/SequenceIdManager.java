package com.vedantu.listing.manager;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import com.vedantu.listing.dao.entity.SequenceId;
import com.vedantu.listing.viewobject.request.GetSequenceReq;
import com.vedantu.util.LogFactory;
import com.vedantu.listing.dao.serializers.SequenceIdDAO;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

@Service
public class SequenceIdManager {

    @Autowired
    public SequenceIdDAO sequenceDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SequenceIdManager.class);

    public SequenceId sequenceId(SequenceId sequenceId) {

        SequenceIdDAO sequenceDAO = new SequenceIdDAO();
        String id = sequenceId.getId();
        logger.info("DT: ");

        if (id == null) {
            sequenceDAO.create(sequenceId);
        } else {
            Update update = createUpdateObject(sequenceId);
            Query query = new Query();
            query.addCriteria(Criteria.where("_id").is(id));
            sequenceDAO.update(query, update);
            sequenceId = (SequenceId) sequenceDAO.getEntityById(id, SequenceId.class);
        }

        logger.info("ID:");
        return sequenceId;
    }

    public SequenceId getSequenceById(String id) {
        SequenceId sequenceId = sequenceDAO.getById(id);
        return sequenceId;
    }

    public List<SequenceId> getSequences(GetSequenceReq req) {
        Query query = new Query();
        query.addCriteria(Criteria.where("enabled").is(true));
        query.with(Sort.by(Sort.Direction.DESC, SequenceId.Constants.CREATION_TIME));
        long totalCount = sequenceDAO.queryCount(query, SequenceId.class);
        logger.info("count query :" + query + totalCount);
        Integer start = req.getStart();
        Integer limit = req.getSize();
        if (start == null || start < 0) {
            start = 0;
        }

        if (limit == null || limit <= 0) {
            limit = 20;
        }

        query.skip(start);
        query.limit(limit);
        List<SequenceId> results = sequenceDAO.runQuery(query, SequenceId.class);
        return results;
    }

    public Update createUpdateObject(SequenceId sequenceId) {

        if (sequenceId == null) {
            return null;
        }
        Update update = new Update();
        String name = sequenceId.getName();
        Long seq = sequenceId.getSeq();
        EntityState entityState = sequenceId.getEntityState();

        if (name != null) {
            update.set("name", name);
        }

        update.set("entityState", entityState);

        if (name != null) {
            update.set("name", name);
        }
        update.set("seq", seq);

        return update;
    }

}
