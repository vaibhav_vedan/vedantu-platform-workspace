package com.vedantu.listing.manager;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import com.vedantu.listing.dao.entity.Feature;
import com.vedantu.listing.viewobject.request.GetFeatureReq;
import com.vedantu.util.LogFactory;
import com.vedantu.listing.dao.serializers.FeatureDAO;
import com.vedantu.listing.dao.serializers.SequenceIdDAO;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

@Service
public class FeatureManager {

	@Autowired
	public FeatureDAO featureDAO;

	@Autowired
	public SequenceIdDAO sequenceIdDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(FeatureManager.class);

	public Feature feature(Feature feature) {

		FeatureDAO featureDAO = new FeatureDAO();
		String id = feature.getId();
		logger.info("DT: ");

		if (id == null) {
			Long seq_id = sequenceIdDAO.getNextSequenceId("feature");
			feature.setId(seq_id.toString());
			featureDAO.create(feature);
		} else {
			Update update = createUpdateObject(feature);
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(id));
			featureDAO.update(query, update);
			feature = (Feature) featureDAO.getEntityById(id, Feature.class);
		}

		logger.info("ID:");
		return feature;
	}

	public Feature getFeatureById(String id) {
		Feature feature = featureDAO.getById(id);
		return feature;
	}

	public List<Feature> getFeatures(GetFeatureReq req) {
		Query query = new Query();
		query.addCriteria(Criteria.where("enabled").is(true));
		query.with(Sort.by(Sort.Direction.DESC, Feature.Constants.CREATION_TIME));
		Long totalCount = featureDAO.queryCount(query, Feature.class);
		logger.info("count query :" + query + totalCount);
		Integer start = req.getStart();
		Integer limit = req.getSize();
		if (start == null || start < 0) {
			start = 0;
		}

		if (limit == null || limit <= 0) {
			limit = 20;
		}

		query.skip(start);
		query.limit(limit);
		List<Feature> results = featureDAO.runQuery(query, Feature.class);
		return results;
	}

	public Update createUpdateObject(Feature feature) {

		if (feature == null) {
			return null;
		}
		Update update = new Update();
		String name = feature.getName();
                EntityState entityState = feature.getEntityState();

		if (name != null){
                    update.set("name", name);
                }

		update.set("entityState", entityState);

		return update;
	}
}
