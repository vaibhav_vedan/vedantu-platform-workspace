package com.vedantu.listing.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.index.query.FilterBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.exception.ErrorCode;
import com.vedantu.listing.dao.entity.FeatureMapping;
import com.vedantu.listing.dao.entity.Operator;
import com.vedantu.listing.dao.entity.Param;
import com.vedantu.listing.dao.serializers.FeatureMappingDAO;
import com.vedantu.listing.dao.serializers.OperatorDAO;
import com.vedantu.listing.dao.serializers.ParamDAO;
import com.vedantu.listing.dao.serializers.SequenceIdDAO;
import com.vedantu.listing.enums.OperatorType;
import com.vedantu.listing.enums.ParamType;
import com.vedantu.listing.pojo.Filter;
import com.vedantu.listing.pojo.Function;
import com.vedantu.listing.pojo.Teacher;
import com.vedantu.listing.pojo.Tuple;
import com.vedantu.listing.pojo.WeightedParam;
import com.vedantu.listing.util.ESConfig;
import com.vedantu.listing.util.StringUtils;
import com.vedantu.listing.viewobject.request.GetFeatureMappingReq;
import com.vedantu.listing.viewobject.request.GetTeacherReq;
import com.vedantu.listing.viewobject.response.BaseResponse;
import com.vedantu.listing.viewobject.response.GetTeacherResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

@Service
public class FeatureMappingManager {

	@Autowired
	public FeatureMappingDAO featureMappingDAO;

	@Autowired
	public OperatorDAO operatorDAO;

	@Autowired
	public ParamDAO paramDAO;

	@Autowired
	public SequenceIdDAO sequenceIdDAO;

	@Autowired
	private LogFactory logFactory;


	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(FeatureMappingManager.class);

	@Autowired
	private ESConfig eSConfig;

	public FeatureMapping featureMapping(FeatureMapping featureMapping) {

		String id = featureMapping.getId();

		if (id == null) {
			Long seq_id = sequenceIdDAO.getNextSequenceId("featureMapping");
			featureMapping.setId(seq_id.toString());
			featureMappingDAO.create(featureMapping);
		} else {
			Update update = createUpdateObject(featureMapping);
			Query query = new Query();
			query.addCriteria(Criteria.where("_id").is(id));
			featureMappingDAO.update( query, update);
			featureMapping = (FeatureMapping) featureMappingDAO.getEntityById(id, FeatureMapping.class);
		}

		return featureMapping;
	}

	public FeatureMapping getFeatureMappingById(String id) {
		FeatureMapping featureMapping = featureMappingDAO.getById(id);
		return featureMapping;
	}

	public void deleteFeatureMappingsByFeatureId(String featureId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("featureId").is(featureId));
		List<FeatureMapping> featureMappings = featureMappingDAO.runQuery(query, FeatureMapping.class);
		if (featureMappings == null || featureMappings.isEmpty()) {
		} else {
			for (FeatureMapping f : featureMappings) {
				f.setEntityState(EntityState.INACTIVE);
				featureMapping(f);
			}
		}
	}

	public void deleteOperatorMappingsByOperatorId(String operatorId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("operatorId").is(operatorId));
		List<FeatureMapping> featureMappings = featureMappingDAO.runQuery(query, FeatureMapping.class);
		if (featureMappings == null || featureMappings.isEmpty()) {
		} else {
			for (FeatureMapping f : featureMappings) {
				f.setEntityState(EntityState.INACTIVE);
				featureMapping(f);
			}
		}
	}

	public void deleteFeatureMappingsByParamId(String paramId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("paramId").is(paramId));
		List<FeatureMapping> featureMappings = featureMappingDAO.runQuery(query, FeatureMapping.class);

		if (featureMappings != null && !featureMappings.isEmpty()) {
			for (FeatureMapping f : featureMappings) {
				/* Unary Op for Param deleted here */
				f.setEntityState(EntityState.INACTIVE);
				featureMapping(f);
				Integer uOpSeq = f.getOpSequence();
				Query query2 = new Query();
				/* Binary Op for Param deleted here */
				query2.addCriteria(Criteria.where("opSequence").is(uOpSeq - 1));
				query2.addCriteria(Criteria.where("operatorType").is(OperatorType.BINARY));
				List<FeatureMapping> featureMappings2 = featureMappingDAO.runQuery(query2, FeatureMapping.class);
				if (featureMappings2 != null && !featureMappings2.isEmpty()) {
					FeatureMapping f2 = featureMappings2.get(0);
					f2.setEntityState(EntityState.INACTIVE);
					featureMapping(f2);
				}
			}
		}
	}

	public GetTeacherResponse getTeachers(String featureId, GetTeacherReq GTR) throws Exception {
		logger.info("Request getTeachers");
		String script = createScoreScript(featureId);
		GetTeacherResponse resp = getTeachersFromES(script, GTR);
		return resp;
	}

	public String createScoreScript(String featureId) {

		logger.info("Creating Score Script for feature: " + featureId);
		String script = null;
		Query query = new Query();
		query.addCriteria(Criteria.where("featureId").is(featureId));
		query.addCriteria(Criteria.where("enabled").is(true));
		List<FeatureMapping> featureMappings = featureMappingDAO.runQuery(query, FeatureMapping.class);

		if (featureMappings == null || featureMappings.isEmpty()) {
			logger.error("feature mapping with featureId " + featureId + " doesn't exist");
		} else {
			List<Operator> uOperators = new ArrayList<Operator>();
			List<Double> weights = new ArrayList<Double>();
			Map<Double, String> weightOpMapping = new HashMap<Double, String>();
			List<Operator> bOperators = new ArrayList<Operator>();
			List<Param> params = new ArrayList<Param>();
			Map<Integer, String> bOpsecOpMapping = new HashMap<Integer, String>();
			Map<Integer, Boolean> bOpOrderMap = new HashMap<Integer, Boolean>();

			Collections.sort(featureMappings, new Comparator<FeatureMapping>() {
				@Override
				public int compare(FeatureMapping f1, FeatureMapping f2) {
					return new CompareToBuilder().append(f1.getOperatorType(), f2.getOperatorType())
							.append(f1.getOpSequence(), f2.getOpSequence()).toComparison();
				}
			});

			for (int i = 0; i < featureMappings.size(); ++i) {
				logger.info(featureMappings.get(i).toString());
			}

			for (int i = 0; i < featureMappings.size(); ++i) {

				String operatorId = featureMappings.get(i).getOperatorId();
				String secOpSymbol = featureMappings.get(i).getSecOpSymbol();
				Double weight = featureMappings.get(i).getWeight();
				String weightOp = featureMappings.get(i).getWeightOp();
				Boolean bOpOrder = featureMappings.get(i).getbOpOrder();

				Query q1 = new Query();
				q1.addCriteria(Criteria.where("_id").is(operatorId));
				q1.addCriteria(Criteria.where("enabled").is(true));				
				List<Operator> operators1 = operatorDAO.runQuery(q1, Operator.class);
				if (operators1 == null || operators1.isEmpty()) {
					logger.error("operators with operatorId " + operatorId + " doesn't exist");
				} else {
					Operator operator = operators1.get(0);
					OperatorType operatorType = operator.getType();

					if (operatorType.equals(OperatorType.UNARY)) {
						uOperators.add(operator);
						weights.add(weight);
						weightOpMapping.put(weight, weightOp);
					} else {
						bOperators.add(operator);
						bOpsecOpMapping.put(bOperators.size() - 1, secOpSymbol);
						bOpOrderMap.put(bOperators.size() - 1, bOpOrder);
					}

					String paramId = featureMappings.get(i).getParamId();
					if (operatorType.equals(OperatorType.UNARY) && !paramId.equals(null)) {
						Query q2 = new Query();
						q2.addCriteria(Criteria.where("_id").is(paramId));
						q2.addCriteria(Criteria.where("enabled").is(true));
						List<Param> params1 = paramDAO.runQuery(q2, Param.class);
						if (params1 == null || params1.isEmpty()) {
							logger.error("params with paramId " + paramId + " doesn't exist");
						} else {
							Param param = params1.get(0);
							params.add(param);
						}
					}
				}
			}

			List<String> paramScripts = new ArrayList<String>();
			for (int i = 0; i < featureMappings.size(); ++i) {

				if (featureMappings.get(i).getOperatorType().equals(OperatorType.UNARY)) {
					try {
						String s = uOperators.get(i).getSymbol();
						Param p = params.get(i);
						if (p.getType().equals(ParamType.DYNAMIC)) {
							s = "(" + s + "(doc['" + p.getName() + "'].value))";
						} else {
							s = "(" + s + p.getValue() + ")";
						}
						Double w = weights.get(i);
						String wOp = weightOpMapping.get(w);

						if (wOp != null) {
							s = "(" + s + wOp + w + ")";
						}
						paramScripts.add(s);
					} catch (IllegalArgumentException e) {
					}
				}
			}

			script = paramScripts.get(0);
			for (int i = 1; i < paramScripts.size(); ++i) {
				Operator o = bOperators.get(i - 1);
				String secSymbol = bOpsecOpMapping.get(i - 1);
				Boolean bOpOrder = bOpOrderMap.get(i - 1);
				if (bOpOrder) {
					script = "(" + paramScripts.get(i) + o.getSymbol() + script + ")";
				} else {
					script = "(" + script + o.getSymbol() + paramScripts.get(i) + ")";
				}
				if (!secSymbol.equals(null)) {
					script = "(" + secSymbol + script + ")";
				}
			}

			logger.info("Created Script: " + script);
		}
		return script;
	}

	public List<FeatureMapping> getFeatureMappings(GetFeatureMappingReq req) {
		Query query = new Query();
		query.addCriteria(Criteria.where("enabled").is(true));
		query.with(Sort.by(Sort.Direction.DESC, FeatureMapping.Constants.CREATION_TIME));
		Long totalCount = featureMappingDAO.queryCount(query, FeatureMapping.class);
		logger.info("count query :" + query + totalCount);
		Integer start = req.getStart();
		Integer limit = req.getSize();
		if (start == null || start < 0) {
			start = 0;
		}

		if (limit == null || limit <= 0) {
			limit = 20;
		}

		query.skip(start);
		query.limit(limit);
		List<FeatureMapping> results = featureMappingDAO.runQuery(query, FeatureMapping.class);
		return results;
	}

	public Update createUpdateObject(FeatureMapping featureMapping) {

		if (featureMapping == null) {
			return null;
		}

		Update update = new Update();
		String featureId = featureMapping.getFeatureId();
		String paramId = featureMapping.getParamId();
		Double weight = featureMapping.getWeight();
		String weightOp = featureMapping.getWeightOp();
		String operatorId = featureMapping.getOperatorId();
		OperatorType opType = featureMapping.getOperatorType();
		Boolean bOpOrder = featureMapping.getbOpOrder();
		String secOpSymbol = featureMapping.getSecOpSymbol();
		Integer opSequence = featureMapping.getOpSequence();
		Long lastUpdated = featureMapping.getLastUpdated();
		EntityState entityState = featureMapping.getEntityState();

		if (featureId != null)
			update.set("featureId", featureId);

		if (paramId != null)
			update.set("paramId", paramId);

		if (weight != null)
			update.set("weight", weight);

		if (weightOp != null)
			update.set("weightOp", weightOp);

		if (operatorId != null)
			update.set("operatorId", operatorId);

		if (opType != null)
			update.set("operatorType", opType);

		if (bOpOrder != null)
			update.set("bOpOrder", bOpOrder);

		if (secOpSymbol != null)
			update.set("secOpSymbol", secOpSymbol);

		if (opSequence != null)
			update.set("opSequence", opSequence);

		update.set("lastUpdated", lastUpdated);

		update.set("entityState", entityState);

		return update;
	}

	public GetTeacherResponse getTeachersFromES(String script, GetTeacherReq GTR) throws Exception {

		logger.info("Getting Teachers info from ES");
		Client client = eSConfig.getTransportClient();
		SearchRequestBuilder srb = client.prepareSearch(eSConfig.getIndex()).setTypes(eSConfig.getType())
				.setTrackScores(true).setExplain(true);

		List<Long> teacherIds = new ArrayList<Long>();
//		if (GTR.getOfferingId() != null) {
//			String offeringEndpoint = ConfigUtils.INSTANCE.getStringValue("userFetchUrl");
//			ClientResponse resp = WebUtils.INSTANCE.doCall(
//					offeringEndpoint + "offering/getOfferingTeacherIds?offeringId=" + GTR.getOfferingId(),
//					HttpMethod.GET, null);
//			String offeringResponse = resp.getEntity(String.class);
//			Type LongType = new TypeToken<List<Long>>() {
//			}.getType();
//
//			if (offeringResponse != null && !offeringResponse.isEmpty()) {
//				teacherIds = new Gson().fromJson(offeringResponse, LongType);
//			}
//		}
                if (GTR.getOfferingId() != null && GTR.getTeacherIds()!= null) {
                    teacherIds = GTR.getTeacherIds();
                }

		Tuple funcs = GTR.getFunctions();
		Filter f = GTR.getFilter();
		if (f != null) {
			srb.setQuery(createQuery(script, f, funcs, teacherIds));
		}

		logger.info("Sort creation");
		com.vedantu.listing.pojo.Sort sort = GTR.getSort();
		if (sort != null && sort.getParams() != null && !sort.getParams().isEmpty()) {
			for (String i : sort.getParams()) {
				srb.addSort(i, SortOrder.DESC);
			}
			logger.info(" Inside Sort creation");
		}
		logger.info("Sort creation done");

		if (GTR.getSize() != null) {
			srb.setSize(GTR.getSize());
		}
		logger.info("size creation done");

		if (GTR.getFrom() != null) {
			srb.setFrom(GTR.getFrom());
		}
		logger.info("from creation done");

		logger.info("ES execution");

		logger.info("ES execution" + srb.toString());

		SearchResponse response = srb.execute().actionGet();
		logger.info("ES response status:" + response.status());
		logger.info("ES response:" + response.toString());

		SearchHit[] hits = response.getHits().getHits();
		long count = response.getHits().totalHits();
		logger.info("Total Hits: " + count + " Hits: " + hits.length);

		List<Teacher> teachers = new ArrayList<Teacher>();
		Gson gson = new Gson();
		for (SearchHit i : response.getHits().getHits()) {
			Map<String, Object> m = i.getSource();
			Double x = (double) i.getScore();
			logger.info(" Score=" + i.getScore());

			String filterJson = gson.toJson(m);

			if (!StringUtils.isEmpty(filterJson)) {
				Teacher t = null;
				t = gson.fromJson(filterJson, Teacher.class);
				t.setScore(x);
				teachers.add(t);
				logger.info("Teachers Info: " + t.toString());
			}

			// Teacher t = new Teacher();
			// if (m.get("firstName") != null) {
			// t.setFirstName((String)m.get("firstName"));
			// }
			// if (m.get("lastName") != null) {
			// t.setLastName((String)m.get("lastName"));
			// }
			// if (m.get("gender") != null) {
			// t.setGender((String)m.get("gender"));
			// }
			// if (m.get("locationInfo.country") != null) {
			// t.setCountry(m.get("locationInfo.country").getValue());
			// }
			// if (m.get("locationInfo.state") != null) {
			// t.setState(m.get("locationInfo.state").getValue());
			// }
			// if (m.get("locationInfo.city") != null) {
			// t.setCity(m.get("locationInfo.city").getValue());
			// }
			// if (m.get("onlineStatus") != null) {
			// t.setOnlineStatus(m.get("onlineStatus").getValue());
			// }
			// if (m.get("professionalCategory") != null) {
			// t.setProfessionalCategory(m.get("professionalCategory").getValue());
			// }
			// if (m.get("primaryCallingNumber") != null) {
			// t.setPrimaryCallingNumber(m.get("primaryCallingNumber").getValue());
			// }
			// if (m.get("latestEducation") != null) {
			// t.setLatestEducation(m.get("latestEducation").getValue());
			// }
			// if (m.get("extensionNumber") != null) {
			// t.setExtensionNumber(m.get("extensionNumber").getValue());
			// }
			// if (m.get("biggestStrength") != null) {
			// t.setBiggestStrength(m.get("biggestStrength").getValue());
			// }
			// if (m.get("active") != null) {
			// t.setActive(m.get("active").getValue());
			// }
			// if (m.get("responseTime") != null) {
			// String s = m.get("responseTime").getValue().toString();
			// Long l = Long.parseLong(s);
			// t.setResponseTime(l);
			// }
			// if (m.get("experience") != null) {
			// t.setExperience(m.get("experience").getValue());
			// }
			// if (m.get("sessionHours") != null) {
			// String s = m.get("sessionHours").getValue().toString();
			// Double d = Double.parseDouble(s);
			// t.setSessionHours(d);
			// }
			// if (m.get("sessions") != null) {
			// String s = m.get("sessions").getValue().toString();
			// Long l = Long.parseLong(s);
			// t.setSessions(l);
			// }
			// if (m.get("rating") != null) {
			// t.setRating(Double.parseDouble(m.get("rating").getValue().toString()));
			// }
			// if (m.get("rate") != null) {
			// t.setRate(Double.parseDouble(m.get("rate").getValue().toString()));
			// }
			// if (m.get("teacherId") != null) {
			// String s = m.get("teacherId").getValue().toString();
			// Long l = Long.parseLong(s);
			// t.setTeacherId(l);
			// }
			// if (m.get("fullname") != null) {
			// t.setFullname(m.get("fullname").getValue());
			// }
			// if (m.get("startPrice") != null) {
			// String s = m.get("startPrice").getValue().toString();
			// Long l = Long.parseLong(s);
			// t.setStartPrice(l);
			// }
			// if (m.get("profilePicUrl") != null) {
			// t.setProfilePicUrl(m.get("profilePicUrl").getValue());
			// }
			// if (m.get("pincode") != null) {
			// String s = m.get("pincode").getValue().toString();
			// Long l = Long.parseLong(s);
			// t.setPincode(l);
			// }
			// if (m.get("street") != null) {
			// t.setStreet(m.get("street").getValue());
			// }

		}

		GetTeacherResponse resp = new GetTeacherResponse();
		resp.setHits(count);
		resp.setTeachers(teachers);
		return resp;
	}

	QueryBuilder createQuery(String script, Filter filter, Tuple functions, List<Long> teacherIds) throws Exception {

		logger.info("Creating ES Query");
		QueryBuilder q1;
		if (teacherIds != null && !teacherIds.isEmpty()) {
			String teacherIdsQueryString = StringUtils.join(teacherIds.toArray(), ",");
			String[] teacherIds1 = teacherIdsQueryString.split(",");
			q1 = QueryBuilders.idsQuery("teachers").ids(teacherIds1);
		} else {
			q1 = QueryBuilders.matchAllQuery();
		}
		FunctionScoreQueryBuilder qb = QueryBuilders.functionScoreQuery(q1).scoreMode("sum")
				.boostMode(CombineFunction.REPLACE);

		if (functions != null && functions.getValue() != null) {
			for (Object o : functions.getValue()) {
				String json = new Gson().toJson(o);
				Function func = new Gson().fromJson(json, Function.class);
				Filter f = func.getFilter();
				ScoreFunctionBuilder function = func.getFunction(script);
				if (f != null && function != null) {
					qb.add(f.getFilter(), function);
				} else if (function != null) {
					qb.add(function);
				}
			}
		}

		FilterBuilder fb = null;
		try {
			fb = filter.getFilter();
		} catch (Exception e) {
			logger.info(e.toString());
		}

		@SuppressWarnings("deprecation")
		QueryBuilder query = QueryBuilders.filtered(qb, fb);
		logger.info("Created Query: " + query.toString());
		return query;
	}

	public BaseResponse updateFeatureMappingWeight(WeightedParam weightedParam) {

		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(weightedParam.getParamName()));
		List<Param> results = paramDAO.runQuery(query, Param.class);
		query = new Query();
		if (results.size() > 0) {
				query = new Query();
				//change later
				query.addCriteria(Criteria.where("paramId").is(results.get(0).getId()));
				logger.info("results size:"+results.size());
				logger.info("param id to update:"+results.get(0).getId());
				List<FeatureMapping> featureMappings = featureMappingDAO.runQuery(query, FeatureMapping.class);
				logger.info("feature mappings size:"+featureMappings.size());
				if (featureMappings.size() > 0) {
					logger.info("feature mapping id :"+featureMappings.get(0).getId());
					FeatureMapping featureMapping=featureMappings.get(0);
					featureMapping.setWeight(weightedParam.getWeight());
					List<FeatureMapping> updatedFeatureMappings=new ArrayList<FeatureMapping>();
					updatedFeatureMappings.add(featureMapping);
					featureMappingDAO.updateAll(updatedFeatureMappings);
				}
				
		}
		BaseResponse baseResponse=new BaseResponse();
		baseResponse.setErrorCode(ErrorCode.SUCCESS);
		return baseResponse;

	}
}
