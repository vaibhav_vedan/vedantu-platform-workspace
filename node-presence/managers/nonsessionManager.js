/* global exports, process */

var dao = require(process.env["APP_HOME"] + "/daos/nonsessiondao.js");
var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var socketio = require(process.env["APP_HOME"] + "/modules/socketio.js");
var mongo = require(process.env["APP_HOME"] + "/modules/mongo.js");
var esclientInstance = require(process.env["APP_HOME"] + "/modules/esclient.js");
var Q = require("q");

// var informOthers = function (userId, type, statusKey, statusVal) {
//     dao.getUserIdListners(userId).then(function (data) {
//         if (data.success && data.resp.length > 0) {
//             var interestedSocketIds = [], list = data.resp, l = list.length;
//             logger.log(l + " number of people are interested in status of " + userId + " to " + statusVal);
//             for (var k = 0; k < l; k++) {
//                 interestedSocketIds.push(list[k].split(";;")[0]);
//             }
//             socketio.informSockets(interestedSocketIds, userId, type, statusKey, statusVal);
//         }
//     });
// };

exports.onConnected = function (socketId, userId, deviceId, deviceType) {
    logger.log("Added socketId " + socketId + " to userId " + userId + "'s socketlist");
    dao.addSocketIdForUser(socketId, userId);


    //might have done this only if the this socketId was the first one, but doing it 
    //for all the onconnected events, because at any given time  person might connect to not more
    //than 2 to 3 systems at the same time    
    //logger.log("Informing others that userId " + userId + " came online");
    var status = "ONLINE";


////remove this when once we open chat app, for now OFFLINE is default statusType
//    //remove from here ---------
//    var statusType = "OFFLINE";
//    dao.addStatusTypeForUser(userId, statusType);
//    informOthers(userId, "INFORM_OTHERS_ABOUT_STATUS_TYPE", "statusType", statusType);
//    socketio.tellAllUserSocketsStatusChanged(userId, "MY_STATUS_TYPE_CHANGED", "statusType", statusType);
//    //--------------- till here

    //fetch rooms of this user and join him
    //dao.getRoomsForUser(userId).then(function (data) {
    //    if (data.success && data.resp.length > 0) {
    //        socketio.joinUserToRooms(socketId, data.resp);
    //    }
    //});

    //TODO sending this info to listeners when this user marked himself as OFFLINE/INVISIBLE is harmful
    //informOthers(userId, "INFORM_OTHERS_ABOUT_ONLINE_STATUS", "status", status);

    //telling es client to update the status if it is not online/ready_to_teach
    dao.getStatusTypeForUser(userId).then(function (data) {
        if (data.success && data.resp) {
            //there is a status set and it is not expired, so sending it to the user
            //informOthers(userId, "INFORM_OTHERS_ABOUT_STATUS_TYPE", "statusType", data.resp);
            logger.log(data.resp);
            dao.removeExpiryStatusTypeForUser(userId);
            mongo.saveStatusTypeChangeItem(userId, deviceId, deviceType, {type: "STATUS_TYPE", value: data.resp, details: {action: "AUTO"}});
            if (data.resp === "READY_TO_TEACH") {
                status = data.resp;
                dao.addStatusTypeForUser(userId, data.resp);
            }
            else
                dao.addStatusTypeForUser(userId, "NONE");
            
        }

        //or if for a normal 1-to-1 chat, fetch the friends and tell all of them        
        esclientInstance.updateStatusInES(status, userId);
    });

    //mongo.saveStatusChangeItem(userId, deviceId, deviceType, {type: "ONLINE_STATUS", value: "ONLINE"});
};
exports.onDisconnected = function (socketId, userId, userIdsToListenTo, deviceId, deviceType) {
    dao.removeSocketIdForUser(socketId, userId);

    //mongo.saveStatusChangeItem(userId, deviceId, deviceType, {type: "ONLINE_STATUS", value: "OFFLINE"});
    var status = "OFFLINE";
    //var statusType = "OFFLINE";

    // dao.getStatusTypeForUser(userId).then(function (data) {
    //             if (data.success && data.resp) {
    //                 logger.log(data.resp);
    //             }
    //         });


    //check if the user has gone completely offline, tell the interested parties    
    dao.getSocketIdsCountForUser(userId).then(function (data) {
        //TODO these operations should be done in a blocking thread, else if the user joins online
        //the dbstate will go into an unstable state.
        if (data.success && data.resp == 0) {
            //logger.log("Informing others that userId " + userId + " went offline");
            //informOthers(userId, "INFORM_OTHERS_ABOUT_ONLINE_STATUS", "status", status);
            //logger.log("removing the rooms of this user " + userId);
            //dao.removeAllRoomsForUser(userId);

            //var currentStatus=
            logger.log("setting expiry for status");
            dao.setExpiryStatusTypeForUser(userId);
            //telling es client to update the status
            esclientInstance.updateStatusInES(status, userId);
            //mongo.saveStatusTypeChangeItem(userId, deviceId, deviceType, {type: "STATUS_TYPE", value: "OFFLINE"});

            
        }
    });


    //tell the userIds he is currently listening to, to remove his reference as he will no longer listen to them
    // if (userIdsToListenTo && userIdsToListenTo.length > 0) {
    //     var l = userIdsToListenTo.length;
    //     for (var k = 0; k < l; k++) {
    //         logger.log("UserId " + userId + " is offline, so he will no longer listen to anybody, removing mappings");
    //         dao.removeUserIdListner(userIdsToListenTo[k], socketId, userId);
    //     }
    // }
};
var allowedStatusTypes = ["OFFLINE", "READY_TO_TEACH"];
exports.setStatusType = function (userId, statusType, deviceId, deviceType) {
    if (allowedStatusTypes.indexOf(statusType) > -1) {
        dao.addStatusTypeForUser(userId, statusType);
    } else {
        dao.removeStatusTypeForUser(userId);
    }
    //informOthers(userId, "INFORM_OTHERS_ABOUT_STATUS_TYPE", "statusType", statusType);
    socketio.tellAllUserSocketsStatusChanged(userId, "MY_STATUS_TYPE_CHANGED", "statusType", statusType);
    //telling es client to update the status
    esclientInstance.updateStatusInES(statusType, userId);
    mongo.saveStatusTypeChangeItem(userId, deviceId, deviceType, {type: "STATUS_TYPE", value: statusType, details: {action: "MANUAL"}});
};
// exports.setStatusMessage = function (userId, statusMessage) {
//     mongo.setStatusMessage(userId, statusMessage);
//     //informOthers(userId, "INFORM_OTHERS_ABOUT_STATUS_MESSAGE", "statusMessage", statusMessage);
//     socketio.tellAllUserSocketsStatusChanged(userId, "MY_STATUS_MESSAGE_CHANGED", "statusMessage", statusMessage);
// };

exports.getOnlineStatusOfUsers = function (userIds) {
    var deferred = Q.defer();
    var onlineUsers = [], count = 0;
    for (var k = 0, l = userIds.length; k < l; k++) {
        //TODO corner case if a teacher joins and update listner happens after that
        var userId = userIds[k];
        (function (_userId) {
            dao.getSocketIdsCountForUser(_userId).then(function (data) {
                if (data.success && data.resp > 0) {
                    onlineUsers.push(_userId);
                }
                count++;
                if (count === l) {//all userIds' socketIds evaluated
                    deferred.resolve(onlineUsers);
                }
            });
        })(userId);
    }
    return deferred.promise;
};
