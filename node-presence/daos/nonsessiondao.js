/* global exports, require, process */

var redis = require('redis');
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var Q = require("q");


var redisOptions = configProps.redis;
var redisClient = redis.createClient(redisOptions.port, redisOptions.host, {
    detect_buffers: true
});



//redisClient.select(0, function(err, reply) {
//    logger.warn("selected db 0, reply: " + reply);
//});



var getKeys = {
    USER_SOCKETIDS_NON_SESSION: function (userId) {
        return "SOCKETIDS_NON_SESSION_" + userId;
    },
    // LISTNERS_FOR_STATUS_NON_SESSION: function (userId) {
    //     return "LISTNERS_NON_SESSION_" + userId;
    // },
    USER_STATUS_TYPE: function (userId) {
        return "USER_STATUS_TYPE_" + userId;
    }
};
// var getVals = {
//     LISTNERS_FOR_STATUS_NON_SESSION: function (listenerSocketId, listenerUserId) {
//         return listenerSocketId + ";;" + listenerUserId;
//     }
// };
var processResp = function (err, reply, deferred, logContext) {
    if (!err) {
        deferred.resolve({success: true, resp: reply});
        logger.log("Success: " + logContext + ", Reply: " + reply);
    } else {
        var resp = {errorCode: err, errorMessage: err.message, success: false};
        deferred.resolve(resp);
        logger.error("Error:- " + logContext + ": " + JSON.stringify(resp));
    }
};
function printStats(key, userId) {
    return;
    if (process.env["DEPLOYMENT_MODE"] === "local") {
        redisClient.scard(key, function (err, reply) {
            logger.log("Total Number of sockets for userId " + userId + " is " + reply);
        });
    }
}

///////////////////////////////////
//      userid vs socketids
///////////////////////////////////
exports.addSocketIdForUser = function (socketId, userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_SOCKETIDS_NON_SESSION(userId);
    redisClient.sadd([key, socketId], function (err, reply) {
        processResp(err, reply, deferred, "Add socket for userId " + userId);
        printStats(key, userId);
    });
    return deferred.promise;
};
exports.removeSocketIdForUser = function (socketId, userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_SOCKETIDS_NON_SESSION(userId);
    redisClient.srem([key, socketId], function (err, reply) {
        processResp(err, reply, deferred, "Remove socket for userId " + userId);
        printStats(key, userId);
    });
    return deferred.promise;
};
exports.getSocketIdsForUser = function (userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_SOCKETIDS_NON_SESSION(userId);
    redisClient.smembers(key, function (err, reply) {
        processResp(err, reply, deferred, "Get socketIds for userId " + userId);
    });
    return deferred.promise;
};
exports.getSocketIdsCountForUser = function (userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_SOCKETIDS_NON_SESSION(userId);
    redisClient.scard(key, function (err, reply) {
        processResp(err, reply, deferred, "Get socketIds count for userId " + userId);
    });
    return deferred.promise;
};


///////////////////////////////////
//      teacherid vs socketid_userid
///////////////////////////////////
// exports.addUserIdListner = function (listenToUserId, listenerSocketId, listenerUserId) {
//     var deferred = Q.defer();
//     var key = getKeys.LISTNERS_FOR_STATUS_NON_SESSION(listenToUserId);
//     var val = getVals.LISTNERS_FOR_STATUS_NON_SESSION(listenerSocketId, listenerUserId);
//     redisClient.sadd(key, val, function (err, reply) {
//         processResp(err, reply, deferred, "Add userId " + listenerUserId + " to listeners, so he can listen to  " + listenToUserId);
//     });
//     return deferred.promise;
// };
// exports.removeUserIdListner = function (listenToUserId, listenerSocketId, listenerUserId) {
//     var deferred = Q.defer();
//     var key = getKeys.LISTNERS_FOR_STATUS_NON_SESSION(listenToUserId);
//     var val = getVals.LISTNERS_FOR_STATUS_NON_SESSION(listenerSocketId, listenerUserId);
//     redisClient.srem(key, val, function (err, reply) {
//         processResp(err, reply, deferred, "Remove userId " + listenerUserId + " from listeners of " + listenToUserId);
//     });
//     return deferred.promise;
// };
// exports.getUserIdListners = function (listenToUserId) {
//     var deferred = Q.defer();
//     var key = getKeys.LISTNERS_FOR_STATUS_NON_SESSION(listenToUserId);
//     redisClient.smembers(key, function (err, reply) {
//         processResp(err, reply, deferred, "Get userIds who are listening to " + listenToUserId);
//     });
//     return deferred.promise;
// };

//////////////////////////////////////////////////////////////////////
//      userid vs status type <OFFLINE/READY_TO_TEACH>
//////////////////////////////////////////////////////////////////////
exports.addStatusTypeForUser = function (userId, statusType) {
    var deferred = Q.defer();
    var key = getKeys.USER_STATUS_TYPE(userId);
    redisClient.set([key, statusType], function (err, reply) {
        processResp(err, reply, deferred, "set status type for userId " + userId + " to " + statusType);
    });
    return deferred.promise;
};
exports.getStatusTypeForUser = function (userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_STATUS_TYPE(userId);
    redisClient.get(key, function (err, reply) {
        processResp(err, reply, deferred, "get status type for userId " + userId);
    });
    return deferred.promise;
};
exports.getStatusTypeForUsers = function (userIds) {
    userIds = userIds || [];
    if (userIds.length > 0) {
        var deferred = Q.defer();
        var keys = [];
        for (var k = 0; k < userIds.length; k++) {
            keys.push(getKeys.USER_STATUS_TYPE(userIds[k]));
        }
        redisClient.mget(keys, function (err, reply) {
            processResp(err, reply, deferred, "get status type for userIds " + userIds);
        });
        return deferred.promise;
    }
};
exports.removeStatusTypeForUser = function (userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_STATUS_TYPE(userId);
    redisClient.del(key, function (err, reply) {
        processResp(err, reply, deferred, "Removed status type for " + userId);
    });
    return deferred.promise;
};
exports.setExpiryStatusTypeForUser = function (userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_STATUS_TYPE(userId);
    redisClient.expire([key, 60], function (err, reply) {
        processResp(err, reply, deferred, "Expire set 60 seconds status type for " + userId);
    });
    return deferred.promise;
};
exports.removeExpiryStatusTypeForUser = function (userId) {
    var deferred = Q.defer();
    var key = getKeys.USER_STATUS_TYPE(userId);
    redisClient.persist(key, function (err, reply) {
        processResp(err, reply, deferred, "Removing Expiry if set for status type for " + userId);
    });
    return deferred.promise;
};


///////////////////////////////////
//      userid vs rooms
///////////////////////////////////
// exports.addRoomForUser = function (userId, roomId) {
//     var deferred = Q.defer();
//     var key = getKeys.USER_ROOMS_NON_SESSION(userId);
//     redisClient.sadd(key, roomId, function (err, reply) {
//         processResp(err, reply, deferred, "Added room " + roomId + " for " + userId);
//     });
//     return deferred.promise;
// };
// exports.removeAllRoomsForUser = function (userId) {
//     var deferred = Q.defer();
//     var key = getKeys.USER_ROOMS_NON_SESSION(userId);
//     redisClient.del(key, function (err, reply) {
//         processResp(err, reply, deferred, "Removed all rooms of " + userId);
//     });
//     return deferred.promise;
// };
// exports.getRoomsForUser = function (userId) {
//     var deferred = Q.defer();
//     var key = getKeys.USER_ROOMS_NON_SESSION(userId);
//     redisClient.smembers(key, function (err, reply) {
//         processResp(err, reply, deferred, "Rooms of " + userId);
//     });
//     return deferred.promise;
// };

////////////////////////////////////////////
//      session/room vs managerId
///////////////////////////////////////////
// exports.getSetManagerIdOfSession = function (sessionId, managerId) {
//     var deferred = Q.defer();
//     var key = getKeys.SESSIONID_VS_MSMANAGERID(sessionId);
//     var luaScript = "if redis.call('GET', KEYS[1]) then \
//                     return redis.call('GET', KEYS[1]) \
//                     else \
//                      redis.call('SET', KEYS[1],ARGV[1])\
//                      return ARGV[1]\
//     end \
//     ";
//     //reference http://www.lua.org/pil/1.html
//     //https://www.npmjs.com/package/redis-evalsha
//     redisClient.eval([luaScript, 1, key, managerId], function (err, reply) {
//         processResp(err, reply, deferred, "get/set managerId of session " + sessionId);
//     });
//     return deferred.promise;
// };
// exports.getManagerIdOfSession = function (sessionId) {
//     var deferred = Q.defer();
//     var key = getKeys.SESSIONID_VS_MSMANAGERID(sessionId);
//     redisClient.get(key, function (err, reply) {
//         processResp(err, reply, deferred, "get managerId of session " + sessionId);
//     });
//     return deferred.promise;
// };
// exports.removeManagerIdOfSession = function (sessionId) {
//     var deferred = Q.defer();
//     var key = getKeys.SESSIONID_VS_MSMANAGERID(sessionId);
//     redisClient.del(key, function (err, reply) {
//         processResp(err, reply, deferred, "removed managerId of session " + sessionId);
//     });
//     return deferred.promise;
// };

// //
// exports.addSessionPublisher = function (sessionId, publisherId) {
//     var deferred = Q.defer();
//     var key = getKeys.SESSIONID_VS_PUBLISHERIDS(sessionId);
//     redisClient.sadd(key, publisherId, function (err, reply) {
//         processResp(err, reply, deferred, "Added publisherId " + publisherId + " for " + sessionId);
//     });
//     return deferred.promise;
// };
// exports.getSessionPublishers = function (sessionId) {
//     var deferred = Q.defer();
//     var key = getKeys.SESSIONID_VS_PUBLISHERIDS(sessionId);
//     redisClient.smembers(key, function (err, reply) {
//         processResp(err, reply, deferred, "Publishers of " + sessionId);
//     });
//     return deferred.promise;
// };
// exports.removeSessionPublisher = function (sessionId, publisherId) {
//     var deferred = Q.defer();
//     var key = getKeys.SESSIONID_VS_PUBLISHERIDS(sessionId);
//     redisClient.srem(key, publisherId, function (err, reply) {
//         processResp(err, reply, deferred, "Removed publisher  " + publisherId + " from " + sessionId);
//     });
//     return deferred.promise;
// };
// exports.removeSessionPublishers = function (sessionId) {
//     var deferred = Q.defer();
//     var key = getKeys.SESSIONID_VS_PUBLISHERIDS(sessionId);
//     redisClient.del(key, function (err, reply) {
//         processResp(err, reply, deferred, "Removed all publishers of " + sessionId);
//     });
//     return deferred.promise;
// };