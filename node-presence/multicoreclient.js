/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var io = require("socket.io-client");
var logger = require("./utils/logger.js");
var server = "https://rtc.vedantu.com";
server = "http://127.0.0.1:20001";
var socketsStore = [];
var maxSessionsToCreateAtATime = 25;
var periodForWhichAConcurrencyRuns = 5 * 60;//seconds
var periodBetweenEachConcurrencyTest = 5;//seconds
var messagesPerSecond = 7;
var messageSendInterval = parseInt(1000 / messagesPerSecond); //gap between sending each message
var payload = 300;
var payloadStr = "";
for (var k = 0; k < payload; k++) {
    payloadStr += "a";
}
var loadTestResults = [];
var storeRoundTripTimes = [];
var testRunning = true;
var runTest = function() {
    if (!testRunning) {
        logger.info("testrunning is false, so stopping testing");
        return;
    }
    var totalMessagesPerSecond = messagesPerSecond * currentConcurrency;


    var newSessionsToBeMade = currentConcurrency - oldConcurrency;

    var sessionsToCreateAtATime = maxSessionsToCreateAtATime;
    if (newSessionsToBeMade < maxSessionsToCreateAtATime) {
        sessionsToCreateAtATime = newSessionsToBeMade;
    }


    logger.log("currentConcurrency:" + currentConcurrency);
    logger.log("newSessionsToBeMade:" + newSessionsToBeMade);

    var sessionConnectionsMade = oldConcurrency;
    var timerForConnections = setInterval(function() {


        if (sessionConnectionsMade >= currentConcurrency || !testRunning) {
            if (timerForConnections) {
                clearInterval(timerForConnections);
            }
            return;
        }
        var initTimeForSocketCreation = new Date().getTime();
        for (i = 0; i < sessionsToCreateAtATime; i++) {
            var _room = "room_" + (sessionConnectionsMade + i + 1);
            var _socketPub = io(server, {'force new connection': true, query: {type: "PUB", room: _room}});
            var _socketSub = io(server, {'force new connection': true, query: {type: "SUB", room: _room}});

            socketsStore.push(_socketPub);
            socketsStore.push(_socketSub);
            logger.log("created sockets for " + _room);

            var messageSentCount = 0;
            (function(socketPub, socketSub, room, initTime) {
                var messageSendIntervalTimer = setInterval(function() {
                    if (!testRunning) {
                        if (messageSendIntervalTimer) {
                            clearInterval(messageSendIntervalTimer);
                        }
                        return;
                    }
                    var gradualMessageSendIntervalTimer = setInterval(function() {
                        if (messageSentCount >= messagesPerSecond || !testRunning) {
                            if (gradualMessageSendIntervalTimer) {
                                clearInterval(gradualMessageSendIntervalTimer);
                                messageSentCount = 0;
                            }
                            return;
                        }
                        socketPub.emit("ping", {context: "wb", type: "VIDEO",
                            sessionId: 23456789, userId: 23534543,
                            data: payloadStr, time: new Date().getTime(), socketId: "sfdjdgskfhhhhhhh"});
                        messageSentCount++;
                    }, messageSendInterval);
                }, 1000);


                //socket events
                socketSub.on('pong', function(data) {
                    storeRoundTripTimes.push((new Date().getTime() - data.time));
                });

                socketPub.on("DISCONNECT_EVERYBODY", function(data) {
                    logger.info("Stopping the test, reason: " + data.reason);
                    var l = storeRoundTripTimes.length, sum = 0;
                    for (var p = 0; p < l; p++) {
                        sum += storeRoundTripTimes[p];
                    }
                    updateResultsForConcurrency(parseInt(sum / l));
                    testRunning = false;
                    setTimeout(function() {
                        for (var u = 0; u < socketsStore.length; u++) {
                            socketsStore[u].disconnect();
                        }
                    }, periodBetweenEachConcurrencyTest);
                });

                socketPub.on("connect", function(number) {
                    logger.warn("connection is made for pub socket of "
                            + room + ", transport: "
                            + socketPub.io.engine.transport.name
                            + ", time taken: " + (new Date().getTime() - initTime) + " millis");
                    setTimeout(function() {
                        logger.warn("the transport for pub of " + room + " is " + socketPub.io.engine.transport.name);
                    }, 3000);
                });
                socketSub.on("connect", function(number) {
                    logger.warn("connection is made for sub socket of "
                            + room + ", transport: "
                            + socketPub.io.engine.transport.name
                            + ", time taken: " + (new Date().getTime() - initTime) + " millis");
                    setTimeout(function() {
                        logger.warn("the transport for sub of " + room + " is " + socketSub.io.engine.transport.name);
                    }, 3000);
                });

                socketPub.on("reconnect", function(number) {
                    logger.warn("Reconnection is made for pub socket of " + room);
                });
                socketSub.on("reconnect", function(number) {
                    logger.warn("Reconnection is made for sub socket of " + room);
                });

                socketPub.on("error", function(err) {
                    logger.error("Error in connecting pub socket of " + room + ", error:" + err);
                });
                socketSub.on("error", function(err) {
                    logger.error("Error in connecting sub socket of " + room + ", error:" + err);
                });

                socketPub.on("disconnect", function(reason) {
                    logger.error("Disconnect in connecting pub socket of " + room + ", reason:" + reason);
                });
                socketSub.on("disconnect", function(reason) {
                    logger.error("Disconnect in connecting sub socket of " + room + ", reason:" + reason);
                });


            })(_socketPub, _socketSub, _room, initTimeForSocketCreation);
        }
        sessionConnectionsMade += sessionsToCreateAtATime;
    }, 1000);

    var timeCounterInSecs = 0;
    var mainIntervalTimer = setInterval(function() {
        timeCounterInSecs++;
        var l = storeRoundTripTimes.length, sum = 0;
        for (var p = 0; p < l; p++) {
            sum += storeRoundTripTimes[p];
        }
        //logger.log("Messages Per Sec: " + totalMessagesPerSecond + ",Time : " + (sum / l).toFixed(0));
        storeRoundTripTimes = [];
        var socketToUse = getConnectedSocket();
        if (socketToUse) {
            socketToUse.emit("CURRENT_RTT", {rtt: parseInt(sum / l)});
        }

        if (timeCounterInSecs >= periodForWhichAConcurrencyRuns) {
            if (mainIntervalTimer) {
                clearInterval(mainIntervalTimer);
            }
            updateResultsForConcurrency(parseInt(sum / l));
            runLoadTest();
        }
    }, 1000);
};

var concurrencyLevels = [10];//, 25, 50, 60, 70, 80, 90, 100, 120, 140, 160, 180, 200
var currentConcurrency = 0;
var oldConcurrency = 0;
function runLoadTest() {
    if (concurrencyLevels.length > 0) {
        //var testSocket = io('http://127.0.0.1:9000', {'force new connection': true, query: {type: "PUB", room: _room}});
        setTimeout(function() {
            oldConcurrency = currentConcurrency;
            currentConcurrency = concurrencyLevels.shift();
            logger.info("Running test for  concurrency :" + currentConcurrency);
            runTest();
        }, periodBetweenEachConcurrencyTest);
    } else {
        logger.info("Completed testing ");
    }
}
var createClient = function() {
    var multicoreclient = io(server, {'force new connection': true, query: {type: "multicoreclient", room: "multicoreclient"}});
    multicoreclient.on("connect", function(number) {
        logger.log("Multicore client connected");
    });
    multicoreclient.on("START_TESTING", function(number) {
        logger.log("got request for start testing");
        runLoadTest();
    });
};
createClient();

function getConnectedSocket() {
    var l = socketsStore.length;
    var s;
    for (var k = 0; k < l; k++) {
        if (socketsStore[k].connected) {
            s = socketsStore[k];
            break;
        }
    }
    return s;
}
function updateResultsForConcurrency(rtt) {
    var socketToUse = getConnectedSocket();
    if (socketToUse) {
        socketToUse.emit("STOP_TESTING", {roundTripTime: rtt, concurrency: currentConcurrency});
    }
}
