var fs = require('fs'), dateFormat = require('dateformat'), tinytim = require('tinytim');
var logProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps(process.env["DEPLOYMENT_MODE"]).logs;
var logFilePath = logProps.filePath;


var _conf = {
    root: logFilePath,
    logPathFormat: '{{root}}/{{prefix}}.log',
    splitFormat: 'yyyymmdd'
};

function LogFile(prefix, date) {
    this.date = date;
    this.prefix = prefix;
    this.path = tinytim.tim(_conf.logPathFormat, {root: _conf.root, prefix: prefix, date: date});
    this.stream = fs.createWriteStream(this.path, {
        flags: "a",
        encoding: "utf8",
        mode: 0666
    });
}

LogFile.prototype.write = function (str) {
    this.stream.write(str + "\n");
};

LogFile.prototype.destroy = function () {
    if (this.stream) {
        this.stream.end();
        this.stream.destroySoon();
        fs.renameSync(logFilePath+'/presenceapp.log', logFilePath+'/presenceapp.'+this.date+'.log', function (err) {
        if (err) throw err;
            console.log('renamed complete');
        });
        this.stream = null;
    }
};

var _logMap = {};

function _push2File(str, title) {
    title = "presenceapp";
    var logFile = _logMap[title], now = dateFormat(new Date(), _conf.splitFormat);
    if (logFile && logFile.date != now) {
        logFile.destroy();
        logFile = null;
    }
    if (!logFile)
        logFile = _logMap[title] = new LogFile(title, now);
    logFile.write(str);
}

var logger;
if (process.env["DEPLOYMENT_MODE"] === "local") {
    logger = require('tracer').colorConsole({
        format: "{{timestamp}} pid:" + process.pid + " <{{title}}> {{message}} (in {{file}}:{{line}})"
    });
} else {
    console.log("//////////////////////////////////////////////////////");
    console.log("Logs can be seen at " + logFilePath + "/presenceapp.<date>.log");
    console.log("//////////////////////////////////////////////////////");
    logger = require('tracer').colorConsole({
        transport: function (data) {
            if (data.title == "error") {
                console.error(data.output);
            }
            _push2File(data.output, data.title);
        },
        format: "{{timestamp}} pid:" + process.pid + " <{{title}}> {{message}} (in {{file}}:{{line}})"
    });
}



setInterval(function () {
    console.log("checking log files");
    try {
        var allowedOldFiles = new Date().getTime() - (86400000 * logProps.maxDaysToKeepFile);
        fs.readdir(logFilePath, function (err, files) {
            if (!err && files) {
                for (var k = 0; k < files.length; k++) {
                    var fileName = files[k];
                    if (fileName.indexOf("presenceapp") === -1) {
                        continue;
                    }
                    var fileDate = fileName.substring(fileName.indexOf(".") + 1, fileName.indexOf(".log"));
                    var yr = fileDate.substring(0, 4);
                    var month = fileDate.substring(4, 6);
                    var date = fileDate.substring(6, 8);
                    var dateInMillis = new Date(month + '/' + date + "/" + yr).getTime();
                    if (dateInMillis < allowedOldFiles) {
                        console.log("The file is old, created on " + new Date(dateInMillis) + ", so deleting it");
                        fs.unlink(logFilePath + "/" + fileName, function (err) {
                            if (err) {
                                console.error("error in deleting file " + logFilePath + "/" + fileName);
                                console.error(err);
                            } else {
                                console.log('successfully deleted ' + logFilePath + "/" + fileName);
                            }
                        });
                    } else {
                        console.log("The file is not so old,it is dated: " + new Date(dateInMillis));
                    }
                }
            } else {
                console.log("some error in reading files for " + logFilePath + ", error: " + err);
            }
        });
    } catch (e) {
        console.error("error in checking files for deletion");
        console.error(e);
    }
}, (logProps.intervalToCheckFiles * 60 * 60 * 1000));
module.exports = logger;
