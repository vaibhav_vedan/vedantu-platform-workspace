Prerequisites:

* Install node.js
	sudo apt-get update
	sudo apt-get install -y python-software-properties g++ make
	sudo add-apt-repository ppa:chris-lea/node.js
	sudo apt-get update
	sudo apt-get install nodejs

* Install npm 
	above steps should have installed npm
* You need to run vedantu fruitable online study application before this application
  Provide configuration in config.js

How to run:

1) Run "sudo npm install" to install all dependencies for node project
2) Update  config.js if needed for configurations specific to deployment modes
3) Run 
   node app.js -m [deployment.mode]
   or do 
   node app.js --help for detailed help for command line arguments


 
