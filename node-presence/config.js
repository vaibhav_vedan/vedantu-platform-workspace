const SecretService = require('./utils/secretsService.js');
/* global module */
var getJwtSecretKeyFor = function (mode) {
    var secretKey = "";
    switch (mode) {
        case 'local':
            secretKey = "X08L0G9c5vNppAhwc15QC30WN1zFBvk5";
            break;
        case 'qa':
            secretKey = "S5WgbeU44oMfqVxTLajmN1lE83wFVq7I";
            break;
        case 'dev':
            secretKey = "7x6W8157H32NvFf9364OiT3IJLH6k3Tl";
            break;
        case 'wave':
            secretKey = "BB4BA92E59EC875863A8D4142CC19";
            break;
        case 'qa1':
            secretKey = "oK74oJ8UGrn7YcQRtdYMV6Kd497GSfT6";
            break;
        case 'dev1':
            secretKey = "8C697D58FA68AABACDB39E25A5177";
            break;
        case 'dev2':
            secretKey = "2F62E9E1F423F3D8CA467412635E9";
            break;
        case 'mobile':
            secretKey = "MK2gwE0iSM03yjU9leGUz00iQ4NQR9VS";
            break;

    }
    return secretKey;
};
var getJwtSecretClientsFor = function (mode) {
    var clients = {
        "VEDANTU_BACKEND": {
            id: "1F9B7",
            secret: "6r605RTxER60IYkA89k2340mxxA2FqR6"
        },
        "VEDANTU_FRONT_END": {
            id: "225F7",
            secret: "ErG3ke0MsR8vtd35GyVri1hJea212wdr"
        },
        "NODE": {
            id: "E25D9",
            secret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk"
        },
        "SESSION_METRICS": {
            id: "472FA",
            secret: "CMdqxxzouZlwp3WA1Dt3Aw4HwiliyL3U"
        },
        "MONGO_TO_S3": {
            id: "F2B85",
            secret: "y57H8qp3FUUs8n6eFF3OreeTZA75W4HA"
        },
        "PRESENCE": {
            id: "DC2A1",
            secret: "ka2m7FbC74aQ3me2Tp4wndOna5hsIpW1"
        },
        "PREPARE_REPLAY": {
            id: "2E8B5",
            secret: "WOtFnVpGG7UBn7K1jLzIApN363eL1cTs"
        },
        "ENGAGEMENT_ANALYSIS": {
            id: "C2B8B",
            secret: "qfUD85nZmj749DO8zv66rnrGuVMcQab5"
        },
        "YOUSCORE": {
            id: "EBD25",
            secret: "2Q0CyCwgOFNUEpc1j3FmtMV1Ey71917e"
        },
        "MOODLE": {
            id: "D8563",
            secret: "1V3mHB1dwtPkvWh3HjI2EifMpml3ipX1"
        },
        "LEADSQUARED": {
            id: "DD831",
            secret: "YtMI7B8M2021om5jIb6Su1DNkAIr5v4j"
        },
        "PDF_TO_IMAGE_CONVERTER": {
            id: "2CDFC",
            secret: "wTf9Coqjh581xR51c5qr5ahJF46ZZFJr"
        },
        "TWILIO": {
            id: "2CDFC",
            secret: "wTf9Coqjh581xR51c5qr5ahJF46ZZFJr"
        }
    };

    return clients;
};
var localsettings = {
    server_host: "localhost",
    server_port: 20002,
    allowedOrigins: ["http://localhost:8080"],
    nonsessionModules: [],
    secured: false, // this is responsible for lauching http vs https enabled server
    certificate: {
        // this path is wrt user home
        path: "certificates",
        keyfile: "vedantu.key",
        certfile: "vedantu.com.chained.crt"
    },
    logs: {
        level: "log",
        filePath: "/var/log",
        maxDaysToKeepFile: 7, //days
        intervalToCheckFiles: 5 //hrs
    },
    redis: {
        host: '127.0.0.1',
        port: 6379,
        password: 'PiTihVamtacqeNvfIKCzn8cjzJNsnUisrtzvmGiBsMU4MxcBKwcZ5CQKWPjPkYS1'
    },
    mongo: {
        host: ["localhost"],
        port: 27017,
        dbname: "presencedb"
    },
    es: {
        url: "localhost:9200",
        logLevel: "trace"
    },
    raven: {
        key: "https://a6d982ce5c5949e8ac99117c1e91c5fc:23a3dc21359b4c62a737fc60ce71958f@app.getsentry.com/65239"
    },
    "jwt-auth": {
        secretkey: getJwtSecretKeyFor('local'),
        client: getJwtSecretClientsFor('local'),
        tokenName: "X-Ved-Token"
    },
    "noAuthApis": []
};

var prodsettings = {
    "server_port": 443,
    "allowedOrigins": [
        "https://www.vedantu.com",
        "https://vedantu.com",
        "https://uae.vedantu.com"
    ],
    "nonsessionModules": [],
    "secured": true,
    "certificate": {
        "path": "certs/vedantu.com",
        "keyfile": "vedantu.key",
        "certfile": "vedantu.com.chained.crt"
    },
    "logs": {
        "level": "info",
        "filePath": "/var/log",
        "maxDaysToKeepFile": 30,
        "intervalToCheckFiles": 5
    },
    "es": {
        "url": "search.vedantu.com",
        "logLevel": "info"
    },
    "noAuthApis": [
        "/checkStatus"
    ]
};
var propsMap = {
    local: localsettings,
    prod: prodsettings
};
var propertiesForCurrentMode = {};
var getPropsForQAProfiles = function (mode) {
    var settings = {
        server_host: "localhost",
        server_port: 20002,
        allowedOrigins: ["https://fos-" + mode + ".vedantu.com"],
        secured: false,
        certificate: {
            // this path is wrt user home
            path: "server-artifacts/certs/vedantu.com",
            keyfile: "vedantu.key",
            certfile: "vedantu.com.chained.crt"
        },
        logs: {
            level: "log",
            filePath: "/var/log",
            maxDaysToKeepFile: 7, //days
            intervalToCheckFiles: 5 //hrs
        },
        redis: {
            host: '127.0.0.1',
            port: 6379,
            password: 'PiTihVamtacqeNvfIKCzn8cjzJNsnUisrtzvmGiBsMU4MxcBKwcZ5CQKWPjPkYS1'
        },
        mongo: {
            host: ["127.0.0.1"],
            port: 27017,
            dbname: "vedantuchat"
        },
        es: {
            url: "127.0.0.1:9200",
            logLevel: "info"
        },
        raven: {
            key: "https://a6d982ce5c5949e8ac99117c1e91c5fc:23a3dc21359b4c62a737fc60ce71958f@app.getsentry.com/65239"
        },
        "jwt-auth": {
            secretkey: getJwtSecretKeyFor(mode),
            client: getJwtSecretClientsFor(mode),
            tokenName: "X-Ved-Token"
        },
        "noAuthApis": []
    };
    //add any exceptions here
    return settings;
};
module.exports = {
    modes: ['local', 'qa', 'dev', 'prod', 'wave', "mobile", "qa1", "dev1", "dev2", "dev3", "qa2", "qa3"],
    appVersion: "1.0",
    setConfigPropsForCurrentMode: async function (mode) {
        if (propsMap[mode]) {
            propertiesForCurrentMode = propsMap[mode];
        } else {
            propertiesForCurrentMode = getPropsForQAProfiles(mode);
        }
        if (mode === "prod") {
            let secrets = await SecretService.getSecrets();
            propertiesForCurrentMode = Object.assign(propertiesForCurrentMode, secrets);
        }
    },
    getConfigProps: function (mode) {
        if (!propertiesForCurrentMode.server_port) {
            //doing this, because when accessed from a child process, this may not have been initialized
            module.exports.setConfigPropsForCurrentMode(mode);
        }
        return propertiesForCurrentMode;
    }
};
