var static = require('node-static');
var express = require('express');
var app = express();
var file = new (static.Server)();
var usage = require('usage');
var sticky = require("sticky-session");
var redis = require('redis');
var redisAdapter = require('socket.io-redis');
var http = require("http");
var secured = false;
var port = 20001;
var io;
var enableCluster = false;
var channelName = "DEFAULT";
var pubRedisClient;
var fs = require('fs');
var path = require('path');
process.env["APP_HOME"] = path.dirname(fs.realpathSync(__filename));
var config = require("./config.js");

config.setConfigPropsForCurrentMode(process.argv[3]);
var configProps = config.getConfigProps();


var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var mongo_session = require(process.env["APP_HOME"] + "/modules/mongo-migration.js");


var socketsCount = 0;
var pingsCount = 0;
var dropsCount = 0;
var loadTestResults = [];
var clients = [];
var storeDBPushTimes = [];


if (enableCluster) {
    useCluster();
} else {
    doNotUseCluster();
}



function useCluster() {

    var numCPUs = 2;//require('os').cpus().length
//    if (process.argv[2]) {
//        numCPUs = parseInt(process.argv[2]);
//    }
    logger.info("cpu core count is " + numCPUs);


    sticky(numCPUs, function() {

        var server;
        if (secured) {
            logger.info("No certie, Add them please");
        } else {
            logger.info("Launching http server on " + port + "... ");
            server = http.createServer(app);
        }

        io = require('socket.io')(server, {
            //        "browser client etag": true,
            "pingTimeout": 10000,
            "pingInterval": 15000,
            "origins": "*:*"
        });


        initSocketIO();

        addRedisAdapter();
        showStats();

        return server;

    }).listen(port, function() {

        // this code is executed in both slaves and master
        logger.info('server started on port ' + port);

    });
}

function doNotUseCluster() {
    var server = require("http").createServer(function(req, res) {
        file.serve(req, res);
    }).listen(port);
    io = require("socket.io")(server, {
        //        "browser client etag": true,
        "pingTimeout": 10000,
        "pingInterval": 15000,
        "origins": "*:*"
    });
    initSocketIO();
    addRedisAdapter();
    showStats();
}

function initSocketIO() {
    io.use(function(socket, next) {
        var queryParams = socket.handshake.query;
        if (queryParams.room) {
            socket.room = queryParams.room;
        }
        if (queryParams.type) {
            socket.type = queryParams.type;
        }
        next();
    });
    io.on('connection', function(socket) {
        clients.push(socket);
        socketsCount++;
        socket.join(socket.room);
        logger.log("Socket of room " + socket.room + " and type " + socket.type + " got connected");
        socket.on('disconnect', function(reason) {
//            logger.error("socket got disconnected, socket of room "
//                    + socket.room + ", and type: " + socket.type + ", reason: " + reason);
            socketsCount--;
            dropsCount++;
        });

        socket.on('ping', function(data) {
            pingsCount++;
            socket.broadcast.to(socket.room).emit("pong", data);
            data.serverTime = new Date().getTime();
            var pr = mongo_session.saveSessionDataItem(data);
            var initTime = new Date().getTime();
            pr.then(function() {
                storeDBPushTimes.push((new Date().getTime() - initTime));
            });
        });

        socket.on('STOP_TESTING', function(data) {
            logger.info("Got mesage for stopping the test for concurrency level: " + data.concurrency);
            var l = storeDBPushTimes.length, sum = 0;
            for (var p = 0; p < l; p++) {
                sum += storeDBPushTimes[p];
            }
            storeDBPushTimes = [];
            var avgTime = parseInt(sum / l);

            loadTestResults.push({
                memory: currentMemUsage,
                cpu: currentCPUUsage,
                socketsConnected: socketsCount,
                socketsDropped: dropsCount,
                concurrency: data.concurrency,
                roundTripTime: data.roundTripTime,
                avgTimeToStoreInDB: avgTime,
                pingsCount: pingsCount
            });


            logger.info("\n\n");
            logger.info("Results till now :");
            logger.info(loadTestResults);
            logger.info("\n\n");
//            socket.broadcast.to(socket.room).emit("STOP_TESTING", {
//                memory: currentMemUsage,
//                cpu: currentCPUUsage,
//                socketsConnected: socketsCount,
//                socketsDropped: dropsCount
//            });
            //pubRedisClient.publish(channelName, "hi");
        });

        socket.on("CURRENT_RTT", function(data) {
            currRoundTripTime = data.rtt;
        });
    });
}

var testRunning = true;
var currentMemUsage, currentCPUUsage;
var currRoundTripTime = 0;
var statsTimer;
function showStats() {
    fillUsageStats();
    statsTimer = setInterval(fillUsageStats, 15000);
}
function fillUsageStats() {
    if (!testRunning) {
        if (statsTimer) {
            clearInterval(statsTimer);
        }
        return;
    }
    var l = storeDBPushTimes.length, sum = 0;
    for (var p = 0; p < l; p++) {
        sum += storeDBPushTimes[p];
    }
    storeDBPushTimes = [];
    var avgTime = parseInt(sum / l);

    usage.lookup(process.pid, {keepHistory: true}, function(err, result) {
        if (!err && clients.length > 0) {
            currentMemUsage = computeSize(result.memory);
            currentCPUUsage = result.cpu.toFixed(2);
            logger.info("Sockets: " + socketsCount + ", Mem: "
                    + currentMemUsage + ", CPU: " + currentCPUUsage + "%, Drops: " + dropsCount
                    + ", currRoundTripTime: " + currRoundTripTime
                    + ", Avg DB save time: " + avgTime + ", pings count is " + pingsCount);
         //   pingsCount = 0;
        }
    });

    var reason = "";
    if (avgTime > 3000) {
        logger.warn("Found avg time to save in db is high: " + avgTime + " millis.");
        reason = "avgTime: " + avgTime;
    }
    if (currRoundTripTime > 3000) {
        logger.warn("Current Round Trip time is high: " + currRoundTripTime + " millis.");
        reason = "currRoundTripTime: " + currRoundTripTime;
    }
    if (dropsCount > 50) {
        logger.warn("Drop rate is high: " + dropsCount);
        reason = "dropsCount: " + dropsCount;
    }
    if (reason) {
        sendStopTestingMessage(reason);
        pubRedisClient.publish(channelName, {type: "STOP_TESTING"});
    }
}

var computeSize = function(bytes) {
    var k = 1024;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    if (bytes === 0)
        return '0 Bytes';
    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(k)), 10);
    return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
};

function sendStartTestingMessage() {
    var socket = getConnectedSocket();
    logger.log("got message for START_TESTING");
    if (socket) {
        logger.info("sending message to start the testing..");
        socket.emit("START_TESTING");
    }
}
function sendStopTestingMessage(reason) {
    testRunning = false;
    var socket = getConnectedSocket();
    if (socket) {
        logger.info("sending message to stop the testing..");
        socket.emit("DISCONNECT_EVERYBODY", {reason: reason});
    } else {
        logger.error("No socket for broadcasting message");
    }
}

function addRedisAdapter() {

    var redisOptions = configProps.redis;
    var subRedisClient = redis.createClient(redisOptions.port, redisOptions.host, {
        detect_buffers: true
    });
    subRedisClient.subscribe(channelName);
    subRedisClient.on("subscribe", function(channel) {
        logger.info("subscribed to " + channel);
    });
    subRedisClient.on("message", function(channel, _message) {
        var message = JSON.parse(_message);
        switch (message.type) {
            case "START_TESTING":
                sendStartTestingMessage();
                break;
            case "STOP_TESTING":
                sendStopTestingMessage();
                break;
        }
    });


    pubRedisClient = redis.createClient(redisOptions.port, redisOptions.host, {
        detect_buffers: true
    });

    var pub = redis.createClient(redisOptions.port, redisOptions.host, {
        detect_buffers: true
    });
    var sub = redis.createClient(redisOptions.port, redisOptions.host, {
        detect_buffers: true
    });

    io.adapter(redisAdapter({
        pubClient: pub,
        subClient: sub
    }));

    logger.log('Redis adapter started with url: ' + redisOptions.host + ":" + redisOptions.port);
}

function getConnectedSocket() {
    var l = clients.length;
    var s;
    for (var k = 0; k < l; k++) {
        if (clients[k].connected) {
            s = clients[k];
            break;
        }
    }
    return s;
}


//router
(function(app) {
    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    app.get("/starttesting", function(req, res) {
        console.log(req.header("Connection"));
//        pubRedisClient.publish(channelName, {type: "START_TESTING"});
//        sendStartTestingMessage();
        res.send("Started, now see console for progress...");
    });
})(app);