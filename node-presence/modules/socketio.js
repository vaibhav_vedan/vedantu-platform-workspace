/* global exports, process */

var io;
var nonsessionManager = require(process.env["APP_HOME"] + "/managers/nonsessionManager.js");
var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var utils = require(process.env["APP_HOME"] + "/utils/utils.js");
var channelName = "INTER_NODE_PROCESSES_CHANNEL";
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var redis = require("redis");
var dao = require(process.env["APP_HOME"] + "/daos/nonsessiondao.js");
var mongo = require(process.env["APP_HOME"] + "/modules/mongo.js");
var authenticator = require(process.env["APP_HOME"] + "/modules/authenticator.js");
var Q = require('q');


var redisOptions = configProps.redis;
var subRedisClient = redis.createClient(redisOptions.port, redisOptions.host, {
    detect_buffers: true
});
var pubRedisClient = redis.createClient(redisOptions.port, redisOptions.host, {
    detect_buffers: true
});

subRedisClient.subscribe(channelName);
subRedisClient.on("subscribe", function (channel) {
    logger.info("subscribed to " + channel);
});

subRedisClient.on("message", function (channel, _message) {
    logger.info("got message on " + channel + ", message is " + _message);
    try {
        var message = JSON.parse(_message);
        switch (message.type) {
            case "tellAllUserSocketsStatusChanged":
                message.emitTo = "STATUS_TYPE_CHANGED";
                emitMyStatusChanged(message);
                break;
            case "MY_STATUS_TYPE_CHANGED":
                message.emitTo = "STATUS_TYPE_CHANGED";
                emitMyStatusChanged(message);
                break;
            default :
                logger.info("did not understand what subclient sent");
                break;
        }
    } catch (err) {
        logger.error("Error in deserializing message, " + err);
    }
});

var emitMyStatusChanged = function (message) {
    var userId = message.userId;
    dao.getSocketIdsForUser(userId).then(function (data) {
        var resp = {userId: userId};
        resp[message.statusKey] = message.statusVal;
        if (data.success && data.resp.length > 0) {
            var interestedSocketIds = data.resp, l = interestedSocketIds.length;
            logger.info("Informing about status change: userId " + userId + " has " + l + " connections");
            for (var k = 0; k < l; k++) {
                var socket = io.sockets.connected[interestedSocketIds[k]];
                if (socket) {
                    logger.info(message.emitTo);
                    logger.info(resp);
                    socket.emit(message.emitTo, resp);
                }
            }
        }
    });
};

function addIOEventHandlers(_io) {
    io = _io;
    io.use(function (socket, next) {
        authenticator.authenticateSocket(socket,next);
    });
//    io.use(function (socket, next) {
//        var queryParams = socket.handshake.query;
//
//        logger.info("Query params " + JSON.stringify(queryParams));
//
//        if (queryParams.nonSession) {
//            socket.nonSession = true;
//        }
//        if (queryParams.userId) {
//            socket.userId = queryParams.userId;
//        }
//        if (queryParams.sessionId) {//will be used in case of sessions
//            socket.sessionId = queryParams.sessionId;
//            socket.selfPingTimer = null;
//        }
//        logger.debug("Remote socket ip is " + socket.request.connection.remoteAddress);
//        if (queryParams.deviceId) {
//            logger.debug("device id is " + queryParams.deviceId);
//            socket.deviceId = queryParams.deviceId;
//            //since deviceId is only coming from Mobile devices, setting the device type to MOBILE
//            socket.deviceType = "MOBILE";
//        }
//
//        next();
//    });
    io.sockets.on('connection', function (socket) {
        logger.info("New Connection Created with id === " + socket.id + " and userId " + socket.userId);
        socket.emit("connected", "" + socket.id);
        printSocketStats();


        if (socket.nonSession) {
            if (!socket.userId) {
                logger.warn("socket connection received without userid for non session, so ignoring it");
                //the above socket will still recieve messages but cannot emit them.
                return;
            }
            nonsessionManager.onConnected(socket.id, socket.userId, socket.deviceId, socket.deviceType);


            //out of session related event handling for chat and offline messages etc
            
            
            socket.on("SET_STATUS_TYPE", function (message) {
                var statusType = message.statusType;
                if (message && typeof statusType === "string") {
                    nonsessionManager.setStatusType(socket.userId, statusType, socket.deviceId, socket.deviceType);
                }
            });
            // socket.on("SET_STATUS_MESSAGE", function (message) {
            //     var statusMessage = message.statusMessage;
            //     if (message && typeof statusMessage === "string") {
            //         nonsessionManager.setStatusMessage(socket.userId, statusMessage);
            //     }
            // });
            socket.on("GET_MY_STATUS_TYPE", function (message) {
                var userIds = [socket.userId];
                var statusTypePromise = dao.getStatusTypeForUser(socket.userId);
                //var statusPromise = mongo.getStatus(userIds);
                var queue = Q.all([statusTypePromise]);
                queue.then(function (resultsArr, errArr) {
                    //var docs = resultsArr[1] || [];
                    //message.status = docs[0] || {};
                    var statusType = resultsArr[0].resp;
                    if (!statusType || statusType == "nil") {
                        statusType = "NONE";
                    }
                    message.status = {};
                    message.status.statusType = statusType;
                    message.status.statusMessage = "";

                    //message.status.onlineStatus = "ONLINE"; //just putting to adhere to response structure
                    socket.emit('MY_STATUS_TYPE', message.status);
                });
            });
        } 

        //disconnect global event
        socket.on('disconnect', function (reason) {
            logger.info("socket-" + socket.id + " disconnected of " + socket.userId + ", Reason: " + reason);
            if (socket.nonSession) {//this is socket is out of session for chat related
                nonsessionManager.onDisconnected(socket.id, socket.userId, socket.userIdsToListenTo, socket.deviceId, socket.deviceType);
            } else {
                if (socket.userId && socket.sessionId) {
                    if (socket.selfPingTimer) {
                        clearInterval(socket.selfPingTimer);
                    }
                    ;
                    var message = {userId: socket.userId, sessionId: socket.sessionId, socketId: socket.id};
                }
            }
            printSocketStats();
        });
    });
}

function tellAllUserSocketsStatusChanged(userId, type, statusKey, statusVal) {
    pubRedisClient.publish(channelName, JSON.stringify({
        userId: userId,
        statusKey: statusKey,
        statusVal: statusVal,
        type: type
    }));
}



function printSocketStats() {
    //TODO do we neet the result of closed and handshaken ,are they really available in 1.3.5 version
    logger.info("======================");
    logger.info("Number of active connections " + (Object.keys(io.sockets.connected).length) + " process pid " + process.pid);
    logger.info("======================");
}

function getSocketobjInThisServer(socketId) {
    return io.sockets.connected[socketId];
}


exports.getSocketobjInThisServer = getSocketobjInThisServer;
exports.addIOEventHandlers = addIOEventHandlers;
exports.tellAllUserSocketsStatusChanged = tellAllUserSocketsStatusChanged;
exports.emitMyStatusChanged = emitMyStatusChanged;