var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var elasticsearch = require('elasticsearch');


var esProps = configProps.es;
var esclient;
try {
    esclient = new elasticsearch.Client({
        host: esProps.url,
        log: esProps.logLevel
    });
} catch (e) {
    logger.error("Error in making esclient " + e);
}

function updateStatusInES(status, userId) {
    if (status && esclient) {
        var statusNumToSend = -2;
        if (status === "ONLINE" || status === "NONE") {
            statusNumToSend = 0;
        } else if (status === "OFFLINE") {
            statusNumToSend = -1;
        } else if (status === "READY_TO_TEACH") {
            statusNumToSend = 1;
        }
        if (statusNumToSend > -2) {
            logger.info("updating the status of " + userId + " to " + status + " in ES");
            try {
                esclient.update({
                    index: 'vedantu',
                    type: 'teachers',
                    id: userId,
                    body: {
                        // put the partial document under the `doc` key
                        doc: {
                            onlineStatus: statusNumToSend
                        }
                    }
                }, function (error, response) {
                    if (error) {
                        logger.error("Error in updating the status in ES, error " + error);
                    }
                });
            } catch (e) {
                logger.error("Catching the error in updating the status " + e);
            }
        } else {
            logger.error("Status not applicable " + status);
        }
    } else if (!esclient) {
        logger.error("ES Client not found");
    } else if (!status) {
        logger.error("status not given " + status);
    }
}

// Functions which will be available to external callers
exports.updateStatusInES = updateStatusInES;
