/* global exports, process, require */

var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var bodyParser = require('body-parser');
//var nonsessionManager = require(process.env["APP_HOME"] + "/managers/nonsessionManager.js");
var authenticator = require(process.env["APP_HOME"] + "/modules/authenticator.js");
var dao = require(process.env["APP_HOME"] + "/daos/nonsessiondao.js");
var Q = require('q');
var responseUtils = require(process.env["APP_HOME"] + "/utils/responseUtils.js");
var cookieParser = require('cookie-parser');

function route(app) {
    app.use(bodyParser.json({
        limit: 1024 * 1024 * 1024
    }));

    app.use(bodyParser.urlencoded({
        extended: true,
        parameterLimit: 10000000,
        limit: 1024 * 1024 * 1024
    }));
    app.use(cookieParser());
    app.use(function (req, res, next) {
        authenticator.authenticateRequest(req, res, next);
//        var origin = req.header("Origin");
//        logger.info("origin of request "+origin);
//        if(allowedOrigins.indexOf(origin)>-1){
//            res.header("Access-Control-Allow-Origin", origin);
//            res.header("Access-Control-Allow-Credentials", true);
//        }
//        next();
    });

    app.get("/getStatusTypes", function (req, res) {
        var query = req.query, userId = query.userId, userIds = query.userIds;
        if (userId && userIds && userIds.length > 0) {
            logger.log("Got request for statusTypes of   " + userId + " and userIds " + JSON.stringify(userIds));
            var statusTypePromise = dao.getStatusTypeForUsers(userIds);
            var queuedPromise = Q.all([statusTypePromise]);
            queuedPromise.then(function (resultsArr, errsArr) {
                if (resultsArr) {
                    var statusTypes = [];
                    if (resultsArr[0].success) {
                        statusTypes = resultsArr[0].resp || [];
                    }
                    var statuses = {};
                    for (var m = 0; m < userIds.length; m++) {
                        var uid = userIds[m];
                        statuses[uid] = {userId: uid};
                        var statusType = statusTypes[m];
                        if (!statusType || statusType == "nil") {
                            statusType = "NONE";
                        }
                        statuses[uid].statusType = statusType;
                    }
                    res.send(responseUtils.getSuccessRespObj(statuses));
                } else {
                    res.send(responseUtils.getErrRespObj());
                }
            });
        } else {
            res.send(responseUtils.getErrRespObj("BAD_REQUEST"));
        }
    });
    app.get("/checkStatus", function(req, res) {
        logger.log("Got req for checkStatus");
        res.send(responseUtils.getSuccessRespObj(true));
    });
}
exports.route = route;