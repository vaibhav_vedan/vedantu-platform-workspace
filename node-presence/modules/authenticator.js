var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var responseUtils = require(process.env["APP_HOME"] + "/utils/responseUtils.js");
var jwtProps = configProps['jwt-auth'];
var noAuthApis = configProps['noAuthApis'];
var allowedOrigins = configProps['allowedOrigins'];
var jwtSecertKey = new Buffer(jwtProps.secretkey, 'base64');
var jwtClients = jwtProps.client;
var jwt = require('jsonwebtoken');
var cookie = require('cookie');
var tokenName = jwtProps.tokenName;
var ERROR_CODES = {
    "FORBIDDEN_ERROR": "FORBIDDEN_ERROR",
    "NOT_LOGGED_IN": "NOT_LOGGED_IN"
};
var USER_ROLES = {
    admin: "ADMIN",
    all: "ALL",
    parent: "PARENT",
    seoEditor: "SEO_EDITOR",
    seoManager: "SEO_MANAGER",
    student: "STUDENT",
    studentCare: "STUDENT_CARE",
    teacher: "TEACHER"
};
var authenticateRequest = function (req, res, next) {
    logger.log('authenticating request for api  ', req._parsedUrl.pathname);
    var origin = req.header("Origin");
    //        logger.info("origin of request "+origin);
    if (allowedOrigins.indexOf(origin) > -1) {
        res.header("Access-Control-Allow-Origin", origin);
        res.header("Access-Control-Allow-Credentials", true);
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
        res.header('Access-Control-Allow-Headers', req.header("Access-Control-Request-Headers"));
    }
    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
        res.sendStatus(204);
        return;
    }
    var appProps = {
        client: req.header("X-Ved-Client"),
        clientId: req.header("X-Ved-Client-Id"),
        clientSecert: req.header("X-Ved-Client-Secret")
    };
    if (noAuthApis.indexOf(req._parsedUrl.pathname) > -1) {
        logger.log('It is a no auth Api');
        next();
        return;
    }
    if (isAllowedApp(appProps)) {
        req.isAppRequest = true;
        logger.log('got request from a app', appProps);
        next();
    } else {
        var token = (req.cookies[tokenName]) || (req.header(tokenName)) || (req.header("x-ved-token"));
        if (!token) {
            logger.log('no cookies available in the request', req.cookies);
            return res.status(403).send(responseUtils.getErrRespObj(ERROR_CODES.NOT_LOGGED_IN));
        } else {
            logger.log('lets check that this is a valid cookie', token);
            jwt.verify(token, jwtSecertKey, function (err, decoded) {
                if (err) {
                    logger.log('cookies not valid in the request', err, decoded);
                    return res.status(403).send(responseUtils.getErrRespObj(ERROR_CODES.NOT_LOGGED_IN));
                } else {
                    var sessionData = JSON.parse(decoded.sessionData);
                    req.sessionData = sessionData;
//                  logger.log('decoded session data>>>>>>>>...', decoded, new Date(sessionData.refreshedTime));
                    next();
                }
            });
        }
    }
};

var authenticateSocket = function (socket, next) {
    logger.log('authenticating Socket');
    var queryParams = socket.handshake.query;

    logger.info("Query params " + JSON.stringify(queryParams));
    if (!socket.handshake.headers.cookie && !socket.handshake.headers[tokenName] && !socket.handshake.headers["x-ved-token"]) {
        logger.log('cookies not valid in the request', socket.handshake.headers);
        next(createSocketError(ERROR_CODES.NOT_LOGGED_IN));
        disconnectSocket(socket, 50);
        return;
    }
    var cookies = {};
    if (socket.handshake.headers.cookie) {
        cookies = cookie.parse(socket.handshake.headers.cookie);
    }

    var token = (cookies[tokenName]) || (socket.handshake.headers[tokenName]) || (socket.handshake.headers["x-ved-token"]);
    if (!token) {
        logger.log('no cookies available in the request', cookies);
        next(createSocketError(ERROR_CODES.NOT_LOGGED_IN));
        disconnectSocket(socket, 50);
    } else {
        logger.log('lets check that this is a valid cookie', token);
        jwt.verify(token, jwtSecertKey, function (err, decoded) {
            if (err) {
                logger.log('cookies not valid in the request', err);
                next(createSocketError(ERROR_CODES.NOT_LOGGED_IN));
                disconnectSocket(socket, 50);
            } else {
                logger.log('got request for userId:' + queryParams.userId, JSON.parse(decoded.sessionData));
                if (queryParams.userId == JSON.parse(decoded.sessionData).userId) {
                    if (queryParams.nonSession) {
                        socket.nonSession = true;
                    }
                    if (queryParams.userId) {
                        socket.userId = queryParams.userId;
                    }
                    if (queryParams.sessionId) {//will be used in case of sessions
                        socket.sessionId = queryParams.sessionId;
                        socket.selfPingTimer = null;
                    }
                    socket.publicIp = socket.handshake.headers['x-forwarded-for'] || socket.request.connection.remoteAddress;
                    socket.publicIp = socket.publicIp.replace(/^.*:/, '');
                    logger.debug("Remote socket ip is " + socket.publicIp);
                    if (queryParams.deviceId) {
                        logger.debug("device id is " + queryParams.deviceId);
                        socket.deviceId = queryParams.deviceId;
                        //since deviceId is only coming from Mobile devices, setting the device type to MOBILE
                        socket.deviceType = "MOBILE";
                    }
                    next();
                } else {
                    logger.log('cookies not valid in the request');
                    next(createSocketError(ERROR_CODES.FORBIDDEN_ERROR));
                    disconnectSocket(socket, 50);
                }
            }
        });
    }
};
var disconnectSocket = function (socket, time) {
    time = (!time) ? 0 : time;
    setTimeout(function () {
        socket.disconnect();
    }, time);
};
var createSocketError = function (code) {
    var err = new Error('Authentication error');
    var data = responseUtils.getErrRespObj(code);
    data.server = true;
    err.data = data;
    return err;
};
var isAllowedApp = function (appConfig) {
    var client = appConfig.client;
    if (!client) {
        return false;
    }
    client = client.toUpperCase();
    client = jwtClients[client];
    if (!client) {
        return false;
    } else if ((client.id == appConfig.clientId) && (client.secret == appConfig.clientSecert)) {
        return true;
    }
    return false;
};
var checkIfAllowed = function (req, res, roles, userIds, dontAllowApp) {
    logger.log('checking if request is allowed for ', roles, userIds, dontAllowApp);
    var allowed = true;
    if (dontAllowApp && req.isAppRequest) {
        allowed = false;
    } else if (!req.isAppRequest) {
        var reqRole = req.sessionData.role;
        var reqUserId = req.sessionData.userId;
        if (roles && roles.indexOf(reqRole) < 0 && roles.indexOf(USER_ROLES.all) < 0) {
            allowed = false;
        }
        if (userIds && userIds.indexOf(reqUserId) < 0) {
            allowed = false;
        }
    }
    if (!allowed) {
        logger.log('This request is not allowed ');
        res.status(403).send(responseUtils.getErrRespObj(ERROR_CODES.FORBIDDEN_ERROR));
    }
    return allowed;
};
exports.authenticateRequest = authenticateRequest;
exports.authenticateSocket = authenticateSocket;
exports.USER_ROLES = USER_ROLES;
exports.checkIfAllowed = checkIfAllowed;
exports.authenticateApp = isAllowedApp;