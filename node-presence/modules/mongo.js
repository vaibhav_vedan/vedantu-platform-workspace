var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var Q = require("q");
var MongoClient = require('mongodb').MongoClient;
var mongoProps = configProps.mongo;
var mongoDB, statuschanges;
var createMongoUrlFromProps = function (props) {
    if(props.isAtlas){
        return props.dburi;
    }    
    var url = "mongodb://";
    if (props.username && props.password) {
        var user = encodeURIComponent(props.username);
        var password = encodeURIComponent(props.password);
        url += user + ":" + password + "@";
    }
    for (var i = 0; i < props.host.length; i++) {
        url += props.host[i] + ":" + props.port;
        if (i != (props.host.length - 1)) {
            url += ',';
        }
    }
    url += "/" + props.dbname+"?authMechanism=DEFAULT";
    if (props.replicaSet) {
        url += '&replicaSet=' + props.replicaSet;
    }
    //using sslenabled based on username and password, to differentiate between prod and qa
    if (props.username && props.password) {
        url += '&ssl=true';
    }
    
    return url;
};
MongoClient.connect(createMongoUrlFromProps(mongoProps), function (err, client) {
    if (err) {
        logger.error("Error in connecting to mongo " + err);
        process.exit();
    }
    logger.log("Connection to mongo server opened");
    mongoDB = client.db(mongoProps.dbname);
    statuschanges = mongoDB.collection('statuschanges');
    statuschanges.ensureIndex({userId: 1, timeCreated: -1}, {background: true}, function (err, result) {
        if (err) {
            logger.error("There is some error while SessionIds ensureIndex:", err);
            process.exit();
        }
    });
});
var allowedDeviceTypes = ["DESKTOP", "MOBILE"];
module.exports = {
    saveStatusTypeChangeItem: function (userId, deviceId, deviceType, change) {
        try {
            logger.log("saving json mongo " + JSON.stringify(change) + " for userId " + userId);
            if (!userId || typeof change !== "object") {
                return;
            }
            var timeCreated = new Date().getTime();
            if (allowedDeviceTypes.indexOf(deviceType) === -1) {
                deviceType = "DESKTOP";
            }
            var item = {
                userId: userId,
                deviceId: deviceId || "",
                deviceType: deviceType,
                change: change,
                timeCreated: timeCreated,
                lastUpdated: timeCreated
            };
            statuschanges.insertOne(item, function (err, result) {
                if (err) {
                    logger.error("Error in saving json to mongo " + JSON.stringify(item) + ", err: " + err);
                } else {
                    logger.log("saved json mongo ", JSON.stringify(change));
                }
            });
        } catch (err) {
            logger.error("Error in saving json to mongo " + err);
        }
    },
    removeStatusTypeChangeItems: function (beforeTimeStamp) {
        var deferred = Q.defer();
        beforeTimeStamp = beforeTimeStamp || new Date().getTime();
        logger.log("removing status change items before " + new Date(beforeTimeStamp));
        statuschanges.remove({timeCreated: {$lt: beforeTimeStamp}}, function (err, numberOfRemovedDocs) {
            if (err) {
                logger.error("Error in deleting status change items before : " + beforeTimeStamp,err);
                deferred.resolve(false);
            } else {
                deferred.resolve(true);
            }
        });
        return deferred.promise;
    }
}