/* global process */


//require('nodetime').profile({
//    accountKey: '116a5185e91d7cd241e9b9febef2dbeefb80858b',
//    appName: 'Node.js Application'
//});
var https = require('https');
var http = require('http');
var express = require('express');
var app = express();
var program = require('commander');
var fs = require('fs');
var Q = require('q');
var path = require('path');
var config = require("./config.js");
var redis = require('redis');
var redisAdapter = require('socket.io-redis');
var io;
var enableCluster = false;


//storing the app path
process.env["APP_HOME"] = path.dirname(fs.realpathSync(__filename));


// reading commandline parameters
program
        .version(config.appVersion)
        .option('-m,--mode [type]', 'Running Mode', 'MODE_NOT_PROVIDED')
        .option('-p,--port [type]', 'Port to run')
        .parse(process.argv);

if (config.modes.indexOf(program.mode) == -1) {
    console.error(" Illegal application mode for node server:" + program.mode);
    return 1;
}


config.setConfigPropsForCurrentMode(program.mode).then(function () {

    var configProps = config.getConfigProps();

    configProps.server_port = program.port || configProps.server_port;
    process.env["DEPLOYMENT_MODE"] = program.mode;


//getting the logger
    var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
//var mongo_session = require(process.env["APP_HOME"] + "/modules/mongo-session.js");




//pre start tasks
//1.loading google end points
//var insessiondao = require("./daos/insessiondao.js");
//var loadEndPointsPromise = insessiondao.loadEndPoints();


//2.flushing the redis db 
//TODO do not do this, this way, instead write a script which does this
    var flushDBClient;
    var deleteRedisDBPromise = (function () {
        var redisOptions = configProps.redis;
        flushDBClient = redis.createClient(redisOptions.port, redisOptions.host, {
            detect_buffers: true,
            auth_pass: redisOptions.password
        });
        return flushRedis();
    })();




    var preStartPromises = Q.all([deleteRedisDBPromise]);



    preStartPromises.then(function (resultsArray) {
        // var endPointsResp = resultsArray[0];
        // for (var k = 0; k < endPointsResp.length; k++) {
        //     if (!endPointsResp[k].success) {
        //         logger.error("Error in loading endpoint");
        //         process.exit();
        //         break;
        //     }
        // }
        // logger.info("Successfully loaded all endpoints");

        var deleteDBRes = resultsArray[0];
        logger.info("result of delete redis database is " + deleteDBRes);
        // if (enableCluster) {
        //     ///////////////////////////////////
        //     //      sticky session handling
        //     ///////////////////////////////////
        //     logger.info("Enabling cluster");
        //     var numCPUs = require('os').cpus().length;
        //     logger.info("cpu core count is " + numCPUs);
        //     sticky(numCPUs, function () {
        //         return createServer();
        //     }).listen(configProps.server_port, function () {
        //         // this code is executed in both slaves and master
        //         logger.info('server started on port ' + configProps.server_port + '. process id = ' + process.pid);

        //     });

        // } else {
        ///////////////////////////////////
        //      CREATING SERVER
        ///////////////////////////////////
        logger.info("Creating server");
        var server = createServer();
        server.listen(configProps.server_port);
        logger.info("Listening to server on " + configProps.server_port);
        // }


    }, function (err) {
        logger.error("Error in  deleting redis database");
        logger.error(err);
    });

    function createServer() {
        //preloading all modules
        //var inSessionManager = require("./managers/insessionManager/moduleLoader.js");
        //inSessionManager.loadModules(configProps.insessionModules);


        var server;
        if (configProps.secured) {
            logger.info("Launching https server... ");
            var options = {
                key: fs.readFileSync(process.env["HOME"] + "/" + configProps.certificate.path + "/" + configProps.certificate.keyfile),
                cert: fs.readFileSync(process.env["HOME"] + "/" + configProps.certificate.path + "/" + configProps.certificate.certfile)
            };
            server = https.createServer(options, app);
        } else {
            logger.info("Launching http server on " + configProps.server_port + "... ");
            server = http.createServer(app);
        }

//    //TODO io.configure should that be used to configure something
//    server.listen(configProps.server_port);

        io = require('socket.io')(server, {
            "browser client etag": true,
            "pingTimeout": 10000,
            "pingInterval": 15000,
            "origins": "*:*"
        });

        addRedisAdapter();
        var socketioModule = require("./modules/socketio.js");
        socketioModule.addIOEventHandlers(io);

        return server;
    }

///////////////////////////////////
//      ROUTER
///////////////////////////////////

    logger.info("Mapping the routes " + process.pid);
    var router = require('./modules/router.js');
    router.route(app);


///////////////////////////////////
//      REDIS ADAPTER
///////////////////////////////////

    function addRedisAdapter() {
        var redisOptions = configProps.redis;
        var pub = redis.createClient(redisOptions.port, redisOptions.host, {
            detect_buffers: true
        });
        var sub = redis.createClient(redisOptions.port, redisOptions.host, {
            detect_buffers: true
        });

        io.adapter(redisAdapter({
            pubClient: pub,
            subClient: sub
        }));

        console.log('Redis adapter started with url: ' + redisOptions.host + ":" + redisOptions.port);
    }


    process.on("exit", function () {
        logger.error("Exiting now, performing some cleaning operations");
        flushRedis();
    });

    function flushRedis() {
        var deferred = Q.defer();
        //EVAL "return redis.call('del', unpack(redis.call('keys', ARGV[1])))" 0 prefix:*
        //http://stackoverflow.com/questions/4006324/how-to-atomically-delete-keys-matching-a-pattern-using-redis
        var prefix = "SOCKETIDS_NON_SESSION_*";
        var luaScript = "return redis.call('del', unpack(redis.call('keys', ARGV[1])))";
        flushDBClient.eval([luaScript, 0, prefix], function (err, reply) {
            //sending resolve after the second call only
        });

        prefix = "USER_STATUS_TYPE_*";
        flushDBClient.eval([luaScript, 0, prefix], function (err, reply) {
            deferred.resolve(true);
        });
        return deferred.promise;
    }

    logger.info("initializing cron jobs");
});

