#!/bin/bash
source build.config

#echo "FROM_JENKINS=${BUILD_ENV} FROM_FILE=${profile}"
#echo "FROM_JENKINS=${DEPLOY_DIRECTLY} FROM_FILE=${deployDirectly}"
#echo "FROM_JENKINS=${BUILD_ALL} FROM_FILE=${buildAll}"
#echo "FROM_JENKINS=${SUBSYSTEM_TO_BUILD} FROM_FILE=${subsystemsToBuild}"

DATE_STR=$(date +%Y%m%d)
#################################################################
if [ "${profile}" != "prod" ]; then
  [[ ! -z ${BUILD_ENV} ]] && export profile=${BUILD_ENV} || export profile=${profile}
  [[ ! -z ${DEPLOY_DIRECTLY} ]] && export deployDirectly=${DEPLOY_DIRECTLY} || export deployDirectly=${deployDirectly}
  [[ ! -z ${BUILD_ALL} ]] && export buildAll=${BUILD_ALL} || export buildAll=${buildAll}
  [[ ! -z ${SUBSYSTEM_TO_BUILD} ]] && export subsystemsToBuild=${SUBSYSTEM_TO_BUILD} || export subsystemsToBuild=${subsystemsToBuild}

  echo "profile=${profile}"
  echo "deployDirectly=${deployDirectly}"
  echo "buildAll=${buildAll}"
  echo "subsystemsToBuild=${subsystemsToBuild}"
fi
#################################################################

branch=default
bucket=builds-vedantu-qa
if [ "$CI_BRANCH" == "master" ] || [ "$CI_BRANCH" == "develop" ]; then
  branch="$CI_BRANCH"
fi

export REPO_HOME="/home/rof/.m2/${profile}"
[[ -d ${REPO_HOME} ]] || mkdir -p "${REPO_HOME}"
export BUILD_PROFILE=${profile}
export BUILD_ALL=${buildAll}

if [ "$profile" == "prod" ]; then
  echo "PROD DEPLOYMENT"
  export REPO="vedantu-platform-workspace"
  export HOME="/home/rof/src/bitbucket.org/vedantu"
else
  echo "QA DEPLOYMENT"
  export HOME="/home/rof/src/bitbucket.org/vedantu-${profile}"
  [[ -d ${HOME} ]] || mkdir -p "${HOME}"
  export REPO="vedantu-platform-workspace"
fi

cd utils || exit 1
if [ "$profile" == "prod" ]; then
  echo "deleting swagger utils"
  rm -r src/main/java/com/vedantu/util/swagger
fi
mvn clean install -Dmaven.repo.local="$REPO_HOME"

cd ..

if [ "$skipCI" == "false" ]; then
  tag=$tag
  #if [ "$CI_BRANCH" == "master" ]
  #	then
  #		tag="$(git describe --tags --exact-match "$CI_COMMIT_ID"~1)"
  #fi
  #echo $tag
  if [ "$tag" == "" ]; then
    tag=$CI_COMMIT_ID
  fi
  echo "$tag"
  echo "$profile"

  if [ "$profile" == "prod" ]; then
    export AWS_DEFAULT_REGION="ap-southeast-1"
    export AWS_ACCESS_KEY_ID="AKIAJNCJQKYFXLSY54LA"
    export AWS_SECRET_ACCESS_KEY="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"
    bucket=builds-vedantu-mumbai
  else
    if [ "$buildAll" == "true" ]; then
      subsystemsToBuild="platform dinero listing lms reports notification-centre scheduling subscription user growth"

    fi
  fi

  export BUILD_BUCKET=${bucket}
  echo "build bucket is $BUILD_BUCKET"
  # export HOME="/Users/ajith/projects/git"

  #export REPO=vedantu-platform-workspace

  IFS=' ' read -a subsystems <<<"${subsystemsToBuild}"

  mkdir -p "${HOME}/build/all"
  mkdir -p "${HOME}/build/tar"

  for i in "${subsystems[@]}"; do
    echo "$i"

    cd "${HOME}/${REPO}/$i" || exit 1
    var=$(pwd)
    echo "${var}"
    chmod +x build.sh
    . ./build.sh
  done

  echo "copying appspec and deploymentScripts to build all folder"
  cp "${HOME}/${REPO}/appspec.yml" "${HOME}/build/all/appspec.yml"
  cp -R "${HOME}/${REPO}/deploymentScripts" "${HOME}/build/all/deploymentScripts"

  #creating utility function for deploying or copying
  deployorcopy() {
    if [ "$deployDirectly" == "true" ]; then
      echo "copying the tar file directly to $profile server"
      ssh ubuntu@"$profile"-rtc.vedantu.com "
		  sudo rm -r /home/ubuntu/server-artifacts/warfiles
		  mkdir /home/ubuntu/server-artifacts/warfiles
		"
      scp "${BUILD_PROFILE}-${DATE_STR}.tar.gz" ubuntu@"$profile"-rtc.vedantu.com:/home/ubuntu/server-artifacts/warfiles
      echo "copying done"
      ssh ubuntu@"$profile"-rtc.vedantu.com "
			cd /home/ubuntu/server-artifacts/warfiles
			echo "extracting "${BUILD_PROFILE}"-"${DATE_STR}".tar.gz"
			sudo tar -xvf ${BUILD_PROFILE}-${DATE_STR}.tar.gz
			sleepsecs=45
			for entry in *
			do
				if [[ "\$entry" == *".war"* ]]; then
				  echo "copying \$entry"
				  sudo cp -R \$entry /home/ubuntu/server-artifacts/tomcat/webapps/
				  echo "waiting for \$sleepsecs secs"
				  sleep \$sleepsecs
				fi
			done
		"
      echo "deployment done"
    else
      echo "uploading  to s3"
      aws s3 cp . "s3://${bucket}/${REPO}/${branch}/combined" --recursive --exclude "*" --include "*.tar.gz"
    fi
  }

  if [ "$profile" == "prod" ] ; then
    echo "NOT CREATING COMBINED BUILDS"
  else
    if [ "$buildAll" == "true" ]; then
      rm ./*.tar.gz
      tar -czvf "${BUILD_PROFILE}-${DATE_STR}.tar.gz" -C "${HOME}/build/all" .
      mv "${BUILD_PROFILE}-${DATE_STR}.tar.gz" "${HOME}/build/tar/${BUILD_PROFILE}-${DATE_STR}.tar.gz"
      cd "${HOME}/build/tar" || exit 1
      deployorcopy
    else
      echo "CREATING COMBINED BUILD FOR REQUESTED SUBSYSTEMS"

      cd "${HOME}/build/all" || exit 1
      pwd
      ls -alh
      rm ./*.tar.gz
      tar cvf "${BUILD_PROFILE}-${DATE_STR}.tar" appspec.yml deploymentScripts
      for i in "${subsystems[@]}"; do
        tar uvf "${BUILD_PROFILE}-${DATE_STR}.tar" "$i.war"
      done
      gzip "${BUILD_PROFILE}-${DATE_STR}.tar"
      deployorcopy
    fi
  fi
fi
