package com.vedantu.dinero.interfaces;


public interface IEntity {

	public void preStore();

	public void postStore();
}
