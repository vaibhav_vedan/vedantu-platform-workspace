package com.vedantu.dinero.serializers;


import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.Plan;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class PlanDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PlanDAO.class);


        public PlanDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
	public void create(Plan p) {
			if (p != null) {
				saveEntity(p);
			}
	}

	public Plan getById(String id) {
		Plan Plan = null;
			if (!StringUtils.isEmpty(id)) {
				Plan = getEntityById(id, Plan.class);
			}
		return Plan;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, Plan.class);
		} catch (Exception ex) {
			logger.error("Error in deleting plan with id: "+id, ex);
		}

		return result;
	}

}
