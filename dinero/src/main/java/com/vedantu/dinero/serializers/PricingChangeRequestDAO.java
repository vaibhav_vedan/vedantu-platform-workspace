package com.vedantu.dinero.serializers;

import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.PricingChangeRequest;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class PricingChangeRequestDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PricingChangeRequestDAO.class);


        public PricingChangeRequestDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }

	public void create(PricingChangeRequest p) {
			if (p != null) {
				saveEntity(p);
			}
	}

	public PricingChangeRequest getById(String id) {
		PricingChangeRequest PricingChangeRequest = null;
			if (!StringUtils.isEmpty(id)) {
				PricingChangeRequest = getEntityById(id, PricingChangeRequest.class);
			}
		return PricingChangeRequest;
	}


	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, PricingChangeRequest.class);
		} catch (Exception ex) {
			logger.error("Error in deleting entity with id: "+id, ex);
		}

		return result;
	}

}
