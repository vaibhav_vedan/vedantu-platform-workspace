package com.vedantu.dinero.serializers;

import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.PaymentInvoice;
import com.vedantu.dinero.entity.PaymentInvoiceTypeDetails;
import com.vedantu.dinero.enums.PaymentInvoiceType;
import com.vedantu.dinero.request.GetPaymentInvoiceReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class PaymentInvoiceDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(PaymentInvoiceDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public PaymentInvoiceDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void create(PaymentInvoice p) {
		logger.debug("ENTRY: " + p);
		if (p != null) {
			saveEntity(p);
		}
		logger.debug("EXIT: " + p);
	}

	public void createPaymentInvoiceTypeDetails(PaymentInvoiceTypeDetails p) {
		logger.debug("ENTRY: " + p);
		if (p != null) {
			saveEntity(p);
		}
		logger.debug("EXIT: " + p);
	}

	public PaymentInvoice getById(String id) {
		PaymentInvoice paymentInvoice = null;
		if (!StringUtils.isEmpty(id)) {
			paymentInvoice = getEntityById(id, PaymentInvoice.class);
		}
		return paymentInvoice;
	}

	public PaymentInvoiceTypeDetails getPaymentInvoiceTypeDetailsById(String id) {
		PaymentInvoiceTypeDetails paymentInvoiceTypeDetails = null;
		if (!StringUtils.isEmpty(id)) {
			paymentInvoiceTypeDetails = getEntityById(id, PaymentInvoiceTypeDetails.class);
		}
		return paymentInvoiceTypeDetails;
	}

	public PaymentInvoiceTypeDetails getPaymentInvoiceTypeDetailsByType(PaymentInvoiceType paymentInvoiceType) {
		PaymentInvoiceTypeDetails paymentInvoiceTypeDetails = null;
		if (paymentInvoiceType != null) {
			Query query = new Query();
			query.addCriteria(Criteria.where(PaymentInvoiceTypeDetails.Constants.PAYMENT_INVOICE_TYPE).is(paymentInvoiceType));
			query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
			List<PaymentInvoiceTypeDetails> results = runQuery(query, PaymentInvoiceTypeDetails.class);
			if (!CollectionUtils.isEmpty(results)) {
				if (results.size() > 1) {
					logger.error("Multiple active invoice type details exist for the same type:" + paymentInvoiceType);
				}
				paymentInvoiceTypeDetails = results.get(0);
			}
		}
		return paymentInvoiceTypeDetails;
	}

	public List<PaymentInvoice> getPaymentInvoices(GetPaymentInvoiceReq req) {
		Query query = new Query();
		query.addCriteria(
				new Criteria().orOperator(Criteria.where(PaymentInvoice.Constants.FROM_USER_ID).is(req.getUserId()),
						Criteria.where(PaymentInvoice.Constants.TO_USER_ID).is(req.getUserId())));
		query.addCriteria(Criteria.where(PaymentInvoice.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
                query.with(Sort.by(Sort.Direction.DESC, PaymentInvoice.Constants.INVOICE_TIME));
		setFetchParameters(query, req);
		return runQuery(query, PaymentInvoice.class);
	}


	public List<PaymentInvoice> getPaymentInvoices(Integer start, Integer size) {
		Query query = new Query();
		query.addCriteria(Criteria.where(PaymentInvoice.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		query.with(Sort.by(Sort.Direction.ASC, PaymentInvoice.Constants.INVOICE_TIME));
		setFetchParameters(query, start, size);

		return runQuery(query, PaymentInvoice.class);
	}
}

