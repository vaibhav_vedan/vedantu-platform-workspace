package com.vedantu.dinero.serializers;

import com.vedantu.dinero.entity.EmiCard;
import com.vedantu.dinero.enums.EmiCardType;
import com.vedantu.dinero.enums.EmiType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

@Service
public class EmiCardDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(EmiCardDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(List<EmiCard> emiCards) {
        logger.info("creating emi options ");
        emiCards.stream().filter(Objects::nonNull).forEach(emiCard -> {
            saveEntity(emiCard);
        });
    }

    public EmiCard getById(String id) {


        return getEntityById(id, EmiCard.class);
    }

    public List<EmiCard> getCardsByAmount(Long amount) {
        Query query = new Query();
        query.addCriteria(Criteria.where(EmiCard.Constants.MINIMUM_AMOUNT).lte(amount));
        query.addCriteria(Criteria.where(EmiCard.Constants.IS_ACTIVE).is(true));
        query.addCriteria(Criteria.where(EmiCard.Constants.TYPE).is(EmiType.NO_COST_EMI));
        query.addCriteria(Criteria.where(EmiCard.Constants.IS_ENABLE).is(true));
        query.addCriteria(Criteria.where(EmiCard.Constants.OFFER_ID).exists(true));
        query.with(Sort.by(Sort.Direction.ASC, EmiCard.Constants.CODE));
        logger.info("EMI card Query  " + query);
        return runQuery(query, EmiCard.class);
    }

    public List<EmiCard> getEmiCardByCode(EmiCardType cardType, String code) {
        Query query = new Query();
        if (StringUtils.isNotEmpty(code)) {
            query.addCriteria(Criteria.where(EmiCard.Constants.CODE).is(code));
        }
        if (null != cardType && StringUtils.isNotEmpty(cardType.getName())) {
            query.addCriteria(Criteria.where(EmiCard.Constants.EMI_CARD_TYPE).is(cardType));
        }
        logger.info("EMI card Query by code " + query);
        return runQuery(query, EmiCard.class);
    }

    public List<EmiCard> getBankNames(EmiCardType code) {
        Query query = new Query();
        query.addCriteria(Criteria.where(EmiCard.Constants.EMI_CARD_TYPE).is(code));
        logger.info("get all bank name by code " + query);
        return runQuery(query, EmiCard.class);
    }

    public List<EmiCard> getEmiCards() {
        Query query = new Query();
        logger.info("get all emi card" + query);
        return runQuery(query, EmiCard.class);
    }

    public List<EmiCard> getEmiCardByIds(List<String> ids) {
        return getEntitiesForIds(ids, EmiCard.class);
    }

    public List<String> findDistinctCode(EmiCardType emiCardType) {
        Query query = new Query();
        query.addCriteria(Criteria.where(EmiCard.Constants.EMI_CARD_TYPE).is(emiCardType));
        logger.info("distinct emi code" + query);
        return findDistinct(query, "code", EmiCard.class, String.class);
    }
}



