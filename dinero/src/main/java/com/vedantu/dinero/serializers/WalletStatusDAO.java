package com.vedantu.dinero.serializers;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.WalletStatus;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class WalletStatusDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(WalletStatus.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public WalletStatusDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(WalletStatus p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public WalletStatus getById(String id) {
        WalletStatus WalletStatus = null;
            if (!StringUtils.isEmpty(id)) {
                WalletStatus = getEntityById(id, WalletStatus.class);
            }
        return WalletStatus;
    }
}
