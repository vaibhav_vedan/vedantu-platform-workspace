package com.vedantu.dinero.serializers;

import com.vedantu.dinero.entity.PaymentOffer;
import com.vedantu.dinero.request.PaymentOfferReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaymentOfferDao extends AbstractMongoDAO {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentGatewayDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;


    public PaymentOfferDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public List<PaymentOffer> getPaymentOffers(PaymentOfferReq paymentOfferReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where(PaymentOffer.Constants.GATEWAY).is(paymentOfferReq.getGateway()));
        long currentMillis = System.currentTimeMillis();
        query.addCriteria(Criteria.where(PaymentOffer.Constants.OFFER_START_TIME).lte(currentMillis));
        query.addCriteria(Criteria.where(PaymentOffer.Constants.OFFER_END_TIME).gt(currentMillis));
        if (paymentOfferReq.getPaymentMethod() != null) {
            query.addCriteria(Criteria.where(PaymentOffer.Constants.PAYMENT_METHOD).is(paymentOfferReq.getPaymentMethod()));
        }
        if (StringUtils.isNotEmpty(paymentOfferReq.getServiceProvider())) {
            query.addCriteria(Criteria.where(PaymentOffer.Constants.SERVICE_PROVIDER).is(paymentOfferReq.getServiceProvider()));
        }
        if (StringUtils.isNotEmpty(paymentOfferReq.getCampaignName())) {
            query.addCriteria(Criteria.where(PaymentOffer.Constants.CAMPAIGN_NAME).is(paymentOfferReq.getCampaignName()));
        }
        if (StringUtils.isNotEmpty(paymentOfferReq.getCampaignName())) {
            query.addCriteria(Criteria.where(PaymentOffer.Constants.CAMPAIGN_NAME).is(paymentOfferReq.getCampaignName()));
        }
        if (ArrayUtils.isNotEmpty(paymentOfferReq.getCourseType())) {
            query.addCriteria(Criteria.where(PaymentOffer.Constants.COURSE_TYPE).in(paymentOfferReq.getCourseType()));
        }

        query.with(Sort.by(Sort.Direction.ASC, PaymentOffer.Constants.CREATION_TIME));
        logger.info("PAYMENT OFFER QUERY " + query);
        return runQuery(query, PaymentOffer.class);
    }

    public void save(PaymentOffer paymentOffer) {
        saveEntity(paymentOffer);
    }
}
