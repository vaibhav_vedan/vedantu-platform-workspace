package com.vedantu.dinero.serializers;

import com.vedantu.dinero.entity.InvoiceCounter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class InvoiceCounterService {

    public InvoiceCounterService() {
        super();
    }

    @Autowired
    private MongoClientFactory mongoClientFactory;

    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public Long getNextSequence(String fieldName) {
        return getNextSequence(fieldName, 1);
    }

    public Long getNextSequence(String fieldName, int seqInc) {
        InvoiceCounter counter = getMongoOperations().findAndModify(
                query(where("_id").is(fieldName)),
                new Update().inc("seq", seqInc),
                options().returnNew(true).upsert(true),
                InvoiceCounter.class);
        return counter.getSeq();
    }

}
