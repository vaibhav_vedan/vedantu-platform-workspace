package com.vedantu.dinero.serializers;

import java.util.List;

import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.managers.InstalmentManager;
import com.vedantu.dinero.response.GetUserDashboardAccountInfoRes;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

@Service
public class TransactionDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;
    
    @Autowired
    private InstalmentManager instalmentManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TransactionDAO.class);

    public TransactionDAO() {
        super();
    }

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Transaction p) {
        if (p != null) {
            boolean newCreated = StringUtils.isEmpty(p.getId());
            if (StringUtils.isEmpty(p.getUniqueId())) {
                String id = String.valueOf(System.currentTimeMillis()) + String.valueOf((int) (Math.random() * 1001));
                p.setUniqueId(id);
            }
            logger.info("Transaction saved " + "id : " + p.getId() + " uniqueId : " + p.getUniqueId());

            try{
                saveEntity(p);
                instalmentManager.triggerLeadSquareQueueForTransactionAsync(p, newCreated);
                logger.info("Transaction saved " + "id : " + p.getId() + " uniqueId : " + p.getUniqueId());
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }

    public Transaction getById(String id) {
        Transaction Transaction = null;
            if (!StringUtils.isEmpty(id)) {
                Transaction = getEntityById(id, Transaction.class);
            }
        return Transaction;
    }


    public List<Transaction> getTransactionsByCreditToAccounts(List<String> accountIds, TransactionRefType refType, Long fromTime, Long toTime) {

        Query query;

        query = new Query();
        query.addCriteria(Criteria.where(Transaction.Constants.CREDIT_TO_ACCOUNT).in(accountIds));
        query.addCriteria(Criteria.where(Transaction.Constants.CREATION_TIME).gte(fromTime).lt(toTime));
        if (refType != null) {
            query.addCriteria(Criteria.where(Transaction.Constants.REASON_REF_TYPE).is(refType));
        }

        query.with(Sort.by(Sort.Direction.DESC, Transaction.Constants.CREATION_TIME));

        return runQuery(query, Transaction.class);
    }

    public List<Transaction> getTransactionsByDebitFromAccounts(List<String> accountIds, TransactionRefType refType, Long fromTime, Long toTime) {

        Query query;

        query = new Query();
        query.addCriteria(Criteria.where(Transaction.Constants.DEBIT_FROM_ACCOUNT).in(accountIds));
        query.addCriteria(Criteria.where(Transaction.Constants.CREATION_TIME).gte(fromTime).lt(toTime));
        if (refType != null) {
            query.addCriteria(Criteria.where(Transaction.Constants.REASON_REF_TYPE).is(refType));
        }

        query.with(Sort.by(Sort.Direction.DESC, Transaction.Constants.CREATION_TIME));

        return runQuery(query, Transaction.class);
    }

    public List<Transaction> getTransactionsByRefType(TransactionRefType reasonRefType, Long fromTime, Long toTime) {

        Query query;

        query = new Query();
        query.addCriteria(Criteria.where(Transaction.Constants.CREATION_TIME).gte(fromTime).lt(toTime));
        query.addCriteria(Criteria.where(Transaction.Constants.REASON_REF_TYPE).is(reasonRefType));

        query.with(Sort.by(Sort.Direction.DESC, Transaction.Constants.CREATION_TIME));

        return runQuery(query, Transaction.class);
    }

    public List<Transaction> getTransactionsByRefTypeAndCreditTo(TransactionRefType reasonRefType, String creditToAccount) {

        Query query;

        query = new Query();
        query.addCriteria(Criteria.where(Transaction.Constants.REASON_REF_TYPE).is(reasonRefType));
        query.addCriteria(Criteria.where(Transaction.Constants.CREDIT_TO_ACCOUNT).is(creditToAccount));

        return runQuery(query, Transaction.class);
    }
    
    public AggregationResults<GetUserDashboardAccountInfoRes> runAggregation(Aggregation aggregation){
        return getMongoOperations().aggregate(aggregation,
                    Transaction.class.getSimpleName(), GetUserDashboardAccountInfoRes.class);
    }
}
