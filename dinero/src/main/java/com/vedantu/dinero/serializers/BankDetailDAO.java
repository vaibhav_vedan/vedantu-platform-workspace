package com.vedantu.dinero.serializers;

import com.vedantu.dinero.enums.EmiCardType;
import com.vedantu.dinero.response.BankDetail;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class BankDetailDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(BankDetailDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public List<BankDetail> getBankNames(EmiCardType emiCardType) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BankDetail.Constants.EMI_CARD_TYPE).in(Collections.singletonList(emiCardType)));
        logger.info("Bank detail by emi crad type query is:" + query);
        return runQuery(query, BankDetail.class);
    }

    public void saveBank(BankDetail bankDetail) {
        saveEntity(bankDetail);
    }
}
