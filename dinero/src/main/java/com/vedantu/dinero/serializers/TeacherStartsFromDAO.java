package com.vedantu.dinero.serializers;

import java.util.List;

import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.TeacherStartsFrom;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class TeacherStartsFromDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TeacherStartsFromDAO.class);

    public TeacherStartsFromDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(TeacherStartsFrom p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public TeacherStartsFrom getById(String id) {
        TeacherStartsFrom TeacherStartsFrom = null;
            if (!StringUtils.isEmpty(id)) {
                TeacherStartsFrom = getEntityById(id, TeacherStartsFrom.class);
            }
        return TeacherStartsFrom;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, TeacherStartsFrom.class);
        } catch (Exception ex) {
            logger.error("Error in deleting entity with id: "+id, ex);
        }

        return result;
    }
}
