package com.vedantu.dinero.serializers;

import com.vedantu.dinero.entity.Slab;
import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.SlabRange;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class SlabRangeDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public SlabRangeDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(SlabRange p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public SlabRange getById(String id) {
        SlabRange SlabRange = null;
            if (!StringUtils.isEmpty(id)) {
                SlabRange = getEntityById(id, SlabRange.class);
            }
        return SlabRange;
    }


    public List<SlabRange> getAllSlabRanges() {
        Query query = new Query();
        setFetchParameters(query, 0, 100);
        List<SlabRange> slabRanges = runQuery(query, SlabRange.class);
        return slabRanges;
    }

}
