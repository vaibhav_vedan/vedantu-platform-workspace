package com.vedantu.dinero.serializers;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.UpdateSessionPayoutRequest;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class UpdateSessionPayoutRequestDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public UpdateSessionPayoutRequestDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(UpdateSessionPayoutRequest p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public UpdateSessionPayoutRequest getById(String id) {
        UpdateSessionPayoutRequest UpdateSessionPayoutRequest = null;
            if (!StringUtils.isEmpty(id)) {
                UpdateSessionPayoutRequest = getEntityById(id, UpdateSessionPayoutRequest.class);
            }
        return UpdateSessionPayoutRequest;
    }
}
