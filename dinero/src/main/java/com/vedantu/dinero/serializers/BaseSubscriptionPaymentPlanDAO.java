package com.vedantu.dinero.serializers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.dinero.entity.BaseSubscriptionPaymentPlan;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class BaseSubscriptionPaymentPlanDAO extends AbstractMongoDAO {


    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(BaseSubscriptionPaymentPlanDAO.class);


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public BaseSubscriptionPaymentPlanDAO() {
        super();
    }

    public void save(BaseSubscriptionPaymentPlan p) {
        if (p != null) {
            if(StringUtils.isNotEmpty( p.getId()) ){
                BaseSubscriptionPaymentPlan	_p =  getEntityById(p.getId(), BaseSubscriptionPaymentPlan.class);
                if(_p != null) {
                    p.setCreationTime(_p.getCreationTime());
                    p.setCreatedBy(_p.getCreatedBy());
                }
            }

            saveEntity(p);
        }
    }

    public BaseSubscriptionPaymentPlan getById(String id) {
        BaseSubscriptionPaymentPlan baseSubscriptionPaymentPlan = null;
        if (!StringUtils.isEmpty(id)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(BaseSubscriptionPaymentPlan.Constants.ID).in(id));
            query.addCriteria(Criteria.where(BaseSubscriptionPaymentPlan.Constants.ENTITY_STATE).is(EntityStatus.ACTIVE));
            baseSubscriptionPaymentPlan = findOne(query, BaseSubscriptionPaymentPlan.class);
        }

        return baseSubscriptionPaymentPlan;
    }
    public List<BaseSubscriptionPaymentPlan> getByEntityId(List<String> ids) {
       Query query = new Query();
       query.addCriteria(Criteria.where(BaseSubscriptionPaymentPlan.Constants.PURCHASE_ENTITY_ID).in(ids));
        query.addCriteria(Criteria.where(BaseSubscriptionPaymentPlan.Constants.ENTITY_STATE).is(EntityStatus.ACTIVE));
        query.with(Sort.by(Direction.ASC, BaseSubscriptionPaymentPlan.Constants.VALID_MONTHS)); 
        return runQuery(query, BaseSubscriptionPaymentPlan.class);
    }


}
