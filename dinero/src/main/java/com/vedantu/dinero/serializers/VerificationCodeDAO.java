package com.vedantu.dinero.serializers;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.VerificationCode;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class VerificationCodeDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public VerificationCodeDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(VerificationCode p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public VerificationCode getById(String id) {
        VerificationCode VerificationCode = null;
            if (!StringUtils.isEmpty(id)) {
                VerificationCode = getEntityById(id, VerificationCode.class);
            }
        return VerificationCode;
    }



}
