package com.vedantu.dinero.serializers;

import com.google.gson.Gson;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.exception.VException;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.request.GetRegisteredCoursesReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class OrdersDAO extends AbstractMongoDAO {

    private final Gson gson = new Gson();
    @Autowired
    private LogFactory logFactory;
    private final Logger logger = logFactory.getLogger(OrdersDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;


    public OrdersDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Orders p) {
        logger.debug("ENTRY: " + p);
        if (p != null) {
            saveEntity(p);
        }
        logger.debug("EXIT: " + p);

    }

    public Orders getById(String id) {
        Orders Orders = null;
        if (!StringUtils.isEmpty(id)) {
            Orders = getEntityById(id, Orders.class);
        }
        return Orders;
    }


    public List<Orders> getOrders(Long userId, int start, int limit) {

        Query query = new Query();

        if (userId != null) {
            query.addCriteria(Criteria.where("userId").is(userId));
        }

        query.skip(start);
        query.limit(limit);
        query.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        List<Orders> orders = runQuery(query, Orders.class);

        return orders;
    }

    public List<Orders> getOrders(Query query) {
        logger.info("ENTRY: " + query);
        List<Orders> props = runQuery(query, Orders.class);
        logger.info("EXIT: " + props);
        return props;
    }

    public List<Orders> isDoubtsPaidUser(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Orders.Constants.PURCHASING_ENTITIES_ENTITY_TYPE).is(EntityType.BUNDLE));
        List<String> statusOptions = new ArrayList<>();
        statusOptions.add("PAID");
        statusOptions.add("PARTIALLY_PAID");
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(statusOptions));
//        query.with(new Sort(Sort.Direction.DESC, Orders.Constants.CREATION_TIME));
//        return findOne(query, Orders.class);
        return runQuery(query, Orders.class);
    }

    public List<Orders> getOrdersByStatus(PaymentStatus status, Long fromTime, Long toTime) {

        Query query;

        query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).gte(fromTime).lt(toTime));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(status));

        query.with(Sort.by(Sort.Direction.DESC, Orders.Constants.CREATION_TIME));

        return runQuery(query, Orders.class);
    }

    public List<Orders> getRegisteredCourses(GetRegisteredCoursesReq req) {
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(req.getUserId()));
        //query.addCriteria(Criteria.where("items.entityType").in(Arrays.asList(EntityType.OTF_COURSE_REGISTRATION.name(),EntityType.OTO_COURSE_REGISTRATION.name())));
        query.addCriteria(Criteria.where("referenceTags.referenceType").in(Arrays.asList("OTO_STRUCTURED_COURSE_ID", "OTF_COURSE_ID")));
        query.with(Sort.by(Sort.Direction.DESC, Orders.Constants.CREATION_TIME));
        setFetchParameters(query, req);

        return runQuery(query, Orders.class);
    }


    public Orders getOTFRegisteredCourse(Long userId, String courseId, String batchId) {
        Orders orders = null;
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where("items.entityType").in(Arrays.asList(EntityType.OTF_COURSE_REGISTRATION.name(), EntityType.OTF_BATCH_REGISTRATION.name())));
        query.addCriteria(Criteria.where("items.entityId").in(Arrays.asList(courseId, batchId)));
        List<Orders> ordersList = runQuery(query, Orders.class);
        if (ordersList != null && !ordersList.isEmpty()) {
            orders = ordersList.get(0);
        }
        return orders;
    }

    public List<Orders> getOrdersByAgentEmailId(String emailId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.AGENT_EMAIL_ID).is(emailId.trim()));
        List<Orders> ordersList = runQuery(query, Orders.class);
        if (ordersList != null && !ordersList.isEmpty()) {
            return ordersList;
        }
        return null;
    }

    public List<Orders> getOrdersByListOfAgentEmailId(List<String> emailIds) {
        List<String> trimmedEmailIds = new ArrayList<>();
        for (String emailId : emailIds) {
            trimmedEmailIds.add(emailId.trim());
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.AGENT_EMAIL_ID).in(trimmedEmailIds));
        List<Orders> ordersList = runQuery(query, Orders.class);
        if (ordersList != null && !ordersList.isEmpty()) {
            return ordersList;
        }
        return null;
    }


    public void updateMultiple(Query query, Update update) {
        updateMulti(query, update, Orders.class);
    }

    public Orders getLatestOrderByUserId(Long userId)throws VException{
        logger.info("userId : " + userId );
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).nin(EntityType.BUNDLE_TRIAL));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS)
                .in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.FORFEITED, PaymentStatus.PAYMENT_SUSPENDED)));
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.CREATION_TIME));
        query.fields().include(Orders.Constants.ID);
        query.fields().include(Orders.Constants._ID);
        logger.info("query : "+query);
        Orders order = findOne(query,Orders.class);
        logger.info("order : "+order);
        return order;
    }

    public Orders getLatestTrialOrderByUserId(Long userId, Long startTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(EntityType.BUNDLE_TRIAL));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).ne(PaymentStatus.NOT_PAID));
        query.addCriteria(Criteria.where(Orders.Constants.AMOUNT).is(0));
        query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).gt(startTime));
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.CREATION_TIME));
        query.fields().include(Orders.Constants.ID);
        query.fields().include(Orders.Constants._ID);
        logger.info("query : "+query);
        return findOne(query,Orders.class);
    }

    public List<Orders> exportOrdersCsv(PaymentStatus paymentStatus, Integer start, Integer limit, Long startTime, Long endTime, Integer minAmount, Integer maxAmount){
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(paymentStatus));
        if(startTime != null){
            if(endTime != null){
                query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).gte(startTime).lte(endTime));
            }else{
                query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).gte(startTime));
            }
        }else if(endTime != null){
            query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).lte(endTime));
        }
        if (minAmount != null && maxAmount != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AMOUNT).gte(minAmount).lte(maxAmount));
        } else if (minAmount != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AMOUNT).gte(minAmount));
        } else if (maxAmount != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AMOUNT).lte(maxAmount));
        }
        setFetchParameters(query, start, limit);
        query.fields().include(Orders.Constants.ID);
        query.fields().include(Orders.Constants.USER_ID);
        query.fields().include(Orders.Constants.ITEMS_ENTITY_ID);
        query.fields().include(Orders.Constants.ITEMS_ENTITY_TYPE);
        query.fields().include(Orders.Constants.AMOUNT);
        query.fields().include(Orders.Constants.AMOUNT_PAID);
        query.fields().include(Orders.Constants.INITIAL_FIXED_AMOUNT);
        query.fields().include(Orders.Constants.AGENT_CODE);
        query.fields().include(Orders.Constants.MANUALLY_CHANGED_LOGS_AGENT_CODE);
        query.fields().include(Orders.Constants.EMPLOYEE_ID);
        query.fields().include(Orders.Constants.CREATION_TIME);
        query.fields().include(Orders.Constants.PAYMENT_TYPE);
        return runQuery(query, Orders.class);
    }
}
