package com.vedantu.dinero.serializers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.PaymentGateway;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class PaymentGatewayDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PaymentGatewayDAO.class);

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public PaymentGatewayDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
	public void create(PaymentGateway p) {
			if (p != null) {
				saveEntity(p);
			}
	}

	public PaymentGateway getById(String id) {
		PaymentGateway PaymentGateway = null;
			if (!StringUtils.isEmpty(id)) {
				PaymentGateway = getEntityById(id, PaymentGateway.class);
			}
		return PaymentGateway;
	}


	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, PaymentGateway.class);
		} catch (Exception ex) {
			logger.error("Error in deleting paymentGateway with id: "+id, ex);
		}

		return result;
	}

	public List<PaymentGateway> getPaymentGateways(String id, PaymentGatewayName name, Boolean enabled, Long start,
			Long limit) {

		logger.info("getPaymentGateways", "id:" + id + ", PaymentGatewayName:" + name + ", enabled:" + enabled
				+ ", start:" + start + ", limit:" + limit);

		Query query = new Query();
		if(name != null)
			query.addCriteria(Criteria.where("name").is(name));
		if(enabled != null)
			query.addCriteria(Criteria.where("enabled").is(enabled));
		if(id != null)
			query.addCriteria(Criteria.where("_id").is(id));

		if(start == null)
			start = 0l;
		query.skip(start.intValue());
		
		if(limit == null)
			limit = 0l;
		query.limit(limit.intValue());

		List<PaymentGateway> paymentGateWays = runQuery(query, PaymentGateway.class);

		logger.info(paymentGateWays.toString());
		
		return paymentGateWays;
	}
}
