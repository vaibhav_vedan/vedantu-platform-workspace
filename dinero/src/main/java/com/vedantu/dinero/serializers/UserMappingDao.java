package com.vedantu.dinero.serializers;

import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoWriteException;
import com.vedantu.dinero.entity.UserMapping;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserMappingDao extends AbstractMongoDAO {

    private Logger logger = LogFactory.getLogger(ExtTransactionDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(UserMapping userMapping) {
        try {
            saveEntity(userMapping);
        } catch (DuplicateKeyException | org.springframework.dao.DuplicateKeyException e) {
            update(userMapping);
        } catch (MongoWriteException e) {
            if (e.getMessage().contains("duplicate key error")) {
                update(userMapping);
            } else {
                throw e;
            }
        }
    }

    private void update(UserMapping userMapping) {
        logger.info("UPDATING JUSPAY CUSTOMER : " + userMapping);
        Query query = new Query();
        query.addCriteria(Criteria.where(UserMapping.Constants.VEDANTU_USER_ID).is(userMapping.getVedantuUserId()));
        UserMapping one = findOne(query, UserMapping.class);
        boolean change = false;

        Update update = new Update();
        if (userMapping.getJuspayCustomerId() != null && !userMapping.getJuspayCustomerId().equals(one.getJuspayCustomerId())) {
            update.set(UserMapping.Constants.VEDANTU_USER_ID, userMapping.getVedantuUserId());
            change = true;
        }
        if (change) {
            updateFirst(query, update, UserMapping.class);
        }
    }

    public UserMapping getUserMapping(Long vedantuUserId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(UserMapping.Constants.VEDANTU_USER_ID).is(vedantuUserId));
        List<UserMapping> userMappings = runQuery(query, UserMapping.class);
        if (ArrayUtils.isNotEmpty(userMappings)) {
            return userMappings.get(0);
        }
        return null;
    }
}
