package com.vedantu.dinero.serializers;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.Slab;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class SlabDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public SlabDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Slab p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public Slab getById(String id) {
        Slab Slab = null;
            if (!StringUtils.isEmpty(id)) {
                Slab = getEntityById(id, Slab.class);
            }
        return Slab;
    }


    public List<Slab> getAllSlabs() {
        Query query = new Query();
        setFetchParameters(query, 0, 100);
        List<Slab> slabs = runQuery(query, Slab.class);
        return slabs;
    }

}
