package com.vedantu.dinero.serializers;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.TeacherIncentive;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class TeacherIncentiveDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public TeacherIncentiveDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(TeacherIncentive p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public TeacherIncentive getById(String id) {
        TeacherIncentive TeacherIncentive = null;
            if (!StringUtils.isEmpty(id)) {
                TeacherIncentive = getEntityById(id, TeacherIncentive.class);
            }
        return TeacherIncentive;
    }

}
