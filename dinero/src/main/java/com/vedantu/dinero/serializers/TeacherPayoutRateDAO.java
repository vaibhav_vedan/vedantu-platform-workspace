package com.vedantu.dinero.serializers;

import java.util.List;

import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.TeacherPayoutRate;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class TeacherPayoutRateDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TeacherPayoutRateDAO.class);

    public TeacherPayoutRateDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(TeacherPayoutRate p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public TeacherPayoutRate getById(String id) {
        TeacherPayoutRate TeacherPayoutRate = null;
            if (!StringUtils.isEmpty(id)) {
                TeacherPayoutRate = getEntityById(id, TeacherPayoutRate.class);
            }
        return TeacherPayoutRate;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, TeacherPayoutRate.class);
        } catch (Exception ex) {
            logger.error("Error in deleting entity with id: "+id, ex);
        }

        return result;
    }

}
