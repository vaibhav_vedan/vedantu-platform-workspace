package com.vedantu.dinero.serializers;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.DuplicateKeyException;
import com.vedantu.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.Coupon;
import com.vedantu.dinero.entity.RedeemedCoupon;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.enums.CouponType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.dinero.managers.AccountManager;
import com.vedantu.dinero.request.TransferFromFreebiesAccountReq;
import com.vedantu.dinero.response.TransferFromFreebiesAccountRes;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class CouponDAO extends AbstractMongoDAO {

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private RedeemedCouponDAO redeemedCouponDAO;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public CouponDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Coupon p) throws ConflictException {
        if (p != null) {
            try {
                saveEntity(p);
            }
            catch(org.springframework.dao.DuplicateKeyException | com.mongodb.DuplicateKeyException  e){
                throw new ConflictException(ErrorCode.COUPON_CODE_ALREADY_EXIST, "Coupon code already exists");
            }
        }
    }

    public Coupon getById(String id) {
        Coupon Coupon = null;
        if (!StringUtils.isEmpty(id)) {
            Coupon = getEntityById(id, Coupon.class);
        }
        return Coupon;
    }
    
    public List<Coupon> getByIds(List<String> ids){
    	if(ArrayUtils.isEmpty(ids)) {
    		return null;
    	}
		Query query=new Query();
		query.addCriteria(Criteria.where(Coupon.Constants.ID).in(ids));
		List<Coupon> coupons=runQuery(query, Coupon.class);
    	return coupons;
    }

    public Coupon getCouponByCode(String code) {
        if (StringUtils.isEmpty(code)) {
            return null;
        }
        code = code.toUpperCase();

        Query query = new Query();
        query.addCriteria(Criteria.where("code").is(code));

        query.limit(1);
        List<Coupon> coupons = runQuery(query, Coupon.class);
        if (ArrayUtils.isEmpty(coupons)) {
            return null;
        } else {
            return coupons.get(0);
        }
    }

    public List<Coupon> getCoupons(int start, int limit) {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, "lastUpdated"));
        setFetchParameters(query, start, limit);
        return runQuery(query, Coupon.class);

    }

    public List<Coupon> getPublicCoupons(String target, int start, int limit) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Coupon.Constants.ACCESS_LEVEL).is(AccessLevel.PUBLIC));
        query.addCriteria(Criteria.where("targets.targetStrs").is(target));
        query.with(Sort.by(Sort.Direction.DESC, "lastUpdated"));
        setFetchParameters(query, start, limit);
        return runQuery(query, Coupon.class);

    }

    public Object applyCoupon(Coupon coupon, Long userId) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {

        if (coupon.getType().equals(CouponType.CREDIT)) {
            Integer redeemValue = coupon.getRedeemValue().intValue();
            RedeemedCoupon redeemedCoupon = new RedeemedCoupon(coupon.getCode(), userId, redeemValue, null, null, RedeemedCouponState.PROCESSED);

            redeemedCouponDAO.create(redeemedCoupon);

            Long toUserId = userId;
            TransferFromFreebiesAccountReq req = new TransferFromFreebiesAccountReq(toUserId, redeemValue,
                    TransactionRefType.COUPON_CREDIT, BillingReasonType.FREEBIES, false, false, true,
                    redeemedCoupon.getId(), null);

            req.setCallingUserId(toUserId);

            TransferFromFreebiesAccountRes response = accountManager.transferFromFreebiesAccount(req);

            return response;
        } else {
            return null;
        }
    }

}
