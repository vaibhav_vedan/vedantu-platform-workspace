/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.serializers;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.entity.Instalment.InstalmentConfigProps;
import com.vedantu.dinero.managers.InstalmentAsyncTasksManager;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;

/**
 *
 * @author ajith
 */
@Service
public class InstalmentDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(InstalmentDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

        @Autowired
        private InstalmentAsyncTasksManager instalmentAsyncTasksManager;
        
        private final Gson gson = new Gson();

	public InstalmentDAO() {
		super();
	}

	@Override
	public MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public <E extends AbstractMongoStringIdEntity> void save(E p, String callingUserId) {
		logger.info("ENTRY: " + p);
                Instalment instalment = (Instalment) p;
                boolean newCreated = false;
                if(instalment != null){
                    if(StringUtils.isEmpty(instalment.getId())){
                        newCreated = true;
                    }
                }
		if (p != null) {
			saveEntity(p, callingUserId);
		}
		logger.info("EXIT");
                Instalment temp = (Instalment) p;
                if(newCreated){
                    instalmentAsyncTasksManager.triggerLeadSquareQueue(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.INSTALMENT_SAVED, gson.toJson(temp), temp.getUserId().toString()+"_INST_"+1);
                }else {
                    instalmentAsyncTasksManager.triggerLeadSquareQueue(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.INSTALMENT_UPDATED, gson.toJson(temp), temp.getUserId().toString()+"_INST_"+1);
                }
	}

	public <E extends AbstractMongoStringIdEntity> void save(E p) {
		save(p, null);
	}

	public List<InstalmentConfigProps> getConfigProps(Query query) {
		logger.info("ENTRY: " + query);
		List<InstalmentConfigProps> props = runQuery(query, InstalmentConfigProps.class);
		logger.info("EXIT: " + props);
		return props;
	}
	public List<Instalment> getInstalments(Query query) {
			return getInstalments(query,null);
	}
	public List<Instalment> getInstalments(Query query , List<String> includeFields) {
		logger.info("ENTRY: " + query);
		query.addCriteria(Criteria.where(Instalment.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		List<Instalment> props = new ArrayList<>();
		if( ArrayUtils.isNotEmpty(includeFields)){
			props	= runQuery(query, Instalment.class,includeFields);
		}
		else{
			props	= runQuery(query, Instalment.class);
		}

		logger.info("EXIT: " + props);
		return props;
	}

	public void saveInstalments(List<Instalment> instalments) {
		logger.info("ENTRY: " + instalments);
		insertAllEntities(instalments, Instalment.class.getSimpleName());
                logger.info("EXIT");
                sendToSqsList(instalments);
        }

        public List<DashboardUserInstalmentHistoryRes> getUserInstalmentHistoryAggregation(List<Long> userIds) {
            logger.info("ENTRY: " + userIds);
            AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
            Aggregation agg = newAggregation(
                    Aggregation.match(Criteria.where(Instalment.Constants.USER_ID).in(userIds)),
					Aggregation.match(Criteria.where(Instalment.Constants.ENTITY_STATE).is(EntityState.ACTIVE)),
                    Aggregation.sort(Sort.Direction.ASC, Instalment.Constants.DUE_TIME),
                    Aggregation.group(Instalment.Constants.USER_ID)
                            .first(Instalment.Constants.PAID_TIME).as(DashboardUserInstalmentHistoryRes.Constants.FIRST_INSTALMENT_PAID_TIME)
                            .first(Instalment.Constants.TOTAL_AMOUNT).as(DashboardUserInstalmentHistoryRes.Constants.FIRST_INSTALMENT_PAID_AMOUNT)
                            .last(Instalment.Constants.DUE_TIME).as(DashboardUserInstalmentHistoryRes.Constants.LAST_INSTALMENT_TIME)
            ).withOptions(aggregationOptions);
            logger.info("Agrregation :"+agg);
            AggregationResults<DashboardUserInstalmentHistoryRes> groupResults
                = getMongoOperations().aggregate(agg, Instalment.class.getSimpleName(), DashboardUserInstalmentHistoryRes.class);
            List<DashboardUserInstalmentHistoryRes> result = groupResults.getMappedResults();
            logger.info("EXIT:"+result);
            return result;
        }

        public void sendToSqsList(List<Instalment> instalments){
            if(ArrayUtils.isNotEmpty(instalments)){
                int count = 1;
                for(Instalment instalemnt : instalments){
                    instalmentAsyncTasksManager.triggerLeadSquareQueue(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.INSTALMENT_SAVED, gson.toJson(instalemnt), instalemnt.getUserId().toString()+"_INST_"+count);
                }
            }
        }
	public void updateMultiple(Query query, Update update) {
		updateMulti(query,update, Instalment.class);
	}

	public List<Instalment> getDueInstalments() {
		Long days7Millis = new Long(DateTimeUtils.MILLIS_PER_DAY * 7);
		Query query = new Query();
		query.addCriteria(Criteria.where(Instalment.Constants.CREATION_TIME).gt( 1546353532000L));
		query.addCriteria(Criteria.where(Instalment.Constants.DUE_TIME).lt( System.currentTimeMillis() -  days7Millis));
		query.addCriteria(Criteria.where(Instalment.Constants.ENTITY_STATE).ne(EntityState.DELETED));
		query.addCriteria(Criteria.where(Instalment.Constants.PAYMENT_STATUS).is(PaymentStatus.NOT_PAID));
		query.addCriteria(Criteria.where(Instalment.Constants.CONTEXT_TYPE).ne(InstalmentPurchaseEntity.COURSE_PLAN));
		return runQuery(query, Instalment.class);
	}
}
