package com.vedantu.dinero.serializers;

import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoWriteException;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.PaymentGateway;
import com.vedantu.dinero.entity.UserMapping;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

@Service
public class ExtTransactionDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ExtTransactionDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public ExtTransactionDAO() {
        super();
    }

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(ExtTransaction extTransaction) {
        saveEntity(extTransaction);
    }

    public void create(ExtTransaction p, Long callingUserId) {
        String callingUserIdString = null;
        if (callingUserId != null) {
            callingUserIdString = callingUserId.toString();
        }
        if (p != null) {
            int retryCount = 0;
            while (retryCount < 5) {
                retryCount++;
                try {
                    saveEntity(p, callingUserIdString);
                    break;
                } catch (org.springframework.dao.DuplicateKeyException | com.mongodb.DuplicateKeyException  e) {
                    p.createUniqueOrderId();
                    logger.warn(e.getMessage(), e);
                    if (retryCount > 5) throw e;
                }
            }
        }
    }

    public ExtTransaction getById(String id) {
        ExtTransaction ExtTransaction = null;
        if (!StringUtils.isEmpty(id)) {
            ExtTransaction = getEntityById(id, ExtTransaction.class);
        }
        return ExtTransaction;
    }

    public List<ExtTransaction> getByIds(List<String> ids) {

        Query query = new Query();
        query.addCriteria(Criteria.where("_id").in(ids));
        query.with(Sort.by(Sort.Direction.DESC, "creationTime"));
        return runQuery(query, ExtTransaction.class);
    }

    public List<ExtTransaction> getByVedantuOrderIds(List<String> vedantuOrderIds) {
        if (ArrayUtils.isNotEmpty(vedantuOrderIds) && vedantuOrderIds.size() > 100) {
//            vedantuOrderIds.subList(0, 100);
            logger.error("more than 100 vedantuOrderIds in fetching exttransactions");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(ExtTransaction.Constants.VEDANTU_ORDER_ID).in(vedantuOrderIds));
        return runQuery(query, ExtTransaction.class);
    }

    public List<ExtTransaction> getExtTransactionsToCheckStatus(PaymentGatewayName gateway, String gateWayStatus, TransactionStatus status,
                                                                long fromTime, long start, int limit) {

        List<ExtTransaction> list = null;

        Query query = new Query();
        query.addCriteria(Criteria.where(ExtTransaction.Constants.CREATION_TIME).gt(fromTime));
        query.addCriteria(Criteria.where(ExtTransaction.Constants.GATEWAY_NAME).is(gateway));
        query.addCriteria(Criteria.where(ExtTransaction.Constants.STATUS).is(status));
        query.addCriteria(Criteria.where(ExtTransaction.Constants.GATEWAY_STATUS).is(gateWayStatus));
        query.with(Sort.by(Sort.Direction.ASC, ExtTransaction.Constants.CREATION_TIME));
        query.skip(start);
        query.limit(limit);
        list = runQuery(query, ExtTransaction.class);
        logger.info("Query: " + query);
        return list;
    }

    public List<ExtTransaction> getExtTransactions(Long userId, Long start, Long limit) {

        List<ExtTransaction> list = null;

        Query query = new Query();
        query.addCriteria(Criteria.where(ExtTransaction.Constants.USER_ID).is(userId));
        query.with(Sort.by(Sort.Direction.DESC, ExtTransaction.Constants.CREATION_TIME));
        if (start != null) {
            query.skip(start.intValue());
        }

        if (limit != null) {
            query.limit(limit.intValue());
        }

        list = runQuery(query, ExtTransaction.class);

        return list;
    }
    
    public List<DashboardUserInstalmentHistoryRes> getUserTransactionHistoryAggregation(List<Long> userIds) {
            logger.info("ENTRY: " + userIds);
            AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
            Aggregation agg = newAggregation(
                    Aggregation.match(Criteria.where(ExtTransaction.Constants.USER_ID).in(userIds)),
                    Aggregation.sort(Sort.Direction.ASC, ExtTransaction.Constants.CREATION_TIME),
                    Aggregation.group(ExtTransaction.Constants.USER_ID)
                            .first(ExtTransaction.Constants.CREATION_TIME)
                            .as(DashboardUserInstalmentHistoryRes.Constants.FIRST_EXT_TRANSACTION_TIME)
            ).withOptions(aggregationOptions);
            logger.info("Agrregation :"+agg);
            AggregationResults<DashboardUserInstalmentHistoryRes> groupResults
                = getMongoOperations().aggregate(agg, ExtTransaction.class.getSimpleName(), DashboardUserInstalmentHistoryRes.class);
            List<DashboardUserInstalmentHistoryRes> result = groupResults.getMappedResults();
            logger.info("EXIT:"+result);
            return result;
    }
    
    public List<ExtTransaction> getPendingTransactions(Integer start, Integer limit, Long userId){

        Query query = new Query();
        query.addCriteria(Criteria.where(ExtTransaction.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(ExtTransaction.Constants.STATUS).is(TransactionStatus.PENDING));
        query.with(Sort.by(Sort.Direction.DESC, ExtTransaction.Constants.CREATION_TIME));
        setFetchParameters(query, start, limit);
        return runQuery(query, ExtTransaction.class);
        
    }

    public List<ExtTransaction> getTransactionsForGatewayAlert(int start, int limit, long fromTime, long toTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ExtTransaction.Constants.CREATION_TIME).gte(fromTime).lte(toTime));
        query.fields().include(ExtTransaction.Constants.STATUS)
                .include(ExtTransaction.Constants.CREATION_TIME)
                .include(ExtTransaction.Constants.GATEWAY_NAME)
                .include(ExtTransaction.Constants.PAYMENT_METHOD)
                .include(ExtTransaction.Constants.PAYMENT_INSTRUMENT);
        setFetchParameters(query, start, limit);
        logger.info("Tx Alert Query == " + query);
        return runQuery(query, ExtTransaction.class);
    }

    public List<ExtTransaction> getByParentTxnId(String id, TransactionStatus transactionStatus) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ExtTransaction.Constants.PARENT_TXN_ID).is(id));
        query.addCriteria(Criteria.where(ExtTransaction.Constants.STATUS).is(transactionStatus));
        logger.info("Tx Parent Query == " + query);
        return runQuery(query, ExtTransaction.class);
    }
}
