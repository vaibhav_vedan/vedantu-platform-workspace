package com.vedantu.dinero.serializers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.dinero.request.GetPassRedeemInfoReq;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.dinero.entity.RedeemedCoupon;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class RedeemedCouponDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public RedeemedCouponDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(RedeemedCoupon p) {
            if (p != null) {
                saveEntity(p);
            }
    }

    public RedeemedCoupon getById(String id) {
        RedeemedCoupon RedeemedCoupon = null;
            if (!StringUtils.isEmpty(id)) {
                RedeemedCoupon = getEntityById(id, RedeemedCoupon.class);
            }
        return RedeemedCoupon;
    }


    public long getCouponUsesCount(String couponCode, boolean lockedCountToo) {
        Query query = new Query();
        query.addCriteria(Criteria.where("code").is(couponCode));
        List<RedeemedCouponState> states = new ArrayList<>();
        if (lockedCountToo) {
            states.add(RedeemedCouponState.LOCKED);
        }
        states.add(RedeemedCouponState.PROCESSED);
        query.addCriteria(Criteria.where("state").in(states));
        return queryCount(query, RedeemedCoupon.class);
    }

    public long getUserCouponUsesCount(String couponCode, Long userId, boolean lockedCountToo) {
        Query query = new Query();
        query.addCriteria(Criteria.where("code").is(couponCode));
        query.addCriteria(Criteria.where("userId").is(userId));
        List<RedeemedCouponState> states = new ArrayList<>();
        if (lockedCountToo) {
            states.add(RedeemedCouponState.LOCKED);
        }
        states.add(RedeemedCouponState.PROCESSED);
        query.addCriteria(Criteria.where("state").in(states));
        long count = queryCount(query, RedeemedCoupon.class);
        System.err.println(">>>>>>>> count " + count);
        return count;
    }

    public List<String> getUsedCodes(List<String> codes) {
        if (CollectionUtils.isEmpty(codes)) {
            return null;
        }
        List<String> usedCodes = new ArrayList<String>();
        Query query = new Query();
        query.addCriteria(Criteria.where("code").in(codes));
        List<RedeemedCoupon> coupons = runQuery(query, RedeemedCoupon.class);
        for (RedeemedCoupon coupon : coupons) {
            usedCodes.add(coupon.getCode());
        }

        return usedCodes;

    }

    public Map<String, Integer> getUsesCount(List<String> codes) {

        Map<String, Integer> usesCountMap = new HashMap<String, Integer>();
        if (CollectionUtils.isEmpty(codes)) {
            return usesCountMap;
        }
        List<String> usedCodes = new ArrayList<String>();
        Query query = new Query();
        query.addCriteria(Criteria.where("code").in(codes));
        List<RedeemedCouponState> states = new ArrayList<>();
        states.add(RedeemedCouponState.LOCKED);
        states.add(RedeemedCouponState.PROCESSED);
        query.addCriteria(Criteria.where("state").in(states));
        List<RedeemedCoupon> coupons = runQuery(query, RedeemedCoupon.class);
        for (RedeemedCoupon coupon : coupons) {
            usedCodes.add(coupon.getCode());
        }

        Integer usedCount;
        for (String usedCode : usedCodes) {
            if (usesCountMap.containsKey(usedCode)) {
                usedCount = usesCountMap.get(usedCode);
                usesCountMap.put(usedCode, usedCount + 1);
            } else {
                usesCountMap.put(usedCode, 1);
            }
        }
        return usesCountMap;
    }

    public List<RedeemedCoupon> getRedeemedCoupon(String couponCode, String purchaseFlowId,
            RedeemedCouponState state) {
        Query query = new Query();
        if (!StringUtils.isEmpty(couponCode)) {
            query.addCriteria(Criteria.where("code").is(couponCode));
        }
        if (!StringUtils.isEmpty(purchaseFlowId)) {
            query.addCriteria(Criteria.where("purchaseFlowId").is(purchaseFlowId));
        }
        if (state != null) {
            query.addCriteria(Criteria.where("state").is(state));
        }
        List<RedeemedCoupon> coupons = runQuery(query, RedeemedCoupon.class);
        return coupons;
    }

    public List<RedeemedCoupon> getPassRedeemInfo(GetPassRedeemInfoReq req) {
        Query query = new Query();
        query.addCriteria(Criteria.where(RedeemedCoupon.Constants.PASS_PROCESSING_STATE).is(req.getPassProcessingState()));
        query.addCriteria(Criteria.where(RedeemedCoupon.Constants.PASS_EXPIRATION_TIME).lte(req.getPassExpirationTime()));
        setFetchParameters(query, req);
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.CREATION_TIME));
        List<RedeemedCoupon> coupons = runQuery(query, RedeemedCoupon.class);
        return coupons;
    }

    public RedeemedCoupon getRedeemedCouponByRefId(String refId) {
        RedeemedCoupon redeemedCoupon = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(RedeemedCoupon.Constants.REF_IDS).is(refId));
        query.addCriteria(Criteria.where(RedeemedCoupon.Constants.PASS_PROCESSING_STATE).is(PassProcessingState.VALID));
        List<RedeemedCoupon> coupons = runQuery(query, RedeemedCoupon.class);
        if(CollectionUtils.isNotEmpty(coupons)) {
            redeemedCoupon = coupons.get(0);
        }
        return redeemedCoupon;
    }

    public int updateFirst(Query q, Update u) {
        return updateFirst(q,u,RedeemedCoupon.class);
    }
}
