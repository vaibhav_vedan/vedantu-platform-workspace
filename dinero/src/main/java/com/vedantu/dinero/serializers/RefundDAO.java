/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.serializers;

import com.vedantu.dinero.entity.Refund;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class RefundDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public RefundDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Refund p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public List<Refund> getRefundsByOrderId(List<String> orderIds) {

        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").in(orderIds));

        setFetchParameters(query, 0, 100);
        return runQuery(query, Refund.class);
    }

    public Refund getRefundByTransactionId(String transactionId){
        Query query = new Query();
        query.addCriteria(Criteria.where("transactionId").is(transactionId));
        return findOne(query, Refund.class);
    }
    
}
