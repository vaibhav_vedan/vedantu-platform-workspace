package com.vedantu.dinero.serializers;

import com.vedantu.dinero.entity.Instalment.BaseInstalment;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class BaseInstalmentDAO extends AbstractMongoDAO {


    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(BaseInstalmentDAO.class);
    
    
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public BaseInstalmentDAO() {
        super();
    }

    public void save(BaseInstalment p) {
        if (p != null) {
            if (CollectionUtils.isNotEmpty(p.getInfo())) {
                Collections.sort(p.getInfo(), new Comparator<BaseInstalmentInfo>() {
                    @Override
                    public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                        return o1.getDueTime().compareTo(o2.getDueTime());
                    }
                }
                );
            }
            saveEntity(p);
        }
    }

    public BaseInstalment getById(String id) {
        BaseInstalment BaseInstalment = null;
        if (!StringUtils.isEmpty(id)) {
            BaseInstalment = (BaseInstalment) getEntityById(id, BaseInstalment.class);
        }
        return BaseInstalment;
    }

    public int deleteById(String id) {
        return deleteEntityById(id, BaseInstalment.class);
    }

    public BaseInstalment getBaseInstalment(InstalmentPurchaseEntity purchaseEntityType,
            String purchaseEntityId, Long userId) {
        BaseInstalment baseInstalment = null;
        Query query = new Query();
        if (purchaseEntityType != null) {
            query.addCriteria(Criteria.where(BaseInstalment.Constants.PURCHASE_ENTITY_TYPE).is(purchaseEntityType));
        }
        if (!StringUtils.isEmpty(purchaseEntityId)) {
            query.addCriteria(Criteria.where(BaseInstalment.Constants.PURCHASE_ENTITY_ID).is(purchaseEntityId));
        }

        if (userId != null) {
            Criteria orConditionCriteria = new Criteria();
            orConditionCriteria.orOperator(Criteria.where(BaseInstalment.Constants.USER_ID).exists(false),
                    Criteria.where(BaseInstalment.Constants.USER_ID).is(userId));
            query.addCriteria(orConditionCriteria);
        }
        else{
            query.addCriteria(Criteria.where(BaseInstalment.Constants.USER_ID).exists(false));
        }

        //query.with(new Sort(Sort.Direction.ASC, BaseInstalment.Constants.DUE_TIME));
        List<BaseInstalment> results = runQuery(query, BaseInstalment.class);

        if (results != null && !results.isEmpty()) {
            baseInstalment = results.get(0);
            for (BaseInstalment _baseInstalment : results) {
                if (userId != null && userId.equals(_baseInstalment.getUserId())) {
                    baseInstalment = _baseInstalment;
                    break;
                }
            }
        }
        return baseInstalment;
    }

    public List<BaseInstalment> getBaseInstalments(InstalmentPurchaseEntity purchaseEntityType, List<String> purchaseEntityIds) {
        Query query = new Query();
        if (purchaseEntityType != null) {
            query.addCriteria(Criteria.where(BaseInstalment.Constants.PURCHASE_ENTITY_TYPE).is(purchaseEntityType));
        }
        if (purchaseEntityIds != null && !purchaseEntityIds.isEmpty()) {
            query.addCriteria(Criteria.where(BaseInstalment.Constants.PURCHASE_ENTITY_ID).in(purchaseEntityIds));
        }
        //query.with(new Sort(Sort.Direction.ASC, BaseInstalment.Constants.DUE_TIME));
        List<BaseInstalment> results = runQuery(query, BaseInstalment.class);

        return results;
    }

    public List<BaseInstalment> getFirstBaseInstalmentsForDueTime(Long startTime, Long endTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseInstalment.Constants.INFO + ".0.dueTime").gte(startTime).lt(endTime));

        query.addCriteria(Criteria.where(BaseInstalment.Constants.PURCHASE_ENTITY_TYPE).is(InstalmentPurchaseEntity.BATCH));
        //query.with(new Sort(Sort.Direction.ASC, BaseInstalment.Constants.DUE_TIME));
        List<BaseInstalment> results = runQuery(query, BaseInstalment.class);

        return results;
    }

    public BaseInstalment getStudentBaseInstalment(InstalmentPurchaseEntity purchaseEntityType,
            String purchaseEntityId, Long userId) {
        if (purchaseEntityType == null || StringUtils.isEmpty(purchaseEntityId) || userId == null) {
            return null;
        }
        BaseInstalment baseInstalment = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseInstalment.Constants.PURCHASE_ENTITY_TYPE).is(purchaseEntityType));
        query.addCriteria(Criteria.where(BaseInstalment.Constants.PURCHASE_ENTITY_ID).is(purchaseEntityId));
        query.addCriteria(Criteria.where(BaseInstalment.Constants.USER_ID).is(userId));

        List<BaseInstalment> results = runQuery(query, BaseInstalment.class);

        if (results != null && !results.isEmpty()) {
            baseInstalment = results.get(0);
            if (results.size() > 1) {
                logger.error("multiple baseinstalments found for combo purchaseEntityId "
                        + purchaseEntityId + ", userId " + userId);
            }
        }
        return baseInstalment;
    }

}
