package com.vedantu.dinero.pojo;

public class InstalmentChangeTime {

	private Long changeTime;
	private Long changedBy;
	private Integer previousAmount;
	private Integer newAmount;
	private Long previousDueDate;
	private Long newDueDate;

	public Long getChangeTime() {
		return changeTime;
	}

	public void setChangeTime(Long changeTime) {
		this.changeTime = changeTime;
	}

	public Long getChangedBy() {
		return changedBy;
	}

	public void setChangedBy(Long changedBy) {
		this.changedBy = changedBy;
	}

	public Integer getPreviousAmount() {
		return previousAmount;
	}

	public void setPreviousAmount(Integer previousAmount) {
		this.previousAmount = previousAmount;
	}

	public Integer getNewAmount() {
		return newAmount;
	}

	public void setNewAmount(Integer newAmount) {
		this.newAmount = newAmount;
	}

	public Long getPreviousDueDate() {
		return previousDueDate;
	}

	public void setPreviousDueDate(Long previousDueDate) {
		this.previousDueDate = previousDueDate;
	}

	public Long getNewDueDate() {
		return newDueDate;
	}

	public void setNewDueDate(Long newDueDate) {
		this.newDueDate = newDueDate;
	}
}
