/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

/**
 *
 * @author darshit
 */
public class OrderAmountChangeLog {

    private Integer prevDiscount;
    private Integer newDiscount;
    private String couponCode;;
    private Integer prevAmount;
    private Integer newAmount;
    private Long modifiedOn;
    private Long modifiedBy;

    public OrderAmountChangeLog(Integer prevDiscount, Integer newDiscount, String couponCode, Integer prevAmount, Integer newAmount, Long modifiedOn, Long modifiedBy) {
        this.prevDiscount = prevDiscount;
        this.newDiscount = newDiscount;
        this.couponCode = couponCode;
        this.prevAmount = prevAmount;
        this.newAmount = newAmount;
        this.modifiedOn = modifiedOn;
        this.modifiedBy = modifiedBy;
    }

    public Integer getPrevDiscount() {
        return prevDiscount;
    }

    public void setPrevDiscount(Integer prevDiscount) {
        this.prevDiscount = prevDiscount;
    }

    public Integer getNewDiscount() {
        return newDiscount;
    }

    public void setNewDiscount(Integer newDiscount) {
        this.newDiscount = newDiscount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getPrevAmount() {
        return prevAmount;
    }

    public void setPrevAmount(Integer prevAmount) {
        this.prevAmount = prevAmount;
    }

    public Integer getNewAmount() {
        return newAmount;
    }

    public void setNewAmount(Integer newAmount) {
        this.newAmount = newAmount;
    }

    public Long getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Long modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}