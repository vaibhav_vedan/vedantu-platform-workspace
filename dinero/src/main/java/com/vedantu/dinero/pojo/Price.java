package com.vedantu.dinero.pojo;

public class Price {

	int min;
	int max;
	Long price;

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Price() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Price(int min, int max, Long price) {
		super();
		this.min = min;
		this.max = max;
		this.price = price;
	}

	@Override
	public String toString() {
		return "Price [min=" + min + ", max=" + max + ", price=" + price + "]";
	}

}
