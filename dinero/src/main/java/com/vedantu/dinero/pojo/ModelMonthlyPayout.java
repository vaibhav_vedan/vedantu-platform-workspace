package com.vedantu.dinero.pojo;

import com.vedantu.util.enums.SessionModel;
import java.util.List;


public class ModelMonthlyPayout {

	private Long teacherId;
	private List<TotalMonthlyPayout> monthlyPayouts;
	private SessionModel model;
	private int totalSessions;
	private int studentsTaught;
	private int sessionEarnings;
	private int cut;
	private int incentive;
	private int totalEarnings;

	public ModelMonthlyPayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ModelMonthlyPayout [teacherId=" + teacherId + ", monthlyPayouts=" + monthlyPayouts + ", model=" + model
				+ ", totalSessions=" + totalSessions + ", studentsTaught=" + studentsTaught + ", sessionEarnings="
				+ sessionEarnings + ", cut=" + cut + ", incentive=" + incentive + ", totalEarnings=" + totalEarnings
				+ "]";
	}

	public ModelMonthlyPayout(Long teacherId, List<TotalMonthlyPayout> monthlyPayouts, SessionModel model,
			int totalSessions, int studentsTaught, int sessionEarnings, int cut, int incentive, int totalEarnings) {
		super();
		this.teacherId = teacherId;
		this.monthlyPayouts = monthlyPayouts;
		this.model = model;
		this.totalSessions = totalSessions;
		this.studentsTaught = studentsTaught;
		this.sessionEarnings = sessionEarnings;
		this.cut = cut;
		this.incentive = incentive;
		this.totalEarnings = totalEarnings;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public List<TotalMonthlyPayout> getMonthlyPayouts() {
		return monthlyPayouts;
	}

	public void setMonthlyPayouts(List<TotalMonthlyPayout> monthlyPayouts) {
		this.monthlyPayouts = monthlyPayouts;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public int getTotalSessions() {
		return totalSessions;
	}

	public void setTotalSessions(int totalSessions) {
		this.totalSessions = totalSessions;
	}

	public int getStudentsTaught() {
		return studentsTaught;
	}

	public void setStudentsTaught(int studentsTaught) {
		this.studentsTaught = studentsTaught;
	}

	public int getSessionEarnings() {
		return sessionEarnings;
	}

	public void setSessionEarnings(int sessionEarnings) {
		this.sessionEarnings = sessionEarnings;
	}

	public int getCut() {
		return cut;
	}

	public void setCut(int cut) {
		this.cut = cut;
	}

	public int getIncentive() {
		return incentive;
	}

	public void setIncentive(int incentive) {
		this.incentive = incentive;
	}

	public int getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(int totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

}
