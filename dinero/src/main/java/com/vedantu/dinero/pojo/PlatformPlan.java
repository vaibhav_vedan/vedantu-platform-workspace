/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.interfaces.IEntity;

/**
 *
 * @author ajith
 */
//implementing this Ientity even though the PlatformPlan is not part of entitytype
//here but actually all entities in EntityType should
//be implement one IEntity
public class PlatformPlan implements IEntity {

    private String id;
    private Integer min;
    private Integer max;
    private Integer price;
    private Long teacherId;
    private UserBasicInfo teacherInfo;

    public PlatformPlan() {
    }

    public PlatformPlan(String id, Integer min, Integer max, Integer price, Long teacherId) {
        this(id, min, max, price, teacherId, null);
    }

    public PlatformPlan(String id, Integer min, Integer max, Integer price,
            Long teacherId, UserBasicInfo teacherInfo) {
        this.id = id;
        this.min = min;
        this.max = max;
        this.price = price;
        this.teacherId = teacherId;
        this.teacherInfo = teacherInfo;
    }

    public PlatformPlan(TeacherSlabPlan plan) {
    	this.id = plan.getId();
        this.min = plan.getMin();
        this.max = plan.getMax();
        this.price = plan.getPrice().intValue();
        this.teacherId = plan.getTeacherId();
	}

	public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getMin() {
        return min;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public Integer getMax() {
        return max;
    }

    public void setMax(Integer max) {
        this.max = max;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public UserBasicInfo getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(UserBasicInfo teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    @Override
    public String toString() {
        return "PlatformPlan{" + "id=" + id + ", min=" + min + ", max=" + max + ", price=" + price + ", teacherId=" + teacherId + ", teacherInfo=" + teacherInfo + '}';
    }

    @Override
    public void preStore() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void postStore() {
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
