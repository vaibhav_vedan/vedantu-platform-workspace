package com.vedantu.dinero.pojo;

import java.util.List;

public class TeacherRequest {

	private List<ApprovePriceWithTime> requests;
	private Long userId;

	public List<ApprovePriceWithTime> getRequests() {
		return requests;
	}

	public void setRequests(List<ApprovePriceWithTime> requests) {
		this.requests = requests;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String toString() {
		return "TeacherRequest [requests=" + requests + ", userId=" + userId + "]";
	}

	public TeacherRequest(List<ApprovePriceWithTime> requests, Long userId) {
		super();
		this.requests = requests;
		this.userId = userId;
	}

	public TeacherRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

}
