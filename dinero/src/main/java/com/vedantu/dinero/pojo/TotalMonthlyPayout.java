package com.vedantu.dinero.pojo;

public class TotalMonthlyPayout {

	private int month;
	private int year;
	private int totalSessions = 0;
	private int studentsTaught = 0;
	private int sessionEarnings = 0;
	private int cut = 0;
	private int incentive = 0;
	private int totalEarnings = 0;

	public int getMonth() {
		return month;
	}

	public void setMonth(int month) {
		this.month = month;
	}

	public int getCut() {
		return cut;
	}

	public void setCut(int cut) {
		this.cut = cut;
	}

	@Override
	public String toString() {
		return "TotalMonthlyPayout [month=" + month + ", year=" + year + ", totalSessions=" + totalSessions
				+ ", studentsTaught=" + studentsTaught + ", sessionEarnings=" + sessionEarnings + ", cut=" + cut
				+ ", incentive=" + incentive + ", totalEarnings=" + totalEarnings + "]";
	}

	public TotalMonthlyPayout(int month, int year, int totalSessions, int studentsTaught, int sessionEarnings, int cut,
			int incentive, int totalEarnings) {
		super();
		this.month = month;
		this.year = year;
		this.totalSessions = totalSessions;
		this.studentsTaught = studentsTaught;
		this.sessionEarnings = sessionEarnings;
		this.cut = cut;
		this.incentive = incentive;
		this.totalEarnings = totalEarnings;
	}

	public TotalMonthlyPayout() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getTotalSessions() {
		return totalSessions;
	}

	public void setTotalSessions(int totalSessions) {
		this.totalSessions = totalSessions;
	}

	public int getStudentsTaught() {
		return studentsTaught;
	}

	public void setStudentsTaught(int studentsTaught) {
		this.studentsTaught = studentsTaught;
	}

	public int getSessionEarnings() {
		return sessionEarnings;
	}

	public void setSessionEarnings(int sessionEarnings) {
		this.sessionEarnings = sessionEarnings;
	}

	public int getIncentive() {
		return incentive;
	}

	public void setIncentive(int incentive) {
		this.incentive = incentive;
	}

	public int getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(int totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

}
