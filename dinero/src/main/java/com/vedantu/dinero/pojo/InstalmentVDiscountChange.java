/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

/**
 *
 * @author ajith
 */
public class InstalmentVDiscountChange {

    private Integer prevDiscount;
    private Integer newDiscount;
    private String couponCode;
    private Long modifiedOn;
    private Long modifiedBy;

    public InstalmentVDiscountChange(Integer prevDiscount, Integer newDiscount, String couponCode, Long modifiedOn, Long modifiedBy) {
        this.prevDiscount = prevDiscount;
        this.newDiscount = newDiscount;
        this.couponCode = couponCode;
        this.modifiedOn = modifiedOn;
        this.modifiedBy = modifiedBy;
    }

    public InstalmentVDiscountChange() {
    }

    public Integer getPrevDiscount() {
        return prevDiscount;
    }

    public void setPrevDiscount(Integer prevDiscount) {
        this.prevDiscount = prevDiscount;
    }

    public Integer getNewDiscount() {
        return newDiscount;
    }

    public void setNewDiscount(Integer newDiscount) {
        this.newDiscount = newDiscount;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public Long getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(Long modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public Long getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(Long modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

}
