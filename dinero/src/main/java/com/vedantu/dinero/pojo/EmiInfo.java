package com.vedantu.dinero.pojo;

import com.vedantu.User.Role;
import com.vedantu.dinero.enums.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class EmiInfo {
    private String bankCode;
    private Integer tenure;
    private float rate;

}
