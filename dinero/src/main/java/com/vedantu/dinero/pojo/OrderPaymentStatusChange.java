package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.PaymentStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author MNPK
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderPaymentStatusChange {
    private PaymentStatus prevStatus;
    private PaymentStatus newStatus;
    private Long changedTime;
    private String changedBy;
}
