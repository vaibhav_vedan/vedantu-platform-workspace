package com.vedantu.dinero.pojo;

import lombok.Data;

@Data
public class EmiTenure {
    private Integer months;
    private float rate;
}
