/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.PaymentStatus;

/**
 *
 * @author ajith
 */
public class InstalmentStatusChange {

    private PaymentStatus prevStatus;
    private PaymentStatus newStatus;
    private Long changeTime;

    public InstalmentStatusChange() {
    }

    public InstalmentStatusChange(PaymentStatus prevStatus, PaymentStatus newStatus, Long changeTime) {
        this.prevStatus = prevStatus;
        this.newStatus = newStatus;
        this.changeTime = changeTime;
    }

    public PaymentStatus getPrevStatus() {
        return prevStatus;
    }

    public void setPrevStatus(PaymentStatus prevStatus) {
        this.prevStatus = prevStatus;
    }

    public PaymentStatus getNewStatus() {
        return newStatus;
    }

    public void setNewStatus(PaymentStatus newStatus) {
        this.newStatus = newStatus;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    @Override
    public String toString() {
        return "InstalmentStatusChange{" + "prevStatus=" + prevStatus + ", newStatus=" + newStatus + ", changeTime=" + changeTime + '}';
    }

}
