/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.coupon.CouponTargetEntityType;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CouponTarget {
    private CouponTargetEntityType targetType;//eg Subject,target,grade
    //will go with strings for all entities, so comparison becomes easy
    private List<String> targetStrs;//eg<physics,maths><cbse,icse>,<11,12>

    public CouponTarget() {
    }

    public CouponTarget(CouponTargetEntityType targetType, List<String> targetStrs) {
        this.targetType = targetType;
        this.targetStrs = targetStrs;
    }

    public CouponTargetEntityType getTargetType() {
        return targetType;
    }

    public void setTargetType(CouponTargetEntityType targetType) {
        this.targetType = targetType;
    }

    public List<String> getTargetStrs() {
        return targetStrs;
    }

    public void setTargetStrs(List<String> targetStrs) {
        this.targetStrs = targetStrs;
    }

    @Override
    public String toString() {
        return "CouponTarget{" + "targetType=" + targetType + ", targetStrs=" + targetStrs + '}';
    }
    
}
