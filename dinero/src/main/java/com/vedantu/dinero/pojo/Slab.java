package com.vedantu.dinero.pojo;

public class Slab {

	int min;
	int max;
	Long price;
	Long minValue;

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getMinValue() {
		return minValue;
	}

	public void setMinValue(Long minValue) {
		this.minValue = minValue;
	}

	public Slab(int min, int max, Long price, Long minValue) {
		super();
		this.min = min;
		this.max = max;
		this.price = price;
		this.minValue = minValue;
	}

	public Slab() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Slab [min=" + min + ", max=" + max + ", price=" + price + ", minValue=" + minValue + "]";
	}

}
