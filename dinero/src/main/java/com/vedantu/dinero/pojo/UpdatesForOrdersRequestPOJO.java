package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.*;
import lombok.Data;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Data
public class UpdatesForOrdersRequestPOJO{
	private String id;

	private PaymentThrough paymentThrough;
	private TeamType teamType;
	private String agentCode;
	private String agentEmailId;
	private String reportingTeamLeadId;
	private String teamLeadEmailId;
	private Centre centre;
	private EnrollmentType enrollmentType;
	private FintechRisk fintechRisk;
	private long netCollection;
	private String notes;
	private String employeeId;
	private RequestRefund requestRefund;


	private static final Set<PaymentThrough> paymentThroughSet=new HashSet();
	private static final Set<TeamType> teamTypeSet=new HashSet();
	private static final Set<Centre> centreSet=new HashSet();
	private static final Set<EnrollmentType> enrollmentTypeSet=new HashSet();
	private static final Set<FintechRisk> fintechRiskSet=new HashSet();
	private static final Set<RequestRefund> requestRefundSet=new HashSet();

	public UpdatesForOrdersRequestPOJO() {
		paymentThroughSet.addAll(Arrays.asList(PaymentThrough.values()));
		teamTypeSet.addAll(Arrays.asList(TeamType.values()));
		centreSet.addAll(Arrays.asList(Centre.values()));
		enrollmentTypeSet.addAll(Arrays.asList(EnrollmentType.values()));
		fintechRiskSet.addAll(Arrays.asList(FintechRisk.values()));
		requestRefundSet.addAll(Arrays.asList(RequestRefund.values()));
	}

	public static Set<PaymentThrough> getPaymentThroughSet() {
		return paymentThroughSet;
	}

	public static Set<TeamType> getTeamTypeSet() {
		return teamTypeSet;
	}

	public static Set<Centre> getCentreSet() {
		return centreSet;
	}

	public static Set<EnrollmentType> getEnrollmentTypeSet() {
		return enrollmentTypeSet;
	}

	public static Set<FintechRisk> getFintechRiskSet() {
		return fintechRiskSet;
	}

	public static Set<RequestRefund> getRequestRefundSet() { return  requestRefundSet;}
}
