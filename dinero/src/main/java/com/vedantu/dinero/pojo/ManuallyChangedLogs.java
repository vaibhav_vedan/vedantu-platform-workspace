package com.vedantu.dinero.pojo;

import com.vedantu.User.Role;
import com.vedantu.dinero.enums.Centre;
import com.vedantu.dinero.enums.EnrollmentType;
import com.vedantu.dinero.enums.FintechRisk;
import com.vedantu.dinero.enums.PaymentThrough;
import com.vedantu.dinero.enums.TeamType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ManuallyChangedLogs {
    private PaymentThrough paymentThrough;
    private TeamType teamType;
    private String agentCode;
    private String agentEmailId;
    private String reportingTeamLeadId;
    private String teamLeadEmailId;
    private Centre centre;
    private EnrollmentType enrollmentType;
    private FintechRisk fintechRisk;
    private long netCollection;
    private long changedTime;
    private long changedBy;
    private Role role;
    private String notes;
}
