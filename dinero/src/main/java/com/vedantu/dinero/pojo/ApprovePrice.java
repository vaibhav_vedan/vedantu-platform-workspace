package com.vedantu.dinero.pojo;

public class ApprovePrice {

	int min;
	int max;
	Long price;
	boolean approved;

	public ApprovePrice() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ApprovePrice(int min, int max, Long price, boolean approved) {
		super();
		this.min = min;
		this.max = max;
		this.price = price;
		this.approved = approved;
	}

	@Override
	public String toString() {
		return "ApprovePrice [min=" + min + ", max=" + max + ", price=" + price + ", approved=" + approved + "]";
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public boolean isApproved() {
		return approved;
	}

	public boolean getApproved() {
		return approved;
	}
	
	public void setApproved(boolean approved) {
		this.approved = approved;
	}

}
