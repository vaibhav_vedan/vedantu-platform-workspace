package com.vedantu.dinero.pojo;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author SP
 */
@Data
@EqualsAndHashCode(exclude = {"reportMap"})
public class GatewayReport {
    private String key;
    private TransactionStatus status;
    private PaymentGatewayName gatewayName;
    private Map<Long, Long> reportMap = new TreeMap<>();

    public GatewayReport(String key, TransactionStatus status, PaymentGatewayName gatewayName) {
        this.key = key;
        this.status = status;
        this.gatewayName = gatewayName;
    }
}
