package com.vedantu.dinero.pojo;

public class WalletStatusInfo {

	private String holderId;
        private Long currentTimeMilli;
        private Long previousTimeMilli;
	private int accountBalanceCurrent;
	private int accountBalancePrevious;
	private int accountPromotionalBalanceCurrent;
	private int accountPromotionalBalancePrevious;
	private int accountNonPromotionalBalanceCurrent;
	private int accountNonPromotionalBalancePrevious;

    public String getHolderId() {
        return holderId;
    }

    public void setHolderId(String holderId) {
        this.holderId = holderId;
    }

    public Long getCurrentTimeMilli() {
        return currentTimeMilli;
    }

    public void setCurrentTimeMilli(Long currentTimeMilli) {
        this.currentTimeMilli = currentTimeMilli;
    }

    public Long getPreviousTimeMilli() {
        return previousTimeMilli;
    }

    public void setPreviousTimeMilli(Long previousTimeMilli) {
        this.previousTimeMilli = previousTimeMilli;
    }

    public int getAccountBalanceCurrent() {
        return accountBalanceCurrent;
    }

    public void setAccountBalanceCurrent(int accountBalanceCurrent) {
        this.accountBalanceCurrent = accountBalanceCurrent;
    }

    public int getAccountBalancePrevious() {
        return accountBalancePrevious;
    }

    public void setAccountBalancePrevious(int accountBalancePrevious) {
        this.accountBalancePrevious = accountBalancePrevious;
    }

    public int getAccountPromotionalBalanceCurrent() {
        return accountPromotionalBalanceCurrent;
    }

    public void setAccountPromotionalBalanceCurrent(int accountPromotionalBalanceCurrent) {
        this.accountPromotionalBalanceCurrent = accountPromotionalBalanceCurrent;
    }

    public int getAccountPromotionalBalancePrevious() {
        return accountPromotionalBalancePrevious;
    }

    public void setAccountPromotionalBalancePrevious(int accountPromotionalBalancePrevious) {
        this.accountPromotionalBalancePrevious = accountPromotionalBalancePrevious;
    }

    public int getAccountNonPromotionalBalanceCurrent() {
        return accountNonPromotionalBalanceCurrent;
    }

    public void setAccountNonPromotionalBalanceCurrent(int accountNonPromotionalBalanceCurrent) {
        this.accountNonPromotionalBalanceCurrent = accountNonPromotionalBalanceCurrent;
    }

    public int getAccountNonPromotionalBalancePrevious() {
        return accountNonPromotionalBalancePrevious;
    }

    public void setAccountNonPromotionalBalancePrevious(int accountNonPromotionalBalancePrevious) {
        this.accountNonPromotionalBalancePrevious = accountNonPromotionalBalancePrevious;
    }

        

	public WalletStatusInfo(String holderId, Long currentTimeMilli, Long previousTimeMilli, int accountBalanceCurrent, int accountBalancePrevious,
			int accountPromotionalBalanceCurrent, int accountPromotionalBalancePrevious,
			int accountNonPromotionalBalanceCurrent, int accountNonPromotionalBalancePrevious) {
		super();
		this.holderId = holderId;
                this.currentTimeMilli = currentTimeMilli;
                this.previousTimeMilli = previousTimeMilli;
		this.accountBalanceCurrent = accountBalanceCurrent;
		this.accountBalancePrevious = accountBalancePrevious;
		this.accountPromotionalBalanceCurrent = accountPromotionalBalanceCurrent;
		this.accountPromotionalBalancePrevious = accountPromotionalBalancePrevious;
		this.accountNonPromotionalBalanceCurrent = accountNonPromotionalBalanceCurrent;
		this.accountNonPromotionalBalancePrevious = accountNonPromotionalBalancePrevious;
	}

	public WalletStatusInfo() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "WalletStatusInfo [holderId=" + holderId + ", accountBalanceCurrent=" + accountBalanceCurrent
				+ ", accountBalancePrevious=" + accountBalancePrevious
				+ ", accountPromotionalBalanceCurrent=" + accountPromotionalBalanceCurrent
				+ ", accountPromotionalBalancePrevious=" + accountPromotionalBalancePrevious
				+ ", accountNonPromotionalBalanceCurrent=" + accountNonPromotionalBalanceCurrent
				+ ", accountNonPromotionalBalancePrevious=" + accountNonPromotionalBalancePrevious 
                                + ", currentTimeMilli=" + currentTimeMilli
                                + ", previousTimeMilli=" + previousTimeMilli
                                + "]";
	}

}
