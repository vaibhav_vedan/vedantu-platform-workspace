package com.vedantu.dinero.pojo;

import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class InstalmentUpdateReq extends AbstractFrontEndReq {

    private List<String> removedInstalments;
    private List<Instalment> addUpdateInstalments;
    private String contextId;
    private String orderId;
    private InstalmentPurchaseEntity contextType;
    private Long userId;
    private String couponCode;
    private boolean checkValidity;

    public InstalmentPurchaseEntity getContextType() {
        return contextType;
    }

    public void setContextType(InstalmentPurchaseEntity contextType) {
        this.contextType = contextType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userID) {
        userId = userID;
    }

    public List<String> getRemovedInstalments() {
        return removedInstalments;
    }

    public void setRemovedInstalments(List<String> removedInstalments) {
        this.removedInstalments = removedInstalments;
    }

    public List<Instalment> getAddUpdateInstalments() {
        return addUpdateInstalments;
    }

    public void setAddUpdateInstalments(List<Instalment> addUpdateInstalments) {
        this.addUpdateInstalments = addUpdateInstalments;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(String couponCode) {
        this.couponCode = couponCode;
    }

    public boolean isCheckValidity() {
        return checkValidity;
    }

    public void setCheckValidity(boolean checkValidity) {
        this.checkValidity = checkValidity;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (contextId == null && orderId == null) {
            errors.add("No entity or order selected");
        }
        if (contextType == null) {
            errors.add("No entity type selected");
        }
        if (userId == null) {
            errors.add("No user selected");
        }

        return errors;
    }

}
