package com.vedantu.dinero.request;

import java.util.List;

import com.vedantu.dinero.pojo.ApprovePrice;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ApprovePricingChangeRequest extends AbstractFrontEndReq {

    private Long teacherId;
    private List<ApprovePrice> approvePrices;

    public ApprovePricingChangeRequest() {
        super();
        // TODO Auto-generated constructor stub
    }

    public ApprovePricingChangeRequest(Long teacherId, List<ApprovePrice> approvePrices) {
        super();
        this.teacherId = teacherId;
        this.approvePrices = approvePrices;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public List<ApprovePrice> getApprovePrices() {
        return approvePrices;
    }

    public void setApprovePrices(List<ApprovePrice> approvePrices) {
        this.approvePrices = approvePrices;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == teacherId) {
            errors.add("teacherId");
        }

        if (null == approvePrices || approvePrices.size() == 0) {
            errors.add("approvePrices");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "ApprovePricingChangeRequest{" + "teacherId=" + teacherId + ", approvePrices=" + approvePrices + "}" + super.toString();
    }

}
