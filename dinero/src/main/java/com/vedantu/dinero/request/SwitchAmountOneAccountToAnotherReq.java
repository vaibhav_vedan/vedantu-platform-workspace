package com.vedantu.dinero.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

/**
 *
 * @author mnpk
 */

public class SwitchAmountOneAccountToAnotherReq extends AbstractFrontEndReq {
    public String fromUserId;
    public String toUserId;
    public Integer amount;

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(fromUserId)) {
            errors.add("fromUserId not found");
        }
        if (StringUtils.isEmpty(toUserId)) {
            errors.add("toUserId not found");
        }
        if(amount == null || amount <=0){
            errors.add("amount should be greater than zero");
        }
        return errors;
    }
}
