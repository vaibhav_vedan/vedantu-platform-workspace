package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.HolderType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class DeductAccountBalanceRequest extends AbstractFrontEndReq {

	private String userId;
	private HolderType holderType;
	private int verificationCode;
	private String verificationId;
	private int amountToAdd;
	private String reasonNote;
	private int promotionalAmount;
	private int nonPromotionalAmount;

	public DeductAccountBalanceRequest(String userId, HolderType holderType, int verificationCode, String verificationId,
			int amountToAdd, String reasonNote, int promotionalAmount, int nonPromotionalAmount) {
		super();
		this.userId = userId;
		this.holderType = holderType;
		this.verificationCode = verificationCode;
		this.verificationId = verificationId;
		this.amountToAdd = amountToAdd;
		this.reasonNote = reasonNote;
		this.promotionalAmount = promotionalAmount;
		this.nonPromotionalAmount = nonPromotionalAmount;
	}

	public int getPromotionalAmount() {
		return promotionalAmount;
	}

	public void setPromotionalAmount(int promotionalAmount) {
		this.promotionalAmount = promotionalAmount;
	}

	public int getNonPromotionalAmount() {
		return nonPromotionalAmount;
	}

	public void setNonPromotionalAmount(int nonPromotionalAmount) {
		this.nonPromotionalAmount = nonPromotionalAmount;
	}

	public String getReasonNote() {
		return reasonNote;
	}

	public void setReasonNote(String reasonNote) {
		this.reasonNote = reasonNote;
	}


	public int getAmountToAdd() {
		return amountToAdd;
	}

	public void setAmountToAdd(int amountToAdd) {
		this.amountToAdd = amountToAdd;
	}

	public String getVerificationId() {
		return verificationId;
	}

	public void setVerificationId(String verificationId) {
		this.verificationId = verificationId;
	}

	@Override
	public String toString() {
		return "DeductAccountBalanceReq [userId=" + userId + ", holderType=" + holderType + ", verificationCode="
				+ verificationCode + ", verificationId=" + verificationId + ", amountToAdd=" + amountToAdd
				+ ", reasonNote=" + reasonNote + ", promotionalAmount=" + promotionalAmount + ", nonPromotionalAmount="
				+ nonPromotionalAmount + "]";
	}

	public DeductAccountBalanceRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	public HolderType getHolderType() {
		return holderType;
	}

	public void setHolderType(HolderType holderType) {
		this.holderType = holderType;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(int verificationCode) {
		this.verificationCode = verificationCode;
	}

}
