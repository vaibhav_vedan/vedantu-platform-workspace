package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

/**
 * @author MNPK
 */

@Data
public class ExportOrdersCsvReq extends AbstractFrontEndReq {
    private Long startTime;
    private Long endTime;
    private PaymentStatus paymentStatus;
    private Integer minimumAmount;
    private Integer maximumAmount;


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        Long ONE_MONTH_IN_MILLIS = 2592000000L;
        if(startTime == null || endTime == null){
            errors.add("startTime and endTime mandatory");
        }
        else if (startTime > endTime) {
            errors.add("end date time should be greate than the start date time");
        }
        else if((endTime - startTime) > ONE_MONTH_IN_MILLIS){
            errors.add("startTime and endTime range should be in 30 Days");
        }
        if (minimumAmount != null && maximumAmount != null && maximumAmount < minimumAmount) {
            errors.add("maximum amount should be greater than the minimum amount");
        }
        return errors;
    }

}
