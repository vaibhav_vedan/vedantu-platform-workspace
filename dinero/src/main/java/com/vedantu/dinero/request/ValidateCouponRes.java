package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.CouponType;
import com.vedantu.dinero.enums.RedeemType;
import com.vedantu.dinero.pojo.CouponTarget;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

public class ValidateCouponRes extends AbstractRes {

    private Boolean applicable;
    private RedeemType redeemType;
    private Float redeemValue;
    private Integer discount;
    private Integer maxDiscount;
    private String couponId;
    private CouponType type;
    private List<CouponTarget> targets;
    private Long targetUserId;
    private Long passDurationInMillis;

    public ValidateCouponRes() {
        super();
    }

    public ValidateCouponRes(Boolean applicable, RedeemType redeemType,
            Float redeemValue, Integer discount, Integer maxDiscount,
            String couponId, List<CouponTarget> targets, Long targetUserId, CouponType couponType, Long passDurationInMillis) {
        this.applicable = applicable;
        this.redeemType = redeemType;
        this.redeemValue = redeemValue;
        this.discount = discount;
        this.maxDiscount = maxDiscount;
        this.couponId = couponId;
        this.targets = targets;
        this.targetUserId = targetUserId;
        this.type = couponType;
        this.passDurationInMillis = passDurationInMillis;
    }

    public RedeemType getRedeemType() {
        return redeemType;
    }

    public Integer getDiscount() {
        return discount;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public Integer getMaxDiscount() {
        return maxDiscount;
    }

    public void setMaxDiscount(Integer maxDiscount) {
        this.maxDiscount = maxDiscount;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    public Long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(Long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public Boolean getApplicable() {
        return applicable;
    }

    public void setApplicable(Boolean applicable) {
        this.applicable = applicable;
    }

    public void setRedeemType(RedeemType redeemType) {
        this.redeemType = redeemType;
    }

    public void setCouponType(CouponType type) {
        this.type = type;
    }

    public CouponType getCouponType() {
        return this.type;
    }

    public List<CouponTarget> getTargets() {
        return targets;
    }

    public void setTargets(List<CouponTarget> targets) {
        this.targets = targets;
    }

    public CouponType getType() {
        return type;
    }

    public void setType(CouponType type) {
        this.type = type;
    }

    public Long getPassDurationInMillis() {
        return passDurationInMillis;
    }

    public void setPassDurationInMillis(Long passDurationInMillis) {
        this.passDurationInMillis = passDurationInMillis;
    }

    public Float getRedeemValue() {
        return redeemValue;
    }

    public void setRedeemValue(Float redeemValue) {
        this.redeemValue = redeemValue;
    }

}
