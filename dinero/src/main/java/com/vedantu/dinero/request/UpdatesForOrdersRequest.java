package com.vedantu.dinero.request;

import com.vedantu.dinero.controllers.PaymentController;
import com.vedantu.dinero.enums.Centre;
import com.vedantu.dinero.enums.EnrollmentType;
import com.vedantu.dinero.enums.FintechRisk;
import com.vedantu.dinero.enums.PaymentThrough;
import com.vedantu.dinero.enums.TeamType;
import com.vedantu.dinero.pojo.UpdatesForOrdersRequestPOJO;
import com.vedantu.util.LogFactory;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdatesForOrdersRequest extends AbstractFrontEndReq {

    
    private List<UpdatesForOrdersRequestPOJO> updateOrders;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (updateOrders==null || updateOrders.isEmpty()) {
            errors.add("orders can't be null or empty");
        }
        else {

            Set<PaymentThrough> paymentThroughSet = UpdatesForOrdersRequestPOJO.getPaymentThroughSet();
            Set<TeamType> teamTypeSet = UpdatesForOrdersRequestPOJO.getTeamTypeSet();
            Set<Centre> centreSet = UpdatesForOrdersRequestPOJO.getCentreSet();
            Set<EnrollmentType> enrollmentTypeSet = UpdatesForOrdersRequestPOJO.getEnrollmentTypeSet();
            Set<FintechRisk> fintechRiskSet = UpdatesForOrdersRequestPOJO.getFintechRiskSet();
            updateOrders.forEach(order -> {
                if (!(order.getPaymentThrough() != null && paymentThroughSet.contains(order.getPaymentThrough()))) {
                    errors.add("`paymentThrough` entry is "+ order.getPaymentThrough() +" and is not valid for order id "+order.getId());
                }
                if (!(order.getTeamType() != null && teamTypeSet.contains(order.getTeamType()))) {
                    errors.add("`teamType` entry is "+ order.getTeamType()  +" and is not valid for order id "+order.getId());
                }
                if (!(order.getCentre() != null && centreSet.contains(order.getCentre()))) {
                    errors.add("`centre` entry is "+ order.getCentre()  +" and is not valid for order id "+order.getId());
                }
                if (!(order.getEnrollmentType() != null && enrollmentTypeSet.contains(order.getEnrollmentType()))) {
                    errors.add("`enrollmentType` entry is "+ order.getEnrollmentType() +" and is not valid for order id "+order.getId());
                }
                if (!(order.getFintechRisk() != null && fintechRiskSet.contains(order.getFintechRisk()))) {
                    errors.add("`fintechRisk` entry is "+ order.getFintechRisk() +" and is not valid for order id "+order.getId());
                }
            });
        }
        return errors;
    }
}
