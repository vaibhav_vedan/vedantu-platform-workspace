package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.payment.WalletPaymentMethod;
import lombok.Data;

@Data
public class WalletRequest {
    private String otp;
    private String walletId;
    private PaymentGatewayName gateway = PaymentGatewayName.JUSPAY;
    private WalletPaymentMethod wallet;
    private String mobileNo;
}
