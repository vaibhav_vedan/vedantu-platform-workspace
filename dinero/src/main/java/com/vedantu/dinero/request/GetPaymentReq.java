package com.vedantu.dinero.request;

public class GetPaymentReq extends GetAbstractReq {
	
	private Long studentId;
	private Long teacherId;

	public GetPaymentReq() {
		super();
	}

	public GetPaymentReq(Integer start, Integer size, Long studentId, Long teacherId) {
		super(start, size);
		this.studentId = studentId;
		this.teacherId = teacherId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
}
