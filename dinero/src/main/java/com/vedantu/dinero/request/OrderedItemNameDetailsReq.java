package com.vedantu.dinero.request;

import com.vedantu.dinero.pojo.OrderedItemNames;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @author MNPK
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderedItemNameDetailsReq extends AbstractFrontEndReq {
    private List<OrderedItemNames> orderedItemNames;
}
