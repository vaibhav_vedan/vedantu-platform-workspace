package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.EmiCardType;
import com.vedantu.dinero.enums.EmiType;
import com.vedantu.dinero.pojo.EmiTenure;
import lombok.Data;

import java.util.List;

@Data
public class EmiCardRequest {
    private String id;
    private String name;
    private String code;
    private EmiCardType cardType;
    private EmiType emiType;
    private List<EmiTenure> emiTenures;
    private Integer minimumAmount;
    private Boolean isActive;
}
