/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request.Instalment;

import java.util.List;

import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author ajith
 */
public class GetInstalmentsReq extends AbstractFrontEndReq {

    private Long subscriptionId;
    private String batchId;//only for OTF
    private Long userId;//only for OTF
    private String orderId;
    private PaymentStatus paymentStatus;
    private String coursePlanId; //For coursePlan only
    private List<String> contextIds;
    private List<String> deliverableIds;

    public GetInstalmentsReq() {
        super();
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public List<String> getContextIds() {
        return contextIds;
    }

    public void setContextIds(List<String> contextIds) {
        this.contextIds = contextIds;
    }

    public List<String> getDeliverableIds() {
        return deliverableIds;
    }

    public void setDeliverableIds(List<String> deliverableIds) {
        this.deliverableIds = deliverableIds;
    }

}
