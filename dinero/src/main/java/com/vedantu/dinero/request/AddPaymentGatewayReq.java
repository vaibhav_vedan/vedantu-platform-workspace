package com.vedantu.dinero.request;

import java.util.List;

import com.vedantu.User.Role;
import com.vedantu.dinero.entity.PaymentGateway;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddPaymentGatewayReq extends AbstractFrontEndReq {

	private String name;
	private Boolean enabled;
	private Integer percentageTraffic;

	public AddPaymentGatewayReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AddPaymentGatewayReq(String name, Boolean enabled, Integer percentageTraffic) {
		super();
		this.name = name;
		this.enabled = enabled;
		this.percentageTraffic = percentageTraffic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getPercentageTraffic() {
		return percentageTraffic;
	}

	public void setPercentageTraffic(Integer percentageTraffic) {
		this.percentageTraffic = percentageTraffic;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (name == null) {
			errors.add(PaymentGateway.Constants.NAME);
		}

		if (enabled == null) {
			errors.add(PaymentGateway.Constants.ENABLED);
		}
		if (enabled == true && percentageTraffic == null) {
			errors.add(PaymentGateway.Constants.PERCENTAGE_TRAFFIC);
		}
		return errors;
	}

	@Override
	public String toString() {
		return "AddPaymentGatewayReq [name=" + name + ", enabled=" + enabled + ", percentageTraffic="
				+ percentageTraffic + "]";
	}

	public PaymentGateway toPaymentGateway() {
		if (this.percentageTraffic == null) {
			this.percentageTraffic = 0;
		}
		return new PaymentGateway(PaymentGatewayName.valueOf(name), enabled, this.percentageTraffic);
	}
}
