package com.vedantu.dinero.request;

import java.util.List;

import com.vedantu.dinero.entity.PaymentGateway;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ModifyPaymentGatewayReq extends AbstractFrontEndReq {

	private String id;
	private String name;
	private Boolean enabled;
	private Integer percentageTraffic;

	public ModifyPaymentGatewayReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ModifyPaymentGatewayReq(String id, String name, Boolean enabled, Integer percentageTraffic) {
		super();
		this.id = id;
		this.name = name;
		this.enabled = enabled;
		this.percentageTraffic = percentageTraffic;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getPercentageTraffic() {
		return percentageTraffic;
	}

	public void setPercentageTraffic(Integer percentageTraffic) {
		this.percentageTraffic = percentageTraffic;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();

		if (id == null) {
			errors.add(PaymentGateway.Constants.ID);
		}
		if (name == null) {
			errors.add(PaymentGateway.Constants.NAME);
		}

		if (enabled == null) {
			errors.add(PaymentGateway.Constants.ENABLED);
		}
		if (enabled == true && percentageTraffic == null) {
			errors.add(PaymentGateway.Constants.PERCENTAGE_TRAFFIC);
		}
		return errors;
	}

	@Override
	public String toString() {
		return "ModifyPaymentGatewayReq [id=" + id + ", name=" + name + ", enabled=" + enabled + ", percentageTraffic="
				+ percentageTraffic + "]";
	}

	public PaymentGateway toPaymentGateway() {
		if (this.percentageTraffic == null) {
			this.percentageTraffic = 0;
		}
		PaymentGateway paymentGateway = new PaymentGateway(PaymentGatewayName.valueOf(name), enabled,
				this.percentageTraffic);
		paymentGateway.setId(this.id);
		return paymentGateway;
	}

}
