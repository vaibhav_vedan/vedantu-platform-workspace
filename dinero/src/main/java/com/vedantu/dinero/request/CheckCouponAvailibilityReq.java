package com.vedantu.dinero.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

public class CheckCouponAvailibilityReq extends AbstractFrontEndReq {

    private String code;

    public CheckCouponAvailibilityReq() {
        super();
        // TODO Auto-generated constructor stub
    }

    public CheckCouponAvailibilityReq(String code) {
        super();
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(code)) {
            errors.add("code");
        }
        return errors;
    }

    @Override
    public String toString() {
        return "CheckCouponAvailibilityReq [code=" + code + "]";
    }

}
