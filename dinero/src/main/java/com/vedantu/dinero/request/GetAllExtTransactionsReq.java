package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class GetAllExtTransactionsReq extends AbstractFrontEndReq {

    private Long userId;
    public Long tillTime;
    public Long fromTime;
    public int start;
    public int limit;
    private TransactionStatus paymentStatus;
    private PaymentGatewayName paymentGatewayName;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getTillTime() {
        return tillTime;
    }

    public void setTillTime(Long tillTime) {
        this.tillTime = tillTime;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public TransactionStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(TransactionStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public PaymentGatewayName getPaymentGatewayName() {
        return paymentGatewayName;
    }

    public void setPaymentGatewayName(PaymentGatewayName paymentGatewayName) {
        this.paymentGatewayName = paymentGatewayName;
    }
}
