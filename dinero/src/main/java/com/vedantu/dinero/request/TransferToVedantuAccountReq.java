package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.VedantuAccount;

public class TransferToVedantuAccountReq extends RechargeFreebiesAccountReq {

    private String reasonNote;
    private String reasonNoteType;
    private int promotionalAmount;
    private int nonPromotionalAmount;
    private VedantuAccount vedantuAccount;


    public TransferToVedantuAccountReq() {
        super();
        // TODO Auto-generated constructor stub
    }    

    public int getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(int promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public int getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(int nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public VedantuAccount getVedantuAccount() {
        return vedantuAccount;
    }

    public void setVedantuAccount(VedantuAccount vedantuAccount) {
        this.vedantuAccount = vedantuAccount;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public String getReasonNoteType() {
        return reasonNoteType;
    }

    public void setReasonNoteType(String reasonNoteType) {
        this.reasonNoteType = reasonNoteType;
    }
    

}
