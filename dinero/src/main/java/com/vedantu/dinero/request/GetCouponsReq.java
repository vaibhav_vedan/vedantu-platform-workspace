package com.vedantu.dinero.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetCouponsReq extends AbstractFrontEndListReq {

    // if code is provided then it will return coupon with this code
    private String code;
    private String target;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

}
