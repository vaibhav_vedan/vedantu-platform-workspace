package com.vedantu.dinero.request;

import java.util.List;

import com.vedantu.dinero.pojo.Slab;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class CreateSlabRangesRequest extends AbstractFrontEndReq{
	
	private List<Slab> slabs;

	public CreateSlabRangesRequest(List<Slab> slabs) {
		super();
		this.slabs = slabs;
	}

	public List<Slab> getSlabs() {
		return slabs;
	}

	public void setSlabs(List<Slab> slabs) {
		this.slabs = slabs;
	}

	public CreateSlabRangesRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "CreateSlabRangesRequest [slabs=" + slabs + "]";
	}
	
}
