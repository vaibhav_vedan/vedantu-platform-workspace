package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentMethod;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import org.apache.commons.lang.StringUtils;

import java.util.List;

@Data
public class GetEmiCardReq extends AbstractFrontEndReq {
    private Long amount;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (amount == null || amount == 0) {
            errors.add(" order value ");
        }

        return errors;
    }

}
