package com.vedantu.dinero.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetMyOrdersReq extends AbstractFrontEndListReq {

	public GetMyOrdersReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetMyOrdersReq(int start, int limit) {
		super(start, limit);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetMyOrdersReq []";
	}

}
