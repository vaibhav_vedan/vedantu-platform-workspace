package com.vedantu.dinero.request;

import java.util.ArrayList;

public class BulkTextSMSRequest {
	
	private ArrayList<String> to;
	private String body;

	
	public BulkTextSMSRequest() {
		super();
	}
	
	public BulkTextSMSRequest(ArrayList<String> to, String body) {
		super();
		this.to = to;
		this.body = body;
	}
	
	public ArrayList<String> getTo() {
		return to;
	}
	public void setTo(ArrayList<String> to) {
		this.to = to;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}


	@Override
	public String toString() {
		return "BulkTextSms to:" + to.toString() + ", body:" + body;
	}
	

}
