package com.vedantu.dinero.request;


import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetTransactionsReq extends AbstractFrontEndReq {

	private Long userId;
	private String reason;
	private int start;
	private int limit;
	private Long fromTime;
	private Long tillTime;
	private Integer minAmountForFilter;
	private Integer maxAmountForFilter;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public Long getFromTime() {
		return fromTime;
	}

	public void setFromTime(Long fromTime) {
		this.fromTime = fromTime;
	}

	public Long getTillTime() {
		return tillTime;
	}

	public void setTillTime(Long tillTime) {
		this.tillTime = tillTime;
	}

	public GetTransactionsReq(Long userId, String reason, int start, int limit, Long fromTime, Long tillTime) {
		super();
		this.userId = userId;
		this.reason = reason;
		this.start = start;
		this.limit = limit;
		this.fromTime = fromTime;
		this.tillTime = tillTime;
	}

	public Integer getMinAmountForFilter() {
		return minAmountForFilter;
	}

	public void setMinAmountForFilter(Integer minAmountForFilter) {
		this.minAmountForFilter = minAmountForFilter;
	}

	public Integer getMaxAmountForFilter() {
		return maxAmountForFilter;
	}

	public void setMaxAmountForFilter(Integer maxAmountForFilter) {
		this.maxAmountForFilter = maxAmountForFilter;
	}

	public GetTransactionsReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetTransactionsReq [userId=" + userId + ", reason=" + reason + ", start=" + start + ", limit=" + limit
				+ ", fromTime=" + fromTime + ", tillTime=" + tillTime + "]";
	}

}
