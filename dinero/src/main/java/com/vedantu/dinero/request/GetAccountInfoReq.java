package com.vedantu.dinero.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;


public class GetAccountInfoReq extends AbstractFrontEndReq {

	public GetAccountInfoReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetAccountInfoReq(Long userId) {
		super();
		this.userId = userId;
	}

	private Long userId;

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}
	
        @Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();

		if (null == userId) {
			errors.add("userId");
		}
		return errors;
	}
}
