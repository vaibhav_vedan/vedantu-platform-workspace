/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.Instalment.InstalmentChargeType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.RedeemType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class SetInstalmentConfigPropsReq extends AbstractFrontEndReq {

    private RedeemType chargeType;
    private Integer chargeValue = 0;//in paisa
    private InstalmentPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private InstalmentChargeType instalmentChargeType;//required

    public SetInstalmentConfigPropsReq() {
    }

    public RedeemType getChargeType() {
        return chargeType;
    }

    public void setChargeType(RedeemType chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getChargeValue() {
        return chargeValue;
    }

    public void setChargeValue(Integer chargeValue) {
        this.chargeValue = chargeValue;
    }

    public InstalmentPurchaseEntity getPurchaseEntityType() {
        return purchaseEntityType;
    }

    public void setPurchaseEntityType(InstalmentPurchaseEntity purchaseEntityType) {
        this.purchaseEntityType = purchaseEntityType;
    }

    public String getPurchaseEntityId() {
        return purchaseEntityId;
    }

    public void setPurchaseEntityId(String purchaseEntityId) {
        this.purchaseEntityId = purchaseEntityId;
    }

    public InstalmentChargeType getInstalmentChargeType() {
        return instalmentChargeType;
    }

    public void setInstalmentChargeType(InstalmentChargeType instalmentChargeType) {
        this.instalmentChargeType = instalmentChargeType;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (instalmentChargeType == null) {
            errors.add("instalmentChargeType");
        }

        if (chargeValue > 0 && chargeType == null) {
            errors.add("chargeType");
        }
        return errors;
    }

}
