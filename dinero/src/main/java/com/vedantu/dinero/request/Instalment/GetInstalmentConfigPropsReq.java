/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request.Instalment;

import com.vedantu.dinero.enums.Instalment.InstalmentChargeType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetInstalmentConfigPropsReq extends AbstractFrontEndReq {

    private InstalmentPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private InstalmentChargeType instalmentChargeType;

    public GetInstalmentConfigPropsReq() {
    }

    public InstalmentPurchaseEntity getPurchaseEntityType() {
        return purchaseEntityType;
    }

    public void setPurchaseEntityType(InstalmentPurchaseEntity purchaseEntityType) {
        this.purchaseEntityType = purchaseEntityType;
    }

    public String getPurchaseEntityId() {
        return purchaseEntityId;
    }

    public void setPurchaseEntityId(String purchaseEntityId) {
        this.purchaseEntityId = purchaseEntityId;
    }

    public InstalmentChargeType getInstalmentChargeType() {
        return instalmentChargeType;
    }

    public void setInstalmentChargeType(InstalmentChargeType instalmentChargeType) {
        this.instalmentChargeType = instalmentChargeType;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        return errors;
    }
}
