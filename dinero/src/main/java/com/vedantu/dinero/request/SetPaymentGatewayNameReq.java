package com.vedantu.dinero.request;

import java.util.List;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SetPaymentGatewayNameReq extends AbstractFrontEndReq{
	private String userId;
	private PaymentGatewayName paymentGatewayName;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public PaymentGatewayName getPaymentGatewayName() {
		return paymentGatewayName;
	}

	public void setPaymentGatewayName(PaymentGatewayName paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		// TODO Auto-generated method stub
		 List<String> errors = super.collectVerificationErrors();
		
		 if (StringUtils.isEmpty(userId) || userId == null) {
             errors.add("userId not found");
         }
		 if (paymentGatewayName == null) {
             errors.add("paymentGatewayName not found");
         }	 
		return errors;
	}
}
