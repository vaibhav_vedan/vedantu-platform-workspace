package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentMethod;
import com.vedantu.onetofew.enums.CourseTerm;
import lombok.Data;

import java.util.List;

@Data
public class PaymentOfferReq {
    private PaymentMethod paymentMethod;
    private String serviceProvider;
    private String campaignName;
    private PaymentGatewayName gateway;
    private long offerStartTime;
    private long offerEndTime;
    private List<CourseTerm> courseType;
    private String message;
}
