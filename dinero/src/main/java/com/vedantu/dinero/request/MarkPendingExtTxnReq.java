/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author parashar
 */
public class MarkPendingExtTxnReq extends AbstractFrontEndReq {

    private String extId;
    private ExtTransaction.MarkSuccessType markSuccessType;
    private String markSuccessReason;

    /**
     * @return the extId
     */
    public String getExtId() {
        return extId;
    }

    /**
     * @param extId the extId to set
     */
    public void setExtId(String extId) {
        this.extId = extId;
    }

    public ExtTransaction.MarkSuccessType getMarkSuccessType() {
        return markSuccessType;
    }

    public void setMarkSuccessType(ExtTransaction.MarkSuccessType markSuccessType) {
        this.markSuccessType = markSuccessType;
    }

    public String getMarkSuccessReason() {
        return markSuccessReason;
    }

    public void setMarkSuccessReason(String markSuccessReason) {
        this.markSuccessReason = markSuccessReason;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (markSuccessType == null) {
            errors.add("Empty Reason not allowed");
        }

        if (StringUtils.isEmpty(extId)) {
            errors.add("Empty Ext Transaction Id not allowed");
        }

        return errors;

    }

}
