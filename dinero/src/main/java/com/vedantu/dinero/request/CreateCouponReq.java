package com.vedantu.dinero.request;

import java.util.List;

import com.vedantu.dinero.entity.Coupon;
import com.vedantu.dinero.enums.CouponType;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.RedeemType;
import com.vedantu.dinero.pojo.CouponTarget;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

import javax.validation.constraints.Size;
public class CreateCouponReq extends AbstractFrontEndReq {

    // comma(,) separated values in case of multiple coupons of same type need
    // to be created
    private String code;
    private CouponType type;
    // all value are in INR
    private RedeemType redeemType;

    // actual amount redeemed (paise in case of monetry term)
    private Float redeemValue;

    // optional --> minOrderValue (in paisa) above which this coupon code is
    // valid
    private Integer minOrderValue;

    private Integer maxOrderValue;

    // max amount (in paisa) that can be redeemed using this coupon --> null or
    // 0 means no
    // limit
    private Integer maxRedeemValue;

    // null or 0 means no start time
    private Long validFrom;

    // null or 0 means no end time
    private Long validTill;

    private List<String> terms;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String description;

    // null or 0 means unlimited
    private Integer usesCount;

    // null or 0 means unlimited
    private Integer usesCountPerUser;

    private Long targetUserId;

    private List<CouponTarget> targets;

    private boolean edit;

    private Long passDurationInMillis;

    private boolean satisfyAtleast1Condition = false;
    
    private PaymentType forPaymentType = PaymentType.BULK;

    private List<Integer> targetOrder;

    public CreateCouponReq() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CouponType getType() {
        return type;
    }

    public void setType(CouponType type) {
        this.type = type;
    }

    public RedeemType getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(RedeemType redeemType) {
        this.redeemType = redeemType;
    }

    public Float getRedeemValue() {
        return redeemValue;
    }

    public void setRedeemValue(Float redeemValue) {
        this.redeemValue = redeemValue;
    }

    public Integer getMinOrderValue() {
        return minOrderValue;
    }

    public void setMinOrderValue(Integer minOrderValue) {
        this.minOrderValue = minOrderValue;
    }

    public Integer getMaxRedeemValue() {
        return maxRedeemValue;
    }

    public void setMaxRedeemValue(Integer maxRedeemValue) {
        this.maxRedeemValue = maxRedeemValue;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTill() {
        return validTill;
    }

    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUsesCount() {
        return usesCount;
    }

    public void setUsesCount(Integer usesCount) {
        this.usesCount = usesCount;
    }

    public Integer getUsesCountPerUser() {
        return usesCountPerUser;
    }

    public void setUsesCountPerUser(Integer usesCountPerUser) {
        this.usesCountPerUser = usesCountPerUser;
    }

    public Long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(Long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public List<CouponTarget> getTargets() {
        return targets;
    }

    public void setTargets(List<CouponTarget> targets) {
        this.targets = targets;
    }

    public boolean isEdit() {
        return edit;
    }

    public void setEdit(boolean edit) {
        this.edit = edit;
    }

    public Long getPassDurationInMillis() {
        return passDurationInMillis;
    }

    public void setPassDurationInMillis(Long passDurationInMillis) {
        this.passDurationInMillis = passDurationInMillis;
    }

    public boolean isSatisfyAtleast1Condition() {
        return satisfyAtleast1Condition;
    }

    public void setSatisfyAtleast1Condition(boolean satisfyAtleast1Condition) {
        this.satisfyAtleast1Condition = satisfyAtleast1Condition;
    }

    public PaymentType getForPaymentType() {
        return forPaymentType;
    }

    public void setForPaymentType(PaymentType forPaymentType) {
        this.forPaymentType = forPaymentType;
    }

    public List<Integer> getTargetOrder() {
        return targetOrder;
    }

    public void setTargetOrder(List<Integer> targetOrder) {
        this.targetOrder = targetOrder;
    }

    public Integer getMaxOrderValue() {
        return maxOrderValue;
    }

    public void setMaxOrderValue(Integer maxOrderValue) {
        this.maxOrderValue = maxOrderValue;
    }

    public Coupon toCoupon(String code) {
        Coupon coupon = new Coupon(code, getCallingUserId(), type, redeemType, redeemValue, minOrderValue,
                maxRedeemValue, validFrom, validTill, terms, description, usesCount, usesCountPerUser,
                targetUserId, targets, satisfyAtleast1Condition, forPaymentType);
        coupon.setPassDurationInMillis(passDurationInMillis);
        coupon.setTargetOrder(targetOrder);
        coupon.setMaxOrderValue(maxOrderValue);
        return coupon;

    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(code)) {
            errors.add("code");
        }
        if (type == null) {
            errors.add("type");
        }
        if (redeemType == null) {
            errors.add(Coupon.Constants.REDEEM_TYPE);
        }
        if (!CouponType.PASS.equals(type) && (redeemValue == null || redeemValue < 1)) {
            errors.add(Coupon.Constants.REDEEM_VALUE);
        }

        if (maxOrderValue != null && maxOrderValue > 0 && minOrderValue != null &&
                minOrderValue > 0 && minOrderValue > maxOrderValue) {
            errors.add("max_order_val_greater_than_min_order_val");
        }
        return errors;
    }

}
