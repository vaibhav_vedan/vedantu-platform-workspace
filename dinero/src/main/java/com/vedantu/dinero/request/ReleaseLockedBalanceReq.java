package com.vedantu.dinero.request;

public class ReleaseLockedBalanceReq extends GetAccountInfoReq {

	private int amount;

	public ReleaseLockedBalanceReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReleaseLockedBalanceReq(Long userId) {
		super(userId);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "ReleaseLockedBalanceReq [amount=" + amount + "]";
	}

	public ReleaseLockedBalanceReq(int amount) {
		super();
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
