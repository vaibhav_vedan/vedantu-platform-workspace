/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request.Instalment;

import com.vedantu.dinero.entity.Instalment.Instalment;
import java.util.List;

/**
 *
 * @author ajith
 */
public class SendInstalmentPaidEmailReq {
    private Long subscriptionId;    //null for OTF
    private String subscriptionTitle; //Course title for OTF
    private Long teacherId;
    private Long studentId;
    private String subscriptionLink; //only for OTF
    private List<Instalment> instalments;

    public SendInstalmentPaidEmailReq() {
        super();
    }    
    
    public SendInstalmentPaidEmailReq(Long subscriptionId, String subscriptionTitle, Long teacherId, Long studentId, List<Instalment> instalments) {
        this.subscriptionId = subscriptionId;
        this.subscriptionTitle = subscriptionTitle;
        this.teacherId = teacherId;
        this.studentId = studentId;
        this.instalments = instalments;
    }

    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getSubscriptionTitle() {
        return subscriptionTitle;
    }

    public void setSubscriptionTitle(String subscriptionTitle) {
        this.subscriptionTitle = subscriptionTitle;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public List<Instalment> getInstalments() {
        return instalments;
    }

    public void setInstalments(List<Instalment> instalments) {
        this.instalments = instalments;
    }

    public String getSubscriptionLink() {
        return subscriptionLink;
    }

    public void setSubscriptionLink(String subscriptionLink) {
        this.subscriptionLink = subscriptionLink;
    }

    @Override
    public String toString() {
        return "SendInstalmentPaidEmailReq{" +
                "subscriptionId=" + subscriptionId +
                ", subscriptionTitle='" + subscriptionTitle + '\'' +
                ", teacherId=" + teacherId +
                ", studentId=" + studentId +
                ", subscriptionLink='" + subscriptionLink + '\'' +
                ", instalments=" + instalments +
                '}';
    }
}
