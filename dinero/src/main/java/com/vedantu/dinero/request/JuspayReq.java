package com.vedantu.dinero.request;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public final class JuspayReq {

    @SerializedName("order_id")
    private String orderId;

    @SerializedName("merchant_id")
    private String merchantId;

    @SerializedName("payment_method_type")
    private String paymentMethodType;

    @SerializedName("payment_method")
    private String paymentMethod;

    @SerializedName("format")
    private String format;

    @SerializedName("redirect_after_payment")
    private boolean redirectAfterPayment;

    // CARD REQ

    @SerializedName("card_token")
    private String cardToken;

    @SerializedName("card_number")
    private String cardNumber;

    @SerializedName("name_on_card")
    private String nameOnCard;

    @SerializedName("card_exp_year")
    private String cardExpYear;

    @SerializedName("card_exp_month")
    private String cardExpMonth;

    @SerializedName("card_security_code")
    private String cardSecurityCode;

    @SerializedName("save_to_locker")
    private boolean saveToLocker;

    @SerializedName("is_emi")
    private boolean isEmi;

    @SerializedName("emi_bank")
    private boolean emiBank;

    @SerializedName("emi_tenure")
    private Integer emiTenure;

    @SerializedName("auth_type")
    private String authType;

    // WALLET DEBIT

    @SerializedName("direct_wallet_token")
    private String directWalletToken;


}
