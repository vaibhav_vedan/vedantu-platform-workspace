/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.request;

import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class RefundMoneyToStudentWalletReq extends AbstractFrontEndReq {

    private String reasonNote;
    private int promotionalAmount;
    private int nonPromotionalAmount;
    private Long userId;
    private int amount;
    private DeliverableEntityType contextType;
    private String contextId;
    private Long approvedBy;

    public RefundMoneyToStudentWalletReq() {
        super();
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public int getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(int promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public int getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(int nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public DeliverableEntityType getContextType() {
        return contextType;
    }

    public void setContextType(DeliverableEntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Long getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Long approvedBy) {
        this.approvedBy = approvedBy;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (approvedBy == null) {
            errors.add("approvedBy");
        }

        if (amount <= 0) {
            errors.add("amount<=0");
        } else if (nonPromotionalAmount < 0) {
            errors.add("nonPromotionalAmount<0");
        } else if (promotionalAmount < 0) {
            errors.add("promotionalAmount<0");
        } else if (amount != (promotionalAmount + nonPromotionalAmount)) {
            errors.add("amount != promotionalAmount + nonPromotionalAmount");
        }

        if (userId == null) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(reasonNote)) {
            errors.add("reasonNote");
        }
        return errors;
    }

}
