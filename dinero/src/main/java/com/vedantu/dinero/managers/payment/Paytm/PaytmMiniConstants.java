package com.vedantu.dinero.managers.payment.Paytm;

import com.vedantu.util.ConfigUtils;

public final class PaytmMiniConstants {
    private PaytmMiniConstants() {
    }
    public static final String MID  = ConfigUtils.INSTANCE.getStringValue("paytm.mini.mid");
    public static final String MERCHANT_KEY = ConfigUtils.INSTANCE.getStringValue("paytm.mini.merchant_key");
    public static final String WEBSITE = ConfigUtils.INSTANCE.getStringValue("paytm.mini.website");
    public static final String PAYTM_TXN_STATUS_URL = ConfigUtils.INSTANCE.getStringValue("paytm.mini.txn_status_url");
    public static final String PAYTM_INIT_TXN_URL = ConfigUtils.INSTANCE.getStringValue("paytm.mini.init_txn_url");
    public static final String PAYTM_REDIRECT_URL =  ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT")+ "dinero/payment/onPaymentReceived/PAYTM_MINI";
}