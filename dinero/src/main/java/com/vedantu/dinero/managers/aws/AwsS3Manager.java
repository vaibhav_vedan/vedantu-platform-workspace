package com.vedantu.dinero.managers.aws;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@Service
public class AwsS3Manager {

	private AmazonS3Client s3Client;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(AwsS3Manager.class);

	public static final String INVOICES_BUCKET = ConfigUtils.INSTANCE.getStringValue("vedantu.invoices.pdf.bucket.name");
        private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase();

	@PostConstruct
	public void init() {

		s3Client = new AmazonS3Client();
	}

	public String getPreSignedUrl(String bucket, String key, String contentType) {
		String presignedUrl = "";

		try {
			java.util.Date expiration = new java.util.Date();
			long milliSeconds = expiration.getTime();
			milliSeconds += 1000 * 60 * 60; // Add 1 hour.
			expiration.setTime(milliSeconds);

			GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, key);
			generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
			generatePresignedUrlRequest.setExpiration(expiration);
			generatePresignedUrlRequest.setContentType(contentType);

			presignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();

			logger.info("preSignedUrl:" + presignedUrl);
		} catch (Exception exception) {
			logger.error(exception.getMessage());
		}

		return presignedUrl;
	}

	public List<String> getBucketContents(String bucket, String prefix) {
		List<String> contents = new ArrayList<>();

		try {
			s3Client.listObjects(bucket, prefix).getObjectSummaries().forEach(objectSummary -> {
				contents.add(s3Client.getUrl(bucket, objectSummary.getKey()).toString());
			});
		} catch (Exception ex) {
		}

		return contents;
	}

	public Boolean uploadFile(String bucket, String key, File contentFile) {
		Boolean isUploadContentFileSuccessfull = false;
                key = ENV+"/"+key;
		try {
			s3Client.putObject(bucket, key, contentFile);

			isUploadContentFileSuccessfull = true;
		} catch (Exception ex) {
			isUploadContentFileSuccessfull = false;
		}

		return isUploadContentFileSuccessfull;
	}
        
        public String getPublicBucketDownloadUrl(String bucket, String key) {
            return s3Client.getUrl(bucket, key).toExternalForm();
        }
}
