package com.vedantu.dinero.managers.aws;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSSession;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.vedantu.aws.AwsCloudWatchManager;
import com.vedantu.dinero.listeners.SQSListener;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;

import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableJms
public class JmsConfig {

    @Autowired
    private SQSListener sqsListener;

    private SQSConnection connection;

    private static final Logger logger = LogManager.getRootLogger();

    private Set<SQSQueue> queueListToCreateAlarm = new HashSet<>();

    @Autowired
    private AwsCloudWatchManager awsCloudWatchManager;

    @Bean
    public SQSConnection sqsConnection() throws JMSException {
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
            SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
                    .withRegion(Region.getRegion(Regions.AP_SOUTHEAST_1))
                    .withNumberOfMessagesToPrefetch(10).build();

            connection = sqsConnectionFactory.createConnection();
            Session session = connection.createSession(false, SQSSession.CLIENT_ACKNOWLEDGE);
            MessageConsumer dmlc = session.createConsumer(session.createQueue(SQSQueue.DINERO_INVOICES_QUEUE.getQueueName(env)));

            // base instalment message consumer
            MessageConsumer baseInstalmentOpsConsumer = session.createConsumer(session.createQueue(SQSQueue.BASE_INSTALMENT_OPS.getQueueName(env)));
            MessageConsumer createNewUserAccountConsumer = session.createConsumer(session.createQueue(SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE.getQueueName(env)));


            dmlc.setMessageListener(sqsListener);
            baseInstalmentOpsConsumer.setMessageListener(sqsListener);
            createNewUserAccountConsumer.setMessageListener(sqsListener);

            //Add queues to create alarm
            queueListToCreateAlarm.add(SQSQueue.DINERO_INVOICES_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.BASE_INSTALMENT_OPS);
            queueListToCreateAlarm.add(SQSQueue.BASE_INSTALMENT_OPS_DL);
            queueListToCreateAlarm.add(SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.CREATE_NEW_USER_ACCOUNT_DL_QUEUE);

            connection.start();
            awsCloudWatchManager.createAlarms(queueListToCreateAlarm);
            logger.info("JMS Bean created");

            return connection;
        } else {
            return null;
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing sqs connection ", e);
        }
    }

}
