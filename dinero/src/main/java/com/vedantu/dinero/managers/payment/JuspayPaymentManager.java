package com.vedantu.dinero.managers.payment;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.dinero.entity.EmiCard;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.UserMapping;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefSubType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.enums.juspay.JuspayGateway;
import com.vedantu.dinero.enums.payment.NetbankingPaymentMethod;
import com.vedantu.dinero.enums.payment.WalletPaymentMethod;
import com.vedantu.dinero.managers.payment.juspay.WalletPayment;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.EmiCardDAO;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.serializers.UserMappingDao;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.dinero.util.RedisDAO;
import com.vedantu.exception.*;
import com.vedantu.util.*;
import in.juspay.exception.*;
import in.juspay.model.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
@Service
public class JuspayPaymentManager implements IPaymentManager {

    private Logger logger = LogFactory.getLogger(JuspayPaymentManager.class);

    @Autowired
    public FosUtils fosUtils;
    @Autowired
    EmiCardDAO emiCardDao;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    public UserMappingDao userMappingDao;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public AccountDAO accountDAO;

    @Autowired
    private RedisDAO redisDAO;

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    private static final String PAYMENT_CHANNEL = "JUSPAY";

    private final PaymentGatewayName gatewayName = PaymentGatewayName.JUSPAY;

    private static final String MERCHANT_ID = ConfigUtils.INSTANCE.getStringValue("billing.juspay.merchant.id");
    private static final String CHARGING_URL = ConfigUtils.INSTANCE.getStringValue("billing.juspay.charging.url");
    private static final String REDIRECT_URL = ConfigUtils.INSTANCE.getStringValue("billing.juspay.redirect.url");
    private static final String API_KEY = ConfigUtils.INSTANCE.getStringValue("billing.juspay.api.key");
    private static final String RESPONSE_KEY = ConfigUtils.INSTANCE.getStringValue("billing.juspay.checksum.response.key");
    private static final String ENCRYPTION_KEY = ConfigUtils.INSTANCE.getStringValue("billing.juspay.checksum.encryption.key");
    private final Gson gson = new Gson();

    public JuspayPaymentManager() {
        super();
        JuspayEnvironment.withBaseUrl(CHARGING_URL);
        JuspayEnvironment.withApiKey(API_KEY);
        JuspayEnvironment.withMerchantId(MERCHANT_ID);
    }

    @Override
    public PaymentGatewayName getName() {
        return this.gatewayName;
    }

    @Override
    public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException {


        User user = getUser(transaction.getUserId());

        String juspayCustomerId = getJuspayCustomerId(transaction.getUserId());

        Order order = createOrder(transaction, user, paymentOption, juspayCustomerId);

        if (order == null) {
            throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY: Order Creation Failed");
        }
        Payment payment = getPayment(order, paymentOption); // order.getPaymentLinks().getWebLink();
        //noinspection unchecked
        return new RechargeUrlInfo(PaymentGatewayName.JUSPAY, payment.getUrl(), payment.getMethod(), (Map) payment.getParams());
    }

    public List<Card> getStoredCards(Long userId) throws InternalServerErrorException {
        logger.info("GET STORED CARDS FOR " + userId);
        String juspayCustomerId = getJuspayCustomerId(userId);
        return getStoredCards(juspayCustomerId);
    }

    public List<Card> getStoredCards(String juspayCustomerId) throws InternalServerErrorException {
        logger.info("GET STORED CARDS FOR JUSPAY CUSTOMER " + juspayCustomerId);
        if (juspayCustomerId != null) {
            Map<String, Object> params = new HashMap<>();
            params.put("customer_id", juspayCustomerId);
            int count = 0;
            do {
                try {
                    count++;
                    return Card.list(params);
                } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                    logger.error(e.getMessage(), e);
                    throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:CARD:LIST " + juspayCustomerId + " "
                            + e.getErrorMessage());
                } catch (APIConnectionException e) {
                    logger.error(e.getMessage(), e);
                }
            } while (count < 2);
        }
        return new ArrayList<>();
    }

    private Wallet createWallet(String juspayCustomerId, WalletPaymentMethod walletPaymentMethod) throws InternalServerErrorException {
        if (juspayCustomerId != null) {
            logger.info("CREATING WALLET FOR " + juspayCustomerId + " - " + walletPaymentMethod);
            int count = 0;
            do {
                try {
                    count++;
                    return Wallet.create(juspayCustomerId, walletPaymentMethod.name());
                } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                    logger.error(e.getMessage(), e);
                    throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:WALLET:CREATE " + juspayCustomerId + " "
                            + e.getErrorMessage());
                } catch (APIConnectionException e) {
                    logger.error(e.getMessage(), e);
                }
            } while (count < 2);
        }
        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "UNABLE TO CREATE WALLET FOR " + juspayCustomerId);
    }

    public Wallet authenticateWallet(Long userId, WalletPaymentMethod walletPaymentMethod, String mobileNo) throws InternalServerErrorException {

        logger.info("AUTHENTICATE WALLET " + userId + " - " + walletPaymentMethod + " - " + mobileNo);
        Customer juspayCustomer;
        UserMapping userMapping = userMappingDao.getUserMapping(userId);
        if (userMapping == null || StringUtils.isEmpty(userMapping.getJuspayCustomerId())) {
            juspayCustomer = createJuspayCustomer(getUser(userId));
        } else {
            juspayCustomer = getJuspayCustomer(userMapping.getJuspayCustomerId());
        }

        logger.info("AUTHENTICATE WALLET CUSTOMER " + juspayCustomer.getId() + " - " + juspayCustomer.getMobileNumber());
        if (juspayCustomer.getMobileNumber() == null || !juspayCustomer.getMobileNumber().equalsIgnoreCase(mobileNo)) {
            Map<String, Object> update = new HashMap<>();
            update.put("mobile_number", mobileNo);
            try {
                logger.info("AUTHENTICATE WALLET UPDATE CUSTOMER");
                Customer.update(juspayCustomer.getId(), update);
            } catch (APIException | APIConnectionException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                throw new InternalServerErrorException(ErrorCode.GATEWAY_UPDATE_CUSTOMER_ERROR, "Can't update customer for " + juspayCustomer.getId());
            }
        }

        Wallet wallet = getWalletFor(juspayCustomer.getId(), walletPaymentMethod);
        int count = 0;
        do {
            try {
                count++;
                return Wallet.authenticate(wallet.getId());
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:WALLET:AUTHENTICATE " + juspayCustomer.getId() + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2);
        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "UNABLE TO AUTHENTICATE WALLET");
    }

    private Customer getJuspayCustomer(String juspayCustomerId) throws InternalServerErrorException {
        try {
            logger.info("GET JUSPAY CUSTOMER " + juspayCustomerId);
            return Customer.get(juspayCustomerId);
        } catch (APIException | APIConnectionException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
            throw new InternalServerErrorException(ErrorCode.BAD_REQUEST_ERROR, "Can't get juspay customer for " + juspayCustomerId);
        }
    }

    public Wallet linkWallet(String walletId, String otp) throws InternalServerErrorException {
        int count = 0;
        logger.info("JUSPAY LINK WALLET " + walletId + " - " + otp);
        do {
            try {
                count++;
                Wallet.link(walletId, otp);
                return Wallet.refreshByWalletId(walletId);
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:WALLET:LINK " + walletId + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2);
        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "UNABLE TO LINK WALLET");
    }

    public List<Wallet> listWallets(Long vedantuUserId) throws InternalServerErrorException {
        String juspayCustomerId = getJuspayCustomerId(vedantuUserId);
        return listWallets(juspayCustomerId);
    }

    public List<Wallet> listWallets(String juspayCustomerId) throws InternalServerErrorException {
        logger.info("JUSPAY LIST WALLET " + juspayCustomerId);
        if (juspayCustomerId == null) {
            return new ArrayList<>();
        }
        int count = 0;
        do {
            try {
                count++;
                WalletList list = Wallet.refresh(juspayCustomerId);
                return list.getList();
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:WALLET:LIST " + juspayCustomerId + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2);
        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "UNABLE FETCH WALLET LIST");
    }

    private Wallet getWalletFor(String juspayCustomerId, WalletPaymentMethod walletPaymentMethod) throws InternalServerErrorException {
        logger.info("GETTING WALLET FOR " + juspayCustomerId + " - " + walletPaymentMethod);
        List<Wallet> wallets = listWallets(juspayCustomerId);
        for (Wallet wallet : wallets) {
            if (walletPaymentMethod.equals(WalletPaymentMethod.valueOf(wallet.getWallet()))) {
                return wallet;
            }
        }

        return createWallet(juspayCustomerId, walletPaymentMethod);
    }

    private User getUser(Long userId) {
        String key = "USER_JSON_" + userId;
        try {
            String userJson = redisDAO.get(key);
            if (userJson != null) {
                User user = gson.fromJson(userJson, User.class);
                if (user != null && StringUtils.isNotEmpty(user.getContactNumber())) {
                    return user;
                } else {
                    redisDAO.del(key);
                }
            }
        } catch (InternalServerErrorException | BadRequestException e) {
            logger.error(e.getErrorMessage(), e);
        }
        ClientResponse response = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + userId, HttpMethod.GET, null);
        String json = response.getEntity(String.class);
        User user = gson.fromJson(json, User.class);
        try {
            if (user != null && StringUtils.isNotEmpty(user.getContactNumber())) {
                redisDAO.set(key, json);
            }
        } catch (InternalServerErrorException e) {
            logger.error(e.getErrorMessage(), e);
        }
        return user;
    }

    private Payment getPayment(Order order, PaymentOption paymentOption) throws InternalServerErrorException {
        logger.info("GET PAYMENT FOR ORDER " + order.getOrderId());
        Payment payment = new Payment();
        Map<String, String> params = new HashMap<>();

        params.put("order_id", order.getOrderId());
        params.put("merchant_id", MERCHANT_ID);
        payment.setMethod(order.getPaymentMethod());
        payment.setUrl(order.getPaymentLinks().getWebLink());
        payment.setOrderId(order.getOrderId());
        Map<String, String> paymentParams = payment.getParams();
        paymentParams = paymentParams == null ? new HashMap<>() : paymentParams;
        paymentParams.putAll(params);
        payment.setParams(paymentParams);


        if (paymentOption != null && paymentOption.getPaymentInstrument() != null) {
            switch (paymentOption.getPaymentInstrument().getOptedPaymentMethodType()) {
                case CARD:
                    payment = getCardRequest(paymentOption, order);
                    break;
                case NETBANKING:
                    payment = getNetbankingRequest(paymentOption, order);
                    break;
                case WALLET:
                    payment = getWalletRequest(paymentOption, order);
                    break;
                case WALLET_DIRECT_DEBIT:
                    payment = getWalletDirectDebitRequest(paymentOption, order);
                    break;
                case UPI_PAY:
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "UPI PAY NOT CONFIGURED");
                case UPI_COLLECT:
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "UPI COLLECT NOT CONFIGURED");
                case ATM_REDIRECTION:
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "ATM REDIRECTION NOT CONFIGURED");
                case ATM_SEAMLESS_PIN:
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "ATM SEAMLESS PIN NOT CONFIGURED");
                default:
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "UNKNOWN PAYMENT OPTION : " +
                            paymentOption.getPaymentInstrument().getOptedPaymentMethodType());
            }
        }
        return payment;
    }

    private Payment getWalletDirectDebitRequest(PaymentOption paymentOption, Order order) throws InternalServerErrorException {
        logger.info("JUSPAY WALLET DIRECT DEBIT REQ " + order.getOrderId() + " - " + gson.toJson(paymentOption));
        Map<String, Object> params = new HashMap<>();

        params.put("order_id", order.getOrderId());
        params.put("merchant_id", MERCHANT_ID);
        params.put("redirect_after_payment", "true");
        params.put("format", "json");
        PaymentOption.Wallet paymentInstrument = (PaymentOption.Wallet) paymentOption.getPaymentInstrument();
        params.put("payment_method", paymentInstrument.getPaymentMethod());
        params.put("payment_method_type", paymentInstrument.getPaymentMethodType());
        params.put("direct_wallet_token", paymentInstrument.getDirectDebitToken());

        int count = 0;
        do {
            try {
                count++;
                Payment payment = Payment.create(params);
                logger.info("PAYMENT : " + gson.toJson(payment));
                logger.info("PAYMENT WALLET PARAM: " + gson.toJson(params));
                return payment;
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:TXN:WALLET:DIRECTDEBIT " + order.getUdf1() + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2);
        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "WALLET PAYMENT CREATION FAILED");
    }

    private Payment getWalletRequest(PaymentOption paymentOption, Order order) throws InternalServerErrorException {
        logger.info("JUSPAY WALLET REQ " + order.getOrderId() + " - " + gson.toJson(paymentOption));

        Map<String, Object> params = new HashMap<>();

        params.put("order_id", order.getOrderId());
        params.put("merchant_id", MERCHANT_ID);
        params.put("redirect_after_payment", "true");
        params.put("format", "json");
        PaymentOption.Wallet paymentInstrument = (PaymentOption.Wallet) paymentOption.getPaymentInstrument();
        params.put("payment_method_type", paymentInstrument.getPaymentMethodType());
        params.put("payment_method", String.valueOf(paymentInstrument.getPaymentMethod()));

        int count = 0;
        do {
            try {
                count++;
                Payment payment = WalletPayment.create(params);
                logger.info("PAYMENT : " + gson.toJson(payment));
                return payment;
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:TXN:WALLET " + order.getUdf1() + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2);
        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "WALLET PAYMENT CREATION FAILED");
    }

    private Payment getNetbankingRequest(PaymentOption paymentOption, Order order) throws InternalServerErrorException {
        logger.info("JUSPAY NETBANKING REQ " + order.getOrderId() + " - " + gson.toJson(paymentOption));
        Map<String, Object> params = new TreeMap<>();

        params.put("order_id", order.getOrderId());
        params.put("merchant_id", MERCHANT_ID);
        params.put("redirect_after_payment", "true");
        params.put("format", "json");
        PaymentOption.NetBanking paymentInstrument = (PaymentOption.NetBanking) paymentOption.getPaymentInstrument();
        params.put("payment_method_type", paymentInstrument.getPaymentMethodType());
        params.put("payment_method", paymentInstrument.getPaymentMethod());

        int count = 0;
        do {
            try {
                count++;
                Payment payment = Payment.create(params);
                logger.info("PAYMENT : " + gson.toJson(payment));
                return payment;
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:TXN:NB " + order.getUdf1() + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2);

        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "NETBANKING PAYMENT CREATION FAILED");
    }

    private Payment getCardRequest(PaymentOption paymentOption, Order order) throws InternalServerErrorException {
        logger.info("JUSPAY CARD REQ " + order.getOrderId() + " - " + gson.toJson(paymentOption));
        Map<String, Object> params = new TreeMap<>();


        params.put("order_id", order.getOrderId());
        params.put("merchant_id", MERCHANT_ID);
        params.put("redirect_after_payment", "true");
        params.put("format", "json");
        PaymentOption.Card paymentInstrument = (PaymentOption.Card) paymentOption.getPaymentInstrument();
        params.put("payment_method_type", paymentInstrument.getPaymentMethodType());
        params.put("card_security_code", paymentInstrument.getCardSecurityCode());
        if (paymentInstrument.getCardToken() != null) {
            params.put("card_token", paymentInstrument.getCardToken());
        } else {
            if (paymentInstrument.getPaymentMethod() != null) {
                params.put("payment_method", paymentInstrument.getCardToken());
            }
            params.put("card_number", paymentInstrument.getCardNumber());
            params.put("card_exp_month", paymentInstrument.getCardExpiryMonth());
            params.put("card_exp_year", paymentInstrument.getCardExpiryYear());
            params.put("name_on_card", paymentInstrument.getNameOnCard());
            params.put("save_to_locker", paymentInstrument.isSaveToLocker());
        }

        int count = 0;
        do {
            try {
                count++;
                Payment payment = Payment.create(params);
                logger.info("PAYMENT : " + gson.toJson(payment));
                return payment;
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:TXN:CARD " + order.getUdf1() + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2);

        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "CARD PAYMENT CREATION FAILED");
    }

    private Order createOrder(ExtTransaction transaction, User user, PaymentOption paymentOption, String juspayCustomerId) throws InternalServerErrorException {

        logger.info("CREATE ORDER FOR " + juspayCustomerId);
        BigDecimal amount = new BigDecimal(transaction.getAmount()).divide(new BigDecimal(100), RoundingMode.HALF_EVEN);
        Map<String, Object> params = new TreeMap<>();

//        params.put("order_id", transaction.getId());
        params.put("order_id", transaction.getUniqueOrderId());
        params.put("amount", amount.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        params.put("currency", "INR");
        params.put("customer_id", juspayCustomerId);
        params.put("customer_email", user.getEmail());
        params.put("return_url", REDIRECT_URL);
        params.put("description", "Payment Request From " + user.getFullName());
        params.put("udf1", transaction.getId());
        params.put("udf2", user.getId());
        params.put("udf3", "Email: " + org.apache.commons.lang3.StringUtils.defaultString(user.getEmail(), "-")
                + " Phone: " + org.apache.commons.lang3.StringUtils.defaultString(user.getContactNumber(), "-"));
        if(transaction.getEmiInfo()!=null) {
             EmiCard emiCard =  emiCardDao.getById(paymentOption.getEmiId());
            logger.info( "emiCard   " + emiCard);
//            params.put("amount", amount.setScale(2, BigDecimal.ROUND_HALF_EVEN));
//                             params.put("metadata.subvention_amount", 0);

             if(emiCard != null && StringUtils.isNotEmpty( emiCard.getOfferId())) {
                 params.put("metadata.RAZORPAY:offer_id", emiCard.getOfferId());
             }
             else{
                 throw new RuntimeException("No EMI found");

             }
        }
        int count = 0;
        boolean tryAgain = true;
        Order order = null;
        do {
            try {
                count++;
                order = Order.create(params);
                tryAgain = false;
                logger.info("ORDER : " + gson.toJson(order));
                logger.info("ORDER PARAM: " + gson.toJson(params));
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:TXN " + transaction.getId() + " "
                        + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        } while (count < 2 && tryAgain);

        logger.info("payment channel [" + PAYMENT_CHANNEL + "] payment url :  " + CHARGING_URL);
        logger.info("payment channel [" + PAYMENT_CHANNEL + "] payment params :  " + params);
        return order;
    }

    public String getJuspayCustomerId(Long vedantuUserId) throws InternalServerErrorException {
        UserMapping userMapping = userMappingDao.getUserMapping(vedantuUserId);
        if (userMapping == null || StringUtils.isEmpty(userMapping.getJuspayCustomerId())) {
            User user = getUser(vedantuUserId);
            return createJuspayCustomer(user).getId();
        }
        return userMapping.getJuspayCustomerId();
    }

    private Customer createJuspayCustomer(User user) throws InternalServerErrorException {
        try {
            return getCustomer(user);
        } catch (Exception e) {
            logger.warn("JUSPAY GET CUSTOMER INVALID REQ " + e.getMessage(), e);
        }

        logger.info("JUSPAY CREATE CUSTOMER FOR VUSER " + user.getId());
        Map<String, Object> customerMap = new HashMap<>();
        customerMap.put("object_reference_id", user.getId());
        customerMap.put("mobile_number", user.getContactNumber());
        customerMap.put("email_address", user.getEmail());
        customerMap.put("mobile_country_code", user.getPhoneCode());
        customerMap.put("first_name", user.getFirstName());
        customerMap.put("last_name", user.getLastName());
        int count = 0;
        logger.info("JUSPAY CREATE CUSTOMER WITH DATA : " + gson.toJson(customerMap));
        do {
            try {
                count++;
                Customer customer = Customer.create(customerMap);
                UserMapping mapping = new UserMapping();
                mapping.setVedantuUserId(user.getId());
                mapping.setJuspayCustomerId(customer.getId());
                userMappingDao.save(mapping);
                return customer;
            } catch (APIConnectionException e) {
                if (count >= 2) {
                    logger.error(e.getMessage(), e);
                }
            } catch (APIException | AuthorizationException | AuthenticationException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_CREATE_CUSTOMER_ERROR, "unable to create customer - " + e.getMessage());
            } catch (InvalidRequestException e) {
                if ("Customer with given object reference id already exist for your account.".equalsIgnoreCase(e.getErrorMessage())) {
                    try {
                        return getCustomer(user);
                    } catch (Exception ex) {
                        logger.error(ex.getMessage(), ex);
                        throw new InternalServerErrorException(ErrorCode.GATEWAY_GET_CUSTOMER_ERROR, "unable to get customer - " + ex.getMessage());
                    }
                } else {
                    logger.error(e.getMessage(), e);
                    throw new InternalServerErrorException(ErrorCode.GATEWAY_CREATE_CUSTOMER_ERROR, "unable to create customer");
                }
            }
        } while (count < 2);
        throw new RuntimeException("Juspay customer creation failed");
    }

    private Customer getCustomer(User user) throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException, InternalServerErrorException {
        Customer customer = Customer.get(String.valueOf(user.getId()));
        UserMapping mapping = new UserMapping();
        mapping.setVedantuUserId(user.getId());
        mapping.setJuspayCustomerId(customer.getId());

        if (customer.getEmailAddress() != null && customer.getEmailAddress().trim().equalsIgnoreCase(user.getEmail())) {
            logger.info("EMAIL IN VED DB: " + user.getEmail() + " EMAIL IN JUSPAY SERVER: " + customer.getEmailAddress());
            Map<String, Object> update = new HashMap<>();
            update.put("email_address", user.getEmail());
            update.put("mobile_number", user.getContactNumber());
            try {
                logger.info("CREATE CUSTOMER -> UPDATE CUSTOMER");
                Customer.update(customer.getId(), update);
            } catch (APIException | APIConnectionException | AuthorizationException | AuthenticationException | InvalidRequestException ex) {
                logger.error(ex.getMessage(), ex);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_UPDATE_CUSTOMER_ERROR, "Can't update customer for " + customer.getId());
            }
            // EXTRA SECURITY MEASURE IF SOME THING IS CHANGED IN DB
            List<Wallet> wallets = listWallets(customer.getId());
            for (Wallet wallet : wallets) {
                Wallet.delink(wallet.getId());
            }
            List<Card> storedCards = getStoredCards(customer.getId());
            for (Card card : storedCards) {
                Card.delete(card.getCardToken());
            }
        }
        userMappingDao.save(mapping);
        return customer;
    }

    @Override
    public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId) throws NotFoundException, InternalServerErrorException, ConflictException, BadRequestException {

        // VERIFY THE RESPONSE FROM SERVER
        verifyRequest(transactionInfo, callingUserId);

        logger.info("JUSPAY ONPAYMENT RECEIVED " + gson.toJson(transactionInfo));

        Map<String, String> collect = new TreeMap<>();


        transactionInfo.forEach((key, value) -> collect.put(key, ((List<String>) value).get(0)));
        String signatureAlgorithm = collect.remove("signature_algorithm");
        String signature = collect.remove("signature");

        String str = collect.entrySet().stream()
                .map(JuspayPaymentManager::getEncode)
                .collect(Collectors.joining("%26")); // URL Encoded Value of &
        String signatureCalculated = getSignature(str);

        Order status = null;
        String order_id = collect.get("order_id");

        int count = 0;
        boolean tryAgain = true;
        while (count < 2 && tryAgain) {
            try {
                count++;
                status = Order.status(order_id);
                tryAgain = false;
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "AXIS JUSPAY: API Error Occurred while fetching status");
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        }


        ExtTransaction extTransaction;
        if (status != null) {
            double amount = status.getAmount() * 100;
            int amountPaid = (int) amount;

            String paymentStatus = status.getStatus();

            String transactionId = status.getUdf1(); // ExtTransaction ID Stored in UDF1 Param While Creating Payment URL

            String paymentChannelTransactionId = status.getPaymentGatewayResponse() == null ? null :
                    status.getPaymentGatewayResponse().getTxnId();

            TransactionStatus transactionStatus;

            if ("charged".equalsIgnoreCase(paymentStatus) && signature.equals(signatureCalculated)) {
                transactionStatus = TransactionStatus.SUCCESS;
            } else if ("AUTHENTICATION_FAILED".equalsIgnoreCase(paymentStatus)) {
                transactionStatus = TransactionStatus.CANCELLED;
            } else {
                transactionStatus = TransactionStatus.FAILED;
            }
            String bankRefNo = status.getTxnId();

            String paymentMethod = status.getPaymentMethod();

            String paymentInstrument = status.getPaymentMethodType();


            if (transactionStatus != TransactionStatus.SUCCESS) {
                amountPaid = 0;
            }

            logger.info("transaction status : " + transactionStatus);

            logger.info("starting transaction ");
            extTransaction = extTransactionDAO.getById(transactionId);
            logger.info("extTransaction before updation : " + extTransaction);

            // Save output to DB
            extTransaction.setGatewayResponse(gson.toJson(status));

            if (extTransaction.getStatus() != TransactionStatus.PENDING) {

                throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
                        "transaction with id " + transactionId + " is already processed");
            }
            extTransaction.setStatus(transactionStatus);
            extTransaction.setGatewayStatus(paymentStatus);
            extTransaction.setTransactionTime(String.valueOf(System.currentTimeMillis()));
            extTransaction.setPaymentChannelTransactionId(paymentChannelTransactionId);
            extTransaction.setPaymentInstrument(paymentInstrument);
            extTransaction.setPaymentMethod(paymentMethod);
            extTransaction.setBankRefNo(bankRefNo);
            extTransaction.setChecksum(signature);
            extTransaction.setBankResponseMessage(status.getBankErrorMessage());
            JuspayGateway gateway = JuspayGateway.getGateway(status.getGatewayId());
            extTransaction.setRedirectedGateway(gateway == null ? null : gateway.name());
            logger.info("saving extTransaction: " + extTransaction);
            extTransactionDAO.create(extTransaction, callingUserId);
            if (extTransaction.getAmount() == amountPaid && transactionStatus == TransactionStatus.SUCCESS) {
                // TODO: these functionality can be moved to abstract class
                // transaction = TransactionDAO.INSTANCE
                // .upsertTransaction(transaction);

                Account account;
                // TODO: take this code to common place, where update and
                // deduct
                // operation can be done
                logger.info("transaction was successful:" + extTransaction);
                account = accountDAO
                        .getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
                if (account == null) {
                    logger.error("no account found for userid:" + extTransaction.getUserId());

                    throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                            "no account found for userId:" + extTransaction.getUserId());
                }
                int subventionAmount = 0;
                logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);
//                if(extTransaction.getPreSubventionAmount() != null &&  extTransaction.getPreSubventionAmount() >0 ) {
//                    amountPaid =extTransaction.getPreSubventionAmount();
//                    subventionAmount = amountPaid - extTransaction.getAmount();
//                    logger.info("subvention amount  [" + subventionAmount + "]  total  amount: " + amountPaid);
//                }
                    account.setBalance(account.getBalance() + amountPaid);
                    account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

                logger.info("new account balance : " + account.getBalance());

                accountDAO.updateWithoutSession(account, null);

                if(extTransaction.getEmiInfo() != null) {
                    float rateofIntrest = extTransaction.getEmiInfo().getRate();
                    Integer duration = extTransaction.getEmiInfo().getTenure();
                    float R = (float) rateofIntrest / (float) ( 100);
                    int Total = extTransaction.getAmount().intValue();
                    //https://razorpay.com/docs/offers/no-cost-emi/
                    //Total = [P x R x (1+R)^N]/[(1+R)^N-1]
                    if(R > 0) {
                        int calculatedPrincipal = (int) (((Math.pow((1 + R / 12), duration) - 1) * Total / duration) / (R / 12 * Math.pow(1 + (R / 12), duration)));
//                        amountPaid = amountPaid-subventionAmount;
                        subventionAmount = amountPaid - calculatedPrincipal;
                        amountPaid = calculatedPrincipal;
                    }


                }

                com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                        amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                        BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                        TransactionRefType.EXTERNAL_TRANSACTION, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
                transactionDAO.create(vedantuTransaction);

                if (subventionAmount != 0) {
                    //Create Subvention Transaction
                    com.vedantu.dinero.entity.Transaction vedantuSubvention = new com.vedantu.dinero.entity.Transaction(
                            subventionAmount, 0, subventionAmount, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                            BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                            TransactionRefType.EXTERNAL_TRANSACTION, null,
                            com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
                    vedantuSubvention.setReasonRefSubType(TransactionRefSubType.RAZORPAY_SUBVENTION_FEE);
                    transactionDAO.create(vedantuSubvention);
                }
                logger.info("Account " + account.toString());
            }
            return extTransaction;
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "AXIS JUSPAY: Order status not found for JuspayOrder ID " + order_id);
        }
    }

    private static String getEncode(Map.Entry<String, String> e) {
        try {
            return URLEncoder.encode(e.getKey() + "=" + e.getValue(), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private String getSignature(String fields) throws InternalServerErrorException {

        try {
            final Mac instance = Mac.getInstance("HmacSHA256");
            final SecretKeySpec secret_key = new SecretKeySpec(RESPONSE_KEY.getBytes(), "HmacSHA256");
            instance.init(secret_key);
            final byte[] mac_data = instance.doFinal(fields.getBytes());
            return Base64.getEncoder().encodeToString(mac_data);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            logger.error(e.getMessage(), e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    public List<Map<String, String>> getNetbankingDetails() {
        return NetbankingPaymentMethod.getNetBankingList();
    }

    public List<Map<String, String>> getWalletDetails() throws InternalServerErrorException {
        int count = 0;
        do {
            try {
                count++;
                Field[] fields = PaymentMethodList.class.getDeclaredFields();
                Field field = Arrays.stream(fields)
                        .filter(e -> "paymentMethods".equals(e.getName())
                                && e.getType().equals(List.class))
                        .findAny().orElseThrow( () -> new RuntimeException("No Field named 'paymentMethods'"));
                if (field != null) {
                    PaymentMethodList list = PaymentMethod.list();
                    field.setAccessible(true);
                    //noinspection unchecked
                    List<PaymentMethod> paymentMethods = (List<PaymentMethod>) field.get(list);
                    List<Map<String, String>> data = new ArrayList<>();
                    for (PaymentMethod paymentMethod : paymentMethods) {
                        Map<String, String> map = new HashMap<>();
                        if ("WALLET".equalsIgnoreCase(paymentMethod.getPaymentMethodType())) {
                            map.put("wallet_code", paymentMethod.getPaymentMethod());
                            map.put("wallet_description", paymentMethod.getDescription());
                            data.add(map);
                        }
                    }
                    return data;
                }
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.GATEWAY_ERROR, "JUSPAY:TXN " + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        } while (count < 1);

        return new ArrayList<>();
    }

}
