package com.vedantu.dinero.managers.payment.payu;

public class PayuTransactionInitiationReq {
	
	//Merchant Key provided by PayUMoney
	//Compulsory
	public String key;
	//Compulsory
	public String txnid;
	//Payment amount
	//Compulsory
	public String amount;
	//Product Description
	//Compulsory
	public String productinfo;
	//(only alphabets a-z are allowed)
	//Compulsory
	public String firstname;
	//(only alphabets a-z are allowed)
	public String lastname;
	/*(Length of address1 and address2 must not more than 100 characters each and the allowed
	//characters are only) A TO Z, a to z, 0 to 9, @, - (Minus), _ (Underscore), / (Backslash), (Space), (Dot)*/
	public String address1;
	public String address2;
	public String city;
	public String state;
	public String country;
	//Customer’s email Id Compulsory
	public String email;
	public Long zipcode;
	//mobile number or landline number (numeric value only)
	//Compulsory
	public Long phone;
	//Self-Explanatory (constraints same as firstname) Used in case of COD
	public String shipping_firstname;
	public String shipping_lastname;
	public String shipping_address1;
	public String shipping_address2;
	public String shipping_city;
	public String shipping_state;
	public String shipping_country;
	public Long shipping_zipcode;
	public Long shipping_phone;
	//Self-Explanatory (yes/no) Used in case of COD
	public String shipping_phoneverified;
	//user defined field 1
	public String udf1;
	//user defined field 2
	public String udf2;
	/*user defined field 3 */
	public String udf3;
	/*user defined field 4 */
	public String udf4;
	/*user defined field 5 */
	public String udf5;
	/*Success URL where PayUMoney will redirect after successful payment.
	Compulsory */
	public String surl;
	//Cancel URL where PayUMoney will redirect when user cancel the transaction.
	//Compulsory
	public String curl;
	//Failure URL where PayUMoney will redirect after failed payment.
	//Compulsory
	public String furl;
	/*Hash or Checksum =sha512(key|txnid|amount|productinfo|firstname|email|u
	￼￼//df1|udf2|udf3|udf4|udf5||||||salt) (SALT will be provided by PayUMoney)
	//Compulsory*/
	public String hash;
	//	Compulsory
	//payu_paisa
//	public String service_provider="payu_biz";//payu_biz , payu_paisa
	
	public PayuTransactionInitiationReq() {
		super();
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getTxnid() {
		return txnid;
	}

	public void setTxnid(String txnid) {
		this.txnid = txnid;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getProductinfo() {
		return productinfo;
	}

	public void setProductinfo(String productinfo) {
		this.productinfo = productinfo;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Long getZipcode() {
		return zipcode;
	}

	public void setZipcode(Long zipcode) {
		this.zipcode = zipcode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}

	public String getShipping_firstname() {
		return shipping_firstname;
	}

	public void setShipping_firstname(String shipping_firstname) {
		this.shipping_firstname = shipping_firstname;
	}

	public String getShipping_lastname() {
		return shipping_lastname;
	}

	public void setShipping_lastname(String shipping_lastname) {
		this.shipping_lastname = shipping_lastname;
	}

	public String getShipping_address1() {
		return shipping_address1;
	}

	public void setShipping_address1(String shipping_address1) {
		this.shipping_address1 = shipping_address1;
	}

	public String getShipping_address2() {
		return shipping_address2;
	}

	public void setShipping_address2(String shipping_address2) {
		this.shipping_address2 = shipping_address2;
	}

	public String getShipping_city() {
		return shipping_city;
	}

	public void setShipping_city(String shipping_city) {
		this.shipping_city = shipping_city;
	}

	public String getShipping_state() {
		return shipping_state;
	}

	public void setShipping_state(String shipping_state) {
		this.shipping_state = shipping_state;
	}

	public String getShipping_country() {
		return shipping_country;
	}

	public void setShipping_country(String shipping_country) {
		this.shipping_country = shipping_country;
	}

	public Long getShipping_zipcode() {
		return shipping_zipcode;
	}

	public void setShipping_zipcode(Long shipping_zipcode) {
		this.shipping_zipcode = shipping_zipcode;
	}

	public Long getShipping_phone() {
		return shipping_phone;
	}

	public void setShipping_phone(Long shipping_phone) {
		this.shipping_phone = shipping_phone;
	}

	public String getShipping_phoneverified() {
		return shipping_phoneverified;
	}

	public void setShipping_phoneverified(String shipping_phoneverified) {
		this.shipping_phoneverified = shipping_phoneverified;
	}

	public String getUdf1() {
		return udf1;
	}

	public void setUdf1(String udf1) {
		this.udf1 = udf1;
	}

	public String getUdf2() {
		return udf2;
	}

	public void setUdf2(String udf2) {
		this.udf2 = udf2;
	}

	public String getUdf3() {
		return udf3;
	}

	public void setUdf3(String udf3) {
		this.udf3 = udf3;
	}

	public String getUdf4() {
		return udf4;
	}

	public void setUdf4(String udf4) {
		this.udf4 = udf4;
	}

	public String getUdf5() {
		return udf5;
	}

	public void setUdf5(String udf5) {
		this.udf5 = udf5;
	}

	public String getSurl() {
		return surl;
	}

	public void setSurl(String surl) {
		this.surl = surl;
	}

	public String getCurl() {
		return curl;
	}

	public void setCurl(String curl) {
		this.curl = curl;
	}

	public String getFurl() {
		return furl;
	}

	public void setFurl(String furl) {
		this.furl = furl;
	}

	public String getHash() {
		return hash;
	}

	public void setHash(String hash) {
		this.hash = hash;
	}

//	public String getService_provider() {
//		return service_provider;
//	}
//
//	public void setService_provider(String service_provider) {
//		this.service_provider = service_provider;
//	}

	@Override
	public String toString() {
		return "PayuTransactionInitiationReq [key=" + key + ", txnid=" + txnid
				+ ", amount=" + amount + ", productinfo=" + productinfo
				+ ", firstname=" + firstname + ", lastname=" + lastname
				+ ", address1=" + address1 + ", address2=" + address2
				+ ", city=" + city + ", state=" + state + ", country="
				+ country + ", zipcode=" + zipcode + ", email=" + email
				+ ", phone=" + phone + ", shipping_firstname="
				+ shipping_firstname + ", shipping_lastname="
				+ shipping_lastname + ", shipping_address1="
				+ shipping_address1 + ", shipping_address2="
				+ shipping_address2 + ", shipping_city=" + shipping_city
				+ ", shipping_state=" + shipping_state + ", shipping_country="
				+ shipping_country + ", shipping_zipcode=" + shipping_zipcode
				+ ", shipping_phone=" + shipping_phone
				+ ", shipping_phoneverified=" + shipping_phoneverified
				+ ", udf1=" + udf1 + ", udf2=" + udf2 + ", udf3=" + udf3
				+ ", udf4=" + udf4 + ", udf5=" + udf5 + ", surl=" + surl
				+ ", curl=" + curl + ", furl=" + furl + ", hash=" + hash
//				+ ", service_provider=" + service_provider 
                        + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((address1 == null) ? 0 : address1.hashCode());
		result = prime * result
				+ ((address2 == null) ? 0 : address2.hashCode());
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((city == null) ? 0 : city.hashCode());
		result = prime * result + ((country == null) ? 0 : country.hashCode());
		result = prime * result + ((curl == null) ? 0 : curl.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
				+ ((firstname == null) ? 0 : firstname.hashCode());
		result = prime * result + ((furl == null) ? 0 : furl.hashCode());
		result = prime * result + ((hash == null) ? 0 : hash.hashCode());
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		result = prime * result
				+ ((lastname == null) ? 0 : lastname.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		result = prime * result
				+ ((productinfo == null) ? 0 : productinfo.hashCode());
//		result = prime
//				* result
//				+ ((service_provider == null) ? 0 : service_provider.hashCode());
		result = prime
				* result
				+ ((shipping_address1 == null) ? 0 : shipping_address1
						.hashCode());
		result = prime
				* result
				+ ((shipping_address2 == null) ? 0 : shipping_address2
						.hashCode());
		result = prime * result
				+ ((shipping_city == null) ? 0 : shipping_city.hashCode());
		result = prime
				* result
				+ ((shipping_country == null) ? 0 : shipping_country.hashCode());
		result = prime
				* result
				+ ((shipping_firstname == null) ? 0 : shipping_firstname
						.hashCode());
		result = prime
				* result
				+ ((shipping_lastname == null) ? 0 : shipping_lastname
						.hashCode());
		result = prime * result
				+ ((shipping_phone == null) ? 0 : shipping_phone.hashCode());
		result = prime
				* result
				+ ((shipping_phoneverified == null) ? 0
						: shipping_phoneverified.hashCode());
		result = prime * result
				+ ((shipping_state == null) ? 0 : shipping_state.hashCode());
		result = prime
				* result
				+ ((shipping_zipcode == null) ? 0 : shipping_zipcode.hashCode());
		result = prime * result + ((state == null) ? 0 : state.hashCode());
		result = prime * result + ((surl == null) ? 0 : surl.hashCode());
		result = prime * result + ((txnid == null) ? 0 : txnid.hashCode());
		result = prime * result + ((udf1 == null) ? 0 : udf1.hashCode());
		result = prime * result + ((udf2 == null) ? 0 : udf2.hashCode());
		result = prime * result + ((udf3 == null) ? 0 : udf3.hashCode());
		result = prime * result + ((udf4 == null) ? 0 : udf4.hashCode());
		result = prime * result + ((udf5 == null) ? 0 : udf5.hashCode());
		result = prime * result + ((zipcode == null) ? 0 : zipcode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PayuTransactionInitiationReq other = (PayuTransactionInitiationReq) obj;
		if (address1 == null) {
			if (other.address1 != null)
				return false;
		} else if (!address1.equals(other.address1))
			return false;
		if (address2 == null) {
			if (other.address2 != null)
				return false;
		} else if (!address2.equals(other.address2))
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (city == null) {
			if (other.city != null)
				return false;
		} else if (!city.equals(other.city))
			return false;
		if (country == null) {
			if (other.country != null)
				return false;
		} else if (!country.equals(other.country))
			return false;
		if (curl == null) {
			if (other.curl != null)
				return false;
		} else if (!curl.equals(other.curl))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (firstname == null) {
			if (other.firstname != null)
				return false;
		} else if (!firstname.equals(other.firstname))
			return false;
		if (furl == null) {
			if (other.furl != null)
				return false;
		} else if (!furl.equals(other.furl))
			return false;
		if (hash == null) {
			if (other.hash != null)
				return false;
		} else if (!hash.equals(other.hash))
			return false;
		if (key == null) {
			if (other.key != null)
				return false;
		} else if (!key.equals(other.key))
			return false;
		if (lastname == null) {
			if (other.lastname != null)
				return false;
		} else if (!lastname.equals(other.lastname))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		if (productinfo == null) {
			if (other.productinfo != null)
				return false;
		} else if (!productinfo.equals(other.productinfo))
			return false;
//		if (service_provider == null) {
//			if (other.service_provider != null)
//				return false;
//		} else if (!service_provider.equals(other.service_provider))
//			return false;
		if (shipping_address1 == null) {
			if (other.shipping_address1 != null)
				return false;
		} else if (!shipping_address1.equals(other.shipping_address1))
			return false;
		if (shipping_address2 == null) {
			if (other.shipping_address2 != null)
				return false;
		} else if (!shipping_address2.equals(other.shipping_address2))
			return false;
		if (shipping_city == null) {
			if (other.shipping_city != null)
				return false;
		} else if (!shipping_city.equals(other.shipping_city))
			return false;
		if (shipping_country == null) {
			if (other.shipping_country != null)
				return false;
		} else if (!shipping_country.equals(other.shipping_country))
			return false;
		if (shipping_firstname == null) {
			if (other.shipping_firstname != null)
				return false;
		} else if (!shipping_firstname.equals(other.shipping_firstname))
			return false;
		if (shipping_lastname == null) {
			if (other.shipping_lastname != null)
				return false;
		} else if (!shipping_lastname.equals(other.shipping_lastname))
			return false;
		if (shipping_phone == null) {
			if (other.shipping_phone != null)
				return false;
		} else if (!shipping_phone.equals(other.shipping_phone))
			return false;
		if (shipping_phoneverified == null) {
			if (other.shipping_phoneverified != null)
				return false;
		} else if (!shipping_phoneverified.equals(other.shipping_phoneverified))
			return false;
		if (shipping_state == null) {
			if (other.shipping_state != null)
				return false;
		} else if (!shipping_state.equals(other.shipping_state))
			return false;
		if (shipping_zipcode == null) {
			if (other.shipping_zipcode != null)
				return false;
		} else if (!shipping_zipcode.equals(other.shipping_zipcode))
			return false;
		if (state == null) {
			if (other.state != null)
				return false;
		} else if (!state.equals(other.state))
			return false;
		if (surl == null) {
			if (other.surl != null)
				return false;
		} else if (!surl.equals(other.surl))
			return false;
		if (txnid == null) {
			if (other.txnid != null)
				return false;
		} else if (!txnid.equals(other.txnid))
			return false;
		if (udf1 == null) {
			if (other.udf1 != null)
				return false;
		} else if (!udf1.equals(other.udf1))
			return false;
		if (udf2 == null) {
			if (other.udf2 != null)
				return false;
		} else if (!udf2.equals(other.udf2))
			return false;
		if (udf3 == null) {
			if (other.udf3 != null)
				return false;
		} else if (!udf3.equals(other.udf3))
			return false;
		if (udf4 == null) {
			if (other.udf4 != null)
				return false;
		} else if (!udf4.equals(other.udf4))
			return false;
		if (udf5 == null) {
			if (other.udf5 != null)
				return false;
		} else if (!udf5.equals(other.udf5))
			return false;
		if (zipcode == null) {
			if (other.zipcode != null)
				return false;
		} else if (!zipcode.equals(other.zipcode))
			return false;
		return true;
	}
	
}
