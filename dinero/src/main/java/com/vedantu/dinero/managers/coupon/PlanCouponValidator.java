/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import com.vedantu.dinero.managers.PricingManager;
import com.vedantu.dinero.response.GetPlanByIdResponse;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class PlanCouponValidator extends AbstractCouponValidator {

    @Autowired
    private PricingManager pricingManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PlanCouponValidator.class);

    public PlanCouponValidator() {
        super();
    }

    @Override
    public boolean supportsPlan() {
        return true;
    }

    @Override
    public List<String> getPlanIds(String purchasingEntityId) {
        return Arrays.asList(purchasingEntityId);
    }

    @Override
    public boolean supportsTeacher() {
        return true;
    }

    @Override
    public List<String> getTeacherIds(String purchasingEntityId) {
        logger.info("ENTRY " + purchasingEntityId);
        List<String> list = new ArrayList<>();
        try {
            GetPlanByIdResponse res = pricingManager.getPlanById(purchasingEntityId);
            if (res != null && res.getTeacherId() != null) {
                list = Arrays.asList(res.getTeacherId().toString());
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        logger.info("EXIT " + list);
        return list;
    }

    @Override
    public List<String> getGrades(String purchasingEntityId) {
        //TODO validation should be done against mappings rather than individual grade,subject,target
        //eg. Mappings for a teacher are  11,Phy,cbse and 10,Chem,ICSE will pass for a coupon which 
        //has target of 10,phy,cbse
        logger.info("ENTRY " + purchasingEntityId);
        Set<String> list = new HashSet<>();
        try {
            JSONArray mappingsList = getMappings(purchasingEntityId);
            if (mappingsList != null) {
                for (int k = 0; k < mappingsList.length(); k++) {
                    JSONObject mapping = mappingsList.getJSONObject(k);
                    if (mapping.has("grade")) {
                        list.add(mapping.getString("grade"));
                    }
                }
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        logger.info("EXIT " + list);
        return new ArrayList<>(list);
    }

    @Override
    public boolean supportsGrade() {
        return true;
    }

    @Override
    public List<String> getCategories(String purchasingEntityId) {
        logger.info("ENTRY " + purchasingEntityId);
        Set<String> list = new HashSet<>();
        try {
            JSONArray mappingsList = getMappings(purchasingEntityId);
            if (mappingsList != null) {
                for (int k = 0; k < mappingsList.length(); k++) {
                    JSONObject mapping = mappingsList.getJSONObject(k);
                    if (mapping.has("category")) {
                        list.add(mapping.getString("category"));
                    }
                }
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        logger.info("EXIT " + list);
        return new ArrayList<>(list);
    }

    @Override
    public boolean supportsCategory() {
        return true;
    }

    @Override
    public List<String> getSubjects(String purchasingEntityId) {
        logger.info("ENTRY " + purchasingEntityId);
        Set<String> list = new HashSet<>();
        try {
            JSONArray mappingsList = getMappings(purchasingEntityId);
            if (mappingsList != null) {
                for (int k = 0; k < mappingsList.length(); k++) {
                    JSONObject mapping = mappingsList.getJSONObject(k);
                    if (mapping.has("boards")) {
                        JSONArray boardInfos = mapping.getJSONArray("boards");
                        if (boardInfos != null) {
                            for (int i = 0; i < boardInfos.length(); i++) {
                                JSONObject boardInfo = boardInfos.getJSONObject(i);
                                if (boardInfo.has("slug")) {
                                    list.add(boardInfo.getString("slug"));
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        logger.info("EXIT " + list);
        return new ArrayList<>(list);
    }

    @Override
    public boolean supportsSubject() {
        return true;
    }

    private JSONArray getMappings(String purchasingEntityId) {
        logger.info("ENTRY " + purchasingEntityId);
        JSONArray mappingsList = null;
        try {
            GetPlanByIdResponse res = pricingManager.getPlanById(purchasingEntityId);
            if (res != null && res.getTeacherId() != null) {
                String _mappings = fosUtils.getTeacherBoardMappings(res.getTeacherId().toString());
                JSONObject mappings = new JSONObject(_mappings);
                if (mappings.has("list") && mappings.getJSONArray("list").length() > 0) {
                    mappingsList = mappings.getJSONArray("list");
                }
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        logger.info("EXIT " + mappingsList);
        return mappingsList;
    }

}
