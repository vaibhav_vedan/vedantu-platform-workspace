package com.vedantu.dinero.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.Instalment.BaseInstalment;
import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.entity.Instalment.InstalmentConfigProps;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.enums.CouponType;
import com.vedantu.dinero.enums.Instalment.InstalmentChargeType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.RedeemType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.InstalmentChangeTime;
import com.vedantu.dinero.pojo.InstalmentMap;
import com.vedantu.dinero.pojo.InstalmentUpdate;
import com.vedantu.dinero.pojo.InstalmentUpdateReq;
import com.vedantu.dinero.pojo.InstalmentVDiscountChange;
import com.vedantu.dinero.pojo.OrderAmountChangeLog;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.request.ChangeInstalmentDueDateReq;
import com.vedantu.dinero.request.ContextWiseNextDueInstalmentsReq;
import com.vedantu.dinero.request.GetDueInstalmentsForTimeReq;
import com.vedantu.dinero.request.Instalment.GetInstalmentConfigPropsReq;
import com.vedantu.dinero.request.Instalment.GetInstalmentsReq;
import com.vedantu.dinero.request.PayInstalmentReq;
import com.vedantu.dinero.request.SetInstalmentConfigPropsReq;
import com.vedantu.dinero.response.ContextWiseNextDueInstalmentsRes;
import com.vedantu.dinero.response.CounselorDashboardInstalmentInfoRes;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;
import com.vedantu.dinero.response.InstalmentDues;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.PayInstalmentRes;
import com.vedantu.dinero.response.UserDueInstalmentsResp;
import com.vedantu.dinero.serializers.BaseInstalmentDAO;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.InstalmentDAO;
import com.vedantu.dinero.serializers.OrdersDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.dinero.util.InstalmentUtils;
import com.vedantu.dinero.util.PaymentUtils;
import com.vedantu.dinero.util.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.scheduling.response.session.CounselorDashboardSessionsInfoRes;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.request.GetBundleEnrolmentsReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.request.GetCoursePlansReq;
import com.vedantu.subscription.request.RenewSubscriptionForInstalmentReq;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.request.OrderEndType;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.scheduling.SchedulingUtils;
import com.vedantu.util.security.DevicesAllowed;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

/**
 *
 * @author ajith
 */
@Service
public class InstalmentManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    public AccountManager accountManager;

    @Autowired
    public AccountDAO accountDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    public FosUtils fosUtils;
    @Autowired
    private HttpSessionUtils sessionUtils;
    @Autowired
    public OrdersDAO ordersDAO;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    private InstalmentDAO instalmentDAO;

    @Autowired
    private BaseInstalmentDAO baseInstalmentDAO;

    @Autowired
    private InstalmentUtils instalmentUtils;

    @Autowired
    private InstalmentAsyncTasksManager instalmentAsyncTasksManager;

    @Autowired
    public PaymentManager paymentManager;

    @Autowired
    private CouponManager couponManager;

    @Autowired
    private AwsSQSManager sqsManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstalmentManager.class);

    private final String platformEndPoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    @Autowired
    private DozerBeanMapper mapper;

    public InstalmentManager() {
        super();
    }

    public List<InstalmentConfigProps> getInstalmentConfigProps(GetInstalmentConfigPropsReq req) {
        logger.info("Entering " + req.toString());

        Query query = new Query();
        if (req.getPurchaseEntityId() != null) {
            query.addCriteria(Criteria.where("purchaseEntityId").is(req.getPurchaseEntityId()));
        }

        if (req.getPurchaseEntityType() != null) {
            query.addCriteria(Criteria.where("purchaseEntityType").is(req.getPurchaseEntityType()));
        }

        if (req.getInstalmentChargeType() != null) {
            query.addCriteria(Criteria.where("instalmentChargeType").is(req.getInstalmentChargeType()));
        }

        List<InstalmentConfigProps> props = instalmentDAO.getConfigProps(query);
        logger.info("Exiting " + props);
        return props;
    }

    public List<InstalmentConfigProps> getSuitableInstalmentConfigProps(GetInstalmentConfigPropsReq req)
            throws BadRequestException {
        logger.info("Entering " + req.toString());
        List<InstalmentConfigProps> returnProps = new ArrayList<>();
        if (req.getInstalmentChargeType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Instalment charge type not found");
        }

        Query query = new Query();
        if (req.getPurchaseEntityId() != null) {
            query.addCriteria(Criteria.where("purchaseEntityId").is(req.getPurchaseEntityId()));
            query.addCriteria(Criteria.where("instalmentChargeType").is(req.getInstalmentChargeType()));
            List<InstalmentConfigProps> props = instalmentDAO.getConfigProps(query);
            if (props.size() > 0) {
                return props;
            }
        }

        query = new Query();
        query.addCriteria(Criteria.where("instalmentChargeType").is(req.getInstalmentChargeType()));
        query.addCriteria(Criteria.where("purchaseEntityType").is(req.getPurchaseEntityType()));
        query.addCriteria(Criteria.where("purchaseEntityId").exists(false));
        //not a bad way as the number of results are limited.

        List<InstalmentConfigProps> props = instalmentDAO.getConfigProps(query);
        InstalmentConfigProps defaultGlobalProp = null;

        if (ArrayUtils.isNotEmpty(props)) {
            for (InstalmentConfigProps prop : props) {
                if (prop.getPurchaseEntityType() != null
                        && prop.getPurchaseEntityType().equals(req.getPurchaseEntityType())) {
                    returnProps.add(prop);
                    break;
                } else if (prop.getPurchaseEntityType() == null) {
                    defaultGlobalProp = prop;
                }
            }
        }

        if (returnProps.isEmpty() && defaultGlobalProp != null) {
            returnProps.add(defaultGlobalProp);//adding default one if no specific model type of config prop is found
        }
        logger.info("Exiting " + returnProps);
        return returnProps;
    }

    public InstalmentConfigProps setInstalmentConfigProps(SetInstalmentConfigPropsReq req)
            throws BadRequestException, InternalServerErrorException {
        logger.info("Entering " + req.toString());

        Query query = new Query();

        query.addCriteria(Criteria.where("instalmentChargeType").is(req.getInstalmentChargeType()));
        if (req.getPurchaseEntityId() == null && req.getPurchaseEntityType() == null) {
            //creating props at global level
            query.addCriteria(Criteria.where("purchaseEntityType").exists(false));
            query.addCriteria(Criteria.where("purchaseEntityId").exists(false));
        } else if (req.getPurchaseEntityId() == null && req.getPurchaseEntityType() != null) {
            //creating props at otf/oto level
            query.addCriteria(Criteria.where("purchaseEntityType").is(req.getPurchaseEntityType()));
            query.addCriteria(Criteria.where("purchaseEntityId").exists(false));
        } else {
            //creating props at otf/oto and a particular course level
            query.addCriteria(Criteria.where("purchaseEntityType").is(req.getPurchaseEntityType()));
            query.addCriteria(Criteria.where("purchaseEntityId").is(req.getPurchaseEntityId()));
        }
        InstalmentConfigProps result;
        List<InstalmentConfigProps> props = instalmentDAO.getConfigProps(query);
        InstalmentConfigProps prop;

        if (props.size() > 1) {
            prop = props.get(0);
            logger.error("Cannot have more than 1 prop for same combination ", props);
        } else if (props.size() > 0) {
            prop = props.get(0);
        } else {
            prop = new InstalmentConfigProps();
        }
        prop = prop.toInstalmentConfigProps(prop, req);
        instalmentDAO.save(prop, req.getCallingUserId() != null ? req.getCallingUserId().toString() : null);
        result = prop;

        logger.info("Exiting " + result);
        return result;
    }
//Check 2

    public List<Instalment> prepareInstalments(SessionSchedule sessionSchedule,
            Integer hourlyRate, InstalmentPurchaseEntity instalmentPurchaseEntity,
            String instalmentPurchaseEntityId, Integer vedantuDiscountPerHour,
            Integer teacherDiscountPerHour,
            String orderId, String callingUserId)
            throws BadRequestException, InternalServerErrorException, NotFoundException {

        if (sessionSchedule == null) {
            throw new NotFoundException(ErrorCode.SESSION_SCHEDULE_EMPTY, "SESSION_SCHEDULE_EMPTY");
        }

        List<Instalment> instalments = new ArrayList<>();
        Map<Long, InstalmentMap> map = instalmentUtils.getInstalmentMap(sessionSchedule, hourlyRate, vedantuDiscountPerHour, teacherDiscountPerHour);

        Integer totalCostAfterDiscount = 0;
        for (InstalmentMap _map : map.values()) {
            totalCostAfterDiscount += _map.hoursCost;
        }
        logger.info("total cost of order " + totalCostAfterDiscount);
        logger.info("Instalmentmap: " + map);

        //creating instalments
        Integer convenienceCharge = getInstalmentCharge(totalCostAfterDiscount, InstalmentChargeType.CONVENIENCE,
                instalmentPurchaseEntity, instalmentPurchaseEntityId);
        Integer securityCharge = getInstalmentCharge(totalCostAfterDiscount, InstalmentChargeType.SECURITY,
                instalmentPurchaseEntity, instalmentPurchaseEntityId);

        int count = 0;
        SortedSet<Long> keys = new TreeSet<>(map.keySet());
        for (Long key : keys) {
            InstalmentMap instMap = map.get(key);
            Long dueTime = key;
            if (count > 0) {
                convenienceCharge = securityCharge = 0;//adding convenience and security charge on the first 
                //instalment only
            }
            SessionSchedule _scheduleEntry = instMap.sessionSchedule;
            _scheduleEntry.setStartTime(CommonCalendarUtils.getDayStartTime(_scheduleEntry.getStartDate()));//setting to day start time for easy reference
            _scheduleEntry.setEndTime(_scheduleEntry.getEndDate());
            Integer totalAmountToBePaid = instMap.hoursCost + securityCharge + convenienceCharge;
            Instalment instalment = new Instalment(orderId, dueTime, PaymentStatus.NOT_PAID,
                    convenienceCharge, securityCharge, instMap.hoursCost, totalAmountToBePaid,
                    0, 0, _scheduleEntry, instMap.hours, callingUserId, instalmentPurchaseEntity,
                    instalmentPurchaseEntityId);
            instalment.setVedantuDiscountAmount(instMap.vedantuDiscountAmount);
            instalment.setTeacherDiscountAmount(instMap.teacherDiscountAmount);
            instalments.add(instalment);
            count++;
        }

        logger.info("EXIT: " + instalments);
        return instalments;
    }

    public List<Instalment> getInstalmentsForSubscription(GetInstalmentsReq getInstalmentsReq) {
        logger.info("ENTRY: " + getInstalmentsReq);
        Query query = new Query();
        query.addCriteria(Criteria.where("contextId").is(getInstalmentsReq.getSubscriptionId()));
        List<Orders> orders = ordersDAO.getOrders(query);
        List<Instalment> instalments;
        if (ArrayUtils.isNotEmpty(orders)) {
            instalments = getInstalmentsForOrder(orders.get(0).getId());
        } else {
            instalments = new ArrayList<>();
        }
        logger.info("EXIT: " + instalments);
        return instalments;
    }

    public List<Instalment> getInstalmentsForOTFBatch(GetInstalmentsReq getInstalmentsReq) {
        logger.info("ENTRY: " + getInstalmentsReq);
        Query query = new Query();
        query.addCriteria(Criteria.where("items")
                .elemMatch(Criteria.where("entityId").is(getInstalmentsReq.getBatchId())));
        query.addCriteria(Criteria.where("userId").is(getInstalmentsReq.getUserId()));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAID, PaymentStatus.PAYMENT_SUSPENDED)));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_TYPE).is(PaymentType.INSTALMENT));
        List<Orders> orders = ordersDAO.getOrders(query);
        List<Instalment> instalments;
        if (ArrayUtils.isNotEmpty(orders)) {
            instalments = getInstalmentsForOrder(orders.get(0).getId());
        } else {
            instalments = new ArrayList<>();
        }
        logger.info("EXIT: " + instalments);
        return instalments;
    }

    public List<Instalment> getInstalmentsForOrder(String orderId) {
        return getInstalmentsForOrder(orderId, null);
    }

    public List<Instalment> getInstalmentsForOrder(String orderId, PaymentStatus paymentStatus) {
        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(orderId));
        if (paymentStatus != null) {
            query.addCriteria(Criteria.where("paymentStatus").is(paymentStatus));
        }
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        return instalments;
    }

    public List<Instalment> getInstalmentsForOrders(List<String> orderIds, PaymentStatus paymentStatus) {
        Query query = new Query();
        if (ArrayUtils.isNotEmpty(orderIds)) {
            query.addCriteria(Criteria.where("orderId").in(orderIds));
        }
        if (paymentStatus != null) {
            query.addCriteria(Criteria.where("paymentStatus").is(paymentStatus));
        }
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        return instalments;
    }

    public List<InstalmentDues> getDueInstalments(int start, int limit) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(
                match(Criteria.where("paymentStatus").is(PaymentStatus.NOT_PAID.name())),
                match(Criteria.where(Instalment.Constants.ENTITY_STATE).is(EntityState.ACTIVE.toString())),
                group("orderId").min("dueTime").as("dueTime"),
                project("dueTime").and("orderId").previousOperation(),
                skip(start),
                limit(limit)).withOptions(aggregationOptions);
        AggregationResults<InstalmentDues> groupResults
                = instalmentDAO.getMongoOperations().aggregate(agg, Instalment.class.getSimpleName(), InstalmentDues.class);
        List<InstalmentDues> result = groupResults.getMappedResults();
        logger.info("EXIT " + result);
        return result;
    }

    public Instalment getInstalment(String instalmentId) throws NotFoundException {
        logger.info("ENTRY: " + instalmentId);
        Instalment instalment = instalmentDAO.getEntityById(instalmentId, Instalment.class);
        logger.info("EXIT: " + instalment);
        if (instalment == null) {
            throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND, "Instalment not found");
        }
        return instalment;
    }

    public Instalment getInstalment(String orderId, Long dueTime) throws NotFoundException {
        logger.info("ENTRY: orderId :" + orderId + ", dueTime " + dueTime);
        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(orderId));
        query.addCriteria(Criteria.where("dueTime").is(dueTime));
        query.addCriteria(Criteria.where(Instalment.Constants.ENTITY_STATE).is(EntityState.ACTIVE.toString()));

        List<Instalment> instalments = instalmentDAO.runQuery(query, Instalment.class);

        if (ArrayUtils.isEmpty(instalments)) {
            throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND, "Instalment not found");
        }
        logger.info("EXIT: " + instalments);
        return instalments.get(0);
    }

    public PayInstalmentRes payInstalment(PayInstalmentReq payInstalmentReq, DevicesAllowed device) throws NotFoundException,
            InternalServerErrorException,
            ConflictException,
            ForbiddenException,
            BadRequestException,
            VException {
        //TODO make sure the instalment belongs to the user
        logger.info("ENTRY: " + payInstalmentReq);
        Instalment instalment = instalmentDAO.getEntityById(payInstalmentReq.getInstalmentId(), Instalment.class);
        logger.info("Instalment to pay for " + instalment);
        if (instalment == null) {
            throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND, "Instalment not found");
        } else if (PaymentStatus.PAID.equals(instalment.getPaymentStatus())) {
            throw new ForbiddenException(ErrorCode.ALREADY_PAID, "Instalment already paid");
        }
        /*
        //Allowing instalment old flow for now
        if (!Boolean.TRUE.equals(payInstalmentReq.getNewFlow())
                && InstalmentPurchaseEntity.BATCH.equals(instalment.getContextType())) {
            throw new ConflictException(ErrorCode.FORBIDDEN_ERROR, "Only new Flow allowed for batch/bundle");
        }
         */

        Orders order = ordersDAO.getById(instalment.getOrderId());
        if (order == null) {
            logger.error("ERROR: Order not found for instalment " + instalment);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "no order found");
        }

        //TODO ensure that other subsystems calling this api should have authorized checks
//        if (!order.getUserId().equals(payInstalmentReq.getCallingUserId())) {
//            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, payInstalmentReq.getUserId()
//                    + " cannot pay for instalment of student Id " + order.getUserId());
//        }
        if (!order.getUserId().equals(payInstalmentReq.getCallingUserId())) {
            payInstalmentReq.setCallingUserId(order.getUserId());
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(instalment.getOrderId()));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        if (ArrayUtils.isNotEmpty(instalments)) {
            Collections.sort(instalments, new Comparator<Instalment>() {
                @Override
                public int compare(Instalment o1, Instalment o2) {
                    return o1.getDueTime().compareTo(o2.getDueTime());
                }
            });
            boolean foundNotPaid = false;
            for (Instalment _inst : instalments) {
                if (_inst.getId().equals(instalment.getId())) {
                    break;
                }
                if (PaymentStatus.PAYMENT_SUSPENDED.equals(_inst.getPaymentStatus()) || PaymentStatus.NOT_PAID.equals(_inst.getPaymentStatus())) {
                    foundNotPaid = true;
                }
            }
            if (foundNotPaid) {
                throw new ConflictException(ErrorCode.INSTALMENT_NOT_PAID_IN_ORDER, "Instalment not paid in order");
            }
        }

        Integer promotionalAmtToBePaid = 0;
        Integer nonPromotionalAmtToBePaid = 0;
        Account account = accountManager.getAccountByUserId(payInstalmentReq.getCallingUserId());
        Integer promotionalBalance = account.getPromotionalBalance();
        if (instalment.getHoursCost() < promotionalBalance) {
            promotionalAmtToBePaid = instalment.getHoursCost();
        } else {
            promotionalAmtToBePaid = promotionalBalance;
            nonPromotionalAmtToBePaid = instalment.getHoursCost() - promotionalBalance;
        }
        nonPromotionalAmtToBePaid += instalment.getConvenienceCharge() + instalment.getSecurityCharge();

        //putting the promotional and non promotional amount to be paid in instalment
//        instalment.setTotalNonPromotionalAmount(nonPromotionalAmtToBePaid);
//        instalment.setTotalPromotionalAmount(promotionalAmtToBePaid);
        instalment.setTriggeredBy(payInstalmentReq.getCallingUserId() != null ? payInstalmentReq.getCallingUserId().toString() : null);
        //instalment.setLastUpdatedBy(instalment.getTriggeredBy());
        instalmentDAO.save(instalment, instalment.getTriggeredBy());

        Integer rechargeAmount;
        PayInstalmentRes payInstalmentRes = new PayInstalmentRes((nonPromotionalAmtToBePaid + promotionalAmtToBePaid), false);
        if (payInstalmentReq.getUseAccountBalance()) {
            if (account.getNonPromotionalBalance() < nonPromotionalAmtToBePaid) {
                rechargeAmount = nonPromotionalAmtToBePaid - account.getNonPromotionalBalance();
            } else {
                // transaction will be commited inside processOrder
                try {
                    if (!Boolean.TRUE.equals(payInstalmentReq.getNewFlow())) {
                        processInstalmentPayment(instalment, account);
                    }
                } catch (Exception e) {
                    logger.error("Error in process instalment payment " + instalment + " error " + e.getMessage());
                }
                instalment = instalmentDAO.getEntityById(payInstalmentReq.getInstalmentId(), Instalment.class);
                payInstalmentRes.setPaymentStatus(PaymentStatus.PAID);
                payInstalmentRes.setInstalmentInfo(mapper.map(instalment, InstalmentInfo.class));
                return payInstalmentRes;
            }
        } else {
            rechargeAmount = nonPromotionalAmtToBePaid + promotionalAmtToBePaid;
        }

        rechargeAmount = ((int) Math.ceil((double) rechargeAmount / 100)) * 100;
        RechargeUrlInfo rechargeUrlInfo = accountManager.rechargeAccount(payInstalmentReq.getCallingUserId(),
                payInstalmentReq.getIpAddress(),
                rechargeAmount, instalment.getId(),
                PaymentType.INSTALMENT, payInstalmentReq.getRedirectUrl(),
                null, null, payInstalmentReq.getCallingUserId(), device);
        payInstalmentRes.setNeedRecharge(true);
        payInstalmentRes.setGatewayName(rechargeUrlInfo.getGatewayName());
        payInstalmentRes.setRechargeUrl(rechargeUrlInfo.getRechargeUrl());
        payInstalmentRes.setForwardHttpMethod(rechargeUrlInfo.getForwardHttpMethod());
        if (rechargeUrlInfo.getPostParams() != null && !rechargeUrlInfo.getPostParams().isEmpty()) {
            payInstalmentRes.setPostParams(rechargeUrlInfo.getPostParams());
        }
        logger.info("EXIT: " + payInstalmentRes);

        return payInstalmentRes;
    }

    public void processInstalmentPayment(Instalment instalment, Account userAccount) throws InternalServerErrorException,
            NotFoundException,
            BadRequestException,
            ConflictException,
            VException,
            CloneNotSupportedException {
        logger.info("ENTRY instalmentId " + instalment.getId());
        Orders order = ordersDAO.getById(instalment.getOrderId());
        if (order == null || ArrayUtils.isEmpty(order.getItems())) {
            logger.error("ERROR: Order not found for instalment " + instalment);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "no order found or items zero with orderid["
                    + instalment.getOrderId() + "]");
        }
        OrderedItem item = order.getItems().get(0);

        //creating entities
        if (EntityType.PLAN.equals(item.getEntityType())) {

            //adding remaining hrs to subscriptiondetails
            Long hoursInMillis = PaymentUtils.getRequestedHours(instalment.getSessionSchedule());
            SubscriptionResponse subscriptionResponse = addOrUpdateHrsForInstalment(instalment,
                    item, hoursInMillis, null);
            //setting the same subscriptiondetailsId as DeliverableEntityId
            instalment.setDeliverableEntityId(item.getDeliverableEntityId());

            //async op for scheduling sessions and adding blocking as availability
            instalmentAsyncTasksManager.scheduleSessionsForInstalment(instalment, order, item, subscriptionResponse);

        } else if (EntityType.OTF.equals(item.getEntityType())) {
            Boolean orderState = resetOrderInstallmentStatePostPayment(item.getEntityId(), item.getEntityType(), order.getUserId().toString(), null, instalment.getId());
            if (!Boolean.FALSE.equals(orderState)) {

                logger.info("making the enrollment active for batchId " + item.getEntityId());
                String updateEnrollmentUrl = SUBSCRIPTION_ENDPOINT + "/"
                        + ConfigUtils.INSTANCE.getStringValue("otf.markstatus");
                JSONObject updateEnrollmentReq = new JSONObject();
                updateEnrollmentReq.put("batchId", item.getEntityId());
                updateEnrollmentReq.put("userId", order.getUserId());
                updateEnrollmentReq.put("status", "ACTIVE");
                updateEnrollmentReq.put("role", "STUDENT");
                updateEnrollmentReq.put("id", item.getDeliverableEntityId());

                ClientResponse resp = WebUtils.INSTANCE.doCall(updateEnrollmentUrl, HttpMethod.POST,
                        updateEnrollmentReq.toString(), true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
                String updateEnrollmentRes = resp.getEntity(String.class);
                logger.info("Response of otf.markstatus " + updateEnrollmentRes);
                if (instalment.getDueTime() < System.currentTimeMillis()) {
                    logger.info("the user has paid the instalment post due time, blocking the calendar " + instalment);
                    ClientResponse resp2 = WebUtils.INSTANCE.doCall(platformEndPoint + "/onetofew/enroll/updateCalendar", HttpMethod.POST,
                            updateEnrollmentRes, true);
                    VExceptionFactory.INSTANCE.parseAndThrowException(resp2, true);
                }
            }
        }

        //transferring the money from student account to vedantu accounts
        Integer instHrsPromotionalAmount = 0;
        Integer instHrsNonPromotionalAmount = 0;
        if (instalment.getHoursCost() < userAccount.getPromotionalBalance()) {
            instHrsPromotionalAmount = instalment.getHoursCost();
        } else {
            instHrsPromotionalAmount = userAccount.getPromotionalBalance();
            instHrsNonPromotionalAmount = instalment.getHoursCost()
                    - userAccount.getPromotionalBalance();
        }
        Integer totalNonPromotionalAmountToBePaid = instHrsNonPromotionalAmount
                + instalment.getConvenienceCharge() + instalment.getSecurityCharge();
        String uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.ACCOUNT_DEDUCTION + "_" + instalment.getId();

        accountManager.transferAmount(userAccount, accountDAO.getVedantuDefaultAccount(),
                (instalment.getHoursCost() + instalment.getSecurityCharge()),
                instHrsPromotionalAmount, (instHrsNonPromotionalAmount + instalment.getSecurityCharge()),
                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                TransactionRefType.INSTALMENT, instalment.getId(),
                Transaction._getTriggredByUser(Long.parseLong(instalment.getTriggeredBy())), false, uniqueId);

        //convenience charge to revenue wallet,going with the same ref and ref nos
        if (instalment.getConvenienceCharge() != null && instalment.getConvenienceCharge() > 0) {
            uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.ACCOUNT_DEDUCTION.name()
                    + "_CONVENIENCE_CHARGE_" + instalment.getId();
            accountManager.transferAmount(userAccount, accountDAO.getVedantuCutAccount(),
                    instalment.getConvenienceCharge(), 0, instalment.getConvenienceCharge(),
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE,
                    BillingReasonType.ACCOUNT_DEDUCTION,
                    TransactionRefType.INSTALMENT, instalment.getId(),
                    Transaction._getTriggredByUser(Long.parseLong(instalment.getTriggeredBy())), false, uniqueId);
        }

        if (instalment.getVedantuDiscountAmount() != null && instalment.getVedantuDiscountAmount() > 0l) {
            uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.DISCOUNT.name() + "_" + instalment.getId();
            accountManager.transferAmount(accountDAO.getVedantuFreebiesAccount(), accountDAO.getVedantuDefaultAccount(),
                    instalment.getVedantuDiscountAmount(), instalment.getVedantuDiscountAmount(), 0,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.DISCOUNT,
                    TransactionRefType.INSTALMENT, instalment.getId(), Transaction._getTriggredByUser(Long.parseLong(instalment.getTriggeredBy())),
                    true, uniqueId);
        }

        instalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.PAID);
        instalment.setPaidTime(System.currentTimeMillis());
        instalment.setTotalNonPromotionalAmount(totalNonPromotionalAmountToBePaid);
        instalment.setTotalPromotionalAmount(instHrsPromotionalAmount);
        instalmentDAO.save(instalment);

        order.updateOrderPaidDetails(order, instalment);

        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(order.getId()));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        updateOrderPojo(order, instalments);

        //finally upsert the order
        ordersDAO.create(order);
        paymentManager.triggerOrderForLeadSquare(order, false);

        //update the blocked slots for instalments
        //we will use first deliverableEntityId till the reliance on renew subscription is removed
        if (item.getEntityType().equals(EntityType.PLAN)) {
            instalmentAsyncTasksManager.updateBlockedInstalmentSlots(instalments, instalment, item.getDeliverableEntityId());
            instalmentAsyncTasksManager.sendInstalmentPaidEmailForSubscription(instalments, order.getContextId());
        } else if (EntityType.OTF.equals(item.getEntityType())) {
            instalmentAsyncTasksManager.sendInstalmentPaidEmailForOTF(instalments, item.getEntityId(), order.getUserId());
        }
        paymentManager.triggerOrderUpdatedForUser(order);
        logger.info("EXIT");
    }

    public List<Instalment> processInstalmentPaymentNew(String instalmentId) throws InternalServerErrorException,
            NotFoundException,
            BadRequestException,
            ConflictException,
            VException,
            CloneNotSupportedException {
        Instalment instalment = instalmentDAO.getEntityById(instalmentId, Instalment.class);
        if (instalment == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "instalment not found for id " + instalmentId);
        }

        Orders order = ordersDAO.getById(instalment.getOrderId());
        if (order == null || ArrayUtils.isEmpty(order.getItems())) {
            logger.error("ERROR: Order not found for instalment " + instalment);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "no order found or items zero with orderid["
                    + instalment.getOrderId() + "]");
        }
        Account userAccount = accountManager.getAccountByUserId(order.getUserId());

        //transferring the money from student account to vedantu accounts
        Integer instHrsPromotionalAmount = 0;
        Integer instHrsNonPromotionalAmount = 0;
        if (instalment.getHoursCost() < userAccount.getPromotionalBalance()) {
            instHrsPromotionalAmount = instalment.getHoursCost();
        } else {
            instHrsPromotionalAmount = userAccount.getPromotionalBalance();
            instHrsNonPromotionalAmount = instalment.getHoursCost()
                    - userAccount.getPromotionalBalance();
        }
        Integer totalNonPromotionalAmountToBePaid = instHrsNonPromotionalAmount
                + instalment.getConvenienceCharge() + instalment.getSecurityCharge();

        String uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.ACCOUNT_DEDUCTION.name() +
                          "_" + instalment.getId();
        accountManager.transferAmount(userAccount, accountDAO.getVedantuDefaultAccount(),
                (instalment.getHoursCost() + instalment.getSecurityCharge()),
                instHrsPromotionalAmount, (instHrsNonPromotionalAmount + instalment.getSecurityCharge()),
                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                TransactionRefType.INSTALMENT, instalment.getId(),
                Transaction._getTriggredByUser(Long.parseLong(instalment.getTriggeredBy())), false, uniqueId);

        //convenience charge to revenue wallet,going with the same ref and ref nos
        if (instalment.getConvenienceCharge() != null && instalment.getConvenienceCharge() > 0) {
            uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.ACCOUNT_DEDUCTION.name()
                    + "_CONVENIENCE_CHARGE_" + instalment.getId();
            accountManager.transferAmount(userAccount, accountDAO.getVedantuCutAccount(),
                    instalment.getConvenienceCharge(), 0, instalment.getConvenienceCharge(),
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE,
                    BillingReasonType.ACCOUNT_DEDUCTION,
                    TransactionRefType.INSTALMENT, instalment.getId(),
                    Transaction._getTriggredByUser(Long.parseLong(instalment.getTriggeredBy())), false, uniqueId);
        }

        if (instalment.getVedantuDiscountAmount() != null && instalment.getVedantuDiscountAmount() > 0l) {
            uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.DISCOUNT.name() + "_" + instalment.getId();
            accountManager.transferAmount(accountDAO.getVedantuFreebiesAccount(), accountDAO.getVedantuDefaultAccount(),
                    instalment.getVedantuDiscountAmount(), instalment.getVedantuDiscountAmount(), 0,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.DISCOUNT,
                    TransactionRefType.INSTALMENT, instalment.getId(), Transaction._getTriggredByUser(Long.parseLong(instalment.getTriggeredBy())),
                    true, uniqueId);
        }

        instalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.PAID);
        instalment.setPaidTime(System.currentTimeMillis());
        instalment.setTotalNonPromotionalAmount(totalNonPromotionalAmountToBePaid);
        instalment.setTotalPromotionalAmount(instHrsPromotionalAmount);
        instalmentDAO.save(instalment);

        order.updateOrderPaidDetails(order, instalment);

        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(order.getId()));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        updateOrderPojo(order, instalments);

        //finally upsert the order
        ordersDAO.create(order);
        paymentManager.triggerOrderForLeadSquare(order, false);

        paymentManager.triggerOrderUpdatedForUser(order);

        return instalments;
    }

    public SubscriptionResponse addOrUpdateHrsForInstalment(Instalment instalment, OrderedItem item,
            Long millisToAdd, Long millisToLock) throws VException {
        logger.info("Adding/updating hours to subscriptionDetails " + item.getDeliverableEntityId());
        String renewSubscriptionForInstalmentUrl = SUBSCRIPTION_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("vedantu.platform.subscription.addorUpdateHrsForInstalment");
        RenewSubscriptionForInstalmentReq renewSubscriptionForInstalmentReq = new RenewSubscriptionForInstalmentReq();
        renewSubscriptionForInstalmentReq.setSubscriptionDetailsId(Long.parseLong(item.getDeliverableEntityId()));
        renewSubscriptionForInstalmentReq.setCallingUserId(Long.parseLong(instalment.getTriggeredBy()));
        renewSubscriptionForInstalmentReq.setInstalmentId(instalment.getId());
        if (millisToAdd != null) {
            renewSubscriptionForInstalmentReq.setMillisToAdd(millisToAdd);
        } else if (millisToLock != null) {
            renewSubscriptionForInstalmentReq.setMillisToLock(millisToLock);
        }

        logger.info("Request  " + renewSubscriptionForInstalmentReq);

        ClientResponse resp = WebUtils.INSTANCE.doCall(renewSubscriptionForInstalmentUrl, HttpMethod.POST,
                new Gson().toJson(renewSubscriptionForInstalmentReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
        String jsonString = resp.getEntity(String.class);
        SubscriptionResponse subscriptionResponse = new Gson().fromJson(jsonString, SubscriptionResponse.class);
        logger.info("Response " + subscriptionResponse);
        return subscriptionResponse;
    }

    //utils
    public Integer getInstalmentCharge(Integer totalCost, InstalmentChargeType instalmentChargeType,
            InstalmentPurchaseEntity instalmentPurchaseEntity,
            String purchaseEntityId) throws BadRequestException {
        Integer charge = 0;
        GetInstalmentConfigPropsReq configPropsReq = new GetInstalmentConfigPropsReq();
        configPropsReq.setInstalmentChargeType(instalmentChargeType);
        configPropsReq.setPurchaseEntityId(purchaseEntityId);
        configPropsReq.setPurchaseEntityType(instalmentPurchaseEntity);
        List<InstalmentConfigProps> configProps = getSuitableInstalmentConfigProps(configPropsReq);
        if (configProps.size() > 0) {
            InstalmentConfigProps configProp = configProps.get(0);
            if (configProp.getChargeType().equals(RedeemType.FIXED)) {
                charge = configProp.getChargeValue();
            } else if (configProp.getChargeType().equals(RedeemType.PERCENTAGE)) {
                charge = configProp.getChargeValue() * totalCost / 100;
            }
        }
        return charge;
    }

    public Orders updateOrderPojo(Orders order, List<Instalment> instalments) {
        logger.info("ENTRY: ordeId " + order.getId() + " Instalments " + instalments);
        if (ArrayUtils.isEmpty(instalments)) {
            return order;
        }
        boolean allPaid = true;
        for (Instalment instalment : instalments) {
            if (!PaymentStatus.PAID.equals(instalment.getPaymentStatus())) {
                allPaid = false;
                break;
            }
        }
        if (allPaid) {
            order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PAID, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//            order.setPaymentStatus(PaymentStatus.PAID);
            order.setPromotionalAmount(order.getPromotionalAmountPaid());
            order.setNonPromotionalAmount(order.getNonPromotionalAmountPaid());
            if (order.getItems().size() > 0) {
                OrderedItem item = order.getItems().get(0);
                item.setPromotionalCost(order.getPromotionalAmount());
                item.setNonPromotionalCost(order.getNonPromotionalAmountPaid());
            }
        }
        logger.info("EXIT: " + order);
        return order;
    }

    public Boolean markOrderForfeitedForEntityId(String entityId, EntityType entityType, String userId) throws NotFoundException {
        Query query = new Query();
        query.addCriteria(Criteria.where("items.entityId").is(entityId));
        query.addCriteria(Criteria.where("items.entityType").is(entityType));
        query.addCriteria(Criteria.where("userId").is(Long.parseLong(userId)));
        query.addCriteria(Criteria.where("paymentStatus").is(PaymentStatus.PARTIALLY_PAID));
        List<Orders> orders = ordersDAO.getOrders(query);
        if (ArrayUtils.isNotEmpty(orders)) {
            if (orders.size() > 1) {
                logger.error("Alert! multiple orders found for same entityId,userId and they are requested for forfeiting entityId "
                        + entityId + ", userId " + userId);
            }
            //in loop as many cases this will be 1 entity only, if more will think of a different approach
            for (Orders order : orders) {
                _markOrderForfeited(order);
            }
        }
        return true;
    }

    public Boolean markOrderForfeited(String orderId) throws NotFoundException {
        logger.info("ENTRY: " + orderId);
        Orders order = ordersDAO.getById(orderId);
        return _markOrderForfeited(order);
    }

    public Boolean _markOrderForfeited(Orders order) throws NotFoundException {
        if (order == null) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "Order not found");
        }

        if (PaymentStatus.PARTIALLY_PAID.equals(order.getPaymentStatus())
                && PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PAYMENT_SUSPENDED, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//            order.setPaymentStatus(PaymentStatus.PAYMENT_SUSPENDED);
            ordersDAO.create(order);
            paymentManager.triggerOrderForLeadSquare(order, false);
            Query query = new Query();
            query.addCriteria(Criteria.where("orderId").is(order.getId()));
            query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
            List<Instalment> instalments = instalmentDAO.getInstalments(query);
            boolean foundNotPaid = false;
            if (ArrayUtils.isNotEmpty(instalments)) {
                for (Instalment instalment : instalments) {
                    if (PaymentStatus.PAYMENT_SUSPENDED.equals(instalment.getPaymentStatus())
                            || PaymentStatus.NOT_PAID.equals(instalment.getPaymentStatus())) {
                        foundNotPaid = true;
                        instalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.PAYMENT_SUSPENDED);
                        instalmentDAO.save(instalment);
                    }
                }
                if (!foundNotPaid) {
                    logger.error("Order is not paid but all the instalments are paid, orderId "
                            + order.getId());
                }
            }
        }
        logger.info("EXIT ");
        return true;
    }

    public BaseInstalment getBaseInstalment(InstalmentPurchaseEntity instalmentPurchaseEntity, String purchaseEntityId, Long userId) {
        return baseInstalmentDAO.getBaseInstalment(instalmentPurchaseEntity, purchaseEntityId, userId);
    }

    public List<BaseInstalment> getBaseInstalments(InstalmentPurchaseEntity instalmentPurchaseEntity, List<String> purchaseEntityIds) {
        return baseInstalmentDAO.getBaseInstalments(instalmentPurchaseEntity, purchaseEntityIds);
    }

    //this method is only for updateBaseInstalment for higher level entitytype and entityId and no user is present
    public void updateBaseInstalment(BaseInstalment baseInstalment) {
        BaseInstalment oldBaseInstalment = baseInstalmentDAO.getBaseInstalment(baseInstalment.getPurchaseEntityType(),
                baseInstalment.getPurchaseEntityId(), null);
        logger.info("oldBaseInstalment: " + oldBaseInstalment);
        if (oldBaseInstalment == null) {
            if (CollectionUtils.isNotEmpty(baseInstalment.getInfo())) {
                baseInstalmentDAO.save(baseInstalment);
            }
        } else {
            oldBaseInstalment.setInfo(baseInstalment.getInfo());
            baseInstalmentDAO.save(oldBaseInstalment);
        }
    }

    public void addEditStudentBaseInstalment(BaseInstalment baseInstalment) throws VException {
        if (baseInstalment.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is not present");
        }

        if (StringUtils.isEmpty(baseInstalment.getPurchaseEntityId())
                || baseInstalment.getPurchaseEntityType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "purchaseEntityId/purchaseEntityType is not present");
        }

        if (ArrayUtils.isEmpty(baseInstalment.getInfo())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "instalments  not present");
        }
        Integer totalInstalmentAmount = validateBaseInstalment(baseInstalment.getInfo());

        switch (baseInstalment.getPurchaseEntityType()) {
            case BATCH:
                String url = SUBSCRIPTION_ENDPOINT
                        + "/batch/getBatchPurchasePrice?batchId=" + baseInstalment.getPurchaseEntityId();
                ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                PlatformBasicResponse platformBasicResponse = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
                Integer price = Integer.parseInt(platformBasicResponse.getResponse());
                if (totalInstalmentAmount < price) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "totalInstalmentAmount not matching "
                            + baseInstalment.getPurchaseEntityType() + " price " + price);
                }
                break;
            case BUNDLE:
                ClientResponse resp2 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                        + "/bundle/" + baseInstalment.getPurchaseEntityId(), HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp2);
                String jsonString2 = resp2.getEntity(String.class);
                BundleInfo bundleInfo = new Gson().fromJson(jsonString2, BundleInfo.class);
                if (totalInstalmentAmount < bundleInfo.getPrice()) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "totalInstalmentAmount not matching "
                            + baseInstalment.getPurchaseEntityType() + " price " + bundleInfo.getPrice());
                }
                break;
            case OTF_BUNDLE:
                ClientResponse resp3 = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/otfBundle/" + baseInstalment.getPurchaseEntityId(), HttpMethod.GET, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp3);
                String jsonString3 = resp3.getEntity(String.class);
                OTFBundleInfo info = new Gson().fromJson(jsonString3, OTFBundleInfo.class);
                if (totalInstalmentAmount < info.getPrice()) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "totalInstalmentAmount not matching "
                            + baseInstalment.getPurchaseEntityType() + " price " + info.getPrice());
                }
                break;
            default:
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "not allowed to create student level instalment for " + baseInstalment.getPurchaseEntityType());

        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("baseInstalment", baseInstalment);
        logger.info("sending base instalment save request to baseInstalmentOps queue");
        logger.info("payload for baseInstalment request : " + payload.toString());
        sqsManager.sendToSQS(SQSQueue.BASE_INSTALMENT_OPS, SQSMessageType.BASE_INSTALMENT_SAVE, new Gson().toJson(payload));
    }

    public Integer validateBaseInstalment(List<BaseInstalmentInfo> baseInstalmentInfos) throws BadRequestException {
        int count = 0;

        if (baseInstalmentInfos == null) {
            return 0;
        }

        Set<Long> installmentDueTimes = new HashSet<>();
        int totalAmount = 0;
        for (BaseInstalmentInfo baseInstalmentInfo : baseInstalmentInfos) {
            if (count != 0 && (baseInstalmentInfo.getDueTime() == null || baseInstalmentInfo.getDueTime() <= 0L
                    || baseInstalmentInfo.getDueTime() <= System.currentTimeMillis())) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Due date must be specified or is in the past");
            }
            if (baseInstalmentInfo.getAmount() == null || baseInstalmentInfo.getAmount() <= 0) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "amount not specified");
            }
            totalAmount += baseInstalmentInfo.getAmount();
            installmentDueTimes.add(baseInstalmentInfo.getDueTime());
            count++;
        }
        if (installmentDueTimes.size() != baseInstalmentInfos.size()) {
            throw new BadRequestException(ErrorCode.SAME_DUE_TIME_FOR_INSTALMENTS_NOT_ALLOWED, "Due time can't be same for two installments");
        }
        return totalAmount;
    }

//Check 1 - Def add here//Done
    public List<Instalment> prepareInstalments(InstalmentPurchaseEntity instalmentPurchaseEntity,
            String instalmentPurchaseEntityId, Integer vedantuDiscountAmount,
            Integer teacherDiscountAmount,
            String orderId, String callingUserId)
            throws BadRequestException, InternalServerErrorException, NotFoundException, VException {

        BaseInstalment baseInstalment = null;
        if (InstalmentPurchaseEntity.BATCH.equals(instalmentPurchaseEntity) || InstalmentPurchaseEntity.OTF_BUNDLE.equals(instalmentPurchaseEntity)) {
            if (StringUtils.isNotEmpty(instalmentPurchaseEntityId) && StringUtils.isNotEmpty(callingUserId)) {
                List<BaseInstalmentInfo> baseInstalmentInfos = getBaseInstalmentFromRegistration(Long.parseLong(callingUserId), instalmentPurchaseEntityId);
                if (ArrayUtils.isNotEmpty(baseInstalmentInfos)) {
                    baseInstalment = new BaseInstalment();
                    baseInstalment.setPurchaseEntityId(instalmentPurchaseEntityId);
                    baseInstalment.setPurchaseEntityType(instalmentPurchaseEntity);
                    baseInstalment.setInfo(baseInstalmentInfos);
                }
            }
        }
        if (baseInstalment == null) {
            baseInstalment = getBaseInstalment(instalmentPurchaseEntity, instalmentPurchaseEntityId, (StringUtils.isNotEmpty(callingUserId) ? Long.parseLong(callingUserId) : null));
        }

        if (baseInstalment == null || baseInstalment.getInfo() == null || baseInstalment.getInfo().isEmpty()) {
            throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND, "BaseInstalment not found");
        }

        //Integer discountPerInstalment = totalDiscount/baseInstalment.getInfo().size();
        List<Instalment> instalments = new ArrayList<>();
        //Map<Long, InstalmentMap> map = instalmentUtils.getInstalmentMap(baseInstalment, totalDiscount);

        List<BaseInstalmentInfoWithDiscounts> baseInstalmentInfos = updateBaseInstalmentInfosWithDiscount(baseInstalment.getInfo(), vedantuDiscountAmount, teacherDiscountAmount);

        Integer totalCostAfterDiscount = 0;
        for (BaseInstalmentInfoWithDiscounts baseInstalmentInfo : baseInstalmentInfos) {
            //baseInstalmentInfo.setAmount(baseInstalmentInfo.getAmount()-discountPerInstalment);
            totalCostAfterDiscount += baseInstalmentInfo.getAmount();
        }
        logger.info("total cost of order " + totalCostAfterDiscount);
        //logger.info("Instalmentmap: " + map);

        //creating instalments
        Integer convenienceCharge = getInstalmentCharge(totalCostAfterDiscount, InstalmentChargeType.CONVENIENCE,
                instalmentPurchaseEntity, instalmentPurchaseEntityId);
        Integer securityCharge = getInstalmentCharge(totalCostAfterDiscount, InstalmentChargeType.SECURITY,
                instalmentPurchaseEntity, instalmentPurchaseEntityId);

        Collections.sort(baseInstalmentInfos, new Comparator<BaseInstalmentInfo>() {
            @Override
            public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });

        int count = 0;
        //SortedSet<Long> keys = new TreeSet<>(map.keySet());
        for (BaseInstalmentInfoWithDiscounts baseInstalmentInfo : baseInstalmentInfos) {
            //InstalmentMap instMap = map.get(key);
            //Long dueTime = key;
            if (count > 0) {
                convenienceCharge = securityCharge = 0;//adding convenience and security charge on the first 
                //instalment only
            }
            //SessionSchedule _scheduleEntry = instMap.sessionSchedule;
            //_scheduleEntry.setStartTime(CommonCalendarUtils.getDayStartTime(_scheduleEntry.getStartDate()));//setting to day start time for easy reference
            //_scheduleEntry.setEndTime(_scheduleEntry.getEndDate());
            Integer totalAmountToBePaid = baseInstalmentInfo.getAmount() + securityCharge + convenienceCharge;
            Instalment instalment = new Instalment(orderId, baseInstalmentInfo.getDueTime(), PaymentStatus.NOT_PAID,
                    convenienceCharge, securityCharge, baseInstalmentInfo.getAmount(), totalAmountToBePaid,
                    0, 0, null, null, callingUserId, instalmentPurchaseEntity, instalmentPurchaseEntityId);
            instalment.setVedantuDiscountAmount(baseInstalmentInfo.getVedantuDiscountAmount());
            instalment.setTeacherDiscountAmount(baseInstalmentInfo.getTeacherDiscountAmount());
            instalments.add(instalment);
            count++;
        }

        logger.info("EXIT: " + instalments);
        return instalments;
    }

    public List<Instalment> prepareInstalments(List<BaseInstalmentInfo> baseInstalmentInfos,
            InstalmentPurchaseEntity instalmentPurchaseEntity,
            String instalmentPurchaseEntityId, Integer vedantuDiscountAmount,
            Integer teacherDiscountAmount,
            String orderId, String callingUserId)
            throws BadRequestException, InternalServerErrorException, NotFoundException {

        List<Instalment> instalments = new ArrayList<>();

        List<BaseInstalmentInfoWithDiscounts> baseInstalmentInfosUpdated = updateBaseInstalmentInfosWithDiscount(baseInstalmentInfos, vedantuDiscountAmount, teacherDiscountAmount);

        Integer totalCostAfterDiscount = 0;
        for (BaseInstalmentInfoWithDiscounts baseInstalmentInfo : baseInstalmentInfosUpdated) {
            totalCostAfterDiscount += baseInstalmentInfo.getAmount();
        }
        logger.info("total cost of order after discount " + totalCostAfterDiscount);

        //creating instalments
        Integer convenienceCharge = getInstalmentCharge(totalCostAfterDiscount, InstalmentChargeType.CONVENIENCE,
                instalmentPurchaseEntity, instalmentPurchaseEntityId);
        Integer securityCharge = getInstalmentCharge(totalCostAfterDiscount, InstalmentChargeType.SECURITY,
                instalmentPurchaseEntity, instalmentPurchaseEntityId);

        Collections.sort(baseInstalmentInfosUpdated, new Comparator<BaseInstalmentInfo>() {
            @Override
            public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });

        int count = 0;
        for (BaseInstalmentInfoWithDiscounts baseInstalmentInfo : baseInstalmentInfosUpdated) {
            if (count > 0) {
                convenienceCharge = securityCharge = 0;//adding convenience and security charge on the first 
                //instalment only
            }
            Integer totalAmountToBePaid = baseInstalmentInfo.getAmount() + securityCharge + convenienceCharge;
            Instalment instalment = new Instalment(orderId, baseInstalmentInfo.getDueTime(), PaymentStatus.NOT_PAID,
                    convenienceCharge, securityCharge, baseInstalmentInfo.getAmount(),
                    totalAmountToBePaid,
                    0, 0, null, null,
                    callingUserId, instalmentPurchaseEntity, instalmentPurchaseEntityId);
            instalment.setVedantuDiscountAmount(baseInstalmentInfo.getVedantuDiscountAmount());
            instalment.setTeacherDiscountAmount(baseInstalmentInfo.getTeacherDiscountAmount());
            instalments.add(instalment);
            count++;
        }

        logger.info("EXIT: " + instalments);
        return instalments;
    }

    private List<BaseInstalmentInfoWithDiscounts> updateBaseInstalmentInfosWithDiscount(List<BaseInstalmentInfo> baseInstalmentInfos, Integer vedantuDiscountAmount,
            Integer teacherDiscountAmount) {
        if (vedantuDiscountAmount == null) {
            vedantuDiscountAmount = 0;
        }
        if (teacherDiscountAmount == null) {
            teacherDiscountAmount = 0;
        }

        Integer totalCost = 0;
        for (BaseInstalmentInfo baseInstalmentInfo : baseInstalmentInfos) {
            //baseInstalmentInfo.setAmount(baseInstalmentInfo.getAmount()-discountPerInstalment);
            totalCost += baseInstalmentInfo.getAmount();
        }

        Double teacherDiscountProportion = new Double(0);
        Double vedantuDiscountProportion = new Double(0);
        if (totalCost > 0) {
            teacherDiscountProportion = teacherDiscountAmount.doubleValue() / totalCost.doubleValue();
            vedantuDiscountProportion = vedantuDiscountAmount.doubleValue() / totalCost.doubleValue();
        }

        List<BaseInstalmentInfoWithDiscounts> infos = new ArrayList<>();

        Integer tDiscountAccounted = 0, vDiscountAccounted = 0;
        int count = 0;
        for (BaseInstalmentInfo baseInstalmentInfo : baseInstalmentInfos) {
            Integer teacherDiscountAmountForInst;
            Integer vedantuDiscountAmountForInst;
            if (count == baseInstalmentInfos.size() - 1) {
                teacherDiscountAmountForInst = teacherDiscountAmount - tDiscountAccounted;
                vedantuDiscountAmountForInst = vedantuDiscountAmount - vDiscountAccounted;
            } else {
                teacherDiscountAmountForInst = (int) (baseInstalmentInfo.getAmount() * teacherDiscountProportion);
                vedantuDiscountAmountForInst = (int) (baseInstalmentInfo.getAmount() * vedantuDiscountProportion);
                tDiscountAccounted += teacherDiscountAmountForInst;
                vDiscountAccounted += vedantuDiscountAmountForInst;
            }
            Integer newAmount = baseInstalmentInfo.getAmount() - (teacherDiscountAmountForInst + vedantuDiscountAmountForInst);
            if (newAmount < 0) {
                newAmount = 0;
                logger.error("Instalment amount less than 0 after discount ");
            }
            infos.add(new BaseInstalmentInfoWithDiscounts(teacherDiscountAmountForInst, vedantuDiscountAmountForInst, baseInstalmentInfo.getDueTime(), newAmount));
            count++;
        }
        return infos;
    }

    private class BaseInstalmentInfoWithDiscounts extends BaseInstalmentInfo {

        private final Integer teacherDiscountAmount;
        private final Integer vedantuDiscountAmount;

        public BaseInstalmentInfoWithDiscounts(Integer teacherDiscountAmount,
                Integer vedantuDiscountAmount, Long dueTime, Integer amount) {
            super(dueTime, amount);
            this.teacherDiscountAmount = teacherDiscountAmount;
            this.vedantuDiscountAmount = vedantuDiscountAmount;
        }

        public Integer getTeacherDiscountAmount() {
            if (teacherDiscountAmount == null) {
                return 0;
            }
            return teacherDiscountAmount;
        }

        public Integer getVedantuDiscountAmount() {
            if (vedantuDiscountAmount == null) {
                return 0;
            }
            return vedantuDiscountAmount;
        }

    }

    public Integer getTotalAmountForDiscount(InstalmentPurchaseEntity instalmentPurchaseEntity,
            String instalmentPurchaseEntityId, Long userId) throws VException {
        logger.info("ENTRY: " + instalmentPurchaseEntity + ", instalmentPurchaseEntityId: "
                + instalmentPurchaseEntityId);
        BaseInstalment baseInstalment = getBaseInstalment(instalmentPurchaseEntity, instalmentPurchaseEntityId, userId);
        if (baseInstalment == null || baseInstalment.getInfo() == null || baseInstalment.getInfo().isEmpty()) {
            throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND, "BaseInstalment not found");
        }
        Integer totalCost = 0;
        for (BaseInstalmentInfo baseInstalmentInfo : baseInstalment.getInfo()) {
            //baseInstalmentInfo.setAmount(baseInstalmentInfo.getAmount()-discountPerInstalment);
            totalCost += baseInstalmentInfo.getAmount();
        }
        return totalCost;
    }

    public static void main(String[] args) throws IOException {
        System.err.println(">>>");
//        InstalmentManager i = new InstalmentManager();
//        Long n = i.getNextInstStartTime(1482949800000l);
//
//        SessionSchedule sessionSchedule = new SessionSchedule();
//        sessionSchedule.setNoOfWeeks(23l);
//        List<SessionSlot> slots = new ArrayList<>();
//        slots.add(new SessionSlot(1467311400000l, 1467315000000l));
//        slots.add(new SessionSlot(1467322200000l, 1467325800000l));
//        sessionSchedule.setSessionSlots(slots);
//        
//        try {
//            List<Instalment> instalments=i.prepareInstalments(sessionSchedule, 100, null,
//                    null, 10, null, null);
//            System.err.println("ajith " + instalments);
//            System.err.println("length " + instalments.size());
//            for(Instalment in: instalments){
//                System.err.println("start time "+in.getDueTime());
//            }
//        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(InstalmentManager.class.getName()).log(Level.SEVERE, null, ex);
//        } 
//        

//        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
//        nameValuePairs.add(new BasicNameValuePair("subscriptionRequestId", "234sd"));
//        JSONObject request = new JSONObject();
//        request.put("subscriptionRequestId"llllllllll, "234sd");
//        long[] slots = {15l, 64424509440l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 15l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 15l};
        long[] slots = {0l, 15360l, 65970697666560l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 15360l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 15360l};

//        long[] slots={0l,0l, 65970697666560l,0l, 15360l,0l,0l,0l,0l,0l,0l,0l,0l,0l, 65970697666560l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l,0l, 65970697666560l};
        long[] conflictBitSet = {0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 65970697666560l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 15360l, 0l, 0l, 15360l, 0l, 0l, 0l, 0l, 0l, 0l, 65970697666560l, 0l, 0l, 65970697666560l, 0l, 0l, 0l, 0l, 0l, 0l, 0l, 15360l};
//        BitSet slotsB = BitSet.valueOf(slots);
//        BitSet slotsC = BitSet.valueOf(conflicts);
//        slotsB.andNot(slotsC);
        List<SessionSlot> sessionSlots = SchedulingUtils.createSessionSlots(slots, 1492108200000l);
        Long hoursToLock = 0l;
        for (SessionSlot s : sessionSlots) {
            System.err.println(s);
            System.err.println(new Date(s.getStartTime()) + " to " + new Date(s.getEndTime()));
            hoursToLock += (s.getEndTime() - s.getStartTime());
        }
        System.err.println(sessionSlots.size());
        System.err.println(">>>>>AjMM ss " + BitSet.valueOf(slots).cardinality());
        System.err.println(">>>>>hours locked  " + hoursToLock);

        int newDiscountAvailable = 70000;
        int newVDisAmt = (int) (70000
                * ((float) 50000 / 100000));
        newDiscountAvailable -= newVDisAmt;
        System.err.println(">> " + newVDisAmt + " newDiscountAvailable " + newDiscountAvailable);
    }

    public List<Instalment> getInstalmentsForCoursePlan(GetInstalmentsReq getInstalmentsReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where("contextId").is(getInstalmentsReq.getCoursePlanId()));
        query.addCriteria(Criteria.where("contextType").is(InstalmentPurchaseEntity.COURSE_PLAN.name()));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        return instalments;
    }

    public List<Instalment> getInstalmentsForContextIds(GetInstalmentsReq getInstalmentsReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants.CONTEXT_ID).in(getInstalmentsReq.getContextIds()));
        query.addCriteria(Criteria.where(Instalment.Constants.USER_ID).is(getInstalmentsReq.getUserId()));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        return instalments;
    }

    public List<Instalment> getInstalmentsForDeliverableIds(GetInstalmentsReq getInstalmentsReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants.PURCHASING_ENTITIES_DELIVERABLE_ID).in(getInstalmentsReq.getDeliverableIds()));
        query.addCriteria(Criteria.where(Instalment.Constants.USER_ID).is(getInstalmentsReq.getUserId()));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        return instalments;
    }

    public Boolean resetOrderInstallmentState(String entityId, EntityType entityType, String userId, String deliverableEntityId) throws NotFoundException, BadRequestException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(entityType));
        if (StringUtils.isNotEmpty(deliverableEntityId)) {
            query.addCriteria(Criteria.where(Orders.Constants.PURCHASING_ENTITIES_DELIVERABLE_ID).is(deliverableEntityId));
        }

        query.addCriteria(Criteria.where("userId").is(Long.parseLong(userId)));
        query.addCriteria(Criteria.where("paymentStatus").is(PaymentStatus.PAYMENT_SUSPENDED));
        List<Orders> orders = ordersDAO.getOrders(query);
        Orders order = null;
        if (ArrayUtils.isNotEmpty(orders)) {
            if (orders.size() > 1) {
                logger.error("Alert! multiple orders found for same entityId,userId and they are requested for changing state: entityId "
                        + entityId + ", userId " + userId + ", deliverableEntityId " + deliverableEntityId);
            }

            order = orders.get(0);

            List<Instalment> instalments = getInstalmentsForOrder(order.getId(), PaymentStatus.PAYMENT_SUSPENDED);

            if (CollectionUtils.isEmpty(instalments)) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                        "No forfeited installments found for orderid:" + order.getId());
            }

            // Mark the installments NOT_PAID
            for (Instalment instalment : instalments) {
                logger.info("Installment pre update : " + instalment.toString());
                if (instalment.getDueTime() < System.currentTimeMillis()) {
                    instalment.setDueTime(System.currentTimeMillis());
                }
                instalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.NOT_PAID);
                instalmentDAO.save(instalment);
                logger.info("Installment post update : " + instalment.toString());

            }
            order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PARTIALLY_PAID, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//            order.setPaymentStatus(PaymentStatus.PARTIALLY_PAID);
            ordersDAO.create(order);
            paymentManager.triggerOrderForLeadSquare(order, false);
            logger.info("Post update Order " + order.toString());
        } else {
            logger.info("No forfeited orders found for " + entityId);
        }
        return true;
    }

    public PlatformBasicResponse changeInstalmentDueDate(ChangeInstalmentDueDateReq req) throws VException {
        Instalment instalment = instalmentDAO.getEntityById(req.getInstalmentId(), Instalment.class);
        logger.info("EXIT: " + instalment);
        if (instalment == null) {
            throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND, "Instalment not found");
        }

        if (PaymentStatus.PAID.equals(instalment.getPaymentStatus())) {
            throw new ForbiddenException(ErrorCode.ALREADY_PAID,
                    "Instalment already paid at " + instalment.getPaidTime());
        }

        if (!InstalmentPurchaseEntity.BATCH.equals(instalment.getContextType())) {
            throw new ForbiddenException(ErrorCode.INSTALMENT_DUE_TIME_CHANGE_ONLY_ALLOWED_FOR_OTF,
                    "Instalment contextType  is " + instalment.getContextType());
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(instalment.getOrderId()));
        query.addCriteria(Criteria.where("paymentStatus").is(PaymentStatus.PAID));
        query.with(Sort.by(Sort.Direction.DESC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        if (ArrayUtils.isNotEmpty(instalments)) {
            Instalment lastPaidInstalment = instalments.get(0);
            if (req.getDueTime() <= lastPaidInstalment.getDueTime()) {
                throw new ForbiddenException(ErrorCode.DUE_TIME_BEFORE_LAST_PAID_INSTALMENT,
                        "req due time is before the last paid instalment " + lastPaidInstalment.getDueTime());
            }
        }

        InstalmentUpdate update = new InstalmentUpdate();
        update.setOldDueTime(instalment.getDueTime());
        update.setChangedBy(req.getCallingUserId());
        update.setChangeTime(System.currentTimeMillis());
        instalment.setDueTime(req.getDueTime());
        instalment.getUpdates().add(update);
        instalmentDAO.save(instalment);
        return new PlatformBasicResponse();
    }

    public List<CounselorDashboardInstalmentInfoRes> getDashboardDueInstalmentsForContextTypeAndTime(GetDueInstalmentsForTimeReq req) throws VException {
        List<InstalmentInfo> instalments = getDueInstalmentsForContextTypeAndTime(req);
        List<CounselorDashboardInstalmentInfoRes> response = new ArrayList<>();
        if (ArrayUtils.isEmpty(instalments)) {
            return response;
        }
        Set<String> coursePlanIds = new HashSet<>();
        Set<String> batchIds = new HashSet<>();
        Set<String> bundleIds = new HashSet<>();
        Set<String> enrollmentIds = new HashSet<>();
        Set<Long> studentIds = new HashSet<>();
        Set<Long> userIdsForBasicInfo = new HashSet<>();

        for (InstalmentInfo info : instalments) {
            if (info.getUserId() != null) {
                studentIds.add(info.getUserId());
            }
            if (null != info.getContextType()) {
                switch (info.getContextType()) {
                    case BATCH:
                        batchIds.add(info.getContextId());
                        if (StringUtils.isNotEmpty(info.getDeliverableEntityId())) {
                            enrollmentIds.add(info.getDeliverableEntityId());
                        }
                        break;
                    case COURSE_PLAN:
                        coursePlanIds.add(info.getContextId());
                        break;
                    case OTF_BUNDLE:
                        bundleIds.add(info.getContextId());
                        break;
                    default:
                        break;
                }
            }
        }
        logger.info("got installments:" + instalments);

        Map<String, CoursePlanInfo> coursePlanInfoMap = getCoursePlanInfoMap(coursePlanIds);
        logger.info("got coursePlanInfoMap:" + coursePlanInfoMap);

        for (CoursePlanInfo coursePlanInfo : coursePlanInfoMap.values()) {
            if (coursePlanInfo.getTeacherId() != null) {
                userIdsForBasicInfo.add(coursePlanInfo.getTeacherId());
            }
        }
        userIdsForBasicInfo.addAll(studentIds);
        logger.info("userIdsForBasicInfo:" + userIdsForBasicInfo);

        Map<String, CounselorDashboardSessionsInfoRes> coursePlanSessionInfoMap = getCoursePlanSessionInfoMap(coursePlanIds);
        logger.info("got coursePlanSessionInfoMap:" + coursePlanSessionInfoMap);

        Map<String, BatchBasicInfo> batchInfoMap = getBatchBasicInfoMap(batchIds);
        logger.info("got batchInfoMap:" + batchInfoMap);

        Map<String, CounselorDashboardSessionsInfoRes> batchSessionInfoMap = getBatchSessionInfoMap(batchIds);
        logger.info("got batchSessionInfoMap:" + batchSessionInfoMap);

        Map<String, EnrollmentPojo> enrollmentInfoMap = getEnrollmentInfoMap(enrollmentIds);
        logger.info("got enrollmentInfoMap:" + enrollmentInfoMap);

        Map<String, OTFBundleInfo> bundleInfoMap = getOTFBundleInfoMap(bundleIds);
        logger.info("got bundleInfoMap:" + bundleInfoMap);

        List<Long> studentList = new ArrayList<>(studentIds);
        Map<Long, DashboardUserInstalmentHistoryRes> installmentHistoryMap = getUserInstalmentTransactionHistory(studentList);
        logger.info("got installmentHistoryMap:" + installmentHistoryMap);

        logger.info("got userIdsForBasicInfo:" + userIdsForBasicInfo);
        Map<Long, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(userIdsForBasicInfo, true);
        if (userBasicInfoMap == null) {
            userBasicInfoMap = new HashMap();
        }

        for (InstalmentInfo instal : instalments) {
            CounselorDashboardInstalmentInfoRes instalRes = new CounselorDashboardInstalmentInfoRes();
            String contextKey = instal.getContextId();
            instalRes.setContextId(instal.getContextId());
            instalRes.setContextType(instal.getContextType());
            instalRes.setDueTime(instal.getDueTime());
            instalRes.setTotalAmount(instal.getTotalAmount());
            if (instal.getUserId() != null) {
                instalRes.setStudent(userBasicInfoMap.get(instal.getUserId()));
            }
            CounselorDashboardSessionsInfoRes sessionInfo = null;
            if (null != instal.getContextType()) {
                switch (instal.getContextType()) {
                    case BATCH:
                        BatchBasicInfo batchBasicInfo = batchInfoMap.get(contextKey);
                        if (batchBasicInfo != null) {
                            instalRes.setContextTitle(batchBasicInfo.getGroupName());
                            instalRes.setContextStartTime(batchBasicInfo.getStartTime());
                        }
                        sessionInfo = batchSessionInfoMap.get(contextKey);
                        if (StringUtils.isNotEmpty(instal.getDeliverableEntityId())) {
                            EnrollmentPojo pojo = enrollmentInfoMap.get(instal.getDeliverableEntityId());
                            if (pojo != null) {
                                if (pojo.getState() != null) {
                                    instalRes.setState(pojo.getState().name());
                                }
                                if (pojo.getStatus() != null) {
                                    instalRes.setStatus(pojo.getStatus().name());
                                }
                            }
                        }
                        break;
                    case COURSE_PLAN:
                        CoursePlanInfo coursePlanInfo = coursePlanInfoMap.get(contextKey);
                        if (coursePlanInfo != null) {
                            instalRes.setContextTitle(coursePlanInfo.getTitle());
                            instalRes.setContextStartTime(coursePlanInfo.getStartDate());
                            if (coursePlanInfo.getState() != null) {
                                instalRes.setState(coursePlanInfo.getState().name());
                            }
                            if (coursePlanInfo.getTeacherId() != null) {
                                instalRes.setTeacher(userBasicInfoMap.get(coursePlanInfo.getTeacherId()));
                            }
                        }
                        sessionInfo = coursePlanSessionInfoMap.get(contextKey);

                        break;
                    case OTF_BUNDLE:
                        OTFBundleInfo bundleInfo = bundleInfoMap.get(contextKey);
                        if (bundleInfo != null) {
                            instalRes.setContextTitle(bundleInfo.getTitle());
                        }
                        break;
                    default:
                        break;
                }
            }
            DashboardUserInstalmentHistoryRes history = installmentHistoryMap.get(instal.getUserId());
            if (history != null) {
                instalRes.setFirstExTransactionTime(history.getFirstExTransactionTime());
                instalRes.setFirstInstalmentPaidAmount(history.getFirstInstalmentPaidAmount());
                instalRes.setFirstInstalmentPaidTime(history.getFirstInstalmentPaidTime());
                instalRes.setLastInstalmentTime(history.getLastInstalmentTime());
            }

            if (sessionInfo != null) {
                instalRes.setLastSessionEndTime(sessionInfo.getLastSessionEndTime());
                instalRes.setTotalNoOfSessions(sessionInfo.getTotalNoOfSessions());
            }
            response.add(instalRes);
        }

        return response;
    }

    ;

    public List<InstalmentInfo> getDueInstalmentsForContextTypeAndTime(GetDueInstalmentsForTimeReq req) throws BadRequestException {
        //Create Indices
        req.verify();
        List<InstalmentInfo> instalmentInfos = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants.CONTEXT_TYPE).is(req.getContextType()));
        query.addCriteria(Criteria.where(Instalment.Constants.PAYMENT_STATUS).is(PaymentStatus.NOT_PAID));
        query.addCriteria(Criteria.where(Instalment.Constants.DUE_TIME).gte(req.getStartTime()).lt(req.getEndTime()));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        if (req.getStart() != null || req.getSize() != null) {
            instalmentDAO.setFetchParameters(query, req);
        }
        List<Instalment> instalments = instalmentDAO.getInstalments(query);
        List<String> orderIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(instalments)) {
            for (Instalment instalment : instalments) {
                instalmentInfos.add(mapper.map(instalment, InstalmentInfo.class));
                orderIds.add(instalment.getOrderId());
            }
        }
        if (req.getContextType().equals(InstalmentPurchaseEntity.BATCH) && ArrayUtils.isNotEmpty(orderIds)) {
            Query orderQuery = new Query();
            orderQuery.addCriteria(Criteria.where(Orders.Constants._ID).in(orderIds));
            List<Orders> orders = ordersDAO.runQuery(orderQuery, Orders.class);
            Map<String, Orders> orderMap = new HashMap<>();
            if (ArrayUtils.isNotEmpty(orders)) {
                for (Orders order : orders) {
                    if (order != null && StringUtils.isNotEmpty(order.getId())) {
                        orderMap.put(order.getId(), order);
                    }
                }
                for (InstalmentInfo instalmentInfo : instalmentInfos) {
                    if (orderMap.containsKey(instalmentInfo.getOrderId())) {
                        Orders order = orderMap.get(instalmentInfo.getOrderId());
                        if (order != null && ArrayUtils.isNotEmpty(order.getItems())
                                && order.getItems().get(0) != null) {
                            instalmentInfo.setDeliverableEntityId(order.getItems().get(0).getDeliverableEntityId());
                        }
                        if (order != null && order.getUserId() != null) {
                            instalmentInfo.setUserId(order.getUserId());
                        }
                    }
                }
            }

        }
        return instalmentInfos;
    }

    public List<BaseInstalment> getFirstBaseInstalmentsForDueTime(Long startTime, Long endTime) {
        return baseInstalmentDAO.getFirstBaseInstalmentsForDueTime(startTime, endTime);
    }

    public List<BaseInstalmentInfo> getBaseInstalmentFromRegistration(Long userId, String entityId) throws VException {
        String url = SUBSCRIPTION_ENDPOINT
                + "/registration/getApplicableBaseInstalment";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url + "?userId=" + userId + "&entityId=" + entityId, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<BaseInstalmentInfo>>() {
        }.getType();
        List<BaseInstalmentInfo> response = new Gson().fromJson(jsonString, listType);
        if (response != null && ArrayUtils.isNotEmpty(response)) {
            return response;
        }
        return new ArrayList<>();
    }



    public List<UserDueInstalmentsResp> getDueInstalmentsForUser()
            throws VException {
        List<UserDueInstalmentsResp> userDueInstalmentsResps= new ArrayList<>();
        Long callingUserId = null;
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (sessionData != null && sessionData.getUserId() != null) {
            callingUserId   = sessionData.getUserId();
        }
        else{
            return userDueInstalmentsResps;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants.USER_ID).is(callingUserId));
        query.addCriteria(Criteria.where(Instalment.Constants.CONTEXT_TYPE).is(InstalmentPurchaseEntity.BUNDLE));
        query.addCriteria(Criteria.where(Instalment.Constants.DUE_TIME).lt(System.currentTimeMillis() + (10 * DateTimeUtils.MILLIS_PER_DAY)));
        query.addCriteria(Criteria.where(Instalment.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.NOT_PAID,PaymentStatus.PAYMENT_SUSPENDED)));
        query.with(Sort.by(Sort.Direction.DESC, Instalment.Constants.DUE_TIME));
        query.skip(0);
        query.limit(100);
        List<Instalment> instalments = instalmentDAO.getInstalments(query , Arrays.asList(Instalment.Constants.CONTEXT_ID,Instalment.Constants.DUE_TIME,Instalment.Constants.TOTAL_AMOUNT));
        if(ArrayUtils.isEmpty(instalments)){
            return userDueInstalmentsResps;
        }

        Map<String,Instalment> instalmentMap = new LinkedHashMap<>();
        instalments.forEach(instalment -> {
            instalmentMap.put(instalment.getContextId(),instalment);
        });
        Map<String,String> bundleMap = new HashMap<>();
         bundleMap = getBundleName(new ArrayList<>(instalmentMap.keySet()));
logger.info("============================    " +bundleMap.toString() );
        if(bundleMap == null){
            return userDueInstalmentsResps;
        }
        for(Instalment value  :instalmentMap.values())
        {
            String key =value.getContextId();
            if(bundleMap.containsKey(key)) {
                UserDueInstalmentsResp resp = new UserDueInstalmentsResp();
                resp.setContextId(key);
                resp.setAmount(value.getTotalAmount());
                resp.setDueDate(value.getDueTime());
                resp.setContextName(bundleMap.get(key));
                userDueInstalmentsResps.add(resp);
            }
        }
        return userDueInstalmentsResps;
    }
    public List<ContextWiseNextDueInstalmentsRes> getContextWiseNextDueInstalments(ContextWiseNextDueInstalmentsReq req)
            throws VException {
        List<ContextWiseNextDueInstalmentsRes> res = new ArrayList<>();
        if (ArrayUtils.isEmpty(req.getDeliverableIds()) || req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "deliverableIds or userId null");
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants.USER_ID).is(req.getUserId()));
        query.addCriteria(Criteria.where(Instalment.Constants.PURCHASING_ENTITIES_DELIVERABLE_ID).in(req.getDeliverableIds()));
        query.skip(0);
        query.limit(InstalmentDAO.MAX_ALLOWED_FETCH_SIZE);
        List<Instalment> instalments = instalmentDAO.getInstalments(query);

        if (ArrayUtils.isNotEmpty(instalments)) {
            if (instalments.size() >= InstalmentDAO.MAX_ALLOWED_FETCH_SIZE) {
                logger.error("we have hit the limit for getContextWiseNextDueInstalments, use agg query");
            }
            Map<String, ContextWiseNextDueInstalmentsRes> respMap = new HashMap<>();
            for (Instalment instalment : instalments) {
                if (!respMap.containsKey(instalment.getContextId())) {
                    ContextWiseNextDueInstalmentsRes _res = new ContextWiseNextDueInstalmentsRes();
                    _res.setContextId(instalment.getContextId());
                    respMap.put(instalment.getContextId(), _res);
                }
                ContextWiseNextDueInstalmentsRes existing = respMap.get(instalment.getContextId());
                if (PaymentStatus.PAID.equals(instalment.getPaymentStatus())) {
                    if (existing.getLastPaid() == null) {
                        existing.setLastPaid(mapper.map(instalment, InstalmentInfo.class));
                    } else if (instalment.getDueTime() > existing.getLastPaid().getDueTime()) {
                        existing.setLastPaid(mapper.map(instalment, InstalmentInfo.class));
                    }
                }

                if (PaymentStatus.NOT_PAID.equals(instalment.getPaymentStatus())) {
                    if (existing.getNextDue() == null) {
                        existing.setNextDue(mapper.map(instalment, InstalmentInfo.class));
                    } else if (instalment.getDueTime() < existing.getNextDue().getDueTime()) {
                        existing.setNextDue(mapper.map(instalment, InstalmentInfo.class));
                    }
                }
            }
            res = new ArrayList<>(respMap.values());
        }
        return res;
    }

    public InstalmentInfo getLastestInstalmentPaid(String orderId) throws NotFoundException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants.ORDER_ID).is(orderId));
        query.addCriteria(Criteria.where(Instalment.Constants.PAYMENT_STATUS).is(PaymentStatus.PAID));
        query.addCriteria(Criteria.where(Instalment.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.with(Sort.by(Sort.Direction.DESC, Instalment.Constants.PAID_TIME));
        query.limit(1);
        List<Instalment> instalments = instalmentDAO.runQuery(query, Instalment.class);

        if (instalments == null || instalments.size() < 1) {
            throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND, "");
        }

        Instalment instalment = instalments.get(0);

        InstalmentInfo instalmentInfo = new InstalmentInfo();

        instalmentInfo.setUserId(instalment.getUserId());
        instalmentInfo.setTotalAmount(instalment.getTotalAmount());
        instalmentInfo.setTotalPromotionalAmount(instalment.getTotalPromotionalAmount());
        instalmentInfo.setTotalNonPromotionalAmount(instalment.getTotalNonPromotionalAmount());
        instalmentInfo.setId(instalment.getId());
        return instalmentInfo;

    }

    public List<OrderInfo> getOrderInfo(List<String> orderIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants._ID).in(orderIds));

        List<Orders> ordersList = ordersDAO.getOrders(query);
        List<OrderInfo> orderInfosList = new ArrayList<>();
        for (Orders orders : ordersList) {
            OrderInfo orderInfo = new OrderInfo();

            orderInfo.setAmount(orders.getAmount());
            orderInfo.setAmountPaid(orders.getAmountPaid());
            orderInfo.setNonpromotionalAmount(orders.getNonPromotionalAmount());
            orderInfo.setPromotionalAmount(orders.getPromotionalAmount());
            orderInfo.setOrderedItems(orders.getItems());
            orderInfo.setPurchasingEntities(orders.getPurchasingEntities());

            orderInfosList.add(orderInfo);
        }
        return orderInfosList;
    }

    public String markOrderEndedForEntityId(String entityId, EntityType entityType, String userId, OrderEndType orderEndType) throws NotFoundException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(entityType));
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(Long.parseLong(userId)));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED, PaymentStatus.PAID)));
        List<Orders> orders = ordersDAO.getOrders(query);
        if (ArrayUtils.isNotEmpty(orders)) {
            if (orders.size() > 1) {
                logger.error("Alert! multiple orders found for same entityId,userId and they are requested for forfeiting entityId "
                        + entityId + ", userId " + userId);
            }
            //in loop as many cases this will be 1 entity only, if more will think of a different approach
            for (Orders order : orders) {
                if(order != null){
                    order.setOrderEndType(orderEndType);
                }
                _markOrderEnded(order);
                return order.getId();
            }
        }
        return "";
    }

    public Boolean markOrderEnded(String orderId) throws NotFoundException {
        logger.info("ENTRY: " + orderId);
        Orders order = ordersDAO.getById(orderId);
        return _markOrderEnded(order);
    }

    public Boolean _markOrderEnded(Orders order) throws NotFoundException {
        if (order == null) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "Order not found");
        }

        if ((PaymentStatus.PARTIALLY_PAID.equals(order.getPaymentStatus()) || PaymentStatus.PAYMENT_SUSPENDED.equals(order.getPaymentStatus()))
                && PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            Query query = new Query();
            query.addCriteria(Criteria.where("orderId").is(order.getId()));
            query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
            List<Instalment> instalments = instalmentDAO.getInstalments(query);
            boolean foundNotPaid = false;
            if (ArrayUtils.isNotEmpty(instalments)) {
                for (Instalment instalment : instalments) {
                    if (PaymentStatus.PAYMENT_SUSPENDED.equals(instalment.getPaymentStatus())
                            || PaymentStatus.NOT_PAID.equals(instalment.getPaymentStatus())) {
                        foundNotPaid = true;
                        instalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.FORFEITED);
                        instalmentDAO.save(instalment);
                    }
                }
                if (!foundNotPaid) {
                    logger.error("Order is not paid but all the instalments are paid, orderId "
                            + order.getId());
                }
            }
        }
        order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.FORFEITED, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//        order.setPaymentStatus(PaymentStatus.FORFEITED);
        ordersDAO.create(order);
        paymentManager.triggerOrderForLeadSquare(order, false);
        logger.info("EXIT ");
        return true;
    }

    public Boolean resetOrderInstallmentStatePostPayment(String entityId, EntityType entityType,
            String userId, String deliverableEntityId, String deliverableId) throws NotFoundException, BadRequestException {
        return resetOrderInstallmentStatePostPayment(entityId, entityType, userId, deliverableEntityId, deliverableId, null);
    }

    public Boolean resetOrderInstallmentStatePostPayment(String entityId, EntityType entityType,
            String userId, String deliverableEntityId, String deliverableId, String instalmentId) throws NotFoundException, BadRequestException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(entityType));
        if (StringUtils.isNotEmpty(deliverableEntityId)) {
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID).is(deliverableEntityId));
        }
        if (StringUtils.isNotEmpty(deliverableId)) {
            query.addCriteria(Criteria.where(Orders.Constants.PURCHASING_ENTITIES_DELIVERABLE_ID).is(deliverableId));
        }

        query.addCriteria(Criteria.where("userId").is(Long.parseLong(userId)));
        query.addCriteria(Criteria.where("paymentStatus").in(Arrays.asList(PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));
        List<Orders> orders = ordersDAO.getOrders(query);
        Orders order = null;
        if (ArrayUtils.isNotEmpty(orders)) {
            if (orders.size() > 1) {
                logger.error("Alert! multiple orders found for same entityId,userId and they are requested for changing state: entityId "
                        + entityId + ", userId " + userId + ", deliverableEntityId " + deliverableEntityId);
            }

            order = orders.get(0);

            List<Instalment> instalments = getInstalmentsForOrder(order.getId(), PaymentStatus.PAYMENT_SUSPENDED);

            if (CollectionUtils.isEmpty(instalments)) {
                logger.info("No forfeited instalments found for " + entityId);
                return true;
            }

            // Mark the installments NOT_PAID
            for (Instalment instalment : instalments) {
                logger.info("Installment pre update : " + instalment.toString());
                if (StringUtils.isNotEmpty(instalmentId) && instalmentId.equalsIgnoreCase(instalment.getId())) {
                    logger.info("Installment in question : " + instalment.toString());
                    continue;
                }
                if (instalment.getDueTime() < System.currentTimeMillis()) {
                    order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PAYMENT_SUSPENDED, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//                    order.setPaymentStatus(PaymentStatus.PAYMENT_SUSPENDED);
                    ordersDAO.create(order);
                    paymentManager.triggerOrderForLeadSquare(order, false);
                    logger.info("user not paid all previous instalments");
                    return false;
                }
                instalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.NOT_PAID);
                instalmentDAO.save(instalment);
                logger.info("Installment post update : " + instalment.toString());

            }
            order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PARTIALLY_PAID, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//            order.setPaymentStatus(PaymentStatus.PARTIALLY_PAID);
            ordersDAO.create(order);
            logger.info("Post update Order " + order.toString());
        } else {
            logger.info("No forfeited orders found for " + entityId);
        }
        return true;
    }

    public void updatePurchasingEntitiesInInstalment(String orderId, List<PurchasingEntity> purchasingEntities)
            throws VException {
        if (StringUtils.isEmpty(orderId) || ArrayUtils.isEmpty(purchasingEntities)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bad request : orderId is " + orderId + " purchasingEntities is " + purchasingEntities);
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants.ORDER_ID).in(orderId));
        query.with(Sort.by(Sort.Direction.ASC, "dueTime"));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);

        if (ArrayUtils.isNotEmpty(instalments)) {
            for (Instalment instalment : instalments) {
                instalment.setPurchasingEntities(purchasingEntities);
                instalmentDAO.save(instalment);
            }
        }

    }

    public Map<Long, DashboardUserInstalmentHistoryRes> getUserInstalmentTransactionHistory(List<Long> userIds) {
        Map<Long, DashboardUserInstalmentHistoryRes> response = new HashMap();
        if (ArrayUtils.isEmpty(userIds)) {
            return response;
        }
        List<DashboardUserInstalmentHistoryRes> historyRes = instalmentDAO.getUserInstalmentHistoryAggregation(userIds);
        List<DashboardUserInstalmentHistoryRes> transactionRes = extTransactionDAO.getUserTransactionHistoryAggregation(userIds);
        Map<Long, DashboardUserInstalmentHistoryRes> transactionMap = new HashMap();
        for (DashboardUserInstalmentHistoryRes res : transactionRes) {
            transactionMap.put(res.getId(), res);
        }
        for (DashboardUserInstalmentHistoryRes res : historyRes) {
            DashboardUserInstalmentHistoryRes transaction = transactionMap.get(res.getId());
            if (transaction != null) {
                res.setFirstExTransactionTime(transaction.getFirstExTransactionTime());
            }
            response.put(res.getId(), res);
        }
        return response;
    }

    private Map<String, CoursePlanInfo> getCoursePlanInfoMap(Set<String> coursePlanIds) throws VException {
        Map<String, CoursePlanInfo> coursePlanInfoMap = new HashMap();
        if (ArrayUtils.isNotEmpty(coursePlanIds)) {
            String url = SUBSCRIPTION_ENDPOINT
                    + "/courseplan/getCousePlansInfosByIds" + "?";
            for (String id : coursePlanIds) {
                url += "&coursePlanIds=" + id;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<Map<String, CoursePlanInfo>>() {
            }.getType();
            coursePlanInfoMap = new Gson().fromJson(jsonString, listType);
            if (coursePlanInfoMap == null) {
                coursePlanInfoMap = new HashMap();
            }
        }
        return coursePlanInfoMap;
    }

    private Map<String, CounselorDashboardSessionsInfoRes> getCoursePlanSessionInfoMap(Set<String> coursePlanIds) throws VException {
        Map<String, CounselorDashboardSessionsInfoRes> coursePlanSessionInfoMap = new HashMap();
        if (ArrayUtils.isNotEmpty(coursePlanIds)) {
            String url = SCHEDULING_ENDPOINT
                    + "/session/getSessionInfosForContextIds" + "?";
            for (String id : coursePlanIds) {
                url += "&contextIds=" + id;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<Map<String, CounselorDashboardSessionsInfoRes>>() {
            }.getType();
            coursePlanSessionInfoMap = new Gson().fromJson(jsonString, listType);
            if (coursePlanSessionInfoMap == null) {
                coursePlanSessionInfoMap = new HashMap();
            }
        }
        return coursePlanSessionInfoMap;
    }

    private Map<String, BatchBasicInfo> getBatchBasicInfoMap(Set<String> batchIds) throws VException {
        Map<String, BatchBasicInfo> batchInfoMap = new HashMap();
        if (ArrayUtils.isNotEmpty(batchIds)) {
            String url = SUBSCRIPTION_ENDPOINT
                    + "/batch/getBatchBasicInfoForBatchIds" + "?";
            for (String id : batchIds) {
                url += "&batchIds=" + id;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<Map<String, BatchBasicInfo>>() {
            }.getType();
            batchInfoMap = new Gson().fromJson(jsonString, listType);
            if (batchInfoMap == null) {
                batchInfoMap = new HashMap();
            }
        }
        return batchInfoMap;
    }

    private Map<String, CounselorDashboardSessionsInfoRes> getBatchSessionInfoMap(Set<String> batchIds) throws VException {
        Map<String, CounselorDashboardSessionsInfoRes> batchSessionInfoMap = new HashMap();
        if (ArrayUtils.isNotEmpty(batchIds)) {
            String url = SCHEDULING_ENDPOINT
                    + "/onetofew/session/getSessionInfosForBatchIds" + "?";
            for (String id : batchIds) {
                url += "&batchIds=" + id;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<Map<String, CounselorDashboardSessionsInfoRes>>() {
            }.getType();
            batchSessionInfoMap = new Gson().fromJson(jsonString, listType);
            if (batchSessionInfoMap == null) {
                batchSessionInfoMap = new HashMap();
            }
        }
        return batchSessionInfoMap;
    }

    private Map<String, OTFBundleInfo> getOTFBundleInfoMap(Set<String> bundleIds) throws VException {
        Map<String, OTFBundleInfo> bundleInfoMap = new HashMap();
        if (ArrayUtils.isNotEmpty(bundleIds)) {
            GetOTFBundlesReq getOTFBundlesReq = new GetOTFBundlesReq();
            List<String> bundleList = new ArrayList<>(bundleIds);
            getOTFBundlesReq.setOtfBundleIds(bundleList);
            String url = SUBSCRIPTION_ENDPOINT
                    + "/otfBundle/get?" + WebUtils.INSTANCE.createQueryStringOfObject(getOTFBundlesReq);
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,
                    null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp, true);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<List<OTFBundleInfo>>() {
            }.getType();
            List<OTFBundleInfo> otfBundles = new Gson().fromJson(jsonString, listType);
            if (ArrayUtils.isNotEmpty(otfBundles)) {
                for (OTFBundleInfo bundleInfo : otfBundles) {
                    bundleInfoMap.put(bundleInfo.getId(), bundleInfo);
                }
            }
        }
        return bundleInfoMap;
    }

    private Map<String, EnrollmentPojo> getEnrollmentInfoMap(Set<String> enrollmentIds) throws VException {
        Map<String, EnrollmentPojo> response = new HashMap();
        if (ArrayUtils.isNotEmpty(enrollmentIds)) {
            GetEnrollmentsReq enrollmentReq = new GetEnrollmentsReq();
            List<String> enrollList = new ArrayList<>(enrollmentIds);
            enrollmentReq.setEnrollmentIds(enrollList);
            String queryString = WebUtils.INSTANCE.createQueryStringOfObject(enrollmentReq);
            String getEnrollmentsUrl = SUBSCRIPTION_ENDPOINT + "enroll/getEnrollmentsByEnrollmentIds";
            if (!StringUtils.isEmpty(queryString)) {
                getEnrollmentsUrl += "?" + queryString;
            }

            ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<EnrollmentPojo>>() {
            }.getType();
            List<EnrollmentPojo> slist = new Gson().fromJson(jsonString, listType);
            if (ArrayUtils.isNotEmpty(slist)) {
                for (EnrollmentPojo en : slist) {
                    response.put(en.getId(), en);
                }
            }
        }
        return response;
    }

    public void triggerLeadSquareQueueForTransactionAsync(Transaction transaction, boolean createdNew) {
        instalmentAsyncTasksManager.triggerLeadSquareQueueForTransaction(transaction, createdNew);
    }

    public List<Instalment> updateInstalmentsForUser(InstalmentUpdateReq instalmentUpdateReq) throws VException {

        List<OrderedItem> orderItems = new ArrayList<>();

        /*
         * Checking if order id is passed, if passed handle it accordingly.
         */

        String orderId = instalmentUpdateReq.getOrderId();
        String contextId = instalmentUpdateReq.getContextId();
        List<Orders> orders = new ArrayList<>();
        if (orderId == null && contextId != null) {
            orders = paymentManager.getInstallmentOrdersByDeliverableId(instalmentUpdateReq.getContextId());
        } else if (contextId == null && orderId != null) {
            orders.add(paymentManager.getOrderById(orderId));
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "At least contextId or orderId should be present. Also both shouldn't be present");
        }

        logger.info("   " + orders.size() + "   ");

        if (ArrayUtils.isNotEmpty(orders)) {
            logger.info("   " + orders.get(0).getItems().size() + "   ");
            orderItems = orders.get(0).getItems();
            if (PaymentStatus.PAID.equals(orders.get(0).getPaymentStatus())
                    || PaymentStatus.FORFEITED.equals(orders.get(0).getPaymentStatus())
                    || PaymentStatus.NOT_PAID.equals(orders.get(0).getPaymentStatus())) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "order is in paid/forfeited/not_paid state");
            }
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, " No order found for installments ");
        }

        OrderedItem orderedItem = new OrderedItem();
        if (!orderItems.isEmpty() && orderItems.get(0) != null) {
            orderedItem = orderItems.get(0);
        } else {
            throw new ConflictException(ErrorCode.NOT_FOUND_ERROR, "orderItem for the contextid not found");
        }

        Long orderUserId = orders.get(0).getUserId();
        List<Instalment> instalments = getInstalmentsForOrder(orders.get(0).getId());
        HashMap<String, Instalment> instalmentMap = new HashMap<>();
        Boolean isPaymentSuspended = false;
        List<PurchasingEntity> purchasingEntities = new ArrayList<>();
        String instalmentContextId = "";
        int totalInstalmentToBePaid = 0;
        int availableDiscountFromToBePaidInstalments = 0;
        for (Instalment instalment : instalments) {
            logger.info("   " + instalment.getId() + "   ");
            instalmentMap.put(instalment.getId(), instalment);
            if (PaymentStatus.PAYMENT_SUSPENDED.equals(instalment.getPaymentStatus())) {
                isPaymentSuspended = true;
            }
            if (instalment.getPurchasingEntities() != null && purchasingEntities.isEmpty()) {
                purchasingEntities = instalment.getPurchasingEntities();
            }
            if (StringUtils.isEmpty(instalmentContextId)) {
                instalmentContextId = instalment.getContextId();
            }
            if ((PaymentStatus.PAYMENT_SUSPENDED.equals(instalment.getPaymentStatus())
                    || PaymentStatus.NOT_PAID.equals(instalment.getPaymentStatus()))
                    && EntityState.ACTIVE.equals(instalment.getEntityState())) {
                totalInstalmentToBePaid += instalment.getTotalAmount();
                if (instalment.getVedantuDiscountAmount() != null) {
                    availableDiscountFromToBePaidInstalments += instalment.getVedantuDiscountAmount();
                }
            }

        }

        logger.info("totalInstalmentToBePaid " + totalInstalmentToBePaid);
        logger.info("availableDiscountFromToBePaidInstalments " + availableDiscountFromToBePaidInstalments);

        if (instalmentUpdateReq.getAddUpdateInstalments() != null && !instalmentUpdateReq.getAddUpdateInstalments().isEmpty()) {
            int i = 0;
            for (Instalment addUpdateInstalment : instalmentUpdateReq.getAddUpdateInstalments()) {

                if (addUpdateInstalment.getId() != null) {
                    Instalment instalment = instalmentMap.get(addUpdateInstalment.getId());
                    if (!addUpdateInstalment.getTotalAmount().equals(instalment.getTotalAmount()) || !addUpdateInstalment.getDueTime().equals(instalment.getDueTime())) {
                        logger.info("   " + instalmentMap.get(addUpdateInstalment.getId()) + "   ");
                        if (!PaymentStatus.NOT_PAID.equals(instalment.getPaymentStatus())) {
                            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, " can not update paid instalment ");
                        }
                        InstalmentChangeTime instalmentChangeTime = new InstalmentChangeTime();
                        instalmentChangeTime.setChangeTime(System.currentTimeMillis());
                        instalmentChangeTime.setChangedBy(instalmentUpdateReq.getCallingUserId());
                        if (!addUpdateInstalment.getTotalAmount().equals(instalment.getTotalAmount())) {
                            instalmentChangeTime.setNewAmount(addUpdateInstalment.getTotalAmount());
                            instalmentChangeTime.setPreviousAmount(instalment.getTotalAmount());
                        }
                        if (!addUpdateInstalment.getDueTime().equals(instalment.getDueTime())) {
                            instalmentChangeTime.setNewDueDate(addUpdateInstalment.getDueTime());
                            instalmentChangeTime.setPreviousDueDate(instalment.getDueTime());
                        }
                        if (ArrayUtils.isEmpty(instalment.getInstalmentChangeTime())) {
                            instalment.setInstalmentChangeTime(Arrays.asList(instalmentChangeTime));
                        } else {
                            instalment.getInstalmentChangeTime().add(instalmentChangeTime);
                        }
                        instalment.setDueTime(addUpdateInstalment.getDueTime());
                        instalment.setTotalAmount(addUpdateInstalment.getTotalAmount());
                        instalment.setHoursCost(addUpdateInstalment.getTotalAmount());
                    }
                } else {

                    Instalment instalment = new Instalment();
                    if (isPaymentSuspended) {
                        instalment.setPaymentStatus(PaymentStatus.PAYMENT_SUSPENDED);
                    } else {
                        instalment.setPaymentStatus(PaymentStatus.NOT_PAID);
                    }
                    instalment.setEntityState(EntityState.ACTIVE);
                    instalment.setTotalAmount(addUpdateInstalment.getTotalAmount());
                    instalment.setHoursCost(addUpdateInstalment.getTotalAmount());
                    instalment.setDueTime(addUpdateInstalment.getDueTime());
                    instalment.setOrderId(orders.get(0).getId());
                    instalment.setUserId(instalmentUpdateReq.getUserId());
                    instalment.setContextId(instalmentContextId);
                    instalment.setContextType(instalmentUpdateReq.getContextType());
                    if (!purchasingEntities.isEmpty()) {
                        instalment.setPurchasingEntities(purchasingEntities);
                    }
                    instalmentMap.put(i++ + "", instalment);
                }

            }
        }
        if (instalmentUpdateReq.getRemovedInstalments() != null && !instalmentUpdateReq.getRemovedInstalments().isEmpty()) {
            for (String removedInstalment : instalmentUpdateReq.getRemovedInstalments()) {
                Instalment instalment = instalmentMap.get(removedInstalment);

                if (PaymentStatus.PAID.equals(instalment.getPaymentStatus())) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, " can not update paid instalment ");
                }

                instalment.setEntityState(EntityState.DELETED);
            }
        }

        logger.info("instalmentmap after making updates");
        logger.info(instalmentMap);

        int amt = 0;
        List<Instalment> toBePaidInstalments = new ArrayList<>();
        List<Instalment> otherInstalments = new ArrayList<>();
        for (Instalment instalment : instalmentMap.values()) {
            if ((PaymentStatus.PAYMENT_SUSPENDED.equals(instalment.getPaymentStatus())
                    || PaymentStatus.NOT_PAID.equals(instalment.getPaymentStatus()))
                    && EntityState.ACTIVE.equals(instalment.getEntityState())) {
                amt += instalment.getTotalAmount();
                toBePaidInstalments.add(instalment);
            } else {
                otherInstalments.add(instalment);
            }
        }

        if (amt != totalInstalmentToBePaid) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "total to be paid instalment value should be " + totalInstalmentToBePaid + " but it is  " + amt);
        }

        //note: this way of adding discount in the midway of instalment order will result in negative margins while consumption
        int newDiscountAvailable = 0;
        if (StringUtils.isNotEmpty(instalmentUpdateReq.getCouponCode())) {
            newDiscountAvailable = couponManager.validateAndCalculateDiscountForCouponCode(instalmentUpdateReq.getCouponCode(),
                    orderUserId, orderedItem.getEntityId(),
                    orderedItem.getEntityType(), CouponType.VEDANTU_DISCOUNT, totalInstalmentToBePaid, null, PaymentType.INSTALMENT);
        }

        int newDiscountAvailableTemp = newDiscountAvailable;
        int availableDiscountFromToBePaidInstalmentsTemp = availableDiscountFromToBePaidInstalments;
        boolean orderUpdated = false;
        Orders order= orders.get(0);
        int preDiscount = order.getItems().get(0).getVedantuDiscountAmount();
        int preAmount =order.getAmount();

        logger.info("newDiscountAvailable after applying coupon " + newDiscountAvailable);

        logger.info("applying discounts for " + toBePaidInstalments);
        if ((newDiscountAvailable > 0 || availableDiscountFromToBePaidInstalments > 0)
                && totalInstalmentToBePaid > 0) {
            Iterator<Instalment> itr = toBePaidInstalments.iterator();
            while (itr.hasNext()) {
                Instalment instalment = itr.next();
                logger.info("calculating discount for " + instalment);
                if (itr.hasNext()) {
                    float proportion = ((float) instalment.getTotalAmount()) / totalInstalmentToBePaid;
                    logger.info("proportion " + proportion);
                    int originalVDisAmt = (int) (availableDiscountFromToBePaidInstalmentsTemp * proportion);
                    int newVDisAmt = (int) (newDiscountAvailableTemp * proportion);
                    availableDiscountFromToBePaidInstalments -= originalVDisAmt;
                    newDiscountAvailable -= newVDisAmt;
                    int newAmtToPay = instalment.getTotalAmount() - newVDisAmt;
                    logger.info("newVDisAmt " + newVDisAmt + " originalVDisAmt " + originalVDisAmt);
                    logger.info("availableDiscountFromToBePaidInstalments " + availableDiscountFromToBePaidInstalments);
                    logger.info("newDiscountAvailable " + newDiscountAvailable);
                    if (newAmtToPay < 0) {
                        logger.error("amt to pay after new discount is -ve, instalment: " + instalment + " newAmtToPay " + newAmtToPay);
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "amt to pay after new discount is -ve");
                    }
                    instalment.setTotalAmount(newAmtToPay);
                    instalment.setHoursCost(newAmtToPay);
                    //adding history of vdiscounts
                    instalment.getvDiscountChanges().add(new InstalmentVDiscountChange(instalment.getVedantuDiscountAmount(),
                            (originalVDisAmt + newVDisAmt), instalmentUpdateReq.getCouponCode(), System.currentTimeMillis(),
                            instalmentUpdateReq.getCallingUserId()));
                    instalment.setVedantuDiscountAmount(originalVDisAmt + newVDisAmt);

                    if(newVDisAmt > 0 ) {
                        orderUpdated = true;
                        order.setAmount(order.getAmount() - newVDisAmt);
                        order.getItems().get(0).setCost(order.getItems().get(0).getCost() - newVDisAmt);
                        order.getItems().get(0).setVedantuDiscountAmount(order.getItems().get(0).getVedantuDiscountAmount() + newVDisAmt);
                    }
                } else {
                    logger.info("newDiscountAvailable " + newDiscountAvailable);
                    int newAmtToPay = instalment.getTotalAmount() - newDiscountAvailable;
                    if (newAmtToPay < 0) {
                        logger.error("amt to pay after new discount is -ve, instalment: " + instalment + " newAmtToPay " + newAmtToPay);
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "amt to pay after new discount is -ve");
                    }
                    instalment.setTotalAmount(newAmtToPay);
                    instalment.setHoursCost(newAmtToPay);
                    //adding history of vdiscounts
                    instalment.getvDiscountChanges().add(new InstalmentVDiscountChange(instalment.getVedantuDiscountAmount(),
                            (availableDiscountFromToBePaidInstalments + newDiscountAvailable), instalmentUpdateReq.getCouponCode(), System.currentTimeMillis(),
                            instalmentUpdateReq.getCallingUserId()));
                    instalment.setVedantuDiscountAmount(availableDiscountFromToBePaidInstalments + newDiscountAvailable);

                    if(newDiscountAvailable > 0 ) {
                        orderUpdated = true;
                        order.setAmount(order.getAmount() - newDiscountAvailable);
                        order.getItems().get(0).setCost(order.getItems().get(0).getCost() - newDiscountAvailable);
                        order.getItems().get(0).setVedantuDiscountAmount(order.getItems().get(0).getVedantuDiscountAmount() + newDiscountAvailable);
                        instalment.setVedantuDiscountAmount(availableDiscountFromToBePaidInstalments + newDiscountAvailable);
                    }


                }
            }
            logger.info("after applying the  discounts the new instalments ");
            logger.info(toBePaidInstalments);
        }

        if (!instalmentUpdateReq.isCheckValidity()) {
            for (Instalment instalment : toBePaidInstalments) {
                instalmentDAO.save(instalment);
            }
            //mainly for saving deleted entries
            for (Instalment instalment : otherInstalments) {
                instalmentDAO.save(instalment);
            }
            if(orderUpdated){

                order.getOrderAmountChangeLog().add( new OrderAmountChangeLog(
                        preDiscount,order.getItems().get(0).getVedantuDiscountAmount(),
                        instalmentUpdateReq.getCouponCode(),preAmount,order.getAmount(),
                        System.currentTimeMillis(), instalmentUpdateReq.getCallingUserId()));
                ordersDAO.create(order);
            }

            if (StringUtils.isNotEmpty(instalmentUpdateReq.getCouponCode())) {
                couponManager.addRedeemEntryForCouponCode(instalmentUpdateReq.getCouponCode(), orderUserId,
                        newDiscountAvailable, orderedItem.getEntityId(),
                        orderedItem.getEntityType(),
                        RedeemedCouponState.PROCESSED, null, null);
            }
            //why are we making another db query here
            return getInstalmentsForOrder(orders.get(0).getId());
        } else {
            otherInstalments.addAll(toBePaidInstalments);
            return otherInstalments;
        }
    }

    public List<String> markBulkOrderEnded() throws VException {

        Boolean flag = true;
        int start = 0;
        List<String> delivarableIds = new ArrayList();
        while (flag) {

            GetBundleEnrolmentsReq req = new GetBundleEnrolmentsReq();
            req.setStatus(EntityStatus.ENDED);
            req.setLastUpdated(1543622425000L);
            req.setSize(20);
            req.setStart(start);
            String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
            String getEnrollmentsUrl = SUBSCRIPTION_ENDPOINT + "bundle/getEnrolments";
            if (!StringUtils.isEmpty(queryString)) {
                getEnrollmentsUrl += "?" + queryString;
            }

            ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<BundleEnrolmentInfo>>() {
            }.getType();
            List<BundleEnrolmentInfo> bundleEnrolmentInfos = new Gson().fromJson(jsonString, listType);
            if (ArrayUtils.isEmpty(bundleEnrolmentInfos) || bundleEnrolmentInfos.size() < 20) {
                flag = false;
            }
            for (BundleEnrolmentInfo bundleEnrolmentInfo : bundleEnrolmentInfos) {
                delivarableIds.add(bundleEnrolmentInfo.getId());
            }
            start += 20;
        }
        flag = true;
        start = 0;

        while (flag) {
            GetEnrollmentsReq batchReq = new GetEnrollmentsReq();
            batchReq.setStatus(EntityStatus.ENDED);
            batchReq.setLastUpdated(1543622425000L);
            batchReq.setSize(20);
            batchReq.setStart(start);
            String batchQueryString = WebUtils.INSTANCE.createQueryStringOfObject(batchReq);
            String getBatchEnrollmentsUrl = SUBSCRIPTION_ENDPOINT + "enroll/enrollments";
            if (!StringUtils.isEmpty(batchQueryString)) {
                getBatchEnrollmentsUrl += "?" + batchQueryString;
            }

            ClientResponse batchResp = WebUtils.INSTANCE.doCall(getBatchEnrollmentsUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(batchResp);
            String batchJsonString = batchResp.getEntity(String.class);
            Type batchListType = new TypeToken<ArrayList<EnrollmentPojo>>() {
            }.getType();
            List<EnrollmentPojo> enrollmentPojoList = new Gson().fromJson(batchJsonString, batchListType);
            if (ArrayUtils.isEmpty(enrollmentPojoList) || enrollmentPojoList.size() < 20) {
                flag = false;
            }
            for (EnrollmentPojo enrollmentPojo : enrollmentPojoList) {
                delivarableIds.add(enrollmentPojo.getId());
            }
            start += 20;
        }

        flag = true;
        start = 0;

        while (flag) {
            GetCoursePlansReq courseReq = new GetCoursePlansReq();
            courseReq.setStates(Arrays.asList(CoursePlanEnums.CoursePlanState.ENDED));
            courseReq.setLastUpdated(1543622425000L);
            courseReq.setSize(20);
            courseReq.setStart(start);
            String batchQueryString = WebUtils.INSTANCE.createQueryStringOfObject(courseReq);
            String getBatchEnrollmentsUrl = SUBSCRIPTION_ENDPOINT + "courseplan/get";
            if (!StringUtils.isEmpty(batchQueryString)) {
                getBatchEnrollmentsUrl += "?" + batchQueryString;
            }

            ClientResponse batchResp = WebUtils.INSTANCE.doCall(getBatchEnrollmentsUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(batchResp);
            String batchJsonString = batchResp.getEntity(String.class);
            Type batchListType = new TypeToken<ArrayList<CoursePlanInfo>>() {
            }.getType();
            List<CoursePlanInfo> enrollmentPojoList = new Gson().fromJson(batchJsonString, batchListType);
            if (ArrayUtils.isEmpty(enrollmentPojoList) || enrollmentPojoList.size() < 20) {
                flag = false;
            }
            for (CoursePlanInfo enrollmentPojo : enrollmentPojoList) {
                delivarableIds.add(enrollmentPojo.getId());
            }
            start += 20;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.PURCHASING_ENTITIES_DELIVERABLE_ID).in(delivarableIds));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).ne(EntityType.BUNDLE_PACKAGE));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_TYPE).in(PaymentType.BULK));
        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);
        List<String> orderIds = new ArrayList<>();
        for (Orders order : orders) {
            orderIds.add(order.getId());
        }
        Update update = new Update();
        update.set(Instalment.Constants.PAYMENT_STATUS, PaymentStatus.FORFEITED);
        update.set(Instalment.Constants.LAST_UPDATED, System.currentTimeMillis());
        update.set(Instalment.Constants.LAST_UPDATED_BY, "SYSTEM");
        ordersDAO.updateMultiple(query, update);
        return orderIds;
    }
    public List<String> markInstallmentForfited() throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(PaymentStatus.FORFEITED));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_TYPE).is(PaymentType.INSTALMENT));
        query.addCriteria(Criteria.where(Orders.Constants.LAST_UPDATED).gt(1558692632000L));
        List<Orders> orders = ordersDAO.getOrders(query);
        List<String> orderId = new ArrayList<>();
        for(Orders order :  orders){
            orderId.add(order.getId());
        }
        Query inQuery = new Query();
        inQuery.addCriteria(Criteria.where(Instalment.Constants.PAYMENT_STATUS).in(PaymentStatus.NOT_PAID,PaymentStatus.PAYMENT_SUSPENDED));
        inQuery.addCriteria(Criteria.where(Instalment.Constants.ORDER_ID).in(orderId));
        Update update = new Update();
        update.set(Instalment.Constants.PAYMENT_STATUS, PaymentStatus.FORFEITED);
        update.set(Instalment.Constants.LAST_UPDATED, System.currentTimeMillis());
        update.set(Instalment.Constants.LAST_UPDATED_BY, "SYSTEM");
        update.set("markForfitedManualy", true);
        instalmentDAO.updateMultiple(inQuery, update);
        return orderId;

    }

    public List<Instalment>  getDueInstallment() throws VException {

        return instalmentDAO.getDueInstalments();
    }

    public void addEditBaseInstalmentForUsers(BaseInstalment baseInstalment) {
        BaseInstalment oldBaseInstalment = baseInstalmentDAO.getStudentBaseInstalment(baseInstalment.getPurchaseEntityType(),
                baseInstalment.getPurchaseEntityId(), baseInstalment.getUserId());
        logger.info("oldBaseInstalment: " + oldBaseInstalment);
        if (oldBaseInstalment == null) {
            if (CollectionUtils.isNotEmpty(baseInstalment.getInfo())) {
                baseInstalmentDAO.save(baseInstalment);
            }
        } else {
            oldBaseInstalment.setInfo(baseInstalment.getInfo());
            baseInstalmentDAO.save(oldBaseInstalment);
        }
    }
    public Map<String,String> getBundleName(List<String> bundleIds) throws VException {
        Set<String> cachedKeys = new HashSet<>();
        Map <String , String> bundleMap = new HashMap<>();
        List<String> nonExistingBundles = new ArrayList<>();
        bundleIds.forEach(bundleId ->{
            cachedKeys.add(getAioNameCacheKey(bundleId));
        });
        logger.info("============================  2  " +bundleIds.toString() );
        Map<String,String> resp = null;
        try {
            resp = redisDAO.getValuesForKeys(cachedKeys);
        } catch (Exception e) {
            logger.error("Error in getting from redis for key: "  + e.getMessage());
        }
        GetBundlesReq req = new GetBundlesReq ();
        if (resp != null && !resp.isEmpty()) {
            for(String bundleId: bundleIds   ){
                if(resp.containsKey(getAioNameCacheKey( bundleId)) && StringUtils.isNotEmpty(resp.get(getAioNameCacheKey(bundleId))) ){
                    bundleMap.put(bundleId,resp.get(getAioNameCacheKey(bundleId)));
                }
                else{
                    nonExistingBundles.add(bundleId);

                }
            }
            req.setBundleIds(nonExistingBundles);
        }
        else{

            req.setBundleIds(bundleIds);
        }


if(ArrayUtils.isNotEmpty( req.getBundleIds())) {

    ClientResponse apiResp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT + "/bundle/getBundlesNames", HttpMethod.POST,
            new Gson().toJson(req));
    VExceptionFactory.INSTANCE.parseAndThrowException(apiResp);
    String jsonString = apiResp.getEntity(String.class);
    Type listType1 = new TypeToken<ArrayList<BundleInfo>>() {
    }.getType();
    List<BundleInfo> bundleInfoList = new Gson().fromJson(jsonString, listType1);
    if (ArrayUtils.isNotEmpty(bundleInfoList)) {

        bundleInfoList.forEach(bundleInfo -> {
            bundleMap.put(bundleInfo.getId(), bundleInfo.getTitle());
            try {
                redisDAO.set(getAioNameCacheKey(bundleInfo.getId()), bundleInfo.getTitle());
            } catch (InternalServerErrorException e) {
                logger.error("Error in getting from redis for key: " + e.getMessage());
            }

        });

    }
}

       return bundleMap;

    }

    private  String getAioNameCacheKey(String key){
        return "AIO_NAME_" + key;
    }

    public void syncInstallmentsWithLS(List<String> installmentIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Instalment.Constants._ID).in(installmentIds));
        List<Instalment> instalments = instalmentDAO.getInstalments(query);

        for(Instalment instalment : instalments){
            instalmentAsyncTasksManager.triggerLeadSquareQueue(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.INSTALMENT_SAVED, new Gson().toJson(instalment), instalment.getUserId().toString()+"_INST_"+1);
        }
    }
}
