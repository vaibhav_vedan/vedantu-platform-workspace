/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ajith
 */
@Service
public class BundlePackageCouponValidator extends AbstractCouponValidator {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BundlePackageCouponValidator.class);

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    public BundlePackageCouponValidator() {
        super();
    }

    @Override
    public List<String> getGrades(String bundleId) {
        BundlePackageInfo bundleInfo = getBundlePackageInfo(bundleId);
        if (bundleInfo != null && bundleInfo.getGrade() != null) {
            return Arrays.asList(bundleInfo.getGrade().toString());
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsGrade() {
        return true;
    }

    @Override
    public List<String> getCategories(String bundleId) {
        BundlePackageInfo bundleInfo = getBundlePackageInfo(bundleId);
        if (bundleInfo != null) {
            return Arrays.asList(bundleInfo.getTarget());
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsCategory() {
        return true;
    }

//    @Override
//    public List<String> getSubjects(String bundleId) {
//        BundlePackageInfo bundleInfo = getBundlePackageInfo(bundleId);
//        if (bundleInfo != null) {
//            return Arrays.asList(bundleInfo.getSubject().toLowerCase());
//        } else {
//            return null;
//        }
//    }
//
//    @Override
//    public boolean supportsSubject() {
//        return true;
//    }

    @Override
    public List<String> getBundlePackageIds(String purchasingEntityId) {
        return Arrays.asList(purchasingEntityId);
    }

    @Override
    public boolean supportsBundlePackage() {
        return true;
    }

//    @Override
//    public List<String> getTeacherIds(String id) {
//        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanInfo(id);
//        if (coursePlanBasicInfo != null) {
//            return Arrays.asList(coursePlanBasicInfo.getTeacherId().toString());
//        } else {
//            return null;
//        }
//    }
//
//    @Override
//    public boolean supportsTeacher() {
//        return true;
//    }

    public BundlePackageInfo getBundlePackageInfo(String id) {
        BundlePackageInfo bundleInfo = null;
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                    + "/bundlePackage/" + id, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            bundleInfo = new Gson().fromJson(jsonString, BundlePackageInfo.class);
        } catch (Exception e) {
            logger.error("Error in getBundleInfo " + id);
        }
        return bundleInfo;
    }

}
