package com.vedantu.dinero.managers;

import com.google.common.collect.Lists;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.entity.*;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.enums.*;
import com.vedantu.dinero.entity.PaymentGateway;
import com.vedantu.dinero.entity.PaymentOffer;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.enums.*;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.dinero.managers.payment.*;
import com.vedantu.dinero.managers.payment.Paytm.PayTmMiniPaymentManager;
import com.vedantu.dinero.pojo.*;
import com.vedantu.dinero.request.*;
import com.vedantu.dinero.managers.payment.AxisJuspayPaymentManager;
import com.vedantu.dinero.managers.payment.AxisPaymentManager;
import com.vedantu.dinero.managers.payment.CCAvenuePaymentManager;
import com.vedantu.dinero.managers.payment.IPaymentManager;
import com.vedantu.dinero.managers.payment.JuspayPaymentManager;
import com.vedantu.dinero.managers.payment.PayuPaymentManager;
import com.vedantu.dinero.managers.payment.RazorpayPaymentManager;
import com.vedantu.dinero.managers.payment.ZaakpayPaymentManager;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.EmiInfo;
import com.vedantu.dinero.pojo.ExtTransactionPojo;
import com.vedantu.dinero.pojo.FirstExtTransaction;
import com.vedantu.dinero.pojo.LockBalanceInfo;
import com.vedantu.dinero.pojo.ManuallyChangedLogs;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.OrderedItemNames;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.pojo.UpdatesForOrdersRequestPOJO;
import com.vedantu.dinero.request.AddPaymentGatewayReq;
import com.vedantu.dinero.request.BuyItemsReq;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.request.CreatePaymentInvoiceReq;
import com.vedantu.dinero.request.ExportOrdersCsvReq;
import com.vedantu.dinero.request.GetDueInstalmentsForTimeReq;
import com.vedantu.dinero.request.GetMyOrdersReq;
import com.vedantu.dinero.request.GetOrderDetailsReq;
import com.vedantu.dinero.request.GetOrderInfoReq;
import com.vedantu.dinero.request.GetOrdersReq;
import com.vedantu.dinero.request.GetOrdersWithoutDeliverableEntityReq;
import com.vedantu.dinero.request.GetTransactionsReq;
import com.vedantu.dinero.managers.payment.*;
import com.vedantu.dinero.pojo.*;
import com.vedantu.dinero.request.*;
import com.vedantu.dinero.request.Instalment.GetInstalmentsReq;
import com.vedantu.dinero.response.*;
import com.vedantu.dinero.serializers.*;
import com.vedantu.dinero.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.dao.SessionPayoutDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.dinero.sql.entity.SessionPayout;
import com.vedantu.dinero.util.PaymentUtils;
import com.vedantu.dinero.util.RedisDAO;
import com.vedantu.exception.*;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.request.AddRegistrationReq;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.scheduling.pojo.session.ReferenceType;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.pojo.*;
import com.vedantu.subscription.request.GetRegisteredCoursesReq;
import com.vedantu.subscription.request.ResetInstallmentStateReq;
import com.vedantu.subscription.request.SubscriptionVO;
import com.vedantu.subscription.response.StructuredCourseInfo;
import com.vedantu.subscription.response.SubscriptionPlanInfoResp;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SortType;
import com.vedantu.util.response.FOSAgentRes;
import com.vedantu.util.security.DevicesAllowed;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.util.subscription.SubscriptionRequestSubState;
import com.vedantu.util.subscription.UpdateSubscriptionRequestSubState;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import com.paytm.pg.merchant.*;

import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;

@Service
public class PaymentManager {

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    public FosUtils fosUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentManager.class);

    @Autowired
    private CCAvenuePaymentManager cCAvenuePaymentManager;

    @Autowired
    EmiCardDAO emiCardDao;

    @Autowired
    private ZaakpayPaymentManager zaakpayPaymentManager;

    @Autowired
    private PayuPaymentManager payuPaymentManager;

    @Autowired
    private RazorpayPaymentManager razorpayPaymentManager;

    @Autowired
    private AxisPaymentManager axisPaymentManager;

    @Autowired
    private AxisJuspayPaymentManager axisJuspayPaymentManager;

    @Autowired
    private JuspayPaymentManager juspayPaymentManager;

    @Autowired
    private PayTmMiniPaymentManager payTmMiniPaymentManager;

    @Autowired
    private PaymentInvoiceManager paymentInvoiceManager;

    @Autowired
    private CouponManager couponManager;

    @Autowired
    public PaymentGatewayDAO paymentGatewayDAO;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    BaseSubscriptionPaymentPlanDAO baseSubscriptionPaymentPlanDAO;

    @Autowired
    private PlatformTools platformTools;

    @Autowired
    public OrdersDAO ordersDAO;

    @Autowired
    public AccountDAO accountDAO;

    @Autowired
    public PricingManager pricingManager;

    @Autowired
    public AccountManager accountManager;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public InstalmentManager instalmentManager;

    @Autowired
    public InstalmentAsyncTasksManager instalmentAsyncTasksManager;

    @Autowired
    public SessionPayoutDAO sessionPayoutDAO;

    @Autowired
    public InstalmentDAO instalmentDAO;

    @Autowired
    public PaymentOfferDao paymentOfferDao;

    @Autowired
    public BankDetailDAO bankDetailDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    private static final Gson gson = new Gson();

    private static int totalPaymentsInCycle;
    private static int totalPayments = 0;

    private String subscriptionEndPoint;
    private final static String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");

    // TODO: Move this to cache or db
    private static Map<PaymentGatewayName, Integer> paymentGatewaysDistribution = new HashMap<>();

    private transient final Predicate<EmiCard> nonNullAndEmpty = e -> e != null && !StringUtils.isNotEmpty(e.getId());

    private static final int MAX_AMOUNT_TO_USE_AXIS_JUSPAY = 200000;
    //Added to check userAgent and save source of the order placed
    private static final String ANDROID = "Android";
    private static final String ANDROID_SDK = "Android SDK";
    private static final String IPHONE = "iPhone";
    private static final String ANDROID_BUILD = "Build";

    @Autowired
    private DozerBeanMapper mapper;
    private final Long maxDiffBetweenTransactions;

    public PaymentManager() {
        super();
        totalPaymentsInCycle = ConfigUtils.INSTANCE.getIntValue("payment.noofpaymentsInCycle");
        subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        maxDiffBetweenTransactions = ConfigUtils.INSTANCE.getLongValue("payment.maxDiffBetweenTransactions");

    }

    public IPaymentManager getPaymentManger(PaymentGatewayName gatewayName) throws BadRequestException {
        logger.info("Entering " + gatewayName.name());
        switch (gatewayName) {
            case CCAVENUE:
                return cCAvenuePaymentManager;
            case ZAAKPAY:
                return zaakpayPaymentManager;
            case PAYU:
                return payuPaymentManager;
            case RAZORPAY:
                return razorpayPaymentManager;
            case AXIS:
                return axisPaymentManager;
            case AXIS_JUSPAY:
                return axisJuspayPaymentManager;
            case JUSPAY:
                return juspayPaymentManager;
            case PAYTM_MINI:
                return payTmMiniPaymentManager;

            default:
                logger.error("Payment Gateway not supported");
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Payment Gateway not supported");
        }
    }

    public IPaymentManager getPaymentManager(Long userId, int amount) throws ForbiddenException {

        logger.info("Entering " + userId.toString());

        List<PaymentGateway> paymentGateways = paymentGatewayDAO.getPaymentGateways(null, null, true,
                paymentGatewayDAO.NO_START, paymentGatewayDAO.NO_LIMIT);

        if (amount < MAX_AMOUNT_TO_USE_AXIS_JUSPAY && paymentGateways != null) {
            PaymentGateway paymentGatewayToRemove = null;
            for (PaymentGateway paymentGateway : paymentGateways) {
                if (PaymentGatewayName.AXIS_JUSPAY.name().equalsIgnoreCase(paymentGateway.getName())) {
                    paymentGatewayToRemove = paymentGateway;
                    break;
                }
            }
            paymentGateways.remove(paymentGatewayToRemove);
        }

        if (ArrayUtils.isEmpty(paymentGateways)) {
            return razorpayPaymentManager;
        }

        List<ExtTransaction> extTransactions = extTransactionDAO.getExtTransactions(userId, 0L, 20L);
        logger.info("Last External Transactions count " + extTransactions.size());

        return _getPaymentManager(paymentGateways, extTransactions);
    }

    public IPaymentManager _getPaymentManager(List<PaymentGateway> paymentGateways,
            List<ExtTransaction> extTransactions) throws ForbiddenException {
        if (totalPayments == 0 || totalPayments >= totalPaymentsInCycle || paymentGatewaysDistribution == null) {
            paymentGatewaysDistribution.clear();

            for (PaymentGateway paymentGateway : paymentGateways) {
                paymentGatewaysDistribution.put(PaymentGatewayName.getPaymentGatewayName(paymentGateway.getName()), 0);
            }
            // VCacheManager.INSTANCE.put("paymentGatewaysDistribution",
            // paymentGatewaysDistribution);
            totalPayments = 0;
        }

        Map<PaymentGatewayName, Integer> gatewayFailure = new HashMap<>();

        for (PaymentGateway paymentGateway : paymentGateways) {
            gatewayFailure.put(PaymentGatewayName.getPaymentGatewayName(paymentGateway.getName()), 0);
        }
        if (extTransactions != null) {
            Long lastNonSuccessTime = null;
            for (ExtTransaction extTransaction : extTransactions) {
                if (extTransaction.getGatewayName() != null
                        && gatewayFailure.get(extTransaction.getGatewayName()) != null) {

                    if (TransactionStatus.SUCCESS.equals(extTransaction.getStatus())
                            && gatewayFailure.get(extTransaction.getGatewayName()) < 2) {
                        try {
                            logger.info("Returning last successful gateway");
                            return getPaymentManger(extTransaction.getGatewayName());
                        } catch (BadRequestException ex) {
                            logger.error(ex.getErrorMessage());
                        }
                    } else if (TransactionStatus.FAILED.equals(extTransaction.getStatus())) {
                        if (lastNonSuccessTime != null
                                && (lastNonSuccessTime - extTransaction.getCreationTime() < maxDiffBetweenTransactions)) {
                            gatewayFailure.put(extTransaction.getGatewayName(),
                                    (gatewayFailure.get(extTransaction.getGatewayName()) + 2));
                        } else {
                            gatewayFailure.put(extTransaction.getGatewayName(),
                                    (gatewayFailure.get(extTransaction.getGatewayName()) + 1));
                        }
                        lastNonSuccessTime = extTransaction.getCreationTime();
                    } else if (TransactionStatus.CANCELLED.equals(extTransaction.getStatus())
                            || TransactionStatus.PENDING.equals(extTransaction.getStatus())) {
                        if (lastNonSuccessTime != null
                                && (lastNonSuccessTime - extTransaction.getCreationTime() < maxDiffBetweenTransactions)) {
                            gatewayFailure.put(extTransaction.getGatewayName(),
                                    (gatewayFailure.get(extTransaction.getGatewayName()) + 2));
                        }
                        lastNonSuccessTime = extTransaction.getCreationTime();
                    }
                    //no need of resetting when only success happens
                    //when the failed count is increased from 2, the logic needs to relooked
                }
            }
        }

        logger.info("Gateway Failure map " + gatewayFailure);

        Iterator<PaymentGateway> it = paymentGateways.iterator();
        List<PaymentGateway> paymentGatewaysClone = new ArrayList<>();
        while (it.hasNext()) {
            PaymentGateway gateway = it.next();
            if (gatewayFailure.get(PaymentGatewayName.getPaymentGatewayName(gateway.getName())) >= 2) {
                it.remove();
            }
            paymentGatewaysClone.add(gateway);
        }

        if (paymentGateways.isEmpty()) {
            //if none is found, all payment gateways will be in contention
            paymentGateways.addAll(paymentGatewaysClone);
        }

        int totalAssignedPercentage = 0;
        for (PaymentGateway paymentGateway : paymentGateways) {
            totalAssignedPercentage += paymentGateway.getPercentageTraffic();
        }

        if (totalAssignedPercentage == 0) {
            totalAssignedPercentage = 100;
        }

        int allottedPercentage = 0;
        for (int k = 0; k < paymentGateways.size() - 1; k++) {
            int percent = paymentGateways.get(k).getPercentageTraffic() * 100 / totalAssignedPercentage;
            paymentGateways.get(k).setPercentageTraffic(percent);
            allottedPercentage += percent;
        }
        paymentGateways.get(paymentGateways.size() - 1)
                .setPercentageTraffic(100 - allottedPercentage);

        logger.info("Now paymentGateways for round robin " + paymentGateways);
        logger.info("Values " + totalPayments + " " + totalPaymentsInCycle + " " + paymentGatewaysDistribution);

        totalPayments++;
        // TODO Delete this part
        paymentGateways.removeIf(e -> e != null && e.getName() != null
                && ("axis".equalsIgnoreCase(e.getName().trim()) || "juspay".equalsIgnoreCase(e.getName().trim())));

        for (int i = 0; i < 500; i++) {
            int noOfenabledGateways = paymentGateways.size();
            Random randomGenerator = new Random();
            int randomInt = randomGenerator.nextInt(noOfenabledGateways);
            PaymentGateway paymentGateway = paymentGateways.get(randomInt);
            int disbursedRequestsToCurrentGateway = 0;
            if (paymentGatewaysDistribution
                    .get(PaymentGatewayName.getPaymentGatewayName(paymentGateway.getName())) != null) {
                disbursedRequestsToCurrentGateway = paymentGatewaysDistribution
                        .get(PaymentGatewayName.getPaymentGatewayName(paymentGateway.getName()));
            } else {
                paymentGatewaysDistribution
                        .put(PaymentGatewayName.getPaymentGatewayName(paymentGateway.getName()), 0);
            }
            int maxAllottedDisbursalRequestsToCurrentGateway = (paymentGateway.getPercentageTraffic()
                    * totalPaymentsInCycle) / 100;

            if (disbursedRequestsToCurrentGateway < maxAllottedDisbursalRequestsToCurrentGateway) {
                paymentGatewaysDistribution.put(PaymentGatewayName.getPaymentGatewayName(paymentGateway.getName()),
                        disbursedRequestsToCurrentGateway + 1);

                logger.info("Exiting");
                try {
                    return getPaymentManger(PaymentGatewayName.getPaymentGatewayName(paymentGateway.getName()));
                } catch (BadRequestException e) {
                    logger.error(e.getErrorMessage());
                }
            }

        }
        logger.info("No gateway found, returning default");
        return razorpayPaymentManager;
    }

    //Careful with new changes related to otm_bundle
    public PaymentReceiveResponse onPaymentReceive(Map<String, Object> transactionInfo, PaymentGatewayName gatewayName,
            Long callingUserId) throws VException {

        logger.info("Entering");
        IPaymentManager paymentManager = getPaymentManger(gatewayName);
        OrderInfo orderInfo = new OrderInfo();
        PaymentReceiveResponse response = new PaymentReceiveResponse();
        ExtTransaction transaction = paymentManager.onPaymentReceive(transactionInfo, callingUserId);
        response.setTransaction(mapper.map(transaction, ExtTransactionPojo.class));
        if (com.vedantu.dinero.enums.TransactionStatus.SUCCESS.equals(transaction.getStatus())
                && transaction.getVedantuOrderId() != null) {
            try {
                // TODO - SEND EMAIL LIKE SENT in ProcessOrder
                if (PaymentType.BULK.equals(transaction.getPaymentType())) {
                    Orders order = ordersDAO.getById(transaction.getVedantuOrderId());
                    logger.info("order before updating : " + order);
                    if (order == null) {
                        logger.error("no order found with id[" + transaction.getVedantuOrderId() + "]");
                        throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND,
                                "no order found with id[" + transaction.getVedantuOrderId() + "]");
                    }
                    if (!(EntityType.COURSE_PLAN.equals(order.getItems().get(0).getEntityType())
                            || EntityType.OTF_COURSE_REGISTRATION.equals(order.getItems().get(0).getEntityType())
                            || EntityType.OTF_BATCH_REGISTRATION.equals(order.getItems().get(0).getEntityType())
                            || EntityType.OTF.equals(order.getItems().get(0).getEntityType())
                            || EntityType.OTM_BUNDLE_REGISTRATION.equals(order.getItems().get(0).getEntityType())
                            || EntityType.OTM_BUNDLE.equals(order.getItems().get(0).getEntityType())
                            || EntityType.OTM_BUNDLE_ADVANCE_PAYMENT.equals(order.getItems().get(0).getEntityType())
                            || EntityType.COURSE_PLAN_REGISTRATION.equals(order.getItems().get(0).getEntityType())
                            || (EntityType.BUNDLE_PACKAGE.equals(order.getItems().get(0).getEntityType())
                            && ArrayUtils.isNotEmpty(order.getPurchasingEntities())))) {
                        Account account = accountManager.getAccountByUserId(order.getUserId());
                        processOrder(order, account, callingUserId);
                    }
                    orderInfo = getOrderItemPlanInfo(order);
                    response.setOrderInfo(orderInfo);
                } else if (PaymentType.INSTALMENT.equals(transaction.getPaymentType())) {
                    Instalment instalment = instalmentDAO.getEntityById(transaction.getVedantuOrderId(),
                            Instalment.class);
                    if (instalment == null) {
                        logger.error("no instalment found with id[" + transaction.getVedantuOrderId() + "]");
                        throw new NotFoundException(ErrorCode.INSTALMENT_NOT_FOUND,
                                "no instalment found with id[" + transaction.getVedantuOrderId() + "]");
                    }
                    //Process otf_bundle instalment in platform
                    if (!InstalmentPurchaseEntity.COURSE_PLAN.equals(instalment.getContextType())
                            && !InstalmentPurchaseEntity.OTF_BUNDLE.equals(instalment.getContextType())
                            && !InstalmentPurchaseEntity.BATCH.equals(instalment.getContextType())
                            && !InstalmentPurchaseEntity.BUNDLE.equals(instalment.getContextType())) {
                        Account account = accountManager.getAccountByUserId(Long.valueOf(instalment.getTriggeredBy()));
                        instalmentManager.processInstalmentPayment(instalment, account);
                    }
                    response.setInstalmentInfo(mapper.map(instalment, InstalmentInfo.class));
                }
            } catch (Exception e) {
                logger.error(e);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
            }
        }
        if (com.vedantu.dinero.enums.TransactionStatus.SUCCESS.equals(transaction.getStatus())) {
            createPaymentInvoice(transaction);
        }
        logger.info("Exiting " + response.toString());
        return response;
    }

    public OrderInfo buyItems(BuyItemsReq req, DevicesAllowed device) throws VException {
        /*
         * =============================================================
         * ================ Dear future me. Please forgive me. =================
         * ================ I can’t even begin to express how sorry I am. I hope
         * u understand that I hate boilerplate code, but I had no
         * choice=================
         *
         * ================ (if you have not written this code, hope god gives
         * you the strength to carry this forward) =================
         * =============================================================
         */
        logger.info("ENTRY " + req.toString());
        // checks for instalments
        Long callingUserId = req.getCallingUserId();
        OrderItem item = req.getItems().get(0);
        if (req.getPaymentType() == null) {
            req.setPaymentType(PaymentType.BULK);
        }

        // TODO: Remove this once race condition for instalment is handled
        // if (PaymentType.INSTALMENT.equals(req.getPaymentType())) {
        // throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "INSTALMENT
        // not allowed");
        // }
        Integer totalAmount = 0;
        Integer minimumPurchaseAmount = 0;
        Integer totalPromotionalAmount = 0;
        Integer totalNonPromotionalAmount = 0;

        Integer totalPromotionalAmountPayable = 0;
        Integer totalNonPromotionalAmountPayable = 0;

        Long requestUserId = req.getUserId();

        User userInfo = fosUtils.getUserInfo(requestUserId, true);

        if(null == userInfo){
            logger.error("Error While fetching userInfo : userInfo is null");
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Error While fetching userInfo: userInfo is null");
        }
        if (!Role.STUDENT.equals(userInfo.getRole())
                && (EntityType.BUNDLE.equals(item.getEntityType())
                || EntityType.BUNDLE_TRIAL.equals(item.getEntityType()))) {
            //not making the check for all entity types now, as teacher can trigger buyitems via accepting course plan
            throw new ForbiddenException(ErrorCode.ONLY_STUDENT_CAN_PURCHASE, "Non student roles cannot purchase subscription");
        }

        if (Role.ADMIN.equals(sessionUtils.getCallingUserRole())) {
            if (StringUtils.isEmpty(userInfo.getContactNumber())) {
                throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND, "contact number not found");
            }
        } else {
            if (StringUtils.isEmpty(userInfo.getContactNumber()) || !userInfo.getIsContactNumberVerified()) {
                throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND_OR_NOT_VERIFIED, "contact number not found or not verified");
            }
            if (StringUtils.isEmpty(userInfo.getEmail())) {
                throw new ConflictException(ErrorCode.EMAIL_NOT_FOUND, "email not found");
            }
        }

        Account account = accountManager.getAccountByUserId(requestUserId);
        Integer promotionalBalance = account.getPromotionalBalance();

        List<String> couponsUsedInCurrentReq = new ArrayList<>();

        Integer teacherDiscount = 0;
        Integer vedantuDiscount = 0;
        Integer rate = 0;// for OTF cost price, for non-OTF hourly rate
        GetPlanByIdResponse plan = null;// applicable for non OTF,
        if (null != item.getEntityType()) // for now we are only processing plans, this need to be generic if
        // we want to extend this for other options
        {
            switch (item.getEntityType()) {
                case OTF:
                    if (!PaymentType.INSTALMENT.equals(req.getPaymentType())) {
                        String batchUrl = subscriptionEndPoint + "/batch";
                        String url = batchUrl + "/" + item.getEntityId();
                        PlatformBasicResponse response = platformTools.postPlatformData(url, null, false, HttpMethod.GET);
                        if (!response.isSuccess()) {
                            logger.error("Error While fetching batch");
                            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Error While fetching batch");
                        }

                        JsonElement jelement = new JsonParser().parse(response.getResponse());
                        JsonObject jobject = jelement.getAsJsonObject();
                        // JsonPrimitive jobjectCutPrice =
                        // jobject.getAsJsonPrimitive("cutPrice");
                        JsonPrimitive jobjectPurchasePrice = jobject.getAsJsonPrimitive("purchasePrice");
                        // Integer cutPrice = jobjectCutPrice.getAsInt();
                        // Integer purchasePrice = jobjectPurchasePrice.getAsInt();

                        if (jobjectPurchasePrice == null) {
                            logger.error("Error While fetching batch: purchase price is null");
                            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                                    "Error While fetching batch: purchase price is null");
                        }

                        // if (cutPrice != null && cutPrice != 0) {
                        // price = cutPrice;
                        // } else {
                        // price = purchasePrice;
                        // }
                        //
                        Integer price = jobjectPurchasePrice.getAsInt();

                        totalAmount = price * item.getCount();
                        rate = price;
                    } else {

                        Integer price = instalmentManager.getTotalAmountForDiscount(InstalmentPurchaseEntity.BATCH,
                                item.getEntityId(), req.getUserId());

                        totalAmount = price * item.getCount();
                        rate = price;
                    }
                    break;
                case PLAN:
                    if (PurchaseFlowType.SUBSCRIPTION_REQUEST.equals(req.getPurchaseFlowType())) {
                        // TODO move this logic to platform, shyam please do this
                        String getSubscriptionRequestUrl = subscriptionEndPoint + "subscriptionRequest/get/"
                                + req.getPurchaseFlowId();
                        ClientResponse resp = WebUtils.INSTANCE.doCall(getSubscriptionRequestUrl, HttpMethod.GET, null);

                        // TODO will handle multiple accept of requests later, for now
                        // throwing error
                        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

                        String jsonString = resp.getEntity(String.class);
                        logger.info("Response from subscriptionRequest/get/: " + jsonString);
                        com.vedantu.dinero.pojo.SubscriptionRequest subscriptionRequest = gson.fromJson(jsonString,
                                com.vedantu.dinero.pojo.SubscriptionRequest.class);

                        if (!subscriptionRequest.getSubState().equals(SubscriptionRequestSubState.AMOUNT_DEDUCTED)) {
                            // unlock the Balance
                            LockBalanceInfo lockBalanceInfo = gson.fromJson(subscriptionRequest.getLockBalanceInfo(),
                                    LockBalanceInfo.class);
                            lockBalanceInfo.setReferenceId(String.valueOf(subscriptionRequest.getId()));
                            accountManager.unlockBalance(lockBalanceInfo);
                            account = accountDAO.getAccountByUserId(requestUserId);
                            promotionalBalance = account.getPromotionalBalance();
                        }
                    }
                    plan = pricingManager.getPlanById(item.getEntityId());
                    rate = plan.getPrice().intValue();
                    // TODO make null check for hours
                    // TODO not making a check for item.gethours vs session slots
                    // scheduled
                    // if these are different, it will create problems
                    totalAmount = (int) (rate * (item.getHours()) / 3600000);
                    break;
                case INSTALEARN:
                    item.setEntityType(EntityType.PLAN);
                    plan = pricingManager.getPlanById(item.getEntityId());
                    rate = plan.getPrice().intValue();
                    totalAmount = (int) (rate * (item.getHours()) / 3600000);
                    break;
                case OTF_COURSE_REGISTRATION: {
                    String courseId = item.getEntityId();
                    Query query = new Query();
                    query.addCriteria(Criteria.where("userId").is(req.getUserId()));
                    query.addCriteria(
                            Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(EntityType.OTF_COURSE_REGISTRATION.name()));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(courseId));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID).exists(false));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
                    List<Orders> orders = ordersDAO.getOrders(query);
                    if (ArrayUtils.isNotEmpty(orders)) {
                        for (Orders order : orders) {
                            if (PaymentStatus.PAID.equals(order.getPaymentStatus())) {
                                throw new ConflictException(ErrorCode.ALREADY_REGISTERED,
                                        "user is already registered for the course " + courseId);
                            }
                        }
                    }
                    String courseUrl = subscriptionEndPoint + "/course/" + courseId;
                    ClientResponse courseResp = WebUtils.INSTANCE.doCall(courseUrl, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(courseResp);
                    String jsonString = courseResp.getEntity(String.class);
                    CourseBasicInfo courseBasicInfo = gson.fromJson(jsonString, CourseBasicInfo.class);
                    if (courseBasicInfo.getRegistrationFee() == null || courseBasicInfo.getRegistrationFee() < 0) {
                        throw new BadRequestException(ErrorCode.REGISTRATION_FEE_NOT_FOUND, "registration fee not found for course "
                                + courseId + ", coursename " + courseBasicInfo.getTitle());
                    }
                    totalAmount = courseBasicInfo.getRegistrationFee();
                    break;
                }
                case OTF_BATCH_REGISTRATION: {
                    String batchId = item.getEntityId();
                    Query query = new Query();
                    query.addCriteria(Criteria.where("userId").is(req.getUserId()));
                    query.addCriteria(
                            Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(EntityType.OTF_BATCH_REGISTRATION.name()));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(batchId));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID).exists(false));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
                    List<Orders> orders = ordersDAO.getOrders(query);
                    if (ArrayUtils.isNotEmpty(orders)) {
                        for (Orders order : orders) {
                            if (PaymentStatus.PAID.equals(order.getPaymentStatus())) {
                                throw new ConflictException(ErrorCode.ALREADY_REGISTERED,
                                        "user is already registered for the batch " + batchId);
                            }
                        }
                    }
                    String courseUrl = subscriptionEndPoint + "/batch/basicInfo/" + batchId;
                    ClientResponse courseResp = WebUtils.INSTANCE.doCall(courseUrl, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(courseResp);
                    String jsonString = courseResp.getEntity(String.class);
                    BatchBasicInfo batchBasicInfo = gson.fromJson(jsonString, BatchBasicInfo.class);
                    if (batchBasicInfo.getRegistrationFee() == null || batchBasicInfo.getRegistrationFee() < 0) {
//                if (batchBasicInfo.getCourseInfo() == null
//                        || batchBasicInfo.getCourseInfo().getRegistrationFee() == null
//                        || batchBasicInfo.getCourseInfo().getRegistrationFee() <= 0) {
//
//                }
                        throw new BadRequestException(ErrorCode.REGISTRATION_FEE_NOT_FOUND,
                                "registration fee not found for batch " + batchBasicInfo.getCourseInfo().getId());
//                totalAmount = batchBasicInfo.getCourseInfo().getRegistrationFee();

                    } else {
                        totalAmount = batchBasicInfo.getRegistrationFee();
                    }
                    break;
                }
                case OTO_COURSE_REGISTRATION: {
                    String courseId = item.getEntityId();
                    Query query = new Query();
                    query.addCriteria(Criteria.where("userId").is(req.getUserId()));
                    query.addCriteria(
                            Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(EntityType.OTO_COURSE_REGISTRATION.name()));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(courseId));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
                    List<Orders> orders = ordersDAO.getOrders(query);
                    if (ArrayUtils.isNotEmpty(orders)) {
                        for (Orders order : orders) {
                            if (PaymentStatus.PAID.equals(order.getPaymentStatus())) {
                                throw new ConflictException(ErrorCode.ALREADY_REGISTERED,
                                        "user is already registered for the course " + courseId);
                            }
                        }
                    }
                    String courseUrl = subscriptionEndPoint + "/courseplan/structuredCourse/" + courseId;
                    ClientResponse courseResp = WebUtils.INSTANCE.doCall(courseUrl, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(courseResp);
                    String jsonString = courseResp.getEntity(String.class);
                    StructuredCourseInfo courseBasicInfo = gson.fromJson(jsonString, StructuredCourseInfo.class);
                    if (courseBasicInfo.getRegistrationFee() == null) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "registration fee not found for course "
                                + courseId + ", coursename " + courseBasicInfo.getTitle());
                    }
                    totalAmount = courseBasicInfo.getRegistrationFee();
                    break;
                }
                case BUNDLE: {
                    String entityId = item.getEntityId();
                    if(StringUtils.isEmpty( req.getSubscriptionPlanId())) {

                        String checkUrl = subscriptionEndPoint + "/bundle/hasEnrolment?bundleId=" + entityId + "&userId=" + req.getUserId() + "&newState=" + EnrollmentState.REGULAR;
                        ClientResponse checkResp = WebUtils.INSTANCE.doCall(checkUrl, HttpMethod.GET, null);
                        VExceptionFactory.INSTANCE.parseAndThrowException(checkResp);
                        String hasEnrolment = checkResp.getEntity(String.class);
                        logger.info("hasEnrolment " + hasEnrolment);
                        if (StringUtils.isNotEmpty(hasEnrolment)
                                && Boolean.TRUE.equals(Boolean.valueOf(hasEnrolment))) {
                            throw new ConflictException(ErrorCode.ALREADY_ENROLLED,
                                    "user has already purchased for the bundle " + entityId);
                        }
                    }

                    String url = subscriptionEndPoint + "/bundle/" + entityId;
                    ClientResponse courseResp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(courseResp);
                    String jsonString = courseResp.getEntity(String.class);
                    BundleInfo bundleInfo = gson.fromJson(jsonString, BundleInfo.class);
                    if (bundleInfo.getPrice() == null) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                                "registration fee not found for course " + entityId + ", coursename " + bundleInfo.getTitle());
                    }
                    if (bundleInfo.getMinimumPurchasePrice() != null) {
                        minimumPurchaseAmount = bundleInfo.getMinimumPurchasePrice();
                    }
                    if (bundleInfo.getLastPurchaseDate() != null && bundleInfo.getLastPurchaseDate() < System.currentTimeMillis()) {
                        throw new BadRequestException(ErrorCode.LAST_PURCHASE_DATE_IS_OVER,
                                "LastPurchaseDate is over for this AIO " + entityId + ", AIO name " + bundleInfo.getTitle());
                    }

                    if (bundleInfo.getValidTill() != null && bundleInfo.getValidTill() < System.currentTimeMillis()) {
                        throw new BadRequestException(ErrorCode.VALIDITY_IS_OVER,
                                "Validity is over for this AIO " + entityId + ", AIO name " + bundleInfo.getTitle());
                    }

                    if (!BundleState.ACTIVE.equals(bundleInfo.getState())) {
                        throw new BadRequestException(ErrorCode.BUNDLE_NOT_ACTIVE,
                                "AIO is not active for purchase " + entityId + ", AIO name " + bundleInfo.getTitle());
                    }


                    if (StringUtils.isNotEmpty(req.getSubscriptionPlanId())) {

                        BaseSubscriptionPaymentPlan baseSubscriptionPaymentPlan = baseSubscriptionPaymentPlanDAO.getById(req.getSubscriptionPlanId());
                        if(baseSubscriptionPaymentPlan == null){
                            throw new BadRequestException(ErrorCode.PLAN_DOES_NOT_EXIST,
                                    "Subscription Payment Plan  does not exist plan id " + req.getSubscriptionPlanId() );
                        }
                        if (baseSubscriptionPaymentPlan.getPlanDuration() != null && baseSubscriptionPaymentPlan.getPlanDuration().equals(BaseSubscriptionDuration.FULLCOURSE)) {
                            totalAmount = bundleInfo.getPrice();
                        } else {
                            totalAmount = baseSubscriptionPaymentPlan.getPrice();
                        }
                    }else {

                        totalAmount = bundleInfo.getPrice();
                    }
                    break;
                }
                case BUNDLE_TRIAL: {
                    String entityId = item.getEntityId();
                    String checkUrl = subscriptionEndPoint + "/bundle/hasEnrolment?bundleId=" + entityId + "&userId=" + req.getUserId() + "&newState=" + EnrollmentState.TRIAL;
                    ClientResponse checkResp = WebUtils.INSTANCE.doCall(checkUrl, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(checkResp);
                    String hasEnrolment = checkResp.getEntity(String.class);
                    logger.info("hasEnrolment " + hasEnrolment);
                    if (StringUtils.isNotEmpty(hasEnrolment)
                            && Boolean.TRUE.equals(Boolean.valueOf(hasEnrolment))) {
                        throw new ConflictException(ErrorCode.ALREADY_ENROLLED,
                                "user has already purchased for the bundle or already purchased a trail of same grade once" + entityId);
                    }

                    String url = subscriptionEndPoint + "/bundle/" + entityId;
                    ClientResponse courseResp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(courseResp);
                    String jsonString = courseResp.getEntity(String.class);
                    BundleInfo bundleInfo = gson.fromJson(jsonString, BundleInfo.class);
                    if (!bundleInfo.getTrailIncluded()) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                                "Trail not found for AIO " + entityId + ", AIO name " + bundleInfo.getTitle());
                    }
                    if (bundleInfo.getLastPurchaseDate() != null && bundleInfo.getLastPurchaseDate() < System.currentTimeMillis()) {
                        throw new BadRequestException(ErrorCode.LAST_PURCHASE_DATE_IS_OVER,
                                "LastPurchaseDate is over for this AIO " + entityId + ", AIO name " + bundleInfo.getTitle());
                    }
                    if (bundleInfo.getValidTill() != null && bundleInfo.getValidTill() < System.currentTimeMillis()) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                                "Validity is over for this AIO " + entityId + ", AIO name " + bundleInfo.getTitle());
                    }
                    if (!BundleState.ACTIVE.equals(bundleInfo.getState())) {
                        throw new BadRequestException(ErrorCode.BUNDLE_NOT_ACTIVE,
                                "AIO is not active for purchase " + entityId + ", AIO name " + bundleInfo.getTitle());
                    }

                    logger.info("Getting trial data {} and the trial id from the item is {}", bundleInfo.getTrialData(), item.getTrialId());
                    totalAmount
                            = Optional.ofNullable(bundleInfo.getTrialData())
                                    .map(Collection::stream)
                                    .orElseGet(Stream::empty)
                                    .filter(trialData -> trialData.getId().equals(item.getTrialId()))
                                    .map(TrialData::getTrialAmount)
                                    .map(Math::toIntExact)
                                    .findFirst()
                                    .orElse(null);
                    if (Objects.isNull(totalAmount)) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Trial amount cann't be null");
                    }
                    break;
                }
                case BUNDLE_PACKAGE: {
                    String entityId = item.getEntityId();
                    Query query = new Query();
                    query.addCriteria(Criteria.where("userId").is(req.getUserId()));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(EntityType.BUNDLE_PACKAGE.name()));
                    query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(entityId));
                    List<Orders> orders = ordersDAO.getOrders(query);
                    if (ArrayUtils.isNotEmpty(orders)) {
                        for (Orders order : orders) {
                            if (PaymentStatus.PAID.equals(order.getPaymentStatus())) {
                                throw new ConflictException(ErrorCode.ALREADY_PURCHASED,
                                        "user has already paid for the bundle package" + entityId);
                            }
                        }
                    }
                    String url = subscriptionEndPoint + "/bundlePackage/" + entityId;
                    ClientResponse courseResp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(courseResp);
                    String jsonString = courseResp.getEntity(String.class);
                    BundlePackageInfo bundleInfo = gson.fromJson(jsonString, BundlePackageInfo.class);
                    if (bundleInfo.getPrice() == null) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                                "price is not defined for the bundle package " + entityId);
                    }
                    totalAmount = bundleInfo.getPrice();
                    break;
                }
                default:
                    break;
            }
        }

        logger.info("Account of user: " + account);

        Discounts discounts = getCouponDiscount(item.getEntityId(), item.getEntityType(),
                item.getTeacherDiscountCouponId(), item.getVedantuDiscountCouponId(), couponsUsedInCurrentReq,
                requestUserId, totalAmount, req.getPurchaseFlowId(),req.getSubscriptionPlanId());
        teacherDiscount = discounts.teacherDiscount;
        vedantuDiscount = discounts.vedantuDiscount;

        Long vedantuDiscountPerHr = 0l, teacherDiscountPerHr = 0l;

        if (item.getHours() != null && item.getHours() != 0L) {
            vedantuDiscountPerHr = vedantuDiscount * DateTimeUtils.MILLIS_PER_HOUR / item.getHours();
            teacherDiscountPerHr = teacherDiscount * DateTimeUtils.MILLIS_PER_HOUR / item.getHours();
        }
        Integer securityCharge = 0;
        Integer convenienceCharge = 0;
        Integer validMonths = 0;


        if (PaymentType.INSTALMENT.equals(req.getPaymentType())) {
            // get instalments
            Integer pricePerHr;
            List<Instalment> instalments;

            if (EntityType.BUNDLE_PACKAGE.equals(item.getEntityType())) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Instalment not allowed in Bundle");
            }

            if (EntityType.OTF.equals(item.getEntityType())) {
                instalments = instalmentManager.prepareInstalments(InstalmentPurchaseEntity.BATCH, item.getEntityId(),
                        vedantuDiscount, teacherDiscount, null, requestUserId.toString());
            } else if (EntityType.BUNDLE.equals(item.getEntityType())) {
                instalments = instalmentManager.prepareInstalments(InstalmentPurchaseEntity.BUNDLE, item.getEntityId(),
                        vedantuDiscount, teacherDiscount, null, requestUserId.toString());
            } else {
                pricePerHr = plan.getPrice().intValue();
                instalments = instalmentManager.prepareInstalments(item.getSchedule(), pricePerHr,
                        InstalmentPurchaseEntity.PLAN, plan.getId(), vedantuDiscountPerHr.intValue(),
                        teacherDiscountPerHr.intValue(), null,
                        requestUserId.toString());
            }

            if (ArrayUtils.isNotEmpty(instalments)) {
                Instalment firstInstalment = instalments.get(0);
                int hrsCost = firstInstalment.getHoursCost();
                convenienceCharge = firstInstalment.getConvenienceCharge();
                securityCharge = firstInstalment.getSecurityCharge();
                totalAmount = 0;// reset and calculate using instalments
                for (Instalment inst : instalments) {
                    totalAmount += inst.getTotalAmount();
                }
                if (hrsCost < promotionalBalance) {
                    totalPromotionalAmountPayable += hrsCost;
                } else {
                    totalPromotionalAmountPayable += promotionalBalance;
                    totalNonPromotionalAmountPayable += hrsCost - promotionalBalance;
                }
                totalNonPromotionalAmountPayable += (convenienceCharge + securityCharge);// these
                // have
                // to
                // paid
                // from
                // np
                // only
            } else {
                logger.error("Instalments zero for buyItems " + req);
            }
        }
        else {
            totalAmount -= discounts.totalDiscount;
            if (totalAmount < promotionalBalance) {
                totalPromotionalAmount = totalAmount;
                totalPromotionalAmountPayable = totalAmount;
            } else {
                totalPromotionalAmount = promotionalBalance;
                totalPromotionalAmountPayable = promotionalBalance;

                totalNonPromotionalAmount = totalAmount - totalPromotionalAmount;
                totalNonPromotionalAmountPayable = totalNonPromotionalAmount;
            }
        }

        String schedule = null;
        if (item.getSchedule() != null) {
            schedule = gson.toJson(item.getSchedule());
        }

        if (minimumPurchaseAmount > 0) {
            String subUrl = subscriptionEndPoint + "/bundle/isUserPartOfMinMaxExceptionList?userId=" + req.getUserId();
            ClientResponse resp = WebUtils.INSTANCE.doCall(subUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            PlatformBasicResponse platformBasicResponse = gson.fromJson(jsonString, PlatformBasicResponse.class);

            if (platformBasicResponse != null && !platformBasicResponse.isSuccess() && totalAmount < minimumPurchaseAmount) {
                throw new ForbiddenException(ErrorCode.PURCHASE_AMOUNT_LESS_THAN_MINIMUM_PURCHASE_AMOUNT, "TotalAmount is should be be greater then or equal to the minimum puchaseAmount");
            }
        }
        List<OrderedItem> orderedItems = new ArrayList<>();
        orderedItems.add(new OrderedItem(item.getEntityId(), item.getEntityType(), item.getCount(), rate, totalAmount,
                totalPromotionalAmount, totalNonPromotionalAmount, item.getHours(), item.getModel(), item.getTarget(),
                item.getGrade(), item.getBoardId(), schedule, item.getTeacherId(), item.getOfferingId(),
                item.getRenew(), item.getNote(), item.getTeacherDiscountCouponId(), teacherDiscount,
                item.getVedantuDiscountCouponId(), vedantuDiscount, item.getSubModel(), item.getSessionSource()));

        PaymentSource paymentSource = findPaymentSource(req.getUserAgent());

        Orders order = new Orders(requestUserId, orderedItems, req.getIpAddress(), totalAmount, totalPromotionalAmount,
                totalNonPromotionalAmount, PaymentStatus.NOT_PAID, req.getPurchaseFlowType(), req.getPurchaseFlowId(),
                req.getPaymentType(), paymentSource, req.getUserAgent());

        List<PurchasingEntity> purchasingEntities = new ArrayList<>();
        PurchasingEntity purchasingEntity = new PurchasingEntity(getEntityTypeForPurchasingEntity(item.getEntityType()), item.getEntityId());
        purchasingEntities.add(purchasingEntity);
        order.setPurchasingEntities(purchasingEntities);

        if(req.getSubscriptionPlanId() != null){
            order.setSubscriptionPlanId(req.getSubscriptionPlanId());
        }
        // adding UTM params to order
        order.setUtm_campaign(req.getUtm_campaign());
        order.setUtm_content(req.getUtm_content());
        order.setUtm_medium(req.getUtm_medium());
        order.setUtm_source(req.getUtm_source());
        order.setUtm_term(req.getUtm_term());
        order.setChannel(req.getChannel());

        if(req.getPaymentOption() != null && StringUtils.isNotEmpty( req.getPaymentOption().getEmiId())){
            EmiCard emiCard =  emiCardDao.getById(req.getPaymentOption().getEmiId());
            if(emiCard!=null){
                order.setEmiInfo(new EmiInfo(emiCard.getCode(),emiCard.getMonths(),emiCard.getRate()));
            }
        }
        if (StringUtils.isNotEmpty(req.getReEnrollmentOrderId())) {
            order.setReEnrollmentOrderId(req.getReEnrollmentOrderId());
            order.setEnrollmentType(EnrollmentType.RE_ENROLLED);
        }
        if (StringUtils.isNotEmpty(req.getAgentEmployeId())) {
            order.setAgentCode(req.getAgentEmployeId());
        }

        ordersDAO.create(order);

        OrderInfo orderInfo = getOrderItemPlanInfo(order);
        Integer rechargeAmount = null;// this is the non promotional amount to
        // be paid

        if (req.getUseAccountBalance()) {

            // making only check for np as promotional balance is taken into
            // account and require np
            // is calculated
            if (account.getNonPromotionalBalance() < totalNonPromotionalAmountPayable) {
                rechargeAmount = totalNonPromotionalAmountPayable - account.getNonPromotionalBalance();
            } else {
                // transaction will be commited inside processOrder
                processOrder(order, account, req.getCallingUserId(), item.getTrialId());
                order = ordersDAO.getById(order.getId());
                orderInfo = getOrderItemPlanInfo(order);
                orderInfo.setPaymentStatus(order.getPaymentStatus());
                orderInfo.setNeedRecharge(false);
                return orderInfo;
            }
        } else {
            rechargeAmount = (totalNonPromotionalAmountPayable + totalPromotionalAmountPayable);
        }
        if (rechargeAmount == 0) {
            processOrder(order, account, req.getCallingUserId(), item.getTrialId());
            order = ordersDAO.getById(order.getId());
            orderInfo = getOrderItemPlanInfo(order);
            orderInfo.setPaymentStatus(order.getPaymentStatus());
            orderInfo.setNeedRecharge(false);
            return orderInfo;
        }
        //noinspection ConstantConditions
        if (rechargeAmount != null) {
            rechargeAmount = ((int) Math.ceil((double) rechargeAmount / 100)) * 100;
            RechargeReq rechargeReq = new RechargeReq(req.getCallingUserId(), order.getIpAddress(), order.getUserId(),
                    rechargeAmount, req.getRedirectUrl(), null, null, req.getPaymentOption());
            RechargeUrlInfo rechargeUrlInfo = accountManager.rechargeAccount(rechargeReq, device, PaymentType.BULK, order.getId());

            /*RechargeUrlInfo rechargeUrlInfo = accountManager.rechargeAccount(order.getUserId(), order.getIpAddress(),
                    rechargeAmount, order.getId(), PaymentType.BULK, req.getRedirectUrl(), null, null,
                    req.getCallingUserId(), device, req.getPaymentOption() == null ? null : req.getPaymentOption().getPreferredGateway());*/
            orderInfo.setNeedRecharge(true);
            orderInfo.setGatewayName(rechargeUrlInfo.getGatewayName());
            orderInfo.setRechargeUrl(rechargeUrlInfo.getRechargeUrl());
            orderInfo.setForwardHttpMethod(rechargeUrlInfo.getForwardHttpMethod());
            if (rechargeUrlInfo.getPostParams() != null && !rechargeUrlInfo.getPostParams().isEmpty()) {
                orderInfo.setPostParams(rechargeUrlInfo.getPostParams());
            }
        }

        // return payment gateway url
        logger.info("Exiting " + orderInfo.toString());
        return orderInfo;
    }

    private void processOrder(Orders order, Account userAccount, Long callingUserId) throws VException {
        processOrder(order, userAccount, callingUserId, null);
    }

    private void processOrder(Orders order, Account userAccount, Long callingUserId, String trialId) throws VException {
        logger.info("processOrder - Entering " + order.toString() + " userAccount:" + userAccount.toString() + "trialId: " + trialId);

        List<OrderedItem> items = order.getItems();
        if (ArrayUtils.isEmpty(items)) {
            logger.error("OrderedItems empty in process order, OrderId: " + order.getId());
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OrderedItems is empty in process order");
        } else if (items.size() > 1) {
            logger.warn("Order items more than, but processing only one ");
        }
        OrderedItem item = order.getItems().get(0);
        String deliverableEntityId = null;
        String sessionsToScheduleStr = item.getSchedule();// will change if it
        // is instalment

        Transaction transaction = null;
        // create instalments
        List<Instalment> instalments = null;
        if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            // since there was a restriction already for instalment payment as
            // min one instalment
            // just taking the first item and processing it.
            SessionSchedule schedule = new Gson().fromJson(item.getSchedule(), SessionSchedule.class);
            Long vedantuDiscountPerHr = 0l, teacherDiscountPerHr = 0l;

            if (item.getHours() != null && item.getHours() != 0L) {
                vedantuDiscountPerHr = item.getVedantuDiscountAmount().longValue() * DateTimeUtils.MILLIS_PER_HOUR / item.getHours();
                teacherDiscountPerHr = item.getTeacherDiscountAmount().longValue() * DateTimeUtils.MILLIS_PER_HOUR / item.getHours();
            }
            if (EntityType.OTF.equals(item.getEntityType())) {
                // Integer pricePerHr = getOTFHourlyRate(schedule,
                // item.getRate());
                // instalments = instalmentManager.prepareInstalments(schedule,
                // pricePerHr, InstalmentPurchaseEntity.BATCH,
                // item.getEntityId(), discountPerHr.intValue(),
                // order.getId(), order.getUserId().toString());
                instalments = instalmentManager.prepareInstalments(InstalmentPurchaseEntity.BATCH, item.getEntityId(),
                        vedantuDiscountPerHr.intValue(), teacherDiscountPerHr.intValue(), order.getId(), order.getUserId().toString());

            } else if (EntityType.BUNDLE.equals(item.getEntityType())) {
                instalments = instalmentManager.prepareInstalments(InstalmentPurchaseEntity.BUNDLE, item.getEntityId(),
                        item.getVedantuDiscountAmount(), item.getTeacherDiscountAmount(), order.getId(), order.getUserId().toString());

            } else {
                instalments = instalmentManager.prepareInstalments(schedule, item.getRate(),
                        InstalmentPurchaseEntity.PLAN, item.getEntityId(), vedantuDiscountPerHr.intValue(),
                        teacherDiscountPerHr.intValue(), order.getId(),
                        order.getUserId().toString());
                if (ArrayUtils.isNotEmpty(instalments)) {
                    // scheduling only the first instalment slots, so
                    // manipulating the item schedule
                    SessionSchedule sessionSchedule = instalments.get(0).getSessionSchedule();
                    sessionsToScheduleStr = new Gson().toJson(sessionSchedule);
                    item.setHours(sessionSchedule.calculateTotalHours());
                }
            }
            instalmentDAO.saveInstalments(instalments);
        } else {
            transaction = accountManager.transferAmount(userAccount, accountDAO.getVedantuDefaultAccount(),
                    order.getAmount(), order.getPromotionalAmount(), order.getNonPromotionalAmount(),
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                    TransactionRefType.ITEM_ORDERED, order.getId(), Transaction._getTriggredByUser(order.getUserId()),
                    false, null);
            order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PAID, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//            order.setPaymentStatus(PaymentStatus.PAID);
        }

        // creating entities
        if (EntityType.OTF.equals(item.getEntityType())) {

        } else if (EntityType.PLAN.equals(item.getEntityType()) || EntityType.INSTALEARN.equals(item.getEntityType())) {

            try {
                ClientResponse resp;
                if (PurchaseFlowType.SUBSCRIPTION_REQUEST.equals(order.getPurchaseFlowType())) {

                    String createSubscriptionFromRequestUrl = subscriptionEndPoint
                            + ConfigUtils.INSTANCE.getStringValue("vedantu.platform.subscription.createFromRquest");

                    JSONObject createSubscriptionFromRequestParams = new JSONObject();
                    createSubscriptionFromRequestParams.put("subscriptionRequestId", order.getPurchaseFlowId());
                    createSubscriptionFromRequestParams.put("hoursToSchedule", item.getHours());
                    createSubscriptionFromRequestParams.put("callingUserId", callingUserId);

                    resp = WebUtils.INSTANCE.doCall(createSubscriptionFromRequestUrl, HttpMethod.POST,
                            createSubscriptionFromRequestParams.toString());

                } else {
                    SubscriptionVO subscriptionRequest = PaymentUtils.getSubscriptionRequest(item);
                    List<Long> boardIds = new ArrayList<>();
                    boardIds.add(item.getBoardId());
                    // TODO idiotic call, remove this asap
                    Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
                    if (boardMap.containsKey(item.getBoardId())) {
                        subscriptionRequest.setSubject(boardMap.get(item.getBoardId()).getSlug());
                    }
                    logger.info("subscriptionRequest " + subscriptionRequest.toString());
                    SessionSchedule schedule = new Gson().fromJson(sessionsToScheduleStr, SessionSchedule.class);
                    ScheduleType scheduleType;
                    if (schedule == null) {
                        scheduleType = ScheduleType.NFS;
                    } else {
                        subscriptionRequest.setSchedule(schedule);
                        scheduleType = ScheduleType.FS;
                    }
                    subscriptionRequest.setScheduleType(scheduleType);
                    subscriptionRequest.setStudentId(order.getUserId());
                    subscriptionRequest.setCallingUserId(order.getUserId());
                    if (order.getContextId() != null) {
                        subscriptionRequest.setSubscriptionId(order.getContextId());
                    }
                    logger.info("Calling Create Subscription");
                    String createSubscriptionUrl = subscriptionEndPoint
                            + ConfigUtils.INSTANCE.getStringValue("vedantu.platform.subscription.create");

                    resp = WebUtils.INSTANCE.doCall(createSubscriptionUrl, HttpMethod.POST,
                            new Gson().toJson(subscriptionRequest));

                }
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                JsonObject jsonObject = new Gson().fromJson(jsonString, JsonObject.class);
                logger.info("subscription Creation Response " + jsonObject.toString());
                String subscriptionId = jsonObject.get("subscriptionId").getAsString();
                deliverableEntityId = jsonObject.get("subscriptionDetailsId").getAsString();

                item.setDeliverableEntityId(deliverableEntityId);
                item.setDeliverableEntityType(DeliverableEntityType.SUBSCRIPTION_DETAILS);
                order.setContextId(Long.parseLong(subscriptionId));

            } catch (Throwable e) {
                logger.error("Error in processing orderId " + order.getId() + ", Error: " + e.getMessage(), e);
                if (!PaymentType.INSTALMENT.equals(order.getPaymentType())) {
                    logger.info("Reversing transaction " + transaction);
                    transaction = accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(), userAccount,
                            order.getAmount(), order.getPromotionalAmount(), order.getNonPromotionalAmount(),
                            ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                            TransactionRefType.REVERSE_ITEM_ORDERED, order.getId(),
                            Transaction._getTriggredByUser(order.getUserId()), true, null);
                }

                if (e instanceof VException) {
                    VException exception = (VException) e;
                    throw exception;
                } else {
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
                }
            }
        } else if (EntityType.OTF_COURSE_REGISTRATION.equals(item.getEntityType())
                || (EntityType.OTF_BATCH_REGISTRATION.equals(item.getEntityType()))) {
            AddRegistrationReq addRegistrationReq = new AddRegistrationReq();
            addRegistrationReq.setEntityType(item.getEntityType());
            addRegistrationReq.setEntityId(item.getEntityId());
            addRegistrationReq.setUserId(order.getUserId());
            addRegistrationReq.setOrderId(order.getId());
            ClientResponse resp1 = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/registration/addRegistration", HttpMethod.POST, new Gson().toJson(addRegistrationReq));
            VExceptionFactory.INSTANCE.parseAndThrowException(resp1);
        } else if (EntityType.BUNDLE.equals(item.getEntityType()) || EntityType.BUNDLE_TRIAL.equals(item.getEntityType())) {
            try {
                ClientResponse resp;

                String url = subscriptionEndPoint + "/bundle/createEnrolment";

                JSONObject createSubscriptionFromRequestParams = new JSONObject();
                createSubscriptionFromRequestParams.put("entityId", item.getEntityId());
                createSubscriptionFromRequestParams.put("userId", order.getUserId());
                createSubscriptionFromRequestParams.put("orderId", order.getId());
                if (item.getEntityType().equals(EntityType.BUNDLE_TRIAL)) {
                    logger.info("Making order for trial with trialId {}", trialId);
                    createSubscriptionFromRequestParams.put("trialId", trialId);
                    createSubscriptionFromRequestParams.put("state", EnrollmentState.TRIAL);
                } else {
                    createSubscriptionFromRequestParams.put("state", EnrollmentState.REGULAR);
                }

                if(StringUtils.isNotEmpty( order.getSubscriptionPlanId())) {
                    BaseSubscriptionPaymentPlan baseSubscriptionPaymentPlan = baseSubscriptionPaymentPlanDAO.getById(order.getSubscriptionPlanId() );
                    Integer validMonths = baseSubscriptionPaymentPlan.getValidMonths();
                    createSubscriptionFromRequestParams.put("validMonths", validMonths);
                    createSubscriptionFromRequestParams.put("isSubscription", true);
                }


                logger.info("createSubscriptionFromRequestParams -- {}", createSubscriptionFromRequestParams);
                resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, createSubscriptionFromRequestParams.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                JsonObject jsonObject = gson.fromJson(jsonString, JsonObject.class);
                logger.info("enrollment Creation Response " + jsonObject.toString());
                if( jsonObject.get("id") != null) {

                    deliverableEntityId = jsonObject.get("id").getAsString();

                    item.setDeliverableEntityId(deliverableEntityId);
                    item.setDeliverableEntityType(DeliverableEntityType.BUNDLE_ENROLMENT);

                }
                if(jsonObject.get("subscriptionUpdateId") != null ){
                    String  subscriptionUpdateId = jsonObject.get("subscriptionUpdateId").getAsString();
                    order.setSubscriptionUpdateId(subscriptionUpdateId);
                }
                // order.setContextId(Long.parseLong(item.getEntityId()));

            } catch (Throwable e) {
                if(e instanceof VException){
                    VException exception = (VException) e;
                    if(ErrorCode.ALREADY_ENROLLED.equals(exception.getErrorCode())||
                            ErrorCode.BUNDLE_ENROLLMENT_ALREADY_EXIST.equals(exception.getErrorCode())){
                        logger.info("Error in processing orderId {} , Error message: {}, Stack Trace: {}",order.getId(), e.getMessage(), e);
                    }else{
                        logger.error("Error in processing orderId " + order.getId() + ", Error: " + e.getMessage(), e);
                    }
                }else {
                    logger.error("Error in processing orderId " + order.getId() + ", Error: " + e.getMessage(), e);
                }
                if (!PaymentType.INSTALMENT.equals(order.getPaymentType())) {
                    logger.info("Reversing transaction " + transaction);
                    transaction = accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(), userAccount,
                            order.getAmount(), order.getPromotionalAmount(), order.getNonPromotionalAmount(),
                            ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                            TransactionRefType.REVERSE_ITEM_ORDERED, order.getId(),
                            Transaction._getTriggredByUser(order.getUserId()), true, null);
                }

                if (e instanceof VException) {
                    VException exception = (VException) e;
                    throw exception;
                } else {
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
                }
            }
        } else if (EntityType.BUNDLE_PACKAGE.equals(item.getEntityType())) {
            try {
                ClientResponse resp;

                String url = subscriptionEndPoint + "/bundlePackage/createEnrolments";
                JSONObject createSubscriptionFromRequestParams = new JSONObject();
                createSubscriptionFromRequestParams.put("entityId", item.getEntityId());
                createSubscriptionFromRequestParams.put("userId", order.getUserId());

                resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, createSubscriptionFromRequestParams.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                // JsonArray jsonObject = new Gson().fromJson(jsonString,
                // JsonArray.class);
                logger.info("enrollment Creation Response " + jsonString);
                // deliverableEntityId =
                // jsonObject.get("enrolmentId").getAsString();
                //
                //
                // item.setDeliverableEntityId(deliverableEntityId);
                // item.setDeliverableEntityType(DeliverableEntityType.BUNDLE_ENROLMENT);
                // order.setContextId(Long.parseLong(item.getEntityId()));

            } catch (Throwable e) {
                logger.error("Error in processing orderId " + order.getId() + ", Error: " + e.getMessage(), e);
                if (!PaymentType.INSTALMENT.equals(order.getPaymentType())) {
                    logger.info("Reversing transaction " + transaction);
                    transaction = accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(), userAccount,
                            order.getAmount(), order.getPromotionalAmount(), order.getNonPromotionalAmount(),
                            ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                            TransactionRefType.REVERSE_ITEM_ORDERED, order.getId(),
                            Transaction._getTriggredByUser(order.getUserId()), true, null);
                }

                if (e instanceof VException) {
                    VException exception = (VException) e;
                    throw exception;
                } else {
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
                }
            }
        }

        // transfer money to vedantu accounts from student account
        Integer vedantuDiscountToTransfer = item.getVedantuDiscountAmount();
        if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            if (ArrayUtils.isNotEmpty(instalments)) {
                // doing this again because I do not want to save the
                // instalments before the first
                // payment
                Instalment firstInstalment = instalments.get(0);
                Integer instHrsPromotionalAmount = 0;
                Integer instHrsNonPromotionalAmount = 0;
                if (firstInstalment.getHoursCost() < userAccount.getPromotionalBalance()) {
                    instHrsPromotionalAmount = firstInstalment.getHoursCost();
                } else {
                    instHrsPromotionalAmount = userAccount.getPromotionalBalance();
                    instHrsNonPromotionalAmount = firstInstalment.getHoursCost() - userAccount.getPromotionalBalance();
                }
                Integer totalNonPromotionalAmountToBePaid = instHrsNonPromotionalAmount
                        + firstInstalment.getConvenienceCharge() + firstInstalment.getSecurityCharge();

                // as two transfers are needed, making a check beforehand to see
                // if both can go through
                if (userAccount.getPromotionalBalance() >= instHrsPromotionalAmount
                        && userAccount.getNonPromotionalBalance() >= totalNonPromotionalAmountToBePaid) {
                    firstInstalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.PAID);
                    firstInstalment.setPaidTime(System.currentTimeMillis());
                    firstInstalment.setTotalNonPromotionalAmount(totalNonPromotionalAmountToBePaid);
                    firstInstalment.setTotalPromotionalAmount(instHrsPromotionalAmount);
                    firstInstalment.setTriggeredBy(order.getUserId().toString());
                    firstInstalment.setDeliverableEntityId(deliverableEntityId);
                    instalmentDAO.save(firstInstalment);

                    String uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.ACCOUNT_DEDUCTION.name() + "_" + firstInstalment.getId();

                    // hrs cost +security charge goes to default account
                    transaction = accountManager.transferAmount(userAccount, accountDAO.getVedantuDefaultAccount(),
                            (firstInstalment.getHoursCost() + firstInstalment.getSecurityCharge()),
                            instHrsPromotionalAmount,
                            (instHrsNonPromotionalAmount + firstInstalment.getSecurityCharge()),
                            ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                            TransactionRefType.INSTALMENT, firstInstalment.getId(),
                            Transaction._getTriggredByUser(order.getUserId()), false, uniqueId);

                    // convenience charge to revenue wallet,going with the same
                    // ref and ref nos
                    if (firstInstalment.getConvenienceCharge() != null && firstInstalment.getConvenienceCharge() > 0) {
                        accountManager.transferAmount(userAccount, accountDAO.getVedantuCutAccount(),
                                firstInstalment.getConvenienceCharge(), 0, firstInstalment.getConvenienceCharge(),
                                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                                TransactionRefType.INSTALMENT, firstInstalment.getId(),
                                Transaction._getTriggredByUser(order.getUserId()), false, uniqueId);
                    }
                    vedantuDiscountToTransfer = firstInstalment.getVedantuDiscountAmount();

                    order.updateOrderPaidDetails(order, firstInstalment);
                    // check and update the order pojo, so that we can save it
                    // after this if-else block
                    instalmentManager.updateOrderPojo(order, instalments);
                } else {
                    // this is not an error but occurs in rare cases, so
                    // capturing to understand the circumstances
                    logger.error("ACCOUNT_INSUFFICIENT_BALANCE in process order " + order);
                    throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
                }
            } else {
                logger.error("Payment type instalment but no instalments found " + order);
                throw new ConflictException(ErrorCode.INSTALMENTS_INCONSISTENT, "Insufficient account balance");
            }
        } else {
            if (vedantuDiscountToTransfer != null && vedantuDiscountToTransfer > 0l) {
                item.setVedantuDiscountAmountClaimed(vedantuDiscountToTransfer);
            }
        }

        // creating transaction for vedantu discount amount from Vm to Vd
        if (vedantuDiscountToTransfer != null && vedantuDiscountToTransfer > 0l) {
            accountManager.transferAmount(accountDAO.getVedantuFreebiesAccount(), accountDAO.getVedantuDefaultAccount(),
                    vedantuDiscountToTransfer, vedantuDiscountToTransfer, 0,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.DISCOUNT,
                    TransactionRefType.ITEM_ORDERED, order.getId(), Transaction._getTriggredByUser(order.getUserId()),
                    true, null);
        }

        if (transaction != null) {
            order.setTransactionId(transaction.getId());
        }
        addReferenceTag(order);

        boolean updateInstalments = false;
        if (StringUtils.isNotEmpty(deliverableEntityId)) {
            PurchasingEntity purchasingEntity = null;
            List<PurchasingEntity> purchasingEntites = order.getPurchasingEntities();
            if (ArrayUtils.isNotEmpty(purchasingEntites)) {

                if (purchasingEntites.size() > 1) {
                    logger.info("Multiple purchasing entities for : " + order.getId() + " .Not updating from here.");
                } else {
                    purchasingEntity = purchasingEntites.get(0);
                }
            } else {
                purchasingEntites = new ArrayList<>();
                purchasingEntity = new PurchasingEntity(getEntityTypeForPurchasingEntity(item.getEntityType()), item.getEntityId());
                purchasingEntites.add(purchasingEntity);
            }
            if (purchasingEntity != null) {
                purchasingEntites.get(0).setDeliverableId(deliverableEntityId);
                order.setPurchasingEntities(purchasingEntites);
                //TODO remove this hard coding later, no time so to avoid complications, setting updateInstalments only in case of bundle.java
                if (item.getEntityType().equals(EntityType.BUNDLE)) {
                    updateInstalments = true;
                }
            }
        }

        // finally upsert the order
        logger.info("saving the order");
        ordersDAO.create(order);
        triggerOrderForLeadSquare(order, true);

        // block calendar for instalments for PLAN(except first instalment as
        // sessions are already scheduled for them)
        if (instalments != null && item.getEntityType().equals(EntityType.PLAN)) {
            instalmentAsyncTasksManager.blockInstalmentSessionSlots(instalments, item.getTeacherId(), order.getUserId(),
                    deliverableEntityId);
        }

        // update coupon usage
        if (StringUtils.isNotEmpty(item.getTeacherDiscountCouponId())) {
            if (PurchaseFlowType.SUBSCRIPTION_REQUEST.equals(order.getPurchaseFlowType())) {
                // TODO handle the exception gracefully
                // old state is UNUSED because first the unlock balance happens
                // and then usage happens
                couponManager.updateRedeemEntry(item.getTeacherDiscountCouponId(), order.getPurchaseFlowId(),
                        item.getTeacherDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.UNUSED, RedeemedCouponState.PROCESSED);
            } else {
                couponManager.addRedeemEntry(item.getTeacherDiscountCouponId(), order.getUserId(),
                        item.getTeacherDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.PROCESSED);
            }
        }
        if (StringUtils.isNotEmpty(item.getVedantuDiscountCouponId())) {
            if (PurchaseFlowType.SUBSCRIPTION_REQUEST.equals(order.getPurchaseFlowType())) {
                // TODO handle the exception gracefully
                couponManager.updateRedeemEntry(item.getVedantuDiscountCouponId(), order.getPurchaseFlowId(),
                        item.getTeacherDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.UNUSED, RedeemedCouponState.PROCESSED);
            } else {
                couponManager.addRedeemEntry(item.getVedantuDiscountCouponId(), order.getUserId(),
                        item.getVedantuDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.PROCESSED);
            }
        }

        // update subscription request state
        if (PurchaseFlowType.SUBSCRIPTION_REQUEST.equals(order.getPurchaseFlowType())) {
            try {
                updateSubscriptionRequestSubstate(Long.parseLong(order.getPurchaseFlowId()),
                        SubscriptionRequestSubState.AMOUNT_DEDUCTED);
            } catch (VException e) {
                logger.error("Failed to update SubscriptionRequest id, substate: " + order.getPurchaseFlowId() + ", "
                        + SubscriptionRequestSubState.AMOUNT_DEDUCTED);
            }
        }

        if (updateInstalments && PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            try {
                instalmentAsyncTasksManager.updatePurchasingEntitiesInInstalment(order.getId(), order.getPurchasingEntities());
            } catch (Exception e) {
                logger.info("Error in updating enrollments" + e);
            }

        }
        triggerOrderUpdatedForUser(order);
        // TODO quantify the response times for each steps
        // TODO see if we can move some of these to asyn operations if the need
        // be
    }

    private Discounts getCouponDiscount(String entityId, EntityType entityType, String teacherCouponId,
            String vedantuCouponId, List<String> usedCoupons, Long requestUserId, Integer totalAmount,
            String purchaseFlowId,String subscriptionPlanId) throws VException {
        Integer teacherDiscount = 0;
        Integer vedantuDiscount = 0;
        if (StringUtils.isNotEmpty(teacherCouponId) && usedCoupons.indexOf(teacherCouponId) == -1) {
            teacherDiscount = couponManager.validateAndCalculateDiscount(teacherCouponId, requestUserId, entityId,
                    entityType, CouponType.TEACHER_DISCOUNT, totalAmount, purchaseFlowId, PaymentType.BULK,subscriptionPlanId);
            usedCoupons.add(teacherCouponId);
        }

        if (StringUtils.isNotEmpty(vedantuCouponId) && usedCoupons.indexOf(vedantuCouponId) == -1) {
            vedantuDiscount = couponManager.validateAndCalculateDiscount(vedantuCouponId, requestUserId, entityId,
                    entityType, CouponType.VEDANTU_DISCOUNT, totalAmount, purchaseFlowId, PaymentType.BULK,subscriptionPlanId);
            usedCoupons.add(vedantuCouponId);
        }
        Discounts resp = new Discounts(teacherDiscount, vedantuDiscount);
        logger.info("EXIT " + resp);
        return resp;
    }

    public OrderInfo getOrderInfo(GetOrderInfoReq req)
            throws NotFoundException, NumberFormatException, InternalServerErrorException {

        logger.info("Entering " + req.toString());
        Orders order = ordersDAO.getById(req.getOrderId());
        if (order == null) {
            // logger.error("no order found with id[" + req.getOrderId() + "]");
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "no order found with id[" + req.getOrderId() + "]");
        }
        OrderInfo orderInfo = getOrderItemInfo(order);
        logger.info("Exiting " + orderInfo.toString());
        return orderInfo;
    }

    public GetMyOrdersRes getMyOrders(GetMyOrdersReq req)
            throws NumberFormatException, NotFoundException, InternalServerErrorException {
        logger.info("Entering " + req);
        GetMyOrdersRes res = new GetMyOrdersRes();
        List<Orders> orders = ordersDAO.getOrders(req.getCallingUserId(), req.getStart(), req.getSize());
        for (Orders order : orders) {
            OrderInfo orderInfo = getOrderItemInfo(order);
            if (CollectionUtils.isNotEmpty(orderInfo.getItems())) {
                res.addItem(orderInfo);
            }
        }
        logger.info("Exiting" + res);
        return res;
    }

    private OrderInfo getOrderItemInfo(Orders order)
            throws NumberFormatException, NotFoundException, InternalServerErrorException {
        logger.info("Entering " + order.toString());
        OrderInfo orderInfo = new OrderInfo(order.getId(), order.getUserId(), order.getAmount(), order.getAmountPaid(),
                order.getPaymentStatus(), order.getPaymentType(), order.getCreationTime(), order.getContextId(),
                order.getSecurity(), order.getConvenienceCharge());
        List<OrderItemInfo> items = orderInfo.getItems();
        if (order.getPurchasingEntities() != null) {
            orderInfo.setPurchasingEntities(order.getPurchasingEntities());
        }

        Set<Long> userIds = new HashSet<>();
        List<Long> boardIds = new ArrayList<>();
        for (OrderedItem _oItem : order.getItems()) {
            if (_oItem.getTeacherId() != null && _oItem.getTeacherId() > 0) {
                userIds.add(_oItem.getTeacherId());
            }
            if (_oItem.getBoardId() != null) {
                boardIds.add(_oItem.getBoardId());
            }
        }

        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);

        for (OrderedItem oItem : order.getItems()) {
            OrderItemInfo itemInfo = new OrderItemInfo(oItem);
            SubscriptionBasicInfo subscriptionBasicInfo = new SubscriptionBasicInfo();
            try {
                subscriptionBasicInfo.setHours(oItem.getHours());
                if (userMap != null && userMap.containsKey(oItem.getTeacherId())) {
                    subscriptionBasicInfo.setTeacher(userMap.get(oItem.getTeacherId()));
                }
                if (boardMap != null && boardMap.containsKey(oItem.getBoardId())) {
                    Board board = boardMap.get(oItem.getBoardId());
                    String subscriptionName = oItem.getGrade() + oItem.getTarget() + board.getName();
                    subscriptionBasicInfo.setSubscriptionName(subscriptionName);
                }
                subscriptionBasicInfo.setModel(oItem.getModel());
                itemInfo.setExtraInfo(subscriptionBasicInfo);
            } catch (Throwable t) {
                logger.error(t);
                continue;
            }
            items.add(itemInfo);
        }
        orderInfo.setItems(items);
        logger.info("Exiting " + orderInfo.toString());
        return orderInfo;
    }

    private OrderInfo getOrderItemPlanInfo(Orders order) throws NumberFormatException, NotFoundException {
        logger.info("Entering " + order.toString());
        OrderInfo orderInfo = new OrderInfo(order.getId(), order.getUserId(), order.getAmount(), order.getAmountPaid(),
                order.getPaymentStatus(), order.getPaymentType(), order.getCreationTime(), order.getContextId(),
                order.getSecurity(), order.getConvenienceCharge());
        List<OrderItemInfo> items = orderInfo.getItems();
        for (OrderedItem oItem : order.getItems()) {
            OrderItemInfo itemInfo = new OrderItemInfo(oItem);
            items.add(itemInfo);
        }
        orderInfo.setItems(items);
        orderInfo.setPurchasingEntities(order.getPurchasingEntities());
        // added in case of upsell only
        if (order.getPurchaseContextId() != null) {
            orderInfo.setPurchaseContextId(order.getPurchaseContextId());
        }

        logger.info("Exiting " + orderInfo.toString());
        return orderInfo;
    }

    public PaymentGateway addPaymentGateways(AddPaymentGatewayReq addPaymentGatewayReq) {
        logger.info("Entering " + addPaymentGatewayReq.toString());
        List<PaymentGateway> paymentGateways = paymentGatewayDAO.getPaymentGateways(null, null, null, null,
                PaymentGatewayDAO.NO_LIMIT);
        // int totalPercentAssigned =0;
        // for(PaymentGateway paymentGateway : paymentGateways){
        // totalPercentAssigned+=paymentGateway.getPercentageTraffic();
        // }
        // totalPercentAssigned += addPaymentGatewayReq.getPercentageTraffic();
        PaymentGateway resPojo = addPaymentGatewayReq.toPaymentGateway();
        paymentGatewayDAO.create(resPojo);

        // Map<PaymentGatewayName, Integer> paymentGatewaysDistribution =
        // (Map<PaymentGatewayName, Integer>)
        // VCacheManager.INSTANCE.get("paymentGatewaysDistribution");
        //
        // if(paymentGatewaysDistribution==null ||
        // paymentGatewaysDistribution.size()==0){
        // paymentGatewaysDistribution = new HashMap<PaymentGatewayName,
        // Integer>();
        // }
        // paymentGatewaysDistribution.put(PaymentGatewayName
        // .getPaymentGatewayName(addPaymentGatewayReq.getName()), 0);
        // VCacheManager.INSTANCE.put("paymentGatewaysDistribution",
        // paymentGatewaysDistribution);
        // mgr.close();
        logger.info("Exiting " + resPojo.toString());
        return resPojo;
    }

    public List<PaymentGateway> getPaymentGateways() {
        logger.info("Entering ");

        List<PaymentGateway> paymentGateways = paymentGatewayDAO.getPaymentGateways(null, null, null, null,
                PaymentGatewayDAO.NO_LIMIT);

        logger.info("Exiting " + paymentGateways.toString());

        return paymentGateways;
    }

    public PaymentGateway modifyPaymentGateway(ModifyPaymentGatewayReq modifyPaymentGatewayReq) {
        logger.info("Entering " + modifyPaymentGatewayReq.toString());

        PaymentGateway resPojo = modifyPaymentGatewayReq.toPaymentGateway();
        paymentGatewayDAO.create(resPojo);

        logger.info("Exiting " + resPojo.toString());

        return resPojo;
    }

    public Boolean deletePaymentGateway(String paymentGatewayId) throws BadRequestException {

        logger.info("Entering " + paymentGatewayId);

        if (paymentGatewayId != null) {
            PaymentGateway paymentGateway = paymentGatewayDAO.getById(paymentGatewayId);
            int deleted = paymentGatewayDAO.deleteById(paymentGateway.getId());
            if (deleted == 1) {

                logger.info("Exiting " + Boolean.TRUE.toString());
                return Boolean.TRUE;
            } else {
                logger.error("paymentGatewayId not present");
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "paymentGatewayId not present");
            }
        }
        return Boolean.FALSE;
    }

    public List<Orders> getOrdersBySubscriptionId(Long subscriptionId) {

        logger.info("Entering id:" + subscriptionId);
        Query query = new Query();
        query.addCriteria(Criteria.where("contextId").is(subscriptionId));

        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);

        logger.info("Exiting " + orders);
        return orders;
    }

    public Map<Long, List<Orders>> getOrdersBySubscriptionIds(List<Long> subscriptionIds) {
        logger.info("Entering ids:" + subscriptionIds.toString());

        Query query;
        Map<Long, List<Orders>> response = new HashMap<Long, List<Orders>>();
        for (Long subscriptionId : subscriptionIds) {
            query = new Query();
            query.addCriteria(Criteria.where("contextId").is(subscriptionId));
            List<Orders> orders = ordersDAO.runQuery(query, Orders.class);
            response.put(subscriptionId, orders);
        }

        logger.info("Exiting " + response.toString());
        return response;
    }

    public RefundRes findPromotionalValueForCancelSubscription(Long subscriptionId, Integer refundAmount,
            Float remainingPercentage) throws InternalServerErrorException {
        logger.info("Entering id:" + subscriptionId + " refundAmount:" + refundAmount + ", remainingPercentage "
                + remainingPercentage);

        Query query = new Query();
        int totalPromotionalCost = 0;
        int totalNonPromotionalCost = 0;
        int totalSecurityAmount = 0;
        int totalConvenienceCharge = 0;
        int totalVedantuDiscountGiven = 0;
        int totalVedantuDiscountToNotRefund = 0;
        query.addCriteria(Criteria.where("contextId").is(subscriptionId));

        // payment details
        List<PaymentType> paymentTypes = new ArrayList<>();
        List<String> deliverableEntityIds = new ArrayList<>();
        List<String> orderIds = new ArrayList<>();

        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);
        for (Orders order : orders) {
            paymentTypes.add(order.getPaymentType());
            orderIds.add(order.getId());
            if (order.getSecurity() != null) {
                // refundAmount in the request only contains hrs amount to
                // refund
                totalSecurityAmount += order.getSecurity();
            }
            if (order.getConvenienceCharge() != null) {
                // refundAmount in the request only contains hrs amount to
                // refund
                totalConvenienceCharge += order.getConvenienceCharge();
            }
            if (ArrayUtils.isNotEmpty(order.getItems())) {
                // TODO what if there are multiple subscriptiondetails
                OrderedItem item = order.getItems().get(0);
                deliverableEntityIds.add(item.getDeliverableEntityId());
                if (item.getDeliverableEntityType() != null
                        && item.getDeliverableEntityType().equals(DeliverableEntityType.SUBSCRIPTION_DETAILS)) {
                    if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
                        totalPromotionalCost += order.getPromotionalAmountPaid();
                        totalNonPromotionalCost += order.getNonPromotionalAmountPaid();
                    } else {
                        totalPromotionalCost += item.getPromotionalCost();
                        totalNonPromotionalCost += item.getNonPromotionalCost();
                    }
                }
                if (item.getVedantuDiscountAmount() != null && remainingPercentage != null) {
                    float instalmentPercentagePaid = 1;
                    if (PaymentType.INSTALMENT.equals(order.getPaymentType()) && order.getAmount() != null
                            && order.getAmountPaid() != null) {
                        // basic amount to pay - security and convenience
                        int basicAmountToPay = order.getAmount() - totalSecurityAmount - totalConvenienceCharge;
                        // basic amount paid - security and convenience
                        int basicAmountToPaid = order.getAmountPaid() - totalSecurityAmount - totalConvenienceCharge;
                        instalmentPercentagePaid = (float) basicAmountToPaid / basicAmountToPay;
                        logger.info("instalmentPercentagePaid " + instalmentPercentagePaid);
                    }
                    // discount applicable according to instalmentPercentagePaid
                    Integer discount = (int) (item.getVedantuDiscountAmount() * instalmentPercentagePaid);
                    logger.info("Discount applicable as per instalmentPercentagePaid " + discount);
                    totalVedantuDiscountGiven += discount;

                    // TODO remaining percentage won't make sense when multiple
                    // orders are under
                    // the same subscription with different discounts for each
                    discount = (int) (remainingPercentage * discount);
                    logger.info("discountUsedTillNow : " + discount + " OrderedItem: " + item.getDeliverableEntityId());
                    totalVedantuDiscountToNotRefund += discount;
                }
            }
        }

        // deducting the convenience charge from the np as it is not refundable
        totalNonPromotionalCost -= totalConvenienceCharge;

        // calculating remaining amount from the session payouts of the older
        // sessions
        SessionFactory sessionFactory = null;
        Session session = null;
        List<SessionPayout> sessionPayouts = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            org.hibernate.Criteria cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.eq("subscriptionId", subscriptionId));
            sessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);
        } catch (Exception e) {
            logger.error(e);
            if (session != null) {
                session.close();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        // TODO, this will break if a session's payout(session which was taken
        // before this session) is not calculated
        int totalPromotionalAmountUsedTillNow = 0;
        int totalNonPromotionalAmountUsedTillNow = 0;
        if (ArrayUtils.isNotEmpty(sessionPayouts)) {
            for (SessionPayout payout : sessionPayouts) {
                if (payout != null) {
                    totalPromotionalAmountUsedTillNow += (payout.getTeacherPromotionalPayout()
                            + payout.getPromotionalCut());
                    totalNonPromotionalAmountUsedTillNow += (payout.getTeacherNonPromotionalPayout()
                            + payout.getNonPromotionalCut());
                }
            }
        }

        int totalVedantuDiscountUsedTillNow = totalVedantuDiscountGiven - totalVedantuDiscountToNotRefund;
        int totalPromotionalAmountUsedFromStudentPaid = totalPromotionalAmountUsedTillNow
                - totalVedantuDiscountUsedTillNow;

        logger.info("totalPromotionalAmountUsedTillNow " + totalPromotionalAmountUsedTillNow);
        logger.info("totalNonPromotionalAmountUsedTillNow " + totalNonPromotionalAmountUsedTillNow);
        logger.info("totalVedantuDiscountUsedTillNow " + totalVedantuDiscountUsedTillNow);
        logger.info("totalPromotionalAmountUsedFromStudentPaid " + totalPromotionalAmountUsedFromStudentPaid);
        logger.info("totalVedantuDiscountToNotRefund " + totalVedantuDiscountToNotRefund);

        int promotionalRefund = totalPromotionalCost - totalPromotionalAmountUsedFromStudentPaid;
        int nonPromotionalRefund = totalNonPromotionalCost - totalNonPromotionalAmountUsedTillNow;

        int finaRefundAmt = promotionalRefund + nonPromotionalRefund;

        RefundRes refundRes = new RefundRes(finaRefundAmt, promotionalRefund, nonPromotionalRefund, totalSecurityAmount,
                totalVedantuDiscountToNotRefund, paymentTypes, deliverableEntityIds, orderIds);

        logger.info("Exiting " + refundRes);
        return refundRes;

    }

    public Pair<Integer, Integer> calculateBillingSplit(Long subscriptionId, Integer hourlyRate, Long totalHours,
            Long consumedHours, Integer billingAmount, Long sessionId) throws InternalServerErrorException {
        logger.info("Entering id:" + subscriptionId + " totalHours:" + totalHours + " consumedHours:" + consumedHours
                + " hourlyRate:" + hourlyRate);
        Query query = new Query();
        int totalAmount = 0;
        int totalPromotionalCost = 0;
        int totalNonPromotionalCost = 0;
        int totalSecurityAmount = 0;
        int totalConvenienceCharge = 0;
        int vedantuDiscount = 0;
        int totalVedantuDiscount = 0;
        query.addCriteria(Criteria.where("contextId").is(subscriptionId));

        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);
        for (Orders order : orders) {
            if (order.getAmount() != null) {
                totalAmount += order.getAmount();
            }
            if (order.getSecurity() != null) {
                // refundAmount in the request only contains hrs amount to
                // refund
                totalSecurityAmount += order.getSecurity();
            }
            if (order.getConvenienceCharge() != null) {
                // refundAmount in the request only contains hrs amount to
                // refund
                totalConvenienceCharge += order.getConvenienceCharge();
            }
            for (OrderedItem item : order.getItems()) {
                if (item.getDeliverableEntityType() != null
                        && item.getDeliverableEntityType().equals(DeliverableEntityType.SUBSCRIPTION_DETAILS)) {
                    if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
                        totalPromotionalCost += order.getPromotionalAmountPaid();
                        totalNonPromotionalCost += order.getNonPromotionalAmountPaid();
                    } else {
                        totalPromotionalCost += item.getPromotionalCost();
                        totalNonPromotionalCost += item.getNonPromotionalCost();
                    }
                }
                if (item.getVedantuDiscountAmount() != null) {
                    float instalmentPercentagePaid = 1;
                    if (PaymentType.INSTALMENT.equals(order.getPaymentType()) && order.getAmount() != null
                            && order.getAmountPaid() != null) {
                        // basic amount to pay - security and convenience
                        int basicAmountToPay = order.getAmount() - totalSecurityAmount - totalConvenienceCharge;
                        // basic amount paid - security and convenience
                        int basicAmountPaid = order.getAmountPaid() - totalSecurityAmount - totalConvenienceCharge;
                        instalmentPercentagePaid = (float) basicAmountPaid / basicAmountToPay;
                        logger.info("instalmentPercentagePaid " + instalmentPercentagePaid);
                    }
                    // discount applicable according to instalmentPercentagePaid
                    vedantuDiscount += (item.getVedantuDiscountAmount() * instalmentPercentagePaid);
                    totalVedantuDiscount += item.getVedantuDiscountAmount();
                }
            }
        }

        // remove the security charge and convenience charge from np
        totalNonPromotionalCost -= totalSecurityAmount;
        totalNonPromotionalCost -= totalConvenienceCharge;

        int basicAmountToPay = totalAmount - totalSecurityAmount - totalConvenienceCharge;

        logger.info("total vedantu discount" + totalVedantuDiscount);
        logger.info("vedantu discount applicable" + vedantuDiscount);
        logger.info("totalNonPromotionalCost paid till now " + totalNonPromotionalCost);
        logger.info("totalPromotionalCost paid till now " + totalPromotionalCost);
        logger.info("totalAmount to pay without security and convenience charges " + basicAmountToPay);

        float vDiscountContributionToTotalCost = (float) totalVedantuDiscount
                / (basicAmountToPay + totalVedantuDiscount);
        logger.info("Discount percentage " + vDiscountContributionToTotalCost);

        // calculating remaining amoutn from the session payouts of the older
        // sessions
        SessionFactory sessionFactory = null;
        Session session = null;
        List<SessionPayout> sessionPayouts = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            org.hibernate.Criteria cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.eq("subscriptionId", subscriptionId));
            sessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);
        } catch (Exception e) {
            logger.error(e);
            if (session != null) {
                session.close();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        } finally {
            if (session != null) {
                session.close();
            }
        }

        // TODO, this will break if a session's payout(session which was taken
        // before this session) is not calculated
        int totalPromotionalAmountUsedTillNow = 0;
        int totalPromotionalAmountUsedFromStudentAmt = 0;
        int totalNonPromotionalAmountUsedTillNow = 0;
        if (ArrayUtils.isNotEmpty(sessionPayouts)) {
            for (SessionPayout payout : sessionPayouts) {
                if (payout != null && sessionId != null && !Objects.equals(payout.getSessionId(), sessionId)) {
                    totalPromotionalAmountUsedTillNow += (payout.getTeacherPromotionalPayout()
                            + payout.getPromotionalCut());
                    totalNonPromotionalAmountUsedTillNow += (payout.getTeacherNonPromotionalPayout()
                            + payout.getNonPromotionalCut());
                }
            }
        }

        logger.info("totalPromotionalAmountUsedTillNow " + totalPromotionalAmountUsedTillNow);
        logger.info("totalNonPromotionalAmountUsedTillNow " + totalNonPromotionalAmountUsedTillNow);

        int netPromotionalBillAmount = 0;
        int netNonPromotionalBillAmount = 0;
        Integer remainingPromotionalCostFromStudentPaidAmt = 0;
        Integer remainingNonPromotionalCostFromtStudentAmt = 0;
        Integer remainingDiscountAmount = 0;

        logger.info("calculating the billing splits from the student paid amount");
        // hourly rate wrt to amount paid by student
        // TODO remove 1- eg do (1-0.8)*45000 will be 8999 instead of 90000
        Integer hourlyRatewrtAmountPaid = (int) ((1 - vDiscountContributionToTotalCost) * hourlyRate);

        logger.info("hourly rate to be used from student amount paid " + hourlyRatewrtAmountPaid);

        int amountUsed = (int) ((consumedHours * hourlyRatewrtAmountPaid) / DateTimeUtils.MILLIS_PER_HOUR);
        logger.info("amountUsed from student amount paid: " + amountUsed);

        remainingNonPromotionalCostFromtStudentAmt = totalNonPromotionalCost - totalNonPromotionalAmountUsedTillNow;
        totalPromotionalAmountUsedFromStudentAmt = amountUsed - totalNonPromotionalAmountUsedTillNow;
        remainingPromotionalCostFromStudentPaidAmt = totalPromotionalCost - totalPromotionalAmountUsedFromStudentAmt;

        logger.info("Student: remainingPromotionalCost " + remainingPromotionalCostFromStudentPaidAmt
                + ", remainingNonPromotionalCost " + remainingNonPromotionalCostFromtStudentAmt
                + ", totalPromotionalAmountUsedFromStudentAmt " + totalPromotionalAmountUsedFromStudentAmt);

        if (remainingNonPromotionalCostFromtStudentAmt < 0 || remainingPromotionalCostFromStudentPaidAmt < 0) {
            logger.info("Remianing amount to deduct from is " + "zero for calculate Billing splits for sessionId "
                    + sessionId);
        }

        // TODO remove 1-
        int netBillingToDeductFromStudentPaidAmt = (int) ((1 - vDiscountContributionToTotalCost) * billingAmount);
        if (netBillingToDeductFromStudentPaidAmt < remainingPromotionalCostFromStudentPaidAmt) {
            netPromotionalBillAmount = netBillingToDeductFromStudentPaidAmt;
        } else {
            netPromotionalBillAmount = remainingPromotionalCostFromStudentPaidAmt;
            netNonPromotionalBillAmount = netBillingToDeductFromStudentPaidAmt - netPromotionalBillAmount;
        }

        logger.info("calculating the billing splits vedantu discount amount");
        // hourlyrate wrt to vedantu discount
        Integer hourlyRatewrtVDiscount = hourlyRate - hourlyRatewrtAmountPaid;
        logger.info("hourly rate to be used from vedantu discount " + hourlyRatewrtVDiscount);

        int discountAmountUsed = (int) ((consumedHours * hourlyRatewrtVDiscount) / DateTimeUtils.MILLIS_PER_HOUR);
        logger.info("amountUsed from vedantu discount : " + discountAmountUsed);

        remainingDiscountAmount = vedantuDiscount - discountAmountUsed;
        logger.info("remaining vedantu discount amount to use " + remainingDiscountAmount);

        int netBillingToDeductFromVedantuDiscount = billingAmount - netBillingToDeductFromStudentPaidAmt;
        logger.info("netBillingToDeductFromVedantuDiscount " + netBillingToDeductFromVedantuDiscount);

        netPromotionalBillAmount += netBillingToDeductFromVedantuDiscount;

        Pair<Integer, Integer> response = Pair.of(netPromotionalBillAmount, netNonPromotionalBillAmount);

        logger.info("Exiting " + response.toString());
        return response;

    }

    public List<SubscriptionMetric> getSubscriptionMetrices() {

        logger.info("Entering");
        Long currentTime = System.currentTimeMillis();
        Long diff = ConfigUtils.INSTANCE.getLongValue("subscription.metrices.time");
        Long fromTime = currentTime - diff;
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

        Query query = new Query();
        query.addCriteria(Criteria.where("creationTime").gte(fromTime));
        // query.addCriteria(Criteria.where("paymentStatus")
        // .is(PaymentStatus.REVERTED).orOperator(Criteria.where("paymentStatus").is(PaymentStatus.PAID)));

        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);
        List<SubscriptionMetric> results = new ArrayList<SubscriptionMetric>();
        for (Orders order : orders) {
            for (OrderedItem item : order.getItems()) {
                if (item.getEntityType() != null && (item.getEntityType().equals(EntityType.PLAN)
                        || item.getEntityType().equals(EntityType.SUBSCRIPTION_REQUEST))) {
                    String status = "";

                    if (order.getPaymentStatus() != null) {
                        switch (order.getPaymentStatus()) {
                            case PAID:
                                status = "SUCCESS";
                                break;
                            case REVERTED:
                                status = "FAILED";
                                break;
                            default:
                                continue;
                        }
                    }

                    String date = sdf.format(new Date(order.getCreationTime()));
                    // String title = item.getGrade() + "th " +
                    // item.getTarget();
                    Integer t = item.getHours().intValue();
                    String hours = (t / DateTimeUtils.MILLIS_PER_HOUR) + "hrs:"
                            + ((t % DateTimeUtils.MILLIS_PER_HOUR) / DateTimeUtils.MILLIS_PER_MINUTE) + "mins";
                    Double amount = (double) (item.getCost() / 100);
                    SubscriptionMetric s = new SubscriptionMetric(status, date, order.getContextId(),
                            item.getModel().name(), hours, amount, order.getUserId(), item.getTeacherId());
                    results.add(s);
                }
            }
        }

        logger.info("Exiting " + results.toString());
        return results;
    }

    public List<Transaction> getTransactions(GetTransactionsReq req) throws InternalServerErrorException {

        logger.info("Entering " + req.toString());

        Query query = new Query();
        if (req.getMinAmountForFilter() != null && req.getMaxAmountForFilter() != null) {
            query.addCriteria(Criteria.where("amount").gte(req.getMinAmountForFilter()).lte(req.getMaxAmountForFilter()));
        } else if (req.getMinAmountForFilter() != null) {
            query.addCriteria(Criteria.where("amount").gte(req.getMinAmountForFilter()));
        } else if (req.getMaxAmountForFilter() != null) {
            query.addCriteria(Criteria.where("amount").lte(req.getMaxAmountForFilter()));
        }
        if (req.getUserId() != null && req.getUserId() != 0) {
            Account account = accountDAO
                    .getAccountByHolderId(Account.__getUserAccountHolderId(req.getUserId().toString()));
            Criteria userCriteria = new Criteria();
            Criteria creditCriteria = Criteria.where("creditToAccount").is(account.getId().toString());
            Criteria debitCriteria = Criteria.where("debitFromAccount").is(account.getId().toString());
            userCriteria.orOperator(creditCriteria, debitCriteria);
            query.addCriteria(userCriteria);
        }

        if (req.getReason() != null) {
            query.addCriteria(Criteria.where("reasonType").is(req.getReason()));
        }

        if (req.getFromTime() != null) {
            query.addCriteria(Criteria.where("creationTime").gte(req.getFromTime()));
        }

        if (req.getTillTime() != null) {
            query.addCriteria(Criteria.where("creationTime").lte(req.getTillTime()));
        }

        int start = req.getStart();
        query.skip(start);

        int limit = req.getLimit();
        if (limit != 0 && (limit >= start)) {
            query.limit((int) (limit - start));
        }
        query.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        logger.info("Exiting");
        return transactionDAO.runQuery(query, Transaction.class);
    }

    public List<ExtTransaction> pendingExtTransactions() {

        logger.info("Entering");
        Calendar cal = Calendar.getInstance();
        Long currentTime = cal.getTimeInMillis() - DateTimeUtils.MILLIS_PER_HOUR;
        cal.add(Calendar.DATE, -1);
        Long yesterdayTime = cal.getTimeInMillis() - DateTimeUtils.MILLIS_PER_HOUR;

        Query query;

        query = new Query();
        query.addCriteria(Criteria.where("status").is(TransactionStatus.PENDING));
        query.addCriteria(Criteria.where("creationTime").gte(yesterdayTime).lt(currentTime));

        query.with(Sort.by(Sort.Direction.DESC, "creationTime"));
        List<ExtTransaction> response = extTransactionDAO.runQuery(query, ExtTransaction.class);

        logger.info("Exiting size:" + response.size());

        return response;
    }

    public List<OrderInfo> getOrdersByOrderIds(List<String> orderIds) throws NumberFormatException, NotFoundException {
        List<OrderInfo> response = new ArrayList<>();
        ;
        if (ArrayUtils.isEmpty(orderIds)) {
            return response;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.ID).in(orderIds));
        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);
        if (ArrayUtils.isNotEmpty(orders)) {
            for (Orders order : orders) {
                OrderInfo orderInfo = getOrderItemPlanInfo(order);
                response.add(orderInfo);
            }
        }
        return response;
    }

    ;

    public List<Orders> getOrdersByEntityIds(List<String> entityIds, Long userId) throws NumberFormatException, NotFoundException {
        List<Orders> response = new ArrayList<>();
        ;
        if (ArrayUtils.isEmpty(entityIds)) {
            return response;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).in(entityIds));
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(userId));
        return ordersDAO.runQuery(query, Orders.class);
    }

    public List<Orders> getOrders(GetOrdersReq req) throws InternalServerErrorException {

        logger.info("Entering " + req.toString());

        Query query = new Query();
        if (req.getMinAmounForFilter() != null && req.getMaxAmounForFilter() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AMOUNT).gte(req.getMinAmounForFilter()).lte(req.getMaxAmounForFilter()));
        } else if (req.getMinAmounForFilter() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AMOUNT).gte(req.getMinAmounForFilter()));
        } else if (req.getMaxAmounForFilter() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AMOUNT).lte(req.getMaxAmounForFilter()));
        }
        if (req.getUserId() != null && req.getUserId() != 0) {
            query.addCriteria(Criteria.where("userId").is(req.getUserId()));
        }

        if (req.getEntityType() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(req.getEntityType()));
        }

        if (req.getPaymentStatus() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(req.getPaymentStatus()));
        } else if (ArrayUtils.isNotEmpty(req.getPaymentStatuses())) {
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(req.getPaymentStatuses()));
        }

        if (req.getPaymentType() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_TYPE).is(req.getPaymentType()));
        }

        if (StringUtils.isNotEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(req.getEntityId()));
        }

        if (req.getPaymentThrough() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_THROUGH).is(req.getPaymentThrough()));
        }

        if (req.getTeamType() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.TEAM_TYPE).is(req.getTeamType()));
        }

        if (req.getAgentEmailId() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AGENT_EMAIL_ID).is(req.getAgentEmailId()));
        }

        if (req.getTeamLeadEmailId() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.TEAM_LEAD_EMAIL_ID).is(req.getTeamLeadEmailId()));
        }

        if (req.getCentre() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.CENTRE).is(req.getCentre()));
        }

        if (req.getEnrollmentType() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.ENROLLMENT_TYPE).is(req.getEnrollmentType()));
        }

        if (req.getFintechRisk() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.FINTECH_RISK).is(req.getFintechRisk()));
        }

        if (req.getRequestRefund() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.REQUEST_REFUND).is(req.getRequestRefund()));
        }

        if (req.getAgentCode() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.AGENT_CODE).is(req.getAgentCode()));
        }

        // Filter by the given time interval
        if (req.getStartTime() != null && req.getStartTime() > 0l) {
            if (req.getEndTime() != null && req.getEndTime() > 0l) {
                query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.LAST_UPDATED).gte(req.getStartTime())
                        .lt(req.getEndTime()));
            } else {
                query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.LAST_UPDATED).gte(req.getStartTime()));
            }
        } else if (req.getEndTime() != null && req.getEndTime() > 0l) {
            query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.LAST_UPDATED).lte(req.getEndTime()));
        }

        if (req.getStart() != null) {
            query.skip(req.getStart());
        } else {
            query.skip(0);
        }

        if (req.getSize() != null && req.getStart() != null) {
            int limit = req.getSize();
            // check if difference also needs to be here
            if (limit != 0 && (limit >= req.getStart())) {
                query.limit((int) (limit - req.getStart()));
            } else {
                query.limit(OrdersDAO.DEFAULT_FETCH_SIZE);
            }
        } else {
            query.limit(OrdersDAO.DEFAULT_FETCH_SIZE);
        }

        if (SortType.LAST_UPDATED_DESC.equals(req.getOrderSortType())) {
            query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.LAST_UPDATED));
        } else {
            query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.CREATION_TIME));
        }

        if (req.getCreationTimeUpperLimit() != null && req.getCreationTimeUpperLimit() > 0l
                && req.getCreationTimeLowerLimit() != null && req.getCreationTimeLowerLimit() > 0l) {
            query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).lte(req.getCreationTimeUpperLimit())
                    .gte(req.getCreationTimeLowerLimit()));
        }

        if (Boolean.TRUE.equals(req.getConditionalFiltering())) {
            logger.info("Conditional filtering of orders");
            if (req.getPaymentStatus() == null && ArrayUtils.isEmpty(req.getPaymentStatuses())) {
                query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).ne(PaymentStatus.NOT_PAID));
            }
            if (req.getEntityType() == null) {
                query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).nin(Arrays.asList(
                        EntityType.COURSE_PLAN_REGISTRATION, EntityType.OTF_BATCH_REGISTRATION,
                        EntityType.OTO_COURSE_REGISTRATION, EntityType.OTM_BUNDLE_REGISTRATION,
                        EntityType.OTF_COURSE_REGISTRATION, EntityType.PLAN, EntityType.BUNDLE_TRIAL
                )));
            }
        }
        logger.info("getOrders - query - {}",query);
        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);
        return orders;
    }

    public List<Orders> getNonRefundedOrdersWithoutDeliverableEntityId(String entityId, Long userId)
            throws InternalServerErrorException {

        Query query = new Query();
        if (userId != null) {
            query.addCriteria(Criteria.where("userId").is(userId));
        }

        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID).exists(false));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(PaymentStatus.PAID));

        if (entityId != null) {
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(entityId));
        }

        query.skip(0);
        query.limit(20);
        return ordersDAO.runQuery(query, Orders.class);
    }

    public void updateSubscriptionRequestSubstate(Long id, SubscriptionRequestSubState state) throws VException {
        String platformEndPoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
        String updateSubstateUrl = platformEndPoint + "subscriptionRequest/updateSubState";
        UpdateSubscriptionRequestSubState updateReq = new UpdateSubscriptionRequestSubState(id, state);
        ClientResponse resp = WebUtils.INSTANCE.doCall(updateSubstateUrl, HttpMethod.POST, new Gson().toJson(updateReq),
                true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public void cancelOrder(String id, EntityType type) {

    }

    public BasicResponse updateDeliverableEntityIdInOrder(
            UpdateDeliverableEntityIdInOrderReq updateDeliverableEntityIdInOrderReq) throws NotFoundException {
        Orders order = ordersDAO.getById(updateDeliverableEntityIdInOrderReq.getOrderId());
        if (order == null || ArrayUtils.isEmpty(order.getItems())) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "no order found or items zero with orderid["
                    + updateDeliverableEntityIdInOrderReq.getOrderId() + "]");
        }
        OrderedItem item = order.getItems().get(0);
        item.setDeliverableEntityType(updateDeliverableEntityIdInOrderReq.getDeliverableEntityType());
        item.setDeliverableEntityId(updateDeliverableEntityIdInOrderReq.getDeliverableEntityId());

        if (ArrayUtils.isNotEmpty(order.getPurchasingEntities()) && order.getPurchasingEntities().size() == 1) {
            order.getPurchasingEntities().get(0).setDeliverableId(updateDeliverableEntityIdInOrderReq.getDeliverableEntityId());
        } else {
            logger.info("Purchasing entiyIds don't exist.");
        }
        if (StringUtils.isNotEmpty(updateDeliverableEntityIdInOrderReq.getCourseId())) {

            List<ReferenceTag> referenceTags = order.getReferenceTags();
            ReferenceTag referenceTag = new ReferenceTag();
            if (referenceTags == null) {
                referenceTags = new ArrayList<>();
                order.setReferenceTags(referenceTags);
            }
            referenceTag.setReferenceType(ReferenceType.OTF_COURSE_ID);
            referenceTag.setReferenceId(updateDeliverableEntityIdInOrderReq.getCourseId());
            referenceTags.add(referenceTag);
        }

        ordersDAO.create(order);

        if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            try {
                instalmentAsyncTasksManager.updatePurchasingEntitiesInInstalment(order.getId(), order.getPurchasingEntities());
            } catch (Exception e) {
                logger.info("Error in updating enrollments" + e);
            }
        }
        return new BasicResponse();
    }

    public OrderDetails getOrderBasicInfo(GetOrderDetailsReq req) throws VException {
        req.verify();
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(req.getUserId()));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(req.getEntityType().name()));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(req.getEntityId()));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
        List<Orders> orders = ordersDAO.getOrders(query);
        OrderDetails res = new OrderDetails();
        if (ArrayUtils.isNotEmpty(orders)) {
            for (Orders order : orders) {
                if (PaymentStatus.PAID.equals(order.getPaymentStatus())
                        || PaymentStatus.PARTIALLY_PAID.equals(order.getPaymentStatus())) {
                    res.setPaid(Boolean.TRUE);
                    res.setAmountPaid(order.getAmount());
                    res.setOrderId(order.getId());
                    res.setPaymentStatus(order.getPaymentStatus());
                    break;
                }
            }
        }
        return res;
    }

    public List<Orders> getOrderFullDetails(GetOrderDetailsReq req) throws VException {
        req.verify();
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(req.getUserId()));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(req.getEntityType().name()));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(req.getEntityId()));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
        if (ArrayUtils.isNotEmpty(req.getPaymentStatuses())) {
            query.addCriteria(Criteria.where("paymentStatus").in(req.getPaymentStatuses()));
        }
        return ordersDAO.getOrders(query);

    }

    public PlatformBasicResponse updateOTFOrderReferenceTag(String orderId, String courseId) {
        Orders orders = ordersDAO.getById(orderId);
        if (orders != null) {
            List<ReferenceTag> referenceTags = orders.getReferenceTags();
            ReferenceTag referenceTag = new ReferenceTag();
            if (referenceTags == null) {
                referenceTags = new ArrayList<>();
                orders.setReferenceTags(referenceTags);
            }
            referenceTag.setReferenceType(ReferenceType.OTF_COURSE_ID);
            referenceTag.setReferenceId(courseId);
            referenceTags.add(referenceTag);
        }
        ordersDAO.create(orders);

        return new PlatformBasicResponse(true, null, null);
    }

    public GetInstalmentDetailsOTFRes getInstalmentDetailsOTF(String courseId, String batchId, String userId) {
        Orders registrationOrder = ordersDAO.getOTFRegisteredCourse(Long.parseLong(userId), courseId, batchId);
        GetInstalmentsReq req = new GetInstalmentsReq();
        req.setBatchId(batchId);
        req.setUserId(Long.parseLong(userId));
        List<Instalment> instalments = instalmentManager.getInstalmentsForOTFBatch(req);
        GetInstalmentDetailsOTFRes res = new GetInstalmentDetailsOTFRes();
        List<InstalmentInfo> instalmentInfos = new ArrayList<>();
        if (instalments != null) {
            for (Instalment instalment : instalments) {
                InstalmentInfo instalmentInfo = mapper.map(instalment, InstalmentInfo.class);
                instalmentInfos.add(instalmentInfo);
            }
        }
        res.setInstalments(instalmentInfos);
        if (registrationOrder != null) {
            res.setCreationTime(registrationOrder.getCreationTime());
            res.setRegistrationAmount(registrationOrder.getAmount());
        }
        return res;

    }

    public List<Instalment> prepareDiscountedInstalmentsCoursePlan(BuyItemsReqNew req) throws VException {
        List<String> couponsUsedInCurrentReq = new ArrayList<>();

        Discounts discounts = getCouponDiscount(req.getEntityId(), req.getEntityType(),
                req.getTeacherDiscountCouponId(), req.getVedantuDiscountCouponId(), couponsUsedInCurrentReq,
                req.getUserId(), req.getAmountToPay(), req.getPurchaseFlowId(),null);
        List<Instalment> instalments = instalmentManager.prepareInstalments(req.getBaseInstalmentInfos(),
                InstalmentPurchaseEntity.COURSE_PLAN, req.getEntityId(),
                discounts.vedantuDiscount, discounts.teacherDiscount, null,
                req.getUserId().toString());

        return instalments;
    }

    public List<Orders> getOrdersByDeliverableOrEntity(List<String> deliverableorEntityIds) {
        List<Orders> orders = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(deliverableorEntityIds)) {
            Query query = new Query();
            Criteria criteria = new Criteria();
            Criteria entityCriteria = Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).in(deliverableorEntityIds);
            Criteria deliverableCriteria = Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID)
                    .in(deliverableorEntityIds);
            criteria.orOperator(entityCriteria, deliverableCriteria);
            query.addCriteria(criteria);
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS)
                    .in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));
            orders = ordersDAO.getOrders(query);
        }
        return orders;
    }

    public List<FirstExtTransaction> getFirstExtTransactions(List<Long> userIds) throws InternalServerErrorException {
        List<String> holderIds = new ArrayList<>();
        for (Long userId : userIds) {
            holderIds.add(Account.__getUserAccountHolderId(userId.toString()));
        }
        Map<String, Long> accountUserMap = new HashMap<>();
        List<FirstExtTransaction> result = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(holderIds)) {
            List<Account> accounts = accountDAO.getAccountListByHolderIds(holderIds);
            for (Account account : accounts) {
                Long userId = Long.parseLong(account.getHolderId().split("/")[1]);
                accountUserMap.put(account.getId().toString(), userId);
            }
            Criteria criteria = new Criteria();
            Criteria statusCriteria = Criteria.where("reasonType").is(BillingReasonType.RECHARGE.name());
            Criteria accountCriteria = Criteria.where(Transaction.Constants.CREDIT_TO_ACCOUNT)
                    .in(accountUserMap.keySet());
            criteria.andOperator(statusCriteria, accountCriteria);
            AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
            Aggregation agg = newAggregation(match(criteria), sort(Sort.Direction.ASC, "creationTime"),
                    group(Transaction.Constants.CREDIT_TO_ACCOUNT).first("creationTime").as("creationTime"),
                    project("creationTime").and(Transaction.Constants.CREDIT_TO_ACCOUNT).previousOperation()).withOptions(aggregationOptions);
            AggregationResults<FirstExtTransaction> groupResults = transactionDAO.getMongoOperations().aggregate(agg,
                    Transaction.class.getSimpleName(), FirstExtTransaction.class);
            result = groupResults.getMappedResults();

        }
        if (CollectionUtils.isNotEmpty(result)) {
            for (FirstExtTransaction firstExtTransaction : result) {
                Long userId = accountUserMap.get(firstExtTransaction.getCreditToAccount());
                firstExtTransaction.setUserId(userId);
            }
        }

        return result;
    }

    public Map<String, InstalmentPaymentInfoRes> getOrderIdInstalmentInfoMap(List<String> ids) {
        List<Instalment> instalments = instalmentManager.getInstalmentsForOrders(ids, null);
        Map<String, InstalmentPaymentInfoRes> response = new HashMap<>();
        if (CollectionUtils.isNotEmpty(instalments)) {
            for (Instalment instalment : instalments) {
                InstalmentPaymentInfoRes instalmentPaymentInfoRes = response.get(instalment.getOrderId());
                if (instalmentPaymentInfoRes == null) {
                    instalmentPaymentInfoRes = new InstalmentPaymentInfoRes();
                    response.put(instalment.getOrderId(), instalmentPaymentInfoRes);
                }
                if (PaymentStatus.PAID.equals(instalment.getPaymentStatus())) {
                    if (instalmentPaymentInfoRes.getNoOfPaid() == null) {
                        instalmentPaymentInfoRes.setNoOfPaid(1);
                    } else {
                        instalmentPaymentInfoRes.setNoOfPaid(instalmentPaymentInfoRes.getNoOfPaid() + 1);
                    }
                } else if (PaymentStatus.NOT_PAID.equals(instalment.getPaymentStatus())
                        || PaymentStatus.PAYMENT_SUSPENDED.equals(instalment.getPaymentStatus())) {
                    if (instalmentPaymentInfoRes.getDueDate() == null
                            || instalment.getDueTime() < instalmentPaymentInfoRes.getDueDate()) {
                        instalmentPaymentInfoRes.setDueDate(instalment.getDueTime());
                        instalmentPaymentInfoRes.setAmount(instalment.getTotalAmount());
                    }
                }

                if (instalment.getDueTime() != 0 && (instalmentPaymentInfoRes.getLastInstalmentDate() == null
                        || instalment.getDueTime() > instalmentPaymentInfoRes.getLastInstalmentDate())) {
                    instalmentPaymentInfoRes.setLastInstalmentDate(instalment.getDueTime());
                }

            }
        }
        return response;
    }

    public void checkGatewayStatus(PaymentGatewayName gatewayName) throws VException {
        switch (gatewayName) {
            case CCAVENUE:
                CCAvenuePaymentManager paymentManger = (CCAvenuePaymentManager) getPaymentManger(gatewayName);
                long hourBefore = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
                logger.info("FROM TIME " + hourBefore);
                paymentManger.checkGatewayStatus(hourBefore);
                break;
            case ZAAKPAY:
                break;
            case PAYU:
                break;
            case RAZORPAY:
                break;
            case AXIS:
                break;
            case AXIS_JUSPAY:
                break;
            case JUSPAY:
                break;
        }
    }

    public List<BankDetail> getBankNames(EmiCardType code) {
        logger.info("fetching bank names for card type: " + code);
        List<BankDetail> bankDetails = bankDetailDAO.getBankNames(code);
        Map<String, BankDetail> map = bankDetails.stream().collect(Collectors.toMap(BankDetail::getEmiCode,bankDetail->bankDetail));
        List<String> bankCodes = emiCardDao.findDistinctCode(code);
        map.entrySet().removeIf(e-> bankCodes.contains(e.getKey()));
        return new ArrayList<>(map.values());
    }

    public List<EmiCard> getEmiCards() {
        return emiCardDao.getEmiCards();
    }

    public List<EmiCard> getEmiCardsByCodeAndType(EmiCardType type, String code) {
        return emiCardDao.getEmiCardByCode(type,code);
    }

    public void saveBank(BankDetail bankDetail) {
        bankDetailDAO.saveBank(bankDetail);
    }

    public void checkIfAllowed(Long userId, String customerId) throws BadRequestException, InternalServerErrorException {
        String juspayCustomerId = juspayPaymentManager.getJuspayCustomerId(userId);
        if(null != juspayCustomerId && !juspayCustomerId.equalsIgnoreCase(customerId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER, "Customer ID is not valid for the user");
        }
    }

    private class Discounts {

        private Integer teacherDiscount;

        private Integer vedantuDiscount;
        private Integer totalDiscount;

        private Discounts(Integer teacherDiscount, Integer vedantuDiscount) {
            this.teacherDiscount = teacherDiscount;
            this.vedantuDiscount = vedantuDiscount;
            this.totalDiscount = teacherDiscount + vedantuDiscount;
        }

        @Override
        public String toString() {
            return "Discounts{" + "teacherDiscount=" + teacherDiscount + ", vedantuDiscount=" + vedantuDiscount
                    + ", totalDiscount=" + totalDiscount + '}';
        }

    }

    public List<Orders> getRegisteredCourses(GetRegisteredCoursesReq req) throws VException {
        return ordersDAO.getRegisteredCourses(req);
    }

    public OrderInfo checkAndGetAccountInfoForPayment(BuyItemsReqNew req, DevicesAllowed device) throws VException {
        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(req.getUserId(), false);
        if (!Role.STUDENT.equals(userInfo.getRole())) {
            throw new ForbiddenException(ErrorCode.NON_STUDENT_NOT_ALLOWED_TO_PURCHASE, "Only students are allowed to purchase");
        }
        if (req.getThrowErrorIfAlreadyPaid()) {
            GetOrderDetailsReq getOrderDetailsReq = new GetOrderDetailsReq(req.getUserId(), req.getEntityId(),
                    req.getEntityType(), Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID));
            OrderDetails orderDetails = getOrderBasicInfo(getOrderDetailsReq);
            if (Boolean.TRUE.equals(orderDetails.getPaid())) {
                throw new ConflictException(ErrorCode.ALREADY_PAID,
                        "user is already paid for entity" + req.getEntityId() + "(" + req.getEntityType() + ")");
            }
        }

        Integer totalAmount = 0;
        Integer totalPromotionalAmount = 0;
        Integer totalNonPromotionalAmount = 0;

        Integer totalPromotionalAmountPayable = 0;
        Integer totalNonPromotionalAmountPayable = 0;

        Account account = accountManager.getAccountByUserId(req.getUserId());
        Integer promotionalBalance = account.getPromotionalBalance();

        List<String> couponsUsedInCurrentReq = new ArrayList<>();

        logger.info("Account of user: " + account);

        Discounts discounts = getCouponDiscount(req.getEntityId(), req.getEntityType(),
                req.getTeacherDiscountCouponId(), req.getVedantuDiscountCouponId(), couponsUsedInCurrentReq,
                req.getUserId(), req.getAmountToPay(), req.getPurchaseFlowId(),null);
        Integer teacherDiscount = discounts.teacherDiscount;
        Integer vedantuDiscount = discounts.vedantuDiscount;
        Integer securityCharge = 0;
        Integer convenienceCharge = 0;

        if (PaymentType.INSTALMENT.equals(req.getPaymentType())) {
            // get instalments
            List<Instalment> instalments;
            InstalmentPurchaseEntity purchasingEntityType = InstalmentPurchaseEntity.COURSE_PLAN;
            if (req.getPurchaseEntityType() != null) {
                purchasingEntityType = req.getPurchaseEntityType();
            }
            if (ArrayUtils.isEmpty(req.getBaseInstalmentInfos())) {
                // TODO remove the InstalmentPurchaseEntity.COURSE_PLAN
                // hardcoding
                instalments = instalmentManager.prepareInstalments(purchasingEntityType,
                        req.getEntityId(), vedantuDiscount, teacherDiscount, null, req.getUserId().toString());

            } else {
                instalments = instalmentManager.prepareInstalments(req.getBaseInstalmentInfos(),
                        purchasingEntityType, req.getEntityId(), vedantuDiscount, teacherDiscount, null,
                        req.getUserId().toString());
            }

            if (ArrayUtils.isNotEmpty(instalments)) {
                Instalment firstInstalment = instalments.get(0);
                int hrsCost = firstInstalment.getHoursCost();
                convenienceCharge = firstInstalment.getConvenienceCharge();
                securityCharge = firstInstalment.getSecurityCharge();
                totalAmount = 0;// reset and calculate using instalments
                for (Instalment inst : instalments) {
                    totalAmount += inst.getTotalAmount();
                }
                if (hrsCost < promotionalBalance) {
                    totalPromotionalAmountPayable += hrsCost;
                } else {
                    totalPromotionalAmountPayable += promotionalBalance;
                    totalNonPromotionalAmountPayable += hrsCost - promotionalBalance;
                }
                totalNonPromotionalAmountPayable += (convenienceCharge + securityCharge);// these
                // have
                // to
                // paid
                // from
                // np
                // only
            } else {
                logger.error("Instalments zero for buyItems " + req);
            }
        }
        else {
            totalAmount = req.getAmountToPay();
            totalAmount -= discounts.totalDiscount;
            if (totalAmount < promotionalBalance) {
                totalPromotionalAmount = totalAmount;
                totalPromotionalAmountPayable = totalAmount;
            } else {
                totalPromotionalAmount = promotionalBalance;
                totalPromotionalAmountPayable = promotionalBalance;

                totalNonPromotionalAmount = totalAmount - totalPromotionalAmount;
                totalNonPromotionalAmountPayable = totalNonPromotionalAmount;
            }
        }

        List<OrderedItem> orderedItems = new ArrayList<>();
        orderedItems.add(new OrderedItem(req.getEntityId(), req.getEntityType(), 1, totalAmount, totalAmount,
                totalPromotionalAmount, totalNonPromotionalAmount, null, null, null, null, null, null, null, null, null,
                null, req.getTeacherDiscountCouponId(), teacherDiscount, req.getVedantuDiscountCouponId(),
                vedantuDiscount, null, null));

        PaymentSource paymentSource = findPaymentSource(req.getUserAgent());

        Orders order = new Orders(req.getUserId(), orderedItems, req.getIpAddress(), totalAmount,
                totalPromotionalAmount, totalNonPromotionalAmount, PaymentStatus.NOT_PAID, req.getPurchaseFlowType(),
                req.getPurchaseFlowId(), req.getPaymentType(), paymentSource, req.getUserAgent());


        if (ArrayUtils.isNotEmpty(req.getPurchasingEntities())) {
            order.setPurchasingEntities(req.getPurchasingEntities());
        } else {
            List<PurchasingEntity> purchasingEntities = new ArrayList<>();
            PurchasingEntity purchasingEntity = new PurchasingEntity(req.getEntityType(), req.getEntityId());
            purchasingEntities.add(purchasingEntity);
            order.setPurchasingEntities(purchasingEntities);
        }

        if (req.getPurchaseContextId() != null) {
            // Setting in case of upsell
            order.setPurchaseContextId(req.getPurchaseContextId());
        }

        order.setUtm_campaign(req.getUtm_campaign());
        order.setUtm_content(req.getUtm_content());
        order.setUtm_medium(req.getUtm_medium());
        order.setUtm_source(req.getUtm_source());
        order.setUtm_term(req.getUtm_term());
        order.setChannel(req.getChannel());
        ordersDAO.create(order);

        OrderInfo orderInfo = getOrderItemPlanInfo(order);
        Integer rechargeAmount;// this is the non promotional amount to be paid

        if (req.getUseAccountBalance()) {
            // making only check for np as promotional balance is taken into
            // account and require np
            // is calculated
            if (account.getNonPromotionalBalance() < totalNonPromotionalAmountPayable) {
                rechargeAmount = totalNonPromotionalAmountPayable - account.getNonPromotionalBalance();
            } else {
                orderInfo = getOrderItemPlanInfo(order);
                orderInfo.setPaymentStatus(order.getPaymentStatus());
                orderInfo.setNeedRecharge(false);
                orderInfo.setPaymentStatus(PaymentStatus.PAID);
                return orderInfo;
            }
        } else {
            rechargeAmount = (totalNonPromotionalAmountPayable + totalPromotionalAmountPayable);
        }

        logger.info("creating ext transaction for payment ");
        rechargeAmount = ((int) Math.ceil((double) rechargeAmount / 100)) * 100;
        RechargeUrlInfo rechargeUrlInfo = accountManager.rechargeAccount(order.getUserId(), order.getIpAddress(),
                rechargeAmount, order.getId(), PaymentType.BULK, req.getRedirectUrl(), null, null,
                req.getCallingUserId(), device);
        orderInfo.setNeedRecharge(true);
        orderInfo.setGatewayName(rechargeUrlInfo.getGatewayName());
        orderInfo.setRechargeUrl(rechargeUrlInfo.getRechargeUrl());
        orderInfo.setForwardHttpMethod(rechargeUrlInfo.getForwardHttpMethod());
        if (rechargeUrlInfo.getPostParams() != null && !rechargeUrlInfo.getPostParams().isEmpty()) {
            orderInfo.setPostParams(rechargeUrlInfo.getPostParams());
        }
        // return payment gateway url
        logger.info("Exiting " + orderInfo.toString());
        return orderInfo;
    }

    public OrderInfo processOrderAfterPayment(String orderId, ProcessPaymentReq req) throws VException {
        req.verify();
        Orders order = ordersDAO.getById(orderId);
        if (order == null || ArrayUtils.isEmpty(order.getItems())) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "order or items not found for id " + orderId);
        }

        if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            if (req.getPurchaseEntityType() == null || req.getPurchaseEntityId() == null) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "purchaseentityType and id not found");
            }
        }

        Account userAccount = accountManager.getAccountByUserId(order.getUserId());
        List<OrderedItem> items = order.getItems();
        if (items.size() > 1) {
            logger.error("Order items more than, but processing only one, id " + orderId);
        }
        OrderedItem item = order.getItems().get(0);

        Transaction transaction = null;
        // create instalments
        List<Instalment> instalments = null;
        if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            if (ArrayUtils.isEmpty(req.getBaseInstalmentInfos())) {
                instalments = instalmentManager.prepareInstalments(req.getPurchaseEntityType(),
                        req.getPurchaseEntityId(), item.getVedantuDiscountAmount(), item.getTeacherDiscountAmount(), orderId, req.getUserId().toString());
            } else {
                instalments = instalmentManager.prepareInstalments(req.getBaseInstalmentInfos(),
                        req.getPurchaseEntityType(), req.getPurchaseEntityId(),
                        item.getVedantuDiscountAmount(), item.getTeacherDiscountAmount(), orderId,
                        req.getUserId().toString());
            }
            instalmentDAO.saveInstalments(instalments);
        } else {
            transaction = accountManager.transferAmount(userAccount, accountDAO.getVedantuDefaultAccount(),
                    order.getAmount(), order.getPromotionalAmount(), order.getNonPromotionalAmount(),
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                    TransactionRefType.ITEM_ORDERED, order.getId(), Transaction._getTriggredByUser(order.getUserId()),
                    false, null);
            order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PAID, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//            order.setPaymentStatus(PaymentStatus.PAID);
        }

        // transfer money to vedantu accounts from student account
        // Transaction transaction;
        Integer vedantuDiscountToTransfer = item.getVedantuDiscountAmount();
        if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            if (ArrayUtils.isNotEmpty(instalments)) {
                // doing this again because I do not want to save the
                // instalments before the first
                // payment
                Instalment firstInstalment = instalments.get(0);
                Integer instHrsPromotionalAmount = 0;
                Integer instHrsNonPromotionalAmount = 0;
                if (firstInstalment.getHoursCost() < userAccount.getPromotionalBalance()) {
                    instHrsPromotionalAmount = firstInstalment.getHoursCost();
                } else {
                    instHrsPromotionalAmount = userAccount.getPromotionalBalance();
                    instHrsNonPromotionalAmount = firstInstalment.getHoursCost() - userAccount.getPromotionalBalance();
                }
                Integer totalNonPromotionalAmountToBePaid = instHrsNonPromotionalAmount
                        + firstInstalment.getConvenienceCharge() + firstInstalment.getSecurityCharge();

                // as two transfers are needed, making a check beforehand to see
                // if both can go through
                if (userAccount.getPromotionalBalance() >= instHrsPromotionalAmount
                        && userAccount.getNonPromotionalBalance() >= totalNonPromotionalAmountToBePaid) {
                    firstInstalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.PAID);
                    firstInstalment.setPaidTime(System.currentTimeMillis());
                    firstInstalment.setTotalNonPromotionalAmount(totalNonPromotionalAmountToBePaid);
                    firstInstalment.setTotalPromotionalAmount(instHrsPromotionalAmount);
                    firstInstalment.setTriggeredBy(order.getUserId().toString());
                    instalmentDAO.save(firstInstalment);

                    String uniqueId = TransactionRefType.INSTALMENT.name() + "_" + BillingReasonType.ACCOUNT_DEDUCTION.name() + "_" + firstInstalment.getId();
                    // hrs cost +security charge goes to default account
                    transaction = accountManager.transferAmount(userAccount, accountDAO.getVedantuDefaultAccount(),
                            (firstInstalment.getHoursCost() + firstInstalment.getSecurityCharge()),
                            instHrsPromotionalAmount,
                            (instHrsNonPromotionalAmount + firstInstalment.getSecurityCharge()),
                            ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                            TransactionRefType.INSTALMENT, firstInstalment.getId(),
                            Transaction._getTriggredByUser(order.getUserId()), false, uniqueId);

                    // convenience charge to revenue wallet,going with the same
                    // ref and ref nos
                    if (firstInstalment.getConvenienceCharge() != null && firstInstalment.getConvenienceCharge() > 0) {
                        accountManager.transferAmount(userAccount, accountDAO.getVedantuCutAccount(),
                                firstInstalment.getConvenienceCharge(), 0, firstInstalment.getConvenienceCharge(),
                                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                                TransactionRefType.INSTALMENT, firstInstalment.getId(),
                                Transaction._getTriggredByUser(order.getUserId()), false, uniqueId);
                    }

                    vedantuDiscountToTransfer = firstInstalment.getVedantuDiscountAmount();

                    order.updateOrderPaidDetails(order, firstInstalment);
                    // check and update the order pojo, so that we can save it
                    // after this if-else block
                    instalmentManager.updateOrderPojo(order, instalments);
                } else {
                    // this is not an error but occurs in rare cases, so
                    // capturing to understand the circumstances
                    logger.error("ACCOUNT_INSUFFICIENT_BALANCE in process order " + order);
                    throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
                }
            } else {
                logger.error("Payment type instalment but no instalments found " + order);
                throw new ConflictException(ErrorCode.INSTALMENTS_INCONSISTENT, "no instalments found");
            }
        } else {
            if (vedantuDiscountToTransfer != null && vedantuDiscountToTransfer > 0l) {
                item.setVedantuDiscountAmountClaimed(vedantuDiscountToTransfer);
            }
        }

        // creating transaction for vedantu discount amount from Vm to Vd
        if (vedantuDiscountToTransfer != null && vedantuDiscountToTransfer > 0l) {
            accountManager.transferAmount(accountDAO.getVedantuFreebiesAccount(), accountDAO.getVedantuDefaultAccount(),
                    vedantuDiscountToTransfer, vedantuDiscountToTransfer, 0,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.DISCOUNT,
                    TransactionRefType.ITEM_ORDERED, order.getId(), Transaction._getTriggredByUser(order.getUserId()),
                    true, null);
        }

        if (transaction != null) {
            order.setTransactionId(transaction.getId());
        }
        addReferenceTag(order);
        boolean updateInstalments = false;
        if (StringUtils.isNotEmpty(req.getDeliverableEntityId())) {
            if (ArrayUtils.isNotEmpty(order.getPurchasingEntities()) && order.getPurchasingEntities().size() == 1) {
                order.getPurchasingEntities().get(0).setDeliverableId(req.getDeliverableEntityId());
                updateInstalments = true;
            }
        }
        if (ArrayUtils.isNotEmpty(req.getPurchasingEntites())) {
            order.setPurchasingEntities(order.getPurchasingEntities());
            updateInstalments = true;
        }

        // finally upsert the order
        logger.info("saving the order");
        ordersDAO.create(order);
        triggerOrderForLeadSquare(order, true);
        if (updateInstalments && PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            try {
                instalmentAsyncTasksManager.updatePurchasingEntitiesInInstalment(order.getId(), order.getPurchasingEntities());
            } catch (Exception e) {
                logger.info("Error in updating enrollments" + e);
            }

        }

        // update coupon usage
        if (StringUtils.isNotEmpty(item.getTeacherDiscountCouponId())) {
            if (PurchaseFlowType.SUBSCRIPTION_REQUEST.equals(order.getPurchaseFlowType())) {
                // TODO handle the exception gracefully
                // old state is UNUSED because first the unlock balance happens
                // and then usage happens
                couponManager.updateRedeemEntry(item.getTeacherDiscountCouponId(), order.getPurchaseFlowId(),
                        item.getTeacherDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.UNUSED, RedeemedCouponState.PROCESSED);
            } else {
                couponManager.addRedeemEntry(item.getTeacherDiscountCouponId(), order.getUserId(),
                        item.getTeacherDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.PROCESSED);
            }
        }
        if (StringUtils.isNotEmpty(item.getVedantuDiscountCouponId())) {
            if (PurchaseFlowType.SUBSCRIPTION_REQUEST.equals(order.getPurchaseFlowType())) {
                // TODO handle the exception gracefully
                couponManager.updateRedeemEntry(item.getVedantuDiscountCouponId(), order.getPurchaseFlowId(),
                        item.getTeacherDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.UNUSED, RedeemedCouponState.PROCESSED);
            } else {
                couponManager.addRedeemEntry(item.getVedantuDiscountCouponId(), order.getUserId(),
                        item.getVedantuDiscountAmount(), item.getEntityId(), item.getEntityType(),
                        RedeemedCouponState.PROCESSED);
            }
        }
        OrderInfo orderInfo = getOrderItemPlanInfo(order);
        triggerOrderUpdatedForUser(order);
        if (PaymentType.BULK.equals(order.getPaymentType())) {
            orderInfo.setPromotionalAmount(order.getPromotionalAmount());
            orderInfo.setNonpromotionalAmount(order.getNonPromotionalAmount());
        } else if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            orderInfo.setPromotionalAmount(order.getPromotionalAmountPaid());
            orderInfo.setNonpromotionalAmount(order.getNonPromotionalAmountPaid());
        }
        orderInfo.setOrderedItems(order.getItems());
        return orderInfo;
    }

    public Orders resetInstallmentState(ResetInstallmentStateReq req) throws BadRequestException, NotFoundException, InternalServerErrorException, ConflictException {
        logger.info("req:" + req.toString());
        req.validate();

        // Fetch Orders
        Query ordersQuery = new Query();
        ordersQuery.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(req.getStudentId()));
        ordersQuery.addCriteria(Criteria.where("items.0.entityType").is(EntityType.OTF));
        ordersQuery.addCriteria(Criteria.where("items.0.entityId").is(req.getBatchId()));
        ordersQuery.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(PaymentStatus.PAYMENT_SUSPENDED));
        List<Orders> orders = ordersDAO.runQuery(ordersQuery, Orders.class);
        if (CollectionUtils.isEmpty(orders)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No orders found in FORFEITED state");
        }
        if (orders.size() > 1) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Multiple orders found. No idea what to do");
        }

        Orders order = orders.get(0);
        logger.info("Order pre update : " + order.toString());

        // Fetch respective installments
        List<Instalment> instalments = instalmentManager.getInstalmentsForOrder(order.getId(), PaymentStatus.PAYMENT_SUSPENDED);
        if (CollectionUtils.isEmpty(instalments)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "No installments found for orderid:" + order.getId());
        }

        Collections.sort(instalments, new Comparator<Instalment>() {
            @Override
            public int compare(Instalment o1, Instalment o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });

        int duePastInstalments = 0;
        int amount = instalments.get(0).getTotalAmount();
        Account account = accountManager.getAccountByUserId(req.getStudentId());
        if (account.getBalance() < amount) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Insufficient balance to pay the installment. AccountBalance:" + account.getBalance()
                    + " required installment amount:" + instalments.get(0).getTotalAmount() + " installment:"
                    + instalments.get(0).toString());
        }

        for (Instalment instalment : instalments) {
            if (instalment.getDueTime() < System.currentTimeMillis()) {
                duePastInstalments++;
            } else {
                break;
            }
        }

        if (duePastInstalments == 0) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No instalments in past");
        }
        if (duePastInstalments > 1) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Multiple installments in past");
        }

        // Mark the installments NOT_PAID
        for (Instalment instalment : instalments) {
            logger.info("Installment pre update : " + instalment.toString());
            instalment.setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus.NOT_PAID);
            instalmentDAO.save(instalment);
            logger.info("Installment post update:" + instalment.toString());
        }
        order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.PARTIALLY_PAID, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//        order.setPaymentStatus(PaymentStatus.PARTIALLY_PAID);
        ordersDAO.create(order);
        triggerOrderForLeadSquare(order, false);
        logger.info("Oder post update :" + order.toString());
        return order;
    }

    private void addReferenceTag(Orders orders) {
        OrderedItem orderItem = orders.getItems().get(0);
        List<ReferenceTag> referenceTags = orders.getReferenceTags();
        ReferenceTag referenceTag = new ReferenceTag();
        if (referenceTags == null) {
            referenceTags = new ArrayList<>();
            orders.setReferenceTags(referenceTags);
        }

        if (EntityType.OTO_COURSE_REGISTRATION.equals(orderItem.getEntityType())) {
            referenceTag.setReferenceType(ReferenceType.OTO_STRUCTURED_COURSE_ID);
            referenceTag.setReferenceId(orderItem.getEntityId());
            referenceTags.add(referenceTag);
        } else if (EntityType.OTF_COURSE_REGISTRATION.equals(orderItem.getEntityType())) {
            referenceTag.setReferenceType(ReferenceType.OTF_COURSE_ID);
            referenceTag.setReferenceId(orderItem.getEntityId());
            referenceTags.add(referenceTag);
        }
    }

    public Map<String, List<DashBoardInstalmentInfo>> getInstallmentforBatches(List<String> enrollmentIds)
            throws VException {

        if (CollectionUtils.isEmpty(enrollmentIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "no enrollmentids in request");

        }

        Map<String, List<DashBoardInstalmentInfo>> enrollmentInstalmentMap = new HashMap<String, List<DashBoardInstalmentInfo>>();
        List<DashBoardInstalmentInfo> oTFBasicInstalmentInfos = new ArrayList<DashBoardInstalmentInfo>();
        Query orderQuery = new Query();
        orderQuery.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(EntityType.OTF));
        orderQuery.addCriteria(Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID).in(enrollmentIds));
        orderQuery.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS)
                .in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));
        orderQuery.fields().include(Orders.Constants.ID);
        orderQuery.fields().include(Orders.Constants._ID);
        orderQuery.fields().include(Orders.Constants.ITEMS);
        orderQuery.fields().include(Orders.Constants.USER_ID);
        orderQuery.fields().include(Orders.Constants.PAYMENT_STATUS);
        orderQuery.fields().include(Orders.Constants.PAYMENT_TYPE);
        List<Orders> orders = ordersDAO.getOrders(orderQuery);
        if (CollectionUtils.isEmpty(orders)) {
            logger.info("orders null for enrollmentId.");
            return enrollmentInstalmentMap;
        }
        List<String> orderIds = new ArrayList<String>();
        Map<String, String> orderenrollmentMap = new HashMap<>();
        for (Orders order : orders) {
            String enrollmentId = order.getItems().get(0).getDeliverableEntityId();
            orderenrollmentMap.put(order.getId(), enrollmentId);
            if (order.getPaymentType().equals(PaymentType.INSTALMENT)) {
                orderIds.add(order.getId());
                enrollmentInstalmentMap.put(enrollmentId, new ArrayList<>());
            } else {
                DashBoardInstalmentInfo oTFBasicInstalmentInfo = new DashBoardInstalmentInfo();
                oTFBasicInstalmentInfo.setPaymentType(PaymentType.BULK);
                oTFBasicInstalmentInfo.setOrderId(order.getId());
                oTFBasicInstalmentInfo.setPaymentStatus(order.getPaymentStatus());
                oTFBasicInstalmentInfo.setPaidTime(order.getCreationTime());
                oTFBasicInstalmentInfo.setTotalAmount(order.getAmount());
                enrollmentInstalmentMap.put(enrollmentId, Arrays.asList(oTFBasicInstalmentInfo));
            }
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").in(orderIds));
        query.addCriteria(Criteria.where(Instalment.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        query.fields().include(Instalment.Constants.ID);
        query.fields().include(Instalment.Constants._ID);
        query.fields().include(Instalment.Constants.CONTEXT_ID);
        query.fields().include(Instalment.Constants.ORDER_ID);
        query.fields().include(Instalment.Constants.PAYMENT_STATUS);
        query.fields().include(Instalment.Constants.PAID_TIME);
        query.fields().include(Instalment.Constants.TOTAL_AMOUNT);
        query.fields().include(Instalment.Constants.DUE_TIME);

        List<Instalment> instalments = instalmentDAO.runQuery(query, Instalment.class);
        if (CollectionUtils.isEmpty(instalments)) {
            return enrollmentInstalmentMap;
        }
        Collections.sort(instalments, new Comparator<Instalment>() {
            @Override
            public int compare(Instalment o1, Instalment o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });

        for (Instalment instalment : instalments) {
            DashBoardInstalmentInfo oTFBasicInstalmentInfo = mapper.map(instalment, DashBoardInstalmentInfo.class);
            oTFBasicInstalmentInfo.setId(instalment.getId());
            oTFBasicInstalmentInfos.add(oTFBasicInstalmentInfo);
            String enrollmentId = orderenrollmentMap.get(instalment.getOrderId());
            List<DashBoardInstalmentInfo> tempList = enrollmentInstalmentMap.get(enrollmentId);
            tempList.add(oTFBasicInstalmentInfo);
            // enrollmentInstalmentMap.put(enrollmentId,tempList);

        }
        return enrollmentInstalmentMap;

    }

    public Map<String, List<DashBoardInstalmentInfo>> getInstallmentsByContextIds(List<String> contextIds, Long userId)
            throws VException {

        Map<String, List<DashBoardInstalmentInfo>> contextIdInstalmentMap = new HashMap<>();
        if (CollectionUtils.isEmpty(contextIds) || userId == null) {
            return contextIdInstalmentMap;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("contextId").in(contextIds));
        query.addCriteria(Criteria.where(Instalment.Constants.USER_ID).in(userId));
        query.addCriteria(Criteria.where(Instalment.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        query.fields().include(Instalment.Constants.ID);
        query.fields().include(Instalment.Constants._ID);
        query.fields().include(Instalment.Constants.CONTEXT_ID);
        query.fields().include(Instalment.Constants.ORDER_ID);
        query.fields().include(Instalment.Constants.PAYMENT_STATUS);
        query.fields().include(Instalment.Constants.PAID_TIME);
        query.fields().include(Instalment.Constants.TOTAL_AMOUNT);
        query.fields().include(Instalment.Constants.TOTAL_PROMOTIONAL_AMOUNT);
        query.fields().include(Instalment.Constants.DUE_TIME);

        List<Instalment> instalments = instalmentDAO.runQuery(query, Instalment.class);
        if (CollectionUtils.isEmpty(instalments)) {
            return contextIdInstalmentMap;
        }
        Collections.sort(instalments, new Comparator<Instalment>() {
            @Override
            public int compare(Instalment o1, Instalment o2) {
                return o1.getDueTime().compareTo(o2.getDueTime());
            }
        });

        for (Instalment instalment : instalments) {
            DashBoardInstalmentInfo oTFBasicInstalmentInfo = mapper.map(instalment, DashBoardInstalmentInfo.class);
            oTFBasicInstalmentInfo.setId(instalment.getId());
            String key = instalment.getContextId();
            if (StringUtils.isNotEmpty(key)) {
                if (contextIdInstalmentMap.containsKey(key)) {
                    List<DashBoardInstalmentInfo> pojoList = contextIdInstalmentMap.get(key);
                    pojoList.add(oTFBasicInstalmentInfo);
                } else {
                    List<DashBoardInstalmentInfo> pojoList = new ArrayList<>();
                    pojoList.add(oTFBasicInstalmentInfo);
                    contextIdInstalmentMap.put(key, pojoList);
                }
            }

        }
        return contextIdInstalmentMap;

    }

    public List<Orders> getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded(
            GetOrdersWithoutDeliverableEntityReq req)
            throws InternalServerErrorException, NumberFormatException, NotFoundException {

        Query query = new Query();
        EntityType entityType;
        if (req.getEntityType() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(req.getEntityType()));
        }
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID).exists(false));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(PaymentStatus.PAID));

        if (req.getUserId() != null) {
            query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(req.getUserId()));
        }
        if (StringUtils.isNotEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).in(req.getEntityId()));
        }
        ordersDAO.setFetchParameters(query, req);

        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);

        return orders;
    }

    public Map<String, Orders> getRegistrationforEnrollments(List<String> enrollmentIds) throws VException {

        if (CollectionUtils.isEmpty(enrollmentIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "no enrollmentids in request");

        }
        Map<String, Orders> enrollmentOrdersMap = new HashMap<>();
        Query orderQuery = new Query();
        orderQuery.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE)
                .in(Arrays.asList(EntityType.OTF_COURSE_REGISTRATION, EntityType.OTF_BATCH_REGISTRATION)));
        orderQuery.addCriteria(Criteria.where(Orders.Constants.ITEMS_DELIVERABLE_ID).in(enrollmentIds));
        orderQuery.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS)
                .in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));

        orderQuery.fields().include(Orders.Constants.ID);
        orderQuery.fields().include(Orders.Constants._ID);
        orderQuery.fields().include(Orders.Constants.ITEMS);
        orderQuery.fields().include(Orders.Constants.USER_ID);
        orderQuery.fields().include(Orders.Constants.PAYMENT_STATUS);
        orderQuery.fields().include(Orders.Constants.PAYMENT_TYPE);
        orderQuery.fields().include(Orders.Constants.AMOUNT);
        List<Orders> orders = ordersDAO.getOrders(orderQuery);
        if (CollectionUtils.isEmpty(orders)) {
            logger.info("orders null for enrollmentId.");
            return enrollmentOrdersMap;
        }

        for (Orders order : orders) {
            enrollmentOrdersMap.put(order.getItems().get(0).getDeliverableEntityId(), order);
        }
        return enrollmentOrdersMap;
    }

    public Set<Long> getNewStudentsForTimePeriod(GetDueInstalmentsForTimeReq req) {
        Set<Long> userIds = new HashSet<>();
        if (req.getStartTime() == null || req.getEndTime() == null) {
            return userIds;
        }

        List<Orders> orders = new ArrayList<>();
        int start = 0;
        int limit = 200;
        List<EntityType> entityTypes = new ArrayList<>();
        entityTypes.add(EntityType.COURSE_PLAN);
        entityTypes.add(EntityType.OTF);
        entityTypes.add(EntityType.BUNDLE);
        entityTypes.add(EntityType.BUNDLE_PACKAGE);
        while (true) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).gte(req.getStartTime()).lt(req.getEndTime()));
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).in(entityTypes));
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));

            query.with(Sort.by(Sort.Direction.ASC, AbstractMongoEntity.Constants.CREATION_TIME));
            ordersDAO.setFetchParameters(query, start, limit);
            List<Orders> tempOrders = ordersDAO.runQuery(query, Orders.class);

            if (ArrayUtils.isEmpty(tempOrders)) {
                break;
            }
            start += limit;
            orders.addAll(tempOrders);

        }
        if (ArrayUtils.isNotEmpty(orders)) {
            for (Orders order : orders) {
                userIds.add(order.getUserId());
            }
        }
        List<Orders> prevOrders = new ArrayList<>();
        start = 0;
        limit = 500;
        while (true) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Orders.Constants.USER_ID).in(userIds));
            query.addCriteria(Criteria.where(Orders.Constants.CREATION_TIME).lt(req.getStartTime()));
            query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).in(entityTypes));
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));
            query.fields().include(Orders.Constants.USER_ID);
            query.fields().include(Orders.Constants.CREATION_TIME);
            query.fields().include(Orders.Constants.ITEMS_ENTITY_TYPE);
            query.with(Sort.by(Sort.Direction.ASC, AbstractMongoEntity.Constants.CREATION_TIME));
            ordersDAO.setFetchParameters(query, start, limit);
            List<Orders> tempOrders = ordersDAO.runQuery(query, Orders.class);

            if (ArrayUtils.isEmpty(tempOrders)) {
                break;
            }
            //start += limit;
            if (ArrayUtils.isNotEmpty(tempOrders)) {
                for (Orders order : tempOrders) {
                    userIds.remove(order.getUserId());
                }
            }
            if (ArrayUtils.isEmpty(userIds)) {
                break;
            }
            prevOrders.addAll(tempOrders);
        }
//    	if(ArrayUtils.isNotEmpty(prevOrders)){
//    		for(Orders order : prevOrders){
//    			userIds.remove(order.getUserId());
//    		}
//    	}
        return userIds;
    }

    public PlatformBasicResponse reverseOrderTransaction(String orderId) throws VException {
        logger.info("Reversing transaction orderId " + orderId);
        Orders order = ordersDAO.getById(orderId);
        if (order == null || ArrayUtils.isEmpty(order.getItems())) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "order or items not found for id " + orderId);
        }

        if (!(PaymentStatus.PAID.equals(order.getPaymentStatus())
                || PaymentStatus.PARTIALLY_PAID.equals(order.getPaymentStatus()))) {
            throw new ForbiddenException(ErrorCode.ORDER_NOT_IN_PAID_STATE, "order is not in paid state");
        }

        if (!PaymentType.BULK.equals(order.getPaymentType())) {
            throw new ForbiddenException(ErrorCode.NON_BULK_ORDER_CANNOT_BE_REVERTED, "order payment is not of type Bulk");
        }
        Account account = accountManager.getAccountByUserId(order.getUserId());

        accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(), account,
                order.getAmount(), order.getPromotionalAmount(), order.getNonPromotionalAmount(),
                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                TransactionRefType.REVERSE_ITEM_ORDERED, order.getId(),
                Transaction._getTriggredBySystem(), true, null);
        order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.REVERTED, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//        order.setPaymentStatus(PaymentStatus.REVERTED);
        ordersDAO.create(order);
        triggerOrderForLeadSquare(order, false);
        return new PlatformBasicResponse();
    }

    public static void main(String[] args) throws ForbiddenException {
//            logger=LogManager.getRootLogger();
        List<PaymentGateway> paymentGateways = new ArrayList<>();
        paymentGateways.add(new PaymentGateway(PaymentGatewayName.PAYU, true, 20));
        paymentGateways.add(new PaymentGateway(PaymentGatewayName.CCAVENUE, true, 30));
        paymentGateways.add(new PaymentGateway(PaymentGatewayName.RAZORPAY, true, 60));
        paymentGateways.add(new PaymentGateway(PaymentGatewayName.ZAAKPAY, true, 90));

        List<ExtTransaction> extTransactions = new ArrayList<>();
        ExtTransaction ext = new ExtTransaction();
        ext.setGatewayName(PaymentGatewayName.CCAVENUE);
        ext.setStatus(TransactionStatus.FAILED);
        ext.setCreationTime(System.currentTimeMillis());
        extTransactions.add(ext);

        ExtTransaction ext1 = new ExtTransaction();
        ext1.setGatewayName(PaymentGatewayName.CCAVENUE);
        ext1.setStatus(TransactionStatus.FAILED);
        ext1.setCreationTime(System.currentTimeMillis() - 4800000);
        extTransactions.add(ext1);

        ExtTransaction ext12 = new ExtTransaction();
        ext12.setGatewayName(PaymentGatewayName.CCAVENUE);
        ext12.setStatus(TransactionStatus.SUCCESS);
        ext12.setCreationTime(System.currentTimeMillis() - 7200000);
        extTransactions.add(ext12);

        Collections.sort(extTransactions, new Comparator<ExtTransaction>() {
            @Override
            public int compare(ExtTransaction o1, ExtTransaction o2) {
                return o2.getCreationTime().compareTo(o1.getCreationTime());
            }
        });
        System.out.println(">> " + extTransactions.get(2));
        PaymentManager pgr = new PaymentManager();
        pgr._getPaymentManager(paymentGateways, extTransactions);
        System.err.println(">>rss");
    }

    public PlatformBasicResponse updateDeliverableDataFetchOrderIds(UpdateDeliverableDataReq req) throws BadRequestException, VException {
        if (StringUtils.isEmpty(req.getEntityId()) || req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId/entityId is null");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(req.getUserId()));

        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_REFUND_STATUS).exists(false));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));

        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(req.getEntityId()));

        List<Orders> orders = ordersDAO.getOrders(query);
        if (ArrayUtils.isNotEmpty(orders)) {
            for (Orders order : orders) {
                updateDeliverableData(req, order);
            }
        }

        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse updateDeliverableData(UpdateDeliverableDataReq req) throws VException {
        req.verify();
        if (StringUtils.isEmpty(req.getOrderId())) {
            return updateDeliverableDataFetchOrderIds(req);
        } else {
            Orders order = ordersDAO.getById(req.getOrderId());
            return updateDeliverableData(req, order);

        }

    }

    public PlatformBasicResponse updateDeliverableData(UpdateDeliverableDataReq req, Orders order) throws VException {
        req.verify();

        if (order == null || ArrayUtils.isEmpty(order.getPurchasingEntities())) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "order or purchasing entities not found for id " + req.getOrderId());
        }

        if (!(PaymentStatus.PAID.equals(order.getPaymentStatus())
                || PaymentStatus.PARTIALLY_PAID.equals(order.getPaymentStatus()))) {
            throw new ForbiddenException(ErrorCode.ORDER_NOT_IN_PAID_STATE, "order is not in paid state");
        }
        Map<String, String> dMap = req.getEntityDeliverableMap();
//        for (int k = 0; k < req.getPurchasingEntityIds().size(); k++) {
//            dMap.put(req.getPurchasingEntityIds().get(k), req.getDeliverableIds().get(k));
//        }

        for (PurchasingEntity purchasingEntity : order.getPurchasingEntities()) {
            if (StringUtils.isNotEmpty(purchasingEntity.getDeliverableId())) {
                continue;
            }
            if (dMap.containsKey(purchasingEntity.getEntityId())) {
                purchasingEntity.setDeliverableId(dMap.get(purchasingEntity.getEntityId()));
            }
        }

        ordersDAO.create(order);

        if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
            try {
                instalmentAsyncTasksManager.updatePurchasingEntitiesInInstalment(order.getId(), order.getPurchasingEntities());
            } catch (Exception e) {
                logger.info("Error in updating enrollments" + e);
            }
        }
        return new PlatformBasicResponse();
    }

    public List<Orders> getOrdersByDeliverableIds(List<String> deliverableIds) {
        List<Orders> orders = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(deliverableIds)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Orders.Constants.PURCHASING_ENTITIES_DELIVERABLE_ID)
                    .in(deliverableIds));
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS)
                    .in(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID, PaymentStatus.PAYMENT_SUSPENDED)));
            query.with(Sort.by(Direction.ASC, Orders.Constants.CREATION_TIME));
            orders = ordersDAO.getOrders(query);
        }
        return orders;
    }

    public List<Orders> getInstallmentOrdersByDeliverableId(String deliverableId) {
        List<Orders> orders = new ArrayList<>();
        if (StringUtils.isNotEmpty(deliverableId)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Orders.Constants.PURCHASING_ENTITIES_DELIVERABLE_ID).is(deliverableId));
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_TYPE).is(PaymentType.INSTALMENT));
            query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.PARTIALLY_PAID)));
            query.with(Sort.by(Direction.DESC, Orders.Constants.CREATION_TIME));
            orders = ordersDAO.getOrders(query);
        }
        return orders;
    }

    public void triggerOrderUpdatedForUser(Orders order) {
        try {
            instalmentAsyncTasksManager.triggerOrderUpdatedForUser(order);
        } catch (Exception e) {
            logger.error("Error in triggering sns. Error :" + e);
        }

    }

    public void triggerOrderForLeadSquare(Orders order, boolean newCreated) {
        if (newCreated) {
            instalmentAsyncTasksManager.triggerLeadSquareQueue(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.ORDER_SAVED_LEADSQUARE, gson.toJson(order), order.getUserId().toString() + "_ORDER");
            if(PaymentStatus.PAID.equals(order.getPaymentStatus()) || PaymentStatus.PARTIALLY_PAID.equals(order.getPaymentStatus())) {
                instalmentAsyncTasksManager.triggerLeadSquareQueue(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.STUDENT_ENROLLED, gson.toJson(order), order.getUserId().toString() + "_STUDENT_ENROLL");
            }
        } else {
            instalmentAsyncTasksManager.triggerLeadSquareQueue(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.ORDER_UPDATED, gson.toJson(order), order.getUserId().toString() + "_ORDER");
        }

    }

    public EntityType getEntityTypeForPurchasingEntity(EntityType entityType) {

        if (entityType == null) {
            return entityType;
        }
        switch (entityType) {
            case OTO_COURSE_REGISTRATION:
            case COURSE_PLAN_REGISTRATION:
            case COURSE_PLAN:
                return EntityType.COURSE_PLAN;
            case OTF:
            case OTF_BATCH_REGISTRATION:
                return EntityType.OTF;
            case OTF_COURSE_REGISTRATION:
                return EntityType.OTF_COURSE;
            case SUBSCRIPTION:
            case SUBSCRIPTION_REQUEST:
                return EntityType.SUBSCRIPTION;
            case PLAN:
                return EntityType.PLAN;
            default:
                return entityType;
        }
    }

    public PlatformBasicResponse markPendingExtTransaction(MarkPendingExtTxnReq markPendingExtTxnReq) throws InternalServerErrorException, NotFoundException, ForbiddenException {
        ExtTransaction extTransaction = extTransactionDAO.getById(markPendingExtTxnReq.getExtId());

        if (extTransaction == null) {
            throw new NotFoundException(ErrorCode.TRANSACTION_NOT_FOUND, "Transaction not found");
        }

        if (!TransactionStatus.PENDING.equals(extTransaction.getStatus())) {
            return new PlatformBasicResponse(false, null, "Ext Transaction status not pending");
        }

//        if(PaymentGatewayName.RAZORPAY.equals(extTransaction.getGatewayName())) {
//        	throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "RazorPay transactions will be handled automatically");
//        }
        extTransaction.setStatus(TransactionStatus.SUCCESS);
        extTransaction.setMarkSuccessType(markPendingExtTxnReq.getMarkSuccessType());
        extTransaction.setMarkSuccessReason(markPendingExtTxnReq.getMarkSuccessReason());
        int amountPaid = extTransaction.getAmount();
        logger.info("transaction was successful:" + extTransaction);
        Account account = accountDAO
                .getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
        if (account == null) {
            logger.error("no account found for userid:" + extTransaction.getUserId());

            throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                    "no account found for userId:" + extTransaction.getUserId());
        }
        logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

        account.setBalance(account.getBalance() + amountPaid);
        account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

        logger.info("new account balance : " + account.getBalance());


        accountDAO.updateWithoutSession(account, null);

        com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                TransactionRefType.EXTERNAL_TRANSACTION, null,
                com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
        transactionDAO.create(vedantuTransaction);


        extTransactionDAO.create(extTransaction, markPendingExtTxnReq.getCallingUserId());
        createPaymentInvoice(extTransaction);
        return new PlatformBasicResponse(true, "", "");

    }

    public PlatformBasicResponse generateInvoiceForExTransaction(String id) {
        PlatformBasicResponse res = new PlatformBasicResponse();
        ExtTransaction extTransaction = extTransactionDAO.getById(id);
        if (extTransaction == null) {
            res.setSuccess(false);
            return res;
        }
        createPaymentInvoice(extTransaction);
        return res;
    }

    public PlatformBasicResponse generateInvoiceForTransaction(String id, Long userId) {
        PlatformBasicResponse res = new PlatformBasicResponse();
        Transaction transaction = transactionDAO.getById(id);
        if (transaction == null) {
            res.setSuccess(false);
            return res;
        }
        createPaymentInvoice(transaction, userId);
        return res;
    }

    public void createPaymentInvoice(ExtTransaction extTransaction) {
        if (extTransaction == null || !TransactionStatus.SUCCESS.equals(extTransaction.getStatus())) {
            return;
        }
        CreatePaymentInvoiceReq req = new CreatePaymentInvoiceReq(
                extTransaction.getIpAddress(),
                extTransaction.getUserId(),
                PaymentInvoiceContextType.EXTRANSACTION,
                extTransaction.getGatewayName(),
                extTransaction.getId(),
                extTransaction.getAmount(),
                extTransaction.getLastUpdated()
        );
        awsSQSManager.sendToSQS(SQSQueue.DINERO_INVOICES_QUEUE, SQSMessageType.GENERATE_STUDENT_INVOICE,
                gson.toJson(req));
    }

    public void createPaymentInvoice(Transaction transaction, Long userId) {
        if (transaction == null || !TransactionRefType.CASH_CHEQUE.equals(transaction.getReasonRefType()) || userId == null) {
            return;
        }
        CreatePaymentInvoiceReq req = new CreatePaymentInvoiceReq(
                null,
                userId,
                PaymentInvoiceContextType.CASH_CHEQUE,
                null,
                transaction.getId(),
                transaction.getAmount(),
                transaction.getLastUpdated()
        );
        awsSQSManager.sendToSQS(SQSQueue.DINERO_INVOICES_QUEUE, SQSMessageType.GENERATE_STUDENT_INVOICE,
                gson.toJson(req));
    }

    public Orders getOrderById(String id) {
        return ordersDAO.getById(id);
    }

    public Map<String, Boolean> isDoubtsPaidUser(Long userId) {
        List<Orders> o = ordersDAO.isDoubtsPaidUser(userId);
        Map<String, Boolean> resp = new HashMap<>();
        // Paid user definition for now
        if (ArrayUtils.isNotEmpty(o)) {
            for (Orders order : o) {
                int promotionAmount = 0;
                if (ArrayUtils.isNotEmpty(order.getItems())) {
                    if (order.getItems().get(0).getVedantuDiscountAmount() != null) {
                        promotionAmount = order.getItems().get(0).getVedantuDiscountAmount();
                    }
                }
                if (order.getAmount() != null && order.getAmount() + promotionAmount > 300000) {
                    resp.put("isPaid", true);
                    return resp;
                }
            }
        }
        resp.put("isPaid", false);
        return resp;
    }

    public List<Orders> getOrdersByAgentEmailId(String emailId) {
        return ordersDAO.getOrdersByAgentEmailId(emailId);
    }

    public List<Orders> getOrdersByListOfAgentEmailId(List<String> emailIds) {
        return ordersDAO.getOrdersByListOfAgentEmailId(emailIds);
    }

    public PlatformBasicResponse updateOrders(UpdatesForOrdersRequest updateOrders) throws VException {
        List<UpdatesForOrdersRequestPOJO> updateOrdersForFOSAgent = updateOrders.getUpdateOrders();
        Set<String> orderIds = new HashSet<>();
        List<String> agentEmailIds = new ArrayList<>();
        List<String> agentTeamleadEmailIds = new ArrayList<>();

        for (UpdatesForOrdersRequestPOJO ordersPojo : updateOrdersForFOSAgent) {
            if (StringUtils.isNotEmpty(ordersPojo.getId())) {
                orderIds.add(ordersPojo.getId());
            }
            if (StringUtils.isNotEmpty(ordersPojo.getAgentEmailId())) {
                agentEmailIds.add(ordersPojo.getAgentEmailId());
            }
            if (StringUtils.isNotEmpty(ordersPojo.getTeamLeadEmailId())) {
                agentTeamleadEmailIds.add(ordersPojo.getTeamLeadEmailId());
            }
        }

        agentTeamleadEmailIds.forEach(el -> {
            if (StringUtils.isNotEmpty(el)) {
                agentEmailIds.add(el);
            }
        });

        Map<String, Orders> ordersmap = new HashMap<>();
        Query query = new Query();
        if (orderIds != null) {
            query.addCriteria(Criteria.where(Orders.Constants._ID).in(orderIds));
        }
        query.fields().include(Orders.Constants._ID)
                .include(Orders.Constants.PAYMENT_THROUGH)
                .include(Orders.Constants.TEAM_TYPE)
                .include(Orders.Constants.AGENT_EMAIL_ID)
                .include(Orders.Constants.TEAM_LEAD_EMAIL_ID)
                .include(Orders.Constants.CENTRE)
                .include(Orders.Constants.ENROLLMENT_TYPE)
                .include(Orders.Constants.FINTECH_RISK)
                .include(Orders.Constants.REQUEST_REFUND)
                .include(Orders.Constants.NET_COLLECTION)
                .include(Orders.Constants.NOTES)
                .include(Orders.Constants.AGENT_CODE)
                .include(Orders.Constants.MANUALLY_CHANGED_LOGS)
                .include(Orders.Constants.REPORT_TEAMLEAD_ID);
        List<Orders> orders = ordersDAO.getOrders(query);
        orders.forEach(o -> ordersmap.put(o.getId(), o));

        Map<String, List<String>> params = new HashMap<>();
        params.put("agentEmailIds", agentEmailIds);

        String json = gson.toJson(params);
        ClientResponse resp = WebUtils.INSTANCE.doCall(PLATFORM_ENDPOINT
                + "fosAgent/getFOSAgentsByEmails", HttpMethod.POST, json);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type fosAgentsList = new TypeToken<List<FOSAgentRes>>() {
        }.getType();
        List<FOSAgentRes> fosAgentInfoPojos = gson.fromJson(jsonString, fosAgentsList);
        Map<String, FOSAgentRes> fosAgentInfoPojoMap = new HashMap<>();
        fosAgentInfoPojos.forEach(fosAgentInfo -> fosAgentInfoPojoMap.put(fosAgentInfo.getAgentEmailId(), fosAgentInfo));

        for (String ordid : orderIds) {
            if (!ordersmap.containsKey(ordid)) {
                throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "No order found for order id " + ordid);
            }
        }

        for (String email : agentEmailIds) {
            if (!fosAgentInfoPojoMap.containsKey(email)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "AgentEmailId is not correct :" + email);
            }
        }

        for (String emailid : agentTeamleadEmailIds) {
            if (StringUtils.isNotEmpty(emailid) && !fosAgentInfoPojoMap.containsKey(emailid)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "AgentTeamLeadEmailId is not correct :" + emailid);
            }
        }

        for (UpdatesForOrdersRequestPOJO order : updateOrdersForFOSAgent) {
            Orders currentOrder = ordersmap.get(order.getId());
            Query query1 = new Query();
            if (order.getId() != null) {
                query1.addCriteria(Criteria.where(Orders.Constants._ID).is(order.getId()));
            }
            Update update = new Update();
            update.set(Orders.Constants.MANUALLY_CHANGED_LOGS, addChangedLogs(currentOrder, order, updateOrders));
            if (order.getPaymentThrough() != null) {
                update.set(Orders.Constants.PAYMENT_THROUGH, order.getPaymentThrough());
            }
            if (order.getTeamType() != null) {
                update.set(Orders.Constants.TEAM_TYPE, order.getTeamType());
            }
            if (order.getCentre() != null) {
                update.set(Orders.Constants.CENTRE, order.getCentre());
            }
            if (order.getEnrollmentType() != null) {
                update.set(Orders.Constants.ENROLLMENT_TYPE, order.getEnrollmentType());
            }
            if (order.getFintechRisk() != null) {
                update.set(Orders.Constants.FINTECH_RISK, order.getFintechRisk());
            }
            if (order.getAgentEmailId() != null) {
                update.set(Orders.Constants.AGENT_EMAIL_ID, order.getAgentEmailId());
            }
            if (order.getTeamLeadEmailId() != null) {
                update.set(Orders.Constants.TEAM_LEAD_EMAIL_ID, order.getTeamLeadEmailId());
            }
            if (order.getAgentCode() != null) {
                update.set(Orders.Constants.AGENT_CODE, order.getAgentCode());
            }
            if (order.getRequestRefund() != null) {
                update.set(Orders.Constants.REQUEST_REFUND, order.getRequestRefund());
            }
            update.set(Orders.Constants.NET_COLLECTION, order.getNetCollection());
            if (order.getNotes() != null) {
                update.set(Orders.Constants.NOTES, order.getNotes());
            }
            update.set(Orders.Constants.LAST_UPDATED, System.currentTimeMillis());
            update.set(Orders.Constants.LAST_UPDATED_BY, Long.toString(updateOrders.getCallingUserId()));
            if (order.getEmployeeId() != null) {
                update.set(Orders.Constants.EMPLOYEE_ID, order.getEmployeeId());
            }
            if (order.getAgentCode() != null) {
                update.set(Orders.Constants.AGENT_CODE, order.getAgentCode());
            }
            if (order.getReportingTeamLeadId() != null) {
                update.set(Orders.Constants.REPORT_TEAMLEAD_ID, order.getReportingTeamLeadId());
            }
            ordersDAO.updateFirst(query1, update, Orders.class);

        }
        return new PlatformBasicResponse();
    }

    private List<ManuallyChangedLogs> addChangedLogs(Orders currentOrder, UpdatesForOrdersRequestPOJO currentReq, UpdatesForOrdersRequest updateOrders) {
        logger.info("Inside changing the logs");
        List<ManuallyChangedLogs> currentLogs;
        if (currentOrder.getManuallyChangedLogs() == null) {
            currentLogs = new ArrayList<>();
        } else {
            currentLogs = currentOrder.getManuallyChangedLogs();
        }
        ManuallyChangedLogs currentLog = new ManuallyChangedLogs();
        boolean isChanged = false;
        if (currentReq.getPaymentThrough() != null) {
            if (currentOrder.getPaymentThrough() == null || !currentOrder.getPaymentThrough().equals(currentReq.getPaymentThrough())) {
                currentLog.setPaymentThrough(currentReq.getPaymentThrough());
                logger.info("Adding current payment through " + currentReq.getPaymentThrough());
                isChanged = true;
            }
        }
        if (currentReq.getTeamType() != null) {
            if (currentOrder.getTeamType() == null || !currentOrder.getTeamType().equals(currentReq.getTeamType())) {
                currentLog.setTeamType(currentReq.getTeamType());
                logger.info("Adding current team type " + currentReq.getTeamType());
                isChanged = true;
            }
        }
        if (currentReq.getAgentEmailId() != null) {
            if (currentOrder.getAgentEmailId() == null || !currentOrder.getAgentEmailId().equals(currentReq.getAgentEmailId())) {
                currentLog.setAgentEmailId(currentReq.getAgentEmailId());
                logger.info("Adding current agent email id " + currentReq.getAgentEmailId());
                isChanged = true;
            }
        }
        if (currentReq.getTeamLeadEmailId() != null) {
            if (currentOrder.getTeamLeadEmailId() == null || !currentOrder.getTeamLeadEmailId().equals(currentReq.getTeamLeadEmailId())) {
                currentLog.setTeamLeadEmailId(currentReq.getTeamLeadEmailId());
                logger.info("Adding team lead email id " + currentReq.getTeamLeadEmailId());
                isChanged = true;
            }
        }
        if (currentReq.getCentre() != null) {
            if (currentOrder.getCentre() == null || !currentOrder.getCentre().equals(currentReq.getCentre())) {
                currentLog.setCentre(currentReq.getCentre());
                logger.info("Adding centre " + currentLog.getCentre());
                isChanged = true;
            }
        }
        if (currentReq.getEnrollmentType() != null) {
            if (currentOrder.getEnrollmentType() == null || !currentOrder.getEnrollmentType().equals(currentReq.getEnrollmentType())) {
                currentLog.setEnrollmentType(currentReq.getEnrollmentType());
                logger.info("Adding enrollment type " + currentReq.getEnrollmentType());
                isChanged = true;
            }
        }
        if (currentReq.getFintechRisk() != null) {
            if (currentOrder.getFintechRisk() == null || !currentOrder.getFintechRisk().equals(currentReq.getFintechRisk())) {
                currentLog.setFintechRisk(currentReq.getFintechRisk());
                logger.info("Adding fintech risk  " + currentReq.getFintechRisk());
                isChanged = true;
            }
        }
        if (currentReq.getNetCollection() != 0) {
            if (currentOrder.getNetCollection() != currentReq.getNetCollection()) {
                currentLog.setNetCollection(currentReq.getNetCollection());
                logger.info("Changed net collection is " + currentReq.getNetCollection());
                isChanged = true;
            }
        }
        if (currentReq.getNotes() != null) {
            if (currentOrder.getNotes() == null || !currentOrder.getNotes().equals(currentReq.getNotes())) {
                currentLog.setNotes(currentReq.getNotes());
                logger.info("Changed notes is " + currentReq.getNotes());
                isChanged = true;
            }
        }
        if (currentReq.getAgentCode() != null) {
            if (currentOrder.getAgentCode() == null || !currentOrder.getAgentCode().equals(currentReq.getAgentCode())) {
                currentLog.setAgentCode(currentReq.getAgentCode());
                logger.info("Changed agent code is " + currentReq.getAgentCode());
                isChanged = true;
            }
        }
        if (currentReq.getReportingTeamLeadId() != null) {
            if (currentOrder.getReportingTeamLeadId() == null || !currentOrder.getReportingTeamLeadId().equals(currentReq.getReportingTeamLeadId())) {
                currentLog.setReportingTeamLeadId(currentReq.getReportingTeamLeadId());
                logger.info("Changed reporting team lead id is " + currentReq.getReportingTeamLeadId());
                isChanged = true;
            }
        }
        if (isChanged) {
            currentLog.setChangedTime(System.currentTimeMillis());
            currentLog.setChangedBy(updateOrders.getCallingUserId());
            currentLog.setRole(updateOrders.getCallingUserRole());
            currentLogs.add(currentLog);
        }
        return currentLogs;
    }

    public String markTrialOrderEnded(String entityId, EntityType entityType, String userId) throws InternalServerErrorException, BadRequestException, ConflictException {
        //TODO what if the regular order got created after trial???
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(Orders.Constants.ITEMS_ENTITY_TYPE).is(entityType));
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(Long.parseLong(userId)));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).is(PaymentStatus.PAID));
        query.limit(1);
        query.with(Sort.by(Sort.Direction.DESC, "creationTime"));
        List<Orders> orders = ordersDAO.getOrders(query);
        if (ArrayUtils.isNotEmpty(orders)) {
            Orders order = orders.get(0);
            order.setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus.FORFEITED, sessionUtils.getCallingUserId() != null ? sessionUtils.getCallingUserId().toString() : "SYSTEM");
//            order.setPaymentStatus(PaymentStatus.FORFEITED);
            ordersDAO.create(order);
            return order.getId();
        } else {
            return "";
        }
    }

    public List<PaymentOffer> getPaymentOffers(PaymentOfferReq paymentOfferReq) {
        return paymentOfferDao.getPaymentOffers(paymentOfferReq);
    }

    public PlatformBasicResponse createPaymentOffer(PaymentOffer paymentOffer) {
        paymentOfferDao.save(paymentOffer);
        return new PlatformBasicResponse();
    }

    public Map<String, Object> getPaymentMode() throws BadRequestException, InternalServerErrorException {
        Map<String, Object> map = new HashMap<>();
        String paymentMode = redisDAO.get("PAYMENT_MODE_IN_USE");
        map.put("payment_mode_in_use", paymentMode == null ? "DEFAULT" : paymentMode);
        return map;
    }

    private PaymentSource findPaymentSource(String userAgent) {

        PaymentSource paymentSource = PaymentSource.SOURCE_UNKNOWN;

        if (null != userAgent && StringUtils.isNotEmpty(userAgent)) {
            if (userAgent.contains(ANDROID) || userAgent.toUpperCase().contains(ANDROID.toUpperCase())) {
                if (userAgent.contains(ANDROID_BUILD) || userAgent.toUpperCase().contains(ANDROID_BUILD.toUpperCase())) {
                    if (userAgent.contains(ANDROID_SDK) || userAgent.toUpperCase().contains(ANDROID_SDK.toUpperCase())) {
                        paymentSource = PaymentSource.DESKTOP_APP;
                    } else {
                        paymentSource = PaymentSource.MOBILE_APP;
                    }
                } else {
                    paymentSource = PaymentSource.MOBILE_WEB;
                }
            } else if (userAgent.contains(IPHONE)) {
                paymentSource = PaymentSource.IOS_WEB;
            } else {
                paymentSource = PaymentSource.DESKTOP_WEB;
            }
        } else {
            paymentSource = PaymentSource.SOURCE_UNKNOWN;
        }
        return paymentSource;

    }

    public PaymentMessageStrip getPaymentMessageStrip() {
        String key1 = "STRIP_PAYMENT";
        String key2 = "PAYMENT_URL";
        String messageStrip = "";
        String messageStripUrl = "";

        try {
            messageStrip = redisDAO.get(key1);
            messageStripUrl = redisDAO.get(key2);
        } catch (Exception e) {
            logger.info("Could not get payment message from redis");
        }

        PaymentMessageStrip paymentMessageStrip = new PaymentMessageStrip();
        paymentMessageStrip.setMessageStrip(messageStrip);
        paymentMessageStrip.setMessageStripUrl(messageStripUrl);

        return paymentMessageStrip;
    }

    public Orders getLatestOrderByUserId(Long userId) throws VException {
        logger.info("userId : " + userId);
        return ordersDAO.getLatestOrderByUserId(userId);
    }
    public void updateBaseInstalment(List<BaseSubscriptionPaymentPlan> baseSubscriptionPaymentPlanReq) {
        baseSubscriptionPaymentPlanReq.forEach(baseSubscriptionPaymentPlan -> {
            baseSubscriptionPaymentPlanDAO.save(baseSubscriptionPaymentPlan);
        });
    }

    public BaseSubscriptionPaymentPlan getBaseSubscriptionPlan(String subscriptionId) {
        return baseSubscriptionPaymentPlanDAO.getById(subscriptionId );
    }


    public List<BaseSubscriptionPaymentPlan> getBaseSubscriptionPlanByBundles(List<String> bundleIds) {
        if(ArrayUtils.isEmpty(bundleIds)){
            return new ArrayList<>();
        }
        return baseSubscriptionPaymentPlanDAO.getByEntityId(bundleIds );
    }


    public void syncOrdersWithLS(List<String> orderIds) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants._ID).in(orderIds));
        List<Orders> orders = ordersDAO.runQuery(query, Orders.class);

        for(Orders order : orders){
            triggerOrderForLeadSquare(order, true);
        }
    }
    public void syncTransactionsWithLS(List<String> transactionIds) {
        List<Transaction> transactions = transactionDAO.getEntitiesForIds(transactionIds, Transaction.class);
        for(Transaction transaction : transactions){
            instalmentManager.triggerLeadSquareQueueForTransactionAsync(transaction, true);
        }
    }
    public List<EMICardResp>  getEMICards(Long amount) {
        List<EmiCard>  emiCards =  emiCardDao.getCardsByAmount(amount);

        Map<String , List<EmiCard> >  codeCardMap= Optional.ofNullable(emiCards)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .collect(Collectors.groupingBy(EmiCard::getCode));
        List<EMICardResp> emiCardResps = new ArrayList<>();
        for(List<EmiCard>  emiCardList : codeCardMap.values()){

            List<EMITenure> emiTenuresCredit = new ArrayList<>();
            List<EMITenure> emiTenuresDebit = new ArrayList<>();
            List<EMITenure> emiTenuresCardLess = new ArrayList<>();
            String name="";
            String code="";
            EmiCardType cardType = EmiCardType.CREDIT;
            for(EmiCard card      :emiCardList) {
                name = card.getBankName();
                cardType = card.getCardType();
                code=card.getCode();
                if (cardType.equals(EmiCardType.CREDIT)){
                    emiTenuresCredit.add(new EMITenure(card.getMonths(), card.getRate(),card.getId(), card.getMinimumAmount()));
                }
                else if (cardType.equals(EmiCardType.DEBIT)){
                    emiTenuresDebit.add(new EMITenure(card.getMonths(), card.getRate(),card.getId(), card.getMinimumAmount()));
                }
                else if (cardType.equals(EmiCardType.CARDLESS)){
                    emiTenuresCardLess.add(new EMITenure(card.getMonths(), card.getRate(),card.getId(), card.getMinimumAmount()));
                }
            }
            if (ArrayUtils.isNotEmpty(emiTenuresCredit)){
                emiCardResps.add(new EMICardResp(name ,code,EmiCardType.CREDIT,emiTenuresCredit));
            }
            if (ArrayUtils.isNotEmpty(emiTenuresDebit)){
                emiCardResps.add(new EMICardResp(name ,code,EmiCardType.DEBIT,emiTenuresDebit));
            }
            if (ArrayUtils.isNotEmpty(emiTenuresCardLess)){
                emiCardResps.add(new EMICardResp(name ,code,EmiCardType.CARDLESS,emiTenuresCardLess));
            }
        }


        return emiCardResps;

    }

    public void emiCardInsert(List<EmiCard> emiCards) {
        if (ArrayUtils.isNotEmpty(emiCards)){
            List<String> ids = emiCards.stream().filter(Objects::nonNull).map(EmiCard::getId).collect(Collectors.toList());
            logger.info(" fetching persist emi card...");
            List<EmiCard> emiCards1 = emiCardDao.getEmiCardByIds(ids);
            Map<String, EmiCard> map = emiCards1.stream().filter(nonNullAndEmpty).collect(Collectors.toMap(EmiCard::getId, Function.identity()));
            emiCards.stream().filter(nonNullAndEmpty).forEach(emiCard -> {
                if(StringUtils.isNotEmpty(emiCard.getId()) && map.containsKey(emiCard.getId())){
                    emiCard.setCreationTime(map.get(emiCard.getId()).getCreationTime());
                    emiCard.setCreatedBy(map.get(emiCard.getId()).getCreatedBy());
                }
            });
            emiCardDao.save(emiCards);
        }
    }

    public Orders getLatestTrialOrderByUserId(Long userId, Long startTime) {
        logger.info("userId : (), startTime: {}", userId, startTime);
        return ordersDAO.getLatestTrialOrderByUserId(userId, startTime);
    }

    public void exportOrdersCsvHandler(String message) throws UnsupportedEncodingException, VException {
        logger.info("message : " + message);
        if (StringUtils.isNotEmpty(message)) {
            ExportOrdersCsvReq exportOrdersCsvReq = new Gson().fromJson(message, ExportOrdersCsvReq.class);
            exportOrdersCsv(exportOrdersCsvReq);
        } else {
            logger.warn("empty message getting from EXPORT_ORDERS_CSV");
        }
    }

    public void exportOrdersCsv(ExportOrdersCsvReq req) throws UnsupportedEncodingException, VException {
        Integer start = 0, limit = 1000;
        while (true) {
            List<Orders> orders = ordersDAO.exportOrdersCsv(req.getPaymentStatus(), start, limit, req.getStartTime(), req.getEndTime(), req.getMinimumAmount(), req.getMaximumAmount());
            logger.info("start : " + start + " limit : " + limit + " orders : " + orders);
            if (orders != null) {
                Map<String, String> orderIdItemsNamesMap = getOrderIdItemNameMap(orders);
                Map<Long, UserBasicInfo> userMap = userIdUserInfoMap(orders);
                List<String> resultEntries = resultEntriesList(orders, userMap, orderIdItemsNamesMap);
                List<String> headers = Arrays.asList("OrderId", "userId", "email", "grade", "entityId", "entityTitle", "bookingValue", "downPayment", "enrollmentAgentCode", "manualChangedLongsAgentCode", "enrollmentDate");
                UserBasicInfo callingUser = fosUtils.getUserBasicInfo(req.getCallingUserId(), true);
                sendEmilaWithAttachmentData(req, headers, resultEntries, callingUser, orders);
            }
            if (orders == null || orders.size() != limit) {
                break;
            }
            start += limit;
        }
    }

    private Map<String, String> getOrderIdItemNameMap(List<Orders> orders) throws VException {
        logger.info("Orders count : " + orders.size());
        Map<String, String> map = new HashMap<>();
        if (ArrayUtils.isNotEmpty(orders)) {
            List<OrderedItemNames> orderedItemNames = new ArrayList<>();
            for (Orders order : orders) {
                if (StringUtils.isNotEmpty(order.getId()) && order.getItems() != null && StringUtils.isNotEmpty(order.getItems().get(0).getEntityId()) && order.getItems().get(0).getEntityType() != null) {
                    OrderedItemNames orderedItemName = new OrderedItemNames(order.getId(), order.getItems().get(0).getEntityId(), order.getItems().get(0).getEntityType(), "");
                    logger.info("orderedItemName : " + orderedItemName);
                    orderedItemNames.add(orderedItemName);
                }
            }
            if (ArrayUtils.isNotEmpty(orderedItemNames)) {
                //batchwise fetching
                List<List<OrderedItemNames>> itemNames = Lists.partition(orderedItemNames, 100);
                for (List<OrderedItemNames> items : itemNames) {
                    OrderedItemNameDetailsReq req = new OrderedItemNameDetailsReq(items);
                    logger.info("Calling Subscription for orderItems Title with req : " + req);
                    String url = subscriptionEndPoint + "subscriptionDetails/getOrderedItemNames";
                    ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(req));
                    VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                    String jsonString = resp.getEntity(String.class);
                    logger.info("jsonString : " + jsonString);
                    OrderedItemNameDetailsRes response = new Gson().fromJson(jsonString, OrderedItemNameDetailsRes.class);
                    logger.info("response : " + response);
                    if (response != null && ArrayUtils.isNotEmpty(response.getOrderedItemNames())) {
                        for (OrderedItemNames orderedItemNames1 : response.getOrderedItemNames()) {
                            logger.info("orderedItemNames1 : " + orderedItemNames1);
                            if (StringUtils.isNotEmpty(orderedItemNames1.getOrderId()) && StringUtils.isNotEmpty(orderedItemNames1.getEntityTitle())) {
                                map.put(orderedItemNames1.getOrderId(), orderedItemNames1.getEntityTitle());
                            }
                        }
                    }
                }
            }
        }
        logger.info("map : " + map);
        return map;
    }

    private Map<Long, UserBasicInfo> userIdUserInfoMap(List<Orders> orders) {
        Set<Long> userIds = new HashSet<>();
        for (Orders order : orders) {
            if (order.getUserId() != null) {
                userIds.add(order.getUserId());
            }
        }
        Map<Long, UserBasicInfo> userMap = new HashMap<>();
        if (userIds.size() > 0) userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
        return userMap;
    }

    private List<String> resultEntriesList(List<Orders> orders, Map<Long, UserBasicInfo> userMap, Map<String,String> orderIdItemsNamesMap) {
        List<String> exportEntries = new ArrayList<>();
        for (Orders order : orders) {
            List<String> resultEntries = new ArrayList<>();
            //orderId
            resultEntries.add(StringUtils.isNotEmpty(order.getId()) ? escapeCommas(order.getId()) : "");
            //userId
            //email
            //grade
            if (order.getUserId() != null && userMap.containsKey(order.getUserId())) {
                resultEntries.add(escapeCommas(Long.toString(order.getUserId())));
                resultEntries.add(StringUtils.isNotEmpty(userMap.get(order.getUserId()).getEmail()) ? escapeCommas(userMap.get(order.getUserId()).getEmail()) : "");
                resultEntries.add(StringUtils.isNotEmpty(userMap.get(order.getUserId()).getGrade()) ? escapeCommas(userMap.get(order.getUserId()).getGrade()) : "");
            } else {
                resultEntries.add("");
                resultEntries.add("");
                resultEntries.add("");
            }
            //entityId
            if (ArrayUtils.isNotEmpty(order.getItems())) {
                resultEntries.add((order.getItems().get(0) != null && StringUtils.isNotEmpty(order.getItems().get(0).getEntityId())) ? escapeCommas(order.getItems().get(0).getEntityId()) : "");
            } else {
                resultEntries.add("");
            }
            //entityTitle
            if(orderIdItemsNamesMap != null && StringUtils.isNotEmpty(order.getId()) && StringUtils.isNotEmpty(orderIdItemsNamesMap.get(order.getId()))){
                resultEntries.add(escapeCommas(orderIdItemsNamesMap.get(order.getId())));
            }else {
                resultEntries.add("");
            }
            //bookingValue
            resultEntries.add(order.getAmount() != null ? escapeCommas(Integer.toString(order.getAmount() / 100)) : "");
            //downPayment
            if(order.getPaymentType() == PaymentType.INSTALMENT){
                resultEntries.add(order.getAmountPaid() != null ? escapeCommas(Integer.toString(order.getAmountPaid() / 100)) : "");
            }else{
                resultEntries.add(order.getInitialFixedAmount() != null ? escapeCommas(Integer.toString(order.getInitialFixedAmount() / 100)) : "");
            }
            //enrollmentAgentCode
            resultEntries.add(StringUtils.isNotEmpty(order.getAgentCode()) ? escapeCommas(order.getAgentCode()) : "");
            //manualChangedLongsAgentCode
            if(ArrayUtils.isNotEmpty(order.getManuallyChangedLogs()) && StringUtils.isNotEmpty(order.getManuallyChangedLogs().get(0).getAgentCode())){
                resultEntries.add(escapeCommas(order.getManuallyChangedLogs().get(0).getAgentCode()));
            }else{
                resultEntries.add("");
            }
            //enrollmentDate
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss a");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            resultEntries.add(order.getCreationTime() != null ? sdf.format(new Date(order.getCreationTime())) : "");
            logger.info("resultEntries : " + resultEntries);
            exportEntries.add(String.join(",", resultEntries));
        }
        return exportEntries;
    }

    private String escapeCommas(String input) {
        if (StringUtils.isNotEmpty(input)) {
            input = input.replace(",", "-");
            input = input.replace("\n", " ");
            return input;
        } else {
            return "";
        }
    }

    private void sendEmilaWithAttachmentData(ExportOrdersCsvReq req, List<String> headers, List<String> entries, UserBasicInfo callingUser, List<Orders> orders) throws UnsupportedEncodingException, VException, UnsupportedEncodingException {
        logger.info("req : " + req + " headers : " + headers.size() + " entries : " + entries.size() + " calling user : " + callingUser + " orders : " + orders.size());
        if (req != null && ArrayUtils.isNotEmpty(headers) && ArrayUtils.isNotEmpty(entries) && callingUser != null && StringUtils.isNotEmpty(callingUser.getEmail()) && ArrayUtils.isNotEmpty(orders)) {

            //TODO: email validation for callingUser.getEmail()
            ArrayList<InternetAddress> toList = new ArrayList<>();
            toList.add(new InternetAddress(callingUser.getEmail(), "Admin"));

            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

            HashMap<String, Object> bodyScopes = new HashMap<>();
            if (orders.get(0) != null && orders.get(0).getCreationTime() != null) {
                bodyScopes.put("startDate", sdf.format(new Date(orders.get(0).getCreationTime())));
            } else {
                bodyScopes.put("startDate", sdf.format(new Date(req.getStartTime())));
            }
            if (orders.get(orders.size() - 1) != null && orders.get(orders.size() - 1).getCreationTime() != null) {
                bodyScopes.put("endDate", sdf.format(new Date(orders.get(orders.size() - 1).getCreationTime())));
            } else {
                bodyScopes.put("endDate", sdf.format(new Date(req.getEndTime())));
            }
            bodyScopes.put("linesCount", entries.size());

            HashMap<String, Object> subjectScopes = new HashMap<>();
            subjectScopes.put("startDate", sdf.format(new Date(req.getStartTime())));
            subjectScopes.put("endDate", sdf.format(new Date(req.getEndTime())));

            EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, CommunicationType.ORDERS_EXPORT_EMAIL, Role.ADMIN);

            String exportData = String.join(",", headers) + "\n" + String.join("\n", entries);
            EmailAttachment attachment = new EmailAttachment();
            attachment.setApplication("application/csv");
            attachment.setAttachmentData(exportData);
            attachment.setFileName("orders_" + sdf.format(new Date(System.currentTimeMillis())) + ".csv");
            request.setAttachment(attachment);

            communicationManager.sendEmailViaRest(request);
        }
    }

    public SubscriptionPlanInfoResp getBaseSubscriptionPlanByDeliverableId(String deliverableId) {
           String subscriptionId = "";
            String orderId = "";
            Long creationTime = 0L;
            List<Orders> orders = getOrdersByDeliverableIds(Arrays.asList(deliverableId));
            if(ArrayUtils.isEmpty(orders)){
                    return null;
            }

            for(Orders order : orders){
                 if(StringUtils.isNotEmpty(order.getSubscriptionPlanId())) {
                     subscriptionId = order.getSubscriptionPlanId();
                     orderId = order.getId();
                     creationTime = order.getCreationTime();
                 }
            }
            if(StringUtils.isNotEmpty(subscriptionId)) {
                BaseSubscriptionPaymentPlan  baseSubscriptionPaymentPlan = baseSubscriptionPaymentPlanDAO.getById(subscriptionId);
                SubscriptionPlanInfoResp  subscriptionPlanInfoResp = new SubscriptionPlanInfoResp(baseSubscriptionPaymentPlan.getValidMonths(),orderId,creationTime);
                return subscriptionPlanInfoResp;
            }else{
                return null;
            }
    }

}