package com.vedantu.dinero.managers.payment;

import com.razorpay.Customer;
import com.razorpay.Order;
import com.razorpay.Payment;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Token;
import com.sun.jersey.api.client.Client;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class RazorpayAPIManager {

    private static Client client = Client.create();

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RazorpayAPIManager.class);

    // Initialize client
    RazorpayClient razorpayClient = new RazorpayClient(ConfigUtils.INSTANCE.getStringValue("billing.razorpay.key_id")
            , ConfigUtils.INSTANCE.getStringValue("billing.razorpay.key_secret"));

    String OfferCodes =  ConfigUtils.INSTANCE.getStringValue("billing.razorpay.offers");

    public RazorpayAPIManager() throws RazorpayException {

    }

    public Payment getPaymentById(String paymentId) {
        try {
            return razorpayClient.Payments.fetch(paymentId);
        } catch (RazorpayException e) {
            // Handle Exception
            logger.error(e.getMessage());
        }
        return null;
    }

    public Payment fetchTokenByPaymentId(String paymentId) {
        return getPaymentById(paymentId);
    }

    public List<Token> fetchTokensByCustomer(String customerId) throws RazorpayException {
        return razorpayClient.Customers.fetchTokens(customerId);
    }

    public Token fetchTokenByCustomer(String customerId, String tokenId) throws RazorpayException {
        return razorpayClient.Customers.fetchToken(customerId, tokenId);
    }



    public Order createOrder(int amount, String transactionId) {
        return createOrder( amount,  transactionId, false);
    }

    /**
     * Explicit offers gets the offer codes in application.props and overrides any global offers in Dashboard.
     * @param amount
     * @param transactionId
     * @param explicitOffersEnabled
     * @return
     */
    public Order createOrder(int amount, String transactionId, Boolean explicitOffersEnabled) {
        try {
            JSONObject orderRequest = new JSONObject();
            orderRequest.put("amount", amount); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", transactionId);
            orderRequest.put("payment_capture", false);
            if(explicitOffersEnabled){
                List<String> offers = Arrays.asList(OfferCodes.split("\\s*,\\s*"));
                orderRequest.put("offers", new JSONArray(offers.toArray()));
            }

            return razorpayClient.Orders.create(orderRequest);
        } catch (RazorpayException e) {
            // Handle Exception
            logger.error(e.getMessage());
        }
        return null;
    }


    public Order createOrderForRecurringPayment(String transactionId, String customerId, String method) {
        return createOrderForRecurringPayment(transactionId, customerId, method, null);
    }

    public Order createOrderForRecurringPayment(String transactionId, String customerId, String method, Integer chargeAmount) {
        int amount;
        String auth_type = null;
        if (method.equals("emandate")) {
            amount = 0;
            auth_type = "netbanking";
        } else {
            amount = 100;
        }
        try {
            JSONObject orderRequest = new JSONObject();
            JSONObject token = new JSONObject();
            token.put("auth_type", auth_type);
            token.put("max_amount", 10000);
            if (chargeAmount != null) {
                token.put("first_payment_amount", chargeAmount);
            }
            orderRequest.put("amount", amount); // amount in the smallest currency unit
            orderRequest.put("currency", "INR");
            orderRequest.put("receipt", transactionId);
            orderRequest.put("customer_id", customerId);
            orderRequest.put("method", method);
            orderRequest.put("payment_capture", true);
            orderRequest.put("token", token);

            return razorpayClient.Orders.create(orderRequest);
        } catch (RazorpayException e) {
            // Handle Exception
            logger.error(e.getMessage());
        }
        return null;
    }


    public Payment captureTransaction(int amount, String paymentId) {
        try {
            JSONObject captureRequest = new JSONObject();
            captureRequest.put("amount", amount);
            captureRequest.put("currency", "INR");

            return razorpayClient.Payments.capture(paymentId, captureRequest);
        } catch (RazorpayException e) {
            // Handle Exception
            logger.error(e.getMessage());
        }
        return null;
    }

    public Customer createCustomer(String name, String email, String phoneNumber, String userId) {
        try {
            JSONObject customerObj = new JSONObject();
            customerObj.put("name", "");
            customerObj.put("email", "");
            customerObj.put("contact", "");
            customerObj.put("notes", "{\"userId\":" + userId + "}");
            customerObj.put("fail_existing", "0");

            return razorpayClient.Customers.create(customerObj);
        } catch (RazorpayException e) {
            // Handle Exception
            logger.error(e.getMessage());
        }
        return null;
    }

    public void createRecurringPayment() {
        //TODO: Add API to Razorpay SDK
    }


}
