package com.vedantu.dinero.managers.payment.juspay;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import in.juspay.exception.*;
import in.juspay.model.JuspayEntity;
import in.juspay.model.Payment;
import in.juspay.model.RequestOptions;

import java.util.Map;

public final class WalletPayment extends JuspayEntity {

    private WalletPayment() {

    }

    public static Payment create(Map<String, Object> params)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        return create(params, null);
    }

    public static Payment create(Map<String, Object> params, RequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        // We will always send it as json in SDK.
        params.put("format", "json");
        JsonObject response = makeServiceCall("/txns#WalletPayment", params, RequestMethod.POST, requestOptions);
        updatePaymentResponseStructure(response);
        return createEntityFromResponse(response, Payment.class);
    }

    // Restructuring the payment response. Removed unnecessary hierarchy in the response.
    private static void updatePaymentResponseStructure(JsonObject response) {
        JsonObject authResp = response.get("payment").getAsJsonObject().get("authentication").getAsJsonObject();
        response.add("method", authResp.get("method"));
        response.add("url", authResp.get("url"));
        if (response.get("method").getAsString().equals("POST")) {
            response.add("params", new JsonObject());
            for (Map.Entry<String, JsonElement> entry : authResp.get("params").getAsJsonObject().entrySet()) {
                response.get("params").getAsJsonObject().add(entry.getKey(), entry.getValue());
            }
        }
        response.remove("payment");
    }
}
