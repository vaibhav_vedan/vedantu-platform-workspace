package com.vedantu.dinero.managers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.Coupon;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.RedeemedCoupon;
import com.vedantu.dinero.enums.*;
import com.vedantu.dinero.enums.coupon.CouponTargetEntityType;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.dinero.managers.coupon.CouponValidatorFactory;
import com.vedantu.dinero.managers.coupon.ICouponValidator;
import com.vedantu.dinero.pojo.CouponTarget;
import com.vedantu.dinero.request.*;
import com.vedantu.dinero.response.ApplyCouponRes;
import com.vedantu.dinero.response.CheckCouponAvailibilityRes;
import com.vedantu.dinero.response.CouponInfoRes;
import com.vedantu.dinero.response.GetCouponsRes;
import com.vedantu.dinero.serializers.CouponDAO;
import com.vedantu.dinero.serializers.OrdersDAO;
import com.vedantu.dinero.serializers.RedeemedCouponDAO;
import com.vedantu.exception.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class CouponManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CouponManager.class);

    @Autowired
    private CouponDAO couponDAO;

    @Autowired
    private RedeemedCouponDAO redeemedCouponDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private CouponValidatorFactory couponValidatorFactory;

    @Autowired
    private OrdersDAO ordersDAO;

    public CouponManager() {
        super();
    }

    public CouponInfoRes createCoupon(
            CreateCouponReq req)
            throws ConflictException, ForbiddenException, InternalServerErrorException {
        logger.info("Entering " + req.toString());
        // adminAccessOnlyCheck(req);
        CouponInfoRes couponInfoRes = null;
        String[] couponCodes = req.getCode().split(",");
        Coupon coupon = null;
        for (String code : couponCodes) {
            // check if this coupon code is available
            coupon = couponDAO.getCouponByCode(code);
            if (coupon != null) {
                throw new ConflictException(ErrorCode.COUPON_CODE_ALREADY_USED,
                        "a coupon is already created with code[" + code + "]");
            }
            coupon = req.toCoupon(code);
            coupon.setSatisfyAtleast1Condition(req.isSatisfyAtleast1Condition());
            couponDAO.create(coupon);
        }
        couponInfoRes = new CouponInfoRes(coupon);

        logger.info("Exiting " + couponInfoRes.toString());
        return couponInfoRes;
    }

    public CheckCouponAvailibilityRes isCouponCodeAvailable(CheckCouponAvailibilityReq req) throws ForbiddenException {
        logger.info("Entering " + req.toString());
        // adminAccessOnlyCheck(req);

        Coupon coupon = couponDAO.getCouponByCode(req.getCode());
        CheckCouponAvailibilityRes res = new CheckCouponAvailibilityRes(coupon == null);
        logger.info("Exiting " + res.toString());
        return res;

    }

    public CouponInfoRes editCoupon(EditCouponReq req)
            throws ForbiddenException, NotFoundException, InternalServerErrorException, ConflictException, BadRequestException {
        logger.info("Entering " + req.toString());
        // adminAccessOnlyCheck(req);

        // check if this coupon code is available
        Coupon coupon = couponDAO.getCouponByCode(req.getCode());
        if (coupon == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "no coupon found for code[" + req.getCode() + "]");
        }
//        List<RedeemedCoupon> RedeemedCoupons = redeemedCouponDAO.getRedeemedCoupon(coupon.getCode(),null,null);
//        if(ArrayUtils.isNotEmpty(RedeemedCoupons)){
//            throw new BadRequestException(ErrorCode.NOT_ALLOWED_TO_EDIT_COUPON, "coupon code already in used state won't be allowed to edit");
//        }
        coupon.setDescription(req.getDescription());
        coupon.setMaxRedeemValue(req.getMaxRedeemValue());
        coupon.setMinOrderValue(req.getMinOrderValue());
        coupon.setRedeemType(req.getRedeemType());
        coupon.setRedeemValue(req.getRedeemValue());
        coupon.setTerms(req.getTerms());
        coupon.setType(req.getType());
        coupon.setUsesCount(req.getUsesCount());
        coupon.setUsesCountPerUser(req.getUsesCountPerUser());
        coupon.setValidFrom(req.getValidFrom());
        coupon.setValidTill(req.getValidTill());
        coupon.setTargetUserId(req.getTargetUserId());
        coupon.setTargets(req.getTargets());
        coupon.setPassDurationInMillis(req.getPassDurationInMillis());
        coupon.setSatisfyAtleast1Condition(req.isSatisfyAtleast1Condition());
        coupon.setForPaymentType(req.getForPaymentType());
        coupon.setTargetOrder(req.getTargetOrder());
        coupon.setMaxOrderValue(req.getMaxOrderValue());
        couponDAO.create(coupon);
        CouponInfoRes couponInfoRes = new CouponInfoRes(coupon);
        logger.info("Exiting " + couponInfoRes.toString());
        return couponInfoRes;
    }

    public GetCouponsRes getCoupons(GetCouponsReq req) throws ForbiddenException, InternalServerErrorException {
        logger.info("Entering " + req.toString());
        // adminAccessOnlyCheck(req);
        GetCouponsRes res = new GetCouponsRes();
        if (StringUtils.isNotEmpty(req.getCode())) {
            Coupon coupon = couponDAO.getCouponByCode(req.getCode().trim().toUpperCase());
            //TODO populate infos for targets
            if (coupon != null) {
                CouponInfoRes couponInfoRes = new CouponInfoRes(coupon);
                if (couponInfoRes.getTargetUserId() != null && couponInfoRes.getTargetUserId() > 0) {
                    UserBasicInfo targetUser = fosUtils.getUserBasicInfo(couponInfoRes.getTargetUserId().toString(), false);
                    couponInfoRes.setTargetUser(targetUser);
                }
                res.addItem(couponInfoRes);
            }
        } else {

            List<Coupon> coupons = couponDAO.getCoupons(req.getStart(), req.getSize());
            logger.info(coupons.toString());
            //TODO remove getuserinfo from loop
            for (Coupon coupon : coupons) {
                //TODO populate infos
                CouponInfoRes couponInfoRes = new CouponInfoRes(coupon);
                if (couponInfoRes.getTargetUserId() != null && couponInfoRes.getTargetUserId() > 0) {
                    UserBasicInfo targetUser = fosUtils.getUserBasicInfo(couponInfoRes.getTargetUserId().toString(), false);
                    couponInfoRes.setTargetUser(targetUser);
                }
                res.addItem(couponInfoRes);
            }
        }

        if (res.getList().size() > 0) {
            addCouponUsesCount(res.getList());
        }
        logger.info("Exiting ", res.toString());
        return res;
    }

    public GetCouponsRes getPublicCoupons(GetCouponsReq req) throws ForbiddenException, InternalServerErrorException, BadRequestException {
        logger.info("Entering " + req.toString());
        if (StringUtils.isEmpty(req.getTarget())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "target not given");
        }
        GetCouponsRes res = new GetCouponsRes();
        List<Coupon> coupons = couponDAO.getPublicCoupons(req.getTarget(), req.getStart(), req.getSize());
        logger.info(coupons.toString());
        //TODO remove getuserinfo from loop
        for (Coupon coupon : coupons) {
            //TODO populate infos
            CouponInfoRes couponInfoRes = new CouponInfoRes(coupon);
            res.addItem(couponInfoRes);
        }
        logger.info("Exiting ", res.toString());
        return res;
    }

    public ApplyCouponRes applyCreditCoupon(ApplyCouponReq req) throws VException {
        logger.info("Entering " + req.toString());
        Object result = processCoupon(req.getCode(), req.getCallingUserId(), CouponType.CREDIT);
        ApplyCouponRes res = new ApplyCouponRes(result != null);
        logger.info("Exiting " + res.toString());
        return res;
    }

    public Object processCoupon(String couponCode, Long userId, CouponType couponTypeTarget)
            throws VException {
        logger.info("Entering couponCode:" + couponCode + " userId:" + userId);
        Coupon coupon = couponDAO.getCouponByCode(couponCode);
        if (coupon == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Invalid coupon");
        }

        validateCoupon(coupon, userId, null, null, couponTypeTarget, null, null, null, null,null);
        Object object = couponDAO.applyCoupon(coupon, userId);
        return object;
    }

    public ValidateCouponRes checkValidity(ValidateCouponReq req)
            throws NotFoundException, ConflictException, ForbiddenException, BadRequestException, VException {
        logger.info("Entering " + req.toString());
        String couponCode = req.getCode();
        Coupon coupon = couponDAO.getCouponByCode(couponCode);
        if (coupon == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Invalid coupon");
        }

        if (EntityType.COURSE_PLAN.equals(req.getEntityType())
                || EntityType.OTF.equals(req.getEntityType())) {
            ICouponValidator validator = getCouponValidator(req.getEntityType());
            req.setTotalAmount(validator.getTotalAmount(req.getEntityId()));
        }

        Integer discount =
                validateAndCalculateDiscount(coupon, req.getCallingUserId(),
                req.getEntityId(), req.getEntityType(), coupon.getType(), req.getTotalAmount(), null,
                req.getForPaymentType(), coupon.getTargetOrder(),req.getSubscriptionPlanId());

        ValidateCouponRes res = new ValidateCouponRes(true, coupon.getRedeemType(), coupon.getRedeemValue(), discount,
                coupon.getMaxRedeemValue(), coupon.getId(), coupon.getTargets(), coupon.getTargetUserId(), coupon.getType(), coupon.getPassDurationInMillis());

        logger.info("Exiting " + res.toString());
        return res;
    }

    public Integer validateAndCalculateDiscountForCouponCode(String couponCode, Long userId, String purchasingEntityId,
            EntityType purchasingEntityType, CouponType couponTypeTarget, Integer totalAmount,
            String purchaseFlowId, PaymentType paymentType)
            throws VException {
        Coupon coupon = couponDAO.getCouponByCode(couponCode);
        if (coupon == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Invalid coupon");
        }
        return validateAndCalculateDiscount(coupon, userId, purchasingEntityId,
                purchasingEntityType, couponTypeTarget, totalAmount, purchaseFlowId,
                paymentType, coupon.getTargetOrder(),null);
    }

    public Integer validateAndCalculateDiscount(String couponId, Long userId, String purchasingEntityId,
            EntityType purchasingEntityType, CouponType couponTypeTarget, Integer totalAmount,
            String purchaseFlowId, PaymentType paymentType,String subscriptionPlanId)
            throws VException {
        Coupon coupon = couponDAO.getById(couponId);
        if (coupon == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "Invalid coupon");
        }
        return validateAndCalculateDiscount(coupon, userId, purchasingEntityId,
                purchasingEntityType, couponTypeTarget, totalAmount, purchaseFlowId,
                paymentType, coupon.getTargetOrder(),subscriptionPlanId);
    }

    public Integer validateAndCalculateDiscount(Coupon coupon, Long userId, String purchasingEntityId,
                                                EntityType purchasingEntityType, CouponType couponTypeTarget, Integer totalAmount,
                                                String purchaseFlowId, PaymentType paymentType, List<Integer> targetOrder,String subscriptionPlanId)
            throws VException {
        validateCoupon(coupon, userId, purchasingEntityId, purchasingEntityType, couponTypeTarget,
                totalAmount, purchaseFlowId, paymentType, targetOrder,subscriptionPlanId);
        Integer discount = coupon.getType().getRedeemValue(coupon, totalAmount);

        if (totalAmount != null && discount != null && discount > totalAmount) {
            throw new ConflictException(ErrorCode.DISCOUNT_EXCEEDS_TOTAL_AMOUNT,
                    "Discount is exceeding purchase amount");
        }
        return discount;
    }

    private void validateCoupon(Coupon coupon, Long userId, String purchasingEntityId,
                                EntityType purchasingEntityType, CouponType couponTypeTarget, Integer totalAmount,
                                String purchaseFlowId, PaymentType paymentType, List<Integer> targetOrder,String subscriptionPlanId)
            throws VException {

        logger.info("ENTRY coupon " + coupon + ", userId " + userId + ", purchasingEntityId "
                + purchasingEntityId + ", purchasingEntityType " + purchasingEntityType
                + ", couponTypeTarget " + couponTypeTarget
                + ", totalAmount " + totalAmount + ", purchaseFlowId " + purchaseFlowId
                + ", PaymentType " + paymentType);

        if (StringUtils.isNotEmpty(purchaseFlowId)) {
            //checking if the coupon was locked under this context
            logger.info("Checking if any coupon is already locked for the purchase flowId " + purchaseFlowId);
            List<RedeemedCoupon> redeemedCoupons = redeemedCouponDAO.getRedeemedCoupon(coupon.getCode(), purchaseFlowId, null);
            if (ArrayUtils.isNotEmpty(redeemedCoupons)) {
                logger.info("EXIT, locked coupon found for purchaseFlowId " + purchaseFlowId);
                return;
            }
        }

        Long cTime = System.currentTimeMillis();
        if (!coupon.getType().equals(couponTypeTarget)) {
            throw new ConflictException(ErrorCode.COUPON_INVALID, "Invalid coupon");
        }

        if (coupon.getType().equals(CouponType.CREDIT) && purchasingEntityType != null) {
            throw new ConflictException(ErrorCode.COUPON_INVALID, "Invalid coupon");
        }

        // validFrom check
        if (coupon.getValidFrom() != null && coupon.getValidFrom() > 0 && coupon.getValidFrom() > cTime) {
            throw new ConflictException(ErrorCode.COUPON_CODE_NOT_ACTIVE,
                    "Invalid coupon");
        }

        // validTill check
        if (coupon.getValidTill() != null && coupon.getValidTill() > 0 && coupon.getValidTill() < cTime) {
            throw new ConflictException(ErrorCode.COUPON_CODE_EXPIRED,
                    "Coupon has expired");
        }

        // per user limit check
        if (coupon.getUsesCountPerUser() != null && coupon.getUsesCountPerUser() > 0) {
            int userCouponUsesCount = (int) redeemedCouponDAO.getUserCouponUsesCount(coupon.getCode(), userId, true);
            if (userCouponUsesCount >= coupon.getUsesCountPerUser()) {
                throw new ConflictException(ErrorCode.COUPON_USES_PER_USER_LIMIT_REACHED,
                        "Coupon usage limit has been reached");
            }
        }

        // over all limit check
        if (coupon.getUsesCount() != null && coupon.getUsesCount() > 0) {
            int couponUsesCount = (int) redeemedCouponDAO.getCouponUsesCount(coupon.getCode(), true);
            if (couponUsesCount >= coupon.getUsesCount()) {
                throw new ConflictException(ErrorCode.COUPON_USES_LIMIT_REACHED,
                        "Coupon usage limit has been reached");
            }
        }

        // targetusercheck
        if (coupon.getTargetUserId() != null && coupon.getTargetUserId() > 0
                && !coupon.getTargetUserId().equals(userId)) {
            throw new ConflictException(ErrorCode.COUPON_UNATHOURIZED_USAGE,
                    "Coupon is not available");
        }

        //change the logic when new payment types are introduced
        if (PaymentType.INSTALMENT.equals(coupon.getForPaymentType())
                && !PaymentType.INSTALMENT.equals(paymentType)) {
            throw new ConflictException(ErrorCode.PAYMENT_TYPE_NOT_ALLOWED_FOR_COUPON,
                    "Coupon not applicable on this transaction");
        }

        if (PaymentType.INSTALMENT.equals(paymentType)
                && !PaymentType.INSTALMENT.equals(coupon.getForPaymentType())) {
            throw new ConflictException(ErrorCode.PAYMENT_TYPE_NOT_ALLOWED_FOR_COUPON,
                    "Coupon not applicable on this transaction");
        }

        if (!EntityType.BUNDLE.equals(purchasingEntityType)
                && PaymentType.INSTALMENT.equals(paymentType)) {
            throw new ConflictException(ErrorCode.COUPON_UNATHOURIZED_USAGE,
                    "Coupon is not available");
        }

        if (ArrayUtils.isNotEmpty(targetOrder) && targetOrder.stream().allMatch(e -> e > 0)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(userId))
                    .addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS)
                            .nin(Arrays.asList(PaymentStatus.FAILED, PaymentStatus.CANCELLED,
                                    PaymentStatus.NOT_PAID, PaymentStatus.REVERTED)));
            long ordersCount = ordersDAO.queryCount(query, Orders.class);
            if (targetOrder.stream().noneMatch(e -> e - 1 == ordersCount)) {
                throw new ConflictException(ErrorCode.COUPON_TARGET_ORDER_MISMATCH, "Not valid on this order.");
            }
        }

        ICouponValidator couponValidator = getCouponValidator(purchasingEntityType);
        if (ArrayUtils.isNotEmpty(coupon.getTargets())) {
            if (couponValidator == null) {
                logger.info("Coupon validator not available for purchasing "
                        + purchasingEntityId + " with coupon " + coupon);
                throw new ForbiddenException(ErrorCode.COUPON_INVALID, "Coupon not applicable on this transaction");
            }
            int conditionsSatisfied = 0;
            List<String> allowedTargetStrs = new ArrayList<>();
            // targets check
            for (CouponTarget couponTarget : coupon.getTargets()) {
                CouponTargetEntityType allowedTarget = couponTarget.getTargetType();

                allowedTargetStrs.add(couponTarget.getTargetType().name());
                List<String> allowedStrs = couponTarget.getTargetStrs();
                boolean canBeValidated = allowedTarget.supports(couponValidator);
                if (canBeValidated) {
                    conditionsSatisfied++;
                    List<String> purchasingEntityStrs = new ArrayList<>();
                    if(CouponTargetEntityType.SUBSCRIPTION_PLAN.equals(allowedTarget)){
                        if(StringUtils.isNotEmpty(subscriptionPlanId)){
                            purchasingEntityStrs = allowedTarget.get(couponValidator, subscriptionPlanId);
                        }

                    }else {
                        purchasingEntityStrs = allowedTarget.get(couponValidator, purchasingEntityId);
                    }
                    logger.info("purchasingEntityStrs " + purchasingEntityStrs);
                    if (ArrayUtils.isNotEmpty(allowedStrs)) {
                        boolean found = false;
                        if (ArrayUtils.isNotEmpty(purchasingEntityStrs)) {
                            for (String purchasingEntityStr : purchasingEntityStrs) {
                                if (allowedStrs.indexOf(purchasingEntityStr) > -1) {
                                    found = true;
                                    break;
                                }
                            }
                        }
                        if (!found) {
                            throw new ForbiddenException(ErrorCode.COUPON_NOT_APPLICABLE,
                                    /*"Coupon of allowed target " + allowedTarget.name()
                                    + " with " + allowedStrs + " not applicable for purchasing entity "
                                    + purchasingEntityType + "," + purchasingEntityId
                                    + " with " + allowedTarget + " values: "
                                    + purchasingEntityStrs*/
                                    "Coupon not applicable on this transaction");
                        }
                    }

                    if(null != couponTarget && null != couponTarget.getTargetType() && couponTarget.getTargetType().equals(CouponTargetEntityType.EARLY_LEARNING)){
                        List<String> bundleCategoryList = couponValidator.getCategories(purchasingEntityId);
                        if(null != bundleCategoryList && !bundleCategoryList.contains("EARLY_LEARNING")){
                            throw new ForbiddenException(ErrorCode.COUPON_NOT_APPLICABLE,
                                    /*"Coupon of allowed target " + allowedTarget.name()
                                    + " with " + allowedStrs + " not applicable for purchasing entity "
                                    + purchasingEntityType + "," + purchasingEntityId
                                    + " with " + allowedTarget + " values: "
                                    + purchasingEntityStrs*/
                                    "Coupon not applicable on this transaction");
                        }
                    }
                }
            }

            if (!((conditionsSatisfied == coupon.getTargets().size())
                    || (conditionsSatisfied > 0 && coupon.isSatisfyAtleast1Condition()))) {
                throw new ForbiddenException(ErrorCode.COUPON_NOT_APPLICABLE,
                        /*"Coupon of targets " + org.apache.commons.lang3.ArrayUtils.toString(allowedTargetStrs)
                        + " not applicable for " + purchasingEntityType*/
                        "Not applicable on this transaction");
            }

        }
        // min order value check
        if (null != totalAmount && null != coupon.getMinOrderValue() && coupon.getMinOrderValue() > 0
                && totalAmount < coupon.getMinOrderValue()) {
            throw new ConflictException(ErrorCode.MIN_ORDER_NOT_SATISFIED,
                    "Coupon is valid only for order value above " + (coupon.getMinOrderValue()/ 100) + " Rs")  ;
        }
        // max order value check
        if (null != totalAmount && null != coupon.getMaxOrderValue() && coupon.getMaxOrderValue() > 0
                && totalAmount > coupon.getMaxOrderValue()) {
            throw new ConflictException(ErrorCode.MAX_ORDER_NOT_SATISFIED,
                    "Coupon is valid only for order value upto " + (coupon.getMaxOrderValue() / 100) + " Rs") ;
        }

        if (RedeemType.FLAT_PRICE == coupon.getRedeemType() && totalAmount != null &&
                coupon.getRedeemValue() != null && coupon.getRedeemValue() > totalAmount) {
            throw new ConflictException(ErrorCode.FLAT_PRICE_NOT_APPLICABLE,
                    "Coupon not applicable on this transaction");
        }

        logger.info("EXIT");
    }

    private void addCouponUsesCount(List<CouponInfoRes> coupons) {
        List<String> codes = new ArrayList<>();
        for (CouponInfoRes cRes : coupons) {
            codes.add(cRes.getCode());
        }
        List<String> results = redeemedCouponDAO.getUsedCodes(codes);
        codes.clear();
        for (String result : results) {
            if (!codes.contains(result)) {
                codes.add(result);
            }

        }
        Map<String, Integer> couponUsesCountMap = redeemedCouponDAO.getUsesCount(codes);
        for (CouponInfoRes cRes : coupons) {
            Integer count = couponUsesCountMap.get(cRes.getCode());
            cRes.setTotalUsesCount(count == null ? 0 : count);
        }

    }

    public RedeemedCoupon addRedeemEntry(AddCouponRedeemEntryReq req) {
        if (StringUtils.isNotEmpty(req.getPassCode())) {
            req.setPassCode(req.getPassCode().trim().toUpperCase());
        }
        RedeemedCoupon redeemedCoupon = new RedeemedCoupon(req.getPassCode(), req.getUserId(), null, req.getEntityId(),
                req.getEntityType(), req.getState());
        redeemedCoupon.setPassProcessingState(req.getPassProcessingState());
        redeemedCoupon.setPassExpirationTime(req.getPassExpirationTime());
        redeemedCoupon.setReferenceTags(req.getReferenceTags());
        redeemedCouponDAO.create(redeemedCoupon);
        return redeemedCoupon;
    }

    public void addRedeemEntry(String couponId, Long userId, Integer redeemValue,
            String entityId, EntityType entityType, RedeemedCouponState state) {
        addRedeemEntry(couponId, userId, redeemValue, entityId, entityType, state, null, null);
    }

    public void addRedeemEntry(String couponId, Long userId, Integer redeemValue,
            String entityId, EntityType entityType, RedeemedCouponState state,
            PurchaseFlowType purchaseFlowType, String purchaseFlowId) {
        logger.info("ENTRY couponId " + couponId
                + ", userId " + userId
                + ", redeemValue " + redeemValue
                + ", entityId " + entityId
                + ", entityType " + entityType
                + ", state " + state
                + ", purchaseFlowType " + purchaseFlowType
                + ", purchaseFlowId " + purchaseFlowId);
        Coupon coupon = couponDAO.getById(couponId);
        if (coupon != null) {
            addRedeemEntryForCouponCode(coupon.getCode(), userId, redeemValue, entityId, entityType,
                    state, purchaseFlowType, purchaseFlowId);
        } else {
            logger.error("Error in adding redeem entry for " + couponId + ", userId " + userId);
        }
    }

    public void addRedeemEntryForCouponCode(String couponCode, Long userId, Integer redeemValue,
            String entityId, EntityType entityType, RedeemedCouponState state,
            PurchaseFlowType purchaseFlowType, String purchaseFlowId) {
        RedeemedCoupon redeemedCoupon = new RedeemedCoupon(couponCode, userId, redeemValue, entityId,
                entityType, state);
        redeemedCoupon.setPurchaseFlowType(purchaseFlowType);
        redeemedCoupon.setPurchaseFlowId(purchaseFlowId);
        redeemedCouponDAO.create(redeemedCoupon);
    }

    public void updateRedeemEntry(String couponId, String purchaseFlowId, Integer redeemValue,
            String entityId, EntityType entityType, RedeemedCouponState oldState,
            RedeemedCouponState newState) throws ConflictException {
        logger.info("ENTRY couponId " + couponId + " purchaseFlowId " + purchaseFlowId
                + ", oldState " + oldState + ", newState " + newState);
        Coupon coupon = couponDAO.getById(couponId);
        if (coupon != null) {
            List<RedeemedCoupon> redeemedCoupons = redeemedCouponDAO.getRedeemedCoupon(coupon.getCode(), purchaseFlowId, oldState);
            if (redeemedCoupons == null || redeemedCoupons.isEmpty() || redeemedCoupons.size() > 1) {
                logger.error("Error in updateRedeemEntry for " + couponId);
                logger.info(redeemedCoupons);
                throw new ConflictException(ErrorCode.INCONSISTENT_REDEEMED_ENTRIES,
                        "INCONSISTENT_REDEEMED_ENTRIES for couponId " + couponId
                        + " and purchaseFlowId " + purchaseFlowId);
            }
            RedeemedCoupon redeemedCoupon = redeemedCoupons.get(0);
            redeemedCoupon.setState(newState);
            redeemedCoupon.setRedeemValue(redeemValue);
            redeemedCoupon.setEntityType(entityType);
            redeemedCoupon.setEntityId(entityId);
            redeemedCouponDAO.create(redeemedCoupon);
        } else {
            logger.error("Error in updateRedeemEntry for " + couponId + ", purchaseFlowId " + purchaseFlowId);
        }
    }

    public List<RedeemedCoupon> unlockRedeemedCoupons(String purchaseFlowId) {
        logger.info("ENTRY purchaseFlowId " + purchaseFlowId);
        List<RedeemedCoupon> redeemedCoupons = redeemedCouponDAO.getRedeemedCoupon(null, purchaseFlowId, null);
        if (redeemedCoupons != null) {
            for (RedeemedCoupon redeemedCoupon : redeemedCoupons) {
                if (redeemedCoupon != null) {
                    redeemedCoupon.setState(RedeemedCouponState.UNUSED);
                    redeemedCouponDAO.create(redeemedCoupon);
                }
            }
        }
        logger.info("EXIT redeemedCoupons " + redeemedCoupons);
        return redeemedCoupons;
    }

    private ICouponValidator getCouponValidator(EntityType entityType) {
        ICouponValidator couponValidator = null;
        if (null != entityType) {
            switch (entityType) {
                case PLAN:
                    couponValidator = couponValidatorFactory.getPlanCouponValidator();
                    break;
                case OTF:
                    couponValidator = couponValidatorFactory.getBatchCouponValidator();
                    break;
                case OTF_BATCH_REGISTRATION:
                    couponValidator = couponValidatorFactory.getBatchRegCouponValidator();
                    break;
                case OTF_COURSE_REGISTRATION:
                    couponValidator = couponValidatorFactory.getCourseRegCouponValidator();
                    break;
                case COURSE_PLAN:
                    couponValidator = couponValidatorFactory.getCoursePlanCouponValidator();
                    break;
                case COURSE_PLAN_REGISTRATION:
                    couponValidator = couponValidatorFactory.getCoursePlanRegCouponValidator();
                    break;
                case BUNDLE:
                    couponValidator = couponValidatorFactory.getBundleCouponValidator();
                    break;
                case BUNDLE_PACKAGE:
                    couponValidator = couponValidatorFactory.getBundlePackageCouponValidator();
                    break;
                case OTM_BUNDLE_REGISTRATION:
                    couponValidator = couponValidatorFactory.getOTMBundleRegCouponValidator();
                    break;
                case OTM_BUNDLE_ADVANCE_PAYMENT:
                    couponValidator = couponValidatorFactory.getOTMBundleCouponValidator();
                    break;
                case OTM_BUNDLE:
                    couponValidator = couponValidatorFactory.getOTMBundleCouponValidator();
                    break;
                default:
                    break;
            }
        }
        return couponValidator;
    }

    public List<RedeemedCoupon> getPassRedeemInfo(GetPassRedeemInfoReq req) {
        return redeemedCouponDAO.getPassRedeemInfo(req);
    }

    public PlatformBasicResponse markPassProcessingState(MarkPassProcessingStateReq req) throws NotFoundException {

        Query query = new Query();
        query.addCriteria(Criteria.where(RedeemedCoupon.Constants._ID).is(req.getId()));
        Update update = new Update();
        update.set(RedeemedCoupon.Constants.PASS_PROCESSING_STATE, req.getPassProcessingState());
        int numberOfDocuments = redeemedCouponDAO.updateFirst(query, update);
        if (numberOfDocuments == 0) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "not found");
        }
        return new PlatformBasicResponse();
    }

    public RedeemedCoupon getRedeemCouponInfoByRefId(String refId) {
        return redeemedCouponDAO.getRedeemedCouponByRefId(refId);
    }

    public List<Coupon> getCouponByIds(List<String> ids) {
        logger.info(ids + " " + ids.size());
        return couponDAO.getByIds(ids);
    }
}
