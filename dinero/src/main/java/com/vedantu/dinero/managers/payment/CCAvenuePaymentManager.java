package com.vedantu.dinero.managers.payment;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Maps;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.payment.ccavenue.AesCryptUtil;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.logger.LoggingMarkers;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

@Service
public class CCAvenuePaymentManager implements IPaymentManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CCAvenuePaymentManager.class);

    @Autowired
    public FosUtils fosUtils;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public AccountDAO accountDAO;

    public static final String PAYMENT_CHANNEL = "CCAvenue";
    private static final String FIELD_MERCHANT_ID = "merchant_id";
    private static final String FIELD_ACCESS_CODE = "access_code";
    private static final String MERCHANT_PARAM_VALUE_SAPERATOR = ",";
    private static final Gson gson = new Gson();

    private String CHARGING_URL;
    private String STATUS_URL;
    private String MERCHANT_ID;
    private String access_code;
    private String enc_key;
    private String redirect_url;

    private final PaymentGatewayName gatewayName = PaymentGatewayName.CCAVENUE;

    public CCAvenuePaymentManager() {
        super();
        CHARGING_URL = ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.charging.url");
        STATUS_URL = ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.txn.status.url");
        MERCHANT_ID = ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.merchant_id");
        access_code = ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.access_code");
        enc_key = ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.enc_key");
        redirect_url = ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.redirect_url");
    }

    @Override
    public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) {

        Map<String, Object> httpParams = new HashMap<String, Object>();
        // required params
        httpParams.put("order_id", transaction.getId());
        httpParams.put("currency", "INR");
        // as the amount is in paisa
        float amountValue = transaction.getAmount() / 100;

        httpParams.put("amount", amountValue);

        UserBasicInfo user = fosUtils.getUserBasicInfo(transaction.getUserId().toString(), true);
        String name = user.getFullName();

        httpParams.put("billing_name", name);
        httpParams.put("billing_email", user.getEmail());
        httpParams.put("billing_address", ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.address.default"));
        httpParams.put("billing_city", ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.city.default"));
        httpParams.put("billing_state", ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.state.default"));
        httpParams.put("billing_zip", ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.zip.default"));
        httpParams.put("billing_country", ConfigUtils.INSTANCE.getStringValue("billing.ccavenue.country.default"));
        httpParams.put("billing_tel", "1800-120-456-456");

        httpParams.put("delivery_name", name);
        httpParams.put("integration_type", "iframe_normal");
        httpParams.put("language", "EN");
        httpParams.put(FIELD_MERCHANT_ID, MERCHANT_ID);

        httpParams.put("redirect_url", redirect_url);
        httpParams.put("cancel_url", redirect_url);

        // optional params
        httpParams.put("merchant_param1", "userId#" + user.getUserId());
        String url = getPaymentUrl(httpParams);
        return new RechargeUrlInfo(PaymentGatewayName.CCAVENUE, url, "POST", null);
    }

    @SuppressWarnings("unchecked")
    @Override
    public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId)
            throws NotFoundException, InternalServerErrorException, ConflictException, BadRequestException {

        // VERIFY THE RESPONSE FROM SERVER
        verifyRequest(transactionInfo, callingUserId);

        logger.info(transactionInfo.toString());
        String encResp;
        List<String> list = new ArrayList<String>();

        if (transactionInfo.get("encResp") == null) {
            encResp = null;
        } else {
            list = (List<String>) transactionInfo.get("encResp");
            encResp = list.get(0);
        }
//			encResp = ((List<String>) transactionInfo.get("encResp")).get(0);
        if (StringUtils.isEmpty(encResp)) {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");

        }
        logger.info("encResp: " + encResp);
        AesCryptUtil aesUtil = new AesCryptUtil(enc_key);

        String responseParams = aesUtil.decrypt(encResp);
        logger.info("responseParams: " + responseParams);

        Map<String, Object> resParamMap = StringUtils.toKeyValueMap(responseParams, "&", "=");

        logger.info("response params : " + resParamMap);
        String transactionId = (String) resParamMap.get("order_id");

        String paymentChannelTransactionId = (String) resParamMap.get("tracking_id");

        String orderStatus = (String) resParamMap.get("order_status");

        TransactionStatus transactionStatus = null;

		if (orderStatus.equalsIgnoreCase("Success")) {
			transactionStatus = TransactionStatus.SUCCESS;
		} else if (orderStatus.equalsIgnoreCase("Aborted")) {
			transactionStatus = TransactionStatus.CANCELLED;
		} else {
			transactionStatus = TransactionStatus.FAILED;
		}

        String paymentMethod = (String) resParamMap.get("payment_mode");

        String paymentInstrument = (String) resParamMap.get("card_name");

        String bankRefNo = (String) resParamMap.get("bank_ref_no");

        String amount = (String) resParamMap.get("amount");

        int amountPaid = (int) (Float.parseFloat(amount) * 100);

        if (transactionStatus != TransactionStatus.SUCCESS) {
            amountPaid = 0;
        }

        Map<String, Object> merchantParamMap = StringUtils.toKeyValueMap((String) resParamMap.get("merchant_param1"),
                "#", MERCHANT_PARAM_VALUE_SAPERATOR);

        logger.info("merchantParamMap : " + merchantParamMap);
        logger.info("transaction status : " + transactionStatus);

        ExtTransaction extTransaction = null;
        logger.info("starting transaction ");
        extTransaction = extTransactionDAO.getById(transactionId);
        logger.info("extTransaction before updation : " + extTransaction);

        // Save output to DB
        extTransaction.setGatewayResponse(resParamMap.toString());

        // ExtTransactionDAO.INSTANCE
        // .getById(transactionId);

        if (extTransaction.getStatus() != TransactionStatus.PENDING) {

            throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
                    "transaction with id " + transactionId + " is already processed");
        }
        extTransaction.setStatus(transactionStatus);
        extTransaction.setGatewayStatus(orderStatus);
        extTransaction.setTransactionTime(String.valueOf(System.currentTimeMillis()));
        extTransaction.setPaymentChannelTransactionId(paymentChannelTransactionId);
        extTransaction.setPaymentInstrument(paymentInstrument);
        extTransaction.setPaymentMethod(paymentMethod);
        extTransaction.setBankRefNo(bankRefNo);
        logger.info("saving extTransaction: " + extTransaction);
        extTransactionDAO.create(extTransaction, callingUserId);
        if (extTransaction.getAmount().intValue() == amountPaid && transactionStatus == TransactionStatus.SUCCESS) {
            // TODO: these functionality can be moved to abstract class
            // transaction = TransactionDAO.INSTANCE
            // .upsertTransaction(transaction);

            Account account = null;
            if (transactionStatus == TransactionStatus.SUCCESS) {
                // TODO: take this code to common place, where update and
                // deduct
                // operation can be done
                logger.info("transaction was successful:" + extTransaction);
                account = accountDAO
                        .getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
                if (account == null) {
                    logger.error("no account found for userid:" + extTransaction.getUserId());

                    throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                            "no account found for userId:" + extTransaction.getUserId());
                }
                logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

                account.setBalance(account.getBalance() + amountPaid);
                account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

                logger.info("new account balance : " + account.getBalance());

                accountDAO.updateWithoutSession(account, null);
                
                com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                        amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                        BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                        TransactionRefType.EXTERNAL_TRANSACTION, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
                transactionDAO.create(vedantuTransaction);

                logger.info("Account " + account.toString());
            }
        }
        return extTransaction;
    }

    private Map<String, Object> getStatus(String referenceNo, String orderNo) {
        Map<String, String> params = ImmutableMap.<String, String>builder()
                .put("reference_no", referenceNo)
                .put("order_no", orderNo).build();
        String statusTracker = getStatusUrl(params);
        logger.info( "STATUS TRACKER " + statusTracker);
        ClientResponse response = WebUtils.INSTANCE.doCall(statusTracker, HttpMethod.POST, null);
        if (response.getStatus() == HttpStatus.OK.value()) {
            String entity = response.getEntity(String.class);
            if (StringUtils.isNotEmpty(entity) && entity.contains("status=0")) {
                String[] entityParams = entity.split("&");
                for (String entityParam : entityParams) {
                    if (entityParam.contains("enc_response")) {
                        String hexCipherText = entityParam.split("=")[1].trim();
                        String responseParams = null;
                        try {
                            AesCryptUtil aesUtil = new AesCryptUtil(enc_key);
                            responseParams = aesUtil.decrypt(hexCipherText);
                        } catch (Exception e) {
                            logger.error(e.getMessage(), e);
                        }
                        Map<String, Object> o = gson.fromJson(responseParams, new TypeToken<Map<String, Object>>() {
                        }.getType());
                        return o;
                    }
                }
            }
        }
        return Maps.newHashMap();
    }

    private String getStatusUrl(Map<String, String> httpParams) {
        logger.info("payment channel [" + PAYMENT_CHANNEL + "] getPaymentUrl: " + httpParams);
        StringBuilder sb = new StringBuilder();
        sb.append(STATUS_URL);
        sb.append("?command=").append("orderStatusTracker");
        sb.append("&request_type=JSON");
        sb.append("&version=1.1");
        String encryptionReq = gson.toJson(httpParams);
        sb.append("&");
        sb.append("enc_request");
        sb.append("=");
        AesCryptUtil aesUtil = new AesCryptUtil(enc_key);
        String encRequest = aesUtil.encrypt(encryptionReq);
        sb.append(encRequest);
        sb.append("&");
        sb.append(FIELD_ACCESS_CODE);
        sb.append("=");
        sb.append(access_code);

        String url = sb.toString();
        logger.info(LoggingMarkers.JSON_MASK, "payment channel [" + PAYMENT_CHANNEL + "] payment url :  " + url);
        return url;
    }

    private String getPaymentUrl(Map<String, Object> httpParams) {
        logger.info("payment channel [" + PAYMENT_CHANNEL + "] getPaymentUrl: " + httpParams);
        StringBuilder sb = new StringBuilder();
        sb.append(CHARGING_URL);
        sb.append("?command=").append("initiateTransaction");
        sb.append("&");
        sb.append(FIELD_MERCHANT_ID);
        sb.append("=");
        sb.append(MERCHANT_ID);

        StringBuilder encReqBuilder = new StringBuilder();
        for (Entry<String, Object> entry : httpParams.entrySet()) {
            encReqBuilder.append(entry.getKey());
            encReqBuilder.append("=");
            encReqBuilder.append(entry.getValue());
            encReqBuilder.append("&");
        }
        sb.append("&");
        sb.append("encRequest");
        sb.append("=");
        AesCryptUtil aesUtil = new AesCryptUtil(enc_key);
        String encRequest = aesUtil.encrypt(encReqBuilder.toString());
        sb.append(encRequest);
        sb.append("&");
        sb.append(FIELD_ACCESS_CODE);
        sb.append("=");
        sb.append(access_code);

        String url = sb.toString();
        logger.info(LoggingMarkers.JSON_MASK, "payment channel [" + PAYMENT_CHANNEL + "] payment url :  " + url);
        return url;
    }

    public void checkGatewayStatus(long fromTime) throws ConflictException, InternalServerErrorException, NotFoundException {
        long start = 0, limit = 100;
        List<ExtTransaction> transactions = new ArrayList<>();
        do {
            for (ExtTransaction transaction : transactions) {
                try {
                    Map<String, Object> status = getStatus(transaction.getPaymentChannelTransactionId(), transaction.getId());
                    if ("Successful".equalsIgnoreCase(String.valueOf(status.get("order_status")))) {
                        status.put("successType", ExtTransaction.MarkSuccessType.PAYMENT_GATEWAY_STATUS_CALLBACK);
                        updateExtTransaction(status, transaction);
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
            transactions = extTransactionDAO.getExtTransactionsToCheckStatus(gatewayName, "Awaited", TransactionStatus.FAILED,
                    fromTime, start, (int) limit);
            start += limit;
            logger.info("TRANSACTIONS " + transactions.size());

        } while (ArrayUtils.isNotEmpty(transactions));
    }

    private void updateExtTransaction(Map<String, Object> map, ExtTransaction transaction) throws InternalServerErrorException, NotFoundException, ConflictException {

        List<ExtTransaction> extTransactions = extTransactionDAO.getByParentTxnId(transaction.getId(), TransactionStatus.SUCCESS);
        if (ArrayUtils.isNotEmpty(extTransactions)) {
            logger.warn("Transaction already processed");
            return;
        }

        ExtTransaction childTxn = gson.fromJson(gson.toJson(transaction), ExtTransaction.class);
        childTxn.setId(null);
        childTxn.setStatus(TransactionStatus.PENDING);
        childTxn.setCreationTime(System.currentTimeMillis());
        childTxn.setLastUpdated(System.currentTimeMillis());
        childTxn.setCreatedBy("SYSTEM");
        childTxn.setLastUpdatedBy("SYSTEM");

        childTxn.setParentTransactionId(transaction.getId());
        childTxn.setBankResponseMessage(gson.toJson(map));
        childTxn.setMarkSuccessType(ExtTransaction.MarkSuccessType.PAYMENT_GATEWAY_STATUS_CALLBACK);
        String orderStatus = String.valueOf(map.get("order_status"));

        TransactionStatus transactionStatus;
        if ("Successful".equalsIgnoreCase(orderStatus)) {
            transactionStatus = TransactionStatus.SUCCESS;
        } else {
            transactionStatus = TransactionStatus.FAILED;
        }

        logger.info("MAP " + map);
        String amount = String.valueOf(map.get("order_amt"));

        int amountPaid = (int) (Float.parseFloat(amount) * 100);

        if (transactionStatus != TransactionStatus.SUCCESS) {
            amountPaid = 0;
        }

        logger.info("transaction status : " + transactionStatus);
        logger.info("extTransaction before updation : " + childTxn);


        // ExtTransactionDAO.INSTANCE
        // .getById(transactionId);

        if (childTxn.getStatus() != TransactionStatus.PENDING) {
            throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
                    "transaction with id " + transaction.getId() + " is already processed");
        }
        childTxn.setStatus(transactionStatus);
        childTxn.setGatewayStatus(orderStatus);
        childTxn.setTransactionTime(String.valueOf(System.currentTimeMillis()));
        logger.info("saving extTransaction: " + childTxn);

        extTransactionDAO.create(childTxn, null);
        if (childTxn.getAmount() == amountPaid && transactionStatus == TransactionStatus.SUCCESS) {
            // TODO: these functionality can be moved to abstract class
            // transaction = TransactionDAO.INSTANCE
            // .upsertTransaction(transaction);

            Account account = null;
            if (transactionStatus == TransactionStatus.SUCCESS) {
                // TODO: take this code to common place, where update and
                // deduct
                // operation can be done
                logger.info("transaction was successful:" + childTxn);
                account = accountDAO
                        .getAccountByHolderId(Account.__getUserAccountHolderId(childTxn.getUserId().toString()));
                if (account == null) {
                    logger.error("no account found for userid:" + childTxn.getUserId());

                    throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                            "no account found for userId:" + childTxn.getUserId());
                }
                logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

                account.setBalance(account.getBalance() + amountPaid);
                account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

                logger.info("new account balance : " + account.getBalance());

                accountDAO.updateWithoutSession(account, null);
                
                com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                        amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                        BillingReasonType.RECHARGE.name(), childTxn.getId(),
                        TransactionRefType.EXTERNAL_TRANSACTION, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredByUser(childTxn.getUserId()));
                transactionDAO.create(vedantuTransaction);

                logger.info("Account " + account.toString());
            }
        }
        transaction.setGatewayStatus(orderStatus);
        extTransactionDAO.create(transaction, null);
    }

    @Override
    public PaymentGatewayName getName() {
        return this.gatewayName;
    }
}
