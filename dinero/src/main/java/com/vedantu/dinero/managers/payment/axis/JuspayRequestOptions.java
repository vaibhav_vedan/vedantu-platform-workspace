package com.vedantu.dinero.managers.payment.axis;

import in.juspay.model.JuspayEnvironment;

public class JuspayRequestOptions {
    private String apiKey;
    private String merchantId;
    private String apiVersion;
    private int connectTimeoutInMilliSeconds;
    private int readTimeoutInMilliSeconds;
    private String baseUrl;

    private JuspayRequestOptions(String apiKey, String merchantId, String baseUrl) {
        this.apiKey = apiKey ;
        this.merchantId = merchantId;
        this.baseUrl = baseUrl;
        connectTimeoutInMilliSeconds = JuspayEnvironment.getConnectTimeoutInMilliSeconds();
        readTimeoutInMilliSeconds = JuspayEnvironment.getReadTimeoutInMilliSeconds();
        apiVersion = JuspayEnvironment.API_VERSION;
    }

    public static JuspayRequestOptions createDefault(String apiKey, String merchantId, String baseUrl) {
        return new JuspayRequestOptions(apiKey, merchantId, baseUrl);
    }

    public String getApiKey() {
        return apiKey;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public JuspayRequestOptions withApiKey(String apiKey) {
        this.apiKey = apiKey;
        return this;
    }

    public int getConnectTimeoutInMilliSeconds() {
        return connectTimeoutInMilliSeconds;
    }

    public JuspayRequestOptions withConnectTimeout(int connectTimeout) {
        this.connectTimeoutInMilliSeconds = connectTimeout;
        return this;
    }

    public int getReadTimeoutInMilliSeconds() {
        return readTimeoutInMilliSeconds;
    }

    public JuspayRequestOptions withReadTimeout(int readTimeout) {
        this.readTimeoutInMilliSeconds = readTimeout;
        return this;
    }

    public String getApiVersion() {
        return apiVersion;
    }

    public JuspayRequestOptions withApiVersion(String version) {
        this.apiVersion = version;
        return this;
    }

    public String getBaseUrl() {
        return baseUrl;
    }
}
