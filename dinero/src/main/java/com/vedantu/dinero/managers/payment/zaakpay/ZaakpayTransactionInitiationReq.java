package com.vedantu.dinero.managers.payment.zaakpay;


public class ZaakpayTransactionInitiationReq{

	private String merchantIdentifier;
	private String orderId;
	private String returnUrl;
	private String buyerEmail;
	private String buyerFirstName;
	private String buyerLastName;
	private String buyerAddress;
	private String buyerCity;
	private String buyerState;
	private String buyerCountry;
	private String buyerPincode;
	private String buyerPhoneNumber;
	private Integer txnType=1;
	private Integer zpPayOption=1;
	private Integer mode = 1;
	private String currency = "INR";
	//Amount in paisa. Min 100.
	private Long amount;
	private String merchantIpAddress;
	//Transaction date in yyyy-MM-dd format
	
	private Integer purpose=0;
	//Text description of what you are selling. At least 1 product description is mandatory to show in the bill on payment page.
	private String productDescription;
	private String txnDate;
	//min and max numeric 1 digit.You must specify the purpose of the transaction
	private String product1Description;
	private String product2Description;
	private String product3Description;
	private String product4Description;
	//You may specify this only when buyer's address is different from shipping address.30 alphanumeric
	private String shipToAddress;
	//shipping address city. 30 alphabet, minimum 3
	private String shipToCity;
	//shipping address state
	private String shipToState;
	// shipping address country
	private String shipToCountry;
	//Shipping address pin/zip code. 2 to 12 digits.Can have Numbers,Spaces and Hyphens (-) only
	private String shipToPincode;
	//shipping address landline or mobile phone number numeric only, no dashes, no spaces
	private String shipToPhoneNumber;
	//max 30 alphanumeric characters,no special characters or dashes
	private String shipToFirstname;
	/*false : We show the full fledged version unconditionally.
	DETECT : We do detection of the userAgent of the browser from which the request is sent & route accordingly.
	true : We show the mobile page unconditionally.
	missing / not sent : Same as DETECT ( ie We do detection at our end )*/
	private String showMobile;
	//checksum is calculated on a string with parameters in same order as you post to zaakpay
	private String checksum;
	
	public ZaakpayTransactionInitiationReq() {
	}
	
	public String getMerchantIdentifier() {
		return merchantIdentifier;
	}
	public void setMerchantIdentifier(String merchantIdentifier) {
		this.merchantIdentifier = merchantIdentifier;
	}
	public String getOrderId() {
		return orderId;
	}
	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}
	public String getReturnUrl() {
		return returnUrl;
	}
	public void setReturnUrl(String returnUrl) {
		this.returnUrl = returnUrl;
	}
	public String getBuyerEmail() {
		return buyerEmail;
	}
	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	public String getBuyerFirstName() {
		return buyerFirstName;
	}
	public void setBuyerFirstName(String buyerFirstName) {
		this.buyerFirstName = buyerFirstName;
	}
	public String getBuyerLastName() {
		return buyerLastName;
	}
	public void setBuyerLastName(String buyerLastName) {
		this.buyerLastName = buyerLastName;
	}
	public String getBuyerAddress() {
		return buyerAddress;
	}
	public void setBuyerAddress(String buyerAddress) {
		this.buyerAddress = buyerAddress;
	}
	public String getBuyerCity() {
		return buyerCity;
	}
	public void setBuyerCity(String buyerCity) {
		this.buyerCity = buyerCity;
	}
	public String getBuyerState() {
		return buyerState;
	}
	public void setBuyerState(String buyerState) {
		this.buyerState = buyerState;
	}
	public String getBuyerCountry() {
		return buyerCountry;
	}
	public void setBuyerCountry(String buyerCountry) {
		this.buyerCountry = buyerCountry;
	}
	public String getBuyerPincode() {
		return buyerPincode;
	}
	public void setBuyerPincode(String buyerPincode) {
		this.buyerPincode = buyerPincode;
	}
	public String getBuyerPhoneNumber() {
		return buyerPhoneNumber;
	}
	public void setBuyerPhoneNumber(String buyerPhoneNumber) {
		this.buyerPhoneNumber = buyerPhoneNumber;
	}
	public Integer getTxnType() {
		return txnType;
	}
	public void setTxnType(Integer txnType) {
		this.txnType = txnType;
	}
	public Integer getZpPayOption() {
		return zpPayOption;
	}
	public void setZpPayOption(Integer zpPayOption) {
		this.zpPayOption = zpPayOption;
	}
	public Integer getMode() {
		return mode;
	}
	public void setMode(Integer mode) {
		this.mode = mode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public Long getAmount() {
		return amount;
	}
	public void setAmount(Long amount) {
		this.amount = amount;
	}
	public String getMerchantIpAddress() {
		return merchantIpAddress;
	}
	public void setMerchantIpAddress(String merchantIpAddress) {
		this.merchantIpAddress = merchantIpAddress;
	}
	public String getTxnDate() {
		return txnDate;
	}
	public void setTxnDate(String txnDate) {
		this.txnDate = txnDate;
	}
	public Integer getPurpose() {
		return purpose;
	}
	public void setPurpose(Integer purpose) {
		this.purpose = purpose;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	public String getProduct1Description() {
		return product1Description;
	}
	public void setProduct1Description(String product1Description) {
		this.product1Description = product1Description;
	}
	public String getProduct2Description() {
		return product2Description;
	}
	public void setProduct2Description(String product2Description) {
		this.product2Description = product2Description;
	}
	public String getProduct3Description() {
		return product3Description;
	}
	public void setProduct3Description(String product3Description) {
		this.product3Description = product3Description;
	}
	public String getProduct4Description() {
		return product4Description;
	}
	public void setProduct4Description(String product4Description) {
		this.product4Description = product4Description;
	}
	public String getShipToAddress() {
		return shipToAddress;
	}
	public void setShipToAddress(String shipToAddress) {
		this.shipToAddress = shipToAddress;
	}
	public String getShipToCity() {
		return shipToCity;
	}
	public void setShipToCity(String shipToCity) {
		this.shipToCity = shipToCity;
	}
	public String getShipToState() {
		return shipToState;
	}
	public void setShipToState(String shipToState) {
		this.shipToState = shipToState;
	}
	public String getShipToCountry() {
		return shipToCountry;
	}
	public void setShipToCountry(String shipToCountry) {
		this.shipToCountry = shipToCountry;
	}
	public String getShipToPincode() {
		return shipToPincode;
	}
	public void setShipToPincode(String shipToPincode) {
		this.shipToPincode = shipToPincode;
	}
	public String getShipToPhoneNumber() {
		return shipToPhoneNumber;
	}
	public void setShipToPhoneNumber(String shipToPhoneNumber) {
		this.shipToPhoneNumber = shipToPhoneNumber;
	}
	public String getShipToFirstname() {
		return shipToFirstname;
	}
	public void setShipToFirstname(String shipToFirstname) {
		this.shipToFirstname = shipToFirstname;
	}
	public String getShowMobile() {
		return showMobile;
	}
	public void setShowMobile(String showMobile) {
		this.showMobile = showMobile;
	}
	public String getChecksum() {
		return checksum;
	}
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}
	@Override
	public String toString() {
		return "merchantIdentifier="
				+ merchantIdentifier + ", orderId=" + orderId + ", returnUrl="
				+ returnUrl + ", buyerEmail=" + buyerEmail
				+ ", buyerFirstName=" + buyerFirstName + ", buyerAddress="
				+ buyerAddress + ", buyerCity=" + buyerCity + ", buyerState="
				+ buyerState + ", buyerCountry=" + buyerCountry
				+ ", buyerPincode=" + buyerPincode + ", buyerPhoneNumber="
				+ buyerPhoneNumber + ", txnType=" + txnType + ", zpPayOption="
				+ zpPayOption + ", mode=" + mode + ", currency=" + currency
				+ ", amount=" + amount + ", merchantIpAddress="
				+ merchantIpAddress + ", txnDate=" + txnDate + ", purpose="
				+ purpose + ", productDescription=" + productDescription
				+ ", product1Description=" + product1Description
				+ ", product2Description=" + product2Description
				+ ", product3Description=" + product3Description
				+ ", product4Description=" + product4Description
				+ ", shipToAddress=" + shipToAddress + ", shipToCity="
				+ shipToCity + ", shipToState=" + shipToState
				+ ", shipToCountry=" + shipToCountry + ", shipToPincode="
				+ shipToPincode + ", shipToPhoneNumber=" + shipToPhoneNumber
				+ ", shipToFirstname=" + shipToFirstname + ", showMobile="
				+ showMobile + ", checksum=" + checksum;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result
				+ ((buyerAddress == null) ? 0 : buyerAddress.hashCode());
		result = prime * result
				+ ((buyerCity == null) ? 0 : buyerCity.hashCode());
		result = prime * result
				+ ((buyerCountry == null) ? 0 : buyerCountry.hashCode());
		result = prime * result
				+ ((buyerEmail == null) ? 0 : buyerEmail.hashCode());
		result = prime * result
				+ ((buyerFirstName == null) ? 0 : buyerFirstName.hashCode());
		result = prime
				* result
				+ ((buyerPhoneNumber == null) ? 0 : buyerPhoneNumber.hashCode());
		result = prime * result
				+ ((buyerPincode == null) ? 0 : buyerPincode.hashCode());
		result = prime * result
				+ ((buyerState == null) ? 0 : buyerState.hashCode());
		result = prime * result
				+ ((checksum == null) ? 0 : checksum.hashCode());
		result = prime * result
				+ ((currency == null) ? 0 : currency.hashCode());
		result = prime
				* result
				+ ((merchantIdentifier == null) ? 0 : merchantIdentifier
						.hashCode());
		result = prime
				* result
				+ ((merchantIpAddress == null) ? 0 : merchantIpAddress
						.hashCode());
		result = prime * result + ((mode == null) ? 0 : mode.hashCode());
		result = prime * result + ((orderId == null) ? 0 : orderId.hashCode());
		result = prime
				* result
				+ ((product1Description == null) ? 0 : product1Description
						.hashCode());
		result = prime
				* result
				+ ((product2Description == null) ? 0 : product2Description
						.hashCode());
		result = prime
				* result
				+ ((product3Description == null) ? 0 : product3Description
						.hashCode());
		result = prime
				* result
				+ ((product4Description == null) ? 0 : product4Description
						.hashCode());
		result = prime
				* result
				+ ((productDescription == null) ? 0 : productDescription
						.hashCode());
		result = prime * result + ((purpose == null) ? 0 : purpose.hashCode());
		result = prime * result
				+ ((returnUrl == null) ? 0 : returnUrl.hashCode());
		result = prime * result
				+ ((shipToAddress == null) ? 0 : shipToAddress.hashCode());
		result = prime * result
				+ ((shipToCity == null) ? 0 : shipToCity.hashCode());
		result = prime * result
				+ ((shipToCountry == null) ? 0 : shipToCountry.hashCode());
		result = prime * result
				+ ((shipToFirstname == null) ? 0 : shipToFirstname.hashCode());
		result = prime
				* result
				+ ((shipToPhoneNumber == null) ? 0 : shipToPhoneNumber
						.hashCode());
		result = prime * result
				+ ((shipToPincode == null) ? 0 : shipToPincode.hashCode());
		result = prime * result
				+ ((shipToState == null) ? 0 : shipToState.hashCode());
		result = prime * result
				+ ((showMobile == null) ? 0 : showMobile.hashCode());
		result = prime * result + ((txnDate == null) ? 0 : txnDate.hashCode());
		result = prime * result + ((txnType == null) ? 0 : txnType.hashCode());
		result = prime * result
				+ ((zpPayOption == null) ? 0 : zpPayOption.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ZaakpayTransactionInitiationReq other = (ZaakpayTransactionInitiationReq) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (buyerAddress == null) {
			if (other.buyerAddress != null)
				return false;
		} else if (!buyerAddress.equals(other.buyerAddress))
			return false;
		if (buyerCity == null) {
			if (other.buyerCity != null)
				return false;
		} else if (!buyerCity.equals(other.buyerCity))
			return false;
		if (buyerCountry == null) {
			if (other.buyerCountry != null)
				return false;
		} else if (!buyerCountry.equals(other.buyerCountry))
			return false;
		if (buyerEmail == null) {
			if (other.buyerEmail != null)
				return false;
		} else if (!buyerEmail.equals(other.buyerEmail))
			return false;
		if (buyerFirstName == null) {
			if (other.buyerFirstName != null)
				return false;
		} else if (!buyerFirstName.equals(other.buyerFirstName))
			return false;
		if (buyerPhoneNumber == null) {
			if (other.buyerPhoneNumber != null)
				return false;
		} else if (!buyerPhoneNumber.equals(other.buyerPhoneNumber))
			return false;
		if (buyerPincode == null) {
			if (other.buyerPincode != null)
				return false;
		} else if (!buyerPincode.equals(other.buyerPincode))
			return false;
		if (buyerState == null) {
			if (other.buyerState != null)
				return false;
		} else if (!buyerState.equals(other.buyerState))
			return false;
		if (checksum == null) {
			if (other.checksum != null)
				return false;
		} else if (!checksum.equals(other.checksum))
			return false;
		if (currency == null) {
			if (other.currency != null)
				return false;
		} else if (!currency.equals(other.currency))
			return false;
		if (merchantIdentifier == null) {
			if (other.merchantIdentifier != null)
				return false;
		} else if (!merchantIdentifier.equals(other.merchantIdentifier))
			return false;
		if (merchantIpAddress == null) {
			if (other.merchantIpAddress != null)
				return false;
		} else if (!merchantIpAddress.equals(other.merchantIpAddress))
			return false;
		if (mode == null) {
			if (other.mode != null)
				return false;
		} else if (!mode.equals(other.mode))
			return false;
		if (orderId == null) {
			if (other.orderId != null)
				return false;
		} else if (!orderId.equals(other.orderId))
			return false;
		if (product1Description == null) {
			if (other.product1Description != null)
				return false;
		} else if (!product1Description.equals(other.product1Description))
			return false;
		if (product2Description == null) {
			if (other.product2Description != null)
				return false;
		} else if (!product2Description.equals(other.product2Description))
			return false;
		if (product3Description == null) {
			if (other.product3Description != null)
				return false;
		} else if (!product3Description.equals(other.product3Description))
			return false;
		if (product4Description == null) {
			if (other.product4Description != null)
				return false;
		} else if (!product4Description.equals(other.product4Description))
			return false;
		if (productDescription == null) {
			if (other.productDescription != null)
				return false;
		} else if (!productDescription.equals(other.productDescription))
			return false;
		if (purpose == null) {
			if (other.purpose != null)
				return false;
		} else if (!purpose.equals(other.purpose))
			return false;
		if (returnUrl == null) {
			if (other.returnUrl != null)
				return false;
		} else if (!returnUrl.equals(other.returnUrl))
			return false;
		if (shipToAddress == null) {
			if (other.shipToAddress != null)
				return false;
		} else if (!shipToAddress.equals(other.shipToAddress))
			return false;
		if (shipToCity == null) {
			if (other.shipToCity != null)
				return false;
		} else if (!shipToCity.equals(other.shipToCity))
			return false;
		if (shipToCountry == null) {
			if (other.shipToCountry != null)
				return false;
		} else if (!shipToCountry.equals(other.shipToCountry))
			return false;
		if (shipToFirstname == null) {
			if (other.shipToFirstname != null)
				return false;
		} else if (!shipToFirstname.equals(other.shipToFirstname))
			return false;
		if (shipToPhoneNumber == null) {
			if (other.shipToPhoneNumber != null)
				return false;
		} else if (!shipToPhoneNumber.equals(other.shipToPhoneNumber))
			return false;
		if (shipToPincode == null) {
			if (other.shipToPincode != null)
				return false;
		} else if (!shipToPincode.equals(other.shipToPincode))
			return false;
		if (shipToState == null) {
			if (other.shipToState != null)
				return false;
		} else if (!shipToState.equals(other.shipToState))
			return false;
		if (showMobile == null) {
			if (other.showMobile != null)
				return false;
		} else if (!showMobile.equals(other.showMobile))
			return false;
		if (txnDate == null) {
			if (other.txnDate != null)
				return false;
		} else if (!txnDate.equals(other.txnDate))
			return false;
		if (txnType == null) {
			if (other.txnType != null)
				return false;
		} else if (!txnType.equals(other.txnType))
			return false;
		if (zpPayOption == null) {
			if (other.zpPayOption != null)
				return false;
		} else if (!zpPayOption.equals(other.zpPayOption))
			return false;
		return true;
	}
}
