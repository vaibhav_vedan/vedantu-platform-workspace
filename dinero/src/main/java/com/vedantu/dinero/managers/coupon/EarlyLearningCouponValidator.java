package com.vedantu.dinero.managers.coupon;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class EarlyLearningCouponValidator extends AbstractCouponValidator {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EarlyLearningCouponValidator.class);

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    @Override
    public boolean supportsBundle() {
        return true;
    }

    public BundlePackageInfo getBundlePackageInfo(String id) {
        BundlePackageInfo bundleInfo = null;
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                    + "/bundlePackage/" + id, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            bundleInfo = new Gson().fromJson(jsonString, BundlePackageInfo.class);
        } catch (Exception e) {
            logger.error("Error in getBundleInfo " + id);
        }
        return bundleInfo;
    }
}
