package com.vedantu.dinero.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.EmiCard;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.entity.VedantuAccountAuth;
import com.vedantu.dinero.entity.VerificationCode;
import com.vedantu.dinero.entity.WalletStatus;
import com.vedantu.dinero.enums.CouponType;
import com.vedantu.dinero.enums.HolderType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.enums.TimeState;
import com.vedantu.dinero.enums.TransactionLevel;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.enums.TransactionType;
import com.vedantu.dinero.enums.VedantuAccount;
import com.vedantu.dinero.enums.VerificationCodeStatus;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.dinero.managers.payment.IPaymentManager;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.EmiInfo;
import com.vedantu.dinero.pojo.LockBalanceInfo;
import com.vedantu.dinero.pojo.LockBalanceRes;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.pojo.WalletStatusInfo;
import com.vedantu.dinero.request.AccountVerificationSMSRequest;
import com.vedantu.dinero.request.AddAccountPancardReq;
import com.vedantu.dinero.request.AddOrDeductAccountBalanceReq;
import com.vedantu.dinero.request.BulkTextSMSRequest;
import com.vedantu.dinero.request.GetAccountInfoReq;
import com.vedantu.dinero.request.GetAllExtTransactionsReq;
import com.vedantu.dinero.request.GetFreebiesAccountInfoReq;
import com.vedantu.dinero.request.GetRechargeHistoryReq;
import com.vedantu.dinero.request.GetTransactionsReq;
import com.vedantu.dinero.request.LockBalanceReq;
import com.vedantu.dinero.request.OTMConsumptionReq;
import com.vedantu.dinero.request.OTMRefundAdjustmentReq;
import com.vedantu.dinero.request.OTMRefundReq;
import com.vedantu.dinero.request.RechargeReq;
import com.vedantu.dinero.request.RefundMoneyToStudentWalletReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.request.RefundStudentWalletRequest;
import com.vedantu.dinero.request.SalesTransactionReq;
import com.vedantu.dinero.request.SetPaymentGatewayNameReq;
import com.vedantu.dinero.request.SubmitCashChequeReq;
import com.vedantu.dinero.request.SwitchAmountOneAccountToAnotherReq;
import com.vedantu.dinero.request.TransferFromFreebiesAccountReq;
import com.vedantu.dinero.request.TransferToVedantuAccountReq;
import com.vedantu.dinero.request.TransferToVedantuDefaultAccountReq;
import com.vedantu.dinero.response.GetAccountBalanceResponse;
import com.vedantu.dinero.response.GetAccountInfoRes;
import com.vedantu.dinero.response.GetAllExtTransactionsRes;
import com.vedantu.dinero.response.GetFreebiesAccountInfoRes;
import com.vedantu.dinero.response.GetPlanByIdResponse;
import com.vedantu.dinero.response.GetRechargeHistoryRes;
import com.vedantu.dinero.response.GetUserDashboardAccountInfoRes;
import com.vedantu.dinero.response.OTMConsumptionPaymentRes;
import com.vedantu.dinero.response.OTMRefundAdjustmentRes;
import com.vedantu.dinero.response.PaymentGatewayNameRes;
import com.vedantu.dinero.response.SalesTransactionInfo;
import com.vedantu.dinero.response.SubmitCashChequeRes;
import com.vedantu.dinero.response.TransactionInfoRes;
import com.vedantu.dinero.response.TransferFromFreebiesAccountRes;
import com.vedantu.dinero.response.TransferToVedantuDefaultAccountRes;
import com.vedantu.dinero.serializers.EmiCardDAO;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.OrdersDAO;
import com.vedantu.dinero.serializers.RefundDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.serializers.VedantuAccountAuthDAO;
import com.vedantu.dinero.serializers.VerificationCodeDAO;
import com.vedantu.dinero.serializers.WalletStatusDAO;
import com.vedantu.dinero.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.dinero.util.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.IPUtil;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.DevicesAllowed;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.codehaus.jettison.json.JSONException;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Random;
import java.util.function.Function;

@Service
public class AccountManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AccountManager.class);

    @Autowired
    public AccountDAO accountDAO;

    @Autowired
    EmiCardDAO emiCardDao;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    public OrdersDAO ordersDAO;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public RefundDAO refundDAO;

    @Autowired
    public PaymentManager paymentManager;

    @Autowired
    public PricingManager pricingManager;

    @Autowired
    public CouponManager couponManager;

    @Autowired
    public RefundManager refundManager;

    @Autowired
    public InstalmentManager instalmentManager;

    @Autowired
    public WalletStatusDAO walletStatusDAO;

    @Autowired
    public VerificationCodeDAO verificationCodeDAO;

    @Autowired
    public VedantuAccountAuthDAO vedantuAccountAuthDAO;

    @Autowired
    public SqlSessionFactory sqlSessionFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    HttpSessionUtils sessionUtils;

    private static String vedantuCutHolderId;
    private static String vedantuCutHolderName;
    private static String vedantuTechHolderId;
    private static String vedantuTechHolderName;
    private static String vedantuDefaultHolderId;
    private static String vedantuDefaultHolderName;
    private static String vedantuFreebiesHolderId;
    private static String vedantuFreebiesHolderName;
    private static String vedantuAIRHolderId;
    private static String vedantuAIRHolderName;

    public AccountManager() {
        vedantuCutHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuCutHolderId");
        vedantuCutHolderName = ConfigUtils.INSTANCE.getStringValue("account.vedantuCutHolderName");
        vedantuTechHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuTechHolderId");
        vedantuTechHolderName = ConfigUtils.INSTANCE.getStringValue("account.vedantuTechHolderName");
        vedantuDefaultHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuDefaultHolderId");
        vedantuDefaultHolderName = ConfigUtils.INSTANCE.getStringValue("account.vedantuDefaultHolderName");
        vedantuAIRHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuAIRHolderId");
        vedantuAIRHolderName = ConfigUtils.INSTANCE.getStringValue("account.vedantuAIRHolderName");
        vedantuFreebiesHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuFreebiesHolderId");
        vedantuFreebiesHolderName = ConfigUtils.INSTANCE.getStringValue("account.vedantuFreebiesHolderName");
    }

    // public Account createAccount(User user, Long callingUserId)
    // throws ForbiddenException, BadRequestException, NotFoundException,
    // ConflictException {
    // return createAccount(user, callingUserId, false);
    // }
    //
    // public Account createAccount(User user, Long callingUserId, boolean
    // shouldGiveSignUpCredit)
    // throws ForbiddenException, BadRequestException, NotFoundException,
    // ConflictException {
    // // create user account
    // Account account = new
    // Account(Account.__getUserAccountHolderId(user.getId().toString()));
    // accountDAO.create(account);
    // return account;
    // }

    public RechargeUrlInfo rechargeAccount(RechargeReq rechargeReq, DevicesAllowed device, PaymentType paymentType,
                                           String vedantuOrderId) throws InternalServerErrorException, ForbiddenException, BadRequestException, ConflictException {
        Long userId = rechargeReq.getUserId() == null ? rechargeReq.getCallingUserId() : rechargeReq.getUserId();
        return rechargeAccount(userId, rechargeReq.getIpAddress(),
                rechargeReq.getAmount(), vedantuOrderId, paymentType, rechargeReq.getRedirectUrl(),
                rechargeReq.getRefType(), rechargeReq.getRefId(), rechargeReq.getCallingUserId(), device, rechargeReq.getPaymentOption());
    }
    public RechargeUrlInfo rechargeAccount(Long userId, String ipAddress, int amount, String vedantuOrderId, PaymentType paymentType, String redirectUrl,
                                           TransactionRefType refType, String refId, Long callingUserId, DevicesAllowed device) throws InternalServerErrorException, ForbiddenException, BadRequestException, ConflictException {
        return rechargeAccount(userId, ipAddress, amount, vedantuOrderId, paymentType, redirectUrl,
                refType, refId, callingUserId,  device, null);
    }

    private RechargeUrlInfo rechargeAccount(Long userId, String ipAddress, /*
     * amount in paisa
     */
                                            int amount, String vedantuOrderId/*
     * if user is placing an order and doing the payment at the same time
     */, PaymentType paymentType, String redirectUrl,
                                            TransactionRefType refType, String refId, Long callingUserId, DevicesAllowed device, PaymentOption paymentOption)
            throws InternalServerErrorException, ForbiddenException, ConflictException, BadRequestException {

        logger.info("Entering " + userId.toString() + " amount:" + amount);

        User userInfo = fosUtils.getUserInfo(userId, true);

        if (Role.ADMIN.equals(sessionUtils.getCallingUserRole())) {
            if(StringUtils.isEmpty(userInfo.getContactNumber())){
                throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND, "contact number not found");
            }
        } else {
            if (StringUtils.isEmpty(userInfo.getContactNumber()) || !userInfo.getIsContactNumberVerified()) {
                throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND_OR_NOT_VERIFIED, "contact number not found or not verified");
            }
            if (StringUtils.isEmpty(userInfo.getEmail())) {
                throw new ConflictException(ErrorCode.EMAIL_NOT_FOUND, "email not found");
            }
        }


        if (amount < ConfigUtils.INSTANCE.getIntValue("payment.min.amount.paisa")) {
            throw new ConflictException(ErrorCode.INVALID_AMOUNT, "provided amount is less than min required amount");
        }
        float floatAmount = (float) amount / 100;
        floatAmount = Double.valueOf(Math.ceil(floatAmount)).floatValue();
        amount = (int) (floatAmount * 100);

        // IPaymentManager paymentManager = ZaakpayPaymentManager.INSTANCE;
        IPaymentManager ipaymentManager;
        String userPaymentGateway = redisDAO.get("PAYMENT_GATEWAY_" + userId);
        if (paymentOption != null && paymentOption.getOptedGateway() != null) {
            ipaymentManager = paymentManager.getPaymentManger(paymentOption.getOptedGateway());
        } else if (StringUtils.isNotEmpty(userPaymentGateway)) {
            ipaymentManager = paymentManager.getPaymentManger(PaymentGatewayName.valueOf(userPaymentGateway));
        } else if (DevicesAllowed.ANDROID.equals(device) || DevicesAllowed.IOS.equals(device)) {
            ipaymentManager = paymentManager.getPaymentManger(PaymentGatewayName.PAYU);
        }/* else if ( isInternationalPayment(userInfo, ipAddress) ) {
            ipaymentManager = paymentManager.getPaymentManger(PaymentGatewayName.AXIS);
        }*/ else {
            ipaymentManager = paymentManager.getPaymentManager(userId, amount);
        }
        // IPaymentManager paymentManager = CCAvenuePaymentManager.INSTANCE;
        ExtTransaction transaction = new ExtTransaction(userId, ipAddress, TransactionType.CREDIT,
                TransactionStatus.PENDING, amount, "INR", redirectUrl, ipaymentManager.getName());
        if (vedantuOrderId != null) {
            transaction.setVedantuOrderId(vedantuOrderId);
        }
        if (paymentType != null) {
            transaction.setPaymentType(paymentType);
        }

        if (refType != null) {
            transaction.setRefType(refType);
        }

        if (StringUtils.isNotEmpty(refId)) {
            transaction.setRefId(refId);
        }

        if (paymentOption != null && paymentOption.getPaymentInstrument() != null ) {
            transaction.setOptedPaymentMethodType(paymentOption.getPaymentInstrument().getOptedPaymentMethodType());
            if (paymentOption.getDevice() == null) {
                paymentOption.setDevice(device);
            }
        }

        if(paymentOption != null && StringUtils.isNotEmpty( paymentOption.getEmiId())){
            EmiCard emiCard =  emiCardDao.getById(paymentOption.getEmiId());

            if(emiCard!=null){
                transaction.setEmiInfo(new EmiInfo(emiCard.getCode(),emiCard.getMonths(),emiCard.getRate()));
//                Integer subventionAmount ;
//                                    float rateofIntrest =emiCard.getRate();
//                    Integer duration = emiCard.getMonths();
//                    float R = (float) rateofIntrest / (float) ( 100);
//                    int Total = transaction.getAmount();
//                    //https://razorpay.com/docs/offers/no-cost-emi/
//                    //Total = [P x R x (1+R)^N]/[(1+R)^N-1]
//                    if(R > 0) {
//                        int calculatedPrincipal = (int) (((Math.pow((1 + R / 12), duration) - 1) * Total / duration) / (R / 12 * Math.pow(1 + (R / 12), duration)));
//                        calculatedPrincipal =new BigDecimal(calculatedPrincipal).divide(new BigDecimal(100), RoundingMode.HALF_EVEN).multiply(new BigDecimal(100)).intValue();
//                        subventionAmount = Total - calculatedPrincipal;
//                        transaction.setAmount(calculatedPrincipal);
//                        transaction.setPreSubventionAmount(Total);
//                        transaction.setSubventionAmount(subventionAmount);
//
//                    }
            }
        }

        extTransactionDAO.create(transaction, callingUserId);

        RechargeUrlInfo rechargeUrlInfo;
        try {
            rechargeUrlInfo = ipaymentManager.getPaymentUrl(transaction, callingUserId, paymentOption);
        } catch (JSONException e) {
            throw new BadRequestException( ErrorCode.BAD_REQUEST_ERROR, "error in transaction " + e.getMessage());
        } catch (MalformedURLException e) {
            throw new BadRequestException( ErrorCode.BAD_REQUEST_ERROR, "error in transaction " + e.getMessage());
        }
        String paymentPageUrl = ConfigUtils.INSTANCE.getStringValue("payment.page.url");
        // String method = "POST";
        if (StringUtils.isNotEmpty(paymentPageUrl) && ipaymentManager.getName().equals(PaymentGatewayName.CCAVENUE)) {
//            String encodedUrl = Base64.encodeBase64URLSafeString(rechargeUrlInfo.getRechargeUrl().getBytes());
            String encodedUrl = Base64.getEncoder().encodeToString(rechargeUrlInfo.getRechargeUrl().getBytes());
            logger.info("final payment encodedUrl : " + encodedUrl);
            rechargeUrlInfo.setRechargeUrl(paymentPageUrl + "?url=" + encodedUrl);
            rechargeUrlInfo.setForwardHttpMethod("GET");
        }

        logger.info("Exiting " + rechargeUrlInfo.toString());

        return rechargeUrlInfo;
    }

    public SubmitCashChequeRes submitCashCheque(SubmitCashChequeReq submitCashChequeReq)
            throws ForbiddenException, InternalServerErrorException, BadRequestException, ConflictException {

        logger.info("Entering " + submitCashChequeReq.toString());
        Long userId = submitCashChequeReq.getCallingUserId();
        Long toUserId = submitCashChequeReq.getUserId();

        User userInfo = fosUtils.getUserInfo(toUserId, true);
        if (StringUtils.isEmpty(userInfo.getContactNumber()) || !userInfo.getIsContactNumberVerified()) {
            throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND_OR_NOT_VERIFIED, "contact number not found or not verified");
        }

        if(StringUtils.isEmpty(userInfo.getEmail())){
            throw new ConflictException(ErrorCode.EMAIL_NOT_FOUND, "email not found");
        }

        Integer amount = submitCashChequeReq.getAmount();
        TransactionRefType transactionRefType = TransactionRefType.CASH_CHEQUE;
        BillingReasonType reasonType = BillingReasonType.RECHARGE;
        String reasonRefNo = submitCashChequeReq.getReasonRefNo();
        String reasonNote = submitCashChequeReq.getReasonNote();

        Account toUserAccount = accountDAO.getAccountByUserId(toUserId);
        
        UserBasicInfo user = fosUtils.getUserBasicInfo(userId.toString(), false);

        if (user == null || Role.TEACHER.equals(user.getRole())
                || (!toUserId.equals(userId) && !Role.ADMIN.equals(user.getRole()))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Insufficient User Role");
        }

        toUserAccount.setBalance(toUserAccount.getBalance() + amount);
        toUserAccount.setNonPromotionalBalance(toUserAccount.getNonPromotionalBalance() + amount);
        accountDAO.updateWithoutSession(toUserAccount, null);

        Transaction transaction = new Transaction(amount, 0, amount, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null,
                toUserAccount.getId().toString(), toUserAccount.getBalance(), reasonType.name(), reasonRefNo,
                transactionRefType, reasonNote, Transaction._getTriggredByUser(userId));
        transaction.setReasonNoteType(submitCashChequeReq.getReasonNoteType());
        transaction.setReasonRefSubType(submitCashChequeReq.getReasonRefSubType());
        transaction.setLoanType(submitCashChequeReq.getLoanType());
        transactionDAO.create(transaction);
        paymentManager.createPaymentInvoice(transaction, toUserId);
        SubmitCashChequeRes res = new SubmitCashChequeRes(toUserAccount);

        logger.info("Exiting " + res.toString());

        return res;
    }

    public TransferFromFreebiesAccountRes transferFromFreebiesAccount(
            TransferFromFreebiesAccountReq transferFromFreebiesAccountReq) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {

        logger.info("Entering " + transferFromFreebiesAccountReq.toString());
        Long userId = transferFromFreebiesAccountReq.getCallingUserId();
        Long toUserId = transferFromFreebiesAccountReq.getUserId();

        User userInfo = fosUtils.getUserInfo(toUserId, true);
        if (StringUtils.isEmpty(userInfo.getContactNumber()) || !userInfo.getIsContactNumberVerified()) {
            throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND_OR_NOT_VERIFIED, "contact number not found or not verified");
        }

        if(StringUtils.isEmpty(userInfo.getEmail())){
            throw new ConflictException(ErrorCode.EMAIL_NOT_FOUND, "email not found");
        }

        Integer amount = transferFromFreebiesAccountReq.getAmount();
        TransactionRefType transactionRefType = transferFromFreebiesAccountReq.getTransactionRefType();
        BillingReasonType reasonType = transferFromFreebiesAccountReq.getBillingReasonType();
        String reasonRefNo = transferFromFreebiesAccountReq.getReasonRefNo();
        Boolean allowNegativeBlance = transferFromFreebiesAccountReq.getAllowNegativeBalance();

        String reasonNote = transferFromFreebiesAccountReq.getReasonNote();
        Account toUserAccount = accountDAO.getAccountByUserId(toUserId);

        Transaction transaction = null;

        // UserBasicInfo user = fosUtils.getUserBasicInfo(userId.toString());
        // TODO not sure why this was commented in dinero_reporting branch
        // if (user == null || Role.TEACHER.equals(user.getRole())
        // || (!toUserId.equals(userId) && !Role.ADMIN.equals(user.getRole()))) {
        // throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Insufficient User
        // Role");
        // }
        if (reasonType == null) {
            reasonType = BillingReasonType.FREEBIES;
        }

        if (TransactionRefType.SINGUP_CREDIT.equals(transactionRefType)) {
            List<Transaction> signupFreebiesTransactions = transactionDAO.getTransactionsByRefTypeAndCreditTo(
                    TransactionRefType.SINGUP_CREDIT, toUserAccount.getId().toString());
            if (signupFreebiesTransactions != null && !signupFreebiesTransactions.isEmpty()) {
                logger.info("Signup Credits already added, returning : userId" + toUserId);
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                        "userId: " + toUserId + " signup Credits already added");
            }
        }

        Account vedantuFreebiesAccount = accountDAO.getVedantuFreebiesAccount();
        accountDAO.transfer(vedantuFreebiesAccount, toUserAccount, amount, amount, 0, allowNegativeBlance);
        transaction = new Transaction(amount, amount, 0, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE,
                vedantuFreebiesAccount.getId().toString(), vedantuFreebiesAccount.getBalance(),
                toUserAccount.getId().toString(), toUserAccount.getBalance(), reasonType.name(), reasonRefNo,
                transactionRefType, reasonNote, Transaction._getTriggredByUser(userId));
        transaction.setTags(transferFromFreebiesAccountReq.getTags());
        transactionDAO.create(transaction);

        TransferFromFreebiesAccountRes res = new TransferFromFreebiesAccountRes(toUserAccount);

        logger.info("Exiting " + res.toString());

        return res;

        // TODO checkout out how to send notification - DONE
        // if (amount != null && amount > 0 &&
        // !transactionRefType.equals(TransactionRefType.REFERRAL_CREDIT)
        // && !noAlert) {
        // // since the transfer succeeded, lets add the notification
        // // in case of TransactionRefType.REFERRAL_CREDIT the email will be
        // // send separately
        //
        // FreebieInfo freebieInfo = new FreebieInfo();
        // int rupees = MoneyUtils.toRupees(amount);
        // freebieInfo.setType("walletBalance");
        // freebieInfo.setValue(rupees);
        //
        // NotificationType notificationType = NotificationType.FREEBIES;
        // if (TransactionRefType.END_SUBSCRIPTION.equals(transactionRefType)) {
        // notificationType = NotificationType.REFUND;
        // }
        //
        // NotificationManager.INSTANCE.createNotification(toUserId,
        // notificationType, freebieInfo, null);
        //
        // if (!TransactionRefType.END_SUBSCRIPTION.equals(transactionRefType))
        // {
        // try {
        // User toUser = DAOFactory.INSTANCE.getUserDAO().getUserById(toUserId);
        // EmailManager.INSTANCE.sendFreebieBalanceEmail(toUser, amount);
        // } catch (AddressException | IOException e) {
        // logger.error(e);
        // }
        // }
        // }
    }

    public Boolean transferFromVedantuTechAccount(Long toUserId, Integer amount) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {
        return transferFromVedantuTechAccount(toUserId, amount, null);
    }

    public Boolean transferFromVedantuTechAccount(Long toUserId, Integer amount, TransactionRefType transactionRefType)
            throws ForbiddenException, BadRequestException, NotFoundException, ConflictException,
            InternalServerErrorException {

        Account vedantuTechAccount = accountDAO.getVedantuTechAccount();
        Account toUserAccount = accountDAO.getAccountByUserId(toUserId);
        if (toUserAccount == null) {
            UserBasicInfo toUser = fosUtils.getUserBasicInfo(toUserId.toString(), false);
            logger.warn("Account not found for userId: " + toUserId);
            toUserAccount = createUserAccount(toUser);
        }

        // TODO - find out what to do, currently transferring as promotional
        accountDAO.transfer(vedantuTechAccount, toUserAccount, amount, amount, 0, false);
        logger.info("Exiting");
        return true;
    }

    public Boolean transferToVedantuDefaultAccount(Long fromUserId, Long userId, Integer amount,
            Integer promotionalAmount, Integer nonPromotionalAmount, String reasonNote) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {

        UserBasicInfo user = fosUtils.getUserBasicInfo(userId.toString(), false);
        if (user == null || Role.ADMIN != user.getRole()) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                    "userId: " + userId + " is not allowed to perform this operation");
        }

        Account fromUserAccount = accountDAO.getAccountByUserId(fromUserId);
        if (fromUserAccount == null) {
            UserBasicInfo toUser = fosUtils.getUserBasicInfo(fromUserId.toString(), false);
            logger.warn("Account not found for userId: " + fromUserId);
            fromUserAccount = createUserAccount(toUser);
        }

        Account vedantuDefaultAccount = accountDAO.getVedantuDefaultAccount();

        accountDAO.transfer(fromUserAccount, vedantuDefaultAccount, amount, promotionalAmount, nonPromotionalAmount,
                false);
        Transaction transaction = new Transaction(amount, promotionalAmount, nonPromotionalAmount,
                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, fromUserAccount.getId().toString(),
                fromUserAccount.getBalance(), vedantuDefaultAccount.getId().toString(),
                vedantuDefaultAccount.getBalance(), BillingReasonType.ACCOUNT_DEDUCTION.name(), null,
                TransactionRefType.ACCOUNT_DEBIT, null, Transaction._getTriggredByUser(userId));
        transaction.setReasonNote(reasonNote);
        transactionDAO.create(transaction);

        return true;
    }

    public Boolean transferToVedantuAccount(VedantuAccount vedantuAccount, Long fromUserId, Long userId, Integer amount,
            Integer promotionalAmount, Integer nonPromotionalAmount, String reasonNote, String reasonNoteType) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {

        logger.info("transferToVedantuAccount");
        Account fromUserAccount = accountDAO.getAccountByUserId(fromUserId);
        if (fromUserAccount == null) {
            UserBasicInfo toUser = fosUtils.getUserBasicInfo(fromUserId.toString(), false);
            logger.warn("Account not found for userId: " + fromUserId);
            fromUserAccount = createUserAccount(toUser);
        }

        logger.info("fromUserAccount " + fromUserAccount);

        if (fromUserAccount.getBalance() < amount) {
            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
        } else if (fromUserAccount.getPromotionalBalance() < promotionalAmount) {
            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE,
                    "Insufficient promotional account balance");
        } else if (fromUserAccount.getNonPromotionalBalance() < nonPromotionalAmount) {
            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE,
                    "Insufficient non-promotional account balance");
        }

        if (nonPromotionalAmount > 0
                && (VedantuAccount.FREEBIES.equals(vedantuAccount) || VedantuAccount.TECH.equals(vedantuAccount))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                    "Non-promotional amount not allowed for this vedantu wallet");
        }

        Account toVedantuAccount = getVedantuAccount(vedantuAccount);
        String toVedantuAccountId = null;

        logger.info("toVedantuAccount " + toVedantuAccount);
        Integer toVedantuAccountBalance = null;

        if (toVedantuAccount != null) {
            toVedantuAccountId = toVedantuAccount.getId().toString();
            accountDAO.transfer(fromUserAccount, toVedantuAccount, amount, promotionalAmount, nonPromotionalAmount,
                    false);
            toVedantuAccountBalance = toVedantuAccount.getBalance();
        } else {
            fromUserAccount.setBalance(fromUserAccount.getBalance() - amount);
            fromUserAccount.setPromotionalBalance(fromUserAccount.getPromotionalBalance() - promotionalAmount);
            fromUserAccount.setNonPromotionalBalance(fromUserAccount.getNonPromotionalBalance() - nonPromotionalAmount);
            accountDAO.updateWithoutSession(fromUserAccount, null);
        }

        Transaction transaction = new Transaction(amount, promotionalAmount, nonPromotionalAmount,
                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, fromUserAccount.getId().toString(),
                fromUserAccount.getBalance(), toVedantuAccountId, toVedantuAccountBalance,
                BillingReasonType.ACCOUNT_DEDUCTION.name(), null, TransactionRefType.ACCOUNT_DEBIT, null,
                Transaction._getTriggredByUser(userId));
        transaction.setReasonNote(reasonNote);
        transaction.setReasonNoteType(reasonNoteType);
        transactionDAO.create(transaction);
        logger.info("transferToVedantuAccount success");

        return true;
    }

    private Account getVedantuAccount(VedantuAccount vedantuAccount) throws InternalServerErrorException, NotFoundException, ForbiddenException {
        Account toVedantuAccount = null;
        switch (vedantuAccount) {
            case DEFAULT:
                toVedantuAccount = accountDAO.getVedantuDefaultAccount();
                break;
            case FREEBIES:
                toVedantuAccount = accountDAO.getVedantuFreebiesAccount();
                break;
            case TECH:
                toVedantuAccount = accountDAO.getVedantuTechAccount();
                break;
            case NOTHING:
                toVedantuAccount = null;
                break;
            default:
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                        "vedantuAccount: " + vedantuAccount + " is not allowed.");
        }
        return toVedantuAccount;
    }

    public Transaction transferAmountFromVedantuDefaultAccount(Long toUserId, int amount, int promotionalAmount,
            int nonPromotionalAmount, String currencyCode, BillingReasonType billingReason,
            TransactionRefType reasonRefType, String reasonRefNo, String triggredBy)
            throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException {
        Account fromVedantuDefaultAccount = accountDAO.getVedantuDefaultAccount();
        Account toUserAccount = accountDAO.getAccountByUserId(toUserId);
        logger.info(toUserAccount.toString());
        return transferAmount(fromVedantuDefaultAccount, toUserAccount, amount, promotionalAmount, nonPromotionalAmount,
                currencyCode, billingReason, reasonRefType, reasonRefNo, triggredBy, true, null);

    }

    public Transaction transferAmountFromVedantuTechAccount(Long toUserId, int amount, String currencyCode,
            BillingReasonType billingReason, TransactionRefType reasonRefType, String reasonRefNo, String triggredBy)
            throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException {
        Account fromVedantuTechAccount = accountDAO.getVedantuTechAccount();
        Account toUserAccount = accountDAO.getAccountByUserId(toUserId);
        logger.info(toUserAccount.toString());
        return transferAmount(fromVedantuTechAccount, toUserAccount, amount, amount, 0, currencyCode, billingReason,
                reasonRefType, reasonRefNo, triggredBy, true, null);

    }

    public Transaction transferAmount(Account fromAccount, Account toAccount, int amount, int promotionalAmount,
            int nonPromotionalAmount, String currencyCode, BillingReasonType billingReason,
            TransactionRefType reasonRefType, String reasonRefNo, String triggredBy, boolean allowNegativeBalance, String uniqueId)
            throws BadRequestException, ConflictException, InternalServerErrorException {

        logger.info("Entering fromAccount" + fromAccount.toString() + " amount:" + amount + " toAccount:"
                + toAccount.toString());
        boolean transffered = accountDAO.transfer(fromAccount, toAccount, amount, promotionalAmount,
                nonPromotionalAmount, allowNegativeBalance);
        Transaction transaction = null;
        if (transffered) {
            transaction = new Transaction(amount, promotionalAmount, nonPromotionalAmount, currencyCode,
                    fromAccount.getId().toString(), fromAccount.getBalance(), toAccount.getId().toString(),
                    toAccount.getBalance(), billingReason.name(), reasonRefNo, reasonRefType, null, triggredBy);

            transaction.setUniqueId(uniqueId);
            transactionDAO.create(transaction);
            logger.info("created transaction: " + transaction);
        }
        logger.info("Exiting " + transaction.toString());
        return transaction;

    }

    public RefundRes getRefundInfo(RefundStudentWalletRequest refundStudentWalletRequest)
            throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException {

        logger.info("Entering " + refundStudentWalletRequest.toString());

        // refundAmount in the request is hrs cost amount paid-all discounts
        RefundRes refundRes = paymentManager.findPromotionalValueForCancelSubscription(
                refundStudentWalletRequest.getSubscriptionId(), refundStudentWalletRequest.getRefundAmount(),
                refundStudentWalletRequest.getRemainingPercentage());

        logger.info("Exiting " + refundRes);
        return refundRes;
    }

    public RefundRes refundToStudentWallet(RefundStudentWalletRequest refundStudentWalletRequest)
            throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException {

        RefundRes refundRes = getRefundInfo(refundStudentWalletRequest);

        refundToStudentWallet(refundStudentWalletRequest.getStudentId(), refundRes.getRefundAmount(),
                refundRes.getPromotionalAmount(), refundRes.getNonPromotionalAmount(),
                refundStudentWalletRequest.getSubscriptionId(), BillingReasonType.REFUND,
                TransactionRefType.END_SUBSCRIPTION);

        if (refundRes.getAmountGivenasDiscount() > 0) {
            transferAmount(accountDAO.getVedantuDefaultAccount(), accountDAO.getVedantuFreebiesAccount(),
                    refundRes.getAmountGivenasDiscount(), refundRes.getAmountGivenasDiscount(), 0,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.REFUND,
                    TransactionRefType.END_SUBSCRIPTION, refundStudentWalletRequest.getSubscriptionId().toString(),
                    "Vedantu-Automated-Refund", true, null);
        }

        logger.info("Exiting ");
        return refundRes;
    }

    public Boolean refundToStudentWallet(Long studentId, Integer refundAmount, Integer promotionalRefundAmount,
            Integer nonPromotionalRefundAmount, Long subscriptionId, BillingReasonType billingType,
            TransactionRefType refType)
            throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException {

        transferAmountFromVedantuDefaultAccount(studentId, refundAmount, promotionalRefundAmount,
                nonPromotionalRefundAmount, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, billingType, refType,
                subscriptionId.toString(), "Vedantu-Automated-Refund");

        return true;

    }

    public static String getUserAccountHolderId(String userId) {

        return "user/" + userId;
    }

    public static String getVedantuAccountHolderId(String id) {

        return "vedantu/" + id;

    }

    public Account createUserAccount(UserBasicInfo user) throws ConflictException, InternalServerErrorException {

        logger.info("Entering " + user.toString());
        Account foundAccount = accountDAO
                .getAccountByHolderId(Account.__getUserAccountHolderId(user.getUserId().toString()));

        if (foundAccount == null) {
            HolderType holderType;
            if (user.getRole().equals(Role.STUDENT)) {
                holderType = HolderType.STUDENT;
            } else if (user.getRole().equals(Role.TEACHER)) {
                holderType = HolderType.TEACHER;
            } else {
                holderType = HolderType.VEDANTU;
            }

            Account account = accountDAO.createNewAccount(Account.__getUserAccountHolderId(user.getUserId().toString()),
                    user.getFullName(), holderType);

            logger.info("Exiting Success");
            return account;

        } else {
            logger.info("Account already exists for the user");
            throw new ConflictException(ErrorCode.ACCOUNT_ALREADY_EXISTS, "Account already exists");
        }
    }

    public Boolean createVedantuAccounts() {

        logger.info("Entering");
        Account foundAccount;

        try {
            String vedantuCutAccountHolderId = Account.__getVedantuAccountHolderId(vedantuCutHolderId);
            foundAccount = accountDAO.getAccountByHolderId(vedantuCutAccountHolderId);
            if (foundAccount == null) {
                Account account = accountDAO.createNewAccount(vedantuCutAccountHolderId, vedantuCutHolderName, HolderType.VEDANTU);
                logger.info("Cut Account created " + account.toString());
            } else {
                logger.error("Cut Account already Exists");
            }
        } catch (InternalServerErrorException e) {
            logger.error(e.getErrorMessage());
        }

        try {
            String vedantuAIRAccountHolderId = Account.__getVedantuAccountHolderId(vedantuAIRHolderId);
            foundAccount = accountDAO.getAccountByHolderId(vedantuAIRAccountHolderId);
            if (foundAccount == null) {
                Account account = accountDAO.createNewAccount(vedantuAIRAccountHolderId, vedantuAIRHolderName, HolderType.VEDANTU);
                logger.info("AIR Account created " + account.toString());

            } else {
                logger.error("AIR Account already Exits");
            }
        } catch (InternalServerErrorException e) {
            logger.error(e.getErrorMessage());
        }

        try {
            String vedantuDefaultAccountHolderId = Account.__getVedantuAccountHolderId(vedantuDefaultHolderId);
            foundAccount = accountDAO.getAccountByHolderId(vedantuDefaultAccountHolderId);
            if (foundAccount == null) {
                Account account = accountDAO.createNewAccount(vedantuDefaultAccountHolderId, vedantuDefaultHolderName,
                        HolderType.VEDANTU);
                logger.info("Default Account created " + account.toString());

            } else {
                logger.error("Defualt Account already Exits");
            }
        } catch (InternalServerErrorException e) {
            logger.error(e.getErrorMessage());
        }

        try {
            String vedantuTechAccountHolderId = Account.__getVedantuAccountHolderId(vedantuTechHolderId);
            foundAccount = accountDAO.getAccountByHolderId(vedantuTechAccountHolderId);
            if (foundAccount == null) {
                Account account = accountDAO.createNewAccount(vedantuTechAccountHolderId, vedantuTechHolderName, HolderType.VEDANTU);
                logger.info("Tech Account created " + account.toString());

            } else {
                logger.error("Tech Account already Exits");
            }

        } catch (InternalServerErrorException e) {
            logger.error(e.getErrorMessage());
        }

        try {
            String vedantuFreebiesAccountHolderId = Account.__getVedantuAccountHolderId(vedantuFreebiesHolderId);
            foundAccount = accountDAO.getAccountByHolderId(vedantuFreebiesAccountHolderId);
            if (foundAccount == null) {
                Account account = accountDAO.createNewAccount(vedantuFreebiesAccountHolderId, vedantuFreebiesHolderName,
                        HolderType.VEDANTU);
                logger.info("Freebies Account created " + account.toString());
                
            } else {
                logger.error("Freebies Account already Exits");
            }

        } catch (InternalServerErrorException e) {
            logger.error(e.getErrorMessage());
        }

        logger.info("Exiting");
        return true;
    }

    public GetAccountBalanceResponse getAccountBalance(String userId, HolderType holderType)
            throws InternalServerErrorException, NotFoundException, ConflictException {

        logger.info("Entering userId" + userId + " holderType:" + holderType.name());
        GetAccountBalanceResponse successResponse;

        String holderId;
        if (holderType.equals(HolderType.VEDANTU)) {
            holderId = getVedantuAccountHolderId(userId);
        } else {
            holderId = getUserAccountHolderId(userId);
        }

        Account foundAccount = accountDAO.getAccountByHolderId(holderId);

        if (foundAccount == null) {
            if (holderId.split("/")[0].equals("user")) {
                UserBasicInfo toUser = fosUtils.getUserBasicInfo(holderId.split("/")[1], false);
                logger.warn("Account not found for holderId: " + holderId);
                foundAccount = createUserAccount(toUser);
            }

        }
        successResponse = new GetAccountBalanceResponse();
        successResponse.setBalance(foundAccount.getBalance());
        successResponse.setPromotionalBalance(foundAccount.getPromotionalBalance());
        successResponse.setNonPromotionalBalance(foundAccount.getNonPromotionalBalance());
        logger.info("Exiting " + successResponse.toString());
        return successResponse;

    }

    public AddOrDeductAccountBalanceReq addOrDeductAccountBalance(
            AddOrDeductAccountBalanceReq addOrDeductAccountBalanceReq) throws VException {
        logger.info("Entering " + addOrDeductAccountBalanceReq.toString());
        if (addOrDeductAccountBalanceReq.getUserId() == null || addOrDeductAccountBalanceReq.getHolderType() == null) {
            logger.error("Insufficient Paramameters");
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Insufficient Paramameters");
        }

        if (addOrDeductAccountBalanceReq.getVerificationId() == null) {
            Long callingUserId = addOrDeductAccountBalanceReq.getCallingUserId();
            UserBasicInfo callingUser = fosUtils.getUserBasicInfo(callingUserId.toString(), true);
            logger.info(callingUser);
            if (StringUtils.isEmpty(callingUser.getContactNumber())) {
                throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND, "contact number not found for requester");
            }
            String phoneNumber = callingUser.getContactNumber();

            String[] phoneNumbers = new String[1];
            phoneNumbers[0] = phoneNumber;

            ArrayList<String> phNumbers = new ArrayList<>(Arrays.asList(phoneNumbers));

            Random rnd = new Random();
            int code = 100000 + rnd.nextInt(900000);

            VerificationCode verificationCode = new VerificationCode(callingUserId, code, VerificationCodeStatus.ACTIVE,
                    addOrDeductAccountBalanceReq.getAmountToAdd());

            verificationCodeDAO.create(verificationCode);

            BulkTextSMSRequest request = new BulkTextSMSRequest(phNumbers, "Account Verification Code: " + code);

            logger.info("Sending SMS via Notification-Centre: " + request.toString());

            String sendBulkSMSUrl = ConfigUtils.INSTANCE
                    .getStringValue("vedantu.platform.notification.sms.sendBulkSMS");

            ClientResponse resp = WebUtils.INSTANCE.doCall(sendBulkSMSUrl, HttpMethod.POST, new Gson().toJson(request));

            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            addOrDeductAccountBalanceReq.setVerificationId(verificationCode.getId());

            logger.info("Exiting " + addOrDeductAccountBalanceReq.toString());
            return addOrDeductAccountBalanceReq;
        } else {

            VerificationCode verificationCode = verificationCodeDAO
                    .getById(addOrDeductAccountBalanceReq.getVerificationId());
            if (addOrDeductAccountBalanceReq.getVerificationCode() == verificationCode.getVerificationCode()) {
                if (verificationCode.getStatus().equals(VerificationCodeStatus.ACTIVE)) {
                    if (verificationCode.getCreationTime()
                            .compareTo(System.currentTimeMillis()
                                    - (ConfigUtils.INSTANCE.getIntValue("account.verificationCodeExpiryTimeInMinutes")
                                    * DateTimeUtils.MILLIS_PER_MINUTE)) < 0) {
                        verificationCode.setStatus(VerificationCodeStatus.EXPIRED);
                        verificationCodeDAO.create(verificationCode);
                        throw new BadRequestException(ErrorCode.VERIFICATION_CODE_EXPIRED,
                                "Verification Code Expired. Please try with a new request");
                    } else {
                        logger.info("Verification Code correct and active userId:"
                                + addOrDeductAccountBalanceReq.getCallingUserId());
                        String userId = addOrDeductAccountBalanceReq.getUserId();
                        HolderType holderType = addOrDeductAccountBalanceReq.getHolderType();
                        int amount = addOrDeductAccountBalanceReq.getAmountToAdd();
                        String accountHolderId;
                        if (holderType.equals(HolderType.TEACHER)) {
                            // accountHolderId = getUserAccountHolderId(userId);
                            // updateAccountByHolderId(accountHolderId, amount);
                            // logger.info("Exiting");
                            // TODO
                            return addOrDeductAccountBalanceReq;
                        } else {
                            TransactionLevel transactionLevel;
                            /*
							 * if (addOrDeductAccountBalanceReq.getCredit()) { transactionLevel =
							 * TransactionLevel.CREDIT; } else { transactionLevel = TransactionLevel.DEBIT;
							 * }
                             */
                            transactionLevel = TransactionLevel.ALL;

                            Query query = new Query();
                            query.addCriteria(
                                    Criteria.where("userId").is(addOrDeductAccountBalanceReq.getCallingUserId()));
                            query.addCriteria(Criteria.where("vedantuAccount").is(userId.split("-")[1]));
                            query.addCriteria(Criteria.where("transactionLevel").is(transactionLevel));
                            logger.info(
                                    "userId: " + addOrDeductAccountBalanceReq.getCallingUserId() + " vedantuAccount: "
                                    + userId.split("-")[1] + " transactionLevel: " + transactionLevel);

                            List<VedantuAccountAuth> vedantuAccountAuths = vedantuAccountAuthDAO.runQuery(query,
                                    VedantuAccountAuth.class);
                            if (vedantuAccountAuths.size() == 0) {
                                logger.info("Insufficient User Role userId:"
                                        + addOrDeductAccountBalanceReq.getCallingUserId());
                                /*
								 * vedantuAccountAuthDAO .create(new
								 * VedantuAccountAuth(addOrDeductAccountBalanceReq.getCallingUserId(),
								 * VedantuAccount.valueOf(userId.split("-")[1]), transactionLevel, 0,
								 * Integer.MAX_VALUE));
                                 */

                                // TODO throw not enough permissions exception
                                // instead of creating the auth and put the rest in else
                                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Insufficient User Role");
                            }

                            accountHolderId = getVedantuAccountHolderId(userId);
                            int promotionalAmount = 0, nonPromotionalAmount = 0;
                            if (userId.equals(vedantuTechHolderId) || userId.equals(vedantuFreebiesHolderId)
                                    || userId.equals(vedantuAIRHolderId)) {
                                promotionalAmount = amount;
                            } else {
                                nonPromotionalAmount = amount;
                            }
                            updateAccountByHolderId(accountHolderId, amount, promotionalAmount, nonPromotionalAmount,
                                    addOrDeductAccountBalanceReq.getReasonNote(),
                                    addOrDeductAccountBalanceReq.getCredit());

                            verificationCode.setStatus(VerificationCodeStatus.USED);
                            verificationCodeDAO.create(verificationCode);
                            logger.info("Exiting");
                            return addOrDeductAccountBalanceReq;
                        }
                    }

                } else if (verificationCode.getStatus().equals(VerificationCodeStatus.EXPIRED)) {
                    throw new BadRequestException(ErrorCode.VERIFICATION_CODE_EXPIRED,
                            "Verification Code Expired. Please try with a new request.");
                } else {
                    throw new BadRequestException(ErrorCode.VERIFICATION_CODE_ALREADY_USED,
                            "Verification Code Already Used. Please try with a new request.");
                }
            } else {
                logger.info(
                        "Invalid verification Code Entered userId:" + addOrDeductAccountBalanceReq.getCallingUserId());
                throw new BadRequestException(ErrorCode.INVALID_VERIFICATION_CODE,
                        "Invalid verification Code Entered. Please try again");
            }

        }

    }

    private Boolean updateAccountByHolderId(String accountHolderId, int amount, int promotionalAmount,
            int nonPromotionalAmount, String reasonNote, Boolean credit)
            throws InternalServerErrorException, NotFoundException, ConflictException {

        logger.info("Entering accountHolderId:" + accountHolderId + " amount:" + amount + " promotionalAmount:"
                + promotionalAmount + " nonPromotionalAmount:" + nonPromotionalAmount);
        Account foundAccount = accountDAO.getAccountByHolderId(accountHolderId);

        if (foundAccount == null) {
            if (accountHolderId.split("/")[0].equals("user")) {
                UserBasicInfo toUser = fosUtils.getUserBasicInfo(accountHolderId.split("/")[1], false);
                logger.warn("Account not found for holderId: " + accountHolderId);
                foundAccount = createUserAccount(toUser);
            }
        }

        if (foundAccount == null) {
            throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND, "Account not found");
        }

        com.vedantu.dinero.entity.Transaction accountTransaction;
        if (!credit) {
            foundAccount.setBalance(foundAccount.getBalance() - amount);
            foundAccount.setPromotionalBalance(foundAccount.getPromotionalBalance() - promotionalAmount);
            foundAccount.setNonPromotionalBalance(foundAccount.getNonPromotionalBalance() - nonPromotionalAmount);
            // Create a Transaction
            accountTransaction = new com.vedantu.dinero.entity.Transaction(amount, promotionalAmount,
                    nonPromotionalAmount, com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE, null,
                    null, foundAccount.getId().toString(), foundAccount.getBalance(),
                    com.vedantu.dinero.pojo.BillingReasonType.RECHARGE.name(), "ADMIN_DINERO_UPDATE",
                    TransactionRefType.INTERNAL_RECHARGE, reasonNote,
                    com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
        } else {
            foundAccount.setBalance(foundAccount.getBalance() + amount);
            foundAccount.setPromotionalBalance(foundAccount.getPromotionalBalance() + promotionalAmount);
            foundAccount.setNonPromotionalBalance(foundAccount.getNonPromotionalBalance() + nonPromotionalAmount);
            // Create a Transaction
            accountTransaction = new com.vedantu.dinero.entity.Transaction(amount, promotionalAmount,
                    nonPromotionalAmount, com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                    foundAccount.getId().toString(), foundAccount.getBalance(), null, null,
                    com.vedantu.dinero.pojo.BillingReasonType.ACCOUNT_DEDUCTION.name(), "ADMIN_DINERO_UPDATE",
                    TransactionRefType.INTERNAL_DEBIT, reasonNote,
                    com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
        }

        accountDAO.updateWithoutSession(foundAccount, null);
        logger.info("Update " + accountHolderId + " to:" + foundAccount.toString());

        transactionDAO.create(accountTransaction);

        logger.info("Exiting");
        return true;
    }

    public GetRechargeHistoryRes getRechargeHistory(GetRechargeHistoryReq getRechargeHistoryReq)
            throws InternalServerErrorException {
        logger.info("Entering " + getRechargeHistoryReq.toString());
        GetRechargeHistoryRes getRechargeHistoryRes = new GetRechargeHistoryRes();
        try {
            List<ExtTransaction> results = extTransactionDAO.getExtTransactions(getRechargeHistoryReq.getUserId(), null,
                    null);

            for (ExtTransaction transaction : results) {
                getRechargeHistoryRes.addItem(new TransactionInfoRes(transaction));
            }
        } catch (Exception e) {
            logger.error(e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        logger.info("Exiting " + getRechargeHistoryRes);
        return getRechargeHistoryRes;
    }

    public GetAccountInfoRes getAccountInfo(GetAccountInfoReq getAccountInfoReq)
            throws InternalServerErrorException, NotFoundException, ConflictException, BadRequestException {

        logger.info("Entering " + getAccountInfoReq.toString());

        Account account = accountDAO.getAccountByUserId(getAccountInfoReq.getUserId());
        if (account == null) {
//            logger.error("No account found for userId: " + getAccountInfoReq.getUserId());
            UserBasicInfo user = fosUtils.getUserBasicInfo(getAccountInfoReq.getUserId().toString(), false);
            logger.warn("Account not found for userId: " + user.getUserId());
            account = createUserAccount(user);
        }

        GetAccountInfoRes accountInfoRes = new GetAccountInfoRes(account);

        logger.info("Exiting " + accountInfoRes.toString());

        return accountInfoRes;
    }

    public GetFreebiesAccountInfoRes getFreebiesAccountInfo(GetFreebiesAccountInfoReq getFreebiesAccountInfoReq)
            throws InternalServerErrorException, NotFoundException {

        logger.info("Entering " + getFreebiesAccountInfoReq.toString());

        Account account = null;
        account = accountDAO.getVedantuFreebiesAccount();
        if (null == account) {
            throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND, "Freebies account not found");
        }

        GetFreebiesAccountInfoRes accountInfoRes = new GetFreebiesAccountInfoRes(account);
        logger.info("Exiting " + accountInfoRes.toString());

        return accountInfoRes;
    }

    public TransferToVedantuDefaultAccountRes transferToVedantuDefaultAccount(
            TransferToVedantuDefaultAccountReq transferToVedantuDefaultAccountReq) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {
        logger.info("Entering " + transferToVedantuDefaultAccountReq.toString());

        Boolean success = transferToVedantuDefaultAccount(transferToVedantuDefaultAccountReq.getUserId(),
                transferToVedantuDefaultAccountReq.getCallingUserId(), transferToVedantuDefaultAccountReq.getAmount(),
                transferToVedantuDefaultAccountReq.getPromotionalAmount(),
                transferToVedantuDefaultAccountReq.getNonPromotionalAmount(),
                transferToVedantuDefaultAccountReq.getReasonNote());
        if (!success) {
            logger.error("transfer can not be completed");
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "transfer can not be completed");
        }

        Account account = accountDAO.getAccountByUserId(transferToVedantuDefaultAccountReq.getUserId());
        logger.info("User account " + account);
        TransferToVedantuDefaultAccountRes res = new TransferToVedantuDefaultAccountRes(account);

        logger.info("Exiting " + res.toString());
        return res;
    }

    public TransferToVedantuDefaultAccountRes transferToVedantuAccount(
            TransferToVedantuAccountReq transferToVedantuAccountReq) throws ForbiddenException, BadRequestException,
            NotFoundException, ConflictException, InternalServerErrorException {
        logger.info("Entering " + transferToVedantuAccountReq.toString());

        Boolean success = transferToVedantuAccount(transferToVedantuAccountReq.getVedantuAccount(),
                transferToVedantuAccountReq.getUserId(), transferToVedantuAccountReq.getCallingUserId(),
                transferToVedantuAccountReq.getAmount(), transferToVedantuAccountReq.getPromotionalAmount(),
                transferToVedantuAccountReq.getNonPromotionalAmount(), transferToVedantuAccountReq.getReasonNote(),
                transferToVedantuAccountReq.getReasonNoteType());
        if (!success) {
            logger.error("transfer can not be completed");
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "transfer can not be completed");
        }

        Account account = accountDAO.getAccountByUserId(transferToVedantuAccountReq.getUserId());
        TransferToVedantuDefaultAccountRes res = new TransferToVedantuDefaultAccountRes(account);

        logger.info("Exiting " + res.toString());
        return res;
    }

    public Account createAccount(Account account) throws InternalServerErrorException, ConflictException {
        logger.info("Entering " + account.toString());
        Account foundAccount = accountDAO.getAccountByHolderId(account.getHolderId());

        if (foundAccount == null) {
            accountDAO.createNewAccount(account.getHolderId(),account.getHolderName(),account.getHolderType());
        } else {
            logger.info("Account already exists for the user");
            throw new ConflictException(ErrorCode.ACCOUNT_ALREADY_EXISTS,
                    "Account already exists for account " + account);
        }

        logger.info("Exiting Success");
        return account;
    }

    public Account getAccountById(String id) throws NotFoundException {
        logger.info("Entering " + id);
        Account foundAccount = accountDAO.getById(Long.parseLong(id), true);

        if (foundAccount == null) {
            logger.warn("No Account found");
            throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND, "Account not found for id " + id);
        }
        logger.info("Exiting Success");
        return foundAccount;
    }

    public Account getAccountByUserId(Long userId)
            throws InternalServerErrorException, ConflictException, BadRequestException {
        logger.info("Entering " + userId);
        Account foundAccount = accountDAO.getAccountByUserId(userId);

        if (foundAccount == null) {
            logger.warn("No Account found");
            UserBasicInfo user = fosUtils.getUserBasicInfo(userId.toString(), false);
            foundAccount = createUserAccount(user);
        }

        logger.info("Exiting " + foundAccount);
        return foundAccount;
    }

    public LockBalanceRes lockBalance(LockBalanceReq req) throws VException {
        logger.info("lockBalance - " + req.toString());
        LockBalanceRes response = new LockBalanceRes();

        // prefilling the exising data
        response.setTeacherDiscountCouponId(req.getTeacherDiscountCouponId());
        response.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());

        // Validate lock balance request
        List<String> errors = req.collectErrors();
        if (!errors.isEmpty()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, Arrays.toString(errors.toArray()));
        }

        // Fetch plan and account
        GetPlanByIdResponse plan = pricingManager.getPlanById(req.getEntityId());
        Account studentAccount = accountDAO.getAccountByUserId(req.getStudentId());
        if (studentAccount == null) {
            String errorString = "lockBalanceAccountNotFound for id : " + req.getStudentId() + " req - "
                    + req.toString();
            logger.error(errorString);
            throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND, errorString);
        }

        // Calculate amount to be locked
        Integer cost = Long.valueOf(((plan.getPrice() * req.getTotalHours()) / DateTimeUtils.MILLIS_PER_HOUR))
                .intValue();
        int totalAmount = cost;

        logger.info("Initial cost:" + totalAmount + " PB:" + studentAccount.getPromotionalBalance() + " NPB:"
                + studentAccount.getNonPromotionalBalance());

        int teacherDiscount = 0;
        if (req.getTeacherDiscountCouponId() != null) {
            // TODO what happens if the coupon is invalid when buyItems is happening
            teacherDiscount = couponManager.validateAndCalculateDiscount(req.getTeacherDiscountCouponId(),
                    req.getStudentId(), req.getEntityId(), req.getEntityType(), CouponType.TEACHER_DISCOUNT,
                    totalAmount, null, PaymentType.BULK,null);// not passing purchaseFlowId, as this is the first time coupon will be
            // locked for the subscription request
            cost -= teacherDiscount;
            response.setTeacherDiscountAmount(Long.valueOf(teacherDiscount));
        }
        int vedantuDiscount = 0;
        if (req.getVedantuDiscountCouponId() != null) {
            vedantuDiscount = couponManager.validateAndCalculateDiscount(req.getVedantuDiscountCouponId(),
                    req.getStudentId(), req.getEntityId(), req.getEntityType(), CouponType.VEDANTU_DISCOUNT,
                    totalAmount, null, PaymentType.BULK,null);
            cost -= vedantuDiscount;
        }

        response.setAccountBalance(Long.valueOf(studentAccount.getBalance()));

        int promotionalAmountPayable = 0;
        int nonPromotionalAmountPayable = 0;

        if (PaymentType.INSTALMENT.equals(req.getPaymentType())) {
            Long vedantuDiscountPerHr = 0l, teacherDiscountPerHr = 0l;

            if (req.getTotalHours() != null) {
                vedantuDiscountPerHr = vedantuDiscount * DateTimeUtils.MILLIS_PER_HOUR / req.getTotalHours();
                teacherDiscountPerHr = teacherDiscount * DateTimeUtils.MILLIS_PER_HOUR / req.getTotalHours();
            }

            List<Instalment> instalments = instalmentManager.prepareInstalments(req.getSessionSchedule(),
                    plan.getPrice().intValue(), InstalmentPurchaseEntity.PLAN, req.getEntityId(),
                    vedantuDiscountPerHr.intValue(), teacherDiscountPerHr.intValue(), null, null);
            if (ArrayUtils.isNotEmpty(instalments)) {
                Instalment firstInstalment = instalments.get(0);
                cost = firstInstalment.getTotalAmount();
                Integer hrsCost = firstInstalment.getHoursCost();
                if (hrsCost < studentAccount.getPromotionalBalance()) {
                    promotionalAmountPayable = hrsCost;
                } else {
                    promotionalAmountPayable = studentAccount.getPromotionalBalance();
                    nonPromotionalAmountPayable = hrsCost - promotionalAmountPayable;
                }
                nonPromotionalAmountPayable += firstInstalment.getConvenienceCharge()
                        + firstInstalment.getSecurityCharge();
            } else {
                logger.info("Instalments not found while locking balance - " + req.toString());
                throw new NotFoundException(ErrorCode.INSTALMENTS_INCONSISTENT, "Instalments not found");
            }
        } else if (cost < studentAccount.getPromotionalBalance()) {
            promotionalAmountPayable = cost;
        } else {
            promotionalAmountPayable = studentAccount.getPromotionalBalance();
            nonPromotionalAmountPayable = cost - promotionalAmountPayable;
        }

        response.setAmountToBeLocked(Long.valueOf(cost));

        // since we already consume promotional cash, so check is only made for np
        if (nonPromotionalAmountPayable > studentAccount.getNonPromotionalBalance()) {
            logger.info("Account balance insufficient for account - " + studentAccount.toString() + " , req - "
                    + req.toString());
            int amountToBePaid = nonPromotionalAmountPayable - studentAccount.getNonPromotionalBalance();
            response.setAmountToBePaid(amountToBePaid);
            response.setErrorCode(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE);
        } else {
            // Lock the amount
            response.setUserId(req.getStudentId());
            response.setReferenceId(req.getEntityId());
            response.setReferenceType(req.getEntityType());
            response.setPaymentType(req.getPaymentType());
            response.setPurchaseFlowId(req.getPurchaseFlowId());
            response.setPurchaseFlowType(req.getPurchaseFlowType());

            studentAccount.lockAmount(promotionalAmountPayable, nonPromotionalAmountPayable);
            response.setPromotionalCost(promotionalAmountPayable);
            response.setNonPromotionalCost(nonPromotionalAmountPayable);

            accountDAO.updateWithoutSession(studentAccount, null);

            // Create locking transaction
            TransactionRefType transactionRefType = null;
            if (req.getPurchaseFlowType() != null) {
                switch (req.getPurchaseFlowType()) {
                    case SUBSCRIPTION_REQUEST:
                        transactionRefType = TransactionRefType.SUBSCRIPTION_REQUEST;
                        break;
                    default:
                        break;
                }
            }

            Transaction transaction = new Transaction(cost, promotionalAmountPayable, nonPromotionalAmountPayable,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, String.valueOf(studentAccount.getId()),
                    studentAccount.getBalance(), String.valueOf(studentAccount.getId()), studentAccount.getBalance(),
                    BillingReasonType.LOCKING.name(), req.getPurchaseFlowId(), transactionRefType, req.toString(),
                    String.valueOf(req.getStudentId()));
            transactionDAO.create(transaction);

            // creating redemeed locked entries for coupons
            if (teacherDiscount > 0) {
                logger.info("Creating redeemed entry for the coupon " + req.getTeacherDiscountCouponId());
                couponManager.addRedeemEntry(req.getTeacherDiscountCouponId(), req.getStudentId(), null, null, null,
                        RedeemedCouponState.LOCKED, PurchaseFlowType.SUBSCRIPTION_REQUEST, req.getPurchaseFlowId());
            }
            if (vedantuDiscount > 0) {
                logger.info("Creating redeemed entry for the coupon " + req.getVedantuDiscountCouponId());
                couponManager.addRedeemEntry(req.getVedantuDiscountCouponId(), req.getStudentId(), null, null, null,
                        RedeemedCouponState.LOCKED, PurchaseFlowType.SUBSCRIPTION_REQUEST, req.getPurchaseFlowId());
            }
        }
        logger.info("lockBalanceResponse : " + response.toString());

        return response;
    }

    public LockBalanceInfo unlockBalance(LockBalanceInfo req) throws BadRequestException, NotFoundException,
            ConflictException, ForbiddenException, InternalServerErrorException {
        logger.info("unlockBalance - " + req.toString());

        // Validate lock balance request
        List<String> errors = req.collectErrors();
        if (!errors.isEmpty()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, Arrays.toString(errors.toArray()));
        }

        // Fetch user account
        Account userAccount = accountDAO.getAccountByUserId(req.getUserId());
        if (userAccount == null) {
            String errorString = "unlockBalance AccountNotFound for id : " + req.getUserId() + " req - "
                    + req.toString();
            logger.error(errorString);
            throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND, errorString);
        }

        // unlock the amount
        if (req.getPromotionalCost() == null) {
            req.setPromotionalCost(0);
        }

        if (req.getNonPromotionalCost() == null) {
            req.setNonPromotionalCost(0);
        }

        Boolean status = userAccount.unLockAmount(req.getPromotionalCost(), req.getNonPromotionalCost());
        if (status != null && status.equals(false)) {
            logger.info("Account balance insufficient for account - " + userAccount.toString() + " , req - "
                    + req.toString());
            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, userAccount.toString());
        }
        // updating the account
        accountDAO.updateWithoutSession(userAccount, null);

        // Create unlocking transaction
        TransactionRefType transactionRefType = null;
        if (req.getReferenceType() != null) {
            switch (req.getReferenceType()) {
                case SUBSCRIPTION_REQUEST:
                    transactionRefType = TransactionRefType.SUBSCRIPTION_REQUEST;
                    break;
                default:
                    break;
            }
        }

        Transaction transaction = new Transaction(req.getPromotionalCost() + req.getNonPromotionalCost(),
                req.getPromotionalCost(), req.getNonPromotionalCost(), ExtTransaction.Constants.DEFAULT_CURRENCY_CODE,
                userAccount.getId().toString(), userAccount.getBalance(), userAccount.getId().toString(),
                userAccount.getBalance(), BillingReasonType.UNLOCKING.name(), req.getReferenceId(), transactionRefType,
                req.toString(), String.valueOf(req.getUserId()));
        transactionDAO.create(transaction);

        // unlocking any used coupons
        couponManager.unlockRedeemedCoupons(req.getPurchaseFlowId());

        logger.info("PC:" + userAccount.getPromotionalBalance() + " NPC:" + userAccount.getNonPromotionalBalance());
        return req;
    }

    public List<Account> createAccounts(List<Account> accounts) throws VException {

        logger.info("Entering " + accounts.toString());

        int count = 0;
        for (Account account : accounts) {

            Account foundAccount = accountDAO.getAccountByHolderId(account.getHolderId());

            if (foundAccount != null) {
                logger.error("Account already exists" + foundAccount);
                accounts.set(count, foundAccount);
            } else {
                accountDAO.createNewAccount(account.getHolderId(), account.getHolderName(), account.getHolderType());
                logger.info("Created new account. Balance:" + 0);
                accounts.set(count, account);
            }
            count++;
        }

        logger.info("Exiting " + accounts.toString());
        return accounts;
    }

    public List<WalletStatusInfo> getVedantuWalletsStatus() throws InternalServerErrorException {

        logger.info("Entering");
        List<String> accountHolderIds = new ArrayList<>();
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuCutHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuTechHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuDefaultHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuFreebiesHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuAIRHolderId));

        List<Account> accounts = accountDAO.getAccountListByHolderIds(accountHolderIds);

        Calendar cal = Calendar.getInstance();
        Long currentTime = cal.getTimeInMillis();
        cal.add(Calendar.DATE, -1);

        Query query;

        List<WalletStatusInfo> response = new ArrayList<>();
        for (Account account : accounts) {
            Long previousTime = 0L;

            int previousBalance = 0, previousPromotionalBalance = 0, previousNonPromotionalBalance = 0;

            int currentBalance = account.getBalance();
            int currentPromotionalBalance = account.getPromotionalBalance();
            int currentNonPromotionalBalance = account.getNonPromotionalBalance();

            query = new Query();
            query.addCriteria(Criteria.where("holderId").is(account.getHolderId()));
            query.addCriteria(Criteria.where("timeState").is(TimeState.YESTERDAY));

            List<WalletStatus> foundWalletStatuses = walletStatusDAO.runQuery(query, WalletStatus.class);
            WalletStatus walletStatus;

            if (foundWalletStatuses.isEmpty()) {
                logger.info("Previous walletStatus not found " + account.getHolderId());
                walletStatus = new WalletStatus(account.getHolderId(), TimeState.YESTERDAY, currentBalance,
                        currentPromotionalBalance, currentNonPromotionalBalance);
            } else {
                walletStatus = foundWalletStatuses.get(0);
                logger.info("Found previous walletStatus " + walletStatus);

                previousBalance = walletStatus.getBalance();
                previousPromotionalBalance = walletStatus.getPromotionalBalance();
                previousNonPromotionalBalance = walletStatus.getNonPromotionalBalance();
                previousTime = walletStatus.getLastUpdated();

                walletStatus.setBalance(currentBalance);
                walletStatus.setPromotionalBalance(currentPromotionalBalance);
                walletStatus.setNonPromotionalBalance(currentNonPromotionalBalance);
            }
            response.add(new WalletStatusInfo(account.getHolderId(), currentTime, previousTime, currentBalance,
                    previousBalance, currentPromotionalBalance, previousPromotionalBalance,
                    currentNonPromotionalBalance, previousNonPromotionalBalance));
            walletStatusDAO.create(walletStatus);

        }

        logger.info("Exiting " + response);
        return response;
    }

    @CrossOrigin
    public Boolean accountVerificationSMS(AccountVerificationSMSRequest accountVerificationSMSRequest)
            throws VException {

        Long userId = accountVerificationSMSRequest.getCallingUserId();
        UserBasicInfo user = fosUtils.getUserBasicInfo(userId.toString(), true);
        if (StringUtils.isEmpty(user.getContactNumber())) {
            throw new ConflictException(ErrorCode.CONTACT_NUMBER_NOT_FOUND, "contact number not found for requester");
        }
        String phoneNumber = user.getContactNumber();

        String[] phoneNumbers = new String[1];
        phoneNumbers[0] = phoneNumber;

        ArrayList<String> phNumbers = new ArrayList<String>(Arrays.asList(phoneNumbers));
        // Where is the code

        BulkTextSMSRequest request = new BulkTextSMSRequest(phNumbers, "Account Verification Code:");

        logger.info("Sending SMS via Notification-Centre: " + request.toString());

        String sendBulkSMSUrl = ConfigUtils.INSTANCE.getStringValue("vedantu.platform.notification.sms.sendBulkSMS");

        ClientResponse resp = WebUtils.INSTANCE.doCall(sendBulkSMSUrl, HttpMethod.POST, new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        return null;
    }

    public List<SalesTransactionInfo> getTransactions(SalesTransactionReq salesTransactionReq)
            throws VException, IllegalAccessException, InvocationTargetException {
        // TODO: Higly unoptimized, change the whole logic and move to platform
        // accordingly
        GetTransactionsReq req = new GetTransactionsReq(salesTransactionReq.getUserId(),
                salesTransactionReq.getReason(), salesTransactionReq.getStart(), salesTransactionReq.getLimit(),
                salesTransactionReq.getFromTime(), salesTransactionReq.getTillTime());
        if(salesTransactionReq.getMinAmounForFilter() != null){
            req.setMinAmountForFilter(salesTransactionReq.getMinAmounForFilter());
        }
        if(salesTransactionReq.getMaxAmounForFilter() != null){
            req.setMaxAmountForFilter(salesTransactionReq.getMaxAmounForFilter());
        }
        List<Transaction> transactions = paymentManager.getTransactions(req);
        if (transactions == null) {
            transactions = new ArrayList<>();
        }
        List<SalesTransactionInfo> salesTransactionInfos = new ArrayList<SalesTransactionInfo>();
        // IAccountDAO accountDAO = DAOFactory.INSTANCE.getAccountDAO();
        for (Transaction transaction : transactions) {
            SalesTransactionInfo info = new SalesTransactionInfo();
            // BeanUtils.copyProperties(info, transaction);
            info = mapper.map(transaction, SalesTransactionInfo.class);
            info.setTransactionId(transaction.getId());
            try {
                if (transaction.getDebitFromAccount() != null) {
                    Account account = getAccountById(transaction.getDebitFromAccount());
                    if (account != null) {
                        if (!StringUtils.isEmpty(account.getHolderId())) {
                            String[] holderId = account.getHolderId().split("/");
                            if (!holderId[0].equals("vedantu")) {
                                UserBasicInfo user = fosUtils.getUserBasicInfo(holderId[1], true);
                                if (user != null) {
                                    info.setDebitFromAccountEmail(user.getEmail());
                                    info.setDebitFromAccountName(user.getFullName());
                                    info.setDebitFromAccountNumber(transaction.getDebitFromAccount());
                                } else {
                                    info.setDebitFromAccountName(account.getHolderId());
                                    info.setDebitFromAccountNumber(account.getId().toString());
                                }

                            } else {
                                info.setDebitFromAccountName(account.getHolderId());
                                info.setDebitFromAccountNumber(account.getId().toString());
                            }
                        }

                    }
                }

                if (transaction.getCreditToAccount() != null) {
                    Account account = getAccountById(transaction.getCreditToAccount());
                    if (account != null) {
                        if (!StringUtils.isEmpty(account.getHolderId())) {
                            String[] holderId = account.getHolderId().split("/");
                            if (!holderId[0].equals("vedantu")) {
                                UserBasicInfo user = fosUtils.getUserBasicInfo(holderId[1], true);
                                if (user != null) {
                                    info.setCreditToAccountEmail(user.getEmail());
                                    info.setCreditToAccountName(user.getFullName());
                                    info.setCreditToAccountNumber(transaction.getCreditToAccount());
                                } else {
                                    info.setCreditToAccountName(account.getHolderId());
                                    info.setCreditToAccountNumber(account.getId().toString());
                                }

                            } else {
                                info.setCreditToAccountName(account.getHolderId());
                                info.setCreditToAccountNumber(account.getId().toString());
                            }
                        }
                    }
                }
                if (transaction.getTriggredBy() != null) {
                    String[] triggeredBy = transaction.getTriggredBy().split("/");
                    if (triggeredBy[0].equals("user")) {
                        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(triggeredBy[1], true);
                        // UserInfo userInfo = new UserInfo(user, null);
                        info.setTriggredBy(new Gson().toJson(userInfo));
                    } else {
                        info.setTriggredBy(transaction.getTriggredBy());
                    }

                }
            } catch (Exception e) {
                logger.warn("Error in fetching transaction data" + e);
            }
            salesTransactionInfos.add(info);
        }
        return salesTransactionInfos;

    }

    public void addAccountPancard(AddAccountPancardReq req)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        accountDAO.updateAccountPancard(req.getUserId(), req.getPanCard());
    }

    public PlatformBasicResponse refundMoneyToStudentWallet(RefundMoneyToStudentWalletReq req) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {
        logger.info("refundMoneyToStudentWallet");

        UserBasicInfo user = fosUtils.getUserBasicInfo(req.getApprovedBy(), false);
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "invalid approved by userid");
        }

        Account toUserAccount = accountDAO.getAccountByUserId(req.getUserId());
        if (toUserAccount == null) {
            throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND, "Account not found");
        }

        logger.info("toUserAccount " + toUserAccount);

        Account fromVedantuAccount = getVedantuAccount(VedantuAccount.DEFAULT);

        logger.info("fromVedantuAccount " + fromVedantuAccount);

        accountDAO.transfer(fromVedantuAccount, toUserAccount, req.getAmount(),
                req.getPromotionalAmount(), req.getNonPromotionalAmount(), true);

        Transaction transaction = new Transaction(req.getAmount(), req.getPromotionalAmount(),
                req.getNonPromotionalAmount(),
                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, fromVedantuAccount.getId().toString(),
                fromVedantuAccount.getBalance(), toUserAccount.getId().toString(), toUserAccount.getBalance(),
                BillingReasonType.REFUND.name(), null, TransactionRefType.CUSTOMER_GRIEVENCES, null,
                Transaction._getTriggredByUser(req.getCallingUserId()));
        transaction.setReasonNote(req.getReasonNote());
        transaction.setContextId(req.getContextId());
        transaction.setContextType(req.getContextType());
        transaction.setApprovedBy(req.getApprovedBy());
        transactionDAO.create(transaction);
        logger.info("refundMoneyToStudentWallet success");
        return new PlatformBasicResponse();
    }

    public boolean checkForConsumption(EnrollmentTransactionContextType enrollmentTransactionContextType) {

        if (EnrollmentTransactionContextType.END_ENROLLMENT_CONSUMPTION.equals(enrollmentTransactionContextType)
                || EnrollmentTransactionContextType.END_BATCH_CONSUMPTION.equals(enrollmentTransactionContextType)
                || EnrollmentTransactionContextType.SESSION_CONSUMPTION.equals(enrollmentTransactionContextType)
                || EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION.equals(enrollmentTransactionContextType)
                || EnrollmentTransactionContextType.NEGATIVE_MARGIN_ADJUSTMENT.equals(enrollmentTransactionContextType)
                || EnrollmentTransactionContextType.CONTENT_BATCH_CONSUMPTION.equals(enrollmentTransactionContextType)) {
            return true;
        }

        logger.error("Invalid EnrollmentTransactionContextType: " + enrollmentTransactionContextType);
        return false;

    }

    public OTMConsumptionPaymentRes OTMConsumptionPayment(OTMConsumptionReq otmConsumptionReq) throws NotFoundException, BadRequestException, InternalServerErrorException, ConflictException {
        EnrollmentTransactionContextType enrollmentTransactionContextType = otmConsumptionReq.getEnrollmentTransactionContextType();
        Account fromAccount = accountDAO.getVedantuDefaultAccount();
        Account toAccount = accountDAO.getVedantuCutAccount();

        if (otmConsumptionReq.getAmount() == null || otmConsumptionReq.getNonPromotionalAmt() == null || otmConsumptionReq.getPromotionalAmt() == null) {
            throw new NotFoundException(ErrorCode.INVALID_AMOUNT, "Null Amount");
        }

        if ((!otmConsumptionReq.isNegativeMargin()) && (otmConsumptionReq.getAmount() != otmConsumptionReq.getNonPromotionalAmt() + otmConsumptionReq.getPromotionalAmt())) {
            throw new InternalServerErrorException(ErrorCode.INVALID_AMOUNT, "Sum of Promotional & Non-Promotional not equal to Amount");
        }

        if (!checkForConsumption(enrollmentTransactionContextType)) {
            throw new InternalServerErrorException(ErrorCode.INVALID_ENROLLMENT_TRANSACTION_CONTEXT_TYPE, enrollmentTransactionContextType.toString());
        }

        OTMConsumptionPaymentRes oTMConsumptionPaymentRes = new OTMConsumptionPaymentRes();
        Transaction transaction = null;
        if (otmConsumptionReq.isNegativeMargin()) {
            boolean transferred = accountDAO.transferForNegativeMargin(otmConsumptionReq.getNegativeMarginAmount(), otmConsumptionReq.getPromotionalAmt(), otmConsumptionReq.getNonPromotionalAmt(), true);
            if (transferred) {
                Account defaultAccount = accountDAO.getVedantuDefaultAccount();
                Account marketingAccount = accountDAO.getVedantuFreebiesAccount();
                Account cutAccount = accountDAO.getVedantuCutAccount();
                Transaction marketingToDefaultTransaction = new Transaction(otmConsumptionReq.getNegativeMarginAmount(),
                        otmConsumptionReq.getNegativeMarginAmount(), 0, Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        marketingAccount.getId().toString(), marketingAccount.getBalance(), defaultAccount.getId().toString(),
                        defaultAccount.getBalance(), BillingReasonType.NEGATIVE_MARGIN.toString(), otmConsumptionReq.getEnrollmentId(), TransactionRefType.NEGATIVE_MARGIN_OTM, null, "SYSTEM");
                transactionDAO.create(marketingToDefaultTransaction);
                logger.info("created marketing to default transaction: " + marketingToDefaultTransaction);

                transaction = new Transaction(otmConsumptionReq.getAmount(),
                        otmConsumptionReq.getPromotionalAmt() + otmConsumptionReq.getNegativeMarginAmount(), otmConsumptionReq.getNonPromotionalAmt(), Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        defaultAccount.getId().toString(), defaultAccount.getBalance(), cutAccount.getId().toString(),
                        cutAccount.getBalance(), BillingReasonType.CONSUMPTION.toString(), otmConsumptionReq.getEnrollmentId(), TransactionRefType.OTM_CONSUMPTION, null, "SYSTEM");
                transactionDAO.create(transaction);
                logger.info("created default to cut transaction: " + transaction);
            }

        } else if (otmConsumptionReq.isNegativeMarginAdjustment()) {

            transaction = transferAmount(fromAccount, toAccount, otmConsumptionReq.getAmount(), otmConsumptionReq.getPromotionalAmt(), otmConsumptionReq.getNonPromotionalAmt(),
                    Transaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.CONSUMPTION,
                    TransactionRefType.NEGATIVE_MARGIN_CONSUMPTION, otmConsumptionReq.getEnrollmentId(), "SYSTEM", true, null);

        } else {

            transaction = transferAmount(fromAccount, toAccount, otmConsumptionReq.getAmount(), otmConsumptionReq.getPromotionalAmt(), otmConsumptionReq.getNonPromotionalAmt(),
                    Transaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.CONSUMPTION,
                    TransactionRefType.OTM_CONSUMPTION, otmConsumptionReq.getEnrollmentId(), "SYSTEM", true, null);

        }
        if (transaction == null) {
            throw new InternalServerErrorException(ErrorCode.TRANSACTION_NOT_FOUND, "EnrollmentId : " + otmConsumptionReq.getEnrollmentId());
        }

        oTMConsumptionPaymentRes.setAmount(transaction.getAmount());
        oTMConsumptionPaymentRes.setTransactionId(transaction.getId());

        return oTMConsumptionPaymentRes;
    }

    public OTMConsumptionPaymentRes OTMRefundPayment(OTMRefundReq oTMRefundReq) throws InternalServerErrorException, BadRequestException, NotFoundException {

        Session session = sqlSessionFactory.getSessionFactory().openSession();

        org.hibernate.Transaction transaction = session.getTransaction();

        boolean isTransactionSuccess = false;

        try {
            transaction.begin();
            if (oTMRefundReq.getUserId() == null) {
                throw new NotFoundException(ErrorCode.INVALID_USER_ID, "OTMRefundPayment");
            }

            if (oTMRefundReq.isCustomerGrievence()) {

                Account fromAccount = accountDAO.getVedantuDefaultAccount();
                Account toAccount = accountDAO.getAccountByUserId(oTMRefundReq.getUserId());

                int amt = oTMRefundReq.getAmtCGP() + oTMRefundReq.getAmtCGNP();

                if (amt > 0) {
                    boolean result = accountDAO.transfer(fromAccount, toAccount, amt, oTMRefundReq.getAmtCGP(), oTMRefundReq.getAmtCGNP(), true, session);

                    if (!result) {
                        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Payment failed");
                    }
                }
                //OTF_BATCH_ENROLLMENT_END
                //OTF_BATCH_REGISTRATION_REFUND
                if (oTMRefundReq.getAmtCGFromDiscount() > 0) {
                    toAccount = accountDAO.getVedantuFreebiesAccount();
                    boolean result = accountDAO.transfer(fromAccount, toAccount, oTMRefundReq.getAmtCGFromDiscount(), oTMRefundReq.getAmtCGFromDiscount(), 0, true, session);

                    if (!result) {
                        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Payment failed");
                    }
                }

            } else {
                Account fromAccount = accountDAO.getVedantuDefaultAccount();
                Account toAccount = accountDAO.getAccountByUserId(oTMRefundReq.getUserId());

                int amt = oTMRefundReq.getPromotionalAmt() + oTMRefundReq.getNonPromotionalAmt();

                if (amt > 0) {
                    boolean result = accountDAO.transfer(fromAccount, toAccount, amt, oTMRefundReq.getPromotionalAmt(), oTMRefundReq.getNonPromotionalAmt(), true, session);

                    if (!result) {
                        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Payment failed");
                    }
                }

                if (oTMRefundReq.getAmtFromDiscount() > 0) {
                    toAccount = accountDAO.getVedantuFreebiesAccount();
                    boolean result = accountDAO.transfer(fromAccount, toAccount, oTMRefundReq.getAmtFromDiscount(), oTMRefundReq.getAmtFromDiscount(), 0, true, session);

                    if (!result) {
                        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Payment failed");
                    }
                }

            }

            transaction.commit();
            isTransactionSuccess = true;

        } catch (Exception ex) {
            logger.error("Exception while processing refund: " + ex.getMessage());
            session.getTransaction().rollback();

        } finally {

            session.close();
        }

        OTMConsumptionPaymentRes oTMConsumptionPaymentRes = new OTMConsumptionPaymentRes();

        if (isTransactionSuccess) {
            Account defaultAccount = accountDAO.getVedantuDefaultAccount();
            Account marketingAccount = accountDAO.getVedantuFreebiesAccount();
            Account userAccount = accountDAO.getAccountByUserId(oTMRefundReq.getUserId());

            if (oTMRefundReq.isCustomerGrievence()) {
                TransactionRefType transactionRefType = TransactionRefType.CUSTOMER_GRIEVENCES_REGULAR;
                if (oTMRefundReq.isRegistration()) {
                    transactionRefType = TransactionRefType.CUSTOMER_GRIEVENCES_TRIAL;
                }
                if (oTMRefundReq.getAmtCGNP() + oTMRefundReq.getAmtCGP() > 0) {
                    Transaction cgTransaction = new Transaction(oTMRefundReq.getAmtCGNP() + oTMRefundReq.getAmtCGP(),
                            oTMRefundReq.getAmtCGP(), oTMRefundReq.getAmtCGNP(), Transaction.Constants.DEFAULT_CURRENCY_CODE,
                            defaultAccount.getId().toString(), defaultAccount.getBalance(), userAccount.getId().toString(),
                            userAccount.getBalance(), BillingReasonType.REFUND.toString(), oTMRefundReq.getEnrollmentId(), transactionRefType, null, "SYSTEM");
                    transactionDAO.create(cgTransaction);
                    logger.info("created cutomer grievance transaction");
                }
                if (oTMRefundReq.getAmtCGFromDiscount() > 0) {
                    Transaction marketingTransaction = new Transaction(oTMRefundReq.getAmtCGFromDiscount(),
                            oTMRefundReq.getAmtCGFromDiscount(), 0, Transaction.Constants.DEFAULT_CURRENCY_CODE,
                            defaultAccount.getId().toString(), defaultAccount.getBalance(), marketingAccount.getId().toString(),
                            marketingAccount.getBalance(), BillingReasonType.REFUND_DISCOUNT.toString(), oTMRefundReq.getEnrollmentId(), transactionRefType, null, "SYSTEM");
                    transactionDAO.create(marketingTransaction);
                    logger.info("created marketing transaction: " + marketingTransaction);
                }
            } else {
                TransactionRefType transactionRefType = TransactionRefType.OTF_BATCH_ENROLLMENT_END;
                if (oTMRefundReq.isRegistration()) {
                    transactionRefType = TransactionRefType.OTF_BATCH_REGISTRATION_REFUND;
                }
                if (oTMRefundReq.getAmount() > 0) {
                    Transaction studentTransaction = new Transaction(oTMRefundReq.getNonPromotionalAmt() + oTMRefundReq.getPromotionalAmt(),
                            oTMRefundReq.getPromotionalAmt(), oTMRefundReq.getNonPromotionalAmt(), Transaction.Constants.DEFAULT_CURRENCY_CODE,
                            defaultAccount.getId().toString(), defaultAccount.getBalance(), userAccount.getId().toString(),
                            userAccount.getBalance(), BillingReasonType.REFUND.toString(), oTMRefundReq.getEnrollmentId(), transactionRefType, null, "SYSTEM");
                    transactionDAO.create(studentTransaction);
                    oTMConsumptionPaymentRes.setTransactionId(studentTransaction.getId());
                    oTMConsumptionPaymentRes.setAmount(oTMRefundReq.getAmount());
                    logger.info("created student transaction: " + studentTransaction);
                }
                if (oTMRefundReq.getAmtFromDiscount() > 0) {
                    Transaction marketingTransaction = new Transaction(oTMRefundReq.getAmtFromDiscount(),
                            oTMRefundReq.getAmtFromDiscount(), 0, Transaction.Constants.DEFAULT_CURRENCY_CODE,
                            defaultAccount.getId().toString(), defaultAccount.getBalance(), marketingAccount.getId().toString(),
                            marketingAccount.getBalance(), BillingReasonType.REFUND_DISCOUNT.toString(), oTMRefundReq.getEnrollmentId(), transactionRefType, null, "SYSTEM");
                    transactionDAO.create(marketingTransaction);
                    logger.info("created marketing transaction: " + marketingTransaction);
                }
            }

        }

        oTMConsumptionPaymentRes.setSuccess(isTransactionSuccess);

        return oTMConsumptionPaymentRes;

    }

    /*    
    public OTMConsumptionPaymentRes OTMRefundPayment(OTMRefundReq oTMRefundReq) throws VException {
        if (oTMRefundReq.getUserId() == null) {
            throw new VException(ErrorCode.INVALID_USER_ID, "OTMRefundPayment");
        }
        Account userAccount = accountDAO.getAccountByUserId(oTMRefundReq.getUserId());
        logger.info("Entering fromAccount" + userAccount.toString() + " amount:" + oTMRefundReq.getAmount() + " toAccount: VedantuDefault and VedantuMarketing");

        boolean transferred = false;
        logger.info("req: " + oTMRefundReq);
        if (oTMRefundReq.isCustomerGrievence()) {
            logger.info("CG Promotional: " + oTMRefundReq.getAmtCGP());
            logger.info("Promotional: " + (oTMRefundReq.getAmtCGP() + oTMRefundReq.getAmountP()));
            logger.info("NonPromotional: " + (oTMRefundReq.getAmtCGNP() + oTMRefundReq.getAmountNP()));
            transferred = accountDAO.transferRefundToStudentAndMarketing(userAccount, oTMRefundReq.getAmountFromDiscount() + oTMRefundReq.getAmtCGFromDiscount(),
                    oTMRefundReq.getAmountP() + oTMRefundReq.getAmtCGP(), oTMRefundReq.getAmountNP() + oTMRefundReq.getAmtCGNP(), true);
        } else {
            transferred = accountDAO.transferRefundToStudentAndMarketing(userAccount, oTMRefundReq.getAmountFromDiscount(),
                    oTMRefundReq.getAmountP(), oTMRefundReq.getAmountNP(), true);
        }

        OTMConsumptionPaymentRes oTMConsumptionPaymentRes = new OTMConsumptionPaymentRes();

        if (transferred) {
            Account defaultAccount = accountDAO.getVedantuDefaultAccount();
            Account marketingAccount = accountDAO.getVedantuFreebiesAccount();
            Transaction studentTransaction = new Transaction(oTMRefundReq.getAmountNP() + oTMRefundReq.getAmountP(),
                    oTMRefundReq.getAmountP(), oTMRefundReq.getAmountNP(), Transaction.Constants.DEFAULT_CURRENCY_CODE,
                    defaultAccount.getId().toString(), defaultAccount.getBalance(), userAccount.getId().toString(),
                    userAccount.getBalance(), BillingReasonType.REFUND.toString(), oTMRefundReq.getEnrollmentId(), TransactionRefType.OTM_REFUND, null, "SYSTEM");
            transactionDAO.create(studentTransaction);
            oTMConsumptionPaymentRes.setTransactionId(studentTransaction.getId());
            oTMConsumptionPaymentRes.setAmount(oTMRefundReq.getAmount());
            logger.info("created student transaction: " + studentTransaction);

            if (oTMRefundReq.isCustomerGrievence()) {
                Transaction cgTransaction = new Transaction(oTMRefundReq.getAmtCGNP() + oTMRefundReq.getAmtCGP(),
                        oTMRefundReq.getAmtCGP(), oTMRefundReq.getAmtCGNP(), Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        defaultAccount.getId().toString(), defaultAccount.getBalance(), userAccount.getId().toString(),
                        userAccount.getBalance(), BillingReasonType.REFUND.toString(), oTMRefundReq.getEnrollmentId(), TransactionRefType.CUSTOMER_GRIEVENCES, null, "SYSTEM");
                transactionDAO.create(cgTransaction);
                logger.info("created cutomer grievance transaction");
            }

            Transaction marketingTransaction = new Transaction(oTMRefundReq.getAmountFromDiscount() + oTMRefundReq.getAmtCGFromDiscount(),
                    oTMRefundReq.getAmountFromDiscount() + oTMRefundReq.getAmtCGFromDiscount(), 0, Transaction.Constants.DEFAULT_CURRENCY_CODE,
                    defaultAccount.getId().toString(), defaultAccount.getBalance(), marketingAccount.getId().toString(),
                    marketingAccount.getBalance(), BillingReasonType.REFUND.toString(), oTMRefundReq.getEnrollmentId(), TransactionRefType.OTM_REFUND, null, "SYSTEM");
            transactionDAO.create(marketingTransaction);
            logger.info("created marketing transaction: " + marketingTransaction);
        }

        return oTMConsumptionPaymentRes;
    }
     */
    public GetUserDashboardAccountInfoRes getUserDashboardAccountInfo(Long userId) throws InternalServerErrorException, BadRequestException, ConflictException {
        GetUserDashboardAccountInfoRes accountDashboardInfo = new GetUserDashboardAccountInfoRes();
        Account account = getAccountByUserId(userId);

        accountDashboardInfo.setLockedBalance(account.getLockedBalance());
        accountDashboardInfo.setBalance(account.getBalance());
        accountDashboardInfo.setPromotionalBalance(account.getPromotionalBalance());
        accountDashboardInfo.setNonPromotionalBalance(account.getNonPromotionalBalance());
        Aggregation aggregation;
        Criteria criteria = Criteria.where(Transaction.Constants.CREDIT_TO_ACCOUNT).is(account.getId().toString()).and(Transaction.Constants.REASON_TYPE).is(BillingReasonType.RECHARGE.name());
        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        aggregation = Aggregation.newAggregation(
                Aggregation.match(criteria),
                Aggregation.sort(Sort.Direction.ASC, Transaction.Constants.CREATION_TIME),
                Aggregation.group().sum(Transaction.Constants.AMOUNT).as(GetUserDashboardAccountInfoRes.Constants.TOTAL_AMOUNT_PAID)
                        .first(Transaction.Constants.CREATION_TIME).as(GetUserDashboardAccountInfoRes.Constants.FIRST_TRANSACTION_TIME)
                        .last(Transaction.Constants.CREATION_TIME).as(GetUserDashboardAccountInfoRes.Constants.LAST_TRANSACTION_TIME)
        ).withOptions(aggregationOptions);
        logger.info("running aggregation got userId: " + userId + "  aggregation:" + aggregation);
        AggregationResults<GetUserDashboardAccountInfoRes> aggregationResults = transactionDAO.runAggregation(aggregation);
        GetUserDashboardAccountInfoRes result = aggregationResults.getUniqueMappedResult();
        if (result != null) {
            accountDashboardInfo.setTotalAmountPaid(result.getTotalAmountPaid());
            accountDashboardInfo.setFirstTransactionTime(result.getFirstTransactionTime());
            accountDashboardInfo.setLastTransactionTime(result.getLastTransactionTime());
        }
        return accountDashboardInfo;
    }

    public OTMRefundAdjustmentRes makeRefundAdjustment(OTMRefundAdjustmentReq oTMRefundAdjustmentReq) throws NotFoundException, InternalServerErrorException, BadRequestException, ConflictException {
        logger.info("In refund Adjustment");
        String enrollmentId = oTMRefundAdjustmentReq.getEnrollmentId();

        OTMRefundAdjustmentRes oTMRefundAdjustmentRes = new OTMRefundAdjustmentRes();

        //List<PaymentStatus> paymentStatuses = new ArrayList<>();
        //paymentStatuses.add(PaymentStatus.PAID);
        //paymentStatuses.add(PaymentStatus.PARTIALLY_PAID);
        //paymentStatuses.add(PaymentStatus.PAYMENT_SUSPENDED);
        //paymentStatuses.add(PaymentStatus.FORFEITED);
        /*String orderId = null;
        if(oTMRefundAdjustmentReq.isTrial()){
            Query query = new Query();
            query.addCriteria(Criteria.where("items.devliverableEntityId").is(enrollmentId));
            query.addCriteria(Criteria.where("paymentStatus").is(PaymentStatus.PAID));
            Orders order = ordersDAO.findOne(query, Orders.class);
            if(order == null){
                throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, enrollmentId);
            }
            orderId = order.getId();
        }else{
            Query query = new Query();
            query.addCriteria(Criteria.where("purchasingEntity.deliverableId").is(enrollmentId));
            query.addCriteria(Criteria.where("paymentStatus").in(paymentStatuses));
            Orders order = ordersDAO.findOne(query, Orders.class);
            if(order == null){
                throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, enrollmentId);
            }
            orderId = order.getId();
        }*/
        Account fromAccount = accountDAO.getVedantuDefaultAccount();
        Account toAccount = accountDAO.getAccountByUserId(oTMRefundAdjustmentReq.getUserId());

        Transaction transaction = transferAmount(fromAccount, toAccount, oTMRefundAdjustmentReq.getAmount(), 0, oTMRefundAdjustmentReq.getNonPromotionalAmount(), Transaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.REFUND, TransactionRefType.CUSTOMER_GRIEVENCES, enrollmentId, "", true, null);
        transaction.setRemarks("MIGRATION_TIME_EQUALITY");
        transactionDAO.create(transaction);
        Transaction transaction1 = transferAmount(toAccount, fromAccount, oTMRefundAdjustmentReq.getAmount(), 0, oTMRefundAdjustmentReq.getNonPromotionalAmount(), Transaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION, TransactionRefType.REFUND_ADJUSTMENT, enrollmentId, "", true, null);
        transaction1.setRemarks("MIGRATION_TIME_EQUALITY");
        transactionDAO.create(transaction1);
        oTMRefundAdjustmentRes.setExtraRefundAmount(0);

        return oTMRefundAdjustmentRes;

    }

    public PlatformBasicResponse revertVedantuDiscountNotClaimed(OTMRefundAdjustmentReq oTMRefundAdjustmentReq) throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException {
        PlatformBasicResponse response = new PlatformBasicResponse();
        if (oTMRefundAdjustmentReq.getAmount() == null || oTMRefundAdjustmentReq.getEnrollmentId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "amount and orderId(enrollmentId) both need to present");
        }
        Account defaultAccount = accountDAO.getVedantuDefaultAccount();
        Account marketingAccount = accountDAO.getVedantuFreebiesAccount();
        Transaction transaction = transferAmount(defaultAccount, marketingAccount, oTMRefundAdjustmentReq.getAmount(), oTMRefundAdjustmentReq.getAmount(), 0,
                Transaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.ACCOUNT_DEDUCTION,
                TransactionRefType.REVERT_NOT_CLAIMED_DISCOUNT, oTMRefundAdjustmentReq.getEnrollmentId(), "SYSTEM", true, null);
        if (transaction == null) {
            throw new NotFoundException(ErrorCode.TRANSACTION_NOT_FOUND, "orderId : " + oTMRefundAdjustmentReq.getEnrollmentId());
        }
        return response;
    }

    private boolean isInternationalPayment(User userInfo, String ipAddress) {
        Function<LocationInfo, Boolean> checkLocation = (location) -> Objects.nonNull(location) && StringUtils.isNotEmpty(location.getCountry())
                && !location.getCountry().trim().equalsIgnoreCase("india") && !location.getCountry().trim().equalsIgnoreCase("in");
        return checkLocation.apply(userInfo.getLocationInfo()) || (StringUtils.isNotEmpty(ipAddress) && checkLocation.apply(ipUtil.getLocationFromIp(ipAddress)));
    }

    public PlatformBasicResponse setPaymentGatewayName(SetPaymentGatewayNameReq setpaymentGatewayNameReq) throws InternalServerErrorException, ForbiddenException, ConflictException, BadRequestException {
        setpaymentGatewayNameReq.verify();
        int expiryInSeconds = ConfigUtils.INSTANCE.getIntValue("setpaymentgatewayname.expiretime.min") * 60;
        redisDAO.setex("PAYMENT_GATEWAY_" + setpaymentGatewayNameReq.getUserId().toString(), setpaymentGatewayNameReq.getPaymentGatewayName().toString(), expiryInSeconds);
        return new PlatformBasicResponse();
    }

    public PaymentGatewayNameRes getPaymentGatewayName(String userId) throws InternalServerErrorException, BadRequestException, ConflictException {
        PaymentGatewayNameRes paymentGatewayNameRes = new PaymentGatewayNameRes();
        if (userId == null || StringUtils.isEmpty(userId.toString())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Insufficient Paramameters");
        }
        paymentGatewayNameRes.setUserId(userId);
        String paymentGatewayNameString = redisDAO.get("PAYMENT_GATEWAY_" + userId);
        if (paymentGatewayNameString != null) {
            paymentGatewayNameRes.setPaymentGatewayName(PaymentGatewayName.valueOf(paymentGatewayNameString));
        }
        return paymentGatewayNameRes;
    }

    public List<GetAllExtTransactionsRes> getAllExtTransactions(GetAllExtTransactionsReq getAllExtTransactionsReq) throws InternalServerErrorException, BadRequestException, ConflictException {

        Query query = new Query();
        if (getAllExtTransactionsReq.getUserId() != null && getAllExtTransactionsReq.getUserId() != 0) {
            query.addCriteria(Criteria.where("userId").is(getAllExtTransactionsReq.getUserId()));
        }
        if (getAllExtTransactionsReq.getPaymentGatewayName() != null) {
            query.addCriteria(Criteria.where("gatewayName").is(getAllExtTransactionsReq.getPaymentGatewayName()));
        }
        if (getAllExtTransactionsReq.getPaymentStatus() != null) {
            query.addCriteria(Criteria.where("status").is(getAllExtTransactionsReq.getPaymentStatus()));
        }
        if (getAllExtTransactionsReq.getFromTime() != null) {
            query.addCriteria(Criteria.where("creationTime").gte(getAllExtTransactionsReq.getFromTime()));
        }
        if (getAllExtTransactionsReq.getTillTime() != null) {
            query.addCriteria(Criteria.where("creationTime").lte(getAllExtTransactionsReq.getTillTime()));
        }
        int start = getAllExtTransactionsReq.getStart();
        query.skip(start);
        int limit = getAllExtTransactionsReq.getLimit();
        if (limit != 0 && (limit >= start)) {
            query.limit((int) (limit - start));
        }
        query.with(Sort.by(Sort.Direction.DESC, "creationTime"));
        List<ExtTransaction> extTransactions = extTransactionDAO.runQuery(query, ExtTransaction.class);

        List<Long> userIds = new ArrayList<>();
        for (ExtTransaction extTransaction : extTransactions) {
            if (extTransaction.getUserId() != null) {
                userIds.add(extTransaction.getUserId());
            }
            if (extTransaction.getLastUpdatedBy() != null && !"SYSTEM".equals(extTransaction.getLastUpdatedBy()) && !userIds.contains(Long.parseLong(extTransaction.getLastUpdatedBy()))) {
                userIds.add(Long.parseLong(extTransaction.getLastUpdatedBy()));
            }
        }
        Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);

        List<GetAllExtTransactionsRes> getAllExtTransactionsRes = new ArrayList<>();
        for (ExtTransaction extTransaction : extTransactions) {
            GetAllExtTransactionsRes response = new GetAllExtTransactionsRes();
            response.setExtTransaction(extTransaction);
            response.setUser(usersMap.get(extTransaction.getUserId()));
            response.setLastUpdateduser("SYSTEM".equals(extTransaction.getLastUpdatedBy()) || extTransaction.getLastUpdatedBy() == null ? new UserBasicInfo() : usersMap.get(Long.parseLong(extTransaction.getLastUpdatedBy())));
            getAllExtTransactionsRes.add(response);
        }

        return getAllExtTransactionsRes;
    }

    public PlatformBasicResponse checkAnyPreviousOrders(Long userId) throws BadRequestException {
        if(userId == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId not Found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in("PAID"));
        query.addCriteria(Criteria.where(Orders.Constants.NON_PROMOTIONAL_AMOUNT).gt(0));
        ordersDAO.setFetchParameters(query,null,null);
        long ordersCount = ordersDAO.queryCount(query,Orders.class);
        if(ordersCount <=1) {
            return new PlatformBasicResponse(true, "NewUser", "");
        }
        else {
            return new PlatformBasicResponse(false,"RepeatedUser","");
        }
    }

    public PlatformBasicResponse checkIfHasPaidOrders(Long userId) throws BadRequestException {
        if(userId == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId not Found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Orders.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Orders.Constants.PAYMENT_STATUS).in(Arrays.asList(PaymentStatus.PAID,PaymentStatus.PARTIALLY_PAID)));
        ordersDAO.setFetchParameters(query,null,null);
        long ordersCount = ordersDAO.queryCount(query,Orders.class);
        if(ordersCount < 1) {
            return new PlatformBasicResponse(true, "NewUser", "");
        }
        else {
            return new PlatformBasicResponse(false,"RepeatedUser","");
        }
    }

    public PlatformBasicResponse switchAmountOneAccountToAnother(SwitchAmountOneAccountToAnotherReq req) throws VException{
        req.verify();
        logger.info("Entering " + req.toString());
        if(req.getFromUserId().equals(req.getToUserId())){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Should be use Different userId for amount switching!");
        }
        Account fromAccount = accountDAO.getAccountByUserId(Long.parseLong(req.getFromUserId()));
        if (fromAccount == null) {
            UserBasicInfo toUser = fosUtils.getUserBasicInfo(req.getFromUserId(), false);
            if(toUser == null){
                throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "User not found for id - " + req.getFromUserId());
            }
            logger.warn("Account not found for userId: " + req.getFromUserId());
            fromAccount = createUserAccount(toUser);
        }

        Account toAccount = accountDAO.getAccountByUserId(Long.parseLong(req.getToUserId()));
        if (toAccount == null) {
            UserBasicInfo toUser = fosUtils.getUserBasicInfo(req.getToUserId(), false);
            if(toUser == null){
                throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "User not found for id - " + req.getToUserId());
            }
            logger.warn("Account not found for userId: " + req.getToUserId());
            toAccount = createUserAccount(toUser);
        }

        int amount = req.getAmount();
        int promotionalAmount = 0;
        int nonPromotionalAmount = req.getAmount();
        boolean allowNegativeBalance = false;

        if(fromAccount.getNonPromotionalBalance() < nonPromotionalAmount){
            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
        }

        boolean transffered = accountDAO.transfer(fromAccount, toAccount, amount, promotionalAmount,
                nonPromotionalAmount, allowNegativeBalance);

        if(transffered){
            Transaction transaction = new Transaction(amount, promotionalAmount, nonPromotionalAmount,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, fromAccount.getId().toString(),
                    fromAccount.getBalance(), null,
                    null, BillingReasonType.AMOUNT_SWITCH.name(), null,
                    TransactionRefType.ACCOUNT_DEBIT, null,  Transaction._getTriggredByUser(req.getCallingUserId()));
            transaction.setReasonNote(req.getAmount() + " Amount transferred from userId : "+ req.getFromUserId() + " to : " + req.getToUserId());
            transactionDAO.create(transaction);

            Transaction transaction1 = new Transaction(amount, promotionalAmount, nonPromotionalAmount,
                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null,
                    null, toAccount.getId().toString(),
                    toAccount.getBalance(), BillingReasonType.AMOUNT_SWITCH.name(), null,
                    TransactionRefType.ACCOUNT_CREDIT, null, Transaction._getTriggredByUser(req.getCallingUserId()));
            transaction1.setReasonNote(req.getAmount() + " Amount transferred from userId : "+ req.getFromUserId() + " to : " + req.getToUserId());
            transactionDAO.create(transaction1);
        }
        return new PlatformBasicResponse(transffered,"","");
    }

    public PlatformBasicResponse extTransactionStatusChangeById(String extTransactionId, TransactionStatus status) throws VException {
        if (status == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Requested transaction status not found : " + status);
        }
        ExtTransaction extTransaction = extTransactionDAO.getById(extTransactionId);
        if (extTransaction == null) {
            throw new NotFoundException(ErrorCode.EXT_TRANSACTION_NOT_FOUND, "Ext Transaction Not found Id is - " + extTransactionId);
        }
        if (!extTransaction.getStatus().equals(status)) {
            extTransaction.setStatus(status);
            extTransactionDAO.save(extTransaction);
        }
        return new PlatformBasicResponse();
    }

    public void createUserAccountFromSQS(User userReq) throws ConflictException, InternalServerErrorException {
        UserBasicInfo user = new UserBasicInfo(userReq, true);
        if (user == null) {
            logger.error("user details not found for creating the account");
            return;
        }
        logger.info("Entering from SQSListner : " + user.toString());
        Account foundAccount = accountDAO.getAccountByHolderId(Account.__getUserAccountHolderId(user.getUserId().toString()));
        if (foundAccount == null) {
            HolderType holderType;
            if (user.getRole().equals(Role.STUDENT)) {
                holderType = HolderType.STUDENT;
            } else if (user.getRole().equals(Role.TEACHER)) {
                holderType = HolderType.TEACHER;
            } else {
                holderType = HolderType.VEDANTU;
            }
            Account account = accountDAO.createNewAccount(Account.__getUserAccountHolderId(user.getUserId().toString()),
                    user.getFullName(), holderType);
            logger.info("Exiting Success account : " + account);
        } else {
            logger.warn("account already created userId : " + user.getUserId());
        }
    }
}
