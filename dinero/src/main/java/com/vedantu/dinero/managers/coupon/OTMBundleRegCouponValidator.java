/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class OTMBundleRegCouponValidator extends AbstractOTMBundleCouponValidator{

    @Override
    public boolean supportsOTMBundleRegistration() {
        return true;
    }
    
}
