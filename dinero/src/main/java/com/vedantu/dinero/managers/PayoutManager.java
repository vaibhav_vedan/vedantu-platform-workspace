package com.vedantu.dinero.managers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.vedantu.session.pojo.EntityType;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.TeacherIncentive;
import com.vedantu.dinero.entity.TeacherPayoutRate;
import com.vedantu.dinero.entity.UpdateSessionPayoutRequest;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.enums.DurationConflictReason;
import com.vedantu.dinero.enums.SessionEntity;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.FailedPayoutInfo;
import com.vedantu.dinero.pojo.TeacherSessionInfo;
import com.vedantu.dinero.pojo.TotalMonthlyPayout;
import com.vedantu.dinero.request.SessionPayoutRequest;
import com.vedantu.dinero.response.ExportDailyTransactionsResponse;
import com.vedantu.dinero.response.GetMonthlyPayoutResponse;
import com.vedantu.dinero.response.GetTeacherSessionInfoResponse;
import com.vedantu.dinero.response.GetTotalPayoutResponse;
import com.vedantu.dinero.response.SessionPayoutResponse;
import com.vedantu.dinero.response.UpdateSessionPayoutResponse;
import com.vedantu.dinero.serializers.TeacherIncentiveDAO;
import com.vedantu.dinero.serializers.TeacherPayoutRateDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.serializers.UpdateSessionPayoutRequestDAO;
import com.vedantu.dinero.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.dao.SessionPayoutDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.dinero.sql.entity.SessionPayout;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LiveSessionPlatformType;
import com.vedantu.util.enums.SessionModel;
import org.springframework.http.HttpMethod;

/*
 * Manager Service for Pricing APIs
 */
@Service
public class PayoutManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PayoutManager.class);

    @Autowired
    private SqlSessionFactory sqlSessionFactory;

    @Autowired
    public TeacherPayoutRateDAO teacherPayoutRateDAO;

    @Autowired
    public TeacherIncentiveDAO teacherIncentiveDAO;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public SessionPayoutDAO sessionPayoutDAO;

    @Autowired
    public UpdateSessionPayoutRequestDAO updateSessionPayoutRequestDAO;

    @Autowired
    public AccountDAO accountDAO;

    @Autowired
    public AccountManager accountManager;

    @Autowired
    public PaymentManager paymentManager;

    @Autowired
    public FosUtils fosUtils;

    //private static String userFetchUrl;
    private static String getEndedSessionsTodayUrl;

    public PayoutManager() {
        getEndedSessionsTodayUrl = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT")+ ConfigUtils.INSTANCE.getStringValue("payout.getEndedSessionsTodayUrl");
    }    
    

    @SuppressWarnings("finally")
    public SessionPayoutResponse calculateSessionPayout(SessionPayoutRequest sessionPayoutRequest)
            throws InternalServerErrorException, BadRequestException {

        logger.info("Entering " + sessionPayoutRequest.toString());

        Long billingDuration=0l;
        if(LiveSessionPlatformType.GTT.equals(sessionPayoutRequest.getLiveSessionPlatformType())
                ||LiveSessionPlatformType.GTM.equals(sessionPayoutRequest.getLiveSessionPlatformType())){
            billingDuration=sessionPayoutRequest.getSessionDuration();
        }else{
            billingDuration = sessionPayoutRequest.getBillingDuration();
        }
        
        logger.info("billing duration "+billingDuration);
        
        Long hourlyRate = sessionPayoutRequest.getHourlyRate();
        int teacherPayPromotionalAmount = 0;
        int teacherPayNonPromotionalAmount = 0;
        int vedantuPayPromotionalAmount = 0;
        int vedantuPayNonPromotionalAmount = 0;

        SessionPayout sessionPayout;
        Long teacherId = sessionPayoutRequest.getTeacherId();
        SessionPayoutResponse successResponse = new SessionPayoutResponse();
        Double cut;

//        UserBasicInfo teacherPojo = fosUtils.getUserBasicInfo(teacherId.toString());
//
//        if (teacherPojo == null || !teacherPojo.getRole().name().equals(Role.TEACHER.name())) {
//            logger.error("Role for the given Id " + teacherId + " doesnt match to TEACHER.");
//            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
//                    "Role for the given Id doesnt match to TEACHER.");
//        }

        // Billing duration
//        int minBillDurationInMillis = ConfigUtils.INSTANCE.getIntValue("payout.minimumBillableMinutes")
//                * DateTimeUtils.MILLIS_PER_MINUTE;

        long netBillDurationInMillis = billingDuration > 0 ? billingDuration : 0;

        billingDuration = netBillDurationInMillis;
        sessionPayoutRequest.setBillingDuration(billingDuration);

        float billingTimeInSeconds = (float) netBillDurationInMillis / DateTimeUtils.MILLIS_PER_SECOND;

        int netBillAmount = (int) ((billingTimeInSeconds * hourlyRate) / DateTimeUtils.SECONDS_PER_HOUR);

        Pair<Integer, Integer> billingSplit = paymentManager.calculateBillingSplit(
                sessionPayoutRequest.getSubscriptionId(), sessionPayoutRequest.getHourlyRate().intValue(),
                sessionPayoutRequest.getTotalHours(), sessionPayoutRequest.getConsumedHours(), netBillAmount,
                sessionPayoutRequest.getSessionId());

        int netPromotionalBillAmount = billingSplit.getLeft();
        int netNonPromotionalBillAmount = billingSplit.getRight();

        logger.info("netPromotionalBillAmount:" + netPromotionalBillAmount + " netNonPromotionalBillAmount:"
                + netNonPromotionalBillAmount);

        // Find teacher cut
        cut = findAndUpdateCut(teacherId, sessionPayoutRequest.getModel(), sessionPayoutRequest.getSubscriptionId(),
                sessionPayoutRequest.getSessionId());

        vedantuPayPromotionalAmount = (int) ((netPromotionalBillAmount * cut) / 100);
        teacherPayPromotionalAmount = netPromotionalBillAmount - vedantuPayPromotionalAmount;

        vedantuPayNonPromotionalAmount = (int) ((netNonPromotionalBillAmount * cut) / 100);
        teacherPayNonPromotionalAmount = netNonPromotionalBillAmount - vedantuPayNonPromotionalAmount;

        logger.info("teacherPayPromotionalAmount:" + teacherPayPromotionalAmount + " vedantuPayPromotionalAmount:"
                + vedantuPayPromotionalAmount + " teacherPayNonPromotionalAmount:" + teacherPayNonPromotionalAmount
                + " vedantuPayNonPromotionalAmount:" + vedantuPayNonPromotionalAmount);

        int totalTeacherPayout = (teacherPayPromotionalAmount + teacherPayNonPromotionalAmount);
        int totalVedantuPayout = (vedantuPayPromotionalAmount + vedantuPayNonPromotionalAmount);

        logger.info("Total bill:" + netBillAmount + " teacherPayout:" + totalTeacherPayout + " vedantuPayout:"
                + totalVedantuPayout);

        List<SessionPayout> sessionPayouts;
        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            transaction = session.getTransaction();
        } catch (Exception e) {
            logger.error(e);
            if (session != null) {
                session.close();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        org.hibernate.Criteria cr;
        try {

            transaction.begin();
            // Update SessionPayout
            sessionPayout = new SessionPayout(sessionPayoutRequest.getSessionId(),
                    sessionPayoutRequest.getSubscriptionId(), billingDuration,
                    sessionPayoutRequest.getSessionDuration(), sessionPayoutRequest.getDate(),
                    sessionPayoutRequest.getModel(), totalTeacherPayout, teacherPayPromotionalAmount,
                    teacherPayNonPromotionalAmount, totalVedantuPayout, vedantuPayPromotionalAmount,
                    vedantuPayNonPromotionalAmount, teacherId, sessionPayoutRequest.getStudentId(),
                    SessionEntity.SUBSCRIPTION, sessionPayoutRequest.getSubscriptionId().toString(), hourlyRate);
            //sessionPayout.setCallingUserId("DineroSessionPayout");
            //sessionPayout.setCreatedBy("DineroSessionPayout");
            sessionPayout.setCreationTime(System.currentTimeMillis());

            cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.eq("sessionId", sessionPayoutRequest.getSessionId()));
            sessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);

            if (!sessionPayouts.isEmpty()) {
                throw new Exception(
                        "Session Payout already calculated. Please use updateSessionPayout API in case of update");
            }
            logger.info("Inserting payout "+sessionPayout);

            sessionPayoutDAO.create(sessionPayout, session, "DineroSessionPayout");

            logger.info("Creating Transactions and updating teacher and vedantu accounts");
            // Create Transactions
            Account teacherAccount = accountManager.getAccountByUserId(teacherId);
            Account cutAccount = accountDAO.getVedantuCutAccount();
            Account defaultAccount = accountDAO.getVedantuDefaultAccount();

            if (totalTeacherPayout != 0) {
                accountManager.transferAmount(defaultAccount, teacherAccount, totalTeacherPayout + totalVedantuPayout,
                        teacherPayPromotionalAmount + vedantuPayPromotionalAmount,
                        teacherPayNonPromotionalAmount + vedantuPayNonPromotionalAmount,
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION, TransactionRefType.SESSION_PAYOUT,
                        sessionPayoutRequest.getSessionId().toString(),
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem(), true, null);
            }

            if (totalVedantuPayout != 0) {
                accountManager.transferAmount(teacherAccount, cutAccount, totalVedantuPayout,
                        vedantuPayPromotionalAmount, vedantuPayNonPromotionalAmount,
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION, TransactionRefType.SESSION_PAYOUT,
                        sessionPayoutRequest.getSessionId().toString(),
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem(), true, null);
            }

            logger.info("Payout calculated and updated successfully");

            logger.info("Processing Session Payout");
            // Debit fos account
            /*
            Boolean fosResp = processSessionPayout(sessionPayoutRequest.getSessionId(),
                    sessionPayoutRequest.getBillingDuration(), totalTeacherPayout,
                    (totalTeacherPayout + totalVedantuPayout), false);
            if (!fosResp) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
                        "Error Procession session payout in FOS");
            }
             logger.info(fosResp.toString());
            */

           

            transaction.commit();

            successResponse = new SessionPayoutResponse(sessionPayout);

        } catch (Exception e) {
            logger.error(e);
            if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                logger.error("calculateSessionPayout Rollback: " + e.getMessage());
                session.getTransaction().rollback();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());

        } finally {
            session.close();            
        }
        logger.info("Exiting " + successResponse.toString());
        return successResponse;
    }

    /*
    public List<UserBasicInfo> getUserBasicInfos(List<String> userIds) {

        logger.info("Entering userIds:" + userIds.toString());

        if (userIds == null || userIds.isEmpty()) {
            return null;
        }

        List<UserBasicInfo> users = new ArrayList<UserBasicInfo>();
        String url = userFetchUrl + "?type=user&" + "userIdsList="
                + StringUtils.arrayToDelimitedString(userIds.toArray(), ",");
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        String output = response.getEntity(String.class);
        if (!StringUtils.isEmpty(output)) {
            try {
                Gson gson = new Gson();
                Type listType = new TypeToken<ArrayList<UserBasicInfo>>() {
                }.getType();
                List<UserBasicInfo> result = gson.fromJson(output, listType);
                if (result != null && !result.isEmpty()) {
                    users.addAll(result);
                }
            } catch (Exception ex) {
                logger.error("Exception while parsing userBasicInfos", ex);
            }
        }

        logger.info("Exiting users:" + users);
        return users;
    }
    */

    public UpdateSessionPayoutResponse updateSessionPayout(UpdateSessionPayoutRequest updateSessionPayoutRequest)
            throws InternalServerErrorException, NotFoundException {
        logger.info("Entering " + updateSessionPayoutRequest.toString());

        Long newBillingDuration = updateSessionPayoutRequest.getNewDuration();
        Long previousBillingDuration;
        Long hourlyRate = updateSessionPayoutRequest.getHourlyRate();
        int totalTeacherPayout;
        int totalVedantuPayout = 0;
        Long teacherId = updateSessionPayoutRequest.getTeacherId();
        UpdateSessionPayoutResponse successResponse = new UpdateSessionPayoutResponse();
        Double cut;
        int previousBillAmount = 0;
        int refundAmount = 0;
        int durationDifference = 0;
        List<SessionPayout> sessionPayouts;
        SessionPayout foundSesssionPayout;

        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            transaction = session.getTransaction();
        } catch (Exception e) {
            logger.error(e);
            if (session != null) {
                session.close();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        org.hibernate.Criteria cr;
        cr = session.createCriteria(SessionPayout.class);

        // Find Remaining prom and non promotional value bu sending comsumed -
        // previousDuration
        cr.add(Restrictions.eq("sessionId", updateSessionPayoutRequest.getSessionId()));
        sessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);
        if (sessionPayouts.size() != 1) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No Entry for session found in SessionPayout");
        }

        foundSesssionPayout = sessionPayouts.get(0);
        previousBillingDuration = foundSesssionPayout.getBillingDuration();

        long netBillDurationInMillis = newBillingDuration;

        float billingTimeInSeconds = (float) netBillDurationInMillis / DateTimeUtils.MILLIS_PER_SECOND;

        int netBillAmount = (int) (billingTimeInSeconds * hourlyRate / DateTimeUtils.SECONDS_PER_HOUR);

        Pair<Integer, Integer> billingSplits = paymentManager.calculateBillingSplit(
                updateSessionPayoutRequest.getSubscriptionId(), updateSessionPayoutRequest.getHourlyRate().intValue(),
                updateSessionPayoutRequest.getTotalHours(),
                updateSessionPayoutRequest.getConsumedHours() - previousBillingDuration,
                netBillAmount, updateSessionPayoutRequest.getSessionId());

        int netPromotionalBillAmount = billingSplits.getLeft();
        int netNonPromotionalBillAmount = billingSplits.getRight();

        logger.info("netPromotionalBillAmount:" + netPromotionalBillAmount + " netNonPromotionalBillAmount:"
                + netNonPromotionalBillAmount);

        int teacherPayoutDifference, vedantuPayoutDifference;

        boolean isSubscriptionEndedOrInsufficient = false;
        Long previousSessionDuration;
        try {

            transaction.begin();

            previousSessionDuration = foundSesssionPayout.getSessionDuration();
            if (newBillingDuration > previousSessionDuration) {
                throw new Exception("New Duration given is Greater than the booked session duration");
            }

            previousBillAmount = foundSesssionPayout.getTeacherPayout() + foundSesssionPayout.getCut();
            // Using old cut percentage
            if (previousBillAmount != 0) {
                cut = (double) ((((double) foundSesssionPayout.getCut() / (double) previousBillAmount)) * 100);
            } else {
                cut = new Double(0);
            }

            int previousTeacherPayPromotionalAmount = foundSesssionPayout.getTeacherPromotionalPayout();
            int previousVedantuPayPromotionalAmount = foundSesssionPayout.getPromotionalCut();

            int previousTeacherPayNonPromotionalAmount = foundSesssionPayout.getTeacherNonPromotionalPayout();
            int previousVedantuPayNonPromotionalAmount = foundSesssionPayout.getNonPromotionalCut();

            // totalVedantuPayout = (int) (netBillAmount * cut);
            // totalTeacherPayout = netBillAmount - totalVedantuPayout;
            int vedantuPayPromotionalAmount = (int) ((netPromotionalBillAmount * cut) / 100);
            int teacherPayPromotionalAmount = netPromotionalBillAmount - vedantuPayPromotionalAmount;

            int vedantuPayNonPromotionalAmount = (int) ((netNonPromotionalBillAmount * cut) / 100);
            int teacherPayNonPromotionalAmount = netNonPromotionalBillAmount - vedantuPayNonPromotionalAmount;

            totalVedantuPayout = vedantuPayNonPromotionalAmount + vedantuPayPromotionalAmount;
            totalTeacherPayout = teacherPayPromotionalAmount + teacherPayNonPromotionalAmount;

            logger.info("Total bill:" + netBillAmount + " teacherPayout:" + totalTeacherPayout + "vedantuPayout:"
                    + totalVedantuPayout);

            refundAmount = previousBillAmount - netBillAmount;
            int promotionalRefundAmount = previousTeacherPayPromotionalAmount + previousVedantuPayPromotionalAmount
                    - teacherPayPromotionalAmount - vedantuPayPromotionalAmount;
            int nonPromotionalRefundAmount = previousTeacherPayNonPromotionalAmount
                    + previousVedantuPayNonPromotionalAmount - teacherPayNonPromotionalAmount
                    - vedantuPayNonPromotionalAmount;

            logger.info("refundAmount:" + refundAmount + " promotionalRefundAmount:" + promotionalRefundAmount
                    + " nonPromotionalRefundAmount:" + nonPromotionalRefundAmount);

            durationDifference = (int) (newBillingDuration - previousBillingDuration);
            teacherPayoutDifference = totalTeacherPayout - foundSesssionPayout.getTeacherPayout();
            vedantuPayoutDifference = totalVedantuPayout - foundSesssionPayout.getCut();

            int promotionalTeacherPayoutDifference = teacherPayPromotionalAmount - previousTeacherPayPromotionalAmount;
            int nonPromotionalTeacherPayoutDifference = teacherPayNonPromotionalAmount
                    - previousTeacherPayNonPromotionalAmount;

            int promotionalVedantuPayoutDifference = vedantuPayPromotionalAmount - previousVedantuPayPromotionalAmount;
            int nonPromotionalVedantuPayoutDifference = vedantuPayNonPromotionalAmount
                    - previousVedantuPayNonPromotionalAmount;

            logger.info("Teacher Payout Difference:" + teacherPayoutDifference + " promotionalTeacherPayoutDifference:"
                    + promotionalTeacherPayoutDifference + " nonPromotionalTeacherPayoutDifference:"
                    + nonPromotionalTeacherPayoutDifference);
            logger.info("Vedantu Payout Difference:" + vedantuPayoutDifference + " promotionalVedantuPayoutDifference:"
                    + promotionalVedantuPayoutDifference + " nonPromotionalVedantuPayoutDifference:"
                    + nonPromotionalVedantuPayoutDifference);

            if (!updateSessionPayoutRequest.getReason().equals(DurationConflictReason.TECH_ISSUES)) {

                foundSesssionPayout.setTeacherPayout(totalTeacherPayout);
                foundSesssionPayout.setCut(totalVedantuPayout);
                foundSesssionPayout.setTeacherPromotionalPayout(teacherPayPromotionalAmount);
                foundSesssionPayout.setTeacherNonPromotionalPayout(teacherPayNonPromotionalAmount);
                foundSesssionPayout.setPromotionalCut(vedantuPayPromotionalAmount);
                foundSesssionPayout.setNonPromotionalCut(vedantuPayNonPromotionalAmount);
                foundSesssionPayout.setBillingDuration(newBillingDuration);
                foundSesssionPayout.setUpdated(true);
                sessionPayoutDAO.create(foundSesssionPayout, session);

                // If subscription has ended, doesn't have enough hours to
                // update, and refundAmount > 0
                if (updateSessionPayoutRequest.isRefundToStudentWallet()
                        || durationDifference > updateSessionPayoutRequest.getRemainingHours()) {
                    isSubscriptionEndedOrInsufficient = true;
                    Long studentId = updateSessionPayoutRequest.getStudentId();
                    if (refundAmount > 0) {
                        Boolean response = accountManager.refundToStudentWallet(studentId, refundAmount,
                                promotionalRefundAmount, nonPromotionalRefundAmount,
                                updateSessionPayoutRequest.getSubscriptionId(), BillingReasonType.SESSION,
                                TransactionRefType.UPDATE_SESSION_PAYOUT);
                        if (!response) {
                            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
                                    "Exception in refunding Student Wallet. Rolling back..");
                        }
                    }
                }

                Account teacherDebitFrom, teacherCreditTo, cutDebitFrom, cutCreditTo;
                Account teacherAccount = accountManager.getAccountByUserId(teacherId);
                Account cutAccount = accountDAO.getVedantuCutAccount();
                Account defaultAccount = accountDAO.getVedantuDefaultAccount();
                Account techAccount = accountDAO.getVedantuTechAccount();

                // Create Transactions
                if (teacherPayoutDifference < 0) {
                    teacherDebitFrom = teacherAccount;
                    teacherCreditTo = defaultAccount;
                    cutDebitFrom = cutAccount;
                    cutCreditTo = teacherAccount;
                } else if (isSubscriptionEndedOrInsufficient) {
                    teacherDebitFrom = techAccount;
                    teacherCreditTo = teacherAccount;
                    cutDebitFrom = teacherAccount;
                    cutCreditTo = cutAccount;
                } else {
                    teacherDebitFrom = defaultAccount;
                    teacherCreditTo = teacherAccount;
                    cutDebitFrom = teacherAccount;
                    cutCreditTo = cutAccount;
                }

                // Debit fos account or tech wallet
                // If debit from is tech wallet, all amount is promotional
                if (teacherDebitFrom.equals(techAccount)) {
                    promotionalTeacherPayoutDifference = teacherPayoutDifference;
                    nonPromotionalTeacherPayoutDifference = 0;
                    promotionalVedantuPayoutDifference = vedantuPayoutDifference;
                    nonPromotionalVedantuPayoutDifference = 0;
                }

                accountManager.transferAmount(teacherDebitFrom, teacherCreditTo,
                        Math.abs(teacherPayoutDifference + vedantuPayoutDifference),
                        Math.abs(promotionalTeacherPayoutDifference + promotionalVedantuPayoutDifference),
                        Math.abs(nonPromotionalTeacherPayoutDifference + nonPromotionalVedantuPayoutDifference),
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION, TransactionRefType.UPDATE_SESSION_PAYOUT,
                        updateSessionPayoutRequest.getSessionId().toString(),
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem(), true, null);

                accountManager.transferAmount(cutDebitFrom, cutCreditTo, Math.abs(vedantuPayoutDifference),
                        Math.abs(promotionalVedantuPayoutDifference), Math.abs(nonPromotionalVedantuPayoutDifference),
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION, TransactionRefType.UPDATE_SESSION_PAYOUT,
                        updateSessionPayoutRequest.getSessionId().toString(),
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem(), true, null);

                /*
                logger.info("Processing Session Payout");
                // Debit fos account
                Boolean fosResp = processSessionPayout(updateSessionPayoutRequest.getSessionId(),
                        netBillDurationInMillis, totalTeacherPayout, (totalTeacherPayout + totalVedantuPayout), true);
                if (!fosResp) {
                    throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
                            "Error Procession session payout in FOS");
                }
                */

            } else if (refundAmount > 0) {
                //Give student refund
                Account studentAccount = accountManager.getAccountByUserId(foundSesssionPayout.getStudentId());
                logger.info("Previous student balance:" + studentAccount.getBalance());
                studentAccount.setBalance(studentAccount.getBalance() + refundAmount);
                studentAccount
                        .setPromotionalBalance(studentAccount.getPromotionalBalance() + promotionalRefundAmount);
                studentAccount.setNonPromotionalBalance(
                        studentAccount.getNonPromotionalBalance() + nonPromotionalRefundAmount);
                logger.info("Final student balance:" + studentAccount.getBalance());
                accountDAO.update(studentAccount, session);

                //Convert teacher payout difference to promotional
                Account teacherAccount = accountManager.getAccountByUserId(foundSesssionPayout.getTeacherId());
                logger.info("Previous teacher balance:" + teacherAccount.getBalance());
                teacherAccount.setBalance(teacherAccount.getBalance() - foundSesssionPayout.getTeacherPayout()
                        + (foundSesssionPayout.getTeacherPayout() - totalTeacherPayout));
                teacherAccount.setPromotionalBalance(
                        teacherAccount.getPromotionalBalance() - foundSesssionPayout.getTeacherPromotionalPayout()
                        + foundSesssionPayout.getTeacherPayout() - totalTeacherPayout);
                teacherAccount.setNonPromotionalBalance(teacherAccount.getNonPromotionalBalance()
                        - foundSesssionPayout.getTeacherNonPromotionalPayout());
                logger.info("Final teacher balance:" + teacherAccount.getBalance());
                accountDAO.update(teacherAccount, session);

                //convert cut difference to promotional
                Account cutAccount = accountDAO.getVedantuCutAccount();
                logger.info("Previous cut balance:" + cutAccount.getBalance());
                cutAccount.setBalance(cutAccount.getBalance() - foundSesssionPayout.getCut()
                        + (foundSesssionPayout.getCut() - totalVedantuPayout));
                cutAccount.setPromotionalBalance(
                        cutAccount.getPromotionalBalance() - foundSesssionPayout.getPromotionalCut()
                        + foundSesssionPayout.getCut() - totalVedantuPayout);
                cutAccount.setNonPromotionalBalance(
                        cutAccount.getNonPromotionalBalance() - foundSesssionPayout.getNonPromotionalCut());
                logger.info("Final cut balance:" + cutAccount.getBalance());
                accountDAO.update(cutAccount, session);

                //Give promotional payout from tech account
                Account techAccount = accountDAO.getVedantuTechAccount();
                logger.info("Previous tech balance:" + techAccount.getBalance());
                techAccount.setBalance(
                        techAccount.getBalance() - (foundSesssionPayout.getTeacherPayout() - totalTeacherPayout)
                        - (foundSesssionPayout.getCut() - totalVedantuPayout));
                techAccount.setPromotionalBalance(techAccount.getPromotionalBalance()
                        - (foundSesssionPayout.getTeacherPayout() - totalTeacherPayout)
                        - (foundSesssionPayout.getCut() - totalVedantuPayout));
                logger.info("Final tech balance:" + techAccount.getBalance());

                accountDAO.update(techAccount, session);

                //correct sessionpayout table to show promotional payout and cut
                foundSesssionPayout.setTeacherPayout(foundSesssionPayout.getTeacherPayout() - totalTeacherPayout);
                foundSesssionPayout.setCut(foundSesssionPayout.getCut() - totalVedantuPayout);
                foundSesssionPayout
                        .setTeacherPromotionalPayout(foundSesssionPayout.getTeacherPayout() - totalTeacherPayout);
                foundSesssionPayout.setTeacherNonPromotionalPayout(0);
                foundSesssionPayout.setPromotionalCut((foundSesssionPayout.getCut() - totalVedantuPayout));
                foundSesssionPayout.setNonPromotionalCut(0);
                foundSesssionPayout.setBillingDuration(previousBillingDuration - newBillingDuration);
                foundSesssionPayout.setUpdated(true);
                sessionPayoutDAO.update(foundSesssionPayout, session);

                //Refund transaction
                Account defaultAccount = accountDAO.getVedantuDefaultAccount();
                com.vedantu.dinero.entity.Transaction studentTransaction = new com.vedantu.dinero.entity.Transaction(
                        refundAmount, promotionalRefundAmount, nonPromotionalRefundAmount,
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        defaultAccount.getId().toString(), defaultAccount.getBalance(), studentAccount.getId().toString(), studentAccount.getBalance(),
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION.name(),
                        updateSessionPayoutRequest.getSessionId().toString(),
                        TransactionRefType.SESSION_PAYOUT_REFUND, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
                transactionDAO.create(studentTransaction);

                //Reverse payout transactions for teacher and cut
                com.vedantu.dinero.entity.Transaction reverseTeacherTransaction = new com.vedantu.dinero.entity.Transaction(
                        foundSesssionPayout.getTeacherPayout(), foundSesssionPayout.getTeacherPromotionalPayout(),
                        foundSesssionPayout.getTeacherNonPromotionalPayout(),
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        teacherAccount.getId().toString(), teacherAccount.getBalance(), defaultAccount.getId().toString(), defaultAccount.getBalance(),
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION.name(),
                        updateSessionPayoutRequest.getSessionId().toString(),
                        TransactionRefType.REVERSE_SESSION_PAYOUT, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
                transactionDAO.create(reverseTeacherTransaction);

                com.vedantu.dinero.entity.Transaction reverseCutTransaction = new com.vedantu.dinero.entity.Transaction(
                        foundSesssionPayout.getCut(), foundSesssionPayout.getPromotionalCut(),
                        foundSesssionPayout.getNonPromotionalCut(),
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        cutAccount.getId().toString(), cutAccount.getBalance(), defaultAccount.getId().toString(), defaultAccount.getBalance(),
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION.name(),
                        updateSessionPayoutRequest.getSessionId().toString(),
                        TransactionRefType.REVERSE_SESSION_PAYOUT, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
                transactionDAO.create(reverseCutTransaction);

                //Payout transactions from tech wallet to teacher and cut
                com.vedantu.dinero.entity.Transaction teacherTransaction = new com.vedantu.dinero.entity.Transaction(
                        (foundSesssionPayout.getTeacherPayout() - totalTeacherPayout),
                        (foundSesssionPayout.getTeacherPayout() - totalTeacherPayout), 0,
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        techAccount.getId().toString(), techAccount.getBalance(), teacherAccount.getId().toString(), teacherAccount.getBalance(),
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION.name(),
                        updateSessionPayoutRequest.getSessionId().toString(),
                        TransactionRefType.SESSION_PAYOUT_TECH_ISSUES, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
                transactionDAO.create(teacherTransaction);

                com.vedantu.dinero.entity.Transaction cutTransaction = new com.vedantu.dinero.entity.Transaction(
                        (foundSesssionPayout.getCut() - totalVedantuPayout),
                        (foundSesssionPayout.getCut() - totalVedantuPayout), 0,
                        com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
                        techAccount.getId().toString(), techAccount.getBalance(), cutAccount.getId().toString(), cutAccount.getBalance(),
                        com.vedantu.dinero.pojo.BillingReasonType.SESSION.name(),
                        updateSessionPayoutRequest.getSessionId().toString(),
                        TransactionRefType.SESSION_PAYOUT_TECH_ISSUES, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
                transactionDAO.create(cutTransaction);

                logger.info("Updated successfully");

            }

            successResponse = new UpdateSessionPayoutResponse(foundSesssionPayout, previousBillingDuration);

            if (updateSessionPayoutRequest.getReason().equals(DurationConflictReason.TECH_ISSUES)
                    || updateSessionPayoutRequest.isRefundToStudentWallet()
                    || durationDifference > updateSessionPayoutRequest.getRemainingHours()) {
                successResponse.setIsSubscriptionUpdated(Boolean.FALSE);
            } else {
                successResponse.setIsSubscriptionUpdated(Boolean.TRUE);
            }

            transaction.commit();

        } catch (Exception e) {
            logger.error(e);
            if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                logger.error("calculateSessionPayout Rollback: " + e.getMessage());
                session.getTransaction().rollback();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());

        } finally {
            session.close();
        }

        updateSessionPayoutRequestDAO.create(updateSessionPayoutRequest);
        logger.info("Exiting " + successResponse.toString());
        return successResponse;
    }

    // Migration API - If needed to be use test it and make it like
    // calculateSessionPayout (Account stuff)
    // Re-calculate Session Payouts for ended Subscriptions. Money is paid from
    // Tech Wallet
    // @SuppressWarnings("finally")
    // public BaseResponse
    // recalculateSessionPayoutEndedSubscriptions(SessionPayoutRequest
    // sessionPayoutRequest) {
    //
    // logger.info("Entering " + sessionPayoutRequest.toString());
    //
    // Long billingDuration = sessionPayoutRequest.getBillingDuration();
    // Long hourlyRate = sessionPayoutRequest.getHourlyRate();
    // BaseResponse jsonResp = new BaseResponse();
    // int teacherPayAmount;
    // int vedantuPayAmount = 0;
    // SessionPayout sessionPayout;
    // Long teacherId = sessionPayoutRequest.getTeacherId();
    // Account account;
    // SessionPayoutResponse successResponse = new SessionPayoutResponse();
    // BaseResponse errorResponse;
    // Double cut;
    //
    // List<String> userIds = new ArrayList<String>();
    // userIds.add(teacherId.toString());
    // List<UserBasicInfo> userBasicInfos = getUserBasicInfos(userIds);
    //
    // if (userBasicInfos != null && userBasicInfos.size() == 1) {
    // UserBasicInfo teacherPojo = userBasicInfos.get(0);
    // if (!teacherPojo.getRole().equals(Role.TEACHER)) {
    // logger.error("Role for the given Id doesnt match to TEACHER.");
    // errorResponse = new BaseResponse();
    // errorResponse.setErrorCode(ErrorCode.BAD_REQUEST_ERROR);
    // errorResponse.setErrorMessage("Role for the given Id doesnt match to
    // TEACHER.");
    // logger.info("Exiting " + errorResponse.toString());
    // return errorResponse;
    // }
    // } else {
    // logger.error("Unable to fetch UserBasicInfo pojo for the teacherId
    // given");
    // errorResponse = new BaseResponse();
    // errorResponse.setErrorCode(ErrorCode.SERVICE_ERROR);
    // errorResponse.setErrorMessage("Unable to fetch UserBasicInfo pojo for the
    // teacherId given");
    // logger.info("Exiting " + errorResponse.toString());
    // return errorResponse;
    // }
    //
    // // Billing duration
    // int minBillDurationInMillis =
    // ConfigUtils.INSTANCE.getIntValue("payout.minimumBillableMinutes")
    // * DateTimeUtils.MILLIS_PER_MINUTE;
    //
    // long netBillDurationInMillis = billingDuration > 0 ?
    // Math.max(minBillDurationInMillis, billingDuration) : 0;
    //
    // billingDuration = netBillDurationInMillis;
    // sessionPayoutRequest.setBillingDuration(billingDuration);
    //
    // float billingTimeInSeconds = (float) netBillDurationInMillis /
    // DateTimeUtils.MILLIS_PER_SECOND;
    //
    // int netBillAmount = (int) ((billingTimeInSeconds * hourlyRate) /
    // DateTimeUtils.SECONDS_PER_HOUR);
    //
    // // Find teacher cut
    // cut = findAndUpdateCut(teacherId, sessionPayoutRequest.getModel(),
    // sessionPayoutRequest.getSubscriptionId(),
    // sessionPayoutRequest.getSessionId());
    //
    // vedantuPayAmount = (int) ((netBillAmount * cut) / 100);
    // teacherPayAmount = netBillAmount - vedantuPayAmount;
    //
    // logger.info("Total bill:" + netBillAmount + " teacherPayout:" +
    // teacherPayAmount + "vedantuPayout:"
    // + vedantuPayAmount);
    //
    // List<Account> accounts;
    // List<SessionPayout> sessionPayouts;
    // SessionFactory sessionFactory = null;
    // Session session = null;
    // Transaction transaction = null;
    // try {
    // sessionFactory = sqlSessionFactory.getSessionFactory();
    // session = sessionFactory.openSession();
    // transaction = session.getTransaction();
    // } catch (Exception e) {
    // jsonResp = new BaseResponse();
    // jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
    // jsonResp.setErrorMessage(e.getMessage());
    // logger.info("Exiting " + jsonResp.toString());
    // return jsonResp;
    // }
    // int balance;
    // org.hibernate.Criteria cr;
    // String teacherAccountId;
    // String vedantuCutAccountId;
    //
    // try {
    //
    // transaction.begin();
    // // Update SessionPayout
    // sessionPayout = new SessionPayout(sessionPayoutRequest.getSessionId(),
    // sessionPayoutRequest.getSubscriptionId(), billingDuration,
    // sessionPayoutRequest.getSessionDuration(),
    // sessionPayoutRequest.getDate(),
    // sessionPayoutRequest.getModel(), teacherPayAmount, vedantuPayAmount,
    // teacherId,
    // sessionPayoutRequest.getStudentId(), SessionEntity.SUBSCRIPTION,
    // sessionPayoutRequest.getSubscriptionId().toString(), hourlyRate);
    // sessionPayout.setCallingUserId("DineroSessionPayout");
    // sessionPayout.setCreatedBy("DineroSessionPayout");
    // sessionPayout.setCreationTime(System.currentTimeMillis());
    //
    // cr = session.createCriteria(SessionPayout.class);
    // cr.add(Restrictions.eq("sessionId",
    // sessionPayoutRequest.getSessionId()));
    // sessionPayouts = sessionPayoutDAO.runQuery(session, cr,
    // SessionPayout.class);
    //
    // if (sessionPayouts.size() != 0)
    // throw new Exception(
    // "Session Payout already calculated. Please use updateSessionPayout API in
    // case of update");
    //
    // sessionPayoutDAO.create(sessionPayout, session);
    //
    // // Update Teacher Account
    // cr = session.createCriteria(Account.class);
    // teacherAccountId =
    // accountManager.getUserAccountHolderId(teacherId.toString());
    // logger.info(teacherAccountId);
    // cr.add(Restrictions.eq("holderId", teacherAccountId));
    // accounts = accountDAO.runQuery(session, cr, Account.class);
    // if (accounts.size() == 1) {
    // account = accounts.get(0);
    // balance = account.getBalance();
    // balance = balance + teacherPayAmount;
    // account.setBalance(balance);
    // accountDAO.create(account, session);
    // logger.info("Update teacher balance to:" + balance);
    // } else {
    // throw new Exception("Teacher Account does not Exist");
    // }
    //
    // // Update Vedantu Account
    // cr = session.createCriteria(Account.class);
    // vedantuCutAccountId =
    // accountManager.getVedantuAccountHolderId(vedantuCutHolderId);
    // cr.add(Restrictions.eq("holderId", vedantuCutAccountId));
    // cr.add(Restrictions.eq("holderName", vedantuCutHolderName));
    // cr.add(Restrictions.eq("holderType", HolderType.VEDANTU));
    //
    // accounts = accountDAO.runQuery(session, cr, Account.class);
    // if (accounts.size() == 1) {
    // account = accounts.get(0);
    // balance = account.getBalance();
    // balance = balance + vedantuPayAmount;
    // account.setBalance(balance);
    // accountDAO.create(account, session);
    // logger.info("Update vedantu cut balance to:" + balance);
    // } else {
    // throw new Exception("Vedantu Cut Account does not Exist");
    // }
    //
    // logger.info("Payout calculated and updated successfully");
    //
    // logger.info("Debiting tech Wallet");
    // cr = session.createCriteria(Account.class);
    // String vedantuTechAccountId =
    // accountManager.getVedantuAccountHolderId(vedantuTechHolderId);
    // logger.info(vedantuTechAccountId);
    // logger.info(vedantuTechHolderId);
    // cr.add(Restrictions.eq("holderId", vedantuTechAccountId));
    // cr.add(Restrictions.eq("holderName", vedantuTechHolderName));
    // cr.add(Restrictions.eq("holderType", HolderType.VEDANTU));
    //
    // accounts = accountDAO.runQuery(session, cr, Account.class);
    // if (accounts.size() == 1) {
    // account = accounts.get(0);
    // balance = account.getBalance();
    // balance = balance - teacherPayAmount - vedantuPayAmount;
    // account.setBalance(balance);
    // accountDAO.create(account, session);
    // logger.info("Update vedantu tech balance to:" + balance);
    // } else
    // throw new Exception("Vedantu Tech Wallet Entry Not Found");
    //
    // logger.info("Creating Transactions");
    //
    // // Create Transactions
    // if (teacherPayAmount != 0) {
    // com.vedantu.dinero.entity.Transaction teacherTransaction = new
    // com.vedantu.dinero.entity.Transaction(
    // teacherPayAmount,
    // com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
    // vedantuTechHolderId, teacherAccountId.toString(),
    // com.vedantu.dinero.enums.BillingReasonType.SESSION_PAYOUT.name(),
    // sessionPayoutRequest.getSessionId().toString(),
    // TransactionRefType.SESSION_PAYOUT,
    // com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
    // logger.info(teacherTransaction.toString());
    // transactionDAO.create(teacherTransaction);
    // }
    //
    // if (vedantuPayAmount != 0) {
    // com.vedantu.dinero.entity.Transaction vedantuCutTransaction = new
    // com.vedantu.dinero.entity.Transaction(
    // vedantuPayAmount,
    // com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
    // vedantuTechHolderId, vedantuCutAccountId.toString(),
    // com.vedantu.dinero.enums.BillingReasonType.SESSION_PAYOUT.name(),
    // sessionPayoutRequest.getSessionId().toString(),
    // TransactionRefType.SESSION_PAYOUT,
    // com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
    // logger.info(vedantuCutTransaction.toString());
    // transactionDAO.create(vedantuCutTransaction);
    //
    // }
    //
    // transaction.commit();
    //
    // successResponse = new SessionPayoutResponse(sessionPayout);
    //
    // } catch (Exception e) {
    // logger.error(e);
    // if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
    // logger.error("calculateSessionPayout Rollback: " + e.getMessage());
    // session.getTransaction().rollback();
    // }
    // if (e instanceof SQLException) {
    // errorResponse = new BaseResponse();
    // errorResponse.setErrorCode(ErrorCode.SERVICE_ERROR);
    // errorResponse.setErrorMessage(e.getMessage());
    // logger.info("Exiting " + errorResponse.toString());
    // return errorResponse;
    // } else {
    // errorResponse = new BaseResponse();
    // errorResponse.setErrorCode(ErrorCode.SERVICE_ERROR);
    // errorResponse.setErrorMessage(e.getMessage());
    // logger.info("Exiting " + errorResponse.toString());
    // return errorResponse;
    // }
    // } finally {
    // session.close();
    // successResponse.setErrorCode(ErrorCode.SUCCESS);
    // successResponse.setErrorMessage("Payout updated successfully");
    // logger.info("Exiting " + successResponse.toString());
    // return successResponse;
    // }
    //
    // }
    public Double findAndUpdateCut(Long teacherId, SessionModel model, Long subscriptionId, Long sessionId) {

        logger.info("Entering teacherId" + teacherId);
        List<TeacherPayoutRate> foundTeacherPayoutRates;
        Double cut = new Double(20);
        Query query = new Query();
        Double entityCut = new Double(-1);
        Double modelCut = new Double(-1);
        Double teacherCut = new Double(-1);

        query.addCriteria(Criteria.where("teacherId").is(teacherId));
        foundTeacherPayoutRates = teacherPayoutRateDAO.runQuery(query, TeacherPayoutRate.class);
        if (!foundTeacherPayoutRates.isEmpty()) {
            for (TeacherPayoutRate foundTeacherPayoutRate : foundTeacherPayoutRates) {
                if (foundTeacherPayoutRate.getModel() != null) {
                    // Get Cut by Entity
                    if (foundTeacherPayoutRate.getEntity() != null) {
                        if (foundTeacherPayoutRate.getEntity().equals(SessionEntity.SESSION)
                                && foundTeacherPayoutRate.getEntityId().equals(sessionId.toString())) {
                            entityCut = foundTeacherPayoutRate.getCut();
                            logger.info("Found Session Cut:" + entityCut);
                        } else if (foundTeacherPayoutRate.getEntity().equals(SessionEntity.SUBSCRIPTION)
                                && foundTeacherPayoutRate.getEntityId().equals(subscriptionId.toString())) {
                            entityCut = foundTeacherPayoutRate.getCut();
                            logger.info("Found Subscription Cut:" + entityCut);
                        }
                    } // Get Cut by Model
                    else if (foundTeacherPayoutRate.getModel().equals(model)) {
                        modelCut = foundTeacherPayoutRate.getCut();
                        logger.info("Found Model Cut:" + modelCut);
                    }
                } // Teacher Cut
                else {
                    teacherCut = foundTeacherPayoutRate.getCut();
                    logger.info("Found teacher Cut:" + teacherCut);
                }
            }
        }

        if (Double.compare(entityCut, -1) > 0) {
            cut = entityCut;
        } else if (Double.compare(modelCut, -1) > 0) {
            cut = modelCut;
        } else if (Double.compare(teacherCut, -1) > 0) {
            cut = teacherCut;
        }

        logger.info("Final teacher cut: " + cut);
        return cut;

    }

//    public Boolean processSessionPayout(Long sessionId, Long billingDuration, int teacherPayout, int studentCharge,
//            boolean isUpdateRequest) {
//        logger.info("Entering sessionId" + sessionId);
//
//        MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
//        formData.add("sessionId", String.valueOf(sessionId));
//        formData.add("billingDuration", String.valueOf(billingDuration));
//        formData.add("teacherPayout", String.valueOf(teacherPayout));
//        formData.add("studentCharge", String.valueOf(studentCharge));
//        formData.add("isUpdateRequest", String.valueOf(isUpdateRequest));
//        formData.add("key", processSessionPayoutKey);
//
//        try {
//            return processSessionPayoutGapi(formData);
//        } catch(Exception e) {
//            logger.info("Error in processSessionPayoutGapi, Retrying again ", e);
//            return processSessionPayoutGapi(formData);
//        }
//    }
//    
//    public Boolean processSessionPayoutGapi(MultivaluedMap<String, String> formData) {
//        WebResource webResource = WebUtils.INSTANCE.getClient().resource(processSessionPayoutUrl);
//        ClientResponse resp = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class,
//                formData);
//
//        String output = resp.getEntity(String.class);
//        logger.info(output);
//        Boolean response = new Gson().fromJson(output, Boolean.class);
//        logger.info("Exiting " + response.toString());
//        return response;
//    }
//    

    // Migration API
    public GetTeacherSessionInfoResponse getTeacherSessionInfos(Long startDate, Long endDate)
            throws InternalServerErrorException {

        logger.info(startDate + "---" + endDate);

        Map<Long, TeacherSessionInfo> teacherTeacherSessionInfoMap = new HashMap<Long, TeacherSessionInfo>();
        List<TeacherSessionInfo> teacherSessionInfos = new ArrayList<TeacherSessionInfo>();
        SessionFactory sessionFactory = null;
        Session session = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception e) {
            logger.error(e);
            if (session != null) {
                session.close();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        try {
            org.hibernate.Criteria cr;
            cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.between("creationTime", startDate, endDate));
            cr.addOrder(Order.asc("creationTime"));

            List<SessionPayout> foundSessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);

            Long foundTeacherId;
            TeacherSessionInfo foundTeacherSessionInfo;
            List<Long> teacherIds = new ArrayList<Long>();
            for (SessionPayout foundSessionPayout : foundSessionPayouts) {
                foundTeacherId = foundSessionPayout.getTeacherId();
                if (teacherTeacherSessionInfoMap.containsKey(foundTeacherId)) {
                    foundTeacherSessionInfo = teacherTeacherSessionInfoMap.get(foundTeacherId);
                    foundTeacherSessionInfo.setBillingDuration(
                            foundTeacherSessionInfo.getBillingDuration() + foundSessionPayout.getBillingDuration());
                    foundTeacherSessionInfo.setSessionsTaught(foundTeacherSessionInfo.getSessionsTaught() + 1);
                    teacherTeacherSessionInfoMap.put(foundTeacherId, foundTeacherSessionInfo);
                } else {
                    foundTeacherSessionInfo = new TeacherSessionInfo(foundTeacherId,
                            foundSessionPayout.getBillingDuration(), 1);
                    teacherTeacherSessionInfoMap.put(foundTeacherId, foundTeacherSessionInfo);
                    teacherIds.add(foundTeacherId);
                }
            }

            for (Long teacherId : teacherIds) {
                teacherSessionInfos.add(teacherTeacherSessionInfoMap.get(teacherId));
            }
        } catch (Exception e) {
            logger.error(e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        } finally {
            session.close();

        }

        GetTeacherSessionInfoResponse response = new GetTeacherSessionInfoResponse();
        response.setTeacherSessionInfos(teacherSessionInfos);
        logger.info("Exiting " + response.toString());
        return response;
    }

    public ExportDailyTransactionsResponse exportTransactions(Long startDate, Long endDate) throws ParseException {

        Long startTimeInMillis, endTimeInMillis;
        if (startDate != null && endDate != null) {
            startTimeInMillis = startDate;
            endTimeInMillis = endDate;
        } else {
            Calendar cal = Calendar.getInstance();

            Date currTime = cal.getTime();
            endTimeInMillis = currTime.getTime();

            cal.add(Calendar.DAY_OF_MONTH, -1);
            Date previousTime = cal.getTime();
            startTimeInMillis = previousTime.getTime();
        }
        logger.info(startTimeInMillis + "---" + endTimeInMillis);

        Query query = new Query();

        query.addCriteria(Criteria.where("creationTime").gte(startTimeInMillis)
                .andOperator(Criteria.where("creationTime").lte(endTimeInMillis)));

        List<com.vedantu.dinero.entity.Transaction> transactions = transactionDAO.runQuery(query,
                com.vedantu.dinero.entity.Transaction.class);

        ExportDailyTransactionsResponse response = new ExportDailyTransactionsResponse();
        response.setTransactions(transactions);
        logger.info("Exiting " + response.toString());
        return response;

    }

    public GetMonthlyPayoutResponse getMonthlyTeacherPayout(Long teacherId, int month)
            throws ParseException, InternalServerErrorException {

        logger.info("Entering: " + teacherId + " month:" + month);

        SessionFactory sessionFactory = null;
        Session session = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception e) {
            logger.error(e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        String formattedDate;
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        DateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
        Date date01, date31;
        Long time01, time31;
        Month requiredMonth = Month.of(month);
        GetMonthlyPayoutResponse response = new GetMonthlyPayoutResponse();
        Integer taughtStudents = 0;
        Integer taughtSessions = 0;
        Long studentId;
        Integer sessionEarnings = 0;
        Integer cut = 0;
        List<Long> studentsTaught = new ArrayList<Long>();
        int incentiveGiven = 0;
        List<SessionPayout> sessionPayouts = new ArrayList<SessionPayout>();

        try {
            org.hibernate.Criteria cr;

            // Add Criteria for session Payout table
            formattedDate = "01-" + requiredMonth + "-" + currentYear + " " + "00:00:00";
            date01 = (Date) formatter.parse(formattedDate);
            if (!requiredMonth.equals(Month.FEBRUARY)) {
                formattedDate = "31-" + requiredMonth + "-" + currentYear + " " + "23:59:00";
            } else if (currentYear % 4 == 0) {
                formattedDate = "29-" + requiredMonth + "-" + currentYear + " " + "23:59:00";
            } else {
                formattedDate = "28-" + requiredMonth + "-" + currentYear + " " + "23:59:00";
            }
            date31 = (Date) formatter.parse(formattedDate);
            time01 = date01.getTime();
            time31 = date31.getTime();

            cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.between("date", time01, time31));
            if (teacherId != null) {
                cr.add(Restrictions.eq("teacherId", teacherId));
            }

            cr.addOrder(Order.asc("date"));

            // Find Incentive for month and model
            List<TeacherIncentive> foundTeacherIncentives;
            // Map<Integer, Integer> monthIncentiveMap = new HashMap<Integer,
            // Integer>();
            Query query = new Query();
            query.addCriteria(Criteria.where("teacherId").is(teacherId)
                    .andOperator(Criteria.where("month").is(requiredMonth.toString())));
            foundTeacherIncentives = teacherIncentiveDAO.runQuery(query, TeacherIncentive.class);
            for (TeacherIncentive foundTeacherIncentive : foundTeacherIncentives) {
                // incentiveMonth = foundTeacherIncentive.getMonth().ordinal() +
                // 1;

                // if (monthIncentiveMap.containsKey(incentiveMonth)) {
                // incentiveGiven = monthIncentiveMap.get(incentiveMonth);
                incentiveGiven += foundTeacherIncentive.getIncentive();
                // monthIncentiveMap.put(incentiveMonth, incentiveGiven);
                // } else
                // monthIncentiveMap.put(incentiveMonth,
                // foundTeacherIncentive.getIncentive());
            }

            List<SessionPayout> foundSessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);
            sessionPayouts = new ArrayList<SessionPayout>();

            for (SessionPayout foundSessionPayout : foundSessionPayouts) {
                studentId = foundSessionPayout.getStudentId();
                if (!studentsTaught.contains(studentId)) {
                    studentsTaught.add(studentId);
                    taughtStudents += 1;
                }
                taughtSessions += 1;
                sessionEarnings += foundSessionPayout.getTeacherPayout();
                cut += foundSessionPayout.getCut();
                sessionPayouts.add(foundSessionPayout);
            }

        } catch (Exception e) {
            logger.error(e);
        } finally {
            session.close();

        }

        response.setCut(cut);
        response.setTeacherId(teacherId);
        response.setSessionEarnings(sessionEarnings);
        response.setIncentive(incentiveGiven);
        response.setStudentsTaught(taughtStudents);
        response.setTotalSessions(taughtSessions);
        response.setTotalEarnings(sessionEarnings + incentiveGiven);
        response.setSessionPayouts(sessionPayouts);
        logger.info("Exiting " + response.toString());
        return response;

    }

    public GetTotalPayoutResponse getTotalTeacherPayout(Long teacherId)
            throws ParseException, InternalServerErrorException {

        logger.info("Entering: " + teacherId);
        SessionFactory sessionFactory = null;
        Session session = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception e) {
            logger.error(e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        Long time;
        Calendar c = Calendar.getInstance();
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int currMonth = Calendar.getInstance().get(Calendar.MONTH);
        DateFormat formatter = new SimpleDateFormat("dd-mm-yyyy hh:mm:ss");
        Date minDate;
        Long minTime;
        logger.info(currMonth);
        int previousYear = currentYear - 1;
        if (currMonth >= 3) {
            minDate = (Date) formatter.parse("01-04-" + currentYear + " " + "00:00:00");
        } else {
            minDate = (Date) formatter.parse("01-04-" + previousYear + " " + "00:00:00");
        }
        minTime = minDate.getTime();

        List<TotalMonthlyPayout> monthlyPayouts = new ArrayList<TotalMonthlyPayout>();
        GetTotalPayoutResponse response = new GetTotalPayoutResponse();
        Integer incentive;
        Integer totalEarnings;
        Integer taughtStudents;
        Integer taughtSessions;

        try {
            org.hibernate.Criteria cr;
            cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.eq("teacherId", teacherId));
            cr.add(Restrictions.ge("date", minTime));
            cr.addOrder(Order.asc("date"));
            int year, month;
            List<Integer> months = new ArrayList<Integer>();
            Map<Integer, List<Long>> monthStudentsTaughtMap = new HashMap<Integer, List<Long>>();
            Map<Integer, Integer> monthSessionsMap = new HashMap<Integer, Integer>();
            Map<Integer, Integer> monthSessionEarningsMap = new HashMap<Integer, Integer>();
            Map<Integer, Integer> monthCutMap = new HashMap<Integer, Integer>();
            Map<Integer, Integer> monthIncentiveMap = new HashMap<Integer, Integer>();

            List<Long> studentsTaught = new ArrayList<Long>();
            Integer sessionsTaught;
            Long studentId;
            Integer sessionEarnings;
            Integer cut;
            TotalMonthlyPayout monthlyPayout;

            List<SessionPayout> foundSessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);

            for (SessionPayout foundSessionPayout : foundSessionPayouts) {
                time = foundSessionPayout.getDate();
                studentId = foundSessionPayout.getStudentId();
                c.setTimeInMillis(time);
                year = c.get(Calendar.YEAR);
                month = c.get(Calendar.MONTH) + 1;
                if (year == currentYear) {
                    if (!months.contains(month)) {
                        months.add(month);
                    }

                    if (monthStudentsTaughtMap.containsKey(month)) {
                        studentsTaught = monthStudentsTaughtMap.get(month);
                        if (!studentsTaught.contains(studentId)) {
                            studentsTaught.add(studentId);
                        }
                    } else {
                        studentsTaught = new ArrayList<Long>();
                        studentsTaught.add(studentId);
                        monthStudentsTaughtMap.put(month, studentsTaught);
                    }

                    if (monthSessionsMap.containsKey(month)) {
                        sessionsTaught = monthSessionsMap.get(month);
                        sessionsTaught += 1;
                        monthSessionsMap.put(month, sessionsTaught);
                    } else {
                        monthSessionsMap.put(month, 1);
                    }

                    if (monthSessionEarningsMap.containsKey(month)) {
                        sessionEarnings = monthSessionEarningsMap.get(month);
                        sessionEarnings += foundSessionPayout.getTeacherPayout();
                        monthSessionEarningsMap.put(month, sessionEarnings);
                    } else {
                        monthSessionEarningsMap.put(month, foundSessionPayout.getTeacherPayout());
                    }

                    if (monthCutMap.containsKey(month)) {
                        cut = monthCutMap.get(month);
                        cut += foundSessionPayout.getCut();
                        monthCutMap.put(month, cut);
                    } else {
                        monthCutMap.put(month, foundSessionPayout.getCut());
                    }

                }
            }

            // Find Incentive for month and model
            List<TeacherIncentive> foundTeacherIncentives;
            Query query = new Query();
            query.addCriteria(
                    Criteria.where("teacherId").is(teacherId).andOperator(Criteria.where("creationTime").gte(minTime)));
            foundTeacherIncentives = teacherIncentiveDAO.runQuery(query, TeacherIncentive.class);
            int incentiveMonth, incentiveGiven;
            for (TeacherIncentive foundTeacherIncentive : foundTeacherIncentives) {
                incentiveMonth = foundTeacherIncentive.getMonth().ordinal() + 1;

                if (monthIncentiveMap.containsKey(incentiveMonth)) {
                    incentiveGiven = monthIncentiveMap.get(incentiveMonth);
                    incentiveGiven += foundTeacherIncentive.getIncentive();
                    monthIncentiveMap.put(incentiveMonth, incentiveGiven);
                } else {
                    monthIncentiveMap.put(incentiveMonth, foundTeacherIncentive.getIncentive());
                }
            }
            Collections.sort(months);

            for (Integer currentMonth : months) {
                incentive = 0;
                sessionEarnings = 0;
                cut = 0;
                taughtStudents = 0;
                taughtSessions = 0;
                if (monthSessionEarningsMap.containsKey(currentMonth)) {
                    sessionEarnings = monthSessionEarningsMap.get(currentMonth);
                }
                if (monthCutMap.containsKey(currentMonth)) {
                    cut = monthCutMap.get(currentMonth);
                }
                if (monthIncentiveMap.containsKey(currentMonth)) {
                    incentive = monthIncentiveMap.get(currentMonth);
                }
                totalEarnings = sessionEarnings + incentive;
                if (monthStudentsTaughtMap.containsKey(currentMonth)) {
                    taughtStudents = monthStudentsTaughtMap.get(currentMonth).size();
                }
                if (monthSessionsMap.containsKey(currentMonth)) {
                    taughtSessions = monthSessionsMap.get(currentMonth);
                }
                monthlyPayout = new TotalMonthlyPayout(currentMonth, currentYear, taughtSessions, taughtStudents,
                        sessionEarnings, cut, incentive, totalEarnings);
                monthlyPayouts.add(monthlyPayout);
                response.setCut(response.getCut() + cut);
                response.setIncentive(response.getIncentive() + incentive);
                response.setSessionEarnings(response.getSessionEarnings() + sessionEarnings);
                response.setStudentsTaught(response.getStudentsTaught() + taughtStudents);
                response.setTotalSessions(response.getTotalSessions() + taughtSessions);
                response.setTotalEarnings(response.getTotalEarnings() + totalEarnings);
            }

        } catch (Exception e) {
            logger.error(e);
        } finally {
            session.close();

        }
        response.setTeacherId(teacherId);
        response.setTeacherId(teacherId);
        response.setMonthlyPayouts(monthlyPayouts);
        logger.info("Exiting " + response.toString());
        return response;
    }

    public List<com.vedantu.dinero.entity.Transaction> getDailyTransactions() {

        Calendar now = Calendar.getInstance();
        now.set(Calendar.HOUR_OF_DAY, 0);
        now.set(Calendar.MINUTE, 0);
        now.set(Calendar.SECOND, 0);
        Long minTime = now.getTimeInMillis();
        now.set(Calendar.HOUR_OF_DAY, 23);
        now.set(Calendar.MINUTE, 59);
        now.set(Calendar.SECOND, 59);
        Long maxTime = now.getTimeInMillis();
        logger.info(minTime + "---" + maxTime);
        Query query = new Query();
        query.addCriteria(
                Criteria.where("creationTime").gte(minTime).andOperator(Criteria.where("creationTime").lte(maxTime)));

        return transactionDAO.runQuery(query, com.vedantu.dinero.entity.Transaction.class);
    }

    public Boolean uploadTeacherPayout(File payoutFile) throws InternalServerErrorException {

        logger.info("Entering ");
        String line;
        String[] inputLine;
        int count = 1;
        Query query;
        int amount;
        Long teacherId;
        String refNo;
        String reasonNote;
        int promotionalAmount, nonPromotionalAmount;

        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            transaction = session.getTransaction();
        } catch (Exception e) {
            logger.error(e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        org.hibernate.Criteria cr;

        if (payoutFile != null) {
            try (BufferedReader br = new BufferedReader(new FileReader(payoutFile))) {
                // ignoring first header line
                line = br.readLine();
                for (; (line = br.readLine()) != null;) {
                    if (!line.isEmpty()) {
                        try {
                            transaction.begin();
                            inputLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
                            // Incorrect input
                            teacherId = Long.parseLong(inputLine[0]);
                            amount = (int) (Double.parseDouble(inputLine[1]) * 100);
                            refNo = inputLine[2];
                            reasonNote = inputLine[3];

                            if (refNo == "") {
                                throw new RuntimeException("No refenrece number supplies");
                            }

                            logger.info("Processing teacher payout teacherId:" + teacherId + " amount:" + amount
                                    + " refNo:" + refNo + " reasonNote:" + reasonNote);

                            Account teacherAccount = accountManager.getAccountByUserId(teacherId);
                            teacherAccount.setBalance(teacherAccount.getBalance() - amount);

                            if (amount < teacherAccount.getPromotionalBalance()) {
                                promotionalAmount = amount;
                                nonPromotionalAmount = 0;
                            } else {
                                promotionalAmount = teacherAccount.getPromotionalBalance();
                                nonPromotionalAmount = amount - promotionalAmount;
                            }

                            teacherAccount
                                    .setPromotionalBalance(teacherAccount.getPromotionalBalance() - promotionalAmount);
                            teacherAccount.setNonPromotionalBalance(
                                    teacherAccount.getNonPromotionalBalance() - nonPromotionalAmount);

                            accountDAO.update(teacherAccount, session);

                            com.vedantu.dinero.entity.Transaction transaction1 = new com.vedantu.dinero.entity.Transaction(
                                    amount, promotionalAmount, nonPromotionalAmount,
                                    ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null,
                                    teacherAccount.getId().toString(), teacherAccount.getBalance(), BillingReasonType.ACCOUNT_DEDUCTION.name(),
                                    refNo, TransactionRefType.TEACHER_PAYOUT, reasonNote, "ADMIN-DINERO");

                            transactionDAO.create(transaction1);

                            logger.info("Debited from account successfully");
                            count++;
                        } catch (RuntimeException e) {
                            logger.warn(e);
                            logger.warn("llegal field values on entry number: " + count);
                            count++;
                        }
                    }
                }
            } catch (Exception e) {
                logger.error(e);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
            }
        }
        logger.info("Exiting");
        return true;
    }

    public Map<Long, SessionPayout> getPayoutsBySessionIds(List<Long> ids) throws InternalServerErrorException {
        logger.info("Entering ids:" + ids.toString());

        Map<Long, SessionPayout> response = new HashMap<Long, SessionPayout>();
        SessionFactory sessionFactory = null;
        Session session = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
        } catch (Exception e) {
            logger.error(e);
            session.close();
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        org.hibernate.Criteria cr;

        for (Long id : ids) {
            cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.eq("sessionId", id));
            List<SessionPayout> payouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);
            if (payouts.size() == 1) {
                response.put(id, payouts.get(0));
            }
        }

        session.close();

        logger.info("Exiting " + response.toString());
        return response;
    }

    public List<FailedPayoutInfo> getFailedPayoutsForEndedSessions() throws InternalServerErrorException, VException {

        logger.info("Entering");

        ClientResponse resp = WebUtils.INSTANCE.doCall(getEndedSessionsTodayUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
        Type listType = new TypeToken<ArrayList<FailedPayoutInfo>>() {
        }.getType();        
        List<FailedPayoutInfo> sessionList = new Gson().fromJson(jsonString, listType);        
        
        logger.info("Length of sessionList " + sessionList.size());

        List<FailedPayoutInfo> responseList = new ArrayList<>();

        List<SessionPayout> sessionPayouts;
        Map<Long, SessionPayout> sessionPayoutMap = new HashMap<>();

        for (FailedPayoutInfo endedSession : sessionList) {
            if(EntityType.COURSE_PLAN.equals(endedSession.getContextType())) {
                if (endedSession.getBillingDuration() == null) {
                    endedSession.setPayoutFound(false);
                    responseList.add(endedSession);
                } else if (endedSession.getBillingDuration() == 0) {
                    endedSession.setPayoutFound(true);
                    responseList.add(endedSession);
                }
            } else {
                sessionPayoutMap.put(endedSession.getSessionId(), null);
            }
        }

        sessionPayouts = sessionPayoutDAO.getSessionPayouts(new ArrayList<>(sessionPayoutMap.keySet()));
        logger.info("Length of found sessionPayouts " + sessionPayouts.size());
        for (SessionPayout sessionPayout : sessionPayouts) {
            sessionPayoutMap.put(sessionPayout.getSessionId(), sessionPayout);
        }

        for (Map.Entry<Long, SessionPayout> entry : sessionPayoutMap.entrySet()) {
            if (entry.getValue() == null) {
                responseList.add(new FailedPayoutInfo(entry.getKey(), false, null));
            } else if (entry.getValue().getBillingDuration() == 0L) {
                responseList.add(new FailedPayoutInfo(entry.getKey(), true, 0));
            }
        }

        logger.info("Exiting: " + responseList);
        return responseList;
    }

    public SessionPayoutResponse insertSessionPayout(SessionPayout sessionPayout)
            throws InternalServerErrorException, BadRequestException {

        logger.info("Entering " + sessionPayout.toString());
        SessionPayoutResponse successResponse = new SessionPayoutResponse();

        SessionFactory sessionFactory = null;
        Session session = null;
        Transaction transaction = null;
        try {
            sessionFactory = sqlSessionFactory.getSessionFactory();
            session = sessionFactory.openSession();
            transaction = session.getTransaction();
        } catch (Exception e) {
            logger.error(e);
            if (session != null) {
                session.close();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        org.hibernate.Criteria cr;
        try {

            transaction.begin();
            //sessionPayout.setCallingUserId("DineroSessionPayout");
            //sessionPayout.setCreatedBy("DineroSessionPayout");
            sessionPayout.setCreationTime(System.currentTimeMillis());

            cr = session.createCriteria(SessionPayout.class);
            cr.add(Restrictions.eq("sessionId", sessionPayout.getSessionId()));
            List<SessionPayout> sessionPayouts = sessionPayoutDAO.runQuery(session, cr, SessionPayout.class);

            if (sessionPayouts.size() != 0) {
                throw new Exception(
                        "Session Payout already calculated. Please use updateSessionPayout API in case of update");
            }

            sessionPayoutDAO.create(sessionPayout, session, "DineroSessionPayout");

            logger.info("Payout calculated and updated successfully");
            /*
            logger.info("Processing Session Payout");
            // Debit fos account
            Boolean fosResp = processSessionPayout(sessionPayout.getSessionId(),
                    sessionPayout.getBillingDuration(), sessionPayout.getTeacherPayout(),
                    (sessionPayout.getTeacherPayout() + sessionPayout.getCut()), true);
            if (!fosResp) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
                        "Error Procession session payout in FOS");
            }

            logger.info(fosResp.toString());
            */
            transaction.commit();

            successResponse = new SessionPayoutResponse(sessionPayout);

        } catch (Exception e) {
            logger.error(e);
            if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                logger.error("calculateSessionPayout Rollback: " + e.getMessage());
                session.getTransaction().rollback();
            }
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());

        } finally {
            session.close();
        }
        logger.info("Exiting " + successResponse.toString());
        return successResponse;
    }
}
