package com.vedantu.dinero.managers.payment;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.exception.*;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.payment.payu.PayuChecksumCalculator;
import com.vedantu.dinero.managers.payment.payu.PayuTransactionInitiationReq;
import com.vedantu.dinero.managers.payment.payu.PayuTransactionsInitiationRes;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.text.DecimalFormat;
import java.util.stream.Collectors;

@Service
public class PayuPaymentManager implements IPaymentManager {

	

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PayuPaymentManager.class);

	@Autowired
	public FosUtils fosUtils;

	@Autowired
	public ExtTransactionDAO extTransactionDAO;

	@Autowired
	public TransactionDAO transactionDAO;

	@Autowired
	public AccountDAO accountDAO;

	public static final String PAYMENT_CHANNEL = "PAYU";

	private String MERCHANT_ID;
	private String redirect_url;

	private final PaymentGatewayName gatewayName = PaymentGatewayName.PAYU;
	private String serverUrl;
	private String salt;

	public PayuPaymentManager() {
		super();
		MERCHANT_ID = ConfigUtils.INSTANCE.getStringValue("billing.payu.merchant_id");
		redirect_url = ConfigUtils.INSTANCE.getStringValue("billing.payu.redirect_url");
		serverUrl = ConfigUtils.INSTANCE.getStringValue("billing.payu.charging.url");
		salt = ConfigUtils.INSTANCE.getStringValue("billing.payu.checksum.salt");                
	}
        

	@Override
	public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException {

		PayuTransactionInitiationReq payuTransactionInitiationReq = new PayuTransactionInitiationReq();

		// required params
		payuTransactionInitiationReq.setKey(MERCHANT_ID);
		payuTransactionInitiationReq.setTxnid(transaction.getId());
                
                Float amount = transaction.getAmount().floatValue() / 100;
                DecimalFormat format = new DecimalFormat("0.00");
                String formattedamount = format.format(amount);
		payuTransactionInitiationReq.setAmount(formattedamount);
                UserBasicInfo user = fosUtils.getUserBasicInfo(transaction.getUserId().toString(), true);
		String name = user.getFullName();
		payuTransactionInitiationReq.setProductinfo("AccountRecharge");
		payuTransactionInitiationReq.setFirstname(user.getFirstName());

		payuTransactionInitiationReq.setAddress1(ConfigUtils.INSTANCE.getStringValue("billing.payu.address.default"));
		payuTransactionInitiationReq.setCity(ConfigUtils.INSTANCE.getStringValue("billing.payu.city.default"));
		payuTransactionInitiationReq.setState(ConfigUtils.INSTANCE.getStringValue("billing.payu.state.default"));
		payuTransactionInitiationReq.setCountry(ConfigUtils.INSTANCE.getStringValue("billing.payu.country.default"));
		payuTransactionInitiationReq.setEmail(user.getEmail());
		payuTransactionInitiationReq.setZipcode(Long.parseLong(ConfigUtils.INSTANCE.getStringValue("billing.payu.zip.default")));
		payuTransactionInitiationReq.setPhone(Long.parseLong(user.getContactNumber()));

		payuTransactionInitiationReq.setUdf1("");
		payuTransactionInitiationReq.setUdf2("");
		payuTransactionInitiationReq.setUdf3("");
		payuTransactionInitiationReq.setUdf4("");
		payuTransactionInitiationReq.setUdf5("");

		payuTransactionInitiationReq.setFurl(ConfigUtils.INSTANCE.getStringValue("billing.payu.redirect_url"));
		payuTransactionInitiationReq.setCurl(redirect_url);
		payuTransactionInitiationReq.setSurl(redirect_url);

		if (user.getLastName() != null) {
			payuTransactionInitiationReq.setLastname(user.getLastName());
		}

		ObjectMapper objectMapper = new ObjectMapper();

		@SuppressWarnings("unchecked")
		Map<String, Object> httpParams = objectMapper.convertValue(payuTransactionInitiationReq, Map.class);
		httpParams.put("amount", payuTransactionInitiationReq.getAmount().toString());
		try {

			String checksum = PayuChecksumCalculator.calculateChecksum(
					PayuChecksumCalculator.getStringForChecksumBeforeTransaction(payuTransactionInitiationReq));
			if (!StringUtils.isEmpty(checksum)) {
				payuTransactionInitiationReq.setHash(checksum);
				httpParams.put("hash", checksum);
				transaction.setChecksum(checksum);
				extTransactionDAO.create(transaction, callingUserId);
				logger.info("Transaction:" + transaction);
			} else {
				throw new IllegalArgumentException("Checksum generation failed");
			}
		} catch (Exception e) {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Checksum generation failed");
		}
//		httpParams.put("checkSumString",
//				PayuChecksumCalculator.getStringForChecksumBeforeTransaction(payuTransactionInitiationReq));
		String url = serverUrl;
		RechargeUrlInfo rechargeUrlInfo = new RechargeUrlInfo(PaymentGatewayName.PAYU, url, "POST", httpParams);
		return rechargeUrlInfo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId)
			throws NotFoundException, InternalServerErrorException, ConflictException, BadRequestException {
		// VERIFY THE RESPONSE FROM SERVER
		verifyRequest(transactionInfo, callingUserId);


		Map<String, Object> paramMap = new LinkedHashMap<>();
		Object[] hash = ((List<String>) transactionInfo.get("hash")).toArray();
		Object[] txnids =((List<String>) transactionInfo.get("txnid")).toArray();
		paramMap.put("txnid", txnids[0].toString());

		String txnid = txnids[0].toString();
		ExtTransaction extTransaction = null;
		logger.info("Hash: " + hash);

		Object[] emails = ((List<String>) transactionInfo.get("email")).toArray();
		paramMap.put("email", emails[0].toString());

		Object[] statuses;
		if(transactionInfo.containsKey("status") && transactionInfo.get("status") != null) {
			statuses = ((List<String>) transactionInfo.get("status")).toArray();
			paramMap.put("status", statuses[0].toString());
		} else {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
		}

		Object[] amount = ((List<String>) transactionInfo.get("amount")).toArray();
		paramMap.put("amount", amount[0].toString());

		Object[] firstname =  ((List<String>) transactionInfo.get("firstname")).toArray();
		paramMap.put("firstname", firstname[0].toString());
		Object[] productinfo = ((List<String>) transactionInfo.get("productinfo")).toArray();
		paramMap.put("productinfo", productinfo[0].toString());

		Gson gson = new Gson();

		PayuTransactionsInitiationRes transactionsInitiationRes = gson.fromJson(gson.toJson(paramMap),
				PayuTransactionsInitiationRes.class);

		transactionsInitiationRes.setKey(MERCHANT_ID);
		if (txnid != null) {
			extTransaction = extTransactionDAO.getById(txnid);
			boolean checksumVerified;
			try {
				checksumVerified = PayuChecksumCalculator.verifyChecksum(
						PayuChecksumCalculator.getStringForChecksumAfterTransaction(transactionsInitiationRes),
						hash[0].toString());
			} catch (Exception e) {
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
			}
			if (StringUtils.isEmpty(hash[0].toString()) || extTransaction == null || !checksumVerified) {
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
			}
		} else {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
		}

		TransactionStatus transactionStatus = null;
		if (transactionInfo.get("Error") != null) {
			List<String> responseCodes =(List<String>) transactionInfo.get("Error");
		}
		List<String> unmappedStatus = null;
		if (transactionInfo.get("unmappedstatus") != null) {
			unmappedStatus = (List<String>) transactionInfo.get("unmappedstatus");
		}
		
		if (statuses[0].equals("success")) {
			transactionStatus = TransactionStatus.SUCCESS;
		} else if (unmappedStatus != null && unmappedStatus.get(0).equals("userCancelled")) {
			transactionStatus = TransactionStatus.CANCELLED;
		} else {
			transactionStatus = TransactionStatus.FAILED;
		}

		//Object[] paymentInstrument = ((List<String>) transactionInfo.get("mode")).toArray();

		//Object[] bankRefNo =  ((List<String>) transactionInfo.get("bank_ref_num")).toArray();

		//Object[] PG_TYPE =  ((List<String>) transactionInfo.get("PG_TYPE")).toArray();
                
                
                String paymentInstrument=null;
                String bankRefNo=null;
                String PG_TYPE=null;
                
                if(transactionInfo.get("mode")!=null) {
                    List<String> paymentInstruments = (List<String>) transactionInfo.get("mode");
                    if(paymentInstruments!=null && !paymentInstruments.isEmpty()) {
                        paymentInstrument = paymentInstruments.get(0);
                    }
                }
                
                if(transactionInfo.get("bank_ref_num")!=null) {
                    List<String> bankRefNos = (List<String>) transactionInfo.get("bank_ref_num");
                    if(bankRefNos!=null && !bankRefNos.isEmpty()) {
                        bankRefNo = bankRefNos.get(0);
                    }
                }
                
                if(transactionInfo.get("PG_TYPE")!=null) {
                    List<String> PG_TYPEs = (List<String>) transactionInfo.get("PG_TYPE");
                    if(PG_TYPEs!=null && !PG_TYPEs.isEmpty()) {
                        PG_TYPE = PG_TYPEs.get(0);
                    }
                }

		int amountPaid = (int) (Float.parseFloat(amount[0].toString()) * 100);

		if (transactionStatus != TransactionStatus.SUCCESS) {
			amountPaid = 0;
		}


		// Map<String, Object> merchantParamMap = StringUtils.toKeyValueMap(
		// (String) resParamMap.get("merchant_param1"), "#",
		// MERCHANT_PARAM_VALUE_SAPERATOR);
		//
		// logger.info("merchantParamMap : " + merchantParamMap);
		logger.info("transaction status : " + transactionStatus);

		logger.info("starting transaction ");
		logger.info("extTransaction before updation : " + extTransaction);
		// ExtTransactionDAO.INSTANCE
		// .getById(transactionId);

		if (extTransaction.getStatus() != TransactionStatus.PENDING) {

			throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
					"transaction with id " + txnid + " is already processed");
		}
		
		// Save output to DB
                extTransaction.setGatewayResponse(transactionInfo.toString());
		
		extTransaction.setStatus(transactionStatus);
		extTransaction.setGatewayStatus(Arrays.stream(statuses).map(Object::toString).collect(Collectors.joining(",")));
		extTransaction.setTransactionTime(String.valueOf(System.currentTimeMillis()));
                extTransaction.setPaymentChannelTransactionId(txnid);
                extTransaction.setPaymentInstrument(paymentInstrument);
                extTransaction.setPaymentMethod(PG_TYPE);
                extTransaction.setBankRefNo(bankRefNo);
                logger.info("saving extTransaction: " + extTransaction);
                extTransactionDAO.create(extTransaction, callingUserId);                
		if (extTransaction.getAmount() == amountPaid && transactionStatus == TransactionStatus.SUCCESS) {
			// TODO: these functionality can be moved to abstract class
			// transaction = TransactionDAO.INSTANCE
			// .upsertTransaction(transaction);

			Account account = null;
			if (transactionStatus == TransactionStatus.SUCCESS) {
				// TODO: take this code to common place, where update and
				// deduct
				// operation can be done
				logger.info("transaction was successful:" + extTransaction);
				account = accountDAO
						.getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
				if (account == null) {
					logger.error("no account found for userid:" + extTransaction.getUserId());

					throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
							"no account found for userId:" + extTransaction.getUserId());
				}
				logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

				account.setBalance(account.getBalance() + amountPaid);
				account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

				logger.info("new account balance : " + account.getBalance());
                                
                                accountDAO.updateWithoutSession(account, null);

				com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
						amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
						BillingReasonType.RECHARGE.name(), extTransaction.getId(),
						TransactionRefType.EXTERNAL_TRANSACTION, null, 
						com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
				transactionDAO.create(vedantuTransaction);

			}
		} 

		return extTransaction;
	}

	@Override
	public PaymentGatewayName getName() {
		return this.gatewayName;
	}
}
