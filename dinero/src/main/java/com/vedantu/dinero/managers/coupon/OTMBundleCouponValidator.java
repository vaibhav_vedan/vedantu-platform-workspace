package com.vedantu.dinero.managers.coupon;

import org.springframework.stereotype.Service;

@Service
public class OTMBundleCouponValidator extends AbstractOTMBundleCouponValidator {

    @Override
    public boolean supportsOTFBundle() {
        return true;
    }

}
