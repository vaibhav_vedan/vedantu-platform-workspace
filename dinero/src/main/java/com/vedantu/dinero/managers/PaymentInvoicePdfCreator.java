/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.lowagie.text.DocumentException;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.PaymentInvoice;
import com.vedantu.dinero.entity.PaymentInvoiceTypeDetails;
import com.vedantu.dinero.enums.InvoiceStateCode;
import com.vedantu.dinero.managers.aws.AwsS3Manager;
import com.vedantu.dinero.pojo.TaxPercentage;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author jeet
 */
@Service
public class PaymentInvoicePdfCreator {

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentInvoicePdfCreator.class);

    private final String unitsArray[] = {"Zero", "One", "Two", "Three", "Four", "Five", "Six",
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
        "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
        "Eighteen", "Nineteen"};
    private final String tensArray[] = {"Zero", "Ten", "Twenty", "Thirty", "Forty", "Fifty",
        "Sixty", "Seventy", "Eighty", "Ninety"};
    public String htmlFileName = ConfigUtils.INSTANCE.getStringValue("vedantu.invoices.vedantu_student.html");

    public void createPaymnetInvoiceStudentPdf(PaymentInvoice invoice) throws VException, FileNotFoundException, DocumentException, IOException {
        File pdfFile = null;
        try {
            HashMap<String, Object> bodyScopes = getHtmlBodyScope(invoice);
            String fileName = "/tmp/" + invoice.getId() + ".pdf";
            OutputStream out = new FileOutputStream(fileName);
            ITextRenderer renderer = new ITextRenderer();
            String content = getHTMLContent(bodyScopes, htmlFileName);
            renderer.setDocumentFromString(content);
            renderer.layout();
            renderer.createPDF(out);
            out.close();
            pdfFile= new File(fileName);
            String key = invoice.getId() + ".pdf";
            awsS3Manager.uploadFile(AwsS3Manager.INVOICES_BUCKET, key, pdfFile);
        } catch (Exception e) {
           throw e;
        } finally {
           if(pdfFile != null){
               pdfFile.delete();
           }
        }
    }

    private HashMap<String, Object> getHtmlBodyScope(PaymentInvoice invoice) {
        UserBasicInfo student = invoice.getToUser();
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name", student.getFullName());
        bodyScopes.put("address", invoice.getAddress());
        bodyScopes.put("email", student.getEmail());
        bodyScopes.put("contactNo", student.getContactNumber());
        bodyScopes.put("userId", student.getUserId());
        bodyScopes.put("invoiceNo", invoice.getInvoiceNo());
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        String strDate = formatter.format(invoice.getInvoiceTime());
        bodyScopes.put("invoiceDate", strDate);
        InvoiceStateCode state = invoice.getInvoiceStateCode();
        if (state != null) {
            bodyScopes.put("state", invoice.getInvoiceStateCode().getStrVal());
            bodyScopes.put("stateCode", invoice.getInvoiceStateCode().getNumVal());
        }
        String pattern = "###,##,##0.00";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);
        float amount = invoice.getTaxableAmount() / 100;
        String taxableAmount = decimalFormat.format(amount);
        bodyScopes.put("taxableAmount", taxableAmount);
        PaymentInvoiceTypeDetails details = invoice.getPaymentInvoiceTypeDetails();
        if (details != null) {
            List<TaxPercentage> taxPercentages = details.getTaxPercentages();
            if (ArrayUtils.isNotEmpty(taxPercentages)) {
                for (TaxPercentage taxPercentage : taxPercentages) {
                    if (null != taxPercentage.getTaxType()) {
                        switch (taxPercentage.getTaxType()) {
                            case CGST: {
                                amount = taxPercentage.getAmount() / 100;
                                String amountStr = decimalFormat.format(amount);
                                bodyScopes.put("cgstAmount", amountStr);
                                break;
                            }
                            case IGST: {
                                amount = taxPercentage.getAmount() / 100;
                                String amountStr = decimalFormat.format(amount);
                                bodyScopes.put("igstAmount", amountStr);
                                break;
                            }
                            case SGST: {
                                amount = taxPercentage.getAmount() / 100;
                                String amountStr = decimalFormat.format(amount);
                                bodyScopes.put("sgstAmount", amountStr);
                                break;
                            }
                            default:
                                break;
                        }
                    }
                }
            }
        }
        amount = invoice.getTotalAmount() / 100;
        String amountStr = decimalFormat.format(amount);
        bodyScopes.put("totalAmount", amountStr);
        bodyScopes.put("amountWords", numberToWord((int) amount));
        return bodyScopes;
    }

    private String numberToWord(Integer number) {
        String words = "";
        if (number == 0) {
            return "zero";
        }
        if ((number / 100000) > 0) {
            words += numberToWord(number / 100000) + " Lakh ";
            number %= 100000;
        }
        if ((number / 1000) > 0) {
            words += numberToWord(number / 1000) + " Thousand ";
            number %= 1000;
        }
        if ((number / 100) > 0) {
            words += numberToWord(number / 100) + " Hundred ";
            number %= 100;
        }

        if (number > 0) {
            if (number < 20) {
                words += unitsArray[number];
            } else {
                words += tensArray[number / 10];
                if ((number % 10) > 0) {
                    words += "-" + unitsArray[number % 10];
                }
            }
        }

        return words;
    }

    private String getHTMLContent(Map<String, Object> scopes, String fileName) throws VException, IOException {

        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        InputStream is = null;
        final String confDir = "htmlTemplates";

        String propertiesFilePath = confDir + java.io.File.separator + fileName;

        try {
            is = ConfigUtils.class.getClassLoader().getResourceAsStream(propertiesFilePath);
            Mustache mustache = mf.compile(new InputStreamReader(is, "UTF-8"), "emailContent");
            mustache.execute(writer, scopes);
            writer.flush();
        } catch (Exception e) {
            logger.info("error in mustache:" + e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return writer.toString();
    }
}
