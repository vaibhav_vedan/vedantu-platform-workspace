package com.vedantu.dinero.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vedantu.dinero.enums.PaymentInvoiceType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.LocationInfo;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.PaymentInvoice;
import com.vedantu.dinero.entity.PaymentInvoiceTypeDetails;
import com.vedantu.dinero.enums.InvoiceStateCode;
import com.vedantu.dinero.enums.PaymentInvoiceContextType;
import com.vedantu.dinero.enums.TaxType;
import com.vedantu.dinero.pojo.PaymentInvoiceBasicInfo;
import com.vedantu.dinero.pojo.PaymentInvoiceInfo;
import com.vedantu.dinero.pojo.PaymentInvoiceUserInfo;
import com.vedantu.dinero.pojo.TaxPercentage;
import com.vedantu.dinero.request.AddPaymentInvoiceReq;
import com.vedantu.dinero.request.CreatePaymentInvoiceReq;
import com.vedantu.dinero.request.GetPaymentInvoiceReq;
import com.vedantu.dinero.serializers.PaymentInvoiceDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.IPUtil;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.util.Arrays;
import org.apache.commons.lang3.EnumUtils;
import org.json.JSONObject;
import org.springframework.http.HttpMethod;
import org.dozer.DozerBeanMapper;

@Service
public class PaymentInvoiceManager {

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private PaymentInvoicePdfCreator paymentInvoicePdfCreator;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private PaymentInvoiceDAO paymentInvoiceDAO;

    @Autowired
    private IPUtil ipUtil;

    @Autowired
    private InstalmentAsyncTasksManager instalmentAsyncTasksManager;

    @Autowired
    private InvoiceNumberGenerator invoiceNumberGenerator;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings({"static-access"})
    private Logger logger = logFactory.getLogger(PaymentInvoiceManager.class);

    private final String apigurusUrl = ConfigUtils.INSTANCE.getStringValue("apigurus.url");
    @Autowired
    private DozerBeanMapper mapper;

    public PlatformBasicResponse updatePaymentInvoiceTypeDetails(PaymentInvoiceTypeDetails paymentInvoiceTypeDetails)
            throws BadRequestException {
        // Validate request
        if (paymentInvoiceTypeDetails.getType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Payment invoice type is mandatory.");
        }

        // Check if there is an active one for the same type
        PaymentInvoiceTypeDetails existingEntry = paymentInvoiceDAO
                .getPaymentInvoiceTypeDetailsByType(paymentInvoiceTypeDetails.getType());
        if (existingEntry == null) {
            // Create a new entry
            paymentInvoiceDAO.createPaymentInvoiceTypeDetails(paymentInvoiceTypeDetails);
        } else {
            // Update the existing entry
            logger.info("Pre update: " + existingEntry.toString());
            existingEntry.updateEntry(paymentInvoiceTypeDetails);
            paymentInvoiceDAO.createPaymentInvoiceTypeDetails(existingEntry);
            logger.info("Post update:" + existingEntry.toString());
        }

        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse createPaymentInvoice(AddPaymentInvoiceReq req)
            throws BadRequestException, InternalServerErrorException {
        // Validate the req
        req.verify();

        // Create PaymentInvoice
        PaymentInvoice paymentInvoice = new PaymentInvoice(req);

        // Fetch the tax details
        PaymentInvoiceTypeDetails typeDetails = paymentInvoiceDAO
                .getPaymentInvoiceTypeDetailsByType(req.getPaymentInvoiceType());
        if (typeDetails == null) {
            String errorMessage = "No entries found for invoice type : " + req.getPaymentInvoiceType() + " req:"
                    + req.toString();
            logger.error(errorMessage);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
        }

        // Update total amount
        if (req.getTotalAmount() != null) {
            paymentInvoice.updateTotalAmount(req.getTotalAmount(), typeDetails);
        }
        if (req.getActualAmount() != null) {
            paymentInvoice.updateActualAmount(req.getActualAmount(), typeDetails);
        }

        // Update the user details based on the invoice type
        paymentInvoice.updateUserIds(req);
        String invoiceNumber = invoiceNumberGenerator.getInvoiceNumber(req.getPaymentInvoiceType(), paymentInvoice.getInvoiceTime(), req.getTeacherId());
        paymentInvoice.setInvoiceNo(invoiceNumber);
        // upsert invoice
        paymentInvoiceDAO.create(paymentInvoice);

        return new PlatformBasicResponse();
    }

    public PaymentInvoiceInfo getPaymentInvoiceInfoById(String id) throws VException {
        PaymentInvoice paymentInvoice = getPaymentInvoiceById(id);
        PaymentInvoiceInfo paymentInvoiceInfo = mapper.map(paymentInvoice, PaymentInvoiceInfo.class);

        // Fill user info
        Set<Long> userIds = new HashSet<>();
        Long fromUserId = paymentInvoiceInfo.getFromUserId();
        Long toUserId = paymentInvoiceInfo.getToUserId();
        if (fromUserId != null && fromUserId > 0l) {
            userIds.add(fromUserId);
        }
        if (toUserId != null && toUserId > 0l) {
            userIds.add(toUserId);
        }

        Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, false);
        if (fromUserId != null && fromUserId > 0l && userInfos.containsKey(fromUserId)) {
            try {
                paymentInvoiceInfo
                        .setFromUserInfo(constructPaymentInvoiceUserInfo(fromUserId, userInfos.get(fromUserId)));
            } catch (VException ex) {
                logger.error("fromUserIdError occured fetching account details : " + id + " ex:" + ex.getMessage());
                throw ex; // Continuing might cause partial invoice generation
            }
        }

        if (toUserId != null && toUserId > 0l && userInfos.containsKey(toUserId)) {
            try {
                paymentInvoiceInfo.setToUserInfo(constructPaymentInvoiceUserInfo(toUserId, userInfos.get(toUserId)));
            } catch (VException ex) {
                logger.error("toUserIdError occured fetching account details : " + id + " ex:" + ex.getMessage());
                throw ex; // Continuing might cause partial invoice generation
            }
        }

        return paymentInvoiceInfo;
    }

    public PaymentInvoice getPaymentInvoiceById(String id) throws BadRequestException, NotFoundException {
        PaymentInvoice paymentInvoice = paymentInvoiceDAO.getById(id);
        if (paymentInvoice == null) {
            throw new NotFoundException(ErrorCode.BAD_REQUEST_ERROR, "Payment invoice not found for id : " + id);
        }

        return paymentInvoice;
    }

    public List<PaymentInvoice> getPaymentInvoices(GetPaymentInvoiceReq req) throws BadRequestException {
        // Handle userId
        if (!Role.ADMIN.equals(req.getCallingUserRole())) {
            req.setUserId(req.getCallingUserId());
        }

        // Validate request
        req.verify();

        // Should be valid only for student as of now
        // User user = fosUtils.getUserInfo(req.getUserId());
        // if (!Role.STUDENT.equals(user.getRole())) {
        // throw new BadRequestException(ErrorCode.INVOICE_ROLE_NOT_SUPPORTED,
        // "Invoice is only supported for student");
        // }
        return paymentInvoiceDAO.getPaymentInvoices(req);
    }

    public List<PaymentInvoiceBasicInfo> getPaymentInvoicesBasicInfos(GetPaymentInvoiceReq req)
            throws BadRequestException {
        List<PaymentInvoice> invoices = getPaymentInvoices(req);
        List<PaymentInvoiceBasicInfo> results = new ArrayList<>();
        for (PaymentInvoice invoice : invoices) {
            results.add(mapper.map(invoice, PaymentInvoiceBasicInfo.class));
        }

        return results;
    }

    private PaymentInvoiceUserInfo constructPaymentInvoiceUserInfo(Long userId, User user)
            throws InternalServerErrorException, ConflictException, BadRequestException {
        PaymentInvoiceUserInfo paymentInvoiceUserInfo = mapper.map(user, PaymentInvoiceUserInfo.class);
        if (Role.TEACHER.equals(user.getRole())) {
            Account account = accountManager.getAccountByUserId(userId);
            paymentInvoiceUserInfo.updateAdditionalDetails(user, account.getPanCard());
        }

        return paymentInvoiceUserInfo;
    }

    public void generateInvoiceNumbers(Integer start, Integer size, Boolean all) {
        while (true) {

            List<PaymentInvoice> results = paymentInvoiceDAO.getPaymentInvoices(start, size);

            if (results == null || results.isEmpty()) {
                break;
            }

            for (PaymentInvoice paymentInvoice : results) {
                logger.info("paymentInvoice: " + paymentInvoice);
                try {

                    PaymentInvoiceType paymentInvoiceType = paymentInvoice.getPaymentInvoiceTypeDetails().getType();
                    Long teacherId = null;
                    if (PaymentInvoiceType.TEACHER_STUDENT.equals(paymentInvoiceType)) {
                        teacherId = paymentInvoice.getFromUserId();
                    }

                    String invoiceNumber = invoiceNumberGenerator.getInvoiceNumber(paymentInvoiceType, paymentInvoice.getInvoiceTime(), teacherId);
                    paymentInvoice.setInvoiceNo(invoiceNumber);
                    // upsert invoice
                    paymentInvoiceDAO.create(paymentInvoice);

                } catch (Exception ex) {
                    logger.error("Exception in invoice: " + paymentInvoice, ex);
                }

            }
            start += size;
            if (!all) {
                break;
            }
        }

    }

    public void createStudentPaymentInvoice(CreatePaymentInvoiceReq req) throws Exception {
        req.verify();
        PaymentInvoice invoice = new PaymentInvoice();
        invoice.setContextId(req.getContextId());
        invoice.setContextType(req.getContextType());
        invoice.setToUserId(req.getUserId());
        invoice.setToUserRole(Role.STUDENT);
        invoice.setFromUserRole(Role.ADMIN);
        LocationInfo loc = null;
        User userInfo = fosUtils.getUserInfo(req.getUserId(), true);
        if (StringUtils.isNotEmpty(req.getIpAddress())) {
            String[] ips = req.getIpAddress().split(",");
            loc = ipUtil.getLocationFromIp(ips[0]);
        }
        if (loc == null || (StringUtils.isEmpty(loc.getState()) || (StringUtils.isNotEmpty(loc.getState()) && loc.getState().equals("-")))) {
            loc = new LocationInfo();
            if (userInfo.getLocationInfo() == null || StringUtils.isEmpty(userInfo.getLocationInfo().getState())) {
                loc.setCity("Bangalore");
                loc.setState("Karnataka");
                loc.setPincode("560102");
                loc.setCountry("India");
            } else {
                loc.setCity(userInfo.getLocationInfo().getCity());
                loc.setState(userInfo.getLocationInfo().getState());
                loc.setCountry(userInfo.getLocationInfo().getCountry());
                loc.setPincode(userInfo.getLocationInfo().getPincode());
            }
        }

        if (StringUtils.isNotEmpty(loc.getCountry()) && loc.getCountry().toUpperCase().equals("IN")) {
            loc.setCountry("India");
        }

        if (StringUtils.isEmpty(loc.getCountry()) || !loc.getCountry().toUpperCase().equals("INDIA")) {
            loc.setCity("Bangalore");
            loc.setState("Karnataka");
            loc.setPincode("560102");
            loc.setCountry("India");
        }

        if (loc.getCountry().toUpperCase().equals("INDIA") && (StringUtils.isEmpty(loc.getState()) || (StringUtils.isNotEmpty(loc.getState()) && loc.getState().equals("-")))) {
            loc.setCity("Bangalore");
            loc.setState("Karnataka");
            loc.setPincode("560102");
            loc.setCountry("India");
        }

        String address = "";
        if (StringUtils.isNotEmpty(loc.getCity())) {
            address = address + loc.getCity();
        }

        if (StringUtils.isNotEmpty(loc.getState())) {
            if (StringUtils.isNotEmpty(address)) {
                address = address + ", ";
            }
            address = address + loc.getState();
        }

        if (StringUtils.isNotEmpty(loc.getCountry())) {
            if (StringUtils.isNotEmpty(address)) {
                address = address + ", ";
            }
            address = address + loc.getCountry();
        }

        if (StringUtils.isNotEmpty(loc.getPincode())) {
            if (StringUtils.isNotEmpty(address)) {
                address = address + ", ";
            }
            address = address + loc.getPincode();
        }

        String[] arr = {"HYDERABAD", "ADILABAD", "KARIMNAGAR", "WARANGAL", "NIZAMABAD", "NALGONDA", "MAHBUBNAGAR"};
        List<String> telanganaCities = Arrays.asList(arr);

        if (StringUtils.isNotEmpty(loc.getCity()) && telanganaCities.contains(loc.getCity().toUpperCase())) {
            loc.setState("TELANGANA");
        }

        invoice.setSupplyState(loc.getState());
        invoice.setCountry(loc.getCountry());

        invoice.setAddress(address);
        UserBasicInfo student = fosUtils.getUserBasicInfo(invoice.getToUserId(), true);
        invoice.setToUser(student);
        InvoiceStateCode invoiceStateCode = getStateFromLocationData(loc);
        invoice.setInvoiceStateCode(invoiceStateCode);
        float totalTax = getPaymentInvoiceTaxByState(invoiceStateCode);
        PaymentInvoiceTypeDetails details = getPaymentInvoiceTypeDetailsByState(invoiceStateCode);
        Long taxableAmount = (long) (Math.round(req.getAmount() * 100 / (100 + totalTax) / 100.0) * 100);
        long totalTaxAmount = req.getAmount() - taxableAmount;
        for (TaxPercentage taxPercentage : details.getTaxPercentages()) {
            if (taxPercentage.getPercentage() == 9.0) {
                taxPercentage.setAmount(totalTaxAmount / 2);
            } else if (taxPercentage.getPercentage() == 18.0) {
                taxPercentage.setAmount(totalTaxAmount);
            }
        }
        invoice.setPaymentInvoiceTypeDetails(details);
        invoice.setTaxableAmount(taxableAmount);
        invoice.setTotalAmount((long) req.getAmount());
        String invoiceNumber = invoiceNumberGenerator.getVedantuStudentInvoiceNumber(req.getInvoiceTime(), req.getGatewayName());
        if (StringUtils.isEmpty(invoiceNumber)) {
            throw new VException(ErrorCode.SERVICE_ERROR, "not able to generate student invoice no:");
        }
        invoice.setInvoiceTime(req.getInvoiceTime());
        invoice.setInvoiceNo(invoiceNumber);
        paymentInvoiceDAO.create(invoice);
        if (PaymentInvoiceContextType.EXTRANSACTION.equals(req.getContextType())) {
            instalmentAsyncTasksManager.updateLocationValuesInExtTransaction(req.getContextId(), loc.getCity(), loc.getState(), loc.getCountry());
        }
//        paymentInvoicePdfCreator.createPaymnetInvoiceStudentPdf(invoice);
    }

    public InvoiceStateCode getStateFromLocationData(LocationInfo loc) throws VException {
        String country = loc.getCountry();
        if (StringUtils.isEmpty(country)) {
            throw new VException(ErrorCode.SERVICE_ERROR, "not able to identify country from  loc:" + loc);
        }
        if (!country.toUpperCase().equals("INDIA")) {
            return InvoiceStateCode.INTERNATIONAL;
        }
        String state = loc.getState();
        if (StringUtils.isEmpty(state)) {
            throw new VException(ErrorCode.SERVICE_ERROR, "not able to identify state from  loc:" + loc);
        }
        state = state.replaceAll("\\s", "")
                .replaceAll("\\p{P}", "")
                .toUpperCase();
        if (!EnumUtils.isValidEnum(InvoiceStateCode.class, state)) {
            throw new VException(ErrorCode.SERVICE_ERROR, "not able to identify state from loc:" + loc + " state:" + state);
        }
        return InvoiceStateCode.valueOf(state);
    }

    public PaymentInvoiceTypeDetails getPaymentInvoiceTypeDetailsByState(InvoiceStateCode state) {
        PaymentInvoiceTypeDetails response = new PaymentInvoiceTypeDetails();
        response.setType(PaymentInvoiceType.VEDANTU_STUDENT);
        List<TaxPercentage> taxPercentages = new ArrayList<>();
        switch (state.getNumVal()) {
            case 29:
                taxPercentages.add(new TaxPercentage(TaxType.CGST, 9));
                taxPercentages.add(new TaxPercentage(TaxType.SGST, 9));
                taxPercentages.add(new TaxPercentage(TaxType.IGST, 0));
                break;
            case 0:
                taxPercentages.add(new TaxPercentage(TaxType.CGST, 0));
                taxPercentages.add(new TaxPercentage(TaxType.SGST, 0));
                taxPercentages.add(new TaxPercentage(TaxType.IGST, 0));
                break;
            default:
                taxPercentages.add(new TaxPercentage(TaxType.CGST, 0));
                taxPercentages.add(new TaxPercentage(TaxType.SGST, 0));
                taxPercentages.add(new TaxPercentage(TaxType.IGST, 18));
                break;
        }
        response.setTaxPercentages(taxPercentages);
        return response;
    }

    public float getPaymentInvoiceTaxByState(InvoiceStateCode state) {
        if (state.getNumVal() == 0) {
            return 0;
        }
        return 18;
    }
}
