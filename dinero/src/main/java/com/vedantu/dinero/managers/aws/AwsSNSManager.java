/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.aws;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.*;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

/**
 *
 * @author somil
 */
@Service
public class AwsSNSManager extends AbstractAwsSNSManager{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);

    

    private String env;
    private String arn;
    private AmazonSNSAsync snsClient;
    private String snsArnBuilder;
    
    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public AwsSNSManager() {
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        snsArnBuilder = ConfigUtils.INSTANCE.getStringValue("aws.sns.arn.builder");
        snsClient = AmazonSNSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
        logger.info("initializing AwsSNSManager");
        try {
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                //creating SNS topic CMS_WEBINAR_EVENTS
                CreateTopicRequest createTopicRequest = new CreateTopicRequest(SNSTopic.CMS_WEBINAR_EVENTS.getTopicName(env));
                snsClient.createTopic(createTopicRequest);
            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSNSManager " + e.getMessage());
        }
        
    }
    
    
    @Override
    public void triggerSNS(SNSTopic topic, String subject, String message) {
        String topicArn = getTopicArn(topic); 
        PublishRequest publishRequest = new PublishRequest(topicArn, message, subject);
        logger.info("publishRequest "+publishRequest);
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }               
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }
    
    @Override
    public String getTopicArn(SNSTopic topic) {
        return String.format(snsArnBuilder,Regions.AP_SOUTHEAST_1.getName(), arn, topic.getTopicName(env));
    }
    
    public SubscribeResult createSubscription(SNSTopic topic, String type, String typeArn) {
        SubscribeRequest subscribeRequest = new SubscribeRequest(getTopicArn(topic), type, typeArn);
        SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
        return subscribeResult;
    }
    
    @Override
    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSNSManager connection ", e);
        }
    }              

    @Override
    public void createTopics() {
        createSNSTopic(SNSTopic.EXPORT_ORDERS_CSV_SNS);
        createSNSTopic(SNSTopic.DINERO_REDIS_OPS);
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createSubscriptions() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.


        createCronSubscription(CronTopic.CRON_CHIME_DAILY_3PM_IST, "report/get/ext-transaction/status/alert");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "payment/ccavenue/check/status");
        createCronSubscription(SNSTopic.EXPORT_ORDERS_CSV_SNS, "/payment/exportOrdersCsvFromSNS");
        createCronSubscription(SNSTopic.DINERO_REDIS_OPS, "misc/performRedisOps");
    }
}
