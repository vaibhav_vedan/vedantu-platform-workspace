/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import com.vedantu.exception.VException;
import java.util.List;

/**
 *
 * @author ajith
 */
public interface ICouponValidator {

    //purchasable entities
    boolean supportsBatch();

    List<String> getBatchIds(String purchasingEntityId);

    boolean supportsPlan();

    List<String> getPlanIds(String purchasingEntityId);

    boolean supportsTeacher();

    List<String> getTeacherIds(String purchasingEntityId);

    boolean supportsCourse();

    List<String> getCourseIds(String purchasingEntityId);

    boolean supportsCoursePlan();

    List<String> getCoursePlanIds(String purchasingEntityId);

    //their metadata
    boolean supportsSubject();

    List<String> getSubjects(String purchasingEntityId);

    boolean supportsCategory();

    List<String> getCategories(String purchasingEntityId) throws VException;

    boolean supportsGrade();

    List<String> getGrades(String purchasingEntityId) throws VException;

    boolean supportsBundle();

    List<String> getBundleIds(String purchasingEntityId);

    boolean supportsBundlePackage();

    List<String> getBundlePackageIds(String purchasingEntityId);

    Integer getTotalAmount(String purchasingEntityId) throws VException;

    List<String> getOTFBundleIds(String purchasingEntityId);
    List<String> getValidMonths(String planId);

    boolean supportsOTFBundle();

    public boolean supportsCoursePlanRegistration();
            
    public boolean supportsBatchRegistration();
    
    public boolean supportsCourseRegistration();
    
    public boolean supportsOTMBundleRegistration();

}
