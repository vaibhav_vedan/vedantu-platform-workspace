/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers;

import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.Refund;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.RefundPolicy;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.request.GetOrderDetailsReq;
import com.vedantu.dinero.request.RefundReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.serializers.OrdersDAO;
import com.vedantu.dinero.serializers.RefundDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.exception.*;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class RefundManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RefundManager.class);

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AccountManager accountManager;

    @Autowired
    public RefundDAO refundDAO;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public OrdersDAO ordersDAO;

    @Autowired
    private AccountDAO accountDAO;

    public RefundRes refund(RefundReq refundReq)
            throws VException {
        refundReq.verify();
        RefundRes res = new RefundRes();
        switch (refundReq.getEntityType()) {
            case OTO_COURSE_REGISTRATION:
            case OTF_COURSE_REGISTRATION:
            case COURSE_PLAN_REGISTRATION:
            case OTM_BUNDLE_REGISTRATION:
            case OTM_BUNDLE_ADVANCE_PAYMENT:
                List<String> orderIds = new ArrayList<>();
                List<Orders> orders = null;
                if (StringUtils.isEmpty(refundReq.getOrderId())) {
                    if (StringUtils.isEmpty(refundReq.getDeliverableEntityId())) {
                        orders = paymentManager.getOrderFullDetails(new GetOrderDetailsReq(refundReq.getUserId(),
                                refundReq.getEntityId(), refundReq.getEntityType(), Arrays.asList(PaymentStatus.PAID)));
                    } else {
                        orders = paymentManager.getOrdersByDeliverableOrEntity(Arrays.asList(refundReq.getDeliverableEntityId()));
                    }

                } else {
                    Orders order = paymentManager.getOrderById(refundReq.getOrderId());
                    if (order == null) {
                        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "order not found with orderId " + refundReq.getOrderId());
                    }
                    orders = new ArrayList<>();
                    orders.add(order);
                }

                if (ArrayUtils.isEmpty(orders)) {
                    throw new ConflictException(ErrorCode.ORDER_NOT_FOUND,
                            "no orders found for req " + refundReq);
                }
                if (Boolean.FALSE.equals(refundReq.getRefundAllOrders()) && orders.size() > 1) {
                    throw new ConflictException(ErrorCode.MULTIPLE_ORDERS_FOUND,
                            "multiple orders found for req " + refundReq);
                }

                Integer promotionalRefund = 0,
                 nonPromotionalRefund = 0,
                 discountLeft = 0;
                for (Orders order : orders) {
                    promotionalRefund += order.getPromotionalAmount();
                    nonPromotionalRefund += order.getNonPromotionalAmount();
                    if (order.getItems().get(0).getVedantuDiscountAmount() != null) {
                        discountLeft += order.getItems().get(0).getVedantuDiscountAmount();
                    }
                    orderIds.add(order.getId());
                }
                List<Refund> refunds = refundDAO.getRefundsByOrderId(orderIds);
                if (ArrayUtils.isNotEmpty(refunds)) {
                    throw new ConflictException(ErrorCode.SOME_ORDERS_REFUNDED,
                            "some of the orders are refunded " + refunds);
                }

                TransactionRefType refType = null;
                switch (refundReq.getEntityType()) {
                    case OTO_COURSE_REGISTRATION:
                        refType = TransactionRefType.STRUCTURED_COURSE_TRIAL_REFUND;
                        break;
                    case OTF_COURSE_REGISTRATION:
                        refType = TransactionRefType.OTF_COURSE_TRIAL_REFUND;
                        break;
                    case OTF_BATCH_REGISTRATION:
                        refType = TransactionRefType.OTF_BATCH_REGISTRATION_REFUND;
                        break;
                    case OTM_BUNDLE_REGISTRATION:
                        refType = TransactionRefType.OTM_BUNDLE_REGISTRATION_REFUND;
                        break;
                    case OTM_BUNDLE_ADVANCE_PAYMENT:
                        refType = TransactionRefType.OTM_BUNDLE_ADVANCE_REFUND;
                        break;
                    default:
                        refType = TransactionRefType.COURSE_PLAN_TRIAL_REFUND;
                        break;
                }

                logger.info("promotionalRefund " + promotionalRefund);
                logger.info("nonPromotionalRefund " + nonPromotionalRefund);
                logger.info("nonPromotionalRefund " + refundReq.getDiscountLeft());

                if (!Boolean.TRUE.equals(refundReq.getGetRefundInfoOnly())) {
                    Transaction transaction = accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(),
                            accountDAO.getAccountByUserId(refundReq.getUserId()),
                            (promotionalRefund + nonPromotionalRefund), promotionalRefund, nonPromotionalRefund,
                            ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.REFUND,
                            refType, refundReq.getEntityId(),
                            (refundReq.getCallingUserId() != null) ? refundReq.getCallingUserId().toString() : null,
                            true, StringUtils.join(orderIds, "_") + "_" + BillingReasonType.REFUND.name());
                    if (discountLeft > 0) {
                        accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(),
                                accountDAO.getVedantuFreebiesAccount(),
                                refundReq.getDiscountLeft(), refundReq.getDiscountLeft(), 0,
                                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.REFUND_DISCOUNT,
                                refType, refundReq.getEntityId(),
                                (refundReq.getCallingUserId() != null) ? refundReq.getCallingUserId().toString() : null,
                                true, StringUtils.join(orderIds, "_") + "_" + BillingReasonType.REFUND_DISCOUNT.name());
                    }
                    //TODO remove db save from the loop
                    for (Orders order : orders) {
                        Integer pr = order.getPromotionalAmount();
                        Integer npr = order.getNonPromotionalAmount();
                        Refund refund = new Refund();
                        refund.setOrderId(order.getId());
                        refund.setTransactionId(transaction.getId());
                        refund.setAmount(pr + npr);
                        refund.setPromotionalAmount(pr);
                        refund.setNonPromotionalAmount(npr);
                        if (discountLeft > 0) {
                            refund.setDiscountLeft(refundReq.getDiscountLeft());
                        }
                        refund.setReason(refundReq.getReason());
                        refund.setActualRefundDate(refundReq.getActualRefundDate());
                        refundDAO.create(refund);
                        List<OrderedItem> items = order.getItems();
                        for (OrderedItem item : items) {
                            item.setRefundStatus(RefundPolicy.FULL_REFUND);
                            res.getDeliverableEntityIds().add(item.getDeliverableEntityId());
                        }
                        ordersDAO.create(order);
                        res.getPaymentTypes().add(order.getPaymentType());
                        res.getOrderIds().add(order.getId());
                    }
                }
                res.setRefundAmount(promotionalRefund + nonPromotionalRefund);
                res.setNonPromotionalAmount(nonPromotionalRefund);
                res.setPromotionalAmount(promotionalRefund);
                break;
            case COURSE_PLAN:
            case BUNDLE:
                //TODO when the actual logic for refund comes, update order with the refundStatus
                logger.info("promotionalRefund " + refundReq.getPromotionalAmount());
                logger.info("nonPromotionalRefund " + refundReq.getNonPromotionalAmount());
                logger.info("discount left  " + refundReq.getDiscountLeft());
                if ((refundReq.getOrderId() == null || StringUtils.isEmpty(refundReq.getOrderId()))
                        && !Boolean.TRUE.equals(refundReq.getGetRefundInfoOnly())) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "orderId Missing");
                }
                TransactionRefType transactionRefType = null;
                switch (refundReq.getEntityType()) {
                    case OTF:
                        transactionRefType = TransactionRefType.OTF_BATCH_ENROLLMENT_END;
                        break;
                    case COURSE_PLAN:
                        transactionRefType = TransactionRefType.END_COURSE_PLAN;
                        break;
                    case BUNDLE:
                        transactionRefType = TransactionRefType.END_AIO;
                        break;

                    default:
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "EntityType not available");
                }
                if (!Boolean.TRUE.equals(refundReq.getGetRefundInfoOnly())) {
                    Transaction transaction = accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(),
                            accountDAO.getAccountByUserId(refundReq.getUserId()),
                            (refundReq.getPromotionalAmount() + refundReq.getNonPromotionalAmount()),
                            refundReq.getPromotionalAmount(), refundReq.getNonPromotionalAmount(),
                            ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.REFUND,
                            transactionRefType, refundReq.getEntityId(),
                            (refundReq.getCallingUserId() != null) ? refundReq.getCallingUserId().toString() : null,
                            true, refundReq.getOrderId() + "_" + BillingReasonType.REFUND.name());
                    if (refundReq.getDiscountLeft() != null && refundReq.getDiscountLeft() > 0) {
                        accountManager.transferAmount(accountDAO.getVedantuDefaultAccount(),
                                accountDAO.getVedantuFreebiesAccount(),
                                refundReq.getDiscountLeft(), refundReq.getDiscountLeft(), 0,
                                ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, BillingReasonType.REFUND_DISCOUNT,
                                transactionRefType, refundReq.getEntityId(),
                                (refundReq.getCallingUserId() != null) ? refundReq.getCallingUserId().toString() : null,
                                true, refundReq.getOrderId() + "_" + BillingReasonType.REFUND_DISCOUNT.name());
                    }
                    Refund refund = new Refund();
                    refund.setOrderId(refundReq.getOrderId());
                    refund.setTransactionId(transaction.getId());
                    refund.setAmount(refundReq.getPromotionalAmount() + refundReq.getNonPromotionalAmount());
                    refund.setPromotionalAmount(refundReq.getPromotionalAmount());
                    refund.setNonPromotionalAmount(refundReq.getNonPromotionalAmount());
                    if (refundReq.getDiscountLeft() != null && refundReq.getDiscountLeft() > 0) {
                        refund.setDiscountLeft(refundReq.getDiscountLeft());
                    }
                    refund.setReason(refundReq.getReason());
                    refund.setActualRefundDate(refundReq.getActualRefundDate());
                    refundDAO.create(refund);
                }
                res.setRefundAmount(refundReq.getPromotionalAmount() + refundReq.getNonPromotionalAmount());
                res.setNonPromotionalAmount(refundReq.getNonPromotionalAmount());
                res.setPromotionalAmount(refundReq.getPromotionalAmount());
                break;
            default:
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        refundReq.getEntityType() + " refund logic is not available");
        }
        return res;
    }

    public List<Refund> getRefunds(List<String> orderIds) {
        return refundDAO.getRefundsByOrderId(orderIds);
    }

    public Refund getRefundForAdjustment(String batchId, Long userId) throws InternalServerErrorException, BadRequestException, ConflictException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Transaction.Constants.REASON_REF_TYPE).is(TransactionRefType.OTF_BATCH_ENROLLMENT_END));
        query.addCriteria(Criteria.where(Transaction.Constants.REASON_REF_NO).is(batchId));
        query.addCriteria(Criteria.where(Transaction.Constants.CREDIT_TO_ACCOUNT).is(accountDAO.getAccountByUserId(userId).getId().toString()));
        List<Transaction> transactions = transactionDAO.runQuery(query, Transaction.class);

        if (ArrayUtils.isEmpty(transactions) || (transactions.size() > 1)) {
            throw new ConflictException(ErrorCode.ACTION_NOT_ALLOWED, "");
        }

        return refundDAO.getRefundByTransactionId(transactions.get(0).getId());

    }

}
