package com.vedantu.dinero.managers.payment;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.razorpay.Customer;
import com.razorpay.Order;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.enums.TransactionRefSubType;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.CommunicationManager;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.*;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.internet.InternetAddress;
import javax.ws.rs.core.MediaType;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RazorpayPaymentManager implements
        IPaymentManager {

    private static Client client = Client.create();

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RazorpayPaymentManager.class);

    @Autowired
    public FosUtils fosUtils;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    public CommunicationManager communicationManager;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public AccountDAO accountDAO;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private RazorpayAPIManager razorpayAPIManager;

    public static final String PAYMENT_CHANNEL = "RAZORPAY";

    private String KEY_ID;
    private String KEY_SECRET;
    private String redirect_url;
    private String serverUrl;
    private final PaymentGatewayName gatewayName = PaymentGatewayName.RAZORPAY;

    public RazorpayPaymentManager() {
        KEY_ID = ConfigUtils.INSTANCE.getStringValue("billing.razorpay.key_id");
        KEY_SECRET = ConfigUtils.INSTANCE.getStringValue("billing.razorpay.key_secret");
        redirect_url = ConfigUtils.INSTANCE.getStringValue("billing.razorpay.redirect_url");
        serverUrl = ConfigUtils.INSTANCE.getStringValue("billing.razorpay.server_url");
        client.setConnectTimeout(20000);
        client.setReadTimeout(60000);
    }

    public void handleRazorpayWebhook(Map<String, Object> object) throws VException, UnsupportedEncodingException {

        try {
            String event = (String) object.get("event");
            if(event.equals("payment.dispute.created") || event.contains("payment.dispute.created")) {
                EmailRequest email = new EmailRequest();
                email.setBody(object.toString());

                String subject = "Payment Dispute-Created";
                email.setSubject(subject);

                ArrayList<InternetAddress> toList = new ArrayList<>();
                toList.add(new InternetAddress(ConfigUtils.INSTANCE.getStringValue("email.accounts"), "Vedantu Accounts"));
                toList.add(new InternetAddress(ConfigUtils.INSTANCE.getStringValue("email.vcare"), "Vedantu Care"));
                email.setTo(toList);
                email.setType(CommunicationType.WEBHOOK_RAZORPAY);
                communicationManager.sendEmailViaRest(email);
                return;
            }

            ExtTransaction parentTransaction = null;
            ExtTransaction childTransaction = null;
            String parentTxnId = null;
            String razorPayId = null;

            Gson gson = new Gson();
            JsonObject jObject = gson.toJsonTree(object).getAsJsonObject();

            JsonObject entityObj = jObject.get("payload").getAsJsonObject().get("payment").getAsJsonObject().get("entity").getAsJsonObject();

            razorPayId = entityObj.get("id").getAsString();

            logger.info("RAZORPAYTESTING id: " + razorPayId);

            parentTxnId = entityObj.get("notes").getAsJsonObject().get("vedantu_txnid").getAsString();
            //		captured = entityObj.get("captured").getAsBoolean();

            if (parentTxnId != null) {
                parentTransaction = extTransactionDAO.getById(parentTxnId);
                if (parentTransaction == null) {
                    logger.error("razorpay invalid response, txn not found " + parentTxnId);
                    return;
                }
            } else {
                logger.error("razorpay invalid response, txnid null");
                return;
            }

            if(event.equals("payment.failed") || event.contains("payment.failed")) {
                logger.info("in payment.failed");
                childTransaction = mapper.map(parentTransaction, ExtTransaction.class);

                childTransaction.setId(null);
                childTransaction.setPaymentChannelTransactionId(razorPayId);
                childTransaction.setParentTransactionId(parentTxnId);

                client.addFilter(new HTTPBasicAuthFilter(KEY_ID, KEY_SECRET));
                WebResource webResource = client.resource(serverUrl + "/payments/" + parentTransaction.getPaymentChannelTransactionId());
                ClientResponse response;

                response = webResource.accept("application/json").get(ClientResponse.class);
                String paymentJson = response.getEntity(String.class);
                JsonObject paymentResObj = new JsonParser().parse(paymentJson).getAsJsonObject();

                if(TransactionStatus.SUCCESS.equals(parentTransaction.getStatus()) && !(paymentResObj != null &&
                        paymentResObj.get("captured").getAsBoolean() && "captured".equals(paymentResObj.get("status").getAsString()))) {
                    logger.error("payment.failed received for parent transaction "
                            +parentTxnId+" already in success state");
                }
                childTransaction.setStatus(TransactionStatus.FAILED);
                childTransaction.setGatewayResponse(entityObj.toString());
                logger.info("parentTransaction: " + parentTransaction);
                logger.info("childTransaction: " + childTransaction);
                extTransactionDAO.create(parentTransaction, parentTransaction.getUserId());
                extTransactionDAO.create(childTransaction, parentTransaction.getUserId());
            }

            if(event.equals("payment.authorized") || event.contains("payment.authorized")) {
                logger.info("in payment.authorized");
                if(!TransactionStatus.SUCCESS.equals(parentTransaction.getStatus())) {
                    logger.info("not already success");
                    captureTransaction("handleRazorpayWebhook", parentTxnId, razorPayId, parentTransaction);
                } else {
                    logger.info("razorpay transaction already successful: "+entityObj.toString());
                }
            }
        } catch(Exception ex) {
            logger.error("Invalid webhook Razorpay object: " + object + " Exception: " + ex.getMessage());
        }
    }

    @Override
    public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException {
        logger.info("ENTERING getPaymentUrl of RazorpayPaymentManager "+transaction.toString());

        Map<String, Object> httpParams = new HashMap<>();

        // required params
        httpParams.put("key", KEY_ID);
        //in paisa
        httpParams.put("amount", transaction.getAmount().toString());
        //httpParams.put("name", "Vedantu");

        httpParams.put("txnid", transaction.getId());
        httpParams.put("redirectUrl", redirect_url);

        UserBasicInfo user = fosUtils.getUserBasicInfo(transaction.getUserId().toString(), true);
        String name = user.getFullName();
        String email = user.getEmail();
        String number = user.getContactNumber();
        String userId = user.getUserId().toString();
        //Optional
        //httpParams.put("description", "Vedantu");
        //httpParams.put("image","");
        //httpParams.put("prefill.name", name);
        httpParams.put("prefill.email", user.getEmail());
        httpParams.put("prefill.contact", user.getContactNumber());
        RechargeUrlInfo rechargeUrlInfo = new RechargeUrlInfo(PaymentGatewayName.RAZORPAY, null, null, httpParams);
        //Order flow starts
        //TODO: Check if we need an Order flow for every transaction In RazorPay.
        Order order = razorpayAPIManager.createOrder(transaction.getAmount(), transaction.getId());
        if (order != null) {
            logger.info("Received order from Razorpay PaymentManager " + order.toString());
            transaction.setUniqueOrderId(order.get("id"));
            httpParams.put("orderid", order.get("id"));
            extTransactionDAO.save(transaction);
            //Order flow ends
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Order Was not created in razorpay");
        }
        logger.info("Exiting getPaymentUrl of Razorpay PaymentManager " + rechargeUrlInfo.toString());
        return rechargeUrlInfo;
    }


    @Override
    public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId)
            throws NotFoundException, InternalServerErrorException,
            ConflictException, BadRequestException {
        // VERIFY THE RESPONSE FROM SERVER
        verifyRequest(transactionInfo, callingUserId);

        logger.info("ENTERING onPaymentReceive of RazorpayPaymentManager "+transactionInfo.toString());
        String razorpay_payment_id = null;
        String txnid = null;
        ExtTransaction extTransaction = null;

        Object[] razorpay_payment_ids =((List<String>) transactionInfo.get("razorpay_payment_id")).toArray();
        Object[] txnids =((List<String>) transactionInfo.get("txnid")).toArray();

        if (razorpay_payment_ids != null && razorpay_payment_ids.length > 0) {
            razorpay_payment_id = razorpay_payment_ids[0].toString();
        }

        logger.info("RAZORPAYTESTING id: " + razorpay_payment_id);

        if (txnids != null && txnids.length > 0) {
            txnid = txnids[0].toString();
        }

        if (txnid != null) {
            extTransaction = extTransactionDAO.getById(txnid);

            if (extTransaction == null) {
                logger.warn("razorpay invalid response, txn not found "+txnid);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response, txn not found");
            }
        } else {
            logger.warn("razorpay invalid response, txnid null");
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response, txnid null");

        }

        if (razorpay_payment_id == null || "".equals(razorpay_payment_id)) {
            //transactionStatus = TransactionStatus.FAILED;
            logger.warn("razorpay_payment_id null for txnid" + txnid);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response, razorpay_payment_id null for txnid" + txnid);
        } else {
            captureTransaction("onPaymentReceive", txnid, razorpay_payment_id, extTransaction);
        }

        return extTransaction;
    }

    private void captureTransaction(String mode, String txnid, String razorpay_payment_id, ExtTransaction extTransaction) throws InternalServerErrorException, ConflictException, NotFoundException {
        String status = null;
        int amountPaid = 0;
        int subventionAmount = 0;
        int roundedAmount = 0;
        int bufferAmount = 0;
        JsonObject emiPlan = null;
        String method = null;
        int rateofIntrest = 0;
        int duration = 0;

        client.addFilter(new HTTPBasicAuthFilter(KEY_ID, KEY_SECRET));
        WebResource webResource = client.resource(serverUrl + "/payments/" + razorpay_payment_id + "/capture");
        ClientResponse response;
        String query = "amount=" + extTransaction.getAmount();

        //TODO: Use RazorPai APi dont use custom Libs
        response = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).accept("application/json").post(ClientResponse.class, query);
        String output = response.getEntity(String.class);

        // Save output to DB
        extTransaction.setGatewayResponse(output);

        logger.info("Razorpay capture response for txnid " + txnid + " razorpay_payment_id " + razorpay_payment_id
                + "response code " + response.getStatus() + " : " + output);
        if (response.getStatus() == 200) {
            JsonObject jObject = new JsonParser().parse(output).getAsJsonObject();

            status = jObject.get("status").getAsString();
            amountPaid = jObject.get("amount").getAsInt();
            if(jObject.get("emi_plan")!=null){
                emiPlan = jObject.get("emi_plan").getAsJsonObject();
                if(emiPlan != null){
                    rateofIntrest = emiPlan.get("rate").getAsInt();
                    duration = emiPlan.get("duration").getAsInt();
                }                
            }
            method = jObject.get("method").getAsString();
            if (!jObject.get("error_code").isJsonNull()) {
                String errorCode = jObject.get("error_code").getAsString();
                if (errorCode != null) {
                    //What error can be there if status is captured and response is 200
                    logger.warn("error in Razorpay capture, errorCode " + errorCode
                            + " ,errorDescription " + jObject.get("error_description") + " for txnid " + txnid + " razorpay_payment_id " + razorpay_payment_id
                            + "response code 200" + output);
                    if("onPaymentReceive".equals(mode)) {
                        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "error in Razorpay capture response 200, errorCode " + errorCode
                                + " ,errorDescription " + jObject.get("error_description"));
                    }
                }
            }
        } else {
            logger.warn("error in Razorpay capture for txnid " + txnid + " razorpay_payment_id " + razorpay_payment_id
                    + "response code " + response.getStatus() + " : " + output);
            if("onPaymentReceive".equals(mode)) {
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "error in Razorpay capture, response code " + response.getStatus()
                        + " response " + output);
            }
        }

        TransactionStatus transactionStatus = null;
        if ("captured".equals(status)) {
            transactionStatus = TransactionStatus.SUCCESS;
        } else {
            logger.error("Razorpay capture status not captured for txnid " + txnid + " razorpay_payment_id " + razorpay_payment_id);
            transactionStatus = TransactionStatus.FAILED;
        }

        if (transactionStatus != TransactionStatus.SUCCESS) {
            amountPaid = 0;
        }

        logger.info("transaction status : " + transactionStatus);

        logger.info("starting transaction ");
        logger.info("extTransaction before updation : " + extTransaction);

        if (extTransaction.getStatus() == TransactionStatus.SUCCESS	) {
            logger.error("razorpay transaction already successful: "+txnid);
            if("onPaymentReceive".equals(mode)) {
                throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
                        "transaction with id " + txnid + " is already processed");
            }
        }
        extTransaction.setStatus(transactionStatus);
        extTransaction.setGatewayStatus(status);
        extTransaction.setTransactionTime(String.valueOf(System
                .currentTimeMillis()));
        if (emiPlan != null && rateofIntrest != 0 && duration != 0) {
            //This Rate of interest comes as yearly and multiplied by 100
            float R = (float) rateofIntrest / (float) (12 * 100 * 100);
            int Total = extTransaction.getAmount().intValue();
            //https://razorpay.com/docs/offers/no-cost-emi/
            //Total = [P x R x (1+R)^N]/[(1+R)^N-1]
            float calculatedPrincipal = (float) (((Math.pow((1 + R), duration) - 1) * Total / duration) / (R * Math.pow(1 + R, duration)));
            roundedAmount = amountPaid - (int) calculatedPrincipal;
            subventionAmount = Total - amountPaid;
            bufferAmount = (int) (Total * 0.0001);
            if (Math.abs(roundedAmount) >= bufferAmount) {
                logger.error("Mismatch in calculation of EMI for extTransaction " + extTransaction.getId());
            }
        }
        if (extTransaction.getAmount().intValue() == amountPaid && transactionStatus == TransactionStatus.SUCCESS
                || ((Math.abs(roundedAmount) < bufferAmount && emiPlan != null && transactionStatus == TransactionStatus.SUCCESS))) {
            // TODO: these functionality can be moved to abstract class

            extTransaction.setPaymentChannelTransactionId(razorpay_payment_id);
            //extTransaction.setPaymentInstrument(paymentInstrument[0].toString());
            extTransaction.setPaymentMethod(method);
            //extTransaction.setBankRefNo(bankRefNo[0].toString());
            logger.info("saving extTransaction: " + extTransaction);
            extTransactionDAO.create(extTransaction, extTransaction.getUserId());

            // transaction = TransactionDAO.INSTANCE
            // .upsertTransaction(transaction);
            Account account = null;
            if (transactionStatus == TransactionStatus.SUCCESS) {
                // TODO: take this code to common place, where update and
                // deduct
                // operation can be done
                logger.info("transaction was successful:" + extTransaction);
                account = accountDAO
                        .getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
                if (account == null) {
                    logger.error("no account found for userid:" + extTransaction.getUserId());
                    if ("onPaymentReceive".equals(mode)) {
                        throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                                "no account found for userId:" + extTransaction.getUserId());
                    }
                }
                logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

                if (emiPlan != null) {
                    account.setBalance(account.getBalance() + amountPaid + subventionAmount);
                    account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid + subventionAmount);
                } else {
                    account.setBalance(account.getBalance() + amountPaid);
                    account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);
                }

                logger.info("new account balance : " + account.getBalance());
                accountDAO.updateWithoutSession(account, null);

                com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                        amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                        BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                        TransactionRefType.EXTERNAL_TRANSACTION, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
                transactionDAO.create(vedantuTransaction);

                if (emiPlan != null) {
                    //Create Subvention Transaction
                    com.vedantu.dinero.entity.Transaction vedantuSubvention = new com.vedantu.dinero.entity.Transaction(
                            subventionAmount, 0, subventionAmount, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                            BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                            TransactionRefType.EXTERNAL_TRANSACTION, null,
                            com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
                    vedantuSubvention.setReasonRefSubType(TransactionRefSubType.RAZORPAY_SUBVENTION_FEE);
                    transactionDAO.create(vedantuSubvention);
                }

                logger.info("new account balance : " + account.getBalance());
            }
        } else {
            extTransactionDAO.create(extTransaction, extTransaction.getUserId());
        }

        logger.info("Exiting onPaymentReceive of RazorpayPaymentManager "+ extTransaction.toString());
    }


    @Override
    public PaymentGatewayName getName() {
        return this.gatewayName;
    }

}
