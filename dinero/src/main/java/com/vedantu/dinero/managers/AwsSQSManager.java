
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers;

import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

/**
 *
 * @author pranavm
 */
@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew{
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager");
        try {
            String env = ConfigUtils.INSTANCE.getStringValue("environment");
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                
                logger.info("creating dead letter queue for DINERO_INVOICES_QUEUE_DL");
                CreateQueueRequest cqrDl = new CreateQueueRequest(SQSQueue.DINERO_INVOICES_QUEUE_DL
                        .getQueueName(env));
                cqrDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(cqrDl);
                logger.info("dead letter queue for DINERO_INVOICES_QUEUE_DL created" + SQSQueue.DINERO_INVOICES_QUEUE_DL.getQueueName(env));
                
                
                CreateQueueRequest cqr = new CreateQueueRequest(SQSQueue.DINERO_INVOICES_QUEUE
                        .getQueueName(env));
                cqr.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(cqr);
                logger.info("created queue for student invoices");

                // -- init -- queue creation and config for base instalments

                logger.info("creating dead letter queue for base instalment operations");
                CreateQueueRequest baseInstalmentOpsDl = new CreateQueueRequest(SQSQueue.BASE_INSTALMENT_OPS_DL
                        .getQueueName(env));
                baseInstalmentOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(baseInstalmentOpsDl);
                logger.info("dead letter queue for base instalment operations created" + SQSQueue.BASE_INSTALMENT_OPS_DL.getQueueName(env));

                logger.info("creating queue for bundle instalment operations");
                CreateQueueRequest baseInstalmentOps = new CreateQueueRequest(SQSQueue.BASE_INSTALMENT_OPS
                        .getQueueName(env));
                baseInstalmentOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                baseInstalmentOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.BASE_INSTALMENT_OPS.getVisibilityTimeout());
                //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(baseInstalmentOps);
                logger.info("queue for base instalment operations created" + SQSQueue.BASE_INSTALMENT_OPS.getQueueName(env));

                // -- end -- queue creation and config for base instalments

                // -- init -- queue creation and config for creating new account

                logger.info("creating dead letter queue for create New Account");
                CreateQueueRequest createNewAccountDl = new CreateQueueRequest(SQSQueue.CREATE_NEW_USER_ACCOUNT_DL_QUEUE.getQueueName(env));
                createNewAccountDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                //createNewAccount.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(createNewAccountDl);
                logger.info("dead letter queue for create New Account created : " + SQSQueue.CREATE_NEW_USER_ACCOUNT_DL_QUEUE.getQueueName(env));

                logger.info("creating queue for createNewAccount : " + SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE.getQueueName(env));
                CreateQueueRequest createNewAccount = new CreateQueueRequest(SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE.getQueueName(env));
                createNewAccount.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                createNewAccount.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE.getVisibilityTimeout());
                //createNewAccount.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                sqsClient.createQueue(createNewAccount);
                logger.info("queue for createNewAccount created : " + SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE.getQueueName(env));

                // -- end -- queue creation and config for creating new account
                
                

            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }
    }

    private void assignDeadLetterQueue(String opsQueueName, String dlQueueName, Integer maxReceive){
        if(maxReceive == null){
            maxReceive = 6;
        }

        // Get dead-letter queue ARN
        String dlQueueURL = sqsClient.getQueueUrl(dlQueueName)
                .getQueueUrl();

        GetQueueAttributesResult queueAttributes = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dlQueueURL)
                        .withAttributeNames("QueueArn"));

        String dlQueueArn = queueAttributes.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String opsQueueURL = sqsClient.getQueueUrl(opsQueueName)
                .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(opsQueueURL)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\""+maxReceive+"\", \"deadLetterTargetArn\":\""
                                + dlQueueArn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: "+dlQueueName+"  to:" + opsQueueName);

    }

    @PostConstruct
    public void postQueueCreation(){
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
            assignDeadLetterQueue(SQSQueue.BASE_INSTALMENT_OPS.getQueueName(env), SQSQueue.BASE_INSTALMENT_OPS_DL.getQueueName(env), 6);
            assignDeadLetterQueue(SQSQueue.DINERO_INVOICES_QUEUE.getQueueName(env), SQSQueue.DINERO_INVOICES_QUEUE_DL.getQueueName(env), 2);
            assignDeadLetterQueue(SQSQueue.CREATE_NEW_USER_ACCOUNT_QUEUE.getQueueName(env), SQSQueue.CREATE_NEW_USER_ACCOUNT_DL_QUEUE.getQueueName(env), 2);
        }

    }

}
