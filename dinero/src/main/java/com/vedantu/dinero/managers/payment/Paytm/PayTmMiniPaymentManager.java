package com.vedantu.dinero.managers.payment.Paytm;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.paytm.pg.merchant.PaytmChecksum;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.payment.IPaymentManager;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.*;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@SuppressWarnings("Duplicates")
@Service
public class PayTmMiniPaymentManager implements IPaymentManager {


    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String PAYMENT_CHANNEL = "PAYTM_MINI";
    private final PaymentGatewayName gatewayName = PaymentGatewayName.PAYTM_MINI;
    private final Gson gson = new Gson();
    @Autowired
    public FosUtils fosUtils;
    @Autowired
    public ExtTransactionDAO extTransactionDAO;
    @Autowired
    public TransactionDAO transactionDAO;
    @Autowired
    public AccountDAO accountDAO;
    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PayTmMiniPaymentManager.class);

    public PayTmMiniPaymentManager() {
        super();
    }

    @Override
    public PaymentGatewayName getName() {
        return this.gatewayName;
    }


    @Override
    public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException, JSONException, MalformedURLException {

        BigDecimal amount = new BigDecimal(transaction.getAmount()).divide(new BigDecimal(100));

        ClientResponse response = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + transaction.getUserId(), HttpMethod.GET, null);
        String json = response.getEntity(String.class);
        User user = gson.fromJson(json, User.class);

        JSONObject body = new JSONObject();
        JSONObject userInfo = new JSONObject();
        JSONObject txnAmount = new JSONObject();
        JSONObject paytmParams = new JSONObject();
        JSONObject head = new JSONObject();


        userInfo.put("custId", user.getId());

        /*
         * Amount in INR that is payble by customer
         * this should be numeric with optionally having two decimal points
         */

        txnAmount.put("value", String.format("%.2f", amount.doubleValue()));
        txnAmount.put("currency", "INR");


        body.put("mid", PaytmMiniConstants.MID);
        body.put("websiteName", PaytmMiniConstants.WEBSITE);
        body.put("orderId", transaction.getId());
        body.put("userInfo", userInfo);
        body.put("txnAmount", txnAmount);
        /* on completion of transaction, we will send you the response on this URL */
        body.put("callbackUrl", PaytmMiniConstants.PAYTM_REDIRECT_URL);
        body.put("requestType", "Payment");


        String paytmChecksum = null;
        try {
            paytmChecksum = PaytmChecksum.generateSignature(body.toString(), PaytmMiniConstants.MERCHANT_KEY);
            head.put("signature", paytmChecksum);
        } catch (Exception e) {
            logger.info("error in payth checksum " + e.getMessage());
        }


        paytmParams.put("body", body);
        paytmParams.put("head", head);

        String post_data = paytmParams.toString();

        /* for Staging */
        URL url = new URL(PaytmMiniConstants.PAYTM_INIT_TXN_URL + "?mid=" + PaytmMiniConstants.MID + "&orderId=" + transaction.getId());

        String responseData = "";
        /* for Staging */
        logger.info("url: " + url);
        logger.info("paytmChecksum: " + paytmChecksum);


        try {
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setDoOutput(true);

            DataOutputStream requestWriter = new DataOutputStream(connection.getOutputStream());
            requestWriter.writeBytes(post_data);
            requestWriter.close();
            InputStream is = connection.getInputStream();
            BufferedReader responseReader = new BufferedReader(new InputStreamReader(is));
            if ((responseData = responseReader.readLine()) != null) {
                logger.info("Response: " + responseData);
            }
            responseReader.close();
        } catch (Exception exception) {
            exception.printStackTrace();
        }


        JsonObject responseJson = new Gson().fromJson(responseData, JsonObject.class);
        String tokn = "";
        logger.info(responseJson.get("body").toString());
        if (responseJson.get("body") != null && responseJson.get("body").getAsJsonObject().get("txnToken") != null) {
            tokn = responseJson.get("body").getAsJsonObject().get("txnToken").getAsString() + "OrderId" + transaction.getId();
        }
        return new RechargeUrlInfo(PaymentGatewayName.PAYTM_MINI, tokn, "POST", null);
    }

    @Override
    public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId) throws BadRequestException, InternalServerErrorException, NotFoundException, ConflictException {


        logger.info(transactionInfo.toString());

        Map<String, String> callbackResponseMap = new TreeMap<>();
        transactionInfo.forEach((key, value) -> callbackResponseMap.put(key, ((List<String>) value).get(0)));

        String mid = callbackResponseMap.get("MID");
        String txnid = callbackResponseMap.get("TXNID");
        String orderid = callbackResponseMap.get("ORDERID");
        String banktxnid = callbackResponseMap.get("BANKTXNID");
        String txnamount = callbackResponseMap.get("TXNAMOUNT");
        String currency = callbackResponseMap.get("CURRENCY");
        String status = callbackResponseMap.get("STATUS");
        String respcode = callbackResponseMap.get("RESPCODE");
        String respmsg = callbackResponseMap.get("RESPMSG");
        String txndate = callbackResponseMap.get("TXNDATE");


        Map<String, String> txnStatusParamMap = new TreeMap<>();
        txnStatusParamMap.put("MID", mid);
        txnStatusParamMap.put("ORDERID", orderid);
        try {
            txnStatusParamMap.put("CHECKSUMHASH", PaytmChecksum.generateSignature((TreeMap<String, String>) txnStatusParamMap, PaytmMiniConstants.MERCHANT_KEY));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);

            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());

        }
        String txnParamJson = gson.toJson(txnStatusParamMap);
        Map<String, String> txnResponseMap = new HashMap<>();
        int count = 0;
        boolean tryAgain = true;
        while (count < 2 && tryAgain) {
            try {
                count++;
                ClientResponse response = WebUtils.INSTANCE.doCall(PaytmMiniConstants.PAYTM_TXN_STATUS_URL, HttpMethod.POST, txnParamJson);
                VExceptionFactory.INSTANCE.parseAndThrowException(response);
                String entity = response.getEntity(String.class);
                txnResponseMap = gson.fromJson(entity, new TypeToken<Map<String, String>>() {
                }.getType());
                tryAgain = false;
                logger.info( "paytm status response " + response);
                logger.info( "paytm status response entity " + entity);
                logger.info( "paytm status response map   " + txnResponseMap.toString());
            } catch (VException e) {
                if (count >= 2) {
                    logger.error(e.getMessage(), e);
                }
            }
        }


        boolean signatureCheck;
        try {
            signatureCheck = validateSignature(callbackResponseMap);
            signatureCheck &= Objects.equals(callbackResponseMap.get("MID"), txnResponseMap.get("MID"));
            signatureCheck &= Objects.equals(callbackResponseMap.get("TXNID"), txnResponseMap.get("TXNID"));
            signatureCheck &= Objects.equals(callbackResponseMap.get("ORDERID"), txnResponseMap.get("ORDERID"));
            signatureCheck &= Objects.equals(callbackResponseMap.get("BANKTXNID"), txnResponseMap.get("BANKTXNID"));
            signatureCheck &= Objects.equals(callbackResponseMap.get("TXNAMOUNT"), txnResponseMap.get("TXNAMOUNT"));
            signatureCheck &= Objects.equals(callbackResponseMap.get("STATUS"), txnResponseMap.get("STATUS"));
            signatureCheck &= Objects.equals(callbackResponseMap.get("RESPCODE"), txnResponseMap.get("RESPCODE"));
            signatureCheck &= Objects.equals(callbackResponseMap.get("TXNDATE"), txnResponseMap.get("TXNDATE"));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }


        ExtTransaction extTransaction;
        if (status != null) {
            double amount = Double.parseDouble(txnamount) * 100;
            int amountPaid = (int) amount;

            String transactionId = orderid;

            String paymentChannelTransactionId = txnid;

            TransactionStatus transactionStatus;

            if (status.equalsIgnoreCase("TXN_SUCCESS") && signatureCheck) {
                transactionStatus = TransactionStatus.SUCCESS;
            } else if (status.equalsIgnoreCase("PENDING")) {
                transactionStatus = TransactionStatus.PENDING;
            } else {
                transactionStatus = TransactionStatus.FAILED;
            }
            String bankRefNo = banktxnid;

            String paymentMethod = txnResponseMap.get("GATEWAYNAME");

            String paymentInstrument = txnResponseMap.get("PAYMENTMODE");


            if (transactionStatus != TransactionStatus.SUCCESS) {
                amountPaid = 0;
            }

            logger.info("transaction status : " + transactionStatus);

            logger.info("starting transaction ");
            extTransaction = extTransactionDAO.getById(transactionId);
            logger.info("extTransaction before updation : " + extTransaction);

            // Save output to DB
            Map<String, Map<String, String>> responseMap = new HashMap<>();
            responseMap.put("CallBack", callbackResponseMap);
            responseMap.put("ServerTxnStatus", txnResponseMap);
            extTransaction.setGatewayResponse(gson.toJson(responseMap));

            if (extTransaction.getStatus() != TransactionStatus.PENDING) {

                throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
                        "transaction with id " + transactionId + " is already processed");
            }
            extTransaction.setStatus(transactionStatus);
            extTransaction.setTransactionTime(String.valueOf(System.currentTimeMillis()));
            extTransaction.setPaymentChannelTransactionId(paymentChannelTransactionId);
            extTransaction.setPaymentInstrument(paymentInstrument);
            extTransaction.setPaymentMethod(paymentMethod);
            extTransaction.setBankRefNo(bankRefNo);
            logger.info("saving extTransaction: " + extTransaction);
            extTransactionDAO.create(extTransaction, callingUserId);
            if (extTransaction.getAmount() == amountPaid && transactionStatus == TransactionStatus.SUCCESS) {
                // TODO: these functionality can be moved to abstract class
                // transaction = TransactionDAO.INSTANCE
                // .upsertTransaction(transaction);

                Account account;
                // TODO: take this code to common place, where update and
                // deduct
                // operation can be done
                logger.info("transaction was successful:" + extTransaction);
                account = accountDAO
                        .getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
                if (account == null) {
                    logger.error("no account found for userid:" + extTransaction.getUserId());

                    throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                            "no account found for userId:" + extTransaction.getUserId());
                }
                logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

                account.setBalance(account.getBalance() + amountPaid);
                account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

                logger.info("new account balance : " + account.getBalance());

                accountDAO.updateWithoutSession(account, null);
                com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                        amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                        BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                        TransactionRefType.EXTERNAL_TRANSACTION, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
                transactionDAO.create(vedantuTransaction);


                logger.info("Account " + account.toString());
            }
            return extTransaction;
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "PayTM: Order status not found for PayTM ID " + orderid);
        }

    }

    private boolean validateSignature(Map<String, String> fields) throws Exception {

        String paytmChecksum = null;

        /* Create a TreeMap from the parameters received in POST */
        TreeMap<String, String> body = new TreeMap<>();
        for (Map.Entry<String, String> requestParamsEntry : fields.entrySet()) {
            if ("CHECKSUMHASH".equalsIgnoreCase(requestParamsEntry.getKey())) {
                paytmChecksum = requestParamsEntry.getValue();
            } else {
                body.put(requestParamsEntry.getKey(), requestParamsEntry.getValue());
            }
        }

        /*
         * Verify checksum
         * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys
         */
        return PaytmChecksum.verifySignature(body, PaytmMiniConstants.MERCHANT_KEY, paytmChecksum);
    }

}
