package com.vedantu.dinero.managers.payment;

import java.net.MalformedURLException;
import java.util.Map;

import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.exception.*;
import org.codehaus.jettison.json.JSONException;

public interface IPaymentManager {

	PaymentGatewayName getName();

	RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException, JSONException, MalformedURLException;

	ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId)
			throws NotFoundException, InternalServerErrorException, ConflictException, BadRequestException;

	default void verifyRequest(Map<String, Object> transactionInfo, Long callingUserId) throws BadRequestException {
		if (transactionInfo == null || transactionInfo.size() == 0) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Response from payment server is empty. Can't proceed further.");
		}
	}
}
