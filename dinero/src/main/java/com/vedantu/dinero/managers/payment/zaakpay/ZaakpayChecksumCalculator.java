package com.vedantu.dinero.managers.payment.zaakpay;

import java.util.Collections;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;

public class ZaakpayChecksumCalculator {

	public ZaakpayChecksumCalculator() {
	}

	private static String toHex(byte[] bytes) {
		StringBuilder buffer = new StringBuilder(bytes.length * 2);

		byte[] arrayOfByte = bytes;
		int j = bytes.length;
		for (int i = 0; i < j; i++) {
			Byte b = Byte.valueOf(arrayOfByte[i]);
			String str = Integer.toHexString(b.byteValue());
			int len = str.length();
			if (len == 8) {
				buffer.append(str.substring(6));
			} else if (str.length() == 2) {
				buffer.append(str);
			} else {
				buffer.append("0" + str);
			}
		}
		return buffer.toString();
	}

	public static String calculateChecksum(String secretKey,
			String allParamValue) throws Exception {
		byte[] dataToEncryptByte = allParamValue.getBytes();
		byte[] keyBytes = secretKey.getBytes();
		SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(secretKeySpec);
		byte[] checksumByte = mac.doFinal(dataToEncryptByte);
		String checksum = toHex(checksumByte);

		return checksum;
	}

	public static boolean verifyChecksum(String secretKey,
			String allParamVauleExceptChecksum, String checksumReceived)
			throws Exception {
		byte[] dataToEncryptByte = allParamVauleExceptChecksum.getBytes();
		byte[] keyBytes = secretKey.getBytes();
		SecretKeySpec secretKeySpec = new SecretKeySpec(keyBytes, "HmacSHA256");
		Mac mac = Mac.getInstance("HmacSHA256");
		mac.init(secretKeySpec);
		byte[] checksumCalculatedByte = mac.doFinal(dataToEncryptByte);
		String checksumCalculated = toHex(checksumCalculatedByte);
		if (checksumReceived.equals(checksumCalculated)) {
			return true;
		}
		return false;
	}

	public static String getAllNotEmptyParamValue(HttpServletRequest request) {
		@SuppressWarnings("unchecked")
		Enumeration<String> en = request.getParameterNames();
		List<String> list = Collections.list(en);

		String allNonEmptyParamValue = "";
		for (int i = 0; i < list.size(); i++) {
			String paramName = (String) list.get(i);
			String paramValue = "";
			if (paramName.equals("returnUrl")) {
				paramValue = ParamSanitizer.SanitizeURLParam(request
						.getParameter(paramName));
			} else
				paramValue = ParamSanitizer.sanitizeParam(request
						.getParameter(paramName));
			if (paramValue != null) {
				allNonEmptyParamValue = allNonEmptyParamValue + "'"
						+ paramValue + "'";
			}
		}

		return allNonEmptyParamValue;
	}
}