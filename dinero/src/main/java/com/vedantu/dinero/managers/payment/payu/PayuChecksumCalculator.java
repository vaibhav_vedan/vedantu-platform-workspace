package com.vedantu.dinero.managers.payment.payu;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;



public class PayuChecksumCalculator {

	private static String SALT = ConfigUtils.INSTANCE.getStringValue("billing.payu.checksum.salt");

	public static String getStringForChecksumBeforeTransaction(
			PayuTransactionInitiationReq payuTransactionInitiationReq) {

		String allParamValueStr = "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s||||||"
				+ SALT;
		String allParamValue = String.format(allParamValueStr,
				payuTransactionInitiationReq.getKey(),
				payuTransactionInitiationReq.getTxnid(),
				payuTransactionInitiationReq.getAmount(),
				payuTransactionInitiationReq.getProductinfo(),
				payuTransactionInitiationReq.getFirstname(),
				payuTransactionInitiationReq.getEmail(),
				payuTransactionInitiationReq.getUdf1(),
				payuTransactionInitiationReq.getUdf2(),
				payuTransactionInitiationReq.getUdf3(),
				payuTransactionInitiationReq.getUdf4(),
				payuTransactionInitiationReq.getUdf5());

		return allParamValue;
	}

	public static String getStringForChecksumAfterTransaction(
			PayuTransactionsInitiationRes payuTransactionInitiationRes) {

		// <SALT>|status||||||udf5|udf4|udf3|udf2|udf1|email|firstname|productinfo|amount|txnid|key

		String allParamValueStr = SALT
				+ "|%s||||||%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s";
		String allParamValue = String.format(allParamValueStr,
				payuTransactionInitiationRes.getStatus(),
				payuTransactionInitiationRes.getUdf5(),
				payuTransactionInitiationRes.getUdf4(),
				payuTransactionInitiationRes.getUdf3(),
				payuTransactionInitiationRes.getUdf2(),
				payuTransactionInitiationRes.getUdf1(),
				payuTransactionInitiationRes.getEmail(),
				payuTransactionInitiationRes.getFirstname(),
				payuTransactionInitiationRes.getProductinfo(),
				payuTransactionInitiationRes.getAmount(),
				payuTransactionInitiationRes.getTxnid(),
				payuTransactionInitiationRes.getKey());

		return allParamValue;
	}

	public static String calculateChecksum(String allParamValue)
			throws Exception {
		String type = "SHA-512";
		byte[] hashseq = allParamValue.getBytes();
		StringBuffer hexString = new StringBuffer();
		try {
			MessageDigest algorithm = MessageDigest.getInstance(type);
			algorithm.reset();
			algorithm.update(hashseq);
			byte messageDigest[] = algorithm.digest();

			for (int i = 0; i < messageDigest.length; i++) {
				String hex = Integer.toHexString(0xFF & messageDigest[i]);
				if (hex.length() == 1)
					hexString.append("0");
				hexString.append(hex);
			}

		} catch (NoSuchAlgorithmException nsae) {
		}

		return hexString.toString();

	}

	public static Boolean verifyChecksum(String allParamValue, String hash)
			throws Exception {
		if (StringUtils.isNotEmpty(allParamValue)) {
			String checksumCalculated = calculateChecksum(allParamValue);
			if (hash.equals(checksumCalculated)) {
				return true;
			} else {
				return false;
			}
		}
		return false;
	}
        
        public static void main(String[] args) throws Exception {
            String allParamValue="gtKFFx|59563090d4c6cd7bd59d65ea|100.00|AccountRecharge|Ajith|ajith.reddy+10008@vedantu.com|||||||||||eCwWELxi";
            System.err.println(">>>sss "+calculateChecksum(allParamValue));
    }

}
