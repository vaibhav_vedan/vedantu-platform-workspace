package com.vedantu.dinero.managers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentInvoiceType;
import com.vedantu.dinero.serializers.InvoiceCounterService;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.text.DecimalFormat;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by somil on 04/08/17.
 */

@Service
public class InvoiceNumberGenerator {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InvoiceNumberGenerator.class);
    

    @Autowired
    private InvoiceCounterService invoiceCounterService;

    @Autowired
    private FosUtils userUtils;

    private static final String STUDENT_INVOICE_YEAR = "student_invoice_";
    
    public String getInvoiceNumber(PaymentInvoiceType type, long invoiceTime, Long teacherId) {
        String prefix = "";
        switch (type) {
            case VEDANTU_TEACHER:
                prefix = getVTInvoicePrefix(invoiceTime);
                break;
            case TEACHER_STUDENT:
                prefix = getSTInvoicePrefix(invoiceTime, teacherId);
                break;
        }
        Long serialNumber = getSerialNumber(prefix);
        return prefix+serialNumber;
    }
    
    public String getVedantuStudentInvoiceNumber(long invoiceTime, PaymentGatewayName gatewayName) {
        String finYear = getFinancialYear(invoiceTime);
        String prefix = finYear;
        prefix += getPaymentGatewayStr(gatewayName);
        prefix += getMonth(invoiceTime);
        Long serialNumber = getSerialNumber(STUDENT_INVOICE_YEAR+finYear);
        DecimalFormat formatter = new DecimalFormat("0000000");
        String result = formatter.format(serialNumber);
        return prefix+result;
    }

    public Long getSerialNumber(String invoiceNumberPrefix) {
        return invoiceCounterService.getNextSequence(invoiceNumberPrefix);
    }


    public String getVTInvoicePrefix(long invoiceTime) {
        String prefix = "VIPL/";
        prefix += getFinancialYear(invoiceTime);
        prefix += "/";
        prefix += getMonthYear(invoiceTime);
        prefix += "/ST/";
        return prefix;
    }



    public String getSTInvoicePrefix(long invoiceTime, Long teacherId) {

        UserBasicInfo userBasicInfo = userUtils.getUserBasicInfo(teacherId, true);
        String prefix = getTeacherIdentifier(userBasicInfo);
        prefix += "/";
        prefix += getFinancialYear(invoiceTime);
        prefix += "/";
        prefix += getMonthYear(invoiceTime);
        prefix += "/";
        return prefix;
    }

    private String getTeacherIdentifier(UserBasicInfo userBasicInfo) {
        String identifier = "";
        if(StringUtils.isNotEmpty(userBasicInfo.getFirstName())) {
            identifier += Character.toUpperCase(userBasicInfo.getFirstName().trim().charAt(0));
        }

        if(StringUtils.isNotEmpty(userBasicInfo.getLastName())) {
            identifier += Character.toUpperCase(userBasicInfo.getLastName().trim().charAt(0));
        }

        identifier += userBasicInfo.getExtensionNumber();
        return identifier;
    }


    private String getMonthYear(long invoiceTime) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM''yy");
        sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return sdf.format(invoiceTime).toUpperCase();

    }

    private String getFinancialYear(long invoiceTime) {
        String result = "";
        Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Asia/Kolkata"));
        calendar.setTimeInMillis(invoiceTime);

        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        int yearLastTwoDigits = year % 100;

        if(month>=4) {
            result += yearLastTwoDigits;
            result += (yearLastTwoDigits+1);
        } else {
            result += yearLastTwoDigits-1;
            result += (yearLastTwoDigits);
        }
        return result;

    }

    private String getMonth(long invoiceTime) {
        Calendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Asia/Kolkata"));
        calendar.setTimeInMillis(invoiceTime);
        int month = calendar.get(Calendar.MONTH) + 1;
        DecimalFormat formatter = new DecimalFormat("00");
        String result = formatter.format(month);
        return result;
    }
    
    private String getPaymentGatewayStr(PaymentGatewayName gatewayName) {
        String result = "C/B";
        if(gatewayName != null){
            result = ConfigUtils.INSTANCE.getStringValue("student.invoice.prefix."+gatewayName.name());
        }
        return result;
    }

}
