package com.vedantu.dinero.managers.payment.axis;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.validation.constraints.NotNull;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.MessageDigest;
import java.util.*;

@SuppressWarnings("Duplicates")
public class AxisEncrpytionUtil {

    private static final String ENCRYPTION_ALGORITHM = "AES";
    private static final String HASHING_ALGORITHM = "SHA-256";

    private String SECURE_SECRET;
    private String ENCRYPTION_KEY;

    public AxisEncrpytionUtil(@NotNull String SECURE_SECRET, @NotNull String ENCRYPTION_KEY) {
        this.SECURE_SECRET = SECURE_SECRET;
        this.ENCRYPTION_KEY = ENCRYPTION_KEY;
    }

    public String hashAllFields(Map<String, String> fields) {

        List<String> fieldNames = new ArrayList<>(fields.keySet());
        Collections.sort(fieldNames);

        // create a buffer for the SHA-256 input and add the secure secret first
        StringBuilder builder = new StringBuilder();
        builder.append(SECURE_SECRET);

        // iterate through the list and add the remaining field values

        for (String fieldName : fieldNames) {
            String fieldValue = fields.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                builder.append(fieldValue);
            }
        }
        try {
            MessageDigest digest = MessageDigest.getInstance(HASHING_ALGORITHM);
            byte[] hash = digest.digest(builder.toString().getBytes(StandardCharsets.UTF_8));
            StringBuilder hexString = new StringBuilder();
            for (byte b : hash) {
                String hex = Integer.toHexString(0xff & b);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }

            return hexString.toString();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }

    }

    public String encrypt(String data) throws Exception {
        byte[] keyByte = ENCRYPTION_KEY.getBytes();
        Key key = generateKey(keyByte);
        Cipher c = Cipher.getInstance(ENCRYPTION_ALGORITHM);
        c.init(Cipher.ENCRYPT_MODE, key); //2
        byte[] encVal = c.doFinal(data.getBytes()); //1
        byte[] encryptedByteValue = Base64.getEncoder().encode(encVal); //3
        return new String(encryptedByteValue);
    }

    private static Key generateKey(byte[] keyByte) {
        return new SecretKeySpec(keyByte, ENCRYPTION_ALGORITHM);
    }
}
