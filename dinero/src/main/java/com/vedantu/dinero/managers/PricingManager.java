package com.vedantu.dinero.managers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.Plan;
import com.vedantu.dinero.entity.PricingChangeRequest;
import com.vedantu.dinero.entity.Slab;
import com.vedantu.dinero.entity.SlabRange;
import com.vedantu.dinero.entity.TeacherIncentive;
import com.vedantu.dinero.entity.TeacherPayoutRate;
import com.vedantu.dinero.entity.TeacherStartsFrom;
import com.vedantu.dinero.enums.SessionEntity;
import com.vedantu.dinero.enums.State;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.pojo.ApprovePrice;
import com.vedantu.dinero.pojo.ApprovePriceWithTime;
import com.vedantu.dinero.pojo.Price;
import com.vedantu.dinero.pojo.TeacherRequest;
import com.vedantu.dinero.pojo.TeacherSlabPlan;
import com.vedantu.dinero.request.ApprovePricingChangeRequest;
import com.vedantu.dinero.request.CreatePricingChangeRequest;
import com.vedantu.dinero.request.CreateSlabRangesRequest;
import com.vedantu.dinero.request.UpdateCustomPlanRequest;
import com.vedantu.dinero.response.ApprovePricingChangeResponse;
import com.vedantu.dinero.response.GetAllRequestsResponse;
import com.vedantu.dinero.response.GetPlanByIdResponse;
import com.vedantu.dinero.response.GetPricingChangeRequestsResponse;
import com.vedantu.dinero.response.GetTeacherPlansResponse;
import com.vedantu.dinero.response.GetTeacherSlabsResponse;
import com.vedantu.dinero.serializers.PlanDAO;
import com.vedantu.dinero.serializers.PricingChangeRequestDAO;
import com.vedantu.dinero.serializers.SlabDAO;
import com.vedantu.dinero.serializers.SlabRangeDAO;
import com.vedantu.dinero.serializers.TeacherIncentiveDAO;
import com.vedantu.dinero.serializers.TeacherPayoutRateDAO;
import com.vedantu.dinero.serializers.TeacherStartsFromDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SessionModel;

/*
 * Manager Service for Pricing APIs
 */
@Service
public class PricingManager {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	public SlabRangeDAO slabRangeDAO;

	@Autowired
	public SlabDAO slabDAO;

	@Autowired
	public PlanDAO planDAO;

	@Autowired
	public TeacherStartsFromDAO teacherStartsFromDAO;

	@Autowired
	public PricingChangeRequestDAO pricingChangeRequestDAO;

	@Autowired
	public TeacherIncentiveDAO teacherIncentiveDAO;

	@Autowired
	public TeacherPayoutRateDAO teacherPayoutRateDAO;

	@Autowired
	public TransactionDAO transactionDAO;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PricingManager.class);

	//private static String userFetchUrl;

	@Autowired
	public AccountDAO accountDAO;

	@Autowired
	public AccountManager accountManager;


	private static String updateStartsFromInESUrl;

	public PricingManager() {
		super();
                updateStartsFromInESUrl = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT")+ConfigUtils.INSTANCE.getStringValue("payout.updateStartsFromInESUrl");
	}

	public BaseResponse createSlabRanges(CreateSlabRangesRequest createSlabRangesRequest) throws BadRequestException {

		int i;

		List<com.vedantu.dinero.pojo.Slab> slabs = createSlabRangesRequest.getSlabs();

		if (slabs.size() < 2) {
			logger.error("Input size should be minimum 2");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Input size should be minimum 2");
		}

		// sort the ranges
		Collections.sort(slabs, new Comparator<com.vedantu.dinero.pojo.Slab>() {
                        @Override
			public int compare(final com.vedantu.dinero.pojo.Slab object1, final com.vedantu.dinero.pojo.Slab object2) {
				return ((Integer) object1.getMin()).compareTo((Integer) object2.getMax());
			}
		});

		if (slabs.get(0).getMin() < 0) {
			logger.error("Minimum input range incorrect");
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Minimum input range incorrect");
		}

		SlabRange slabRangeToInsert;
		Slab slabToCreate;
		Integer min, max;
		Query query;
		List<SlabRange> foundSlabRanges;
		boolean lastSlabExists = false;

		// Disable slabs
		for (i = 0; i < slabs.size() - 1; i++) {

			min = slabs.get(i).getMin();
			max = slabs.get(i + 1).getMax();
			query = new Query();

			query.addCriteria(
					Criteria.where("min").lte(min.intValue()).andOperator(Criteria.where("max").gt(min.intValue())
							.andOperator(Criteria.where("max").ne(-1).andOperator(Criteria.where("active").is(true)))));
			findAndDisableSlab(query, min, max);

			query = new Query();
			query.addCriteria(
					Criteria.where("min").lt(max.intValue()).andOperator(Criteria.where("max").gte(max.intValue())
							.andOperator(Criteria.where("max").ne(-1).andOperator(Criteria.where("active").is(true)))));
			findAndDisableSlab(query, min, max);

			query = new Query();
			query.addCriteria(Criteria.where("min").is(min)
					.andOperator(Criteria.where("max").is(max).andOperator(Criteria.where("active").is(true))));
			foundSlabRanges = slabRangeDAO.runQuery(query, SlabRange.class);
			if (foundSlabRanges.size() == 1) {
				// slab range already exists and is active
				slabs.remove(i);
				i--;
			}
		}

		query = new Query();
		query.addCriteria(Criteria.where("min").lte(slabs.get(i).getMin())
				.andOperator(Criteria.where("max").is(-1).andOperator(Criteria.where("active").is(true))));
		findAndDisableSlab(query, slabs.get(i).getMin(), -1);

		query = new Query();
		query.addCriteria(Criteria.where("min").is(slabs.get(i).getMin())
				.andOperator(Criteria.where("max").is(-1).andOperator(Criteria.where("active").is(true))));
		foundSlabRanges = slabRangeDAO.runQuery(query, SlabRange.class);
		if (foundSlabRanges.size() == 1) {
			// last slab special condition, slab already exists
			lastSlabExists = true;
		}

		for (i = 0; i < slabs.size() - 1; i++) {

			min = slabs.get(i).getMin();
			max = slabs.get(i + 1).getMin();

			slabRangeToInsert = new SlabRange(min, max, slabs.get(i).getMinValue(), true);
			slabRangeDAO.create(slabRangeToInsert);
			// Create slab for every slab range
			// TODO subject, target, grade wise slabe creation for every slab
			// range created

			slabToCreate = new Slab(slabRangeToInsert.getId(), null, null, null, true);
			slabDAO.create(slabToCreate);
		}

		// last slab
		if (slabs.size() > 0 && lastSlabExists == false) {
			slabRangeToInsert = new SlabRange(slabs.get(i).getMin(), -1, slabs.get(i).getMinValue(), true);
			slabRangeDAO.create(slabRangeToInsert);
			slabToCreate = new Slab(slabRangeToInsert.getId(), null, null, null, true);
			slabDAO.create(slabToCreate);
		}

		logger.info("Exiting");
		return new BaseResponse(ErrorCode.SUCCESS, "");
	}

	private void findAndDisableSlab(Query query, Integer min, Integer max) {

		List<SlabRange> foundSlabRanges;
		SlabRange foundSlabRange;
		String foundSlabId;
		List<Slab> foundSlabs;
		Slab foundSlab;
		foundSlabRanges = slabRangeDAO.runQuery(query, SlabRange.class);
		for (int i = 0; i < foundSlabRanges.size(); i++) {
			// logger.info("FoundSlabaRange min:" +
			// foundSlabRanges.get(i).getMin() + " max:" +
			// foundSlabRanges.get(i).getMax());
			foundSlabRange = foundSlabRanges.get(i);
			if (foundSlabRange.getMin() == min && foundSlabRange.getMax() == max) {
			} else {
				foundSlabId = foundSlabRange.getId();
				foundSlabRange.setActive(false);
				logger.info("Disabling slabRange:" + foundSlabId);
				slabRangeDAO.create(foundSlabRange);

				query = new Query();
				query.addCriteria(Criteria.where("slabRangeId").is(foundSlabId));
				foundSlabs = slabDAO.runQuery(query, Slab.class);
				for (int j = 0; j < foundSlabs.size(); j++) {
					foundSlab = foundSlabs.get(j);
					foundSlab.setActive(false);
					logger.info("Disabling slab:" + foundSlab.getId());
					slabDAO.create(foundSlab);
				}
			}
		}
	}

	// public BaseResponse getSlabRanges() {
	//
	// logger.info("Entering");
	// BaseResponse jsonResp;
	//
	// Query query = new Query();
	// query.addCriteria(Criteria.where("active").is(true));
	// query.with(new Sort(Sort.Direction.ASC, "min"));
	// List<SlabRange> foundSlabRanges = slabRangeDAO.runQuery(query,
	// SlabRange.class);
	// if (foundSlabRanges.size() == 0) {
	// jsonResp = new BaseResponse();
	// jsonResp.setErrorCode(ErrorCode.SUCCESS);
	// jsonResp.setErrorMessage("No Slab ranges Found");
	// } else {
	// List<Integer> slabRangeMins = new ArrayList<Integer>();
	// for (int i = 0; i < foundSlabRanges.size(); i++) {
	// slabRangeMins.add(foundSlabRanges.get(i).getMin());
	// }
	//
	// jsonResp = new GetSlabRangesResponse(slabRangeMins);
	// jsonResp.setErrorCode(ErrorCode.SUCCESS);
	// jsonResp.setErrorMessage("Fetched All Slab Ranges Successfully");
	// }
	//
	// logger.info("Exiting Response:" + jsonResp.toString());
	// return jsonResp;
	//
	// }
	public BaseResponse createPricingChangeRequest(CreatePricingChangeRequest createPricingChangeRequest)
			throws InternalServerErrorException {

		logger.info("Entering " + createPricingChangeRequest.toString());

		// BaseResponse response = getSlabRanges();
		// if(response.getErrorCode().equals(ErrorCode.SUCCESS)){
		//
		try {
			// GetSlabRangesResponse getSlabrangesResponse =
			// (GetSlabRangesResponse) response;
			String slabId;
			Query query;
			Long teacherId = createPricingChangeRequest.getTeacherId();
			List<Price> prices = createPricingChangeRequest.getPrices();
			Long updateTime = System.currentTimeMillis();
			List<PricingChangeRequest> foundPricingChangeRequests;
			PricingChangeRequest pricingChangeRequest;

			// if(slabRangeMins.size() != prices.size())
			// throw new Exception("Number of prices don't match with number of
			// active slabs");
			for (Price price : prices) {
				if (price.getPrice() < 0) {
					throw new Exception("Invalid input price:" + price);
				}

				slabId = getSlabId(price.getMin(), price.getMax(), teacherId);

				query = new Query();
				query.addCriteria(Criteria.where("slabId").is(slabId)
						.andOperator(Criteria.where("teacherId").is(teacherId).andOperator(Criteria.where("active")
								.is(true).andOperator(Criteria.where("teacherId").is(teacherId)))));

				foundPricingChangeRequests = pricingChangeRequestDAO.runQuery(query, PricingChangeRequest.class);
				if (!foundPricingChangeRequests.isEmpty()) {
					logger.info("Found requests for the same slab Id:" + foundPricingChangeRequests.size());
					pricingChangeRequest = foundPricingChangeRequests.get(0);
					if (!pricingChangeRequest.getPrice().equals(price.getPrice())) {
						logger.info("Disabling request for price:" + pricingChangeRequest.getPrice());
						pricingChangeRequest.setActive(false);
						pricingChangeRequest.setUpdateTime(updateTime);
						pricingChangeRequest.setState(State.PASSED);
						pricingChangeRequestDAO.deleteById(pricingChangeRequest.getId());
						pricingChangeRequestDAO.create(pricingChangeRequest);

						pricingChangeRequest = new PricingChangeRequest(slabId, teacherId, price.getPrice(), updateTime,
								true, State.PENDING);
						pricingChangeRequestDAO.create(pricingChangeRequest);
					} else {
						pricingChangeRequest.setUpdateTime(updateTime);
						pricingChangeRequestDAO.create(pricingChangeRequest);
					}

				} else {
					pricingChangeRequest = new PricingChangeRequest(slabId, teacherId, price.getPrice(), updateTime,
							true, State.PENDING);

					pricingChangeRequestDAO.create(pricingChangeRequest);
				}
			}

		} catch (Exception e) {

			logger.error(e);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
		}
		// }
		// else{
		// jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
		// jsonResp.setErrorMessage("Error fetching slab ranges");
		// }

		logger.info("Exiting");

		return new BaseResponse(ErrorCode.SUCCESS, "Created successfully");
	}

	public ApprovePricingChangeResponse approvePricingChangeRequest(ApprovePricingChangeRequest approvePricingChangeRequest)
			throws InternalServerErrorException {

		logger.info("Entering " + approvePricingChangeRequest.toString());

            Long teacherId = approvePricingChangeRequest.getTeacherId();
            int teacherMinPrice = 1000000000;
            int teacherMaxPrice = -1;
            int count = 0;
            int teacherOneHourRate = 0;
		try {
			String slabId;
			Query query;
			
			List<ApprovePrice> approvePrices = approvePricingChangeRequest.getApprovePrices();
			List<PricingChangeRequest> foundPricingChangeRequests;
			PricingChangeRequest pricingChangeRequest;
			List<Plan> foundPlans;
			Plan foundPlan, newPlan;
			

			for (ApprovePrice approvePrice : approvePrices) {
				if (approvePrice.isApproved()) {
					query = new Query();

					if (count == 0) {
						teacherOneHourRate = approvePrice.getPrice().intValue();
					}
					count++;
					slabId = getSlabId(approvePrice.getMin(), approvePrice.getMax(), teacherId);

					if (approvePrice.getPrice() <= teacherMinPrice) {
						teacherMinPrice = approvePrice.getPrice().intValue();
					}
					if (approvePrice.getPrice() >= teacherMaxPrice) {
						teacherMaxPrice = approvePrice.getPrice().intValue();
					}

					query = new Query();
					query.addCriteria(Criteria.where("slabId").is(slabId)
							.andOperator(Criteria.where("price").is(approvePrice.getPrice()).andOperator(Criteria
									.where("active").is(true).andOperator(Criteria.where("teacherId").is(teacherId)))));

					foundPricingChangeRequests = pricingChangeRequestDAO.runQuery(query, PricingChangeRequest.class);
					if (foundPricingChangeRequests.size() == 1) {
						pricingChangeRequest = foundPricingChangeRequests.get(0);
						pricingChangeRequest.setActive(false);
						pricingChangeRequest.setState(State.APPROVED);
						pricingChangeRequestDAO.deleteById(pricingChangeRequest.getId());
						pricingChangeRequestDAO.create(pricingChangeRequest);

						query = new Query();
						query.addCriteria(Criteria.where("slabId").is(slabId).andOperator(Criteria.where("teacherId")
								.is(teacherId).andOperator(Criteria.where("active").is(true))));

						foundPlans = planDAO.runQuery(query, Plan.class);
						if (foundPlans.size() == 1) {
							foundPlan = foundPlans.get(0);
							if (!foundPlan.getPrice().equals(approvePrice.getPrice())) {
								logger.info("Disabling Plan");
								foundPlan.setActive(false);
								planDAO.deleteById(foundPlan.getId());
								planDAO.create(foundPlan);

								newPlan = new Plan(slabId, teacherId, approvePrice.getPrice(), true);
								planDAO.create(newPlan);
							} else {
								planDAO.create(foundPlan);
							}

						} else if (foundPlans.size() == 0) {
							newPlan = new Plan(slabId, teacherId, approvePrice.getPrice(), true);
							planDAO.create(newPlan);
						} else {
							logger.error("More than 1 plans found for the same slab. Corrupt Data.");
							throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
									"More than 1 plans found for the same slab. Corrupt Data.");
						}

					} else {
						logger.error("Error fetching pricing change Request");
						throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
								"Error fetching pricing change Request");
					}

				}
			}

			List<TeacherStartsFrom> foundTeacherStartsFroms;
			TeacherStartsFrom foundTeacherStartsFrom;
			query = new Query();
			query.addCriteria(Criteria.where("teacherId").is(teacherId));
			foundTeacherStartsFroms = teacherStartsFromDAO.runQuery(query, TeacherStartsFrom.class);
			if (foundTeacherStartsFroms.size() == 1) {
				foundTeacherStartsFrom = foundTeacherStartsFroms.get(0);
				foundTeacherStartsFrom.setMinPrice(teacherMinPrice);
				foundTeacherStartsFrom.setMaxPrice(teacherMaxPrice);
				teacherStartsFromDAO.deleteById(foundTeacherStartsFrom.getId());
				foundTeacherStartsFrom.setId(null);
				teacherStartsFromDAO.create(foundTeacherStartsFrom);
			} else {
				teacherStartsFromDAO.create(new TeacherStartsFrom(teacherId, teacherMinPrice, teacherMaxPrice));
			}
                        //updateStartsFromInES(teacherId, teacherMinPrice, teacherOneHourRate);
		} catch (Exception e) {
			logger.error(e);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
		}
		logger.info("Exiting");

		//return new BaseResponse(ErrorCode.SUCCESS, "Approved successfully");
                return new ApprovePricingChangeResponse(teacherId, teacherMinPrice, teacherOneHourRate);
	}
//
//	private void updateStartsFromInES(Long teacherId, int teacherMinPrice, int teacherOneHourRate) {
//		logger.info("Entering teacherId:" + teacherId + " teacherMinPrice:" + teacherMinPrice);
//
//		teacherMinPrice = teacherMinPrice / 100;
//		teacherOneHourRate = teacherOneHourRate / 100;
//		MultivaluedMap<String, String> formData = new MultivaluedMapImpl();
//		formData.add("teacherId", String.valueOf(teacherId));
//		formData.add("teacherMinPrice", String.valueOf(teacherMinPrice));
//		formData.add("teacherOneHourRate", String.valueOf(teacherOneHourRate));
//		WebResource webResource = WebUtils.INSTANCE.getClient().resource(updateStartsFromInESUrl + "?teacherId="
//				+ teacherId + "&teacherMinPrice=" + teacherMinPrice + "&teacherOneHourRate=" + teacherOneHourRate);
//		ClientResponse resp = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).get(ClientResponse.class);
//
//		String output = resp.getEntity(String.class);
//		logger.info(output);
//	}

	public GetPricingChangeRequestsResponse getPricingChangeRequests(Long teacherId) {

		logger.info("Entering teacherId:" + teacherId);

		GetPricingChangeRequestsResponse jsonResp = new GetPricingChangeRequestsResponse();
		List<PricingChangeRequest> foundPricingChangeRequests;
		List<Plan> foundPlans;
		String slabId;
		Query query;
		List<ApprovePrice> approvePrices = new ArrayList<ApprovePrice>();
		ApprovePrice approvePrice;

		query = new Query();
		query.addCriteria(Criteria.where("teacherId").is(teacherId).andOperator(Criteria.where("active").is(true)));
		foundPricingChangeRequests = pricingChangeRequestDAO.runQuery(query, PricingChangeRequest.class);
		for (PricingChangeRequest foundPricingChangeRequest : foundPricingChangeRequests) {
			slabId = foundPricingChangeRequest.getSlabId();
			Tuple slabRangetuple = getSlabRanges(slabId);
			int min = (int) slabRangetuple.x;
			int max = (int) slabRangetuple.y;
			approvePrice = new ApprovePrice(min, max, foundPricingChangeRequest.getPrice(), false);
			approvePrices.add(approvePrice);

		}

		query = new Query();
		query.addCriteria(Criteria.where("teacherId").is(teacherId).andOperator(Criteria.where("active").is(true)));
		foundPlans = planDAO.runQuery(query, Plan.class);
		for (Plan foundPlan : foundPlans) {
			slabId = foundPlan.getSlabId();
			Tuple slabRangetuple = getSlabRanges(slabId);
			int min = (int) slabRangetuple.x;
			int max = (int) slabRangetuple.y;
			approvePrice = new ApprovePrice(min, max, foundPlan.getPrice(), true);
			approvePrices.add(approvePrice);
		}

		jsonResp.setRequests(approvePrices);
		logger.info("Exiting Response: " + jsonResp.toString());
		return jsonResp;

	}

	public GetTeacherSlabsResponse getTeacherSlabs(Long teacherId) {

		List<Plan> foundPlans;
		List<Slab> foundSlabs;
		List<SlabRange> foundSlabRanges;
		String slabId;
		Query query;
		List<com.vedantu.dinero.pojo.Slab> slabs = new ArrayList<com.vedantu.dinero.pojo.Slab>();
		int min, max;
		com.vedantu.dinero.pojo.Slab slab;

		GetTeacherSlabsResponse jsonResp = new GetTeacherSlabsResponse();
		List<String> slabIds = new ArrayList<String>();
		List<String> slabRangeIds = new ArrayList<String>();

		Map<String, Integer> finalSlabRangeIndexMap = new HashMap<String, Integer>();
		Map<String, Integer> finalSlabIndexMap = new HashMap<String, Integer>();

		query = new Query();
		query.addCriteria(Criteria.where("active").is(true));
		query.with(Sort.by(Sort.Direction.ASC, "creationTime"));
		foundSlabRanges = slabRangeDAO.runQuery(query, SlabRange.class);
		int count = 0, index;
		for (SlabRange foundSlabRange : foundSlabRanges) {
			slab = new com.vedantu.dinero.pojo.Slab();
			slab.setMin(foundSlabRange.getMin());
			slab.setMax(foundSlabRange.getMax());
			slab.setMinValue(foundSlabRange.getMinValue());
			finalSlabRangeIndexMap.put(foundSlabRange.getId(), count);
			count++;
			slabs.add(slab);
			slabRangeIds.add(foundSlabRange.getId());

		}

		query = new Query();
		query.addCriteria(
				Criteria.where("slabRangeId").in(slabRangeIds).andOperator(Criteria.where("active").is(true)));
		foundSlabs = slabDAO.runQuery(query, Slab.class);
		for (Slab foundSlab : foundSlabs) {
			index = finalSlabRangeIndexMap.get(foundSlab.getSlabRangeId());
			finalSlabIndexMap.put(foundSlab.getId(), index);
			slabIds.add(foundSlab.getId());
		}

		query = new Query();
		query.addCriteria(Criteria.where("teacherId").is(teacherId).andOperator(Criteria.where("active").is(true)));
		query.with(Sort.by(Sort.Direction.ASC, "creationTime"));
		foundPlans = planDAO.runQuery(query, Plan.class);
		for (Plan foundPlan : foundPlans) {
			slabId = foundPlan.getSlabId();
			if (finalSlabIndexMap.containsKey(slabId)) {
				index = finalSlabIndexMap.get(slabId);
				slab = slabs.get(index);
				slab.setPrice(foundPlan.getPrice());
				slabs.set(index, slab);
			}
		}

		Collections.sort(slabs, new Comparator<com.vedantu.dinero.pojo.Slab>() {
			public int compare(final com.vedantu.dinero.pojo.Slab object1, final com.vedantu.dinero.pojo.Slab object2) {
				return (object1.getMin() - object2.getMin());
			}
		});

		jsonResp.setSlabs(slabs);
		logger.info("Exiting Response: " + jsonResp.toString());
		return jsonResp;
	}

	public GetTeacherPlansResponse getTeacherPlans(Long teacherId) {

		Query query;
		List<TeacherSlabPlan> plans = new ArrayList<>();
		TeacherSlabPlan plan;
		List<Slab> foundSlabs;
		List<SlabRange> foundSlabRanges;

		GetTeacherPlansResponse jsonResp = new GetTeacherPlansResponse();
		List<String> slabIds = new ArrayList<>();
		List<String> slabRangeIds = new ArrayList<>();

		Map<String, String> slabSlabRangeMap = new HashMap<>();
		Map<String, SlabRange> slabRangeMinMaxMap = new HashMap<>();

		query = new Query();
		query.addCriteria(Criteria.where("teacherId").is(teacherId).andOperator(Criteria.where("active").is(true)));
		List<Plan> foundPlans = planDAO.runQuery(query, Plan.class);
		for (Plan foundPlan : foundPlans) {
			slabIds.add(foundPlan.getSlabId());
		}

		query = new Query();
		query.addCriteria(Criteria.where("_id").in(slabIds));
		foundSlabs = slabDAO.runQuery(query, Slab.class);
		for (Slab foundSlab : foundSlabs) {
			slabSlabRangeMap.put(foundSlab.getId(), foundSlab.getSlabRangeId());
			slabRangeIds.add(foundSlab.getSlabRangeId());
		}

		query = new Query();
		query.addCriteria(Criteria.where("_id").in(slabRangeIds));
		foundSlabRanges = slabRangeDAO.runQuery(query, SlabRange.class);
		for (SlabRange foundSlabRange : foundSlabRanges) {
			slabRangeMinMaxMap.put(foundSlabRange.getId(), foundSlabRange);
		}

		SlabRange slabRange;
		for (Plan foundPlan : foundPlans) {
			slabRange = slabRangeMinMaxMap.get(slabSlabRangeMap.get(foundPlan.getSlabId()));
			plan = new TeacherSlabPlan(foundPlan.getId(), slabRange.getMin(), slabRange.getMax(), foundPlan.getPrice());
			plans.add(plan);
		}

		Collections.sort(plans, new Comparator<TeacherSlabPlan>() {
			public int compare(final TeacherSlabPlan object1, final TeacherSlabPlan object2) {
				return (object1.getMin() - object2.getMin());
			}
		});

		jsonResp.setPlans(plans);
		logger.info("Exiting Response: " + jsonResp.toString());
		return jsonResp;
	}

	public GetPlanByIdResponse getPlanById(String planId) throws NotFoundException {

		logger.info("Entering " + planId);
		List<Plan> foundPlans;
		Plan foundPlan;

		Query query;
		GetPlanByIdResponse successResp;

		query = new Query();
		query.addCriteria(Criteria.where("id").is(planId));
		foundPlans = planDAO.runQuery(query, Plan.class);
		if (foundPlans.size() == 1) {
			foundPlan = foundPlans.get(0);
			successResp = new GetPlanByIdResponse(foundPlan);
		} else {
			logger.error("No Plan Found");
			throw new NotFoundException(ErrorCode.PLAN_NOT_FOUND, "No plan found");
		}

		logger.info("Exiting Response: " + successResp.toString());
		return successResp;
	}

	public GetTeacherPlansResponse getPlansByIds(List<String> planIds) {
		//
		// List<Plan> foundPlans;
		//
		// Query query;
		// GetPlansByIdsResponse successResp = new GetPlansByIdsResponse();
		//
		// query = new Query();
		// query.addCriteria(Criteria.where("id").in(planIds));
		// foundPlans = planDAO.runQuery(query, Plan.class);
		// for (Plan foundPlan : foundPlans) {
		// successResp.addPlan(foundPlan);
		// }
		//
		// successResp.setErrorCode(ErrorCode.SUCCESS);
		// successResp.setErrorMessage("Plans fetched Successfully");
		// logger.info("Exiting Response: " + successResp.toString());
		// return successResp;
		List<Plan> foundPlans;
		String slabId;
		Query query;
		List<TeacherSlabPlan> plans = new ArrayList<TeacherSlabPlan>();
		int min, max;
		TeacherSlabPlan plan;
		List<Slab> foundSlabs;
		List<SlabRange> foundSlabRanges;

		GetTeacherPlansResponse jsonResp = new GetTeacherPlansResponse();
		List<String> slabIds = new ArrayList<String>();
		List<String> slabRangeIds = new ArrayList<String>();

		Map<String, String> slabSlabRangeMap = new HashMap<String, String>();
		Map<String, SlabRange> slabRangeMinMaxMap = new HashMap<String, SlabRange>();

		query = new Query();
		query.addCriteria(Criteria.where("id").in(planIds));
		foundPlans = planDAO.runQuery(query, Plan.class);
		for (Plan foundPlan : foundPlans) {
			logger.info(foundPlan.toString());
			slabIds.add(foundPlan.getSlabId());
		}

		query = new Query();
		query.addCriteria(Criteria.where("_id").in(slabIds));
		foundSlabs = slabDAO.runQuery(query, Slab.class);
		for (Slab foundSlab : foundSlabs) {
			slabSlabRangeMap.put(foundSlab.getId(), foundSlab.getSlabRangeId());
			slabRangeIds.add(foundSlab.getSlabRangeId());
		}

		query = new Query();
		query.addCriteria(Criteria.where("_id").in(slabRangeIds));
		foundSlabRanges = slabRangeDAO.runQuery(query, SlabRange.class);
		for (SlabRange foundSlabRange : foundSlabRanges) {
			slabRangeMinMaxMap.put(foundSlabRange.getId(), foundSlabRange);
		}

		SlabRange slabRange;
		for (Plan foundPlan : foundPlans) {
			slabRange = slabRangeMinMaxMap.get(slabSlabRangeMap.get(foundPlan.getSlabId()));
			plan = new TeacherSlabPlan(foundPlan.getId(), slabRange.getMin(), slabRange.getMax(), foundPlan.getPrice(),
					foundPlan.getTeacherId());
			plans.add(plan);
		}

		Collections.sort(plans, new Comparator<TeacherSlabPlan>() {
			public int compare(final TeacherSlabPlan object1, final TeacherSlabPlan object2) {
				return (object1.getMin() - object2.getMin());
			}
		});

		jsonResp.setPlans(plans);
		logger.info("Exiting Response: " + jsonResp.toString());
		return jsonResp;
	}

	public GetPlanByIdResponse getPlanByHours(Long teacherId, Double hours) throws NotFoundException {
		logger.info("Entering teacherId:" + teacherId + " hours:" + hours);
		List<Plan> foundPlans;

		Query query;
		GetPlanByIdResponse successResp = new GetPlanByIdResponse();
		String slabId;
		int min, max;
		query = new Query();
		query.addCriteria(Criteria.where("teacherId").is(teacherId).andOperator(Criteria.where("active").is(true)));
		foundPlans = planDAO.runQuery(query, Plan.class);
		for (Plan foundPlan : foundPlans) {
			slabId = foundPlan.getSlabId();
			Tuple slabRangetuple = getSlabRanges(slabId);
			min = (int) slabRangetuple.x;
			max = (int) slabRangetuple.y;
			if ((Double.compare(Double.parseDouble(String.valueOf(min)), hours) < 0)
					&& ((Double.compare(Double.parseDouble(String.valueOf(max)), hours) == 0)
							|| Double.compare(Double.parseDouble(String.valueOf(max)), hours) > 0 || max == -1)) {
				successResp = new GetPlanByIdResponse(foundPlan);
			}
		}

		if (successResp.getId() == null) {
			logger.warn("No Plan found for the given hours range");
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No Plan found for the given hours range");
		} else {
			logger.info("Exiting Response: " + successResp.toString());
			return successResp;
		}
	}

	public Boolean processIncentiveData(File incentiveFile) throws InternalServerErrorException {

		logger.info("Entering ");
		String line;
		String[] inputLine;
		TeacherIncentive teacherIncentive;
		Query query;
		List<TeacherIncentive> foundTeacherIncentives;
		TeacherIncentive foundTeacherIncentive;
		// SessionFactory sessionFactory = null;
		// Session session = null;
		// Transaction transaction = null;
		// try {
		// sessionFactory = sqlSessionFactory.getSessionFactory();
		// session = sessionFactory.openSession();
		// transaction = session.getTransaction();
		// } catch (Exception e) {
		// logger.error(e);
		// throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR,
		// e.getMessage());
		// }
		// org.hibernate.Criteria cr;
		Long teacherId;
		int count = 1;
		List<Account> accounts;
		Account account;
		int balance;
		int incentive;
		SessionModel model;

		if (incentiveFile != null) {
			// transaction.begin();
			try {
				BufferedReader br = new BufferedReader(new FileReader(incentiveFile));
				line = br.readLine();

				for (; (line = br.readLine()) != null;) {
					if (!line.isEmpty()) {

						inputLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
						// Incorrect input
						if (inputLine.length != 5 || inputLine[0].length() != 16) {
							throw new RuntimeException("Illegal values on line: " + count);
						}

						teacherId = Long.parseLong(inputLine[0]);
						incentive = Integer.valueOf(inputLine[4]);
						model = SessionModel.valueOf(inputLine[1]);
						teacherIncentive = new TeacherIncentive(teacherId, model, Month.valueOf(inputLine[2]),
								inputLine[3], incentive);

						// query = new Query();
						// query.addCriteria(Criteria.where("teacherId").is(teacherIncentive.getTeacherId())
						// .andOperator(Criteria.where("month").is(teacherIncentive.getMonth().toString())
						// .andOperator(Criteria.where("model")
						// .is(teacherIncentive.getModel().toString()))));
						// foundTeacherIncentives =
						// teacherIncentiveDAO.runQuery(query,
						// TeacherIncentive.class);
						//
						// if (!foundTeacherIncentives.isEmpty() &&
						// foundTeacherIncentives.size() == 1) {
						// foundTeacherIncentive =
						// foundTeacherIncentives.get(0);
						// teacherIncentive.setId(foundTeacherIncentive.getId());
						// teacherIncentiveDAO.deleteById(foundTeacherIncentive.getId());
						// teacherIncentiveDAO.create(teacherIncentive);
						// } else {
						teacherIncentiveDAO.create(teacherIncentive);

						// }
						// cr = session.createCriteria(Account.class);
						//
						// // Give Teacher Incentive in Wallet
						// String teacherAccountId =
						// getUserAccountHolderId(teacherId.toString());
						// cr.add(Restrictions.eq("holderId",
						// teacherAccountId));
						// accounts = accountDAO.runQuery(session, cr,
						// Account.class);
						// if (accounts.size() == 1) {
						// account = accounts.get(0);
						// balance = account.getBalance();
						// balance = balance + incentive;
						// account.setBalance(balance);
						// accountDAO.create(account, session);
						// logger.info("Update teacher balance to:" + balance);
						// } else
						// throw new Exception("Teacher Wallet Entry Not
						// Found");
						//
						// // Update Vedantu AIR account with debit
						// cr = session.createCriteria(Account.class);
						// String vedantuAIRAccountId =
						// getVedantuAccountHolderId(vedantuAIRHolderId);
						// cr.add(Restrictions.eq("holderId",
						// vedantuAIRAccountId));
						// cr.add(Restrictions.eq("holderName",
						// vedantuAIRHolderName));
						// cr.add(Restrictions.eq("holderType",
						// HolderType.VEDANTU));
						//
						// accounts = accountDAO.runQuery(session, cr,
						// Account.class);
						// if (accounts.size() == 1) {
						// account = accounts.get(0);
						// balance = account.getBalance();
						// balance = balance - incentive;
						// account.setBalance(balance);
						// accountDAO.create(account, session);
						// logger.info("Update vedantu air balance to:" +
						// balance);
						// } else {
						// throw new Exception("Vedantu Air Account does not
						// Exist");
						// }
						// Add transaction for the incentive
						accountManager.transferAmount(accountDAO.getVedantuAIRAccount(),
								accountManager.getAccountByUserId(teacherId), incentive, incentive, 0,
								com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
								com.vedantu.dinero.pojo.BillingReasonType.FREEBIES, TransactionRefType.INCENTIVE,
								model.name(), com.vedantu.dinero.entity.Transaction._getTriggredBySystem(), true, null);
						// com.vedantu.dinero.entity.Transaction
						// incentiveTransaction = new
						// com.vedantu.dinero.entity.Transaction(
						// incentive,
						// com.vedantu.dinero.entity.Transaction.Constants.DEFAULT_CURRENCY_CODE,
						// vedantuAIRAccountId, teacherAccountId,
						// com.vedantu.dinero.enums.BillingReasonType.INCENTIVE.name(),
						// model.name(),
						// TransactionRefType.INCENTIVE,
						// com.vedantu.dinero.entity.Transaction._getTriggredBySystem());
						// logger.info(incentiveTransaction.toString());
						// transactionDAO.create(incentiveTransaction);
						count++;
					}
				}
				// transaction.commit();
			} catch (Exception e) {
				// if (transaction.getStatus().equals(TransactionStatus.ACTIVE))
				// {
				// logger.error("calculateSessionPayout Rollback: " +
				// e.getMessage());
				// session.getTransaction().rollback();
				// }
				logger.error(e);
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
			} finally {
				// session.close();
			}
		}

		return true;
	}

	public Boolean processTeacherPayoutRate(File payoutFile) throws InternalServerErrorException {

		logger.info("Entering ");
		String line;
		String[] inputLine;
		TeacherPayoutRate teacherPayoutRate;
		int count = 1;
		Query query;
		List<TeacherPayoutRate> foundTeacherPayoutRates;
		TeacherPayoutRate foundTeacherPayoutRate;
		Double cut;

		if (payoutFile != null) {
			try (BufferedReader br = new BufferedReader(new FileReader(payoutFile))) {
				// ignoring first header line
				line = br.readLine();
				for (; (line = br.readLine()) != null;) {
					if (!line.isEmpty()) {
						try {
							inputLine = line.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)", -1);
							// Incorrect input
							cut = Double.parseDouble(inputLine[4]);
							if (inputLine.length != 5 || inputLine[0].length() != 16 || cut < 0 || cut > 100) {
								throw new RuntimeException();
							}

							SessionModel model = null;
							SessionEntity entity = null;
							String entityId = "";
							if (!inputLine[1].equals("")) {
								model = SessionModel.valueOf(inputLine[1]);
							}
							if (!inputLine[3].equals("")) {
								entityId = inputLine[3];
							}
							if (!inputLine[2].equals("")) {
								entity = SessionEntity.valueOf(inputLine[2]);
							} else {
								entityId = "";
							}

							teacherPayoutRate = new TeacherPayoutRate(Long.parseLong(inputLine[0]), model, null, null,
									null, cut, entity, entityId);
							query = new Query();
							query.addCriteria(Criteria.where("teacherId").is(teacherPayoutRate.getTeacherId()));
							query.addCriteria(Criteria.where("model").is(teacherPayoutRate.getModel()));
							query.addCriteria(Criteria.where("entity").is(teacherPayoutRate.getEntity()));
							query.addCriteria(Criteria.where("entityId").is(teacherPayoutRate.getEntityId()));

							foundTeacherPayoutRates = teacherPayoutRateDAO.runQuery(query, TeacherPayoutRate.class);
							if (!foundTeacherPayoutRates.isEmpty() && foundTeacherPayoutRates.size() == 1) {
								foundTeacherPayoutRate = foundTeacherPayoutRates.get(0);
								teacherPayoutRateDAO.deleteById(foundTeacherPayoutRate.getId());

								teacherPayoutRateDAO.create(teacherPayoutRate);
							} else {
								teacherPayoutRateDAO.create(teacherPayoutRate);
							}
							count++;
						} catch (RuntimeException e) {
							logger.warn(e);
							logger.warn("llegal field values on entry number: " + count);
							count++;
						}
					}
				}
			} catch (Exception e) {
				logger.error(e);
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
			}
		}
		logger.info("Exiting");
		return true;
	}

	public GetAllRequestsResponse getAllRequests(Integer start, Integer limit) {

		logger.info("Entering start:" + start + " limit:" + limit);

		GetAllRequestsResponse jsonResp = new GetAllRequestsResponse();
		List<PricingChangeRequest> foundPricingChangeRequests;
		List<Plan> foundPlans;
		String slabId;
		Query query;
		List<ApprovePriceWithTime> approvePrices;
		ApprovePriceWithTime approvePrice;
		Map<Long, List<ApprovePriceWithTime>> teacherIdRequestMap = new HashMap<Long, List<ApprovePriceWithTime>>();
		Long teacherId;
		List<TeacherRequest> teacherRequests = new ArrayList<TeacherRequest>();
		List<String> userIds = new ArrayList<String>();
		List<UserBasicInfo> userBasicInfos = new ArrayList<UserBasicInfo>();
		Map<String, UserBasicInfo> userInfoIndexMap = new HashMap<String, UserBasicInfo>();
		UserBasicInfo userBasicInfo;
		TeacherRequest teacherRequest;

		query = new Query();
		query.addCriteria(Criteria.where("active").is(true));

		query.with(Sort.by(Sort.Direction.DESC, "updateTime"));
		query.skip(start * 5);
		// Every teacher has 5 slabs
		query.limit(limit * 5);

		foundPricingChangeRequests = pricingChangeRequestDAO.runQuery(query, PricingChangeRequest.class);
		for (PricingChangeRequest foundPricingChangeRequest : foundPricingChangeRequests) {

			slabId = foundPricingChangeRequest.getSlabId();
			Tuple slabRangetuple = getSlabRanges(slabId);
			int min = (int) slabRangetuple.x;
			int max = (int) slabRangetuple.y;
			approvePrice = new ApprovePriceWithTime(min, max, foundPricingChangeRequest.getPrice(), false,
					foundPricingChangeRequest.getUpdateTime());
			teacherId = foundPricingChangeRequest.getTeacherId();
			approvePrices = new ArrayList<ApprovePriceWithTime>();
			if (teacherIdRequestMap.containsKey(teacherId)) {
				approvePrices = teacherIdRequestMap.get(teacherId);
				approvePrices.add(approvePrice);
				teacherIdRequestMap.put(teacherId, approvePrices);
			} else {
				// userIds.add(teacherId.toString());
				approvePrices.add(approvePrice);
				teacherIdRequestMap.put(teacherId, approvePrices);
			}
		}
		//
		// userBasicInfos = getUserBasicInfos(userIds);
		//
		// if (userBasicInfos != null && !userBasicInfos.isEmpty()) {
		// for (UserBasicInfo userBasicInfoTemp : userBasicInfos) {
		// if (userBasicInfoTemp.getUserId() != null) {
		// userInfoIndexMap.put(userBasicInfoTemp.getUserId().toString(),
		// userBasicInfoTemp);
		// }
		// }
		//
		// }

		for (Map.Entry<Long, List<ApprovePriceWithTime>> entry : teacherIdRequestMap.entrySet()) {
			teacherId = entry.getKey();
			approvePrices = entry.getValue();

			Collections.sort(approvePrices, new Comparator<ApprovePriceWithTime>() {
				public int compare(final ApprovePriceWithTime object1, final ApprovePriceWithTime object2) {
					return (object1.getMin() - object2.getMin());
				}

			});
			userBasicInfo = userInfoIndexMap.get(teacherId.toString());
			teacherRequest = new TeacherRequest();
			teacherRequest.setRequests(approvePrices);
			teacherRequest.setUserId(teacherId);
			// teacherRequest.setUserBasicInfo(userBasicInfo);
			teacherRequests.add(teacherRequest);
		}

		// sort the requests
		Collections.sort(teacherRequests, new Comparator<TeacherRequest>() {
			public int compare(final TeacherRequest object1, final TeacherRequest object2) {
				return (object1.getRequests().get(0).getUpdateTime())
						.compareTo(object2.getRequests().get(0).getUpdateTime());
			}
		});

		jsonResp.setTeacherRequests(teacherRequests);
		logger.info("Exiting Response: " + jsonResp.toString());
		return jsonResp;
	}

	public String getSlabId(int min, int max, Long teacherId) {

		Query query = new Query();
		String slabRangeId, slabId;

		query.addCriteria(Criteria.where("min").is(min)
				.andOperator(Criteria.where("max").is(max).andOperator(Criteria.where("active").is(true))));
		slabRangeId = slabRangeDAO.runQuery(query, SlabRange.class).get(0).getId();

		query = new Query();
		query.addCriteria(Criteria.where("slabRangeId").is(slabRangeId).andOperator(Criteria.where("active").is(true)));
		slabId = slabDAO.runQuery(query, Slab.class).get(0).getId();

		return slabId;

	}

	public Tuple getSlabRanges(String slabId) {

		String slabRangeId = slabDAO.getById(slabId).getSlabRangeId();
		SlabRange slabRange = slabRangeDAO.getById(slabRangeId);

		return new Tuple(slabRange.getMin(), slabRange.getMax());

	}

	public class Tuple<X, Y> {

		public final X x;
		public final Y y;

		public Tuple(X x, Y y) {
			this.x = x;
			this.y = y;
		}

		@Override
		public String toString() {
			return "Tuple [x=" + x + ", y=" + y + "]";
		}

	}

        /*
	public List<UserBasicInfo> getUserBasicInfos(List<String> userIds) {

		logger.info("Entering userIds:" + userIds.toString());

		if (userIds == null || userIds.isEmpty()) {
			return null;
		}

		List<UserBasicInfo> users = new ArrayList<UserBasicInfo>();
		String url = userFetchUrl + "?type=user&" + "userIdsList="
				+ StringUtils.arrayToDelimitedString(userIds.toArray(), ",");
		ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
		String output = response.getEntity(String.class);
		if (!StringUtils.isEmpty(output)) {
			try {
				Gson gson = new Gson();
				Type listType = new TypeToken<ArrayList<UserBasicInfo>>() {
				}.getType();
				List<UserBasicInfo> result = gson.fromJson(output, listType);
				if (result != null && !result.isEmpty()) {
					users.addAll(result);
				}
			} catch (Exception ex) {
				logger.error("Exception while parsing userBasicInfos", ex);
			}
		}

		logger.info("Exiting users:" + users);
		return users;
	}
        */

	public static String getUserAccountHolderId(String userId) {

		return "user/" + userId;
	}

	public static String getVedantuAccountHolderId(String id) {

		return "vedantu/" + id;

	}

	// to be used only for internal purposes for creating plans directly
	// TODO query is not optimised, too many queries
	public BaseResponse createPricingPlans(CreatePricingChangeRequest pricingReq) throws InternalServerErrorException {

		logger.info("Entering " + pricingReq.toString());

		try {
			String slabId;
			Long teacherId = pricingReq.getTeacherId();
			List<Price> prices = pricingReq.getPrices();
			for (Price price : prices) {
				slabId = getSlabId(price.getMin(), price.getMax(), teacherId);

				Query query = new Query();
				query.addCriteria(
						Criteria.where("slabId").is(slabId).andOperator(Criteria.where("teacherId").is(teacherId)));

				List<Plan> foundPlans = planDAO.runQuery(query, Plan.class);
				Plan foundPlan;
				if (foundPlans.size() > 0) {
					foundPlan = foundPlans.get(0);
					foundPlan.setPrice(price.getPrice());
					foundPlan.setActive(true);
				} else {
					foundPlan = new Plan(slabId, teacherId, price.getPrice(), true);
				}

				planDAO.create(foundPlan);

				if (foundPlans.size() > 1) {
					logger.warn("for slabId " + slabId + ", teacherId " + teacherId + ", active true, plans length is "
							+ foundPlans.size());
				}
			}
		} catch (Exception e) {
			logger.error(e);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
		}
		logger.info("Exiting");
		return new BaseResponse(ErrorCode.SUCCESS, "Approved successfully");
	}

	public Plan updateCustomPlanRequest(UpdateCustomPlanRequest customPlanRequest) throws NotFoundException {
		logger.info("Entering - " + customPlanRequest.toString());
		String slabId = null;

		List<Slab> allSlabs = slabDAO.getAllSlabs();
		List<SlabRange> allSlabRanges = slabRangeDAO.getAllSlabRanges();
		Map<String, SlabRange> slabRangeMap = new HashMap<>();
		if (ArrayUtils.isNotEmpty(allSlabRanges)) {
			for (SlabRange slabRange : allSlabRanges) {
				slabRangeMap.put(slabRange.getId(), slabRange);
			}
		}

		Long hours = customPlanRequest.getTotalHours();
		if (ArrayUtils.isNotEmpty(allSlabs) && hours != null) {
			for (Slab slab : allSlabs) {
				if (slabRangeMap.containsKey(slab.getSlabRangeId())) {
					SlabRange slabRange = slabRangeMap.get(slab.getSlabRangeId());
					if (slabRange != null) {
						int min = slabRange.getMin();
						int max = slabRange.getMax();
						if ((Double.compare(Double.parseDouble(String.valueOf(min)), hours) < 0)
								&& ((Double.compare(Double.parseDouble(String.valueOf(max)), hours) == 0)
										|| Double.compare(Double.parseDouble(String.valueOf(max)), hours) > 0
										|| max == -1)) {
							slabId = slab.getId();
							break;
						}
					}
				}
			}
		}

		if (StringUtils.isEmpty(slabId)) {
			logger.error("updateCustomPlanRequest - Slab not found for request - " + customPlanRequest);
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Slab not found for request : " + customPlanRequest);
		}

		Plan plan;
		Long totalHours = customPlanRequest.getTotalHours();
		if (totalHours == null || totalHours <= 0l) {
			totalHours = 1l;
			logger.error("Total hours is null for request : " + customPlanRequest.toString());
		}

		Long price = customPlanRequest.getPrice() / customPlanRequest.getTotalHours();
		if (customPlanRequest.getPlanId() != null) {
			plan = planDAO.getById(customPlanRequest.getPlanId());
			plan.setTeacherId(customPlanRequest.getTeacherId());
			plan.setPrice(price);
			plan.setActive(customPlanRequest.isActive());
			plan.setSlabId(slabId);
		} else {
			plan = new Plan(slabId, customPlanRequest.getTeacherId(), price, customPlanRequest.isActive());
		}
		planDAO.create(plan);
		logger.info("Exiting - " + plan);
		return plan;
	}
}
