package com.vedantu.dinero.managers.payment;

import com.google.gson.Gson;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.payment.axis.AxisDecryptionUtil;
import com.vedantu.dinero.managers.payment.axis.AxisEncrpytionUtil;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.*;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@SuppressWarnings("Duplicates")
@Service
public class AxisPaymentManager implements IPaymentManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AxisPaymentManager.class);

    @Autowired
    public FosUtils fosUtils;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public AccountDAO accountDAO;

    private static final String PAYMENT_CHANNEL = "AXIS";

    private static final String FIELD_VERSION = "vpc_Version";
    private static final String FIELD_ACCESS_CODE = "vpc_AccessCode";
    private static final String FIELD_COMMAND = "vpc_Command";
    private static final String FIELD_MERC_TX_REF = "vpc_MerchTxnRef";
    private static final String FIELD_MERCHANT_ID = "vpc_MerchantId";
    private static final String FIELD_ORDER_INFO = "vpc_OrderInfo";
    private static final String FIELD_AMOUNT = "vpc_Amount";
    private static final String FIELD_RETURN_URL = "vpc_ReturnURL";
    private static final String FIELD_SECURE_HASH = "vpc_SecureHash";
    private static final String FIELD_TX_RESP_CODE = "vpc_TxnResponseCode";
    private static final String FIELD_ENC_DATA = "EncData";

    private static final String FIELD_MERCHANT_ID_RES = "MerchantId";
    private static final String FIELD_ENC_DATA_RES = "EncDataResp";

    private final PaymentGatewayName gatewayName = PaymentGatewayName.AXIS;

    private String MERCHANT_ID;
    private String CHARGING_URL;
    private String REDIRECT_URL;
    private String ACCESS_CODE;
    private String SECURE_SECRET;
    private String ENCRYPTION_KEY;

    private Gson gson = new Gson();

    public AxisPaymentManager() {
        super();
        MERCHANT_ID = ConfigUtils.INSTANCE.getStringValue("billing.axis.merchant_id");
        CHARGING_URL = ConfigUtils.INSTANCE.getStringValue("billing.axis.charging.url");
        REDIRECT_URL = ConfigUtils.INSTANCE.getStringValue("billing.axis.redirect_url");
        ACCESS_CODE = ConfigUtils.INSTANCE.getStringValue("billing.axis.access_code");
        SECURE_SECRET = ConfigUtils.INSTANCE.getStringValue("billing.axis.checksum.secretKey");
        ENCRYPTION_KEY = ConfigUtils.INSTANCE.getStringValue("billing.axis.checksum.encryptionKey");
    }

    @Override
    public PaymentGatewayName getName() {
        return this.gatewayName;
    }

    @Override
    public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException {
        Map<String, String> httpParams = new HashMap<>();
        int amountValue = transaction.getAmount();

        // Gateway specific params
        httpParams.put(FIELD_VERSION, "1");
        httpParams.put(FIELD_ACCESS_CODE, ACCESS_CODE);
        httpParams.put(FIELD_COMMAND, "pay");
        httpParams.put(FIELD_MERC_TX_REF, transaction.getId());
        httpParams.put(FIELD_MERCHANT_ID, MERCHANT_ID);
        httpParams.put(FIELD_ORDER_INFO, transaction.getId());
        httpParams.put(FIELD_AMOUNT, String.valueOf(amountValue));
        httpParams.put(FIELD_RETURN_URL, REDIRECT_URL);

        Map<String, Object> postParams = getPostParams(httpParams);
        String url = CHARGING_URL;
        logger.info("payment channel [" + PAYMENT_CHANNEL + "] payment url :  " + url);
        logger.info("payment channel [" + PAYMENT_CHANNEL + "] payment params :  " + postParams);
        return new RechargeUrlInfo(PaymentGatewayName.AXIS, url, "POST", postParams);
    }

    @Override
    public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId) throws NotFoundException, InternalServerErrorException, ConflictException, BadRequestException {
        // VERIFY THE RESPONSE FROM SERVER
        verifyRequest(transactionInfo, callingUserId);

        logger.info(transactionInfo.toString());
        String encResp;
        List<String> list;

        if (transactionInfo.get(FIELD_ENC_DATA_RES) == null) {
            encResp = null;
        } else {
            //noinspection unchecked
            list = (List<String>) transactionInfo.get(FIELD_ENC_DATA_RES);
            encResp = list.get(0);
        }
        if (StringUtils.isEmpty(encResp)) {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
        }
        logger.info("encResp: " + encResp);
        AxisDecryptionUtil decryptionUtil = new AxisDecryptionUtil(SECURE_SECRET, ENCRYPTION_KEY);

        String encData = decryptionUtil.decrypt(encResp);
        logger.info("responseParams: " + encData);


        Map<String, String> fields = new HashMap<>();

        String[] pipeSplit = encData.split("::");
        for (String param : pipeSplit) {
            String[] pareValues = param.split("\\|\\|");
            if (pareValues[1] != null && pareValues[1].length() > 0 &&
                    !pareValues[1].equalsIgnoreCase("null")) {
                fields.put(pareValues[0], pareValues[1]);
            }
        }

        /*  If there has been a merchant secret set then sort and loop through all the
            data in the Virtual Payment Client response. while we have the data, we can
            append all the fields that contain values (except the secure hash) so that
            we can create a hash and validate it against the secure hash in the Virtual
            Payment Client response.

            NOTE: If the vpc_TxnResponseCode in not a single character then
            there was a Virtual Payment Client error and we cannot accurately validate
            the incoming data from the secure hash. */

        // remove the vpc_TxnResponseCode code from the response fields as we do not
        // want to include this field in the hash calculation
        String vpc_Txn_Secure_Hash = fields.remove("vpc_SecureHash");

        // defines if error message should be output
        boolean errorExists = false;

        if (SECURE_SECRET != null && SECURE_SECRET.length() > 0 &&
                (fields.get("vpc_TxnResponseCode") != null || !fields.get("vpc_TxnResponseCode").equals("No Value Returned"))) {

            // create secure hash and append it to the hash map if it was created
            // remember if SECURE_SECRET = "" it wil not be created
            String secureHash = decryptionUtil.hashAllFields(fields);

            // Validate the Secure Hash (remember SHA-256 hashes are not case sensitive)
            if (!vpc_Txn_Secure_Hash.equalsIgnoreCase(secureHash)) {
                // Secure Hash validation failed
                errorExists = true;
            }
        } else {
            // Secure Hash was not validated,
            errorExists = true;
        }

        // Extract the available receipt fields from the VPC Response
        // If not present then let the value be equal to 'Unknown'
        // Standard Receipt Data
        String amount = fields.get("vpc_Amount");
        String message = fields.get("vpc_Message");

        String vCSCResultCode = fields.get("vpc_CSCResultCode");
        String cscResponse = decryptionUtil.displayCSCResponse(vCSCResultCode);
        fields.put("CSCResponse", cscResponse);

        String verStatus3DS = fields.get("vpc_VerStatus");
        String statusDescription = decryptionUtil.getStatusDescription(verStatus3DS);
        fields.put("verStatus3DSDescription", statusDescription);

        /*String locale = fields.get("vpc_Locale");
        String batchNo = fields.get("vpc_BatchNo");
        String command = fields.get("vpc_Command");
        String version = fields.get("vpc_Version");
        String cardType = fields.get("vpc_Card");
        String orderInfo = fields.get("vpc_OrderInfo");
        String receiptNo = fields.get("vpc_ReceiptNo");
        String merchantID = fields.get("vpc_Merchant");
        String merchTxnRef = fields.get("vpc_MerchTxnRef");
        String authorizeID = fields.get("vpc_AuthorizeId");
        String transactionNo = fields.get("vpc_TransactionNo");
        String acqResponseCode = fields.get("vpc_AcqResponseCode");
        String txnResponseCode = fields.get("vpc_TxnResponseCode");

        // CSC Receipt Data
        String vCSCResultCode = fields.get("vpc_CSCResultCode");
        String vCSCRequestCode = fields.get("vpc_CSCRequestCode");
        String vACQCSCRespCode = fields.get("vpc_AcqCSCRespCode");

        // 3-D Secure Data
        String transType3DS = fields.get("vpc_VerType");
        String verStatus3DS = fields.get("vpc_VerStatus");
        String token3DS = fields.get("vpc_VerToken");
        String secureLevel3DS = fields.get("vpc_VerSecurityLevel");
        String enrolled3DS = fields.get("vpc_3DSenrolled");
        String xid3DS = fields.get("vpc_3DSXID");
        String eci3DS = fields.get("vpc_3DSECI");
        String status3DS = fields.get("vpc_3DSstatus");*/

        String transactionId = fields.get(FIELD_MERC_TX_REF);

        String paymentChannelTransactionId = fields.get("vpc_TransactionNo");

        String txStatus = fields.get(FIELD_TX_RESP_CODE);
        String responseDescription = decryptionUtil.getResponseDescription(txStatus);

        TransactionStatus transactionStatus;

        if (responseDescription.equalsIgnoreCase("Transaction Successful") && message.trim().equalsIgnoreCase("approved")) {
            transactionStatus = TransactionStatus.SUCCESS;
        } else if (responseDescription.equalsIgnoreCase("Transaction Aborted")) {
            transactionStatus = TransactionStatus.CANCELLED;
        } else {
            transactionStatus = TransactionStatus.FAILED;
        }

        String paymentMethod = fields.get("payment_mode");

        String paymentInstrument = fields.get("vpc_Card");

        String bankRefNo = fields.get("vpc_AuthorizeId");

        int amountPaid = Integer.parseInt(amount);

        if (transactionStatus != TransactionStatus.SUCCESS) {
            amountPaid = 0;
        }

        logger.info("transaction status : " + transactionStatus);

        ExtTransaction extTransaction;
        logger.info("starting transaction ");
        extTransaction = extTransactionDAO.getById(transactionId);
        logger.info("extTransaction before updation : " + extTransaction);

        // Save output to DB
        extTransaction.setGatewayResponse(gson.toJson(fields));

        if (extTransaction.getStatus() != TransactionStatus.PENDING) {

            throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
                    "transaction with id " + transactionId + " is already processed");
        }
        extTransaction.setStatus(transactionStatus);
        extTransaction.setGatewayStatus(message);
        extTransaction.setTransactionTime(String.valueOf(System.currentTimeMillis()));
        extTransaction.setPaymentChannelTransactionId(paymentChannelTransactionId);
        extTransaction.setPaymentInstrument(paymentInstrument);
        extTransaction.setPaymentMethod(paymentMethod);
        extTransaction.setBankRefNo(bankRefNo);
        logger.info("saving extTransaction: " + extTransaction);
        extTransactionDAO.create(extTransaction, callingUserId);
        if (extTransaction.getAmount() == amountPaid && transactionStatus == TransactionStatus.SUCCESS && !errorExists) {
            // TODO: these functionality can be moved to abstract class
            // transaction = TransactionDAO.INSTANCE
            // .upsertTransaction(transaction);

            Account account;
            // TODO: take this code to common place, where update and
            // deduct
            // operation can be done
            logger.info("transaction was successful:" + extTransaction);
            account = accountDAO
                    .getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
            if (account == null) {
                logger.error("no account found for userid:" + extTransaction.getUserId());

                throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                        "no account found for userId:" + extTransaction.getUserId());
            }
            logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

            account.setBalance(account.getBalance() + amountPaid);
            account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

            logger.info("new account balance : " + account.getBalance());

            
            accountDAO.updateWithoutSession(account, null);
            
            com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                    amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                    BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                    TransactionRefType.EXTERNAL_TRANSACTION, null,
                    com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
            transactionDAO.create(vedantuTransaction);            
            
            
            
            
            logger.info("Account " + account.toString());
        }
        return extTransaction;

    }

    private Map<String, Object> getPostParams(Map<String, String> httpParams) throws InternalServerErrorException {
        logger.info("payment channel [" + PAYMENT_CHANNEL + "] getPaymentUrl: " + httpParams);
        AxisEncrpytionUtil aesUtil = new AxisEncrpytionUtil(SECURE_SECRET, ENCRYPTION_KEY);
        String encRequest;


        Map<String, String> fields = new HashMap<>();
        for (String fieldName : httpParams.keySet()) {
            String fieldValue = httpParams.get(fieldName);
            if ((fieldValue != null) && (fieldValue.length() > 0)) {
                fields.put(fieldName, fieldValue);
            }
        }

        // no need to send the ema url and submit button to the ema
        fields.remove("SubButL");
        fields.remove("inprocess");

        fields.put("submit", "Continue");
        if (SECURE_SECRET != null && SECURE_SECRET.length() > 0) {
            String secureHash = aesUtil.hashAllFields(fields);
            logger.info("SECURE HASH : " + secureHash);
            fields.put(FIELD_SECURE_HASH, secureHash);

            List<String> fieldNames = new ArrayList<>(fields.keySet());
            Iterator itr = fieldNames.iterator();
            StringBuilder sb = new StringBuilder();
            while (itr.hasNext()) {
                String fieldName = (String) itr.next();
                String fieldValue = fields.get(fieldName);
                if ((fieldValue != null) && (fieldValue.length() > 0)) {
                    sb.append(fieldName);
                    sb.append("=");
                    sb.append(fieldValue);
                    sb.append("::");
                }
            }
            try {
                encRequest = aesUtil.encrypt(sb.toString());
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Encryption failed");
            }
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "SECURE HASH EMPTY");
        }

        Map<String, Object> params = new HashMap<>();
        params.put(FIELD_MERCHANT_ID, MERCHANT_ID);
        params.put(FIELD_ENC_DATA, encRequest);

        return params;
    }
}
