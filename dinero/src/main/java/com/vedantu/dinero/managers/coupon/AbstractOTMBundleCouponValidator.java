package com.vedantu.dinero.managers.coupon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.util.ArrayUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@Service
public class AbstractOTMBundleCouponValidator extends AbstractCouponValidator {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTMBundleCouponValidator.class);

    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");


    @Override
    public List<String> getOTFBundleIds(String purchasingEntityId) {
        return Arrays.asList(purchasingEntityId);
    }

    @Override
    public List<String> getGrades(String bundleId) throws VException {
        OTFBundleInfo bundleInfo = getOTFBundleInfo(bundleId);
        if (bundleInfo != null && bundleInfo.getGrade() != null) {
            return Arrays.asList(bundleInfo.getGrade().toString());
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsGrade() {
        return true;
    }

    @Override
    public List<String> getCategories(String bundleId) throws VException {
        OTFBundleInfo bundleInfo = getOTFBundleInfo(bundleId);
        List<String> categoties = new ArrayList<String>();
        if (bundleInfo.getTarget() != null) {
            categoties.add(bundleInfo.getTarget());
        }

        if (!ArrayUtils.isEmpty(bundleInfo.getSearchTerms())) {
            categoties.addAll(bundleInfo.getSearchTerms().stream()
                    .map(searchTerm -> searchTerm.name())
                    .collect(Collectors.toList()));
        }
        return categoties;
    }

    @Override
    public boolean supportsCategory() {
        return true;
    }

    private OTFBundleInfo getOTFBundleInfo(String purchasingEntityId) throws VException {
        String batchUrl = subscriptionEndPoint
                + "/otfBundle/" + purchasingEntityId;
        OTFBundleInfo info = null;
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(batchUrl, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            info = new Gson().fromJson(jsonString, OTFBundleInfo.class);
        } catch (VException e) {
            logger.warn("OTM Bundle not found " + purchasingEntityId);
            if (e.getErrorCode() == ErrorCode.NOT_FOUND_ERROR
                    || e.getErrorCode() == ErrorCode.BAD_REQUEST_ERROR) {
                throw new BadRequestException(e.getErrorCode(), e.getErrorMessage());
            } else {
                throw e;
            }
        }
        return info;

    }
}
