/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.SnsOrders;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.pojo.TransactionUserInfo;
import com.vedantu.dinero.request.Instalment.SendInstalmentPaidEmailReq;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.scheduling.request.session.MultipleSessionScheduleReq;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.request.BlockSlotScheduleReq;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.scheduling.SchedulingUtils;
import com.vedantu.util.subscription.GetSlotConflictReq;
import com.vedantu.util.subscription.GetSlotConflictRes;
import com.vedantu.util.subscription.UpdateCalendarSessionSlotsReq;
import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class InstalmentAsyncTasksManager {

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstalmentManager.class);

    @Autowired
    public InstalmentManager instalmentManager;
    
        @Autowired
    public AccountDAO accountDAO;
        
    @Autowired
    public  ExtTransactionDAO extTransactionDAO;
    
    @Autowired
    private AwsSQSManager awsSQSManager;
    

    private String platformEndpoint;
    private String subscriptionEndPoint;
    private String schedulingEndPoint;

    private final String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private final String arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");

    public InstalmentAsyncTasksManager() {
        super();
        subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        schedulingEndPoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
        platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    }

    @Async
    public void sendInstalmentPaidEmailForSubscription(List<Instalment> instalments, Long contextId) throws VException {
        logger.info("ENTRY: subscriptionId " + contextId);
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                subscriptionEndPoint + "subscription/getSubscriptionBasicInfo/" + contextId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        SubscriptionResponse subscriptionResponse = new Gson().fromJson(jsonString, SubscriptionResponse.class);
        logger.info("subscription basic info " + subscriptionResponse);

        String url = platformEndpoint + "/dinero/payment/sendInstalmentPaidEmail";
        SendInstalmentPaidEmailReq req = new SendInstalmentPaidEmailReq(contextId, subscriptionResponse.getTitle(),
                subscriptionResponse.getTeacherId(), subscriptionResponse.getStudentId(), instalments);
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowFosException(response);
        String responseStr = response.getEntity(String.class);
        logger.info("EXIT " + responseStr);
    }

    @Async
    public void scheduleSessionsForInstalment(Instalment instalment, Orders order, OrderedItem item,
            SubscriptionResponse subscriptionResponse) throws VException, CloneNotSupportedException {
        logger.info("ENTRY instalmentId" + instalment.getId());
        try {
            SessionSchedule instalmentSchedule = instalment.getSessionSchedule();
            // getting available slots to schedule sessions
            String getSlotConflictsUrl = schedulingEndPoint
                    + ConfigUtils.INSTANCE.getStringValue("vedantu.scheduling.getSlotConflicts");
            GetSlotConflictReq getSlotConflictReq = new GetSlotConflictReq();
            getSlotConflictReq.setStudentId(order.getUserId());
            getSlotConflictReq.setTeacherId(item.getTeacherId());
            long[] slotBitSetToCheckConflicts = SchedulingUtils.createSlotBitSetLongValues(instalmentSchedule);
            instalmentSchedule.setSlotBitSet(slotBitSetToCheckConflicts);
            instalmentSchedule.setConflictSlotBitSet(slotBitSetToCheckConflicts);
            // cloning instalment schedule to send to getslotconflicts request
            SessionSchedule _sessionSchedule = (SessionSchedule) instalmentSchedule.clone();
            Long noOfWeeks = ((instalmentSchedule.getEndDate() - instalmentSchedule.getStartDate())
                    / CommonCalendarUtils.MILLIS_PER_WEEK);
            noOfWeeks++;// buffer
            _sessionSchedule.setNoOfWeeks(noOfWeeks);
            getSlotConflictReq.setSchedule(_sessionSchedule);
            getSlotConflictReq.setReferenceId(item.getDeliverableEntityId());// detailId
            // which
            // is
            // used
            // for
            // blocking
            // instalments
            // calendar

            logger.info("Getting slot conflicts for " + getSlotConflictReq);
            ClientResponse slotsConflictsResp = WebUtils.INSTANCE.doCall(getSlotConflictsUrl, HttpMethod.POST,
                    new Gson().toJson(getSlotConflictReq));
            VExceptionFactory.INSTANCE.parseAndThrowException(slotsConflictsResp);
            GetSlotConflictRes getSlotConflictRes = new Gson().fromJson(slotsConflictsResp.getEntity(String.class),
                    GetSlotConflictRes.class);
            logger.info("Response from getSlotConflictsUrl: " + getSlotConflictRes);

            BitSet slotsBitSet = BitSet.valueOf(getSlotConflictRes.getSlotBits());
            BitSet conflictBitSet = BitSet.valueOf(getSlotConflictRes.getConflictSlotBits());

            logger.info("slotsBitset " + slotsBitSet);
            logger.info("conflictBitSet " + conflictBitSet);

            // excluding the conflicted slots and scheduling only the available
            // slots
            slotsBitSet.andNot(conflictBitSet);

            logger.info("slotsBitset final " + slotsBitSet);

            // TODO check if task is needed or sync ops is need in fos for
            // scheduling sessions
            long[] slotsBitSetToSchedule = slotsBitSet.toLongArray();
            scheduleSessions(instalment, order, item, subscriptionResponse, slotsBitSetToSchedule);

            List<SessionSlot> scheduledSlots = SchedulingUtils.createSessionSlots(slotsBitSetToSchedule,
                    instalmentSchedule.getStartTime());
            Long millisToLock = 0l;
            for (SessionSlot s : scheduledSlots) {
                millisToLock += (s.getEndTime() - s.getStartTime());
            }

            logger.info("updating remaining hrs in subscriptiondetailsId " + item.getDeliverableEntityId()
                    + " millisToLock " + millisToLock);
            // TODO check the sessions scheduled response and lock the hrs
            instalmentManager.addOrUpdateHrsForInstalment(instalment, item, null, millisToLock);

            updateCalendarForBookedSessions(instalment, subscriptionResponse, slotsBitSet.toLongArray(),
                    order.getUserId().toString(), item.getTeacherId().toString());
        } catch (Exception e) {
            logger.error("scheduleSessionsForInstalment Error " + e.getMessage());
        }
        logger.info("EXIT");
    }

    @Async
    public void updateBlockedInstalmentSlots(List<Instalment> instalments, Instalment paidInstalment,
            String subscriptionDetailsId) {
        logger.info("ENTRY: " + paidInstalment + ", subscriptionDetailsId " + subscriptionDetailsId);
        SessionSchedule sessionSchedule = paidInstalment.getSessionSchedule();
        Long endTime = sessionSchedule.getEndDate();
        String updateBlockCalendarUrl = schedulingEndPoint
                + ConfigUtils.INSTANCE.getStringValue("calendar.block.slot.update") + "?referenceId="
                + subscriptionDetailsId + "&endTime=" + endTime;
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(updateBlockCalendarUrl, HttpMethod.POST, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info("Response from updating the calendar block entry: " + jsonString);
        } catch (Exception ex) {
            logger.error("Error in updating blocking instalment slots for subscriptionDetailsId "
                    + subscriptionDetailsId + ", ex " + ex.getMessage());
        }
        markBlockedSlotsProcessed(instalments, subscriptionDetailsId);
        logger.info("EXIT ");
    }

    @Async
    public void blockInstalmentSessionSlots(List<Instalment> instalments, Long teacherId, Long studentId,
            String subscriptionDetailsId) {
        logger.info("ENTRY: " + instalments + ", subscriptionDetailsId " + subscriptionDetailsId);
        if (ArrayUtils.isEmpty(instalments)) {
            return;
        }
        try {
            String blockCalendarUrl = schedulingEndPoint
                    + ConfigUtils.INSTANCE.getStringValue("calendar.block.slot.schedule");
            if (instalments.size() > 1) {
                // blocking only the remaing instalment slots after scheduling
                List<SessionSlot> slots = new ArrayList<>();
                for (int k = 1; k < instalments.size(); k++) {
                    slots.addAll(instalments.get(k).getSessionSchedule().getSessionSlots());
                }
                SessionSchedule sessionSchedule = new SessionSchedule();
                sessionSchedule.setSessionSlots(slots);
                sessionSchedule.setNoOfWeeks(1l);
                sessionSchedule.setStartTime(CommonCalendarUtils.getDayStartTime(sessionSchedule.getStartDate()));
                sessionSchedule.setEndTime(sessionSchedule.getEndDate());
                BlockSlotScheduleReq req = new BlockSlotScheduleReq();
                if (studentId != null) {
                    req.setStudentId(studentId.toString());
                }
                if (teacherId != null) {
                    req.setTeacherId(teacherId.toString());
                }
                req.setReferenceId(subscriptionDetailsId);
                req.setSchedule(sessionSchedule);
                // TODO reduce payload by taking advantage of no of weeks
                ClientResponse resp = WebUtils.INSTANCE.doCall(blockCalendarUrl, HttpMethod.POST,
                        new Gson().toJson(req));
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                logger.info("Response from blocking instalment slots: " + jsonString);
            }
            markBlockedSlotsProcessed(instalments, subscriptionDetailsId);
        } catch (Exception ex) {
            logger.error("Error in blocking instalment slots for subscriptionDetailsId " + subscriptionDetailsId
                    + ", ex " + ex.getMessage());
        }
        logger.info("EXIT");
    }

    @Async
    public void markBlockedSlotsProcessed(List<Instalment> instalments, String subscriptionDetailsId) {
        logger.info("ENTRY: " + instalments + ", subscriptionDetailsId " + subscriptionDetailsId);
        if (ArrayUtils.isEmpty(instalments)) {
            return;
        }
        boolean allPaid = true;
        for (Instalment instalment : instalments) {
            if (!PaymentStatus.PAID.equals(instalment.getPaymentStatus())) {
                allPaid = false;
                break;
            }
        }
        if (allPaid) {
            try {
                String markprocessedUrl = schedulingEndPoint
                        + ConfigUtils.INSTANCE.getStringValue("calendar.block.slot.markprocessed") + "?referenceId="
                        + subscriptionDetailsId;
                ClientResponse resp = WebUtils.INSTANCE.doCall(markprocessedUrl, HttpMethod.POST, null);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                logger.info("Response for block slot markprocessed : " + jsonString);
            } catch (Exception ex) {
                logger.error("Error in marking instalment slots processed for subscriptionDetailsId "
                        + subscriptionDetailsId + ", ex " + ex.getMessage());
            }
        }
    }

    private void scheduleSessions(Instalment instalment, Orders order, OrderedItem item,
            SubscriptionResponse subscriptionResponse, long[] slotsBitSetToSchedule) throws VException {
        logger.info("ENTRY");
        MultipleSessionScheduleReq scheduleReq = new MultipleSessionScheduleReq();
        List<Long> studentIds = new ArrayList<>();
        studentIds.add(order.getUserId());
        scheduleReq.setStudentIds(studentIds);
        scheduleReq.setTeacherId(item.getTeacherId());
        scheduleReq.setSubscriptionId(subscriptionResponse.getSubscriptionId());
        scheduleReq.setSubject(subscriptionResponse.getSubject());
        scheduleReq.setSessionModel(subscriptionResponse.getModel());
        if (instalment.getTriggeredBy() != null) {
            scheduleReq.setCallingUserId(Long.parseLong(instalment.getTriggeredBy()));
        }

        SessionSchedule schedule = instalment.getSessionSchedule();
        List<SessionSlot> sessionSlots = SchedulingUtils.createSessionSlots(slotsBitSetToSchedule, schedule.getStartTime());
        if (ArrayUtils.isNotEmpty(sessionSlots)) {
            scheduleReq.setSlots(sessionSlots);
            scheduleReq.setTitle(subscriptionResponse.getTitle());
            logger.info("scheduling sessions with req : " + scheduleReq);
            ClientResponse fosResp = WebUtils.INSTANCE.doCall(schedulingEndPoint + "/session/createMultiple",
                    HttpMethod.POST, new Gson().toJson(scheduleReq));
            VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
            String resp = fosResp.getEntity(String.class);
            logger.info("EXIT : " + resp);
        }
    }

    private void updateCalendarForBookedSessions(Instalment instalment, SubscriptionResponse subscriptionResponse,
            long[] slotBitSetScheduled, String studentId, String teacherId) throws VException {
        logger.info("ENTRY : ");
        UpdateCalendarSessionSlotsReq req = new UpdateCalendarSessionSlotsReq();
        logger.info(subscriptionResponse.getStudent());
        logger.info(subscriptionResponse.getTeacher());
        req.setStudentId(studentId);
        req.setTeacherId(teacherId);

        SessionSchedule schedule = instalment.getSessionSchedule();
        req.setStartTime(schedule.getStartTime());
        req.setEndTime(schedule.getEndTime());
        req.setScheduledSlots(slotBitSetScheduled);
        logger.info("marking the calendar entries available and booked " + req);
        ClientResponse fosResp = WebUtils.INSTANCE.doCall(
                schedulingEndPoint + "/calendarEntry/updateCalendarSessionSlots", HttpMethod.POST,
                new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
        String resp = fosResp.getEntity(String.class);
        logger.info("EXIT: " + resp);
    }

    @Async
    public void sendInstalmentPaidEmailForOTF(List<Instalment> instalments, String batchId, Long userId)
            throws VException {
        logger.info("ENTRY: batchId " + batchId);

        ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionEndPoint + "/batch/" + batchId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BatchInfo batchInfo = new Gson().fromJson(jsonString, BatchInfo.class);
        logger.info("batch info " + batchInfo);
        String subscriptionLink = "/1-to-few/cbse/course/" + batchInfo.getCourseInfo().getId() + "/batch-details/list?enrolled=true&batchId=" + batchId;

        String url = platformEndpoint + "/dinero/payment/sendInstalmentPaidEmail";
        SendInstalmentPaidEmailReq req = new SendInstalmentPaidEmailReq(null, batchInfo.getCourseInfo().getTitle(),
                null, userId, instalments);
        req.setSubscriptionLink(subscriptionLink);
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowFosException(response);
        String responseStr = response.getEntity(String.class);
        logger.info("EXIT " + responseStr);
    }

    @Async
    public void triggerOrderUpdatedForUser(Orders order) {
        AmazonSNSAsync snsClient = AmazonSNSAsyncClientBuilder.standard()
                .withRegion(Regions.AP_SOUTHEAST_1)
                .build();
        String topic = SnsOrders.TopicSNS.ORDER_UPDATED.name();

        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic + "_" + env.toUpperCase();
        String msg = new Gson().toJson(order);
        String snsSubject = SnsOrders.Subject.ORDER_UPDATED.name();
        PublishRequest publishRequest = new PublishRequest(topicArn, msg, snsSubject);
        PublishResult publishResult = snsClient.publish(publishRequest);
        logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
    }
    
    @Async
    public void updatePurchasingEntitiesInInstalment(String orderId,List<PurchasingEntity> purchasingEntities)
            throws VException {
        
        instalmentManager.updatePurchasingEntitiesInInstalment(orderId, purchasingEntities);
    }
    
    @Async
    public void triggerLeadSquareQueue(SQSQueue queue, SQSMessageType messageType,
                                       String message, String messageGroupId)
             {
        
        awsSQSManager.sendToSQS(queue, messageType, message, messageGroupId);
    }
    
    @Async
    public void triggerLeadSquareQueueForTransaction(Transaction transaction, boolean createdNew)
             {
        //instalmentManager.triggerLeadSquareQueueForTransaction(transaction, createdNew);
        if(transaction != null && BillingReasonType.RECHARGE.name().equals(transaction.getReasonType()) && StringUtils.isNotEmpty(transaction.getCreditToAccount())){
            
            Account account = accountDAO.getById(Long.parseLong(transaction.getCreditToAccount()), null);
            UserBasicInfo userInfo = new UserBasicInfo();
            
            if(account != null && StringUtils.isNotEmpty(account.getHolderId()) && account.getHolderId().contains("user/")){
                String _id = account.getHolderId().replace("user/", "");
                userInfo.setUserId(Long.parseLong(_id));
                TransactionUserInfo transactionInfo = new TransactionUserInfo(transaction,userInfo);
                if (createdNew) {
                    awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.TRANSACTION_NEW_LS, new Gson().toJson(transactionInfo), _id + "_TRANS");
                }else {
                    awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.TRANSACTION_UPDATE_LS,new Gson().toJson(transactionInfo), _id + "_TRANS");
                }
            }else {
                logger.info("transaction accont not found or no user account");
            }
            
        }
            
        
        
    }
    
    @Async
    public void updateLocationValuesInExtTransaction(String transactionId, String city, String state, String country){
        logger.info("Updating ExtTransaction for country, city and state");
        ExtTransaction extTransaction = extTransactionDAO.getById(transactionId);
        if(extTransaction!=null){
            extTransaction.setCity(city);
            extTransaction.setCountry(country);
            extTransaction.setState(state);
            extTransactionDAO.create(extTransaction, null);
            logger.info("Successfully updated");
        }
        
    }
    
}
