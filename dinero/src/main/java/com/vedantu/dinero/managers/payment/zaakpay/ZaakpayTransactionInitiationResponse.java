package com.vedantu.dinero.managers.payment.zaakpay;

public class ZaakpayTransactionInitiationResponse {

	//max 20 alphanumeric, must be unique per website, no duplicate
	private Long orderId;
	private Integer responseCode;
	private String responseDescription;
	//checksum calculated at zaakpay's server end on a string [joining every parameter value] for security purpose
	private Long checksum;
	//Transaction Amount [ in paisa]
	private Float amount;
	//Unique id for each card number used in transaction
	//For Netbanking txns, value will be â€œNAâ€.
	private String paymentMethod;
	//Payment Method ID for Card and Net Banking transactions. For Card txns, payment Method ID starts with C and N for Net Banking. 
	//It is alphanumeric value with max length 6.
	private String cardhashid;
	
	public ZaakpayTransactionInitiationResponse(Long orderId, Integer responseCode,
			String responseDescription, Long checksum, Float amount,
			String paymentMethod, String cardhashid) {
		super();
		this.orderId = orderId;
		this.responseCode = responseCode;
		this.responseDescription = responseDescription;
		this.checksum = checksum;
		this.amount = amount;
		this.paymentMethod = paymentMethod;
		this.cardhashid = cardhashid;
	}
	
	public Long getOrderId() {
		return orderId;
	}
	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}
	public Integer getResponseCode() {
		return responseCode;
	}
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}
	public String getResponseDescription() {
		return responseDescription;
	}
	public void setResponseDescription(String responseDescription) {
		this.responseDescription = responseDescription;
	}
	public Long getChecksum() {
		return checksum;
	}
	public void setChecksum(Long checksum) {
		this.checksum = checksum;
	}
	public Float getAmount() {
		return amount;
	}
	public void setAmount(Float amount) {
		this.amount = amount;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getCardhashid() {
		return cardhashid;
	}
	public void setCardhashid(String cardhashid) {
		this.cardhashid = cardhashid;
	}
}
