package com.vedantu.dinero.managers.payment;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.stream.Collectors;

import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.exception.*;
import org.apache.logging.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.payment.zaakpay.ParamSanitizer;
import com.vedantu.dinero.managers.payment.zaakpay.ZaakpayChecksumCalculator;
import com.vedantu.dinero.managers.payment.zaakpay.ZaakpayTransactionInitiationReq;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;

@Service
public class ZaakpayPaymentManager implements IPaymentManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ZaakpayPaymentManager.class);

	@Autowired
	public FosUtils fosUtils;

	@Autowired
	public ExtTransactionDAO extTransactionDAO;

	@Autowired
	public TransactionDAO transactionDAO;

	@Autowired
	public AccountDAO accountDAO;

	public static final String PAYMENT_CHANNEL = "ZAAKPAY";

	private final PaymentGatewayName gatewayName = PaymentGatewayName.ZAAKPAY;
	private String serverUrl;
	private String secretKey;

	public ZaakpayPaymentManager() {
		super();
		serverUrl = ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.charging.url");
		secretKey = ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.checksum.secretKey");                
	}

	@Override
	public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException {

		ZaakpayTransactionInitiationReq zaakpayTransactionInitiationReq = new ZaakpayTransactionInitiationReq();

		// required params
		zaakpayTransactionInitiationReq.setOrderId(transaction.getId().toString());
		zaakpayTransactionInitiationReq.setCurrency("INR");
		zaakpayTransactionInitiationReq.setAmount(transaction.getAmount().longValue());
		zaakpayTransactionInitiationReq
				.setMerchantIdentifier(ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.merchant_id"));
                UserBasicInfo user = fosUtils.getUserBasicInfo(transaction.getUserId().toString(), true);
		String name = user.getFullName();
		zaakpayTransactionInitiationReq.setReturnUrl(ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.redirect_url"));
		zaakpayTransactionInitiationReq.setBuyerEmail(user.getEmail());
		zaakpayTransactionInitiationReq.setBuyerFirstName(user.getFirstName());
		if (StringUtils.isNotEmpty(user.getLastName())) {
			zaakpayTransactionInitiationReq.setBuyerLastName(user.getLastName());
		} else {
			zaakpayTransactionInitiationReq.setBuyerLastName(".");
		}

		zaakpayTransactionInitiationReq.setBuyerAddress(ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.address.default"));
		zaakpayTransactionInitiationReq.setBuyerCity(ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.city.default"));
		zaakpayTransactionInitiationReq.setBuyerState(ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.state.default"));
		zaakpayTransactionInitiationReq.setBuyerCountry(ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.country.default"));
		zaakpayTransactionInitiationReq.setBuyerPincode(ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.zip.default"));
		zaakpayTransactionInitiationReq.setBuyerPhoneNumber(user.getContactNumber());
		String mode = ConfigUtils.INSTANCE.getStringValue("billing.zaakpay.mode");
		if (!StringUtils.isEmpty(mode)) {
			zaakpayTransactionInitiationReq.setMode(Integer.parseInt(mode));
		}

		zaakpayTransactionInitiationReq.setMerchantIpAddress("127.0.0.1");
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		df.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

		zaakpayTransactionInitiationReq.setProductDescription("AccountRecharge");
		zaakpayTransactionInitiationReq.setTxnDate(df.format(new Date()));

		ObjectMapper objectMapper = new ObjectMapper();

		@SuppressWarnings("unchecked")
		Map<String, Object> httpParams = objectMapper.convertValue(zaakpayTransactionInitiationReq, Map.class);
		try {

			String checksum = ZaakpayChecksumCalculator.calculateChecksum(secretKey, getChecksumString(httpParams));
			if (!StringUtils.isEmpty(checksum)) {
				zaakpayTransactionInitiationReq.setChecksum(checksum);
				httpParams.put("checksum", checksum);
				transaction.setChecksum(checksum);
				extTransactionDAO.create(transaction, callingUserId);
				logger.info("Transaction:" + transaction);
			} else {
				throw new IllegalArgumentException("Checksum generation failed");
			}
		} catch (Exception e) {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Checksum generation failed");
		}
		String url = getPaymentUrl(httpParams);
		RechargeUrlInfo rechargeUrlInfo = new RechargeUrlInfo(PaymentGatewayName.ZAAKPAY, url, "POST", null);
		return rechargeUrlInfo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId)
			throws NotFoundException, InternalServerErrorException, ConflictException, BadRequestException {

		// VERIFY THE RESPONSE FROM SERVER
		verifyRequest(transactionInfo, callingUserId);

		Map<String, Object> paramMap = new LinkedHashMap<String, Object>();
		Object[] checksum =  ((List<String>)transactionInfo.get("checksum")).toArray();
		Object[] orderIds =  ((List<String>) transactionInfo.get("orderId")).toArray();
		paramMap.put("orderId", orderIds[0].toString());

		String orderId = orderIds[0].toString();
		ExtTransaction extTransaction = null;
		logger.info("checksum: " + checksum.toString());

		Object[] responseCodesArray =  ((List<String>) transactionInfo.get("responseCode")).toArray();
		paramMap.put("responseCode", responseCodesArray[0].toString());

		Object[] responseDescriptionArray =  ((List<String>)transactionInfo.get("responseDescription")).toArray();
		paramMap.put("responseDescription", responseDescriptionArray[0].toString());

		Object[] amount =  ((List<String>) transactionInfo.get("amount")).toArray();
		paramMap.put("amount", amount[0].toString());

		Object[] paymentMethod = ((List<String>)transactionInfo.get("paymentMethod")).toArray();
		paramMap.put("paymentMethod", paymentMethod[0].toString());

		Object[] cardhashid = ((List<String>)transactionInfo.get("cardhashid")).toArray();
		paramMap.put("cardhashid", cardhashid[0].toString());

		if (orderId != null) {
			extTransaction = extTransactionDAO.getById(orderId);
			boolean checksumVerified;
			try {
				checksumVerified = ZaakpayChecksumCalculator.verifyChecksum(secretKey, getChecksumString(paramMap),
						checksum[0].toString());
			} catch (Exception e) {
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
			}
			if (StringUtils.isEmpty(checksum[0].toString()) || extTransaction == null || !checksumVerified || amount[0].toString() == null) {
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
			}
		} else {
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "invalid response");
		}

		TransactionStatus transactionStatus = null;
		List<Object> responseCodes = Arrays.asList(responseCodesArray);
		List<Object> responseDescription = Arrays.asList(responseDescriptionArray);

		if (responseCodes.contains("100")) {
			transactionStatus = TransactionStatus.SUCCESS;
		} else if (responseCodes.contains("102") || responseCodes.contains("610") || responseCodes.contains("611")
				|| responseCodes.contains("612")) {
			transactionStatus = TransactionStatus.CANCELLED;
		} else {
			transactionStatus = TransactionStatus.FAILED;
		}

		String paymentInstrument = cardhashid[0].toString();

		int amountPaid = (int) (Float.parseFloat(amount[0].toString()));

		if (transactionStatus != TransactionStatus.SUCCESS) {
			amountPaid = 0;
		}

		// Map<String, Object> merchantParamMap = StringUtils.toKeyValueMap(
		// (String) resParamMap.get("merchant_param1"), "#",
		// MERCHANT_PARAM_VALUE_SAPERATOR);
		//
		// logger.info("merchantParamMap : " + merchantParamMap);
		logger.info("transaction status : " + transactionStatus);

		logger.info("starting transaction ");
		logger.info("extTransaction before updation : " + extTransaction);
		// ExtTransactionDAO.INSTANCE
		// .getById(transactionId);

		if (extTransaction.getStatus() != TransactionStatus.PENDING) {

			throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
					"transaction with id " + orderId + " is already processed");
		}
		extTransaction.setStatus(transactionStatus);
		extTransaction.setGatewayStatus(responseCodes.stream().map(Object::toString).collect(Collectors.joining(",")));
		extTransaction.setTransactionTime(String.valueOf(System.currentTimeMillis()));

		if (extTransaction.getAmount().intValue() == amountPaid && transactionStatus == TransactionStatus.SUCCESS) {
			// TODO: these functionality can be moved to abstract class

			extTransaction.setPaymentChannelTransactionId(orderId.toString());
			extTransaction.setPaymentInstrument(paymentInstrument);
			extTransaction.setPaymentMethod(paymentMethod[0].toString());
			extTransaction.setBankRefNo(cardhashid[0].toString());
			logger.info("saving extTransaction: " + extTransaction);
			extTransactionDAO.create(extTransaction, callingUserId);

			// transaction = TransactionDAO.INSTANCE
			// .upsertTransaction(transaction);

			Account account = null;
			if (transactionStatus == TransactionStatus.SUCCESS) {
				// TODO: take this code to common place, where update and
				// deduct
				// operation can be done
				logger.info("transaction was successful:" + extTransaction);
				account = accountDAO
						.getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
				if (account == null) {
					logger.error("no account found for userid:" + extTransaction.getUserId());

					throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
							"no account found for userId:" + extTransaction.getUserId());
				}
				logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

				account.setBalance(account.getBalance() + amountPaid);
				account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);


				logger.info("new account balance : " + account.getBalance());

                                accountDAO.updateWithoutSession(account, null);
                                
				com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
						amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
						BillingReasonType.RECHARGE.name(), extTransaction.getId().toString(),
						TransactionRefType.EXTERNAL_TRANSACTION, null, 
						com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
				transactionDAO.create(vedantuTransaction);

			}
		} else {
			extTransactionDAO.create(extTransaction, callingUserId);
		}

		return extTransaction;
	}

	private String getPaymentUrl(Map<String, Object> httpParams) {
		logger.info("payment channel [" + PAYMENT_CHANNEL + "] getPaymentUrl: " + httpParams);
		StringBuilder sb = new StringBuilder();
		sb.append(serverUrl);
		sb.append("?v=3&");
		// sb.append("&");
		// sb.append(FIELD_MERCHANT_ID);
		// sb.append("=");
		// sb.append(MERCHANT_ID);

		StringBuilder encReqBuilder = new StringBuilder();
		for (Entry<String, Object> entry : httpParams.entrySet()) {
			if (entry.getValue() != null) {
				encReqBuilder.append(entry.getKey());
				encReqBuilder.append("=");
				encReqBuilder.append(entry.getValue());
				encReqBuilder.append("&");
			}
		}
		encReqBuilder.deleteCharAt(encReqBuilder.length() - 1);
		// sb.append("&");
		// sb.append("encRequest");
		// sb.append("=");
		// AesCryptUtil aesUtil = new AesCryptUtil(enc_key);
		String encRequest = encReqBuilder.toString();
		sb.append(encRequest);
		// sb.append("&");
		// sb.append(FIELD_ACCESS_CODE);
		// sb.append("=");
		// sb.append(access_code);

		String url = sb.toString();
		logger.info("payment channel [" + PAYMENT_CHANNEL + "] payment url :  " + url);
		return url;
	}

	public String getChecksumString(Map<String, Object> paramMap) {
		StringBuilder checksumString = new StringBuilder();
		for (Entry<String, Object> entry : paramMap.entrySet()) {
			if (entry.getValue() != null) {
				if (entry.getKey().equals("checksum")) {
					continue;
				}
				checksumString.append("'");
				String paramValue = "";
				if (entry.getKey().equals("returnUrl")) {
					paramValue = ParamSanitizer.SanitizeURLParam(entry.getValue().toString());
				} else {
					paramValue = ParamSanitizer.sanitizeParam(entry.getValue().toString());
				}
				checksumString.append(paramValue);
				checksumString.append("'");
			}
		}
		return checksumString.toString();
	}

	@Override
	public PaymentGatewayName getName() {
		return this.gatewayName;
	}

}
