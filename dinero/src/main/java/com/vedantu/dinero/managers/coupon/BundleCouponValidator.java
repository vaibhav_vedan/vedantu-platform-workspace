/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.dinero.entity.BaseSubscriptionPaymentPlan;
import com.vedantu.dinero.enums.BaseSubscriptionDuration;
import com.vedantu.dinero.serializers.BaseSubscriptionPaymentPlanDAO;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 *
 * @author ajith
 */
@Service
public class BundleCouponValidator extends AbstractCouponValidator {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BundleCouponValidator.class);

    @Autowired
    BaseSubscriptionPaymentPlanDAO baseSubscriptionPaymentPlanDAO;
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    public BundleCouponValidator() {
        super();
    }

    @Override
    public List<String> getGrades(String bundleId) {
        BundleInfo bundleInfo = getBundleInfo(bundleId);
        List<String> grades = new ArrayList<>();
        if (bundleInfo != null && bundleInfo.getGrade() != null) {
            bundleInfo.getGrade().forEach( grade -> {
                grades.add(grade.toString());
            });
            return grades;
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsGrade() {
        return true;
    }

    @Override
    public List<String> getCategories(String bundleId) {
        List<String> categoties = null;
        BundleInfo bundleInfo = getBundleInfo( bundleId );
        if (bundleInfo != null) {
            categoties = new ArrayList<String>();
            if (!ArrayUtils.isEmpty( bundleInfo.getTarget() )) {
                categoties.addAll( bundleInfo.getTarget() );
            }

            if (!ArrayUtils.isEmpty( bundleInfo.getSearchTerms() )) {
                categoties.addAll( bundleInfo.getSearchTerms().stream()
                        .map( searchTerm -> searchTerm.name() )
                        .collect( Collectors.toList() ) );
            }
        }
        return categoties;
    }

    @Override
    public boolean supportsCategory() {
        return true;
    }

    @Override
    public List<String> getSubjects(String bundleId) {
        BundleInfo bundleInfo = getBundleInfo(bundleId);
        if (bundleInfo != null) {
            bundleInfo.getSubject().replaceAll(String::toLowerCase);
            return bundleInfo.getSubject();
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsSubject() {
        return true;
    }

    @Override
    public List<String> getBundleIds(String purchasingEntityId) {
        return Arrays.asList(purchasingEntityId);
    }

    @Override
    public boolean supportsBundle() {
        return true;
    }

//    @Override
//    public List<String> getTeacherIds(String id) {
//        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanInfo(id);
//        if (coursePlanBasicInfo != null) {
//            return Arrays.asList(coursePlanBasicInfo.getTeacherId().toString());
//        } else {
//            return null;
//        }
//    }
//
//    @Override
//    public boolean supportsTeacher() {
//        return true;
//    }

    public BundleInfo getBundleInfo(String id) {
        BundleInfo bundleInfo = null;
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                    + "/bundle/" + id, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);
        } catch (Exception e) {
            logger.error("Error in getBundleInfo " + id);
        }
        return bundleInfo;
    }

    @Override
    public List<String> getValidMonths(String planId) {
        BaseSubscriptionPaymentPlan plan = baseSubscriptionPaymentPlanDAO.getById(planId);

        if (plan != null && plan.getValidMonths() != null) {
            if(plan.getPlanDuration() != null && plan.getPlanDuration().equals(BaseSubscriptionDuration.FULLCOURSE)){
                return Arrays.asList(plan.getPlanDuration().getName());
            }else {
                return Arrays.asList(plan.getValidMonths() + "");
            }
        } else {
            return null;
        }
    }


}
