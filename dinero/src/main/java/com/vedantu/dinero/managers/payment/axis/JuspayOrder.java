package com.vedantu.dinero.managers.payment.axis;

import com.google.gson.JsonObject;
import in.juspay.exception.*;
import in.juspay.model.*;

import java.util.List;
import java.util.Map;

public class JuspayOrder extends JuspayCustomEntity {

    private String id;
    private String orderId;
    private String merchantId;
    private String txnId;
    private Double amount;
    private String currency;
    private String customerId;
    private String customerEmail;
    private String customerPhone;
    private String description;
    private String productId;
    private Long gatewayId;
    private String returnUrl;
    private String udf1;
    private String udf2;
    private String udf3;
    private String udf4;
    private String udf5;
    private String udf6;
    private String udf7;
    private String udf8;
    private String udf9;
    private String udf10;
    private String status;
    private Long statusId;
    private Boolean refunded;
    private Double amountRefunded;
    private String bankErrorCode;
    private String bankErrorMessage;
    private String paymentMethod;
    private String paymentMethodType;
    private String payerVpa;
    private String payerAppName;
    private String gatewayReferenceId;
    private String authType;
    private String gatewayPayload;
    private Card card;
    private PaymentGatewayResponse paymentGatewayResponse;
    private PaymentLinks paymentLinks;
    private TxnDetail txnDetail;
    private TxnOfferInfo offer;
    private List<Refund> refunds;
    private List<Chargeback> chargebacks;
    private Mandate mandate;
    private JuspayOptions juspay;


    public static JuspayOrder create(Map<String, Object> params, JuspayRequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/orders", params, RequestMethod.POST, requestOptions);
        response = addInputParamsToResponse(params, response);
        return createEntityFromResponse(response, JuspayOrder.class);
    }

    public static JuspayOrder status(String orderId, JuspayRequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (orderId == null || orderId.equals("")) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/orders/" + orderId, null, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, JuspayOrder.class);
    }

    public static JuspayOrder update(String orderId, Map<String, Object> params, JuspayRequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (orderId == null || orderId.equals("") || params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/orders/" + orderId, params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, JuspayOrder.class);
    }

    @Deprecated
    public static OrderList list(Map<String, Object> params, JuspayRequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        JsonObject response = makeServiceCall("/orders", params, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, OrderList.class);
    }

    public static TxnDetailList listTxns(String orderId, Map<String, Object> params, JuspayRequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        JsonObject response = makeServiceCall("/orders/" + orderId + "/txns", params, RequestMethod.GET, requestOptions);
        return createEntityFromResponse(response, TxnDetailList.class);
    }

    public static JuspayOrder refund(String orderId, Map<String, Object> params, JuspayRequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        if (orderId == null || orderId.equals("") || params == null || params.size() == 0) {
            throw new InvalidRequestException();
        }
        JsonObject response = makeServiceCall("/orders/" + orderId + "/refunds", params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, JuspayOrder.class);
    }

    public static Refund refund(Map<String, Object> params, JuspayRequestOptions requestOptions)
            throws APIException, APIConnectionException, AuthorizationException, AuthenticationException, InvalidRequestException {
        JsonObject response = makeServiceCall("/refunds", params, RequestMethod.POST, requestOptions);
        return createEntityFromResponse(response, Refund.class);
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getCustomerPhone() {
        return customerPhone;
    }

    public void setCustomerPhone(String customerPhone) {
        this.customerPhone = customerPhone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public Long getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(Long gatewayId) {
        this.gatewayId = gatewayId;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public String getUdf2() {
        return udf2;
    }

    public void setUdf2(String udf2) {
        this.udf2 = udf2;
    }

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getUdf4() {
        return udf4;
    }

    public void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public String getUdf6() {
        return udf6;
    }

    public void setUdf6(String udf6) {
        this.udf6 = udf6;
    }

    public String getUdf7() {
        return udf7;
    }

    public void setUdf7(String udf7) {
        this.udf7 = udf7;
    }

    public String getUdf8() {
        return udf8;
    }

    public void setUdf8(String udf8) {
        this.udf8 = udf8;
    }

    public String getUdf9() {
        return udf9;
    }

    public void setUdf9(String udf9) {
        this.udf9 = udf9;
    }

    public String getUdf10() {
        return udf10;
    }

    public void setUdf10(String udf10) {
        this.udf10 = udf10;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public void setMerchantId(String merchantId) {
        this.merchantId = merchantId;
    }

    public Boolean getRefunded() {
        return refunded;
    }

    public void setRefunded(Boolean refunded) {
        this.refunded = refunded;
    }

    public Double getAmountRefunded() {
        return amountRefunded;
    }

    public void setAmountRefunded(Double amountRefunded) {
        this.amountRefunded = amountRefunded;
    }

    public String getTxnId() {
        return txnId;
    }

    public void setTxnId(String txnId) {
        this.txnId = txnId;
    }

    public String getBankErrorCode() {
        return bankErrorCode;
    }

    public void setBankErrorCode(String bankErrorCode) {
        this.bankErrorCode = bankErrorCode;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getBankErrorMessage() {
        return bankErrorMessage;
    }

    public void setBankErrorMessage(String bankErrorMessage) {
        this.bankErrorMessage = bankErrorMessage;
    }

    public String getGatewayReferenceId() {
        return gatewayReferenceId;
    }

    public void setGatewayReferenceId(String gatewayReferenceId) {
        this.gatewayReferenceId = gatewayReferenceId;
    }

    public String getAuthType() {
        return authType;
    }

    public void setAuthType(String authType) {
        this.authType = authType;
    }

    public String getGatewayPayload() {
        return gatewayPayload;
    }

    public void setGatewayPayload(String gatewayPayload) {
        this.gatewayPayload = gatewayPayload;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }

    public PaymentGatewayResponse getPaymentGatewayResponse() {
        return paymentGatewayResponse;
    }

    public void setPaymentGatewayResponse(PaymentGatewayResponse paymentGatewayResponse) {
        this.paymentGatewayResponse = paymentGatewayResponse;
    }

    public List<Refund> getRefunds() {
        return refunds;
    }

    public void setRefunds(List<Refund> refunds) {
        this.refunds = refunds;
    }

    public PaymentLinks getPaymentLinks() {
        return paymentLinks;
    }

    public void setPaymentLinks(PaymentLinks paymentLinks) {
        this.paymentLinks = paymentLinks;
    }

    public TxnDetail getTxnDetail() {
        return txnDetail;
    }

    public void setTxnDetail(TxnDetail txnDetail) {
        this.txnDetail = txnDetail;
    }

    public TxnOfferInfo getOffer() {
        return offer;
    }

    public void setOffer(TxnOfferInfo offer) {
        this.offer = offer;
    }

    public List<Chargeback> getChargebacks() {
        return chargebacks;
    }

    public void setChargebacks(List<Chargeback> chargebacks) {
        this.chargebacks = chargebacks;
    }

    public Mandate getMandate() {
        return mandate;
    }

    public void setMandate(Mandate mandate) {
        this.mandate = mandate;
    }

    public JuspayOptions getJuspayOptions() {
        return juspay;
    }

    public void setJuspayOptions(JuspayOptions juspayOptions) {
        this.juspay = juspayOptions;
    }


    public String getPayerVpa() {
        return payerVpa;
    }

    public void setPayerVpa(String payerVpa) {
        this.payerVpa = payerVpa;
    }

    public String getPayerAppName() {
        return payerAppName;
    }

    public void setPayerAppName(String payerAppName) {
        this.payerAppName = payerAppName;
    }
}