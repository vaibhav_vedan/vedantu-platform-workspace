/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class CouponValidatorFactory {

    @Autowired
    private PlanCouponValidator planCouponValidator;

    @Autowired
    private BatchCouponValidator batchCouponValidator;

    @Autowired
    private BatchRegCouponValidator batchRegCouponValidator;
    
    @Autowired
    private CourseRegCouponValidator courseRegCouponValidator;
    
    @Autowired
    private CoursePlanCouponValidator coursePlanCouponValidator;

    @Autowired
    private CoursePlanRegCouponValidator coursePlanRegCouponValidator;
    
    @Autowired
    private BundleCouponValidator bundleCouponValidator;

    @Autowired
    private BundlePackageCouponValidator bundlePackageCouponValidator;
    
    @Autowired
    private OTMBundleCouponValidator otmbundleCouponValidator;
    
    
    @Autowired
    private OTMBundleRegCouponValidator otmbundleRegCouponValidator;    



    public CouponValidatorFactory() {
        super();
    }

    public ICouponValidator getPlanCouponValidator() {
        return planCouponValidator;
    }

    public ICouponValidator getBatchCouponValidator() {
        return batchCouponValidator;
    }

    public ICouponValidator getBatchRegCouponValidator() {
        return batchRegCouponValidator;
    }
    
    public ICouponValidator getCourseRegCouponValidator() {
        return courseRegCouponValidator;
    }    
    
    public ICouponValidator getCoursePlanCouponValidator() {
        return coursePlanCouponValidator;
    }

    public ICouponValidator getCoursePlanRegCouponValidator() {
        return coursePlanRegCouponValidator;
    }
    
    public ICouponValidator getBundleCouponValidator() {
        return bundleCouponValidator;
    }

    public ICouponValidator getBundlePackageCouponValidator() {
        return bundlePackageCouponValidator;
    }
    
    public ICouponValidator getOTMBundleCouponValidator() {
        return otmbundleCouponValidator;
    }
    
    public ICouponValidator getOTMBundleRegCouponValidator() {
        return otmbundleRegCouponValidator;
    }
    
    
    
}
