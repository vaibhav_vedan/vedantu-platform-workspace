/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.pojo.GatewayReport;
import com.vedantu.dinero.pojo.TransactionUserInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.OrdersDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.dao.SessionPayoutDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.dinero.sql.entity.SessionPayout;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.util.*;
import com.vedantu.util.functional.TriFunction;
import lombok.val;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.ObjLongConsumer;
import java.util.stream.Collectors;

/**
 *
 * @author somil
 */
@Service
public class ReportManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    public AccountDAO accountDAO;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    public OrdersDAO ordersDAO;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public SessionPayoutDAO sessionPayoutDAO;

    @Autowired
    public FosUtils fosUtils;

    @Autowired
    public CommunicationManager communicationManager;

    private static String vedantuCutHolderId;
    private static String vedantuTechHolderId;
    private static String vedantuDefaultHolderId;
    private static String vedantuFreebiesHolderId;
    private static String vedantuAIRHolderId;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ReportManager.class);

    private static final Gson GSON = new Gson();
    private static final Gson GSON_PRETTY = new GsonBuilder().setPrettyPrinting().create();

    public ReportManager() {
        vedantuCutHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuCutHolderId");
        vedantuTechHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuTechHolderId");
        vedantuDefaultHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuDefaultHolderId");
        vedantuAIRHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuAIRHolderId");
        vedantuFreebiesHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuFreebiesHolderId");
    }

    public List<Transaction> getTransactionsToVedantuWallets(Long fromTime, Long toTime) throws InternalServerErrorException {

        logger.info("Entering");
        List<String> accountHolderIds = new ArrayList<>();
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuTechHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuFreebiesHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuAIRHolderId));

        List<Account> accounts = accountDAO.getAccountListByHolderIds(accountHolderIds);

        List<String> accountIds = new ArrayList<>();
        for (Account account : accounts) {
            accountIds.add(account.getId().toString());
        }

        if (fromTime == null || toTime == null) {
            toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
        }

        List<Transaction> transactions = transactionDAO.getTransactionsByCreditToAccounts(accountIds, null, fromTime, toTime);

        logger.info("Exiting " + transactions);
        return transactions;
    }

    public List<TransactionUserInfo> getTransactionsFromVedantuWalletsList(Long fromTime, Long toTime) throws InternalServerErrorException {

        logger.info("Entering");
        List<TransactionUserInfo> response = new ArrayList<>();
        List<String> accountHolderIds = new ArrayList<>();
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuTechHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuFreebiesHolderId));
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuAIRHolderId));

        List<Account> accounts = accountDAO.getAccountListByHolderIds(accountHolderIds);

        List<String> accountIds = new ArrayList<>();
        for (Account account : accounts) {
            accountIds.add(account.getId().toString());
        }

        if (fromTime == null || toTime == null) {
            toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
        }

        List<Transaction> transactions = transactionDAO.getTransactionsByDebitFromAccounts(accountIds, null, fromTime, toTime);
        List<Long> creditToAccountIds = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getCreditToAccount() != null) {
                creditToAccountIds.add(Long.parseLong(transaction.getCreditToAccount()));
            }
            response.add(new TransactionUserInfo(transaction, null));
        }

        List<Account> creditToAccounts = accountDAO.getByIds(creditToAccountIds);

        Map<String, String> accountUserIdMap = new HashMap<>();

        for (Account account : creditToAccounts) {
            String userId;
            if (account != null) {
                userId = getUserIdFromHolderId(account.getHolderId());
                if (userId != null) {
                    accountUserIdMap.put(account.getId().toString(), userId);
                }
            }
        }

        Map<String, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMap(new ArrayList<>(accountUserIdMap.values()), true);

        for (TransactionUserInfo transactionInfo : response) {
            String userId = null;
            if (transactionInfo.getTransaction().getCreditToAccount() != null) {
                userId = accountUserIdMap.get(transactionInfo.getTransaction().getCreditToAccount());
            }
            if (userId != null) {
                transactionInfo.setUserBasicInfo(userInfoMap.get(userId));
            }
        }

        logger.info("Exiting " + response);
        return response;
    }

    public List<TransactionUserInfo> getExternalTransactionsDetailsList(Long fromTime, Long toTime) throws InternalServerErrorException {

        logger.info("Entering");
        List<TransactionUserInfo> response = new ArrayList<>();

        if (fromTime == null || toTime == null) {
            toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
        }

        List<Transaction> externalTransactions = transactionDAO.getTransactionsByRefType(TransactionRefType.EXTERNAL_TRANSACTION, fromTime, toTime);
        List<Transaction> cashTransactions = transactionDAO.getTransactionsByRefType(TransactionRefType.CASH_CHEQUE, fromTime, toTime);
        List<Transaction> transactions = new ArrayList<>();
        transactions.addAll(externalTransactions);
        transactions.addAll(cashTransactions);
        logger.info("Size of transactions " + transactions.size());
        List<Long> creditToAccountIds = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getCreditToAccount() != null) {
                creditToAccountIds.add(Long.parseLong(transaction.getCreditToAccount()));
            }
            response.add(new TransactionUserInfo(transaction, null));
        }

        List<Account> creditToAccounts = accountDAO.getByIds(creditToAccountIds);

        Map<String, String> accountUserIdMap = new HashMap<>();

        for (Account account : creditToAccounts) {
            String userId;
            if (account != null) {
                userId = getUserIdFromHolderId(account.getHolderId());
                if (userId != null) {
                    accountUserIdMap.put(account.getId().toString(), userId);
                }
            }
        }

        Map<String, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMap(new ArrayList<>(accountUserIdMap.values()), true);

        List<String> extTransactionIds = new ArrayList<>();
        for (Transaction transaction : externalTransactions) {
            if (transaction.getReasonRefNo() != null) {
                extTransactionIds.add(transaction.getReasonRefNo());
            }
        }

        Map<String, ExtTransaction> extTransactionMap = new HashMap<>();
        List<ExtTransaction> extTransactions = extTransactionDAO.getByIds(extTransactionIds);
        if (extTransactions != null) {
            logger.info("Size of extTransactions " + extTransactions.size());
            for (ExtTransaction extTransaction : extTransactions) {
                extTransactionMap.put(extTransaction.getId(), extTransaction);
            }
        }

        for (TransactionUserInfo transactionInfo : response) {
            String userId = null;
            if (transactionInfo.getTransaction().getCreditToAccount() != null) {
                userId = accountUserIdMap.get(transactionInfo.getTransaction().getCreditToAccount());
            }
            if (userId != null) {
                transactionInfo.setUserBasicInfo(userInfoMap.get(userId));
            }

            if (transactionInfo.getTransaction().getReasonRefNo() != null) {
                transactionInfo.setExtTransaction(extTransactionMap.get(transactionInfo.getTransaction().getReasonRefNo()));
            }
        }

        logger.info("Exiting " + response);
        return response;
    }

    public List<TransactionUserInfo> getSubscriptionTransactionsDetailsList(Long fromTime, Long toTime) throws InternalServerErrorException {

        logger.info("Entering");
        List<TransactionUserInfo> response = new ArrayList<>();

        if (fromTime == null || toTime == null) {
            toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
        }

        List<Orders> orders = ordersDAO.getOrdersByStatus(PaymentStatus.PAID, fromTime, toTime);
        List<String> userIds = new ArrayList<>();
        for (Orders order : orders) {
            if (order.getUserId() != null) {
                userIds.add(order.getUserId().toString());
            }
            TransactionUserInfo info = new TransactionUserInfo();
            info.setOrders(order);
            response.add(info);
        }

        Map<String, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMap(userIds, true);

        for (TransactionUserInfo transactionInfo : response) {
            if (transactionInfo.getOrders().getUserId() != null) {
                transactionInfo.setUserBasicInfo(userInfoMap.get(transactionInfo.getOrders().getUserId().toString()));
            }
        }

        logger.info("Exiting " + response);
        return response;
    }

    public List<TransactionUserInfo> getTeacherPayoutTransactionsDetailsList(Long fromTime, Long toTime) throws InternalServerErrorException {

        logger.info("Entering");
        List<TransactionUserInfo> response = new ArrayList<>();
        List<String> accountHolderIds = new ArrayList<>();
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuDefaultHolderId));

        List<Account> accounts = accountDAO.getAccountListByHolderIds(accountHolderIds);

        List<String> accountIds = new ArrayList<>();
        accountIds.add(accounts.get(0).getId().toString());

        if (fromTime == null || toTime == null) {
            toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
        }

        List<Transaction> transactions = transactionDAO.getTransactionsByDebitFromAccounts(accountIds, TransactionRefType.SESSION_PAYOUT, fromTime, toTime);
        logger.info("Size of transactions " + transactions.size());
        List<Long> sessionIds = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getReasonRefNo() != null) {
                sessionIds.add(Long.parseLong(transaction.getReasonRefNo()));
            }
            response.add(new TransactionUserInfo(transaction, null));
        }

        List<SessionPayout> sessionPayouts = sessionPayoutDAO.getSessionPayouts(sessionIds);
        Map<String, SessionPayout> sessionPayoutMap = new HashMap<>();
        if (sessionPayouts != null) {
            for (SessionPayout sessionPayout : sessionPayouts) {
                if (sessionPayout.getSessionId() != null) {
                    sessionPayoutMap.put(sessionPayout.getSessionId().toString(), sessionPayout);
                }
            }
        }

        Set<Long> studentIds = new HashSet<>();
        Set<Long> teacherIds = new HashSet<>();
        for (SessionPayout sessionPayout : sessionPayoutMap.values()) {
            if (sessionPayout != null) {
                studentIds.add(sessionPayout.getStudentId());
                teacherIds.add(sessionPayout.getTeacherId());
            }
        }

        Map<Long, UserBasicInfo> studentUserInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(studentIds, true);
        Map<Long, UserBasicInfo> teacherUserInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(teacherIds, true);

        for (TransactionUserInfo transactionInfo : response) {
            if (transactionInfo.getTransaction().getReasonRefNo() != null) {
                SessionPayout sessionPayout = sessionPayoutMap.get(transactionInfo.getTransaction().getReasonRefNo());
                if (sessionPayout != null) {
                    transactionInfo.setSessionPayout(sessionPayout);
                    if (sessionPayout.getStudentId() != null) {
                        transactionInfo.setUserBasicInfo(studentUserInfoMap.get(sessionPayout.getStudentId()));
                    }
                    if (sessionPayout.getTeacherId() != null) {
                        transactionInfo.setTeacherBasicInfo(teacherUserInfoMap.get(sessionPayout.getTeacherId()));
                    }
                }
            }
        }

        logger.info("Exiting " + response);
        return response;
    }

    public List<TransactionUserInfo> getVedantuCutTransactionsDetailsList(Long fromTime, Long toTime) throws InternalServerErrorException {

        logger.info("Entering");
        List<TransactionUserInfo> response = new ArrayList<>();
        List<String> accountHolderIds = new ArrayList<>();
        accountHolderIds.add(Account.__getVedantuAccountHolderId(vedantuCutHolderId));

        List<Account> accounts = accountDAO.getAccountListByHolderIds(accountHolderIds);

        List<String> accountIds = new ArrayList<>();
        accountIds.add(accounts.get(0).getId().toString());

        if (fromTime == null || toTime == null) {
            toTime = System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR;
            fromTime = toTime - DateTimeUtils.MILLIS_PER_DAY;
        }

        List<Transaction> transactions = transactionDAO.getTransactionsByCreditToAccounts(accountIds, TransactionRefType.SESSION_PAYOUT, fromTime, toTime);
        logger.info("Size of transactions " + transactions.size());
        List<Long> sessionIds = new ArrayList<>();
        for (Transaction transaction : transactions) {
            if (transaction.getReasonRefNo() != null) {
                sessionIds.add(Long.parseLong(transaction.getReasonRefNo()));
            }
            response.add(new TransactionUserInfo(transaction, null));
        }

        List<SessionPayout> sessionPayouts = sessionPayoutDAO.getSessionPayouts(sessionIds);
        Map<String, SessionPayout> sessionPayoutMap = new HashMap<>();
        if (sessionPayouts != null) {
            for (SessionPayout sessionPayout : sessionPayouts) {
                if (sessionPayout.getSessionId() != null) {
                    sessionPayoutMap.put(sessionPayout.getSessionId().toString(), sessionPayout);
                }
            }
        }

        Map<String, UserBasicInfo> studentUserInfoMap = new HashMap<String, UserBasicInfo>();
        Map<String, UserBasicInfo> teacherUserInfoMap = new HashMap<String, UserBasicInfo>();

        for (SessionPayout sessionPayout : sessionPayoutMap.values()) {
            Long studentUserId = null;
            Long teacherUserId = null;
            if (sessionPayout != null) {
                studentUserId = sessionPayout.getStudentId();
                teacherUserId = sessionPayout.getTeacherId();
                if (studentUserId != null) {
                    studentUserInfoMap.put(studentUserId.toString(), null);
                }
                if (teacherUserId != null) {
                    teacherUserInfoMap.put(teacherUserId.toString(), null);
                }
            }
        }

        List<UserBasicInfo> studentBasicInfos = fosUtils.getUserBasicInfos(new ArrayList<>(studentUserInfoMap.keySet()), true);

        if (studentBasicInfos != null) {
            for (UserBasicInfo studentBasicInfo : studentBasicInfos) {
                if (studentBasicInfo.getUserId() != null) {
                    studentUserInfoMap.put(studentBasicInfo.getUserId().toString(), studentBasicInfo);
                }
            }
        }

        List<UserBasicInfo> teacherBasicInfos = fosUtils.getUserBasicInfos(new ArrayList<>(teacherUserInfoMap.keySet()), true);

        if (teacherBasicInfos != null) {
            for (UserBasicInfo teacherBasicInfo : teacherBasicInfos) {
                if (teacherBasicInfo.getUserId() != null) {
                    teacherUserInfoMap.put(teacherBasicInfo.getUserId().toString(), teacherBasicInfo);
                }
            }
        }

        for (TransactionUserInfo transactionInfo : response) {
            if (transactionInfo.getTransaction().getReasonRefNo() != null) {
                SessionPayout sessionPayout = sessionPayoutMap.get(transactionInfo.getTransaction().getReasonRefNo());
                if (sessionPayout != null) {
                    transactionInfo.setSessionPayout(sessionPayout);
                    if (sessionPayout.getStudentId() != null) {
                        transactionInfo.setUserBasicInfo(studentUserInfoMap.get(sessionPayout.getStudentId().toString()));
                    }
                    if (sessionPayout.getTeacherId() != null) {
                        transactionInfo.setTeacherBasicInfo(teacherUserInfoMap.get(sessionPayout.getTeacherId().toString()));
                    }
                }
            }
        }

        logger.info("Exiting " + response);
        return response;
    }

    public String getUserIdFromHolderId(String holderId) {
        if (holderId == null) {
            return null;
        }
        String[] holderIdSplit = holderId.split("/");
        if (holderIdSplit.length == 2 && "user".equals(holderIdSplit[0])) {
            return holderIdSplit[1];
        } else {
            return null;
        }
    }

    public void getExtTransactionStatusAlert() throws AddressException, VException {
        int calculateFor = 7;
        Instant toTime = Instant.now().truncatedTo(ChronoUnit.HOURS);
        long currentMillis = toTime.toEpochMilli();
        Instant fromTime = toTime.minus(calculateFor, ChronoUnit.DAYS);
        int start = 0, limit = 200;

        Map<String, GatewayReport> paymentMethodAlertMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);
        Map<String, GatewayReport> paymentInstruAlertMap = new TreeMap<>(String.CASE_INSENSITIVE_ORDER);

        ObjLongConsumer<Map<Long, Long>> increment = (map, day) -> {
            Long count = CollectionUtils.putIfAbsentAndGet(map, day, () -> 0L);
            map.put(day, count + 1);
        };
        while (true) {
            List<ExtTransaction> transactions = extTransactionDAO.getTransactionsForGatewayAlert(start, limit, fromTime.toEpochMilli(), toTime.toEpochMilli());
            if (transactions == null || transactions.isEmpty()) {
                break;
            }
            start += limit;

            for (ExtTransaction transaction : transactions) {
                try {
                    TransactionStatus status = transaction.getStatus();
                    PaymentGatewayName gatewayName = transaction.getGatewayName();

                    String paymentInstrument = transaction.getPaymentInstrument();
                    String paymentMethod = transaction.getPaymentMethod();

                    String instruKey = Objects.toString(gatewayName, "") + Objects.toString(status, "") + Objects.toString(paymentInstrument, "none");
                    String methodKey = Objects.toString(gatewayName, "") + Objects.toString(status, "") + Objects.toString(paymentMethod, "none");

                    val instruReport = CollectionUtils.putIfAbsentAndGet(paymentInstruAlertMap, instruKey, () -> new GatewayReport(paymentInstrument, status, gatewayName) );
                    val methodReport = CollectionUtils.putIfAbsentAndGet(paymentMethodAlertMap, methodKey, () -> new GatewayReport(paymentMethod, status, gatewayName));

                    Map<Long, Long> instruReportMap = instruReport.getReportMap();
                    Map<Long, Long> methodReportMap = methodReport.getReportMap();

                    Long creationTime = transaction.getCreationTime();
                    long day = (currentMillis - creationTime) / DateTimeUtils.MILLIS_PER_DAY;

                    if (day == 0) {
                        increment.accept(instruReportMap, 1);
                        increment.accept(methodReportMap, 1);
                    }

                    if (day < 3) {
                        increment.accept(instruReportMap, 3);
                        increment.accept(methodReportMap, 3);
                    }

                    if (day < 7) {
                        increment.accept(instruReportMap, 7);
                        increment.accept(methodReportMap, 7);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        logger.info("paymentInstruAlertMap = " + GSON.toJson(paymentInstruAlertMap));
        logger.info("paymentMethodAlertMap = " + GSON.toJson(paymentMethodAlertMap));
        sendEmailAlert(getGatewayAlertHtml(paymentMethodAlertMap, "Payment Method"), getGatewayAlertHtml(paymentInstruAlertMap, "Payment Instrument"));
    }

    private String getGatewayAlertHtml(Map<String, GatewayReport> map, String key) {
        Collection<GatewayReport> values = map.values();
        DecimalFormat format = new DecimalFormat("###.##");

        Function<List<GatewayReport>, Map<Long, Long>> sumFunction = (List<GatewayReport> e) -> {
            Map<Long, Long> countMap = new HashMap<>();
            if (e == null) {
                return countMap;
            }
            for (GatewayReport report : e) {
                Map<Long, Long> reportMap = report.getReportMap();
                for (Map.Entry<Long, Long> entry : reportMap.entrySet()) {
                    Long count = CollectionUtils.putIfAbsentAndGet(countMap, entry.getKey(), () -> 0L);
                    countMap.put(entry.getKey(), count + entry.getValue());
                }
            }
            return countMap;
        };

        Map<Long, Long> totalMap = values.stream().collect(Collectors.collectingAndThen(Collectors.toList(), sumFunction));
        Set<Long> totalKeys = new TreeSet<>(totalMap.keySet());
        Map<TransactionStatus, Map<Long, Long>> statusMap = values.stream().collect(Collectors.groupingBy(GatewayReport::getStatus, Collectors.collectingAndThen(Collectors.toList(), sumFunction)));
        Map<PaymentGatewayName, Map<Long, Long>> nameMap = values.stream().collect(Collectors.groupingBy(GatewayReport::getGatewayName, Collectors.collectingAndThen(Collectors.toList(), sumFunction)));
        Map<PaymentGatewayName, Map<TransactionStatus, Map<Long, Long>>> nameStatusMap = values.stream().collect(Collectors.groupingBy(GatewayReport::getGatewayName, Collectors.groupingBy(GatewayReport::getStatus, Collectors.collectingAndThen(Collectors.toList(), sumFunction))));

        Function<Map<Long, Long>, String> dayHeaderFunction = (collection) -> collection.keySet().stream().sorted().map(Object::toString).collect(Collectors.joining(" days count</th><th> Last ", "<th> Last ", " days count</th>"));
        Function<Map<Long, Long>, String> dayValueFunction = (collection) -> {
            StringBuilder builder = new StringBuilder();
            for (Long day : totalKeys) {
                Long dayValue = collection.get(day) == null ? 0L : collection.get(day);
                builder.append("<td>").append(format.format(dayValue)).append("</td>");
            }
            return builder.toString();
        };

        BiFunction<Collection<Long>, String, String> percentageHeader = (keys, dataStr) -> keys.stream().sorted().map(Object::toString).collect(Collectors.joining(" days contribution percentage - " + dataStr + "</th><th>Last ", "<th>Last ", " days contribution percentage - " + dataStr + "</th>"));
        TriFunction<Map<Long, Long>, Map<Long, Long>, Boolean, String> percentageValue = (value, total, applyRedCss) -> {
            StringBuilder builder = new StringBuilder();
            for (Long day : totalKeys) {
                double dayValue = value.get(day) == null ? 0 : value.get(day) * 1D;
                double dayTotal = total.get(day) == null ? 1 : total.get(day) * 1D ;
                double percent = (dayValue / dayTotal) * 100;
                builder.append("<td").append(applyRedCss && percent > 10 ? " style='background-color: salmon'" : "").append(">")
                        .append(format.format(percent)).append("</td>");
            }
            return builder.toString();
        };

        // -------------------------------------------------------------
        StringBuilder totalMapHtml = new StringBuilder("<table><tr>")
                .append(dayHeaderFunction.apply(totalMap)).append("</tr><tr>")
                .append(dayValueFunction.apply(totalMap)).append("</tr></table>");

        // -------------------------------------------------------------
        StringBuilder statusMapHtml = new StringBuilder("<table><tr>")
                .append("<th>Transaction Status</th>")
                .append(dayHeaderFunction.apply(totalMap))
                .append(percentageHeader.apply(totalKeys, "compared to total"))
                .append("</tr>");

        for (TransactionStatus status : statusMap.keySet()) {
            Map<Long, Long> longMap = statusMap.get(status);
            statusMapHtml.append("<tr>")
                    .append("<td>").append(status).append("</td>")
                    .append(dayValueFunction.apply(longMap))
                    .append(percentageValue.apply(longMap, totalMap, TransactionStatus.FAILED.equals(status)))
                    .append("</tr>");
        }
        statusMapHtml.append("</table>");
        // -------------------------------------------------------------

        StringBuilder gatewayMapHtml = new StringBuilder("<table><tr>")
                .append("<th>Gateway Name</th>")
                .append(dayHeaderFunction.apply(totalMap))
                .append(percentageHeader.apply(totalKeys, "compared to total"))
                .append("</tr>");

        for (PaymentGatewayName name : nameMap.keySet()) {
            Map<Long, Long> longMap = nameMap.get(name);
            gatewayMapHtml.append("<tr>")
                    .append("<td>").append(name).append("</td>")
                    .append(dayValueFunction.apply(longMap))
                    .append(percentageValue.apply(longMap, totalMap, false))
                    .append("</tr>");
        }
        gatewayMapHtml.append("</table>");
        // -------------------------------------------------------------

        StringBuilder allDataMapHtml = new StringBuilder("<table><tr>")
                .append("<th>Gateway Name</th>")
                .append("<th>Transaction Status</th>")
                .append(dayHeaderFunction.apply(totalMap))
                .append(percentageHeader.apply(totalKeys, "compared to total"))
                .append(percentageHeader.apply(totalKeys, "compared to gateway"))
                .append("</tr>");

        for (PaymentGatewayName name : nameStatusMap.keySet()) {
            Map<TransactionStatus, Map<Long, Long>> transactionStatusMap = nameStatusMap.get(name);
            for (TransactionStatus status : transactionStatusMap.keySet()) {
                Map<Long, Long> longMap = transactionStatusMap.get(status);
                allDataMapHtml.append("<tr>")
                        .append("<td>").append(name).append("</td>")
                        .append("<td>").append(status).append("</td>")
                        .append(dayValueFunction.apply(longMap))
                        .append(percentageValue.apply(longMap, totalMap, TransactionStatus.FAILED.equals(status)))
                        .append(percentageValue.apply(longMap, nameMap.get(name), TransactionStatus.FAILED.equals(status)))
                        .append("</tr>");
            }
        }
        allDataMapHtml.append("</table>");
        // -------------------------------------------------------------

        StringBuilder actualData = new StringBuilder("<table><tr>")
                .append("<th>Gateway Name</th>")
                .append("<th>Transaction Status</th>")
                .append("<th>").append(key).append("</th>")
                .append(dayHeaderFunction.apply(totalMap))
                .append(percentageHeader.apply(totalKeys, "compared to total"))
                .append(percentageHeader.apply(totalKeys, "compared to gateway"))
                .append(percentageHeader.apply(totalKeys, "compared to gateway > status"))
                .append("</tr>");

        for (String name : map.keySet()) {
            GatewayReport report = map.get(name);
            Map<Long, Long> longMap = report.getReportMap();
            Map<TransactionStatus, Map<Long, Long>> txMap = nameStatusMap.get(report.getGatewayName());
            Map<Long, Long> txTotalMap = txMap == null ? new HashMap<>(0) : txMap.get(report.getStatus());
            actualData.append("<tr>")
                    .append("<td>").append(report.getGatewayName()).append("</td>")
                    .append("<td>").append(report.getStatus()).append("</td>")
                    .append("<td>").append(report.getKey()).append("</td>")
                    .append(dayValueFunction.apply(longMap))
                    .append(percentageValue.apply(longMap, totalMap, TransactionStatus.FAILED.equals(report.getStatus())))
                    .append(percentageValue.apply(longMap, nameMap.get(report.getGatewayName()), TransactionStatus.FAILED.equals(report.getStatus())))
                    .append(percentageValue.apply(longMap, txTotalMap, TransactionStatus.FAILED.equals(report.getStatus())))
                    .append("</tr>");
        }
        actualData.append("</table>");
        // -------------------------------------------------------------

        return new StringBuilder().append("<html>")
                .append("<head><style> table, tr, td, th {border:1px solid black;border-collapse: collapse} \n td {text-align: center}</style></head>")
                .append(totalMapHtml)
                .append("<br>")
                .append("<br>")
                .append(gatewayMapHtml)
                .append("<br>")
                .append("<br>")
                .append(statusMapHtml)
                .append("<br>")
                .append("<br>")
                .append(allDataMapHtml)
                .append("<br>")
                .append("<br>")
                .append(actualData)
                .append("<div style='display: none'><pre>")
                .append("totalMap=")
                .append(GSON_PRETTY.toJson(totalMap))
                .append("<br>")
                .append("nameMap=")
                .append(GSON_PRETTY.toJson(nameMap))
                .append("<br>")
                .append("statusMap=")
                .append(GSON_PRETTY.toJson(statusMap))
                .append("<br>")
                .append("nameStatusMap=")
                .append(GSON_PRETTY.toJson(nameStatusMap))
                .append("map=")
                .append(GSON_PRETTY.toJson(map))
                .append("</pre></div>")
                .append("<br>")
                .append("<hr>")
                .append("<br>")
                .append("</html>")
                .toString();
    }

    private void sendEmailAlert(String paymentMethodJson, String paymentInstrumentJson) throws AddressException, VException {
        EmailRequest request = new EmailRequest();
        List<InternetAddress> to = new ArrayList<>();
        String toAddr = ConfigUtils.INSTANCE.getStringValue("dinero.extransaction.status.alert.to");
        for (String address : toAddr.split(",")) {
            to.add(new InternetAddress(address.trim()));
        }
        request.setTo(to);
        request.setBody(paymentMethodJson);
        List<InternetAddress> bcc = new ArrayList<>();
        String bccAddr = ConfigUtils.INSTANCE.getStringValue("dinero.extransaction.status.alert.bcc");
        for (String addr : bccAddr.split(",")) {
            bcc.add(new InternetAddress(addr.trim()));
        }
        request.setBcc(bcc);
        request.setType(CommunicationType.EXTTRANSACTION_ALERT);
        request.setSubject("Transaction Status Alert - PaymentMethod");
        communicationManager.sendEmailViaRest(request);

        request.setBody(paymentInstrumentJson);
        request.setSubject("Transaction Status Alert - PaymentInstrument");
        communicationManager.sendEmailViaRest(request);
    }

}
