/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import com.vedantu.exception.VException;
import java.util.List;

/**
 *
 * @author ajith
 */
public class AbstractCouponValidator implements ICouponValidator {

    @Override
    public boolean supportsBatch() {
        return false;
    }

    @Override
    public List<String> getBatchIds(String purchasingEntityId) {
        return null;
    }

    @Override
    public boolean supportsPlan() {
        return false;
    }

    @Override
    public List<String> getPlanIds(String purchasingEntityId) {
        return null;
    }

    @Override
    public boolean supportsTeacher() {
        return false;
    }

    @Override
    public List<String> getTeacherIds(String planId) {
        return null;
    }

    @Override
    public boolean supportsCourse() {
        return false;
    }

    @Override
    public List<String> getCourseIds(String purchasingEntityId) {
        return null;
    }

    @Override
    public boolean supportsCoursePlan() {
        return false;
    }

    @Override
    public List<String> getCoursePlanIds(String purchasingEntityId) {
        return null;
    }

    @Override
    public boolean supportsSubject() {
        return false;
    }

    @Override
    public List<String> getSubjects(String planId) {
        return null;
    }

    @Override
    public boolean supportsCategory() {
        return false;
    }

    @Override
    public List<String> getCategories(String purchasingEntityId) throws VException {
        return null;
    }

    @Override
    public boolean supportsGrade() {
        return false;
    }

    @Override
    public List<String> getGrades(String purchasingEntityId) throws VException {
        return null;
    }

    @Override
    public boolean supportsBundle() {
        return false;
    }

    @Override
    public List<String> getBundleIds(String purchasingEntityId) {
        return null;
    }

    @Override
    public boolean supportsBundlePackage() {
        return false;
    }

    @Override
    public List<String> getBundlePackageIds(String purchasingEntityId) {
        return null;
    }

    @Override
    public Integer getTotalAmount(String purchasingEntityId) throws VException {
        return 0;
    }

    @Override
    public List<String> getOTFBundleIds(String purchasingEntityId) {
        return null;
    }

    @Override
    public List<String> getValidMonths(String planId) {
        return null;
    }

    @Override
    public boolean supportsOTFBundle() {
        return false;
    }

    @Override
    public boolean supportsOTMBundleRegistration() {
        return false;
    }

    @Override
    public boolean supportsCoursePlanRegistration() {
        return false;
    }

    @Override
    public boolean supportsCourseRegistration() {
        return false;
    }

    @Override
    public boolean supportsBatchRegistration() {
        return false;
    }
}
