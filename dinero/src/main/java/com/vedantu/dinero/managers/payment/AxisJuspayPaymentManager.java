package com.vedantu.dinero.managers.payment;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.payment.axis.JuspayOrder;
import com.vedantu.dinero.managers.payment.axis.JuspayRequestOptions;
import com.vedantu.dinero.pojo.BillingReasonType;
import com.vedantu.dinero.pojo.PaymentOption;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.dinero.serializers.TransactionDAO;
import com.vedantu.dinero.sql.dao.AccountDAO;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.*;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import in.juspay.exception.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

@SuppressWarnings("Duplicates")
@Service
public class AxisJuspayPaymentManager implements IPaymentManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AxisJuspayPaymentManager.class);

    @Autowired
    public FosUtils fosUtils;

    @Autowired
    public ExtTransactionDAO extTransactionDAO;

    @Autowired
    public TransactionDAO transactionDAO;

    @Autowired
    public AccountDAO accountDAO;

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    private static final String PAYMENT_CHANNEL = "AXIS_JUSPAY";

    private final PaymentGatewayName gatewayName = PaymentGatewayName.AXIS_JUSPAY;

    private static final String MERCHANT_ID = ConfigUtils.INSTANCE.getStringValue("billing.axis.juspay.merchant.id");
    private static final String CHARGING_URL = ConfigUtils.INSTANCE.getStringValue("billing.axis.juspay.charging.url");
    private static final String REDIRECT_URL = ConfigUtils.INSTANCE.getStringValue("billing.axis.juspay.redirect.url");
    private static final String API_KEY = ConfigUtils.INSTANCE.getStringValue("billing.axis.juspay.api.key");
    private static final String RESPONSE_KEY = ConfigUtils.INSTANCE.getStringValue("billing.axis.juspay.checksum.response.key");
    private static final String ENCRYPTION_KEY = ConfigUtils.INSTANCE.getStringValue("billing.axis.juspay.checksum.encryption.key");
    private final Gson gson = new Gson();

    public AxisJuspayPaymentManager() {
        super();
        // DONT SET THIS VALUES AS IT USED FOR JUSPAY GATEWAY
//        JuspayEnvironment.withBaseUrl(CHARGING_URL);
//        JuspayEnvironment.withApiKey(API_KEY);
//        JuspayEnvironment.withMerchantId(MERCHANT_ID);
    }

    @Override
    public PaymentGatewayName getName() {
        return this.gatewayName;
    }

    @Override
    public RechargeUrlInfo getPaymentUrl(ExtTransaction transaction, Long callingUserId, PaymentOption paymentOption) throws InternalServerErrorException {

        BigDecimal amount = new BigDecimal(transaction.getAmount()).divide(new BigDecimal(100), RoundingMode.HALF_EVEN);

        ClientResponse response = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + transaction.getUserId(), HttpMethod.GET, null);
        String json = response.getEntity(String.class);
        User user = gson.fromJson(json, User.class);

        Map<String, Object> params = new TreeMap<>();

        params.put("order_id", transaction.getUniqueOrderId());
        params.put("amount", amount.setScale(2, BigDecimal.ROUND_HALF_EVEN));
        params.put("currency", "INR");
        params.put("customer_id", String.valueOf(transaction.getUserId()));
        params.put("customer_email", user.getEmail());
        params.put("return_url", REDIRECT_URL);
        params.put("description", "Payment Request From " + user.getFullName());
        params.put("udf1", transaction.getId());

        JuspayOrder order = null;

        int count = 0;
        boolean tryAgain = true;
        while (count < 2 && tryAgain) {
            try {
                count++;
                order = JuspayOrder.create(params, getRequestOption());
                tryAgain = false;
                logger.info("ORDER : " + gson.toJson(order));
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "AXIS JUSPAY: " + e.getErrorMessage());
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        }

        if (order == null) {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "AXIS JUSPAY: Order Creation Failed");
        }

        logger.info("payment channel [" + PAYMENT_CHANNEL + "] payment url :  " + CHARGING_URL);
        logger.info("payment channel [" + PAYMENT_CHANNEL + "] payment params :  " + params);
        String link = order.getPaymentLinks().getWebLink();
        return new RechargeUrlInfo(PaymentGatewayName.AXIS_JUSPAY, link, "GET", new HashMap<>());
    }

    private JuspayRequestOptions getRequestOption() {
        return JuspayRequestOptions.createDefault(API_KEY, MERCHANT_ID, CHARGING_URL);
    }

    @Override
    public ExtTransaction onPaymentReceive(Map<String, Object> transactionInfo, Long callingUserId) throws NotFoundException, InternalServerErrorException, ConflictException, BadRequestException {
        // VERIFY THE RESPONSE FROM SERVER
        verifyRequest(transactionInfo, callingUserId);

        logger.info(transactionInfo.toString());

        Map<String, String> collect = new TreeMap<>();


        transactionInfo.forEach((key, value) -> collect.put(key, ((List<String>) value).get(0)));
        String signatureAlgorithm = collect.remove("signature_algorithm");
        String signature = collect.remove("signature");

        String str = collect.entrySet().stream()
                .map(AxisJuspayPaymentManager::getEncode)
                .collect(Collectors.joining("%26")); // URL Encoded Value of &
        String signatureCalculated = getSignature(str);

        JuspayOrder status = null;
        String order_id = collect.get("order_id");

        int count = 0;
        boolean tryAgain = true;
        while (count < 2 && tryAgain) {
            try {
                count++;
                status = JuspayOrder.status(order_id, getRequestOption());
                tryAgain = false;
            } catch (APIException | AuthorizationException | AuthenticationException | InvalidRequestException e) {
                logger.error(e.getMessage(), e);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "AXIS JUSPAY: API Error Occurred while fetching status");
            } catch (APIConnectionException e) {
                logger.error(e.getMessage(), e);
            }
        }


        ExtTransaction extTransaction;
        if (status != null) {
            double amount = status.getAmount() * 100;
            int amountPaid = (int) amount;

            String paymentStatus = status.getStatus();

            String transactionId = status.getUdf1(); // ExtTransaction ID Stored in UDF1 Param While Creating Payment URL

            String paymentChannelTransactionId = status.getPaymentGatewayResponse() == null ? null :
            status.getPaymentGatewayResponse().getTxnId();

            TransactionStatus transactionStatus;

            if (paymentStatus.equalsIgnoreCase("charged") && signature.equals(signatureCalculated)) {
                transactionStatus = TransactionStatus.SUCCESS;
            } /*else if (responseDescription.equalsIgnoreCase("Transaction Aborted")) {
                transactionStatus = TransactionStatus.CANCELLED;
            } */ else {
                transactionStatus = TransactionStatus.FAILED;
            }
            String bankRefNo = status.getTxnId();

            String paymentMethod = status.getPaymentMethod();

            String paymentInstrument = status.getPaymentMethodType();


            if (transactionStatus != TransactionStatus.SUCCESS) {
                amountPaid = 0;
            }

            logger.info("transaction status : " + transactionStatus);

            logger.info("starting transaction ");
            extTransaction = extTransactionDAO.getById(transactionId);
            logger.info("extTransaction before updation : " + extTransaction);

            // Save output to DB
            extTransaction.setGatewayResponse(gson.toJson(status));

            if (extTransaction.getStatus() != TransactionStatus.PENDING) {

                throw new ConflictException(ErrorCode.TRANSACTION_ALREADY_PROCESSED,
                        "transaction with id " + transactionId + " is already processed");
            }
            extTransaction.setStatus(transactionStatus);
            extTransaction.setGatewayStatus(paymentStatus);
            extTransaction.setTransactionTime(String.valueOf(System.currentTimeMillis()));
            extTransaction.setPaymentChannelTransactionId(paymentChannelTransactionId);
            extTransaction.setPaymentInstrument(paymentInstrument);
            extTransaction.setPaymentMethod(paymentMethod);
            extTransaction.setBankRefNo(bankRefNo);
            logger.info("saving extTransaction: " + extTransaction);
            extTransactionDAO.create(extTransaction, callingUserId);
            if (extTransaction.getAmount() == amountPaid && transactionStatus == TransactionStatus.SUCCESS) {
                // TODO: these functionality can be moved to abstract class
                // transaction = TransactionDAO.INSTANCE
                // .upsertTransaction(transaction);

                Account account;
                // TODO: take this code to common place, where update and
                // deduct
                // operation can be done
                logger.info("transaction was successful:" + extTransaction);
                account = accountDAO
                        .getAccountByHolderId(Account.__getUserAccountHolderId(extTransaction.getUserId().toString()));
                if (account == null) {
                    logger.error("no account found for userid:" + extTransaction.getUserId());

                    throw new NotFoundException(ErrorCode.ACCOUNT_NOT_FOUND,
                            "no account found for userId:" + extTransaction.getUserId());
                }
                logger.info("updating account balance for account[" + account.getHolderId() + "]: " + account);

                account.setBalance(account.getBalance() + amountPaid);
                account.setNonPromotionalBalance(account.getNonPromotionalBalance() + amountPaid);

                logger.info("new account balance : " + account.getBalance());


                accountDAO.updateWithoutSession(account, null);
                
                com.vedantu.dinero.entity.Transaction vedantuTransaction = new com.vedantu.dinero.entity.Transaction(
                        amountPaid, 0, amountPaid, ExtTransaction.Constants.DEFAULT_CURRENCY_CODE, null, null, account.getId().toString(), account.getBalance(),
                        BillingReasonType.RECHARGE.name(), extTransaction.getId(),
                        TransactionRefType.EXTERNAL_TRANSACTION, null,
                        com.vedantu.dinero.entity.Transaction._getTriggredByUser(extTransaction.getUserId()));
                transactionDAO.create(vedantuTransaction);
                
                
                logger.info("Account " + account.toString());
            }
            return extTransaction;
        } else {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "AXIS JUSPAY: Order status not found for JuspayOrder ID " + order_id);
        }
    }

    private static String getEncode(Map.Entry<String, String> e)  {
        try {
            return URLEncoder.encode(e.getKey() + "=" + e.getValue(), StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        return "";
    }

    private String getSignature(String fields) throws InternalServerErrorException {

        try {
            final Mac instance = Mac.getInstance("HmacSHA256");
            final SecretKeySpec secret_key = new SecretKeySpec(RESPONSE_KEY.getBytes(), "HmacSHA256");
            instance.init(secret_key);
            final byte[] mac_data = instance.doFinal(fields.getBytes());
            return java.util.Base64.getEncoder().encodeToString(mac_data);
        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            logger.error(e.getMessage(), e);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }
}
