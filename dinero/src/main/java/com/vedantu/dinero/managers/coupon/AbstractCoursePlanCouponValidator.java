
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class AbstractCoursePlanCouponValidator extends AbstractCouponValidator {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CoursePlanCouponValidator.class);

    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    @Override
    public List<String> getGrades(String coursePlanId) {
        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanInfo(coursePlanId);
        if (coursePlanBasicInfo != null && coursePlanBasicInfo.getGrade() != null) {
            return Arrays.asList(coursePlanBasicInfo.getGrade().toString());
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsGrade() {
        return true;
    }

    @Override
    public List<String> getCategories(String coursePlanId) {
        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanInfo(coursePlanId);
        if (coursePlanBasicInfo != null) {
            return Arrays.asList(coursePlanBasicInfo.getTarget());
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsCategory() {
        return true;
    }

    @Override
    public List<String> getSubjects(String coursePlanId) {
        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanInfo(coursePlanId);
        if (coursePlanBasicInfo != null) {
            return Arrays.asList(coursePlanBasicInfo.getSubject().toLowerCase());
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsSubject() {
        return true;
    }

    @Override
    public List<String> getCoursePlanIds(String purchasingEntityId) {
        return Arrays.asList(purchasingEntityId);
    }

    @Override
    public List<String> getTeacherIds(String id) {
        CoursePlanBasicInfo coursePlanBasicInfo = getCoursePlanInfo(id);
        if (coursePlanBasicInfo != null) {
            return Arrays.asList(coursePlanBasicInfo.getTeacherId().toString());
        } else {
            return null;
        }
    }

    @Override
    public boolean supportsTeacher() {
        return true;
    }

    public CoursePlanBasicInfo getCoursePlanInfo(String id) {
        CoursePlanBasicInfo coursePlanBasicInfo = null;
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
                    + "/courseplan/basicInfo/" + id, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            coursePlanBasicInfo = new Gson().fromJson(jsonString, CoursePlanBasicInfo.class);
        } catch (Exception e) {
            logger.error("Error in getCoursePlanInfo " + id);
        }
        return coursePlanBasicInfo;
    }

    @Override
    public Integer getTotalAmount(String purchasingEntityId) throws VException {
        String url = SUBSCRIPTION_ENDPOINT
                + "/courseplan/getCoursePlanPriceSansRegFee?coursePlanId=" + purchasingEntityId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
        Integer price = Integer.parseInt(platformBasicResponse.getResponse());
        return price;
    }

}
