
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.managers.coupon;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.board.pojo.Board;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.WebUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class AbstractBatchCouponValidator extends AbstractCouponValidator {

    @Autowired
    private PlatformTools platformTools;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BatchCouponValidator.class);

    private final String subscriptionEndPoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    public AbstractBatchCouponValidator() {
        super();
    }

    @Override
    public List<String> getBatchIds(String purchasingEntityId) {
        return Arrays.asList(purchasingEntityId);
    }

    @Override
    public List<String> getCourseIds(String purchasingEntityId) {
        logger.info("ENTRY " + purchasingEntityId);
        List<String> list = new ArrayList<>();
        try {
            JSONObject responseObj = getBatchInfo(purchasingEntityId);
            if (responseObj.has("courseInfo")) {
                String courseId = responseObj.getJSONObject("courseInfo").getString("id");
                list = Arrays.asList(courseId);
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        logger.info("EXIT " + list);
        return list;
    }

    @Override
    public List<String> getGrades(String purchasingEntityId) {
        return getBatchMetadata(purchasingEntityId, "grades");
    }

    @Override
    public boolean supportsGrade() {
        return true;
    }

    @Override
    public List<String> getCategories(String purchasingEntityId) {
        List<String> categories = new ArrayList<String>();
        categories.addAll(getBatchMetadata( purchasingEntityId, "targets" ));
        categories.addAll( getBatchMetadata( purchasingEntityId, "searchTerms" ) );
        return categories;
    }

    @Override
    public boolean supportsCategory() {
        return true;
    }

    @Override
    public List<String> getSubjects(String purchasingEntityId) {
        List<String> _boardIds = getBatchMetadata(purchasingEntityId, "subjects");
        List<String> subjects = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(_boardIds)) {
            try {
                List<Long> boardIds = new ArrayList<>();
                for (String boardId : _boardIds) {
                    Long bId = Double.valueOf(boardId).longValue();
                    boardIds.add(bId);
                }
                logger.info("fetching board map of " + boardIds);
                Map<Long, Board> resp = fosUtils.getBoardInfoMap(boardIds);
                if (resp != null) {
                    for (Long boardId : boardIds) {
                        if (resp.containsKey(boardId)) {
                            subjects.add(resp.get(boardId).getSlug());
                        }
                    }
                }
            } catch (Exception e) {
                logger.info("ERROR ", e);
            }
        }
        return subjects;
    }

    @Override
    public boolean supportsSubject() {
        return true;
    }

    public List<String> getBatchMetadata(String purchasingEntityId, String metadataName) {
        logger.info("ENTRY " + purchasingEntityId + " , " + metadataName);
        List<String> list = new ArrayList<>();
        try {
            JSONObject responseObj = getBatchInfo(purchasingEntityId);
            if (responseObj.has("courseInfo")
                    && responseObj.getJSONObject("courseInfo").has(metadataName)) {
                JSONArray arrList = responseObj.getJSONObject("courseInfo").getJSONArray(metadataName);
                for (int i = 0; i < arrList.length(); i++) {
                    list.add(arrList.getString(i));
                }
            }
        } catch (Exception e) {
            logger.info("ERROR", e);
        }
        logger.info("EXIT " + list);
        return list;
    }

    private JSONObject getBatchInfo(String purchasingEntityId) throws BadRequestException {
        String batchUrl = subscriptionEndPoint + "/batch";
        String url = batchUrl + "/" + purchasingEntityId;
        PlatformBasicResponse response = platformTools.postPlatformData(url, null, false, HttpMethod.GET);
        if (!response.isSuccess()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Error While fetching batch");
        }
        return new JSONObject(response.getResponse());
    }

    @Override
    public Integer getTotalAmount(String purchasingEntityId) throws VException {
        String url = subscriptionEndPoint
                + "/batch/getBatchPurchasePrice?batchId=" + purchasingEntityId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
        Integer price = Integer.parseInt(platformBasicResponse.getResponse());
        return price;
    }

}
