package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.EmiCardType;
import com.vedantu.dinero.enums.EmiType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "BankDetail")
public class BankDetail extends AbstractMongoStringIdEntity {
    private String bankName;
    @Indexed(background = true, unique = true)
    private String emiCode;
    private String netBankingCode;
    private List<EmiType> emiType;
    @Indexed(background = true)
    private List<EmiCardType> cardType;

    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String BANK_NAME = "bankName";
        public static final String EMI_CODE = "emiCode";
        public static final String NET_BANKING_CODE = "netBankingCode";
        public static final String EMI_TYPE = "emiType";
        public static final String EMI_CARD_TYPE = "cardType";
    }
}
