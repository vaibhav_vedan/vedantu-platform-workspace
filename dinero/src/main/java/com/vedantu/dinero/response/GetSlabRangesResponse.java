package com.vedantu.dinero.response;

import java.util.List;

public class GetSlabRangesResponse {
	
	private List<Integer> slabRangeMins;

	public List<Integer> getSlabRangeMins() {
		return slabRangeMins;
	}

	public void setSlabRangeMins(List<Integer> slabRangeMins) {
		this.slabRangeMins = slabRangeMins;
	}

	public GetSlabRangesResponse(List<Integer> slabRangeMins) {
		super();
		this.slabRangeMins = slabRangeMins;
	}

	public GetSlabRangesResponse() {
		super();
	}

	@Override
	public String toString() {
		return "GetSlabRangesResponse [slabRangeMins=" + slabRangeMins.toString() + "]";
	}
	
}
