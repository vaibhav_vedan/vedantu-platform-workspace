package com.vedantu.dinero.response;

import java.util.List;

import com.vedantu.util.fos.response.AbstractListRes;

public class GetCouponsRes extends AbstractListRes<CouponInfoRes> {

	public GetCouponsRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetCouponsRes(List<CouponInfoRes> list, int count) {
		super(list, count);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetCouponsRes []";
	}
    
}
