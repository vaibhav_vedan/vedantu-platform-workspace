package com.vedantu.dinero.response;

import com.vedantu.dinero.sql.entity.Account;

public class GetFreebiesAccountInfoRes extends GetAccountInfoRes {

	@Override
	public String toString() {
		return "GetFreebiesAccountInfoRes []";
	}

	public GetFreebiesAccountInfoRes(Integer balance, Integer lockedBalance) {
		super(balance, lockedBalance);
		// TODO Auto-generated constructor stub
	}

	public GetFreebiesAccountInfoRes(Account account) {
		super(account);
	}

	public GetFreebiesAccountInfoRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	
}
