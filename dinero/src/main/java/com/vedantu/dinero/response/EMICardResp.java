package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.EmiCardType;
import com.vedantu.util.fos.response.AbstractListRes;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EMICardResp {

	private String name;
	private String code;
	private EmiCardType cardType;
	private List<EMITenure> tenures;
}