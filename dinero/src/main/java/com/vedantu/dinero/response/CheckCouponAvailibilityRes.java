package com.vedantu.dinero.response;

import com.vedantu.util.fos.response.AbstractRes;

public class CheckCouponAvailibilityRes extends AbstractRes {

	private Boolean available;

	public Boolean getAvailable() {
		return available;
	}

	public void setAvailable(Boolean available) {
		this.available = available;
	}

	@Override
	public String toString() {
		return "CheckCouponAvailibilityRes [available=" + available + "]";
	}

	public CheckCouponAvailibilityRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CheckCouponAvailibilityRes(boolean available) {
		super();
		this.available = available;
	}

}
