package com.vedantu.dinero.response;

import java.util.List;

import com.vedantu.dinero.pojo.TeacherSessionInfo;

public class GetTeacherSessionInfoResponse {

	private List<TeacherSessionInfo> teacherSessionInfos;

	public GetTeacherSessionInfoResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetTeacherSessionInfoResponse(List<TeacherSessionInfo> teacherSessionInfos) {
		super();
		this.teacherSessionInfos = teacherSessionInfos;
	}

	@Override
	public String toString() {
		return "GetTeacherSessionInfoResponse [teacherSessionInfos=" + teacherSessionInfos + "]";
	}

	public List<TeacherSessionInfo> getTeacherSessionInfos() {
		return teacherSessionInfos;
	}

	public void setTeacherSessionInfos(List<TeacherSessionInfo> teacherSessionInfos) {
		this.teacherSessionInfos = teacherSessionInfos;
	}

}
