package com.vedantu.dinero.response;

import com.vedantu.dinero.pojo.OrderedItemNames;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @author MNPK
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderedItemNameDetailsRes {
    private List<OrderedItemNames> orderedItemNames;
}
