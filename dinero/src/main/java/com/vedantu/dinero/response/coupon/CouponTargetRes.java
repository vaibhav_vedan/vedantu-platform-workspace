/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.response.coupon;

import com.vedantu.dinero.enums.coupon.CouponTargetEntityType;
import com.vedantu.dinero.pojo.CouponTarget;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CouponTargetRes extends CouponTarget{
    private List<Object> infos;

    public CouponTargetRes(List<Object> infos) {
        this.infos = infos;
    }

    public CouponTargetRes(List<Object> infos, CouponTargetEntityType targetType, List<String> targetStrs) {
        super(targetType, targetStrs);
        this.infos = infos;
    }

    public List<Object> getInfos() {
        return infos;
    }

    public void setInfos(List<Object> infos) {
        this.infos = infos;
    }

    @Override
    public String toString() {
        return "CouponTargetRes{" + "infos=" + infos + '}';
    }
}
