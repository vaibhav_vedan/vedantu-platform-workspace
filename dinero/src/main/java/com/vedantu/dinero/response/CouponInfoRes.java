package com.vedantu.dinero.response;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.Coupon;
import com.vedantu.dinero.enums.CouponType;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.RedeemType;
import com.vedantu.dinero.pojo.CouponTarget;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.fos.response.AbstractRes;

public class CouponInfoRes extends AbstractRes {

    private String id;
    private String code;
    private CouponType type;
    // all value are in INR
    private RedeemType redeemType;
    // actual amount redeemed (paise in case of monetry term)
    private Float redeemValue;
    // optional --> minOrderValue (in paisa) above which this coupon code is
    // valid
    private Integer minOrderValue;
    private Integer maxOrderValue;
    // max amount (in paisa) that can be redeemed using this coupon --> null or
    // 0 means no
    // limit
    private Integer maxRedeemValue;
    // null or 0 means no start time
    private Long validFrom;
    // null or 0 means no end time
    private Long validTill;
    private List<String> terms;
    private String description;
    // null or 0 means unlimited
    private Integer usesCount;
    // null or 0 means unlimited
    private Integer usesCountPerUser;

    private Integer totalUsesCount;

    private Long creationTime;
    private Long lastUpdated;
    private Long targetUserId;
    private UserBasicInfo targetUser;
    private List<CouponTarget> targetsRes;
    private Long passDurationInMillis;
    private boolean satisfyAtleast1Condition = false;
    private PaymentType forPaymentType = PaymentType.BULK;

    private List<Integer> targetOrder;

    public CouponInfoRes() {
        super();
        // TODO Auto-generated constructor stub
    }

    public CouponInfoRes(Coupon coupon) throws InternalServerErrorException {
        super();
        this.id = coupon.getId();
        this.code = coupon.getCode();
        this.type = coupon.getType();
        this.redeemType = coupon.getRedeemType();
        this.redeemValue = coupon.getRedeemValue();
        this.minOrderValue = coupon.getMinOrderValue();
        this.maxOrderValue = coupon.getMaxOrderValue();
        this.maxRedeemValue = coupon.getMaxRedeemValue();
        this.validFrom = coupon.getValidFrom();
        this.validTill = coupon.getValidTill();
        this.terms = coupon.getTerms();
        this.description = coupon.getDescription();
        this.usesCount = coupon.getUsesCount();
        this.usesCountPerUser = coupon.getUsesCountPerUser();
        this.creationTime = coupon.getCreationTime();
        this.lastUpdated = coupon.getLastUpdated();
        this.targetUserId = coupon.getTargetUserId();
        this.targetsRes = coupon.getTargets();
        this.passDurationInMillis = coupon.getPassDurationInMillis();
        this.satisfyAtleast1Condition = coupon.isSatisfyAtleast1Condition();
        this.forPaymentType = coupon.getForPaymentType();
        this.targetOrder = coupon.getTargetOrder();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CouponType getType() {
        return type;
    }

    public void setType(CouponType type) {
        this.type = type;
    }

    public RedeemType getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(RedeemType redeemType) {
        this.redeemType = redeemType;
    }

    public Float getRedeemValue() {
        return redeemValue;
    }

    public void setRedeemValue(Float redeemValue) {
        this.redeemValue = redeemValue;
    }

    public Integer getMinOrderValue() {
        return minOrderValue;
    }

    public void setMinOrderValue(Integer minOrderValue) {
        this.minOrderValue = minOrderValue;
    }

    public Integer getMaxRedeemValue() {
        return maxRedeemValue;
    }

    public void setMaxRedeemValue(Integer maxRedeemValue) {
        this.maxRedeemValue = maxRedeemValue;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTill() {
        return validTill;
    }

    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUsesCount() {
        return usesCount;
    }

    public void setUsesCount(Integer usesCount) {
        this.usesCount = usesCount;
    }

    public Integer getUsesCountPerUser() {
        return usesCountPerUser;
    }

    public void setUsesCountPerUser(Integer usesCountPerUser) {
        this.usesCountPerUser = usesCountPerUser;
    }

    public Integer getTotalUsesCount() {
        return totalUsesCount;
    }

    public void setTotalUsesCount(Integer totalUsesCount) {
        this.totalUsesCount = totalUsesCount;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(Long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public UserBasicInfo getTargetUser() {
        return targetUser;
    }

    public void setTargetUser(UserBasicInfo targetUser) {
        this.targetUser = targetUser;
    }

    public List<CouponTarget> getTargetsRes() {
        return targetsRes;
    }

    public void setTargetsRes(List<CouponTarget> targetsRes) {
        this.targetsRes = targetsRes;
    }

    public Long getPassDurationInMillis() {
        return passDurationInMillis;
    }

    public void setPassDurationInMillis(Long passDurationInMillis) {
        this.passDurationInMillis = passDurationInMillis;
    }

    public boolean isSatisfyAtleast1Condition() {
        return satisfyAtleast1Condition;
    }

    public void setSatisfyAtleast1Condition(boolean satisfyAtleast1Condition) {
        this.satisfyAtleast1Condition = satisfyAtleast1Condition;
    }

    public PaymentType getForPaymentType() {
        return forPaymentType;
    }

    public void setForPaymentType(PaymentType forPaymentType) {
        this.forPaymentType = forPaymentType;
    }

    public List<Integer> getTargetOrder() {
        return targetOrder;
    }

    public void setTargetOrder(List<Integer> targetOrder) {
        this.targetOrder = targetOrder;
    }

    public Integer getMaxOrderValue() {
        return maxOrderValue;
    }

    public void setMaxOrderValue(Integer maxOrderValue) {
        this.maxOrderValue = maxOrderValue;
    }
}
