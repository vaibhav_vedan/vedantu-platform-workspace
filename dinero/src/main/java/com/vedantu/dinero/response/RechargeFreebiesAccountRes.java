package com.vedantu.dinero.response;

import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.util.fos.response.AbstractRes;

public class RechargeFreebiesAccountRes extends AbstractRes {

	private Integer balance;

	public RechargeFreebiesAccountRes(Account account) {

		super();
		this.balance = account.getBalance();
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getBalance() {

		return balance;
	}

	@Override
	public String toString() {
		return "RechargeFreebiesAccountRes [balance=" + balance + "]";
	}

	public RechargeFreebiesAccountRes(Integer balance) {
		super();
		this.balance = balance;
	}

	public RechargeFreebiesAccountRes() {
		super();
		// TODO Auto-generated constructor stub
	}

}
