package com.vedantu.dinero.response;

import java.util.List;

import com.vedantu.dinero.entity.Transaction;

public class ExportDailyTransactionsResponse {

	private List<Transaction> transactions;

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "ExportDailyTransactionsResponse [transactions=" + transactions + "]";
	}

	public ExportDailyTransactionsResponse(List<Transaction> transactions) {
		super();
		this.transactions = transactions;
	}

	public ExportDailyTransactionsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

}
