package com.vedantu.dinero.response;

import com.vedantu.dinero.sql.entity.Account;

public class TransferFromFreebiesAccountRes extends GetAccountInfoRes {

	public TransferFromFreebiesAccountRes(Account account) {

		super(account);
	}

	@Override
	public String toString() {
		return "TransferFromFreebiesAccountRes []";
	}

	public TransferFromFreebiesAccountRes(Integer balance, Integer lockedBalance) {
		super(balance, lockedBalance);
		// TODO Auto-generated constructor stub
	}

	public TransferFromFreebiesAccountRes() {
		super();
	}
}
