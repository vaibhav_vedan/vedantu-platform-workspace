package com.vedantu.dinero.response;

import java.util.List;

import com.vedantu.dinero.pojo.TotalMonthlyPayout;

public class GetTotalPayoutResponse {

	private Long teacherId;
	private List<TotalMonthlyPayout> monthlyPayouts;
	private int totalSessions = 0;
	private int studentsTaught = 0;
	private int sessionEarnings = 0;
	private int cut = 0;
	private int incentive = 0;
	private int totalEarnings = 0;

	public GetTotalPayoutResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "GetTotalPayoutResponse [teacherId=" + teacherId + ", monthlyPayouts=" + monthlyPayouts
				+ ", totalSessions=" + totalSessions + ", studentsTaught=" + studentsTaught + ", sessionEarnings="
				+ sessionEarnings + ", cut=" + cut + ", incentive=" + incentive + ", totalEarnings=" + totalEarnings
				+ "]";
	}

	public GetTotalPayoutResponse(Long teacherId, List<TotalMonthlyPayout> monthlyPayouts, int totalSessions,
			int studentsTaught, int sessionEarnings, int cut, int incentive, int totalEarnings) {
		super();
		this.teacherId = teacherId;
		this.monthlyPayouts = monthlyPayouts;
		this.totalSessions = totalSessions;
		this.studentsTaught = studentsTaught;
		this.sessionEarnings = sessionEarnings;
		this.cut = cut;
		this.incentive = incentive;
		this.totalEarnings = totalEarnings;
	}

	public int getTotalSessions() {
		return totalSessions;
	}

	public void setTotalSessions(int totalSessions) {
		this.totalSessions = totalSessions;
	}

	public int getStudentsTaught() {
		return studentsTaught;
	}

	public void setStudentsTaught(int studentsTaught) {
		this.studentsTaught = studentsTaught;
	}

	public int getSessionEarnings() {
		return sessionEarnings;
	}

	public void setSessionEarnings(int sessionEarnings) {
		this.sessionEarnings = sessionEarnings;
	}

	public int getCut() {
		return cut;
	}

	public void setCut(int cut) {
		this.cut = cut;
	}

	public int getIncentive() {
		return incentive;
	}

	public void setIncentive(int incentive) {
		this.incentive = incentive;
	}

	public int getTotalEarnings() {
		return totalEarnings;
	}

	public void setTotalEarnings(int totalEarnings) {
		this.totalEarnings = totalEarnings;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public List<TotalMonthlyPayout> getMonthlyPayouts() {
		return monthlyPayouts;
	}

	public void setMonthlyPayouts(List<TotalMonthlyPayout> monthlyPayouts) {
		this.monthlyPayouts = monthlyPayouts;
	}

}
