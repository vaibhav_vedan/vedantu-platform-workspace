package com.vedantu.dinero.response;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class ApplyCouponRes extends AbstractFrontEndReq {

	private Boolean applied;

	public ApplyCouponRes(Boolean applied) {
		super();
		this.applied = applied;
	}

	public ApplyCouponRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Boolean getApplied() {
		return applied;
	}

	public void setApplied(Boolean applied) {
		this.applied = applied;
	}


}
