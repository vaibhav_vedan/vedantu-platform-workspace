package com.vedantu.dinero.response;

import com.vedantu.dinero.sql.entity.Account;

public class SubmitCashChequeRes extends GetAccountInfoRes {

	public SubmitCashChequeRes(Account account) {

		super(account);
	}

	public SubmitCashChequeRes() {
		super();
	}
}
