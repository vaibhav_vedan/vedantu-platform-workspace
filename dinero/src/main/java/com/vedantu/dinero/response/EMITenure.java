package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.EmiCardType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class EMITenure {

	private Integer months;
	private Float rate;
	private String emiId;
	private Long	minTenureAmount;
}

