package com.vedantu.dinero.response;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.dinero.entity.Plan;

public class GetPlansByIdsResponse {

	List<GetPlanByIdResponse> plans = new ArrayList<GetPlanByIdResponse>();

	public GetPlansByIdsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetPlansByIdsResponse(List<GetPlanByIdResponse> plans) {
		super();
		this.plans = plans;
	}

	public List<GetPlanByIdResponse> getPlans() {
		return plans;
	}

	public void setPlans(List<GetPlanByIdResponse> plans) {
		this.plans = plans;
	}

	public void addPlan(Plan foundPlan) {
		GetPlanByIdResponse plan = new GetPlanByIdResponse(foundPlan);
		this.plans.add(plan);
	}

	@Override
	public String toString() {
		return "GetPlansByIdsResponse [plans=" + plans + "]";
	}

}
