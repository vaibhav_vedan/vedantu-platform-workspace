package com.vedantu.dinero.response;

import com.vedantu.dinero.sql.entity.Account;

public class TransferToVedantuDefaultAccountRes extends TransferFromVedantuDefaultAccountRes {

	public TransferToVedantuDefaultAccountRes(Integer balance) {
		super(balance);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "TransferToVedantuDefaultAccountRes []";
	}

	public TransferToVedantuDefaultAccountRes(Account account) {
		super(account);
	}

	public TransferToVedantuDefaultAccountRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
