package com.vedantu.dinero.response;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.util.fos.response.AbstractRes;

public class PaymentGatewayNameRes extends AbstractRes{

	
	private String userId;
	
	private PaymentGatewayName paymentGatewayName;
	
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public PaymentGatewayName getPaymentGatewayName() {
		return paymentGatewayName;
	}

	public void setPaymentGatewayName(PaymentGatewayName paymentGatewayName) {
		this.paymentGatewayName = paymentGatewayName;
	}
}
