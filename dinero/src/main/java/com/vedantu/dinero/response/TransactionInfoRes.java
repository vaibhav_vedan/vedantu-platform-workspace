package com.vedantu.dinero.response;

import java.io.Serializable;

import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.enums.TransactionType;

public class TransactionInfoRes implements Serializable {

	private static final long serialVersionUID = 1L;
	private String id;
	private Long creationTime;
	private Long userId;
	private String ipAddress;
	private TransactionType type;
	private TransactionStatus status;

	// amount in paisa
	private Integer amount;

	// (currencyCode==INR)
	private String currencyCode;
	private String transactionTime;

	public TransactionInfoRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public TransactionInfoRes(ExtTransaction t) {

		super();
		this.id = t.getId();
		this.creationTime = t.getCreationTime();
		this.userId = t.getUserId();
		this.ipAddress = t.getIpAddress();
		this.type = t.getType();
		this.status = t.getStatus();
		this.amount = t.getAmount();
		this.currencyCode = t.getCurrencyCode();
		this.transactionTime = t.getTransactionTime();
	}

	public TransactionInfoRes(String id, Long creationTime, Long userId, String ipAddress, TransactionType type,
			TransactionStatus status, Integer amount, String currencyCode, String transactionTime) {
		super();
		this.id = id;
		this.creationTime = creationTime;
		this.userId = userId;
		this.ipAddress = ipAddress;
		this.type = type;
		this.status = status;
		this.amount = amount;
		this.currencyCode = currencyCode;
		this.transactionTime = transactionTime;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}

	public void setType(TransactionType type) {
		this.type = type;
	}

	public void setStatus(TransactionStatus status) {
		this.status = status;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public void setCurrencyCode(String currencyCode) {
		this.currencyCode = currencyCode;
	}

	public void setTransactionTime(String transactionTime) {
		this.transactionTime = transactionTime;
	}

	/**
	 *
	 */

	public String getId() {

		return id;
	}

	public Long getCreationTime() {

		return creationTime;
	}

	public Long getUserId() {

		return userId;
	}

	public String getIpAddress() {

		return ipAddress;
	}

	public TransactionType getType() {

		return type;
	}

	public TransactionStatus getStatus() {

		return status;
	}

	@Override
	public String toString() {
		return "TransactionInfoRes [id=" + id + ", creationTime=" + creationTime + ", userId=" + userId + ", ipAddress="
				+ ipAddress + ", type=" + type + ", status=" + status + ", amount=" + amount + ", currencyCode="
				+ currencyCode + ", transactionTime=" + transactionTime + "]";
	}

	public Integer getAmount() {

		return amount;
	}

	public String getCurrencyCode() {

		return currencyCode;
	}

	public String getTransactionTime() {

		return transactionTime;
	}

}
