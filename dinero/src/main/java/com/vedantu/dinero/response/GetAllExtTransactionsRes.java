package com.vedantu.dinero.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.util.fos.response.AbstractRes;

public class GetAllExtTransactionsRes  extends AbstractRes {
    private ExtTransaction extTransaction;
    private UserBasicInfo user;
    private UserBasicInfo lastUpdateduser;

    public ExtTransaction getExtTransaction() {
        return extTransaction;
    }

    public void setExtTransaction(ExtTransaction extTransaction) {
        this.extTransaction = extTransaction;
    }

    public UserBasicInfo getUser() {
        return user;
    }

    public void setUser(UserBasicInfo user) {
        this.user = user;
    }

    public UserBasicInfo getLastUpdateduser() {
        return lastUpdateduser;
    }

    public void setLastUpdateduser(UserBasicInfo lastUpdateduser) {
        this.lastUpdateduser = lastUpdateduser;
    }
}
