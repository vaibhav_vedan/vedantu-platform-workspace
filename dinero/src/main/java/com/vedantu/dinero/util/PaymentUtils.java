/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.util;

import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.request.SubscriptionVO;
import com.vedantu.util.DateTimeUtils;
import java.util.List;

/**
 *
 * @author ajith
 */
public class PaymentUtils {

    public static Long getRequestedHours(SessionSchedule schedule) {
        if (schedule == null) {
            return 0l;
        }
        Long hours = 0l;
        List<SessionSlot> slots = schedule.getSessionSlots();
        return getRequestedHours(slots, schedule.getNoOfWeeks());
    }

    public static Long getRequestedHours(List<SessionSlot> slots, Long noOfWeeks) {
        Long hours = 0l;
        if (slots == null || slots.isEmpty()) {
            return 0l;
        }
        for (SessionSlot s : slots) {
            hours += (s.getEndTime() - s.getStartTime());
        }
        if (noOfWeeks == null || noOfWeeks < 2) {
            return hours;
        }
        return hours * (noOfWeeks);
    }

    public static SubscriptionVO getSubscriptionRequest(OrderedItem item) {
        Long teacherHourlyRate = item.getRate().longValue();
        if (item.getTeacherDiscountAmount() != null && item.getTeacherDiscountAmount() > 0l) {
            if (item.getHours() != null && item.getHours() > 0) {
                teacherHourlyRate -= (item.getTeacherDiscountAmount().longValue() * DateTimeUtils.MILLIS_PER_HOUR / item.getHours());
            }
        }
        SubscriptionVO subscriptionRequest = new SubscriptionVO(item.getTeacherId(), null,
                item.getEntityId(), item.getOfferingId(), item.getModel(), null, item.getHours(),
                item.getTeacherDiscountCouponId(), Long.parseLong(item.getTeacherDiscountAmount().toString()),
                teacherHourlyRate, item.getBoardId(), item.getTarget(), item.getGrade(), null, null,
                item.getNote(), item.getRenew(), item.getSubModel(), null, item.getSessionSource());
        return subscriptionRequest;
    }
}
