package com.vedantu.dinero.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.dinero.controllers.MiscController;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.redis.AbstractRedisDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.List;

@Service
public class RedisDAO extends AbstractRedisDAO {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private RedisDAO redisDAO;

    private static final Gson gson = new Gson();

    private Logger logger = LogFactory.getLogger(RedisDAO.class);

    public RedisDAO() {
    }

    public void performOps(SNSSubject subject, String message) throws InternalServerErrorException {
        if (StringUtils.isEmpty(message)) {
            logger.warn("message can not be null/empty");
            return;
        }

        switch(subject) {

            case REMOVE_BUNDLE_NAME_KEYS:
                Type listType = new TypeToken<List<String>>(){}.getType();
                List<String> cachedKeys = gson.fromJson(message, listType);
                String[] keys = cachedKeys.toArray(new String[]{});
                logger.info("removing keys for {} -> {} ", subject.name(), cachedKeys);

                deleteKeys(keys);
                break;
            default:
                logger.warn("Received a sub req with an invalid subject");
        }
    }
}