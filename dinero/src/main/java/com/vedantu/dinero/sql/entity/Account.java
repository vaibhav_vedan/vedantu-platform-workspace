package com.vedantu.dinero.sql.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

import com.vedantu.dinero.enums.HolderType;
import com.vedantu.dinero.pojo.LockBalanceInfo;
import com.vedantu.util.StringUtils;

@Entity
@Table(name = "Account")
public class Account extends com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity {
    
        @Version
	private int version;
        
	private String holderId;
	private String holderName;
	private HolderType holderType;
	private Integer balance;
	private Integer lockedBalance;
	private String panCard;
	
	@Column(nullable = false, columnDefinition = "int default 0")
	private Integer promotionalBalance;
	
	@Column(nullable = false, columnDefinition = "int default 0")
	private Integer nonPromotionalBalance;

	@Column(nullable = false, columnDefinition = "int default 0")
	private Integer promotionalLockedBalance;
	
	@Column(nullable = false, columnDefinition = "int default 0")
	private Integer nonPromotionalLockedBalance;
//TODO have comman state check for all updations of Account where NPB+PB == B

	public Account(String holderId, String holderName, HolderType holderType, Integer balance, Integer lockedBalance,
			Integer promotionalBalance, Integer nonPromotionalBalance, Integer promotionalLockedBalance,
			Integer nonPromotionalLockedBalance) {
		super();
		this.holderId = holderId;
		this.holderName = StringUtils.truncateIfNeeded(holderName, 255);
		this.holderType = holderType;
		this.balance = balance;
		this.lockedBalance = lockedBalance;
		this.promotionalBalance = promotionalBalance;
		this.nonPromotionalBalance = nonPromotionalBalance;
		this.promotionalLockedBalance = promotionalLockedBalance;
		this.nonPromotionalLockedBalance = nonPromotionalLockedBalance;
	}

	public Integer getPromotionalLockedBalance() {
		return promotionalLockedBalance;
	}

	public void setPromotionalLockedBalance(Integer promotionalLockedBalance) {
		this.promotionalLockedBalance = promotionalLockedBalance;
	}

	public Integer getNonPromotionalLockedBalance() {
		return nonPromotionalLockedBalance;
	}

	public void setNonPromotionalLockedBalance(Integer nonPromotionalLockedBalance) {
		this.nonPromotionalLockedBalance = nonPromotionalLockedBalance;
	}

	public Integer getPromotionalBalance() {
		return promotionalBalance;
	}

	public void setPromotionalBalance(Integer promotionalBalance) {
		this.promotionalBalance = promotionalBalance;
	}

	public Integer getNonPromotionalBalance() {
		return nonPromotionalBalance;
	}

	public void setNonPromotionalBalance(Integer nonPromotionalBalance) {
		this.nonPromotionalBalance = nonPromotionalBalance;
	}

	public static String __getUserAccountHolderId(String userId) {

		return "user/" + userId;
	}

	public static String __getVedantuAccountHolderId(String id) {

		return "vedantu/" + id;

	}

	public String getHolderId() {
		return holderId;
	}

	public void setHolderId(String holderId) {
		this.holderId = holderId;
	}

	public String getHolderName() {
		return holderName;
	}

	public void setHolderName(String holderName) {
		this.holderName = StringUtils.truncateIfNeeded(holderName, 255);
	}

	public HolderType getHolderType() {
		return holderType;
	}

	public void setHolderType(HolderType holderType) {
		this.holderType = holderType;
	}

	public Integer getBalance() {
		return balance;
	}

	public void setBalance(Integer balance) {
		this.balance = balance;
	}

	public Integer getLockedBalance() {
		return lockedBalance;
	}

	public void setLockedBalance(Integer lockedBalance) {
		this.lockedBalance = lockedBalance;
	}

	public String getPanCard() {
		return panCard;
	}

	public void setPanCard(String panCard) {
		this.panCard = panCard;
	}

	public Account() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Account(Long id, Long creationTime, String createdBy, Long lastUpdatedTime, String lastUpdatedby,
			Boolean enabled, String callingUser) {
		super(id, creationTime, createdBy, lastUpdatedTime, lastUpdatedby, enabled, callingUser);
		// TODO Auto-generated constructor stub
	}


	public Boolean unLockAmount(int promotionalCost, int nonPromotionalCost) {
		if (this.promotionalLockedBalance < promotionalCost || this.nonPromotionalLockedBalance < nonPromotionalCost) {
			return false;
		}
		unlockPromotionalBalance(promotionalCost);
		unlockNonPromotionalBalance(nonPromotionalCost);
		return true;
	}

	public Boolean lockAmount(int cost, LockBalanceInfo res) {
		if (cost > this.balance) {
			return false;
		}

		int promotionalCost = 0;
		int nonPromotionalCost = 0;
		if (cost < this.promotionalBalance) {
			promotionalCost = cost;
		} else {
			promotionalCost = promotionalBalance;
			nonPromotionalCost = cost - promotionalCost;
		}

		lockAmount(promotionalCost, nonPromotionalCost);
		res.setPromotionalCost(promotionalCost);
		res.setNonPromotionalCost(nonPromotionalCost);
		return true;
	}

	public void lockAmount(int promotionalCost, int nonPromotionalCost) {
		lockPromotionalBalance(promotionalCost);
		lockNonPromotionalBalance(nonPromotionalCost);
	}

	public void deductPromotionalBalance(int cost) {
		this.promotionalBalance -= cost;
		this.balance -= cost;
	}

	public void lockPromotionalBalance(int cost) {
		this.promotionalLockedBalance += cost;
		this.lockedBalance += cost;
		deductPromotionalBalance(cost);
	}

	public void deductNonPromotionalBalance(int cost) {
		this.nonPromotionalBalance -= cost;
		this.balance -= cost;
	}

	public void lockNonPromotionalBalance(int cost) {
		this.nonPromotionalLockedBalance += cost;
		this.lockedBalance += cost;
		deductNonPromotionalBalance(cost);
	}

	public void deductPromotionalLockedBalance(int cost) {
		this.promotionalLockedBalance -= cost;
		this.lockedBalance -= cost;
	}

	public void unlockPromotionalBalance(int cost) {
		this.promotionalBalance += cost;
		this.balance += cost;
		deductPromotionalLockedBalance(cost);
	}

	public void deductNonPromotionalLockedBalance(int cost) {
		this.nonPromotionalLockedBalance -= cost;
		this.lockedBalance -= cost;
	}

	public void unlockNonPromotionalBalance(int cost) {
		this.nonPromotionalBalance += cost;
		this.balance += cost;
		deductNonPromotionalLockedBalance(cost);
	}

	@Override
	public String toString() {
		return "Account [version=" + version + ", holderId=" + holderId + ", holderName=" + holderName + ", holderType="
				+ holderType + ", balance=" + balance + ", lockedBalance=" + lockedBalance + ", panCard=" + panCard
				+ ", promotionalBalance=" + promotionalBalance + ", nonPromotionalBalance=" + nonPromotionalBalance
				+ ", promotionalLockedBalance=" + promotionalLockedBalance + ", nonPromotionalLockedBalance="
				+ nonPromotionalLockedBalance + ", toString()=" + super.toString() + "]";
	}
}
