//package com.vedantu.dinero.sql.dao;
//
//import java.util.List;
//
//import org.apache.logging.log4j.Logger;
//import org.hibernate.Session;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.vedantu.dinero.sql.entity.Payment;
//import com.vedantu.util.LogFactory;
//
//@Service
//public class PaymentDAO extends AbstractSqlDAO {
//	
//	@Autowired
//	private LogFactory logFactory;
//	
//	@SuppressWarnings("static-access")
//	private Logger logger = logFactory.getLogger(PaymentDAO.class);
//
//	public PaymentDAO() {
//		super();
//	}
//
//	public void create(Payment payment, Session session) {
//		try {
//			if (payment != null) {
//				saveEntity(payment, session);
//			}
//		} catch (Exception ex) {
//			logger.error("Create Payment: "+ex.getMessage());
//		}
//	}
//
//	public Payment getById(Long id, Boolean enabled, Session session) {
//		Payment payment = null;
//		try {
//			if (id != null) {
//				payment = (Payment) getEntityById(id, enabled, Payment.class, session);
//			}
//		} catch (Exception ex) {
//			logger.error("getById Payment: "+ex.getMessage());
//			payment = null;
//		}
//		return payment;
//	}
//
//	public void update(Payment p, Session session) {
//		try {
//			if (p != null) {
//				updateEntity(p, session);
//			}
//		} catch (Exception ex) {
//			logger.error("update Payment: "+ex.getMessage());
//		}
//	}
//
//	public void updateAll(List<Payment> p, Session session) {
//		try {
//			if (p != null) {
//				updateAllEntities(p, Payment.class.getSimpleName(), session);
//			}
//		} catch (Exception ex) {
//			logger.error("updateAll Payment: "+ex.getMessage());
//		}
//	}
//}
