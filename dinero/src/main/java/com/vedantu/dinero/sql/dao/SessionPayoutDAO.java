package com.vedantu.dinero.sql.dao;

import java.util.List;

import com.vedantu.dinero.sessionfactory.SqlSessionFactory;
import com.vedantu.util.dbutils.*;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.dinero.sql.entity.SessionPayout;
import com.vedantu.util.LogFactory;
import java.util.ArrayList;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

@Service
public class SessionPayoutDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {
	
	@Autowired
	private LogFactory logFactory;


	@Autowired
	SqlSessionFactory sqlSessionFactory;


	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SessionPayoutDAO.class);
        
        private static final int IN_LIMIT = 200;

	public SessionPayoutDAO() {
		super();
	}

	public void create(SessionPayout SessionPayout, Session session) {
		create(SessionPayout, session, null);
	}

	public void create(SessionPayout SessionPayout, Session session, String callingUserId) {
		try {
			if (SessionPayout != null) {
				saveEntity(SessionPayout, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create SessionPayout: "+ex.getMessage());
		}
	}

	public SessionPayout getById(Long id, Boolean enabled, Session session) {
		SessionPayout SessionPayout = null;
		try {
			if (id != null) {
				SessionPayout = (SessionPayout) getEntityById(id, enabled, SessionPayout.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById SessionPayout: "+ex.getMessage());
			SessionPayout = null;
		}
		return SessionPayout;
	}

	public void update(SessionPayout p, Session session) {
		update(p, session, null);
	}

	public void update(SessionPayout p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateEntity(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("update SessionPayout: "+ex.getMessage());
		}
	}


	public void updateAll(List<SessionPayout> p, Session session) {
		updateAll(p, session, null);
	}

	public void updateAll(List<SessionPayout> p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateAllEntities(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("updateAll SessionPayout: "+ex.getMessage());
		}
	}
        
        
        
        public List<SessionPayout> getSessionPayouts(List<Long> sessionList) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<SessionPayout> sessionPayouts = new ArrayList<>();
                int listSize = sessionList.size();
		try {

                    for (int i = 0; i < listSize; i += IN_LIMIT) {
                        List subList;
                        if (listSize > i + IN_LIMIT) {
                            subList = sessionList.subList(i, (i + IN_LIMIT));
                        } else {
                            subList = sessionList.subList(i, listSize);
                        }
                        Criteria cr = session.createCriteria(SessionPayout.class);
                        cr.add(Restrictions.in("sessionId", subList));
                        sessionPayouts.addAll(runQuery(session, cr, SessionPayout.class));
                    }
                    
		} catch (Exception e) {
			throw e;
		} finally {
			session.close();
		}
		return sessionPayouts;
	}
}
