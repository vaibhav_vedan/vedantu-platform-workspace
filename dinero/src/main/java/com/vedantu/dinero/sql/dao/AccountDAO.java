package com.vedantu.dinero.sql.dao;

import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.vedantu.dinero.enums.HolderType;
import com.vedantu.dinero.managers.aws.AwsSNSManager;
import com.vedantu.dinero.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.util.RedisDAO;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.util.ArrayList;
import java.util.Map;

import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

@Service
public class AccountDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(AccountDAO.class);

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Autowired
	private AwsSNSManager awsSNSManager;

	@Autowired
	private RedisDAO redisDAO;

	private static final String USER_DINERO_ACCOUNT_CREATED_KEY = "USER_DINERO_ACCOUNT_CREATED_";

	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}

	private static String vedantuFreebiesHolderId;
	private static String vedantuCutHolderId;
	private static String vedantuTechHolderId;
	private static String vedantuDefaultHolderId;
	private static String vedantuAIRHolderId;

	public AccountDAO() {
		vedantuFreebiesHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuFreebiesHolderId");
		vedantuCutHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuCutHolderId");
		vedantuTechHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuTechHolderId");
		vedantuDefaultHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuDefaultHolderId");
		vedantuAIRHolderId = ConfigUtils.INSTANCE.getStringValue("account.vedantuAIRHolderId");
	}

        //use this method just to create account, not to transfer money
        public Account createNewAccount(String holderId, String name, HolderType holderType) throws InternalServerErrorException {
            // redis check to avoid race condition
			String key = USER_DINERO_ACCOUNT_CREATED_KEY + holderId;
			boolean isAccountCreated = checkIfAccountCreated(key);
			if(isAccountCreated){
				logger.warn("user dinero account already created for user" + holderId);
				// return a account object as the account was just created
				return getAccountByHolderId(holderId);
			}

			// check if account already exists
			Account account = new Account(holderId, name, holderType, 0, 0, 0, 0, 0, 0);
            try {
                if (account != null) {
                    saveEntity(account, "");
                }
            } catch (Exception ex) {
                logger.error("Create Account: " + ex.getMessage());
            }
            return account;
        }

	private boolean checkIfAccountCreated(String key) {
		return !redisDAO.setnx(key, "1", 10);
	}


	public Account getById(Long id, Boolean enabled, Session session) {
		Account Account = null;
		try {
			if (id != null) {
				Account = (Account) getEntityById(id, enabled, Account.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById Account: " + ex.getMessage());
			Account = null;
		}
		return Account;
	}

	public Account getById(Long id, Boolean enabled) {
		Account Account = null;
		try {
			if (id != null) {
				Account = (Account) getEntityById(id, enabled, Account.class);
			}
		} catch (Exception ex) {
			logger.error("getById Account: " + ex.getMessage());
			Account = null;
		}
		return Account;
	}

	public void updateWithoutSession(Account p, String callingUserId) {                
            try {
                if (p != null) {
                    Boolean isVedantuAccount = p.getHolderType().equals(HolderType.VEDANTU);
                    if(!isVedantuAccount){
                        updateEntity(p, callingUserId);
                    }else{
                        logger.info("not update vedantu account " + p);
                    }
                }
            } catch (Exception ex) {
                logger.error("update Account: " + ex.getMessage());
            }
	}
        
        
	public void update(Account p, Session session) {
		update(p, session, null);
	}

	public void update(Account p, Session session, String callingUserId) {
            try {
                if (p != null) {
                    Boolean isVedantuAccount = p.getHolderType().equals(HolderType.VEDANTU);
                    if (!isVedantuAccount) {
                        updateEntity(p, session, callingUserId);
                    } else {
                        logger.info("not update vedantu account " + p);
                    }
                }
            } catch (Exception ex) {
                logger.error("update Account: " + ex.getMessage());
            }
	}

	public Account getAccountByUserId(Long userId) throws InternalServerErrorException, BadRequestException {

		logger.info("userId: " + userId);

		if (userId == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userid not sent");
		}

		Account account = getAccountByHolderId(Account.__getUserAccountHolderId(userId.toString()));

		logger.info(account);
		return account;
	}

	public Account getAccountByHolderId(String holderId) throws InternalServerErrorException {

		logger.info("holderId: " + holderId);

		Account account = null;

		logger.info(sqlSessionFactory);

		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(Account.class);
			cr.add(Restrictions.eq("holderId", holderId));
			List<Account> accounts = runQuery(session, cr, Account.class);
			if (accounts.size() > 0) {
				account = accounts.get(0);
			}
			if (accounts.size() > 1) {
				logger.error("multiple accounts found for " + holderId);
			}
		} catch (Exception e) {
			logger.error(e);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
		} finally {
			session.close();
		}

		logger.info(account);
		return account;
	}

	public Account updateAccountPancard(Long userId, String pancard) throws InternalServerErrorException {

		logger.info("userId: " + userId + "pancard:" + pancard);

		Account account = null;
		String holderId = Account.__getUserAccountHolderId(String.valueOf(userId));

		logger.info(sqlSessionFactory);

		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		try {
			transaction.begin();
			Criteria cr = session.createCriteria(Account.class);
			cr.add(Restrictions.eq("holderId", holderId));
			List<Account> accounts = runQuery(session, cr, Account.class);
			if (accounts.size() == 0) {
				logger.error("No account found for id : " + userId);
			}
			if (accounts.size() > 0) {
				account = accounts.get(0);
			}
			if (accounts.size() > 1) {
				logger.error("multiple accounts found for " + holderId);
			}

			// Update pan card
			logger.info("pre update : " + account.toString());
			account.setPanCard(pancard);
			update(account, session);
			transaction.commit();
			logger.info("Updated");
		} catch (Exception e) {
			logger.error(e);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
		} finally {
			session.close();
		}

		logger.info(account);
		return account;
	}

	public List<Account> getAccountListByHolderIds(List<String> holderIds) throws InternalServerErrorException {

		logger.info("holderIds: " + holderIds);

		List<Account> accounts = new ArrayList<>();
		if (holderIds.size() > 0) {
			SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
			Session session = sessionFactory.openSession();
			try {
				Criteria cr = session.createCriteria(Account.class);
				cr.add(Restrictions.in("holderId", holderIds));
				accounts = runQuery(session, cr, Account.class);
			} catch (Exception e) {
				logger.error(e);
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
			} finally {
				session.close();
			}
		}

		logger.info(accounts);
		return accounts;
	}

	public List<Account> getByIds(List<Long> ids) throws InternalServerErrorException {

		logger.info("ids: " + ids.size());

		List<Account> accounts = new ArrayList<>();
		if (ids.size() > 0) {
			SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
			Session session = sessionFactory.openSession();
			try {
				Criteria cr = session.createCriteria(Account.class);
				cr.add(Restrictions.in("id", ids));
				accounts = runQuery(session, cr, Account.class);
			} catch (Exception e) {
				logger.error(e);
				throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
			} finally {
				session.close();
			}
		}

		logger.info(accounts.size());
		return accounts;
	}

	public Account getVedantuFreebiesAccount() throws InternalServerErrorException, NotFoundException {

		logger.info("Entering");

		String holderId = Account.__getVedantuAccountHolderId(vedantuFreebiesHolderId);

		Account account = getAccountByHolderId(holderId);

		if (account == null)
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Freebies Account Not Found");

		logger.info(account);
		return account;
	}

	public Account getVedantuDefaultAccount() throws InternalServerErrorException, NotFoundException {

		logger.info("Entering");

		String holderId = Account.__getVedantuAccountHolderId(vedantuDefaultHolderId);

		Account account = getAccountByHolderId(holderId);

		if (account == null)
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Freebies Account Not Found");

		logger.info(account);
		return account;
	}

	public Account getVedantuTechAccount() throws InternalServerErrorException, NotFoundException {

		logger.info("Entering");

		String holderId = Account.__getVedantuAccountHolderId(vedantuTechHolderId);

		Account account = getAccountByHolderId(holderId);

		if (account == null)
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Freebies Account Not Found");

		logger.info(account);
		return account;
	}

	public Account getVedantuCutAccount() throws InternalServerErrorException, NotFoundException {

		logger.info("Entering");

		String holderId = Account.__getVedantuAccountHolderId(vedantuCutHolderId);

		Account account = getAccountByHolderId(holderId);

		if (account == null)
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Freebies Account Not Found");

		logger.info(account);
		return account;
	}

	public Account getVedantuAIRAccount() throws InternalServerErrorException, NotFoundException {

		logger.info("Entering");

		String holderId = Account.__getVedantuAccountHolderId(vedantuAIRHolderId);

		Account account = getAccountByHolderId(holderId);

		if (account == null)
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "AIR Account Not Found");

		logger.info(account);
		return account;
	}              
        
        public boolean transferBackForNegativeMargin(int negativeMarginAmt, int promotionalAmount,
                int nonPromotionalAmount, boolean allowNegativeBalance) throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException{
                logger.info("Entering fromAccount: VedantuDefault, VedantuMarketing toAccount: VedantuCut, promotionalAmount: "+promotionalAmount+
                        ", nonPromotionalAmount: " + nonPromotionalAmount + ", negativeMargin: "+negativeMarginAmt+", allowedNegativeBalance: "+allowNegativeBalance);
                SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
                
                Account defaultAccount = getVedantuDefaultAccount();
                Account marketingWallet = getVedantuFreebiesAccount();
                Account cutAccount = getVedantuCutAccount();
                int NUMBER_OF_TRIES=5;
                for(int retry=0; retry<NUMBER_OF_TRIES; retry++){
                    
                    Session session = sessionFactory.openSession();
                    Transaction transaction = session.getTransaction();
                    
                    try{
                        transaction.begin();
                        logger.info("try: "+retry);
                        
                        if(retry!=0){
                            defaultAccount = getById(defaultAccount.getId(), true);
                            marketingWallet = getById(marketingWallet.getId(), true);
                            cutAccount = getById(cutAccount.getId(), true);
                        }
                        
                        
                        if(!allowNegativeBalance && defaultAccount.getBalance() < (promotionalAmount+nonPromotionalAmount)){
                            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
                        }
                        
                        if(!allowNegativeBalance && marketingWallet.getBalance() < (negativeMarginAmt)){
                            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
                        }
                        
                        
                        defaultAccount.setBalance(defaultAccount.getBalance() - (promotionalAmount + nonPromotionalAmount + negativeMarginAmt));
                        defaultAccount.setPromotionalBalance(defaultAccount.getPromotionalBalance() - (promotionalAmount + negativeMarginAmt));
                        defaultAccount.setNonPromotionalBalance(defaultAccount.getNonPromotionalBalance() - nonPromotionalAmount);
                        
                        marketingWallet.setBalance(marketingWallet.getBalance() + negativeMarginAmt);
                        marketingWallet.setPromotionalBalance(marketingWallet.getPromotionalBalance() + negativeMarginAmt);
                        
                        cutAccount.setBalance(cutAccount.getBalance() + promotionalAmount + nonPromotionalAmount);
                        cutAccount.setPromotionalBalance(cutAccount.getPromotionalBalance() + promotionalAmount);
                        cutAccount.setNonPromotionalBalance(cutAccount.getNonPromotionalBalance() + nonPromotionalAmount);
                        update(cutAccount, session, null);
                        update(defaultAccount, session, null);
                        update(marketingWallet, session, null);
                        transaction.commit();
                        break;
                    }catch (Exception e){
                        logger.info("Exception: transferForNegativeMargin: "+e.getMessage());
                        if (e instanceof ConflictException) {
                                throw e;
                        }
                        if (retry == (NUMBER_OF_TRIES - 1)) {
                                logger.error(e);
                        } else {
                                logger.warn(e);
                        }
                        if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                                logger.info("transfer Rollback: " + e.getMessage());
                                session.getTransaction().rollback();
                        }
                        if (retry == (NUMBER_OF_TRIES - 1)) {
                                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
                        }                        
                    } finally {
                        session.close();
                    }
                    
                }
                logger.info("Exiting");
                return true;
                
        }        
        
        
        public boolean transferForNegativeMargin(int negativeMarginAmt, int promotionalAmount,
                int nonPromotionalAmount, boolean allowNegativeBalance) throws BadRequestException, ConflictException, InternalServerErrorException, NotFoundException{
                logger.info("Entering fromAccount: VedantuDefault, VedantuMarketing toAccount: VedantuCut, promotionalAmount: "+promotionalAmount+
                        ", nonPromotionalAmount: " + nonPromotionalAmount + ", negativeMargin: "+negativeMarginAmt+", allowedNegativeBalance: "+allowNegativeBalance);
                SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
                
                Account defaultAccount = getVedantuDefaultAccount();
                Account marketingWallet = getVedantuFreebiesAccount();
                Account cutAccount = getVedantuCutAccount();
                int NUMBER_OF_TRIES=5;
                for(int retry=0; retry<NUMBER_OF_TRIES; retry++){
                    
                    Session session = sessionFactory.openSession();
                    Transaction transaction = session.getTransaction();
                    
                    try{
                        transaction.begin();
                        logger.info("try: "+retry);
                        
                        if(retry!=0){
                            defaultAccount = getById(defaultAccount.getId(), true);
                            marketingWallet = getById(marketingWallet.getId(), true);
                            cutAccount = getById(cutAccount.getId(), true);
                        }
                        
                        
                        if(!allowNegativeBalance && defaultAccount.getBalance() < (promotionalAmount+nonPromotionalAmount)){
                            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
                        }
                        
                        if(!allowNegativeBalance && marketingWallet.getBalance() < (negativeMarginAmt)){
                            throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
                        }
                        
                        
                        defaultAccount.setBalance(defaultAccount.getBalance() - (promotionalAmount + nonPromotionalAmount));
                        defaultAccount.setPromotionalBalance(defaultAccount.getPromotionalBalance() - promotionalAmount);
                        defaultAccount.setNonPromotionalBalance(defaultAccount.getNonPromotionalBalance() - nonPromotionalAmount);
                        
                        marketingWallet.setBalance(marketingWallet.getBalance() - negativeMarginAmt);
                        marketingWallet.setPromotionalBalance(marketingWallet.getPromotionalBalance() - negativeMarginAmt);
                        
                        cutAccount.setBalance(cutAccount.getBalance() + promotionalAmount + nonPromotionalAmount + negativeMarginAmt);
                        cutAccount.setPromotionalBalance(cutAccount.getPromotionalBalance() + promotionalAmount + negativeMarginAmt);
                        cutAccount.setNonPromotionalBalance(cutAccount.getNonPromotionalBalance() + nonPromotionalAmount);
                        update(defaultAccount, session, null);
                        update(cutAccount, session, null);
                        update(marketingWallet, session, null);
                        transaction.commit();
                        break;
                    }catch (Exception e){
                        logger.info("Exception: transferForNegativeMargin: "+e.getMessage());
                        if (e instanceof ConflictException) {
                                throw e;
                        }
                        if (retry == (NUMBER_OF_TRIES - 1)) {
                                logger.error(e);
                        } else {
                                logger.warn(e);
                        }
                        if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                                logger.info("transfer Rollback: " + e.getMessage());
                                session.getTransaction().rollback();
                        }
                        if (retry == (NUMBER_OF_TRIES - 1)) {
                                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
                        }                        
                    } finally {
                        session.close();
                    }
                    
                }
                logger.info("Exiting");
                return true;
                
        }   
        
        

	public boolean transfer(Account fromAccount, Account toAccount, int amount, int promotionalAmount,
			int nonPromotionalAmount, boolean allowedNegativeBalance)
			throws BadRequestException, ConflictException, InternalServerErrorException {

		logger.info("Entering fromAccount: " + fromAccount + ", toAccount: " + toAccount + ", amount: " + amount
				+ ", promotionalAmount: " + promotionalAmount + ", nonPromotionalAmount: " + nonPromotionalAmount
				+ ", allowedNegativeBalance: " + allowedNegativeBalance);

		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		// Session session = sessionFactory.openSession();
		int NUMBER_OF_TRIES = 5;
		for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
			// if(!session.isOpen()) {
			// session = sessionFactory.openSession();
			// }
			Session session = sessionFactory.openSession();
			Transaction transaction = session.getTransaction();
			try {
				transaction.begin();
				logger.info("try :" + retry);

				if (retry != 0) {
					fromAccount = getById(fromAccount.getId(), true);
					toAccount = getById(toAccount.getId(), true);
				}

				// TODO we should also check for promotional and non-promotional balance
				// availability
				if (!allowedNegativeBalance && fromAccount.getBalance() < amount) {
					throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
				}

				fromAccount.setBalance(fromAccount.getBalance() - amount);
				fromAccount.setPromotionalBalance(fromAccount.getPromotionalBalance() - promotionalAmount);
				fromAccount.setNonPromotionalBalance(fromAccount.getNonPromotionalBalance() - nonPromotionalAmount);

				toAccount.setBalance(toAccount.getBalance() + amount);
				toAccount.setPromotionalBalance(toAccount.getPromotionalBalance() + promotionalAmount);
				toAccount.setNonPromotionalBalance(toAccount.getNonPromotionalBalance() + nonPromotionalAmount);

                                update(fromAccount, session, null);
                                update(toAccount, session, null);
				transaction.commit();
                                if (fromAccount.getHolderType() != HolderType.VEDANTU && StringUtils.isNotEmpty(fromAccount.getHolderId())) {
                                    sendSNSUpdateLeadPayment(fromAccount.getHolderId(), fromAccount.getBalance());
                                } else if (toAccount.getHolderType() != HolderType.VEDANTU && StringUtils.isNotEmpty(toAccount.getHolderId())) {
                                    sendSNSUpdateLeadPayment(toAccount.getHolderId(), toAccount.getBalance());
                                }
                                break;
			} catch (Exception e) {
				if (e instanceof ConflictException) {
					throw e;
				}
				if (retry == (NUMBER_OF_TRIES - 1)) {
					logger.error(e);
				} else {
					logger.warn(e);
				}
				if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
					logger.info("transfer Rollback: " + e.getMessage());
					session.getTransaction().rollback();
				}
				if (retry == (NUMBER_OF_TRIES - 1)) {
					throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
				}
			} finally {
				session.close();
			}
		}

		logger.info("Exiting");
		return true;

	}
        
	public boolean transfer(Account fromAccount, Account toAccount, int amount, int promotionalAmount,
			int nonPromotionalAmount, boolean allowedNegativeBalance, Session session)
			throws BadRequestException, ConflictException, InternalServerErrorException {

            logger.info("Entering fromAccount: " + fromAccount + ", toAccount: " + toAccount + ", amount: " + amount
                            + ", promotionalAmount: " + promotionalAmount + ", nonPromotionalAmount: " + nonPromotionalAmount
                            + ", allowedNegativeBalance: " + allowedNegativeBalance);


            // TODO we should also check for promotional and non-promotional balance
            // availability
            if (!allowedNegativeBalance && fromAccount.getBalance() < amount) {
                throw new ConflictException(ErrorCode.ACCOUNT_INSUFFICIENT_BALANCE, "Insufficient account balance");
            }

            fromAccount.setBalance(fromAccount.getBalance() - amount);
            fromAccount.setPromotionalBalance(fromAccount.getPromotionalBalance() - promotionalAmount);
            fromAccount.setNonPromotionalBalance(fromAccount.getNonPromotionalBalance() - nonPromotionalAmount);

            toAccount.setBalance(toAccount.getBalance() + amount);
            toAccount.setPromotionalBalance(toAccount.getPromotionalBalance() + promotionalAmount);
            toAccount.setNonPromotionalBalance(toAccount.getNonPromotionalBalance() + nonPromotionalAmount);

            update(fromAccount, session, null);
            
            update(toAccount, session, null);

            if (fromAccount.getHolderType() != HolderType.VEDANTU && StringUtils.isNotEmpty(fromAccount.getHolderId())) {
                sendSNSUpdateLeadPayment(fromAccount.getHolderId(), fromAccount.getBalance());
            } else if (toAccount.getHolderType() != HolderType.VEDANTU && StringUtils.isNotEmpty(toAccount.getHolderId())) {
                sendSNSUpdateLeadPayment(toAccount.getHolderId(), toAccount.getBalance());
            }

            return true;

	}
	public void sendSNSUpdateLeadPayment(String userId,Integer amount){
        Map<String,String> payload = new HashMap<>();
        if(StringUtils.isNotEmpty(userId) && amount != null){
			payload.put("userId",userId.replace("user/",""));
			payload.put("amount",String.valueOf(amount));
			logger.info("calling sns for sendSNSUpdateLeadPayment with : "+payload.toString());
			awsSNSManager.triggerSNS(SNSTopic.UPDATE_LEAD_PAYMENT_LS,SNSTopic.UPDATE_LEAD_PAYMENT_LS.name(),new Gson().toJson(payload));
		}
    }

        

}
