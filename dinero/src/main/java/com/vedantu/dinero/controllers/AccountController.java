package com.vedantu.dinero.controllers;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.HolderType;
import com.vedantu.dinero.enums.TransactionStatus;
import com.vedantu.dinero.managers.AccountManager;
import com.vedantu.dinero.pojo.LockBalanceInfo;
import com.vedantu.dinero.pojo.LockBalanceRes;
import com.vedantu.dinero.request.AccountVerificationSMSRequest;
import com.vedantu.dinero.request.AddAccountPancardReq;
import com.vedantu.dinero.request.AddOrDeductAccountBalanceReq;
import com.vedantu.dinero.request.GetAccountInfoReq;
import com.vedantu.dinero.request.GetAllExtTransactionsReq;
import com.vedantu.dinero.request.GetFreebiesAccountInfoReq;
import com.vedantu.dinero.request.GetRechargeHistoryReq;
import com.vedantu.dinero.request.LockBalanceReq;
import com.vedantu.dinero.request.OTMConsumptionReq;
import com.vedantu.dinero.request.OTMRefundAdjustmentReq;
import com.vedantu.dinero.request.OTMRefundReq;
import com.vedantu.dinero.request.RefundMoneyToStudentWalletReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.request.RefundStudentWalletRequest;
import com.vedantu.dinero.request.ReleaseLockedBalanceReq;
import com.vedantu.dinero.request.SalesTransactionReq;
import com.vedantu.dinero.request.SetPaymentGatewayNameReq;
import com.vedantu.dinero.request.SubmitCashChequeReq;
import com.vedantu.dinero.request.SwitchAmountOneAccountToAnotherReq;
import com.vedantu.dinero.request.TransferFromFreebiesAccountReq;
import com.vedantu.dinero.request.TransferToVedantuAccountReq;
import com.vedantu.dinero.request.TransferToVedantuDefaultAccountReq;
import com.vedantu.dinero.response.GetAccountBalanceResponse;
import com.vedantu.dinero.response.GetAccountInfoRes;
import com.vedantu.dinero.response.GetAllExtTransactionsRes;
import com.vedantu.dinero.response.GetFreebiesAccountInfoRes;
import com.vedantu.dinero.response.GetRechargeHistoryRes;
import com.vedantu.dinero.response.GetUserDashboardAccountInfoRes;
import com.vedantu.dinero.response.OTMConsumptionPaymentRes;
import com.vedantu.dinero.response.OTMRefundAdjustmentRes;
import com.vedantu.dinero.response.PaymentGatewayNameRes;
import com.vedantu.dinero.response.SalesTransactionInfo;
import com.vedantu.dinero.response.SubmitCashChequeRes;
import com.vedantu.dinero.response.TransferFromFreebiesAccountRes;
import com.vedantu.dinero.response.TransferToVedantuDefaultAccountRes;
import com.vedantu.dinero.sql.entity.Account;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/account")
public class AccountController {

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AccountController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/getRechargeHistory", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetRechargeHistoryRes getRechargeHistory(@RequestBody GetRechargeHistoryReq getRechargeHistoryReq,HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getRechargeHistory(getRechargeHistoryReq);
    }

    @RequestMapping(value = "/v2/getRechargeHistory", method = RequestMethod.GET)
    @ResponseBody
    public GetRechargeHistoryRes getRechargeHistory_v2(GetRechargeHistoryReq getRechargeHistoryReq) throws VException {
        getRechargeHistoryReq.verify();
        sessionUtils.checkIfAllowed(getRechargeHistoryReq.getUserId(), null, Boolean.FALSE);
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (httpSessionData.getRole().equals(Role.STUDENT)) {
            getRechargeHistoryReq.setUserId(httpSessionData.getUserId());
        }
        return accountManager.getRechargeHistory(getRechargeHistoryReq);
    }

    // @RequestMapping(value = "/rechargeFreebies", method = RequestMethod.POST,
    // consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public RechargeFreebiesAccountRes rechargeFreebies(
    // @RequestBody RechargeFreebiesAccountReq rechargeFreebiesAccountReq) throws
    // VException {
    // return accountManager.rechargeFreebies(rechargeFreebiesAccountReq);
    // }
    @RequestMapping(value = "/transferFromFreebiesAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TransferFromFreebiesAccountRes transferFromFreebiesAccount(
            @RequestBody TransferFromFreebiesAccountReq transferFromFreebiesAccountReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.transferFromFreebiesAccount(transferFromFreebiesAccountReq);
    }

    @RequestMapping(value = "/submitCashCheque", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitCashChequeRes submitCashCheque(@RequestBody SubmitCashChequeReq submitCashChequeReq,HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.submitCashCheque(submitCashChequeReq);
    }

    @RequestMapping(value = "/v2/submitCashCheque", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubmitCashChequeRes submitCashCheque_v2(@RequestBody SubmitCashChequeReq submitCashChequeReq)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return accountManager.submitCashCheque(submitCashChequeReq);

    }

    @RequestMapping(value = "/makeOTMConsumptionPayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTMConsumptionPaymentRes makeOTMConsumptionPayment(@RequestBody OTMConsumptionReq otmConsumptionReq,HttpServletRequest httpServletRequest) throws VException{
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.OTMConsumptionPayment(otmConsumptionReq);
    }
    
    @RequestMapping(value = "/makeOTMRefund", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTMConsumptionPaymentRes makeOTMRefundPayment(@RequestBody OTMRefundReq oTMRefundReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.OTMRefundPayment(oTMRefundReq);
    }
    // @RequestMapping(value = "/transferFromVedantuDefaultAccount", method =
    // RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces =
    // MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public TransferFromVedantuDefaultAccountRes
    // transferFromVedantuDefaultAccount(
    // @RequestBody TransferFromVedantuDefaultAccountReq
    // transferFromVedantuDefaultAccountReq) throws VException {
    // return
    // accountManager.transferFromVedantuDefaultAccount(transferFromVedantuDefaultAccountReq);
    // }
    @RequestMapping(value = "/getAccountInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAccountInfoRes getAccountInfo(@RequestBody GetAccountInfoReq getAccountInfoReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getAccountInfo(getAccountInfoReq);
//        return new GetAccountInfoRes();
    }

    @RequestMapping(value = "/v2/getAccountInfo", method = RequestMethod.GET)
    @ResponseBody
    public GetAccountInfoRes getAccountInfo_v2(GetAccountInfoReq getAccountInfoReq) throws VException {
        sessionUtils.checkIfAllowed(getAccountInfoReq.getUserId(), null, Boolean.TRUE);
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (httpSessionData.getRole().equals(Role.STUDENT)) {
            getAccountInfoReq.setUserId(httpSessionData.getUserId());
        }
        return accountManager.getAccountInfo(getAccountInfoReq);
    }

    @RequestMapping(value = "/releaseLockedBalance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAccountInfoRes releaseLockedBalance(@RequestBody ReleaseLockedBalanceReq releaseLockedBalanceReq, HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED, "Not allowed this anymore. Please talk to tech team");
    }

    @RequestMapping(value = "/v2/releaseLockedBalance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAccountInfoRes releaseLockedBalance_v2(@RequestBody ReleaseLockedBalanceReq releaseLockedBalanceReq)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED, "Not allowed this anymore. Please talk to tech team");
    }

    @RequestMapping(value = "/getFreebiesAccountInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetFreebiesAccountInfoRes getFreebiesAccountInfo(
            @RequestBody GetFreebiesAccountInfoReq getFreebiesAccountInfoReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getFreebiesAccountInfo(getFreebiesAccountInfoReq);
    }

    @RequestMapping(value = "/v2/getFreebiesAccountInfo", method = RequestMethod.GET)
    @ResponseBody
    public GetFreebiesAccountInfoRes getFreebiesAccountInfo_v2(GetFreebiesAccountInfoReq getFreebiesAccountInfoReq)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return accountManager.getFreebiesAccountInfo(getFreebiesAccountInfoReq);
    }

    @RequestMapping(value = "/transferToVedantuDefaultAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TransferToVedantuDefaultAccountRes transferToVedantuDefaultAccount(
            @RequestBody TransferToVedantuDefaultAccountReq transferToVedantuDefaultAccountReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.transferToVedantuDefaultAccount(transferToVedantuDefaultAccountReq);
    }

    @RequestMapping(value = "/v2/transferToVedantuDefaultAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TransferToVedantuDefaultAccountRes transferToVedantuDefaultAccount_v2(
            @RequestBody TransferToVedantuDefaultAccountReq transferToVedantuDefaultAccountReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return accountManager.transferToVedantuDefaultAccount(transferToVedantuDefaultAccountReq);
    }

    @RequestMapping(value = "/transferToVedantuAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TransferToVedantuDefaultAccountRes transferToVedantuAccount(
            @RequestBody TransferToVedantuAccountReq transferToVedantuAccountReq, HttpServletRequest httpServletRequest) throws VException {
        logger.info("Request " + transferToVedantuAccountReq.toString());
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.transferToVedantuAccount(transferToVedantuAccountReq);
    }

    @RequestMapping(value = "/v2/transferToVedantuAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String transferToVedantuAccount_v2(@RequestBody String request) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        TransferToVedantuAccountReq req = new Gson().fromJson(request, TransferToVedantuAccountReq.class);
        TransferToVedantuDefaultAccountRes resp = accountManager.transferToVedantuAccount(req);
        return new Gson().toJson(resp);
    }

    @RequestMapping(value = "/createUserAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Account createUserAccount(@RequestBody User user, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        UserBasicInfo userBasicInfo = new UserBasicInfo(user, true);
        return accountManager.createUserAccount(userBasicInfo);
    }

    @RequestMapping(value = "/createVedantuAccounts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean createVedantuAccounts() throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        return accountManager.createVedantuAccounts();
    }

    @RequestMapping(value = "/getAccountBalance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAccountBalanceResponse getAccountBalance(@RequestParam(value = "userId", required = true) String userId,
            @RequestParam(value = "holderType", required = true) HolderType holderType, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getAccountBalance(userId, holderType);
    }

    @RequestMapping(value = "/v2/getAccountBalance", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetAccountBalanceResponse getAccountBalance_v2(@RequestParam(value = "userId", required = true) String userId,
                                                       @RequestParam(value = "holderType", required = true) HolderType holderType) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return accountManager.getAccountBalance(userId, holderType);
    }

    @RequestMapping(value = "/getAccountById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Account getAccountById(@RequestParam(value = "id", required = true) String id,HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getAccountById(id);
    }

    @RequestMapping(value = "/addOrDeductAccountBalance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AddOrDeductAccountBalanceReq addOrDeductAccountBalance(
            @RequestBody AddOrDeductAccountBalanceReq addOrDeductAccountBalanceReq,HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.addOrDeductAccountBalance(addOrDeductAccountBalanceReq);
    }

    @RequestMapping(value = "/v2/addOrDeductAccountBalance", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AddOrDeductAccountBalanceReq addOrDeductAccountBalance_v2(
            @RequestBody AddOrDeductAccountBalanceReq addOrDeductAccountBalanceReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return accountManager.addOrDeductAccountBalance(addOrDeductAccountBalanceReq);
    }

    // Where is it being called
    @CrossOrigin
    @RequestMapping(value = "/accountVerificationSMS", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean accountVerificationSMS(@RequestBody AccountVerificationSMSRequest accountVerificationSMSRequest, HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        logger.info("Entering " + accountVerificationSMSRequest.toString());
        return accountManager.accountVerificationSMS(accountVerificationSMSRequest);
    }

    @RequestMapping(value = "/createAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Account createAccount(@RequestBody Account account, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.createAccount(account);
    }

    @RequestMapping(value = "/createAccounts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Account> createAccounts(@RequestBody List<Account> accounts, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.createAccounts(accounts);
    }

    @RequestMapping(value = "/refundToStudentWallet", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RefundRes refundToStudentWallet(@RequestBody RefundStudentWalletRequest refundStudentWalletRequest, HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.refundToStudentWallet(refundStudentWalletRequest);
    }

    @RequestMapping(value = "/getRefundInfo", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RefundRes getRefundInfo(RefundStudentWalletRequest refundStudentWalletRequest, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getRefundInfo(refundStudentWalletRequest);
    }

    // @ApiOperation(value = "Lock balance", notes = "Returns lock balance info")
    @RequestMapping(value = "lockBalance", method = RequestMethod.POST)
    @ResponseBody
    public LockBalanceRes lockBalance(@RequestBody LockBalanceReq req, HttpServletRequest httpServletRequest) throws VException {
        logger.info("Request received for lockBalance - " + req.toString());
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.lockBalance(req);
    }

    // @ApiOperation(value = "unlock balance", notes = "Returns lock balance info")
    @RequestMapping(value = "unLockBalance", method = RequestMethod.POST)
    @ResponseBody
    public LockBalanceInfo unLockBalance(@RequestBody LockBalanceInfo req, HttpServletRequest httpServletRequest) throws VException {
        logger.info("Request received for lockBalance - " + req.toString());
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.unlockBalance(req);
    }

    //@RequestMapping(value="/transferForOTM", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    //@ResponseBody
    //public PlatformBasicResponse transferForOTM()

    @RequestMapping(value = "/getTransactions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SalesTransactionInfo> getTransactions(@RequestBody SalesTransactionReq salesTransactionReq, HttpServletRequest httpServletRequest)
            throws VException, IllegalAccessException, InvocationTargetException {
        sessionUtils.isAllowedApp(httpServletRequest);
        salesTransactionReq.verify();
        return accountManager.getTransactions(salesTransactionReq);
    }

    @RequestMapping(value = "/v2/getTransactions", method = RequestMethod.GET)
    @ResponseBody
    public List<SalesTransactionInfo> getTransactions_v2(SalesTransactionReq salesTransactionReq) throws VException, InvocationTargetException, IllegalAccessException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        salesTransactionReq.verify();
        return accountManager.getTransactions(salesTransactionReq);
    }

    @RequestMapping(value = "/addAccountPancard", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addAccountPancard(@RequestBody AddAccountPancardReq addAccountPancardReq,HttpServletRequest httpServletRequest)
            throws VException, IllegalAccessException, InvocationTargetException {
        sessionUtils.isAllowedApp(httpServletRequest);
        accountManager.addAccountPancard(addAccountPancardReq);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/refundMoneyToStudentWallet", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse refundMoneyToStudentWallet(
            @RequestBody RefundMoneyToStudentWalletReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        req.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        req.setCallingUserId(sessionData.getUserId());
        return accountManager.refundMoneyToStudentWallet(req);
    }
    
    @RequestMapping(value = "/getUserDashboardAccountInfo", method = RequestMethod.GET) 
    @ResponseBody
    public GetUserDashboardAccountInfoRes getUserDashboardAccountInfo(@RequestParam(value = "userId", required = true) Long userId, HttpServletRequest httpServletRequest) throws VException{
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getUserDashboardAccountInfo(userId);
    }
    
    
    @RequestMapping(value = "/makeRefundAdjustment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTMRefundAdjustmentRes makeOTMRefundPayment(@RequestBody OTMRefundAdjustmentReq oTMRefundAdjustmentReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.makeRefundAdjustment(oTMRefundAdjustmentReq);
    }
    
    @RequestMapping(value = "/revertVedantuDiscountNotClaimed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse revertVedantuDiscountNotClaimed(@RequestBody OTMRefundAdjustmentReq oTMRefundAdjustmentReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        return accountManager.revertVedantuDiscountNotClaimed(oTMRefundAdjustmentReq);
    }

    @RequestMapping(value = "/setPaymentGatewayName", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setPaymentGatewayName(@RequestBody SetPaymentGatewayNameReq setpaymentGatewayNameReq) throws VException {
    	sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
    	return accountManager.setPaymentGatewayName(setpaymentGatewayNameReq);
    }
        
    @RequestMapping(value = "/getPaymentGatewayName", method = RequestMethod.GET)
    @ResponseBody
    public PaymentGatewayNameRes getPaymentGatewayName(@RequestParam(value = "userId", required = true) String userId) throws VException {
    	sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
    	return accountManager.getPaymentGatewayName(userId);
    }


    @RequestMapping(value = "/getAllExtTransactions", method = RequestMethod.GET)
    @ResponseBody
    public  List<GetAllExtTransactionsRes> getAllExtTransactions(@ModelAttribute GetAllExtTransactionsReq getAllExtTransactionsReq)  throws VException {
        logger.info("getAllExtTransactions in account controller");
        getAllExtTransactionsReq.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return accountManager.getAllExtTransactions(getAllExtTransactionsReq);
    }

    @RequestMapping(value = "/checkAnyPreviousOrders", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse checkAnyPreviousOrders() throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        sessionUtils.checkIfAllowed(null, Role.STUDENT, Boolean.TRUE);
        return accountManager.checkAnyPreviousOrders(sessionData.getUserId());
    }

    @RequestMapping(value = "/checkIfHasPaidOrders", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse checkIfHasPaidOrders() throws VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        sessionUtils.checkIfAllowed(null, Role.STUDENT, Boolean.TRUE);
        return accountManager.checkIfHasPaidOrders(sessionData.getUserId());
    }

    @RequestMapping(value = "/switchAmountOneAccountToAnother", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse switchAmountOneAccountToAnother(@RequestBody SwitchAmountOneAccountToAnotherReq req) throws VException{
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return accountManager.switchAmountOneAccountToAnother(req);
    }

    @RequestMapping(value = "/extTransactionStatusChangeById", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse extTransactionStatusChangeById(@RequestParam(value = "extTransactionId", required = true) String extTransactionId,
                                                                @RequestParam(value = "status", required = true) TransactionStatus status) throws VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return accountManager.extTransactionStatusChangeById(extTransactionId, status);
    }

}
