package com.vedantu.dinero.controllers;

import com.vedantu.User.Role;
import com.vedantu.dinero.entity.PaymentGateway;
import com.vedantu.dinero.managers.AccountManager;
import com.vedantu.dinero.managers.PaymentManager;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.request.AddPaymentGatewayReq;
import com.vedantu.dinero.request.GetMyOrdersReq;
import com.vedantu.dinero.request.ModifyPaymentGatewayReq;
import com.vedantu.dinero.request.RechargeReq;
import com.vedantu.dinero.response.GetMyOrdersRes;
import com.vedantu.exception.VException;
import com.vedantu.util.security.DevicesAllowed;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author mnpk
 */

@EnableAsync
@RestController
@RequestMapping("/payment/v2")
public class PaymentControllerV2 {

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AccountManager accountManager;

    @RequestMapping(value = "/getMyOrders", method = RequestMethod.GET)
    @ResponseBody
    public GetMyOrdersRes getMyOrders(GetMyOrdersReq getMyOrdersReq) throws VException {
        sessionUtils.checkIfAllowed(getMyOrdersReq.getCallingUserId(), null, Boolean.FALSE);
        getMyOrdersReq.setCallingUserId(sessionUtils.getCallingUserId());
        return paymentManager.getMyOrders(getMyOrdersReq);
    }

    @RequestMapping(value = "/rechargeAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RechargeUrlInfo rechargeAccount(@RequestBody RechargeReq rechargeReq) throws VException {
        rechargeReq.verify();
        sessionUtils.checkIfAllowed(rechargeReq.getUserId(), null, Boolean.FALSE);
        rechargeReq.setUserId(rechargeReq.getCallingUserId());
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        DevicesAllowed device = null;
        if (sessionData != null) {
            device = sessionData.getDevice();
        }
        return accountManager.rechargeAccount(rechargeReq, device, null, null);
    }

    @RequestMapping(value = "/addPaymentGateways", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentGateway addPaymentGateways(@RequestBody AddPaymentGatewayReq addPaymentGatewayReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.addPaymentGateways(addPaymentGatewayReq);
    }

    @RequestMapping(value = "/getPaymentGateways", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PaymentGateway> getPaymentGateways() throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.getPaymentGateways();
    }

    @RequestMapping(value = "/modifyPaymentGateway", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentGateway modifyPaymentGateway(@RequestBody ModifyPaymentGatewayReq modifyPaymentGatewayReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.modifyPaymentGateway(modifyPaymentGatewayReq);
    }

}
