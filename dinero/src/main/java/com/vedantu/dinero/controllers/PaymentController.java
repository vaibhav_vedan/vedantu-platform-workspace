package com.vedantu.dinero.controllers;


import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dinero.entity.BaseSubscriptionPaymentPlan;
import com.vedantu.dinero.entity.EmiCard;
import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.Instalment.BaseInstalment;
import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.entity.Instalment.InstalmentConfigProps;
import com.vedantu.dinero.entity.Orders;
import com.vedantu.dinero.entity.PaymentGateway;
import com.vedantu.dinero.entity.PaymentOffer;
import com.vedantu.dinero.entity.Refund;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.enums.EmiCardType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.payment.WalletPaymentMethod;
import com.vedantu.dinero.managers.AccountManager;
import com.vedantu.dinero.managers.InstalmentManager;
import com.vedantu.dinero.managers.PaymentManager;
import com.vedantu.dinero.managers.RefundManager;
import com.vedantu.dinero.managers.aws.AwsSNSManager;
import com.vedantu.dinero.managers.payment.JuspayPaymentManager;
import com.vedantu.dinero.managers.payment.RazorpayPaymentManager;
import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.FirstExtTransaction;
import com.vedantu.dinero.pojo.InstalmentUpdateReq;
import com.vedantu.dinero.pojo.RechargeUrlInfo;
import com.vedantu.dinero.request.AddPaymentGatewayReq;
import com.vedantu.dinero.request.BuyItemsReq;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.request.ChangeInstalmentDueDateReq;
import com.vedantu.dinero.request.ContextWiseNextDueInstalmentsReq;
import com.vedantu.dinero.request.ExportOrdersCsvReq;
import com.vedantu.dinero.request.GetDueInstalmentsForTimeReq;
import com.vedantu.dinero.request.GetEmiCardReq;
import com.vedantu.dinero.request.GetExtTransactionsForUsers;
import com.vedantu.dinero.request.GetInstallmentforBatchesReq;
import com.vedantu.dinero.request.GetInstalmentsForOrdersReq;
import com.vedantu.dinero.request.GetMyOrdersReq;
import com.vedantu.dinero.request.GetOrderDetailsReq;
import com.vedantu.dinero.request.GetOrderInfoReq;
import com.vedantu.dinero.request.GetOrdersByDeliverableEntityReq;
import com.vedantu.dinero.request.GetOrdersReq;
import com.vedantu.dinero.request.GetOrdersWithoutDeliverableEntityReq;
import com.vedantu.dinero.request.GetRegistrationforEnrollmentsReq;
import com.vedantu.dinero.request.GetTransactionsReq;
import com.vedantu.dinero.request.Instalment.GetInstalmentConfigPropsReq;
import com.vedantu.dinero.request.Instalment.GetInstalmentsReq;
import com.vedantu.dinero.request.MarkPendingExtTxnReq;
import com.vedantu.dinero.request.ModifyPaymentGatewayReq;
import com.vedantu.dinero.request.OnPaymentReceiveReq;
import com.vedantu.dinero.request.PayInstalmentReq;
import com.vedantu.dinero.request.PaymentOfferReq;
import com.vedantu.dinero.request.PrepareInstalmentsReq;
import com.vedantu.dinero.request.ProcessPaymentReq;
import com.vedantu.dinero.request.RechargeReq;
import com.vedantu.dinero.request.SetInstalmentConfigPropsReq;
import com.vedantu.dinero.request.UpdateDeliverableDataReq;
import com.vedantu.dinero.request.UpdateDeliverableEntityIdInOrderReq;
import com.vedantu.dinero.request.UpdatesForOrdersRequest;
import com.vedantu.dinero.request.WalletRequest;
import com.vedantu.dinero.response.BankDetail;
import com.vedantu.dinero.response.ContextWiseNextDueInstalmentsRes;
import com.vedantu.dinero.response.CounselorDashboardInstalmentInfoRes;
import com.vedantu.dinero.response.DashboardUserInstalmentHistoryRes;
import com.vedantu.dinero.response.EMICardResp;
import com.vedantu.dinero.response.GetInstalmentDetailsOTFRes;
import com.vedantu.dinero.response.GetMyOrdersRes;
import com.vedantu.dinero.response.InstalmentDues;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.InstalmentPaymentInfoRes;
import com.vedantu.dinero.response.OrderDetails;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.PayInstalmentRes;
import com.vedantu.dinero.response.PaymentMessageStrip;
import com.vedantu.dinero.response.PaymentReceiveResponse;
import com.vedantu.dinero.response.UserDueInstalmentsResp;
import com.vedantu.dinero.serializers.ExtTransactionDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.SubscriptionMetric;
import com.vedantu.subscription.request.GetRegisteredCoursesReq;
import com.vedantu.subscription.request.ResetInstallmentStateReq;
import com.vedantu.subscription.response.SubscriptionPlanInfoResp;
import com.vedantu.util.*;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.BasicResponse;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.ResponseCode;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.request.MarkOrderEndedRes;
import com.vedantu.util.request.OrderEndType;
import com.vedantu.util.security.DevicesAllowed;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import in.juspay.model.Card;
import in.juspay.model.Wallet;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

@EnableAsync
@RestController
@RequestMapping("payment")
public class PaymentController {

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private RazorpayPaymentManager razorpayPaymentManager;

    @Autowired
    private ExtTransactionDAO extTransactionDAO;

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private InstalmentManager instalmentManager;

    @Autowired
    private RefundManager refundManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentController.class);

    @RequestMapping(value = "/buyItems", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo buyItems(@RequestBody BuyItemsReq buyItemsReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        logger.info("ENTRY :" + buyItemsReq);
        buyItemsReq.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        DevicesAllowed device = null;
        if (sessionData != null) {
            device = sessionData.getDevice();
        }
        // If a student calls the api, the user id must for the student calling the api
        if (null != sessionUtils.getCurrentSessionData() && sessionUtils.isStudent(sessionUtils.getCurrentSessionData())) {
            if (!buyItemsReq.getUserId().equals(sessionUtils.getCallingUserId())) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Operation not allowed");
            }
        }
        return paymentManager.buyItems(buyItemsReq, device);
    }

    @RequestMapping(value = "/processOrderAfterPayment/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo processOrderAfterPayment(@PathVariable("id") String id,
                                              @RequestBody ProcessPaymentReq req) throws VException {
        return paymentManager.processOrderAfterPayment(id, req);
    }

    @RequestMapping(value = "/getMyOrders", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetMyOrdersRes getMyOrders(@RequestBody GetMyOrdersReq getMyOrdersReq) throws VException {
        return paymentManager.getMyOrders(getMyOrdersReq);
    }

    @RequestMapping(value = "/getOrderInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo getOrderInfo(@RequestBody GetOrderInfoReq getOrderInfoReq) throws VException {
        return paymentManager.getOrderInfo(getOrderInfoReq);
    }

    @RequestMapping(value = "/handleRazorpayWebhook", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse handleRazorpayWebhook(@RequestBody Map<String, Object> object) throws VException, UnsupportedEncodingException {
        razorpayPaymentManager.handleRazorpayWebhook(object);
        return new PlatformBasicResponse();
    }

    @CrossOrigin
    @RequestMapping(value = "/onPaymentReceived", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentReceiveResponse onPaymentReceived(@RequestBody OnPaymentReceiveReq onPaymentReceiveReq) throws VException {

        return paymentManager.onPaymentReceive(onPaymentReceiveReq.getTransactionInfo(),
                PaymentGatewayName.getPaymentGatewayName(onPaymentReceiveReq.getAction()), onPaymentReceiveReq.getCallingUserId());
    }

    @RequestMapping(value = "/rechargeAccount", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RechargeUrlInfo rechargeAccount(@RequestBody RechargeReq rechargeReq) throws VException {
        rechargeReq.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        DevicesAllowed device = null;
        if (sessionData != null) {
            device = sessionData.getDevice();
        }

        return accountManager.rechargeAccount(rechargeReq, device, null, null);
        /*return accountManager.rechargeAccount(rechargeReq.getUserId(), rechargeReq.getIpAddress(),
                rechargeReq.getAmount(), null, null, rechargeReq.getRedirectUrl(),
                rechargeReq.getRefType(), rechargeReq.getRefId(), rechargeReq.getCallingUserId(), device);*/
    }

    @RequestMapping(value = "/addPaymentGateways", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentGateway addPaymentGateways(@RequestBody AddPaymentGatewayReq addPaymentGatewayReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentManager.addPaymentGateways(addPaymentGatewayReq);
    }

    @RequestMapping(value = "/getPaymentGateways", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PaymentGateway> getPaymentGateways() throws VException {

        return paymentManager.getPaymentGateways();
    }

    @RequestMapping(value = "/modifyPaymentGateway", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PaymentGateway modifyPaymentGateway(@RequestBody ModifyPaymentGatewayReq ModifyPaymentGatewayReq)
            throws VException {

        return paymentManager.modifyPaymentGateway(ModifyPaymentGatewayReq);
    }

    @RequestMapping(value = "/deletePaymentGateway", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean deletePaymentGateway(@RequestBody String paymentGatewayId, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentManager.deletePaymentGateway(paymentGatewayId);
    }

    @RequestMapping(value = "/getTransactions", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Transaction> getTransactions(@RequestBody GetTransactionsReq getTransactionsReq) throws VException {

        return paymentManager.getTransactions(getTransactionsReq);
    }

    //    @ApiOperation(value = "Get Orders By SubscriptionId", notes = "Returns orders")
    @RequestMapping(value = "getOrdersBySubscriptionId", method = RequestMethod.GET)
    @ResponseBody
    public List<Orders> getOrdersBySubscriptionId(@RequestParam(value = "id") Long subscriptionId)
            throws VException {
        logger.info("Request received for getting Orders by SubscriptionId: " + subscriptionId);
        List<Orders> orders = paymentManager.getOrdersBySubscriptionId(subscriptionId);
        logger.info("Response:" + orders);
        return orders;
    }

    @CrossOrigin
    @RequestMapping(value = "getOrdersBySubscriptionIds", method = RequestMethod.GET)
    @ResponseBody
    public Map<Long, List<Orders>> getOrdersBySubscriptionIds(@RequestParam(value = "ids") List<Long> ids)
            throws VException {
        logger.info("Request received for getting Orders by SubscriptionIds: " + ids);
        Map<Long, List<Orders>> orderedItemsres = paymentManager.getOrdersBySubscriptionIds(ids);
        logger.info("Response:" + orderedItemsres.toString());
        return orderedItemsres;
    }

    @RequestMapping(value = "getOrdersByEntityIds", method = RequestMethod.GET)
    @ResponseBody
    public List<Orders> getOrdersByEntityIds(@RequestParam(value = "ids") List<String> ids, @RequestParam(value = "userId") Long userId)
            throws VException {
        return paymentManager.getOrdersByEntityIds(ids, userId);
    }

    @RequestMapping(value = "isDoubtsPaidUser", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Boolean> isDoubtsPaidUser(@RequestParam(value = "userId") Long userId) {
        return paymentManager.isDoubtsPaidUser(userId);
    }

    @RequestMapping(value = "getSubscriptionMetrices", method = RequestMethod.GET)
    @ResponseBody
    public List<SubscriptionMetric> getSubscriptionMetrices() throws VException {
        logger.info("Request received for getting SubscriptionMetrices");
        List<SubscriptionMetric> results = paymentManager.getSubscriptionMetrices();
        logger.info("Response:" + results.toString());
        return results;
    }

    @RequestMapping(value = "getInstalmentConfigProps", method = RequestMethod.GET)
    @ResponseBody
    public List<InstalmentConfigProps> getInstalmentConfigProps(GetInstalmentConfigPropsReq instalmentConfigPropsReq)
            throws VException {
        logger.info("ENTRY: getInstalmentConfigProps " + instalmentConfigPropsReq);
        List<InstalmentConfigProps> results = instalmentManager
                .getInstalmentConfigProps(instalmentConfigPropsReq);
        logger.info("EXIT:" + results);
        return results;
    }

    @RequestMapping(value = "getSuitableInstalmentConfigProps", method = RequestMethod.GET)
    @ResponseBody
    public List<InstalmentConfigProps> getSuitableInstalmentConfigProps(GetInstalmentConfigPropsReq instalmentConfigPropsReq)
            throws VException {
        logger.info("ENTRY: getSuitableInstalmentConfigProps " + instalmentConfigPropsReq);
        List<InstalmentConfigProps> results = instalmentManager
                .getSuitableInstalmentConfigProps(instalmentConfigPropsReq);
        logger.info("EXIT:" + results);
        return results;
    }

    @RequestMapping(value = "setInstalmentConfigProps", method = RequestMethod.POST)
    @ResponseBody
    public InstalmentConfigProps setInstalmentConfigProps(@RequestBody SetInstalmentConfigPropsReq instalmentConfigPropsReq, HttpServletRequest httpServletRequest) throws VException {
        logger.info("ENTRY: setInstalmentConfigProps " + instalmentConfigPropsReq);
        sessionUtils.isAllowedApp(httpServletRequest);
        instalmentConfigPropsReq.verify();
        InstalmentConfigProps result = instalmentManager.setInstalmentConfigProps(instalmentConfigPropsReq);
        logger.info("EXIT:" + result);
        return result;
    }

    @RequestMapping(value = "getInstalments", method = RequestMethod.GET)
    @ResponseBody
    public List<Instalment> getInstalments(@ModelAttribute GetInstalmentsReq getInstalmentsReq)
            throws VException {
        //TODO check if owner is calling or not
        List<Instalment> results = new ArrayList<>();
        if (getInstalmentsReq.getBatchId() != null && getInstalmentsReq.getUserId() != null) {
            results = instalmentManager
                    .getInstalmentsForOTFBatch(getInstalmentsReq);
        } else if (getInstalmentsReq.getOrderId() != null) {
            results = instalmentManager.getInstalmentsForOrder(getInstalmentsReq.getOrderId(),
                    getInstalmentsReq.getPaymentStatus());
        } else if (getInstalmentsReq.getSubscriptionId() != null) {
            results = instalmentManager
                    .getInstalmentsForSubscription(getInstalmentsReq);
        } else if (StringUtils.isNotEmpty(getInstalmentsReq.getCoursePlanId())) {
            results = instalmentManager
                    .getInstalmentsForCoursePlan(getInstalmentsReq);
        } else if (ArrayUtils.isNotEmpty(getInstalmentsReq.getContextIds()) && getInstalmentsReq.getUserId() != null) {
            results = instalmentManager
                    .getInstalmentsForContextIds(getInstalmentsReq);
        } else if (ArrayUtils.isNotEmpty(getInstalmentsReq.getDeliverableIds()) && getInstalmentsReq.getUserId() != null) {
            results = instalmentManager
                    .getInstalmentsForDeliverableIds(getInstalmentsReq);
        }
        logger.info("EXIT:" + results);
        return results;
    }

    @RequestMapping(value = "getInstalmentsForOrderIds", method = RequestMethod.GET)
    @ResponseBody
    public List<Instalment> getInstalmentsForOrderIds(@ModelAttribute GetInstalmentsForOrdersReq getInstalmentsForOrdersReq)
            throws VException {
        List<Instalment> results = instalmentManager.getInstalmentsForOrders(getInstalmentsForOrdersReq.getOrderIds(), getInstalmentsForOrdersReq.getPaymentStatus());
        return results;
    }

    @RequestMapping(value = "getInstalment/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Instalment getInstalment(@PathVariable("id") String id)
            throws VException {
        logger.info("ENTRY: getInstalment :" + id);
        Instalment instalment = instalmentManager.getInstalment(id);
        logger.info("EXIT:" + instalment);
        return instalment;
    }

    @RequestMapping(value = "getInstalmentFromDueTime", method = RequestMethod.GET)
    @ResponseBody
    public Instalment getInstalmentFromDueTime(@RequestParam("orderId") String orderId,
                                               @RequestParam("dueTime") Long dueTime)
            throws VException {
        logger.info("ENTRY: orderId :" + orderId + ", dueTime " + dueTime);
        Instalment instalment = instalmentManager.getInstalment(orderId, dueTime);
        logger.info("EXIT:" + instalment);
        return instalment;
    }

    @RequestMapping(value = "prepareInstalments", method = RequestMethod.POST)
    @ResponseBody
    public List<Instalment> prepareInstalments(@RequestBody PrepareInstalmentsReq prepareInstalmentsReq)
            throws VException {
        logger.info("ENTRY :" + prepareInstalmentsReq);
        prepareInstalmentsReq.verify();
        List<Instalment> results;
        Long userId = prepareInstalmentsReq.getUserId();
        if (userId == null) {
            prepareInstalmentsReq.getCallingUserId();
        }
        if (InstalmentPurchaseEntity.BATCH.equals(prepareInstalmentsReq.getInstalmentPurchaseEntity())
                || InstalmentPurchaseEntity.BUNDLE.equals(prepareInstalmentsReq.getInstalmentPurchaseEntity())
                || InstalmentPurchaseEntity.OTF_BUNDLE.equals(prepareInstalmentsReq.getInstalmentPurchaseEntity())) {
            results = instalmentManager
                    .prepareInstalments(prepareInstalmentsReq.getInstalmentPurchaseEntity(),
                            prepareInstalmentsReq.getInstalmentPurchaseEntityId(),
                            prepareInstalmentsReq.getTotalDiscount(), null, null, (userId != null ? userId.toString() : null));
        } else {
            results = instalmentManager
                    .prepareInstalments(prepareInstalmentsReq.getSessionSchedule(),
                            prepareInstalmentsReq.getHourlyRate(),
                            prepareInstalmentsReq.getInstalmentPurchaseEntity(),
                            prepareInstalmentsReq.getInstalmentPurchaseEntityId(),
                            prepareInstalmentsReq.getDiscountPerHour(), null, null,
                            (userId != null ? userId.toString() : null));
        }
        if (Boolean.TRUE.equals(prepareInstalmentsReq.getOnlyFirstInstalment()) && ArrayUtils.isNotEmpty(results)) {
            results = results.subList(0, 1);
        }
        logger.info("EXIT:" + results);
        return results;
    }


    @RequestMapping(value = "payInstalment", method = RequestMethod.POST)
    @ResponseBody
    public PayInstalmentRes payInstalment(@RequestBody PayInstalmentReq payInstalmentReq)
            throws VException {
        logger.info("ENTRY: " + payInstalmentReq);
        payInstalmentReq.verify();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        DevicesAllowed device = null;
        if (sessionData != null) {
            device = sessionData.getDevice();
        }
        //TODO check if owner is calling or not
        PayInstalmentRes resp = instalmentManager.payInstalment(payInstalmentReq, device);
        logger.info("EXIT:" + resp);
        return resp;
    }

    @RequestMapping(value = "/processInstalmentAfterPayment/{id}", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Instalment> processInstalmentAfterPayment(@PathVariable("id") String id) throws VException, CloneNotSupportedException {
        return instalmentManager.processInstalmentPaymentNew(id);
    }

    @RequestMapping(value = "updateDeliverableEntityIdInOrder", method = RequestMethod.POST)
    @ResponseBody
    public BasicResponse updateDeliverableEntityIdInOrder(@RequestBody UpdateDeliverableEntityIdInOrderReq updateDeliverableEntityIdInOrderReq, HttpServletRequest httpServletRequest) throws VException {
        logger.info("ENTRY: " + updateDeliverableEntityIdInOrderReq);
        sessionUtils.isAllowedApp(httpServletRequest);
        updateDeliverableEntityIdInOrderReq.verify();
        BasicResponse resp = paymentManager.updateDeliverableEntityIdInOrder(updateDeliverableEntityIdInOrderReq);
        logger.info("EXIT:" + resp);
        return resp;
    }

    @RequestMapping(value = "/getOrders", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Orders> getOrders(@RequestBody GetOrdersReq getOrdersReq) throws VException {

        return paymentManager.getOrders(getOrdersReq);
    }

    @RequestMapping(value = "/getOrdersByOrderIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OrderInfo> getOrdersByOrderIds(@RequestParam(value = "orderIds") List<String> orderIds) throws VException {

        return paymentManager.getOrdersByOrderIds(orderIds);
    }

    //    @ApiOperation(value = "getDueInstalments")
    @RequestMapping(value = "getDueInstalments", method = RequestMethod.GET)
    @ResponseBody
    public List<InstalmentDues> getDueInstalments(@RequestParam(value = "start") Integer start,
                                                  @RequestParam(value = "size") Integer size) throws VException {
        logger.info("ENTRY:  start" + start + ", size " + size);
        start = (start == null) ? 0 : start;
        size = (size != null && size != 0) ? size : 20;

        List<InstalmentDues> results = instalmentManager.getDueInstalments(start, size);
        logger.info("EXIT returning size" + ((results != null) ? results.size() : 0));
        return results;
    }

    //    @ApiOperation(value = "markOrderForfeited", notes = "markOrderForfeited")
    @RequestMapping(value = "/markOrderForfeited", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BasicResponse markOrderForfeited(@RequestBody Map<String, String> requestParams) throws Throwable {
        logger.info("ENTRY:" + requestParams);
        String orderId = requestParams.get("orderId");
        String entityId = requestParams.get("entityId");
        String entityType = requestParams.get("entityType");
        String userId = requestParams.get("userId");
        Boolean response = false;
        if (StringUtils.isNotEmpty(orderId)) {
            response = instalmentManager
                    .markOrderForfeited(orderId);
        } else if (StringUtils.isNotEmpty(entityId)
                && StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(entityType)) {
            EntityType entityType1 = EntityType.valueOf(entityType);
            response = instalmentManager
                    .markOrderForfeitedForEntityId(entityId, entityType1, userId);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OrderId/entityId not valid");
        }
        logger.info("EXIT:" + response.toString());
        return new BasicResponse((response) ? ResponseCode.SUCCESS : ResponseCode.FAILED);
    }

    @RequestMapping(value = "getBaseInstalment", method = RequestMethod.GET)
    @ResponseBody
    public BaseInstalment getBaseInstalment(@RequestParam(value = "instalmentPurchaseEntity") InstalmentPurchaseEntity instalmentPurchaseEntity,
                                            @RequestParam(value = "purchaseEntityId") String purchaseEntityId,
                                            @RequestParam(value = "userId", required = false) Long userId)
            throws VException {
        logger.info("getBaseInstalment " + instalmentPurchaseEntity + purchaseEntityId);
        BaseInstalment baseInstalment = instalmentManager.getBaseInstalment(instalmentPurchaseEntity, purchaseEntityId, userId);
        logger.info("Response:" + baseInstalment);
        return baseInstalment;
    }

    @RequestMapping(value = "getBaseInstalments", method = RequestMethod.GET)
    @ResponseBody
    public List<BaseInstalment> getBaseInstalments(@RequestParam(value = "instalmentPurchaseEntity", required = false) InstalmentPurchaseEntity instalmentPurchaseEntity,
                                                   @RequestParam(value = "purchaseEntityIds") List<String> purchaseEntityIds)
            throws VException {
        logger.info("getBaseInstalment " + instalmentPurchaseEntity + purchaseEntityIds);
        List<BaseInstalment> baseInstalments = instalmentManager.getBaseInstalments(instalmentPurchaseEntity, purchaseEntityIds);
        logger.info("Response:" + baseInstalments);
        return baseInstalments;
    }

    @RequestMapping(value = "/updateBaseInstalment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateBaseInstalment(@RequestBody BaseInstalment baseInstalment, HttpServletRequest httpServletRequest) throws VException {
        logger.info("updateBaseInstalment " + baseInstalment);
        sessionUtils.isAllowedApp(httpServletRequest);
        instalmentManager.updateBaseInstalment(baseInstalment);
    }

    @RequestMapping(value = "/getOrderDetails", method = RequestMethod.GET)
    @ResponseBody
    public OrderDetails coursePaymentDetails(GetOrderDetailsReq req)
            throws VException {
        return paymentManager.getOrderBasicInfo(req);
    }

    @RequestMapping(value = "/checkAndGetAccountInfoForPayment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo checkAndGetAccountInfoForPayment(@RequestBody BuyItemsReqNew req, HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        DevicesAllowed device = null;
        if (sessionData == null) {
            device = DevicesAllowed.WEB;
        } else {
            device = sessionData.getDevice();
        }
        return paymentManager.checkAndGetAccountInfoForPayment(req, device);
    }

    @RequestMapping(value = "/getRegisteredCourses", method = RequestMethod.GET)
    @ResponseBody
    public List<Orders> getRegisteredCourses(GetRegisteredCoursesReq req) throws
            VException {
        return paymentManager.getRegisteredCourses(req);
    }

    @RequestMapping(value = "/updateOrderReferenceTag", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse updateOrderReferenceTag(@RequestParam(value = "orderId") String orderId, @RequestParam(value = "courseId") String courseId, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentManager.updateOTFOrderReferenceTag(orderId, courseId);
    }

    @RequestMapping(value = "/getInstalmentDetailsOTF", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetInstalmentDetailsOTFRes getInstalmentDetailsOTF(@RequestParam(value = "courseId") String courseId,
                                                              @RequestParam(value = "batchId") String batchId,
                                                              @RequestParam(value = "userId") String userId) throws VException {
        return paymentManager.getInstalmentDetailsOTF(courseId, batchId, userId);

    }

    @RequestMapping(value = "/getRefunds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Refund> getRefunds(@RequestParam(value = "orderIds") ArrayList<String> orderIds) throws VException {
        return refundManager.getRefunds(orderIds);

    }

    @RequestMapping(value = "/getNonRefundedOrdersWithoutDeliverableEntityId", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Orders> getNonRefundedOrdersWithoutDeliverableEntityId(@RequestParam(value = "entityId") String entityId,
                                                                       @RequestParam(value = "userId") Long userId) throws VException {

        return paymentManager.getNonRefundedOrdersWithoutDeliverableEntityId(entityId, userId);
    }

    @RequestMapping(value = "/prepareDiscountedInstalmentsCoursePlan", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Instalment> prepareDiscountedInstalmentsCoursePlan(@RequestBody BuyItemsReqNew req)
            throws VException {
        return paymentManager.prepareDiscountedInstalmentsCoursePlan(req);
    }

    @RequestMapping(value = "/getOrdersByDeliverableOrEntity", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Orders> getOrdersByDeliverableOrEntity(@RequestBody GetOrdersByDeliverableEntityReq req) throws VException {
        return paymentManager.getOrdersByDeliverableOrEntity(req.getDeliverableorEntityIds());
    }

    @RequestMapping(value = "/getFirstExtTransactions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<FirstExtTransaction> getFirstExtTransactions(@RequestBody GetExtTransactionsForUsers req) throws VException {
        return paymentManager.getFirstExtTransactions(req.getUserIds());
    }

    @RequestMapping(value = "/resetInstallmentState", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Orders resetInstallmentState(@RequestBody ResetInstallmentStateReq req) throws VException {
        return paymentManager.resetInstallmentState(req);
    }

    @RequestMapping(value = "getOrderIdInstalmentInfoMap", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, InstalmentPaymentInfoRes> getOrderIdInstalmentInfoMap(@RequestParam(value = "ids") List<String> ids)
            throws VException {
        return paymentManager.getOrderIdInstalmentInfoMap(ids);
    }

    @RequestMapping(value = "/resetOrderInstallmentState", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BasicResponse resetOrderInstallmentState(@RequestBody Map<String, String> requestParams) throws Throwable {
        logger.info("ENTRY:" + requestParams);

        String entityId = requestParams.get("entityId");
        String entityType = requestParams.get("entityType");
        String userId = requestParams.get("userId");
        String deliverableEntityId = requestParams.get("deliverableEntityId");
        Boolean response = false;
        if (StringUtils.isNotEmpty(entityId) && StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(entityType)) {
            EntityType entityType1 = EntityType.valueOf(entityType);
            response = instalmentManager.resetOrderInstallmentState(entityId, entityType1, userId, deliverableEntityId);

        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "EntityId/userId not valid");
        }
        logger.info("EXIT:" + response.toString());
        return new BasicResponse((response) ? ResponseCode.SUCCESS : ResponseCode.FAILED);

    }

    @RequestMapping(value = "/getInstallmentforBatches", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    Map<String, List<DashBoardInstalmentInfo>> getInstallmentforBatchesPOSTReq(@RequestBody GetInstallmentforBatchesReq req) throws VException {

        return paymentManager.getInstallmentforBatches(req.getIds());
    }

    @RequestMapping(value = "/getInstallmentsByContextIds", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, List<DashBoardInstalmentInfo>> getInstallmentsByContextIds(@RequestParam(value = "ids") List<String> ids, @RequestParam(value = "userId") Long userId) throws VException {

        return paymentManager.getInstallmentsByContextIds(ids, userId);
    }

    @RequestMapping(value = "/getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded", method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Orders> getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded(GetOrdersWithoutDeliverableEntityReq req) throws VException {

        return paymentManager.getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded(req);
    }

    @RequestMapping(value = "/getRegistrationforEnrollments", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    Map<String, Orders> getRegistrationforEnrollmentsPOSTReq(@RequestBody GetRegistrationforEnrollmentsReq req) throws VException {

        return paymentManager.getRegistrationforEnrollments(req.getIds());
    }

    //this api is only used for OTF
    @RequestMapping(value = "/changeInstalmentDueDate", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse changeInstalmentDueDate(@RequestBody ChangeInstalmentDueDateReq req, HttpServletRequest httpServletRequest) throws
            VException, CloneNotSupportedException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return instalmentManager.changeInstalmentDueDate(req);
    }

    @RequestMapping(value = "/getDueInstalmentsForContextTypeAndTime", method = RequestMethod.GET)
    @ResponseBody
    public List<InstalmentInfo> getDueInstalmentsForContextTypeAndTime(GetDueInstalmentsForTimeReq req) throws VException {
        return instalmentManager.getDueInstalmentsForContextTypeAndTime(req);
    }

    @RequestMapping(value = "/getNewStudentsForTimePeriod", method = RequestMethod.GET)
    @ResponseBody
    public Set<Long> getNewStudentsForTimePeriod(GetDueInstalmentsForTimeReq req) {
        return paymentManager.getNewStudentsForTimePeriod(req);
    }

    @RequestMapping(value = "/getFirstBaseInstalmentsForDueTime", method = RequestMethod.GET)
    @ResponseBody
    public List<BaseInstalment> getFirstBaseInstalmentsForDueTime(@RequestParam(value = "startTime") Long startTime,
                                                                  @RequestParam(value = "endTime") Long endTime)
            throws VException {

        List<BaseInstalment> baseInstalments = instalmentManager.getFirstBaseInstalmentsForDueTime(startTime, endTime);
        return baseInstalments;
    }

    @RequestMapping(value = "/reverseOrderTransaction", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse reverseOrderTransaction(@RequestParam(value = "orderId") String orderId, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentManager.reverseOrderTransaction(orderId);
    }

    @RequestMapping(value = "/updateDeliverableData", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse updateDeliverableData(@RequestBody UpdateDeliverableDataReq req, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentManager.updateDeliverableData(req);
    }

    @RequestMapping(value = "/getOrdersByDeliverableIds", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Orders> getOrdersByDeliverableIds(@RequestBody GetOrdersByDeliverableEntityReq req) throws VException {
        return paymentManager.getOrdersByDeliverableIds(req.getDeliverableorEntityIds());
    }

    @RequestMapping(value = "/getExtTransactionsFromOrderIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ExtTransaction> getExtTransactionsFromOrderIds(@RequestParam(value = "vedantuOrderIds") String vedantuOrderIdsListStr) throws VException {
        if (StringUtils.isNotEmpty(vedantuOrderIdsListStr)) {
            List<String> vedantuOrderIds = Arrays.asList(vedantuOrderIdsListStr.split(","));
            return extTransactionDAO.getByVedantuOrderIds(vedantuOrderIds);
        } else {
            return new ArrayList<>();
        }
    }

    @RequestMapping(value = "getContextWiseNextDueInstalments", method = RequestMethod.GET)
    @ResponseBody
    public List<ContextWiseNextDueInstalmentsRes> getContextWiseNextDueInstalments(ContextWiseNextDueInstalmentsReq req)
            throws VException {
        return instalmentManager.getContextWiseNextDueInstalments(req);
    }


    @RequestMapping(value = "getDueInstalmentsForUser", method = RequestMethod.GET)
    @ResponseBody
    public List<UserDueInstalmentsResp> getDueInstalmentsForUser()
            throws VException {
        return instalmentManager.getDueInstalmentsForUser();
    }

    @RequestMapping(value = "/getLatestPaidInstalment", method = RequestMethod.GET)
    @ResponseBody
    public InstalmentInfo getLatestPaidInstalment(@RequestParam(value = "orderId") String orderId) throws VException {
        return instalmentManager.getLastestInstalmentPaid(orderId);
    }

    @RequestMapping(value = "/getOrderInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderInfo> getOrderInfo(@RequestParam(value = "orderIds") List<String> orderIds) throws VException {
        return instalmentManager.getOrderInfo(orderIds);
    }

    @RequestMapping(value = "/getDashboardDueInstalmentsForContextTypeAndTime", method = RequestMethod.GET)
    @ResponseBody
    public List<CounselorDashboardInstalmentInfoRes> getDashboardDueInstalmentsForContextTypeAndTime(GetDueInstalmentsForTimeReq req)
            throws VException {
        return instalmentManager.getDashboardDueInstalmentsForContextTypeAndTime(req);
    }

    @RequestMapping(value = "/getUserInstalmentTransactionHistory", method = RequestMethod.GET)
    @ResponseBody
    public Map<Long, DashboardUserInstalmentHistoryRes> getUserInstalmentTransactionHistory(@RequestParam(value = "userIds") List<Long> userIds)
            throws VException {
        return instalmentManager.getUserInstalmentTransactionHistory(userIds);
    }

    @RequestMapping(value = "/markOrderEnded", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MarkOrderEndedRes markOrderEnded(@RequestBody Map<String, String> requestParams) throws Throwable {
        logger.info("ENTRY:" + requestParams);
        String orderId = requestParams.get("orderId");
        String entityId = requestParams.get("entityId");
        String entityType = requestParams.get("entityType");
        String userId = requestParams.get("userId");
        OrderEndType orderEndType = requestParams.get("orderEndType") != null ? OrderEndType.valueOf(requestParams.get("orderEndType")) : null;
        Boolean response = false;
        String entityOrderId = "";
        if (StringUtils.isNotEmpty(orderId)) {
            response = instalmentManager
                    .markOrderEnded(orderId);
            entityOrderId = orderId;
        } else if (StringUtils.isNotEmpty(entityId)
                && StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(entityType)) {
            EntityType entityType1 = EntityType.valueOf(entityType);
            entityOrderId = instalmentManager
                    .markOrderEndedForEntityId(entityId, entityType1, userId, orderEndType);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "OrderId/entityId not valid");
        }
        MarkOrderEndedRes markOrderEndedRes = new MarkOrderEndedRes();
        markOrderEndedRes.setResponseCode(StringUtils.isNotEmpty(entityOrderId) ? ResponseCode.SUCCESS : ResponseCode.FAILED);
        markOrderEndedRes.setOrderId(entityOrderId);
        logger.info("EXIT:" + response.toString());
        return markOrderEndedRes;
    }

    @RequestMapping(value = "/resetOrderInstallmentStatePostPayment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BasicResponse resetOrderInstallmentStatePostPayment(@RequestBody Map<String, String> requestParams) throws Throwable {
        logger.info("ENTRY:" + requestParams);

        String entityId = requestParams.get("entityId");
        String entityType = requestParams.get("entityType");
        String userId = requestParams.get("userId");
        String deliverableEntityId = requestParams.get("deliverableEntityId");
        String deliverableId = requestParams.get("deliverableId");
        Boolean response = false;
        if (StringUtils.isNotEmpty(entityId) && StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(entityType)) {
            EntityType entityType1 = EntityType.valueOf(entityType);
            response = instalmentManager.resetOrderInstallmentStatePostPayment(entityId,
                    entityType1, userId, deliverableEntityId, deliverableId);

        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "EntityId/userId not valid");
        }
        logger.info("EXIT:" + response.toString());
        return new BasicResponse((response) ? ResponseCode.SUCCESS : ResponseCode.FAILED);

    }

    @RequestMapping(value = "/getPendingExtTransactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ExtTransaction> getPendingExtTransactions(@RequestParam(name = "start") Integer start,
                                                          @RequestParam(name = "limit") Integer limit, @RequestParam(name = "userId") Long userId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return extTransactionDAO.getPendingTransactions(start, limit, userId);
    }

    @RequestMapping(value = "/markPendingExtTransaction", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markPendingExtTransaction(@RequestBody MarkPendingExtTxnReq markPendingExtTxnReq) throws VException, BadRequestException, InternalServerErrorException, NotFoundException, ForbiddenException {
        markPendingExtTxnReq.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.markPendingExtTransaction(markPendingExtTxnReq);
    }

    @RequestMapping(value = "/generateInvoiceForExTransaction", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse generateInvoiceForExTransaction(@RequestParam(name = "id", required = true) String exTransactionId) throws BadRequestException, InternalServerErrorException, NotFoundException {
        return paymentManager.generateInvoiceForExTransaction(exTransactionId);
    }

    @RequestMapping(value = "/generateInvoiceForTransaction", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse generateInvoiceForTransaction(@RequestParam(name = "id", required = true) String transactionId, @RequestParam(name = "userId", required = true) Long userId) throws BadRequestException, InternalServerErrorException, NotFoundException {
        return paymentManager.generateInvoiceForTransaction(transactionId, userId);
    }

    @RequestMapping(value = "updateInstalments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Instalment> updateInstalments(@RequestBody InstalmentUpdateReq instalmentUpdateReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        instalmentUpdateReq.verify();
        return instalmentManager.updateInstalmentsForUser(instalmentUpdateReq);
    }

    @RequestMapping(value = "/addEditStudentBaseInstalment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addEditStudentBaseInstalment(@RequestBody BaseInstalment baseInstalment) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        instalmentManager.addEditStudentBaseInstalment(baseInstalment);
    }

    @RequestMapping(value = "/addEditStudentBaseInstalmentApp", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addEditStudentBaseInstalmentApp(@RequestBody BaseInstalment baseInstalment, HttpServletRequest httpRequest) throws VException {
        sessionUtils.isAllowedApp(httpRequest);
        instalmentManager.addEditStudentBaseInstalment(baseInstalment);
    }

    @RequestMapping(value = "/getOrdersByAgentEmailId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Orders> getOrdersByAgentEmailId(@RequestBody String emailId) {
        return paymentManager.getOrdersByAgentEmailId(emailId);
    }

    @RequestMapping(value = "/markBulkOrderEnded", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> markBulkOrderEnded() throws VException {
        return instalmentManager.markBulkOrderEnded();
    }

    @RequestMapping(value = "/getOrdersByListOfAgentEmailId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Orders> getOrdersByListOfAgentEmailId(@RequestBody List<String> emailIds) {
        return paymentManager.getOrdersByListOfAgentEmailId(emailIds);
    }

    @RequestMapping(value = "/updateOrders", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateOrders(@RequestBody UpdatesForOrdersRequest updateOrders) throws VException {
        updateOrders.verify();
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), true);
        return paymentManager.updateOrders(updateOrders);
    }


    @RequestMapping(value = "/markInstallmentForfited", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> markInstallmentForfited() throws VException {
        return instalmentManager.markInstallmentForfited();
    }


    @RequestMapping(value = "/markTrialOrderEnded", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MarkOrderEndedRes markTrialOrderEnded(@RequestBody Map<String, String> requestParams) throws VException, Throwable {
        logger.info("ENTRY:" + requestParams);
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        String entityId = requestParams.get("entityId");
        String entityType = requestParams.get("entityType");
        String userId = requestParams.get("userId");
        String entityOrderId = "";
        if (StringUtils.isNotEmpty(entityId) && StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(entityType)) {
            EntityType entityType1 = EntityType.valueOf(entityType);
            entityOrderId = paymentManager.markTrialOrderEnded(entityId, entityType1, userId);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "entityId not valid");
        }
        MarkOrderEndedRes markOrderEndedRes = new MarkOrderEndedRes();
        markOrderEndedRes.setOrderId(entityOrderId);
        return markOrderEndedRes;
    }


    @RequestMapping(value = "/getDueInstallment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Instalment> getDueInstallment() throws VException {

        return instalmentManager.getDueInstallment();
    }

    @RequestMapping(value = "/list/cards", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Card> getStoredCards(@RequestParam(value = "gateway", defaultValue = "JUSPAY") PaymentGatewayName gatewayAggregator,
                                     @RequestParam(value = "customer_id", required = false) String customerId) throws VException {
        Long userId = sessionUtils.getCallingUserId();
        logger.info("LIST CARDS REQ FROM " + userId);
        paymentManager.checkIfAllowed(userId, customerId);
        switch (gatewayAggregator) {
            case JUSPAY:
                JuspayPaymentManager paymentManager = (JuspayPaymentManager) this.paymentManager.getPaymentManger(PaymentGatewayName.JUSPAY);
                if (StringUtils.isEmpty(customerId)) {
                    throw new BadRequestException(ErrorCode.MISSING_PARAMETER, "Customer ID is empty for the request");
                }
                return paymentManager.getStoredCards(customerId);
            default:
                throw new VException(ErrorCode.SERVICE_ERROR, "Unknown Payment Gateway");
        }
    }

    @RequestMapping(value = "/list/wallets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Wallet> listWallets(@RequestParam(value = "gateway", defaultValue = "JUSPAY") PaymentGatewayName gatewayAggregator,
                                    @RequestParam(value = "customer_id", required = false) String customerId) throws VException {
        Long userId = sessionUtils.getCallingUserId();
        logger.info("LIST WALLET REQUEST FROM " + userId);
        paymentManager.checkIfAllowed(userId, customerId);
        switch (gatewayAggregator) {
            case JUSPAY:
                JuspayPaymentManager paymentManager = (JuspayPaymentManager) this.paymentManager.getPaymentManger(PaymentGatewayName.JUSPAY);
                if (StringUtils.isEmpty(customerId)) {
                    throw new BadRequestException(ErrorCode.MISSING_PARAMETER, "Customer ID is empty for the request");
                }
                return paymentManager.listWallets(customerId);
            default:
                throw new VException(ErrorCode.SERVICE_ERROR, "Unknown Payment Gateway");
        }
    }

    @RequestMapping(value = "/authenticate/wallet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Wallet authenticateWallet(@RequestBody WalletRequest walletRequest) throws VException {

        Long userId = sessionUtils.getCallingUserId();
        switch (walletRequest.getGateway()) {
            case JUSPAY:
                WalletPaymentMethod wallet = walletRequest.getWallet();
                if (!wallet.isJuspayDirectDebitSupported()) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "WALLET NOT SUPPORTED FOR LINKING");
                }
                JuspayPaymentManager paymentManager = (JuspayPaymentManager) this.paymentManager.getPaymentManger(PaymentGatewayName.JUSPAY);
                return paymentManager.authenticateWallet(userId, wallet, walletRequest.getMobileNo());
            default:
                throw new VException(ErrorCode.SERVICE_ERROR, "Unknown Payment Gateway");
        }
    }

    @RequestMapping(value = "/link/wallet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Wallet linkWallet(@RequestBody WalletRequest walletRequest) throws VException {
        switch (walletRequest.getGateway()) {
            case JUSPAY:
                JuspayPaymentManager paymentManager = (JuspayPaymentManager) this.paymentManager.getPaymentManger(PaymentGatewayName.JUSPAY);
                return paymentManager.linkWallet(walletRequest.getWalletId(), walletRequest.getOtp());
            default:
                throw new VException(ErrorCode.SERVICE_ERROR, "Unknown Payment Gateway");
        }
    }

    @RequestMapping(value = "/details/netbanking", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Map<String, String>> getNetbankingDetails(@RequestParam(value = "gateway", defaultValue = "JUSPAY") PaymentGatewayName gatewayAggregator) throws VException {
        switch (gatewayAggregator) {
            case JUSPAY:
                JuspayPaymentManager paymentManager = (JuspayPaymentManager) this.paymentManager.getPaymentManger(PaymentGatewayName.JUSPAY);
                return paymentManager.getNetbankingDetails();
            default:
                throw new VException(ErrorCode.SERVICE_ERROR, "Unknown Payment Gateway");
        }
    }

    @RequestMapping(value = "/details/juspay/customerid", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> getJuspayCustomerDetails(@RequestParam(value = "gateway", defaultValue = "JUSPAY") PaymentGatewayName gatewayAggregator) throws VException {
        Long userId = sessionUtils.getCallingUserId();

        switch (gatewayAggregator) {
            case JUSPAY:
                JuspayPaymentManager paymentManager = (JuspayPaymentManager) this.paymentManager.getPaymentManger(PaymentGatewayName.JUSPAY);
                return ImmutableMap.<String, String>builder().put("juspayCustomerId", paymentManager.getJuspayCustomerId(userId)).build();
            default:
                throw new VException(ErrorCode.SERVICE_ERROR, "Unknown Payment Gateway");
        }
    }

    @RequestMapping(value = "/details/offers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PaymentOffer> getJuspayCustomerDetails(@RequestBody PaymentOfferReq paymentOfferReq) throws VException {
        return paymentManager.getPaymentOffers(paymentOfferReq);
    }

    @RequestMapping(value = "/save/offers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getJuspayCustomerDetails(@RequestBody PaymentOffer paymentOffer) throws VException {
        sessionUtils.checkIfAllowedList(null, Collections.singletonList(Role.ADMIN), false);
        return paymentManager.createPaymentOffer(paymentOffer);
    }

    @RequestMapping(value = "/paymentmode", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, Object> getPaymentMode() throws VException {
        return paymentManager.getPaymentMode();
    }

    @RequestMapping(value = "/details/wallets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Map<String, String>> getWallets(@RequestParam(value = "gateway", defaultValue = "JUSPAY") PaymentGatewayName gatewayAggregator) throws VException {
        switch (gatewayAggregator) {
            case JUSPAY:
                JuspayPaymentManager paymentManager = (JuspayPaymentManager) this.paymentManager.getPaymentManger(PaymentGatewayName.JUSPAY);
                return paymentManager.getWalletDetails();
            default:
                throw new VException(ErrorCode.SERVICE_ERROR, "Unknown Payment Gateway");
        }
    }

    @RequestMapping(value = "/getPaymentMessageStrip", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PaymentMessageStrip> getPaymentMessageStrip() {
        PaymentMessageStrip messageStrip = paymentManager.getPaymentMessageStrip();
        return ResponseEntity.ok(messageStrip);
    }


    @RequestMapping(value = "/getLatestOrderByUserId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Orders getLatestOrderByUserId(@RequestParam(value = "userId", required = true) Long userId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.getLatestOrderByUserId(userId);
    }

    @RequestMapping(value = "/ccavenue/check/status", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void ccAvenueCheckStatus(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                    @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if ("SubscriptionConfirmation".equals(messageType)) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if ("Notification".equals(messageType)) {
            logger.info("Notification received - SNS - CCAVENUE CHECK STATUS");
            logger.info(subscriptionRequest.toString());
            paymentManager.checkGatewayStatus(PaymentGatewayName.CCAVENUE);
        }
    }

    @RequestMapping(value = "/updateBaseSubscription", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateBaseSubscription(@RequestBody List<BaseSubscriptionPaymentPlan> baseSubscriptionPaymentPlanReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        paymentManager.updateBaseInstalment(baseSubscriptionPaymentPlanReq);
    }

    @RequestMapping(value = "/getBaseSubscriptionPlan", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseSubscriptionPaymentPlan getBaseSubscriptionPlan(@RequestParam(value = "subscriptionId", required = true) String subscriptionId) {

        return paymentManager.getBaseSubscriptionPlan(subscriptionId);
    }

    @RequestMapping(value = "/getBaseSubscriptionPlanByDeliverableId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionPlanInfoResp getBaseSubscriptionPlanByDeliverableId(@RequestParam(value = "deliverableId", required = true) String deliverableId) {

        return paymentManager.getBaseSubscriptionPlanByDeliverableId(deliverableId);
    }


    @RequestMapping(value = "/getBaseSubscriptionPlanByBundles", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BaseSubscriptionPaymentPlan> getBaseSubscriptionPlanByBundles(@RequestBody List<String> bundleIds) {

        return paymentManager.getBaseSubscriptionPlanByBundles(bundleIds);
    }

    //migration
    @RequestMapping(value = "/syncOrdersWithLS", method = RequestMethod.GET)
    public void syncOrdersWithLS(@RequestParam(value = "orderIds") List<String> orderIds,
                                 HttpServletRequest servletRequest) throws VException {

        if (CollectionUtils.isNotEmpty(orderIds)) {
            if (orderIds.size() > 20) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Cannot process more than 20 ids at a time");
            }
            sessionUtils.isAllowedApp(servletRequest);
            paymentManager.syncOrdersWithLS(orderIds);
        }

    }

    //migration
    @RequestMapping(value = "/syncInstallmentsWithLS", method = RequestMethod.GET)
    public void syncInstallmentsWithLS(@RequestParam(value = "installmentIds") List<String> installmentIds,
                                       HttpServletRequest servletRequest) throws VException {

        if (CollectionUtils.isNotEmpty(installmentIds)) {
            if (installmentIds.size() > 20) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Cannot process more than 20 ids at a time");
            }
            sessionUtils.isAllowedApp(servletRequest);
            instalmentManager.syncInstallmentsWithLS(installmentIds);
        }

    }

    //migration
    @RequestMapping(value = "/syncTransactionsWithLS", method = RequestMethod.GET)
    public void syncTransactionsWithLS(@RequestParam(value = "transactionIds") List<String> transactionIds,
                                       HttpServletRequest servletRequest) throws VException {

        if (CollectionUtils.isNotEmpty(transactionIds)) {
            if (transactionIds.size() > 20) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Cannot process more than 20 ids at a time");
            }
            sessionUtils.isAllowedApp(servletRequest);
            paymentManager.syncTransactionsWithLS(transactionIds);
        }
    }

    @RequestMapping(value = "/getEMICards", method = RequestMethod.POST)
    public List<EMICardResp> getEMICards(@RequestBody GetEmiCardReq getEmiCardReq) throws VException {

        getEmiCardReq.verify();
        return paymentManager.getEMICards(getEmiCardReq.getAmount());

    }

    @RequestMapping(value = "/getLatestTrialOrderByUserId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Orders getLatestTrialOrderByUserId(@RequestParam(value = "userId", required = true) Long userId,
                                              @RequestParam(value = "startTime", required = true) Long startTime, HttpServletRequest request) throws VException {
        sessionUtils.isAllowedApp(request);
        return paymentManager.getLatestTrialOrderByUserId(userId, startTime);
    }

    @RequestMapping(value = "/emiCard", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void saveEmiCard(@RequestBody List<EmiCard> emiCards) throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        paymentManager.emiCardInsert(emiCards);
    }

    @RequestMapping(value = "/emiCard", method = RequestMethod.GET)
    public List<EmiCard> getActiveEmiCard() throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.getEmiCards();
    }

    @RequestMapping(value = "/bankNames", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BankDetail> getBankNames(@RequestParam(value = "code", required = true) EmiCardType code, HttpServletRequest request) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("inside payment controller for bank names");
        return paymentManager.getBankNames(code);
    }

    @RequestMapping(value = "/emiCard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EmiCard> getEmiCard(@RequestParam(value = "code", defaultValue = "") String code, @RequestParam(value = "type", defaultValue = "") EmiCardType type) throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return paymentManager.getEmiCardsByCodeAndType(type, code);
    }

    @RequestMapping(value = "/exportOrdersCsv", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse exportOrdersCsv(@RequestBody ExportOrdersCsvReq exportOrdersCsvReq) throws VException, UnsupportedEncodingException {
        exportOrdersCsvReq.verify();
        exportOrdersCsvReq.setCallingUserId(sessionUtils.getCallingUserId());
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        awsSNSManager.triggerSNS(SNSTopic.EXPORT_ORDERS_CSV_SNS, "EXPORT_ORDERS_CSV_SNS", new Gson().toJson(exportOrdersCsvReq));
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/exportOrdersCsvFromSNS", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void exportOrdersCsvFromSNS(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if ("SubscriptionConfirmation".equals(messageType)) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if ("Notification".equals(messageType)) {
            logger.info("Notification received - SNS - EXPORT ORDERS CSV");
            logger.info(subscriptionRequest.toString());
            String message = subscriptionRequest.getMessage();
            paymentManager.exportOrdersCsvHandler(message);
        }
    }
}
