package com.vedantu.dinero.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.vedantu.util.security.HttpSessionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.entity.UpdateSessionPayoutRequest;
import com.vedantu.dinero.managers.PayoutManager;
import com.vedantu.dinero.request.SessionPayoutRequest;
import com.vedantu.dinero.response.ExportDailyTransactionsResponse;
import com.vedantu.dinero.response.GetMonthlyPayoutResponse;
import com.vedantu.dinero.response.GetTeacherSessionInfoResponse;
import com.vedantu.dinero.response.GetTotalPayoutResponse;
import com.vedantu.dinero.response.SessionPayoutResponse;
import com.vedantu.dinero.response.UpdateSessionPayoutResponse;
import com.vedantu.dinero.sql.entity.SessionPayout;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/payout")
public class PayoutController {

    @Autowired
    private PayoutManager payoutManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PayoutController.class);

    /*
     * API to get calculate Session Payout
     *
     * @return json A json with status and Payout info
     */
    @RequestMapping(value = "/calculateSessionPayout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionPayoutResponse calculateSessionPayout(@RequestBody SessionPayoutRequest sessionPayoutRequest, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return payoutManager.calculateSessionPayout(sessionPayoutRequest);
    }

    /*
     * API to get update a miscalculated session payout due to duration
     *
     * @return json A json with status and Payout info
     */
    @RequestMapping(value = "/updateSessionPayout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UpdateSessionPayoutResponse updateSessionPayout(@RequestBody UpdateSessionPayoutRequest updateSessionPayoutRequest, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return payoutManager.updateSessionPayout(updateSessionPayoutRequest);
    }

    // Migration API - dont need it anymore
    /*
     * API to get recalculate Session Payout for ended Subscriptions
     *
     * @return json A json with status and Payout info
     */
    // @RequestMapping(value = "/recalculateSessionPayoutEndedSubscriptions",
    // method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE,
    // produces = MediaType.APPLICATION_JSON_VALUE)
    // @ResponseBody
    // public BaseResponse
    // recalculateSessionPayoutEndedSubscriptions(@RequestBody
    // SessionPayoutRequest sessionPayoutRequest)
    // {
    // return
    // payoutManager.recalculateSessionPayoutEndedSubscriptions(sessionPayoutRequest);
    // }

    /*
     * API to get total payout for a teacher for all months
     *
     * @return json A json with status
     */
    @CrossOrigin
    @RequestMapping(value = "/getTotalTeacherPayout", method = RequestMethod.GET)
    @ResponseBody
    public GetTotalPayoutResponse getTotalTeacherPayout(@RequestParam(value = "teacherId", required = true) Long teacherId, HttpServletRequest httpServletRequest) throws VException, ParseException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return payoutManager.getTotalTeacherPayout(teacherId);
    }

    /*
     * API to get monthly payout for a teacher for all months
     *
     * @return json A json with status
     */
    @CrossOrigin
    @RequestMapping(value = "/getMonthlyTeacherPayout", method = RequestMethod.GET)
    @ResponseBody
    public GetMonthlyPayoutResponse getMonthlyTeacherPayout(@RequestParam(value = "teacherId", required = true) Long teacherId, @RequestParam(value = "month", required = true) int month) throws VException, ParseException {
        return payoutManager.getMonthlyTeacherPayout(teacherId, month);
    }

    /*
     * API to export teacher payout to a CSV
     *
     * @return json A json with status
     */
    @CrossOrigin
    @RequestMapping(value = "/exportTransactions", method = RequestMethod.GET)
    @ResponseBody
    public ExportDailyTransactionsResponse exportTransactions(@RequestParam(value = "startDate", required = false) Long startDate, @RequestParam(value = "endDate", required = false) Long endDate, HttpServletRequest httpServletRequest) throws VException, ParseException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return payoutManager.exportTransactions(startDate, endDate);
    }

    /*
     * API to export daily transactions for finance
     *
     * @return json A json with status
     */
    @CrossOrigin
    @RequestMapping(value = "/getDailyTransactions", method = RequestMethod.GET)
    @ResponseBody
    public List<Transaction> getDailyTransactions() throws VException {
        return payoutManager.getDailyTransactions();
    }

    // Migration API
    /*
     * API to export teacher session Info for migration
     *
     * @return json A json with status
     */
    @RequestMapping(value = "/getTeacherSessionInfos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetTeacherSessionInfoResponse getTeacherSessionInfos(@RequestParam(value = "startDate", required = true) Long startDate, @RequestParam(value = "endDate", required = true) Long endDate, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return payoutManager.getTeacherSessionInfos(startDate, endDate);
    }

    /*
     * API to upload teacher payout rate table from AIR
     *
     * @return json A json with status
     */
    @CrossOrigin
    @RequestMapping(value = "/uploadTeacherPayout", method = RequestMethod.POST)
    @ResponseBody
    public Boolean uploadTeacherPayout(@RequestParam("file") MultipartFile file, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        File uploadedFile = handleFileUpload(file);
        if (uploadedFile == null) {
            logger.error("Empty File");
            return false;
        } else {
            return payoutManager.uploadTeacherPayout(uploadedFile);
        }
    }

    @CrossOrigin
    @RequestMapping(value = "getPayoutsBySessionIds", method = RequestMethod.GET)
    @ResponseBody
    public Map<Long, SessionPayout> getPayoutsBySessionIds(@RequestParam(value = "ids") List<Long> ids) throws VException {
        logger.info("Request received for getting Payouts by sessionIds: " + ids);
        Map<Long, SessionPayout> response = payoutManager.getPayoutsBySessionIds(ids);
        logger.info("Response:" + response.toString());
        return response;
    }


    private File handleFileUpload(MultipartFile file) {
        if (!file.isEmpty()) {
            BufferedOutputStream stream = null;
            try {
                File uploadedFile = File.createTempFile(UUID.randomUUID().toString(), ".csv");
                byte[] bytes = file.getBytes();
                stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                return uploadedFile;
            } catch (Exception e) {
            } finally {
                IOUtils.closeQuietly(stream);
            }
        }
        return null;
    }


    @RequestMapping(value = "/insertSessionPayout", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionPayoutResponse insertSessionPayout(@RequestBody SessionPayout sessionPayoutRequest) throws VException {
        return payoutManager.insertSessionPayout(sessionPayoutRequest);
    }

}
