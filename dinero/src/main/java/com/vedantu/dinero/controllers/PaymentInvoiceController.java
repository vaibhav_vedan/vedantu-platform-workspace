package com.vedantu.dinero.controllers;

import java.util.List;

import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.vedantu.dinero.entity.PaymentInvoiceTypeDetails;
import com.vedantu.dinero.managers.PaymentInvoiceManager;
import com.vedantu.dinero.pojo.PaymentInvoiceBasicInfo;
import com.vedantu.dinero.pojo.PaymentInvoiceInfo;
import com.vedantu.dinero.request.AddPaymentInvoiceReq;
import com.vedantu.dinero.request.CreatePaymentInvoiceReq;
import com.vedantu.dinero.request.GetPaymentInvoiceReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.util.PlatformBasicResponse;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/invoice")
public class PaymentInvoiceController {

    @Autowired
    private PaymentInvoiceManager paymentInvoiceManager;

    @Autowired
    private HttpSessionUtils sessionUtils;


    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public PaymentInvoiceInfo getInvoiceById(@PathVariable("id") String invoiceId) throws VException {
        return paymentInvoiceManager.getPaymentInvoiceInfoById(invoiceId);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET)
    @ResponseBody
    public List<PaymentInvoiceBasicInfo> getInvoices(@ModelAttribute GetPaymentInvoiceReq getPaymentInvoiceReq, HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentInvoiceManager.getPaymentInvoicesBasicInfos(getPaymentInvoiceReq);
    }

    @RequestMapping(value = "/type/update", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updatePaymentInvoiceTypeDetails(
            @RequestBody PaymentInvoiceTypeDetails paymentInvoiceTypeDetails) throws VException {
        return paymentInvoiceManager.updatePaymentInvoiceTypeDetails(paymentInvoiceTypeDetails);
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse createPaymentInvoiceTypeDetails(@RequestBody AddPaymentInvoiceReq req, HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentInvoiceManager.createPaymentInvoice(req);
    }

    @RequestMapping(value = "generateInvoiceNumbers", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse generateInvoiceNumbers(@RequestParam(value = "start") Integer start,
            @RequestParam(value = "size") Integer size,
            @RequestParam(value = "all") Boolean all) throws Exception {
        start = (start == null) ? 0 : start;
        size = (size != null && size != 0) ? size : 100;

        if (all == null) {
            all = true;
        }

        paymentInvoiceManager.generateInvoiceNumbers(start, size, all);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "generateStudentInvoice", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse generateStudentInvoice(CreatePaymentInvoiceReq createPaymentInvoiceReq) throws Exception {
		paymentInvoiceManager.createStudentPaymentInvoice(createPaymentInvoiceReq);
        return new PlatformBasicResponse();
    }

}
