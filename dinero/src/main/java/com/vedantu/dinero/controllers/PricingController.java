package com.vedantu.dinero.controllers;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.UUID;

import com.vedantu.util.security.HttpSessionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vedantu.dinero.entity.Plan;
import com.vedantu.dinero.managers.PricingManager;
import com.vedantu.dinero.request.ApprovePricingChangeRequest;
import com.vedantu.dinero.request.CreatePricingChangeRequest;
import com.vedantu.dinero.request.CreateSlabRangesRequest;
import com.vedantu.dinero.request.GetAllPricingChangeRequestsReq;
import com.vedantu.dinero.request.UpdateCustomPlanRequest;
import com.vedantu.dinero.response.ApprovePricingChangeResponse;
import com.vedantu.dinero.response.GetAllRequestsResponse;
import com.vedantu.dinero.response.GetPlanByIdResponse;
import com.vedantu.dinero.response.GetPricingChangeRequestsResponse;
import com.vedantu.dinero.response.GetTeacherPlansResponse;
import com.vedantu.dinero.response.GetTeacherSlabsResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.BaseResponse;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/pricing")
public class PricingController {

	@Autowired
	private PricingManager pricingManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PricingController.class);

	@Autowired
	private HttpSessionUtils sessionUtils;

	/*
	 * API to get create a slab range
	 * 
	 * @return json A json with status
	 */
	@RequestMapping(value = "/setSlabRanges", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse setSlabRanges(@RequestBody CreateSlabRangesRequest createSlabRangesRequest, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.createSlabRanges(createSlabRangesRequest);
	}

	/*
	 * API to create a new pricing change Request
	 * 
	 * @return json A json with the status
	 */
	@RequestMapping(value = "/createPricingChangeRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse createPricingChangeRequest(@RequestBody CreatePricingChangeRequest createPricingChangeRequest, HttpServletRequest httpServletRequest)
			throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		try {
			createPricingChangeRequest.verify();
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, e.getMessage());
		}
		return pricingManager.createPricingChangeRequest(createPricingChangeRequest);
	}

	/*
	 * API to create a custom plans
	 * 
	 * @return json A json with the status
	 */
	@RequestMapping(value = "/updateCustomPlan", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Plan updateCustomPlan(@RequestBody UpdateCustomPlanRequest updatePlanReq, HttpServletRequest httpServletRequest)
			throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		try {
			updatePlanReq.verify();
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, e.getMessage());
		}
		return pricingManager.updateCustomPlanRequest(updatePlanReq);
	}

	/*
	 * API to create a new pricing change Request
	 * 
	 * @return json A json with the status
	 */
	@RequestMapping(value = "/approvePricingChangeRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ApprovePricingChangeResponse approvePricingChangeRequest(@RequestBody ApprovePricingChangeRequest approvePricingChangeRequest, HttpServletRequest httpServletRequest)
			throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		try {
			approvePricingChangeRequest.verify();
		} catch (IllegalArgumentException e) {
			logger.error(e.getMessage());
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, e.getMessage());
		}
		return pricingManager.approvePricingChangeRequest(approvePricingChangeRequest);
	}

	/*
	 * API to create a get pricing change Requests for a teacher since last
	 * numberOfDays
	 * 
	 * @return json A json with the list of pricingChangeRequests
	 */
	@RequestMapping(value = "/getPricingChangeRequests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public  GetPricingChangeRequestsResponse getPricingChangeRequests(@RequestParam Long teacherId, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.getPricingChangeRequests(teacherId);
	}


	/*
	 * API to create teacher plans directly without requests, do not expose as fos/app engine api
	 * 
	 * @return json Base response
	 */        
	@RequestMapping(value = "/createTeacherPlans", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public  BaseResponse createTeacherPlans(@RequestBody CreatePricingChangeRequest req, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.createPricingPlans(req);
	}
        
        
	/*
	 * API to get all teacher Slabs
	 * 
	 * @return json A json with array with the slab range min values
	 */
	@RequestMapping(value = "/getTeacherSlabs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetTeacherSlabsResponse getTeacherSlabs(@RequestParam Long teacherId, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.getTeacherSlabs(teacherId);
	}

	/*
	 * API to get all teacher Plans
	 * 
	 * @return json A json with array with the slab range min values
	 */
	@CrossOrigin
	@RequestMapping(value = "/getTeacherPlans", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetTeacherPlansResponse getTeacherPlans(@RequestParam Long teacherId, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.getTeacherPlans(teacherId);
	}

	/*
	 * API to get PlanById
	 * 
	 * @return json A json plan
	 */
	@RequestMapping(value = "/getPlanById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetPlanByIdResponse getPlanById(@RequestParam String id, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.getPlanById(id);
	}

	/*
	 * API to get PlansByIds
	 * 
	 * @return json A json with plans
	 */
	@RequestMapping(value = "/getPlansByIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetTeacherPlansResponse getPlansByIds(@RequestParam List<String> ids, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.getPlansByIds(ids);
	}

	/*
	 * API to get PlanById
	 * 
	 * @return json A json plan
	 */
	@RequestMapping(value = "/getPlanByHours", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetPlanByIdResponse getPlanByHours(@RequestParam Long teacherId, @RequestParam Double hours, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.getPlanByHours(teacherId, hours);
	}

	/*
	 * API to upload incentive table from AIR
	 * 
	 * @return json A json with status
	 */
	@CrossOrigin
	@RequestMapping(value = "/uploadTeacherIncentiveTable", method = RequestMethod.POST)
	@ResponseBody
	public Boolean uploadTeacherIncentiveTable(@RequestParam("file") MultipartFile file, HttpServletRequest httpServletRequest) throws VException {

		sessionUtils.isAllowedApp(httpServletRequest);
		File uploadedFile = handleFileUpload(file);

		if (uploadedFile == null) {
			logger.error("Empty uploaded File");
			return false;
		} else {
			return pricingManager.processIncentiveData(uploadedFile);
		}
	}

	/*
	 * API to upload teacher payout rate table from AIR
	 * 
	 * @return json A json with status
	 */
	@CrossOrigin
	@RequestMapping(value = "/uploadTeacherPayoutRateTable", method = RequestMethod.POST)
	@ResponseBody
	public Boolean uploadTeacherPayoutRateTable(@RequestParam("file") MultipartFile file, HttpServletRequest httpServletRequest) throws VException {

		sessionUtils.isAllowedApp(httpServletRequest);
		File uploadedFile = handleFileUpload(file);

		if (uploadedFile == null) {
			logger.error("Empty File");
			return false;
		} else {
			return pricingManager.processTeacherPayoutRate(uploadedFile);
		}
	}

	/*
	 * API to get all pending requests
	 * 
	 * @return json A json with request list
	 */
	@RequestMapping(value = "/getAllRequests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetAllRequestsResponse getAllRequests(GetAllPricingChangeRequestsReq req, HttpServletRequest httpServletRequest) throws VException {
		sessionUtils.isAllowedApp(httpServletRequest);
		return pricingManager.getAllRequests(req.getStart(), req.getSize());
	}

	private File handleFileUpload(MultipartFile file) {
		if (!file.isEmpty()) {
			BufferedOutputStream stream = null;
			try {
				File uploadedFile = File.createTempFile(UUID.randomUUID().toString(), ".csv");
				byte[] bytes = file.getBytes();
				stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
				stream.write(bytes);
				return uploadedFile;
			} catch (Exception e) {
			} finally {
				IOUtils.closeQuietly(stream);
			}
		}
		return null;

	}
}
