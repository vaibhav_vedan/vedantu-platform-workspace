package com.vedantu.dinero.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dinero.util.RedisDAO;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSSubject;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/misc")
public class MiscController {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private RedisDAO redisDAO;

    private Logger logger = LogFactory.getLogger(MiscController.class);

    @ApiOperation(value = "performRedisOps", notes = "for other services to perform ops on scheduling redis cluster keys")
    @RequestMapping(value = "/performRedisOps", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void performRedisOps(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest req = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + req.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(req.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification perform redis operations received - SNS");
            SNSSubject subject = SNSSubject.valueOf(req.getSubject());
            String message = req.getMessage();
            try {
                redisDAO.performOps(subject, message);
            } catch (InternalServerErrorException e) {
                logger.warn("error while performing redis ops for {}", req);
            }
        }
    }
}
