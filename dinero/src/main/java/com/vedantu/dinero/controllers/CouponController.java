package com.vedantu.dinero.controllers;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.dinero.entity.Coupon;
import com.vedantu.dinero.entity.RedeemedCoupon;
import com.vedantu.dinero.managers.CouponManager;
import com.vedantu.dinero.request.*;
import com.vedantu.dinero.response.ApplyCouponRes;
import com.vedantu.dinero.response.CheckCouponAvailibilityRes;
import com.vedantu.dinero.response.CouponInfoRes;
import com.vedantu.dinero.response.GetCouponsRes;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/coupons")
public class CouponController {

    @Autowired
    private CouponManager couponManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CouponController.class);

    @RequestMapping(value = "/applyCreditCoupon", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ApplyCouponRes applyCreditCoupon(@RequestBody ApplyCouponReq applyCouponReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        applyCouponReq.verify();
        return couponManager.applyCreditCoupon(applyCouponReq);
    }

    //migrated from platform
    @RequestMapping(value = "/v2/applyCreditCoupon", method = RequestMethod.POST)
    @ResponseBody
    public String applyCreditCoupon_v2(@RequestBody ApplyCouponReq applyCouponReq)
            throws VException, IOException {
        sessionUtils.checkIfAllowed(applyCouponReq.getCallingUserId(), null, Boolean.FALSE);
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (httpSessionData.getRole().equals(Role.STUDENT)) {
            applyCouponReq.setCallingUserId(httpSessionData.getUserId());
        }
        applyCouponReq.verify();
        ApplyCouponRes response = couponManager.applyCreditCoupon(applyCouponReq);
        return new Gson().toJson(response);
    }

    @RequestMapping(value = "/isCouponCodeAvailable", method = RequestMethod.GET)
    @ResponseBody
    public CheckCouponAvailibilityRes isCouponCodeAvailable(CheckCouponAvailibilityReq checkCouponAvailibilityReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        checkCouponAvailibilityReq.verify();
        return couponManager.isCouponCodeAvailable(checkCouponAvailibilityReq);
    }

    //migrated from platform
    @RequestMapping(value = "/v2/isCouponCodeAvailable", method = RequestMethod.GET)
    @ResponseBody
    public String isCouponCodeAvailable_v2(CheckCouponAvailibilityReq request)
            throws VException, IOException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        request.verify();
        CheckCouponAvailibilityRes resp = couponManager.isCouponCodeAvailable(request);
        return new Gson().toJson(resp);
    }

    @RequestMapping(value = "/createCoupon", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CouponInfoRes createCoupon(@RequestBody @Valid CreateCouponReq createCouponReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        createCouponReq.verify();
        return couponManager.createCoupon(createCouponReq);
    }

    // migrated from platform
    @RequestMapping(value = "/v2/createCoupon", method = RequestMethod.POST)
    @ResponseBody
    public String createCouponV2(@RequestBody CreateCouponReq request)
            throws VException, IOException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
//        CreateCouponReq request = new Gson().fromJson(req.toString(), CreateCouponReq.class);
        request.verify();
        CouponInfoRes response = couponManager.createCoupon(request);
        return new Gson().toJson(response);
    }

    @RequestMapping(value = "/editCoupon", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CouponInfoRes editCoupon(@RequestBody @Valid EditCouponReq editCouponReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        editCouponReq.verify();
        return couponManager.editCoupon(editCouponReq);
    }

    //migrated from platform
    @RequestMapping(value = "/v2/editCoupon", method = RequestMethod.POST)
    @ResponseBody
    public String editCouponV2(@RequestBody EditCouponReq request)
            throws VException, IOException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
//        EditCouponReq request = new Gson().fromJson(req.toString(), EditCouponReq.class);
        request.verify();
        CouponInfoRes response = couponManager.editCoupon(request);
        return new Gson().toJson(response);
    }

    @RequestMapping(value = "/getCoupons", method = RequestMethod.GET)
    @ResponseBody
    public GetCouponsRes getCoupons(GetCouponsReq getCouponsReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return couponManager.getCoupons(getCouponsReq);
    }

    //migrated from platform
    @RequestMapping(value = "/v2/getCoupons", method = RequestMethod.GET)
    @ResponseBody
    public String getCoupons_v2(GetCouponsReq request)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        GetCouponsRes resp = couponManager.getCoupons(request);
        return new Gson().toJson(resp);
    }
    
    @RequestMapping(value = "/getPublicCoupons", method = RequestMethod.GET)
    @ResponseBody
    public GetCouponsRes getPublicCoupons(GetCouponsReq getCouponsReq) throws VException {
        return couponManager.getPublicCoupons(getCouponsReq);
    }    

    @RequestMapping(value = "/checkValidity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ValidateCouponRes checkValidity(@RequestBody ValidateCouponReq validateCouponReq, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        validateCouponReq.verify();
        return couponManager.checkValidity(validateCouponReq);
    }

    //migrated from platform
    @RequestMapping(value = "/v2/checkValidity", method = RequestMethod.POST)
    @ResponseBody
    public String checkValidity(@RequestBody Object req)
            throws VException, IOException {
        ValidateCouponReq request = new Gson().fromJson(req.toString(), ValidateCouponReq.class);
        ValidateCouponRes response = couponManager.checkValidity(request);
        return new Gson().toJson(response);
    }

    @RequestMapping(value = "/processCoupon", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object processCoupon(@RequestBody ProcessCouponReq req, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        req.verify();
        return couponManager.processCoupon(req.getCouponCode(), req.getUserId(), req.getCredit());
    }

    @RequestMapping(value = "/addRedeemEntry", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RedeemedCoupon addRedeemEntry(@RequestBody AddCouponRedeemEntryReq req, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        req.verify();
        return couponManager.addRedeemEntry(req);
    }


    @RequestMapping(value = "/getPassRedeemInfo", method = RequestMethod.GET)
    @ResponseBody
    public List<RedeemedCoupon> getPassRedeemInfo(GetPassRedeemInfoReq req, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        req.verify();
        return couponManager.getPassRedeemInfo(req);
    }


    @RequestMapping(value = "/markPassProcessingState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markPassProcessingState(@RequestBody MarkPassProcessingStateReq req, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        req.verify();
        return couponManager.markPassProcessingState(req);
    }

    @RequestMapping(value = "/getRedeemCouponInfoByRefId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RedeemedCoupon getRedeemCouponInfoByRefId(@RequestParam(value = "refId", required = true) String refId, HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return couponManager.getRedeemCouponInfoByRefId(refId);
    }
    
    @RequestMapping(value="/getCouponsByIds", method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Coupon> getCouponByIds(@RequestBody List<String> ids) throws VException{
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return couponManager.getCouponByIds(ids);
    }

    @RequestMapping(value="/getCouponsByIdsApp", method=RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Coupon> getCouponsByIdsApp(@RequestBody List<String> ids,HttpServletRequest httpRequest) throws VException{
        sessionUtils.isAllowedApp(httpRequest);
        return couponManager.getCouponByIds(ids);
    }


}
