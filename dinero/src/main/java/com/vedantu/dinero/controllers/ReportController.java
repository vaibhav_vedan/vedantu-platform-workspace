package com.vedantu.dinero.controllers;

import java.util.List;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.vedantu.dinero.entity.ExtTransaction;
import com.vedantu.dinero.entity.Transaction;
import com.vedantu.dinero.managers.AccountManager;
import com.vedantu.dinero.managers.PaymentManager;
import com.vedantu.dinero.managers.PayoutManager;
import com.vedantu.dinero.managers.ReportManager;
import com.vedantu.dinero.pojo.FailedPayoutInfo;
import com.vedantu.dinero.pojo.TransactionUserInfo;
import com.vedantu.dinero.pojo.WalletStatusInfo;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;

import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private PayoutManager payoutManager;

    @Autowired
    private AccountManager accountManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private ReportManager reportManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ReportController.class);

    @Deprecated
    @RequestMapping(value = "/getFailedPayoutsForEndedSessions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<FailedPayoutInfo> getFailedPayoutsForEndedSessions(HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return payoutManager.getFailedPayoutsForEndedSessions();
    }

    @Deprecated
    @RequestMapping(value = "/getVedantuWalletsStatus", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<WalletStatusInfo> getVedantuWalletsStatus(HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return accountManager.getVedantuWalletsStatus();
    }

    @Deprecated
    @RequestMapping(value = "/pendingExtTransactions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ExtTransaction> pendingExtTransactions(HttpServletRequest httpServletRequest) throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return paymentManager.pendingExtTransactions();
    }

    @Deprecated
    @RequestMapping(value = "/getTransactionsToVedantuWallets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Transaction> getTransactionsToVedantuWallets(@RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "toTime") Long toTime, HttpServletRequest httpServletRequest) throws VException {
        //TODO: please place the fromTime and toTIme checks if you want Use
        logger.info("fromTime: " + fromTime + " toTime: " + toTime);
        sessionUtils.isAllowedApp(httpServletRequest);
        return reportManager.getTransactionsToVedantuWallets(fromTime, toTime);
    }

    @Deprecated
    @RequestMapping(value = "/getTransactionsFromVedantuWallets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TransactionUserInfo> getTransactionsFromVedantuWallets(@RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "toTime") Long toTime, HttpServletRequest httpServletRequest) throws VException {
        logger.info("fromTime: " + fromTime + " toTime: " + toTime);
        sessionUtils.isAllowedApp(httpServletRequest);
        return reportManager.getTransactionsFromVedantuWalletsList(fromTime, toTime);
    }

    @Deprecated
    @RequestMapping(value = "/getExternalTransactionsDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TransactionUserInfo> getExternalTransactionsDetails(@RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "toTime") Long toTime, HttpServletRequest httpServletRequest) throws VException {
        logger.info("fromTime: " + fromTime + " toTime: " + toTime);
        sessionUtils.isAllowedApp(httpServletRequest);
        return reportManager.getExternalTransactionsDetailsList(fromTime, toTime);
    }


    @Deprecated
    @RequestMapping(value = "/getSubscriptionTransactionsDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TransactionUserInfo> getSubscriptionTransactionsDetails(@RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "toTime") Long toTime, HttpServletRequest httpServletRequest) throws VException {
        logger.info("fromTime: " + fromTime + " toTime: " + toTime);
        sessionUtils.isAllowedApp(httpServletRequest);
        return reportManager.getSubscriptionTransactionsDetailsList(fromTime, toTime);
    }

    @Deprecated
    @RequestMapping(value = "/getTeacherPayoutTransactionsDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TransactionUserInfo> getTeacherPayoutTransactionsDetails(@RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "toTime") Long toTime, HttpServletRequest httpServletRequest) throws VException {
        logger.info("fromTime: " + fromTime + " toTime: " + toTime);
        sessionUtils.isAllowedApp(httpServletRequest);
        return reportManager.getTeacherPayoutTransactionsDetailsList(fromTime, toTime);
    }

    @Deprecated
    @RequestMapping(value = "/getVedantuCutTransactionsDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TransactionUserInfo> getVedantuCutTransactionsDetails(@RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "toTime") Long toTime, HttpServletRequest httpServletRequest) throws VException {
        logger.info("fromTime: " + fromTime + " toTime: " + toTime);
        sessionUtils.isAllowedApp(httpServletRequest);
        return reportManager.getVedantuCutTransactionsDetailsList(fromTime, toTime);
    }

    @RequestMapping(value = "/get/ext-transaction/status/alert", method = RequestMethod.POST, consumes = MediaType.ALL_VALUE,
            produces = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void getExtTransactionStatusAlert(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException, AddressException {

        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if ("SubscriptionConfirmation".equals(messageType)) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if ("Notification".equals(messageType)) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            reportManager.getExtTransactionStatusAlert();
        }
    }
}
