/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.controllers;

import com.vedantu.dinero.managers.RefundManager;
import com.vedantu.dinero.request.RefundReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.exception.VException;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/refund")
public class RefundController {

    @Autowired
    private RefundManager refundManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RefundRes refund(@RequestBody RefundReq refundReq, HttpServletRequest httpServletRequest)
            throws VException {
        sessionUtils.isAllowedApp(httpServletRequest);
        return refundManager.refund(refundReq);
    }
}
