package com.vedantu.dinero.enums;

public enum SessionEntity {

	SESSION, BATCH, SUBSCRIPTION
}
