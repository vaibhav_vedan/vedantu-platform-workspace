package com.vedantu.dinero.enums;

import com.vedantu.dinero.entity.Coupon;
import com.vedantu.exception.*;

public enum CouponType {

    CREDIT {

//		@Autowired
//		private AccountManager accountManager;
//		
//		@Autowired
//		private RedeemedCouponDAO redeemedCouponDAO;
        @Override
        public Object applyCoupon(Coupon coupon, Long userId, Object... objs) throws ForbiddenException,
                BadRequestException, NotFoundException, ConflictException, InternalServerErrorException {

//			Integer redeemValue = coupon.getRedeemValue();
//			RedeemedCoupon redeemedCoupon = new RedeemedCoupon(coupon.getCode(), userId, redeemValue, null, null);
//			
//			redeemedCouponDAO.create(redeemedCoupon);
//
//			Long toUserId = userId;
//			TransferFromFreebiesAccountReq req = new TransferFromFreebiesAccountReq(toUserId, redeemValue,
//					TransactionRefType.COUPON_CREDIT, BillingReasonType.FREEBIES, false, true, redeemedCoupon.getId().toString());
//
//			req.setCallingUserId(toUserId);
//
//			TransferFromFreebiesAccountRes response = accountManager.transferFromFreebiesAccount(req);
//
//			return response;
            return null;
        }

        @Override
        public Integer getRedeemValue(Coupon coupon, Integer totalAmount)
                throws ForbiddenException, BadRequestException, NotFoundException, ConflictException {
            return _getRedeemValue(coupon, totalAmount);
        }
    },
    TEACHER_DISCOUNT {
        @Override
        public Object applyCoupon(Coupon coupon, Long userId, Object... objs)
                throws ForbiddenException, BadRequestException, NotFoundException, ConflictException {
            return null;
        }

        @Override
        public Integer getRedeemValue(Coupon coupon, Integer totalAmount)
                throws ForbiddenException, BadRequestException, NotFoundException, ConflictException {
            return _getRedeemValue(coupon, totalAmount);
        }
    },
    PASS {
        @Override
        public Object applyCoupon(Coupon coupon, Long userId, Object... objs)
                throws ForbiddenException, BadRequestException, NotFoundException, ConflictException {
            return null;
        }

        @Override
        public Integer getRedeemValue(Coupon coupon, Integer totalAmount)
                throws ForbiddenException, BadRequestException, NotFoundException, ConflictException {
            return 0;
        }
    },
    VEDANTU_DISCOUNT {
        @Override
        public Object applyCoupon(Coupon coupon, Long userId, Object... objs)
                throws ForbiddenException, BadRequestException, NotFoundException, ConflictException {
            return null;
        }

        @Override
        public Integer getRedeemValue(Coupon coupon, Integer totalAmount)
                throws ForbiddenException, BadRequestException, NotFoundException, ConflictException {
            return _getRedeemValue(coupon, totalAmount);
        }
    };

    public abstract Object applyCoupon(Coupon coupon, Long userId, Object... objs) throws ForbiddenException,
            BadRequestException, NotFoundException, ConflictException, InternalServerErrorException;

    public abstract Integer getRedeemValue(Coupon coupon, Integer totalAmount)
            throws ForbiddenException, BadRequestException, NotFoundException, ConflictException;

    public Integer _getRedeemValue(Coupon coupon, Integer totalAmount) throws ConflictException {
        int discount = 0;
        if (coupon.getRedeemType().equals(RedeemType.FIXED)) {
            discount = coupon.getRedeemValue().intValue();
        } else if (coupon.getRedeemType().equals(RedeemType.PERCENTAGE)) {
            discount = (int) (totalAmount * coupon.getRedeemValue() / 100);
        } else if (coupon.getRedeemType().equals(RedeemType.FLAT_PRICE)) {
            if (totalAmount > coupon.getRedeemValue()) {
                discount = totalAmount - coupon.getRedeemValue().intValue();
            }
        }
        if (coupon.getMaxRedeemValue() != null && coupon.getMaxRedeemValue() > 0
                    && discount > coupon.getMaxRedeemValue()) {

            if (RedeemType.FLAT_PRICE == coupon.getRedeemType()) {
                throw new ConflictException(ErrorCode.DISCOUNT_EXCEEDS_TOTAL_AMOUNT,
                        "Not valid on this order as it exceeds the max redeemable value.");
            }
            discount = coupon.getMaxRedeemValue();
        }
        return discount;
    }
}
