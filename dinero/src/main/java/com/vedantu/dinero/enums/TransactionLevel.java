package com.vedantu.dinero.enums;

public enum TransactionLevel {
	OFF, DEBIT, CREDIT, ALL
}
