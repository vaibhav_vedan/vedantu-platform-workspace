package com.vedantu.dinero.enums;

public enum VerificationCodeStatus {
	ACTIVE, EXPIRED, USED;
}
