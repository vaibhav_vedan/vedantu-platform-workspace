package com.vedantu.dinero.enums;

public enum PaymentSource {

	MOBILE_WEB("MOBILE_WEB"),
	MOBILE_APP("MOBILE_APP"),
	DESKTOP_APP("DESKTOP_APP"),
	DESKTOP_WEB("DESKTOP_WEB"),
	IOS_WEB("IOS_WEB"),
	IOS_APP("IOS_APP"),
	SOURCE_UNKNOWN("UNKNOWN");
	

	private final String name;
	
	PaymentSource(String name){
		this.name = name;
	}
	
	public String getName() {
		return name;
	}

	
}
