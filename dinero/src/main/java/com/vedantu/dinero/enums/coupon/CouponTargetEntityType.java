/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.enums.coupon;

import com.vedantu.dinero.managers.coupon.ICouponValidator;
import com.vedantu.exception.VException;

import java.util.List;

/**
 *
 * @author ajith
 */
public enum CouponTargetEntityType {

    BATCH {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsBatch();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getBatchIds(purchasingEntityId);
        }
    },
    BATCH_REGISTRATION {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsBatchRegistration();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getBatchIds(purchasingEntityId);
        }
    },    
    PLAN {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsPlan();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getPlanIds(purchasingEntityId);
        }
    },
    COURSE_PLAN {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsCoursePlan();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getCoursePlanIds(purchasingEntityId);
        }
    },
    COURSE_PLAN_REGISTRATION {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsCoursePlanRegistration();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getCoursePlanIds(purchasingEntityId);
        }
    },    
    BUNDLE {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsBundle();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getBundleIds(purchasingEntityId);
        }
    },
    BUNDLE_PACKAGE {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsBundlePackage();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getBundlePackageIds(purchasingEntityId);
        }
    },
    OTM_BUNDLE {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsOTFBundle();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getOTFBundleIds(purchasingEntityId);
        }
    },
    OTM_BUNDLE_REGISTRATION {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsOTMBundleRegistration();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getOTFBundleIds(purchasingEntityId);
        }
    },    
    TEACHER {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsTeacher();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getTeacherIds(purchasingEntityId);
        }

        @Override
        public List<Object> getInfos(ICouponValidator couponValidator, List<String> targetStrs) {
            throw null;//TODO get userinfos
        }
    },
    COURSE {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsCourse();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getCourseIds(purchasingEntityId);
        }

        @Override
        public List<Object> getInfos(ICouponValidator couponValidator, List<String> targetStrs) {
            throw null;//TODO get userinfos
        }
    },
    COURSE_REGISTRATION {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsCourseRegistration();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getCourseIds(purchasingEntityId);
        }

        @Override
        public List<Object> getInfos(ICouponValidator couponValidator, List<String> targetStrs) {
            throw null;//TODO get userinfos
        }
    },    
    SUBJECT {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsSubject();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) {
            return couponValidator.getSubjects(purchasingEntityId);
        }
    },
    CATEGORY {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsCategory();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) throws VException {
            return couponValidator.getCategories(purchasingEntityId);
        }
    },
    GRADE {
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsGrade();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) throws VException {
            return couponValidator.getGrades(purchasingEntityId);
        }
    },
    EARLY_LEARNING{
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsGrade();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) throws VException {
            return couponValidator.getGrades(purchasingEntityId);
        }
    },
    SUBSCRIPTION_PLAN{
        @Override
        public boolean supports(ICouponValidator couponValidator) {
            return couponValidator.supportsGrade();
        }

        @Override
        public List<String> get(ICouponValidator couponValidator, String purchasingEntityId) throws VException {
            return couponValidator.getValidMonths(purchasingEntityId);
        }
    };
    public abstract boolean supports(ICouponValidator couponValidator);

    public abstract List<String> get(ICouponValidator couponValidator, String purchasingEntityId) throws VException;

    public List<Object> getInfos(ICouponValidator couponValidator, List<String> targetStrs) {
        return null;
    }
}
