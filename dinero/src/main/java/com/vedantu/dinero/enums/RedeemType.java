package com.vedantu.dinero.enums;

public enum RedeemType {

	FIXED, PERCENTAGE, DURATION, FLAT_PRICE
}
