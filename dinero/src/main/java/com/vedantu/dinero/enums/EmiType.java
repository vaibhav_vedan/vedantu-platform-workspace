package com.vedantu.dinero.enums;

public enum EmiType {
    NO_COST_EMI("NO_COST_EMI"), INTEREST_EMI("INTEREST_EMI");
    private final String name;

    EmiType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
