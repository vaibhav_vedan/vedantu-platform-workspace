package com.vedantu.dinero.enums;

public enum EmiCardType {
    CARDLESS("CARD_LESS"), CREDIT("CREDIT_CARD"), DEBIT("DEBIT_CARD");
    private final String name;

    EmiCardType(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
