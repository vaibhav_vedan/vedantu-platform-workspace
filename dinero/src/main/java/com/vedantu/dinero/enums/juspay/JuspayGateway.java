package com.vedantu.dinero.enums.juspay;

import java.util.HashMap;
import java.util.Map;

public enum JuspayGateway {

    HDFC(2L),
    CITI(4L),
    AMEX(5L),
    CYBERSOURCE(6L),
    IPG(7L),
    MIGS(8L),
    KOTAK(9L),
    PAYU(12L),
    CITRUS(14L),
    CCAVENUE_V2(16L),
    TPSL(17L),
    PAYTM(18L),
    PAYTM_V2(19L),
    HDFC_EBS_VAS(21L),
    RAZORPAY(23L),
    FSS_ATM_PIN(24L),
    EBS_V3(25L),
    ZAAKPAY(26L),
    BILLDESK(27L),
    BLAZEPAY(29L),
    FSS_ATM_PIN_V2(30L),
    MOBIKWIK(31L),
    OLAMONEY(32L),
    FREECHARGE(33L),
    MPESA(34L),
    SBIBUDDY(35L),
    JIOMONEY(36L),
    AIRTELMONEY(37L),
    AMAZONPAY(38L),
    PHONEPE(39L),
    STRIPE(50L),
    Cashfree(70L),
    HDFC_IVR(201L),
    ZESTMONEY(250L),
    AXISNB(300L),
    TPSL_SI(400L),
    AXIS_UPI(500L),
    HDFC_UPI(501L),
    KOTAK_UPI(503L),
    SBI_UPI(504L),
    ICICI_UPI(505L),
    PAYTM_UPI(509L),

    ;

    private Long gatewayId;
    private static final Map<Long, JuspayGateway> GATEWAY_MAP =  new HashMap<>();
    JuspayGateway(Long gatewayId) {
        this.gatewayId = gatewayId;
    }

    public static JuspayGateway getGateway(Long gatewayId) {
        if (GATEWAY_MAP.containsKey(gatewayId)) {
            return GATEWAY_MAP.get(gatewayId);
        } else {
            for (JuspayGateway value : values()) {
                if (value.gatewayId.equals(gatewayId)) {
                    GATEWAY_MAP.put(gatewayId, value);
                    return value;
                }
            }
        }
        return null;
    }
}
