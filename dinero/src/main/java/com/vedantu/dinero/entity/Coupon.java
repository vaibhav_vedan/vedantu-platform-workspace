package com.vedantu.dinero.entity;

import java.util.List;

import com.vedantu.dinero.enums.CouponType;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.enums.RedeemType;
import com.vedantu.dinero.pojo.CouponTarget;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Coupon extends AbstractMongoStringIdEntity {

    private String code;

    // created by userId
    private Long userId;
    private CouponType type;
    // all value are in INR
    private RedeemType redeemType;

    // actual amount redeemed (paise in case of monetry term)
    private Float redeemValue;

    // optional --> minOrderValue (in paisa) above which this coupon code is
    // valid
    private Integer minOrderValue;

    private Integer maxOrderValue;

    // max amount (in paisa) that can be redeemed using this coupon --> null or
    // 0 means no
    // limit
    private Integer maxRedeemValue;

    // null or 0 means no start time
    private Long validFrom;

    // null or 0 means no end time
    private Long validTill;

    private List<String> terms;
    private String description;

    // null or 0 means unlimited
    private Integer usesCount;

    // null or 0 means unlimited
    private Integer usesCountPerUser;

    // only this targetUserId can use this coupon
    private Long targetUserId;

    // this coupon can be applied only to these targets
    private List<CouponTarget> targets;

    private boolean satisfyAtleast1Condition = false;

    private Long passDurationInMillis;

    private AccessLevel accessLevel;//to decide if the user can query the coupon using target type and id

    private PaymentType forPaymentType = PaymentType.BULK;

    private List<Integer> targetOrder;

    public Coupon() {
        super();
    }

    public Coupon(String code, Long userId, CouponType type, RedeemType redeemType, Float redeemValue,
            Integer minOrderValue, Integer maxRedeemValue, Long validFrom, Long validTill, List<String> terms,
            String description, Integer usesCount, Integer usesCountPerUser, Long targetUserId,
            List<CouponTarget> targets, boolean satisfyAtleast1Condition, PaymentType paymentType) {
        this.code = code;
        this.userId = userId;
        this.type = type;
        this.redeemType = redeemType;
        this.redeemValue = redeemValue;
        this.minOrderValue = minOrderValue;
        this.maxRedeemValue = maxRedeemValue;
        this.validFrom = validFrom;
        this.validTill = validTill;
        this.terms = terms;
        this.description = description;
        this.usesCount = usesCount;
        this.usesCountPerUser = usesCountPerUser;
        this.targetUserId = targetUserId;
        this.targets = targets;
        this.forPaymentType = paymentType;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public CouponType getType() {
        return type;
    }

    public void setType(CouponType type) {
        this.type = type;
    }

    public RedeemType getRedeemType() {
        return redeemType;
    }

    public void setRedeemType(RedeemType redeemType) {
        this.redeemType = redeemType;
    }

    public Float getRedeemValue() {
        return redeemValue;
    }

    public void setRedeemValue(Float redeemValue) {
        this.redeemValue = redeemValue;
    }

    public Integer getMinOrderValue() {
        return minOrderValue;
    }

    public void setMinOrderValue(Integer minOrderValue) {
        this.minOrderValue = minOrderValue;
    }

    public Integer getMaxRedeemValue() {
        return maxRedeemValue;
    }

    public void setMaxRedeemValue(Integer maxRedeemValue) {
        this.maxRedeemValue = maxRedeemValue;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTill() {
        return validTill;
    }

    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    public List<String> getTerms() {
        return terms;
    }

    public void setTerms(List<String> terms) {
        this.terms = terms;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getUsesCount() {
        return usesCount;
    }

    public void setUsesCount(Integer usesCount) {
        this.usesCount = usesCount;
    }

    public Integer getUsesCountPerUser() {
        return usesCountPerUser;
    }

    public void setUsesCountPerUser(Integer usesCountPerUser) {
        this.usesCountPerUser = usesCountPerUser;
    }

    public Long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(Long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public List<CouponTarget> getTargets() {
        return targets;
    }

    public void setTargets(List<CouponTarget> targets) {
        this.targets = targets;
    }

    public Long getPassDurationInMillis() {
        return passDurationInMillis;
    }

    public void setPassDurationInMillis(Long passDurationInMillis) {
        this.passDurationInMillis = passDurationInMillis;
    }

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    public boolean isSatisfyAtleast1Condition() {
        return satisfyAtleast1Condition;
    }

    public void setSatisfyAtleast1Condition(boolean satisfyAtleast1Condition) {
        this.satisfyAtleast1Condition = satisfyAtleast1Condition;
    }

    public PaymentType getForPaymentType() {
        return forPaymentType;
    }

    public void setForPaymentType(PaymentType forPaymentType) {
        this.forPaymentType = forPaymentType;
    }

    public List<Integer> getTargetOrder() {
        return targetOrder;
    }

    public void setTargetOrder(List<Integer> targetOrder) {
        this.targetOrder = targetOrder;
    }

    public Integer getMaxOrderValue() {
        return maxOrderValue;
    }

    public void setMaxOrderValue(Integer maxOrderValue) {
        this.maxOrderValue = maxOrderValue;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String COUPON_TYPE = "couponType";
        public static final String REDEEM_TYPE = "redeemType";
        public static final String REDEEM_VALUE = "redeemValue";
        public static final String MIN_ORDER_VALUE = "minOrderValue";
        public static final String MAX_REDEEM_VALUE = "maxRedeemValue";
        public static final String VALID_FROM = "validFrom";
        public static final String VALID_TILL = "validTill";
        public static final String TERMS = "terms";
        public static final String DESCIPTION = "description";
        public static final String USES_COUNT = "usesCount";
        public static final String USES_COUNT_PER_USER = "usesCountPerUser";
        public static final String TARGET_USER_ID = "targetUserId";
        public static final String TARGETS = "targets";
        public static final String ACCESS_LEVEL = "accessLevel";
    }

}
