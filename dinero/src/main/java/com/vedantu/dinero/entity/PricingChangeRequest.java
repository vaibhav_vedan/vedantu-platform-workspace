package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.State;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class PricingChangeRequest extends AbstractMongoStringIdEntity {

	private String slabId;
	private Long teacherId;
	private Long price;
	private Long updateTime;
	private boolean active;
	private State state;

	public String getSlabId() {
		return slabId;
	}

	public void setSlabId(String slabId) {
		this.slabId = slabId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getPrice() {
		return price;
	}

	public void setPrice(Long price) {
		this.price = price;
	}

	public Long getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Long updateTime) {
		this.updateTime = updateTime;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public State getState() {
		return state;
	}

	public void setState(State state) {
		this.state = state;
	}

	public PricingChangeRequest(String slabId, Long teacherId, Long price, Long updateTime, boolean active,
			State state) {
		super();
		this.slabId = slabId;
		this.teacherId = teacherId;
		this.price = price;
		this.updateTime = updateTime;
		this.active = active;
		this.state = state;
	}

	public PricingChangeRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "PricingChangeRequest [slabId=" + slabId + ", teacherId=" + teacherId + ", price=" + price
				+ ", updateTime=" + updateTime + ", active=" + active + ", state=" + state + "]";
	}

}
