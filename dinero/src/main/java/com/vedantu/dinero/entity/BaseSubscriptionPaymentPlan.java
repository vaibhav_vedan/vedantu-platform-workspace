package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.BaseSubscriptionDuration;
import com.vedantu.dinero.enums.BaseSubscriptionPurchaseEntity;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 *
 * @author darshit
 */
@Document(collection = "BaseSubscriptionPaymentPlan")
@Data
public class BaseSubscriptionPaymentPlan extends AbstractMongoStringIdEntity {

    private Integer validMonths;
    private BaseSubscriptionPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private Integer price;
    private Integer cutPrice;
    private BaseSubscriptionDuration planDuration;




    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String PURCHASE_ENTITY_TYPE = "purchaseEntityType";
        public static final String PURCHASE_ENTITY_ID = "purchaseEntityId";
        public static final String VALID_MONTHS = "validMonths";
        public static final String AMOUNT = "amount";

    }
}
