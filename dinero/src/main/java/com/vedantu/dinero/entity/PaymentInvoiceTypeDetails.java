package com.vedantu.dinero.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.vedantu.dinero.enums.PaymentInvoiceType;
import com.vedantu.dinero.pojo.TaxPercentage;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class PaymentInvoiceTypeDetails extends AbstractMongoStringIdEntity {
	private PaymentInvoiceType type;
	private List<TaxPercentage> taxPercentages = new ArrayList<>();

	public PaymentInvoiceTypeDetails() {
		super();
	}

	public PaymentInvoiceType getType() {
		return type;
	}

	public void setType(PaymentInvoiceType type) {
		this.type = type;
	}

	public List<TaxPercentage> getTaxPercentages() {
		return taxPercentages;
	}

	public void setTaxPercentages(List<TaxPercentage> taxPercentages) {
		this.taxPercentages = taxPercentages;
	}

	public void updateEntry(PaymentInvoiceTypeDetails newEntry) {
		if (newEntry != null) {
			this.taxPercentages = newEntry.getTaxPercentages();
		}
	}

	public void validatePercentages() throws BadRequestException {
		if (!CollectionUtils.isEmpty(this.taxPercentages)) {
			for (TaxPercentage taxPercentage : this.taxPercentages) {
				if (taxPercentage.getTaxType() == null) {
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid tax type");
				}
				if (Float.compare(taxPercentage.getPercentage(), 0f) <= 0
						|| Float.compare(taxPercentage.getPercentage(), 100f) >= 0l) {
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid tax percentage");
				}
			}
		}
	}

	public float calculateTotalTaxPercentage() {
		return (this.taxPercentages == null) ? 0f
				: Double.valueOf(this.taxPercentages.stream().mapToDouble(TaxPercentage::getPercentage).sum())
						.floatValue();
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String PAYMENT_INVOICE_TYPE = "type";
	}

	@Override
	public String toString() {
		return "PaymentInvoiceTypeDetails [type=" + type + ", taxPercentages=" + taxPercentages + ", toString()="
				+ super.toString() + "]";
	}
        
}
