package com.vedantu.dinero.entity.Instalment;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;

/**
 *
 * @author ajith
 */
public class BaseInstalment extends AbstractMongoStringIdEntity {

    private InstalmentPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private List<BaseInstalmentInfo> info;
    Long userId;
    //Integer dueTime;
    //Integer amount;

    public BaseInstalment() {
        super();
    }

    public InstalmentPurchaseEntity getPurchaseEntityType() {
        return purchaseEntityType;
    }

    public void setPurchaseEntityType(InstalmentPurchaseEntity purchaseEntityType) {
        this.purchaseEntityType = purchaseEntityType;
    }

    public String getPurchaseEntityId() {
        return purchaseEntityId;
    }

    public void setPurchaseEntityId(String purchaseEntityId) {
        this.purchaseEntityId = purchaseEntityId;
    }

//    public Integer getDueTime() {
//        return dueTime;
//    }
//
//    public void setDueTime(Integer dueTime) {
//        this.dueTime = dueTime;
//    }
//
//    public Integer getAmount() {
//        return amount;
//    }
//
//    public void setAmount(Integer amount) {
//        this.amount = amount;
//    }
    public List<BaseInstalmentInfo> getInfo() {
        return info;
    }

    public void setInfo(List<BaseInstalmentInfo> info) {
        this.info = info;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String PURCHASE_ENTITY_TYPE = "purchaseEntityType";
        public static final String PURCHASE_ENTITY_ID = "purchaseEntityId";
        //public static final String DUE_TIME = "dueTime";
        //public static final String AMOUNT = "amount";
        public static final String INFO = "info";
        public static final String USER_ID = "userId";
    }
}
