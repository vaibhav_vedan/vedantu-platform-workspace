package com.vedantu.dinero.entity.Instalment;

import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.pojo.InstalmentChangeTime;
import com.vedantu.dinero.pojo.InstalmentStatusChange;
import com.vedantu.dinero.pojo.InstalmentUpdate;
import com.vedantu.dinero.pojo.InstalmentVDiscountChange;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
@Document(collection = "Instalment")
@CompoundIndexes({
    @CompoundIndex(name = "dueTime_1_contextType_1", def = "{'dueTime': 1, 'contextType': 1}", background = true),
        @CompoundIndex(name = "dueTime_1_contextType_1_paymentStatus_1_userId_1", def = "{'dueTime': 1, 'contextType': 1 , 'userId' : 1,  'paymentStatus' : 1}", background = true)
})
public class Instalment extends AbstractMongoStringIdEntity {

    private String orderId;
    private String deliverableEntityId;
    private Long dueTime;//in millis
    private PaymentStatus paymentStatus;
    private Integer convenienceCharge;//in paisa
    private Integer securityCharge;//in paisa
    private Integer hoursCost;//in paisa
    private Integer totalAmount;//in paisa
    private Integer totalPromotionalAmount; //in paisa
    private Integer totalNonPromotionalAmount;//in paisa
    private SessionSchedule sessionSchedule;
    private String triggeredBy;
    private Long paidTime;//in millis
    private Long hours;//in millis
    private String contextId;
    private InstalmentPurchaseEntity contextType;
    private List<InstalmentUpdate> updates = new ArrayList<>();
    private Long userId;
    private Integer teacherDiscountAmount;
    private Integer vedantuDiscountAmount;
    private List<InstalmentVDiscountChange> vDiscountChanges;
    private List<PurchasingEntity> purchasingEntities;
    private List<InstalmentChangeTime> instalmentChangeTime;
    private List<InstalmentStatusChange> instalmentStatusChanges;

    public Instalment() {
        super();
    }

    public Instalment(String orderId, Long dueTime,
            PaymentStatus paymentStatus, Integer convenienceCharge,
            Integer securityCharge, Integer hoursCost, Integer totalAmtToBePaid,
            Integer totalNonPromotionalAmount, Integer totalPromotionalAmount,
            SessionSchedule sessionSchedule, Long hours, String callingUserId,
            InstalmentPurchaseEntity contextType, String contextId) {
        super();
        this.orderId = orderId;
        this.dueTime = dueTime;
        this.paymentStatus = paymentStatus;
        this.convenienceCharge = convenienceCharge;
        this.securityCharge = securityCharge;
        this.hoursCost = hoursCost;
        this.totalAmount = totalAmtToBePaid;
        this.totalNonPromotionalAmount = totalNonPromotionalAmount;
        this.totalPromotionalAmount = totalPromotionalAmount;
        this.sessionSchedule = sessionSchedule;
        this.hours = hours;
        this.contextId = contextId;
        this.contextType = contextType;
        if (StringUtils.isNotEmpty(callingUserId)) {
            this.userId = Long.parseLong(callingUserId);
        }
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Long getDueTime() {
        return dueTime;
    }

    public void setDueTime(Long dueTime) {
        this.dueTime = dueTime;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public Integer getConvenienceCharge() {
        if (convenienceCharge == null) {
            return 0;
        } else {
            return convenienceCharge;
        }
    }

    public void setConvenienceCharge(Integer convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public Integer getSecurityCharge() {
        if (securityCharge == null) {
            return 0;
        } else {
            return securityCharge;
        }
    }

    public void setSecurityCharge(Integer securityCharge) {
        this.securityCharge = securityCharge;
    }

    public Integer getHoursCost() {
        if (hoursCost == null) {
            return 0;
        } else {
            return hoursCost;
        }
    }

    public void setHoursCost(Integer hoursCost) {
        this.hoursCost = hoursCost;
    }

    public Integer getTotalAmount() {
        if (totalAmount == null) {
            return 0;
        } else {
            return totalAmount;
        }
    }

    public void setTotalAmount(Integer totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Integer getTotalPromotionalAmount() {
        if (totalPromotionalAmount == null) {
            return 0;
        } else {
            return totalPromotionalAmount;
        }
    }

    public void setTotalPromotionalAmount(Integer totalPromotionalAmount) {
        this.totalPromotionalAmount = totalPromotionalAmount;
    }

    public Integer getTotalNonPromotionalAmount() {
        if (totalNonPromotionalAmount == null) {
            return 0;
        } else {
            return totalNonPromotionalAmount;
        }
    }

    public void setTotalNonPromotionalAmount(Integer totalNonPromotionalAmount) {
        this.totalNonPromotionalAmount = totalNonPromotionalAmount;
    }

    public String getDeliverableEntityId() {
        return deliverableEntityId;
    }

    public void setDeliverableEntityId(String deliverableEntityId) {
        this.deliverableEntityId = deliverableEntityId;
    }

    public SessionSchedule getSessionSchedule() {
        return sessionSchedule;
    }

    public void setSessionSchedule(SessionSchedule sessionSchedule) {
        this.sessionSchedule = sessionSchedule;
    }

    public String getTriggeredBy() {
        return triggeredBy;
    }

    public void setTriggeredBy(String triggeredBy) {
        this.triggeredBy = triggeredBy;
    }

    public Long getPaidTime() {
        return paidTime;
    }

    public void setPaidTime(Long paidTime) {
        this.paidTime = paidTime;
    }

    public Long getHours() {
        return hours;
    }

    public void setHours(Long hours) {
        this.hours = hours;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public InstalmentPurchaseEntity getContextType() {
        return contextType;
    }

    public void setContextType(InstalmentPurchaseEntity contextType) {
        this.contextType = contextType;
    }

    public List<InstalmentUpdate> getUpdates() {
        if (updates == null) {
            return new ArrayList<>();
        }
        return updates;
    }

    public void setUpdates(List<InstalmentUpdate> updates) {
        this.updates = updates;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getTeacherDiscountAmount() {
        return teacherDiscountAmount;
    }

    public void setTeacherDiscountAmount(Integer teacherDiscountAmount) {
        this.teacherDiscountAmount = teacherDiscountAmount;
    }

    public Integer getVedantuDiscountAmount() {
        if (vedantuDiscountAmount == null) {
            return 0;
        }
        return vedantuDiscountAmount;
    }

    public void setVedantuDiscountAmount(Integer vedantuDiscountAmount) {
        this.vedantuDiscountAmount = vedantuDiscountAmount;
    }

    public List<PurchasingEntity> getPurchasingEntities() {
        return purchasingEntities;
    }

    public void setPurchasingEntities(List<PurchasingEntity> purchasingEntities) {
        this.purchasingEntities = purchasingEntities;
    }

    public List<InstalmentChangeTime> getInstalmentChangeTime() {
        return instalmentChangeTime;
    }

    public void setInstalmentChangeTime(List<InstalmentChangeTime> instalmentChangeTime) {
        this.instalmentChangeTime = instalmentChangeTime;
    }

    public List<InstalmentVDiscountChange> getvDiscountChanges() {
        if (vDiscountChanges == null) {
            vDiscountChanges = new ArrayList<>();
        }
        return vDiscountChanges;
    }

    public void setvDiscountChanges(List<InstalmentVDiscountChange> vDiscountChanges) {
        this.vDiscountChanges = vDiscountChanges;
    }

    public List<InstalmentStatusChange> getInstalmentStatusChanges() {
        return instalmentStatusChanges;
    }

    public void setInstalmentStatusChanges(List<InstalmentStatusChange> instalmentStatusChanges) {
        this.instalmentStatusChanges = instalmentStatusChanges;
    }

    public void setPaymentStatusAndAddInstalmentStatusChange(PaymentStatus newStatus) {
        if (this.instalmentStatusChanges == null) {
            this.instalmentStatusChanges = new ArrayList<>();
        }
        this.instalmentStatusChanges.add(new InstalmentStatusChange(this.paymentStatus, newStatus, System.currentTimeMillis()));
        this.paymentStatus = newStatus;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String PAYMENT_STATUS = "paymentStatus";
        public static final String CONTEXT_ID = "contextId";
        public static final String ORDER_ID = "orderId";
        public static final String PAID_TIME = "paidTime";
        public static final String TOTAL_AMOUNT = "totalAmount";
        public static final String DUE_TIME = "dueTime";
        public static final String CONTEXT_TYPE = "contextType";
        public static final String USER_ID = "userId";
        public static final String TOTAL_PROMOTIONAL_AMOUNT = "totalPromotionalAmount";
        public static final String PURCHASING_ENTITIES_DELIVERABLE_ID = "purchasingEntities.deliverableId";
    }

}
