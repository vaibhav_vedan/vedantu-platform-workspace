package com.vedantu.dinero.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.SessionModel;
import java.time.Month;


public class TeacherIncentive extends AbstractMongoStringIdEntity {

	private Long teacherId;
	private SessionModel model;
	private Month month;
	private String description;
	private int incentive;


	public TeacherIncentive() {
		super();
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public Month getMonth() {
		return month;
	}

	public void setMonth(Month month) {
		this.month = month;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getIncentive() {
		return incentive;
	}

	public void setIncentive(int incentive) {
		this.incentive = incentive;
	}

	public TeacherIncentive(Long teacherId, SessionModel model, Month month, String description, int incentive) {
		super();
		this.teacherId = teacherId;
		this.model = model;
		this.month = month;
		this.description = description;
		this.incentive = incentive;
	}

	@Override
	public String toString() {
		return "TeacherIncentive [teacherId=" + teacherId + ", model=" + model + ", month=" + month + ", description="
				+ description + ", incentive=" + incentive + "]";
	}

}
