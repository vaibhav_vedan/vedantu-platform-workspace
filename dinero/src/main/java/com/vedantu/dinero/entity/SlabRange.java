package com.vedantu.dinero.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class SlabRange extends AbstractMongoStringIdEntity {

	private int min;
	private int max;
	private Long minValue;
	private boolean active = true;

	public SlabRange() {
		super();
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public Long getMinValue() {
		return minValue;
	}

	public void setMinValue(Long minValue) {
		this.minValue = minValue;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	@Override
	public String toString() {
		return "SlabRange [min=" + min + ", max=" + max + ", minValue=" + minValue + ", active=" + active + "]";
	}

	public SlabRange(int min, int max, Long minValue, boolean active) {
		super();
		this.min = min;
		this.max = max;
		this.minValue = minValue;
		this.active = active;
	}

}
