package com.vedantu.dinero.entity;


import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.dinero.enums.PaymentMethod;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Document(collection = "PaymentOffer")
public class PaymentOffer extends AbstractMongoStringIdEntity {
    private PaymentMethod paymentMethod;
    private String serviceProvider;
    private String campaignName;
    private PaymentGatewayName gateway;
    private long offerStartTime;
    private long offerEndTime;
    private List<CourseTerm> courseType;
    private String message;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String PAYMENT_METHOD = "paymentMethod";
        public static final String SERVICE_PROVIDER = "serviceProvider";
        public static final String CAMPAIGN_NAME = "campaignName";
        public static final String GATEWAY = "gateway";
        public static final String OFFER_START_TIME = "offerStartTime";
        public static final String OFFER_END_TIME = "offerEndTime";
        public static final String COURSE_TYPE = "courseType";
        public static final String MESSAGE = "message";
    }
}
