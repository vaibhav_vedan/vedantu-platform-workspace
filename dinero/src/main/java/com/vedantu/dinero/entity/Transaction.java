package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.TransactionRefSubType;
import com.vedantu.dinero.enums.TransactionRefType;
import com.vedantu.dinero.enums.LoanType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "Transaction")
public class Transaction extends AbstractMongoStringIdEntity {

    private Integer amount;// amount in paisa
    private Integer promotionalAmount;
    private Integer nonPromotionalAmount;
    private String currencyCode;
    private String debitFromAccount;
    private String creditToAccount;
    private Integer debitFromClosingBalance;
    private Integer creditToClosingBalance;
    private String reasonType;
    private String reasonRefNo;
    private TransactionRefType reasonRefType;
    private TransactionRefSubType reasonRefSubType;
    private String reasonNote;
    private String triggredBy;
    private List<String> tags;
    private DeliverableEntityType contextType;
    private String contextId;
    private Long approvedBy;
    private String reasonNoteType;
    private String remarks;
    private LoanType loanType;
    @Indexed(unique = true, background = true)
    private String uniqueId;

    @Indexed(background = true)
    private Boolean vAccountTransaction = true;


    public Transaction(Integer amount, Integer promotionalAmount, Integer nonPromotionalAmount, String currencyCode,
            String debitFromAccount, Integer debitFromClosingBalance, String creditToAccount, Integer creditToClosingBalance, String reasonType, String reasonRefNo,
            TransactionRefType reasonRefType, String reasonNote, String triggredBy) {
        super();
        this.amount = amount;
        this.promotionalAmount = promotionalAmount;
        this.nonPromotionalAmount = nonPromotionalAmount;
        this.currencyCode = currencyCode;
        this.debitFromAccount = debitFromAccount;
        this.creditToAccount = creditToAccount;
        this.creditToClosingBalance = creditToClosingBalance;
        this.debitFromClosingBalance = debitFromClosingBalance;
        this.reasonType = reasonType;
        this.reasonRefNo = reasonRefNo;
        this.reasonRefType = reasonRefType;
        this.reasonNote = reasonNote;
        this.triggredBy = triggredBy;
    }

    public Integer getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public Integer getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public Transaction() {

        super();
    }

    public Boolean getvAccountTransaction() {
        return vAccountTransaction;
    }

    public void setvAccountTransaction(Boolean vAccountTransaction) {
        this.vAccountTransaction = vAccountTransaction;
    }

    public static String _getTriggredByUser(Long userId) {

        return "user/" + userId;
    }

    public static String _getTriggredBySystem() {

        return "system";
    }

    public Integer getAmount() {

        return amount;
    }

    public void setAmount(Integer amount) {

        this.amount = amount;
    }

    public String getCurrencyCode() {

        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {

        this.currencyCode = currencyCode;
    }

    public String getDebitFromAccount() {

        return debitFromAccount;
    }

    public void setDebitFromAccount(String debitFromAccount) {

        this.debitFromAccount = debitFromAccount;
    }

    public String getCreditToAccount() {

        return creditToAccount;
    }

    public void setCreditToAccount(String creditToAccount) {

        this.creditToAccount = creditToAccount;
    }

    public String getReasonType() {

        return reasonType;
    }

    public void setReasonType(String reasonType) {

        this.reasonType = reasonType;
    }

    public String getReasonRefNo() {

        return reasonRefNo;
    }

    public void setReasonRefNo(String reasonRefNo) {

        this.reasonRefNo = reasonRefNo;
    }

    public TransactionRefType getReasonRefType() {

        return reasonRefType;
    }

    public void setReasonRefType(TransactionRefType reasonRefType) {

        this.reasonRefType = reasonRefType;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }

    public String getTriggredBy() {
        return triggredBy;
    }

    public void setTriggredBy(String triggredBy) {
        this.triggredBy = triggredBy;
    }

    public Integer getDebitFromClosingBalance() {
        return debitFromClosingBalance;
    }

    public void setDebitFromClosingBalance(Integer debitFromClosingBalance) {
        this.debitFromClosingBalance = debitFromClosingBalance;
    }

    public Integer getCreditToClosingBalance() {
        return creditToClosingBalance;
    }

    public void setCreditToClosingBalance(Integer creditToClosingBalance) {
        this.creditToClosingBalance = creditToClosingBalance;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public DeliverableEntityType getContextType() {
        return contextType;
    }

    public void setContextType(DeliverableEntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Long getApprovedBy() {
        return approvedBy;
    }

    public void setApprovedBy(Long approvedBy) {
        this.approvedBy = approvedBy;
    }

    public String getReasonNoteType() {
        return reasonNoteType;
    }

    public void setReasonNoteType(String reasonNoteType) {
        this.reasonNoteType = reasonNoteType;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String CREDIT_TO_ACCOUNT = "creditToAccount";
        public static final String DEBIT_FROM_ACCOUNT = "debitFromAccount";
        public static final String REASON_REF_TYPE = "reasonRefType";
        public static final String REASON_REF_NO = "reasonRefNo";
        public static final String DEFAULT_CURRENCY_CODE = "INR";
        public static final String REASON_TYPE = "reasonType";
        public static final String AMOUNT = "amount";
        public static final String UNIQUE_ID = "uniqueId";
        public static final String V_ACCOUNT_TRANSACTION = "vAccountTransaction";
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public TransactionRefSubType getReasonRefSubType() {
        return reasonRefSubType;
    }

    public void setReasonRefSubType(TransactionRefSubType reasonRefSubType) {
        this.reasonRefSubType = reasonRefSubType;
    }

    public LoanType getLoanType() {
        return loanType;
    }

    public void setLoanType(LoanType loanType) {
        this.loanType = loanType;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }
}
