package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.PaymentGatewayName;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;


public class PaymentGateway extends AbstractMongoStringIdEntity {

	private String name;
	private Boolean enabled;
	private Integer percentageTraffic;

	public PaymentGateway() {
		super();
		// TODO Auto-generated constructor stub
	}

	public PaymentGateway(String name, Boolean enabled, Integer percentageTraffic) {
		super();
		this.name = name;
		this.enabled = enabled;
		this.percentageTraffic = percentageTraffic;
	}
	
	public PaymentGateway(PaymentGatewayName name, Boolean enabled, Integer percentageTraffic) {
		super();
		this.name = name.name();
		this.enabled = enabled;
		this.percentageTraffic = percentageTraffic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}

	public Integer getPercentageTraffic() {
		return percentageTraffic;
	}

	public void setPercentageTraffic(Integer percentageTraffic) {
		this.percentageTraffic = percentageTraffic;
	}

	@Override
	public String toString() {
		return "Wallet [name=" + name + ", enabled=" + enabled + ", percentageTraffic=" + percentageTraffic + "]";
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {

		public static final String NAME = "name";
		public static final String ENABLED = "enabled";
		public static final String PERCENTAGE_TRAFFIC = "percentageTraffic";
	}
}
