package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.TransactionLevel;
import com.vedantu.dinero.enums.VedantuAccount;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class VedantuAccountAuth extends AbstractMongoStringIdEntity {

	private Long userId;
	private VedantuAccount vedantuAccount;
	private TransactionLevel transactionLevel;
	private Integer minAmount = 0;
	private Integer maxAmount = -1;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public VedantuAccount getVedantuAccount() {
		return vedantuAccount;
	}

	public void setVedantuAccount(VedantuAccount vedantuAccount) {
		this.vedantuAccount = vedantuAccount;
	}

	public TransactionLevel getTransactionLevel() {
		return transactionLevel;
	}

	public void setTransactionLevel(TransactionLevel transactionLevel) {
		this.transactionLevel = transactionLevel;
	}

	public Integer getMinAmount() {
		return minAmount;
	}

	public void setMinAmount(Integer minAmount) {
		this.minAmount = minAmount;
	}

	public Integer getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(Integer maxAmount) {
		this.maxAmount = maxAmount;
	}

	public VedantuAccountAuth(Long userId, VedantuAccount vedantuAccount, TransactionLevel transactionLevel,
			Integer minAmount, Integer maxAmount) {
		super();
		this.userId = userId;
		this.vedantuAccount = vedantuAccount;
		this.transactionLevel = transactionLevel;
		this.minAmount = minAmount;
		this.maxAmount = maxAmount;
	}

	public VedantuAccountAuth() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "VedantuAccountAuth [userId=" + userId + ", vedantuAccount=" + vedantuAccount + ", transactionLevel="
				+ transactionLevel + ", minAmount=" + minAmount + ", maxAmount=" + maxAmount + "]";
	}

}
