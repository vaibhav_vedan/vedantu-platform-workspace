package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.PurchaseFlowType;
import com.vedantu.dinero.enums.coupon.PassProcessingState;
import com.vedantu.dinero.enums.coupon.RedeemedCouponState;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "RedeemedCoupon")
@CompoundIndexes({
        @CompoundIndex(name = "passProcessingState_referenceId", def = "{'passProcessingState' : 1, 'referenceTags.referenceId': 1}", background = true),
})
public class RedeemedCoupon extends AbstractMongoStringIdEntity {

    private String code;
    private Long userId;
    private Integer redeemValue;

    // Redeem on entity
    private String entityId; //changing it to string as the OrderedItem has entityId as string
    //and since it is being used at many places, wouldn't want to change that
    private EntityType entityType;

    private RedeemedCouponState state = RedeemedCouponState.UNUSED;

    private PurchaseFlowType purchaseFlowType;//useful in cases of locking the coupon

    private String purchaseFlowId;//each flow has to be unique, user should not add multiple coupons to same flowId

    private Long passExpirationTime;
    private PassProcessingState passProcessingState;
    private List<ReferenceTag> referenceTags;

    public RedeemedCoupon() {
        super();
    }

    public RedeemedCoupon(String code, Long userId, Integer redeemValue,
            String entityId, EntityType entityType, RedeemedCouponState state) {
        super();
        this.code = code;
        this.userId = userId;
        this.redeemValue = redeemValue;
        this.entityId = entityId;
        this.entityType = entityType;
        this.state = state;
    }

    public List<ReferenceTag> getReferenceTags() {
        return referenceTags;
    }

    public void setReferenceTags(List<ReferenceTag> referenceTags) {
        this.referenceTags = referenceTags;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getRedeemValue() {
        return redeemValue;
    }

    public void setRedeemValue(Integer redeemValue) {
        this.redeemValue = redeemValue;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public RedeemedCouponState getState() {
        return state;
    }

    public void setState(RedeemedCouponState state) {
        this.state = state;
    }

    public PurchaseFlowType getPurchaseFlowType() {
        return purchaseFlowType;
    }

    public void setPurchaseFlowType(PurchaseFlowType purchaseFlowType) {
        this.purchaseFlowType = purchaseFlowType;
    }

    public String getPurchaseFlowId() {
        return purchaseFlowId;
    }

    public void setPurchaseFlowId(String purchaseFlowId) {
        this.purchaseFlowId = purchaseFlowId;
    }

    public Long getPassExpirationTime() {
        return passExpirationTime;
    }

    public void setPassExpirationTime(Long passExpirationTime) {
        this.passExpirationTime = passExpirationTime;
    }

    public PassProcessingState getPassProcessingState() {
        return passProcessingState;
    }

    public void setPassProcessingState(PassProcessingState passProcessingState) {
        this.passProcessingState = passProcessingState;
    }

    @Override
    public String toString() {
        return "RedeemedCoupon{" + "code=" + code + ", userId=" + userId + ", redeemValue=" + redeemValue + ", entityId=" + entityId + ", entityType=" + entityType + ", state=" + state + ", purchaseFlowType=" + purchaseFlowType + ", purchaseFlowId=" + purchaseFlowId + '}';
    }


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String CODE = "code";
        public static final String PASS_EXPIRATION_TIME = "passExpirationTime";
        public static final String PASS_PROCESSING_STATE = "passProcessingState";
        public static final String REF_IDS = "referenceTags.referenceId";
    }

}
