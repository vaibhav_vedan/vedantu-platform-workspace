package com.vedantu.dinero.entity;

import org.springframework.data.annotation.Id;

/**
 * Created by somil on 04/08/17.
 */
public class InvoiceCounter {
    @Id
    private String id;
    private Long seq;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Long getSeq() {
        return seq;
    }

    public void setSeq(Long seq) {
        this.seq = seq;
    }


}
