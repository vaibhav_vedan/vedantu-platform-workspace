package com.vedantu.dinero.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class TeacherStartsFrom extends AbstractMongoStringIdEntity {

	private Long teacherId;
	private int minPrice;
	private int maxPrice;

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public int getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(int minPrice) {
		this.minPrice = minPrice;
	}

	public int getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(int maxPrice) {
		this.maxPrice = maxPrice;
	}

	public TeacherStartsFrom(Long teacherId, int minPrice, int maxPrice) {
		super();
		this.teacherId = teacherId;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
	}

	public TeacherStartsFrom() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "TeacherStartsFrom [teacherId=" + teacherId + ", minPrice=" + minPrice + ", maxPrice=" + maxPrice + "]";
	}

}
