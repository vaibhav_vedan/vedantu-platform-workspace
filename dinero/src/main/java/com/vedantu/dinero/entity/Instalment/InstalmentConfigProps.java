/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.entity.Instalment;

import com.vedantu.dinero.enums.Instalment.InstalmentChargeType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.RedeemType;
import com.vedantu.dinero.request.SetInstalmentConfigPropsReq;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

/**
 *
 * @author ajith
 */
public class InstalmentConfigProps extends AbstractMongoStringIdEntity {

    private RedeemType chargeType;
    private Integer chargeValue = 0;//in paisa
    private InstalmentPurchaseEntity purchaseEntityType;
    private String purchaseEntityId;
    private InstalmentChargeType instalmentChargeType;//required

    public InstalmentConfigProps() {
        super();
    }

    public InstalmentConfigProps(RedeemType chargeType, Integer chargeValue,
            InstalmentPurchaseEntity purchaseEntityType, String modelId, InstalmentChargeType instalmentChargeType, String createdBy) {
        super();
        this.chargeType = chargeType;
        this.chargeValue = chargeValue;
        this.purchaseEntityType = purchaseEntityType;
        this.purchaseEntityId = modelId;
        this.instalmentChargeType = instalmentChargeType;
    }

    public RedeemType getChargeType() {
        return chargeType;
    }

    public void setChargeType(RedeemType chargeType) {
        this.chargeType = chargeType;
    }

    public Integer getChargeValue() {
        return chargeValue;
    }

    public void setChargeValue(Integer chargeValue) {
        this.chargeValue = chargeValue;
    }

    public InstalmentPurchaseEntity getPurchaseEntityType() {
        return purchaseEntityType;
    }

    public void setPurchaseEntityType(InstalmentPurchaseEntity purchaseEntityType) {
        this.purchaseEntityType = purchaseEntityType;
    }

    public String getPurchaseEntityId() {
        return purchaseEntityId;
    }

    public void setPurchaseEntityId(String purchaseEntityId) {
        this.purchaseEntityId = purchaseEntityId;
    }

    public InstalmentChargeType getInstalmentChargeType() {
        return instalmentChargeType;
    }

    public void setInstalmentChargeType(InstalmentChargeType instalmentChargeType) {
        this.instalmentChargeType = instalmentChargeType;
    }

    public InstalmentConfigProps toInstalmentConfigProps(SetInstalmentConfigPropsReq req) {
        InstalmentConfigProps prop = new InstalmentConfigProps();
        return toInstalmentConfigProps(prop, req);
    }

    public InstalmentConfigProps toInstalmentConfigProps(InstalmentConfigProps prop,
            SetInstalmentConfigPropsReq req) {
        prop.setChargeType(req.getChargeType());
        prop.setChargeValue(req.getChargeValue());
        prop.setPurchaseEntityId(req.getPurchaseEntityId());
        prop.setPurchaseEntityType(req.getPurchaseEntityType());
        prop.setInstalmentChargeType(req.getInstalmentChargeType());
        //if(req.getCallingUserId()!=null){
        //    prop.setCreatedBy(req.getCallingUserId().toString());
        //}
        return prop;
    }

    public static class Constants extends com.vedantu.util.Constants {

        public static final String CHARGE_TYPE = "chargeType";
        public static final String CHARGE_VALUE = "chargeValue";
        public static final String PURCHASE_ENTITY_TYPE = "purchaseEntityType";
        public static final String PURCHASE_ENTITY_ID = "purchaseEntityId";
        public static final String INSTALMENT_CHARGE_TYPE = "instalmentChargeType";
    }

    @Override
    public String toString() {
        return "InstalmentConfigProps{" + "chargeType=" + chargeType + ", chargeValue=" + chargeValue + ", purchaseEntityType=" + purchaseEntityType + ", purchaseEntityId=" + purchaseEntityId + ", instalmentChargeType=" + instalmentChargeType + "}"+super.toString();
    }

}
