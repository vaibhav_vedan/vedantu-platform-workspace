package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.VerificationCodeStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class VerificationCode extends AbstractMongoStringIdEntity {

	private Long userId;
	private int verificationCode;
	private VerificationCodeStatus status;
	private int amount;

	public VerificationCode(Long userId, int verificationCode, VerificationCodeStatus status, int amount) {
		super();
		this.userId = userId;
		this.verificationCode = verificationCode;
		this.status = status;
		this.amount = amount;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public int getVerificationCode() {
		return verificationCode;
	}

	public void setVerificationCode(int verificationCode) {
		this.verificationCode = verificationCode;
	}

	public VerificationCodeStatus getStatus() {
		return status;
	}

	public void setStatus(VerificationCodeStatus status) {
		this.status = status;
	}

	public VerificationCode(Long userId, int verificationCode, VerificationCodeStatus status) {
		super();
		this.userId = userId;
		this.verificationCode = verificationCode;
		this.status = status;
	}

	public VerificationCode() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "VerificationCode [userId=" + userId + ", verificationCode=" + verificationCode + ", status=" + status
				+ ", amount=" + amount + "]";
	}

}
