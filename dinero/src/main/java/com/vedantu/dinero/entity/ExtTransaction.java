package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.*;
import com.vedantu.dinero.enums.payment.PaymentMethodType;
import com.vedantu.dinero.pojo.EmiInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.generator.DistributedSequenceGenerator;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;

@CompoundIndexes( {
        @CompoundIndex(name = "creationTime_Idx",def = "{'creationTime':1}",background = true)
})
public class ExtTransaction extends AbstractMongoStringIdEntity {

    private Long userId;
    private String ipAddress;
    private TransactionType type;
    private TransactionStatus status;
    private String gatewayStatus;
    private String gatewayResponse;
    private MarkSuccessType markSuccessType = MarkSuccessType.PAYMENT_GATEWAY;
    private String markSuccessReason;

    @Indexed(background = true)
    private String parentTransactionId;

    // amount in paisa
    private Integer amount;
    // (currencyCode==INR)
    private String currencyCode;
    private String transactionTime;
    private String paymentChannelTransactionId;
    private String paymentMethod;
    private String paymentInstrument;
    private String bankRefNo;
    // vedantuOrderId --> this is id of Order entry created which is
    // in PENDING state when some one directly buys product and do payment from
    // Payment Gateway
    @Indexed(background = true)
    private String vedantuOrderId;//when paymentType is BULK, it is orderId, if it is INSTALMENT, it is instalmentId
    private PaymentType paymentType;
    private String redirectUrl;
    private String checksum;
    private PaymentGatewayName gatewayName;
    private TransactionRefType refType;
    private String refId;
    private String city;
    private String country;
    private String state;

    // USED Only in juspay to create orders
    private PaymentMethodType optedPaymentMethodType;
    private String bankResponseMessage;
    private String redirectedGateway;
    private EmiInfo emiInfo;
    private Integer preSubventionAmount;
    private Integer subventionAmount;

// USED Only in razorpay & jupayto create orders
    private String uniqueOrderId = String.valueOf(DistributedSequenceGenerator.getInstance().nextId());

    public ExtTransaction() {

        super();
    }

    public ExtTransaction(Long userId, String ipAddress, TransactionType type,
            TransactionStatus status, Integer amount, String currencyCode,
            String redirectUrl, PaymentGatewayName gatewayName) {

        this();
        this.userId = userId;
        this.ipAddress = ipAddress;
        this.type = type;
        this.status = status;
        this.amount = amount;
        this.currencyCode = currencyCode;
        this.redirectUrl = redirectUrl;
        this.gatewayName = gatewayName;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public String getIpAddress() {

        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {

        this.ipAddress = ipAddress;
    }

    public TransactionType getType() {

        return type;
    }

    public void setType(TransactionType type) {

        this.type = type;
    }

    public TransactionStatus getStatus() {

        return status;
    }

    public void setStatus(TransactionStatus status) {

        this.status = status;
    }

    public Integer getAmount() {

        return amount;
    }

    public void setAmount(Integer amount) {

        this.amount = amount;
    }

    public String getCurrencyCode() {

        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {

        this.currencyCode = currencyCode;
    }

    public String getTransactionTime() {

        return transactionTime;
    }

    public void setTransactionTime(String transactionTime) {

        this.transactionTime = transactionTime;
    }

    public String getPaymentChannelTransactionId() {

        return paymentChannelTransactionId;
    }

    public void setPaymentChannelTransactionId(
            String paymentChannelTransactionId) {

        this.paymentChannelTransactionId = paymentChannelTransactionId;
    }

    public String getPaymentMethod() {

        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {

        this.paymentMethod = paymentMethod;
    }

    public String getPaymentInstrument() {

        return paymentInstrument;
    }

    public void setPaymentInstrument(String paymentInstrument) {

        this.paymentInstrument = paymentInstrument;
    }

    public String getBankRefNo() {

        return bankRefNo;
    }

    public void setBankRefNo(String bankRefNo) {

        this.bankRefNo = bankRefNo;
    }

    public String getVedantuOrderId() {
        return vedantuOrderId;
    }

    public void setVedantuOrderId(String vedantuOrderId) {
        this.vedantuOrderId = vedantuOrderId;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    public PaymentGatewayName getGatewayName() {
        return gatewayName;
    }

    public void setGatewayName(PaymentGatewayName gatewayName) {
        this.gatewayName = gatewayName;
    }

    public TransactionRefType getRefType() {
        return refType;
    }

    public void setRefType(TransactionRefType refType) {
        this.refType = refType;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String DEFAULT_CURRENCY_CODE = "INR";
        public static final String USER_ID = "userId";
        public static final String VEDANTU_ORDER_ID = "vedantuOrderId";
        public static final String STATUS = "status";
        public static final String GATEWAY_STATUS = "gatewayStatus";
        public static final String GATEWAY_NAME = "gatewayName";
        public static final String PAYMENT_INSTRUMENT = "paymentInstrument";
        public static final String PAYMENT_METHOD = "paymentMethod";
        public static final String PARENT_TXN_ID = "parentTransactionId";
    }

    public MarkSuccessType getMarkSuccessType() {
        return markSuccessType;
    }

    public void setMarkSuccessType(MarkSuccessType markSuccessType) {
        this.markSuccessType = markSuccessType;
    }

    public String getMarkSuccessReason() {
        return markSuccessReason;
    }

    public void setMarkSuccessReason(String markSuccessReason) {
        this.markSuccessReason = markSuccessReason;
    }

    public enum MarkSuccessType {
        MANUAL, PAYMENT_GATEWAY, PAYMENT_GATEWAY_STATUS_CALLBACK
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

	public String getGatewayResponse() {
		return gatewayResponse;
	}

	public void setGatewayResponse(String gatewayResponse) {
		this.gatewayResponse = gatewayResponse;
	}

	public String getParentTransactionId() {
		return parentTransactionId;
	}

	public void setParentTransactionId(String parentTransactionId) {
		this.parentTransactionId = parentTransactionId;
	}

    public void createUniqueOrderId() {
        this.uniqueOrderId = String.valueOf(DistributedSequenceGenerator.getInstance().nextId());
    }

    public String getUniqueOrderId() {
        return uniqueOrderId;
    }

    public void setUniqueOrderId(String uniqueOrderId) {
        this.uniqueOrderId = uniqueOrderId;
    }

    public PaymentMethodType getOptedPaymentMethodType() {
        return optedPaymentMethodType;
    }

    public void setOptedPaymentMethodType(PaymentMethodType optedPaymentMethodType) {
        this.optedPaymentMethodType = optedPaymentMethodType;
    }

    public EmiInfo getEmiInfo() {
        return emiInfo;
    }

    public void setEmiInfo(EmiInfo emiInfo) {
        this.emiInfo = emiInfo;
    }

    public String getBankResponseMessage() {
        return bankResponseMessage;
    }

    public void setBankResponseMessage(String bankResponseMessage) {
        this.bankResponseMessage = bankResponseMessage;
    }

    public String getRedirectedGateway() {
        return redirectedGateway;
    }

    public void setRedirectedGateway(String redirectedGateway) {
        this.redirectedGateway = redirectedGateway;
    }

    public String getGatewayStatus() {
        return gatewayStatus;
    }

    public void setGatewayStatus(String gatewayStatus) {
        this.gatewayStatus = gatewayStatus;
    }

    public Integer getPreSubventionAmount() {
        return preSubventionAmount;
    }

    public void setPreSubventionAmount(Integer preSubventionAmount) {
        this.preSubventionAmount = preSubventionAmount;
    }

    public Integer getSubventionAmount() {
        return subventionAmount;
    }

    public void setSubventionAmount(Integer subventionAmount) {
        this.subventionAmount = subventionAmount;
    }
}
