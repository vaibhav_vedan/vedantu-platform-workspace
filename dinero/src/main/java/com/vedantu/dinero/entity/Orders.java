package com.vedantu.dinero.entity;

import com.vedantu.dinero.entity.Instalment.Instalment;
import com.vedantu.dinero.enums.*;
import com.vedantu.dinero.pojo.*;
import com.vedantu.scheduling.pojo.session.ReferenceTag;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.util.request.OrderEndType;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Document(collection = "Orders")
@CompoundIndexes({
        @CompoundIndex(name = "creationTime_1_items.entityType_1_paymentStatus_1", def = "{'creationTime': 1, 'items.entityType': 1, 'paymentStatus' : 1}", background = true),
        @CompoundIndex(name = "userId_1_paymentStatus_1", def = "{'userId': 1, 'paymentStatus' : 1}", background = true)
})
public class Orders extends AbstractMongoStringIdEntity {

    private Long userId;

    private List<OrderedItem> items = new ArrayList<>();

    private String ipAddress;

    private Integer initialFixedAmount; //this remains unchanged.

    private Integer amount;//items cost +security+convenience-discount

    //value will be zero for instalments initially as we cannot predit 
    //how much promotional and np amout will be paid before hand
    //will update these fields at the end of the payment
    private Integer promotionalAmount;

    private Integer nonPromotionalAmount;

    private Integer amountPaid;//paid till now

    private Integer promotionalAmountPaid;//paid till now

    private Integer nonPromotionalAmountPaid;//paid till now

    private PaymentStatus paymentStatus;

    private PaymentType paymentType = PaymentType.BULK;

    private String subscriptionPlanId ;

    private String transactionId;

    // in case of offering purchase it will be teacherId
    private Long contextId;

    private Integer security;

    private Integer convenienceCharge;

    private PurchaseFlowType purchaseFlowType = PurchaseFlowType.DIRECT;

    private String purchaseFlowId;

    private List<ReferenceTag> referenceTags = new ArrayList<>();
    private List<PurchasingEntity> purchasingEntities;

    private List<OrderPaymentStatusChange> orderPaymentStatusChanges;


    // Added as a part of tools enhancement
    private PaymentThrough paymentThrough;
    private TeamType teamType;
    private String agentCode;
    @Indexed(background = true)
    private String agentEmailId;
    private String reportingTeamLeadId;
    private String teamLeadEmailId;
    private Centre centre;
    private EnrollmentType enrollmentType;
    private FintechRisk fintechRisk;
    private RequestRefund requestRefund;
    private long netCollection;
    private String notes;
    private String purchaseContextId; // Bundle id from where it is coming
    private List<OrderAmountChangeLog> orderAmountChangeLog;
    private String reEnrollmentOrderId;

    // Adding changed logs in case of manual enrollment
    private List<ManuallyChangedLogs> manuallyChangedLogs;

    private OrderEndType orderEndType;
    private EmiInfo emiInfo;


    private String employeeId;

    private PaymentSource paymentSource;

    private String userAgent;

    // adding UTM parameters
    private static final long serialVersionUID = 1L;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_source;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_medium;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_campaign;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_term;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String utm_content;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String channel;

    private Integer initialDiscountAmount;

    private Integer initialPayableAmount;

    private String subscriptionUpdateId;

    public Orders() {
        super();
    }

    public Orders(Long userId, List<OrderedItem> items, String ipAddress, Integer amount, Integer promotionalAmount,
                  Integer nonPromotionalAmount, PaymentStatus paymentStatus, PurchaseFlowType purchaseFlowType,
                  String purchaseFlowId, PaymentType paymentType, PaymentSource paymentSource, String userAgent) {
        super();
        this.userId = userId;
        this.items = items;
        this.ipAddress = ipAddress;
        this.amount = amount;
        this.initialFixedAmount = amount;
        this.initialPayableAmount = amount;
        this.initialDiscountAmount = Optional.ofNullable(items).orElseGet(ArrayList::new)
                .stream().map(OrderedItem::getVedantuDiscountAmount).findFirst().orElse(0);
        this.promotionalAmount = promotionalAmount;
        this.nonPromotionalAmount = nonPromotionalAmount;
        this.paymentStatus = paymentStatus;
        this.purchaseFlowType = purchaseFlowType;
        this.purchaseFlowId = purchaseFlowId;
        this.paymentType = paymentType;
        this.paymentSource = paymentSource;
        this.userAgent = userAgent;
    }

    // TODO add constructor for Orders entity with added UTM parameters

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<OrderedItem> getItems() {
        return items;
    }

    public void setItems(List<OrderedItem> items) {
        this.items = items;
    }

    public Integer getPromotionalAmount() {
        if (promotionalAmount == null) {
            return 0;
        } else {
            return promotionalAmount;
        }
    }

    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public Integer getNonPromotionalAmount() {
        if (nonPromotionalAmount == null) {
            return 0;
        } else {
            return nonPromotionalAmount;
        }
    }

    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public Integer getAmount() {
        if (amount == null) {
            return 0;
        } else {
            return amount;
        }
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Long getContextId() {
        return contextId;
    }

    public void setContextId(Long contextId) {
        this.contextId = contextId;
    }

    public Integer getAmountPaid() {
        if (amountPaid == null) {
            return 0;
        } else {
            return amountPaid;
        }
    }

    public void setAmountPaid(Integer amountPaid) {
        this.amountPaid = amountPaid;
    }

    public Integer getPromotionalAmountPaid() {
        if (promotionalAmountPaid == null) {
            return 0;
        } else {
            return promotionalAmountPaid;
        }
    }

    public void setPromotionalAmountPaid(Integer promotionalAmountPaid) {
        this.promotionalAmountPaid = promotionalAmountPaid;
    }

    public Integer getNonPromotionalAmountPaid() {
        if (nonPromotionalAmountPaid == null) {
            return 0;
        } else {
            return nonPromotionalAmountPaid;
        }
    }

    public void setNonPromotionalAmountPaid(Integer nonPromotionalAmountPaid) {
        this.nonPromotionalAmountPaid = nonPromotionalAmountPaid;
    }

    public PaymentType getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(PaymentType paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getSecurity() {
        if (security == null) {
            return 0;
        } else {
            return security;
        }
    }

    public void setSecurity(Integer security) {
        this.security = security;
    }

    public Integer getConvenienceCharge() {
        if (convenienceCharge == null) {
            return 0;
        } else {
            return convenienceCharge;
        }
    }

    public void setConvenienceCharge(Integer convenienceCharge) {
        this.convenienceCharge = convenienceCharge;
    }

    public PurchaseFlowType getPurchaseFlowType() {
        return purchaseFlowType;
    }

    public void setPurchaseFlowType(PurchaseFlowType purchaseFlowType) {
        this.purchaseFlowType = purchaseFlowType;
    }

    public String getPurchaseFlowId() {
        return purchaseFlowId;
    }

    public void setPurchaseFlowId(String purchaseFlowId) {
        this.purchaseFlowId = purchaseFlowId;
    }

    public List<ReferenceTag> getReferenceTags() {
        return referenceTags;
    }

    public void setReferenceTags(List<ReferenceTag> referenceTags) {
        this.referenceTags = referenceTags;
    }

    public List<PurchasingEntity> getPurchasingEntities() {
        return purchasingEntities;
    }

    public void setPurchasingEntities(List<PurchasingEntity> purchasingEntities) {
        this.purchasingEntities = purchasingEntities;
    }

    public List<ManuallyChangedLogs> getManuallyChangedLogs() {
        return manuallyChangedLogs;
    }

    public void setManuallyChangedLogs(List<ManuallyChangedLogs> manuallyChangedLogs) {
        this.manuallyChangedLogs = manuallyChangedLogs;
    }

    public void updateOrderPaidDetails(Orders order, Instalment instalment) {
        order.setAmountPaid(order.getAmountPaid()
                + instalment.getTotalNonPromotionalAmount()
                + instalment.getTotalPromotionalAmount());
        order.setNonPromotionalAmountPaid(order.getNonPromotionalAmountPaid()
                + instalment.getTotalNonPromotionalAmount());
        order.setPromotionalAmountPaid(order.getPromotionalAmountPaid()
                + instalment.getTotalPromotionalAmount());
        order.setSecurity(order.getSecurity() + instalment.getSecurityCharge());
        order.setConvenienceCharge(order.getConvenienceCharge() + instalment.getConvenienceCharge());
        List<OrderPaymentStatusChange> orderPaymentStatusChange = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(order.getOrderPaymentStatusChanges())) {
            orderPaymentStatusChange = order.getOrderPaymentStatusChanges();
        }
        orderPaymentStatusChange.add(new OrderPaymentStatusChange(this.paymentStatus, PaymentStatus.PARTIALLY_PAID, System.currentTimeMillis(), instalment.getLastUpdatedBy()));
        order.setOrderPaymentStatusChanges(orderPaymentStatusChange);
        order.setPaymentStatus(PaymentStatus.PARTIALLY_PAID);
        OrderedItem orderedItem = order.getItems().get(0);
        if (instalment.getVedantuDiscountAmount() != null) {
            if (orderedItem.getVedantuDiscountAmountClaimed() == null) {
                orderedItem.setVedantuDiscountAmountClaimed(0);
            }
            orderedItem.setVedantuDiscountAmountClaimed(orderedItem.getVedantuDiscountAmountClaimed()
                    + instalment.getVedantuDiscountAmount());
        }
        if (instalment.getTeacherDiscountAmount()!= null) {
            if (orderedItem.getTeacherDiscountAmountClaimed()== null) {
                orderedItem.setTeacherDiscountAmountClaimed(0);
            }
            orderedItem.setTeacherDiscountAmountClaimed(orderedItem.getTeacherDiscountAmountClaimed()
                    + instalment.getTeacherDiscountAmount());
        }
    }

    public EmiInfo getEmiInfo() {
        return emiInfo;
    }

    public void setEmiInfo(EmiInfo emiInfo) {
        this.emiInfo = emiInfo;
    }

    public PaymentThrough getPaymentThrough() {
        return paymentThrough;
    }

    public void setPaymentThrough(PaymentThrough paymentThrough) {
        this.paymentThrough = paymentThrough;
    }

    public TeamType getTeamType() {
        return teamType;
    }

    public void setTeamType(TeamType teamType) {
        this.teamType = teamType;
    }

    public String getAgentEmailId() {
        return agentEmailId;
    }

    public void setAgentEmailId(String agentEmailId) {
        this.agentEmailId = agentEmailId;
    }

    public String getTeamLeadEmailId() {
        return teamLeadEmailId;
    }

    public void setTeamLeadEmailId(String teamLeadEmailId) {
        this.teamLeadEmailId = teamLeadEmailId;
    }

    public Centre getCentre() {
        return centre;
    }

    public void setCentre(Centre centre) {
        this.centre = centre;
    }

    public EnrollmentType getEnrollmentType() {
        return enrollmentType;
    }

    public void setEnrollmentType(EnrollmentType enrollmentType) {
        this.enrollmentType = enrollmentType;
    }

    public FintechRisk getFintechRisk() {
        return fintechRisk;
    }

    public void setFintechRisk(FintechRisk fintechRisk) {
        this.fintechRisk = fintechRisk;
    }

    public RequestRefund getRequestRefund(){return requestRefund;}

    public void setRequestRefund(RequestRefund requestRefund) { this.requestRefund = requestRefund;}

    public long getNetCollection() {
        return netCollection;
    }

    public void setNetCollection(long netCollection) {
        this.netCollection = netCollection;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public OrderEndType getOrderEndType() {
        return orderEndType;
    }

    public void setOrderEndType(OrderEndType orderEndType) {
        this.orderEndType = orderEndType;
    }

    public String getPurchaseContextId() {
        return purchaseContextId;
    }

    public void setPurchaseContextId(String purchaseContextId) {
        this.purchaseContextId = purchaseContextId;
    }

    public String getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    public String getAgentCode() {
        return agentCode;
    }

    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    public String getReportingTeamLeadId() {
        return reportingTeamLeadId;
    }

    public void setReportingTeamLeadId(String reportingTeamLeadId) {
        this.reportingTeamLeadId = reportingTeamLeadId;
    }

    public List<OrderAmountChangeLog> getOrderAmountChangeLog() {
        if (orderAmountChangeLog == null) {
            orderAmountChangeLog = new ArrayList<>();
        }
        return orderAmountChangeLog;
    }

    public void setOrderAmountChangeLog(List<OrderAmountChangeLog> orderAmountChangeLog) {
        this.orderAmountChangeLog = orderAmountChangeLog;
    }

    public PaymentSource getPaymentSource() {
        return paymentSource;
    }

    public void setPaymentSource(PaymentSource paymentSource) {
        this.paymentSource = paymentSource;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getReEnrollmentOrderId() {
        return reEnrollmentOrderId;
    }

    public void setReEnrollmentOrderId(String reEnrollmentOrderId) {
        this.reEnrollmentOrderId = reEnrollmentOrderId;
    }

    public String getSubscriptionPlanId() {
        return subscriptionPlanId;
    }

    public void setSubscriptionPlanId(String subscriptionPlanId) {
        this.subscriptionPlanId = subscriptionPlanId;
    }

    public Integer getInitialFixedAmount() {
        return initialFixedAmount;
    }

    public void setInitialFixedAmount(Integer initialFixedAmount) {
        this.initialFixedAmount = initialFixedAmount;
    }

    public Integer getInitialDiscountAmount() {
        return initialDiscountAmount;
    }

    public void setInitialDiscountAmount(Integer initialDiscountAmount) {
        this.initialDiscountAmount = initialDiscountAmount;
    }

    public Integer getInitialPayableAmount() {
        return initialPayableAmount;
    }

    public void setInitialPayableAmount(Integer initialPayableAmount) {
        this.initialPayableAmount = initialPayableAmount;
    }

    public String getSubscriptionUpdateId() {
        return subscriptionUpdateId;
    }

    public void setSubscriptionUpdateId(String subscriptionUpdateId) {
        this.subscriptionUpdateId = subscriptionUpdateId;
    }

    public List<OrderPaymentStatusChange> getOrderPaymentStatusChanges() {
        return orderPaymentStatusChanges;
    }

    public void setOrderPaymentStatusChanges(List<OrderPaymentStatusChange> orderPaymentStatusChanges) {
        this.orderPaymentStatusChanges = orderPaymentStatusChanges;
    }

    public void setPaymentStatusAndOrderPaymentStatusChange(PaymentStatus paymentStatus , String updatedBy){
        if(this.orderPaymentStatusChanges == null){
            this.orderPaymentStatusChanges = new ArrayList<>();
        }
        this.orderPaymentStatusChanges.add(new OrderPaymentStatusChange(this.paymentStatus, paymentStatus, System.currentTimeMillis(), updatedBy));
        this.paymentStatus = paymentStatus;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String PAYMENT_STATUS = "paymentStatus";
        public static final String PAYMENT_TYPE = "paymentType";
        public static final String USER_ID = "userId";
        public static final String ITEMS_ENTITY_ID = "items.entityId";
        public static final String ITEMS_DELIVERABLE_ID = "items.deliverableEntityId";
        public static final String ITEMS_ENTITY_TYPE = "items.entityType";
        public static final String ITEMS_REFUND_STATUS = "items.refundStatus";
        public static final String ITEMS = "items";
        public static final String AMOUNT = "amount";
        public static final String PURCHASING_ENTITIES_DELIVERABLE_ID = "purchasingEntities.deliverableId";
        public static final String PURCHASING_ENTITIES_ENTITY_TYPE = "purchasingEntities.entityType";
        // Added as a part of tools enhancement
        public static final String AGENT_EMAIL_ID="agentEmailId";
        public static final String PAYMENT_THROUGH="paymentThrough";
        public static final String TEAM_TYPE="teamType";
        public static final String TEAM_LEAD_EMAIL_ID="teamLeadEmailId";
        public static final String CENTRE="centre";
        public static final String ENROLLMENT_TYPE="enrollmentType";
        public static final String FINTECH_RISK="fintechRisk";
        public static final String REQUEST_REFUND = "requestRefund";
        public static final String NET_COLLECTION="netCollection";
        public static final String NON_PROMOTIONAL_AMOUNT="nonPromotionalAmount";
        public static final String MANUALLY_CHANGED_LOGS="manuallyChangedLogs";
        public static final String NOTES="notes";
        public static final String AGENT_CODE="agentCode";
        public static final String REPORT_TEAMLEAD_ID="reportingTeamLeadId";
        public static final String EMPLOYEE_ID = "employeeId";
        public static final String INITIAL_FIXED_AMOUNT = "initialFixedAmount";
        public static final String AMOUNT_PAID = "amountPaid";
        public static final String MANUALLY_CHANGED_LOGS_AGENT_CODE = "manuallyChangedLogs.agentCode";


    }



}
