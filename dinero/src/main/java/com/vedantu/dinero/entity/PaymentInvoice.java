package com.vedantu.dinero.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.util.CollectionUtils;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.InvoiceStateCode;
import com.vedantu.dinero.enums.PaymentInvoiceContextType;
import com.vedantu.dinero.enums.PaymentInvoiceType;
import com.vedantu.dinero.pojo.TaxPercentage;
import com.vedantu.dinero.request.AddPaymentInvoiceReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;

public class PaymentInvoice extends AbstractMongoStringIdEntity {

        @Indexed(background = true)
	private String invoiceNo;
	private Long fromUserId;
	private Role fromUserRole;
        @Indexed(background = true)
	private Long toUserId;
	private Role toUserRole;

	// TODO : Need to introduce unique key to ensure multiple invoice wont be
	// generated in retries

	private Long totalAmount; // In paisa. Including all the taxes
	private Long actualAmount;//includes promotional component in case of vedantu teacher invoices
        private Long promotionalAmount;
        private Long taxableAmount;//totalAmount-taxes=taxableamount, this will be backcalculated
        
	private Long invoiceTime;
	private String invoiceTitle;
	private String invoiceDescription;

	private PaymentInvoiceTypeDetails paymentInvoiceTypeDetails;
	private DeliverableEntityType deliverableEntityType;
	private String deliverableEntityId; // This has to be unique
	private String orderId;
	private List<String> tags;
        private PaymentInvoiceContextType contextType;
        @Indexed(background = true)
        private String contextId;
        private String ipAddress;
        private String supplyState;
        private String address;
        private String country;
        private InvoiceStateCode invoiceStateCode;
        private UserBasicInfo toUser;

	public PaymentInvoice() {
		super();
	}

	public PaymentInvoice(AddPaymentInvoiceReq req) {
		super();
		// this.totalAmount = req.getTotalAmount(); Tax info should be filled when
		// totalAmount is set
		this.invoiceTime = req.getInvoiceTime() == null ? System.currentTimeMillis() : req.getInvoiceTime();
		this.invoiceTitle = req.getInvoiceTitle();
		this.invoiceDescription = req.getInvoiceDescription();
		this.deliverableEntityId = req.getDeliverableEntityId();
		this.deliverableEntityType = req.getDeliverableEntityType();
		this.orderId = req.getOrderId();
		this.tags = req.getTags();
	}

	public Long getFromUserId() {
		return fromUserId;
	}

	public void setFromUserId(Long fromUserId) {
		this.fromUserId = fromUserId;
	}

	public Long getToUserId() {
		return toUserId;
	}

	public void setToUserId(Long toUserId) {
		this.toUserId = toUserId;
	}

	public Long getInvoiceTime() {
		return invoiceTime;
	}

	public void setInvoiceTime(Long invoiceTime) {
		this.invoiceTime = invoiceTime;
	}

	public String getInvoiceTitle() {
		return invoiceTitle;
	}

	public void setInvoiceTitle(String invoiceTitle) {
		this.invoiceTitle = invoiceTitle;
	}

	public String getInvoiceDescription() {
		return invoiceDescription;
	}

	public void setInvoiceDescription(String invoiceDescription) {
		this.invoiceDescription = invoiceDescription;
	}

	public PaymentInvoiceTypeDetails getPaymentInvoiceTypeDetails() {
		return paymentInvoiceTypeDetails;
	}

	public void setPaymentInvoiceTypeDetails(PaymentInvoiceTypeDetails paymentInvoiceTypeDetails) {
		this.paymentInvoiceTypeDetails = paymentInvoiceTypeDetails;
	}

	public DeliverableEntityType getDeliverableEntityType() {
		return deliverableEntityType;
	}

	public void setDeliverableEntityType(DeliverableEntityType deliverableEntityType) {
		this.deliverableEntityType = deliverableEntityType;
	}

	public String getDeliverableEntityId() {
		return deliverableEntityId;
	}

	public void setDeliverableEntityId(String deliverableEntityId) {
		this.deliverableEntityId = deliverableEntityId;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public Role getFromUserRole() {
		return fromUserRole;
	}

	public Role getToUserRole() {
		return toUserRole;
	}

	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public void setFromUserRole(Role fromUserRole) {
		this.fromUserRole = fromUserRole;
	}

	public void setToUserRole(Role toUserRole) {
		this.toUserRole = toUserRole;
	}
        
        public Long getActualAmount() {
            return actualAmount;
        }

        public void setActualAmount(Long actualAmount) {
            this.actualAmount = actualAmount;
        }

        public Long getPromotionalAmount() {
            return promotionalAmount;
        }

        public void setPromotionalAmount(Long promotionalAmount) {
            this.promotionalAmount = promotionalAmount;
        }

        public Long getTaxableAmount() {
            return taxableAmount;
        }

        public void setTaxableAmount(Long taxableAmount) {
            this.taxableAmount = taxableAmount;
        }

        public Long getTotalAmount() {
            return totalAmount;
        }

        public void setTotalAmount(Long totalAmount) {
            this.totalAmount = totalAmount;
        }
        
	public void updateTotalAmount(long totalAmount, PaymentInvoiceTypeDetails paymentInvoiceTypeDetails) {
		if (paymentInvoiceTypeDetails != null) {
			// Update actual amount
			long taxAmount = 0l;
			for (TaxPercentage taxPercentage : paymentInvoiceTypeDetails.getTaxPercentages()) {
				taxPercentage.updateAmount(totalAmount);
				taxAmount += taxPercentage.getAmount();
			}

			this.totalAmount = totalAmount;
			this.actualAmount = totalAmount - taxAmount;
			this.paymentInvoiceTypeDetails = paymentInvoiceTypeDetails;
		}
	}
        
        public void updateTaxableAmount(long taxable, PaymentInvoiceTypeDetails paymentInvoiceTypeDetails, long totalAmount) {
		if (paymentInvoiceTypeDetails != null) {
			// Update actual amount
			long taxAmount = 0l;
			for (TaxPercentage taxPercentage : paymentInvoiceTypeDetails.getTaxPercentages()) {
				taxPercentage.updateAmount(taxable);
				taxAmount += taxPercentage.getAmount();
			}

			this.taxableAmount = taxable;
			this.totalAmount = totalAmount;
			this.paymentInvoiceTypeDetails = paymentInvoiceTypeDetails;
		}
	}

        public PaymentInvoiceContextType getContextType() {
            return contextType;
        }

        public void setContextType(PaymentInvoiceContextType contextType) {
            this.contextType = contextType;
        }

        public String getContextId() {
            return contextId;
        }

        public void setContextId(String contextId) {
            this.contextId = contextId;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public void setIpAddress(String ipAddress) {
            this.ipAddress = ipAddress;
        }

        public String getSupplyState() {
            return supplyState;
        }

        public void setSupplyState(String supplyState) {
            this.supplyState = supplyState;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public InvoiceStateCode getInvoiceStateCode() {
            return invoiceStateCode;
        }

        public UserBasicInfo getToUser() {
            return toUser;
        }

        public void setToUser(UserBasicInfo toUser) {
            this.toUser = toUser;
        }

        public void setInvoiceStateCode(InvoiceStateCode invoiceStateCode) {
            this.invoiceStateCode = invoiceStateCode;
        }

	public void updateActualAmount(long actualAmount, PaymentInvoiceTypeDetails paymentInvoiceTypeDetails) {
		if (paymentInvoiceTypeDetails != null) {
			// Update actual amount
			long taxAmount = 0l;
			for (TaxPercentage taxPercentage : paymentInvoiceTypeDetails.getTaxPercentages()) {
				taxPercentage.updateAmount(actualAmount);
				taxAmount += taxPercentage.getAmount();
			}

			this.actualAmount = actualAmount;
			this.totalAmount = actualAmount + taxAmount;
			this.paymentInvoiceTypeDetails = paymentInvoiceTypeDetails;
		}
	}

	public void updateUserIds(AddPaymentInvoiceReq req) throws BadRequestException {
		updateUserIds(req.getPaymentInvoiceType(), req.getStudentId(), req.getTeacherId());
	}

	public void updateUserIds(PaymentInvoiceType paymentInvoiceType, Long studentId, Long teacherId)
			throws BadRequestException {
		if (paymentInvoiceType == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Payment invoice type is null");
		}

		if (paymentInvoiceType.getFromUserRole() != null) {
			switch (paymentInvoiceType.getFromUserRole()) {
			case STUDENT:
				this.fromUserId = studentId;
				break;
			case TEACHER:
				this.fromUserId = teacherId;
				break;
			default:
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
						"SetFromUserId - Invalid role " + paymentInvoiceType.getFromUserRole());
			}
			this.fromUserRole = paymentInvoiceType.getFromUserRole();
		}

		if (paymentInvoiceType.getToUserRole() != null) {
			switch (paymentInvoiceType.getToUserRole()) {
			case STUDENT:
				this.toUserId = studentId;
				break;
			case TEACHER:
				this.toUserId = teacherId;
				break;
			default:
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
						"SetToUserId - Invalid role " + paymentInvoiceType.getFromUserRole());
			}
			this.toUserRole = paymentInvoiceType.getToUserRole();
		}
	}
        
	public void addTags(Collection<String> c) {
		if (!CollectionUtils.isEmpty(c)) {
			if (this.tags == null) {
				this.tags = new ArrayList<>();
			}

			this.tags.addAll(c);
		}
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String FROM_USER_ID = "fromUserId";
		public static final String TO_USER_ID = "toUserId";
		public static final String INVOICE_TIME = "invoiceTime";
	}
}
