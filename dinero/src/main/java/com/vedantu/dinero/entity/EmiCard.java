package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.EmiCardType;
import com.vedantu.dinero.enums.EmiType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Document(collection = "EmiCard")
@Data
@CompoundIndexes({
        @CompoundIndex(name = "code_1_months_1_type_1_cardtype_1", def = "{'code': 1, 'months': 1, 'cardType':1,'type':1}", background = true, unique = true)
})
public class EmiCard extends AbstractMongoStringIdEntity {
    private String bankName;
    @NotEmpty
    private String code;
    // type of EMI like debit card , credit card, card less
    private EmiCardType cardType;
    private EmiType type;
    @Max(24)
    @Min(0)
    private Integer months;
    @Min(0)
    private Float rate;
    @Indexed(background = true)
    //minimum amount in paisa
    private Long minimumAmount;
    @Indexed(background = true)
    private Boolean isActive;
    @Size(max = 100)
    private String offerId;
    private Boolean isEnable;


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String MINIMUM_AMOUNT = "minimumAmount";
        public static final String IS_ACTIVE = "isActive";
        public static final String CODE = "code";
        public static final String TYPE = "type";
        public static final String EMI_CARD_TYPE = "cardType";
        public static final String IS_ENABLE = "isEnable";
        public static final String OFFER_ID = "offerId";

    }
}
