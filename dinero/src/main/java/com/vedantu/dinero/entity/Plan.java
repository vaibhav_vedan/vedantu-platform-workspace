package com.vedantu.dinero.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;


public class Plan extends AbstractMongoStringIdEntity{
	
	private String slabId;
	private Long teacherId;
	private Long price;
	private boolean active;
	
	public Plan() {
		super();
	}

	public Plan(String slabId, Long teacherId, Long price, boolean active) {
		super();
		this.slabId = slabId;
		this.teacherId = teacherId;
		this.price = price;
		this.active = active;
	}

	@Override
	public String toString() {
		return "Plan [slabId=" + slabId + ", teacherId=" + teacherId + ", price=" + price + ", active=" + active + "]";
	}
	
	public String getSlabId() {
		return slabId;
	}
	public void setSlabId(String slabId) {
		this.slabId = slabId;
	}
	public Long getTeacherId() {
		return teacherId;
	}
	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}
	public Long getPrice() {
		return price;
	}
	public void setPrice(Long price) {
		this.price = price;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	
}
