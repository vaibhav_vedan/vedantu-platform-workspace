package com.vedantu.dinero.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;


public class Slab extends AbstractMongoStringIdEntity{
	
	private String slabRangeId;
	private String subject;
	private String target;
	private Integer grade;
	private boolean active = true;
	
	public String getSlabRangeId() {
		return slabRangeId;
	}
	public void setSlabRangeId(String slabRangeId) {
		this.slabRangeId = slabRangeId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getTarget() {
		return target;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public Integer getGrade() {
		return grade;
	}
	public void setGrade(Integer grade) {
		this.grade = grade;
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
	@Override
	public String toString() {
		return "Slab [slabRangeId=" + slabRangeId + ", subject=" + subject + ", target=" + target + ", grade=" + grade
				+ ", active=" + active + "]";
	}
	
	public Slab(String slabRangeId, String subject, String target, Integer grade, boolean active) {
		super();
		this.slabRangeId = slabRangeId;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.active = active;
	}
	
	public Slab() {
		super();
	}
	
}
