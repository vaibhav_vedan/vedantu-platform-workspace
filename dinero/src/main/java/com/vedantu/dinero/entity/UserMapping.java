package com.vedantu.dinero.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = true)
@Document(collection = "UserMapping")
public class UserMapping extends AbstractMongoStringIdEntity {

    @Indexed(background = true, unique = true)
    @NotNull(message = "User ID cannot be null or empty")
    private Long vedantuUserId;

    @Indexed(background = true, unique = true)
    @NotBlank(message = "Juspay User ID cannot be null or empty")
    private String juspayCustomerId;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String VEDANTU_USER_ID = "vedantuUserId";
        public static final String JUSPAY_CUSTOMER_ID = "juspayCustomerId";
    }
}
