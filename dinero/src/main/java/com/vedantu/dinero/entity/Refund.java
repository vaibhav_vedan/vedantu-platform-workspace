/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "Refund")
public class Refund extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String orderId;
    private String transactionId;
    private Integer amount;// amount in paisa
    private Integer promotionalAmount;
    private Integer nonPromotionalAmount;
    private Integer discountLeft;//the discount amt transferred back to vedantu marketing wallet
    private String reason;
    private Long actualRefundDate;


    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public Integer getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public Integer getDiscountLeft() {
        return discountLeft;
    }

    public void setDiscountLeft(Integer discountLeft) {
        this.discountLeft = discountLeft;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Long getActualRefundDate() {
        return actualRefundDate;
    }
    public void setActualRefundDate(Long actualRefundDate) {
        this.actualRefundDate = actualRefundDate;
    }

}
