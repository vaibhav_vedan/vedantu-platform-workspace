package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.SessionEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.SessionModel;

public class TeacherPayoutRate extends AbstractMongoStringIdEntity {

	private Long teacherId;
	private SessionModel model;
	private String subject;
	private String target;
	private Integer grade;
	private Double cut;
	private SessionEntity entity;
	private String entityId;

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public Double getCut() {
		return cut;
	}

	public void setCut(Double cut) {
		this.cut = cut;
	}

	public SessionEntity getEntity() {
		return entity;
	}

	public void setEntity(SessionEntity entity) {
		this.entity = entity;
	}

	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	@Override
	public String toString() {
		return "TeacherPayoutRate [teacherId=" + teacherId + ", model=" + model + ", subject=" + subject + ", target="
				+ target + ", grade=" + grade + ", cut=" + cut + ", entity=" + entity + ", entityId=" + entityId + "]";
	}

	public TeacherPayoutRate(Long teacherId, SessionModel model, String subject, String target, Integer grade,
			Double cut, SessionEntity entity, String entityId) {

		super();
		this.teacherId = teacherId;
		this.model = model;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.cut = cut;
		this.entity = entity;
		this.entityId = entityId;
	}

	public TeacherPayoutRate() {
		super();
		// TODO Auto-generated constructor stub
	}

}
