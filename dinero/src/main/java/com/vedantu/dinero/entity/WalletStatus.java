package com.vedantu.dinero.entity;

import com.vedantu.dinero.enums.TimeState;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class WalletStatus extends AbstractMongoStringIdEntity {

	private String holderId;
	private TimeState timeState;
	private int balance;
	private int promotionalBalance;
	private int nonPromotionalBalance;

	public WalletStatus(String holderId, TimeState timeState, int balance, int promotionalBalance,
			int nonPromotionalBalance) {
		super();
		this.holderId = holderId;
		this.timeState = timeState;
		this.balance = balance;
		this.promotionalBalance = promotionalBalance;
		this.nonPromotionalBalance = nonPromotionalBalance;
	}

	public int getPromotionalBalance() {
		return promotionalBalance;
	}

	public void setPromotionalBalance(int promotionalBalance) {
		this.promotionalBalance = promotionalBalance;
	}

	public int getNonPromotionalBalance() {
		return nonPromotionalBalance;
	}

	public void setNonPromotionalBalance(int nonPromotionalBalance) {
		this.nonPromotionalBalance = nonPromotionalBalance;
	}

	public WalletStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getHolderId() {
		return holderId;
	}

	public void setHolderId(String holderId) {
		this.holderId = holderId;
	}

	public TimeState getTimeState() {
		return timeState;
	}

	public void setTimeState(TimeState timeState) {
		this.timeState = timeState;
	}

	public int getBalance() {
		return balance;
	}

	public void setBalance(int balance) {
		this.balance = balance;
	}

	@Override
	public String toString() {
		return "VedantuWalletStatus [holderId=" + holderId + ", timeState=" + timeState + ", balance=" + balance 
                        +",nonPromotionalBalance="+nonPromotionalBalance
                        +",promotionalBalance="+promotionalBalance
                        + "]";
	}

}
