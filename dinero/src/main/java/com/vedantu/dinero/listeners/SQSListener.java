/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.listeners;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.vedantu.User.User;
import com.vedantu.dinero.entity.Instalment.BaseInstalment;
import com.vedantu.dinero.managers.AccountManager;
import com.vedantu.dinero.managers.InstalmentManager;
import com.vedantu.dinero.managers.PaymentInvoiceManager;
import com.vedantu.dinero.request.CreatePaymentInvoiceReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.enums.SQSMessageType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;

/**
 *
 * @author jeet
 */
@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    @Autowired
    private PaymentInvoiceManager paymentInvoiceManager;

    @Autowired
    private InstalmentManager instalmentManager;

    @Autowired
    private AccountManager accountManager;

    @Autowired
    StatsdClient statsdClient;

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            Long startTime = System.currentTimeMillis();
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(sqsMessageType, textMessage.getText());
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                if (null != sqsMessageType) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }

        } catch (Exception e) {
            logger.error("Error processing message:" + textMessage + " error:" + e.getMessage());
        }
    }

    public void handleMessage(SQSMessageType sqsMessageType, String text) throws Exception {

        switch (sqsMessageType) {
            case GENERATE_STUDENT_INVOICE:
                CreatePaymentInvoiceReq invoiceRequest = new Gson().fromJson(text, CreatePaymentInvoiceReq.class);
                try {
                    paymentInvoiceManager.createStudentPaymentInvoice(invoiceRequest);
                } catch (Exception ex) {
                    logger.error("there was a error creating Invoice for the transaction:" + " error: " + ex.getMessage() + " req " + invoiceRequest);
                }
                break;
            case BASE_INSTALMENT_SAVE:
                JsonObject baseInstalmentJson = new Gson().fromJson(text, JsonObject.class);
                Type baseInstalmentType = new TypeToken<BaseInstalment>() {
                }.getType();
                BaseInstalment baseInstalment = new Gson().fromJson(baseInstalmentJson.get("baseInstalment"), baseInstalmentType);
                logger.info("baseInstalment save request received for " + baseInstalment.toString());
                instalmentManager.addEditBaseInstalmentForUsers(baseInstalment);
                break;
            case CREATE_NEW_USER_ACCOUNT_REQ:
                User user = new Gson().fromJson(text, User.class);
                logger.info("new Account creation with User : " + user);
                accountManager.createUserAccountFromSQS(user);
                break;
            default:
                logger.error("invalid message received in sqs listener for  " + sqsMessageType.name() + ", " + text);
                break;

        }
        logger.info("Message handled");
    }
}
