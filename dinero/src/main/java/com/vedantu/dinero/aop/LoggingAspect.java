/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.dinero.aop;

import com.vedantu.util.LogFactory;
import com.vedantu.util.aop.AbstractAOPLayer;
import com.vedantu.util.logstash.LogstashLayout;
import javax.servlet.http.HttpServletRequest;
import org.apache.logging.log4j.Logger;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
@Aspect
public class LoggingAspect extends AbstractAOPLayer{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LoggingAspect.class);
    
    @Before("allCtrlMethods()")
    public void beforeExecutionForCtrlMethods(JoinPoint jp) {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes.getRequest() == null) {
            return;
        }

        HttpServletRequest request = servletRequestAttributes.getRequest();
        LogstashLayout.addToMDC(request);

    }
    
    //tracking dao methods calls
    @Pointcut("within(com.vedantu.dinero.serializers..*) || within(com.vedantu.dinero.sql.dao..*)")
    public void allDAOMethods() {
    }
    
}
