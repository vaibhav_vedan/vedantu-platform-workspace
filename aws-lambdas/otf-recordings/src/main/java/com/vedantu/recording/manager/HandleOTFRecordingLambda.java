package com.vedantu.recording.manager;

import java.util.ArrayList;
import java.util.List;

public class HandleOTFRecordingLambda {
	private String environment;
	private String sessionId;
	private List<RecordingEntry> recordings;

	public HandleOTFRecordingLambda() {
		super();
	}

	public HandleOTFRecordingLambda(String environment, String sessionId) {
		super();
		this.environment = environment;
		this.sessionId = sessionId;
		this.recordings = new ArrayList<>();
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public List<RecordingEntry> getRecordings() {
		return recordings;
	}

	public void setRecordings(List<RecordingEntry> recordings) {
		this.recordings = recordings;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public void addRecordingEntry(String recordingId, String downloadUrl) {
		this.recordings.add(new RecordingEntry(recordingId, downloadUrl));
	}

	public static class RecordingEntry {
		private String recordingId;
		private String downloadUrl;

		public RecordingEntry() {
			super();
		}

		public RecordingEntry(String recordingId, String downloadUrl) {
			super();
			this.recordingId = recordingId;
			this.downloadUrl = downloadUrl;
		}

		public String getRecordingId() {
			return recordingId;
		}

		public void setRecordingId(String recordingId) {
			this.recordingId = recordingId;
		}

		public String getDownloadUrl() {
			return downloadUrl;
		}

		public void setDownloadUrl(String downloadUrl) {
			this.downloadUrl = downloadUrl;
		}

		@Override
		public String toString() {
			return "RecordingEntry [recordingId=" + recordingId + ", downloadUrl=" + downloadUrl + ", toString()="
					+ super.toString() + "]";
		}
	}

	@Override
	public String toString() {
		return "HandleOTFRecordingLambdaReq [environment=" + environment + ", sessionId=" + sessionId + ", recordings="
				+ recordings + ", toString()=" + super.toString() + "]";
	}
}
