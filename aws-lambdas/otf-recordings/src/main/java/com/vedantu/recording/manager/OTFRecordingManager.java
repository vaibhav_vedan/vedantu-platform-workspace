package com.vedantu.recording.manager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.io.FileUtils;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;

public class OTFRecordingManager {

    // ----------------------------------------------------------------------------------//
    public static final int MILLIS_PER_SECOND = 1000;
    public static final int SECONDS_PER_MINUTE = 60;
    public static final int MINUTES_PER_HOUR = 60;
    public static final int HOURS_PER_DAY = 24;

    public static final int MILLIS_PER_MINUTE = MILLIS_PER_SECOND * SECONDS_PER_MINUTE;
    public static final int MILLIS_PER_HOUR = MILLIS_PER_MINUTE * MINUTES_PER_HOUR;
    public static final int MILLIS_PER_DAY = MILLIS_PER_HOUR * HOURS_PER_DAY;

    public static final int SECONDS_PER_HOUR = SECONDS_PER_MINUTE * MINUTES_PER_HOUR;

    public static final Long MILLIS_PER_WEEK = (long) (7 * MILLIS_PER_DAY);
    public static final Long IST_TIME_DIFFERENCE = 66600000l;
    public static final Long IST_TIME_DIFFERENCE_NEGATIVE = 19800000l;

    public static final String TIME_ZONE_GMT = "GMT";
    public static final String TIME_ZONE_IN = "Asia/Kolkata";

    // ----------------------------------------------------------------------------------//
    private static final long EXPIRY_WINDOW = 2 * MILLIS_PER_HOUR;

    public AmazonS3 createS3Client() {
        AmazonS3 s3Client = new AmazonS3Client();
        s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        return s3Client;
    }

    public static void main(String[] args) throws IOException {
        OTFRecordingManager orm = new OTFRecordingManager();
        HandleOTFRecordingLambda req = new HandleOTFRecordingLambda("DEV2", "58b7ea0b60b2717279c83ac0");
        req.addRecordingEntry("86734",
                "https://s3-ap-south-1.amazonaws.com/vedantu-otf-recordings-mumbai/PROD/585bfd88e4b0e33e9e4c464b-85504.mp4");
        req.addRecordingEntry("86773",
                "https://s3-ap-south-1.amazonaws.com/vedantu-otf-recordings-mumbai/PROD/585bfd88e4b0e33e9e4c464b-85504.mp4");

        orm.handleOTFRecording(req);
    }

    public void handleOTFRecording(HandleOTFRecordingLambda req) throws IOException {
        moveFFmpegFiles();
        List<File> allFiles = new ArrayList<>();
        File fileList = null;
        PrintWriter pw = null;
        try {
            String fileListName = "/tmp/fileList" + req.getSessionId() + ".txt";
            fileList = new File(fileListName);
            allFiles.add(fileList);
            pw = new PrintWriter(fileListName, "UTF-8");
            for (HandleOTFRecordingLambda.RecordingEntry recordingEntry : req.getRecordings()) {
                // Download the recording
                URL downloadUrl = new URL(recordingEntry.getDownloadUrl());
                ReadableByteChannel rbc = Channels.newChannel(downloadUrl.openStream());
                String absoluteFileName = getDefaultFilePath(recordingEntry.getRecordingId());
                System.out.println("File:" + absoluteFileName);
                File file = new File(absoluteFileName);
                allFiles.add(file);
                @SuppressWarnings("resource")
                FileOutputStream fos = new FileOutputStream(absoluteFileName);
                fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
                System.out.println("File size : " + file.getTotalSpace());

                // TODO : Can check if the key already exists and avoid
                // uploading.
                // Need to validate if partial upload creates an object
                // Upload the recording
                String signedUrl = uploadFile(getRecordingKeyName(recordingEntry.getRecordingId(), req.getSessionId()),
                        file, req.getEnvironment());
                System.out.println("signedUrl:" + signedUrl);

                pw.write("file " + file.getAbsolutePath() + "\n");
            }
            pw.close();

            if (req.getRecordings().size() > 1) {
                String outputAbsoluteFileName = getDefaultFilePath(req.getSessionId()) + ".mp4";
                File outputAbsoluteFile = new File(outputAbsoluteFileName);
                allFiles.add(outputAbsoluteFile);

                String ffmpegCommand = "/tmp/ffmpeg -f concat -i " + fileList.getAbsolutePath() + " -c copy "
                        + outputAbsoluteFileName;
                Process mergeProc = Runtime.getRuntime().exec(ffmpegCommand);
                mergeProc.waitFor();
                System.out.println("Exit code : " + mergeProc.exitValue());
                if (mergeProc.exitValue() != 0) {
                    // Print the error stream
                    printOutput(mergeProc.getErrorStream());
                }

                // Upload to s3
                uploadFile(req.getSessionId(), new FileInputStream(outputAbsoluteFile), req.getEnvironment());
            }
        } catch (Exception ex) {
            System.out.println("Ex : " + ex.toString() + " message:" + ex.getMessage());
        } finally {
            for (File file : allFiles) {
                file.delete();
            }
        }

        /*
		 * System.out.println("Output absolute file name : " +
		 * outputAbsoluteFileName); File outputFile = new
		 * File(outputAbsoluteFileName);
		 * builder.overrideOutputFiles(false).addOutput(outputAbsoluteFileName).
		 * setFormat("mp4").setAudioChannels(1)
		 * .setAudioSampleRate(8000).done();
		 * System.out.println("Builder created");
		 * 
		 * FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);
		 * System.out.println("executore created:" + executor);
		 * 
		 * // Execute the merging executor.createJob(builder).run();
		 * System.out.println("Job created and executed");
		 * 
		 * String signedUrl = uploadFile(req.getSessionId(), outputFile,
		 * req.getEnvironment()); System.out.println("converted file to : " +
		 * outputFile + " signedUrl:" + signedUrl);
         */
    }

    private void moveFFmpegFiles() {
        URL inputUrl = getClass().getResource("/ffprobe");
        File dest = new File(getDefaultFilePath("ffprobe"));
        try {
            FileUtils.copyURLToFile(inputUrl, dest);
            dest.setExecutable(true, false);
            dest.setReadable(true, false);
            dest.setWritable(true, false);

        } catch (IOException e) {
            e.printStackTrace();
        }
        URL inputUrl1 = getClass().getResource("/ffmpeg");
        File dest1 = new File(getDefaultFilePath("ffmpeg"));
        try {
            FileUtils.copyURLToFile(inputUrl1, dest1);
            dest1.setExecutable(true, false);
            dest1.setReadable(true, false);
            dest1.setWritable(true, false);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // AWS methods
    public String uploadFile(String sessionId, File file, String environment) throws FileNotFoundException {
        return uploadFile(sessionId, new FileInputStream(file), environment);
    }

    public String uploadFile(String sessionId, FileInputStream fis, String environment) throws FileNotFoundException {
        System.out.println("Request : " + sessionId);
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentType("audio/mpeg");
        PutObjectRequest putObjectRequest = new PutObjectRequest(getBucketName(environment), sessionId + ".mp4", fis,
                objectMetadata);
        System.out.println("Put object created : " + putObjectRequest.toString());
        AmazonS3 s3Client = createS3Client();
        System.out.println("S3 client created");
        PutObjectResult result = s3Client.putObject(putObjectRequest);
        System.out.println("upload respodnse from s3 : " + result.toString());
        String preSignedUrl = getPreSignedUrl(sessionId, environment);
        return preSignedUrl;
    }

    public boolean keyExists(String sessionId, String environment) {
        return createS3Client().doesObjectExist(getBucketName(environment), sessionId + ".mp4");
    }

    private String getBucketName(String environment) {
        String BUCKET_NAME = "vedantu-otf-recordings-mumbai";
        if (environment != null && !(environment.toLowerCase().equals("prod") || environment.toLowerCase().equals("qa"))) {
            BUCKET_NAME = "vedantu-otf-recordings-qa";
        }
        return BUCKET_NAME + "/" + environment;
    }

    public String getPreSignedUrl(String sessionId, String environment) {
        String presignedUrl = "";

        try {
            GeneratePresignedUrlRequest grt = new GeneratePresignedUrlRequest(getBucketName(environment),
                    sessionId + ".mp4");
            grt.setExpiration(new Date(System.currentTimeMillis() + EXPIRY_WINDOW));
            // grt.setContentType("audio/mpeg");
            presignedUrl = createS3Client().generatePresignedUrl(grt).toString();
        } catch (Exception exception) {
            System.out.println("Error:" + exception.getMessage());
        }

        return presignedUrl;
    }

    public static String getRecordingKeyName(String recordingId, String sessionId) {
        return sessionId + "-" + recordingId;
    }

    private static String getDefaultFilePath(String fileName) {
        return "/tmp/" + fileName;
    }

    private static void printOutput(InputStream is) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String s = "";
        while ((s = br.readLine()) != null) {
            System.out.println("line: " + s);
        }
    }
}
