package com.vedantu.recording.manager;

import com.amazonaws.services.lambda.runtime.Context;

public class AwsHandler {

	private OTFRecordingManager otfRecordingManager = new OTFRecordingManager();

	public AwsHandler() {
		super();
	}

	public void handleRecording(HandleOTFRecordingLambda payload, Context context) {
		System.out.println("Event:" + payload.toString());
		try {
			otfRecordingManager.handleOTFRecording(payload);
		} catch (Exception e) {
			System.out.println("Error:" + e.getMessage());
		}
	}
}