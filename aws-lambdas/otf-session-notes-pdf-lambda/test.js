const {Builder, By, Key, until} = require('selenium-webdriver');

(async function example() {
  let driver = await new Builder().forBrowser('chrome').build();
  try {
    await driver.get("https://local.vedantu.com/v/login");
    var email_input = await driver.findElement(By.css('.viewer #login-email'));
    var pass_input = await driver.findElement(By.css('.viewer #login-password'));
    var submit_button = await driver.findElement(By.css('.viewer input[type="submit"][value="Log in"]'));
    await email_input.sendKeys('admin.gaurav@vedantu.com');
    await pass_input.sendKeys('Gaurav@123');
    await pass_input.submit();
    await driver.wait(until.titleContains('Vedantu - Tools'));
    const succesText = "Notes Downloaded. Please close this window.";
    const messageBox = By.id('LoadingWBMsgText');
    await driver.get("https://local.vedantu.com/v/otfSessionNotes/5b58704f60b27afe0db73aa1");
    await driver.wait(until.elementLocated(messageBox));
    const whatElement = driver.findElement(messageBox);
    await driver.wait(until.elementTextContains(whatElement,succesText));
    const data=await driver.executeScript('return pdfBase64');
    var base64Data = data.replace(/^data:application\/pdf;base64,/, "");
    require("fs").writeFile("out.pdf", base64Data, 'base64', function(err) {
      console.log(err);
    });
  } catch(err){
    console.error(err);
  } finally {
    await driver.quit();
  }
})();
