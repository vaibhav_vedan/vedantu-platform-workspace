'use strict';

const AWS = require('aws-sdk');
const fs = require("fs");
async function dataHandler(event, context, callback) {
  console.log(JSON.stringify(event));
  if (!event || !event.Records || !event.Records[0] || !event.Records[0].Sns) {
    return;
  }
  const {
    Message,
    TopicArn
  } = event.Records[0].Sns;
  const sessionId = JSON.parse(Message)['sessionId'];
  const ENV = TopicArn.substring(TopicArn.lastIndexOf('_') + 1).toLowerCase();
  console.log({
    ENV,
    sessionId
  });

  process.env["DEPLOYMENT_MODE"] = ENV;
  process.env["APP_HOME"] = require("path").dirname(fs.realpathSync(__filename));
  const config = require(process.env["APP_HOME"] + "/config.js");
  config.setConfigPropsForCurrentMode(process.env["DEPLOYMENT_MODE"], sessionId);
  const configProps = config.getConfigProps();
  const logger = require(process.env["APP_HOME"] + "/logger.js");
  process
    .on('unhandledRejection', (reason, p) => {
      logger.error(reason, 'Unhandled Rejection at Promise', p);
    })
    .on('uncaughtException', err => {
      logger.error(err, 'Uncaught Exception thrown');
      process.exit(1);
    });

  logger.log("getting chrome files for selenium");
  const tempDir = '/tmp/lib';
  if (ENV == 'local') {
    const tempDir = process.env["APP_HOME"] + '/tmp/lib';
  }

  AWS.config.update({
    region: configProps.s3Region
  });
  // try {
  //   await getChomeDrivers(tempDir, configProps.libBucket, configProps.s3Region);
  // } catch (err) {
  //   logger.error(err);
  // }
  logger.log("got chrome files for selenium");

  const s3Bucket = new AWS.S3({
    params: {
      Bucket: configProps.bucketName
    }
  });


  const {Builder, By, Key, until} = require('selenium-webdriver');
  const webdriver = require('selenium-webdriver');
  const chrome = require('selenium-webdriver/chrome');
  const builder = new webdriver.Builder().forBrowser('chrome');
  const chromeOptions = new chrome.Options();
  const defaultChromeFlags = [
    '--headless',
    '--disable-gpu',
    '--window-size=1280x1696', // Letter size
    '--no-sandbox',
    '--user-data-dir=/tmp/user-data',
    '--hide-scrollbars',
    '--enable-logging',
    '--log-level=0',
    '--v=99',
    '--single-process',
    '--data-path=/tmp/data-path',
    '--ignore-certificate-errors',
    '--homedir=/tmp',
    '--disk-cache-dir=/tmp/cache-dir',
  ];
  chromeOptions.setChromeBinaryPath("/var/task/lib/chrome");
  chromeOptions.addArguments(defaultChromeFlags);
  builder.setChromeOptions(chromeOptions);
  const driver = builder.build();
  logger.log("started browser");
  try {
    const domain = configProps.domain;
    let url = domain + "/v/login";
    logger.log("url>>>"+url);
    await driver.get(url);
    const email_input = await driver.findElement(By.css('.viewer #login-email'));
    const pass_input = await driver.findElement(By.css('.viewer #login-password'));
    // var submit_button = await driver.findElement(By.css('.viewer input[type="submit"][value="Log in"]'));
    await email_input.sendKeys(configProps.userName);
    await pass_input.sendKeys(configProps.password);
    logger.log("logging in");
    await pass_input.submit();
    await driver.wait(until.titleContains('Vedantu - Tools'));
    logger.log("logged in");
    const succesText = "Notes Downloaded. Please close this window.";
    const messageBox = By.id('LoadingWBMsgText');
    logger.log("downloading notes");
    url = domain + "/v/otfSessionNotes/"+sessionId+"?upload=true"
    logger.log("url>>>"+url);
    await driver.get(url);
    await driver.wait(until.elementLocated(messageBox));
    const whatElement = driver.findElement(messageBox);
    await driver.wait(until.elementTextContains(whatElement, succesText));
    logger.log("notes downloaded");
    // const data = await driver.executeScript('return pdfBase64');
    // logger.log("got pdf data uplaoding to s3");
    // const base64Data = data.replace(/^data:application\/pdf;base64,/, "");
    // logger.log("got pdf data uplaoding to s3");
    // const payload = {
    //   Key: 'SESSION_NOTES/' + ENV.toUpperCase() + '/' + sessionId + '.pdf',
    //   Body: base64Data,
    //   ContentEncoding: 'base64',
    //   ContentType: 'application/pdf'
    // };
    // await s3Bucket.putObject(payload);
  } catch (err) {
    logger.error(err);
  } finally {
    driver.quit();
  }
};

// function getChomeDrivers(tmpDir, bucketName, s3Region) {
//   return new Promise((resolve, reject) => {
//     const s3 = new AWS.S3({
//       params: {
//         Bucket: bucketName
//       }
//     });
//     const params = {
//       Bucket: bucketName,
//       Key: "libs/SELENIUM_LAMBDA.zip"
//     };
//     if (!fs.existsSync(tmpDir)) {
//       fs.mkdirSync(tmpDir);
//     }
//     var file = fs.createWriteStream(tmpDir + '/lib.zip');
//     file.on('close', function () {
//       var extract = require('extract-zip')
//       extract(tmpDir + '/lib.zip', {
//         dir: tmpDir + '/'
//       }, function (err) {
//         if (err) {
//           reject(err);
//         } else {
//           fs.chmodSync(tmpDir + '/chrome', '755');
//           fs.chmodSync(tmpDir + '/chromedriver', '755');
//           fs.chmodSync(tmpDir + '/libosmesa.so', '755');
//           fs.unlinkSync(tmpDir + '/lib.zip');
//           resolve();
//         }
//       });
//     });
//     s3.getObject(params).createReadStream().on('error', function (err) {
//       if (err) {
//         reject(err);
//       }
//     }).pipe(file);
//   });
// }

exports.handler = dataHandler;
// exports.handler({
//   "Records": [{
//     "EventVersion": "1.0",
//     "EventSubscriptionArn": "arn:aws:sns:EXAMPLE",
//     "EventSource": "aws:sns",
//     "Sns": {
//       "SignatureVersion": "1",
//       "Timestamp": "1970-01-01T00:00:00.000Z",
//       "Signature": "EXAMPLE",
//       "SigningCertUrl": "EXAMPLE",
//       "MessageId": "95df01b4-ee98-5cb9-9903-4c221d41eb5e",
//       "Message": "{\"sessionId\":\"5906cb4460b2da6f43b9d8f1\"}",
//       "MessageAttributes": {
//         "Test": {
//           "Type": "String",
//           "Value": "TestString"
//         },
//         "TestBinary": {
//           "Type": "Binary",
//           "Value": "TestBinary"
//         }
//       },
//       "Type": "Notification",
//       "UnsubscribeUrl": "EXAMPLE",
//       "TopicArn": "arn:aws:sns:MONGO_TO_S3_DONE_DEV1",
//       "Subject": "SESSION_ENDED"
//     }
//   }]
// });