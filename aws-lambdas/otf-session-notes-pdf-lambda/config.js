var localsettings = {
    domain: "https://local.vedantu.com",
    sessionId: null,
    env: "local",
    logs: {
        level: "log",
        filePath: "/var/log",
        maxDaysToKeepFile: 7, //days
        intervalToCheckFiles: 5 //hrs
    },
    bucketName: "vedantu-otf-recordings-qa",
    libBucket:"builds-vedantu-qa",
    s3Region:"ap-southeast-1",
    userName:'admin.gaurav@vedantu.com',
    password:'Gaurav@123',
    raven: {
        key: "https://a6d982ce5c5949e8ac99117c1e91c5fc:23a3dc21359b4c62a737fc60ce71958f@app.getsentry.com/65239"
    },
    platform: {
        client: "NODE",
        clientId: "E25D9",
        clientSecret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk",
        url: "http://localhost:8081"
    }
};


var prodsettings = {
    domain: "https://www.vedantu.com",
    sessionId: null,
    env: "prod",
    logs: {
        level: "info",
        filePath: "/var/log",
        maxDaysToKeepFile: 30, //days
        intervalToCheckFiles: 5 //hrs
    },
    bucketName: "vedantu-otf-recordings-mumbai",
    libBucket:"builds-vedantu",
    s3Region:"ap-south-1",
    userName:'session.notes.uploader@vedantu.com',
    password:'Gaurav@123',
    raven: {
        key: "https://b6140e0a17614079888fcb2b550e5185:2643abfc706b4fb684baa104ad76b7e4@app.getsentry.com/79422"
    },
    platform: {
        client: "NODE",
        clientId: "E25D9",
        clientSecret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk",
        url: "https://platform.vedantu.com"
    }
};
var propsMap = {
    local: localsettings,
    prod: prodsettings
};
var propertiesForCurrentMode = {};
var getPropsForQAProfiles = function (qaMode) {
    return {
        domain: "https://fos-" + qaMode + ".vedantu.com",
        logs: {
            level: "log",
            filePath: "/var/log",
            maxDaysToKeepFile: 7, //days
            intervalToCheckFiles: 5 //hrs
        },
        sessionId: null,
        env: qaMode,
        bucketName: "vedantu-otf-recordings-qa",
        libBucket:"builds-vedantu-qa",
        s3Region:"ap-southeast-1",
        userName:'admin.gaurav@vedantu.com',
        password:'Gaurav@123',
        raven: {
            key: "https://a6d982ce5c5949e8ac99117c1e91c5fc:23a3dc21359b4c62a737fc60ce71958f@app.getsentry.com/65239"
        },
        platform: {
            client: "NODE",
            clientId: "E25D9",
            clientSecret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk",
            url: "https://" + qaMode + "-platform.vedantu.com"
        }
    };
};
module.exports = {
    setConfigPropsForCurrentMode: function (mode,sessionId) {
        if (mode === "local" || mode === "prod") {
            propertiesForCurrentMode = propsMap[mode];
        } else {
            propertiesForCurrentMode = getPropsForQAProfiles(mode);
        }
        propertiesForCurrentMode['sessionId'] = sessionId;
    },
    getConfigProps: function () {
        return propertiesForCurrentMode;
    }
};
