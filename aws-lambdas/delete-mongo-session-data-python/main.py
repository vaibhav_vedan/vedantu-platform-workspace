from config_utils import *
import pymongo
from pymongo import MongoClient
import urllib
import vedantu_logger;
authMechanism = 'DEFAULT';

def getMongoUrlFromProps(props):
    if props.isAtlas:
        return props.dburi
    else:
        url = "mongodb://";
        if hasattr(props, 'username') and hasattr(props, 'password'):
            user = urllib.quote(props.username);
            password = urllib.quote(props.password);
            url += user + ":" + password + "@";
        for name in props.host:
            url += name + ":" + str(props.port) + ",";
        url=url[:-1]
        url += "/" + props.dbname + "?authMechanism=" + authMechanism;
        if hasattr(props, 'replicaSet'):
            url += '&replicaSet=' + props.replicaSet;
        return url    


@import_config
def myfunction_handler(event, context, config):
    logger=vedantu_logger.getLogger();
    client = MongoClient(getMongoUrlFromProps(config.mongo));
    sessionids=client[config.mongo.dbname]['sessionids'];
    client = MongoClient(getMongoUrlFromProps(config.mongo_session));
    mongodb=client[config.mongo_session.dbname]
    cursor = mongodb.collection_names()
    count=0;
    for document in cursor:
        count+=1;
        logger.info("count><>>>>"+str(count));
        logger.info("sessionId>>>>"+document);
        res=sessionids.find_one({"sessionId":document},projection={'migratedToS3': True,'sessionId':True})
        if res is not None and "migratedToS3" in res:
            if res["migratedToS3"] is True or res["migratedToS3"] == "true":
                logger.info("already migrated deleting collection");
                mongodb[document].drop()
            else:
                logger.info("not yet migrated");
        else:
            logger.info("not yet migrated");
        # print res
