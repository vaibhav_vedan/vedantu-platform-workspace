import vedantu_logger;
def get_env():
    config = __import__('env')
    return config.env

def import_config(f):
    def wrapper(*args, **kwargs):
        env = get_env()
        config = __import__('config')
        props = config.get_Configs(env);
        args += (props,)
        vedantu_logger.initLogger(props.raven.key);
        return f(*args, **kwargs)
    return wrapper