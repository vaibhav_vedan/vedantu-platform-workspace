class local:
    class mongo:
        host=["localhost"]
        port=27017
        dbname="vedantuwave"

    class mongo_session:
        host=["localhost"]
        port=27017
        dbname="localvedantu"

    class raven:
        key=""


class prod:
    class mongo:
        dbname="vedantuwave"
        dburi= "mongodb+srv://readwrite:rYghHfwTyt3L6AOO@ds021008-uv3wl.mongodb.net/vedantuwave?retryWrites=true&w=majority&ssl=true"
        isAtlas= True  
        
    class mongo_session:
        host=["54.254.150.156"]
        port=27011
        dbname="vedantuwave"
        username= "vedantu"
        password="v3d4ntu@123"

    class raven:
        key="https://39b6921c2e8c483ca692cff3db5dc049:cab02289330041c89054ddc016278275@sentry.io/203351"


class qa:
    class mongo:
        host=["qa-qa-private.vedantu.com"]
        port=27017
        dbname="vedantuwaveqa"

    class mongo_session:
        host=["qa-qa-private.vedantu.com"]
        port=27017
        dbname="vedantuwaveqa"

    class raven:
        key=""


def getPropsForQAProfiles(mode):
    class settings:
        class mongo:
            host=[mode+"-rtc.vedantu.com"]
            port=27017
            dbname="vedantuwave"+mode

        class mongo_session:
            host=[mode+"-rtc.vedantu.com"]
            port=27017
            dbname="vedantuwave"+mode

        class raven:
            key=""

    return settings;

config_map = {
    "prod":prod,
    "qa":qa,
    "local":local
}


def get_Configs(env):
    if env not in config_map:
        return getPropsForQAProfiles(env);
    else:
        return config_map[env]
