/* global process, require, exports, __filename */

var env = require("./env.js").env;
process.env["DEPLOYMENT_MODE"] = env;
process.env['PATH'] = process.env['PATH'] + ':/tmp/:' + process.env['LAMBDA_TASK_ROOT'];
process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'] + '';
process.env["APP_HOME"] = require("path").dirname(require("fs").realpathSync(__filename));
var config = require(process.env["APP_HOME"] + "/config.js");
//setting config parameters
config.setConfigPropsForCurrentMode(process.env["DEPLOYMENT_MODE"]);
var configProps = config.getConfigProps();
var Q = require('q');
var AWS = require('aws-sdk');
var awsS3Props = configProps["aws-s3"];
var socialVidRecord = require(process.env["APP_HOME"] + "/modules/socialVidRecord.js");


var expectedSessionEventSubjects = ["SESSION_ENDED", "SESSION_EXPIRED", "SESSION_CANCELLED"];

//getting the logger
var logger = require(process.env["APP_HOME"] + "/utils/logger.js");

var restler = require("./modules/restlerclient");
exports.handler = function (event, context) {
    logger.log("received a call from SNS in env " + env);
    logger.log(JSON.stringify(event));
    //init
    var socialVidPromise = (function () {
        var deferred = Q.defer();
        var email = "vedantu@socialvid.in";
        var args = {
            "email": email,
            "password": "vedantuIntegration"
        };
        restler.post("https://vedantu.socialvid.in/adminapi/v1/userlogin", args).then(function (resp) {
            if (!resp) {
                deferred.resolve(null);
            } else {
                configProps.socialVidToken = resp.session;
                configProps.socialVidEmail = email;
                logger.debug(typeof resp);
                deferred.resolve(true);
            }
        }).
                catch(function (e) {
                    logger.error(e);
                });
        return deferred.promise;
    })();
    var preStartPromises = Q.all([socialVidPromise]);
    preStartPromises.then(function (resultsArray) {
        var _error = "";

        var socialVidResp = resultsArray[0];
        if (socialVidResp) {
            logger.info("socialvid token created");
        } else {
            _error = "Error in logging to socialvid";
        }
        //        var mongoInitResp = resultsArray[2];
        //        if (mongoInitResp) {
        //            logger.info("mongo connection created");
        //        } else {
        //            _error = "Error in mongo connection creation";
        //        }
        if (_error) {
            logger.error(_error);
            context.fail(_error);
            return;
        }
        ///////////////////////////////////
        //      HANDLING SNS MESSAGE
        ///////////////////////////////////
        var pr = handleMessage(event);
        pr.then(function (result) {
            logger.info("Handled the sns event successfully, now exiting");
            context.succeed(result);
        }, function (err) {
            logger.error("Error in handling sns event " + configProps.sessionId, err);
            context.fail(err);
        }).
                catch(function (err) {
                    logger.error("Error in callbacks of handleMessage function promise " + configProps.sessionId, err);
                    context.fail(err);
                });
    }).
            catch(function (err) {
                logger.error("Error in initing handler.js " + configProps.sessionId, err);
                context.fail(err);
            });
};

function handleMessage(event) {
    var deferred = Q.defer();
    if (event && event.Records) {
        if (event.Records.length > 0) {
            var record = event.Records[0];
            if (!record.Sns) {
                deferred.reject("Was expecting an sns notification but got something else");
                return;
            }
            record = record.Sns;
            var subject = record.Subject;
            if (expectedSessionEventSubjects.indexOf(subject) > -1) {
                var json = {};
                try {
                    json = record.Message;
                    if (!json.sessionId) {
                        json = JSON.parse(json);
                    }
                    if (json.sessionId) {
                        configProps.sessionId = json.sessionId;
                        var pr = socialVidRecord.prepareReplay(json.sessionId);
                        pr.then(function (resp) {
                            AWS.config.update({
//                                accessKeyId: awsS3Props.accessKeyId,
//                                secretAccessKey: awsS3Props.secretAccessKey,
                                region: awsS3Props.snsRegion
                            });
                            var sns = new AWS.SNS();
                            var topicARN = "arn:aws:sns:ap-southeast-1:" + awsS3Props.accountId + ":REPLAY_SESSION_PREPARED_" + env.toUpperCase();
                            logger.info("replay prepared sending sns:", topicARN);
                            sns.publish({
                                Subject: "REPLAY_SESSION_PREPARED",
                                Message: '{"default": "{sessionId:' + json.sessionId.toString() + '}"}',
                                MessageStructure: 'json',
                                TargetArn: topicARN
                            }, function (err, data) {
                                if (err) {
                                    logger.error("error in sending sns event" + json.sessionId.toString(), err);
                                } else {
                                    logger.info("sns event sent successfully" + json.sessionId.toString(), data);
                                }
                                if (resp) {
                                    deferred.resolve(true);
                                } else {
                                    deferred.reject("Error in analysing session " + json.sessionId + ", error " + ((resp) ? resp.errorCode : "UNKNOWN"));
                                }
                            });
                        });

                    } else {
                        deferred.reject("Could not get sessionId from the event ");
                    }
                } catch (e) {
                    logger.error(e);
                    deferred.reject("Could not parse message json str from the event");
                }
            } else {
                logger.warn("received session event which I am not interested in", event);
                deferred.resolve(true);
            }
        } else {
            deferred.reject("received zero records in sns notification");
        }
    } else {
        deferred.reject("received sns notification but no records found in them");
    }
    return deferred.promise;
}

//exports.handler({
//    Records: [{
//        Sns: {
//            Subject: "SESSION_ENDED",
//            Message: {
//                sessionId: '4704929067827200'
//            }
//        }
//    }]
//});

var dummyContext = {
    succeed: function () {
        logger.info("successfully done");
    },
    fail: function (err) {
        logger.error("failed ", err);
    }
};
function manualTrigger() {
    //smaller version of actual sns event received 
    var _event = {
        "Records": [
            {
                "Sns": {
                    "Subject": "SESSION_ENDED",
                    "Message": "{\"sessionId\":\"4102378372520071\"}"
                }
            }
        ]
    };
    exports.handler(_event, dummyContext);
}

//manualTrigger();

/*To run manually change the sessionId in Message attribute in manualTrigger function
 and then uncomment manuallTrigger
 also ensure env.js has value = prod
 */

