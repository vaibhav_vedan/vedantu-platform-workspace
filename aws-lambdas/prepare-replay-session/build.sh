env=$1
echo "started building node project for $env environment"
echo "module.exports.env='${env}'" > env.js
echo "running sudo npm install"
mkdir node_modules
sudo npm install
sudo chmod -R ugo+w node_modules/
rm PREPARE_REPLAY_SESSION_*.zip
zip -r PREPARE_REPLAY_SESSION_${env}.zip .
echo "create the file PREPARE_REPLAY_SESSION_${env}.zip in the current directory"


