/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* global exports */

function getP2PRoomId(userId1, userId2) {
    var key;
    if (userId1 > userId2) {
        key = userId1 + "_" + userId2;
    } else {
        key = userId2 + "_" + userId1;
    }
    return key;
}
function getOtherMemberOfP2PRoom(participants, myUserId) {
    if (participants instanceof Array && participants.length > 1) {
        if (myUserId == participants[0]) {
            return participants[1];
        } else {
            return participants[0];
        }
    }
}
function getOtherMemberOfP2PRoomFromRoomId(roomId, myUserId) {
    var participants = getP2PRoomParticipants(roomId);
    return getOtherMemberOfP2PRoom(participants, myUserId);
}
function getP2PRoomParticipants(roomId) {
    var arr = [];
    if (roomId && roomId.indexOf("_") > -1) {
        arr = roomId.split("_");
    }
    return arr;
}
function randomString() {
    var chars =
            "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var randomstring = '';
    var string_length = 12;
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.charAt(rnum);
    }
    return randomstring;
}

// Functions which will be available to external callers
exports.getP2PRoomId = getP2PRoomId;
exports.getOtherMemberOfP2PRoom = getOtherMemberOfP2PRoom;
exports.getP2PRoomParticipants = getP2PRoomParticipants;
exports.getOtherMemberOfP2PRoomFromRoomId = getOtherMemberOfP2PRoomFromRoomId;
exports.getRandomString=randomString;