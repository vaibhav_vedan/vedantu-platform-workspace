
var getSuccessRespObj = function(result) {
    return {errorCode: "", errorMessage: "", result: result};
};
var getErrRespObj = function(errorCode) {
    if (!errorCode) {
        errorCode = "SERVICE_ERROR";
    }
    var errorMessage = errorCodesToMessagesMap[errorCode];
    if (!errorMessage) {
        errorMessage = "";
    }
    return {errorCode: errorCode, errorMessage: errorMessage};
};
var errorCodesToMessagesMap = {
    SERVICE_ERROR: "Some error occured",
    BAD_REQUEST: "missing params"
};

exports.getSuccessRespObj = getSuccessRespObj;
exports.getErrRespObj = getErrRespObj;