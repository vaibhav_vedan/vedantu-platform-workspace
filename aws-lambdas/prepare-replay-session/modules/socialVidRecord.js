/* global require, process, module */
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var Q = require("q");
var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var httpClient = require(process.env["APP_HOME"] + "/modules/httpclient.js");
var restler = require("./restlerclient");
var twilioClient = require("./twilioRecordings.js");
var fs = require('fs');
var path = require('path');
var mkdirp = require('mkdirp');
var AWS = require('aws-sdk');
var awsS3Props = configProps["aws-s3"];
var tmpDir = '/tmp/';
var platform = require(process.env["APP_HOME"] + "/modules/platform.js");
AWS.config.update({
//    accessKeyId: awsS3Props.accessKeyId,
//    secretAccessKey: awsS3Props.secretAccessKey,
    region: awsS3Props.lambdaRegion
});
var ep = new AWS.Endpoint(awsS3Props.endpoint);
var s3bucket = new AWS.S3({
    endpoint: ep
});

var socialVidUrl = "https://vedantu.socialvid.in";
var socialVidToken;
var socialVidEmail;
var socialVidDeleteUrl = "https://vedantu.socialvid.in/adminapi/v1/user/conference/delete";
var getRecordingUrl = "https://vedantu.socialvid.in/adminapi/v1/user/recording/get";
var nodeClientProps = configProps.nodeClient;
var nodeUrls = {
    GET_CONFERENCEID: nodeClientProps.url + "/getConferenceId",
    SET_CONFERENCE_DELETED: nodeClientProps.url + "/setConferenceDeleted",
    MARK_REPLAY_SESSION_PREPARED: nodeClientProps.url + "/markReplaySessionPrepared",
    GET_SERVICE_USED: nodeClientProps.url + "/getServiceUsed",
    SET_ERROR: nodeClientProps.url + "/setReplaySessionError"
};
var appengineDomian = configProps.appengineProps.domain;
var fosUrls = {
    GET_SESSION_INFO: appengineDomian + "/session/getInfo",
    SAVE_METADATA: appengineDomian + "/filemgnt/insertFileMetadataForAWS"
};

var statusCodes = {
    OK: 200
};
var filesStoredInSystem = {};

var globalDeferred; //using this as the execution is in a lambda function, so not a problem
var globalSessionId;
var commonForAllServices = function (sessionId) {
    globalDeferred = Q.defer();
    globalSessionId=sessionId;
    socialVidToken = configProps.socialVidToken;
    socialVidEmail = configProps.socialVidEmail;
    logger.info("In common for all services for : ", sessionId);
    var servicePromise = restler.get(nodeUrls.GET_SERVICE_USED + "?sessionId=" + sessionId);
    servicePromise.then(function (resp) {
        logger.log("in socialvidrecord got service as ", resp);
        if (resp && resp.avChatServiceUsed === "SOCIALVID") {
            logger.log("service is socialvid for " + sessionId);
            getMasterJson(sessionId);
        } else if (resp && resp.avChatServiceUsed === "JITSI") {
            logger.info("jitsi used, so not doing anything");
            globalDeferred.resolve(true);
        } else if (resp && resp.avChatServiceUsed === "TWILIO") {
            logger.log("service is TWILIO for " + sessionId);
            if(resp.callDetails && resp.callDetails.callIds){
                getTwilioRecordings(resp.callDetails.callIds);
            }else{
                globalDeferred.resolve(true);
            }
        } else {
            logger.log("service is undefined for sessionId " + sessionId);
            globalDeferred.resolve(true);
        }
    }).catch(errorCallBack);
    return globalDeferred.promise;
};

var getMasterJson = function (sessionId) {
    logger.info("getting master JSON for session Id ", sessionId);
    var deferred = Q.defer();
    var recordingsJson = [];
    var confIdPromise = restler.get(nodeUrls.GET_CONFERENCEID + "?sessionId=" + sessionId);
    var conferenceId;
    confIdPromise.then(function (resp) {
        logger.info("received conference ID of socialvid ", resp);
        if (resp && resp.conferenceId) {//TODO if confDeleted do not do anything
            conferenceId = resp.conferenceId;
            // deleteConference(sessionId, resp.conferenceId);
            if(resp.screenShareConferenceId){
                // deleteConference(sessionId, resp.screenShareConferenceId);
            }
            var args = {
                "email": socialVidEmail,
                "session": configProps.socialVidToken,
                "confId": resp.conferenceId
            };
            restler.post(getRecordingUrl, args).then(function (resp) {
                if (resp) {
                    logger.info("Got master json for sessionId " + sessionId + " as ", resp);
                    if (resp.recordings && resp.recordings.length > 0) {
                        for (var y = 0; y < resp.recordings.length; y++) {
                            recordingsJson.push(resp.recordings[y]);
                        }
                        var masterPromise = getFilePath(recordingsJson, deferred);
                        masterPromise.then(function (resp) {
                            if (resp) {
//                                var deferred1 = Q.defer();
//                                uploadSingleFileToS3(sessionId, deferred1);
//                                uploadPromise = deferred1.promise;
//                                uploadPromise.then(function () {
                                    logger.info("fileuploaded successfully");
                                    deleteConference(sessionId, conferenceId);
                                    var pr = restler.post(nodeUrls.MARK_REPLAY_SESSION_PREPARED, {
                                        sessionId: sessionId.toString()
                                    });
                                    pr.then(function () {
                                        globalDeferred.resolve(true);
                                    });
//                                });
                                //                                                                    var pr = mongo_session.markReplaySessionPrepared(sessionId.toString());
                            } else {
                                globalDeferred.resolve(false);
                            }
                        });
                    } else {
                        logger.info("Recordings JSON is empty ", sessionId);
                        var pr = restler.post(nodeUrls.SET_ERROR, {
                            sessionId: sessionId.toString(),
                            err: "RECORD_JSON_EMPTY"
                        });
                        pr.then(function () {
                            globalDeferred.resolve(true);
                        });
                        }
                } else {
                    logger.error("post request to get master JSON of recording failed ", sessionId);
                    globalDeferred.resolve(false);
                }
            }).catch(errorCallBack);
        } else {
            logger.warn("The session with Id :" + sessionId + " does not have a\n\
                 conference Id. Probably did not happen through socialvid \n\
            or already recording is obtained at end session");
            if (!resp) {
                globalDeferred.resolve(false);
            } else if (!resp.conferenceId) { //TODO if replay is already prepared
                globalDeferred.resolve(true);
            }//TODO handle conf deleted case
        }
    }).catch(errorCallBack);
};

var getTwilioRecordings = function (callSids) {
    logger.info("getting recondings for callSids", callSids.length);
    if(!callSids || callSids.length<1){
        globalDeferred.resolve(true);
    }else{
        twilioRecordings = Q.defer();
        twilioRecordings.promise.then(function(resp){
            globalDeferred.resolve(resp);
        }).catch(errorCallBack);
        getTwilioRecordingForSids(callSids,0,twilioRecordings);
    }
};
var getTwilioRecordingForSids=function (list,index,deffered){
    logger.info("getting recondings for callSids ",list[index]);
    twilioSid = Q.defer();
    twilioSid.promise.then(function(resp){
        if(resp){
            index++;
            if(list[index]){
                getTwilioRecordingForSids(list,index,deffered);
            }else{
             deffered.resolve(resp);
            }
        }else{
           deffered.resolve(resp);
        }
        }).catch(errorCallBack);
    getTwilioRecordingForSid(list[index],twilioSid);
};
var getTwilioRecordingForSid=function (callSid,deffered){
    logger.info("getting reconding url for callSid",callSid);
    twilioClient.getReplayUrls(callSid,function(urls){
        if(!urls || urls.length<1){
           deffered.resolve(true);
        }else{
            for (var i = 0; i < urls.length; i++) {
                urls[i].sessionId = globalSessionId;
                urls[i].fileName = globalSessionId + "/";
                urls[i].userId = globalSessionId;
                urls[i].type = "mp3";
                urls[i].startTime = new Date(urls[i].startTime).getTime()+1000;
            }
            var fileDownloadDefferd=Q.defer();
            var downloadPromise = downloadRecordingsTwilio(urls, fileDownloadDefferd);
                downloadPromise.then(function (resp) {
                        deffered.resolve(resp);
                }).catch(errorCallBack);
        }
    });
};
var getFilePath = function (recordingsJson, deferred) {
    var filePathdeferred = Q.defer();
    var audioFilesPath = [];
    var singleRecordingJson = recordingsJson.pop();
    logger.info("Getting audio filepaths from the master json ", singleRecordingJson);
    restler.get(socialVidUrl + singleRecordingJson.path).then(function (resp) {
        logger.info("Got resp for filepaths : ", resp);
        if (resp && resp.participantsAudioData && resp.participantsAudioData.length > 0) {
            for (var i = 0; i < resp.participantsAudioData.length; i++) {
                resp.participantsAudioData[i].sessionId = parseInt(singleRecordingJson.confName);
                resp.participantsAudioData[i].fileName = singleRecordingJson.confName + "/";
                resp.participantsAudioData[i].userId=parseInt((resp.participantsAudioData[i].filePath.split("_")[1]));
                resp.participantsAudioData[i].startTime += singleRecordingJson.startTime;
                resp.participantsAudioData[i].type = "opus";
                audioFilesPath.push(resp.participantsAudioData[i]);
            }
            logger.info("got audio files path ", audioFilesPath);
            if (audioFilesPath.length > 0) {
                var downloadPromise = downloadRecordings(audioFilesPath, filePathdeferred);
                downloadPromise.then(function (resp) {
                    if (resp && recordingsJson.length > 0) {
                        getFilePath(recordingsJson, deferred);
                    } else {
                        deferred.resolve(true);
                    }
                }).catch(errorCallBack);
            } else {
                deferred.resolve(false);
                //TODO why this not handled
            }
        }else if(resp && resp.participantsAudioData && resp.participantsAudioData.length == 0){
            logger.info("This JSON doesn't have any audio files skipping");
             if (recordingsJson.length > 0) {
                getFilePath(recordingsJson, deferred);
            } else {
                deferred.resolve(true);
            }
        } else {
//            logger.error("Failed to get audio paths from master JSON data for sessionId : "+configProps.sessionId+"  conf:" + singleRecordingJson.confName);
//            deferred.resolve(false); //TODO why marked true
            logger.info("This JSON doesn't exists skipping");
            if (recordingsJson.length > 0) {
                getFilePath(recordingsJson, deferred);
            } else {
                deferred.resolve(true);
            }
        }
    }).catch(errorCallBack);
    return deferred.promise;
};



var downloadRecordings = function (audioFilesPath, filePathdeferred) {
    logger.info("downloading audio recording for the files ", audioFilesPath);

    var audioFileDetails = audioFilesPath.pop();
    logger.log("Downloading recordings for audiofile details", audioFileDetails);
    httpClient.get(socialVidUrl + audioFileDetails.filePath).then(function (data) {
        data = data.data;
        if (!data) {
            logger.error("error downloading file for session Id ", audioFileDetails.sessionId);
            filePathdeferred.resolve(false); //TODo why marked true
        } else {
            logger.log("audio file downloaded");
            var uploadPromise = uploadToS3(audioFileDetails, data);
            uploadPromise.then(function (status) {
                logger.info("Uploading files to s3 with status : ", status);
                if (status) {
                    logger.info("successfully uploaded to s3");//why was this error TODO
                    if (audioFilesPath.length > 0) {
                        downloadRecordings(audioFilesPath, filePathdeferred);
                    } else {
                        filePathdeferred.resolve(true);
                    }
                } else {
                    logger.error("error in uploading to s3 for sessionId ", audioFileDetails.sessionId);
                    filePathdeferred.resolve(false); //TODO why marked true
                }
            }).catch(errorCallBack);
        }
    }).catch(errorCallBack);
    return filePathdeferred.promise;
};

var downloadRecordingsTwilio = function (audioFilesPath, filePathdeferred) {
    logger.info("downloading audio recording for the files ", audioFilesPath);

    var audioFileDetails = audioFilesPath.pop();
    logger.log("Downloading recordings for audiofile details", audioFileDetails);
    httpClient.get(audioFileDetails.filePath).then(function (data) {
        if(data.statusCode!=200){
            logger.log("File not found");
            if (audioFilesPath.length > 0) {
                downloadRecordingsTwilio(audioFilesPath, filePathdeferred);
            } else {
                filePathdeferred.resolve(true);
            }
            return;
        }
        data = data.data;
        if (!data) {
            logger.error("error downloading file for session Id ", audioFileDetails.sessionId);
            filePathdeferred.resolve(false); //TODo why marked true
        } else {
            logger.log("audio file downloaded");
            var uploadPromise = uploadToS3(audioFileDetails, data);
            uploadPromise.then(function (status) {
                logger.info("Uploading files to s3 with status : ", status);
                if (status) {
                    logger.info("successfully uploaded to s3");//why was this error TODO
                    if (audioFilesPath.length > 0) {
                        downloadRecordingsTwilio(audioFilesPath, filePathdeferred);
                    } else {
                        filePathdeferred.resolve(true);
                    }
                } else {
                    logger.error("error in uploading to s3 for sessionId ", audioFileDetails.sessionId);
                    filePathdeferred.resolve(false); //TODO why marked true
                }
            }).catch(errorCallBack);
        }
    }).catch(errorCallBack);
    return filePathdeferred.promise;
};

var uploadToS3 = function (audioFileDetails, data) {
    logger.info("In upload to S3 with file details : ", audioFileDetails);
    var deferred = Q.defer();
    var userId = audioFileDetails.userId;
    var sessionId = audioFileDetails.sessionId;
//    var fileName = tmpDir + sessionId + '/' + userId + "/" + audioFileDetails.startTime + ".opus";
//    if (!filesStoredInSystem[sessionId]) {
//        filesStoredInSystem[sessionId] = {};
//    }
//    if (!filesStoredInSystem[sessionId][userId]) {
//        filesStoredInSystem[sessionId][userId] = [];
//    }
//    filesStoredInSystem[sessionId][userId].push({
//        startTime: audioFileDetails.startTime,
//        location: fileName
//    });
//
//    logger.info("creating dir=" + fileName);
//    mkdirp(path.dirname(fileName), function (err) {
//        if (err) {
//            logger.error("error in creating dir ", err);
//            deferred.resolve(false);
//        } else {
//            logger.info("writing file=" + fileName);
//            fs.writeFile(fileName, data, function (err) {
//                if (err) {
//                    logger.error("error in writing to file ", err);
//                    deferred.resolve(false);
//                } else {
//                    deferred.resolve(true);
//                }
//            });
//        }
//    });
var fileName = sessionId + '/' + userId + "/" + audioFileDetails.startTime + "."+audioFileDetails.type;
    uploadFileToS3(fileName, data, function (err) {
        if (err) {
            console.error("error in uploading file to s3",audioFileDetails);
            deferred.resolve(false);
        } else {
            var audioFilesDetail = {
                userId: userId,
                sessionId: sessionId,
                fileName: fileName,
                startTime: audioFileDetails.startTime,
                type:audioFileDetails.type
            };
            uploadMetadataToGapi(audioFilesDetail, function (err) {
                if (err) {
                    deferred.resolve(false);
                } else {
                    deferred.resolve(true);
                }

            },0);
        }
    },audioFileDetails.type);
    return deferred.promise;
};
var uploadMetadataToGapi = function (audioFileDetails, callback,count) {
    logger.info("uploading to filemgmt endpoint");
    var deferred = Q.defer();
    var userId = audioFileDetails.userId;
    var fileName = audioFileDetails.fileName;

    platform.saveSessionMetadata({
        collection: [
            {sessionId: parseInt(audioFileDetails.sessionId),
                fileData: JSON.stringify({
                    startTime: audioFileDetails.startTime,
                    bucketName: configProps["aws-s3"].bucketName,
                    uploadedCloud: "aws_s3",
                    awsS3Key: fileName
                }),
                fileType: "audio/"+audioFileDetails.type,
                userId: userId,
                fileId: audioFileDetails.startTime}]
    }).then(function (resp) {
        if (resp) {
            logger.info("collection response for saving aws metadata: ", resp.fileMetadataCollection);
            callback();
        } else {
            if(count>3){
           callback(true);
            logger.error("Error in saving aws metadata for sessionId ", audioFileDetails.sessionId);
        }else{
            logger.info("saving aws metadata for sessionId failed retry "+count, audioFileDetails.sessionId);
            count++;
            uploadMetadataToGapi(audioFileDetails, callback,count);
        }
        }
    }).catch(errorCallBack);
    return deferred.promise;
};

var deleteConference = function (sessionId, confId) {
    logger.log("deleting conference for session " + sessionId);
    var url = socialVidDeleteUrl;
    var args = {
        "email": socialVidEmail,
        "session": configProps.socialVidToken,
        "id": confId
    };
    restler.post(url, args).then(function (resp) {
        logger.info("deleted conference id " + confId + " for session " + sessionId);
        //            mongo_session.setConferenceDeleted(sessionId);
        restler.post(nodeUrls.SET_CONFERENCE_DELETED, {
            sessionId: sessionId.toString()
        });
    }).catch(errorCallBack);
};
var errorCallBack = function (e) {
    logger.error("an error occured in promise callback "+configProps.sessionId+"" + e);
    globalDeferred.resolve(false);
};
var prepareReplay = function (sessionId) {
    logger.log("Preparing replay session for " + sessionId);
//            return restler.get(fosUrls.GET_SESSION_INFO + "?id=5778504948383744");
    return commonForAllServices(sessionId);
};
var uploadSingleFileToS3 = function (sessionId, uploadS3Promise) {
    var sessionInfoPromise = platform.getSessionInfo(sessionId);
    sessionInfoPromise.then(function (resp) {
        if (resp) {
            var sessionInfo = resp;
            var startTime = parseInt(sessionInfo.startTime);
            var endTime = parseInt(sessionInfo.endTime);
            var duration = (endTime - startTime) / 1000;
            var users = [];
            filesStoredInSession = filesStoredInSystem[resp.id];
            for (var i in filesStoredInSession) {
                users.push(i);
            }
            if (users.length > 0) {
                createFilesOfUser(filesStoredInSession[users[0]], users[0], sessionId, startTime, duration, function () {
                    if (users[1]) {
                        createFilesOfUser(filesStoredInSession[users[1]], users[1], sessionId, startTime, duration, function () {
//                        createSingleFileOpus(sessionId, users, function() {
                            uploadFilesToS3(sessionId, users, startTime, function () {
                                uploadS3Promise.resolve(true);
                            });
//                        });
                        });
                    } else {
                        logger.error("No files found for 2nd user in tmp directory"+configProps.sessionId+"");
                        globalDeferred.resolve(false);
                    }
                });
            } else {
                logger.error("No files add to temp record for any user"+configProps.sessionId+"");
                globalDeferred.resolve(false);
            }
        } else {
            logger.error("an error occured in sessionInfo " + JSON.stringify(resp));
            globalDeferred.resolve(false);
            //uploadSingleFileToS3(sessionId, uploadS3Promise);TODO why recursion
        }
    }).catch(function (e) {
        logger.error(e)
    });
};

function createSingleFileOpus(sessionId, users, callback) {
    var ffmpegOpts = ['-y'];
    for (var i in users) {
        ffmpegOpts.push('-i', tmpDir + sessionId + '/files/' + users[i] + '.mp3');
//        ffmpegOpts.push('-i', tmpDir+sessionId + '/files/' + users[i] + '.opus');
    }
    ffmpegOpts.push('-filter_complex', '[0:a][1:a]amix=inputs=2:duration=longest:dropout_transition=1[aout]', '-map', '[aout]', '-acodec', 'libopus', tmpDir + sessionId + '/files/common.opus');
//    ffmpegOpts.push('-filter_complex', '[0:a][1:a]amix=inputs=2:duration=first:dropout_transition=1[aout]', '-map', '[aout]', tmpDir+sessionId + '/files/common.opus');
    spawn('ffmpeg', ffmpegOpts).on('error', function (err, stdout, stderr) {
        logger.info(err);
    }).on('exit', function (code, signal) {
        if (code) {
            //TODO remove recursion
            createSingleFileOpus(sessionId, users, callback);
        } else {
            callback();
        }
    });
}

function createFilesOfUser(userData, userId, sessionId, startTime, duration, callback) {
    createFileOfUser(userData, userId, sessionId, startTime, duration, function (err) {
        if (err) {
            logger.error("Error storing file ", err);
//            createFilesOfUser(userData, userId, sessionId, startTime, duration, callback);
            //TODO remove recursion
        } else {
            if (callback) {
                callback();
            }
        }
    });
};

function uploadFilesToS3(sessionId, users, sessionStartTime, callback,type) {
    logger.info("uploadFilesToS3  >> sessionID=" + sessionId + " users=" + JSON.stringify(users) + "  sessionStartTime=" + JSON.stringify(sessionStartTime));
    var filesDir = fs.readdirSync(tmpDir + sessionId + '/files');
    var files = [];
    filesDir.forEach(function (file) {
        files.push(file);
    });
    var data = fs.readFileSync(tmpDir + sessionId + '/files/' + files[0]);
    uploadFileToS3(sessionId + '/' + files[0], data, function () {
        var data = fs.readFileSync(tmpDir + sessionId + '/files/' + files[1]);
        uploadFileToS3(sessionId + '/' + files[1], data, function () {
//            var data = fs.readFileSync(tmpDir+sessionId + '/files/' + files[2]);
//            uploadFileToS3(sessionId + '/' + files[2], data, function() {
            uploadfilesMetaData(sessionId, users, sessionStartTime, files, callback,type);
//            });
        });
    });
};

function uploadfilesMetaData(sessionId, users, sessionStartTime, files, callback,type) {
    var audioFilesDetails = {
        userId: users[0],
        sessionId: sessionId,
        fileName: sessionId + '/' + users[0] + '.'+type,
        startTime: sessionStartTime
    };
    uploadMetadataToGapi(audioFilesDetails, function () {
        var audioFilesDetails = {
            userId: users[1],
            sessionId: sessionId,
            fileName: sessionId + '/' + users[1] +'.'+type,
            startTime: sessionStartTime
        };
        uploadMetadataToGapi(audioFilesDetails, function () {
            callback();
        });
    });
}

function uploadFileToS3(fileName, data, callback) {
    logger.info("uploadFileToS3  >> fileName=" + fileName);
    var params = {
        Bucket: configProps["aws-s3"].bucketName,
        Key: fileName,
        Body: data
    };
    s3bucket.putObject(params, function (err, data) {
        logger.info("in s3 upload callback");
        if (!err) {
            callback();
        } else {
            logger.error("error in uploading file to S3"+configProps.sessionId+"", err);
            callback(true);
        }
    });
}

function createFileOfUser(userData, userId, sessionId, startTime, duration, callback) {
    logger.info("createFileOfUser userData=" + JSON.stringify(userData) + " userID=" + userId + " sessionId=" + sessionId + " startTime=" + startTime + " duration=" + duration);
    var ffmpegOpts = ['-y', '-i', 'silence.opus'];
    var ffmpegInputs = [];
    var silenceDuration = [];
    for (var j in userData) {
        var userFile = userData[j];
        ffmpegInputs.push(userFile.location);
        silenceDuration.push((userFile.startTime - startTime) / 1000);
    }
    var miniCommand = '';
    var mainCommand = '';
    for (var input in ffmpegInputs) {
        ffmpegOpts.push('-i', ffmpegInputs[input]);
        miniCommand += '[0:a]atrim=0:' + (parseInt(silenceDuration[input])) + '[s' + input + '];[s' + input + '][' + (parseInt(input) + 1) + ':a]concat=n=2:v=0:a=1[ac' + input + '];';
        mainCommand += '[ac' + input + ']';
    }
    mainCommand += 'amix=inputs=' + (ffmpegInputs.length) + ':duration=longest:dropout_transition=1[aout]';
    mainCommand = miniCommand + mainCommand;
    ffmpegOpts.push('-filter_complex', mainCommand, '-map', '[aout]', '-b:a', '40000', tmpDir + sessionId + '/files/' + userId + '.opus');
    mkdirp(path.dirname(tmpDir + sessionId + '/files/' + userId + '.opus'), function (err) {
        if (err) {
            logger.error(err);
            callback('err');
        } else {
            spawn('ffmpeg', ffmpegOpts).on('error', function (err, stdout, stderr) {
                logger.info(">>>>>>>>>>>>>>>>>>>>>>>");
                if (err) {
                    logger.error(err);
                }
                logger.info(stdout);
                logger.info(stderr);
            }).on('exit', function (code, signal) {
                logger.info("signal>>>>", signal);
                if (code) {
                    logger.error("Error in executing ffmpeg command ", code);
                    callback('err');
                } else {
                    callback();
                }
            });
        }
    });
}

function spawn(name, opt) {
    logger.info('Spawn', {
        cmd: [name].concat(opt).join(' ')
    });
    return require('child_process').spawn(name, opt);
}
//require('child_process').exec(
//        'cp /var/task/ffmpeg /tmp/.; chmod 755 /tmp/ffmpeg;',
//        function (error, stdout, stderr) {
//            if (error) {
//                //handle error
//            } else {
//                console.log("stdout: " + stdout);
//                console.log("stderr: " + stderr);
//                //handle success
//            }
//        }
//);
module.exports.prepareReplay = prepareReplay;