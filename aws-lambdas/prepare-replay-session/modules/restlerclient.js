/* global process, require, exports, module */

var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var Q = require("q");
var restler = require('restler');
var authTokens = require(process.env["APP_HOME"] + "/config.js").getConfigProps().platform;
var statusCodes = {
    OK: 200
};
var addAuthTokensToHeader = function (params) {
    params.headers = params.headers || {};
    params.headers["X-Ved-Client"] = authTokens.client;
    params.headers["X-Ved-Client-Id"] = authTokens.clientId;
    params.headers["X-Ved-Client-Secret"] = authTokens.clientSecret;
    return params;
};
function get(url) {//TODO later add params to the url after encoding
    logger.log("sending get request to URL " + url);
    var deferred = Q.defer();
    var params={};
    params=addAuthTokensToHeader(params);
    restler.get(url,params)
            .on('complete', function (result, response) {
                if (!response || response.statusCode !== statusCodes.OK || result instanceof Error) {
                    if (result instanceof Error) {
                        logger.warn('Error: for url ' + url, result.message);
                    } else {
                        logger.warn("status code of response ", response.statusCode);
                    }
                    deferred.resolve(null);
                } else {
                    if (typeof result === "string") {
                        try {
                            result = JSON.parse(result);
                        } catch (e) {
                            //swallow
                        }
                    }
                    //logger.debug("result for url " + url, result);
                    deferred.resolve(result);
                }
            });
    return deferred.promise;
}

function post(url, params, postURLEncoded) {
    logger.log("sending post request to URL " + url, params);
    var deferred = Q.defer();
    params = params || {};
    var fn = restler.postJson;
    if (postURLEncoded) {
        params = {data: params};
        fn = restler.post;
    }
    var headers=addAuthTokensToHeader({});
    fn(url, params,headers)
            .on('complete', function (result, response) {
                if (!response || response.statusCode !== statusCodes.OK || result instanceof Error) {
                    if (result instanceof Error) {
                        logger.warn('Error: for url ' + url, result.message);
                    } else {
                        logger.warn("status code of response for url " + url, response.statusCode);
                    }
                    deferred.resolve(null);
                } else {
                    if (typeof result === "string") {
                        try {
                            result = JSON.parse(result);
                        } catch (e) {
                            //swallow
                        }
                    }
                    //logger.debug("result for url " + url, result);
                    deferred.resolve(result);
                }
            });
    return deferred.promise;
}
// Functions which will be available to external callers
module.exports.get = get;
module.exports.post = post;