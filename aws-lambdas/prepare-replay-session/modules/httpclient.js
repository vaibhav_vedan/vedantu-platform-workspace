/* global process, require, exports, module */

var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var Q = require("q");
var authTokens=configProps['platform'];
var Client = require('node-rest-client').Client;
var restClient = new Client();
var addAuthTokensToHeader = function (params) {
    params.headers = params.headers || {};
    params.headers["X-Ved-Client"] = authTokens.client;
    params.headers["X-Ved-Client-Id"] = authTokens.clientId;
    params.headers["X-Ved-Client-Secret"] = authTokens.clientSecret;
    return params;
};
function get(url, params) {
    logger.log("sending get request to URL " + url + " with the following params ", params);
    var deferred = Q.defer();
    params = params || {};
    params = addAuthTokensToHeader(params);
    restClient.get(url, params, function (data, response) {
        var _data;
        try {
            console.log(data);
            _data = JSON.parse(data.toString());
        } catch (err) {
            logger.warn("Error in json parsing of http get response ", err);
            _data = data;
        }
        logger.debug("response for url " + url, _data);
        deferred.resolve({statusCode: response.statusCode, data: _data});
    }).on('error', function (err) {
        logger.warn("Error in httpclient get call sessionId:"+configProps.sesessionId+" for url " + url, err);
        deferred.resolve(null);
    });
    return deferred.promise;
}

function post(url, params) {
    logger.log("sending Post request to URL" + url + "With the following params", params);
    var deferred = Q.defer();
    params = params || {};
    params = addAuthTokensToHeader(params);
    //var args = {data : params, headers:{"Content-Type": "application/json"}}; 
    //sending headers as params now directly
    restClient.post(url, params, function (data, response) {
        var _data;
        try {
            _data = JSON.parse(data);
        } catch (err) {
            logger.warn("Error in processing of http post response as json " + err);
            _data = data;
        }
        logger.debug("response for url " + url);
        logger.debug(_data);
        deferred.resolve({statusCode: response.statusCode, data: _data});
    }).on('error', function (err) {
        logger.warn("Error in httpclient post call sessionId:"+configProps.sesessionId+" for url " + url, err);
        deferred.resolve(null);
    });

    return deferred.promise;
}
// Functions which will be available to external callers
module.exports.get = get;
module.exports.post = post;