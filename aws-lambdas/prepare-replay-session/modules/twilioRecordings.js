var configProps = require(  "../config.js").getConfigProps();
var logger = require("../utils/logger.js");
var twilioClient = require('twilio')(configProps.twilio.accountSid, configProps.twilio.authToken);
var twilioHeadr="https://"+configProps.twilio.accountSid+":"+configProps.twilio.authToken+"@api.twilio.com";

var replayUrls=function(callSid,callback){
   logger.log("getting twilio recording urls for callSid>>",callSid);
   twilioClient.recordings.list({ callSid: callSid}, function(err, data) {
       var recordingUrls=[];
       if(err){
          logger.log("error getting twilio recording urls for callSid>>"+callSid,err); 
          callback(null);
       }
        data.recordings.forEach(function(recording) {
            var uri=twilioHeadr+recording.uri;
            uri=uri.substr(0,uri.length-4)+"mp3";
            logger.log("found recoding with sid:"+recording.sid+" url:",uri);
            recordingUrls.push({filePath:uri,startTime:recording.dateCreated});
        });
        callback(recordingUrls);
    });
};

var deleteRecording= (recording) => {
    console.log("Deleting recording: " + recording);
    twilioClient.recordings(recording['sid'])
        .remove()
        .then(recording => logger.log("Deleted recording: " + recording['sid']));
};
module.exports.getReplayUrls = replayUrls;
module.exports.deleteRecordings = deleteRecording;

