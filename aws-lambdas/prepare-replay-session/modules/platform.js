var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var httpClient = require(process.env["APP_HOME"] + "/modules/httpclient.js");
var authTokens=configProps['platform'];
var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var addAuthTokensToHeader=function(params){
    params.headers = params.headers || {};
//    params.headers["X-Ved-Client"]=authTokens.client;
//    params.headers["X-Ved-Client-Id"]=authTokens.clientId;
//    params.headers["X-Ved-Client-Secret"]=authTokens.clientSecret;
    params.headers["Content-Type"]="application/json";
    params.headers["Accept"]="application/json";
    return params;
};

var platformGetRequest = function(url,params,skipToken){
    params = params || {};
    if(!skipToken){
        params=addAuthTokensToHeader(params);
    }
    return httpClient.get(url,params);
};

var platformPostRequest = function(url,params,skipToken){
    params = params || {};
    if(!skipToken){
        params=addAuthTokensToHeader(params);
    }
    return httpClient.post(url,params);
};

var getSessionInfo = function(sessionId){
    var url=authTokens.url+"/platform/session/"+sessionId;
    var params={};
    return platformGetRequest(url,params);
};

var saveSessionMetadata = function(data){
    var params={
        data:data
    };
    var url=authTokens.url+"/platform/filemgmt/insertFileMetadataForAWS";
    return platformPostRequest(url,params);
};


module.exports.getSessionInfo = getSessionInfo;
module.exports.saveSessionMetadata = saveSessionMetadata;