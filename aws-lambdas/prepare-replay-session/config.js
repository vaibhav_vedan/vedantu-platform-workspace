/* global module */
var getTwilioAppSidFor = function (mode) {
    var AppSid = "";
    switch (mode) {
        case 'local':
            AppSid = "APbbc5ae8142e52b3cf0449c5da4461f7a";
            break;
        case 'prod':
            AppSid = "AP8621d9380bdb9e22ed7fad9383da58c2";
            break;
        case 'qa':
            AppSid = "AP515207f1a2a58b7f3488f9913c0349a0";
            break;
        case 'dev':
            AppSid = "AP3872c24ca725a1856dff037b3fe72e71";
            break;
        case 'wave':
            AppSid = "AP0c0f41ef2bcadeb7c885243a724624c5";
            break;
        case 'qa1':
            AppSid = "AP19a47d69e8d78184ca6ecc77ca0d6ff2";
            break;
        case 'dev1':
            AppSid = "AP8976275be6e2881b6e29d991eb0b9e3f";
            break;
        case 'dev2':
            AppSid = "AP14581cc56b4fbd1146eee81e35a09af3";
            break;
        case 'mobile':
            AppSid = "APe3325fb59657767d3b253f941e9bc869";
            break;

    }
    return AppSid;
};
var localsettings = {
    appenginePropss: {
        domain: "http://localhost:8080"
    },
    appengineProps: {
        domain: "http://localhost:8080"
    },
    sessionId: null,
    env: "local",
    logs: {
        level: "log",
        filePath: "/var/log",
        maxDaysToKeepFile: 7, //days
        intervalToCheckFiles: 5 //hrs
    },
    "aws-s3": {
        bucketName: "vedantu-fos-qa-session-recordings-new",
        endpoint: 'https://s3-ap-southeast-1.amazonaws.com',
        accountId:'617558729962',
        lambdaRegion: 'ap-northeast-1',
        snsRegion: 'ap-southeast-1'
    },
    raven: {
        key: "https://a6d982ce5c5949e8ac99117c1e91c5fc:23a3dc21359b4c62a737fc60ce71958f@app.getsentry.com/65239"
    },
    nodeClients: {
        url: "http://localhost:20001"
    },
    nodeClient: {
        url: "http://localhost:20001"
    },
    twilio: {
        accountSid: 'AC939a75b0c4c44b6d540f2aec0466145c',
        authToken: "6c3f5ab870456fa05f3d1d0b822fa785",
        callerId: '+12512377040',
        waveAppSid: getTwilioAppSidFor('local')
    },
    platform: {
        client: "NODE",
        clientId: "E25D9",
        clientSecret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk",
        url: "http://localhost:8081"
    }
};


var prodsettings = {
    appengineProps: {
        domain: "https://www.vedantu.com"
    },
    sessionId: null,
    env: "prod",
    logs: {
        level: "info",
        filePath: "/var/log",
        maxDaysToKeepFile: 30, //days
        intervalToCheckFiles: 5 //hrs
    },
    "aws-s3": {
        bucketName: "vedantu-fos-session-recordings",
        endpoint: 'https://s3-ap-southeast-1.amazonaws.com',
        accountId:'617558729962',
        lambdaRegion: 'ap-northeast-1',
        snsRegion: 'ap-southeast-1'
    },
    raven: {
        key: "https://b6140e0a17614079888fcb2b550e5185:2643abfc706b4fb684baa104ad76b7e4@app.getsentry.com/79422"
    },
    nodeClient: {
        url: "https://ns1.vedantu.com"
    },
    twilio: {
        accountSid: 'AC939a75b0c4c44b6d540f2aec0466145c',
        authToken: "6c3f5ab870456fa05f3d1d0b822fa785",
        callerId: '+12512377040',
        waveAppSid: getTwilioAppSidFor('prod')
    },
    platform: {
        client: "NODE",
        clientId: "E25D9",
        clientSecret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk",
        url: "https://platform.vedantu.com"
    }
};
var qasettings = {
    appengineProps: {
        domain: "https://fos-qa.vedantu.com"
    },
    sessionId: null,
    env: "qa",
    logs: {
        level: "info",
        filePath: "/var/log",
        maxDaysToKeepFile: 30, //days
        intervalToCheckFiles: 5 //hrs
    },
    "aws-s3": {
        bucketName: "vedantu-fos-qa-session-recordings",
        endpoint: 'https://s3-ap-southeast-1.amazonaws.com',
        accountId:'617558729962',
        lambdaRegion: 'ap-northeast-1',
        snsRegion: 'ap-southeast-1'
    },
    raven: {
        key: "https://a6d982ce5c5949e8ac99117c1e91c5fc:23a3dc21359b4c62a737fc60ce71958f@app.getsentry.com/65239"
    },
    nodeClient: {
        url: "https://qa-rtc.vedantu.com:20001"
    },
    twilio: {
        accountSid: 'AC939a75b0c4c44b6d540f2aec0466145c',
        authToken: "6c3f5ab870456fa05f3d1d0b822fa785",
        callerId: '+12512377040',
        waveAppSid: getTwilioAppSidFor('qa')
    },
    platform: {
        client: "NODE",
        clientId: "E25D9",
        clientSecret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk",
        url: "https://qa-platform.vedantu.com"
    }
};

var propsMap = {
    local: localsettings,
    prod: prodsettings,
    qa: qasettings
};
var propertiesForCurrentMode = {};
var getPropsForQAProfiles = function (qaMode) {
    return {
        appengineProps: {
            domain: "https://fos-" + qaMode + ".vedantu.com"
        },
        logs: {
            level: "log",
            filePath: "/var/log",
            maxDaysToKeepFile: 7, //days
            intervalToCheckFiles: 5 //hrs
        },
        sessionId: null,
        env: qaMode,
        "aws-s3": {
            bucketName: "vedantu-fos-qa-session-recordings-new",
            endpoint: 'https://s3-ap-southeast-1.amazonaws.com',
            accountId:'427886946224',
            lambdaRegion: 'ap-southeast-1',
            snsRegion: 'ap-southeast-1'
        },
        raven: {
            key: "https://a6d982ce5c5949e8ac99117c1e91c5fc:23a3dc21359b4c62a737fc60ce71958f@app.getsentry.com/65239"
        },
        nodeClient: {
            url: "https://" + qaMode + "-rtc.vedantu.com"
        },
        twilio: {
            accountSid: 'AC939a75b0c4c44b6d540f2aec0466145c',
            authToken: "6c3f5ab870456fa05f3d1d0b822fa785",
            callerId: '+12512377040',
            waveAppSid: getTwilioAppSidFor(qaMode)
        },
        platform: {
            client: "NODE",
            clientId: "E25D9",
            clientSecret: "mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk",
            url: "https://" + qaMode + "-platform.vedantu.com"
        }
    };
};
module.exports = {
    setConfigPropsForCurrentMode: function (mode) {
        if (mode === "local" || mode === "prod" || mode === "qa") {
            propertiesForCurrentMode = propsMap[mode];
        } else {
            propertiesForCurrentMode = getPropsForQAProfiles(mode);
        }
    },
    getConfigProps: function () {
        return propertiesForCurrentMode;
    }
};
