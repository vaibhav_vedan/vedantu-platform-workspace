var QueueUrl = 'https://sqs.ap-southeast-1.amazonaws.com/617558729962/slashrtc_prod';
var AWS = require('aws-sdk');
var sqs = new AWS.SQS({region: 'ap-southeast-1'});

exports.handler = (event, context, callback) => {

    const params = {
        MessageBody: JSON.stringify(event),
        MessageAttributes: {messageType: {
                DataType: "String", StringValue: "SLASHRTC_ACTIVITY"
            }},
        QueueUrl
    };

    sqs.sendMessage(params, function (err, data) {
        if (err) {
            console.error("Error", JSON.stringify(err));
            callback({
                statusCode: 500,
                body: JSON.stringify(err),
            });
        } else {
            console.log("Success data:" + data, data.MessageId);
            callback(null, data.MessageId)
        }
    });

};
