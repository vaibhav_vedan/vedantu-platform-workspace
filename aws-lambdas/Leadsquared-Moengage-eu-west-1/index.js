var QueueUrl = 'https://sqs.eu-west-1.amazonaws.com/617558729962/leadsquaredEvents_prod';
var AWS = require('aws-sdk');
var sqs = new AWS.SQS({region: 'eu-west-1'});

exports.handler = (event, context, callback) => {

    const params = {
        MessageBody: JSON.stringify(event),
        MessageAttributes: {messageType: {
                DataType: "String", StringValue: "LEAD_STAGE_CHANGE"
            }},
        QueueUrl
    };

    sqs.sendMessage(params, function (err, data) {
        if (err) {
            console.error("Error", JSON.stringify(err));
            callback({
                statusCode: 500,
                body: JSON.stringify(err),
            });
        } else {
            console.log("Success data:" + data, data.MessageId);
            callback(null, data.MessageId)
        }
    });

};
