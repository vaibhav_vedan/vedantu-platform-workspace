package com.vedantu.pdftoimage;

import com.amazonaws.HttpMethod;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.event.S3EventNotification.S3EventNotificationRecord;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.sun.jersey.api.client.ClientResponse;
import org.apache.http.HttpStatus;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Pattern;

public class PDFtoImageManager {

    private ConfigUtils configUtils;
    private AmazonS3 s3Client;

    private static final String EXT_PNG = ".png";
    private static final String EXT_WEBP = ".webp";

    public PDFtoImageManager() {
        configUtils = new ConfigUtils();
        s3Client = new AmazonS3Client();
    }

    public void pdftoimageConverter(S3Event s3event, Context context) throws UnsupportedEncodingException {
        try {
            S3EventNotificationRecord record = s3event.getRecords().get(0);

            String srcBucket = record.getS3().getBucket().getName();
            String srcKey = record.getS3().getObject().getKey();
            srcKey = URLDecoder.decode(srcKey, "UTF-8");

            String environment = getEnvFromKey(srcKey);
            Logger.getLogger("default").info("SRC Path: " + srcKey);
            String contentPath = getContentFilePathFromKey(srcKey);

            S3Object object = s3Client.getObject(new GetObjectRequest(srcBucket, srcKey));
            InputStream objectData = object.getObjectContent();
            Logger.getLogger("default").info(objectData.toString());

            PDDocument document = PDDocument.load(objectData);
            PDFRenderer pdfRenderer = new PDFRenderer(document);

            String contentFileId = getContentIdFromContentPath(contentPath, environment, getDomain(srcBucket));
            Logger.getLogger("default").info("ContentFileId: " + contentFileId );

//            deleteContentFileContents(srcBucket, contentFileId, environment);
            deleteContentFileContentsFromNewPath(srcBucket, contentPath, environment);
            updateContentFileStatus(contentFileId, false, environment);

            Logger.getLogger("default").log(Level.INFO, "pdf page count {0}", document.getNumberOfPages());

            WebpIO webpIO = new WebpIO();
            Logger.getLogger("default").log(Level.INFO, "fetching cwebp file from s3");
            S3Object s3Object = s3Client.getObject(srcBucket, "cwebp");
            InputStream webPFileData = s3Object.getObjectContent();
            webpIO.init(webPFileData);

            for (int pageIndex = 0; pageIndex < document.getNumberOfPages(); pageIndex++) {
                try {
                    String destFileNamePng = getContentImageFilePath(contentPath, pageIndex, environment, EXT_PNG);
                    String destFileNameWebp = getContentImageFilePath(contentPath, pageIndex, environment, EXT_WEBP);
                    File outputPngFile = File.createTempFile(destFileNamePng, EXT_PNG);
                    File outputWebpFile = File.createTempFile(destFileNameWebp, EXT_WEBP);

                    Logger.getLogger("default").log(Level.INFO, "uploading PNG {0}", destFileNamePng);
                    
                    BufferedImage bim = pdfRenderer.renderImageWithDPI(pageIndex, 60);
                    ImageIO.write(bim, "png", outputPngFile);

                    PutObjectRequest putObjectRequest = new PutObjectRequest(srcBucket, destFileNamePng, outputPngFile);
                    putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);

                    s3Client.putObject(putObjectRequest);

                    Logger.getLogger("default").log(Level.INFO, "uploading WEBP {0}", destFileNameWebp);

                    try {

                        webpIO.toWEBP(outputPngFile,outputWebpFile);

                        PutObjectRequest putWebpObjectRequest = new PutObjectRequest(srcBucket, destFileNameWebp, outputWebpFile);
                        putWebpObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);

                        s3Client.putObject(putWebpObjectRequest);

                    }catch (Exception e){
                        Logger.getLogger("default").log(Level.WARNING, e.getMessage());
                    }

                    outputPngFile.delete();
                    outputWebpFile.delete();

                } catch (Exception e) {
                    Logger.getLogger("default").info(e.getMessage());
                }
            }

            updateContentFileStatus(contentFileId, true, environment);
            document.close();
        } catch (IOException e) {
            Logger.getLogger("default").info(e.getMessage());
        }
    }

    private String getDomain(String srcBucket) {
        if (srcBucket == null || "".equals(srcBucket.trim())) {
            throw new RuntimeException("unknown bucket");
        }

        String bucket = srcBucket.toLowerCase();
        Logger.getLogger("default").info("Bucket = " + bucket);
        if ("seo-manager-qa".equals(bucket) || "seo-manager".equals(bucket)) {
            return "VEDANTU";
        } else if ("seo-acadstack".equals(bucket) || "seo-acadstack-qa".equals(bucket)) {
            return "ACADSTACK";
        }
        throw new RuntimeException("unknown bucket");
    }

    private String getContentImageFilePath(String contentPath, int pageIndex, String environment, String type) {
            return environment + "/content-images" + contentPath + "/" + (pageIndex + 1)
                + type;
    }

    private String getContentFilePathFromKey(String key) {
        String[] fileNameSplit = key.split("content-files");
        String filenameWithExtension = fileNameSplit[fileNameSplit.length - 1];
        return filenameWithExtension.replace(".pdf", "");
    }

    private String getEnvFromKey(String key) {
        String[] fileNameSplit = key.split("/");
        //String filenameWithExtension = fileNameSplit[fileNameSplit.length - 1];
        //String[] fileNameWithoutExtensionSplit = filenameWithExtension.split("\\.");
        //String fileName = fileNameWithoutExtensionSplit[0];

        return fileNameSplit[0];
    }

    private void deleteContentFileContents(String bucket, String contentFileId, String environment) {
        try {
            if (contentFileId == null || "".equals(contentFileId.trim()) || "/".equals(contentFileId.trim())) {
                return;
            }
            String prefix = environment + "/content-images/" + contentFileId + "/";

            List<S3ObjectSummary> objectSummaries = s3Client.listObjects(bucket, prefix).getObjectSummaries();
            for (S3ObjectSummary objectSummary : objectSummaries) {
                s3Client.deleteObject(bucket, objectSummary.getKey());
            }
        } catch (Exception ex) {
        }
    }

    private void deleteContentFileContentsFromNewPath(String bucket, String contentPath, String environment) {
        try {

            if (contentPath == null || "".equals(contentPath.trim()) || "/".equals(contentPath.trim())) {
                return;
            }
            if (contentPath.startsWith("/")) {
                contentPath = contentPath.replaceFirst("/", "");
            }

            // Example Path : icse/ --> should not be processed icse/folder --> should be processed
            String[] paths = contentPath.trim().split("/");
            if (paths.length <= 1) {
                return;
            }

            String prefix = environment + "/content-images/" + contentPath + "/";

            Pattern pattern = Pattern.compile("^" + Pattern.quote(prefix) + "\\d+(\\.png|\\.webp)");
            for (S3ObjectSummary objectSummary : s3Client.listObjects(bucket, prefix).getObjectSummaries()) {
                String key = objectSummary.getKey();
                if (key.matches(pattern.pattern())) {
                    Logger.getLogger("default").info("DELETING: " + key);
                    s3Client.deleteObject(bucket, key);
                }
            }
        } catch (Exception ex) {
        }
    }

    private void updateContentFileStatus(String contentFileId, Boolean updateStatus, String environment) {
        String platformUpdateUrl;
        if (environment.equals("prod")) {
            platformUpdateUrl = "https://growth.vedantu.com/growth"
                    + configUtils.getStringValue("contentPageStatusUpdate") + "?contentPageId=" + contentFileId + "&status="
                    + updateStatus;

        } else {
            platformUpdateUrl = "https://" + environment + "-platform.vedantu.com/growth"
                    + configUtils.getStringValue("contentPageStatusUpdate") + "?contentPageId=" + contentFileId + "&status="
                    + updateStatus;
        }

        try {
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("X-Ved-Client", "PDF_TO_IMAGE_CONVERTER");
            headers.put("X-Ved-Client-Id", "2CDFC");
            headers.put("X-Ved-Client-Secret", "wTf9Coqjh581xR51c5qr5ahJF46ZZFJr");
            ClientResponse fosResp = WebUtils.INSTANCE.doCall(platformUpdateUrl, HttpMethod.PUT, null, headers);

            String res = fosResp.getEntity(String.class);
            Logger.getLogger("default").info(res);

            if (fosResp.getStatus() != HttpStatus.SC_OK) {
                throw new Exception("updateContentFileStatus failed for contentFileId:" + contentFileId);
            }
        } catch (Exception e) {
            Logger.getLogger("default").info(e.getMessage());
        }
    }

    private String getContentIdFromContentPath(String contentFilePath, String environment, String domain) {

        String platformUpdateUrl;
        if ("prod".equals(environment)) {
            platformUpdateUrl = "https://growth.vedantu.com/growth";
        } else {
            platformUpdateUrl = "https://" + environment + "-platform.vedantu.com/growth";
        }
        platformUpdateUrl += configUtils.getStringValue("getCategoryPageByUrl") +
                "?categoryPageUrl=" + contentFilePath +
                "&domain=" + domain;

        Logger.getLogger("default").info("Get Content ID Url: " + platformUpdateUrl);
        String res = null;

        try {
            Map<String, String> headers = new HashMap<String, String>();
            headers.put("X-Ved-Client", "PDF_TO_IMAGE_CONVERTER");
            headers.put("X-Ved-Client-Id", "2CDFC");
            headers.put("X-Ved-Client-Secret", "wTf9Coqjh581xR51c5qr5ahJF46ZZFJr");
            ClientResponse fosResp = WebUtils.INSTANCE.doCall(platformUpdateUrl, HttpMethod.GET, null, headers);

            res = fosResp.getEntity(String.class);
            Logger.getLogger("default").info(res);

            if (fosResp.getStatus() != HttpStatus.SC_OK || res == null || res.trim().equals("") || res.trim().equals("/") || res.contains("SERVICE_ERROR")) {
                throw new Exception("getCategoryPageUrlByUrl failed for contentFilePath:" + contentFilePath);
            }
            if (res.startsWith("/")) {
                res = res.replaceFirst("/", "");
            }

        } catch (Exception e) {
            Logger.getLogger("default").info(e.getMessage());
        }

        return res;
    }

 /*  public static void main(String[] args) throws IOException {

        PDFtoImageManager manager = new PDFtoImageManager();
        manager.testRun();
    }

     private void testRun() throws IOException {
        File file = new File("/home/chiru/Downloads/OpTransactionHistory10-11-2019.pdf");
        PDDocument document = PDDocument.load(file);
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        System.out.println("craeted>>>");

        WebpIO webpIO = new WebpIO();

        String accesskey = "AKIAWHIAMU6YFBOILPES";
        String secretKey = "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";

        AWSCredentials credentials = new BasicAWSCredentials(accesskey, secretKey);
        AmazonS3 s3client = new AmazonS3Client(credentials);
        S3Object s3Object = s3client.getObject("seo-manager-qa/local", "cwebp");
        InputStream objectData = s3Object.getObjectContent();
        webpIO.init(objectData);


        for (int pageIndex = 0; pageIndex < document.getNumberOfPages(); pageIndex++) {
            try {
                String destFileName = "/home/chiru/Downloads/tmp/pdftest-" + pageIndex + ".png";
                File outputfile = new File(destFileName);

                BufferedImage bim = pdfRenderer.renderImageWithDPI(pageIndex, 60);
                ImageIO.write(bim, "png", outputfile);

                destFileName = "/home/chiru/Downloads/tmp/pdftest-" + pageIndex + ".webp";
                File outputfileWebp = new File(destFileName);

                webpIO.toWEBP(outputfile, outputfileWebp);

//              outputfile.delete();
            } catch (Exception e) {
                System.err.println(">errror " + e.getMessage());
            }
        }
    }
    */

}