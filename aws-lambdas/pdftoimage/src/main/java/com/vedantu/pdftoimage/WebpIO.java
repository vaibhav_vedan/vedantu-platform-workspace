package com.vedantu.pdftoimage;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WebpIO {

    private String commandDir;
    private String webpTmpDir = "/tmp";

    public WebpIO() {
    }

    protected void init(InputStream objectData) {

        Logger.getLogger("default").log(Level.INFO, "Setting cwebp configs");

        File tmp = new File(webpTmpDir);
        if (!tmp.exists()) {
            tmp.mkdirs();
        }

        this.commandDir = tmp.getPath();

        try {
            Logger.getLogger("default").log(Level.INFO, "copy start : binary to " + tmp.getPath() + "/cwebp");
            copy(objectData, new File(tmp.getPath() + "/cwebp"));
            Logger.getLogger("default").log(Level.INFO, "coppied binary to " + tmp.getPath() + "/cwebp");
        } catch (Exception e) {
            Logger.getLogger("default").log(Level.WARNING, e.getMessage());
        }
    }

    public void toWEBP(File src, File dest) {
        try {
            String command = commandDir +  "/cwebp -q 100 -lossless " + src.getPath() + " -o " + dest.getPath();
            this.executeCommand(command);
        } catch (Exception e) {
            Logger.getLogger("default").log(Level.WARNING, e.getMessage());
        }
    }

    private String executeCommand(String command) {

        Logger.getLogger("default").info("Execute: " + command);

        StringBuilder output = new StringBuilder();
        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
            String line;
            while ((line = reader.readLine()) != null) {
                output.append(line).append("\n");
            }
        } catch (Exception e) {
            Logger.getLogger("default").log(Level.WARNING, e.getMessage());
        }
        if (!"".equals(output.toString())) {
            Logger.getLogger("default").info("Output: " + output);
        }
        return "";
    }

    private void copy(InputStream in, File dest) throws IOException {
        OutputStream out = new FileOutputStream(dest);
        byte[] buffer = new byte[1024];
        int length;
        //copy the file content in bytes
        while ((length = in.read(buffer)) > 0) {
            out.write(buffer, 0, length);
        }
        dest.setExecutable(true);
        in.close();
        out.close();
    }

}
