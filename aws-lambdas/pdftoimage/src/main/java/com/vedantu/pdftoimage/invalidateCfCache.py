import json
import boto3
import time

def invalidateCFCache(key, context):
    path = []

    path.append(key)

    print(path)
    client = boto3.client('cloudfront')
    invalidation = client.create_invalidation(DistributionId='E2549GUMSADVS',
        InvalidationBatch={
            'Paths': {
                'Quantity': 1,
                'Items': path
        },
        'CallerReference': str(time.time())
    })