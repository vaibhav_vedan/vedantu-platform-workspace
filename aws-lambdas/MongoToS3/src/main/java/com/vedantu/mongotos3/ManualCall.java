/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.mongotos3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.zip.GZIPOutputStream;
import javax.net.ssl.HttpsURLConnection;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author somil
 */
public class ManualCall {

    //static String URL = "https://wave-rtc.vedantu.com";
    static String PLATFORM_URL;
    static String bucketName;

    public static final int QUERY_LIMIT = 5000;
    public static final int QUERY_LIMIT_FOR_SESSIONS = 500;
    public static final int MAX_RECORD_SIZE = 50*1024*1024;//20*1024*1024;

    public static String arn;

    public static String clientName;
    public static String clientId;
    public static String clientSecret;

    public static boolean USE_GZIP = true;

    public static String env;
    public static String defaultNodeServerIpPort;
    private static Logger logger;
    private boolean whiteboardDataUploaded = false;

    //public static void main(String[] argv) {
    public void myHandler(String sessionId, boolean otf) {

        ConfigUtils configUtils = new ConfigUtils();

        PLATFORM_URL = configUtils.getStringValue("platformUrl");
        bucketName = configUtils.getStringValue("bucketName");
        env = "PROD";
        defaultNodeServerIpPort = configUtils.getStringValue("defaultNodeServerIpPort");
        arn = configUtils.getStringValue("arn");

        clientName = "MONGO_TO_S3";
        clientId = configUtils.getStringValue("MONGO_TO_S3.CLIENT.ID");
        clientSecret = configUtils.getStringValue("MONGO_TO_S3.CLIENT.SECRET");

        String log4jConfigFile = "ENV-"
                + env
                + File.separator
                + "log.xml";
        System.setProperty("log4j.configurationFile", log4jConfigFile);
        logger = LogManager.getLogger(MongoToS3.class);
        logger.info("Platform URL " + PLATFORM_URL + ", env " + env);

//            String message = event.getRecords().get(0).getSNS().getMessage();
        //String subject = event.getRecords().get(0).getSNS().getSubject();
//            boolean otf = (new JSONObject(message)).optBoolean("otf", false);
        logger.info("Got request for sessionId " + sessionId + "  otf:" + otf);

        if (sessionId != null) {
            String nodeServer = getNodeServer(sessionId);
            logger.info("Got nodeServerIp " + nodeServer);
            if (nodeServer == null) {
                return;
            }
            whiteboardDataUploaded = false;
            logger.info("value of whiteboardDataUploaded:" + whiteboardDataUploaded);
            boolean wbResponse = getData(nodeServer, sessionId, "whiteboard");
            boolean nonwbResponse = getData(nodeServer, sessionId, "nonwb");
            if (wbResponse && nonwbResponse) {
                logger.info("Successfully uploaded for both, now publishing SNS event for sessionId " + sessionId + " otf:" + otf + "  whiteboardDataUploaded:" + whiteboardDataUploaded);
                setMigrated(nodeServer, sessionId);
                if (!otf) {
                    AmazonSNSAsync snsClient = AmazonSNSAsyncClientBuilder.standard()
                            .withRegion(Regions.AP_SOUTHEAST_1)
                            .build();
                    String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":DATA_TO_S3_DONE_" + env.toUpperCase();
                    String msg = "{\"sessionId\":\"" + sessionId + "\"}";
                    String snsSubject = "SESSION_ENDED";
                    PublishRequest publishRequest = new PublishRequest(topicArn, msg, snsSubject);
                    PublishResult publishResult = snsClient.publish(publishRequest);
                    logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
                } else if (otf && whiteboardDataUploaded) {
                    setWhiteboardDataAdded(sessionId);
                    logger.info("setWhiteboardDataAdded for sessionId:" + sessionId);
                    AmazonSNSAsync snsClient = AmazonSNSAsyncClientBuilder.standard()
                            .withRegion(Regions.AP_SOUTHEAST_1)
                            .build();
                    String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":DATA_TO_S3_DONE_OTF_" + env.toUpperCase();
                    String msg = "{\"sessionId\":\"" + sessionId + "\"}";
                    String snsSubject = "SESSION_ENDED";
                    PublishRequest publishRequest = new PublishRequest(topicArn, msg, snsSubject);
                    PublishResult publishResult = snsClient.publish(publishRequest);
                    logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
                }
            }
        } else {
            logger.error("Error: sessionId null in received SNS event");
        }

    }

    public boolean getData(String nodeServer, String sessionId, String context) {
        try {
            InputStream in;
            URL url;
            HttpsURLConnection con;
            int recordsFetched = 0;
            int totalRecordsFetched = 0;
            StringBuilder sb = new StringBuilder("{\"result\":[");
            do {
                recordsFetched = 0;
                String URL_TO_CONNECT;
                if (context.equals("nonwb")) {
                    URL_TO_CONNECT = "https://" + nodeServer + "/fetchnonwbdata?sessionId=" + sessionId + "&order=ASC&start=" + totalRecordsFetched + "&size=" + QUERY_LIMIT;
                } else {
                    URL_TO_CONNECT = "https://" + nodeServer + "/fetchsessiondata?sessionId=" + sessionId + "&context=" + context + "&order=ASC&start=" + totalRecordsFetched + "&size=" + QUERY_LIMIT;
                }
                url = new URL(URL_TO_CONNECT);
                con = (HttpsURLConnection) url.openConnection();
                con.setRequestProperty("X-Ved-Client", clientName);
                con.setRequestProperty("X-Ved-Client-Id", clientId);
                con.setRequestProperty("X-Ved-Client-Secret", clientSecret);
                in = con.getInputStream();
                String response = getStringFromInputStream(in);
                if (response.length() == 0) {
                    logger.error("Error: Response length is 0 for sessionId " + sessionId + " context " + context);
                    return false;
                }
                JSONObject js = new JSONObject(response);
                String errorCode = js.optString("errorCode", "");
                if (!("".equals(errorCode))) {
                    logger.error("Error: Response contains error with code " + errorCode + " and message " + js.optString("errorMessage", "") + " sessionId " + sessionId + " context " + context);
                    return false;
                } else {
                    JSONArray currentResult = js.optJSONArray("result");
                    if (currentResult == null) {
                        logger.error("Error: result array is null for sessionId " + sessionId + " context " + context);
                        return false;
                    }
                    recordsFetched = currentResult.length();
                    for (int i = 0; i < recordsFetched; i++) {
                        String temp;
                        if (totalRecordsFetched == 0 && i == 0) {
                            temp = currentResult.opt(i).toString();
                        } else {
                            temp = "," + currentResult.opt(i).toString();
                        }
                        sb.append(temp);
                    }
                    totalRecordsFetched += recordsFetched;

                }
                if (sb.length() > MAX_RECORD_SIZE) {
                    logger.error("Error: " + context + " " + sessionId + " Size greater than MAX_RECORD_SIZE, not uploading");
                    return false;
                }

                //logger.info("totalRecords " + totalRecordsFetched);
            } while (recordsFetched != 0);
            sb.append("]}");
            //logger.info("Total records downloaded " + totalRecordsFetched + " for sessionId "+sessionId+ " context "+context);

            if (totalRecordsFetched == 0) {
                logger.info(context + " " + sessionId + " 0 records fetched so not uploading ");
                return true;
            }

            InputStream inToS3;
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("application/json");
            byte[] uncompressedBytes = sb.toString().getBytes();

            if (USE_GZIP) {
                logger.info("For sessionid " + sessionId + " context " + context + " Length before compression " + uncompressedBytes.length);
                metadata.setContentEncoding("gzip");
                byte[] compressesBytes = gzipFile(uncompressedBytes);
                inToS3 = new ByteArrayInputStream(compressesBytes);
                metadata.setContentLength(compressesBytes.length);
                logger.info("For sessionid " + sessionId + " context " + context + " Length after compression " + compressesBytes.length);

            } else {
                inToS3 = new ByteArrayInputStream(uncompressedBytes);
                metadata.setContentLength(uncompressedBytes.length); //change this
            }

            return uploadToS3(sessionId, context, inToS3, metadata);

            //logger.info("String response "+ sb);
        } catch (Exception e) {
            logger.error("Error: Caught exception for sessionId: " + sessionId + " context " + context, e);
            return false;
        }

    }

    public boolean uploadToS3(String sessionId, String context, InputStream inToS3, ObjectMetadata metadata) {
        String keyName = sessionId + "/" + context + ".json";

        //AmazonS3 s3client = new AmazonS3Client(new ProfileCredentialsProvider());
        AmazonS3 s3client = new AmazonS3Client();

        try {
            s3client.putObject(new PutObjectRequest(
                    bucketName, keyName, inToS3, metadata)); //may have to set content type, length
            logger.info("Uploaded to S3 for sessionId " + sessionId + " context " + context);
            if (context.equals("whiteboard")) {
                whiteboardDataUploaded = true;
            }
            return true;
        } catch (AmazonServiceException ase) {
            logger.info("Caught an AmazonServiceException, which "
                    + "means your request made it "
                    + "to Amazon S3, but was rejected with an error response"
                    + " for some reason.");
            logger.info("Error Message:    " + ase.getMessage());
            logger.info("HTTP Status Code: " + ase.getStatusCode());
            logger.info("AWS Error Code:   " + ase.getErrorCode());
            logger.info("Error Type:       " + ase.getErrorType());
            logger.info("Request ID:       " + ase.getRequestId());
            logger.error("Error: Caught exception for sessionId: " + sessionId + " context " + context, ase);
            return false;
        } catch (AmazonClientException ace) {
            logger.info("Caught an AmazonClientException, which "
                    + "means the client encountered "
                    + "an internal error while trying to "
                    + "communicate with S3, "
                    + "such as not being able to access the network.");
            logger.info("Error Message: " + ace.getMessage());
            logger.error("Error: Caught exception for sessionId: " + sessionId + " context " + context, ace);
            return false;
        }
    }

    // convert InputStream to String
    private String getStringFromInputStream(InputStream is) {
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    public byte[] gzipFile(byte[] bytes) throws IOException {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            GZIPOutputStream out = new GZIPOutputStream(baos);
            out.write(bytes, 0, bytes.length);
            // Complete the GZIP file
            out.finish();
            out.close();

            return baos.toByteArray();
        } catch (IOException e) {
            throw e;
        }
    }

    public String getNodeServer(String sessionId) {
        try {
            InputStream in;
            URL url;
            HttpsURLConnection con;
            String URL_TO_CONNECT;
            URL_TO_CONNECT = PLATFORM_URL + "/platform/wave/getSelectedNodeServer?sessionId=" + sessionId;
            //System.out.println("URL to connect "+URL_TO_CONNECT);
            url = new URL(URL_TO_CONNECT);
            con = (HttpsURLConnection) url.openConnection();
            con.setRequestProperty("X-Ved-Client", clientName);
            con.setRequestProperty("X-Ved-Client-Id", clientId);
            con.setRequestProperty("X-Ved-Client-Secret", clientSecret);
            in = con.getInputStream();
            String response = getStringFromInputStream(in);
            if (response.length() == 0) {
                logger.error("Error: Response length for getSelectedNodeServer is 0");
                return defaultNodeServerIpPort;
            }
            JSONArray js = new JSONArray(response);
            if (js.length() == 0) {
                logger.info("Length of Platform response for Node Server is 0: sessionId " + sessionId);
                return defaultNodeServerIpPort;
            }
            JSONObject jsObject = js.getJSONObject(0);
            String serverIp = jsObject.optString("serverIp", null);
            String serverPort = jsObject.optString("serverPort", null);
            if (serverIp == null) {
                logger.error("Error: got serverIp null for sessionId " + sessionId);
                return defaultNodeServerIpPort;
            }
            if (serverPort != null) {
                return serverIp + ":" + serverPort;
            } else {
                return serverIp;
            }

        } catch (Exception e) {
            logger.error("Error: Caught exception for sessionId: " + sessionId, e);
            return defaultNodeServerIpPort;
        }

    }

    public void setWhiteboardDataAdded(String sessionId) {
        try {
            InputStream in;
            URL url;
            HttpsURLConnection con;
            String URL_TO_CONNECT;
            URL_TO_CONNECT = PLATFORM_URL + "/platform/onetofew/session/setWhiteboardDataMigrated/" + sessionId;
            //System.out.println("URL to connect "+URL_TO_CONNECT);
            url = new URL(URL_TO_CONNECT);
            con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("X-Ved-Client", clientName);
            con.setRequestProperty("X-Ved-Client-Id", clientId);
            con.setRequestProperty("X-Ved-Client-Secret", clientSecret);
            in = con.getInputStream();
            String response = getStringFromInputStream(in);
            if (response.length() == 0) {
                logger.error("Error: Response length for setWhiteboardDataAdded is " + response);
            }

        } catch (Exception e) {
            logger.error("Error: Caught exception for sessionId: " + sessionId, e);
        }

    }

    public boolean setMigrated(String nodeServer, String sessionId) {
        try {
            InputStream in;
            URL url;
            HttpsURLConnection con;
            String URL_TO_CONNECT;
            URL_TO_CONNECT = "https://" + nodeServer + "/setsessionmigrated?sessionId=" + sessionId + "&migrated=true";
            url = new URL(URL_TO_CONNECT);
            con = (HttpsURLConnection) url.openConnection();
            con.setRequestProperty("X-Ved-Client", clientName);
            con.setRequestProperty("X-Ved-Client-Id", clientId);
            con.setRequestProperty("X-Ved-Client-Secret", clientSecret);
            in = con.getInputStream();
            String response = getStringFromInputStream(in);
            if (response.length() == 0) {
                logger.error("Error: Response length is 0 for sessionId " + sessionId + " migrated");
                return false;
            }
            JSONObject js = new JSONObject(response);
            String errorCode = js.optString("errorCode", "");
            if (!("".equals(errorCode))) {
                logger.error("Error: Response contains error with code " + errorCode + " and message " + js.optString("errorMessage", "") + " sessionId " + sessionId + " migrated ");
                return false;
            } else {
                JSONObject currentResult = js.optJSONObject("result");
                if (currentResult == null) {
                    logger.error("Error: result object is null for sessionId " + sessionId + " migrated ");
                    return false;
                }
                boolean status = currentResult.optBoolean("success", false);
                if (!status) {
                    logger.error("Error: result success is false for sessionId " + sessionId + " migrated ");
                    return false;
                } else {
                    logger.info("Success is true for sessionId " + sessionId + " migrated ");
                    return true;
                }

            }

        } catch (Exception e) {
            logger.error("Error: Caught exception for sessionId: " + sessionId + " migrated ", e);
            return false;
        }

    }

    public static void main(String[] args) {
        //change conf dir in loadEnvProperties method to String confDir = "ENV-PROD";
        //and env = "PROD" in constructor of ManualCall
        ManualCall manualCall = new ManualCall();
        manualCall.myHandler("4102378452773129", false);

        //after the successfull call, go to vedantuwave and check collection
        //db.sessionids.findOne({sessionId:"4102378218474623"}) to see if the data is migrated,
        //data deleted from mongo and recording prepared
    }
}
