#!/usr/bin/env python2
# -*- coding: utf-8 -*-

#https://github.com/jkehler/awslambda-psycopg2
"""
Created on Mon Feb 12 15:23:51 2018

@author: ajith

copied psycopg2 from https://github.com/jkehler/awslambda-psycopg2 (folder psycopg2 3.7 renamed to psycopg2)
"""

import psycopg2
import boto3
import os
import datetime as dt
from secrets_manager import SecretManager
secrets = SecretManager().secrets

def handler(event, context): 
    # con = psycopg2.connect(dbname='vedantu', host='vedantu.cbalxlkgcxvs.ap-south-1.redshift.amazonaws.com',
    #                        port='5439', user='root', password='nATN8C1z0wHvTZqwo6g')
    # print("got the connection")
    # 
    # cur = con.cursor()
    # print("got the cursor")
    # #
    # tables_to_export = ["courseplan", "public.user", "orders", "transaction"]
    # delete_columns = {
    #     "courseplan": ["endreason"]
    # }
    # 
    # for table in tables_to_export:
    #     # cur.execute("SELECT * FROM public.user where id='5749944599183360';")
    #     cur.execute("Select * FROM " + table + " limit 10")
    #     colnames = [desc[0] for desc in cur.description]
    #     print(colnames)
    #     # data = np.array(cur.fetchall())
    #     # print(data)    
    # 
    #     if table in delete_columns:
    #         for deletecol in delete_columns[table]:
    #             if deletecol in colnames:
    #                 colnames.remove(deletecol)
    # 
    #     finalcolmnsstr = ",".join(colnames)
    #     # adding a separate file for headers
    #     s3 = boto3.client('s3',
    #                       aws_access_key_id="AKIAIDBTVTL66XCOMVLQ",
    #                       aws_secret_access_key="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"
    #                       );
    #     headers_file_name = "/tmp/" + table + "-headers.txt"
    #     text_file = open(headers_file_name, "w")
    #     text_file.write(finalcolmnsstr)
    #     text_file.close()
    #     s3.upload_file(headers_file_name, "vedantu-metabase", table + "-headers.txt")
    #     os.remove(headers_file_name)
    #     print("uploaded headers " + headers_file_name)
    # 
    #     querycolnsstr = '","'.join(colnames)
    #     querycolnsstr = '"' + querycolnsstr + '"';
    #     print("exporting the table " + table + " of columns " + finalcolmnsstr)
    #     # for using where clause do this where id="'5749944599183360'"
    #     cur.execute(
    #         "unload ('SELECT " + querycolnsstr + " FROM " + table + "') to 's3://vedantu-metabase/" + table + ".csv' CREDENTIALS 'aws_access_key_id=AKIAIDBTVTL66XCOMVLQ;aws_secret_access_key=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' DELIMITER AS ',' NULL AS '' PARALLEL OFF allowoverwrite;")
    # 
    # cur.close()
    # con.close()
    # 
    # currenthr = dt.datetime.now().hour;
    
    currenthr = dt.datetime.now().hour
    execstring = "vacuum full"
    print(currenthr)
    if currenthr in [9,12,15]:
        execstring = "vaccuum delete only;"
        print("delete only")
        
    print(execstring)
    
    # return True

    con=psycopg2.connect(dbname= 'vedantu', host=secrets["redshift_host"], 
    port= secrets["redshift_port"], user= secrets["redshift_user"], password= secrets["redshift_password"])
    con.autocommit = True
    print("got the connection for vacuuming")    
    cur = con.cursor()
    cur.execute(execstring)
    print("vaccuum done")
    cur.close() 
    con.close()    


    return True

#handler(True,True)

        

