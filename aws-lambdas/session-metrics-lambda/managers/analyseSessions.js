/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global process, exports, require */

var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var mongo_session = require(process.env["APP_HOME"] + "/modules/mongo.js");
var httpclient = require(process.env["APP_HOME"] + "/modules/httpclient.js");
//var gcloud = require(process.env["APP_HOME"] + "/modules/gcloud.js");
//var insessiondaos = require(process.env["APP_HOME"] + "/daos/insessiondao.js");
var platform = require(process.env["APP_HOME"] + "/modules/platform.js");
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var Q = require("q");
var domain = require("domain");
var logsDomain = configProps.logsSource.domain;
//var gCloudProps = configProps.gCloud;
//constants params
var disconnectionsNormalizableVal = 20;
var offlineTimeNormalizableVal = 1200;
var sessionInactiveTimeNormalizableVal = 1200;
var audioDisconnectionTimeNormalizableVal = 1200;
var audioPoorQualityTimeNormalizableVal = 1200;
var audioDisconnectedTimesNormalizableVal = 20;
//var gcloudDataset = require('gcloud').datastore.dataset({
//    //projectId: "vedantu-fos-devv"
//    projectId: gCloudProps.projectId,
//    keyFilename: process.env["APP_HOME"] + '/keys/' + gCloudProps.keyFilename
//});

//var datastore = require('@google-cloud/datastore');
//var gcloudDataset = datastore({
//    projectId: gCloudProps.projectId,
//    keyFilename: process.env["APP_HOME"] + '/keys/' + gCloudProps.keyFilename
//});

function fromDatastore(obj) {
    obj.data.id = obj.key.path[obj.key.path.length - 1];
    return obj.data;
}
var statsInstance = 5;


var idealRecv = 0;
if (statsInstance == 1) {
    idealRecv = 50;  // it is variable based on the sdp parameters socialvid and our calls can have different sdp parameters
} else if (statsInstance == 5) {
    idealRecv = 250;
}
function getSessionS3Data(sessionId) {
    var deferred = Q.defer();
    if (!sessionId) {
        sessionId = 0;
    }
    sessionId = parseInt(sessionId);
    var sessionData, videoData, chatData, whiteboardData;
    var downloadUrlKey = sessionId + "/nonwb.json";
    var downloadWBUrlKey = sessionId + "/whiteboard.json";
    var getUrl =  logsDomain + "/getsessiondataurl?url=" + downloadUrlKey;
    httpclient.get(getUrl).then(function (resp) {
        if (resp&& resp.result && resp.result.url) {
            httpclient.get(resp.result.url).then(function (resp) {
                if(resp.Error){
                    logger.warn("data not exists noWb sessionId:" + sessionId + " data url:" + JSON.stringify(resp.Error));
                    deferred.resolve({});
                    return;
                }
                var datas = resp.result;
                if (datas) {
                    logger.info("Non Whiteboard data analysing");
                    videoData = [];
                    sessionData = [];
                    chatData = [];
                    //segregation logic here
                    for (var k = 0; k < datas.length; k++) {
                        var data = datas[k];
                        if (data.context == "videochat") {
                            videoData.push(data);
                        } else if (data.context == "session") {
                            sessionData.push(data);
                        } else if (data.context == "chat") {
                            chatData.push(data);
                        }
                    }
                    logger.log("Non Whiteboard data download complelted");
                    getUrl = logsDomain + "/getsessiondataurl?url=" + downloadWBUrlKey;
                    httpclient.get(getUrl).then(function (resp) {
                        if (resp&& resp.result&&resp.result.url) {
                            httpclient.get(resp.result.url).then(function (resp) {
                                var datas = resp.result;
                                if (datas) {
                                    whiteboardData = datas;
                                    logger.log(" whiteboard data downloaded ");
                                    deferred.resolve({sessionData: sessionData, videoData: videoData, chatData: chatData, whiteboardData: whiteboardData});
                                } else {
                                    logger.warn("no whiteboard  data sessionId:" + sessionId + " ");
                                    deferred.resolve({sessionData: sessionData, videoData: videoData, chatData: chatData});
                                }
                            }).error(function (e) {
                                logger.warn("no whiteboard  data sessionId:" + sessionId + " ");
                                deferred.resolve({sessionData: sessionData, videoData: videoData, chatData: chatData});
                            });
                        } else {
                            logger.warn("api url for Whiteboard data sessionId:" + sessionId + " is null");
                            deferred.resolve({sessionData: sessionData, videoData: videoData, chatData: chatData});
                        }
                    });
                } else {
                    logger.warn("data not exists noWb sessionId:" + sessionId + " data url:" + resp.result.url);
                    deferred.resolve({});
                }
            });
        } else {
            logger.warn("api url for noWb sessionId:" + sessionId + " is null  ");
            deferred.resolve({});
        }
    });
    return deferred.promise;
}
;
function getSessionData(sessionId) {
    var deferred = Q.defer();
    if (!sessionId) {
        sessionId = 0;
    }
    sessionId = parseInt(sessionId);
//    var sessionLogsPr = mongo_session.getSessionData(sessionId, "session");
//    var videoLogsPr = mongo_session.getSessionData(sessionId, "videochat");
//    var wbLogsPr = mongo_session.getSessionData(sessionId, "whiteboard", 0, 1200);

//    var sessionLogsPr = httpclient.get(logsDomain + "/fetchsessiondata?context=session&sessionId=" + sessionId);
//    var videoLogsPr = httpclient.get(logsDomain + "/fetchsessiondata?context=videochat&sessionId=" + sessionId);
//    var wbLogsPr = httpclient.get(logsDomain + "/fetchsessiondata?context=whiteboard&sessionId=" + sessionId + "&size=1200");
//    var chatLogsPr = httpclient.get(logsDomain + "/fetchsessiondata?context=chat&sessionId=" + sessionId);
      var s3DataPr = getSessionS3Data(sessionId);
    var sessionInfoPr = platform.getSessionInfo(sessionId);

    var feedbackPr = platform.fetchFeedback(sessionId);

    var reviewPr = platform.getReviews(sessionId);

    var finalPr = Q.all([s3DataPr,
        sessionInfoPr, feedbackPr, reviewPr]);
    finalPr.then(function (resultsArray) {
        if (!resultsArray[1].errorCode) {
            deferred.resolve({
                sessionLogs: resultsArray[0].sessionData, videoLogs: resultsArray[0].videoData,
                wbLogs: resultsArray[0].whiteboardData, chatLogs: resultsArray[0].chatData,
                sessionInfo: resultsArray[1], feedbackInfo: resultsArray[2],
                reviews: resultsArray[3].list
            });
        } else {
            deferred.resolve(null);
        }
    }).catch(function (err) {
        logger.error("error getting session data for analysis sessionId:  "+sessionId,err);
        deferred.resolve(null);
    });
    return deferred.promise;
}
function analyse(sessionId) {
    var deferred = Q.defer();
//    var analysisDomain = domain.create();
//    analysisDomain.on('error', function (er) {
//        deferred.resolve({
//            errorCode: "SERVICE_ERROR",
//            errorMessage: er
//        });
//        logger.error('Caught error!', er);
//    });
//
//    analysisDomain.run(function () {
//
//    });

    logger.log("Fetching session data for sessionid " + sessionId);
    getSessionData(sessionId).then(function (data) {
        if (!data || !data.sessionLogs || !data.videoLogs || !data.sessionInfo) {
            logger.warn("Error in fetching data of session " + sessionId + ", aborting analysis");
            deferred.resolve({
                errorCode: "NOT_DATA",
                errorMessage: "Data not available for analysis"
            });
            return;
        }
        
        var results = goThroughData(data);
        getExotelCallData(sessionId).then(function (callDetails) {
             logger.info("The Exotel results of session Id are " + JSON.stringify(callDetails));
             for (var i in results){
                 results[i].exotelCallDuration=callDetails.duration;
                 results[i].exotelCallCount=callDetails.count;
                 results[i].exotelCallDetails=callDetails.details;
             }
            fillISPDetails(results).then(function (finalResults) {
                logger.info("The results of session Id are " + JSON.stringify(finalResults));
                deferred.resolve(finalResults);
            });
        });
        //fetching isp data
        
    }).catch(function (err) {
        //this will mainly catch errors from goThroughData(data), 
        //another promise inside callback has its own 
        //catch block and other lines won't throw any error
        logger.warn(err);
        deferred.resolve({
            errorCode: "SERVICE_ERROR",
            errorMessage: err
        });
    });


    return deferred.promise;
}
function goThroughData(data) {
    var sessionInfo = data.sessionInfo;
    var feedbacks = [];
    if (data.feedbackInfo && data.feedbackInfo.data) {
        feedbacks = data.feedbackInfo.data;
    }
    var r = calculateDataPoints(sessionInfo, data.sessionLogs, data.videoLogs,
            data.wbLogs, data.chatLogs, feedbacks, data.reviews);
    return r;
}
function calculateDataPoints(sessionInfo, sessionLogs, videoLogs, wbLogs, chatLogs, feedbacks, reviews) {
    //checking if analysable
    var analysable = true;
    var notJoined = false;
    for (var i in sessionInfo.attendees) {
        if (sessionInfo.attendees[i].userState === "NOT_JOINED") {
            notJoined = true;
        }
    }
    if (notJoined || sessionInfo.state === "CANCELED" ||
            sessionInfo.state === "EXPIRED" || sessionInfo.displayState === "EXPIRED") {
        analysable = false;
    }

    var attendees = sessionInfo.attendees;
    var sessionId = sessionInfo.id+"";
    var resultsObj = {};
    for (var k = 0; k < attendees.length; k++) {
        var loopItem = attendees[k];
        var _userId = attendees[k].userId;

        var insertable = {
            sessionId: sessionId,
            sessionType: sessionInfo.type,
            userId: _userId,
            role: loopItem.role,
            name: loopItem.fullName,
            email: loopItem.email,
            firstName: loopItem.firstName,
            lastName: loopItem.lastName,
            state: sessionInfo.state,
            displayState: sessionInfo.displayState,
            subject: sessionInfo.subject,
            topic: sessionInfo.topic,
            billingPeriod: loopItem.billingPeriod,
            teacherRating: null,
            techRating: null,
            techRatingReason: "",
            sessionErrors: {"DENIED_PERMISSION_FOR_HARDWARE": 0, "CREATE_PEER_CONNECTION_ERROR": 0, "ERROR_CREATE_OFFER": 0,
                "ERROR_CREATE_ANSWER": 0, "SDP_ERROR_ON_OFFER_RECEIVED": 0,
                "SDP_ERROR_ON_ANSWER_RECEIVED": 0, "ICE_CONNECTION_STATE_FAILED": 0},
            publicIPs: [],
            ispDetails: [],
            callServices: [],
            totalCallDuration: 0,
            totalScreenShareDuration: 0,
            socketDisconnectedAudioActive: 0,
            browserName: "",
            browserVersion: "",
            multipleConnectionCount: 0,
            reconnectionTimeReq: {"0-2": 0, "2-5": 0, "5-10": 0, "10-": 0},
            disconnectionMap: {"ON_PAGE_LEAVE": 0, "ping timeout": 0, "transport close": 0, "transport error": 0, "client namespace disconnect": 0},
            offlineTime: 0,
            offlineTimeNormalized: 0, //in seconds, normalized to 20mins/1200secs
            disconnections: 0,
            disconnectionsNormalized: 0, //normalized to 20 max
            totalSessionInactiveTime: 0, //when no session could go on
            totalSessionInactiveTimeNormalized: 0, //in seconds, normalized to 20mins/1200secs
            audioDisconnectedTimes: 0,
            audioDisconnectedTimesNormalized: 0, //normalized to 20 max
            audioDisconnectionTime: 0, //take the max of teacher and student for session 
            audioDisconnectionTimeNormalized: 0, //in seconds, normalized to 20mins/1200secs
            audioDisconnectedBuckets: {"0-5": 0, "5-10": 0, "10-15": 0, "15-": 0},
            highPacketLossTime: 0,
            highPacketLossTimeNormalized: 0,
            poorAudioQualityTime: 0, //seconds
            poorAudioQualityTimeNormalized: 0,
            avgRtt: 0,
            avgJitter: 0,
            avgPacketLoss: 0,
            poorAudioQualityList: [],
            sessionOnMobile: false,
            timeOnMobile: 0,
            refreshCount: 0,
            phoneCallCount: 0,
            callsCount: 0,
            callsCountNormalized: 0,
            audioCallTries: 0,
            videoCallTries: 0,
            callRequestDeclined: 0,
            iceCandidates: {srflx: 0, relay: 0, host: 0, total: 0},
            timeTakenInitiateFirstCall: 0,
            wbActivityFound: false,
            analysable: analysable,
            whiteboardPauses: 0,
            textchatCount: 0,
            turnStats: {TURN_NONTURN: 0, TURN_TURN: 0, NONTURN_NONTURN: 0},
            turnServerUsed: "",
            mobileData: {},
            startTimeStr: "",
            speedTestStartTime: 0,
            speedTestDuration: 0,
            speedTestFileCount: 0,
            downloadTest: {speed: 0, pass: 0, status: 0},
            uploadTest: {speed: 0, pass: 0, status: 0},
            callsDetails: []
        };
        if (sessionInfo.startedAt) {
            insertable.startTime = sessionInfo.startedAt;
        } else {
            insertable.startTime = sessionInfo.startTime;
        }
        try {
            var tempDate = new Date(parseInt(insertable.startTime));
            insertable.startTimeStr = tempDate.getDate() + '/' + (tempDate.getMonth() + 1) + '/' + tempDate.getFullYear();
        } catch (r) {
            logger.error("error in converting starttime to str sessionId:"+sessionId,r);
        }
        if (loopItem.joinTime) {
            insertable.joinTime = loopItem.joinTime;
            insertable.joinDelay = parseInt((loopItem.joinTime - sessionInfo.startTime) / 1000);
        }
        if (wbLogs && wbLogs.length > 1000) {
            insertable.wbActivityFound = true;
        }

        if (chatLogs) {
            insertable.textchatCount = chatLogs.length;
        }

        if (sessionInfo.endedAt && sessionInfo.endedAt < sessionInfo.endTime) {
            insertable.endTime = sessionInfo.endedAt;
        } else {
            insertable.endTime = sessionInfo.endTime;
        }
        if (reviews.length > 0 && reviews[0].rating) {
            insertable.teacherRating = parseInt(reviews[0].rating);
        }

        //small hack assuming only two participants
        if (k === 0) {
            insertable.otherUserEmail = attendees[1].email;
            insertable.otherUserId = attendees[1].userId;
        } else if (k === 1) {
            insertable.otherUserEmail = attendees[0].email;
            insertable.otherUserId = attendees[0].userId;
        }

        resultsObj[_userId] = insertable;
    }


    for (var key in feedbacks) {
        var _obj = feedbacks[key], _userId = _obj.sender.userId;
        resultsObj[_userId].techRating = parseInt(_obj.rating);
        if (_obj.reason && _obj.reason.length > 0) {
            resultsObj[_userId].techRatingReason = _obj.reason[0];
        }
    }

    if (!analysable) {
        return resultsObj;
    }

    var logsTextData = [];

    //analysing video logs
    analyseVideoChatLogs(videoLogs, resultsObj, logsTextData);

    //analysing session logs
    analyseSessionLogs(sessionLogs, resultsObj, logsTextData);

    //analysing the logstext data
    analyseLogsTextData(resultsObj, logsTextData);
    return resultsObj;
}
var analyseVideoChatLogs = function (videoLogs, resultsObj, logsTextData) {
    if (!videoLogs) {
        return;
    }
    var callConnectionTimes = {};
    for (var k = 0; k < videoLogs.length; k++) {
        var logItem = videoLogs[k];
        var sourceUserId = logItem.userId;
        var pushableUserObj = resultsObj[sourceUserId];
        if (!pushableUserObj) {
            logger.debug("This video log is by an actor who is not part of the session\n\
                attendees, user id " + sourceUserId);
            continue;
        }
        if (logItem.type === "AV_CONNECTION_LOGS") {
            var logItemData = JSON.parse(logItem.data);
            for (var i = 0; i < logItemData.length; i++) {
                var miniLogItem = logItemData[i];
                var action = miniLogItem.action;
                switch (action) {
                    case "dpfls":
                        pushableUserObj.sessionErrors["DENIED_PERMISSION_FOR_HARDWARE"]++;
                        break;
                    case "tfls":
                        pushableUserObj.sessionErrors["DENIED_PERMISSION_FOR_HARDWARE"]++;
                        break;
                    case "sac":
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_REQUESTED",
                            data: {video: false},
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "svc":
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_REQUESTED",
                            data: {video: true},
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "ttfac":
                        var data = miniLogItem.details;
                        if (data) {
                            if (!callConnectionTimes[sourceUserId]) {
                                callConnectionTimes[sourceUserId] = {total: 0, count: 0};
                            }
                            callConnectionTimes[sourceUserId].total += parseInt((data.time || data.timeInSecs));
                            callConnectionTimes[sourceUserId].count++;
                        }
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_STARTED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "cpce":
                        pushableUserObj.sessionErrors["CREATE_PEER_CONNECTION_ERROR"]++;
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_ENDED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "ssisc":
                        logsTextDataPusher({
                            type: "SCREEN_SHARE_STARTED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "ssss":
                        logsTextDataPusher({
                            type: "SCREEN_SHARE_ENDED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "eco":
                        pushableUserObj.sessionErrors["ERROR_CREATE_OFFER"]++;
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_ENDED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "eca":
                        pushableUserObj.sessionErrors["ERROR_CREATE_ANSWER"]++;
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_ENDED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "seoor":
                        pushableUserObj.sessionErrors["SDP_ERROR_ON_OFFER_RECEIVED"]++;
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_ENDED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "seoar":
                        pushableUserObj.sessionErrors["SDP_ERROR_ON_ANSWER_RECEIVED"]++;
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_ENDED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "icsf":
                        pushableUserObj.sessionErrors["ICE_CONNECTION_STATE_FAILED"]++;
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_ENDED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "icsd":
                        pushableUserObj.audioDisconnectedTimes++;
                        logsTextDataPusher({
                            type: "ICE_DISCONNECTED",
                            userId: sourceUserId,
                            serverTime: miniLogItem.time
                        }, logsTextData);
                        break;
                    case "BROWSER_INFO":
                        pushableUserObj.browserName = miniLogItem.details.name;
                        pushableUserObj.browserVersion = miniLogItem.details.version;
                        break;
                }
            }
        } else if (logItem.type === "AV_CHAT_RTC_STATS") {
            //TODO sort before analysing data
            var logItemData = JSON.parse(logItem.data);
            //logger.debug(logItemData);
            var prevPacketLost = 0, prevPacketLostTime;
            var packetReceived;
            for (var i = 0; i < logItemData.length; i++) {
                var rtcDataObj = logItemData[i];
                packetReceived = parseInt(rtcDataObj.packetRecv);
                // everything can be pushed as a single oject and multiple things can be added as data.
                if (!isNaN(packetReceived)) {
                    logsTextDataPusher({
                        type: "PACKET_RECV",
                        data: {recv: packetReceived},
                        userId: pushableUserObj.userId,
                        serverTime: rtcDataObj.time
                    }, logsTextData);
                }
                logsTextDataPusher({
                    type: "AUDIO_BRR",
                    data: {audiobrr: parseInt(rtcDataObj.audiobrr / 1024)},
                    userId: pushableUserObj.userId,
                    serverTime: rtcDataObj.time
                }, logsTextData);
                var packetLost = parseInt(rtcDataObj.packetLost);
                if (prevPacketLost && packetLost &&
                        ((rtcDataObj.time - prevPacketLostTime) < 10000)) {
                    logsTextDataPusher({
                        type: "PACKET_LOSS",
                        data: {loss: (packetLost - prevPacketLost)},
                        userId: pushableUserObj.userId,
                        serverTime: rtcDataObj.time
                    }, logsTextData);
                }
                if (!isNaN(packetLost)) {
                    logsTextDataPusher({
                        type: "PACKET_LOST",
                        data: {lost: (packetLost - prevPacketLost)},
                        userId: pushableUserObj.userId,
                        serverTime: rtcDataObj.time
                    }, logsTextData);
                }
                prevPacketLost = packetLost;
                prevPacketLostTime = rtcDataObj.time;

                logsTextDataPusher({
                    type: "RTT",
                    data: {rtt: parseInt(rtcDataObj.rtt)},
                    userId: pushableUserObj.userId,
                    serverTime: rtcDataObj.time
                }, logsTextData);

                logsTextDataPusher({
                    type: "JITTER",
                    data: {jitter: parseInt(rtcDataObj.jitter)},
                    userId: pushableUserObj.userId,
                    serverTime: rtcDataObj.time
                }, logsTextData);

                logsTextDataPusher({
                    type: "JITTER_BUFFER",
                    data: {jitterBuffer: parseInt(rtcDataObj.jitterBuffer)},
                    userId: pushableUserObj.userId,
                    serverTime: rtcDataObj.time
                }, logsTextData);

                //turn server analysis
                var lc = rtcDataObj.localCandidateType;
                var rc = rtcDataObj.remoteCandidateType;
                if (lc && rc) {
                    if ((lc === "relay" || lc === "relayed") && (rc === "relay" || rc === "relayed")) {
                        pushableUserObj.turnStats["TURN_TURN"] = 1;
                    } else if (((lc !== "relay" && lc !== "relayed") && (rc === "relay" || rc === "relayed"))
                            ||
                            (lc === "relay" || lc === "relayed") && (rc !== "relay" && rc !== "relayed")) {
                        pushableUserObj.turnStats["TURN_NONTURN"] = 1;
                    } else if ((lc !== "relay" && lc !== "relayed") && (rc !== "relay" && rc !== "relayed")) {
                        pushableUserObj.turnStats["NONTURN_NONTURN"] = 1;
                    }
                    if (lc === "relay" || lc === "relayed") {
                        pushableUserObj.turnServerUsed = rtcDataObj.localAddress.substring(0, rtcDataObj.localAddress.indexOf(':'));
                    }
                }
            }
        } else {
            switch (logItem.type) {
                case "CANDIDATE":
                    var ip = extractPublicIP(logItem);
                    if (ip) {
                        pushIfNotFound(pushableUserObj.publicIPs, ip);
                    }
                    countICECandidates(logItem, pushableUserObj);
                    break;
                case "REQUEST_AUDIO_CALL":
                    logsTextDataPusher(logItem, logsTextData);
                    pushableUserObj.audioCallTries++;
                    logsTextDataPusher({
                            type: "WEBRTC_CALL_REQUESTED",
                            data: {video: false},
                            userId: sourceUserId,
                            serverTime: logItem.serverTime
                        }, logsTextData);
                    break;
                case "REQUEST_VIDEO_CALL":
                    logsTextDataPusher(logItem, logsTextData);
                    pushableUserObj.videoCallTries++;
                    logsTextDataPusher({
                            type: "WEBRTC_CALL_REQUESTED",
                            data: {video: true},
                            userId: sourceUserId,
                            serverTime: logItem.serverTime
                        }, logsTextData);
                    break;
                case "REQUEST_DECLINED":
                    pushableUserObj.callRequestDeclined++;
                    break;
                case "PREPARE_UI_FOR_CHAT":
                    logsTextDataPusher(logItem, logsTextData);
                    break;
                case "ACCEPTED_AUDIO_CALL":
                    logsTextDataPusher({
                        type: "WEBRTC_CALL_STARTED",
                        userId: sourceUserId,
                        serverTime: logItem.serverTime
                    }, logsTextData);
                    break;
                case "ACCEPTED_VIDEO_CALL":
                    logsTextDataPusher({
                        type: "WEBRTC_CALL_STARTED",
                        userId: sourceUserId,
                        serverTime: logItem.serverTime
                    }, logsTextData);
                    break;
                case "END_CALL":
                    logsTextDataPusher(logItem, logsTextData);
                    logsTextDataPusher({
                        type: "WEBRTC_CALL_ENDED",
                        userId: sourceUserId,
                        serverTime: logItem.serverTime
                    }, logsTextData);
                    break;
            }
        }
    }
    for (var _userId in callConnectionTimes) {
        if (resultsObj[_userId]) {
            resultsObj[_userId].avgCallConnectionTime = parseInt(callConnectionTimes[_userId].total / callConnectionTimes[_userId].count);
            var c = callConnectionTimes[_userId].count;
            resultsObj[_userId].callsCount = c;
        }
    }

    //normalizations
    var totalICEFailures = 0;
    var callsCount = 0;
    for (var key in resultsObj) {
        var _tempy = resultsObj[key];
        _tempy.audioDisconnectedTimesNormalized = _tempy.audioDisconnectedTimes;
        if (_tempy.audioDisconnectedTimes > audioDisconnectedTimesNormalizableVal) {
            _tempy.audioDisconnectedTimesNormalized = audioDisconnectedTimesNormalizableVal;
        }

        var _iceFailures = _tempy.sessionErrors.ICE_CONNECTION_STATE_FAILED;
        if (_iceFailures > totalICEFailures) {
            totalICEFailures = _iceFailures;
        }

        var _callsCount = _tempy.callsCount;
        if (_callsCount > callsCount) {
            callsCount = _callsCount;
        }
    }
    //sometimes ice failure data, calls connection data is uploaded only from one side, so taking
    //the maximum possible values for both
    for (var key in resultsObj) {
        resultsObj[key].sessionErrors.ICE_CONNECTION_STATE_FAILED = totalICEFailures;
        resultsObj[key].callsCount = resultsObj[key].callsCountNormalized = callsCount;
        if (callsCount > 20) {
            resultsObj[key].callsCountNormalized = 20;
        }
    }
};
var allowedSessionActions = ["DISCONNECT", "ON_PAGE_LEAVE", "JOIN", "CALL_INITIATED", "END_SESSION", "SESSION_LOGS","CONNECTED"];
var analyseSessionLogs = function (sessionLogs, resultsObj, logsTextData) {
    if (sessionLogs) {
        var speedsRecorder = {};
        for (var k = 0; k < sessionLogs.length; k++) {
            var logItem = sessionLogs[k];
            var actorId = logItem.userId;
            if (allowedSessionActions.indexOf(logItem.type) > -1) {
                if (logItem.type !== "SESSION_LOGS") {
                    logsTextDataPusher(logItem, logsTextData);
                    if(logItem.type=="END_SESSION" ||logItem.type== "ON_PAGE_LEAVE"){
                        logsTextDataPusher({
                            type: "WEBRTC_CALL_ENDED",
                            userId: logItem.userId,
                            serverTime: logItem.serverTime
                        }, logsTextData);
                    }
                } else {
                    if (!speedsRecorder[actorId]) {
                        speedsRecorder[actorId] = {sumUploadSpeeds: 0, sumDownloadSpeeds: 0,
                            countUploadSpeeds: 0, countDownloadSpeeds: 0};
                    }
                    var logItemData = JSON.parse(logItem.data);
                    for (var i = 0; i < logItemData.length; i++) {
                        var rtcDataObj = logItemData[i];
                        var rtcDataObjType = rtcDataObj.type;
                        if (rtcDataObjType === "str") {
                            var speeds = rtcDataObj.data || {};
                            if (speeds.DownloadSpeed) {
                                speedsRecorder[actorId].sumDownloadSpeeds += parseInt(speeds.DownloadSpeed);
                                speedsRecorder[actorId].countDownloadSpeeds++;
                            }
                            if (speeds.UploadSpeed) {
                                speedsRecorder[actorId].sumUploadSpeeds += parseInt(speeds.UploadSpeed);
                                speedsRecorder[actorId].countUploadSpeeds++;
                            }
                        } else if (rtcDataObjType === "wcwpd") {
                            var _otherUserId = resultsObj[actorId].otherUserId;
//                            resultsObj[_otherUserId].whiteboardPauses++;
                            //this will decide if we have to use socket disconnections/whiteboard pauses 
                            //for calculating disconnectiontime, no need to store this in DB
                            resultsObj[_otherUserId].swallowingPauses = true;
                            logsTextDataPusher({
                                type: "WB_PAUSED",
                                userId: _otherUserId,
                                serverTime: rtcDataObj.time
                            }, logsTextData);
                        } else if (rtcDataObjType === "wcwr") {
                            var _otherUserId = resultsObj[actorId].otherUserId;
//                            resultsObj[_otherUserId].whiteboardPauses++;
                            //this will decide if we have to use socket disconnections/whiteboard pauses 
                            //for calculating disconnectiontime, no need to store this in DB
                            resultsObj[_otherUserId].swallowingPauses = true;
                            logsTextDataPusher({
                                type: "WB_RESUMED",
                                userId: _otherUserId,
                                serverTime: rtcDataObj.time
                            }, logsTextData);
                        } else if (rtcDataObjType === "SPEED_TEST_RESULT") {
                            var resultObj = resultsObj[actorId];
                            var speedTestData = JSON.parse(rtcDataObj.data);
                            resultObj.speedTestStartTime = speedTestData.startTime;
                            resultObj.speedTestDuration = speedTestData.duration;
                            resultObj.speedTestFileCount = speedTestData.fileCount;
                            resultObj.downloadTest = speedTestData.download;
                            resultObj.uploadTest = speedTestData.upload;
                        }
                    }
                }
            }
        }
        for (var _userId in speedsRecorder) {
            if (resultsObj[_userId]) {
                var s = parseInt(speedsRecorder[_userId].sumDownloadSpeeds / speedsRecorder[_userId].countDownloadSpeeds);
                if (!isNaN(s)) {
                    resultsObj[_userId].avgDownloadSpeedAfterDisconnect = s;
                }
                s = parseInt(speedsRecorder[_userId].sumUploadSpeeds / speedsRecorder[_userId].countUploadSpeeds);
                if (!isNaN(s)) {
                    resultsObj[_userId].avgUploadSpeedAfterDisconnect = s;
                }
            }
        }
    }
};
var analyseLogsTextData = function (resultsObj, logsTextData) {
    logsTextData.sort(sorter);
    var endedSession = false;
    var timelineNoter = {};
    var firstTimeBothJoinedTime = 0;
    var avCallInitiatedAt = 0;
    var timeTakenInitiateFirstCall = 0;
    var oneParticipantDisconnectedAt = 0, sessionInactiveTime = 0;
    var disconnectKey = "DISCONNECT";
    var teacherParams = {
        userId: 0,
        packetRecvAnalyser: false,
        packetLostAnalyser: false,
        jitterBufferAnalyser: false,
        rttAnalyser: false,
        packetRecvStartTime: 0,
        packetRecvEndTime: 0,
        packetLostStartTime: 0,
        packetLostEndTime: 0,
        jitterBufferStartTime: 0,
        jitterBufferEndTime: 0,
        rttStartTime: 0,
        rttEndTime: 0,
        lastAnalysed: 0,
        totalBadTime: 0,
        totalRtt: 0,
        totalJitter: 0,
        totalPacketLost: 0,
        totalCount: 0
    };
    var studentParams = {
        userId: 0,
        packetRecvAnalyser: false,
        packetLostAnalyser: false,
        jitterBufferAnalyser: false,
        rttAnalyser: false,
        packetRecvStartTime: 0,
        packetRecvEndTime: 0,
        packetLostStartTime: 0,
        packetLostEndTime: 0,
        jitterBufferStartTime: 0,
        jitterBufferEndTime: 0,
        rttStartTime: 0,
        rttEndTime: 0,
        lastAnalysed: 0,
        totalBadTime: 0,
        totalRtt: 0,
        totalJitter: 0,
        totalPacketLost: 0,
        totalCount: 0
    };
    var callEnded = false;
    for (var key in resultsObj) {
        if (resultsObj[key].swallowingPauses) {
            disconnectKey = "WB_PAUSED";
        }
        if (resultsObj[key].role == "STUDENT") {
            studentParams.userId = resultsObj[key].userId;
        } else if (resultsObj[key].role == "TEACHER") {
            teacherParams.userId = resultsObj[key].userId;
        }
    }

    var callAcceptTimes = [];

    for (var k = 0; k < logsTextData.length; k++) {
        var obj = logsTextData[k];
        var actorId = obj.userId;
        if (!timelineNoter[actorId]) {
            timelineNoter[actorId] = {currentlyJoined: false, disconnectedAt: 0, iceDisconnectedAt: 0};
        }
        var timelineNoterObj = timelineNoter[actorId];
        if (!timelineNoterObj.joinedsockets) {
            timelineNoterObj.joinedsockets = [];
        }
        var pushableUserObj = resultsObj[actorId];
        if (!pushableUserObj) {
            logger.debug("This logsTextData item is by an actor who is not part of the session\n\
                attendees, user id " + actorId);
            continue;
        }

        if (logsTextData.length - 1 === k) {
            if (studentParams.totalCount > 0) {
                resultsObj[studentParams.userId].avgRtt = parseInt(studentParams.totalRtt / studentParams.totalCount);
                resultsObj[studentParams.userId].avgPacketLoss = parseInt(studentParams.totalPacketLost / studentParams.totalCount);
                resultsObj[studentParams.userId].avgJitter = parseInt(studentParams.totalJitter / studentParams.totalCount);
            }
            if (teacherParams.totalCount > 0) {
                resultsObj[teacherParams.userId].avgRtt = parseInt(teacherParams.totalRtt / teacherParams.totalCount);
                resultsObj[teacherParams.userId].avgPacketLoss = parseInt(teacherParams.totalPacketLost / teacherParams.totalCount);
                resultsObj[teacherParams.userId].avgJitter = parseInt(teacherParams.totalJitter / teacherParams.totalCount);
            }
        }//logger.debug("-------------->",obj);
        if ((obj.name == "DISCONNECT" && (obj.name == "ON_PAGE_LEAVE" || obj.data.reason == "client namespace disconnect")) || obj.name == "END_CALL") {
            callEnded = true;
        } else if (obj.name == "CALL_INITIATED") {
            callEnded = false;
        }
        // ideal cases are hard coded which can be changed
        if (obj.userId == studentParams.userId) {
            if (obj.name == "PACKET_RECV" || callEnded) {
                if (statsInstance === 1) {
                    if (obj.data.recv && obj.data.recv < 35) { // what if the data is recieved 1 sec late from the browser
                        if (!studentParams.packetRecvAnalyser) {
                            studentParams.packetRecvAnalyser = true;
                            studentParams.packetRecvStartTime = obj.time;
                        }
                    } else {
                        if (studentParams.packetRecvAnalyser || (studentParams.packetRecvAnalyser && callEnded)) {
                            studentParams.packetRecvAnalyser = false;
                            studentParams.packetRecvEndTime = obj.time;
                            if (studentParams.packetRecvEndTime - studentParams.packetRecvStartTime >= 3000) {
                                pushableUserObj.poorAudioQualityList.push({from: studentParams.packetRecvStartTime, to: studentParams.packetRecvEndTime, reason: "PACKET_RECV"});
                                analyseTime(studentParams, studentParams.packetRecvStartTime, studentParams.packetRecvEndTime, studentParams.lastAnalysed);
                                studentParams.lastAnalysed = studentParams.packetRecvEndTime;
                            }
                        }
                    }
                } else if (statsInstance === 5) {
                    if (obj.data.recv && obj.data.recv < 180) {

                        if (!studentParams.packetRecvAnalyser) {
                            studentParams.packetRecvAnalyser = true;
                            studentParams.packetRecvStartTime = obj.time;
                        }
                    } else {
                        if (studentParams.packetRecvAnalyser || (studentParams.packetRecvAnalyser && callEnded)) {
                            studentParams.packetRecvAnalyser = false;
                            studentParams.packetRecvEndTime = obj.time;
                            pushableUserObj.poorAudioQualityList.push({from: studentParams.packetRecvStartTime, to: studentParams.packetRecvEndTime, reason: "PACKET_RECV"});
                            analyseTime(studentParams, studentParams.packetRecvStartTime, studentParams.packetRecvEndTime, studentParams.lastAnalysed);
                            studentParams.lastAnalysed = studentParams.packetRecvEndTime;
                        }
                    }
                }
            }
            if (obj.name == "PACKET_LOST" || callEnded) {
                if (obj.data.lost) {
                    studentParams.totalPacketLost += (obj.data.lost / idealRecv) * 100;
                }
                if ((obj.data.lost / idealRecv) * 100 >= 10) {
                    if (!studentParams.packetLostAnalyser) {
                        studentParams.packetLostAnalyser = true;
                        studentParams.packetLostStartTime = obj.time;
                    }
                } else {
                    if (studentParams.packetLostAnalyser || (studentParams.packetLostAnalyser && callEnded)) {
                        studentParams.packetLostAnalyser = false;
                        studentParams.packetLostEndTime = obj.time;
                        if (statsInstance === 1 && (studentParams.packetLostStartTime - studentParams.packetLostEndTime >= 3000)) {
                            pushableUserObj.poorAudioQualityList.push({from: studentParams.packetLostStartTime, to: studentParams.packetLostEndTime, reason: "PACKETS_LOST"});
                            analyseTime(studentParams, studentParams.packetLostStartTime, studentParams.packetLostEndTime, studentParams.lastAnalysed);
                            studentParams.lastAnalysed = studentParams.packetLostEndTime;
                        } else if (statsInstance === 5) {
                            pushableUserObj.poorAudioQualityList.push({from: studentParams.packetLostStartTime, to: studentParams.packetLostEndTime, reason: "PACKETS_LOST"});
                            analyseTime(studentParams, studentParams.packetLostStartTime, studentParams.packetLostEndTime, studentParams.lastAnalysed);
                            studentParams.lastAnalysed = studentParams.packetLostEndTime;
                        }
                    }
                }

            }
            if (obj.name == "RTT") {
                if (obj.data.rtt) {
                    studentParams.totalCount++;
                    studentParams.totalRtt += obj.data.rtt;
                }
            }
            if (obj.name == "JITTER") {
                if (obj.data.jitter) {
                    studentParams.totalJitter += obj.data.jitter;
                }
            }
            /*if (obj.name == "RTT" || callEnded) {
             if (obj.data.rtt && obj.data.rtt >= 400) {
             if (!studentParams.rttAnalyser) {
             studentParams.rttAnalyser = true;
             studentParams.rttStartTime = obj.time;
             }
             } else {
             if (studentParams.rttAnalyser || (studentParams.rttAnalyser && callEnded)) {
             studentParams.rttAnalyser = false;
             studentParams.rttEndTime = obj.time;
             if (statsInstance === 1 && (studentParams.rttStartTime - studentParams.rttEndTime >= 3000)) {
             pushableUserObj.poorAudioQualityList.push({from: studentParams.rttStartTime, to: studentParams.rttEndTime, reason: "RTT"});
             analyseTime(studentParams, studentParams.rttStartTime, studentParams.rttEndTime, studentParams.lastAnalysed);
             studentParams.lastAnalysed = studentParams.rttEndTime;
             } else if (statsInstance === 5) {
             pushableUserObj.poorAudioQualityList.push({from: studentParams.rttStartTime, to: studentParams.rttEndTime, reason: "RTT"});
             analyseTime(studentParams, studentParams.rttStartTime, studentParams.rttEndTime, studentParams.lastAnalysed);
             studentParams.lastAnalysed = studentParams.rttEndTime;
             }
             }
             }
             }*/

            if (obj.name == "JITTER_BUFFER" || callEnded) {
                if (obj.data.jitterBuffer && obj.data.jitterBuffer >= 400) {
                    if (!studentParams.jitterBufferAnalyser) {
                        studentParams.jitterBufferAnalyser = true;
                        studentParams.jitterBufferStartTime = obj.time;
                    }
                } else {
                    if (studentParams.jitterBufferAnalyser || (studentParams.jitterBufferAnalyser && callEnded)) {
                        studentParams.jitterBufferAnalyser = false;
                        studentParams.jitterBufferEndTime = obj.time;
                        if (statsInstance === 1 && (studentParams.jitterBufferStartTime - studentParams.jitterBufferEndTime >= 3000)) {
                            pushableUserObj.poorAudioQualityList.push({from: studentParams.jitterBufferStartTime, to: studentParams.jitterBufferEndTime, reason: "JITTER_BUFFER"});
                            analyseTime(studentParams, studentParams.jitterBufferStartTime, studentParams.jitterBufferEndTime, studentParams.lastAnalysed);
                            studentParams.lastAnalysed = studentParams.jitterBufferEndTime;
                        } else if (statsInstance === 5) {
                            pushableUserObj.poorAudioQualityList.push({from: studentParams.jitterBufferStartTime, to: studentParams.jitterBufferEndTime, reason: "JITTER_BUFFER"});
                            analyseTime(studentParams, studentParams.jitterBufferStartTime, studentParams.jitterBufferEndTime, studentParams.lastAnalysed);
                            studentParams.lastAnalysed = studentParams.jitterBufferEndTime;
                        }
                    }
                }
            }
        } else if (teacherParams.userId == obj.userId) {
            if (obj.name == "PACKET_RECV" || callEnded) {
                if (statsInstance === 1) {
                    if (obj.data.recv < 35) {
                        if (!teacherParams.packetRecvAnalyser) {
                            teacherParams.packetRecvAnalyser = true;
                            teacherParams.packetRecvStartTime = obj.time;
                        }
                    } else {
                        if (teacherParams.packetRecvAnalyser || (teacherParams.packetRecvAnalyser && callEnded)) {
                            teacherParams.packetRecvAnalyser = false;
                            teacherParams.packetRecvEndTime = obj.time;
                            if (teacherParams.packetRecvEndTime - teacherParams.packetRecvStartTime >= 3000) {
                                pushableUserObj.poorAudioQualityList.push({from: teacherParams.packetRecvStartTime, to: teacherParams.packetRecvEndTime, reason: "PACKET_RECV"});
                                analyseTime(teacherParams, teacherParams.packetRecvStartTime, teacherParams.packetRecvEndTime, teacherParams.lastAnalysed);
                                teacherParams.lastAnalysed = teacherParams.packetRecvEndTime;
                            }
                        }
                    }
                } else if (statsInstance === 5) {
                    if (obj.data.recv < 180) {

                        if (!teacherParams.packetRecvAnalyser) {
                            teacherParams.packetRecvAnalyser = true;
                            teacherParams.packetRecvStartTime = obj.time - 5000;
                        }
                    } else {
                        if (teacherParams.packetRecvAnalyser || (teacherParams.packetRecvAnalyser && callEnded)) {
                            teacherParams.packetRecvAnalyser = false;
                            teacherParams.packetRecvEndTime = obj.time - 5000;
                            //if(studentParams.packetRecvEndTime - studentParams.packetRecvStartTime >= 3000){
                            pushableUserObj.poorAudioQualityList.push({from: teacherParams.packetRecvStartTime, to: teacherParams.packetRecvEndTime, reason: "PACKET_RECV"});
                            //}
                            analyseTime(teacherParams, teacherParams.packetRecvStartTime, teacherParams.packetRecvEndTime, teacherParams.lastAnalysed);
                            teacherParams.lastAnalysed = teacherParams.packetRecvEndTime;
                        }
                    }
                }
            }
            if (obj.name == "PACKET_LOST" || callEnded) {
                if (obj.data.lost) {
                    teacherParams.totalPacketLost += (obj.data.lost / idealRecv) * 100;
                }
                if ((obj.data.lost / idealRecv) * 100 >= 10) {
                    if (!teacherParams.packetLostAnalyser) {
                        teacherParams.packetLostAnalyser = true;
                        teacherParams.packetLostStartTime = obj.time;
                    }
                } else {
                    if (teacherParams.packetLostAnalyser || (teacherParams.packetLostAnalyser && callEnded)) {
                        teacherParams.packetLostAnalyser = false;
                        teacherParams.packetLostEndTime = obj.time;
                        if (statsInstance === 1 && (teacherParams.packetLostStartTime - teacherParams.packetLostEndTime >= 3000)) {
                            pushableUserObj.poorAudioQualityList.push({from: teacherParams.packetLostStartTime, to: teacherParams.packetLostEndTime, reason: "PACKETS_LOST"});
                            analyseTime(teacherParams, teacherParams.packetLostStartTime, teacherParams.packetLostEndTime, teacherParams.lastAnalysed);
                            teacherParams.lastAnalysed = teacherParams.packetLostEndTime;
                        } else if (statsInstance === 5) {
                            pushableUserObj.poorAudioQualityList.push({from: teacherParams.packetLostStartTime, to: teacherParams.packetLostEndTime, reason: "PACKETS_LOST"});
                            analyseTime(teacherParams, teacherParams.packetLostStartTime, teacherParams.packetLostEndTime, teacherParams.lastAnalysed);
                            teacherParams.lastAnalysed = teacherParams.packetLostEndTime;
                        }
                    }
                }
            }
            if (obj.name == "RTT") {
                if (obj.data.rtt) {
                    teacherParams.totalCount++;
                    teacherParams.totalRtt += obj.data.rtt;
                }
            }
            if (obj.name == "JITTER") {
                if (obj.data.jitter) {
                    teacherParams.totalJitter += obj.data.jitter;
                }
            }
            /* if (obj.name == "RTT" || callEnded) {
             if (obj.data.rtt && obj.data.rtt >= 400) {
             if (!teacherParams.rttAnalyser) {
             teacherParams.rttAnalyser = true;
             teacherParams.rttStartTime = obj.time;
             }
             } else {
             if (teacherParams.rttAnalyser || (teacherParams.rttAnalyser && callEnded)) {
             teacherParams.rttAnalyser = false;
             teacherParams.rttEndTime = obj.time;
             if (statsInstance === 1 && (teacherParams.rttStartTime - teacherParams.rttEndTime >= 3000)) {
             pushableUserObj.poorAudioQualityList.push({from: teacherParams.rttStartTime, to: teacherParams.rttEndTime, reason: "RTT"});
             analyseTime(teacherParams, teacherParams.rttStartTime, teacherParams.rttEndTime, teacherParams.lastAnalysed);
             teacherParams.lastAnalysed = teacherParams.rttEndTime;
             } else if (statsInstance === 5) {
             pushableUserObj.poorAudioQualityList.push({from: teacherParams.rttStartTime, to: teacherParams.rttEndTime, reason: "RTT"});
             analyseTime(teacherParams, teacherParams.rttStartTime, teacherParams.rttEndTime, teacherParams.lastAnalysed);
             teacherParams.lastAnalysed = teacherParams.rttEndTime;
             }
             }
             }
             }*/

            if (obj.name == "JITTER_BUFFER" || callEnded) {
                if (obj.data.jitterBuffer && obj.data.jitterBuffer >= 400) {
                    if (!teacherParams.jitterBufferAnalyser) {
                        teacherParams.jitterBufferAnalyser = true;
                        teacherParams.jitterBufferStartTime = obj.time;
                    }
                } else {
                    if (teacherParams.jitterBufferAnalyser || (teacherParams.jitterBufferAnalyser && callEnded)) {
                        teacherParams.jitterBufferAnalyser = false;
                        teacherParams.jitterBufferEndTime = obj.time;
                        if (statsInstance === 1 && (teacherParams.jitterBufferStartTime - teacherParams.jitterBufferEndTime >= 3000)) {
                            pushableUserObj.poorAudioQualityList.push({from: teacherParams.jitterBufferStartTime, to: teacherParams.jitterBufferEndTime, reason: "JITTER_BUFFER"});
                            analyseTime(teacherParams, teacherParams.jitterBufferStartTime, teacherParams.jitterBufferEndTime, teacherParams.lastAnalysed);
                            teacherParams.lastAnalysed = teacherParams.jitterBufferEndTime;
                        } else if (statsInstance === 5) {
                            pushableUserObj.poorAudioQualityList.push({from: teacherParams.jitterBufferStartTime, to: teacherParams.jitterBufferEndTime, reason: "JITTER_BUFFER"});
                            analyseTime(teacherParams, teacherParams.jitterBufferStartTime, teacherParams.jitterBufferEndTime, teacherParams.lastAnalysed);
                            teacherParams.lastAnalysed = teacherParams.jitterBufferEndTime;
                        }
                    }
                }
            }
        }
        if (obj.name === "CONNECTED") {
             var ip = obj.data.publicIp;
            if (ip) {
                pushIfNotFound(pushableUserObj.publicIPs, ip);
            }
        }else if (obj.name === "JOIN") {
            timelineNoterObj.joined = true;
            timelineNoterObj.currentlyJoined = true;
            timelineNoterObj.whiteboardResumed = true;
            timelineNoterObj.socketId = obj.socketId;
            timelineNoterObj.joinedsockets.push(obj.socketId);
            if (timelineNoterObj.joinedFlag) {
                pushableUserObj.multipleConnectionCount++;
            } else {
                timelineNoterObj.joinedFlag = true;
            }
            if (timelineNoterObj.disconnectedAt > 0) {
                var diff = (obj.time - timelineNoterObj.disconnectedAt) / 1000;
                if (diff <= 2) {
                    pushableUserObj.reconnectionTimeReq["0-2"]++;
                } else if (diff <= 5) {
                    pushableUserObj.reconnectionTimeReq["2-5"]++;
                } else if (diff <= 10) {
                    pushableUserObj.reconnectionTimeReq["5-10"]++;
                } else {
                    pushableUserObj.reconnectionTimeReq["10-"]++;
                }
                pushableUserObj.offlineTime += parseInt(diff);
                timelineNoterObj.disconnectedAt = 0;
            }
            if (obj.data.deviceType) {
                var deviceType = obj.data.deviceType.toString();
                deviceType = deviceType.toUpperCase();
                if (deviceType == 'ANDROID' || deviceType == 'IOS') {
                    pushableUserObj.sessionOnMobile = true;
                    timelineNoterObj.onMobileSocketId = obj.socketId;
                    if (!timelineNoterObj.mobileJoinedTime) {
                        timelineNoterObj.mobileJoinedTime = obj.time;
                    }
                }
                if (!pushableUserObj.mobileData[obj.socketId]) {
                    pushableUserObj.mobileData[obj.socketId] = {
                        userId: obj.userId,
                        duration: obj.time,
                        networkType: obj.data.networkType,
                        deviceType: obj.data.deviceType,
                        deviceName: obj.data.deviceName,
                        networkOperator: obj.data.networkOperator,
                        screenSize: obj.data.screenSize,
                        creationTime: obj.time
                    };
                }
            }
            if (obj.data.service) {
                for (var key in timelineNoter) {
                        timelineNoter[key].currentCallService = obj.data.service;
                }
                
            }
        } else if (obj.name === "END_SESSION") {
            endedSession = true;
            if (timelineNoterObj.mobileJoinedTime) {
                pushableUserObj.timeOnMobile += (obj.time - timelineNoterObj.mobileJoinedTime);
                delete timelineNoterObj.mobileJoinedTime;
            }
        } else if (obj.name === "DISCONNECT") {
            if (timelineNoterObj.socketId == obj.socketId) {
                timelineNoterObj.joinedFlag = false;
            }
            if (pushableUserObj.mobileData[obj.socketId]) {
                pushableUserObj.mobileData[obj.socketId].duration = (obj.time - pushableUserObj.mobileData[obj.socketId].creationTime) / 1000;
                pushableUserObj.mobileData[obj.socketId].disconnectionTime = obj.time;
            }
            if (timelineNoterObj.mobileJoinedTime && timelineNoterObj.onMobileSocketId == obj.socketId) {
                pushableUserObj.timeOnMobile += (obj.time - timelineNoterObj.mobileJoinedTime);
                delete timelineNoterObj.mobileJoinedTime;
            }
        } else if (obj.name === "WEBRTC_CALL_REQUESTED") {
            for (var key in timelineNoter) {
                timelineNoter[key].webrtcCallRequestedTime = parseInt(obj.time);
                timelineNoter[key].webrtcCallRequestedVideo = obj.data.video;
                timelineNoter[key].webrtcCallRequestedBy = obj.userId;
            }
        } else if (obj.name=="PREPARE_UI_FOR_CHAT"){
            for (var key in timelineNoter) {
                timelineNoter[key].webrtcCallRequestedVideo = !obj.data.audioOnly;
            }
        } else if (obj.name === "WEBRTC_CALL_STARTED") {
            for (var key in timelineNoter) {
                if (!timelineNoter[key].webrtcCallStarted) {
                    timelineNoter[key].webrtcCallStarted = parseInt(obj.time);
                }
                var service = timelineNoter[key].currentCallService;
                if (service) {
                    for (var key in resultsObj) {
                        pushIfNotFound(resultsObj[key].callServices, service);
                    }
                }
            }
        } else if (obj.name === "WEBRTC_CALL_ENDED" ) {
            timelineNoterObj.audiobrrActive = false;
            var service=timelineNoterObj.currentCallService;
            if (timelineNoterObj.webrtcCallStarted) {
                var call_duration = parseInt((parseInt(obj.time) - timelineNoterObj.webrtcCallStarted) / 60000);
                var callStatsObj = {
                    callType: timelineNoterObj.webrtcCallRequestedVideo ? "VIDEO" : "AUDIO",
                    callStartTimeStr: getistTimeStr(parseInt(timelineNoterObj.webrtcCallRequestedTime)),
                    callStartTime: parseInt(timelineNoterObj.webrtcCallRequestedTime),
                    callEndTimeStr: getistTimeStr(parseInt(obj.time)),
                    callEndTime: parseInt(obj.time),
                    callDuration: parseInt((parseInt(obj.time) - timelineNoterObj.webrtcCallRequestedTime) / 1000),
                    callStartedBy: resultsObj[timelineNoterObj.webrtcCallRequestedBy].role,
                    callStartedById: timelineNoterObj.webrtcCallRequestedBy,
                    callEndedBy: resultsObj[obj.userId].role,
                    callEndedById: obj.userId,
                    callService:service
                };
                for (var key in resultsObj) {
                    if (resultsObj[key].role == "TEACHER") {
                        resultsObj[key].callsDetails.push(callStatsObj);
                    }
                }
                for (var key in timelineNoter) {
                    timelineNoter[key].webrtcCallStarted = false;
                }
                for (var key in resultsObj) {
                    resultsObj[key].totalCallDuration += call_duration;
                }
            }
        } else if (obj.name === "SCREEN_SHARE_STARTED") {
            for (var key in timelineNoter) {
                if (!timelineNoter[key].ssCallStarted) {
                    timelineNoter[key].ssCallStarted = parseInt(obj.time);
                }
            }

        } else if (obj.name === "SCREEN_SHARE_ENDED") {
            if (timelineNoterObj.ssCallStarted) {
                var duration = parseInt((parseInt(obj.time) - timelineNoterObj.ssCallStarted) / 60000);
                for (var key in timelineNoter) {
                    timelineNoter[key].ssCallStarted = false;
                }
                for (var key in resultsObj) {
                    resultsObj[key].totalScreenShareDuration += duration;
                }
            }
        } else if (obj.name === "DISCONNECT") {
            if (timelineNoterObj.socketId == obj.socketId) {
                timelineNoterObj.joinedFlag = false;
            }
        }
        var allJoined = true, currentlyAllJoined = true;
        for (var key in timelineNoter) {
            if (!timelineNoter[key].joined) {
                allJoined = false;
            }
            if (!timelineNoter[key].currentlyJoined) {
                currentlyAllJoined = false;
            }
        }
        if (allJoined && !endedSession) {
            if (currentlyAllJoined && oneParticipantDisconnectedAt > 0) {
                sessionInactiveTime += parseInt((obj.time - oneParticipantDisconnectedAt) / 1000);
                oneParticipantDisconnectedAt = 0;
            }

            if (!firstTimeBothJoinedTime) {
                firstTimeBothJoinedTime = obj.time;
            }
            if (obj.name === "ON_PAGE_LEAVE") {
                var index = timelineNoterObj.joinedsockets.indexOf(obj.socketId);
                if ((index > -1) && (timelineNoterObj.joinedsockets.length == 1)) {
                    pushableUserObj.refreshCount++;
                    timelineNoterObj.audiobrrActive = false;
                    if (!pushableUserObj.disconnectionMap["ON_PAGE_LEAVE"]) {
                        pushableUserObj.disconnectionMap["ON_PAGE_LEAVE"] = 0;
                    }
                    pushableUserObj.disconnectionMap["ON_PAGE_LEAVE"]++;
                }
            }
            if (obj.name === "DISCONNECT") {
                var index = timelineNoterObj.joinedsockets.indexOf(obj.socketId);
                if ((index > -1) && (timelineNoterObj.joinedsockets.length == 1)) {
                    if (timelineNoterObj.audiobrrActive) {
                        pushableUserObj.socketDisconnectedAudioActive++;
                    }
                    if (!timelineNoterObj.totalDisconnections) {
                        timelineNoterObj.totalDisconnections = 0;
                    }
                    timelineNoterObj.totalDisconnections++;
                    if (!pushableUserObj.disconnectionMap[obj.data.reason]) {
                        pushableUserObj.disconnectionMap[obj.data.reason] = 0;
                    }
                    pushableUserObj.disconnectionMap[obj.data.reason]++;

                    //time spent on mobile is using socket disconnections directly instead of WB_PAUSED

                    if (disconnectKey === "DISCONNECT") {//will execute this
                        //if we are not using WB_PAUSED
                        timelineNoterObj.currentlyJoined = false;
                        if (oneParticipantDisconnectedAt === 0) {
                            oneParticipantDisconnectedAt = obj.time;
                        }
                        timelineNoterObj.disconnectedAt = obj.time;
                    }
                }
                if (index > -1) {
                    timelineNoterObj.joinedsockets.splice(index, 1);
                }
                ;
            } else if (obj.name === "WB_PAUSED") {
                timelineNoterObj.currentlyJoined = false;
                if(timelineNoterObj.whiteboardResumed){
                    pushableUserObj.whiteboardPauses++;
                    if (oneParticipantDisconnectedAt === 0) {
                        oneParticipantDisconnectedAt = obj.time;
                    }
                    timelineNoterObj.disconnectedAt = obj.time;
                }
                timelineNoterObj.whiteboardResumed=false;
            } else if (obj.name === "CALL_INITIATED") {
                pushableUserObj.phoneCallCount++;
            } else if (obj.name === "PACKET_LOSS") {
                if (obj.data.loss > 100) {
                    pushableUserObj.highPacketLossTime += 5;//assuming 5 sec for each packet loss
                }
            } else if (obj.name === "ICE_DISCONNECTED") {
                timelineNoterObj.iceDisconnectedAt = obj.time;
            } else if (obj.name === "AUDIO_BRR" || obj.name === "END_CALL") {
                var audiobrr = obj.data.audiobrr;
                if (audiobrr >= 5) {
                    timelineNoterObj.audiobrrActive = true;
                } else {
                    timelineNoterObj.audiobrrActive = false;
                }
                if (timelineNoterObj.iceDisconnectedAt > 0 &&
                        audiobrr >= 5 &&
                        (obj.time - timelineNoterObj.iceDisconnectedAt) > 5000) {
                    var diff = parseInt((obj.time - timelineNoterObj.iceDisconnectedAt) / 1000);

//                    logger.debug("for userud " + pushableUserObj.userId
//                            + " diff is " + diff + " at " + new Date(parseInt(obj.time)));

                    diff = diff - 5;//stepping down by 5secs
                    if (diff <= 2) {
                        pushableUserObj.audioDisconnectedBuckets["0-5"]++;
                    } else if (diff <= 5) {
                        pushableUserObj.audioDisconnectedBuckets["5-10"]++;
                    } else if (diff <= 10) {
                        pushableUserObj.audioDisconnectedBuckets["10-15"]++;
                    } else {
                        pushableUserObj.audioDisconnectedBuckets["15-"]++;
                    }
                    pushableUserObj.audioDisconnectionTime += diff;
                    timelineNoterObj.iceDisconnectedAt = 0;
                }
            }

            //after both joined, time taken to initiate call
            if (!timeTakenInitiateFirstCall && firstTimeBothJoinedTime
                    && (obj.name === "REQUEST_AUDIO_CALL" || obj.name === "REQUEST_VIDEO_CALL")) {
                timeTakenInitiateFirstCall = parseInt((obj.time - firstTimeBothJoinedTime) / 1000);
            }

            //avg time in accepting calls
            if (obj.name === "REQUEST_AUDIO_CALL" || obj.name === "REQUEST_VIDEO_CALL") {
                avCallInitiatedAt = obj.time;
            }
            if (avCallInitiatedAt && (obj.name === "PREPARE_UI_FOR_CHAT")) {
                callAcceptTimes.push(parseInt((obj.time - avCallInitiatedAt) / 1000));
                avCallInitiatedAt = 0;
            }
        }

    }
    resultsObj[studentParams.userId].poorAudioQualityTime = parseInt(studentParams.totalBadTime / 1000);
    resultsObj[teacherParams.userId].poorAudioQualityTime = parseInt(teacherParams.totalBadTime / 1000);
    var avgCallAcceptTime = null;
    if (callAcceptTimes.length > 0) {
        var s = 0;
        for (var p in callAcceptTimes) {
            s += callAcceptTimes[p];
        }
        avgCallAcceptTime = parseInt(s / callAcceptTimes.length);
    }
    for (var key in resultsObj) {
        var _tempy = resultsObj[key];
        _tempy.timeTakenInitiateFirstCall = timeTakenInitiateFirstCall;
        if (avgCallAcceptTime) {
            _tempy.avgCallAcceptTime = avgCallAcceptTime;
        }
        var _tempy2 = timelineNoter[key];
        if (_tempy2 && _tempy2.totalDisconnections) {
            _tempy.disconnections = _tempy.disconnectionsNormalized = _tempy2.totalDisconnections - _tempy.refreshCount;
            if (_tempy.disconnections > disconnectionsNormalizableVal) {//>20
                _tempy.disconnectionsNormalized = disconnectionsNormalizableVal;
            }
        }
        if (_tempy && _tempy.whiteboardPauses) {
            _tempy.whiteboardPauses = _tempy.whiteboardPauses- _tempy.refreshCount;
            if(_tempy.whiteboardPauses<0){
                _tempy.whiteboardPauses=0;
            }
            if (_tempy.whiteboardPauses > disconnectionsNormalizableVal) {//>20
                _tempy.whiteboardPauses = disconnectionsNormalizableVal;
            }
        }
        _tempy.totalSessionInactiveTime = sessionInactiveTime;

        //normalizations
        _tempy.offlineTimeNormalized = _tempy.offlineTime;
        if (_tempy.offlineTime > offlineTimeNormalizableVal) {//> 1200 secs/20mins
            _tempy.offlineTimeNormalized = offlineTimeNormalizableVal;
        }

        if (sessionInactiveTime > sessionInactiveTimeNormalizableVal) {//> 1200 secs/20mins
            _tempy.totalSessionInactiveTimeNormalized = sessionInactiveTimeNormalizableVal;
        }

        _tempy.audioDisconnectionTimeNormalized = _tempy.audioDisconnectionTime;
        if (_tempy.audioDisconnectionTime > audioDisconnectionTimeNormalizableVal) {//> 1200 secs/20mins
            _tempy.audioDisconnectionTimeNormalized = audioDisconnectionTimeNormalizableVal;
        }

        _tempy.highPacketLossTimeNormalized = _tempy.highPacketLossTime;
        if (_tempy.highPacketLossTime > audioDisconnectionTimeNormalizableVal) {//> 1200 secs/20mins
            _tempy.highPacketLossTimeNormalized = audioDisconnectionTimeNormalizableVal;
        }

        _tempy.poorAudioQualityTimeNormalized = _tempy.poorAudioQualityTime;
        if (_tempy.poorAudioQualityTime > audioPoorQualityTimeNormalizableVal) {//> 1200 secs/20mins
            _tempy.poorAudioQualityTimeNormalized = audioPoorQualityTimeNormalizableVal;
        }
    }
};
var logsTextDataPusher = function (logItem, logsTextData) {
    var action = logItem.type;
    var data = {};
    if (typeof logItem.data === "object") {
        data = logItem.data;
    } else {
        try {
            data = JSON.parse(logItem.data);
        } catch (err) {
        }
    }
    logsTextData.push({
        name: action,
        time: logItem.serverTime,
        data: data,
        userId: logItem.userId,
        socketId: logItem.socketId
    });
};
var extractPublicIP = function (candidateItem) {
    var ip = "";
    try {
        if (candidateItem.data.indexOf("srflx") > -1) {
            var candidate = JSON.parse(candidateItem.data).candidate;
            ip = candidate.split(" ")[4];
        }
    } catch (err) {
        //swallow
    }
    return ip;
};
var countICECandidates = function (candidateItem, pushableUserObj) {
    try {
        var candiObj = pushableUserObj.iceCandidates;
        if (candidateItem.data.indexOf("srflx") > -1) {
            candiObj.srflx++;
        } else if (candidateItem.data.indexOf("relay") > -1) {
            candiObj.relay++;
        } else if (candidateItem.data.indexOf("host") > -1) {
            candiObj.host++;
        }
    } catch (err) {
        //swallow
    }
};
var fillISPDetails = function (resultsObj) {
    var deferred = Q.defer();
    var publicIPs = [], extractedData = {};
    for (var userId in resultsObj) {
        for (var k in resultsObj[userId].publicIPs) {
            pushIfNotFound(publicIPs, resultsObj[userId].publicIPs[k]);
        }
    }
    var pollForISPDetails = function (_ip) {
        var pr = getISPDetails(_ip);
        pr.then(function (ispData) {
            if (ispData && ispData.query_status && ispData.query_status.query_status_code == "OK" && ispData.geolocation_data) {
                ispData=ispData.geolocation_data;
                extractedData[_ip] = {
                    country: ispData.country_name,
                    city: ispData.city,
                    region: ispData.region_name,
                    name: ispData.isp,
                    org: ispData.organization
                };
            }
            if (publicIPs.length > 0) {
                pollForISPDetails(publicIPs.shift());
            } else {
                for (var userId in resultsObj) {
                    var _temp = resultsObj[userId];
                    for (var k in _temp.publicIPs) {
                        var _publicIP = _temp.publicIPs[k];
                        if (extractedData[_publicIP]) {
                            _temp.ispDetails.push(extractedData[_publicIP]);
                        }
                    }
                }
                deferred.resolve(resultsObj);
            }
        }).catch(function (err) {
            logger.error("error in getting Isp details ip"+_ip,err);
            deferred.resolve(resultsObj);
        });
    };
    if (publicIPs.length > 0) {
        pollForISPDetails(publicIPs.shift());
    } else {
        deferred.resolve(resultsObj);
    }

    return deferred.promise;
};
var getExotelCallData = function (sessionId) {
    var deferred = Q.defer();
//    var query;
//    var start = 0;
//    var limit = 100;
//    query = gcloudDataset.createQuery('PhoneCallMetadata')
//                .limit(limit)
//                .offset(start);
//    query.filter("contextId =", sessionId.toString());
    logger.log("Getting Exotel MetaData for  contextId: " + sessionId.toString());
    platform.getPhoneCallMetaData(sessionId).then(function (resp) {
        if (resp.errorCode) {
            logger.error("Error in fetching PhoneCallMetadata for sessionId: "
                    + sessionId.toString(), resp);
            deferred.resolve({duration: 0, count: 0, details: []});
        } else {
            var list = resp;
            logger.log("Pulled calls Data items, sessionId: " + sessionId, list);
            var duration = 0;
            var detail = {};
            for (var k = 0; k < list.length; k++) {
                duration += list[k].duration;
                if (!detail[list[k].fromNumber + "" + list[k].toNumber]) {
                    detail[list[k].fromNumber + "" + list[k].toNumber] = {from: list[k].fromNumber, to: list[k].toNumber, count: 1};
                } else {
                    detail[list[k].fromNumber + "" + list[k].toNumber].count++;
                }
            }
            var details = [];
            for (var i in detail) {
                details.push(detail[i]);
            }
            logger.log("result for the phone call data>>>>>>>>", {duration: duration, count: list.length, details: details});
            deferred.resolve({duration: duration, count: list.length, details: details});
        }
    });
//    gcloudDataset.runQuery(query, function (err, entities, cursor) {
//        if (err) {
//            logger.error("Error in fetching PhoneCallMetadata for sessionId: "
//                    + sessionId.toString(), err);
//            return false;
//        }
//        var list = entities.map(fromDatastore);
//        logger.log("Pulled calls Data items, sessionId: " + sessionId ,list);
//        var duration = 0;
//        var detail={};
//        for (var k = 0; k < list.length; k++) {
//            duration+=list[k].duration;
//            if(!detail[list[k].fromNumber+""+list[k].toNumber]){
//                 detail[list[k].fromNumber+""+list[k].toNumber]={from:list[k].fromNumber,to:list[k].toNumber,count:1};
//            }else{
//                detail[list[k].fromNumber+""+list[k].toNumber].count++;
//            }
//        }
//        var details=[];
//        for(var i in detail){
//            details.push(detail[i]);
//        }
//        logger.log("result for the phone call data>>>>>>>>",{duration:duration,count:list.length,details:details});
//        deferred.resolve({duration:duration,count:list.length,details:details});
//    });
    return deferred.promise;
};
var getISPDetails = function (ip) {
    logger.info("Fetching details of ip " + ip);
    var url = "http://api.apigurus.com/iplocation/v1.8/locateip?key=SAK8334J2K4VCFLK86KZ&ip="+ip+"&format=json";
    return httpclient.get(url);
};
var sorter = function (a, b) {
    if (a.time > b.time) {
        return 1;
    }
    if (a.time < b.time) {
        return -1;
    }
    // a must be equal to b
    return 0;
};
var pushIfNotFound = function (arr, item) {
    if (arr.indexOf(item) === -1) {
        arr.push(item);
    }
};
var getTimeString = function (millis) {
    var d = new Date(parseInt(millis));
    var timeStr = getZeroedTime(d.getHours()) + ":" +
            getZeroedTime(d.getMinutes()) + ":" + getZeroedTime(d.getSeconds());
    return timeStr;
};
var getZeroedTime = function (t) {
    if (t.toString().length === 1) {
        t = "0" + t;
    }
    return t;
};
var analyseTime = function (userParams, startTime, endTime, lastTime) {
    if (startTime < lastTime && endTime > lastTime) {
        userParams.totalBadTime += endTime - lastTime;
    } else {
        userParams.totalBadTime += endTime - startTime;
    }
};
var getistTimeStr = function (time) {
    var offset = 5.5;
    return new Date(new Date(time).getTime() + offset * 3600 * 1000).toUTCString().replace(/ GMT$/, "");
};

// Functions which will be available to external callers
exports.analyse = analyse;
