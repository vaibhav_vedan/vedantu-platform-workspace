/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/* global process, require, exports */

var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var Q = require("q");
var Client = require('node-rest-client').Client;
var authTokens = require(process.env["APP_HOME"] + "/config.js").getConfigProps().platform;
var restClient = new Client();

var addAuthTokensToHeader = function (params) {
    params.headers = params.headers || {};
    params.headers["X-Ved-Client"] = authTokens.client;
    params.headers["X-Ved-Client-Id"] = authTokens.clientId;
    params.headers["X-Ved-Client-Secret"] = authTokens.clientSecret;
    return params;
};

function get(url, params) {
    var deferred = Q.defer();
    params = params || {};
    params = addAuthTokensToHeader(params);
    restClient.get(url, params, function (data, response) {
        try{
        data = JSON.parse(data);
        }catch(err){
        }
        deferred.resolve(data);
    }).on('error', function (err) {
        logger.warn("Error in get call for url " + url);
        deferred.resolve(null);
    });
    return deferred.promise;
}

function post(url, params) {
    logger.info("sending Post request to URL" + url + "With the following params");
    logger.info(params);
    var deferred = Q.defer();
    params = params || {};
    params = addAuthTokensToHeader(params);
    restClient.post(url, params, function (data, response) {
        try {
            data = JSON.parse(data);
        } catch (err) {
            //logger.warn("Error in processing of http post response as json " + err);
        }
        logger.debug("response for url " + url);
        logger.debug(data);
        deferred.resolve(data);
    }).on('error', function (err) {
        logger.error("Error in post call for url " + url,err);
        deferred.resolve(null);
    });

    return deferred.promise;
}

// Functions which will be available to external callers
exports.get = get;
exports.post = post;
