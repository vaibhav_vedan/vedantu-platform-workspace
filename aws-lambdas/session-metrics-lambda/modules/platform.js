var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var authTokens = require(process.env["APP_HOME"] + "/config.js").getConfigProps().platform;
var httpClient = require(process.env["APP_HOME"] + "/modules/httpclient.js");

var addAuthTokensToHeader = function (params) {
    params.headers = params.headers || {};
//    params.headers["X-Ved-Client"] = authTokens.client;
//    params.headers["X-Ved-Client-Id"] = authTokens.clientId;
//    params.headers["X-Ved-Client-Secret"] = authTokens.clientSecret;
    params.headers["Content-Type"] = "application/json";
    params.headers["Accept"] = "application/json";
    return params;
};

var platformGetRequest = function (url, params, skipToken) {
    params = params || {};
    if (!skipToken) {
        params = addAuthTokensToHeader(params);
    }
    return httpClient.get(url, params);
};

var platformPostRequest = function (url, params, skipToken) {
    params = params || {};
    if (!skipToken) {
        params = addAuthTokensToHeader(params);
    }
    return httpClient.post(url, params);
};

var getSessionInfo = function (sessionId) {
    var url = authTokens.url + "/platform/session/" + sessionId;
    var params = {data: {
        }};
    return platformGetRequest(url, params);
};
var fetchFeedback = function (sessionId) {
    var url = authTokens.url + "/platform/feedback/fetchFeedback?sessionId="+sessionId+"&orderDesc="+true;
    var params = {data: {
        }};
    return platformGetRequest(url, params);
};
var getReviews = function (sessionId) {
    var url = authTokens.url + "/platform/reviews/getReviews?contextType=SESSION&contextId="+sessionId;
   var params = {data: {
        }};
    return platformGetRequest(url, params);
};

var getPhoneCallMetaData = function (sessionId) {
    var url = authTokens.url + "/platform/click2call/getPhonecallMetadata?contextType=SESSION&contextId="+sessionId;;
     var params = {data: {
        }};
    return platformGetRequest(url, params);
};

var getSessionIds = function (startTime, endTime, from, limit) {
    var url = authTokens.url + "/platform/session/getSessions?start="+from+"&size="+limit+"&afterStartTime="+startTime+"&beforeStartTime="+endTime;
    var params = {data: {
        }};
    return platformGetRequest(url, params);
};

module.exports.getSessionInfo = getSessionInfo;
module.exports.fetchFeedback = fetchFeedback;
module.exports.getReviews = getReviews;
module.exports.getPhoneCallMetaData = getPhoneCallMetaData;
module.exports.getSessionIds=getSessionIds;