/* global require, process, module */

var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var mongoose = require('mongoose');
var Q = require("q");
mongoose.Promise=Q.Promise;
var mongoNonSessionProps = configProps["mongo-non-session"];
var hosts = mongoNonSessionProps.host;
if ((hosts && hosts.length > 1 && hosts.length < 6)) {
    var url = "mongodb://";
    var options = {
        user: mongoNonSessionProps.username,
        pass: mongoNonSessionProps.password
    };
    for (i = 0; i < hosts.length; i++)
        url += hosts[i] + ":" + mongoNonSessionProps.port + ",";
    url = url.substring(0, url.length - 1);
    url += "/" + mongoNonSessionProps.dbname;
    url += "?options";
    url += '&ssl=true';
    var mongoNonSessiondb = mongoose.createConnection(url, options);
} else if (mongoNonSessionProps.isAtlas) {
    var mongoNonSessiondb = mongoose.createConnection(mongoNonSessionProps.dburi);
} else {
    var mongoNonSessiondb = mongoose.createConnection('mongodb://' + mongoNonSessionProps.host + ':' + mongoNonSessionProps.port + '/' + mongoNonSessionProps.dbname);
}
mongoNonSessiondb.on('error', function (err) {
    logger.error("Error in connecting to mongo " + err);
    process.exit();
});
var SessionAnalysisModel, EvaluatedMetricsModel;
mongoNonSessiondb.once('open', function () {
    logger.info("Connection to mongo opened");
    var SessionAnalysisSchema = mongoose.Schema({
        sessionId: String,
        sessionType: String,
        userId: String,
        otherUserId: String,
        role: String,
        name: String,
        email: String,
        otherUserEmail: String,
        firstName: String,
        lastName: String,
        state: String,
        displayState: String,
        joinTime: Number,
        startTime: Number,
        endTime: Number,
        subject: String,
        topic: String,
        billingPeriod: Number,
        teacherRating: Number,
        techRating: Number,
        techRatingReason: String,
        sessionErrors: mongoose.Schema.Types.Mixed,
        publicIPs: [String],
        callServices: [String],
        totalCallDuration: Number,
        totalScreenShareDuration:Number,
        socketDisconnectedAudioActive: Number,
        ispDetails: mongoose.Schema.Types.Mixed,
        browserName: String,
        browserVersion: String,
        multipleConnectionCount: Number,
        reconnectionTimeReq: mongoose.Schema.Types.Mixed,
        disconnectionMap: mongoose.Schema.Types.Mixed,
        offlineTime: Number,
        offlineTimeNormalized: Number,
        disconnections: Number,
        disconnectionsNormalized: Number,
        totalSessionInactiveTime: Number,
        totalSessionInactiveTimeNormalized: Number,
        audioDisconnectedTimes: Number,
        audioDisconnectedTimesNormalized: Number,
        audioDisconnectionTime: Number,
        audioDisconnectionTimeNormalized: Number,
        audioDisconnectedBuckets: mongoose.Schema.Types.Mixed,
        highPacketLossTime: Number,
        highPacketLossTimeNormalized: Number,
        poorAudioQualityTime: Number, //seconds
        poorAudioQualityTimeNormalized: Number,
        avgRtt: Number,
        avgJitter: Number,
        avgPacketLoss: Number,
        poorAudioQualityList: mongoose.Schema.Types.Mixed,
        sessionOnMobile: Boolean,
        timeOnMobile: Number, 
        refreshCount: Number,
        phoneCallCount: Number,
        avgCallConnectionTime: Number,
        callsCount: Number,
        callsCountNormalized: Number,
        audioCallTries: Number,
        videoCallTries: Number,
        callRequestDeclined: Number,
        iceCandidates: mongoose.Schema.Types.Mixed,
        joinDelay: Number,
        timeTakenInitiateFirstCall: Number,
        avgCallAcceptTime: Number,
        avgDownloadSpeedAfterDisconnect: Number,
        avgUploadSpeedAfterDisconnect: Number,
        wbActivityFound: Boolean,
        analysable: Boolean,
        whiteboardPauses: Number,
        textchatCount: Number,
        turnStats: mongoose.Schema.Types.Mixed,
        turnServerUsed: String,
        creationTime: Number,
        lastUpdated: Number,
        startTimeStr: String,
        speedTestStartTime: Number,
        speedTestDuration: Number,
        speedTestFileCount: Number,
        downloadTest: mongoose.Schema.Types.Mixed,
        uploadTest: mongoose.Schema.Types.Mixed,
        exotelCallDuration: Number,
        exotelCallCount: Number,
        exotelCallDetails: mongoose.Schema.Types.Mixed
    });
    var SessionMobileDataSchema = mongoose.Schema({
        sessionId: String,
        userId: String,
        duration: Number, // no do seconds
        networkType: String,
        deviceType: String,
        deviceName: String,
        networkOperator: String,
        screenSize: String,
        socketId: String,
        creationTime: Number,
        disconnectionTime: Number,
        role:String
    });
    var SessionCallsDataSchema = mongoose.Schema({
        callType: String,
        callService: String,
        callStartTimeStr: String,
        callStartTime: Number,
        callEndTimeStr: String,
        callEndTime: Number,
        callDuration: Number, //seconds
        callStartedBy: String,
        callStartedById: String,
        callEndedBy: String,
        callEndedById: String,
        sessionId: String,
        teacherId: String,
        studentId: String,
        teacherEmailId: String,
        studentEmailId: String,
        startedAt: Number,
        startedAtStr: String,
        endedAt: Number,
        endedAtStr: String,
        sessionDuration: Number, //seconds
        totalCallCount: Number
    });

    var EvaluatedMetricsSchema = mongoose.Schema({
        metricType: String,
        timeStr: String,
        metricName: String,
        data: mongoose.Schema.Types.Mixed,
        creationTime: Number,
        lastUpdated: Number
    });

    SessionAnalysisSchema.index({startTime: -1});
    SessionAnalysisSchema.index({sessionId: -1});
    SessionAnalysisModel = mongoNonSessiondb.model('SessionAnalysis', SessionAnalysisSchema);//similar to collection

    SessionMobileDataSchema.index({sessionId: 1});
    SessionMobileDataModel = mongoNonSessiondb.model('SessionMobileData', SessionMobileDataSchema);
    SessionCallsDataSchema.index({sessionId: 1});
    SessionCallsDataModel = mongoNonSessiondb.model('SessionCallsData', SessionCallsDataSchema);
    EvaluatedMetricsSchema.index({metricType: 1, metricName: 1});
    EvaluatedMetricsModel = mongoNonSessiondb.model('EvaluatedMetrics', EvaluatedMetricsSchema);
});


module.exports = {
    addSessionUserData: function (obj) {
        var deferred = Q.defer();
        obj.timeCreated = obj.lastUpdated = new Date().getTime();
        logger.info("Got request for adding session user data ");
        logger.debug(obj);
        if (obj.mobileData) {
            var mobileData = obj.mobileData;
            delete obj.mobileData;
            for (var i in mobileData) {
                mobileData[i].sessionId = obj.sessionId;
                mobileData[i].role = obj.role;
                mobileData[i].socketId = i;
                (function (data) {
                    SessionMobileDataModel.update({sessionId: data.sessionId, socketId: data.socketId}, data, {upsert: true}).exec(function (err) {
                        if (err) {
                            logger.error("Error in adding entry " + JSON.stringify(data) + ", err: " + err);
                        } else {
                            logger.info("added entry ", JSON.stringify(data));
                        }
                    });
                })(mobileData[i]);
            }
        }
        if(obj.callsDetails){
            var callData = obj.callsDetails;
            delete obj.callsDetails;
            for (var i in callData) {
                callData[i].sessionId = obj.sessionId;
                if(obj.role=="TEACHER"){
                    callData[i].teacherId=obj.userId;
                    callData[i].studentId=obj.otherUserId;
                    callData[i].teacherEmailId=obj.email;
                    callData[i].studentEmailId=obj.otherUserEmail;
                }else{
                    callData[i].studentId=obj.userId;
                    callData[i].teacherId=obj.otherUserId;
                    callData[i].studentEmailId=obj.email;
                    callData[i].teacherEmailId=obj.otherUserEmail;
                }
                callData[i].startedAt=parseInt(obj.startTime);
                callData[i].startedAtStr=getistTimeStr(parseInt(obj.startTime));
                callData[i].endedAt=parseInt(obj.endTime);
                callData[i].endedAtStr=getistTimeStr(parseInt(obj.endTime));
                callData[i].sessionDuration=parseInt((obj.endTime-obj.startTime)/1000);
                callData[i].totalCallCount=callData.length;
                (function (data) {
                    SessionCallsDataModel.update({sessionId: data.sessionId, callStartTime: data.callStartTime}, data, {upsert: true}).exec(function (err) {
                        if (err) {
                            logger.error("Error in adding entry " + JSON.stringify(data) + ", err: " + err);
                        } else {
                            logger.info("added entry ", JSON.stringify(data));
                        }
                    });
                })(callData[i]);
            }
        }
        SessionAnalysisModel.update({sessionId: obj.sessionId, userId: obj.userId}, obj, {upsert: true})
                .exec(function (err) {
                    if (err) {
                        logger.error("Error in adding entry " + JSON.stringify(obj) + ", err: " + err);
                        deferred.resolve(null);
                    } else {
                        deferred.resolve(true);
                    }
                });
        return deferred.promise;
    },
    aggregateQuery: function (query) {
        var deferred = Q.defer();
        logger.info("Running the query " + JSON.stringify(query));
        SessionAnalysisModel.aggregate(query)
                .exec(function (err, res) {
                    if (err) {
                        logger.error("Error in running query " + query + ", err: " + err);
                        deferred.resolve(null);
                    } else {
                        deferred.resolve(res);
                    }
                });
        return deferred.promise;
    },
    findQuery: function (query) {
        var deferred = Q.defer();
        logger.info("Running the query " + JSON.stringify(query));
        SessionAnalysisModel.find(query)
                .exec(function (err, res) {
                    if (err) {
                        logger.error("Error in running query " + query + ", err: " + err);
                        deferred.resolve(null);
                    } else {
                        deferred.resolve(res);
                    }
                });
        return deferred.promise;
    },
    countQuery: function (query) {
        var deferred = Q.defer();
        logger.info("Running the query " + JSON.stringify(query));
        SessionAnalysisModel.count(query)
                .exec(function (err, res) {
                    if (err) {
                        logger.error("Error in running query " + query + ", err: " + err);
                        deferred.resolve(null);
                    } else {
                        deferred.resolve(res);
                    }
                });
        return deferred.promise;
    },
    addMetric: function (obj) {
        var deferred = Q.defer();
        obj.timeCreated = obj.lastUpdated = new Date().getTime();
        logger.info("Got request saving metric ");
        logger.debug(obj);
        EvaluatedMetricsModel.update({metricType: obj.metricType, metricName: obj.metricName, timeStr: obj.timeStr},
        {$set: obj}, {upsert: true}).exec(function (err) {
            if (err) {
                logger.error("Error in adding metric " + JSON.stringify(obj) + ", err: " + err);
                deferred.resolve(null);
            } else {
                deferred.resolve(true);
            }
        });
        return deferred.promise;
    },
    getSessionUserData: function (userId, email, from, to, orderStr) {
        var deferred = Q.defer();
        logger.info("Got request for getting session user data ");
        logger.debug(userId, email, from, to);
        if (!userId && !email) {
            logger.warn("No userId or email provided for getting session user data");
        }

        var order = -1;
        if (orderStr === "ASC") {
            order = 1;
        }
        var query = {startTime: {"$gte": from, "$lte": to}};
        if (userId) {
            query.userId = userId;
        } else if (email) {
            query.email = email;
        }

        SessionAnalysisModel.find(query)
                .sort({startTime: order}).limit(1000)
                .exec(function (err, resp) {
                    logger.debug("Response for getting user session data");
                    if (err) {
                        logger.error(err);
                        deferred.resolve([]);
                    } else {
                        logger.debug(resp);
                        deferred.resolve(resp);
                    }
                });
        return deferred.promise;
    }
};
var getistTimeStr = function(time){
    var offset=5.5;
   return new Date( new Date(time).getTime() + offset * 3600 * 1000).toUTCString().replace( / GMT$/, "" );
};
