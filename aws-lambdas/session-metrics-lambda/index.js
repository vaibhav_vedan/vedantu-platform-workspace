/* global process, require, exports, __filename */
var env = require("./env.js").env;
process.env["DEPLOYMENT_MODE"] = env;
process.env['PATH'] = process.env['PATH'] + ':/tmp/:' + process.env['LAMBDA_TASK_ROOT'];
process.env['PATH'] = process.env['PATH'] + ':' + process.env['LAMBDA_TASK_ROOT'] + '';
process.env["APP_HOME"] = require("path").dirname(require("fs").realpathSync(__filename));
var config = require(process.env["APP_HOME"] + "/config.js");
//setting config parameters
config.setConfigPropsForCurrentMode(process.env["DEPLOYMENT_MODE"]);
var configProps = config.getConfigProps();
var Q = require('q');


var expectedSessionEventSubjects = ["SESSION_ENDED", "SESSION_EXPIRED", "SESSION_CANCELLED"];

//getting the logger
var logger = require(process.env["APP_HOME"] + "/utils/logger.js");
var analyseSessions = require(process.env["APP_HOME"] + "/managers/analyseSessions.js");
var mongo = require(process.env["APP_HOME"] + "/modules/mongo.js");

exports.handler = function(event, context) {
    logger.log("received a call from SNS in env " + env);
    logger.log(JSON.stringify(event));
    ///////////////////////////////////
    //      HANDLING SNS MESSAGE
    ///////////////////////////////////
    var pr = handleMessage(event);
    pr.then(function (result) {
        logger.info("Handled the sns event successfully, now exiting");
        context.succeed(result);
    }, function (err) {
        logger.error("Error in handling sns event " + configProps.sessionId, err);
        context.fail(err);
    });
};

function handleMessage(event) {
    var deferred = Q.defer();
    if (event && event.Records) {
        if (event.Records.length > 0) {
            var record = event.Records[0];
            if (!record.Sns) {
                deferred.reject("Was expecting an sns notification but got something else");
                return;
            }
            record = record.Sns;
            var subject = record.Subject;
            if (expectedSessionEventSubjects.indexOf(subject) > -1) {
                var json = {};
                try {
                    json = record.Message;
                    if (!json.sessionId) {
                        json = JSON.parse(json);
                    }
                    if (json.sessionId) {
                        configProps.sessionId=json.sessionId;
                        var pr = analyseSessions.analyse(json.sessionId);
                        pr.then(function (data) {
                            if (data && !data.errorCode) {
                                var mongoPromises=[];
                                for (var key in data) {
                                    mongoPromises.push(mongo.addSessionUserData(data[key]));
                                }
                                var lastPromises = Q.all(mongoPromises);
                                lastPromises.then(function(resultsArray) {
                                    for(var i=0;i<resultsArray;i++){
                                        if(resultsArray[i]==null){
                                             logger.error("Error in analysing session " + json.sessionId);
                                             deferred.reject("Error in analysing session " + json.sessionId );
                                        }
                                    }
                                    logger.log("Handled SNS event successfully:", json);
                                    deferred.resolve(true);
                                });
                                
                            } else {
                                logger.warn("Error in analysing session " + json.sessionId);
                            }
                        });

                    } else {
                        deferred.reject("Could not get sessionId from the event ");
                    }
                } catch (e) {
                    logger.error(e);
                    deferred.reject("Could not parse message json str from the event");
                }
            } else {
                logger.warn("received session event which I am not interested in",event);
                deferred.resolve(true);
            }
        } else {
            deferred.reject("received zero records in sns notification");
        }
    } else {
        deferred.reject("received sns notification but no records found in them");
    }
    return deferred.promise;
}

//exports.handler({
//    Records: [{
//        Sns: {
//            Subject: "SESSION_ENDED",
//            Message: {
//                sessionId: '4704929067827200'
//            }
//        }
//    }]
//});
