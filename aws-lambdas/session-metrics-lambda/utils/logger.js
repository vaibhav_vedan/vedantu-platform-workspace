/* global module, require, process */
var raven = require('raven');
var configProps = require(process.env["APP_HOME"] + "/config.js").getConfigProps();
var ravenKey = configProps.raven.key;
var client = new raven.Client(ravenKey);
var consoleFns = ["log", "trace", "info", "info", "warn", "error"];
var logger = require('tracer').colorConsole({
    format: "{{timestamp}} pid:" + process.pid + " <{{title}}> {{message}} (in {{file}}:{{line}})",
    transport: function (data) {
        if (consoleFns[data.level]) {
            console[consoleFns[data.level]](data.output);
        }
        if ((data.level === 5)&&(configProps.env=='prod')) {
            client.captureException(data.output);
        }
    }
});
client.on('error', function (e) {
    // The event contains information about the failure:
    //   e.reason -- raw response body
    //   e.statusCode -- response status code
    //   e.response -- raw http response object

    console.error("Could not send to sentry " + e.reason);
});
module.exports = logger;