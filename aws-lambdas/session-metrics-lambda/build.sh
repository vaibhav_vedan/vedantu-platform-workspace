env=$1
echo "started building node project for $env environment"
echo "module.exports.env='${env}'" > env.js
echo "running sudo npm install"
mkdir node_modules
sudo npm install
sudo chmod -R ugo+w node_modules/
rm SESSION_METRICS_*.zip
zip -r SESSION_METRICS_${env}.zip .
echo "create the file SESSION_METRICS_${env}.zip in the current directory"


