/* global module */

var localsettings = {
    logs: {
        level: "log",
        filePath: "/var/log",
        maxDaysToKeepFile: 7, //days
        intervalToCheckFiles: 5 //hrs
    },
    "mongo-non-session": {
        host: ["localhost"],
        port: 27017,
        username: "vedadmin",
        password: "v3d4ntu@123",
        dbname:  "metrics"
    },
    logsSource:{
        domain:"https://ns1.vedantu.com"
    },
    raven: {
        key: "https://77b55a0978dc41cfa102f93963e9ee42:a5d4e7534df74e50a4c4538929a0f43e@app.getsentry.com/79423"
    },
    platform: {
        client:"SESSION_METRICS",
        clientId:"472FA",
        clientSecret:"CMdqxxzouZlwp3WA1Dt3Aw4HwiliyL3U",
        url:"https://platform.vedantu.com"
    }
};
var prodsettings = {
    logs: {
        level: "info",
        filePath: "/var/log",
        maxDaysToKeepFile: 30, //days
        intervalToCheckFiles: 5 //hrs
    },
    "mongo-non-session": {
        "dburi": "mongodb+srv://readwrite:rYghHfwTyt3L6AOO@ds021008-uv3wl.mongodb.net/vedantuwave?retryWrites=true&w=majority&ssl=true",
        "isAtlas": true,
        "dbname":  "vedantuwave"
    },
    logsSource:{
        domain:"https://ns1.vedantu.com"
    },
    raven: {
        key: "https://77b55a0978dc41cfa102f93963e9ee42:a5d4e7534df74e50a4c4538929a0f43e@app.getsentry.com/79423"
    },
    platform: {
        client:"SESSION_METRICS",
        clientId:"472FA",
        clientSecret:"CMdqxxzouZlwp3WA1Dt3Aw4HwiliyL3U",
        url:"https://platform.vedantu.com"
    }
};
var propsMap = {
    local: localsettings,
    prod: prodsettings
};
var propertiesForCurrentMode = {};
module.exports = {
    modes: ['local', 'prod'],
    appVersion: "1.0",
    setConfigPropsForCurrentMode: function (mode) {
        propertiesForCurrentMode = propsMap[mode];
    },
    getConfigProps: function (mode) {
        return propertiesForCurrentMode;
    }
};
