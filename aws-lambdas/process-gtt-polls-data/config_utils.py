import vedantu_logger;
def get_env():
    config = __import__('env')
    return config.env

def import_config():
    env = get_env()
    env = env.lower()
    config = __import__('config')
    props = config.get_Configs(env);
    return props
