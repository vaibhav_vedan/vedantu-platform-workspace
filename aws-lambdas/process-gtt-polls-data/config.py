class local:
    class platform:
        schedulingEndPoint="http://localhost:8081/scheduling"
        api="/onetofew/session/updatePollsMetadataForSession"

    class headers:
        client="NODE"
        clientId="E25D9"
        clientSecret="mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk"

    class raven:
        key=""

class prod:
    class platform:
        schedulingEndPoint="https://scheduling.vedantu.com/scheduling"
        api="/onetofew/session/updatePollsMetadataForSession"

    class headers:
        client="NODE"
        clientId="E25D9"
        clientSecret="mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk"

    class raven:
        key="https://39b6921c2e8c483ca692cff3db5dc049:cab02289330041c89054ddc016278275@sentry.io/203351"

def getPropsForQAProfiles(mode):
    class settings:
        class platform:
            schedulingEndPoint="https://"+mode+"-platform.vedantu.com/scheduling"
            api="/onetofew/session/updatePollsMetadataForSession"

        class headers:
            client="NODE"
            clientId="E25D9"
            clientSecret="mP1s4xy1ZHI2EFrIv118XQ43jXL3OVuk"

        class raven:
            key=""

    return settings;

config_map = {
    "prod":prod,
    "local":local
}


def get_Configs(env):
    if env not in config_map:
        return getPropsForQAProfiles(env);
    else:
        return config_map[env]
