import logging
import logging.config
from pytz import timezone, utc
from datetime import datetime
LOG_SETTINGS = {
    'version': 1,
    'disable_existing_loggers': True,

    'formatters': {
        'console': {
            'format': '[%(asctime)s][%(levelname)s] '
                      '%(filename)s:%(funcName)s:%(lineno)d | %(message)s',
            'datefmt': '%m/%d/%Y %H:%M:%S %p',
            },
        },

    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'console'
            },
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.handlers.logging.SentryHandler',
            'dsn': '',
            'formatter': 'console'
            }
        },

    'loggers': {
        'default': {
            'handlers': ['console', 'sentry'],
            'level': 'DEBUG',
            'propagate': False,
            },
        'your_app': {
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}

def customTime(*args):
    utc_dt = utc.localize(datetime.utcnow())
    my_tz = timezone("Asia/Calcutta")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()

def initLogger(dsn,sessionId):
    LOG_SETTINGS["handlers"]["sentry"]["dsn"]=dsn;
    # print(LOG_SETTINGS["formatters"]["console"]["format"]);
    LOG_SETTINGS["formatters"]["console"]["format"]='[%(asctime)s][%(levelname)s] %(filename)s:%(funcName)s:%(lineno)d |'+sessionId+' %(message)s';
    logging.config.dictConfig(LOG_SETTINGS)
    logging.Formatter.converter = customTime

def getLogger():
	return logging.getLogger('default');
