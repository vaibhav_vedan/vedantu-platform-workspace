env=$1
echo "started building python project for $env environment"
echo "env='${env}'" > env.py
echo "running pip2 install"
pip2 install -r requirements.txt -t .
rm PROCESS_GTT_POLLS_DATA_*.zip
zip -r PROCESS_GTT_POLLS_DATA_${env}.zip .
echo "create the file PROCESS_GTT_POLLS_DATA_${env}.zip in the current directory"
