from __future__ import print_function

import json
import requests
import csv
import config_utils;
import vedantu_logger;

def lambda_handler(event, context):
    config=config_utils.import_config();
    event=json.loads(event.get('Records')[0]['body'])
    platformApi= config.platform.schedulingEndPoint+config.platform.api;

    headers = {
        'X-Ved-Client': config.headers.client,
        'X-Ved-Client-Id': config.headers.clientId,
        'X-Ved-Client-Secret': config.headers.clientSecret,
    }


    print("Received event: " + json.dumps(event, indent=2))
    message = event['Message']
    # message = "{\"sessionId\":\"5ae6c0a960b29bc2324fd84f\",\"sessionkeys\":[\"5720699\"],\"credentials\":{\"email\":\"gtt1@vedantu.com\",\"password\":\"vedantu@otf124\"},\"deleteRecordings\":true}";
    data = json.loads(message)
    email= data["credentials"]["email"]
    password= data["credentials"]["password"]
    # email= "gtt201@vedantu.com"
    # password= "vedantu@otf1241"
    sessionkeys= data["sessionkeys"]
    # sessionkeys = ["5532926"]
    sessionId= data["sessionId"]
    vedantu_logger.initLogger(config.raven.key,sessionId);
    logger=vedantu_logger.getLogger();
    # sessionId="aa";

    userIdQuestionMap={};
    sessionQuestionsMap={};
    userIdQuestionsList=[];
    sessionQuestionList=[];


    def proccess_csv(data):
        columns = len(data);
        rows = len(data[7]);
        for i in range(7,rows):
            question = data[7][i];
            if not question:
                logger.error("received empty question"+str(data));
                return False;
            if i not in sessionQuestionsMap:
                sessionQuestionsMap[i]={"question":question,"totalAnswers":0,"answers":{}};
            for j in range(8,columns-1):
                answer= data[j][i];
                if answer is not "":
                    userId= data[j][3];
                    sessionQuestionsMap[i]["totalAnswers"]+=1;
                    if answer not in sessionQuestionsMap[i]["answers"]:
                        sessionQuestionsMap[i]["answers"][answer]=0;
                    sessionQuestionsMap[i]["answers"][answer]+=1;
                    userId=userId[:userId.find("@user.com")]
                    if not userId:
                        logger.error("received empty userId"+str(data));
                        return False;
                    if userId not in userIdQuestionMap:
                        userIdQuestionMap[userId]=[];
                    userIdQuestionMap[userId].append({
                        "question":question,
                        "answer":answer
                    })
        return True;

    def post_process_task():
        payload = {
            'sessionId': sessionId,
            'pollsdata': {
                'pollQuestions':sessionQuestionList
            },
            'studentPollDatas': userIdQuestionsList
            };
        logger.info(payload);
        r = requests.post(platformApi, headers=headers, json=payload);
        logger.info(r.status_code);
        logger.info(r.json());

    # sessionId= "5aaf79a360b24d06b6fea578";
    logger.info("received email:"+email+" password:"+password+" sessionkeys:"+str(sessionkeys)+" sessionId:"+sessionId)
    sess = requests.session()
    r=sess.post("https://authentication.logmeininc.com/login?service=https%3A%2F%2Fglobal.gototraining.com%2Fverify_sso", data={"emailAddress": email, "password": password, "submit": "Sign in", "rememberMe": "on", "_eventId": "submit", "lt": "", "execution": ""})
    logger.info("login response code:"+str(r.status_code))
    for key in sessionkeys:
        logger.info("https://global.gototraining.com/training/reporting/attendee.csv?sessionKeyList="+key+"&timeZone=Asia%2FCalcutta");
        response =sess.get("https://global.gototraining.com/training/reporting/attendee.csv?sessionKeyList="+key+"&timeZone=Asia%2FCalcutta")
        logger.info("download response code:"+str(response.status_code))
        datareader = csv.reader(response.text.split("\n"), delimiter=',')
        logger.info("response.text:"+response.text);
        data = []
        for row in datareader:
            data.append(row)
        logger.info("data:"+str(data));
        rest=proccess_csv(data);
        logger.info("userIdQuestionMap:"+str(userIdQuestionMap));
        logger.info("sessionQuestionsMap:"+str(sessionQuestionsMap));
        if not rest:
            logger.error(sessionId+" error file:"+response.text);
    for userId in userIdQuestionMap:
        if not userId:
            logger.error("received empty userId userIdQuestionMap"+str(userIdQuestionMap));
            return False;
        userIdQuestionsList.append({
            "userId": userId,
            "pollAnswered":userIdQuestionMap[userId]
        })
    for question in sessionQuestionsMap:
        if not question:
            logger.error("received empty question sessionQuestionsMap"+str(sessionQuestionsMap));
            return False;
        answers = [];
        for answer in sessionQuestionsMap[question]["answers"]:
            answers.append({
                "answer":answer,
                "totalAnswers":sessionQuestionsMap[question]["answers"][answer]
            })
        sessionQuestionList.append({
            "question": sessionQuestionsMap[question]["question"],
            "totalAnswers":sessionQuestionsMap[question]["totalAnswers"],
            "answers":answers
        })
    logger.info("sessionQuestionList:"+str(sessionQuestionList));
    logger.info("userIdQuestionsList:"+str(userIdQuestionsList));
    post_process_task();

