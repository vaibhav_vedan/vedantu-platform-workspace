from __future__ import print_function

import json
import requests

print('Loading function')


def lambda_handler(event, context):
	#print("Received event: " + json.dumps(event, indent=2))
	message = event['Records'][0]['Sns']['Message']
	data = json.loads(message)
	email= data["email"]
	password= data["password"]
	recordId= data["recordingId"]
	
	print("received email:"+email+" password:"+password+" recordingId:"+recordId)
	
	sess = requests.session()
	r=sess.post("https://login.citrixonline.com/login?service=https://global.gototraining.com/verify_sso", data={"emailAddress": email, "password": password, "submit": "Sign in", "rememberMe": "on", "_eventId": "submit", "lt": "", "execution": ""})
	
	print("login response code:"+str(r.status_code))
	
	response =sess.delete("https://global.gototraining.com/api/v1/recordings/"+recordId)
	
	print("delete response code:"+str(response.status_code))
	return response.status_code
