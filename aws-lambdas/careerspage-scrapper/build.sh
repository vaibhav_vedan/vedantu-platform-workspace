echo "started building for careers page scrapper"
echo "installing dependencies"
rm scrapper.zip
pip install --system --target ./package pymongo
pip install --system --target ./package bs4
pip install --system --target ./package cloudscraper
cd package
zip -r9 ../scrapper.zip .
cd ..
zip -g scrapper.zip scrap.py
echo "create the file scrapper.zip in the current directory"
