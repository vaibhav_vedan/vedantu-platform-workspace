from pymongo import MongoClient
import cloudscraper
import json
from bs4 import BeautifulSoup
import time

def handlerFunction():
	print("AngelList website vedantu job data scrap and store into vedantuplatform DB Career Collection")
	client = MongoClient('mongodb://mongoadmin:zymx6A3JKAPM23Xw@ds021008-a1.mlab.com:21008/vedantuplatform');
	db = client['vedantuplatform'];
	print("got mongodb connection")
	jobs = db.Career;
	result= jobs.delete_many({});
	scraper = cloudscraper.create_scraper()
	jobsdata= scraper.get("https://angel.co/company/vedantu/jobs").content
	print("fetched html page")
	soup = BeautifulSoup(jobsdata, 'html.parser')
	liTag = soup.find_all("li", {"class": "job-listing-role s-vgPadBottom1_5 s-vgBottom2"})
	for tag in liTag:
	    divTags = tag.find_all("div", {"class": "group s-grid-colMd6 u-fontSize18 s-vgPadTop0_5"})
    	aTags = tag.find_all("a", {"class": "u-fontSize18"})
    	for eachaTags in aTags:       
    		print(eachaTags["href"])
	        sfe= scraper.get(eachaTags["href"]).content;
        	soupforlink = BeautifulSoup(sfe, 'html.parser');
        	data = json.loads(soupforlink.find('script', type='application/ld+json').text);
        	Compensation=(soupforlink.find("meta",{"name": "twitter:data2"},"content")["content"]).encode('utf-8','ignore');
        	SkillsTag = soupforlink.find_all("div",{"class": "s-vgBottom2'];"});

        	if len(SkillsTag)==3:
  	                Skills = SkillsTag[1].text
        	else:
                    Skills = ""
        	curentTime = int(time.time()*1000)
        	job ={
            	# 'compensation' : Compensation,
            	'skills' : Skills,
            	'technology' : divTags[0].text.replace("\n",''),
            	'title' : data['title'],
            	'description' :data['description'],
            	'createdBy' : 'aws-lambdas',
            	'lastUpdatedBy' : 'aws-lambdas',
            	'lastUpdated':curentTime,
            	'creationTime':curentTime
        	};
        	result = jobs.insert_one(job);