echo "started building for redshift ops"
echo "installing dependencies"
rm migratebranchdata.zip
rm -r psycopg2*
cd ../
pip install psycopg2 -t migratebranchdata/ 
cd migratebranchdata
zip -r migratebranchdata.zip .
rm -r psycopg2*
echo "create the file migratebranchdata.zip in the current directory"
