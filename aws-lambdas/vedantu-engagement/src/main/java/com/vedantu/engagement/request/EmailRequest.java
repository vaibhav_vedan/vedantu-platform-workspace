package com.vedantu.engagement.request;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.InternetAddress;

import org.springframework.stereotype.Service;

//import com.google.appengine.api.datastore.Text;
import com.vedantu.engagement.entity.AbstractEntity;
import com.vedantu.engagement.enums.EmailType;
import com.vedantu.engagement.pojo.EmailAttachment;

/*
 * Incoming request object for send Email API
 */
@Service
public class EmailRequest extends AbstractEntity {

	private ArrayList<InternetAddress> to;
	private ArrayList<InternetAddress> bcc;
	private ArrayList<InternetAddress> cc;
	private ArrayList<InternetAddress> replyTo;
	private String subject;
	private String body;
	private EmailAttachment attachment;
	private String receiverName;
	private EmailType type;

	public EmailRequest() {
		super();
	}

	public EmailRequest(ArrayList<InternetAddress> to, ArrayList<InternetAddress> bcc, ArrayList<InternetAddress> cc,
			ArrayList<InternetAddress> replyTo, String subject, String body, EmailAttachment attachment,
			String receiverName, EmailType type) {
		super();
		this.to = to;
		this.bcc = bcc;
		this.cc = cc;
		this.replyTo = replyTo;
		this.subject = subject;
		this.body = body;
		this.attachment = attachment;
		this.receiverName = receiverName;
		this.type = type;
	}

	public EmailType getType() {
		return type;
	}

	public void setType(EmailType type) {
		this.type = type;
	}

	public String getReceiverName() {

		return receiverName;
	}

	public void setReceiverName(String receiverName) {

		this.receiverName = receiverName;
	}

	public ArrayList<InternetAddress> getTo() {

		return to;
	}

	public void setTo(ArrayList<InternetAddress> to) {

		this.to = to;
	}

	public ArrayList<InternetAddress> getBcc() {

		return bcc;
	}

	public void setBcc(ArrayList<InternetAddress> bcc) {

		this.bcc = bcc;
	}

	public ArrayList<InternetAddress> getCc() {

		return cc;
	}

	public void setCc(ArrayList<InternetAddress> cc) {

		this.cc = cc;
	}

	public String getSubject() {

		return subject;
	}

	public void setSubject(String subject) {

		this.subject = subject;
	}

	public String getBody() {

		return body;
	}

	public void setBody(String body) {

		this.body = body;
	}

	public ArrayList<InternetAddress> getReplyTo() {
		return replyTo;
	}

	public void setReplyTo(ArrayList<InternetAddress> replyTo) {
		this.replyTo = replyTo;
	}

	public EmailAttachment getAttachment() {
		return attachment;
	}

	public void setAttachment(EmailAttachment attachment) {
		this.attachment = attachment;
	}

	@Override
	public String toString() {

		ArrayList<String> toAddresses = new ArrayList<String>();
		for (InternetAddress address : to) {
			toAddresses.add(address.getAddress());
		}

		return "Email to:" + toAddresses.toString() + ", subject:" + subject + ", body:" + body;
	}

	public static class Constants extends AbstractEntity.Constants {
		public static final String TO = "to";
		public static final String BODY = "body";
		public static final String SUBJECT = "subject";
		public static final String TYPE = "type";
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		int ccSize = 0, bccSize = 0;

		if (null == to || to.size() == 0) {
			errors.add(EmailRequest.Constants.TO);
		}

		if (cc != null)
			ccSize = cc.size();
		if (bcc != null)
			bccSize = bcc.size();

		if (to.size() + ccSize + bccSize > 50)
			throw new IllegalArgumentException(
					"Can only send a Bulk Email to a maximum of 50 emails (to + cc + bcc). Please reduce the size of the input");

		if (null == body) {
			errors.add(EmailRequest.Constants.BODY);
		}

		if (null == subject) {
			errors.add(EmailRequest.Constants.SUBJECT);
		}

		if (null == type) {
			errors.add(EmailRequest.Constants.TYPE);
		}

		return errors;
	}
}
