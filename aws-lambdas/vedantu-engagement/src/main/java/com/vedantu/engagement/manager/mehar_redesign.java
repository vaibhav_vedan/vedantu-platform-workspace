package com.vedantu.engagement.manager;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.vedantu.engagement.util.LogFactory;

import ca.pjer.ekmeans.EKmeans;

class WB //White Board
{
	String _id;
	String context;
	String socketId;
	int sentTime;
	String sessionId;
	long serverTime;
	String userId;
	long lastUpdated;
	long creationTime;
	String data;
	String type;
	long id;
	int __v;
	Data dat;
}
class uncompressedCoordinates
{
	int x;
	int y;
}
class Data
{
	String t;
	String type;
	String c;
	String color;
	int s;
	int size;
	int tm;				//time map
	int time;
	String id;
	int bw;             //width
	int boardWidth;
	int bh;				//height
	int boardHeight;
	String bi;			//page number
	String boardIndex;
	String p;
	String st;
	String en;
	String boardId;
	uncompressedCoordinates[] points;
	uncompressedCoordinates start;
	uncompressedCoordinates end;
	int index;
	long _ts;
	long _timeStamp;
	List<String> pstr = new ArrayList<String>();
	List<String> pstr1 = new ArrayList<String>();
	int[][] coordinates;

	public void uncompresssedToCompressed()
	{
		if(points!=null)
		{
			coordinates = new int[points.length][2];
			for(int i=0;i < points.length;i++)
			{
				coordinates[i][0]=points[i].x;
				coordinates[i][1]=points[i].y;
			}
		}
		else if(points==null && start!=null && end!=null)
		{
			coordinates = new int[2][2];
			coordinates[0][0] = start.x;
			coordinates[0][1] = start.y;
			coordinates[1][0] = end.x;
			coordinates[1][1] = end.y;
		}
		t=type;
		c=color;
		s=size;
		tm=time;
		bw=boardWidth;
		bh=boardHeight;
		bi=boardIndex;
	}


	public void strToInt()
	{
		if(p != null)
		{
			pstr = Arrays.asList(p.split(";"));
			String[][] pint = new String[pstr.size()][2];
			coordinates = new int[pstr.size()][2];
			for(int i=0;i< pstr.size();i++)
			{

				pint[i] = pstr.get(i).split(",");
				coordinates[i][0] = (int)Double.parseDouble(pint[i][0]);
				coordinates[i][1] = (int)Double.parseDouble(pint[i][1]);

			}

		}
		if(p == null  && st != null)
		{
			pstr = Arrays.asList(st.split(";"));
			String[][] pint = new String[2][2];
			coordinates = new int[2][2];

			pint[0] = pstr.get(0).split(",");
			coordinates[0][0] = (int)Double.parseDouble(pint[0][0]);
			coordinates[0][1] = (int)Double.parseDouble(pint[0][1]);

			pstr1 = Arrays.asList(en.split(";"));
			pint[1] = pstr1.get(0).split(",");
			coordinates[1][0] = (int)Double.parseDouble(pint[1][0]);
			coordinates[1][1] = (int)Double.parseDouble(pint[1][1]);
		}
	}
}
class Proximity 
{
	public ArrayList<Double> write(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, int num_pages, String tid, String sid) throws IOException
	{
		ArrayList<Double> final_den = new ArrayList<Double>();
		ArrayList<Double> final_space = new ArrayList<Double>();
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			int [][]grid = new int[902][600];
			for(int i =0; i< 902 ;i++)
			{
				for(int j =0 ;j < 600 ;j++)
				{
					grid[i][j] = 0;
				}
			}
			double[][] points = new double[1000000][2];
			int p_index = 0;
			Integer page_num = page_num_entry.getKey();
			double area = 0;
			double density = 0;
			ArrayList<Double> den = new ArrayList<Double>();

			int temp_points = 0;
			int avg_pixel = 0;
			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				if(userId.getKey().equals(tid))
				{	
					ArrayList<WB> white_board = new ArrayList<WB>();
					white_board = userId.getValue();
					for(int i =0 ;i < white_board.size();i++)
					{
						if(white_board.get(i).type.equals("CREATE") && white_board.get(i).dat.t.equals("DRAW"))
						{
							int num_points = white_board.get(i).dat.coordinates.length;

							for(int m =0 ;m <num_points ;m++)
							{
								if(white_board.get(i).dat.coordinates[m][0] > 0 &&  white_board.get(i).dat.coordinates[m][1] > 0 && white_board.get(i).dat.coordinates[m][0] <= 902 && white_board.get(i).dat.coordinates[m][1] <= 600 )
								{
									try
									{
										avg_pixel += white_board.get(i).dat.s;
									}
									catch(Exception e)
									{

									}
									if(grid[white_board.get(i).dat.coordinates[m][0]-1][white_board.get(i).dat.coordinates[m][1]-1] == 0)
									{
										temp_points++;
										grid[white_board.get(i).dat.coordinates[m][0]-1][white_board.get(i).dat.coordinates[m][1]-1] = 1;
										points[p_index][0] = white_board.get(i).dat.coordinates[m][0];
										points[p_index++][1] = white_board.get(i).dat.coordinates[m][1];
									}

								}
							}
						}
					}
				}
			}
			Empty empty = new Empty();
			double space = empty.empty_space(grid,902,600);
			final_space.add(space);
			int k = 10; // the number of clusters
			if(temp_points <= 500)
			{
				num_pages-- ;
				continue;
			}
			avg_pixel/= temp_points;
			if(avg_pixel <= 2)
				avg_pixel = 2;
			int n = temp_points; // the number of points to cluster
			double[][] centroids = new double[k][2];
			int div = temp_points/k;

			for (int i = 0; i < k ; i++) {
				centroids[i][0] = points[(div-i)*k-1][0];
				centroids[i][1] = points[(div-i)*k-1][1];
			}
			EKmeans eKmeans = new EKmeans(centroids, points);
			eKmeans.setIteration(1000);
			eKmeans.setDistanceFunction(EKmeans.EUCLIDEAN_DISTANCE_FUNCTION);
			eKmeans.run();
			int[] assignments = eKmeans.getAssignments();
			ArrayList<ArrayList<Integer>> clusters = new ArrayList<ArrayList<Integer>>();
			for(int i =0 ;i<k ;i++)
			{
				clusters.add(new ArrayList<Integer>());
			}

			for (int i = 0; i < n; i++) 
			{
				clusters.get(assignments[i]).add(i);
			}	
			for(int i =0 ;i< k ; i++)
			{
				double x_avg =0;
				double y_avg =0;
				double radius = Integer.MIN_VALUE;
				if(clusters.get(i).size() < 10)
				{
					continue;
				}

				for(int j =0 ; j< clusters.get(i).size() ;j++)
				{
					int index = clusters.get(i).get(j);
					y_avg += points[index][1];
					x_avg += points[index][0];
				}
				x_avg/=clusters.get(i).size();
				y_avg/=clusters.get(i).size();
				for(int j =0 ; j < clusters.get(i).size();j++)
				{
					int index = clusters.get(i).get(j);
					double x = (x_avg - points[index][0]);
					double y = (y_avg - points[index][1]);
					double square = x*x+y*y;

					if(square > radius)
						radius = square; //radius is already square of distance (r^2)
				}
				if(radius > 0)
				{
					area = Math.PI*radius;
					density = clusters.get(i).size()/area;
					if(density * 1000 < 30)
						den.add(density);
				}
				else
				{					
					continue;
				}
			}
			// We have the density of all the clusters in a single page 
			double temp_density =0;

			for(int i =0 ;i <den.size();i++)
			{
				temp_density+= den.get(i);
			}
			if(den.size() == 0)
			{
				continue;
			}
			//			temp_density/= den.size();
			//			temp_density*= 1000;

			//------------- for taking median of the data instead of mean ----------------------------

			Collections.sort(den);
			//			for(int i =0 ;i <den.size() ;i ++)
			//			{
			//				System.out.println("Density of Cluster " + i + "is "+ den.get(i)*1000 );
			//			}

			if(den.size() % 2 == 0)
			{
				temp_density = (den.get(den.size()/2) + den.get(den.size()/2-1))/2;
			}
			else
			{
				temp_density = den.get(den.size()/2);
			}
			temp_density = temp_density *1000;
			if(temp_density < 100)
			{
				final_den.add(temp_density);
			}
			den.clear();

		}
		double avg = 0.0;
		for(int i = 0 ; i< final_space.size(); i++)
		{
			avg += final_space.get(i);
		}
		avg/= final_space.size();
		avg = avg*100.0/492;
		double final_density = 0;
		ArrayList<Double> values = new ArrayList<Double>();
		values.add(avg);
		Collections.sort(final_den);
		int sizeFinalDensity = final_den.size();
		if(sizeFinalDensity == 0)
		{
			values.add(final_density);
			return values;
			//return final_density;
		}
		else if(sizeFinalDensity == 1)
		{
			values.add(final_den.get(0));

			return values;
			//return final_den.get(0);
		}

		if(sizeFinalDensity % 2 ==0 && sizeFinalDensity !=0)
		{
			final_density = (final_den.get(sizeFinalDensity/2)+final_den.get(sizeFinalDensity/2-1))/2;
		}
		else
		{
			if(sizeFinalDensity !=0)
			{
				final_density = final_den.get(sizeFinalDensity/2);
			}
		}
		//		System.out.println("avg blocks used  = "+avg );
		System.out.println("Density of the session is = "+final_density);

		values.add(final_density);
		return values;
	}

}

public class mehar_redesign {

	@Autowired
	private LogFactory logfactory;

	@SuppressWarnings("static-access")
	private Logger logger = logfactory.getLogger(mehar_redesign.class);

	int index;
	int newIndex;
	String tid;
	String sid;
	String id;
	Analysis analysis = new Analysis();
	ShapeImageAnalysis shapeimageAnalysis = new ShapeImageAnalysis(0,0,0,0,0);
	ArrayList<Long> image_time = new ArrayList<Long>();
	Long start;
	Long end;
	BufferedReader buf;
	Gson gson;
	Type collectionType;
	ArrayList<WB> wb;
	HashMap<Integer,HashMap<String,ArrayList<WB>>> pages;
	int [][]points_time_map;
	int[] use;
	ArrayList<Integer> time_map;
	double wb_silence;
	Proximity proximity;
	int num_pages=0;
	ArrayList<Long> pageTimeMap = new ArrayList<Long>();
	//--------------------------------------------------Initialization completed  -----------------------------------------------------------------
	public ArrayList<String> check(String args,String[] ids, String wbdataurl) throws FileNotFoundException,Exception {
		// ------------------------------------------------- Automation Part --------------------------------------------------------------------------		
		long startTime = System.nanoTime();
		tid = ids[10];
		sid =ids[11];
		ArrayList<String> testcsv = new ArrayList<>(); 
		// --------------------------------------------------getting data from url------------------------------------------------------------------- 		
		BufferedReader reader = null;
		URLConnection conn = null;
		InputStream is = null;
		GZIPInputStream gZIPInputStream = null;
		InputStreamReader inputStreamReader = null;

		StringBuilder sb = new StringBuilder();
		JsonArray result;
		try {
			String SessionId = args;
			String urlString = "https://ns1.vedantu.com/getSessionDataUrl?url="+SessionId+"/whiteboard.json";
			URL url = new URL(urlString);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("X-Ved-Client", "ENGAGEMENT_ANALYSIS");
			con.setRequestProperty("X-Ved-Client-Id", "C2B8B");
			con.setRequestProperty("X-Ved-Client-Secret", "qfUD85nZmj749DO8zv66rnrGuVMcQab5");
			
			reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

			int read;
			char[] chars = new char[1];
			while ((read = reader.read(chars)) != -1)
			{	
				sb.append(chars);				
			}

			String str = sb.toString();

			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(str).getAsJsonObject();
			JsonObject tempResult = obj.getAsJsonObject("result");
			String signedUrl = tempResult.get("url").getAsString();
			logger.info(signedUrl);

			try
			{
				conn = new URL(signedUrl).openConnection();
				is = conn.getInputStream();
				gZIPInputStream = new GZIPInputStream(is);
				inputStreamReader = new InputStreamReader(gZIPInputStream);
				reader = new BufferedReader(inputStreamReader);
			}
			catch(Exception e)
			{
				logger.info(e.getMessage());
			}
			String readed;
			sb = new StringBuilder();
			while ((readed = reader.readLine()) != null) {
				sb.append(readed);
			}
			obj = parser.parse(sb.toString()).getAsJsonObject();
			result = obj.getAsJsonArray("result");

		} catch(Exception e){
			
			logger.info(e.getMessage(),e);
			return null;
		}
		finally {

			if (reader != null)
				reader.close();

			if(is != null)
				is.close();

			if(gZIPInputStream != null)
				gZIPInputStream.close();
		}
		logger.info("white board data fetched from url");
		//---------------------------------------------------------------------------------------------------------------------------------------------		
		gson = new GsonBuilder().create();
		collectionType = new TypeToken<ArrayList<WB>>() {
		}.getType();

		wb = gson.fromJson(result, collectionType);
		try
		{
			for(int i =wb.size()-1; i >= 0;i--)
			{
				if(wb.get(i).data.startsWith("{"))
				{
					wb.get(i).dat = gson.fromJson(wb.get(i).data,Data.class);

					if(wb.get(i).dat.type!=null)
					{
						wb.get(i).dat.uncompresssedToCompressed();
					}
					else
					{
						wb.get(i).dat.strToInt();
					}
				}
				else {
					wb.remove(i);
				}
			}
		}
		catch(Exception e)
		{
			return null;
		}
		index = 0;
		id ="";

		pages = new HashMap<Integer,HashMap<String,ArrayList<WB>>>();
		
		if(!(wb.get(0).serverTime - wb.get(wb.size()-1).serverTime > 0))
		{
			Collections.reverse(wb);
		}
		
		long audio_start;// = Long.parseLong(ids[4]);
		if(ids[4] != null)
			audio_start = Long.parseLong(ids[4]);
		else 
			audio_start =0;
		long wb_start;// = wb.get(wb.size()-1).serverTime;
		if(wb.size()-1 > 5)
			wb_start = wb.get(wb.size()-1).serverTime;
		else 
			wb_start=0;
		long delta = audio_start - wb_start;
		long active_duration = 0;
		if(wb.size()>5)
			active_duration= (wb.get(0).serverTime - wb.get(wb.size()-1).serverTime);

		newIndex = 1;
		int[] indexMap = new int[100]; //max whiteboard pages
		indexMap[0] = 0;
		indexMap[1] = 1;
		int pos = 1;
		for(int i = wb.size()-1;i>=0;i--)
		{ 
			try
			{
				index = Integer.parseInt(wb.get(i).dat.bi);
			}
			catch(NullPointerException e)
			{

				logger.info("board id is null");

				continue;
			}
			catch(Exception e)
			{
				continue;
			}
			try
			{
				if(wb.get(i).type.equals("CHANGEBOARD") || wb.get(i).type.equals("REDRAWALL")) 
				{
					newIndex = indexMap[index];

					continue;
				}
				else if(wb.get(i).type.equals("CREATEBOARD"))
				{
					pageTimeMap.add(wb.get(i).serverTime);


					newIndex = pages.size()+1;
					indexMap[++pos] = newIndex;
					continue;
				}

				else if(wb.get(i).type.equals("REMOVEALL"))
				{
					System.out.println(wb.get(i)._id);

					newIndex = pages.size()+1;
					indexMap[index] = newIndex;
					continue;
				}
				else if(wb.get(i).type.equals("REMOVE") || wb.get(i).dat.t.equals("ERASER"))
				{
					continue;
				}
				id = wb.get(i).userId;
			}
			catch(Exception e)
			{
				System.out.println(wb.get(i)._id);
				e.printStackTrace();
				continue;
			}
			if(pages.get(newIndex)==null)
			{
				pages.put(newIndex, new HashMap<String, ArrayList<WB>>());
			}
			if(pages.get(newIndex).get(id)==null)
			{
				pages.get(newIndex).put(id, new ArrayList<WB>());
			}

			pages.get(newIndex).get(id).add(wb.get(i));
		}

		// ---------------------------------------------------------- csv ---------------------------------------------------------------------------				
		num_pages = pages.size();
		logger.info(num_pages);
		logger.info("Analysis Started");	
		testcsv = analysis.page_analysis(pages, tid, sid);
		logger.info("Basic Page Analysis Done");
		try
		{
			start = wb.get(wb.size()-1).serverTime;
			logger.info("start = "+start);
			end = wb.get(0).serverTime;
			logger.info("end = "+end);
		}
		catch(java.lang.ArrayIndexOutOfBoundsException e)
		{
			ArrayList<String> empty = new ArrayList<String>();
			return empty;
		}
		shapeimageAnalysis.shapeVicinity(pages, tid, sid);
		double annotation = 0;


		time_map = analysis.Time_Map(pages, tid, sid, start, end);
		points_time_map = analysis.Count_Time_Map(pages, tid, sid, start, end);
		use = analysis.UserWBUsage(time_map);
		testcsv.add(Integer.toString(time_map.size()));

		testcsv.add(Integer.toString(use[0]));

		testcsv.add(Integer.toString(use[1]));

		wb_silence = analysis.WhiteBoardSilence(time_map);
		testcsv.add(Double.toString(wb_silence*100));


		try
		{
			proximity = new Proximity();
		}
		catch(Exception e)
		{
			logger.info(e.getMessage());
		}
		testcsv.add(Integer.toString(pos));
		ArrayList<Double> values = proximity.write(pages, num_pages, tid, sid);

		Variance variance = new Variance();
		double var = variance.slope(pages, tid);

		logger.info("density of the session is "+values.get(1).toString());
		testcsv.add(Double.toString(values.get(1)));

		testcsv.add(0,Long.toString(active_duration/1000));
		testcsv.add(1,Long.toString(delta/1000));
		int shapes = Integer.parseInt(testcsv.get(2))+Integer.parseInt(testcsv.get(3))+Integer.parseInt(testcsv.get(4))+Integer.parseInt(testcsv.get(5));
		testcsv.add(Integer.toString(shapes));
		testcsv.add(testcsv.get(6));
		testcsv.add(testcsv.get(7));
		testcsv.add(testcsv.get(8));
		double space_usage = Double.parseDouble(testcsv.get(10))*100.0;
		testcsv.add(Double.toString(space_usage));
		testcsv.add(testcsv.get(12));
		testcsv.add(testcsv.get(13));
		testcsv.add(testcsv.get(14));
		testcsv.add(testcsv.get(15));
		testcsv.add(testcsv.get(16));
		if(testcsv.get(5).equals("0"))
			testcsv.add("0");
		else
			testcsv.add("1");
		testcsv.add(Double.toString(var));
		testcsv.add(Double.toString(shapeimageAnalysis.imageCount));

		//		testcsv.add(Double.toString(shapeimageAnalysis.ellipCount+shapeimageAnalysis.rectCount+shapeimageAnalysis.rightTrianCount+shapeimageAnalysis.trianCount));

		logger.info("testcsv size is "+testcsv.size());
		return testcsv;

	}
}
