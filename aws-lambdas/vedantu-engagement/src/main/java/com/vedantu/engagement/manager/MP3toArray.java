package com.vedantu.engagement.manager;


import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.engagement.util.LogFactory;

public class MP3toArray {
	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(MP3toArray.class);
	
	public ArrayList<Double> x= new ArrayList<Double>();
	
	public void change(String str,byte[] audioByte,int wav)
	{
	File src = new File(str);
 	try {
		if(wav==0)
		{
		    AudioFormat tempFormat= new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, 8000, 16, 1, 2, 8000, false);
		
		    byte[] temp1 = getAudioDataBytes(audioByte, tempFormat);
		   
		    int k=0,temp;
		  // ArrayList<Double> x= new ArrayList<Double>();
		    while (k<temp1.length) {
		        temp = 0;
		        temp = temp1[k+1];
		        temp <<= 8;
		        temp |= (0x000000FF & temp1[0]);
		        x.add(new Double(temp));
		        k=k+2;
		        
		    }
		}
		else
		{
			BufferedInputStream bfread = new BufferedInputStream(new FileInputStream(src));
		    byte[] buffer= new byte[2];
		    int temp;
		    bfread.skip(44);
		    while (bfread.read(buffer) >= 0) {
                temp = 0;
                temp = buffer[1];
                temp <<= 8;
                temp |= (0x000000FF & buffer[0]);
                x.add(new Double(temp));
            }
            bfread.close();
		}
    } catch (UnsupportedAudioFileException e) {
		// TODO Auto-generated catch block
		logger.error("UnsupportedAudioFileException");
	} catch (IOException e) {
		// TODO Auto-generated catch block
		logger.error("IOException");
	} catch (IllegalArgumentException e) {
		// TODO Auto-generated catch block
		logger.error("IllegalArgumentException");
	} catch (Exception e) {
		// TODO Auto-generated catch block
		logger.error("IllegalArgumentException");
	}
	//return x;

}
public byte [] getAudioDataBytes(byte [] sourceBytes, AudioFormat audioFormat) throws UnsupportedAudioFileException, IllegalArgumentException, Exception {
    if(sourceBytes == null || sourceBytes.length == 0 || audioFormat == null){
        throw new IllegalArgumentException("Illegal Argument passed to this method");
    }

    try (final ByteArrayInputStream bais = new ByteArrayInputStream(sourceBytes);
         final AudioInputStream sourceAIS = AudioSystem.getAudioInputStream(bais)) {
        AudioFormat sourceFormat = sourceAIS.getFormat();
        AudioFormat convertFormat = new AudioFormat(AudioFormat.Encoding.PCM_SIGNED, sourceFormat.getSampleRate(), 16, sourceFormat.getChannels(), sourceFormat.getChannels()*2, sourceFormat.getSampleRate(), false);
        try (final AudioInputStream convert1AIS = AudioSystem.getAudioInputStream(convertFormat, sourceAIS);
             final AudioInputStream convert2AIS = AudioSystem.getAudioInputStream(audioFormat, convert1AIS);
             final ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            byte [] buffer = new byte[8192];
            while(true){
                int readCount = convert2AIS.read(buffer, 0, buffer.length);
                if(readCount == -1){
                    break;
                }
                baos.write(buffer, 0, readCount);
            }
            return baos.toByteArray();
        }
    }
}
	
}
