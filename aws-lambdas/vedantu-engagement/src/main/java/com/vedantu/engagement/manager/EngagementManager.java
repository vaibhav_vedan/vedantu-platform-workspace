package com.vedantu.engagement.manager;

import java.util.ArrayList;

import javax.mail.internet.InternetAddress;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

//import com.google.appengine.api.datastore.Text;
import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.vedantu.engagement.dao.serializers.EngagementDAO;
import com.vedantu.engagement.enums.EmailType;
import com.vedantu.engagement.util.ConfigUtils;
import com.vedantu.engagement.util.LogFactory;
//import com.amazonaws.services.lambda.runtime.Context;
import com.vedantu.engagement.viewobject.response.EngagementAnalysis;
import com.vedantu.engagement.pojo.EmailAttachment;
import com.vedantu.engagement.request.EmailRequest;

@Service
public class EngagementManager {

	private Client client = Client.create();

	//	@Autowired
	//	private EmailRequest emailRequest;
	//
	//	@Autowired
	//	private EmailManager emailManager;

	@Autowired
	private LogFactory logfactory;

	@SuppressWarnings("static-access")
	private Logger logger = logfactory.getLogger(EngagementManager.class);

	@Autowired
	private EngagementDAO engagementDAO;


	@Autowired
	private ConfigUtils configUtils;

	public String beginUrl;
	public String payloadUserId;
	public String wbdataurl;
	public String location;
	public String emailUrl;

	public EngagementManager() {
		super();
		configUtils = new ConfigUtils();
	}
	public Boolean subject(String subject)
	{
		if(subject.equalsIgnoreCase("Mathematics")||subject.equalsIgnoreCase("Physics")||subject.equalsIgnoreCase("Chemistry")||subject.equalsIgnoreCase("Biology"))
			return true;
		else
		{
			logger.info("This Subject is not in our analysis yet. No mail will be sent");
			return false;
		}
	}
	public EngagementAnalysis getEngagement(Long id) throws Exception {


		beginUrl = configUtils.getStringValue("Url");
		payloadUserId = configUtils.getStringValue("userId");
		wbdataurl = configUtils.getStringValue("wburl");
		emailUrl = configUtils.getStringValue("email.url");
	    
		Engag engage = new Engag();
		EngagementAnalysis engagementAnalysis = new EngagementAnalysis();
		engagementDAO = new EngagementDAO();
		System.out.println("The string id is ="+Long.toString(id));
		logger.info("It is working");
		//engagementAnalysis = engagementDAO.getById(Long.toString(id));

		engagementAnalysis = engage.getEngage(id, beginUrl, payloadUserId, wbdataurl);
		if(engagementAnalysis == null)
		{
			return null;
		}
		engagementAnalysis.setServerTime(engagementAnalysis.getSessionStartTime());

		
		logger.info("The _id is "+engagementAnalysis.get_id());
		engagementDAO.create(engagementAnalysis);
		logger.info("DB populated");


		if(engagementAnalysis.getAnalysable() == true && (subject(engagementAnalysis.getSubject())) &&(engagementAnalysis.getQuality().equals("Bad")||engagementAnalysis.getQuality().equals("Bad (WB only)")))
		{
			CsvWriter csv = new CsvWriter();
			StringBuilder sb = new StringBuilder();
			csv.exportCsv(engagementAnalysis,"Alert.csv");
			sb = csv.sb;
// try
			String sid = "SessionId : <b>"+engagementAnalysis.get_id() +"</b> <br>";
			String sub = "Subject : <b>"+engagementAnalysis.getSubject() +"</b><br>";
			String teacher = engagementAnalysis.getTeacherName().toUpperCase();
			String student = engagementAnalysis.getStudentName().toUpperCase();
			String tname= "Teacher : <b>"+teacher +"</b><br>";
			String sname= "Student : <b>"+student +"</b><br>";
			
			
			int activeDuration = (int)(Double.parseDouble(engagementAnalysis.getActiveDuration())*60.0);
			
			String duration = "Active Duration : <b>"+Integer.toString(activeDuration)+" minutes</b> (Scheduled : <b>" + engagementAnalysis.getScheduledTime()+" minutes</b>)<br>" ;
			String audio = "Audio : <b>"+ engagementAnalysis.getAudioPresent()+".</b><br>";
			int stot = (int)(100.0*engagementAnalysis.getScoreTotal());
			Double score = (double) (stot/100.0);
			int sint = (int)(100.0*engagementAnalysis.getScoreInteraction());
			Double scoreInteraction = (double) (sint/100.0);
			String scInter;
			if(engagementAnalysis.getAudioPresent().equals("Audio Absent"))
			{
				scInter = "NA";
			}
			else scInter = scoreInteraction.toString() + "/10";
			String acadTab = "<table border=\"1\" style=\"width:40%\">\r\n  <tr>\r\n    <td>Overall Score</td>\r\n    <td><b>"+score.toString()+"/10</b></td> \r\n  </tr>\r\n<tr>\r\n    <td>Content Usage</td>\r\n    <td><b>"+engagementAnalysis.getScoreContentUsage()+"/10</b></td> \r\n    </tr>\r\n  <tr>\r\n    <td>WhiteBoard Usage</td>\r\n    <td><b>"+engagementAnalysis.getScoreWhiteBoardUsage()+"/10</b></td> </tr>\r\n  <tr>\r\n    <td>Interaction</td>\r\n    <td><b>"+scInter+"</b></td></tr>\r\n</table>";
			String sTech,tTech,acadRating;
			if(engagementAnalysis.getStudentTechRating().equals("-1"))
			{
				sTech = "NA";
				tTech = "NA";
			}
			else {
				sTech = engagementAnalysis.getStudentTechRating();
				tTech = engagementAnalysis.getTeacherTechRating();
			}
			if(engagementAnalysis.getAcadRating().equals("-1"))
				acadRating  = "NA";
			else acadRating = engagementAnalysis.getAcadRating();
			String studentRating = "Student Rated : <br>"+"Technology : <b>"+sTech + "</b> Academic : <b>"+acadRating+"</b><br>";
			String teacherRating = "Teacher Rated Technology : <b>" + tTech+ "</b><br>" ;
			
			String rep = "<a href=\"https://www.vedantu.com/v/replaysession/"+engagementAnalysis.get_id()+"\">View Replay</a>";
			String techLogs = "<a href=\"https://www.vedantu.com/v/timelinenew/"+engagementAnalysis.get_id()+"\">View Session Logs</a>";
			String attach = "Please check the attached document for more Details.";
			
			String audioReason;
			if(engagementAnalysis.getAudioPresent().equalsIgnoreCase("Audio Absent"))
				audioReason = "<br> Audio Reason : "+ engagementAnalysis.getAudioReason()+"<br>";
			else audioReason="";
			
			ArrayList<InternetAddress> to = new ArrayList<InternetAddress>();
			to.add(new InternetAddress(configUtils.getStringValue("email.to")));
			ArrayList<InternetAddress> cc = new ArrayList<InternetAddress>();
			
			cc.add(new InternetAddress(configUtils.getStringValue("email.cc")));
			String body = (sid+sub+tname+sname+duration+audio+audioReason+"<br>Academics<br>"+acadTab+"<br><br>"+studentRating+teacherRating+rep+"<br><br>"+techLogs+"<br><br>"+attach);//"<b>Session with sessionId</b> "+id.toString() + " and teacher "+engagementAnalysis.getTeacherName()+ " and student "+engagementAnalysis.getStudentName() + " needs review.\n\n");
			
			EmailAttachment attachment = new EmailAttachment("Alert.csv", new Object(), "sample");
			attachment.setApplication("application/csv");
			attachment.setAttachmentData(sb.toString());



			EmailType type = EmailType.USER_MESSAGE;


			EmailRequest emailRequest = new EmailRequest();
			emailRequest.setTo(to);
			emailRequest.setCc(cc);
			emailRequest.setBody(body);
			emailRequest.setSubject("Alert! Bad Session. Needs review");
			emailRequest.setType(type);
			emailRequest.setAttachment(attachment);
			Gson gson = new Gson();
			String json = gson.toJson(emailRequest);
//			System.out.println("EMAIL URL ="+emailUrl);
			
	/*		String notificationEndpoint= configUtils.getStringValue("NOTIFICATION_ENDPOINT");
			logger.info("endpoint ="+notificationEndpoint);
			ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST, json);
			logger.info(resp.getEntity(String.class));
		*/	
			String downloadurl = beginUrl+"/platform/notification-centre/email/sendEmail";//"/_ah/api/sessionendpoint/v1/getsessioninfores?alt=json";	
//			String payload = "{id: \""+sessionId+"\", callingUserId: "+payloadUserId+"}";			//  5629499534213120
//			URL newobj = new URL(downloadurl);
//			HttpURLConnection con = (HttpURLConnection) newobj.openConnection();

			postPlatformData(downloadurl, json, true, HttpMethod.POST);
			
//			ClientResponse resp = WebUtils.INSTANCE.doCall(emailUrl,HttpMethod.POST , json);
//			logger.info(resp.getEntity(String.class));
//			postPlatformData(emailUrl, json, true, HttpMethod.POST);
		}

		return engagementAnalysis;

	}

	public void postPlatformData(String location, String body, boolean isJsonReq, HttpMethod httpMethod) {

		WebResource webResource = client.resource(location);
		client.setReadTimeout(30000);
		
		webResource.setProperty("X-Ved-Client", "ENGAGEMENT_ANALYSIS");
		webResource.setProperty("X-Ved-Client-Id", "C2B8B");
		webResource.setProperty("X-Ved-Client-Secret", "qfUD85nZmj749DO8zv66rnrGuVMcQab5");
		ClientResponse response = null;
		switch (httpMethod) {
		case POST:
			response = webResource.type("application/json").post(ClientResponse.class, body);
			break;
		case PUT:
			response = webResource.type("application/json").put(ClientResponse.class, body);
			break;
		case GET:
			response = webResource.get(ClientResponse.class);
			break;
		default:
			break;
		}

		String output = response.getEntity(String.class);
		System.out.println("OUTPUT = "+output);
		if (response.getStatus() != 200 && response.getStatus() != 201) {
			logger.info("Its a failure");
			logger.info("failure case response "+response.toString());

			// throw new RuntimeException(output);
		} else {
			logger.info("request sent");
			logger.info("success response "+response.toString());
		}
	}

}
