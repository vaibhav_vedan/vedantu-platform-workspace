package com.vedantu.engagement.dao.serializers;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import com.vedantu.engagement.viewobject.response.EngagementAnalysis;

@Service
public class EngagementDAO extends AbstractEngagementDAO {

    public EngagementDAO() {
        super();
    }

    public void create(EngagementAnalysis p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public EngagementAnalysis getById(String id) {
        EngagementAnalysis engagementAnalysis = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                engagementAnalysis = (EngagementAnalysis) getEntityById(id, EngagementAnalysis.class);
            }
        } catch (Exception ex) {
            engagementAnalysis = null;
        }
        return engagementAnalysis;
    }

    public void update(EngagementAnalysis p, Query q, Update u) {
        try {
            if (p != null) {
                updateEntity(p, q, u);
            }
        } catch (Exception ex) {
        }
    }

    public List<EngagementAnalysis> getAll() {
        List<EngagementAnalysis> result = getEntities(EngagementAnalysis.class);
        return result;
    }
}
