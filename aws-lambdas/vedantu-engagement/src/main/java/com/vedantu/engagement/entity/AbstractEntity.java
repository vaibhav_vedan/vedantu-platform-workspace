package com.vedantu.engagement.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.annotation.Id;

/*
 * Base Entity class for all incoming requests
 */
public class AbstractEntity {
	@Id
	private String id;
	private Long creationTime;
	private String createdBy;
	private Long lastUpdated;
	private String callingUserId;

	public AbstractEntity() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public Long getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Long lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCallingUserId() {
		return callingUserId;
	}

	public void setCallingUserId(String callingUserId) {
		this.callingUserId = callingUserId;
	}
	
	public void verify() throws IllegalArgumentException {

		List<String> verificationErrors = collectVerificationErrors();
		if (verificationErrors.size() != 0) {
			throw new IllegalArgumentException("Illegal Arguments with fields " + verificationErrors);
		}
	}

	protected List<String> collectVerificationErrors() {

		return new ArrayList<String>();
	}

	public static class Constants {
		public static final String ID = "id";
		public static final String CREATION_TIME = "creationTime";
		public static final String LAST_UPDATED = "lastUpdated";
		public static final String CREATED_BY = "createdBy";
		public static final String CALLING_USERID = "callingUserId";
	}
}
