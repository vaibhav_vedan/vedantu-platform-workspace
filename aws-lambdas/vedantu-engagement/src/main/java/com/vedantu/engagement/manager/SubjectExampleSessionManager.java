package com.vedantu.engagement.manager;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
//import com.vedantu.engagement.dao.serializers.EngagementDAO;
import com.vedantu.engagement.dao.serializers.SubjectExampleSessionDAO;
import com.vedantu.engagement.entity.SubjectExampleSession;
import com.vedantu.engagement.util.LogFactory;

@Service
public class SubjectExampleSessionManager {
		
	@Autowired
	private SubjectExampleSessionDAO subjectExampleSessionDAO;

	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(SubjectExampleSessionManager.class);

	public void createSubjectExampleSession(String message)
	{
		subjectExampleSessionDAO = new SubjectExampleSessionDAO();
		String parts[] = message.split("\\,");
		SubjectExampleSession subjectExampleSession = new SubjectExampleSession();
		if(parts[0].equalsIgnoreCase("2"))
		{
			subjectExampleSession.setSubject(parts[1]);
			subjectExampleSession.setSessionId(parts[2]);
			subjectExampleSession.setStartTime(System.currentTimeMillis());
			logger.info("Checking Example : " + message);
			logger.info("Checking Split : " + parts[0]+"   "+ parts[1]+"   " + parts[2]+"   ");
		}
		subjectExampleSessionDAO.create(subjectExampleSession);
	}
}