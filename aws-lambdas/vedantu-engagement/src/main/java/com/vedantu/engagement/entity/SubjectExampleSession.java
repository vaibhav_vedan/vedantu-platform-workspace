package com.vedantu.engagement.entity;

import com.vedantu.engagement.entity.AbstractEntity;

public class SubjectExampleSession extends AbstractEntity{

	
	private String subject;
	private Long startTime;
	private String sessionId;
	
	
	public SubjectExampleSession() {
		super();

		// TODO Auto-generated constructor stub
	}


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public Long getStartTime() {
		return startTime;
	}


	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}


	public String getSessionId() {
		return sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	
}