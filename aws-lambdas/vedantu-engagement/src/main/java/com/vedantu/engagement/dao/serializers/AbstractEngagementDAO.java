package com.vedantu.engagement.dao.serializers;

import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.mongodb.WriteResult;
import com.mongodb.client.result.DeleteResult;
import com.vedantu.engagement.viewobject.response.EngagementAnalysis;

@Service
public class AbstractEngagementDAO {


    public static final long NO_START = 0;
    public static final long NO_LIMIT = -1;
    public static final long UNINITIALIZED = -1L;

    @Autowired
    private static AbstractMongoConfiguration abstractMongoConfiguration = new AbstractMongoConfiguration();

    public AbstractEngagementDAO() {
        super();
    }

    protected static <E extends EngagementAnalysis> void saveEntity(E entity) {

        abstractMongoConfiguration.getMongoOperations().save(entity, entity.getClass().getSimpleName());
    }

    protected static <E extends EngagementAnalysis> void saveAllEntities(Collection<E> entities, String collectionName) {
        //setEntityDefaultProperties(entities);
        abstractMongoConfiguration.getMongoOperations().insert(entities, collectionName);
    }

    protected static <E extends EngagementAnalysis> void updateEntity(E entity, Query q, Update u) {
        //	setEntityDefaultProperties(entity);
        abstractMongoConfiguration.getMongoOperations().upsert(q, u, entity.getClass().getSimpleName());
    }

    public <T extends EngagementAnalysis> List<T> getEntities(Class<T> calzz) {
        return abstractMongoConfiguration.getMongoOperations().findAll(calzz, calzz.getSimpleName());
    }

    public <T extends EngagementAnalysis> T getEntityById(String id, Class<T> calzz) {
        Query query = new Query(Criteria.where("_id").is(id));
        return abstractMongoConfiguration.getMongoOperations().findOne(query, calzz, calzz.getSimpleName());
    }

    public <T extends EngagementAnalysis> List<T> runQuery(Query query, Class<T> calzz) {
        return abstractMongoConfiguration.getMongoOperations().find(query, calzz, calzz.getSimpleName());
    }

    public <T extends EngagementAnalysis> int queryCount(Query query, Class<T> calzz) {
        return abstractMongoConfiguration.getMongoOperations().find(query, calzz, calzz.getSimpleName()).size();
    }

    public <T> long deleteEntityById(String id, Class<T> calzz) {
        Query query = new Query(Criteria.where("_id").is(id));
        DeleteResult result = abstractMongoConfiguration.getMongoOperations().remove(query, calzz,
                calzz.getSimpleName());
        return result.getDeletedCount();
    }

}
