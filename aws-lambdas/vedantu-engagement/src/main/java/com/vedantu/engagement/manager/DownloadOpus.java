package com.vedantu.engagement.manager;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vedantu.engagement.util.LogFactory;


public class DownloadOpus {

	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(DownloadOpus.class);
	
	public int[] Download(String awsString,String fileName,String wbdataurl){
		
		int[] response = new int[3];
		int tno=0,sno=0;
	//	for(int i=0;i<filedata.data.size();i++ ){
		BufferedReader reader=null;
	     StringBuilder sb = new StringBuilder();
			JsonObject result;
		

			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			try {
			
				logger.info("Downloading Opus");
				String urlString = wbdataurl+"/getReplayUrl?url="+awsString;
				URL url;
				url = new URL(urlString);
				HttpURLConnection con = (HttpURLConnection) url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("X-Ved-Client", "ENGAGEMENT_ANALYSIS");
				con.setRequestProperty("X-Ved-Client-Id", "C2B8B");
				con.setRequestProperty("X-Ved-Client-Secret", "qfUD85nZmj749DO8zv66rnrGuVMcQab5");
				
				reader = new BufferedReader(new InputStreamReader(con.getInputStream()));

					
				
		//		reader = new BufferedReader(new InputStreamReader(url.openStream()));

				int read;
				char[] chars = new char[1];
				try{
				while ((read = reader.read(chars)) != -1)
				{	
					sb.append(chars);				
				}
	            
				String str = sb.toString();

				JsonParser parser = new JsonParser();
				JsonObject obj = parser.parse(str).getAsJsonObject();
				result = obj.getAsJsonObject("result");
				String d_url = result.get("url").getAsString();
				
				URLConnection conn = new URL(d_url).openConnection();
				
				InputStream is;
	         try{
	         is = conn.getInputStream();
	         }
	         catch(Exception e)
	         {
	         	is=null;
	         }
	         //String fileName;
	         
	         OutputStream outstream = new FileOutputStream(new File(fileName));
	         byte[] buffer = new byte[4096];
	         int len;
	         while ((len = is.read(buffer)) > 0) {
	             outstream.write(buffer, 0, len);
	             baos.write(buffer, 0, len);
	         }
	         outstream.close();
	         
	 	 }
			finally {
				if (reader != null)
					reader.close();
			}
	
	}
			catch(Exception e){
				logger.error("Error in Downloading Opus File");
				response[2] = -1;
				return response;
			}

		response[0] =tno;
		response[1] = sno;
		response[2] = 0;
		return response;
	}
}