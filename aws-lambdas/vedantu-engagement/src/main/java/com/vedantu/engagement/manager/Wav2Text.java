package com.vedantu.engagement.manager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.engagement.util.LogFactory;

public class Wav2Text 
{
	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(Wav2Text.class);

	
    private BufferedInputStream bfread;
    private byte[] buffer;
    private int temp;
    public ArrayList<Double> x=new ArrayList<Double>();
    private double normalizationFactor = 15000;

    
    
    public Wav2Text(String input) throws IOException{
        try {
        	
        	//Read wav file
	        //File file = new File(fpath);
            bfread = new BufferedInputStream(new FileInputStream(input));
            
            buffer = new byte[2];
            
            //this.convert();
            
        } catch (Exception e) {
        	//System.err.println(e.getMessage());
        	logger.error("Unable to read Wav File");
        }
    }
    
    public void convert(){
        printHeader();
		printData();
		logger.info("Wav Read Succesfully");
    }

    private void printData(){
        try {
            while (bfread.read(buffer) >= 0) {
                temp = 0;
                temp = buffer[1];
                temp <<= 8;
                temp |= (0x000000FF & buffer[0]);
                x.add(new Double(temp));
            }
            bfread.close();
        //    process();
        } catch (IOException ex) {
            logger.error("IOException: printData()");
        }
    }

    private void process(){
        double dc = 0;
        
        // Perform DC Shift
        for(int i=0;i<x.size();i++)
            dc += ((Double) x.get(i)).doubleValue();
        dc = dc/x.size();
        
        if(dc != 0){
            for(int i=0;i<x.size();i++)
                x.set(i,((Double) x.get(i)) - dc);
        }
        
        // Normalize
        normalize();
        
       
    }

    private void normalize(){
        double max = Math.abs(((Double) x.get(0)).doubleValue());
        double tmp;
        for(int i=1;i<x.size();i++){
            tmp = Math.abs(((Double)x.get(i)).doubleValue());
            if(max < tmp)
                max = tmp;
        }
        for(int i=0;i<x.size();i++){
            tmp = ((Double) x.get(i)).doubleValue();
            tmp = (tmp/max) * normalizationFactor;
            x.set(i,tmp);
        }
    }

    private void printHeader(){
        try {
			bfread.skip(44);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logger.error("IOException");
		}
    }
}

