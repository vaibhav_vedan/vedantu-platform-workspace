package com.vedantu.engagement.manager;
import java.util.ArrayList;
import java.math.*;

public class Pace {
	
	public double Pace(ArrayList<Double> sound)
	{
		int pace=0;
		int i;
		int fs = 80;
		ArrayList<Double> db = new ArrayList<Double>();
		ArrayList<Double> dbm = new ArrayList<Double>();
		double k,avg =0.0;
		double km;
		int count=0;
		int countm=0;
		int anm=0;
		
		int an =0;
		double prev,next;
		double max =0;;
		for(i=1;i<sound.size();i++)
		{
			if(i%fs == 0)
			{
				k= avg/fs;
				k = k/32767;
			//	km = max/32767;
				k = 10*Math.log10(Math.abs((k/Math.pow(10, -12))));
			//	km = 10*Math.log10(Math.abs((km/Math.pow(10, -12))));
				db.add(k);
			//	dbm.add(km);
				avg=0;
				max=0;
			}
			avg += Math.abs((double)sound.get(i));
			if(Math.abs((double)sound.get(i)) > max)
				max = Math.abs((double)sound.get(i));
		}
		for(i=1;i<db.size()-2;i++)
		{
			if(db.get(i) > 100) 
				{
				count++;
				
			//System.out.println(db.get(i));
			prev =	db.get(i)-db.get(i-1);
			next = db.get(i)-db.get(i+1);
			if(prev>0 && next>0 && db.get(i+1)> 100 && db.get(i-1)>100 && prev < .5 && next < .5)
			{
				an++;
			}
				}
		}
		/*
		for(i=1;i<dbm.size()-1;i++)
		{
			if(dbm.get(i) > 100) 
				{
				countm++;
				
			//System.out.println(db.get(i));
			prev =	dbm.get(i)-dbm.get(i-1);
			next = dbm.get(i)-dbm.get(i+1);
			if(prev>0 && next>0 && dbm.get(i+1)> 100 && dbm.get(i-1)>100 && prev < 0.5 && next < 0.5)
			{
				anm++;
			}
				}
		}
		*/
		
		double qwe = ((double)(((double)(1.0*8000/fs)*an*40/count)));
		
		if(qwe > 200)qwe = 210-(qwe%10);
		else if(qwe<110) qwe = 110 + (qwe/10)+(qwe%10);
		if(count == 0)qwe = 0;
	
		return qwe;
	}

}
