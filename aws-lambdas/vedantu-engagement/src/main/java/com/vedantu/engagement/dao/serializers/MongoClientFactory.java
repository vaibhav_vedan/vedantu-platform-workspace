package com.vedantu.engagement.dao.serializers;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.vedantu.engagement.util.ConfigUtils;
import com.vedantu.engagement.util.LogFactory;

public class MongoClientFactory {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MongoClientFactory.class);

    private MongoClient client = null;

    ConfigUtils configUtils = new ConfigUtils();

    public final static MongoClientFactory INSTANCE = new MongoClientFactory();

    public MongoClientFactory() {
        logger.error("test prod error mongo");
        logger.info("MongoClientFactory is inialized");
        MongoClientOptions.Builder builder = new MongoClientOptions.Builder();
        builder.sslEnabled(true);
        MongoClientOptions options = builder.connectionsPerHost(10).build();
        List<ServerAddress> seeds = new ArrayList<ServerAddress>();
        String hosts = configUtils.getStringValue("MONGO_HOST");
        String PORT = configUtils.getStringValue("MONGO_PORT");
        String db = configUtils.getStringValue("MONGO_DB_NAME");
        String user = configUtils.getStringValue("MONGO_USER_NAME");
        String password = configUtils.getStringValue("MONGO_PASSWORD");
        if (hosts != null && !hosts.isEmpty() && PORT != null && !PORT.isEmpty()) {
            String[] hostArray = hosts.split(",");
            for (String host : hostArray) {
                seeds.add(new ServerAddress(host, Integer.parseInt(PORT)));
            }
        }
        Boolean useAuthentication = configUtils.getBooleanValue("useAuthentication");
        if (useAuthentication != null && useAuthentication) {
            List<MongoCredential> credentials = new ArrayList<MongoCredential>();
            credentials.add(
                    MongoCredential.createScramSha1Credential(user, db, password.toCharArray()));
            client = new MongoClient(seeds, credentials, options);
        } else {
            client = new MongoClient(seeds, options);
        }

    }

    public MongoClient getClient() {
        logger.info("MongoClientFactory get client called");
        return client;
    }

    public void setClient(MongoClient client) {
        this.client = client;
    }

}
