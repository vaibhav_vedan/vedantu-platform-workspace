package com.vedantu.engagement.pojo;

public class EmailAttachment {
	private String fileName;
	private Object attachmentData;
	private String application;

	public EmailAttachment(String fileName, Object attachmentData, String application) {
		super();
		this.fileName = fileName;
		this.attachmentData = attachmentData;
		this.application = application;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Object getAttachmentData() {
		return attachmentData;
	}

	public void setAttachmentData(Object attachmentData) {
		this.attachmentData = attachmentData;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	@Override
	public String toString() {
		return "EmailAttachment  fileName:" + fileName + ", attachmentData:"
				+ attachmentData + ", application:" + application;
	}
}
