package com.vedantu.engagement.manager;

import java.util.ArrayList;
import java.util.Arrays;

public class AudioTemp {

	private ArrayList<byte[]> t_byte;
	private ArrayList<byte[]> s_byte;
	private String[] str;
	
	public ArrayList<byte[]> getT_byte() {
		return t_byte;
	}

	public void setT_byte(ArrayList<byte[]> t_byte) {
		this.t_byte.addAll(t_byte);
	}

	public ArrayList<byte[]> getS_byte() {
		return s_byte;
	}

	public void setS_byte(ArrayList<byte[]> s_byte) {
		this.s_byte.addAll(s_byte);
	}

	public String[] getStr() {
		return str;
	}

	public void setStr(String[] str) {
		this.str=Arrays.copyOf(str, str.length);
	}

	public AudioTemp()
	{
		super();
	}
}
