package com.vedantu.engagement.manager;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.vedantu.engagement.util.LogFactory;

public class ScreenSharingCheck {
	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(ScreenSharingCheck.class);
	
	public String SSCheck(String sessionId)
	{
		String screenShareTime;
		String urlString = "https://session-metrics.vedantu.com/sessiondata?sessionId="+sessionId;
		URL url;
		
		try {
			url = new URL(urlString);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
	
		StringBuilder sb = new StringBuilder();
		JsonArray result;
		int read;
		char[] chars = new char[1];
		try{
			while ((read = reader.read(chars)) != -1)
			{	
				sb.append(chars);				
			}
		    
			String str = sb.toString();
		
			JsonParser parser = new JsonParser();
			JsonObject obj = parser.parse(str).getAsJsonObject();
			result = obj.getAsJsonArray("result");
			screenShareTime =result.get(0).getAsJsonObject().get("totalScreenShareDuration").getAsString();
			
			return screenShareTime;
		}
			catch(Exception e)
			{
				logger.info("Unable to get Screen Share Time");
			}
		}
		catch (Exception e1) {
		// TODO Auto-generated catch block
		logger.info("Malformed URL "+urlString);
	}
		
		return "0";
	}

}
