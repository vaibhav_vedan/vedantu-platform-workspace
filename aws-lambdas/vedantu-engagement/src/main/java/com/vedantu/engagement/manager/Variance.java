package com.vedantu.engagement.manager;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class Variance {

	public double slope(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, String tid)
	{
		ArrayList<Double> final_variance = new ArrayList<Double>();
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			Integer page_num = page_num_entry.getKey();
			System.out.println("page_num "+page_num);
			ArrayList<Double> variance = new ArrayList<Double>();

			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				if(userId.getKey().equals(tid))
				{	
					ArrayList<WB> white_board = new ArrayList<WB>();
					white_board = userId.getValue();
					for(int i =0 ;i < white_board.size();i++)
					{
						if(white_board.get(i).type.equals("CREATE"))
						{
							int num_points = white_board.get(i).dat.coordinates.length;
							ArrayList<Double> angle = new ArrayList<Double>();
							double avg = 0;
							double sum = 0;
							for(int m =0 ;m <num_points-1 ;m++)
							{
								if(white_board.get(i).dat.coordinates[m][0] > 0 &&  white_board.get(i).dat.coordinates[m][1] > 0 && white_board.get(i).dat.coordinates[m][0] <= 902 && white_board.get(i).dat.coordinates[m][1] <= 600 && white_board.get(i).dat.coordinates[m+1][0] > 0 &&  white_board.get(i).dat.coordinates[m+1][1] > 0 && white_board.get(i).dat.coordinates[m+1][0] <= 902 && white_board.get(i).dat.coordinates[m+1][1] <= 600 )
								{
									double x1 = white_board.get(i).dat.coordinates[m][0];
									double y1 = white_board.get(i).dat.coordinates[m][1];
									double x2 = white_board.get(i).dat.coordinates[m+1][0];
									double y2 = white_board.get(i).dat.coordinates[m+1][1];

									double theta = Math.atan2(y2-y1, x2-x1);

									angle.add(theta);
								}
							}
							for(int m = 0 ;m< angle.size()-1; m++)
							{
								double consecutive = 0;
								consecutive = angle.get(m+1)-angle.get(m);
								angle.set(m,consecutive);
								avg+= consecutive;
								sum+= consecutive*consecutive;

							}
							if(angle.size() > 1)
							{
								sum/= (angle.size()-1);
								avg/= (angle.size()-1);
								avg = avg*avg;
								double var = sum - avg; //variance of difference of slopes
								variance.add(var);
							}
						}	
					}
				}
			}
			double avg_var = 0;
			for(int i =0; i< variance.size(); i++)
			{
				avg_var+= variance.get(i);
			}
			if(variance.size() > 0)
			{
				avg_var/= variance.size();
				final_variance.add(avg_var);
				System.out.println("variance of the page = "+ avg_var);
			}
		}
		double final_var = 0;
		for(int i =0 ;i<final_variance.size();i++)
		{
			final_var+= final_variance.get(i);
			System.out.println(final_variance.get(i));
		}
		if(final_variance.size() > 0)
		{
			final_var/= final_variance.size();
		}
		System.out.println("variance of the session is "+final_var);
		return final_var;
	}
}
