package com.vedantu.engagement.entity;

import javax.mail.Address;
import com.vedantu.engagement.enums.EmailType;

public class Email extends AbstractEntity {
	
	private Address to;
	private String subject;
	private EmailType type;
	private boolean opened;
	
	public Address getTo() {
		return to;
	}

	public void setTo(Address to) {
		this.to = to;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public EmailType getType() {
		return type;
	}

	public void setType(EmailType type) {
		this.type = type;
	}

	public boolean isOpened() {
		return opened;
	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}

	@Override
	public String toString() {	
		
		return "Email to:" + to +  ", subject:"
				+ subject + ", type:" + type + ", opened:" + opened;
	}
	
	public static class Constants extends AbstractEntity.Constants {
		public static final String TO = "to";
		public static final String SUBJECT = "subject";
		public static final String TYPE = "type";
	}
}
