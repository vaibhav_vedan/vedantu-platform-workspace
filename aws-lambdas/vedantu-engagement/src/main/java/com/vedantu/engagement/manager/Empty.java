package com.vedantu.engagement.manager;
import java.util.*;
public class Empty{

	public double empty_space(int grid[][],int row, int col)
	{
		int space = 0;
		int rlimit = 11,climit = 10;
		int limit = 100 ,i =0 ,j = 0;
		int isempty = 0;

		for(i = 0 ; i <row-rlimit ; )
		{ 
			for(j = 0; j< col-climit ; )
			{
				isempty = empty_check(grid,i,i+rlimit,j,j+climit);
				if(isempty == 1)
				{
					space++;
				}
				j+= climit;
			}
			i+= rlimit;
		}
		return (space * 1100.0)/(row * col) ;
	}
	
	private int empty_check(int grid[][],int x1, int x2, int y1, int y2) {
		for(int i = x1; i < x2 ;i ++)
		{
			for(int j = y1; j <y2 ; j++)
			{
				if(grid[i][j] == 1)
				{
					return 0;
				}
			}
		}
		return 1;
	}
}
