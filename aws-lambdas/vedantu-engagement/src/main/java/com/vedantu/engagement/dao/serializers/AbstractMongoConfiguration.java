package com.vedantu.engagement.dao.serializers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import com.vedantu.engagement.util.ConfigUtils;
import com.vedantu.engagement.util.WebUtils;

import com.mongodb.MongoClient;

@Service
public class AbstractMongoConfiguration {

@Autowired
private ConfigUtils configUtils ;


	private String DATABASE_NAME;
	private static MongoOperations mongoOperations;
	private static String COLLECTION;
	
	

	public AbstractMongoConfiguration() {
		super();
		configUtils = new ConfigUtils();
		DATABASE_NAME = configUtils.getStringValue("MONGO_DB_NAME");
		setMongoOperations(MongoClientFactory.INSTANCE.getClient(), DATABASE_NAME);
	}

	public String getCOLLECTION() {
		return COLLECTION;
	}

	public void setCOLLECTION(String cOLLECTION) {
		COLLECTION = cOLLECTION;
	}

	public MongoOperations getMongoOperations() {
		return mongoOperations;
	}

	public static void setMongoOperations(MongoClient client, String databaseName) {
		try {
			
			mongoOperations = (MongoOperations) new MongoTemplate(client, databaseName);
		} catch (Exception ex) {
			// Throw error here
		}
	}

}
