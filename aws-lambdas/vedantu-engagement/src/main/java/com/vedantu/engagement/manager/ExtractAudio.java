package com.vedantu.engagement.manager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.vedantu.engagement.util.LogFactory;


class user
{
	String userId;
	String role;
	long billingPeriod;
	String joinTime;
	String userState;
	String fullName;
}
class sessionInfo
{
	String sessionId;
	String subject;
	String startedAt;
	String startTime;
	String endTime;
	String endedAt;
	String state;
	ArrayList<user> attendees;
}
class urlData
{
	String url;
}

class fileD
{
	long startTime;
	String awsS3Key;
}

class fileDetail
{
	String sessionId;
	String userId;
	String fileData;
	String fileType;
	long stime;
	String awsString;
	String fileId;

}
class fileMetaData
{
	ArrayList<fileDetail> data;
}


@Service
public class ExtractAudio
{
	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(ExtractAudio.class);

	public ArrayList<Double> teacher_byte = new ArrayList<Double>();
	public ArrayList<Double> student_byte = new ArrayList<Double>();
	public Double pace_final;
	
		
	public  ArrayList<Double> stitchAll(ArrayList<Double> alist1)
	{
		int i,fs=8000;
		int avg = 0;
		double y0;
		ArrayList<Double> tamps = new ArrayList<Double>();
		for(i=0;i<alist1.size();i++)
		{
			if(i%fs == 0)
			{
				y0= avg/fs;
				tamps.add(y0);
				avg=0;
			}
			avg += Math.abs((double)alist1.get(i));
		}

		return (tamps);
	}




	public String[] ExtractFiles(String sessionId, String beginUrl, String payloadUserId,String wbdataurl) throws UnsupportedEncodingException, IOException
	{

		int error =0;
		Gson gson = new Gson();
		ArrayList<Double> tcher = new ArrayList<Double>();
		Double tcher1=0.0;
		String[] str = new String[22];	//teacherid,student id,fileNos,subject,start time of audio

		//	String payloadUserId = "6051427639099392";
		String teacherId="",studentId="",teacherName="",studentName="";
		String teacherTime="",studentTime="";

		String propName1 = "X-Ved-Client";
		String propValue1 = "ENGAGEMENT_ANALYSIS";
		String propName2 = "X-Ved-Client-Id";
		String propValue2 = "C2B8B";
		String propName3 = "X-Ved-Client-Secret";
		String propValue3 = "qfUD85nZmj749DO8zv66rnrGuVMcQab5";
		str[9]="0";
		BufferedReader br;
		
		String downloadurl = beginUrl+"/platform/session/"+sessionId;//"/_ah/api/sessionendpoint/v1/getsessioninfores?alt=json";	
//		String payload = "{id: \""+sessionId+"\", callingUserId: "+payloadUserId+"}";			//  5629499534213120
		URL newobj = new URL(downloadurl);
		HttpURLConnection con = (HttpURLConnection) newobj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty(propName1, propValue1);
		con.setRequestProperty(propName2, propValue2);
		con.setRequestProperty(propName3, propValue3);
		try
		{
			br = new BufferedReader(new InputStreamReader(con.getInputStream()));

			
		}	//convert the json string back to object
		catch(FileNotFoundException e)
		{
			str[0] = "-2";
			logger.info("get session info File not found");
			return str;
		}
		
		
		sessionInfo obj = gson.fromJson(br, sessionInfo.class);
		con.disconnect();
		br.close();
		long time_1;
		double late=0;
		if(!obj.state.equalsIgnoreCase("ENDED"))
		{
			str[0]="Session Not Ended";
			logger.error("Session Not Ended");
			return str;
		}
		logger.info(sessionId+"  " +obj.attendees.get(0).userState +"  " +obj.attendees.get(1).userState);

		logger.info("attendees identification");
		if(obj.attendees.get(0).role.equals("TEACHER"))
		{
			teacherId = obj.attendees.get(0).userId;
			time_1 = obj.attendees.get(0).billingPeriod;
			logger.info("billing Period " + time_1);
			studentId = obj.attendees.get(1).userId;
			teacherName = obj.attendees.get(0).fullName;
			studentName = obj.attendees.get(1).fullName;
			//if()
			if(!obj.attendees.get(0).userState.equals("NOT_JOINED") || !obj.attendees.get(1).userState.equals("NOT_JOINED"))
			{
				late = Long.parseLong(obj.attendees.get(0).joinTime) - Long.parseLong(obj.startedAt);
				teacherTime = obj.attendees.get(0).joinTime;
				studentTime = obj.attendees.get(1).joinTime;
			}
		}
		else {
			teacherId = obj.attendees.get(1).userId;
			time_1 = obj.attendees.get(1).billingPeriod;
			//late = Long.parseLong(obj.attendees.get(1).joinTime) - Long.parseLong(obj.startedAt);
			studentId = obj.attendees.get(0).userId;

			teacherName = obj.attendees.get(1).fullName;
			studentName = obj.attendees.get(0).fullName;
			//if()
			if(!obj.attendees.get(0).userState.equals("NOT_JOINED") || !obj.attendees.get(1).userState.equals("NOT_JOINED"))
			{
				late = Long.parseLong(obj.attendees.get(1).joinTime) - Long.parseLong(obj.startedAt);
				teacherTime = obj.attendees.get(1).joinTime;
				studentTime = obj.attendees.get(0).joinTime;
			}

		}
		logger.info("Teacher ID " + teacherId+ " Student ID " + studentId);
		str[0] = teacherId;
		str[1] = studentId;
		str[10] = teacherId;
		str[11] = studentId;

		str[12] = teacherName;
		str[13] = studentName;
		time_1 = time_1/1000;
		str[6] = Integer.toString((int)time_1);

		String part="";
		if(obj.subject.contains(","))
		{
			String parts[] = obj.subject.split("\\,");
			part +=parts[1]; 
			//System.out.print(parts[0]);
			str[3] = part;
		}
		else
			str[3] = obj.subject;

		if(obj.attendees.get(0).userState.equals("NOT_JOINED") || obj.attendees.get(1).userState.equals("NOT_JOINED"))
		{
			str[0]= "-2";
			logger.info("Not Joined");
			return str;
		}
		str[14] = teacherTime; 
		str[15] = studentTime;

		str[16] = obj.startTime;
		str[17] = obj.endTime;
		late = 1.0*late/60000;
		if(late>0)
			str[8] = String.valueOf(late);
		else str[8] = "0";


		/*if(Long.parseLong(sessionId)%10 == 0)
			return str;
		 */
		String teacherTechRating=""+0;
		String studentTechRating = ""+0;
		try
		{

			String downloadurlfeed = beginUrl +"/platform/feedback/fetchFeedback?sessionId="+sessionId;// "/_ah/api/feedbackendpoint/v1/fetchFeedback?alt=json";	
//			String payloadfeed = "{sessionId: \""+sessionId+"\", orderDesc:true, callingUserId: "+payloadUserId+", userId: "+payloadUserId+"}";
			URL newobjfeed = new URL(downloadurlfeed);
			HttpURLConnection confeed = (HttpURLConnection) newobjfeed.openConnection();
			confeed.setRequestMethod("GET");

			confeed.setRequestProperty(propName1, propValue1);
			confeed.setRequestProperty(propName2, propValue2);
			confeed.setRequestProperty(propName3, propValue3);
			BufferedReader brfeed = new BufferedReader(new InputStreamReader(confeed.getInputStream()));
			String line;
			StringBuilder jsonString = new StringBuilder();
			while ((line = brfeed.readLine()) != null) {
				jsonString.append(line);
			}
			brfeed.close();
			String feedback = jsonString.toString();
			JsonParser new_parser = new JsonParser();


			JsonObject new_obj = new_parser.parse(feedback).getAsJsonObject();
			JsonArray data = new_obj.getAsJsonArray("data");
			if(data.get(0).getAsJsonObject().get("sender").getAsJsonObject().get("role").getAsString().equals("STUDENT"))
			{
				studentTechRating = data.get(0).getAsJsonObject().get("rating").getAsString();
				//		System.out.println("Check_123 "+data.get(0).getAsJsonObject().get("rating").getAsString());
				if(data.size()>1)
					teacherTechRating = data.get(1).getAsJsonObject().get("rating").getAsString();
			}
			else 
			{
				if(data.size()>1){
					//	System.out.println("Check_321 "+data.get(1).getAsJsonObject().get("rating").getAsString());
					studentTechRating = data.get(1).getAsJsonObject().get("rating").getAsString();
				}
				teacherTechRating = data.get(0).getAsJsonObject().get("rating").getAsString();
			}
		}
		catch(FileNotFoundException ex)
		{
			logger.error("FetchFeedback file not found");
			teacherTechRating = "-1";
			studentTechRating = "-1";
		}
		catch(Exception e)
		{
			teacherTechRating = "-1";
			studentTechRating = "-1";
		}
		str[5] = studentTechRating;
		str[18] = teacherTechRating;
		str[19] = studentTechRating;

		String acadRating;
		try{
			String URLString = "https://www.vedantu.com/pub/reviews/getReview?callingUserId="+payloadUserId+"&contextId="+sessionId+"&contextType=SESSION&entityId="+teacherId+"&entityType=USER&userId="+studentId;
			URL url = new URL(URLString);
			HttpURLConnection conReview = (HttpURLConnection) url.openConnection();
			conReview.setRequestMethod("GET");
			conReview.setRequestProperty("Cookie", "JSESSIONID=AaZkRCfI5kkaTwXRs1wsxw; utmParams=%7B%22utm_source%22%3A%22%22%2C%22utm_medium%22%3A%22%22%2C%22utm_campaign%22%3A%22%22%2C%22utm_term%22%3A%22%22%2C%22utm_content%22%3A%22%22%2C%22channel%22%3A%22%22%7D; _we_wk_ss_lsf_=true; pnctest=1; _ga=GA1.3.1976979090.1457340130; _vwo_uuid_v2=45EF86B7D48C138F3100FD919404BA11|0fd0aee1cab0e391be977f69671c3d6b; ORG5927=acc4ae84-16e3-43b4-a136-c9f5b7ee6f6b; __zlcmid=ZXfNjCNEe1job6; _gat=1");
			conReview.connect();
			StringBuilder sb = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(conReview.getInputStream()));
			int read;
			char[] chars = new char[1];
			while ((read = reader.read(chars)) != -1)
			{	
				sb.append(chars);				
			}

			String json = sb.toString();
			JsonParser parser = new JsonParser();
			JsonObject jsonobj = parser.parse(json).getAsJsonObject();
			acadRating = jsonobj.get("rating").getAsString();
		}
		catch(Exception e)
		{
			acadRating = "-1";
		}

		str[20] = acadRating;
		logger.info("fetchFileMetadata being called.");
		int ftype=0;//1-opus. 2- mp3
		fileMetaData fmd = null;
		try
		{
			String downloadurl1 = beginUrl +"/platform/filemgmt/fetchFileMetadata?sessionId="+sessionId;// "/_ah/api/filemgmtendpoint/v1/fetchFileMetadata?alt=json";	
//			String payload1 = "{sessionId: \""+sessionId+"\", callingUserId: "+payloadUserId+"}";
			URL newobj1 = new URL(downloadurl1);
			HttpURLConnection con1 = (HttpURLConnection) newobj1.openConnection();
			con1.setRequestProperty(propName1, propValue1);
			con1.setRequestProperty(propName2, propValue2);
			con1.setRequestProperty(propName3, propValue3);
			
			BufferedReader br1 = new BufferedReader(new InputStreamReader(con1.getInputStream()));

			fmd = gson.fromJson(br1, fileMetaData.class);
			con1.disconnect();
			br1.close();
			if(fmd.data != null){
				for(int i=0;i<fmd.data.size();i++)
				{
					fileD time = gson.fromJson(fmd.data.get(i).fileData, fileD.class);
					fmd.data.get(i).stime = time.startTime;
					if(time.awsS3Key!=null)
					{
						fmd.data.get(i).awsString = time.awsS3Key;
					}
				}
			}
			//--------------------------


			if(fmd.data!=null){
				Collections.sort(fmd.data, new Comparator<fileDetail>() {
					@Override public int compare(fileDetail p1, fileDetail p2) {
						return ((p1.stime < p2.stime)? -1: 1); // Ascending
					}
				});
			}

		}
		catch(FileNotFoundException ex)
		{
			logger.error("fetchFileMetadata file not found");
			str[0] = "-3";
			str[21]= "fetchFileMetaData error";
			return str;
		}
		catch(Exception ex)
		{
			str[0] = "-3";
			logger.error("fetchFileMetaData error");
			str[21]= "fetchFileMetaData error";
			return str;
		}
		//--------------------------

		int t=1;int s=1;
		int tflag=0;
		logger.info("Files being Downloaded");
		ConvertToWav convertWav = new ConvertToWav();
		if(error==0 && fmd.data != null){
			for(int i=0;i<fmd.data.size();i++)
			{
				try{
				if(fmd.data.get(i).fileId != null)
				{
					if(fmd.data.get(i).fileType.equals("audio/mp3"))
					{
						ftype =2;
						String downloadurl2 = beginUrl + "/platform/filemgmt/fetchDownloadUrl?sessionId="+sessionId;// "/_ah/api/filemgmtendpoint/v1/fetchDownloadUrl?alt=json";	
			//			String payload2 = "{id: \""+fmd.data.get(i).fileId+"\", callingUserId: "+payloadUserId+"}";
						URL newobj2 = new URL(downloadurl2);
						HttpURLConnection con2 = (HttpURLConnection) newobj2.openConnection();
						con2.setRequestMethod("GET");

						con2.setRequestProperty(propName1, propValue1);
						con2.setRequestProperty(propName2, propValue2);
						con2.setRequestProperty(propName3, propValue3);
						
						BufferedReader br2 = null;
						urlData tempurl = null;
						ByteArrayOutputStream baos = null;
						InputStream is = null;
						try
						{
							br2 = new BufferedReader(new InputStreamReader(con2.getInputStream()));
							tempurl = gson.fromJson(br2, urlData.class);
							con2.disconnect();
							br2.close();

							baos = new ByteArrayOutputStream();
							URLConnection conn = new URL(tempurl.url).openConnection();
							//						InputStream is;
							try{
								is = conn.getInputStream();
							}
							catch(Exception e)
							{
								logger.error("Error while downloading "+fmd.data.get(i).fileId);
								continue;
							}
						}
						catch(FileNotFoundException ex)
						{
							logger.error("Error file not found");
							continue;
						}
						catch(Exception ex)
						{
							logger.error("Unable to Download Files");
							continue;
						}

						String name;
						if(fmd.data.get(i).userId.equals(teacherId))
						{
							name = "/tmp/t"+ (t++)+".mp3";tflag=1;
							logger.info(fmd.data.get(i).fileId + " with teacher id as " + fmd.data.get(i).userId + " and start time "+ fmd.data.get(i).stime + "is changed to " +name);
						}
						else
						{
							name = "/tmp/s"+ (s++)+".mp3";tflag=2;
							logger.info(fmd.data.get(i).fileId + " with student id as  " + fmd.data.get(i).userId + " and start time "+ fmd.data.get(i).stime + "is changed to " + name);
						}
						OutputStream outstream = new FileOutputStream(new File(name));
						byte[] buffer = new byte[4096];
						int len;
						while ((len = is.read(buffer)) > 0) {
							outstream.write(buffer, 0, len);
							baos.write(buffer, 0, len);
						}
						outstream.close();

						if(tflag==1){
							MP3toArray temp = new MP3toArray();
							//	teacher_byte.add(baos.toByteArray());
							temp.change(name, baos.toByteArray(), 0);
							Pace pc = new Pace();
							tcher.add(pc.Pace(temp.x));
							if(tcher.get(tcher.size()-1)==0){
								tcher.remove(tcher.size()-1);
							}
							else tcher1 += tcher.get(tcher.size()-1);
							teacher_byte.addAll(stitchAll(temp.x));
						}
						else if(tflag==2){
							MP3toArray temp = new MP3toArray();
							//student_byte.add(baos.toByteArray());
							temp.change(name, baos.toByteArray(), 0);
							student_byte.addAll(stitchAll(temp.x));
						}
						if(teacher_byte.size()>0 && student_byte.size()>0){
							logger.info("teacher length_Extract Audio "+teacher_byte.size());
							logger.info("student length_Extract Audio "+student_byte.size());
						}
					}
					else if(fmd.data.get(i).fileType.equals("audio/opus"))
					{
						ftype=1;
						String name;
						String outname;
						if(fmd.data.get(i).userId.equals(teacherId))
						{
							name = "/tmp/t"+ (t)+".opus";tflag=1;
							outname ="/tmp/t"+(t++)+".wav";
							logger.info(fmd.data.get(i).fileId + " with teacher id as " + fmd.data.get(i).userId + " and start time "+ fmd.data.get(i).stime + "is changed to " +name);
						}
						else
						{
							name = "/tmp/s"+ (s)+".opus";
							outname = "/tmp/s"+ (s++)+".wav";
							tflag=2;
							logger.info(fmd.data.get(i).fileId + " with student id as  " + fmd.data.get(i).userId + " and start time "+ fmd.data.get(i).stime + "is changed to " + name);
						}

						DownloadOpus dopus = new DownloadOpus();
						int[] response = dopus.Download(fmd.data.get(i).awsString,name,wbdataurl);
						if(response[2]>=0)
						{
							//ConvertToWav convertWav = new ConvertToWav();
							int temp = convertWav.Convert(name,outname);
						}
						if(tflag==1){
							MP3toArray temp = new MP3toArray();
							//	teacher_byte.add(baos.toByteArray());
							temp.change(outname, null, 1);
							Pace pc = new Pace();
							tcher.add(pc.Pace(temp.x));
							if(tcher.get(tcher.size()-1)==0){
								tcher.remove(tcher.size()-1);
							}
							else tcher1 += tcher.get(tcher.size()-1);
							teacher_byte.addAll(stitchAll(temp.x));
						}
						else if(tflag==2){
							MP3toArray temp = new MP3toArray();
							//student_byte.add(baos.toByteArray());
							temp.change(outname, null, 1);
							student_byte.addAll(stitchAll(temp.x));
						}
					}
					else
					{
						str[0]="-1";
						str[21]= "Some Audio Files are missing.";
						return str;
					}
				}
				else {

					error=1;
					logger.error("Not all files present. error " + error);
					str[2] = Integer.toString(-1);
					//audioTemp.setStr(str);
					str[21]= "Some Audio Files are missing.";
					return str;

				}
				}
				catch(Exception e){
					logger.error("No Files to Download first");
					str[0] = "-1";
					str[21]= "Some Audio Files are missing.";
					return str;
				}
			}
			
		}
		else{
			logger.error("No Files to Download second");
			str[0] = "-1";
			str[21]="No Files to Download"; 
			return str;
		}
		if(t!=s) {
			//error=2;
			logger.info("teacher files is not equal to student files. error ");

			//return str;
		}
		str[7] = Integer.toString((s-1));
		/*if( t < 4 || s< 4 )
		{

			error =3;
			logger.info(" not enough files. error" + error);
			str[2] = Integer.toString(-1);
			//	audioTemp.setStr(str);
			return str;
		}*/


		int k= t-1;
		if(error == 0)
		{
			str[2] =  Integer.toString(k);

		}
		else str[2] = Integer.toString(-1);

		str[4] = String.valueOf(fmd.data.get(0).stime);

		
		if(tcher.size()>0)
		{
			pace_final = tcher1/tcher.size();

		}
		else pace_final = 0.0;
		logger.info("Exiting EA");
		//audioTemp.setT_byte(teacher_byte);
		//	audioTemp.setS_byte(student_byte);
		//	audioTemp.setStr(str);
		return str;
	}

}
