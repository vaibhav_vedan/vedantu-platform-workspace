package com.vedantu.engagement.manager;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.engagement.util.LogFactory;

import net.bramp.ffmpeg.FFmpeg;
import net.bramp.ffmpeg.FFmpegExecutor;
import net.bramp.ffmpeg.FFprobe;
import net.bramp.ffmpeg.builder.FFmpegBuilder;

public class ConvertToWav{

	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(ConvertToWav.class);
	
	public ConvertToWav()
	{
		URL inputUrl = getClass().getResource("/ffprobe");
    	File dest = new File("/tmp/ffprobe");
    	try {
			FileUtils.copyURLToFile(inputUrl, dest);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	URL inputUrl1 = getClass().getResource("/ffmpeg");
    	File dest1 = new File("/tmp/ffmpeg");
    	try {
			FileUtils.copyURLToFile(inputUrl1, dest1);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	dest.setReadable(true, false);
    	dest.setExecutable(true, false);
    	dest.setWritable(true, false);
    	dest1.setReadable(true, false);
    	dest1.setExecutable(true, false);
    	dest1.setWritable(true, false);
		
	}
	
	public int Convert(String inputName,String outputName) throws IOException{
					
    	FFmpeg ffmpeg = new FFmpeg("/tmp/ffmpeg");
    	FFprobe ffprobe = new FFprobe("/tmp/ffprobe");

    	
    	FFmpegBuilder builder = new FFmpegBuilder()

    	  .setInput(inputName)     // Filename, or a FFmpegProbeResult
    	  .overrideOutputFiles(true)
    	  
 
    	  .addOutput(outputName)   // Filename for the destination
    	    .setFormat("wav") 
    	    .setAudioChannels(1)         // Mono audio
     	    .setAudioSampleRate(8000)
    
    	    .done();
    	
    	FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);

    	// Run a one-pass encode
    	executor.createJob(builder).run();
    	logger.info("converted file to "+ outputName);
    	
    		return 1;
	}
}
