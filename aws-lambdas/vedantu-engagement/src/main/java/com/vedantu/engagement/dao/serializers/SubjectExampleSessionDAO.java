package com.vedantu.engagement.dao.serializers;

import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.engagement.entity.SubjectExampleSession;

@Service
public class SubjectExampleSessionDAO extends AbstractDAO {

    public SubjectExampleSessionDAO() {
        super();
    }

    public void create(SubjectExampleSession p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public SubjectExampleSession getById(String id) {
        SubjectExampleSession ChannelUserInfo = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                ChannelUserInfo = (SubjectExampleSession) getEntityById(id, SubjectExampleSession.class);
            }
        } catch (Exception ex) {
            ChannelUserInfo = null;
        }
        return ChannelUserInfo;
    }

    public void update(SubjectExampleSession p, Query q, Update u) {
        try {
            if (p != null) {
                updateEntity(p, q, u);
            }
        } catch (Exception ex) {
        }
    }

    public List<SubjectExampleSession> getAll() {
        List<SubjectExampleSession> result = getEntities(SubjectExampleSession.class);
        return result;
    }

    public List<SubjectExampleSession> getExampleByStartTimeAndSubject(Long startTime, Long endTime, String subject) {
        List<SubjectExampleSession> result = getExampleByStartTimeAndSubject(SubjectExampleSession.class, startTime, endTime, subject);
        return result;
    }
}
