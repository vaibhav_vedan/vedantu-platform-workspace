package com.vedantu.engagement.manager;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.SNSEvent;
import com.google.gson.Gson;

import com.vedantu.engagement.util.LogFactory;
import com.vedantu.engagement.viewobject.response.EngagementAnalysis;

public class AwsHandler {

    EngagementManager engagementManager = new EngagementManager();

   
    @Autowired
    private LogFactory logfactory;
    private Logger logger = logfactory.getLogger(AwsHandler.class);

//    private SNSManager sNSManager=SNSManager.INSTANCE;

    public AwsHandler(){
        super();
    }
    
    class Message {

        Long sessionId;
    }

    public void getResult(SNSEvent event, Context context) {
        Long sessionId = null;
        try {
            String message = event.getRecords().get(0).getSNS().getMessage();

            System.out.println(message);

            if (message.contains(",")) {
                SubjectExampleSessionManager subjectExampleSessionManager = new SubjectExampleSessionManager();
                subjectExampleSessionManager.createSubjectExampleSession(message);
                return;
            }

            Gson gson = new Gson();

            Message msg = gson.fromJson(message, Message.class);
            System.out.println("msg is " + msg.toString());
            sessionId = msg.sessionId;
        } catch (Exception e) {
            logger.info("SNS event incorrect",e);
            return;

        }

        if (event.getRecords().get(0).getSNS().getSubject().equals("REPLAY_SESSION_PREPARED")) {
            System.out.println("session id is " + sessionId);

            try {
            	EngagementAnalysis engagementAnalysis = engagementManager.getEngagement(sessionId);
                if(engagementAnalysis.getAnalysable() == true && engagementAnalysis.getAudioPresent().equalsIgnoreCase("Audio Present") && engagementAnalysis.getScoreTotal() >=8)
            	{
                	//sNSManager.publishMessage(sessionId);
            	}
                
            //	sNSManager.publishMessage(sessionId);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                logger.info("Engagement manager throws error",e);
            }

        } else {
            logger.info("REPLAY_NOT_PREPARED " + event.getRecords().get(0).getSNS().getSubject());
        }

    }
}