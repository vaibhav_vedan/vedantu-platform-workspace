package com.vedantu.engagement.viewobject.response;

import java.util.ArrayList;
import java.util.List;

public class EngagementAnalysis {

	private String _id;
	private String subject;
	private String shapesandImages;
	private String noImages;
	private String colourChanges;
	private String teacherWriteLength;
	private String studentWriteLength;
	private String whiteboardSpaceUsage;
	private String teacherWriteTime;
	private String studentWriteTime;
	private String whiteboardInactiveTime;
	private String whiteBoards;
	private String hWAveragedensity;
	private String hWVariance;
	private String imageAnnotation;
	private String activeDuration;
	private String interaction;
	private String problemsDiscussed;
	private String teacherTime;
	private String studentTime;
	private String sTRatio;
	private String noActivity;
	private String sameTime;
	private String pace;
	private String audioDuration;
	private String score;
	private String quality;
	private Boolean analysable;
	private String analysableReason;
	private Long serverTime;
	private String teacherId;
	private String studentId;
	private String teacherName;
	private String studentName;
	private Long sessionStartTime;
	private Long teacherJoinTime;
	private Long studentJoinTime;
	private Long studentWaitTime;
	private String qualityShapesandImages;
	private String qualityColorChanges;
	private String qualityTeacherWriteLength;
	private String qualityWhiteBoardSpaceUsage;
	private String qualityTeacherWriteTime;
	private String qualityWhiteBoardInactiveTime;
	private String qualityWhiteBoards;
	private String qualityInteraction;
	private String qualitySTRatio;
	private String qualitySameTime;
	private Double scoreInteraction;
	private Double scoreWhiteBoardUsage;
	private Double scoreContentUsage;
	private Double scoreTotal;
	private String audioPresent;
	private String scheduledTime;
	private String teacherTechRating;
	private String studentTechRating;
	private String acadRating;
	private String audioReason;
	private String screenShareTime;


	public String getQualityShapesandImages() {
		return qualityShapesandImages;
	}

	public void setQualityShapesandImages(String qualityShapesandImages) {
		this.qualityShapesandImages = qualityShapesandImages;
	}

	public String getQualityColorChanges() {
		return qualityColorChanges;
	}

	public void setQualityColorChanges(String qualityColorChanges) {
		this.qualityColorChanges = qualityColorChanges;
	}

	public String getQualityTeacherWriteLength() {
		return qualityTeacherWriteLength;
	}

	public void setQualityTeacherWriteLength(String qualityTeacherWriteLength) {
		this.qualityTeacherWriteLength = qualityTeacherWriteLength;
	}

	public String getQualityWhiteBoardSpaceUsage() {
		return qualityWhiteBoardSpaceUsage;
	}

	public void setQualityWhiteBoardSpaceUsage(String qualityWhiteBoardSpaceUsage) {
		this.qualityWhiteBoardSpaceUsage = qualityWhiteBoardSpaceUsage;
	}

	public String getQualityTeacherWriteTime() {
		return qualityTeacherWriteTime;
	}

	public void setQualityTeacherWriteTime(String qualityTeacherWriteTime) {
		this.qualityTeacherWriteTime = qualityTeacherWriteTime;
	}

	public String getQualityWhiteBoardInactiveTime() {
		return qualityWhiteBoardInactiveTime;
	}

	public void setQualityWhiteBoardInactiveTime(String qualityWhiteBoardInactiveTime) {
		this.qualityWhiteBoardInactiveTime = qualityWhiteBoardInactiveTime;
	}

	public String getQualityWhiteBoards() {
		return qualityWhiteBoards;
	}

	public void setQualityWhiteBoards(String qualityWhiteBoards) {
		this.qualityWhiteBoards = qualityWhiteBoards;
	}

	public String getQualityInteraction() {
		return qualityInteraction;
	}

	public void setQualityInteraction(String qualityInteraction) {
		this.qualityInteraction = qualityInteraction;
	}

	public String getQualitySTRatio() {
		return qualitySTRatio;
	}

	public void setQualitySTRatio(String qualitySTRatio) {
		this.qualitySTRatio = qualitySTRatio;
	}

	public String getQualitySameTime() {
		return qualitySameTime;
	}

	public void setQualitySameTime(String qualitySameTime) {
		this.qualitySameTime = qualitySameTime;
	}

	public Double getScoreInteraction() {
		return scoreInteraction;
	}

	public void setScoreInteraction(Double scoreInteraction) {
		this.scoreInteraction = scoreInteraction;
	}

	public Double getScoreWhiteBoardUsage() {
		return scoreWhiteBoardUsage;
	}

	public void setScoreWhiteBoardUsage(Double scoreWhiteBoardUsage) {
		this.scoreWhiteBoardUsage = scoreWhiteBoardUsage;
	}

	public Double getScoreContentUsage() {
		return scoreContentUsage;
	}

	public void setScoreContentUsage(Double scoreContentUsage) {
		this.scoreContentUsage = scoreContentUsage;
	}

	public Double getScoreTotal() {
		return scoreTotal;
	}

	public void setScoreTotal(Double scoreTotal) {
		this.scoreTotal = scoreTotal;
	}

	public String getTeacherName() {
		return teacherName;
	}

	public void setTeacherName(String teacherName) {
		this.teacherName = teacherName;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public Long getSessionStartTime() {
		return sessionStartTime;
	}

	public void setSessionStartTime(Long sessionStartTime) {
		this.sessionStartTime = sessionStartTime;
	}

	public Long getTeacherJoinTime() {
		return teacherJoinTime;
	}

	public void setTeacherJoinTime(Long teacherJoinTime) {
		this.teacherJoinTime = teacherJoinTime;
	}

	public Long getStudentJoinTime() {
		return studentJoinTime;
	}

	public void setStudentJoinTime(Long studentJoinTime) {
		this.studentJoinTime = studentJoinTime;
	}

	public Long getStudentWaitTime() {
		return studentWaitTime;
	}

	public void setStudentWaitTime(Long studentWaitTime) {
		this.studentWaitTime = studentWaitTime;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getShapesandImages() {
		return shapesandImages;
	}

	public void setShapesandImages(String shapesandImages) {
		this.shapesandImages = shapesandImages;
	}

	public String getNoImages() {
		return noImages;
	}

	public void setNoImages(String noImages) {
		this.noImages = noImages;
	}

	public String getColourChanges() {
		return colourChanges;
	}

	public void setColourChanges(String colourChanges) {
		this.colourChanges = colourChanges;
	}

	public String getTeacherWriteLength() {
		return teacherWriteLength;
	}

	public void setTeacherWriteLength(String teacherWriteLength) {
		this.teacherWriteLength = teacherWriteLength;
	}

	public String getStudentWriteLength() {
		return studentWriteLength;
	}

	public void setStudentWriteLength(String studentWriteLength) {
		this.studentWriteLength = studentWriteLength;
	}

	public String getWhiteboardSpaceUsage() {
		return whiteboardSpaceUsage;
	}

	public void setWhiteboardSpaceUsage(String whiteboardSpaceUsage) {
		this.whiteboardSpaceUsage = whiteboardSpaceUsage;
	}

	public String getTeacherWriteTime() {
		return teacherWriteTime;
	}

	public void setTeacherWriteTime(String teacherWriteTime) {
		this.teacherWriteTime = teacherWriteTime;
	}

	public String getStudentWriteTime() {
		return studentWriteTime;
	}

	public void setStudentWriteTime(String studentWriteTime) {
		this.studentWriteTime = studentWriteTime;
	}

	public String getWhiteboardInactiveTime() {
		return whiteboardInactiveTime;
	}

	public void setWhiteboardInactiveTime(String whiteboardInactiveTime) {
		this.whiteboardInactiveTime = whiteboardInactiveTime;
	}

	public String getWhiteBoards() {
		return whiteBoards;
	}

	public void setWhiteBoards(String whiteBoards) {
		this.whiteBoards = whiteBoards;
	}

	public String getHWAveragedensity() {
		return hWAveragedensity;
	}

	public void setHWAveragedensity(String hWAveragedensity) {
		this.hWAveragedensity = hWAveragedensity;
	}

	public String getHWVariance() {
		return hWVariance;
	}

	public void setHWVariance(String hWVariance) {
		this.hWVariance = hWVariance;
	}

	public String getImageAnnotation() {
		return imageAnnotation;
	}

	public void setImageAnnotation(String imageAnnotation) {
		this.imageAnnotation = imageAnnotation;
	}

	public String getActiveDuration() {
		return activeDuration;
	}

	public void setActiveDuration(String activeDuration) {
		this.activeDuration = activeDuration;
	}

	public String getInteraction() {
		return interaction;
	}

	public void setInteraction(String interaction) {
		this.interaction = interaction;
	}

	public String getProblemsDiscussed() {
		return problemsDiscussed;
	}

	public void setProblemsDiscussed(String problemsDiscussed) {
		this.problemsDiscussed = problemsDiscussed;
	}

	public String getTeacherTime() {
		return teacherTime;
	}

	public void setTeacherTime(String teacherTime) {
		this.teacherTime = teacherTime;
	}

	public String getStudentTime() {
		return studentTime;
	}

	public void setStudentTime(String studentTime) {
		this.studentTime = studentTime;
	}

	public String getSTRatio() {
		return sTRatio;
	}

	public void setSTRatio(String sTRatio) {
		this.sTRatio = sTRatio;
	}

	public String getNoActivity() {
		return noActivity;
	}

	public void setNoActivity(String noActivity) {
		this.noActivity = noActivity;
	}

	public String getSameTime() {
		return sameTime;
	}

	public void setSameTime(String sameTime) {
		this.sameTime = sameTime;
	}

	public String getPace() {
		return pace;
	}

	public void setPace(String pace) {
		this.pace = pace;
	}

	public String getAudioDuration() {
		return audioDuration;
	}

	public void setAudioDuration(String audioDuration) {
		this.audioDuration = audioDuration;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public Boolean getAnalysable() {
		return analysable;
	}

	public void setAnalysable(Boolean analysable) {
		this.analysable = analysable;
	}

	public String getAnalysableReason() {
		return analysableReason;
	}

	public void setAnalysableReason(String analysableReason) {
		this.analysableReason = analysableReason;
	}

	public Long getServerTime() {
		return serverTime;
	}

	public void setServerTime(Long serverTime) {
		this.serverTime = serverTime;
	}

	public String getAudioPresent() {
		return audioPresent;
	}

	public void setAudioPresent(String audioPresent) {
		this.audioPresent = audioPresent;
	}

	public String getScheduledTime() {
		return scheduledTime;
	}

	public void setScheduledTime(String scheduledTime) {
		this.scheduledTime = scheduledTime;
	}

	public String getTeacherTechRating() {
		return teacherTechRating;
	}

	public void setTeacherTechRating(String teacherTechRating) {
		this.teacherTechRating = teacherTechRating;
	}

	public String getStudentTechRating() {
		return studentTechRating;
	}

	public void setStudentTechRating(String studentTechRating) {
		this.studentTechRating = studentTechRating;
	}

	public String getAcadRating() {
		return acadRating;
	}

	public void setAcadRating(String acadRating) {
		this.acadRating = acadRating;
	}

	public String getAudioReason() {
		return audioReason;
	}

	public void setAudioReason(String audioReason) {
		this.audioReason = audioReason;
	}

	public String getScreenShareTime() {
		return screenShareTime;
	}

	public void setScreenShareTime(String screenShareTime) {
		this.screenShareTime = screenShareTime;
	}

	public EngagementAnalysis()
	{
		super();
		setAnalysable(false);
		setAnalysableReason("NOT ANALYSABLE");
	}

	public void setEngagementAnalysis(ArrayList<String> temp_edited)
	{	
		set_id(temp_edited.get(0));
		setSubject( temp_edited.get(1));
		setShapesandImages(temp_edited.get(2));
		
		if(temp_edited.get(3).equals("1"))
		setNoImages("Images Absent");
		else setNoImages("Images Present");

		setColourChanges(temp_edited.get(4));
		setTeacherWriteLength(temp_edited.get(5));
		setStudentWriteLength(temp_edited.get(6));
		setWhiteboardSpaceUsage(temp_edited.get(7));
		setTeacherWriteTime(temp_edited.get(8));
		setStudentWriteTime(temp_edited.get(9));
		setWhiteboardInactiveTime(temp_edited.get(10));
		setWhiteBoards(temp_edited.get(11));
		setHWAveragedensity(temp_edited.get(12));
		setHWVariance(temp_edited.get(13));
		setImageAnnotation(temp_edited.get(14));
		setActiveDuration(temp_edited.get(15));
		setInteraction(temp_edited.get(16));
		setProblemsDiscussed(temp_edited.get(17));
		setTeacherTime(temp_edited.get(18));
		setStudentTime(temp_edited.get(19));
		setSTRatio(temp_edited.get(20));
		setNoActivity(temp_edited.get(21));
		setSameTime(temp_edited.get(22));
		setPace(temp_edited.get(23));
		setAudioDuration(temp_edited.get(24));
		setScore(temp_edited.get(25));
		setQuality(temp_edited.get(26));
	}


}