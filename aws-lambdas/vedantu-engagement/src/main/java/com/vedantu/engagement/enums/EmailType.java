package com.vedantu.engagement.enums;

/*
 * Enum for SMS Type Identifiers
 */
public enum EmailType {
	SESSION_VERIFICATION, USER_MESSAGE;
}
