package com.vedantu.engagement.dao.serializers;


import java.util.Collection;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.engagement.entity.AbstractEntity;

@Service
public class AbstractDAO {


	public static final long NO_START = 0;
	public static final long NO_LIMIT = -1;
	public static final long UNINITIALIZED = -1L;

	private static AbstractMongoConfiguration abstractMongoConfiguration = new AbstractMongoConfiguration();

	public AbstractDAO() {
		super();
	}

	public static <E extends AbstractEntity> void saveEntity(E p) {
		setEntityDefaultProperties(p);
		abstractMongoConfiguration.getMongoOperations().save(p, p.getClass().getSimpleName());
	}

	public static <E extends AbstractEntity> void saveAllEntities(Collection<E> entities, String collectionName) {
		setEntityDefaultProperties(entities);
		abstractMongoConfiguration.getMongoOperations().insert(entities, collectionName);
	}
	
	public static <E extends AbstractEntity> void updateAllEntities(Collection<E> entities, String collectionName) {
		setEntityDefaultProperties(entities);
		for(E e: entities){
		saveEntity(e);
		}
	}

	public static <E extends AbstractEntity> void updateEntity(E entity, Query q, Update u) {
		setEntityDefaultProperties(entity);
		abstractMongoConfiguration.getMongoOperations().upsert(q, u, entity.getClass().getSimpleName());
	}

	public static <T extends AbstractEntity> List<T> getEntities(Class<T> calzz) {
		return abstractMongoConfiguration.getMongoOperations().findAll(calzz, calzz.getSimpleName());
	}	
	public <T extends AbstractEntity> T getTeacherPercentile(Class<T> calzz, Long startTimeoFMonth, Long teacherId, int fortnight) {
		Query query = new Query();
		query.addCriteria(Criteria.where("startTimeoFMonth").is(startTimeoFMonth).and("teacherId").is(teacherId).and("fortnight").is(fortnight));
		return abstractMongoConfiguration.getMongoOperations().findOne(query, calzz, calzz.getSimpleName());
	}
	
	public <T extends AbstractEntity> List<T> getEntitiesByStartTimeAndSubject(Class<T> calzz, Long startTime, Long endTime, String subject)
	{
		Query query = new Query();
		query.addCriteria(Criteria.where("startTime").gte(startTime).lte(endTime).and("subject").is(subject));
		query.with(new Sort(Sort.Direction.ASC, "startTime"));
		return abstractMongoConfiguration.getMongoOperations().find(query, calzz, calzz.getSimpleName());
	}
	
	public <T extends AbstractEntity> List<T> getExampleByStartTimeAndSubject(Class<T> calzz, Long startTime, Long endTime, String subject)
	{
		Query query = new Query();
		query.addCriteria(Criteria.where("startTime").gte(startTime).lte(endTime).and("subject").is(subject));
		query.with(new Sort(Sort.Direction.DESC, "startTime"));
		return abstractMongoConfiguration.getMongoOperations().find(query, calzz, calzz.getSimpleName());
	}

	public static <T extends AbstractEntity> T getEntityById(String id, Class<T> calzz) {
		Query query = new Query(Criteria.where("_id").is(id));
		return abstractMongoConfiguration.getMongoOperations().findOne(query, calzz, calzz.getSimpleName());
	}

	public static <T extends AbstractEntity> List<T> runQuery(Query query, Class<T> class1) {
		return abstractMongoConfiguration.getMongoOperations().find(query, class1, class1.getSimpleName());
	}

	public static <T extends AbstractEntity> int queryCount(Query query, Class<T> calzz) {
		return abstractMongoConfiguration.getMongoOperations().find(query, calzz, calzz.getSimpleName()).size();
	}

	public static <E extends AbstractEntity> void setEntityDefaultProperties(Collection<E> entities) {
		if (entities != null && !entities.isEmpty()) {
			for (E entity : entities) {
				setEntityDefaultProperties(entity);
			}
		}
	}

	public static <E extends AbstractEntity> void setEntityDefaultProperties(E entity) {
		if (StringUtils.isEmpty(entity.getId())) {
			entity.setCreationTime(System.currentTimeMillis());
			entity.setCreatedBy(entity.getCallingUserId());
		}
		if (entity.getLastUpdated() == null)
				entity.setLastUpdated(System.currentTimeMillis());
	}
	
}