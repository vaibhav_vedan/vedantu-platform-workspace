package com.vedantu.engagement.manager;
import java.util.*;
import java.util.Map.Entry;
public class ShapeImageAnalysis {

	int rectCount;
	int ellipCount;
	int rightTrianCount;
	int trianCount;
	int imageCount;

	ShapeImageAnalysis(int rc, int ec, int rtc, int tc, int ic)
	{
		rectCount = rc;
		ellipCount = ec;
		rightTrianCount = rtc;
		trianCount = tc;
		imageCount = ic;
	}

	public void commonVicinity(WB wb, HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, Integer page_number, String tid, String sid)
	{
		int x1,y1,x2,y2;
		int distance = 10;
		x1 = wb.dat.coordinates[0][0]; x2 = wb.dat.coordinates[1][0]; y1 = wb.dat.coordinates[0][1]; y2 = wb.dat.coordinates[1][1];

		if(wb.dat.t.equals("TRIANGLE"))
		{
			int eDistance = Math.abs(x2 - x1);
			if(x2 > x1)
				x1 = x1 - eDistance;
			else
				x1 = x1 + eDistance;

			if((x1 > x2) && (y1 < y2))
			{
				int temp;
				temp = x1;x1=x2;x2=temp;
			}
		}

		if((x1 > x2) && (y1 > y2))
		{
			int temp;
			temp = x1;x1=x2;x2=temp;
			temp = y1;y1=y2;y2=temp;
		}
		
		ArrayList<WB> white_board = pages.get(page_number).get(tid);
		if(white_board == null)
			return;
		x1 -= distance;
		y1 -= distance;
		x2 += distance;
		y2 += distance;
		for(int i =0 ;i < white_board.size(); i++)
		{
			int num_points = (white_board.get(i).dat.coordinates!=null)?white_board.get(i).dat.coordinates.length:0;
			Data d = white_board.get(i).dat;
			for(int m =0; m <num_points ; m++)
			{
				int temp_x, temp_y;
				temp_x = d.coordinates[m][0];
				temp_y = d.coordinates[m][1];
				
				if((temp_x >= x1 && temp_x <= x2) && (temp_y >= y1 && temp_y <= y2)) // inside the shape
				{
					if(wb.dat.t.equals("RECT"))
						rectCount++;
					if(wb.dat.t.equals("ELLIPSE"))
						ellipCount++;
					if(wb.dat.t.equals("RIGHTTRIANLE"))
						rightTrianCount++;
					if(wb.dat.t.equals("TRIANLE"))
						trianCount++;
					if(wb.dat.t.equals("IMAGE"))
						imageCount++;
				}
			}
		}
	}
	public void shapeVicinity(HashMap<Integer,HashMap<String,ArrayList<WB>>> pages, String tid, String sid)
	{
		for (Map.Entry<Integer, HashMap<String, ArrayList<WB>>> page_num_entry : pages.entrySet()) 
		{
			Integer page_num = page_num_entry.getKey();
			for (Map.Entry<String, ArrayList<WB>> userId : page_num_entry.getValue().entrySet())
			{
				ArrayList<WB> white_board = new ArrayList<WB>();
				white_board = userId.getValue();
				for(int i =0 ;i < white_board.size();i++)
				{
					try
					{
						if(white_board.get(i).dat.t.equals("RECT") || white_board.get(i).dat.t.equals("ELLIPSE") ||white_board.get(i).dat.t.equals("RIGHTTRIANLE") || white_board.get(i).dat.t.equals("TRIANGLE") || white_board.get(i).dat.t.equals("IMAGE") )
						{
							commonVicinity(white_board.get(i), pages, page_num, tid, sid);
						}
					}
					catch(Exception e)
					{
						System.out.println("error = " +white_board.get(i)._id);
						System.out.println("shapeVicinity "+e.getStackTrace());
					}
				}
			}
		}
	}
}