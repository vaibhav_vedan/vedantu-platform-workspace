package com.vedantu.engagement.manager;
import java.util.*;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.swing.JComponent;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.util.JSON;
import com.vedantu.engagement.util.LogFactory;
import com.vedantu.engagement.viewobject.response.EngagementAnalysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.math.*;
import java.net.UnknownHostException;

public class Engag 
{


	@Autowired
	private LogFactory logfactory;
	private Logger logger = logfactory.getLogger(Engag.class);

	public ArrayList<String> minor_analysis(ArrayList<Double> tamp,ArrayList<Double> samp,String sessionId,String[] ids, String value_name,double pace, String wbdataurl,EngagementAnalysis ea) throws Exception
	{
		logger.info("Audio analysis started");
		double Engagement_Index=0;
		double savg=0;
		int slen=0;
		double tavg=0;
		int tlen=0;
		int siz=0;
		if(samp.size()<tamp.size()) siz = samp.size();
		else siz = tamp.size();
		int zflag=0;	int zcount=0;int zprev=0;
		double zero= (double)samp.get(0)- (double)tamp.get(0);
		logger.info("Engag Check 1");
	//	ArrayList<ArrayList<String>> probs = new ArrayList<ArrayList<String>>();
	//	ArrayList<String> ind_prob = new ArrayList<String>();
	//	ind_prob.add(sessionId);
		ArrayList<ArrayList<String>> ques = new ArrayList<ArrayList<String>>();
	//	ArrayList<String> ind_ques = new ArrayList<String>();
	//	ind_ques.add(sessionId);
		logger.info("Engag Check 2");
		double time_1 = 1.0*Integer.parseInt(ids[6])/(double)3600;
		if(time_1<=0){
			logger.info("time is " +ids[6] + " . time_1 = " + time_1);
			time_1=0.1;
		}
		logger.info("Engag Check 3");
		double tim = 1.0*(double)siz/3600.0;
		if(tim < 0.25 || time_1<0.25){
			logger.info("tim " + tim + ". time_1 "+time_1 );
			return null;
		}
		logger.info("Engag Check 4");

		for(int i=0;i<siz;i++)
		{
			if((double)samp.get(i) > 500)
			{
				savg+=(double)samp.get(i);
				slen++;
			}	
			if((double)tamp.get(i) > 500)
			{
				tavg+=(double)tamp.get(i);
				tlen++;
			}
			if(i>0)
			{
				zero= (double)samp.get(i)- (double)tamp.get(i);
				if(zero < 0 && zflag ==0 && (samp.get(i)>400 || tamp.get(i)>400))
				{
					if((i-zprev) >2)
					{
						zcount++;
						zprev =i;
					}
					//zcount++;
					zflag=1;
				}
				else if (zero > 0 && zflag ==1 && (samp.get(i)>400 || tamp.get(i)>400))
				{
					if((i-zprev) >2)
					{
						zcount++;
						zprev =i;
					}
					zflag=0;
				}
			}


		}
		logger.info("Engag Check 5");
		savg=savg/slen;
		tavg=tavg/tlen;

		double interrupt=0;
		int prob_solving_tolerance=3;  
		double pause=0;
		int tquesflag=0;
		int probflag=0;
		int student_solving_time=0;
		int slow_teacher=0;
		double teacher_time=0;
		double student_time=0;
		double acknowledge=0; 
		int t_rhet=0;
		double student_answering_time=0;
		int tquestions=0;
		int problemgiven=0;
		int echo=0;
		int disturbance=0;
		int teacher_whiteboard_usage=0;
		int student_whiteboard_usage=0;
		int student_paper=0;
		int nothing=0;
		int sametime=0;
	//	ArrayList<String> export= new ArrayList<String>();
		//int icount=0;
		logger.info("Calling WB Analysis");
		mehar_redesign wb_analysis = new mehar_redesign();
		ArrayList<String> tempA = wb_analysis.check(sessionId,ids,wbdataurl);
		logger.info(tempA.size());

		//<<<<<<<<<<<<<<<<<<<<<
		if(tempA == null || tempA.size() < 26)
			return null;
		//>>>>>>>>>>>>>>>>>>>>>

		tempA.remove(27);
		ArrayList<String> temp_edited = new ArrayList<String>();
		temp_edited.add(sessionId);
		temp_edited.add(ids[3]);
		logger.info("Received WB Data");
		for(int t=17;t<=28;t++)
		{
			if(t!=21 && t !=24 && t!=26 && t!=27 && t!=28)
				temp_edited.add(String.valueOf((Double.parseDouble(tempA.get(17))/time_1)));
			else temp_edited.add(tempA.get(17));
			tempA.remove(17);
		}
		int mm;
		if(tempA.get(5).equals("0"))
			mm=1;
		else mm=0;
		temp_edited.add(3,String.valueOf(mm));
		tempA.remove(0);
		int delta = Integer.parseInt(tempA.get(0));
		tempA.remove(0);
//		export.addAll(tempA);
		for(int i=0;i<siz;i++)
		{
			if((double)tamp.get(i)>1500 && (double)samp.get(i)>1500)
			{
				interrupt+=1;
			}
			// else icount =0;
			if((double)tamp.get(i)>500 && ( (double)tamp.get(i)-(double)samp.get(i))>300)  // Change Made from hard coded value to one based on difference
			{
				teacher_time+=1;
				if(i+delta<wb_analysis.points_time_map.length && i+delta >0)
				{
					if(wb_analysis.points_time_map[i+delta][0]>0)
					{
						sametime++;
					}

				}
			}
			else if((double)samp.get(i)>500 && ( (double)samp.get(i)-(double)tamp.get(i))>300)
				student_time+=1;
			else if((double)samp.get(i)<200 && (double)tamp.get(i)<200)
				pause+=1;
			else if((double)tamp.get(i)>20 && (double)tamp.get(i)<50)
				echo+=1;
			else if((double)tamp.get(i)<100 && (double)tamp.get(i)>50)
				disturbance++;
			else {
				if(i+delta<wb_analysis.points_time_map.length && i+delta >0)
				{
					if(wb_analysis.points_time_map[i+delta][0]<20)
					{
						nothing++;
					}
				}
			}
		}
		logger.info("echo " + echo + "	disturbance " + disturbance);
		int temp;
		int i=1;
		int counter=1;
		int answer_1=0;
		int teach_wb=0;int stud_wb=0;
		int y0flag=0;
		//int tquesflag=0;
		int total_stime=0;

		while(i<siz-4)
		{
			if(((double)tamp.get(i-1) > (double)tamp.get(i)) && ((double)tamp.get(i) > (double)tamp.get(i+1)) && i>1)
			{
				int store = i;

				if(((((double)samp.get(store)>500) || (double)samp.get(store+1)>500)) && ((double)samp.get(store+2)- (double)tamp.get(store+2) > 300))    // store +2 is made relative difference instead of absolute values
				{

					store++;
					i++;
					int lala;
					while((((double)samp.get(store) - (double)tamp.get(store))>300) && store < siz-1)  // Changes here for getting dominant voice
					{

						tquesflag=1;
						store++;
						i++;
						student_answering_time++;
						answer_1++;
					}
					/*		if((((double)samp.get(store+1) - (double)tamp.get(store+1))>300) || (((double)samp.get(store+2) - (double)tamp.get(store+2))>300))
					{
						for()
					}
					 */		
					if(tquesflag == 1)
					{

						if(answer_1> 4)
						{
							tquestions++;
	//						ind_ques.add(Integer.toString((i-answer_1)));
	//						ind_ques.add(Integer.toString(answer_1));
						}
						answer_1=0;

					}
					tquesflag=0;

				}

				else if (((double)samp.get(store)> 300 && (double)samp.get(store+1)<500) || ((double)samp.get(store+1)>300 && (double)samp.get(store+2)<500))
				{
					t_rhet+=1;
					i++;
					acknowledge++;
					// rhetorical
				}

				else if (((double)samp.get(store)<100 && (double)samp.get(store+1)<100) && (double)tamp.get(store)< 300 )
				{
					int klm = store;
					while(counter < prob_solving_tolerance)
					{
					//	System.out.println("Problem---- ");
						if((double)tamp.get(store+counter)<400 && (double)samp.get(store+counter)<400 && store < (samp.size()-1))
						{
							counter++;
						}
						else 
						{
							y0flag=1;
							break;
						}
						
					}
					counter =1;
					if(y0flag==0)
					{
						temp=store;
						int tmpflag=0;
						int addTime=0;
						int prob_ttime=0;
						int prob_stime=0;
						while(tmpflag==0)
						{
							
							if(store < tamp.size()-2)
							{
								while((double)tamp.get(store)<200  && (double)samp.get(store)<600 && store < siz-2 )
								{
									student_solving_time++;
									store++;
									i++;
								}
							}
								tmpflag=1;
								store++;i++;
								for(int tmp=0;tmp<3;tmp++)
								{
									int tt = store+tmp;
									if(tt< (siz-2))
									{
										if((double)tamp.get(tt)<200  && (double)samp.get(tt)<400 && tt < siz-2)
										{
											store +=tmp;
											if(store<tamp.size()-2)
											{
												tmpflag=0;
											//	break;
											}
										//	else break;
										}
									}
								}
						}
						
						store++;//i++;
						student_solving_time+=addTime;
						if(student_solving_time>2)
						{
							teach_wb=0;
							stud_wb=0;
							
						//	System.out.println(temp);
							for(int tm =temp;tm < store; tm++)
							{
								if(tm+delta < wb_analysis.points_time_map.length && tm+delta>0)
								{
								teach_wb+= wb_analysis.points_time_map[tm+delta][0];
								stud_wb+= wb_analysis.points_time_map[tm+delta][1];
								}
							}
							for(int prob_wb=temp-5;prob_wb< temp+5;prob_wb++)
							{
								if(prob_wb+delta < wb_analysis.points_time_map.length && prob_wb+delta>0)
								{
								prob_ttime+= wb_analysis.points_time_map[prob_wb+delta][0];
								
								}
								////IMages check for problem
							
								//imo
							}
							int img_check=0;
							for(int img=0;img<wb_analysis.image_time.size();img++)
							{
								if((wb_analysis.image_time.get(img)/1000 + delta) > temp-10 || (wb_analysis.image_time.get(img)/1000 + delta) < temp+10)
									{
									img_check=1;
									break;
									}
							}
							
							for(int k=temp-5;k<temp+student_solving_time+5;k++)
							{
								if(k+delta < wb_analysis.points_time_map.length && k+delta>0)
								{
								prob_stime+= wb_analysis.points_time_map[k+delta][0];
								
								}
								
							}
							if(teach_wb ==0)
								teacher_whiteboard_usage-=2;
							else teacher_whiteboard_usage+=teach_wb;
							
							if(stud_wb == 0)
								student_paper++;
							else student_whiteboard_usage = 2*stud_wb;
							/*IMPORTANT
							 * */
									if(student_solving_time> 15 && (prob_ttime>40 || img_check==1 || prob_stime>20))	//				 
									{ if(student_time >10){
										problemgiven++;
		//					 System.out.println(student_solving_time+" solving_test " + temp);
			//				 ind_prob.add(Double.toString((double)klm*1.0/60));
				//			 ind_prob.add(Integer.toString(student_solving_time));
									}
									}
									}
						total_stime+= student_solving_time;
						student_solving_time=0;
					}
					
					else
					{
						i++;
						y0flag=0;				
					}					
				}
				else i++;							
			}
			else i++;
		}

		int session_length= tamp.size();
		logger.info("Audio Length " + session_length);
		acknowledge = (acknowledge/session_length)*100;
		double newInterrupt = (interrupt/session_length)*100;
		///////////////////////////////////////////////////////
		teacher_time= (teacher_time/session_length)*100;
		student_time= (student_time/session_length)*100;
		double newPause= (pause/session_length)*100;
/*
		int pause_weight= -1;
		int interrupt_weight = 1;
		int tques_weight = 4;
		int ack_weight = 4;
		int interaction_weight=0;
		double t_wb=teacher_whiteboard_usage;
		double s_wb=student_whiteboard_usage;
		double s_paper=4*student_paper;
		double avg_problem_duration=1;
		double problem_weight= ((double)(1.0*t_wb+s_wb+s_paper)/session_length)*100;
		problem_weight = 2;
		//problem_weight = 
		if(problemgiven>0)
		{
			avg_problem_duration= total_stime/problemgiven;
		}

		if(((double)student_time/teacher_time)<0.3)
			interaction_weight=-6;
		else interaction_weight=6;
*/
		//export.add(String.valueOf(ids[6]));

		int ttemp =zcount/3;
		temp_edited.add(String.valueOf(time_1));

		if(tim >= 0.8* time_1 && teacher_time>10)
		{		
			//	
			temp_edited.add(String.valueOf(1.0*(double)(interrupt+tquestions+(t_rhet/2)+ttemp)/tim));
			//

			temp_edited.add(String.valueOf((double)(problemgiven)/tim));
			temp_edited.add(String.valueOf(teacher_time));
			temp_edited.add(String.valueOf(student_time));
			temp_edited.add(String.valueOf((double)1.0*student_time/teacher_time));
			temp_edited.add(String.valueOf((double)nothing/tim));
			temp_edited.add(String.valueOf((double)sametime/tim));
			temp_edited.add(String.valueOf(pace));
			temp_edited.add(String.valueOf(1.0*(double)session_length/3600.0));

			Scoring_1 scr = new Scoring_1(temp_edited,ea);
			temp_edited.add(String.valueOf(scr.ans[0]));

			if(scr.ans[0]<7 || scr.ans[1]>6)
				temp_edited.add("Bad");
			else temp_edited.add("Not Bad");
			Double scoreTotal = (10.0*scr.ans[0])/20;
			ea.setScoreTotal(scoreTotal);
			ea.setAudioPresent("Audio Present");
			//	Database database = new Database(temp_edited);
		}
		else
		{
			int var=temp_edited.size();
			for(;var<25;var++)
			{
				temp_edited.add("0");
			}
			Scoring_1 scr = new Scoring_1(temp_edited,ea);
			temp_edited.add(String.valueOf(scr.ans[0]));

			if(scr.ans[0]<=4 || scr.ans[1]>8)
				temp_edited.add("Bad (WB only)");
			else temp_edited.add("Not Bad (WB only)");
			
			Double scoreTotal = (10.0*scr.ans[0])/14;
			ea.setScoreTotal(scoreTotal);
			ea.setAudioPresent("Audio Absent");
			ea.setAudioReason("Audio Duration less than 80% of Active Duration.");
		}
//
//		//(/*"interrupt = " +*/ interrupt);
//		export.add(String.valueOf(interrupt));
//		//(/*"tquestions = " +*/ tquestions);
//		export.add(String.valueOf(tquestions));
//		//(/*"trhet = " +*/ t_rhet/2);
//		export.add(String.valueOf(t_rhet/2));
//		//(/*"pause = " +*/ pause);
//		export.add(String.valueOf(pause));
//		//(/*"teacher_time = " +*/ teacher_time);
//		export.add(String.valueOf(teacher_time));
//		//(/*"student_time = " +*/ student_time);
//		export.add(String.valueOf(student_time));
//		//(/*"stime = " +*/ total_stime);
//		export.add(String.valueOf(total_stime));
//		//(/*"nothing = " +*/ nothing);
//		export.add(String.valueOf(nothing));
//		//(/*"problem given = " +*/ problemgiven);
//		export.add(String.valueOf(problemgiven));
//		//(/*"crossings = " +*/ zcount/3);
//
//		export.add(String.valueOf(ttemp));
//		//(/*"sametime = " +*/ sametime);
//		export.add((String.valueOf(sametime)));

		//	problemgiven=0;
/*		double pause_index= ((double)(pause-total_stime))/session_length;
		pause_index = 100*pause_index*pause_weight;
		double interrupt_index = ((double)interrupt)/session_length;
		interrupt_index = 100*interrupt_index*interrupt_weight;
		double tques_index = ((double)tquestions*3)/session_length;
		tques_index = 100*tques_index*tques_weight;
		double trhet_index = ((double)(t_rhet*ack_weight))/session_length;
		trhet_index = trhet_index*100;
		probs.add(ind_prob);
		ques.add(ind_ques);
	*/	//	CsvWriter csv = new CsvWriter();
		//	csv.report(probs,"air_probs.csv");
		//	CsvWriter csv2 = new CsvWriter();
		//	csv2.report(ques,"air_ques.csv");
//		ArrayList<ArrayList<String>> edited_out = new ArrayList<ArrayList<String>>();
//		edited_out.add(temp_edited);
		//	CsvWriter csv1 = new CsvWriter();
		//	csv1.report(edited_out,"edited_doc.csv");
/*		Engagement_Index=interaction_weight + pause_index + interrupt_index + 
				tques_index + trhet_index +
				(problem_weight*100*(problemgiven*avg_problem_duration/2)/session_length);
		if(newPause > 45)
			Engagement_Index -=10;
		if(teacher_time <20)
			Engagement_Index-=10;		
		export.add(String.valueOf(Engagement_Index));
		export.add(String.valueOf(pace));
	*/
		logger.info("Exiting minor_analysis");
		return temp_edited;

	}

	public static void deleteFile(File element) {
		if (element.isDirectory()) {
			for (File sub : element.listFiles()) {
				deleteFile(sub);
			}
		}
		element.delete();
	}



	public  ArrayList<Double> stitch(ArrayList<Double> alist1)
	{
		int i,fs=8000;
		int avg = 0;
		double y0;
		ArrayList<Double> tamps = new ArrayList<Double>();
		for(i=0;i<alist1.size();i++)
		{
			if(i%fs == 0)
			{
				y0= avg/fs;
				tamps.add(y0);
				avg=0;
			}
			avg += Math.abs((double)alist1.get(i));
		}

		return (tamps);
	}


	public  ArrayList<String> WBvalue(String sessionId, String[] st, String wbdataurl,EngagementAnalysis ea) throws IOException, Exception
	{
		logger.info("Audio data inconsistent: WBvalue method used");
		mehar_redesign wb_analysis = new mehar_redesign();
		double tim = 1.0*(double)Integer.parseInt(st[6]);
		//tim = 1.0*(double)Integer.parseInt(st[6]);
		
		tim = tim/3600;
		//tim=1;
		if(tim<=0.25)return null;
		ArrayList<String> ind = wb_analysis.check(sessionId, st, wbdataurl);
		logger.info("Received WhiteBoard Data");
	//	System.out.println("size of the white board array = "+ind.size());
		if(ind == null)
			return null;
		if(ind.size()<26)
			return null;
		//		String hwr = ind.get(17);
		//	ind.remove(17);
		ind.remove(27);
		ArrayList<String> tmp_e = new ArrayList<String>();
		tmp_e.add(sessionId);
		tmp_e.add(st[3]);
		for(int t=17;t<=28;t++)
		{

			if(t!=21 && t !=24 && t!=26 && t!=27 && t!=28)
				tmp_e.add(String.valueOf((Double.parseDouble(ind.get(17))/tim)));
			else tmp_e.add(ind.get(17));
			ind.remove(17);



		}
		int mm;
		if(ind.get(5).equals("0"))
			mm=1;
		else mm=0;
		tmp_e.add(3,String.valueOf(mm));
		tmp_e.add(String.valueOf(tim));
		String lm = "";
		if(ind.size() >3)
			lm = ind.get(0);
		ind.remove(0);
		ind.remove(0);
		ind.add(0,sessionId);
		ind.add(1, st[5]);
		ind.add(2, st[3]);
		ind.add(3, st[0]);

		ind.add(4, st[1]);
		//	ind.add(lm);
		ind.add(5,st[8]);
		//	ind.add(hwr);
		ind.add(st[6]);
		for(int m=16;m<=24;m++)
		{
			tmp_e.add("0");
		}
		Scoring_1 scr = new Scoring_1(tmp_e,ea);
		tmp_e.add(String.valueOf(scr.ans[0]));

		if(scr.ans[0]<=4 || scr.ans[1]>8)
			tmp_e.add("Bad (WB only)");
		else tmp_e.add("Not Bad (WB only)");
		
		Double scoreTotal = (10.0*scr.ans[0])/14;
		ea.setScoreTotal(scoreTotal);
		ea.setAudioPresent("Audio Absent");

		ArrayList<ArrayList<String>> temp_out = new ArrayList<ArrayList<String>>();
		ArrayList<ArrayList<String>> temp_e = new ArrayList<ArrayList<String>>();
		temp_out.add(ind);
		temp_e.add(tmp_e);
		//	CsvWriter csv1 = new CsvWriter();
		//	csv1.report(temp_e,"edited_doc.csv");
		//	CsvWriter csv = new CsvWriter();
		//	csv.report(temp_out,"air_new1.csv");
		return tmp_e;
	}


	public EngagementAnalysis getEngage(Long id, String beginUrl, String payloadUserId, String wbdataurl) //throws Exception 
	{

		String finname1,finname2,foutname1,foutname2;
		double index;

		EngagementAnalysis engagementAnalysis = new EngagementAnalysis();
//		ArrayList<ArrayList<String>> finalout = new ArrayList<ArrayList<String>>();
		//ArrayList<String> sids = readFile("sessionidslist.txt");	//temp_id, test_id done
		ArrayList<String> sids = new ArrayList<String>();
		sids.add(Long.toString(id));
		ArrayList<String> index_1 =new ArrayList<String>();
		String teacher_id="";
		String student_id="";
		String teacherName="",studentName="";
		String teacherTech="";
		String studentTech="";
		String acadRating = "";
		Long teacherTime=(long)0;
		Long studentTime=(long)0;
		Long startTime = (long)0;
		Long endTime = (long)0;
		for(int sessionnos=0;sessionnos<sids.size();sessionnos++)
		{
			ArrayList<Double> teacher=new ArrayList<Double>();
			ArrayList<Double> student=new ArrayList<Double>();
			String sessionId = sids.get(sessionnos);
			//String[] st = 
			ExtractAudio ea = new ExtractAudio();
			//AudioTemp atemp=ea.ExtractFiles(sessionId, beginUrl, payloadUserId);
			String[] st= new String[18];
			try {
				st = ea.ExtractFiles(sessionId, beginUrl, payloadUserId,wbdataurl);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				logger.error("UnsupportedEncodingException");
				st[0] = "Session Not Ended";
			} catch (IOException e) {
				// TODO Auto-generated catch block
				logger.error("IOException");
				st[0] = "Session Not Ended";
			}
			teacher_id = ""+st[10];
			student_id = ""+st[11];

			teacherName = "" + st[12];
			studentName = ""+st[13];
			teacherTech = ""+ st[18];
			studentTech = ""+ st[19];
			acadRating = ""+ st[20];
			if(st[14] != null)
			{
				teacherTime = 0+Long.parseLong(st[14]);
				studentTime = 0+Long.parseLong(st[15]);
				startTime = 0+Long.parseLong(st[16]);
				endTime = 0+Long.parseLong(st[17]);
			}
			if(st[0].equals("Session Not Ended"))
			{
				return null;
			}
			if(st[0].equals("-1") || st[0].equals("-3"))
			{
				try {
					index_1 = WBvalue(sessionId, st, wbdataurl,engagementAnalysis);
					if(st[21]!=null)
					engagementAnalysis.setAudioReason(st[21]);
					else 
						engagementAnalysis.setAudioReason("Error in Downloading Files.");
						
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error("IOException");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception");
				}

				continue;
			}
			if(st[0].equals("-2"))
			{
				index_1.add(Long.toString(id));
				for(int p=1 ; p<=25; p++)
				{
					index_1.add("0");
				}
				index_1.add("Not Applicable");

				engagementAnalysis.setAnalysableReason("Session Info Not Found");

				continue;
			}
			
			if(st[9].equals("-1"))
			{
				try {
					index_1 = WBvalue(sessionId, st, wbdataurl,engagementAnalysis);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error("IOException");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception");
				}
				continue;
			}
			logger.info("Teacher Files # "+ st[2]);
			int abc=0;
			abc = Integer.parseInt(st[2]);

			if(abc>0)
			{
				double []tcher = new double[abc];
				double tcher1=0;
				int div=0;
//				for(int i =0;i<abc;i++)
//				{
//					//	foutname1="out1.wav";
//					if(st[9].equals("0"))
//					{
//						MP3toArray mp3t = new MP3toArray();
//						mp3t.change(("/tmp/t"+(i+1) +".mp3"),ea.teacher_byte.get(i));
//						//ea.teacher_byte.remove(0);
//						
//						logger.info("teacher size" +ea.teacher_byte.get(i).length);
//						//	MP3toWav mp3t=new MP3toWav(("mp3/"+sessionId+"/t"+(i+1) +".mp3"),foutname1);
//						//	Wav2Text wavt= new Wav2Text(foutname1);
//						//	wavt.convert();
//						teacher.addAll(stitch(mp3t.x));
//						Pace pc = new Pace();
//						tcher[i]= pc.Pace(mp3t.x);
//						if(tcher[i]!=0){
//	
//							tcher1 += tcher[i];
//							div+=1;
//						}
//					}
//					else
//					{
//						Wav2Text mp3t;
//						try {
//							mp3t = new Wav2Text("/tmp/t"+(i+1) +".wav");
//						
//						mp3t.convert();
//						teacher.addAll(stitch(mp3t.x));
//						Pace pc = new Pace();
//						tcher[i]= pc.Pace(mp3t.x);
//						if(tcher[i]!=0){
//	
//							tcher1 += tcher[i];
//							div+=1;
//						}
//						} catch (IOException e) {
//							// TODO Auto-generated catch block
//							logger.error("IOException");
//						}
//						
//					}
//					//System.out.println("teacher size "+ teacher.size());
//
//				}
//		/*		for(int i =0;i<abc-1;i++)
//				{
//					ea.teacher_byte.remove(0);
//				}
//			*/	for(int i =0;i<Integer.parseInt(st[7]);i++)
//				{
//					//	foutname2="out2.wav";
//				if(st[9].equals("0")){
//					MP3toArray mp3s = new MP3toArray();
//					//	ArrayList<Double>temps=new ArrayList<Double>();
//					mp3s.change(("/tmp/s"+(i+1) +".mp3"),ea.student_byte.get(i));
//					
//					student.addAll(stitch(mp3s.x));
//				}
//				else {
//					//ea.student_byte.remove(0);
//					//MP3toWav mp3s=new MP3toWav(("mp3/"+sessionId+"/s"+(i+1) +".mp3"),foutname2);
//					Wav2Text mp3s;
//					try {
//						mp3s = new Wav2Text("/tmp/s"+(i+1) +".wav");
//					
//					mp3s.convert();
//					
//					student.addAll(stitch(mp3s.x));
//					
//					} catch (IOException e) {
//						// TODO Auto-generated catch block
//						logger.error("IOException");
//					}
//				}
//					
//					//System.out.println("student size "+ student.size());
//				//	logger.info("student size" +ea.student_byte.get(i).length);
//				}
//			/*	for(int i =0;i<Integer.parseInt(st[7])-1;i++)
//				{
//					ea.student_byte.remove(0);
//				}
//				*/
//			
			
			int an_flag;
				//if(abc != Integer.parseInt(st[7]))
				//{
				int diff=ea.teacher_byte.size() - ea.student_byte.size();	
				if(diff<=500 && diff>=-500)
					an_flag = 1;
				else an_flag =0;
				//	}
				if(ea.student_byte.size()<10)
					an_flag=0;
				
				index_1 =new ArrayList<String>();
				if(an_flag ==1)
				{
					try {
						tcher1 = ea.pace_final;
						index_1= minor_analysis(ea.teacher_byte,ea.student_byte,sessionId,st,"teacher_1.txt",tcher1,wbdataurl,engagementAnalysis);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error("Exception");
					}
				}
				else 
				{
					try {
						index_1 = WBvalue(sessionId, st, wbdataurl,engagementAnalysis);
						engagementAnalysis.setAudioReason("Audio Length difference between Teacher and Student is Large.");
					} catch (IOException e) {
						// TODO Auto-generated catch block
						logger.error("IOException");
					} catch (Exception e) {
						// TODO Auto-generated catch block
						logger.error("Exception");
					}
				}
	//			finalout.add(index_1);
	//			ArrayList<ArrayList<String>> temp_out = new ArrayList<ArrayList<String>>();
	//			temp_out.add(index_1);
			}
			else 
			{
				try {
					index_1 = WBvalue(sessionId, st, wbdataurl,engagementAnalysis);
					engagementAnalysis.setAudioReason("Some audio files not present.");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					logger.error("IOException");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					logger.error("Exception");
				}
		//		if(index_1 != null)
		//			finalout.add(index_1);

			}
		}
		//	if(index_1.size()<25)index_1 = null;


		if(index_1!=null)
			{
				if(!index_1.get(26).equals("Not Applicable"))
				{
					engagementAnalysis.setEngagementAnalysis(index_1);
					engagementAnalysis.setAnalysable(true);
					engagementAnalysis.setAnalysableReason("ANALYSABLE");
				}
				else engagementAnalysis.setEngagementAnalysis(index_1);
			}
		else
		{
			index_1 = new ArrayList<String>();
			index_1.add(Long.toString(id));
			for(int p=1 ; p<=25; p++)
			{
				index_1.add("0");
			}
			index_1.add("Not Applicable");
			engagementAnalysis.setEngagementAnalysis(index_1);
		}
		
		if(!teacher_id.equals("") && !student_id.equals(""))
		{
			engagementAnalysis.setTeacherId(teacher_id);
		
			engagementAnalysis.setStudentId(student_id);
			engagementAnalysis.setTeacherName(teacherName);
			engagementAnalysis.setStudentName(studentName);
			engagementAnalysis.setSessionStartTime(startTime);
			engagementAnalysis.setTeacherJoinTime(teacherTime);
			engagementAnalysis.setStudentJoinTime(studentTime);
			Long wait = teacherTime-studentTime;
			if(wait<0)wait =(long)0;
			wait = wait/1000;
			engagementAnalysis.setStudentWaitTime(wait);
			Long scheduledTime = endTime-startTime;
			scheduledTime = (scheduledTime/(1000*60));
			engagementAnalysis.setScheduledTime(scheduledTime.toString());
		}
		else {
			engagementAnalysis.setTeacherId("0");
			
			engagementAnalysis.setStudentId("0");
			engagementAnalysis.setTeacherName(teacherName);
			engagementAnalysis.setStudentName(studentName);
			engagementAnalysis.setSessionStartTime(startTime);
			engagementAnalysis.setTeacherJoinTime(teacherTime);
			engagementAnalysis.setStudentJoinTime(studentTime);
			Long wait = teacherTime-studentTime;
			if(wait<0)wait =(long)0;
			wait = wait/1000;
			engagementAnalysis.setStudentWaitTime(wait);

			Long scheduledTime = endTime-startTime;
			scheduledTime = (scheduledTime/(1000*60));
			engagementAnalysis.setScheduledTime(scheduledTime.toString());
		}
		
		engagementAnalysis.setAcadRating(acadRating);
		engagementAnalysis.setTeacherTechRating(teacherTech);
		engagementAnalysis.setStudentTechRating(studentTech);
		logger.info(engagementAnalysis.getAnalysable());
		logger.info(engagementAnalysis.getAnalysableReason());
		//	CsvWriter csv = new CsvWriter();
		//	csv.report(finalout,"air_new_temmp.csv");
		//	File dir = new File("mp3/"+ String.valueOf(id));
		//	deleteFile(dir);
		//	File dir1 = new File("mp3");
		//	deleteFile(dir1);
		return engagementAnalysis;

	}

}
