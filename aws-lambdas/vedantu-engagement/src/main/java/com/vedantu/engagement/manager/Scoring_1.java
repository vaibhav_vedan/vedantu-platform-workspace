package com.vedantu.engagement.manager;
import java.util.*;

import com.vedantu.engagement.viewobject.response.EngagementAnalysis;

public class Scoring_1 {

	int[] ans;
	public int[] Score(ArrayList<String> param, double[] val,EngagementAnalysis ea)
	{
		int content;
		int whiteboard;
		int interaction;
		int scr=0;
		int flg=0;
		int[] ret = new int[2];
		String temp = param.get(14);
		param.remove(14);
		double sh = Double.parseDouble(param.get(2));
		double cc = Double.parseDouble(param.get(4));
		double twl =  Double.parseDouble(param.get(5));
		double wbsu = Double.parseDouble(param.get(7));
		double twt = Double.parseDouble(param.get(8));
		double wit = Double.parseDouble(param.get(10));
		double nwb = Double.parseDouble(param.get(11));
		double inter = Double.parseDouble(param.get(15));
		double str = Double.parseDouble(param.get(19));
		double st = Double.parseDouble(param.get(21));
		param.add(14,temp);
		if(sh<val[0]){
			scr +=0;flg+=1;
			ea.setQualityShapesandImages("Bad");
			}
		else if(sh<val[1]){
			scr +=1;
			ea.setQualityShapesandImages("Okay");
			}
		else {
			scr+=2;
			ea.setQualityShapesandImages("Good");
			}
		
		if(cc<val[2]){
			scr +=0;flg+=1;
			ea.setQualityColorChanges("Bad");
			}
		else if(cc<val[3]){
			scr +=1;
			ea.setQualityColorChanges("Okay");
			}
		else {
			scr+=2;
			ea.setQualityColorChanges("Good");
		}
		content = scr;
		
		
		if(twl<val[4]){
			scr +=0;flg+=1;
			ea.setQualityTeacherWriteLength("Bad");
			}
		else if(twl<val[5]){
			scr +=1;
			ea.setQualityTeacherWriteLength("Okay");
		}
		else {
			scr+=2;
			ea.setQualityTeacherWriteLength("Good");
		}
		
		if(wbsu<val[6]){
			scr +=0;flg+=1;
			ea.setQualityWhiteBoardSpaceUsage("Bad");
		}
		else if(wbsu<val[7]){
			scr +=1;
			ea.setQualityWhiteBoardSpaceUsage("Okay");
		}
		else {
			scr+=2;
			ea.setQualityWhiteBoardSpaceUsage("Good");
		}
		
		if(twt<val[8]){
			scr +=0;flg+=1;
			ea.setQualityTeacherWriteTime("Bad");
		}
		else if(twt<val[9]){
			scr +=1;
			ea.setQualityTeacherWriteTime("Okay");
		}
		else {
			scr+=2;
			ea.setQualityTeacherWriteTime("Good");
		}
		
		if(wit>val[10]){
			scr +=0;flg+=1;
			ea.setQualityWhiteBoardInactiveTime("Bad");
		}
		else if(wit>val[11]){
			scr +=1;
			ea.setQualityWhiteBoardInactiveTime("Okay");
		}
		else {
			scr+=2;
			ea.setQualityWhiteBoardInactiveTime("Good");
		}
					
		if(nwb<val[12]){
			scr +=0;flg+=1;
			ea.setQualityWhiteBoards("Bad");
		}
		else if(nwb<val[13]){
			scr +=1;
			ea.setQualityWhiteBoards("Okay");
		}
		else {
			scr+=2;
			ea.setQualityWhiteBoards("Good");
		}
		
		whiteboard = scr-content;
		
		
		if(inter<val[14]){
			scr +=0;flg+=1;
			ea.setQualityInteraction("Bad");
			}
		else if(inter<val[15]){
			scr +=1;
			ea.setQualityInteraction("Okay");
		}
		else {
			scr+=2;
			ea.setQualityInteraction("Good");
		}
		
		if(str<val[16]){
			scr +=0;flg+=1;
			ea.setQualitySTRatio("Bad");
			}
		else if(str<val[17]){
			scr +=1;
			ea.setQualitySTRatio("Okay");
		}
		else {
			scr+=2;
			ea.setQualitySTRatio("Good");
		}
		
		if(st<val[18]){
			scr +=0;flg+=1;
			ea.setQualitySameTime("Bad");
			}
		else if(st<val[19]){
			scr +=1;
			ea.setQualitySameTime("Okay");
		}
		else {
			scr+=2;
			ea.setQualitySameTime("Good");
		}
		interaction = scr - whiteboard - content;
		Double con = (10.0*content)/4;
		Double wb = (10.0*whiteboard)/10;
		Double audio = (10.0*interaction)/6;
		ea.setScoreContentUsage(con);
		ea.setScoreWhiteBoardUsage(wb);
		ea.setScoreInteraction(audio);
		ret[0]=scr;
		ret[1]=flg;
		return ret;
	}
	public Scoring_1(ArrayList<String> param,EngagementAnalysis ea)
	{
		String score;
		
		double[] val = new double[24];
		
		if(param.get(1).equalsIgnoreCase("Physics"))
		{
			val[0]=4.0;
			val[1]=12.0;
			val[2]=6;
			val[3]=30.0;
			val[4]=15000;
			val[5]=150000;
			val[6]=45;
			val[7]=68;
			val[8]=180;
			val[9]=940;
			val[10]=89;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=85;
			val[15]=270;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=60;
			val[19]=420;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		else if(param.get(1).equalsIgnoreCase("Chemistry"))
		{
			val[0]=2.5;
			val[1]=10.0;
			val[2]=6;
			val[3]=20.0;
			val[4]=15000;
			val[5]=150000;
			val[6]=45;
			val[7]=68;
			val[8]=250;
			val[9]=1250;
			val[10]=85;
			val[11]=72;
			val[12]=7.0;
			val[13]=16.0;
			val[14]=68;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.20;
			val[18]=58;
			val[19]=550;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		else if(param.get(1).equalsIgnoreCase("Mathematics"))
		{
			val[0]=4.0;
			val[1]=13.0;
			val[2]=6;
			val[3]=30.0;
			val[4]=12000;
			val[5]=115000;
			val[6]=45;
			val[7]=68;
			val[8]=180;
			val[9]=940;
			val[10]=85;
			val[11]=72;
			val[12]=8.0;
			val[13]=16.0;
			val[14]=68;
			val[15]=230;
			val[16]=0.04;
			val[17]=0.30;
			val[18]=26;
			val[19]=350;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		else
		{
			val[0]=2.0;
			val[1]=9.0;
			val[2]=6;
			val[3]=20.0;
			val[4]=12000;
			val[5]=140000;
			val[6]=45;
			val[7]=68;
			val[8]=150;
			val[9]=890;
			val[10]=88;
			val[11]=72;
			val[12]=5.0;
			val[13]=11.0;
			val[14]=70;
			val[15]=200;
			val[16]=0.04;
			val[17]=0.15;
			val[18]=61;
			val[19]=515;
			val[20]=4.75;
			val[21]=10;
			val[22]=1.9;
			val[23]=1.5;
		}
		ans = Score(param,val,ea);

		//return ans;
	}
}
