package com.vedantu.engagement.manager;
import java.io.FileWriter; 
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.vedantu.engagement.viewobject.response.EngagementAnalysis;

import java.util.Arrays;
import java.util.List;




public class CsvWriter { 

	private final String CRLF = "\r\n"; 
	private String delimiter = ",";
	public StringBuilder sb = new StringBuilder();

	public void setDelimiter(String delimiter) { 
		this.delimiter = delimiter; 
	} 

	public CsvWriter() {
		super();
		sb.append("SessionId");
		sb.append(delimiter);
		sb.append("Subject");
		sb.append(delimiter);
		sb.append("TeacherId");
		sb.append(delimiter);
		sb.append("StudentId");
		sb.append(delimiter);
		sb.append("Teacher Name");
		sb.append(delimiter);
		sb.append("Student Name");
		sb.append(delimiter);
		sb.append("Student Wait Time in seconds");
		sb.append(delimiter);
		sb.append("Audio Status");
		sb.append(delimiter);
		sb.append("Quality Shapes and Images");
		sb.append(delimiter);
		sb.append("Quality Color Changes");
		sb.append(delimiter);
		sb.append("Quality TeacherWriteLength");
		sb.append(delimiter);
		sb.append("Quality WhiteBoardSpaceUsage");
		sb.append(delimiter);
		sb.append("Quality Teacher Write Time");
		sb.append(delimiter);
		sb.append("Quality WB Inactive Time");
		sb.append(delimiter);
		sb.append("Quality WhiteBoards");
		sb.append(delimiter);
		sb.append("Quality Interaction");
		sb.append(delimiter);
		sb.append("Quality STRatio");
		sb.append(delimiter);
		sb.append("Quality SameTime");
		sb.append(delimiter);
		sb.append("Score Content Usage");
		sb.append(delimiter);
		sb.append("Score WB Usage");
		sb.append(delimiter);
		sb.append("Score Interaction");
		sb.append(delimiter);
		sb.append("Score Total");
		sb.append(delimiter);
		sb.append(delimiter);
		sb.append(delimiter);
		
		sb.append("ShapesandImages");
		sb.append(delimiter);
		sb.append("NoImages");
		sb.append(delimiter);
		sb.append("ColourChanges");
		sb.append(delimiter);
		sb.append("TeacherWriteLength");
		sb.append(delimiter);
		sb.append("StudentWriteLength");
		sb.append(delimiter);
		sb.append("WhiteboardSpaceUsage");
		sb.append(delimiter);
		sb.append("TeacherWriteTime");
		sb.append(delimiter);
		sb.append("StudentWriteTime");
		sb.append(delimiter);
		sb.append("WhiteboardInactiveTime");
		sb.append(delimiter);
		sb.append("WhiteBoards");
		sb.append(delimiter);
		sb.append("HWAveragedensity"); 
		sb.append(delimiter);
		sb.append("HWVariance"); 
		sb.append(delimiter);
		sb.append("ImageAnnotation"); 
		sb.append(delimiter);
		sb.append( "ActiveDuration"); 
		sb.append(delimiter);
		sb.append( "Interaction"); 
		sb.append(delimiter);
		sb.append( "ProblemsDiscussed"); 
		sb.append(delimiter);
		sb.append( "TeacherTime");
		sb.append(delimiter);
		sb.append( "StudentTime");
		sb.append(delimiter);
		sb.append( "STRatio");
		sb.append(delimiter);
		sb.append( "NoActivity");
		sb.append(delimiter);
		sb.append( "SameTime");
		sb.append(delimiter);
		sb.append( "Pace");
		sb.append(delimiter);
		sb.append( "AudioDuration");
		sb.append(delimiter);
		sb.append( "Score");
		sb.append(delimiter);
		sb.append( "Quality");
		sb.append(delimiter);
		sb.append("Analysable");
		sb.append(delimiter);
		sb.append( "AnalysableReason");


		// TODO Auto-generated constructor stub
	}

	public void exportCsv(EngagementAnalysis list, String filename) { 
		//			Filesb sb = new Filesb(filename,true);
		sb.append(delimiter+ CRLF);

	//	for (int j = 0; j < list.size(); j++) { 
			sb.append( list.get_id()); 
			sb.append(delimiter);
			sb.append( list.getSubject()); 
			sb.append(delimiter);
			sb.append( list.getTeacherId());
			sb.append(delimiter);
			sb.append( list.getStudentId());
			sb.append(delimiter);
			sb.append( list.getTeacherName()); 
			sb.append(delimiter);
			sb.append( list.getStudentName()); 
			sb.append(delimiter);
			sb.append( list.getStudentWaitTime()); 
			sb.append(delimiter);
			sb.append( list.getAudioPresent()); 
			sb.append(delimiter);
			sb.append( list.getQualityShapesandImages()); 
			sb.append(delimiter);
			sb.append( list.getQualityColorChanges()); 
			sb.append(delimiter);

			sb.append( list.getQualityTeacherWriteLength()); 
			sb.append(delimiter);
			sb.append( list.getQualityWhiteBoardSpaceUsage()); 
			sb.append(delimiter);
			sb.append( list.getQualityTeacherWriteTime()); 
			sb.append(delimiter);
			sb.append( list.getQualityWhiteBoardInactiveTime()); 
			sb.append(delimiter);
			sb.append( list.getQualityWhiteBoards()); 
			sb.append(delimiter);
			sb.append( list.getQualityInteraction()); 
			sb.append(delimiter);
			sb.append( list.getQualitySTRatio()); 
			sb.append(delimiter);
			sb.append( list.getQualitySameTime()); 
			sb.append(delimiter);
			sb.append( list.getScoreContentUsage()); 
			sb.append(delimiter);
			sb.append( list.getScoreWhiteBoardUsage()); 
			sb.append(delimiter);
			sb.append( list.getScoreInteraction()); 
			sb.append(delimiter);
			sb.append( list.getScoreTotal()); 
			sb.append(delimiter);
			sb.append(delimiter);
			sb.append(delimiter);
			sb.append( list.getShapesandImages()); 
			sb.append(delimiter);
			sb.append( list.getNoImages()); 
			sb.append(delimiter);
			sb.append( list.getColourChanges()); 
			sb.append(delimiter);
			sb.append( list.getTeacherWriteLength()); 
			sb.append(delimiter);
			sb.append( list.getStudentWriteLength()); 
			sb.append(delimiter);
			sb.append( list.getWhiteboardSpaceUsage()); 
			sb.append(delimiter);
			sb.append( list.getTeacherWriteTime()); 
			sb.append(delimiter);
			sb.append( list.getStudentTime()); 
			sb.append(delimiter);
			sb.append( list.getWhiteboardInactiveTime()); 
			sb.append(delimiter);
			sb.append( list.getWhiteBoards()); 
			sb.append(delimiter);
			sb.append( list.getHWAveragedensity()); 
			sb.append(delimiter);
			sb.append( list.getHWVariance()); 
			sb.append(delimiter);
			sb.append( list.getImageAnnotation()); 
			sb.append(delimiter);
			sb.append( list.getActiveDuration()); 
			sb.append(delimiter);
			sb.append( list.getInteraction()); 
			sb.append(delimiter);
			sb.append( list.getProblemsDiscussed()); 
			sb.append(delimiter);
			sb.append( list.getTeacherTime());
			sb.append(delimiter);
			sb.append( list.getStudentTime());
			sb.append(delimiter);
			sb.append( list.getSTRatio());
			sb.append(delimiter);
			sb.append( list.getNoActivity());
			sb.append(delimiter);
			sb.append( list.getSameTime());
			sb.append(delimiter);
			sb.append( list.getPace());
			sb.append(delimiter);
			sb.append( list.getAudioDuration());
			sb.append(delimiter);
			sb.append( list.getScore());
			sb.append(delimiter);
			sb.append( list.getQuality());
			sb.append(delimiter);
			sb.append( Boolean.toString(list.getAnalysable()));
			sb.append(delimiter);
			sb.append( list.getAnalysableReason());
			//						sb.append(delimiter);
			//						sb.append( Long.toString(list.getServerTime())); 
			//Add delimiter and end of the line 
			//if (j < list.size() - 1) { 
				sb.append(delimiter + CRLF); 
			//} 
		}
	

}