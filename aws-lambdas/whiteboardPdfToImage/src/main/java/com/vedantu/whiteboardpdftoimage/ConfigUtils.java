package com.vedantu.whiteboardpdftoimage;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map.Entry;
import java.util.Properties;

public class ConfigUtils {

	public Properties properties = new Properties();
	java.util.logging.Logger logger = java.util.logging.Logger.getGlobal();
	public ConfigUtils(String env) {
		loadCommonProperties();
		loadEnvProperties(env);
		logger.info("Config Utils started");
	}

	private void loadCommonProperties() {
		InputStream is = null;
		Properties properties = new Properties();

		try {
			final String appenginePropertiesFilePath = "application.properties";
			is = ConfigUtils.class.getClassLoader().getResourceAsStream(
					appenginePropertiesFilePath);
			properties.load(is);
			for (Entry<Object, Object> entry : properties.entrySet()) {
				this.properties.put(entry.getKey(), entry.getValue());
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new RuntimeException(e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {

				}
			}
			properties.clear();
		}
	}

	private void loadEnvProperties(String env) {
		InputStream is = null;
		Properties properties = new Properties();
		String confDir = "ENV-"
				+ env;
		try {
			final String appenginePropertiesFilePath = confDir
					+ java.io.File.separator + "application.properties";
			is = ConfigUtils.class.getClassLoader().getResourceAsStream(
					appenginePropertiesFilePath);
			properties.load(is);
			for (Entry<Object, Object> entry : properties.entrySet()) {
				this.properties.put(entry.getKey(), entry.getValue());
			}
		} catch (Exception e) {
			logger.info(e.getMessage());
			throw new RuntimeException(e);
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {

				}
			}
			properties.clear();
		}

	}

	public String getStringValue(String key) {

		String value = this.properties.getProperty(key);
		return value == null ? value : value.trim();
	}

	public int getIntValue(String key) {

		return Integer.parseInt(properties.getProperty(key).trim());
	}

	public long getLongValue(String key) {

		return Long.parseLong(properties.getProperty(key).trim());
	}

	public boolean getBooleanValue(String key) {

		return properties.getProperty(key) == null ? false : Boolean
				.parseBoolean(properties.getProperty(key).trim());
	}

	public float getFloatValue(String key) {

		return Float.parseFloat(properties.getProperty(key).trim());
	}

}
