/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.whiteboardpdftoimage;

import com.google.auth.oauth2.ServiceAccountCredentials;
import com.google.cloud.storage.Acl;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author jeet
 */
public class GCSFileUploader {

    private String clientId;
    private Storage storage = null;
    private String secretKeyFileNameJson;

    public GCSFileUploader(String _clientId, String _secretKeyFileNameJson) {
        secretKeyFileNameJson = _secretKeyFileNameJson;
        clientId = _clientId;
        try {
            storage = StorageOptions.newBuilder().setProjectId(clientId).setCredentials(ServiceAccountCredentials.fromStream(getClass().getClassLoader().getResourceAsStream(secretKeyFileNameJson))).build().getService();
        } catch (Exception e) {
        }
    }
    
    public String uploadFile(java.io.File file, String path, String bucketName, String acl) throws IOException {
        BlobInfo blobInfo;
        blobInfo = storage.create(
                   BlobInfo
                           .newBuilder(bucketName, path)
                           .setAcl(new ArrayList<>(Arrays.asList(Acl.of(Acl.User.ofAllUsers(), Acl.Role.READER))))
                           .build(),
                   new FileInputStream(file));
        return blobInfo.getMediaLink();
    }
}
