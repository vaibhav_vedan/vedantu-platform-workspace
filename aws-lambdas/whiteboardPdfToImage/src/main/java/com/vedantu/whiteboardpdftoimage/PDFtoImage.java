/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.whiteboardpdftoimage;

import com.amazonaws.HttpMethod;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.event.S3EventNotification;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import com.sun.jersey.api.client.ClientResponse;
import java.awt.image.BufferedImage;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.imageio.ImageIO;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.json.JSONObject;

/**
 *
 * @author jeet
 */
public class PDFtoImage {

    private String env;
    private String pdfFileId;
    private Logger logger;
    private ConfigUtils configUtils;
    private AmazonS3 s3Client;
    private String platformUrl;
    private String platformClient;
    private String platformClientId;
    private String platformClientSecret;
    private String baseFileName;
    private FileACL acl;
    private String folder;
    private String userId;
    
    private enum FileACL {

        PUBLIC_READ {
            @Override
            public String getAclValue() {
                return "public-read";
            }
        },
        PRIVATE {
            @Override
            public String getAclValue() {
                return "private";
            }
        };

        public abstract String getAclValue();
    }

    public PDFtoImage() {

    }

    public void myHandler(S3Event s3event, Context context) {
        try {
            S3EventNotification.S3EventNotificationRecord record = s3event.getRecords().get(0);
            String srcBucket = record.getS3().getBucket().getName();
            String srcKey = record.getS3().getObject().getKey();
//            System.out.println("srcKey: " + srcKey);
            srcKey = URLDecoder.decode(srcKey, "UTF-8");
//            String srcKey = "whiteboardPdf/LOCAL/5948fd928d6ac56214a28a63.pdf";
//            String srcBucket = "vedantu-fos-qa-session-data-new";
            env = getEnvFromKey(srcKey);
            System.out.println("init env: " + env);
            configUtils = new ConfigUtils(env);
//            env = configUtils.getStringValue("environment");
            platformClient = configUtils.getStringValue("platformClient");
            platformClientId = configUtils.getStringValue("platformClientId");
            platformClientSecret = configUtils.getStringValue("platformClientSecret");
            s3Client = new AmazonS3Client();
            platformUrl = configUtils.getStringValue("platformUrl") + "/platform";
            String log4jConfigFile = "ENV-" + env + File.separator + "log.xml";
            System.setProperty("log4j.configurationFile", log4jConfigFile);
            logger = LogManager.getLogger(PDFtoImage.class);
            logger.info("env " + env);

            pdfFileId = getContentFileIdFromKey(srcKey);
            
            JSONObject info = new JSONObject(getPdfInfo());
            String errorCode = info.optString("errorCode", "");
            if (!("".equals(errorCode))) {
                logger.error("Error: Response contains error with code " + errorCode + " and message " + info.optString("errorMessage", "") + " pdfId " + pdfFileId);
            } else {
                JSONObject currentResult = info;
                baseFileName = currentResult.optString("name", "default");
                acl = currentResult.optEnum(FileACL.class, "acl", FileACL.PRIVATE);
                folder = currentResult.optString("folder", null);
                userId = currentResult.optString("userId", "00000000");

                float scale = BigDecimal.valueOf(currentResult.optDouble("scale")).floatValue();
                if (scale == 0.0f) {
                    scale = 3.0f;
                }
                S3Object object = s3Client.getObject(new GetObjectRequest(srcBucket, srcKey));
                InputStream objectData = object.getObjectContent();
                logger.info(objectData.toString());
                PDDocument document = PDDocument.load(objectData);
                PDFRenderer pdfRenderer = new PDFRenderer(document);
                changePdfState(UploadState.CONVERTING, 0,document.getNumberOfPages(),null);
                List<PdfToImageRes> images=new ArrayList<>();
                for (int pageIndex = 0; pageIndex < document.getNumberOfPages(); pageIndex++) {
                    String destinationFileName = baseFileName + pageIndex + ".png";
                    java.io.File outputfile = java.io.File.createTempFile(destinationFileName, ".png");
                    BufferedImage bim = pdfRenderer.renderImage(pageIndex, scale);
                    ImageIO.write(bim, "png", outputfile);
                    JSONObject initRes = new JSONObject(initFileRequest(pageIndex, outputfile.length()));
                    long pageImageId = initRes.getLong("id");
                    if (pageImageId == 0) {
                        throw new Exception("no pageImageId  for pageIndex:" + pageIndex + " pdfFileId:" + pdfFileId);
                    }
                    changeFileState(pageImageId, UploadState.UPLOADING);
                    logger.info("handler: " + outputfile + ", " + initRes.getJSONObject("uploadSignature"));
                    uploadFile(outputfile, initRes.getJSONObject("uploadSignature"));
                    changeFileState(pageImageId, UploadState.COMPLETED);
                    images.add(new PdfToImageRes(null, pageImageId,bim.getHeight(),bim.getWidth()));
                    changePdfState(UploadState.CONVERTING, pageIndex,document.getNumberOfPages(),images);
                    outputfile.delete();
                }
                changePdfState(UploadState.CONVERTED,document.getNumberOfPages(),document.getNumberOfPages(), images);
                document.close();
            }

        } catch (Exception e) {
            logger.error("Error: Caught exception for ", e);
        }

    }

    private String getContentFileIdFromKey(String key) {
        String[] fileNameSplit = key.split("/");
        String filenameWithExtension = fileNameSplit[fileNameSplit.length - 1];
        String[] fileNameWithoutExtensionSplit = filenameWithExtension.split("\\.");
        String fileName = fileNameWithoutExtensionSplit[0];

        return fileName;
    }

    private Map< String, String> getPlatformHeaders() {
        Map< String, String> headers = new HashMap<>();
        headers.put("X-Ved-Client", platformClient);
        headers.put("X-Ved-Client-Id", platformClientId);
        headers.put("X-Ved-Client-Secret", platformClientSecret);
        return headers;
    }

    private String getEnvFromKey(String key) {
        String[] fileNameSplit = key.split("/");
        return fileNameSplit[1];
    }

    private String getPdfInfo() throws Exception {
        String getPdfInfoUrl = platformUrl + "/filemgmt/getWhiteboardPdfInfo?id=" + pdfFileId;

        ClientResponse fosResp = WebUtils.INSTANCE.doCall(getPdfInfoUrl, HttpMethod.GET, null, getPlatformHeaders());

        String res = fosResp.getEntity(String.class);
        logger.info(res);
        JSONObject result = new JSONObject(res);
        String errorCode = result.optString("errorCode", "");
        if (!("".equals(errorCode))) {
            throw new Exception("getPdfInfo failed for pdfFileId:" + pdfFileId + "  errorCode:" + errorCode + "   errorMsg:" + result.optString("errorMessage"));
        }

        return res;
    }

    private String changePdfState(UploadState state, int converted, int total,List<PdfToImageRes> fileIds) throws Exception {
        String getPdfInfoUrl = platformUrl + "/filemgmt/changeWhiteboardPdfState";
        UpdateWhiteboardPdfUploadStatusReq req = new UpdateWhiteboardPdfUploadStatusReq(pdfFileId, state, total, converted, fileIds);
        ClientResponse fosResp = WebUtils.INSTANCE.doCall(getPdfInfoUrl, HttpMethod.POST, new Gson().toJson(req), getPlatformHeaders());

        String res = fosResp.getEntity(String.class);
        logger.info(res);

        JSONObject result = new JSONObject(res);
        String errorCode = result.optString("errorCode", "");
        if (!("".equals(errorCode))) {
            throw new Exception("changePdfState failed for pdfFileId:" + pdfFileId + "  errorCode:" + errorCode + "   errorMsg:" + result.optString("errorMessage"));
        }
        return res;

    }

    private String initFileRequest(int pageIndex, Long size) throws Exception {
        String getPdfInfoUrl = platformUrl + "/filemgmt/initFromApp";

        JSONObject req = new JSONObject();
        req.put("name", baseFileName + pageIndex + ".png");
        req.put("type", "image/png");
        req.put("size", size);
        req.put("uploadType", "MULTIPART");
        req.put("userId", userId);
        req.put("acl", acl);
        req.put("folder", folder);
        ClientResponse fosResp = WebUtils.INSTANCE.doCall(getPdfInfoUrl, HttpMethod.POST, req.toString(), getPlatformHeaders());

        String res = fosResp.getEntity(String.class);
        logger.info(res);

        if (fosResp.getStatus() != HttpStatus.SC_OK) {
            throw new Exception("initFileRequest failed for pageIndex:" + pageIndex + " pdfFileId:" + pdfFileId);
        }
        return res;

    }

    private String changeFileState(Long id, UploadState state) throws Exception {
        String getPdfInfoUrl = platformUrl + "/filemgmt/changeStateFromApp";

        JSONObject req = new JSONObject();
        req.put("state", state);
        req.put("id", id);
        ClientResponse fosResp = WebUtils.INSTANCE.doCall(getPdfInfoUrl, HttpMethod.POST, req.toString(), getPlatformHeaders());

        String res = fosResp.getEntity(String.class);
        logger.info(res);

        if (fosResp.getStatus() != HttpStatus.SC_OK) {
            throw new Exception("changeFileState failed for id:" + id + " pdfFileId:" + pdfFileId);
        }
        return res;

    }

    private void uploadFile(File uploadFile, JSONObject obj) throws IOException, Exception {
        // Info: uploading image to S3
        logger.info("uploadFile args: " + uploadFile + ", " + obj);
        String bucketName = configUtils.getStringValue("bucketName");
        String location = "whiteboardPdf/" + env + "/"  + obj.getString("key");
        logger.info("uploadFile - " + bucketName + ", " + location);
        PutObjectResult putObjectResult = s3Client.putObject(bucketName, location, uploadFile);
        logger.info("response for file upload . :" + putObjectResult.getETag());

//        String actionUrl = obj.getString("actionUrl");
//        String contentType = obj.getString("contentType");
//        int success_action_status = obj.getInt("success_action_status");
//        obj.remove("success_action_status");
//        obj.remove("contentType");
//        obj.remove("GoogleAccessId");
//        obj.remove("actionUrl");
//        HttpPost post = new HttpPost(actionUrl);
//        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
//        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
//        Iterator<String> keys = obj.keys();
//        while (keys.hasNext()) {
//            String key = keys.next();
//            builder.addTextBody(key, obj.getString(key));
//        }
//        builder.addTextBody("success_action_status", "" + success_action_status);
//        builder.addTextBody("content-type", contentType);
//        builder.addBinaryBody("file", uploadFile);
//        HttpEntity entity = builder.build();
//        post.setEntity(entity);
//        CloseableHttpClient client = HttpClients.createDefault();
//        HttpResponse response = client.execute(post);
//        int statusCode = response.getStatusLine().getStatusCode();
//        if (statusCode != success_action_status) {
//            String responseBody = EntityUtils.toString(response.getEntity());
//            throw new Exception("response for file upload for pdfId:" + pdfFileId + "  statusCode:" + statusCode + "   response:" + responseBody);
//        }
//        logger.info("response for file upload . :" + statusCode);

    }
}
