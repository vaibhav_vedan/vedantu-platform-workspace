/* global process, require, exports, module */

var logger = require('tracer').colorConsole({
    format: "{{timestamp}} pid:" + process.pid + " <{{title}}> {{message}} (in {{file}}:{{line}})",
    level: "log"
});
var Q = require("q");
var restler = require('restler');
var Client = require('node-rest-client').Client;
var client = new Client();
var statusCodes = {
    OK: 200
};
function get(url, queryParamsJson, headersJson) {//TODO later add params to the url after encoding
    logger.log("sending get request to URL " + url);
    headersJson = headersJson || {};
    queryParamsJson = queryParamsJson || {};
    var deferred = Q.defer();
    restler.get(url, {
        query: queryParamsJson,
        headers: headersJson
    }).on('complete', function (result, response) {
        if (!response || response.statusCode !== statusCodes.OK || result instanceof Error) {
            if (result instanceof Error) {
                logger.warn('Error: for url ' + url, result.message);
            } else {
                logger.warn("status code of response ", response.statusCode);
            }
            deferred.resolve(null);
        } else {
            if (typeof result === "string") {
                try {
                    result = JSON.parse(result);
                } catch (e) {
                    //swallow
                }
            }
            //logger.debug("result for url " + url, result);
            deferred.resolve(result);
        }
    });
    return deferred.promise;
}

function post(url, params, postURLEncoded, headersJson) {
    logger.info("posting " + url);
    var deferred = Q.defer();
    params = params || {};
    var fn = restler.postJson;
    if (postURLEncoded) {
        params = {data: params};
        fn = restler.post;
    }
    headersJson = headersJson || {};
//    logger.log("sending post request to URL " + url, params);
    logger.info("headers ", headersJson);
    fn(url, params, {headers: headersJson})
        .on('complete', function (result, response) {
            if (!response || response.statusCode !== statusCodes.OK || result instanceof Error) {
                if (result instanceof Error) {
                    logger.warn('Error: for url ' + url, result.message);
                } else {
                    logger.error(result);
                    logger.warn("status code of response for url " + url, response.statusCode);
                }
                deferred.resolve(null);
            } else {
//                    if (typeof result === "string") {
//                        try {
//                            result = JSON.parse(result);
//                        } catch (e) {
//                            //swallow
//                        }
//                    }
//                    logger.debug("result for url " + url, result);
                deferred.resolve(result);
            }
        });
    return deferred.promise;
}
function newpost(url, params, headersJson, ignoreError) {
    var deferred = Q.defer();
    params = params || {};
    headersJson = headersJson || {};
    logger.info(params);
    client.post(url, {
        data: params,
        headers: headersJson
    }, function (data, response) {
        // parsed response body as js object
        // raw response
        if ((!data || response.statusCode !== statusCodes.OK) && !ignoreError) {
            logger.error(data);
            logger.error(data.toString());
            deferred.resolve(null);
        } else {
            deferred.resolve({result: data, headers: response.headers, statusCode: response.statusCode});
        }
    });
    return deferred.promise;
}
function newget(url, queryParamsJson, headersJson) {//TODO later add params to the url after encoding
    var deferred = Q.defer();
//    logger.log("sending get request to URL " + url);
    headersJson = headersJson || {};
    queryParamsJson = queryParamsJson || {};
    logger.info("getting from url " + url);
//    logger.info("params ", queryParamsJson);
//    logger.info("headersJson ", headersJson);
    client.get(url, {
        data: queryParamsJson,
        headers: headersJson
    }, function (data, response) {
        logger.info("GET  request of response for url " + url, response.statusCode);
        if ((!data || response.statusCode !== statusCodes.OK)) {
            logger.error(data.toString());
            console.log("================error===========");
            console.log(data);
            deferred.resolve(null);
        } else {
            var responseHeaders = response.headers;
            var contentType = responseHeaders["content-type"];
            if (!contentType) {
                contentType = responseHeaders["Content-Type"];
            }
            var finalResult = null;
            if (contentType.indexOf("application/json") > -1) {
                finalResult = data;
            } else {
                if (data) {
                    try {
                        finalResult = JSON.parse(data.toString());
                    } catch (e) {
                        //swallow
                        finalResult = data.toString();
                    }
                }
            }
            deferred.resolve({result: finalResult, headers: response.headers, statusCode: response.statusCode});
        }
    });
    return deferred.promise;
}

// Functions which will be available to external callers
module.exports.get = get;
module.exports.post = post;
module.exports.newpost = newpost;
module.exports.newget = newget;
