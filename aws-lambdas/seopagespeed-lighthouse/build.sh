echo "started building for seoPageSpeed"

file="seopagespeed.zip"
if [ -f $file ] ; then
	echo "seopagespeed.zip removed"
    rm $file
fi

echo "installing dependencies"

npm install node-fetch --save
npm install tracer --save
npm install q --save
npm install restler --save
npm install node-rest-client --save

zip -r9 seopagespeed.zip .