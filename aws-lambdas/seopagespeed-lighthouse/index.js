var logger = require('tracer').colorConsole({
    format: "{{timestamp}} pid:" + process.pid + " <{{title}}> {{message}} (in {{file}}:{{line}})",
    level: "log"
});

var Q = require('q');
const fetch = require("node-fetch");
var restler = require("./restlernew");


const secrets = {
    "lighthouse-insights-api": "https://www.googleapis.com/pagespeedonline/v5/runPagespeed",
    "api-key-kasi": "AIzaSyALNB8RHKL83whh8q3vnDeAR7P5wl3JMIM",
    "api-key-phani":"AIzaSyDTbxYxWP5Ob8IzwIAgzWnJ8DvG9W8vKAA",
    "platform-mongo-insert-api": "https://platform.vedantu.com/platform/pageSpeed/createEditPageSpeedData",
    "headers": {
        "Content-Type": "application/json",
        "Accept": "application/json",
        "X-Ved-Client": "VEDANTU_BACKEND",
        "X-Ved-Client-Id": "1F9B7",
        "X-Ved-Client-Secret": "6r605RTxER60IYkA89k2340mxxA2FqR6"
    },
    "category": ["ACCESSIBILITY", "PERFORMANCE", "BEST-PRACTICES", "SEO", "PWA"],
    "locale": "en-US",
    "strategy": "MOBILE"
};


exports.handler = function (event, context) {
    try {
        logger.info("received call from sqs...");
        logger.info(JSON.stringify(event));
        var incomingMsg = handleMessage(event);
        incomingMsg.then(function (result) {
            logger.info("Handled the sqs event successfully",result);
            context.succeed(result);
        }, function (err) {
            logger.error("Error in handling sqs event : ", err);
            // context.fail(err);
            context.succeed(err);
        }).catch(function (err) {
            logger.error("Error in callbacks of handleMessage function promise : ", err);
            // context.fail(err);
            context.succeed(err);
        });
    } catch (e) {
        logger.error(e);
        // context.fail(e);
        context.succeed(e);
    }
};


function postapicall(url, payload, headers) {
    var deferred = Q.defer();
    restler.newpost(url, payload, headers).then(function (response) {
        if (response) {
            deferred.resolve(response);
        } else {
            deferred.reject("Vedantu post api call error ");
        }
    }).catch(function (err) {
        logger.error(err);
        deferred.reject(err);
    });

    return deferred.promise;
}

function generatingPayload(lighthouseresult,sqsBody) {
    var deferred = Q.defer();
    try {
        var payload = {};
        payload.categoryId = sqsBody.categoryId;
        payload.categoryName = sqsBody.categoryName;
        payload.categoryPageId = sqsBody.categoryPageId;
        payload.domain = sqsBody.domain;
        payload.url = lighthouseresult.requestedUrl;
        payload.performance = lighthouseresult.categories.performance.score.toFixed(2);
        payload.accessibility = lighthouseresult.categories.accessibility.score.toFixed(2);
        payload.bestPractices = lighthouseresult.categories["best-practices"].score.toFixed(2);
        payload.seo = lighthouseresult.categories.seo.score.toFixed(2);
        payload.pwa = lighthouseresult.categories.pwa.score.toFixed(2);
        var opportunities = [];
        for (key in lighthouseresult.audits) {
            if ("details" in lighthouseresult.audits[key]) {
                if ("type" in lighthouseresult.audits[key]["details"] && lighthouseresult.audits[key]["details"]["type"] == "opportunity") {
                    let oppObject = {};
                    if (lighthouseresult.audits[key]["score"] < 1) {
                        oppObject.title = lighthouseresult.audits[key]["title"];
                        oppObject.description = lighthouseresult.audits[key]["description"];
                        oppObject.score = lighthouseresult.audits[key]["score"].toFixed(2);
                        oppObject.estimatedSavingsInSeconds = (lighthouseresult.audits[key]["details"]["overallSavingsMs"] / 1000).toFixed(2);
                        opportunities.push(oppObject);
                    }
                }
            }
        }
        payload.opportunityList = opportunities;
        deferred.resolve(payload);
    } catch (e) {
        logger.error(e);
        deferred.reject(e);
    }
    return deferred.promise;
}

function getLightHouseResult(url) {
    var deferred = Q.defer();
    fetch(url).then(function (res) {
        // logger.info(res)
        res.json().then(function (resp) {
            if (!resp.error) {
                logger.info("light house result got");
                deferred.resolve(resp);
            } else {
                logger.info("light house result error");
                deferred.reject(resp.error)
            }
        }).catch(function (ee) {
            logger.error("light house response converting to json error : ", ee);
            deferred.reject(ee);
        })
    }).catch(function (e) {
        logger.error("light house api calling error : ", e);
        deferred.reject(e);
    });
    return deferred.promise;
}

function getLightHouseUrl(url) {
    var deferred = Q.defer();
    try {
        var api_key = "";
        if((new Date().getTime() % 2) ==0 ){
            api_key = secrets['api-key-phani'];
            logger.info("using api-key-phani lightHouse key");
        }else {
            api_key = secrets['api-key-kasi'];
            logger.info("using api-key-kasi lightHouse key");
        }
        var _url = secrets['lighthouse-insights-api'] + "?url=" + url + "&key=" + api_key + "&locale=" + secrets['locale'] + "&strategy=" + secrets['strategy'] + "&category=" + secrets['category'].join("&category=");
        deferred.resolve(_url);
    } catch (e) {
        logger.error(e);
        deferred.reject(e);
    }
    return deferred.promise;
}

function handleMessage(event) {
    var deferred = Q.defer();
    if (event && event.Records) {
        if (event.Records.length > 0) {
            var record = event.Records[0];
            if (record.body) {
                try {
                    var body = JSON.parse(record.body);
                    logger.info("sqs Body : ", JSON.stringify(body));
                    if (body.url) {
                        getLightHouseUrl(body.url).then(function (response) {
                            logger.info("lightHouse Url : ", response);
                            getLightHouseResult(response).then(function (resp) {
                                logger.info(resp);
                                if (resp.lighthouseResult) {
                                    logger.info("lighthouseResult : ",resp.lighthouseResult);
                                    generatingPayload(resp.lighthouseResult,body).then(function (res) {
                                        logger.info("generated payload : ",res);
                                        postapicall(secrets['platform-mongo-insert-api'],res,secrets['headers']).then(function (result) {
                                            if(result.result){
                                                deferred.resolve(true);
                                            }else {
                                                logger.error("vedantu post api call failed : ",result);
                                                deferred.reject(result)
                                            }
                                        }).catch(function (eee) {
                                            logger.error(eee);
                                            deferred.reject(eee);
                                        })
                                    }).catch(function (ee) {
                                        logger.error(ee);
                                        deferred.reject(ee);
                                    })
                                } else {
                                    logger.error("lighthouseResult not found");
                                    deferred.reject("lighthouseResult not found : ", resp.error);
                                }
                            }).catch(function (error) {
                                logger.error("fetching the result from light house producing error : ", error);
                                deferred.reject(error)
                            })
                        }).catch(function (err) {
                            logger.error("url construction error : ", err);
                            deferred.reject(err);
                        })
                    } else {
                        deferred.reject("Url Not found in sqs body");
                    }
                } catch (e) {
                    logger.error(e);
                    deferred.reject(e);
                }
            } else {
                deferred.reject("Was expecting an sqs but body is not there");
            }
        } else {
            deferred.reject("received zero records in sqs");
        }
    } else {
        deferred.reject("received sqs but no records found in them");
    }
    return deferred.promise;
}


var dummyContext = {
    succeed: function () {
        logger.info("successfully done");
    },
    fail: function (err) {
        logger.error("failed ", err);
    }
};

function manualTrigger() {
    var payload = {
        categoryId: "123456789",
        categoryName: "Vedantu Home Page",
        categoryPageId: "987654321",
        url: "https://www.vedantu.com",
        domain: "VEDANTU"
    }
    //smaller version of actual sqs event received
    var _event = {
        "Records": [
            {
                "messageId": "11d6ee51-4cc7-4302-9e22-7cd8afdaadf5",
                "receiptHandle": "AQEBBX8nesZEXmkhsmZeyIE8iQAMig7qw...",
                "body": JSON.stringify(payload),
                "attributes": {
                    "ApproximateReceiveCount": "1",
                    "SentTimestamp": "1573251510774",
                    "SequenceNumber": "18849496460467696128",
                    "MessageGroupId": "1",
                    "SenderId": "AIDAIO23YVJENQZJOL4VO",
                    "MessageDeduplicationId": "1",
                    "ApproximateFirstReceiveTimestamp": "1573251510774"
                },
                "messageAttributes": {},
                "md5OfBody": "e4e68fb7bd0e697a0ae8f1bb342846b3",
                "eventSource": "aws:sqs",
                "eventSourceARN": "arn:aws:sqs:us-east-2:123456789012:fifo.fifo",
                "awsRegion": "us-east-2"
            }
        ]
    };
    exports.handler(_event, dummyContext);
}


// manualTrigger();