/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.listeners;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.managers.CMDSTestAttemptManager;
import com.vedantu.cmds.managers.CMDSTestManager;
import com.vedantu.cmds.managers.CurriculumManager;
import com.vedantu.cmds.managers.TeacherContentDashboardInfoManager;
import com.vedantu.cmds.managers.challenges.ClansManager;
import com.vedantu.cmds.managers.challenges.PostChallengeProcessor;
import com.vedantu.cmds.pojo.TeacherContentDashboardIncrementInfo;
import com.vedantu.cmds.request.challenges.PictureChallengePointsIncrementReq;
import com.vedantu.cmds.request.clans.ProcessClanChallengeAttemptsReq;
import com.vedantu.dashboard.managers.LOAMAmbassadorLMSManager;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.lms.cmds.pojo.ContentInfo;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.lms.cmds.request.ShareContentReq;
import com.vedantu.lms.cmds.request.ShareCurriculumContentReq;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.pojo.PostChangeAnswerSQSPojo;
import com.vedantu.moodle.pojo.PostChangeQuestionSQSPojo;
import com.vedantu.moodle.pojo.UpdateTestAttemptPojoForTestReevaluation;
import com.vedantu.moodle.pojo.UpdateTestAttemptSQSPojoForChangeAnswer;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.vgroups.requests.MessageRequest;
import com.vedantu.vgroups.requests.MoengagePushRequest;
import com.vedantu.vgroups.requests.UpdateChannelRequest;
import com.vedantu.vgroups.service.StreamService;
import com.vedantu.vgroups.service.VGroupChatService;
import com.vedantu.vgroups.service.VGroupService;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author ajith
 */

@Component
public class SQSListener implements MessageListener {

    @Autowired
    private PostChallengeProcessor postChallengeProcessor;

    @Autowired
    private CMDSTestAttemptManager cMDSTestAttemptManager;

    @Autowired
    private ClansManager clansManager;

    @Autowired
    private CMDSTestManager cMDSTestManager;

    @Autowired
    StatsdClient statsdClient;

    @Autowired
    CurriculumManager curriculumManager;


    @Autowired
    private LOAMAmbassadorLMSManager lOAMAmbassadorLMSManager;

    @Autowired
    private VGroupService vGroupService;

    @Autowired
    private VGroupChatService vGroupChatService;

    @Autowired
    private StreamService streamService;

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    private TeacherContentDashboardInfoManager teacherContentDashboardInfoManager;

    private static final Gson gson = new Gson();
    private Logger logger = LogFactory.getLogger(SQSListener.class);
    private String env = ConfigUtils.INSTANCE.getStringValue("environment");


    @Override
    public void onMessage(Message message) {
        logger.info("Message received is {}",message);

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("TestMessage received is {}",textMessage);
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message "+ textMessage.getText());
            long startTime = System.currentTimeMillis();
            String queueName = ((Queue)textMessage.getJMSDestination()).getQueueName();
            logger.info("Message listened for the queue {}",queueName);

            SQSMessageType sqsMessageType = null;
            if (StringUtils.isNotEmpty(textMessage.getStringProperty("messageType"))) {
                sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            }
            if(Objects.nonNull(sqsMessageType)) {
                logger.info("Handling for SQS");
                handleMessage(queueName, sqsMessageType, textMessage.getText());
            }else{
                logger.info("Handling for SNS");
                JSONObject jsonObject = new JSONObject(textMessage.getText());
                logger.info("test message jsonObject: {}",jsonObject);
                String subject = null;
                if (jsonObject.has("Subject")) {
                    subject = jsonObject.getString("Subject");
                }
                logger.info("subject: {}", subject);
                if(jsonObject.has("Message") && StringUtils.isNotEmpty(subject)){
                    String messageData = jsonObject.getString("Message");
                    logger.info("messageData: {}", messageData);
                    SNSSubject snsSubject=SNSSubject.valueOf(subject);
                    handleSNSMessage(snsSubject, messageData);
                }else{
                    logger.error("SNS needs to contain subject and message");
                }
            }
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                if(null != sqsMessageType) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }

        } catch (Exception e) {
            logger.error("Error processing message ",e);
        }
    }

    private void handleSNSMessage(SNSSubject subject, String messageData) {
        logger.info("Handling message for subject {} and messageData {}",subject,messageData);
        switch(subject){
            case UPDATE_TEACHER_CONTENT_DASHBOARD_INFO:
                TeacherContentDashboardIncrementInfo info=gson.fromJson(messageData,TeacherContentDashboardIncrementInfo.class);
                teacherContentDashboardInfoManager.updateTeacherContentDashboardInfo(info);
                break;
            default:
                logger.error("Please specify a proper subject for SNS or add the logic for subject {}",subject);
                break;
        }
    }


    public void handleMessage(String queueName, SQSMessageType sqsMessageType, String text) throws Exception{

        if(SQSQueue.CHALLENGES_QUEUE.getQueueName(env).equals(queueName)){
            handleChallengesMessage(sqsMessageType, text);
        }else if(SQSQueue.POST_CHANGE_ANSWER_QUEUE.getQueueName(env).equals(queueName)){
            handlePostChangeAnswerMessage(sqsMessageType, text);
        }else if(SQSQueue.POST_CHANGE_QUESTION_QUEUE.getQueueName(env).equals(queueName)){
            handlePostChangeQuestionMessage(sqsMessageType, text);
        }
        else if(SQSQueue.POST_TEST_ATTEMPT_OPS.getQueueName(env).equals(queueName)){
            //commenting it out as loam is not used anymore
            //handlePostTestAttemptOps(sqsMessageType, text);
        }
        else if(SQSQueue.POST_TEST_END_OPS.getQueueName(env).equals(queueName)){
            handlePostTestEndOps(sqsMessageType, text);
        } else if (SQSQueue.SHARE_TEST_QUEUE.getQueueName(env).equals(queueName)) {
            handleQueue1(sqsMessageType, text);
        } else if (SQSQueue.SHARE_ASSIGNMENT_QUEUE.getQueueName(env).equals(queueName)) {
            handleQueue2(sqsMessageType, text);
        } else if (SQSQueue.VGROUP_QUEUE.getQueueName(env).equals(queueName)){
            handleVGroupActivity(sqsMessageType, text);
        }  else if(SQSQueue.TEST_END_OPS.getQueueName(env).equals(queueName)){
            handleTestEndOpsActivities(sqsMessageType,text);
        } else if(SQSQueue.MIGRATION_NON_FIFO_OPS.getQueueName(env).equals(queueName)){
            handleMigration(sqsMessageType,text);
        } else if(SQSQueue.BUNDLE_CONTENT_SHARE_OPS.getQueueName(env).equals(queueName)){
            handleBundleContentShareOps(sqsMessageType,text);
        }
        else {
            throw new RuntimeException("Unknown Queue " + queueName);
        }

    }

    private void handleMigration(SQSMessageType sqsMessageType, String text) throws BadRequestException {
        switch (sqsMessageType) {
            case CONTENT_INFO_ASSIGNMENT_MIGRATION: {
                ShareContentReq req = gson.fromJson(text, ShareContentReq.class);
                contentInfoManager.shareContentByTeacherForMigration(req);
                break;
            }
            default: {
                throw new RuntimeException("Unknown Message type in this queue " + sqsMessageType);
            }

        }
    }

    private void handleTestEndOpsActivities(SQSMessageType sqsMessageType, String text) {
        switch (sqsMessageType) {
            case END_TEST_CRON_OPS: {
                String attemptId = gson.fromJson(text, String.class);
                cMDSTestAttemptManager.endAttemptWithRelatedOperation(attemptId);
                break;
            }
            default: {
                throw new RuntimeException("Unknown Message type in this queue " + sqsMessageType);
            }

        }
    }


    private void handleQueue1(SQSMessageType sqsMessageType, String text) throws MalformedURLException, VException {
        switch (sqsMessageType) {
            case SHARE_TEST_TASK:
                // Share test
                ShareCurriculumContentReq req = gson.fromJson(text, ShareCurriculumContentReq.class);
                curriculumManager.shareContentForCurriculum(req,req.isNewContent());
                break;
            default:
                throw new RuntimeException("Unknown Message type in this queue " + sqsMessageType);
        }
    }

    private void handleQueue2(SQSMessageType sqsMessageType, String text) throws VException, MalformedURLException {
        switch (sqsMessageType) {
            case SHARE_ASSIGNMENT_TASK: {
//                Map<String, Object> map = gson.fromJson(text, new TypeToken<Map<String, Object>>() {}.getType());

//                BatchBasicInfo batchBasicInfo =  gson.fromJson(gson.toJson(map.get("batchBasicInfo")), BatchBasicInfo.class);
//                List<ContentInfo> contents = gson.fromJson(gson.toJson(map.get("contents")), new TypeToken<List<ContentInfo>>(){}.getType());
//                List<String> userIds = gson.fromJson(gson.toJson(map.get("userIds")), new TypeToken<List<String>>(){}.getType());
//                curriculumManager.shareAssignmentFor(batchBasicInfo, userIds, contents);
                ShareCurriculumContentReq req = gson.fromJson(text, ShareCurriculumContentReq.class);
                curriculumManager.shareContentForCurriculum(req,req.isNewContent());
                break;
            }
            default:
                throw new RuntimeException("Unknown Message type in this queue " + sqsMessageType);
        }
    }

    private void handlePostChangeAnswerMessage(SQSMessageType sqsMessageType, String text) throws BadRequestException {
        switch(sqsMessageType){

            case UPDATE_CHANGE_ANSWER_RESULT_ENTRIES_AND_REEVALUATE_TEST_IF_NECESSARY:
                PostChangeAnswerSQSPojo postChangeAnswerSQSPojoForChangeAnswer = gson.fromJson(text, PostChangeAnswerSQSPojo.class);
                cMDSTestAttemptManager.changeAnswerInResultEntries(postChangeAnswerSQSPojoForChangeAnswer);
                if(postChangeAnswerSQSPojoForChangeAnswer.isTestIdForReevaluation()){
                    cMDSTestAttemptManager.reevaluateTest(postChangeAnswerSQSPojoForChangeAnswer);
                }
                break;
            case UPDATE_TEST_ATTEMPT_AFTER_CHANGE_ANSWER:
                UpdateTestAttemptSQSPojoForChangeAnswer updateTestAttemptSQSPojoForChangeAnswer = gson.fromJson(text, UpdateTestAttemptSQSPojoForChangeAnswer.class);
                cMDSTestAttemptManager.updateTestAttemptForChangeAnswer(updateTestAttemptSQSPojoForChangeAnswer.getQuestionId(), updateTestAttemptSQSPojoForChangeAnswer.getContentInfoId());
                break;
            case UPDATE_TEST_ATTEMPT_AFTER_REEVALUATION:
                UpdateTestAttemptPojoForTestReevaluation pojo=gson.fromJson(text,UpdateTestAttemptPojoForTestReevaluation.class);
                cMDSTestAttemptManager.udpateTestAttemptDueToReevaluation(pojo.getPostChangeAnswerSQSPojo(), pojo.getTest(),
                        pojo.getQuestionIndex(), pojo.getSection(), pojo.getQuestionId(), pojo.getContentInfo());
                break;
            case UPDATE_RANK_AFTER_REEVALUATION:
                CMDSTest test=gson.fromJson(text,CMDSTest.class);
                contentInfoManager.calculateTestRank(test);
                break;
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "UNKNOWN MESSAGE TYPE FOR SQS - " + sqsMessageType);

        }
    }


    public void handleChallengesMessage(SQSMessageType sqsMessageType, String text) throws Exception {

        switch (sqsMessageType) {
            case PROCESS_CHALLENGE_ATTEMPTS:
            	ProcessClanChallengeAttemptsReq req = gson.fromJson(text, ProcessClanChallengeAttemptsReq.class);
//            	Nov 12. stopping clan point calculations
//            	postChallengeProcessor.addPointsToClanLeaderBoard(req);
                break;
            case CHALLENGE_PROCESSED:
            	Challenge challenge = gson.fromJson(text, Challenge.class);
//            	Nov 12. stopping clan point calculations
//            	postChallengeProcessor.populateClanPointsForChallenge(challenge);
                break;
            case PICTURE_CHALLENGE_PROCESS:
            	PictureChallengePointsIncrementReq pointsReq = gson.fromJson(text, PictureChallengePointsIncrementReq.class);
//            	Nov 12. stopping clan point calculations
//            	clansManager.processPictureChallengePoints(pointsReq);
                break;
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "UNKNOWN MESSAGE TYPE FOR SQS - " + sqsMessageType);

        }
        logger.info("Message handled");
    }

    private void handlePostChangeQuestionMessage(SQSMessageType sqsMessageType, String text) throws BadRequestException {

        switch(sqsMessageType){
            case CHANGE_ANSWER_IN_TEST_RESULT_ENTRIES:
                PostChangeQuestionSQSPojo postChangeQuestionSQSPojoforChangeAnswer = gson.fromJson(text, PostChangeQuestionSQSPojo.class);
                logger.info("CHANGE_ANSWER_IN_TEST_RESULT_ENTRIES - {}",postChangeQuestionSQSPojoforChangeAnswer);
                cMDSTestAttemptManager.changeQuestionInResultEntries(postChangeQuestionSQSPojoforChangeAnswer);
                break;
            case REEVALUATE_TESTS:
                PostChangeQuestionSQSPojo postChangeQuestionSQSPojoForReevaluateTest = gson.fromJson(text, PostChangeQuestionSQSPojo.class);
                logger.info("REEVALUATE_TESTS - {}",postChangeQuestionSQSPojoForReevaluateTest);
                cMDSTestAttemptManager.processTestPostChangeQuestion(postChangeQuestionSQSPojoForReevaluateTest);
                break;
            case REEVALUATE_TESTS_AFTER_CHANGE_MARKS:
                PostChangeQuestionSQSPojo reevaluateTestAfterChangeMarks = gson.fromJson(text, PostChangeQuestionSQSPojo.class);
                logger.info("REEVALUATE_TESTS_AFTER_CHANGE_MARKS - {}",reevaluateTestAfterChangeMarks);
                cMDSTestAttemptManager.processTestPostChangeQuestionMarks(reevaluateTestAfterChangeMarks);
                break;
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "UNKNOWN MESSAGE TYPE FOR SQS - " + sqsMessageType);

        }

    }
//    public void handlePostTestAttemptOps(SQSMessageType sqsMessageType, String text){
//        switch(sqsMessageType){
//            case ADD_ATTEMPT_TOPIC_TREE_DETAILS:
////                SomePojo req = gson.fromJson(text, SomePojo.class);
//            	JsonObject json = gson.fromJson(text, JsonObject.class);
//            	if (json.get("contentInfoId") == null) {
//            		logger.error("invalid message received in sqs listner for ADD_ATTEMPT_TOPIC_TREE_DETAILS " + text);
//            		return;
//            	}
//            	String contentInfoId = json.get("contentInfoId").getAsString();
//            	lOAMAmbassadorLMSManager.updateTestNodeStats(contentInfoId);
//            	break;
//            default:
//            	logger.error("invalid message received in sqs listner for  " + sqsMessageType.name() + ", " + text);
//                break;
//
//        }
//
//    }

    public void handlePostTestEndOps(SQSMessageType sqsMessageType, String text) throws BadRequestException {
        switch (sqsMessageType) {
            case CALCULATE_TEST_ANALYTICS: {
                logger.info("Message Received for CALCULATE_TEST_ANALYTICS with payload " + text);
                cMDSTestManager.calculateTestAnalytics(text);
                break;
            }
            default:
                logger.error("invalid message received in sqs listner for  " + sqsMessageType.name() + ", " + text);
                break;

        }

    }

    public void handleVGroupActivity(SQSMessageType sqsMessageType, String text){
        switch (sqsMessageType) {
            case JOIN_VGROUP: {
                logger.info("Message received to join VGroups");
                UpdateChannelRequest updateChannelRequest = gson.fromJson(text, UpdateChannelRequest.class);
                vGroupService.joinOrLeaveVGroup(updateChannelRequest)
                        .thenApply(response ->
                        {
                            logger.info("Join Group response for userId : {} : {}", updateChannelRequest.getAdd_members().get(0), response);
                            return response;
                        });
                break;
            }
            case BROADCAST_VGROUP_MESSAGE: {
                logger.info("Message received to broadcast VGroup Message");
                MessageRequest messageRequest = gson.fromJson(text, MessageRequest.class);
                vGroupChatService.sendMessage(messageRequest)
                        .thenApply(response ->
                        {
                            logger.info("Sent Message Response : {}", response);
                            return response;
                        });
                break;
            }
            case VGROUP_PUSH_NOTIFICATION:{
                logger.info("Message received to push VGroup Notification.");
                MoengagePushRequest pushRequest = gson.fromJson(text, MoengagePushRequest.class);
                vGroupChatService.pushVGroupNotification(pushRequest)
                        .thenApply(response ->
                        {
                            logger.info("Push noti response : {}", response);
                            return response;
                        });
                break;
            }
            case UPDATE_VGROUP_CHANNEL:{
                logger.info("Message received to update channel data.");
                UpdateChannelRequest updateChannelRequest = gson.fromJson(text, UpdateChannelRequest.class);
                streamService.updateChannel(updateChannelRequest)
                        .thenApply(response ->
                        {
                            logger.info("channel response : {}", response);
                            return response;
                        });
                break;
            }
            default:
                logger.error("invalid message received in sqs listener for  " + sqsMessageType.name() + ", " + text);
                break;
        }
    }

    private void handleBundleContentShareOps(SQSMessageType sqsMessageType, String text) throws BadRequestException {

        switch (sqsMessageType) {
            case AUTO_ENROLL_CONTENT_SHARE: {
                logger.info("deblog-- listener received message - inflight message {}", text);
                curriculumManager.shareContentOnEnrollment(gson.fromJson(text, ShareContentEnrollmentReq.class));
                break;
            }
            default:
                logger.error("invalid message received in sqs listener for  " + sqsMessageType.name() + ", " + text);
                break;
        }
    }
}
