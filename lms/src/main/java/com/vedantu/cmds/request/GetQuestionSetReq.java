package com.vedantu.cmds.request;

import com.vedantu.lms.cmds.enums.VedantuRecordState;

public class GetQuestionSetReq {
	private String questionSetId;
	private VedantuRecordState recordState;
	private Integer start;
	private Integer limit;

	public GetQuestionSetReq() {
		super();
	}

	public GetQuestionSetReq(String questionSetId, VedantuRecordState recordState, Integer start, Integer limit) {
		super();
		this.questionSetId = questionSetId;
		this.recordState = recordState;
		this.start = start;
		this.limit = limit;
	}

	public String getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(String questionSetId) {
		this.questionSetId = questionSetId;
	}

	public VedantuRecordState getRecordState() {
		return recordState;
	}

	public void setRecordState(VedantuRecordState recordState) {
		this.recordState = recordState;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public boolean validLimits() {
		return this.start != null && this.limit != null && this.limit > 0;
	}

	@Override
	public String toString() {
		return "GetQuestionSetReq [questionSetId=" + questionSetId + ", recordState=" + recordState + ", start=" + start
				+ ", limit=" + limit + ", toString()=" + super.toString() + "]";
	}
}
