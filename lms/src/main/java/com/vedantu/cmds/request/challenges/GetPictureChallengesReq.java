package com.vedantu.cmds.request.challenges;

import java.util.List;

import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetPictureChallengesReq extends AbstractFrontEndListReq{

	private String clanId;
	private String category;
	private Long userId;
	private ChallengeStatus status;
    private Boolean attemptedOnly;
    private Difficulty difficulty;
    private String sortOrder;
    private String orderBy;
    private String pictureChallengeId;


	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ChallengeStatus getStatus() {
		return status;
	}

	public void setStatus(ChallengeStatus status) {
		this.status = status;
	}

	public Boolean getAttemptedOnly() {
		return attemptedOnly;
	}

	public void setAttemptedOnly(Boolean attemptedOnly) {
		this.attemptedOnly = attemptedOnly;
	}
	
    public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getPictureChallengeId() {
		return pictureChallengeId;
	}

	public void setPictureChallengeId(String pictureChallengeId) {
		this.pictureChallengeId = pictureChallengeId;
	}


	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (Boolean.TRUE.equals(attemptedOnly) && (StringUtils.isEmpty(clanId))) {
            errors.add("clanId needed for attemptedOnly query");
        }
        return errors;
    }
}
