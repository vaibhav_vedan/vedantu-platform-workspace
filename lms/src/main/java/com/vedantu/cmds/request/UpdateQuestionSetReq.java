package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.Difficulty;

public class UpdateQuestionSetReq {
	private String id;
	public Difficulty difficulty;

	public UpdateQuestionSetReq() {
		super();
	}

	public UpdateQuestionSetReq(String id, Difficulty difficulty) {
		super();
		this.id = id;
		this.difficulty = difficulty;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	@Override
	public String toString() {
		return "UpdateQuestionSetReq [id=" + id + ", difficulty=" + difficulty + "]";
	}
}
