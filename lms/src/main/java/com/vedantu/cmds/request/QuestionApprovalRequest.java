package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.pojo.QuestionApprovalPojo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionApprovalRequest extends AbstractFrontEndReq {

    private String testId;
    private String questionSetId;
    private List<QuestionApprovalPojo> approvalPojo;
    private CMDSApprovalStatus overallApprovalStatus;// MAKER_APPROVED, CHECKER_APPROVED
    private Long checkerId;
    private String rejectionComment;
    private Boolean isQuestionSetIdOfTest;

    public void cleanApprovalPojoData(){
        if(ArrayUtils.isNotEmpty(approvalPojo)) {
            this.approvalPojo.stream().forEach(questionApprovalPojo -> questionApprovalPojo.cleanTopicAndSubTopicAndAnalysisTopicData());
        }
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(StringUtils.isEmpty(testId) && StringUtils.isEmpty(questionSetId)){
            errors.add("Please specify a `questionSetId` or list of `questionIds`");
        }

        if(StringUtils.isNotEmpty(testId) && StringUtils.isNotEmpty(questionSetId)){
            errors.add("Please specify one of `questionSetId` or list of `questionIds`");
        }

        if(ArrayUtils.isEmpty(approvalPojo)){
            errors.add("Request file cann't be empty");
        }

        QuestionApprovalPojo dirtyData = approvalPojo
                .stream()
                .filter(QuestionApprovalPojo::isDirtyData)
                .findFirst()
                .orElse(null);

        if(Objects.nonNull(dirtyData)){
            errors.add("Some of the id(s) or approvalStatus(s) are null valued or " +
                    "approval status is `DUPLICATE` but doesn't contain any reference to its original value");
        }

        if(Objects.isNull(overallApprovalStatus)){
            errors.add("Please specify a proper approval status");
        }

        if(!CMDSApprovalStatus.MAKER_APPROVED.equals(overallApprovalStatus) &&
            !CMDSApprovalStatus.CHECKER_APPROVED.equals(overallApprovalStatus) &&
             !CMDSApprovalStatus.REJECTED.equals(overallApprovalStatus)){
            errors.add("Overall approval status can either be `MAKER_APPROVED` or `CHECKER_APPROVED`");
        }

        if(CMDSApprovalStatus.MAKER_APPROVED.equals(overallApprovalStatus) && Objects.isNull(checkerId)){
            errors.add("Please enter a checker id as status is `MAKER_APPROVED`");
        }

        if(CMDSApprovalStatus.REJECTED.equals(overallApprovalStatus) && StringUtils.isEmpty(rejectionComment)){
            errors.add("Rejection comment needed for approval status `REJECTED`");
        }

        if(Boolean.TRUE.equals(isQuestionSetIdOfTest) && Objects.isNull(questionSetId)){
            errors.add("Please specify the questionSet id as it belongs to some test");
        }

        return errors;
    }
}
