package com.vedantu.cmds.request;


import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChangeQuestionTagsReq extends AbstractFrontEndReq {

    private String questionId;
    private String subject;
    private Set<String> topics;
    private Set<String> subtopics;
    private Set<String> grades;
    private Set<String> targets;
    private Set<String> analysisTags;
    private Integer difficultyValue;
    private QuestionType type;

    public void cleanTopicAndSubTopicAndAnalysisTopicData(){
        if(ArrayUtils.isNotEmpty(topics)){
            this.topics=topics.stream().map(this::replaceDollarWithComma).collect(Collectors.toSet());
        }
        if(ArrayUtils.isNotEmpty(subtopics)){
            this.subtopics=subtopics.stream().map(this::replaceDollarWithComma).collect(Collectors.toSet());
        }
        if(ArrayUtils.isNotEmpty(analysisTags)){
            this.analysisTags=analysisTags.stream().map(this::replaceDollarWithComma).collect(Collectors.toSet());
        }
    }

    private String replaceDollarWithComma(String topic) {
        return topic.replaceAll("\\$", ",");
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();

        if(StringUtils.isEmpty(questionId)) {
            errors.add("Please specify questionId for tags edit");
        }
        if(StringUtils.isEmpty(subject) &&
            ArrayUtils.isEmpty(topics) &&
            ArrayUtils.isEmpty(subtopics) &&
            ArrayUtils.isEmpty(grades) &&
            ArrayUtils.isEmpty(targets) &&
            Objects.isNull(difficultyValue) &&
            Objects.isNull(type) &&
            ArrayUtils.isEmpty(analysisTags)){
            errors.add("everything is null in request");
        }
        return errors;
    }
}
