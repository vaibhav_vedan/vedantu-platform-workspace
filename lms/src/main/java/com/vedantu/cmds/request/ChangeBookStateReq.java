/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.springframework.util.StringUtils;

/**
 *
 * @author ajith
 */
public class ChangeBookStateReq extends AbstractFrontEndReq {

    private String bookId;
    private VedantuRecordState recordState;

    public VedantuRecordState getRecordState() {
        return recordState;
    }

    public void setRecordState(VedantuRecordState recordState) {
        this.recordState = recordState;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (recordState == null) {
            errors.add("recordState");
        }
        if (StringUtils.isEmpty(bookId)) {
            errors.add("bookId");
        }
        return errors;
    }

}
