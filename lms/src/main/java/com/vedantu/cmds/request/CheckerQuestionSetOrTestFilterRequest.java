package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class CheckerQuestionSetOrTestFilterRequest extends AbstractFrontEndReq {
    private CMDSApprovalStatus approvalStatus;
    private Long checkerId;
    private String makerId;
    private long skip=0;
    private int limit=20;
    private String title;
    private boolean actionTimeSort=false;
    private String testId;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(
            !CMDSApprovalStatus.MAKER_APPROVED.equals(approvalStatus) &&
                !CMDSApprovalStatus.CHECKER_APPROVED.equals(approvalStatus) &&
                    !CMDSApprovalStatus.REJECTED.equals(approvalStatus) &&
                        Objects.nonNull(approvalStatus)
        ){
            errors.add("Please mention proper approval status[MAKER_APPROVED,CHECKER_APPROVED,REJECTED,null]");
        }
        return errors;
    }
}
