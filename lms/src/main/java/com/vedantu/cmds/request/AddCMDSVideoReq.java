package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.CMDSVideoSourceType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class AddCMDSVideoReq extends AbstractFrontEndReq {

    private String videoId;
    private String videoUrl;
    private String title;
    private String description;
    private Set<String> targetGrades;
    private Set<String> topics;
    private Set<String> teacherIds;
    private CMDSVideoSourceType videoSourceType;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(videoUrl)) {
            errors.add("videoUrl");
        }
        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }
        if (StringUtils.isEmpty(description)) {
            errors.add("description");
        }
        if (targetGrades == null) {
            errors.add("targetGrades");
        }
        if (topics == null) {
            errors.add("topics");
        }
        if (videoSourceType == null) {
        	errors.add("videoSourceType");
        }
        return errors;
    }

}
