/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author parashar
 */
public class GetCMDSVideoPlaylistReq extends AbstractFrontEndListReq{
    
    private Boolean published;

    /**
     * @return the published
     */
    public Boolean getPublished() {
        return published;
    }

    /**
     * @param published the published to set
     */
    public void setPublished(Boolean published) {
        this.published = published;
    }
    
}
