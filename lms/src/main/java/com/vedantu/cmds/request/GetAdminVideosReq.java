package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.OnboardingVideoType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;

import java.util.Set;

@Data
public class GetAdminVideosReq extends AbstractFrontEndListReq {

    private String videoUrl;
    private Set<String> teacherIds;
    private boolean onboardingVideosRequired=false;
    private OnboardingVideoType onboardingVideoType;

}
