package com.vedantu.cmds.request;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class EditAnswerReq extends AbstractFrontEndReq {

	private String questionId;
	private List<String> answer;

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public List<String> getAnswer() {
		return answer;
	}

	public void setAnswer(List<String> answer) {
		this.answer = answer;
	}

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(answer)) {
            errors.add("answer");
        }
        if (StringUtils.isEmpty(questionId)) {
            errors.add("questionId");
        }
        return errors;
    }
}
