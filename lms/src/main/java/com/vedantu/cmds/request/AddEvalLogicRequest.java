package com.vedantu.cmds.request;

import com.vedantu.cmds.pojo.CustomSectionEvaluationRule;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author parashar
 */
public class AddEvalLogicRequest extends AbstractFrontEndReq{
    
    private List<CustomSectionEvaluationRule> customSectionEvaluationRules;
    private String testId;
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        
        if(StringUtils.isEmpty(testId)){
            errors.add("testId");
        }
        
        if(ArrayUtils.isNotEmpty(customSectionEvaluationRules)){
            Set<String> sectionNames = new HashSet<>();
            for(CustomSectionEvaluationRule customSectionEvaluationRule : customSectionEvaluationRules){
                if(customSectionEvaluationRule.getBestOf() == 0){
                    errors.add("Best of");
                    break;
                }
                if(ArrayUtils.isEmpty(customSectionEvaluationRule.getTestMetadataKey())){
                    errors.add("Test MetaDataKey");
                    break;
                }
                
                for(String metadataKey : customSectionEvaluationRule.getTestMetadataKey()){
                    if(sectionNames.contains(metadataKey)){
                        errors.add("Duplicate Section not allowed");
                    }
                }
                
            }
        }
        
        return errors;
    }    

    /**
     * @return the customSectionEvaluationRules
     */
    public List<CustomSectionEvaluationRule> getCustomSectionEvaluationRules() {
        return customSectionEvaluationRules;
    }

    /**
     * @param customSectionEvaluationRules the customSectionEvaluationRules to set
     */
    public void setCustomSectionEvaluationRules(List<CustomSectionEvaluationRule> customSectionEvaluationRules) {
        this.customSectionEvaluationRules = customSectionEvaluationRules;
    }

    /**
     * @return the testId
     */
    public String getTestId() {
        return testId;
    }

    /**
     * @param testId the testId to set
     */
    public void setTestId(String testId) {
        this.testId = testId;
    }
    
}
