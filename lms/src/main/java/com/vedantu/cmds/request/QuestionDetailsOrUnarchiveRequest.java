package com.vedantu.cmds.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class QuestionDetailsOrUnarchiveRequest extends AbstractFrontEndReq {
    private List<String> questionIds;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (ArrayUtils.isEmpty(questionIds)) {
            errors.add("Please specify one or more question ids");
        }

        return errors;
    }
}
