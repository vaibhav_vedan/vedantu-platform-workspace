package com.vedantu.cmds.request;

import java.util.List;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.ArrayUtils;
import java.util.Set;
import org.apache.commons.lang.StringUtils;

public class AddEditQuestionReq extends AbstractAddCMDSEntityReq {

    private String questionId;
    private RichTextFormat questionBody;
    private QuestionType questionType;
    private List<RichTextFormat> options;
    private List<String> optionOrder;
    private List<RichTextFormat> solutions;
    private List<String> answers;
    private Difficulty difficulty;
    private List<HintFormat> refHints;

    //book related
    private String book;
    private String edition;
    private String chapter;
    private String chapterNo;
    private String exercise;
    private Set<String> pageNos;
    private int slNoInBook;
    private String questionNo;

    public AddEditQuestionReq() {
        super();
    }

    public RichTextFormat getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(RichTextFormat questionBody) {
        this.questionBody = questionBody;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<RichTextFormat> getOptions() {
        return options;
    }

    public void setOptions(List<RichTextFormat> options) {
        this.options = options;
    }

    public List<RichTextFormat> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<RichTextFormat> solutions) {
        this.solutions = solutions;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public List<HintFormat> getRefHints() {
        return refHints;
    }

    public void setRefHints(List<HintFormat> refHints) {
        this.refHints = refHints;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public Set<String> getPageNos() {
        return pageNos;
    }

    public void setPageNos(Set<String> pageNos) {
        this.pageNos = pageNos;
    }

    public int getSlNoInBook() {
        return slNoInBook;
    }

    public void setSlNoInBook(int slNoInBook) {
        this.slNoInBook = slNoInBook;
    }

    public String getQuestionNo() {
        return questionNo;
    }

    public void setQuestionNo(String questionNo) {
        this.questionNo = questionNo;
    }

    public String getChapterNo() {
        return chapterNo;
    }

    public void setChapterNo(String chapterNo) {
        this.chapterNo = chapterNo;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (questionBody == null) {
            errors.add("questionBody");
        } else if (StringUtils.isEmpty(questionBody.getNewText())) {
            errors.add("questionBody newText");
        }

        if (ArrayUtils.isNotEmpty(solutions)) {
            for (RichTextFormat solution : solutions) {
                if (StringUtils.isEmpty(solution.getNewText())) {
                    errors.add("invalid solution");
                }
            }
        }
        if (ArrayUtils.isNotEmpty(options)) {
            for (RichTextFormat option : options) {
                if (StringUtils.isEmpty(option.getNewText())) {
                    errors.add("invalid option");
                }
            }
        }
        if (!(QuestionType.MCQ.equals(questionType)
                || QuestionType.SCQ.equals(questionType)
                || QuestionType.NUMERIC.equals(questionType))) {
            errors.add("only mcq,scq,numeric allowed");
        } else if (ArrayUtils.isEmpty(answers)) {
            errors.add("answers");
        }
        if (ArrayUtils.isNotEmpty(refHints)) {
            for (RichTextFormat hint : refHints) {
                if (StringUtils.isEmpty(hint.getNewText())) {
                    errors.add("invalid hint");
                }
            }
        }
        return errors;
    }

    /**
     * @return the optionsOrder
     */
    public List<String> getOptionOrder() {
        return optionOrder;
    }

    /**
     * @param optionOrder the optionsOrder to set
     */
    public void setOptionOrder(List<String> optionOrder) {
        this.optionOrder = optionOrder;
    }
}
