/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 *
 * @author ajith
 */

@Data
@AllArgsConstructor
public class DeleteQuestionsReq extends AbstractFrontEndReq {

    private List<String> questionIds;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(questionIds)) {
            errors.add("questionIds");
        }
        return errors;
    }

}
