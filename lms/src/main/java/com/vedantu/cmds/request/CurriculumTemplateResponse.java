package com.vedantu.cmds.request;

import com.vedantu.cmds.entities.CurriculumTemplate;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CurriculumTemplateResponse extends CurriculumTemplateTopicTagsAdd{
    private List<CurriculumTemplateResponse> nodes;

    public CurriculumTemplateResponse(CurriculumTemplate other){
        super(other);
    }
}
