package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.ReportStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.Set;

public class GetReportsReq extends AbstractFrontEndListReq {

    private String sortBy;
    private ReportStatus status;
    private Long startTime;
    private Long endTime;
    private Set<String> mainTags;
    private String questionId;

    public String getSortBy() {
        return sortBy;
    }

    public void setSortBy(String sortBy) {
        this.sortBy = sortBy;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    @Override
    public Set<String> getMainTags() {
        return mainTags;
    }

    @Override
    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
