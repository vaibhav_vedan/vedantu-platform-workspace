/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.cmds.pojo.PlaylistVideoPojo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class AddVideoPlaylistReq extends AbstractFrontEndReq {
    
    private String videoPlaylistId;
    private boolean published = false;
    private List<PlaylistVideoPojo> playlist;
    private Set<String> targetGrades;
    private Set<String> topics;
    private Set<String> teacherIds;
    private String title;
    private String description;

    /**
     * @return the published
     */
    public boolean isPublished() {
        return published;
    }

    /**
     * @param published the published to set
     */
    public void setPublished(boolean published) {
        this.published = published;
    }

    /**
     * @return the playlist
     */
    public List<PlaylistVideoPojo> getPlaylist() {
        return playlist;
    }

    /**
     * @param playlist the playlist to set
     */
    public void setPlaylist(List<PlaylistVideoPojo> playlist) {
        this.playlist = playlist;
    }

    /**
     * @return the targetGrades
     */
    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    /**
     * @param targetGrades the targetGrades to set
     */
    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    /**
     * @return the topics
     */
    public Set<String> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    /**
     * @return the teacherIds
     */
    public Set<String> getTeacherIds() {
        return teacherIds;
    }

    /**
     * @param teacherIds the teacherIds to set
     */
    public void setTeacherIds(Set<String> teacherIds) {
        this.teacherIds = teacherIds;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the videoPlaylistId
     */
    public String getVideoPlaylistId() {
        return videoPlaylistId;
    }

    /**
     * @param videoPlaylistId the videoPlaylistId to set
     */
    public void setVideoPlaylistId(String videoPlaylistId) {
        this.videoPlaylistId = videoPlaylistId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }
        if (StringUtils.isEmpty(description)) {
            errors.add("description");
        }

        if (ArrayUtils.isEmpty(playlist)) {
            errors.add("playlist");
        }
//
//        if (ArrayUtils.isEmpty(targetGrades)) {
//            errors.add("targetGrades");
//        }
//        if (ArrayUtils.isEmpty(topics)) {
//            errors.add("topics");
//        }
//
        if(ArrayUtils.isNotEmpty(playlist)){
            for(PlaylistVideoPojo playlistVideoPojo : playlist){
                if(StringUtils.isEmpty(playlistVideoPojo.getEntityId())){
                    errors.add("playlist video id");
                    break;
                }
                
                if(playlistVideoPojo.isLive() && (playlistVideoPojo.getStartTime() == null || playlistVideoPojo.getEndTime() == null)){
                    errors.add("endTime/startTime");
                    break;
                }
                
            }
        }
        
        return errors;
    }

}
