package com.vedantu.cmds.request;

import com.vedantu.cmds.pojo.OnboardingVideoData;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class AddEditOnboardingVideoRequest extends AbstractFrontEndReq {
    private String grade;
    private List<OnboardingVideoData> onboardingVideoData;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(StringUtils.isEmpty(grade)){
            errors.add("[grade] cann't be null");
        }
        if(ArrayUtils.isEmpty(onboardingVideoData)){
            errors.add("[onboardingVideoData] is empty");
        }
        if(ArrayUtils.isNotEmpty(onboardingVideoData)){
            for (OnboardingVideoData onboardingVideoDatum : onboardingVideoData) {
                if(StringUtils.isEmpty(onboardingVideoDatum.getVideoUrl()) || Objects.isNull(onboardingVideoDatum.getOnboardingVideoType())){
                    errors.add("Problem with video data for either videoUrl "+onboardingVideoDatum.getVideoUrl()+" or " +
                            "onboardingVideoType "+onboardingVideoDatum.getOnboardingVideoType());
                    break;
                }
            }
        }
        return errors;
    }
}
