package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AdminRedisSetReq extends AbstractFrontEndReq {

    private String key;
    private String value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
