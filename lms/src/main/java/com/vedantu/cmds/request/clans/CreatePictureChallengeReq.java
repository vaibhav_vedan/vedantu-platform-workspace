/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request.clans;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.challenges.PictureChallengeAnswer;
import com.vedantu.cmds.pojo.challenges.PictureChallengeDayDeductions;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CreatePictureChallengeReq extends AbstractFrontEndReq {

    private String title;
    private Long startTime;
    private Long burstTime;
    private Difficulty difficulty;
    private String category;
    private List<PictureChallengeDayDeductions> dayWisePointsToWin;
    private CloudStorageEntity image;
    private int pointsToRevealFullImage;
    private List<PictureChallengeAnswer> answer;
    private String answerString;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(Long burstTime) {
        this.burstTime = burstTime;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<PictureChallengeDayDeductions> getDayWisePointsToWin() {
        return dayWisePointsToWin;
    }

    public void setDayWisePointsToWin(List<PictureChallengeDayDeductions> dayWisePointsToWin) {
        this.dayWisePointsToWin = dayWisePointsToWin;
    }

    public CloudStorageEntity getImage() {
        return image;
    }

    public void setImage(CloudStorageEntity image) {
        this.image = image;
    }

    public int getPointsToRevealFullImage() {
        return pointsToRevealFullImage;
    }

    public void setPointsToRevealFullImage(int pointsToRevealFullImage) {
        this.pointsToRevealFullImage = pointsToRevealFullImage;
    }

    public List<PictureChallengeAnswer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<PictureChallengeAnswer> answer) {
        this.answer = answer;
    }

    public String getAnswerString() {
		return answerString;
	}

	public void setAnswerString(String answerString) {
		this.answerString = answerString;
	}

	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }
        if (burstTime == null || burstTime < System.currentTimeMillis()) {
            errors.add("burstTime");
        }

        if (StringUtils.isEmpty(category)) {
            errors.add("category");
        }
        if (image == null || image.getFileName() == null) {
            errors.add("image and filename");
        }
        if (pointsToRevealFullImage < 0) {
            errors.add("pointsToRevealFullImage");
        }
        if (ArrayUtils.isEmpty(answer)) {
            errors.add("answer");
        }
        if (ArrayUtils.isNotEmpty(dayWisePointsToWin)) {
            Collections.sort(dayWisePointsToWin, new Comparator<PictureChallengeDayDeductions>() {
                @Override
                public int compare(PictureChallengeDayDeductions o1, PictureChallengeDayDeductions o2) {
                    return o1.getFromTime().compareTo(o2.getFromTime());
                }
            });
            int size = dayWisePointsToWin.size();
            Long currentMillis = System.currentTimeMillis();
            for (int i = 0; i < size; i++) {
                PictureChallengeDayDeductions singleDay = dayWisePointsToWin.get(i);
                if (i < size - 1) {
                    PictureChallengeDayDeductions nextSlot = dayWisePointsToWin.get(i + 1);
                    if (singleDay.getTillTime() > nextSlot.getFromTime()) {
                        errors.add("overlapping slots ");
                        break;
                    }
                }

                if (singleDay.getFromTime() < currentMillis) {
                    errors.add("dayWise in the past " + singleDay);
                    break;
                }
                if (singleDay.getFromTime() > singleDay.getTillTime()) {
                    errors.add("dayWise fromtime > tillTime" + singleDay);
                    break;
                }
                if (singleDay.getPointsToWin() < 0) {
                    errors.add("dayWise points negative");
                    break;
                }
            }
        }
        return errors;
    }

}
