package com.vedantu.cmds.request.challenges;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class SubmitPictureChallengeAnswerReq extends AbstractFrontEndReq{

	private String clanId;
	private String pictureChallengeId;
	private String answer;
	private Long token;
	private Long userId;

	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public String getPictureChallengeId() {
		return pictureChallengeId;
	}

	public void setPictureChallengeId(String pictureChallengeId) {
		this.pictureChallengeId = pictureChallengeId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}


	public Long getToken() {
		return token;
	}

	public void setToken(Long token) {
		this.token = token;
	}

	   public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	    protected List<String> collectVerificationErrors() {
	        List<String> errors = super.collectVerificationErrors();
	        if (token == null) {
	        	if(StringUtils.isEmpty(clanId)){
	        		errors.add("clanId");
	        	}
	        	if(StringUtils.isEmpty(pictureChallengeId)){
	        		errors.add("pictureChallengeId");
	        	}
	        }
	        if (StringUtils.isEmpty(answer)) {
	            errors.add("answer");
	        }
	        if(userId == null){
	        	errors.add("userId");
	        }
	        return errors;
	    }

}
