/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request.challenges;

import com.vedantu.cmds.enums.challenges.ChallengeType;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.challenges.ChallengeQuestion;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.cmds.request.AbstractAddCMDSEntityReq;
import com.vedantu.util.ArrayUtils;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class AddChallengeReq extends AbstractAddCMDSEntityReq {

    private String title;
    private ChallengeType type=ChallengeType.BURST_LATER;
    private Long burstTime;
    private Long startTime;
    private Integer duration = 0;
    private Difficulty difficulty;
    private List<ChallengeQuestion> questions;
    private Integer maxPoints;
    private String category;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ChallengeType getType() {
        return type;
    }

    public void setType(ChallengeType type) {
        this.type = type;
    }

    public Long getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(Long burstTime) {
        this.burstTime = burstTime;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public List<ChallengeQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<ChallengeQuestion> questions) {
        this.questions = questions;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }
        if (ArrayUtils.isEmpty(questions)) {
            errors.add("questions");
        } else {
            Integer _maxPoints = 0;
            for (ChallengeQuestion challengeQuestion : questions) {
                if (challengeQuestion.getMaxPoints() == null || challengeQuestion.getMaxPoints() <= 0) {
                    errors.add("maxPoints for a question should not be 0");
                } else {
                    _maxPoints += challengeQuestion.getMaxPoints();
                }
                if (StringUtils.isEmpty(challengeQuestion.getQid())) {
                    errors.add("invalid qid");
                }
                if (ArrayUtils.isNotEmpty(challengeQuestion.getHints())) {
                    int previousDeducation = 0;
                    int totalHintDeductions = 0;
                    for (HintFormat hintFormat : challengeQuestion.getHints()) {
                        if (hintFormat.getDeduction() < 0 || hintFormat.getDeduction() == null) {
                            errors.add("invalid deduction value for hint");
                            continue;
                        }
                        totalHintDeductions += hintFormat.getDeduction();
                        if ((previousDeducation != 0 && previousDeducation > hintFormat.getDeduction())) {
                            errors.add("invalid hint diduction value[" + hintFormat.getDeduction()
                                    + "], previousHintDeducation value[" + previousDeducation
                                    + "]");
                        }
                        previousDeducation = hintFormat.getDeduction();
                    }
                    if (totalHintDeductions > challengeQuestion.getMaxPoints()) {
                        errors.add("totalHintDeductions>maxPoints of question");
                    }
                }
            }
            maxPoints = _maxPoints;
        }
        if (ChallengeType.BURST_LATER.equals(type)) {
            if (burstTime == null || burstTime < System.currentTimeMillis()) {
                errors.add("invalid bursttime");
            }
            if (startTime != null && startTime > burstTime) {
                errors.add("startTime > burstTime");
            }
        }
        if (duration == null || duration <= 0) {
            errors.add("duration");
        }

        return errors;
    }

}
