package com.vedantu.cmds.request;

import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.util.CollectionUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;

/**
 * Created by somil on 24/04/17.
 */
@Data
@NoArgsConstructor
public class CreateTestReq extends CMDSTest {
    private List<CMDSTestQuestion> questions;

    public void validate() throws BadRequestException {
        List<String> errors = collectErrors();
        if (!CollectionUtils.isEmpty(errors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Missing params : " + Arrays.toString(errors.toArray()));
        }
    }

}
