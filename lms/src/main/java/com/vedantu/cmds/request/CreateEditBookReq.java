/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.cmds.pojo.BookNode;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.util.List;
import org.springframework.util.StringUtils;

/**
 *
 * @author ajith
 */
public class CreateEditBookReq extends AbstractAddCMDSEntityReq {

    private String displayName;
    private String bookId;
    private String description;
    private List<CloudStorageEntity> thumbnails;
    private String edition;
    private List<BookNode> nodes;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getBookId() {
        return bookId;
    }

    public void setBookId(String bookId) {
        this.bookId = bookId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CloudStorageEntity> getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(List<CloudStorageEntity> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public List<BookNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<BookNode> nodes) {
        this.nodes = nodes;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(this.getName())) {
            errors.add("Name");
        } else {
            if (StringUtils.isEmpty(displayName)) {
                this.setDisplayName(this.getName());
            }
        }

        if (!ArrayUtils.isEmpty(nodes)) {
            for (BookNode node : nodes) {
                if (node.getType() == null) {
                    errors.add("node-type");
                }
                if (StringUtils.isEmpty(node.getName())) {
                    errors.add("node-name");
                }
            }
        }
        return errors;
    }

}
