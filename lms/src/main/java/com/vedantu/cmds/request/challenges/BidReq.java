/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request.challenges;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class BidReq extends AbstractFrontEndReq {

    private Long userId;
    private String token;//challengeTakenId//attemptid
    private int bid;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getBid() {
        return bid;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null || userId.equals(0)) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(token)) {
            errors.add("token");
        }
        if (bid < 0) {
            errors.add("bid<0");
        }
        return errors;
    }

}
