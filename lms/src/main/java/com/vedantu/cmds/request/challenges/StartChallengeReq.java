package com.vedantu.cmds.request.challenges;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class StartChallengeReq extends AbstractFrontEndReq {

    private Long userId;
    private String challengeId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null || userId.equals(0)) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(challengeId)) {
            errors.add("challengeId");
        }
        return errors;
    }

}
