package com.vedantu.cmds.request.clans;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetClansReq extends AbstractFrontEndListReq{

	private Long userId;
	private String category;
	private String sortOrder;
	private String orderBy;
	private List<String> shareCodes;
	private boolean addPoints;
	private boolean addMembers;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getSortOrder() {
		return sortOrder;
	}

	public void setSortOrder(String sortOrder) {
		this.sortOrder = sortOrder;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public List<String> getShareCodes() {
		return shareCodes;
	}

	public void setShareCodes(List<String> shareCodes) {
		this.shareCodes = shareCodes;
	}

	public boolean isAddPoints() {
		return addPoints;
	}

	public void setAddPoints(boolean addPoints) {
		this.addPoints = addPoints;
	}

	public boolean isAddMembers() {
		return addMembers;
	}

	public void setAddMembers(boolean addMembers) {
		this.addMembers = addMembers;
	}
}
