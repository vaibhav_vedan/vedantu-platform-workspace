package com.vedantu.cmds.request;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurriculumTemplateInsertionRequest extends AbstractFrontEndReq {
    private String title;
    private Set<String> grades;
    private Set<String> targets;
    private Integer year;
    private CurriculumEntityName entityName;
    private String entityId;
    private Node node;
    private List<String> batchIds;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();
        if(StringUtils.isEmpty(title)){
            errors.add("Title couldn't be null");
        }

        if(ArrayUtils.isEmpty(grades)){
            errors.add("Grades couldn't be null");
        }

        if(ArrayUtils.isEmpty(targets)){
            errors.add("Targets couldn't be null");
        }

        if(year==null){
            errors.add("Year couldn't be null");
        }

        return errors;
    }


}
