package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetCMDSTestListReq extends AbstractFrontEndListReq {

    private Boolean published;

    /**
     * @return the published
     */
    public Boolean getPublished() {
        return published;
    }

    /**
     * @param published the published to set
     */
    public void setPublished(Boolean published) {
        this.published = published;
    }

}
