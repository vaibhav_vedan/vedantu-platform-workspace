package com.vedantu.cmds.request;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.UserReport;
import com.vedantu.cmds.enums.ReportEntityType;
import com.vedantu.cmds.enums.ReportStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class ReviewReportReq extends AbstractFrontEndReq {

    private String reportId;
    private ReportStatus status;
    private String newEntityId;
    private CMDSQuestion question;
    private String reviewBy;
    private String reviewComment;


    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    public String getReportId() {
        return reportId;
    }

    public void setReportId(String reportId) {
        this.reportId = reportId;
    }

    public CMDSQuestion getQuestion() {
        return question;
    }

    public void setQuestion(CMDSQuestion question) {
        this.question = question;
    }

    public String getNewEntityId() {
        return newEntityId;
    }

    public void setNewEntityId(String newEntityId) {
        this.newEntityId = newEntityId;
    }

    public String getReviewComment() {
        return reviewComment;
    }

    public void setReviewComment(String reviewComment) {
        this.reviewComment = reviewComment;
    }
}
