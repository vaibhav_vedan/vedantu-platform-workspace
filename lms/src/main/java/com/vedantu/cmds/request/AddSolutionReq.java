package com.vedantu.cmds.request;

import org.apache.commons.lang.StringUtils;

public class AddSolutionReq {

	public String questionId;
	public String solution;
	public boolean delete;
	public String newSolution;
	public boolean edit;

	public String validate() {
		if (StringUtils.isEmpty(questionId)) {
			return "questionId missing";
		}
		if (edit && StringUtils.isEmpty(newSolution)) {
			return "new solution is missing";
		}
		if (StringUtils.isEmpty(solution)) {
			return "solution is missing";
		}

		if (delete && edit) {
			return "ambiguous input for update";
		}
		return null;
	}
}
