package com.vedantu.cmds.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class GradeCMDSPlaylistOrder extends AbstractFrontEndReq {

    private String playlistOrderId;
    private String grade;
    private List<String> playlistIds;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<String> getPlaylistIds() {
        return playlistIds;
    }

    public void setPlaylistIds(List<String> playlistIds) {
        this.playlistIds = playlistIds;
    }

    public String getPlaylistOrderId() {
        return playlistOrderId;
    }

    public void setPlaylistOrderId(String playlistOrderId) {
        this.playlistOrderId = playlistOrderId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(grade)) {
            errors.add("grade");
        }
        if (ArrayUtils.isEmpty(playlistIds)) {
            errors.add("playlistIds");
        }
        return errors;
    }

}
