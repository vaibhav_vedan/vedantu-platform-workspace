package com.vedantu.cmds.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class CurriculumTemplateRetrievalRequest extends AbstractFrontEndReq{
    private String parentId;

    @Override
    protected List<String> collectVerificationErrors(){
        List<String> errors=super.collectVerificationErrors();

        if(StringUtils.isEmpty(parentId)){
            errors.add("parentId can't be null");
        }

        return errors;
    }
}
