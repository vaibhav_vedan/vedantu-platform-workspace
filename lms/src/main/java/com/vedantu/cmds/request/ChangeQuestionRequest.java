/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.ChangeQuestionActionType;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class ChangeQuestionRequest extends AbstractAddCMDSEntityReq {
    private String questionId;
    private String newQuestionId;
    private RichTextFormat questionBody;
    private List<String> optionOrder;    
    private QuestionType questionType;
    private List<RichTextFormat> options;
    private List<RichTextFormat> solutions;
    private List<String> answers;
    private Difficulty difficulty;
    private List<HintFormat> refHints;
    private Float positiveMarks;
    private Float negativeMarks;

    //book related
    private String book;
    private String edition;
    private String chapter;
    private String chapterNo;
    private String exercise;
    private Set<String> pageNos;
    private int slNoInBook;
    private String questionNo;
    
    private List<String> testIdsForAction;
    private List<String> challengeIdsForAction;
    
    private ChangeQuestionActionType changeQuestionActionType = ChangeQuestionActionType.NO_ACTION;
    private boolean sendNotification = false;
    private List<ChangeQuestionAction> changeQuestionActions = new ArrayList<>();
    private String testId;
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        if(StringUtils.isEmpty(questionId)){
            errors.add("Invalid questionId");
        }
        
        if(super.getCallingUserId() == null){
            errors.add("Invalid callingUserId");
        }
        
        if (questionBody == null) {
            errors.add("questionBody");
        } else if (org.apache.commons.lang.StringUtils.isEmpty(questionBody.getNewText())) {
            errors.add("questionBody newText");
        }

        if (ArrayUtils.isNotEmpty(solutions)) {
            for (RichTextFormat solution : solutions) {
                if (org.apache.commons.lang.StringUtils.isEmpty(solution.getNewText())) {
                    errors.add("invalid solution");
                }
            }
        }
        if (ArrayUtils.isNotEmpty(options)) {
            for (RichTextFormat option : options) {
                if (org.apache.commons.lang.StringUtils.isEmpty(option.getNewText())) {
                    errors.add("invalid option");
                }
            }
        }
        if (!(QuestionType.MCQ.equals(questionType)
                || QuestionType.SCQ.equals(questionType) || QuestionType.NUMERIC.equals(questionType) || QuestionType.SHORT_TEXT.equals(questionType))) {
            errors.add("only mcq,scq, numeric allowed");
        } else if (ArrayUtils.isEmpty(answers)) {
            errors.add("answers");
        }
        if (ArrayUtils.isNotEmpty(refHints)) {
            for (RichTextFormat hint : refHints) {
                if (org.apache.commons.lang.StringUtils.isEmpty(hint.getNewText())) {
                    errors.add("invalid hint");
                }
            }
        }
        return errors;
        
    }
    
    
}
