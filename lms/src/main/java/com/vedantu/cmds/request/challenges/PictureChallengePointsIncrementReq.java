package com.vedantu.cmds.request.challenges;

import java.util.List;

import com.vedantu.cmds.pojo.challenges.ClanMemberCount;
import com.vedantu.cmds.pojo.challenges.PictureChallengeDayDeductions;

public class PictureChallengePointsIncrementReq {

	private String category;
	private List<ClanMemberCount> clans;
	private String pictureChallengeId;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<ClanMemberCount> getClans() {
		return clans;
	}

	public void setClans(List<ClanMemberCount> clans) {
		this.clans = clans;
	}

	public String getPictureChallengeId() {
		return pictureChallengeId;
	}

	public void setPictureChallengeId(String pictureChallengeId) {
		this.pictureChallengeId = pictureChallengeId;
	}

}
