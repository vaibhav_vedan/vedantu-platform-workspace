/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.lms.cmds.enums.ChangeQuestionActionType;

/**
 *
 * @author parashar
 */
public class ChangeQuestionAction {
    
    private ChangeQuestionActionType changeQuestionActionType;
    private String questionId;

    /**
     * @return the changeQuestionActionType
     */
    public ChangeQuestionActionType getChangeQuestionActionType() {
        return changeQuestionActionType;
    }

    /**
     * @param changeQuestionActionType the changeQuestionActionType to set
     */
    public void setChangeQuestionActionType(ChangeQuestionActionType changeQuestionActionType) {
        this.changeQuestionActionType = changeQuestionActionType;
    }

    /**
     * @return the questionId
     */
    public String getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId the questionId to set
     */
    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
    
    
}
