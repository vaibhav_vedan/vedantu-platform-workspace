package com.vedantu.cmds.request;

import com.vedantu.User.Role;

public class DeleteQuestionSetsReq {
	private String questionSetId;
	private String callingUserId;
	private Role callingUserRole;

	public DeleteQuestionSetsReq() {
		super();
	}

	public String getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(String questionSetId) {
		this.questionSetId = questionSetId;
	}

	public String getCallingUserId() {
		return callingUserId;
	}

	public void setCallingUserId(String callingUserId) {
		this.callingUserId = callingUserId;
	}

	public Role getCallingUserRole() {
		return callingUserRole;
	}

	public void setCallingUserRole(Role callingUserRole) {
		this.callingUserRole = callingUserRole;
	}

	@Override
	public String toString() {
		return "DeleteQuestionSetsReq [questionSetId=" + questionSetId + ", callingUserId=" + callingUserId
				+ ", callingUserRole=" + callingUserRole + ", toString()=" + super.toString() + "]";
	}
}
