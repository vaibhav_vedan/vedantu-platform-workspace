package com.vedantu.cmds.request;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.fos.request.AbstractGetListReq;

public class GetQuestionSetsReq extends AbstractGetListReq {

    private List<VedantuRecordState> recordStates;
    private String createdBy;
    private Difficulty difficulty;
    public VedantuRecordState status;

    public GetQuestionSetsReq() {
        super();
    }

    public List<VedantuRecordState> getRecordStates() {
        return recordStates;
    }

    public void setRecordStates(List<VedantuRecordState> recordStates) {
        this.recordStates = recordStates;
    }

    public void addRecordState(VedantuRecordState recordState) {
        if (this.recordStates == null) {
            this.recordStates = new ArrayList<VedantuRecordState>();
        }
        this.recordStates.add(recordState);
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public VedantuRecordState getStatus() {
        return status;
    }

    public void setStatus(VedantuRecordState status) {
        this.status = status;
    }

}
