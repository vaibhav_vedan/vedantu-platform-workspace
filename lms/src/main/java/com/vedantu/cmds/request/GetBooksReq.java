/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author ajith
 */
public class GetBooksReq extends AbstractFrontEndListReq {

    private String grade;
    private String target;
    private String subject;
    private Long lastFetchedTime = 0l;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Long getLastFetchedTime() {
    	 return lastFetchedTime;
    }

    public void setLastFetchedTime(Long lastFetchedTime) {
    	 this.lastFetchedTime = lastFetchedTime;
    }
    

}
