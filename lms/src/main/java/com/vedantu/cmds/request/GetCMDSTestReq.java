package com.vedantu.cmds.request;

import java.util.List;
import java.util.Set;

import com.vedantu.cmds.pojo.CMDSTestState;
import com.vedantu.lms.cmds.enums.VedantuRecordState;

public class GetCMDSTestReq {
	private String questionSetId;
	private Integer versionNo;
	private List<CMDSTestState> testStates;
	private List<VedantuRecordState> recordStates;
	private Integer start;
	private Integer size;

	private boolean visibleInApp;

	public Set<String> boardIds;
	public Set<String> targetIds;
	public Set<String> grades;
	public Set<String> tags;

	public GetCMDSTestReq() {
		super();
	}

	public String getQuestionSetId() {
		return questionSetId;
	}

	public void setQuestionSetId(String questionSetId) {
		this.questionSetId = questionSetId;
	}

	public Integer getVersionNo() {
		return versionNo;
	}

	public void setVersionNo(Integer versionNo) {
		this.versionNo = versionNo;
	}

	public List<CMDSTestState> getTestStates() {
		return testStates;
	}

	public void setTestStates(List<CMDSTestState> testStates) {
		this.testStates = testStates;
	}

	public List<VedantuRecordState> getRecordStates() {
		return recordStates;
	}

	public void setRecordStates(List<VedantuRecordState> recordStates) {
		this.recordStates = recordStates;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getSize() {
		return size;
	}

	public void setSize(Integer size) {
		this.size = size;
	}

	public Set<String> getBoardIds() {
		return boardIds;
	}

	public void setBoardIds(Set<String> boardIds) {
		this.boardIds = boardIds;
	}

	public Set<String> getTargetIds() {
		return targetIds;
	}

	public void setTargetIds(Set<String> targetIds) {
		this.targetIds = targetIds;
	}

	public Set<String> getGrades() {
		return grades;
	}

	public void setGrades(Set<String> grades) {
		this.grades = grades;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	public boolean isVisibleInApp() {
		return visibleInApp;
	}

	public void setVisibleInApp(boolean visibleInApp) {
		this.visibleInApp = visibleInApp;
	}

	@Override
	public String toString() {
		return "GetCMDSTestReq{" +
				"questionSetId='" + questionSetId + '\'' +
				", versionNo=" + versionNo +
				", testStates=" + testStates +
				", recordStates=" + recordStates +
				", start=" + start +
				", size=" + size +
				", boardIds=" + boardIds +
				", targetIds=" + targetIds +
				", grades=" + grades +
				'}';
	}
}
