package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetTestsMobileReq extends AbstractFrontEndListReq {

    private String grade;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
