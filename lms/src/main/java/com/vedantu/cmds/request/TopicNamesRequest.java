package com.vedantu.cmds.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TopicNamesRequest extends AbstractFrontEndReq {
    private String name;
    private List<String> names;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();

        if(ArrayUtils.isEmpty(names) && (name==null || StringUtils.isEmpty(name.trim()))){
            errors.add("Couldn't pass empty names");
        }

        return errors;
    }
}
