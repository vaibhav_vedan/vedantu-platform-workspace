package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CurriculumTemplateRetrievalFilterRequest extends AbstractFrontEndReq{

    private String grade;
    private String target;
    private Integer year;
    private String title;
    private String courseOrBatchId;
    private int start=0;
    private int size=10;
}
