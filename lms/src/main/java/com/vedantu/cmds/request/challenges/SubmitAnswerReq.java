
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request.challenges;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class SubmitAnswerReq extends AbstractFrontEndReq {

    private String token;//challengeTakenId//attemptid
    private List<String> answer;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(token)) {
            errors.add("token");
        }
        if (ArrayUtils.isEmpty(answer)) {
            errors.add("answer");
        } else {
            for (String _answer : answer) {
                if (StringUtils.isEmpty(_answer) || _answer.length() > 240) {
                    errors.add("invalid answer");
                }
            }
        }
        return errors;
    }
}
