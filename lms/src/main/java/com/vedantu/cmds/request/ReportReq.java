package com.vedantu.cmds.request;

import com.vedantu.cmds.entities.UserReport;
import com.vedantu.cmds.enums.ReportEntityType;
import com.vedantu.cmds.enums.ReportStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

public class ReportReq extends AbstractFrontEndReq {

    private String entityId;
    private String newEntityId;
    private ReportEntityType entityType;
    private ReportStatus status;
    private String reviewBy;
    private List<UserReport> userReports;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public ReportEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(ReportEntityType entityType) {
        this.entityType = entityType;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    public List<UserReport> getUserReports() {
        return userReports;
    }

    public void setUserReports(List<UserReport> userReports) {
        this.userReports = userReports;
    }

    public String getNewEntityId() {
        return newEntityId;
    }

    public void setNewEntityId(String newEntityId) {
        this.newEntityId = newEntityId;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if(userReports.size() != 1){
            errors.add("no user Reports");
        }

        if(entityId == null){
            errors.add("entityId");
        }
        if(entityType == null){
            errors.add("entityType");
        }

        return errors;
    }
}
