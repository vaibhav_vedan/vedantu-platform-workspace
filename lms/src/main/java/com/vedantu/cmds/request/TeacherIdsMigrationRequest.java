package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

@Data
public class TeacherIdsMigrationRequest extends AbstractFrontEndReq {
    private List<String> teacherIds;
}
