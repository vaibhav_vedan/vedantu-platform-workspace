package com.vedantu.cmds.request;

import com.vedantu.cmds.entities.CurriculumTemplate;
import com.vedantu.util.ArrayUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
public class CurriculumTemplateTopicTagsAdd extends CurriculumTemplate {

    public Set<String> topicTags;

    public CurriculumTemplateTopicTagsAdd(CurriculumTemplate curriculumTemplate){

        super(curriculumTemplate);
        if(ArrayUtils.isNotEmpty(curriculumTemplate.getMainTags())){
            this.topicTags=curriculumTemplate.getMainTags();
        }

    }

}