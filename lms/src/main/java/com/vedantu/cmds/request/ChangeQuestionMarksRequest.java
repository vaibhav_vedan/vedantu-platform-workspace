package com.vedantu.cmds.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangeQuestionMarksRequest {

    private Long fromTime;
    private Long thruTime;
}
