/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request.challenges;

import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.challenges.ChallengeType;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetChallengesReq extends AbstractFrontEndListReq {

    private ChallengeStatus status;
    private List<String> brdIds;
    private String sortOrder;
    private String orderBy;
    private ChallengeType type;
    private Difficulty difficulty;
    private Boolean attemptedOnly;
    private Long userId;//usually the callinguserId
    private QuestionType qType;
    private String category;

    public ChallengeStatus getStatus() {
        return status;
    }

    public void setStatus(ChallengeStatus status) {
        this.status = status;
    }

    public List<String> getBrdIds() {
        return brdIds;
    }

    public void setBrdIds(List<String> brdIds) {
        this.brdIds = brdIds;
    }

    public String getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(String sortOrder) {
        this.sortOrder = sortOrder;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public ChallengeType getType() {
        return type;
    }

    public void setType(ChallengeType type) {
        this.type = type;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Boolean getAttemptedOnly() {
        return attemptedOnly;
    }

    public void setAttemptedOnly(Boolean attemptedOnly) {
        this.attemptedOnly = attemptedOnly;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public QuestionType getqType() {
        return qType;
    }

    public void setqType(QuestionType qType) {
        this.qType = qType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (Boolean.TRUE.equals(attemptedOnly) && (userId == null || userId.equals(0))) {
            errors.add("userId needed for attemptedOnly query");
        }
        return errors;
    }

}
