package com.vedantu.cmds.request;

import com.vedantu.User.Role;

import java.util.List;

public class CreateQuestionSetReq {

    private String callingUserId;
    private Role callingUserRole;
    private List<String> questionIds;

    public String getCallingUserId() {
        return callingUserId;
    }

    public void setCallingUserId(String callingUserId) {
        this.callingUserId = callingUserId;
    }

    public Role getCallingUserRole() {
        return callingUserRole;
    }

    public void setCallingUserRole(Role callingUserRole) {
        this.callingUserRole = callingUserRole;
    }

    public List<String> getQuestionIds() {
        return questionIds;
    }

    public void setQuestionIds(List<String> questionIds) {
        this.questionIds = questionIds;
    }
}
