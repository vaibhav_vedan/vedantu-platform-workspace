package com.vedantu.cmds.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionDuplicationFindingRequest extends AbstractFrontEndReq {
    private String questionSetId;
    private String testId;
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(StringUtils.isEmpty(testId) && StringUtils.isEmpty(questionSetId)){
            errors.add("Please specify a `questionSetId` or list of `questionIds`");
        }

        if(StringUtils.isNotEmpty(testId) && StringUtils.isNotEmpty(questionSetId)){
            errors.add("Please specify one of `questionSetId` or list of `questionIds`");
        }

        return errors;
    }
}
