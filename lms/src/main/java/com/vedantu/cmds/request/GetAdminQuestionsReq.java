package com.vedantu.cmds.request;

import com.vedantu.cmds.enums.Indexable;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.Set;

/**
 *
 * @author ravneet
 */

public class GetAdminQuestionsReq  extends AbstractFrontEndListReq {

    private Set<String> mainTags;
    private Difficulty difficulty;
//    private VedantuRecordState recordState;
    private String cognitiveLevel;
    private String source;
    private Boolean used;
    private String testId;
    private QuestionType type;
    private Indexable indexable;

    public Boolean isUsed() {
        return used;
    }

    public void setUsed(Boolean used) {
        this.used = used;
    }

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

//    public VedantuRecordState getRecordState() {
//        return recordState;
//    }
//
//    public void setRecordState(VedantuRecordState recordState) {
//        this.recordState = recordState;
//    }

    public String getCognitiveLevel() {
        return cognitiveLevel;
    }

    public void setCognitiveLevel(String cognitiveLevel) {
        this.cognitiveLevel = cognitiveLevel;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public Indexable getIndexable() {
        return indexable;
    }

    public void setIndexable(Indexable indexable) {
        this.indexable = indexable;
    }

}
