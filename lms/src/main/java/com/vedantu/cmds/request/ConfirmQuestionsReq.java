package com.vedantu.cmds.request;

import java.util.List;
import java.util.Set;

import com.vedantu.cmds.pojo.CMDSQuestionIdInfo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ConfirmQuestionsReq extends AbstractFrontEndReq {
	private String questionSetId;
	private String userId;
	private String prefixFileName;
	private String providedQuestionSetName;
	private List<CMDSQuestionIdInfo> questionIdInfos;

	private Set<String> boardIds;
	private Set<String> targetIds;
	private Set<String> grades;
	private Set<String> tags;
	private CreateTestReq cmdsTest;
	private Long doNotShowResultsTill;
	private boolean hardStop=false;
	private Long expiryDate;
	private Long expiryDays;
	private Long checkerId;
	
	
	public void populateDataForCMDSTest(){
		this.cmdsTest.setName(this.providedQuestionSetName);
		this.cmdsTest.setBoardIds(this.boardIds);
		this.cmdsTest.setTargetIds(this.targetIds);
		this.cmdsTest.setGrades(this.grades);
		this.cmdsTest.setTags(this.tags);
		this.cmdsTest.setDoNotShowResultsTill(this.doNotShowResultsTill);
		this.cmdsTest.setHardStop(this.hardStop);
		this.cmdsTest.setExpiryDate(this.expiryDate);
		this.cmdsTest.setExpiryDays(this.expiryDays);
		this.cmdsTest.setAssignedChecker(this.checkerId);
		this.cmdsTest.setCreatedBy(this.getCallingUserId().toString());
	}
}
