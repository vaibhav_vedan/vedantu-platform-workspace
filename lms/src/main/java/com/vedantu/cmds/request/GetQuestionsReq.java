/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class GetQuestionsReq extends AbstractFrontEndListReq {

    private String book;
    private String edition;
    private String chapter;
    private String chapterNo;
    private String exercise;
    private Set<String> pageNos;

    public String getBook() {
        return book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getChapterNo() {
        return chapterNo;
    }

    public void setChapterNo(String chapterNo) {
        this.chapterNo = chapterNo;
    }

    public String getExercise() {
        return exercise;
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
    }

    public Set<String> getPageNos() {
        return pageNos;
    }

    public void setPageNos(Set<String> pageNos) {
        this.pageNos = pageNos;
    }

}
