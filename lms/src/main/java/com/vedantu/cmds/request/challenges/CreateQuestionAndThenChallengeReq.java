/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request.challenges;

import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.ArrayUtils;
import java.util.List;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author ajith
 */
public class CreateQuestionAndThenChallengeReq extends AddChallengeReq {

    public RichTextFormat questionBody;
    public QuestionType questionType;
    public List<RichTextFormat> options;
    public List<RichTextFormat> solutions;
    public List<String> answers;
    public List<HintFormat> refHints;

    public CreateQuestionAndThenChallengeReq() {
    }

    
    public RichTextFormat getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(RichTextFormat questionBody) {
        this.questionBody = questionBody;
    }

    public QuestionType getQuestionType() {
        return questionType;
    }

    public void setQuestionType(QuestionType questionType) {
        this.questionType = questionType;
    }

    public List<RichTextFormat> getOptions() {
        return options;
    }

    public void setOptions(List<RichTextFormat> options) {
        this.options = options;
    }

    public List<RichTextFormat> getSolutions() {
        return solutions;
    }

    public void setSolutions(List<RichTextFormat> solutions) {
        this.solutions = solutions;
    }

    public List<String> getAnswers() {
        return answers;
    }

    public void setAnswers(List<String> answers) {
        this.answers = answers;
    }

    public List<HintFormat> getRefHints() {
        return refHints;
    }

    public void setRefHints(List<HintFormat> refHints) {
        this.refHints = refHints;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        //setting the questions to null as this flow does not need them
        super.setQuestions(null);
                    
        List<String> errors = super.collectVerificationErrors();
        
        //removing the errors due to question  as this field is not needed in this flow
        errors.remove("questions");
        
        if (questionBody == null) {
            errors.add("questionBody");
        } else if (StringUtils.isEmpty(questionBody.getNewText())) {
            errors.add("questionBody newText");
        }

        if (ArrayUtils.isNotEmpty(solutions)) {
            for (RichTextFormat solution : solutions) {
                if (StringUtils.isEmpty(solution.getNewText())) {
                    errors.add("invalid solution");
                }
            }
        }
        if (ArrayUtils.isNotEmpty(options)) {
            for (RichTextFormat option : options) {
                if (StringUtils.isEmpty(option.getNewText())) {
                    errors.add("invalid option");
                }
            }
        }
        if (!(QuestionType.MCQ.equals(questionType)
                || QuestionType.SCQ.equals(questionType)
                || QuestionType.NUMERIC.equals(questionType))) {
            errors.add("only mcq,scq,numeric allowed");
        } else if (ArrayUtils.isEmpty(answers)) {
            errors.add("answers");
        }
        if (ArrayUtils.isNotEmpty(refHints)) {
            for (RichTextFormat hint : refHints) {
                if (StringUtils.isEmpty(hint.getNewText())) {
                    errors.add("invalid hint");
                }
            }
        }
        
        return errors;
    }

    
}
