package com.vedantu.cmds.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class GenericEndTestReq extends AbstractFrontEndReq {

    private List<String> contentInfoIds;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String>errors=super.collectVerificationErrors();

        if(ArrayUtils.isEmpty(contentInfoIds)){
            errors.add("ContentInfoIds are either null or empty");
        }

        return errors;
    }
}
