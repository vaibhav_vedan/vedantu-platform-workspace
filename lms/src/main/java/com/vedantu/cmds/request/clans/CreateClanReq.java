package com.vedantu.cmds.request.clans;

import java.util.List;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.pojo.CloudStorageEntity;

public class CreateClanReq extends AbstractFrontEndReq {

	private Long userId;
	private String title;
	private String tagLine;
	private String event;
	private List<String> allowedCategories;
	private CloudStorageEntity clanPic;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTagLine() {
		return tagLine;
	}

	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public List<String> getAllowedCategories() {
		return allowedCategories;
	}

	public void setAllowedCategories(List<String> allowedCategories) {
		this.allowedCategories = allowedCategories;
	}

	public CloudStorageEntity getClanPic() {
		return clanPic;
	}

	public void setClanPic(CloudStorageEntity clanPic) {
		this.clanPic = clanPic;
	}
	
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(event)) {
            errors.add("event");
        }
        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }
        if (ArrayUtils.isEmpty(allowedCategories)) {
            errors.add("allowedCategories");
        }
        if (userId == null) {
            errors.add("userId");
        }
        
        return errors;
	}
}
