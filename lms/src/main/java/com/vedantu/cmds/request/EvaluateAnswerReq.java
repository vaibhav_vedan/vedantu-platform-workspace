/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class EvaluateAnswerReq extends AbstractFrontEndReq {

    private String qId;
    private List<String> answerGiven;

    public String getqId() {
        return qId;
    }

    public void setqId(String qId) {
        this.qId = qId;
    }

    public List<String> getAnswerGiven() {
        return answerGiven;
    }

    public void setAnswerGiven(List<String> answerGiven) {
        this.answerGiven = answerGiven;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(answerGiven)) {
            errors.add("answerGiven");
        }
        if (StringUtils.isEmpty(qId)) {
            errors.add("qId");
        }
        return errors;
    }


}
