package com.vedantu.cmds.request.clans;

import java.util.List;


public class ProcessClanChallengeAttemptsReq {
	private List<String> clanIds;
	private String challengeId;
	private String category;

	public List<String> getClanIds() {
		return clanIds;
	}

	public void setClanIds(List<String> clanIds) {
		this.clanIds = clanIds;
	}

	public String getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(String challengeId) {
		this.challengeId = challengeId;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
