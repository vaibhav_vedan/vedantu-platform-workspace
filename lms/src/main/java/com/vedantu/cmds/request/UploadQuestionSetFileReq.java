package com.vedantu.cmds.request;

import com.vedantu.cmds.pojo.ParseQuestionMetadata;
import com.vedantu.lms.cmds.enums.AccessLevel;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

import com.vedantu.lms.cmds.enums.VedantuRecordState;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import com.vedantu.util.CommonUtils;
import com.vedantu.util.fos.request.AbstractReq;

@Data
@NoArgsConstructor
public class UploadQuestionSetFileReq extends AbstractReq{

    public File questionSetFile;
    public String questionSetFileName;
    public String userId;
    private AccessLevel accessLevel;
    private ParseQuestionMetadata metadata;
    private VedantuRecordState type;

    public UploadQuestionSetFileReq(File file, String questionSetFileName, String userId,
            AccessLevel accessLevel) {
        this.questionSetFile = file;
        this.questionSetFileName = questionSetFileName;
        this.userId = userId;
        this.accessLevel = accessLevel;
    }
    
    public UploadQuestionSetFileReq(File file, String questionSetFileName, String userId,
            AccessLevel accessLevel, ParseQuestionMetadata metadata, VedantuRecordState type) {
        this.questionSetFile = file;
        this.questionSetFileName = questionSetFileName;
        this.userId = userId;
        this.accessLevel = accessLevel;
        this.metadata = metadata;
        this.type = type;
    }

    @Deprecated
    public UploadQuestionSetFileReq(MultipartFile file, String questionSetFileName,
            String userId, AccessLevel accessLevel)
            throws IllegalStateException, IOException {
        this(CommonUtils.multipartToFile(file), questionSetFileName, userId, accessLevel);
    }

    @Deprecated
    public String validate() {
        if (Objects.isNull(questionSetFile)) {
            return "";
        }
        return null;
    }

}
