package com.vedantu.cmds.dao.sql;

import com.vedantu.cmds.constants.ConstantsGlobal;
import com.vedantu.cmds.entities.challenges.PictureChallengeLeaderBoard;
import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractSqlDAO;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

@Service
public class PictureChallengeLeaderBoardDAO extends AbstractSqlDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PictureChallengeLeaderBoardDAO.class);

	public PictureChallengeLeaderBoardDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}

	public void create(PictureChallengeLeaderBoard pictureChallengeLeaderBoard, String callingUserId) {
		try {
			if (pictureChallengeLeaderBoard != null) {
				saveEntity(pictureChallengeLeaderBoard, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create pictureChallengeLeaderBoard: " + ex.getMessage());
		}
	}

	public void save(PictureChallengeLeaderBoard p, Session session, String callingUserId) {
		if (p != null) {
			logger.info("update PictureChallengeLeaderBoard: " + p.toString());
			saveEntity(p, session, callingUserId);
		}
	}

	public PictureChallengeLeaderBoard getByClanId(String id, String challengeId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		PictureChallengeLeaderBoard pictureChallengeLeaderBoard = null;
		try {
			pictureChallengeLeaderBoard = getByClanId(id, session, challengeId);
		} catch (Exception ex) {
			logger.error("getByPictureChallengeId pictureChallengeLeaderBoard: " + ex.getMessage());
			pictureChallengeLeaderBoard = null;
		} finally {
			session.close();
		}
		return pictureChallengeLeaderBoard;
	}

	public PictureChallengeLeaderBoard getByClanId(String id, Session session, String challengeId) {
		PictureChallengeLeaderBoard pictureChallengeLeaderBoard = null;
		if (StringUtils.isNotEmpty(id)) {
			Criteria cr = session.createCriteria(PictureChallengeLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, id));
			cr.add(Restrictions.eq(ConstantsGlobal.PICTURE_CHALLENGE_ID, challengeId));
			List<PictureChallengeLeaderBoard> results = runQuery(session, cr, PictureChallengeLeaderBoard.class);
			if (!CollectionUtils.isEmpty(results)) {
				if (results.size() > 1) {
					logger.error("Duplicate entries found for id : " + id);
				}
				pictureChallengeLeaderBoard = results.get(0);
			} else {
				logger.info(
						"getByPictureChallengeId PictureChallengeLeaderBoard : no PictureChallengeLeaderBoard found "
								+ id);
			}
		}
		return pictureChallengeLeaderBoard;
	}

	public void update(PictureChallengeLeaderBoard p, Session session, String callingUserId) {
		if (p != null) {
			logger.info("update PictureChallengeLeaderBoard: " + p.toString());
			updateEntity(p, session, callingUserId);
		}
	}

	public List<PictureChallengeLeaderBoard> getPictureChallengeLeaderBoardForChallenge(String challengeId, int start,
			int size) {
		List<PictureChallengeLeaderBoard> leaderBoards = new ArrayList<>();
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(PictureChallengeLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.PICTURE_CHALLENGE_ID, challengeId));

			cr.addOrder(Order.desc(ConstantsGlobal.POINTS));
			setFetchParameters(cr, start, size);
			List<PictureChallengeLeaderBoard> results = runQuery(session, cr, PictureChallengeLeaderBoard.class);
			return results;
		} catch (Exception e) {
			logger.error("getPictureChallengeLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return leaderBoards;
	}

	public Long getPictureChallengeRank(String challengeId, long pictureChallengePoints) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Long rank = null;
		try {
			Criteria cr = session.createCriteria(PictureChallengeLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.PICTURE_CHALLENGE_ID, challengeId));
			cr.add(Restrictions.gt(ConstantsGlobal.POINTS, pictureChallengePoints));

			cr.setProjection(Projections.rowCount());

			List results = runQuery(session, cr, PictureChallengeLeaderBoard.class);
			if (!results.isEmpty()) {
				rank = (Long) results.get(0);
				rank++;
			}
		} catch (Exception e) {
			logger.error("getPictureChallengeLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return rank;
	}

	public List<PictureChallengeLeaderBoard> getByClanIdAndChallengeIds(String id,
			List<String> challengeIds) {
		List<PictureChallengeLeaderBoard> pictureChallengeLeaderBoard = null;
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			if (StringUtils.isNotEmpty(id) && ArrayUtils.isNotEmpty(challengeIds)) {
				Criteria cr = session.createCriteria(PictureChallengeLeaderBoard.class);
				cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, id));
				cr.add(Restrictions.in(ConstantsGlobal.PICTURE_CHALLENGE_ID, challengeIds));
				pictureChallengeLeaderBoard = runQuery(session, cr, PictureChallengeLeaderBoard.class);
	
			} else {
				logger.info(
						"getByPictureChallengeId PictureChallengeLeaderBoard : no PictureChallengeLeaderBoard found " + id);
			}
		} catch (Exception e) {
			logger.error("getPictureChallengeLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}

		return pictureChallengeLeaderBoard;
	}

}
