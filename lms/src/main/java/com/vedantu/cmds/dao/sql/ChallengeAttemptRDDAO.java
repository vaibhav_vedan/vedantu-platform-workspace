package com.vedantu.cmds.dao.sql;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.cmds.constants.ConstantsGlobal;
import com.vedantu.cmds.entities.challenges.ChallengeAttempt;
import com.vedantu.cmds.entities.challenges.ChallengeAttemptRD;
import com.vedantu.cmds.entities.challenges.ClanMember;
import com.vedantu.cmds.pojo.challenges.ChallengeAttemptsCount;
import com.vedantu.lms.cmds.pojo.AttemptedUserIds;
import com.vedantu.lms.cmds.pojo.ChallengeUserPoints;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractSqlDAO;
import com.vedantu.util.enums.SortOrder;

@Service
public class ChallengeAttemptRDDAO extends AbstractSqlDAO {

	
	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ChallengeAttemptRDDAO.class);

	public ChallengeAttemptRDDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}
	
	private static final int DEFAULT_CLAN_SIZE = 10;

	
	public void save(ChallengeAttemptRD challengeAttemptRD){
		save(challengeAttemptRD,"SYSTEM");
	}

	public void save(ChallengeAttemptRD challengeAttemptRD, String callingUserId) {
		try {
			if (challengeAttemptRD != null) {
				saveEntity(challengeAttemptRD, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create UserLeaderBoard: " + ex.getMessage());
		}
	}

	// TODO : category clarify
	public ChallengeAttemptRD getByUserId(Long userId, String challengeId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		ChallengeAttemptRD challengeAttemptRD = null;
		try {
			challengeAttemptRD = getByUserId(userId, session,challengeId);
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
			challengeAttemptRD = null;
		} finally {
			session.close();
		}
		return challengeAttemptRD;
	}

	public ChallengeAttemptRD getByUserId(Long userId, Session session, String challengeId) {
		ChallengeAttemptRD challengeAttemptRD = null;
		if (userId != null) {
			Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
			cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
			cr.add(Restrictions.eq(ConstantsGlobal.CHALLENGE_ID, challengeId));

			List<ChallengeAttemptRD> results = runQuery(session, cr, ChallengeAttemptRD.class);
			if (!CollectionUtils.isEmpty(results)) {
				if (results.size() > 1) {
					logger.info("Duplicate entries found for id : " + userId);// if
																				// hint
																				// taken
																				// will
																				// always
																				// be
																				// multiple//need
																				// to
																				// relook
				}
				challengeAttemptRD = results.get(0);
			} else {
				logger.info("getByUserId UserLeaderBoard : no UserLeaderBoard found " + userId);
			}
		}
		return challengeAttemptRD;
	}

	public void update(ChallengeAttemptRD p, Session session, String callingUserId) {
		if (p != null) {
			logger.info("update UserLeaderBoard: " + p.toString());
			updateEntity(p, session, callingUserId);
		}
	}

	public List<ChallengeAttemptRD> getChallengesTakenByUserId(Long userId, List<String> fields, Integer start,
			Integer size, String sortOrder, String orderBy) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ChallengeAttemptRD> results = new ArrayList<>();
		Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
		try {
			cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
			if (StringUtils.isNotEmpty(orderBy) && StringUtils.isNotEmpty(sortOrder)) {
				SortOrder sortOrder1 = SortOrder.valueQuitentOf(sortOrder);
				if(orderBy.equals("lastUpdated")){
					orderBy = "lastUpdatedTime";
				}
				if (Sort.Direction.ASC.name().equals(sortOrder1.name())) {
					cr.addOrder(Order.asc(orderBy));
				} else {
					cr.addOrder(Order.desc(orderBy));
				}
			} else {
				cr.addOrder(Order.desc(ConstantsGlobal.CREATION_TIME));
			}
			if (start == null) {
				start = 0;
			}
			if (size == null) {
				size = 20;
			}
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			results = runQuery(session, cr, ChallengeAttemptRD.class);
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
		} finally {
			session.close();
		}
		return results;
	}

	public List<ChallengeAttemptRD> getChallengeLeaderBoard(String challengeId, Integer start, Integer size) {
		List<ChallengeAttemptRD> leaderBoards = new ArrayList<>();
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CHALLENGE_ID, challengeId));
			cr.addOrder(Order.desc(ConstantsGlobal.TOTAL_POINTS)).addOrder(Order.asc(ConstantsGlobal.TIME_TAKEN));
			if (start == null) {
				start = 0;
			}
			if (size == null) {
				size = 20;
			}
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			List<ChallengeAttemptRD> results = runQuery(session, cr, ChallengeAttemptRD.class);
			return results;
		} catch (Exception e) {
			logger.error("getUserLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return leaderBoards;
	}

	public Long getMyRank(ChallengeAttemptRD attempt) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Long rank = null;
		try {
			Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CHALLENGE_ID, attempt.getChallengeId()));

			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.gt(ConstantsGlobal.TOTAL_POINTS, attempt.getTotalPoints()));
			disjunction.add(Restrictions.and(Restrictions.eq(ConstantsGlobal.TOTAL_POINTS, attempt.getTotalPoints()),
					Restrictions.lt(ConstantsGlobal.TIME_TAKEN, attempt.getTimeTaken())));
			cr.add(disjunction);

			rank = getCount(session, cr, true);
		} catch (Exception e) {
			logger.error("getUserLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return rank;
	}

	public Long getChallengeAttemptCount(String challengeId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Long count = null;
		try {
			Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CHALLENGE_ID, challengeId));
			count = getCount(session, cr, false);
		} catch (Exception e) {
			logger.error("getUserLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return count;

	}

	public Long getChallengesTakenByUserIdCount(Long userId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Long count = null;
		try {
			Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
			cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
			count = getCount(session, cr, false);
		} catch (Exception e) {
			logger.error("getUserLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return count;
	}

	public List<ChallengeAttemptRD> getChallengeTakensByChallengeId(String challengeId, int start, int size) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ChallengeAttemptRD> results = null;
		Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
		try {
			cr.add(Restrictions.eq(ConstantsGlobal.CHALLENGE_ID, challengeId));
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			results = runQuery(session, cr, ChallengeAttemptRD.class);
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
		} finally {
			session.close();
		}
		return results;
	}
	
    public List<ChallengeAttemptRD> getChallengeAttempts(Set<String> challengeIds, Long userId) {
    	SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ChallengeAttemptRD> results = null;
		Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
		try {
			cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
			cr.add(Restrictions.in(ConstantsGlobal.CHALLENGE_ID, challengeIds));

			results = runQuery(session, cr, ChallengeAttemptRD.class);
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
		} finally {
			session.close();
		}
		return results;
    }
    
    public List<ChallengeAttemptsCount> getAttemptsCount(Set<String> challengeIds) {
    	SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ChallengeAttemptsCount> counts = new ArrayList<>();
		Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
		try {
			
			cr.add(Restrictions.in(ConstantsGlobal.CHALLENGE_ID, challengeIds));
			cr.setProjection(Projections.projectionList()					
					.add(Projections.groupProperty(ConstantsGlobal.CHALLENGE_ID),"challengeId")
					.add(Projections.rowCount(),"attempts"));
//			cr.setResultTransformer(Transformers.aliasToBean(ChallengeAttempt.class));

			List results = runQuery(session, cr, ChallengeAttemptRD.class);
			if (!results.isEmpty()) {
				logger.info("result size : " +results.size() );
				for(int i = 0; i < results.size();i++){
					Object[] temp = (Object[]) results.get(i);
					ChallengeAttemptsCount countAtt = new ChallengeAttemptsCount();
					countAtt.setChallengeId((String) temp[0]);
					Long count = (Long) temp[1];
					if(count != null){
						countAtt.setAttempts(count.intValue());
					}
					logger.info("challengeAttempt id  : " +countAtt.getChallengeId() + " count : "+ countAtt.getAttempts());
					counts.add(countAtt);
				}				
			}
			
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
		} finally {
			session.close();
		}
		return counts;
    }

    //TODO:END of day
    public List<ChallengeUserPoints> getDailyScoreUsers(String category,Long startTime, Long endTime) {
    	SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ChallengeUserPoints> counts = new ArrayList<>();
		Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
		try {
			
			cr.add(Restrictions.in(ConstantsGlobal.EVENT_CATEGORY, category));
			cr.add(Restrictions.gt(ConstantsGlobal.CREATION_TIME, startTime));
			cr.add(Restrictions.lt(ConstantsGlobal.CREATION_TIME, endTime));
			cr.setProjection(Projections.projectionList()					
					.add(Projections.groupProperty(ConstantsGlobal.USER_ID),"userId")
					.add(Projections.sum(ConstantsGlobal.TOTAL_POINTS),"points"));
			cr.setResultTransformer(Transformers.aliasToBean(ChallengeUserPoints.class));

			counts = cr.list();
//			List results = runQuery(session, cr, ChallengeAttemptRD.class);
//			if (!results.isEmpty()) {
//				logger.info("result size : " +results.size() );
//				for(int i = 0; i < results.size();i++){
//					Object[] temp = (Object[]) results.get(i);
//					ChallengeAttemptsCount countAtt = new ChallengeAttemptsCount();
//					countAtt.setChallengeId((String) temp[0]);
//					Long count = (Long) temp[1];
//					if(count != null){
//						countAtt.setAttempts(count.intValue());
//					}
//					logger.info("challengeAttempt id  : " +countAtt.getChallengeId() + " count : "+ countAtt.getAttempts());
//					counts.add(countAtt);
//				}				
//			}
//			Projections.distinct(Projections.projectionList()
//				    .add(Projections.property("type"), "type") )
			
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
		} finally {
			session.close();
		}
    	return counts;
    }
    public List<AttemptedUserIds> getUsersWhoAttemptedChallenge(){
    	SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<Long> counts = new ArrayList<>();
		Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
		try {

			cr.setProjection(Projections.distinct(Projections.projectionList()					
					.add(Projections.groupProperty(ConstantsGlobal.USER_ID),"attemptedUserIds")
					));
		//	cr.setResultTransformer(Transformers.aliasToBean(Long.class));

			counts = (List<Long>)cr.list();
//			List results = runQuery(session, cr, ChallengeAttemptRD.class);
//			if (!results.isEmpty()) {
//				logger.info("result size : " +results.size() );
//				for(int i = 0; i < results.size();i++){
//					Object[] temp = (Object[]) results.get(i);
//					ChallengeAttemptsCount countAtt = new ChallengeAttemptsCount();
//					countAtt.setChallengeId((String) temp[0]);
//					Long count = (Long) temp[1];
//					if(count != null){
//						countAtt.setAttempts(count.intValue());
//					}
//					logger.info("challengeAttempt id  : " +countAtt.getChallengeId() + " count : "+ countAtt.getAttempts());
//					counts.add(countAtt);
//				}				
//			}
//			Projections.distinct(Projections.projectionList()
//				    .add(Projections.property("type"), "type") )
			
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
		} finally {
			session.close();
		}
		AttemptedUserIds resp = new AttemptedUserIds();
		if(ArrayUtils.isNotEmpty(counts)){
			resp.setAttemptedUserIds(new HashSet<>(counts));
		}
    	return Arrays.asList(resp);
    }
    
    public int getPointsForClan(String clanId,String challengeId){
    	SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		int points = 0;
		List<ChallengeAttemptRD> results = null;
		DetachedCriteria dcrit = DetachedCriteria.forClass(ClanMember.class)
				.add(Restrictions.eq("clanId", clanId))
				.setProjection(Projections.property(ConstantsGlobal.USER_ID));
		Criteria cr = session.createCriteria(ChallengeAttemptRD.class);
		try {		
			cr.add(Property.forName(ConstantsGlobal.USER_ID).in(dcrit));
			//cr.add(Restrictions.in(ConstantsGlobal.USER_ID, dcrit));
			cr.add(Restrictions.eq(ConstantsGlobal.CHALLENGE_ID, challengeId));
			cr.add(Restrictions.eq(ConstantsGlobal.SUCCESS, true));
			cr.addOrder(Order.desc(ConstantsGlobal.TOTAL_POINTS));
			cr.setFirstResult(0);
			cr.setMaxResults(DEFAULT_CLAN_SIZE);

			results = runQuery(session, cr, ChallengeAttemptRD.class);
			
			if(ArrayUtils.isNotEmpty(results)){
				logger.info("getPointsForClan Result size : "+ results.size());
				for(ChallengeAttemptRD attempt : results){
					points+=attempt.totalPoints;
					logger.info("getPointsForClan attempt points - "+attempt.totalPoints+" userId : "+attempt.getUserId());
				}
			}
		} catch (Exception ex) {
			logger.error("getPointsForClan getPointsForClan: " + ex.getMessage());
		} finally {
			session.close();
		}
		return points;
    }
    
	public Long getCount(Session session, Criteria cr, boolean rank) {
		Long count = null;
		cr.setProjection(Projections.rowCount());
		List results = runQuery(session, cr, ChallengeAttemptRD.class);
		if (!results.isEmpty()) {
			count = (Long) results.get(0);
			if (rank) {
				count++;
			}
		}
		return count;
	}
}
