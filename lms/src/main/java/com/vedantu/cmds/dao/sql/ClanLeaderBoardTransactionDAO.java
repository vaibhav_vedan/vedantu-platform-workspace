package com.vedantu.cmds.dao.sql;

import com.vedantu.cmds.entities.challenges.ClanLeaderBoardTransaction;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractSqlDAO;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class ClanLeaderBoardTransactionDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ClanLeaderBoardTransactionDAO.class);
    
    private static int MAX_RESULTS = 10;

    public ClanLeaderBoardTransactionDAO() {
        super();
    }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void updateAll(List<ClanLeaderBoardTransaction> p, Session session) {
        updateAll(p, session, null);
    }

    public void updateAll(List<ClanLeaderBoardTransaction> p, Session session, String callingUserId) {
        try {
            if (p != null) {
                updateAllEntities(p, session, callingUserId);
            }
        } catch (Exception ex) {
        	//TODO : Handle Exception
            logger.error("updateAll ClanLeaderBoardTransaction: " + ex.getMessage());
        }
    }	
}
