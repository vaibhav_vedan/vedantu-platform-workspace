package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.CMDSTestList;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;
import com.vedantu.cmds.request.GetCMDSTestListReq;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

@Service
public class CMDSTestListDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSTestListDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public CMDSTestListDAO() {
        super();
    }

    public void update(CMDSTestList testList) {
        logger.info("Request : " + testList.toString());
        try {
            Assert.notNull(testList);
            saveEntity(testList);
            logger.info("Updated id : " + testList.getId());
        } catch (Exception ex) {
            throw new RuntimeException("CMDSTestUpdateError : Error updating the CMDS Test List " + testList.toString(), ex);
        }
    }

    public List<CMDSTestList> getTestList(GetCMDSTestListReq req) {
        Query query = new Query();

        if (ArrayUtils.isNotEmpty(req.getMainTags())) {
            query.addCriteria(Criteria.where(CMDSTestList.Constants.MAIN_TAGS).all(req.getMainTags()));
        }

        if (req.getPublished() != null) {
            query.addCriteria(Criteria.where(CMDSTestList.Constants.PUBLISHED).is(req.getPublished()));
        }

//        query.addCriteria(Criteria.where(CMDSTestList.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        if (req.getStart() != null) {
            query.skip(req.getStart());
        }

        if (req.getSize() != null && req.getSize() > 0) {
            query.limit(req.getSize());
        }
        query.with(Sort.by(Sort.Direction.DESC, CMDSTestList.Constants.CREATION_TIME));

        logger.info("query: " + query);
        return runQuery(query, CMDSTestList.class);

    }

}
