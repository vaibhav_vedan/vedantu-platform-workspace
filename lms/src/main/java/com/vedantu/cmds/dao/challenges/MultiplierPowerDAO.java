

package com.vedantu.cmds.dao.challenges;

import com.vedantu.cmds.entities.challenges.MultiplierPower;
import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.data.domain.Sort;

@Service
public class MultiplierPowerDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MultiplierPowerDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public MultiplierPowerDAO() {
        super();
    }

    public void save(MultiplierPower multiplierPower) {
        try {
            saveEntity(multiplierPower);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ContentInfoUpdateError : Error updating the content info " + multiplierPower, ex);
        }
    }

    public MultiplierPower getByUserIdAndPowerType(Long userId, MultiplierPowerType powerType) {

        Query query = new Query();
        query.addCriteria(Criteria.where(MultiplierPower.Constants.USERID).is(userId));
        query.addCriteria(Criteria.where(MultiplierPower.Constants.TYPE).is(powerType));
        //TODO is the logic correct to use only the one which created last
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.CREATION_TIME));
        setFetchParameters(query, 0, 1);
        List<MultiplierPower> results = runQuery(query, MultiplierPower.class);
        if (ArrayUtils.isNotEmpty(results)) {
            return results.get(0);
        } else {
            return null;
        }
    }
}

