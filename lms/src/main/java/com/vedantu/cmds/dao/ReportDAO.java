package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.*;
import com.vedantu.cmds.request.GetReportsReq;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class ReportDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ReportDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public ReportDAO() {
        super();
    }

    public void update(Report report) {
        logger.info("Request : " + report.toString());
        try {
            Assert.notNull(report);
            saveEntity(report);
            logger.info("Updated id : " + report.getId());
        } catch (Exception ex) {
            
           
            logger.info("Exception in saving: "+ex.getMessage());
            throw new RuntimeException("CMDSTestUpdateError : Error updating the CMDS Video " + report.toString(), ex);
        }
    }
    public List<Report> getReports(GetReportsReq req) {

        Query query = new Query();

        if(req.getStatus() != null ) {
            query.addCriteria( Criteria.where(Report.Constants.STATUS).is(req.getStatus()));

        }


        if(req.getMainTags() != null ) {
            query.addCriteria( Criteria.where(Report.Constants.MAIN_TAGS).in(req.getMainTags()));
        }

        if(Objects.nonNull(req.getStartTime()) && Objects.nonNull(req.getEndTime()))
            query.addCriteria(Criteria.where(Report.Constants.CREATION_TIME).gte(req.getStartTime()).lte(req.getEndTime()));
        else if(Objects.nonNull(req.getEndTime()))
            query.addCriteria(Criteria.where(Report.Constants.CREATION_TIME).lte(req.getEndTime()));
        else if(Objects.nonNull(req.getStartTime()))
            query.addCriteria(Criteria.where(Report.Constants.CREATION_TIME).gte(req.getStartTime()));

        if(Objects.nonNull(req.getQuestionId()))
            query.addCriteria(Criteria.where(Report.Constants.ENTITYID).is(req.getQuestionId()));

        if( !StringUtils.isEmpty(req.getSortBy()) ) {
            if (req.getSortBy().equals("RAISED_BY")) {
                query.with(Sort.by(Sort.Direction.DESC, Report.Constants.COUNT));
            } else {
                query.with(Sort.by(Sort.Direction.DESC, Report.Constants.CREATION_TIME));
            }
        }

        setFetchParameters(query, req);
        logger.info("query {}",query);
        return runQuery(query, Report.class);
    }


    public List<Report> getReportsForCsv(GetReportsReq req) {

        Query query = new Query();
        if(req.getStatus() != null ) {
            query.addCriteria( Criteria.where(Report.Constants.STATUS).is(req.getStatus()));

        }
        if(req.getMainTags() != null ) {
            query.addCriteria( Criteria.where(Report.Constants.MAIN_TAGS).in(req.getMainTags()));

        }
        if(req.getStartTime() != null && req.getEndTime() != null ) {
            query.addCriteria( Criteria.where(Report.Constants.USERREPORTS_RAISEDON).gt(req.getStartTime()).lt(req.getEndTime()));

            //query.addCriteria( Criteria.where(Report.Constants.USERREPORTS_RAISEDON).lt(req.getEndTime()));

        }

        if(Objects.nonNull(req.getStartTime()) && Objects.nonNull(req.getEndTime()))
            query.addCriteria(Criteria.where(Report.Constants.CREATION_TIME).gte(req.getStartTime()).lte(req.getEndTime()));
        else if(Objects.nonNull(req.getEndTime()))
            query.addCriteria(Criteria.where(Report.Constants.CREATION_TIME).lte(req.getEndTime()));
        else if(Objects.nonNull(req.getStartTime()))
            query.addCriteria(Criteria.where(Report.Constants.CREATION_TIME).gte(req.getStartTime()));

        if(Objects.nonNull(req.getQuestionId()))
            query.addCriteria(Criteria.where(Report.Constants.ENTITYID).is(req.getQuestionId()));

        //    setFetchParameters(query, req);
        return runQuery(query, Report.class);
    }
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    

}
