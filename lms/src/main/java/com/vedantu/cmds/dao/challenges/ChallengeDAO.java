/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao.challenges;

import java.util.List;

import com.vedantu.cmds.enums.Difficulty;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.challenges.ChallengeType;
import com.vedantu.cmds.pojo.challenges.ActiveChallengeIds;
import com.vedantu.cmds.pojo.challenges.ChallengeStatusAggResult;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SortOrder;
import java.util.Set;
import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Update;

@Service
public class ChallengeDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ChallengeDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public ChallengeDAO() {
        super();
    }

    public void saveChallenge(Challenge challenge) {
        try {
            saveEntity(challenge);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ContentInfoUpdateError : Error updating the content info " + challenge, ex);
        }
    }

    public Challenge getById(String id) {
        Challenge challenge = null;
        try {
            challenge = (Challenge) getEntityById(id, Challenge.class);
        } catch (Exception ex) {
            throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
        }
        return challenge;
    }

    public List<Challenge> getByIds(List<String> challengeIds, VedantuRecordState recordState) {
        Query query = new Query();
        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.ID).in(challengeIds));
        if (recordState != null) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.RECORD_STATE).is(recordState));
        }

        List<Challenge> results = runQuery(query, Challenge.class);
        return results;
    }

    public void delete(String id) {
        deleteEntityById(id, Challenge.class);
    }

    public List<Challenge> getChallenges(Integer start, Integer size,
            ChallengeStatus status, List<String> brdIds, String sortOrder,
            String orderBy, ChallengeType type, Difficulty difficulty,
            Set<String> takenChallengeIds,
            QuestionType qType,
            String category) {

        Query query = new Query();
        if (status != null) {
            query.addCriteria(Criteria.where(Challenge.Constants.STATUS).is(status));
        }

        if (type != null) {
            query.addCriteria(Criteria.where(Challenge.Constants.TYPE).is(type));
        }
        if (qType != null) {
            query.addCriteria(Criteria.where(Challenge.Constants.QUESTION_TYPES).in(qType));
        }
        if (difficulty != null) {
            query.addCriteria(Criteria.where(Challenge.Constants.DIFFICULTY).is(difficulty));
        }

        if (ArrayUtils.isNotEmpty(brdIds)) {
            query.addCriteria(Criteria.where(Challenge.Constants.BOARD_IDS).in(brdIds));
        }

        if (ArrayUtils.isNotEmpty(takenChallengeIds)) {
            query.addCriteria(Criteria.where(AbstractMongoEntity.Constants._ID).in(takenChallengeIds));
        }

        if (StringUtils.isNotEmpty(category)) {
            query.addCriteria(Criteria.where(Challenge.Constants.CATEGORY).is(category));
        }

        query.addCriteria(Criteria.where(Challenge.Constants.RECORD_STATE).ne(VedantuRecordState.DELETED));
        
        setFetchParameters(query, start, size);

        if (StringUtils.isNotEmpty(orderBy) && StringUtils.isNotEmpty(sortOrder)) {
            SortOrder sortOrder1 = SortOrder.valueQuitentOf(sortOrder);
            if (Sort.Direction.ASC.name().equals(sortOrder1.name())) {
                query.with(Sort.by(Sort.Direction.ASC, orderBy));
            } else {
                query.with(Sort.by(Sort.Direction.DESC, orderBy));
            }
        } else {
            query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.LAST_UPDATED));
        }

        List<Challenge> results = runQuery(query, Challenge.class);
        return results;
    }

    public List<Challenge> getBurstableChallenges() {
        Query query = new Query();
        query.addCriteria(Criteria.where(Challenge.Constants.STATUS).is(ChallengeStatus.ACTIVE));
        query.addCriteria(Criteria.where(Challenge.Constants.BURST_TIME).lt(System.currentTimeMillis()));
        setFetchParameters(query, 0, 1000);
        List<Challenge> results = runQuery(query, Challenge.class);
        if (ArrayUtils.isNotEmpty(results) && results.size() >= 500) {
            logger.error("Too many burstable challenges, change logic");
        }
        return results;
    }

    public void markChallengesActive() {
        Query query = new Query();
        query.addCriteria(Criteria.where(Challenge.Constants.STATUS).is(ChallengeStatus.DRAFT));
        query.addCriteria(Criteria.where(Challenge.Constants.START_TIME).lt(System.currentTimeMillis()));
        Update update = new Update();
        update.set(Challenge.Constants.STATUS, ChallengeStatus.ACTIVE);
        updateMulti(query, update, Challenge.class);
    }
    
    public void deleteChallenge(String challengeId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Challenge.Constants._ID).is(challengeId));
        Update update = new Update();
        update.set(Challenge.Constants.RECORD_STATE, VedantuRecordState.DELETED);
        updateMulti(query, update, Challenge.class);
    }    

    public List<ChallengeStatusAggResult> getCounts() {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(
                group("status").count().as("count").sum("maxPoints").as("points"),
                project("count", "points").and("status").previousOperation()).withOptions(aggregationOptions);
        AggregationResults<ChallengeStatusAggResult> groupResults
                = getMongoOperations().aggregate(agg, Challenge.class.getSimpleName(), ChallengeStatusAggResult.class);
        List<ChallengeStatusAggResult> result = groupResults.getMappedResults();
        return result;
    }
    
    public List<ActiveChallengeIds> getActiveSingleChallenges() {
    	Criteria criteria = new Criteria();
    	criteria = Criteria.where(Challenge.Constants.STATUS).is(ChallengeStatus.ACTIVE.name());
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(Aggregation.match(criteria),
        		Aggregation.sort(Sort.Direction.DESC, "startTime"),
                group(Challenge.Constants.CATEGORY).first(Aggregation.ROOT).as("challenge")).withOptions(aggregationOptions);
        AggregationResults<ActiveChallengeIds> groupResults
                = getMongoOperations().aggregate(agg, Challenge.class.getSimpleName(), ActiveChallengeIds.class);
        List<ActiveChallengeIds> result = groupResults.getMappedResults();
        return result;
    }
    
    public List<Challenge> getChallengesByQuestionId(String questionId){
        Query query = new Query();
        
        query.addCriteria(Criteria.where("questions.qid").is(questionId));
        
        return runQuery(query, Challenge.class);
                
    }

}
