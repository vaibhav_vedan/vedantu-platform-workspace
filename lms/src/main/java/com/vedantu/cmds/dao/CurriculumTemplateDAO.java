package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.CurriculumTemplate;
import com.vedantu.cmds.pojo.CourseDetails;
import com.vedantu.cmds.request.CurriculumTemplateRetrievalFilterRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class CurriculumTemplateDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(CurriculumTemplateDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public CurriculumTemplateDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void saveEntity(CurriculumTemplate curriculumTemplate){
        logger.info("curriculumTemplate to be saved is : "+curriculumTemplate);
        if(curriculumTemplate !=null){
            super.saveEntity(curriculumTemplate);
        }
        logger.info("curriculumTemplate after saving is : "+curriculumTemplate);
    }

    public void insertAll(List<CurriculumTemplate> curriculumTemplateList){
        if(ArrayUtils.isNotEmpty(curriculumTemplateList)){
            insertAllEntities(curriculumTemplateList, CurriculumTemplate.class.getSimpleName());
        }
    }

    public List<CurriculumTemplate> getFilteredCurriculumTemplates(CurriculumTemplateRetrievalFilterRequest request){
        Query query;

        // Check first hand if someone has entered spaces inside the title tab
        if(request.getTitle()!=null){
            request.setTitle(request.getTitle().trim());
        }

        // Add criteria for text indexing in case values are present in title field
        if(StringUtils.isNotEmpty(request.getTitle())){
            TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matchingAny(request.getTitle());
            textCriteria.caseSensitive(false);
            textCriteria.diacriticSensitive(false);
            query = TextQuery.queryText(textCriteria).sortByScore();
        }else{
            query=new Query();
        }

        // Criteria for filtering according to grades
        if(StringUtils.isNotEmpty(request.getGrade())){
            query.addCriteria(Criteria.where(CurriculumTemplate.Constants.GRADES).is(request.getGrade()));
        }

        // Criteria for filtering according to targets
        if(StringUtils.isNotEmpty(request.getTarget())){
            query.addCriteria(Criteria.where(CurriculumTemplate.Constants.TARGETS).is(request.getTarget()));
        }

        // Criteria for filtering according to year
        if(request.getYear()!=null){
            query.addCriteria(Criteria.where(CurriculumTemplate.Constants.YEAR).is(request.getYear()));
        }

        // Criteria for filtering according to course or batch ids
        if(StringUtils.isNotEmpty(request.getCourseOrBatchId())){
            Criteria orCriteria=new Criteria().orOperator(
                    Criteria.where(CurriculumTemplate.Constants.COURSE_ID).is(request.getCourseOrBatchId()),
                    Criteria.where(CurriculumTemplate.Constants.BATCH_IDS).is(request.getCourseOrBatchId())
            );
            query.addCriteria(orCriteria);
        }


        query.addCriteria(Criteria.where(CurriculumTemplate.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.addCriteria(Criteria.where(CurriculumTemplate.Constants.PARENT_ID).is(null));

        query.skip(request.getStart());
        query.limit(request.getSize());

        logger.info("Query to get filtered curriculum template is "+query);
        return runQuery(query,CurriculumTemplate.class);
    }

    public List<CurriculumTemplate> getCurriculumTemplateForCourse(String courseId) throws BadRequestException {

        if(StringUtils.isEmpty(courseId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"courseId cann't be null or empty");
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(CurriculumTemplate.Constants.COURSE_ID).is(courseId));
        query.addCriteria(Criteria.where(CurriculumTemplate.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        return runQuery(query,CurriculumTemplate.class);
    }


    public CurriculumTemplate getEntityById(String parentId) throws BadRequestException {
        if(StringUtils.isEmpty(parentId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"courseId cann't be null or empty");
        }
        return getEntityById(parentId,CurriculumTemplate.class);
    }

    public List<CurriculumTemplate> getCurriculumTemplateBasicDetailsByRootId(String rootId) throws BadRequestException {

        if(StringUtils.isEmpty(rootId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"courseId cann't be null or empty");
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(CurriculumTemplate.Constants.ROOT_ID).is(rootId));

        query.fields().include(CurriculumTemplate.Constants.TITLE);
        query.fields().include(CurriculumTemplate.Constants.PARENT_ID);
        query.fields().include(CurriculumTemplate.Constants.CHILD_ORDER);
        query.fields().include(CurriculumTemplate.Constants.MAIN_TAGS);
        query.fields().include(CurriculumTemplate.Constants.BOARD_ID);

        return runQuery(query,CurriculumTemplate.class);
    }

    public CurriculumTemplate getEntityByTitle(String title) throws BadRequestException {
        if(StringUtils.isEmpty(title)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"courseId cann't be null or empty");
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(CurriculumTemplate.Constants.TITLE).is(title));

        return findOne(query,CurriculumTemplate.class);
    }

    public Set<CourseDetails> getSharedCourseDetailsOfCurriculumTemplateById(String curriculumTemplateId) throws BadRequestException{
        if(StringUtils.isEmpty(curriculumTemplateId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"courseId cann't be null or empty");
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(CurriculumTemplate.Constants._ID).is(curriculumTemplateId));
        query.fields().include(CurriculumTemplate.Constants.SHARED_COURSE_DETAILS);
        query.fields().exclude(CurriculumTemplate.Constants._ID);

        CurriculumTemplate curriculumTemplate=findOne(query,CurriculumTemplate.class);

        if(curriculumTemplate==null){
            throw new BadRequestException(ErrorCode.CURRICULUM_TEMPLATE_NOT_FOUND,"No curriculum template found for id "+curriculumTemplateId);
        }

        return curriculumTemplate.getSharedCourseDetails();
    }

    public void updateSharedCourseDetailsForCurriculumTemplate(String curriculumTemplateId, Set<CourseDetails> sharedCourseDetails) throws BadRequestException {

        if(StringUtils.isEmpty(curriculumTemplateId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"courseId cann't be null or empty");
        }

        Query query=new Query();
        query.addCriteria(
                new Criteria().orOperator(
                        Criteria.where(CurriculumTemplate.Constants._ID).is(curriculumTemplateId),
                        Criteria.where(CurriculumTemplate.Constants.ROOT_ID).is(curriculumTemplateId)
                )
        );

        Update update=new Update();
        update.set(CurriculumTemplate.Constants.SHARED_COURSE_DETAILS,sharedCourseDetails);

        updateMulti(query,update,CurriculumTemplate.class);
    }
}
