package com.vedantu.cmds.dao.es;

import com.google.gson.Gson;
import com.vedantu.cmds.dao.CMDSQuestionDAO;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.elasticsearch.CMDSQuestionBasicInfo;
import com.vedantu.cmds.managers.ESManager;
import com.vedantu.cmds.pojo.CMDSQuestionBasicInfoESPojo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Repository
public class CMDSQuestionESDAO {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private ESManager esManager;

    @Autowired
    private CMDSQuestionDAO cmdsQuestionDAO;

    private static final String CMDS = "cmds";
    private static final String QUESTION = "CMDSQuestionBasicInfo";
    private static final Gson gson=new Gson();


    private Logger logger;


    @PostConstruct
    public void init() {
        logger = logFactory.getLogger(CMDSQuestionESDAO.class);
    }

    public void uploadCMDSQuestionDataToES(CMDSQuestion cmdsQuestion) {
        logger.info("uploadCMDSQuestionDataToES for question with question id {}",cmdsQuestion.getId());

        if(Objects.isNull(cmdsQuestion)){
            try {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Cann't send empty question to elasticsearch");
            } catch (BadRequestException e) {
                logger.error("Cann't send empty question to elasticsearch",e);
            }
        }

        IndexRequest indexRequest = new IndexRequest(CMDS, QUESTION, cmdsQuestion.getId());
        indexRequest.source(gson.toJson(new CMDSQuestionBasicInfo(cmdsQuestion)), XContentType.JSON);
        IndexResponse indexResponse = null;
        try {
            indexResponse = esManager.doIndexRequest(indexRequest);
        } catch (IOException e) {
            logger.error("Error in index creation",e);
        }
        if(Objects.nonNull(indexResponse)){
            if(StringUtils.isNotEmpty(indexResponse.getId())){
                final String id=indexResponse.getId();
                cmdsQuestionDAO.updateCMDSQuestionDataUploadedToES(id);
            }
        }
    }

    public List<CMDSQuestionBasicInfoESPojo> searchCMDSQuestionDataFromES(String id) {
        float deductionScore = 5f;
        return searchCMDSQuestionDataFromES(id,deductionScore);
    }
    public List<CMDSQuestionBasicInfoESPojo> searchCMDSQuestionDataFromES(String id,float deductionScore) {
        CMDSQuestion cmdsQuestion=cmdsQuestionDAO.getById(id);
        if(Objects.nonNull(cmdsQuestion)){
            logger.info("For question id {} cmdsQuestionFound {}",id,cmdsQuestion);

            BoolQueryBuilder boolQueryBuilder=new BoolQueryBuilder();

            QueryBuilder questionTextMatch= QueryBuilders.matchQuery(CMDSQuestionBasicInfo.Constants.QUESTION_TEXT, convertStringForInsertionIntoES(cmdsQuestion.getQuestionBody().getNewText()));
            boolQueryBuilder.must(questionTextMatch);

            QueryBuilder questionTypeMatch= QueryBuilders.matchQuery(CMDSQuestionBasicInfo.Constants.TYPE,cmdsQuestion.getType());
            boolQueryBuilder.must(questionTypeMatch);

            if(ArrayUtils.isNotEmpty(cmdsQuestion.getSolutionInfo().getOptionBody().getNewOptions())) {

                String optionsString =
                        cmdsQuestion
                                .getSolutionInfo()
                                .getOptionBody()
                                .getNewOptions()
                                .stream()
                                .map(this::convertStringForInsertionIntoES)
                                .collect(Collectors.toList())
                                .stream()
                                .reduce(String::concat)
                                .orElse("");

                QueryBuilder optionsTypeMatch=QueryBuilders.matchQuery(CMDSQuestionBasicInfo.Constants.OPTIONS,optionsString);
                boolQueryBuilder.should(optionsTypeMatch);
            }

            SearchSourceBuilder searchSourceBuilder=new SearchSourceBuilder();
            searchSourceBuilder.query(boolQueryBuilder);

            SearchRequest intermediateSearchRequest=new SearchRequest();
            intermediateSearchRequest.source(searchSourceBuilder);
            logger.info("The intermediate search request is: \n"+intermediateSearchRequest);

            SearchResponse intermediateSearchResponse = null;
            try {
                intermediateSearchResponse = esManager.doSearchRequest(intermediateSearchRequest);
            } catch (IOException e) {
                logger.info("Exception in getting intermediate search response: ",e);
                return null;
            }

            searchSourceBuilder.minScore(intermediateSearchResponse.getHits().getMaxScore()-deductionScore);
            searchSourceBuilder.size(100);

            SearchRequest searchRequest=new SearchRequest();
            searchRequest.source(searchSourceBuilder);

            SearchResponse searchResponse= null;
            try {
                searchResponse = esManager.doSearchRequest(searchRequest);
            } catch (IOException e) {
                logger.info("Exception in getting final search response: ",e);
                return null;
            }


            logger.info("The final search request is: \n"+intermediateSearchRequest);
            logger.info("The final search response is: \n"+searchResponse.toString());

            logger.info("Getting the exact required response. ");
            return Arrays.stream(searchResponse.getHits().getHits())
                    .map(Object::toString)
                    .peek(logger::info)
                    .map(this::convertToCMDSQuestionBasicInfoESPojo)
                    .collect(Collectors.toList());
        }
        return null;
    }

    private String convertStringForInsertionIntoES(String option) {
        return option.replaceAll("\\\\", "");
    }

    private CMDSQuestionBasicInfoESPojo convertToCMDSQuestionBasicInfoESPojo(String json){
        logger.info("Json string to convert "+json);
        return gson.fromJson(json,CMDSQuestionBasicInfoESPojo.class);
    }

    public void deleteQuestionInESByIds(Set<String> duplicateIds) {
        logger.info("Deleting duplicate elements in elasticsearch by ids {}",duplicateIds);
        duplicateIds
                .stream()
                .map(this::getDeleteRequestObject)
                .forEach(esManager::doDeleteRequest);
    }

    public DeleteRequest getDeleteRequestObject(String id){
        return new DeleteRequest(CMDS,QUESTION,id);
    }
}
