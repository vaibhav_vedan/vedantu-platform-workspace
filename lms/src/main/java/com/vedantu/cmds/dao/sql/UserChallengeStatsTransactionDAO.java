/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao.sql;

import com.vedantu.cmds.entities.challenges.UserChallengeStatsTransaction;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractSqlDAO;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class UserChallengeStatsTransactionDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserChallengeStatsTransactionDAO.class);
    
    private static int MAX_RESULTS = 10;

    public UserChallengeStatsTransactionDAO() {
        super();
    }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void updateAll(List<UserChallengeStatsTransaction> p, Session session) {
        updateAll(p, session, null);
    }

    public void updateAll(List<UserChallengeStatsTransaction> p, Session session, String callingUserId) {
        try {
            if (p != null) {
                updateAllEntities(p, session, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("updateAll UserChallengeStatsTransaction: " + ex.getMessage());
        }
    }	
}
