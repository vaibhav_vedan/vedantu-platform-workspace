package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.TeacherContentDashboardInfo;
import com.vedantu.cmds.pojo.TeacherContentDashboardIncrementInfo;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Stream;

@Repository
public class TeacherContentDashboardInfoDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    private Logger logger = LogFactory.getLogger(TeacherContentDashboardInfoDAO.class);

    public TeacherContentDashboardInfoDAO() {
        super();
    }

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void updateTeacherContentDashboardInfo(@Valid TeacherContentDashboardIncrementInfo info){
        Query query=new Query();
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.TEACHER_ID).is(info.getTeacherId()));
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.CONTENT_INFO_TYPE).is(info.getContentInfoType()));
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.CONTENT_STATE).is(info.getContentState()));

        TeacherContentDashboardInfo currentInfo=findOne(query,TeacherContentDashboardInfo.class);

        Update update=new Update();
        if(Objects.isNull(currentInfo)){
            update.set(TeacherContentDashboardInfo.Constants.CREATED_BY,"SYSTEM");
            update.set(TeacherContentDashboardInfo.Constants.CREATION_TIME,System.currentTimeMillis());
            update.set(TeacherContentDashboardInfo.Constants.ENTITY_STATE, EntityState.ACTIVE);
        }
        update.inc(TeacherContentDashboardInfo.Constants.COUNT,info.getIncrementCount());
        update.set(TeacherContentDashboardInfo.Constants.LAST_UPDATED,System.currentTimeMillis());
        update.set(TeacherContentDashboardInfo.Constants.LAST_UPDATED_BY,"SYSTEM");
        upsertEntity(query,update,TeacherContentDashboardInfo.class);
    }

    public long getTeacherContentCountExcludingSome(String teacherId, List<ContentState> contentStates) {
        Query query=new Query();
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.TEACHER_ID).is(teacherId));
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.CONTENT_STATE).nin(contentStates));
        query.fields().include(TeacherContentDashboardInfo.Constants.COUNT);
        query.fields().exclude(TeacherContentDashboardInfo.Constants._ID);

        return Optional.ofNullable(runQuery(query,TeacherContentDashboardInfo.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(TeacherContentDashboardInfo::getCount)
                        .reduce(Long::sum)
                        .orElse(0l);

    }

    public long getTeacherContentCount(String teacherId, ContentInfoType contentInfoType, ContentState contentState) {
        Query query=new Query();
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.TEACHER_ID).is(teacherId));
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.CONTENT_INFO_TYPE).is(contentInfoType));
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.CONTENT_STATE).is(contentState));
        query.fields().include(TeacherContentDashboardInfo.Constants.COUNT);
        query.fields().exclude(TeacherContentDashboardInfo.Constants._ID);

        return Optional.ofNullable(runQuery(query,TeacherContentDashboardInfo.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(TeacherContentDashboardInfo::getCount)
                        .reduce(Long::sum)
                        .orElse(0l);

    }

    public boolean contentExists(String teacherId) {
        Query query=new Query();
        query.addCriteria(Criteria.where(TeacherContentDashboardInfo.Constants.TEACHER_ID).is(teacherId));
        query.fields().include(TeacherContentDashboardInfo.Constants._ID);
        return Objects.nonNull(findOne(query,TeacherContentDashboardInfo.class));
    }
}
