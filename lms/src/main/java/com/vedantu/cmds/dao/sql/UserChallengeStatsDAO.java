/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao.sql;

import com.vedantu.cmds.constants.ConstantsGlobal;
import com.vedantu.cmds.entities.challenges.UserChallengeStats;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractSqlDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

/**
 *
 * @author ajith
 */
@Service
public class UserChallengeStatsDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserChallengeStatsDAO.class);

    public UserChallengeStatsDAO() {
        super();
    }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void create(UserChallengeStats userChallengeStats, String callingUserId) {
        try {
            if (userChallengeStats != null) {
                saveEntity(userChallengeStats, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("Create userChallengeStats: " + ex.getMessage());
        }
    }

    public UserChallengeStats getByUserId(Long userId) {
        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        UserChallengeStats userChallengeStats = null;
        try {
            userChallengeStats = getByUserId(userId, session);
        } catch (Exception ex) {
            logger.error("getByUserId userChallengeStats: " + ex.getMessage());
            userChallengeStats = null;
        } finally {
            session.close();
        }
        return userChallengeStats;
    }

    public UserChallengeStats getByUserId(Long userId, Session session) {
        UserChallengeStats userChallengeStats = null;
        if (userId != null) {
            Criteria cr = session.createCriteria(UserChallengeStats.class);
            cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
            cr.setFetchMode("hintsCountMap", FetchMode.JOIN);
            List<UserChallengeStats> results = runQuery(session, cr, UserChallengeStats.class);
            if (!CollectionUtils.isEmpty(results)) {
                if (results.size() > 1) {
                    logger.error("Duplicate entries found for id : " + userId);
                }
                userChallengeStats = results.get(0);
//                logger.info("map ??? " + userChallengeStats.getHintsCountMap());
//                userChallengeStats.setHintsCountMap(userChallengeStats.getHintsCountMap());
            } else {
                logger.info("getByUserId UserChallengeStats : no UserChallengeStats found " + userId);
            }
        }
        return userChallengeStats;
    }

    public void update(UserChallengeStats p, Session session, String callingUserId) {
        if (p != null) {
            logger.info("update UserChallengeStats: " + p.toString());
            updateEntity(p, session, callingUserId);
        }
    }

}
