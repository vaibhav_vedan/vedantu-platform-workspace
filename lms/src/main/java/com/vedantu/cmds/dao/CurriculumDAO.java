package com.vedantu.cmds.dao;

import java.util.*;

import com.google.gson.Gson;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.lms.cmds.enums.CurriculumStatus;
import com.vedantu.lms.cmds.enums.ShareStatus;
import com.vedantu.lms.cmds.enums.ShareType;
import com.vedantu.lms.cmds.pojo.ContentInfo;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.entities.Curriculum;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class CurriculumDAO extends AbstractMongoDAO {

    private static final Gson gson=new Gson();

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(CurriculumDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public CurriculumDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(Curriculum p, Long callingUserId) {

        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db for id for curriculum" + p.getId());
            if(ArrayUtils.isNotEmpty(p.getTopicTags())){
                try {
                    Set<String> keys=p.getTopicTags()
                            .stream()
                            .map(key->"BaseTopicTree:"+key)
                            .collect(Collectors.toSet());
                    Map<String, String> topicNameDetailsMap = redisDAO.getValuesForKeys(keys);
                    logger.info("`topicTag` value received from redis is "+topicNameDetailsMap);
                    if(topicNameDetailsMap.size()!=keys.size()){
                        Set<String> redisKeys=topicNameDetailsMap.keySet();
                        keys.removeAll(redisKeys);
                        throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID,"Some of the topic tags have invalid content ids: "+keys);
                    }
                } catch (InternalServerErrorException e) {
                    logger.error("CurriculumDAO save - InternalServerErrorException: ",e);
                } catch (BadRequestException e) {
                    logger.error("CurriculumDAO save - BadRequestException: ",e);
                }
            }
            saveEntity(p, callingUserIdString);
        }
    }

    public Curriculum getCurriculumById(String id) {
        return getEntityById(id, Curriculum.class);
    }
    public List<Curriculum> getCurriculumByIds(List<String> ids) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.ID).in(ids));
        return runQuery(query, Curriculum.class);
    }

    public List<Curriculum> getCurriculumByRootId(String rootId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.ROOT_ID).is(rootId));
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return runQuery(query, Curriculum.class);
    }

    public List<Curriculum> getCurriculumByParentId(String parentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.PARENT_ID).is(parentId));
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return runQuery(query, Curriculum.class);
    }

    public List<Curriculum> getCurriculumByCourseId(CurriculumEntityName name, String id) {
        return getCurriculumByCourseId(name,id,null,null);
    }
    public List<Curriculum> getCurriculumByCourseId(CurriculumEntityName name, String id, List<String> includeFields, List<String> excludeFields) {
        List<Curriculum> curriculums = new ArrayList<>();
        int start = 0;
        int limit = 400;
        while (true) {
            try {
                Query query = new Query();
                query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(name));
                query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).is(id));
                query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
                if(ArrayUtils.isNotEmpty(includeFields)){
                    includeFields.stream().forEach(field->query.fields().include(field));
                }
                if(ArrayUtils.isNotEmpty(excludeFields)){
                    excludeFields.stream().forEach(field->query.fields().exclude(field));
                }
                query.with(Sort.by(Direction.ASC, Curriculum.Constants.CREATION_TIME));
                setFetchParameters(query, start, limit);
                List<Curriculum> tempCurriculum = runQuery(query, Curriculum.class);
                start += limit;
                if (ArrayUtils.isEmpty(tempCurriculum)) {
                    break;
                }
                curriculums.addAll(tempCurriculum);
            } catch (Exception e) {
                logger.error("Error in fetching Curriculum from DB " + e);
                break;
            }
        }
        return curriculums;
    }

    public List<Curriculum> getCurriculumByBatchIds(List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(CurriculumEntityName.OTF_BATCH));
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(batchIds));

        return runQuery(query, Curriculum.class);
    }

    public List<Curriculum> getCurriculumByBatchIds(final List<String> batchIds, final List<String> includeFields) {
        final Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(CurriculumEntityName.OTF_BATCH));
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(batchIds));
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        Optional.ofNullable(includeFields).orElseGet(ArrayList::new).forEach(query.fields()::include);
        return runQuery(query, Curriculum.class);
    }


    public List<Curriculum> getCurriculumByBatchIds(Collection<String> parentIds, String[] includeFields, String[] excludeFields) {
        List<Curriculum> curriculums = new ArrayList<>();
        int start = 0;
        int limit = 400;
        while (true) {
            try {
                Query query = new Query();
                query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(CurriculumEntityName.OTF_BATCH));
                query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(parentIds));
                query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

                if (includeFields != null && includeFields.length > 0) {
                    Field fields = query.fields();
                    for (String field : includeFields) {
                        fields.include(field);
                    }
                }
                if (excludeFields != null && excludeFields.length > 0) {
                    Field fields = query.fields();
                    for (String field : excludeFields) {
                        fields.exclude(field);
                    }
                }

                query.with(Sort.by(Direction.ASC, Curriculum.Constants.CREATION_TIME));
                setFetchParameters(query, start, limit);

                List<Curriculum> tempCurriculum = runQuery(query, Curriculum.class);
                start += limit;
                if (ArrayUtils.isEmpty(tempCurriculum)) {
                    break;
                }
                curriculums.addAll(tempCurriculum);
            } catch (Exception e) {
                logger.error("Error in fetching Curriculum from DB " + e);
                break;
            }
        }
        return curriculums;

    }

    public List<Curriculum> getCurriculumBySessionIds(List<String> sessionIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(CurriculumEntityName.OTF_BATCH));
        query.addCriteria(Criteria.where("contents.sessionIds").in(sessionIds));
        //query.addCriteria(Criteria.where(Curriculum.Constants.STATUS).ne(CurriculumStatus.PENDING));
        return runQuery(query, Curriculum.class);

    }
    
    public List<Curriculum> getCurriculumBySessionId(CurriculumEntityName name, List<String> contextId, String sessionId, Long afterLastUpdated, Long beforeLastUpdated) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(name));
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(contextId));
        if(StringUtils.isNotEmpty(sessionId)){
            query.addCriteria(Criteria.where(Curriculum.Constants.SESSION_IDS).is(sessionId));
        }
        if(afterLastUpdated != null && beforeLastUpdated != null){
            query.addCriteria(Criteria.where(Curriculum.Constants.LAST_UPDATED).gte(afterLastUpdated).lte(beforeLastUpdated));
        } else if(afterLastUpdated != null){
            query.addCriteria(Criteria.where(Curriculum.Constants.LAST_UPDATED).gte(afterLastUpdated));
        } else if(beforeLastUpdated != null){
            query.addCriteria(Criteria.where(Curriculum.Constants.LAST_UPDATED).lte(beforeLastUpdated));
        }
        return runQuery(query, Curriculum.class);
    }

    public List<Curriculum> getCurriculumByCourseAndBoard(CurriculumEntityName name, String id, Long boardId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(name));
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).is(id));
        query.addCriteria(Criteria.where(Curriculum.Constants.BOARD_ID).is(boardId));
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        return runQuery(query, Curriculum.class);
    }

    public Boolean insertAllChildren(List<Curriculum> curriculums, String callingUserId) {
        try {
            insertAllEntities(curriculums, Curriculum.class.getSimpleName(), callingUserId);
            return true;
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Curriculum> getCurriculumDone(List<String> sessionIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_TYPE).is(CurriculumEntityName.OTF_BATCH));
        query.addCriteria(Criteria.where(Curriculum.Constants.SESSION_IDS).in(sessionIds));
        query.addCriteria(Criteria.where(Curriculum.Constants.STATUS).is(CurriculumStatus.DONE));
        return runQuery(query, Curriculum.class);
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, Curriculum.class);
        } catch (Exception ex) {
            logger.error("Error in deleting Curriculum with id: " + id, ex);
        }

        return result;
    }

    public void changeEntityStateToDeleted(List<String> ids) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants._ID).in(ids));

        Update update = new Update();
        update.set(Curriculum.Constants.ENTITY_STATE, EntityState.DELETED);
        updateMulti(query, update, Curriculum.class);
    }

    public void incrementChildOrders(List<String> ids) {
        Query updateQuery = new Query();
        updateQuery.addCriteria(Criteria.where(Curriculum.Constants._ID).in(ids));

        Update update = new Update();
        update.inc(Curriculum.Constants.CHILD_ORDER, 1);
        updateMulti(updateQuery, update, Curriculum.class);
    }

    public void updateNodesInCurriculum(List<String> ids, String title, String note, Float expectedHours) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants._ID).in(ids));
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        Update update = new Update();
        if (StringUtils.isNotEmpty(title)) {
            update.set(Curriculum.Constants.TITLE, title);
        }
        if (StringUtils.isNotEmpty(note)) {
            update.set(Curriculum.Constants.NOTE, note);
        }
        if (expectedHours != null && expectedHours > 0) {
            update.set(Curriculum.Constants.EXPECTED_HOURS, expectedHours);
        }
        updateMulti(query, update, Curriculum.class);
    }

    public List<String> curriculumExists(Set<String> contextIds) {
        List<String> batchIds = new ArrayList<>();
        List<String> ids = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(contextIds)) {
            for (String id : contextIds) {
                if (StringUtils.isNotEmpty(id)) {
                    ids.add(id);
                }
            }
            if (ArrayUtils.isEmpty(ids)) {
                return batchIds;
            }
            Query query = new Query();
            query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(ids));
            query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
            query.addCriteria(Criteria.where(Curriculum.Constants.PARENT_ID).exists(false));
            //query.limit(1);
            query.fields().include(Curriculum.Constants.CONTEXT_ID);
            query.fields().include(Curriculum.Constants._ID);
            List<Curriculum> curriculums = runQuery(query, Curriculum.class);

            if (ArrayUtils.isNotEmpty(curriculums)) {
                for (Curriculum curriculum : curriculums) {
                    if (StringUtils.isNotEmpty(curriculum.getContextId())) {
                        batchIds.add(curriculum.getContextId());
                    }
                }

            }
        }
        return batchIds;
    }
    
    public List<Curriculum> getTestForBatchIds(List<String> batchIds){
        
        Query query = new Query();
        // query.addCriteria(Criteria.where(Curriculum.Constants.STATUS).in(Arrays.asList(CurriculumStatus.INPROGRESS, CurriculumStatus.DONE)));
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(batchIds));
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTENT_TYPE).is(ContentType.TEST));
        
        return runQuery(query, Curriculum.class);
        
    }

    @Deprecated
    public List<String> migrateExistingData(int dataSize){
        int skip=0;
        int limit=100;
        long startTime=System.currentTimeMillis();
        int totalChanged=0;
        logger.info("Migration Started");
        Map<String,Boolean> curriculumMap=new HashMap<>();
        List<String> changedCurriculum=new ArrayList<>();
        while (skip<dataSize){
            Query query=new Query();
            query.skip(skip);
            query.limit(limit);
            skip+=limit;
            AtomicInteger total= new AtomicInteger();
            List<Curriculum> curriculums=runQuery(query,Curriculum.class);
            if(curriculums!=null){
                logger.info("Curriculums found to update. Going to update "+curriculums.size()+" curriculums");
                logger.info("No of curriculums will be updated after this operation is "+skip);
                for (Curriculum curriculum : curriculums) {
                    boolean changed=false;
                    logger.info("No of items updated are " + total.incrementAndGet());
                    List<ContentInfo> contents = curriculum.getContents();
                    if (contents != null && contents.size() != 0) {
                        logger.info("Contents need to be updated");
                        for (ContentInfo contentInfo : contents) {
                            changed=true;
                            /*
                            // Migration script for content share type
                            contentInfo.setShareType(ShareType.MANUAL);
                            if (ContentType.TEST.equals(contentInfo.getContentType()) && ArrayUtils.isEmpty(contentInfo.getContentInfoId())) {
                                contentInfo.setShareStatus(ShareStatus.UNSHARED);
                            } else {
                                contentInfo.setShareStatus(ShareStatus.SHARED);
                            }
                            */
                            // Migration script for content share time
                            if(ShareStatus.SHARED.equals(contentInfo.getShareStatus())){
                                contentInfo.setSharedTime(System.currentTimeMillis());
                                contentInfo.setSharedBy(curriculum.getLastUpdatedBy());
                                contentInfo.setCreationTime(System.currentTimeMillis());
                                contentInfo.setCreatedBy(curriculum.getCreatedBy());
                            }else{
                                contentInfo.setCreationTime(System.currentTimeMillis());
                                contentInfo.setCreatedBy(curriculum.getCreatedBy());
                            }
                        }
                        contents.forEach(content -> {
                            logger.info("After updation the status of content is of content type " + content.getShareType() + " with share status " + content.getShareStatus());
                        });
                    } else {
                        logger.info("No contents need to be updated.");
                    }
                    if (!curriculumMap.containsKey(curriculum.getId())) {
                        curriculumMap.put(curriculum.getId(), true);
                    } else {
                        try {
                            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "Not allowed to update one data again and again", Level.FATAL);
                        } catch (BadRequestException e) {
                            logger.error("The error is ",e);
                        }
                    }
                    if(changed){
                        ++totalChanged;
                        logger.info("Curriculum updated with curriculum id " + curriculum.getId());
                        changedCurriculum.add(curriculum.getId());
                        curriculum.setContents(contents);
                        saveEntity(curriculum);
                    }
                }
            }else return null;
            logger.info("Updated "+skip+" curriculum.");
        }
        logger.info("Migration Ended");
        long endtime=System.currentTimeMillis();
        logger.info("Total time taken is "+(endtime-startTime));
        logger.info("Changed curriculum are "+changedCurriculum);
        logger.info("Totally changed curriculum are "+totalChanged);
        return changedCurriculum;
    }

    public List<Curriculum> getCurriculumForBatchBoardAndLastUpdated(String batchId, Long boardId, Long lastUpdated) {
        Query query=new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.BOARD_ID).is(boardId));
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).is(batchId));
        query.addCriteria(Criteria.where(Curriculum.Constants.STATUS_CHANGE_TIME).gte(lastUpdated));

        logger.info("The query is for getting last updated is :"+query);

        return runQuery(query,Curriculum.class);
    }

    public List<Curriculum> getCurriculumByContextIdForCurriculumStructure(String id){
        Query query=new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).is(id));
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).ne(EntityState.DELETED));

        query.fields().include(Curriculum.Constants._ID);
        query.fields().include(Curriculum.Constants.PARENT_ID);
        query.fields().include(Curriculum.Constants.TITLE);
        query.fields().include(Curriculum.Constants.TOPIC_TAGS);
        query.fields().include(Curriculum.Constants.CHILD_ORDER);
        query.fields().include(Curriculum.Constants.BOARD_ID);

        return runQuery(query,Curriculum.class);
    }

    public List<Curriculum> getActiveCurriculumNodesForParentAndChildren(String parentId){
        Query query = new Query();

        Criteria criteria = new Criteria();
        criteria.orOperator(
                Criteria.where(Curriculum.Constants.COURSE_NODE_ID).is(parentId),
                Criteria.where(Curriculum.Constants._ID).is(parentId)
        );
        query.addCriteria(criteria);
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return runQuery(query,Curriculum.class);
    }

    public List<Curriculum> getAllActiveChildrenForParentIdsInDescendingChildOrder(Set<String> parentIds){

        Query childQuery = new Query();
        childQuery.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        childQuery.addCriteria(Criteria.where(Curriculum.Constants.PARENT_ID).in(parentIds));
        childQuery.with(Sort.by(Sort.Direction.DESC, Curriculum.Constants.CHILD_ORDER));

        return runQuery(childQuery, Curriculum.class);
    }

    public List<Curriculum> getCurriculumIdOfAllTheActiveChildNodes(List<String> parentIds){
        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.PARENT_ID).in(parentIds));
        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        query.fields().include(Curriculum.Constants._ID);

        return runQuery(query, Curriculum.class);
    }
}
