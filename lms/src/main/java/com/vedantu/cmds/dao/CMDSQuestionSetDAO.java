package com.vedantu.cmds.dao;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.pojo.QuestionApprovalPojo;
import com.vedantu.cmds.request.CheckerQuestionSetOrTestFilterRequest;
import com.vedantu.cmds.request.QuestionApprovalRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import com.vedantu.cmds.entities.CMDSQuestionSet;
import com.vedantu.cmds.request.GetQuestionSetsReq;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class CMDSQuestionSetDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CMDSQuestionSetDAO.class);


	public CMDSQuestionSetDAO() {
		super();
	}

	public void update(CMDSQuestionSet questionSet) {
		try {
			Assert.notNull(questionSet);
			saveEntity(questionSet);
		} catch (Exception ex) {
			throw new RuntimeException(
					"ContentInfoUpdateError : Error updating the content info " + questionSet.toString(), ex);
		}
	}

	public CMDSQuestionSet getById(String id) {
		CMDSQuestionSet questionSet;
		try {
			Assert.hasText(id);
			questionSet = getEntityById(id, CMDSQuestionSet.class);
		} catch (Exception ex) {
			throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
		}
		return questionSet;
	}

	public CMDSQuestionSet updateToProcessing(String questionSetId) throws VException {

		if (StringUtils.isEmpty(questionSetId)) {
			return null;
		}

		CMDSQuestionSet questionSet = getById(questionSetId);
		if (questionSet == null) {
			throw new NotFoundException(ErrorCode.CMDS_QUESTION_SET_NOT_FOUND, "Question set not found");
		}
		if (VedantuRecordState.CONFIRMING.equals(questionSet.recordState)) {
			throw new BadRequestException(ErrorCode.CMDS_UPLOAD_UNDER_PROCESSING,"Another upload is under progress");
		}
		if (VedantuRecordState.ACTIVE.equals(questionSet.recordState)) {
			throw new ConflictException(ErrorCode.CMDS_QUESTION_SET_ALREADY_ADDED, "Question set is already confirmed");
		}

		questionSet.setRecordState(VedantuRecordState.CONFIRMING);
		questionSet.setLastUpdated(System.currentTimeMillis());
		update(questionSet);
		return questionSet;
	}

	public List<CMDSQuestionSet> getQuestionSets(GetQuestionSetsReq req) {
		Query questionQuery = new Query();
		if (CollectionUtils.isEmpty(req.getRecordStates())) {
			req.addRecordState(VedantuRecordState.ACTIVE);
			req.addRecordState(VedantuRecordState.CONFIRMING);
		}

		questionQuery.addCriteria(Criteria.where(CMDSQuestionSet.Constants.RECORD_STATE).in(req.getRecordStates()));
		if (!StringUtils.isEmpty(req.getCreatedBy())) {
			questionQuery.addCriteria(Criteria.where(CMDSQuestionSet.Constants.CREATED_BY).is(req.getCreatedBy()));
		}
		if (req.getDifficulty() != null) {
			questionQuery.addCriteria(Criteria.where(CMDSQuestionSet.Constants.DIFFICULTY).is(req.getDifficulty()));
		}
		if (req.getStatus() != null) {
			questionQuery.addCriteria(Criteria.where(CMDSQuestionSet.Constants.STATUS).is(req.getStatus()));
		}

		List<CMDSQuestionSet> results = runQuery(questionQuery, CMDSQuestionSet.class);
		logger.info("Response : " + results == null ? 0 : results.size());
		return results;
	}

	public void markDeleted(String questionSetId) throws VException {
		logger.info("Request : " + questionSetId);
		CMDSQuestionSet question = getById(questionSetId);
		if (question == null) {
			throw new VException(ErrorCode.NOT_FOUND_ERROR, "Question set not found for id : " + questionSetId);
		}

		markDeleted(question);
	}

	public void markDeleted(CMDSQuestionSet questionSet) {
		questionSet.setRecordState(VedantuRecordState.DELETED);
		update(questionSet);
	}

	public void deleteUnnecessaryQuestionSets(){
		long currentTime=System.currentTimeMillis();
		long fifteenDaysBefore=currentTime-15* DateTimeUtils.MILLIS_PER_DAY;
		long sixteenDaysBefore=currentTime-16*DateTimeUtils.MILLIS_PER_DAY;
		Query query=new Query();
		query.addCriteria(
				new Criteria().andOperator(
						Criteria.where(CMDSQuestionSet.Constants.CREATION_TIME).lte(fifteenDaysBefore),
						Criteria.where(CMDSQuestionSet.Constants.CREATION_TIME).gte(sixteenDaysBefore)
				)
		);
		query.addCriteria(
				Criteria.where(CMDSQuestionSet.Constants.APPROVAL_STATUS).in(
						Arrays.asList(
								CMDSApprovalStatus.REJECTED,
								CMDSApprovalStatus.TEMPORARY,
								CMDSApprovalStatus.DUPLICATE
						)
				)
		);
		Update update=new Update();
		update.set(CMDSQuestionSet.Constants.RECORD_STATE, VedantuRecordState.DELETED);
		updateMulti(query,update,CMDSQuestionSet.class);
	}


	public void updateApprovalStatus(QuestionApprovalRequest request) {

		if(StringUtils.isEmpty(request.getQuestionSetId()) ||
				Objects.isNull(request.getOverallApprovalStatus()) ||
				Objects.isNull(request.getCallingUserId()) ||
				Objects.isNull(request.getCheckerId())
		){
			try {
				throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION,"Please specify valid parameters");
			} catch (BadRequestException e) {
				logger.error("updateApprovalStatus",e);
			}
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSQuestionSet.Constants._ID).is(request.getQuestionSetId()));
		query.addCriteria(Criteria.where(CMDSQuestionSet.Constants.APPROVAL_STATUS).ne(CMDSApprovalStatus.CHECKER_APPROVED));

		Update update=new Update();
		update.set(CMDSQuestionSet.Constants.ASSIGNED_CHECKER,request.getCheckerId());
		if(Boolean.TRUE.equals(request.getIsQuestionSetIdOfTest())) {
			update.set(CMDSQuestionSet.Constants.APPROVAL_STATUS, CMDSApprovalStatus.TEMPORARY);
		}else{
			update.set(CMDSQuestionSet.Constants.APPROVAL_STATUS, request.getOverallApprovalStatus());
		}
		update.set(CMDSQuestionSet.Constants.LAST_UPDATED_BY,request.getCallingUserId());

		// Assigning checker for maker
		if(Objects.nonNull(request.getCheckerId()) &&
				(CMDSApprovalStatus.CHECKER_APPROVED.equals(request.getOverallApprovalStatus()) || CMDSApprovalStatus.MAKER_APPROVED.equals(request.getOverallApprovalStatus()))) {
			CMDSQuestionSet cmdsQuestionSet=getById(request.getQuestionSetId());
			if(Objects.isNull(cmdsQuestionSet)){
				try {
					throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION,"Invalid questionSetId id "+request.getQuestionSetId(), Level.ERROR);
				} catch (BadRequestException e) {
					logger.error("Error in test id ",e);
				}
			}
			Map<String, String> questionDuplicateMap = request.getApprovalPojo()
					.stream()
					.filter(this::isDuplicate)
					.collect(Collectors.toMap(
							QuestionApprovalPojo::getId,
							QuestionApprovalPojo::getIsACopyOf
					));
			logger.info("questionDuplicateMap {}",questionDuplicateMap);

			// Remove unused questions
			List<String> allQuestionIds = request.getApprovalPojo().stream().map(QuestionApprovalPojo::getId).collect(Collectors.toList());
			cmdsQuestionSet.removeUnusedQuestion(allQuestionIds);
			
			// Remove and replace duplicate questions
			cmdsQuestionSet.removeAndReplaceDuplicates(questionDuplicateMap);

			if(CMDSApprovalStatus.MAKER_APPROVED.equals(request.getOverallApprovalStatus())) {
				update.set(CMDSQuestionSet.Constants.CREATED_BY, request.getCallingUserId());
			}

			update.set(CMDSQuestionSet.Constants.QUESTION_IDS, cmdsQuestionSet.getQuestionIds());
			update.set(CMDSQuestionSet.Constants.ASSIGNED_CHECKER, request.getCheckerId());
		}
		logger.info("updating approval status of question set with `query` {} and `update` {}",query,update);
		updateFirst(query,update,CMDSQuestionSet.class);
	}

	private boolean isDuplicate(QuestionApprovalPojo questionApprovalPojo) {
		return CMDSApprovalStatus.DUPLICATE.equals(questionApprovalPojo.getStatus());
	}

	public List<CMDSQuestionSet> filterQuestionSetCheckerData(CheckerQuestionSetOrTestFilterRequest request) {
		logger.info("Request for filtering question set checker data is {} ",request);
		Query query=new Query();
		if(Objects.nonNull(request.getApprovalStatus())) {
			logger.info("Required approval status is there so going for the same.");
			query.addCriteria(Criteria.where(CMDSQuestionSet.Constants.APPROVAL_STATUS).is(request.getApprovalStatus()));
		}else {
			logger.info("Required approval status is not there so going for default ones.");
			query.addCriteria(Criteria.where(CMDSQuestionSet.Constants.APPROVAL_STATUS).in(Arrays.asList(
					CMDSApprovalStatus.MAKER_APPROVED,
					CMDSApprovalStatus.CHECKER_APPROVED,
					CMDSApprovalStatus.REJECTED
			)));
		}

		if(Objects.nonNull(request.getCheckerId())){
			logger.info("As the checker id is given going for checker id data only.");
			query.addCriteria(Criteria.where(CMDSQuestionSet.Constants.ASSIGNED_CHECKER).is(request.getCheckerId()));
		}else{
			query.addCriteria(Criteria.where(CMDSQuestionSet.Constants.ASSIGNED_CHECKER).ne(null));
		}

		if(Objects.nonNull(request.getTitle())){
			logger.info("Filtering also with the basis of title");
			query.addCriteria(Criteria.where(CMDSQuestionSet.Constants.FILE_NAME).is(request.getTitle()));
		}

		if(Objects.nonNull(request.getMakerId())){
			logger.info("Filtering on the basis of maker id also");
			query.addCriteria(Criteria.where(CMDSQuestion.Constants.CREATED_BY).is(request.getMakerId()));
		}

		if(request.isActionTimeSort()){
			logger.info("Sorting on the basis of last updated");
			query.with(Sort.by(Sort.Direction.DESC, CMDSQuestionSet.Constants.LAST_UPDATED));
		}else{
			logger.info("Sorting on the basis of creation time");
			query.with(Sort.by(Sort.Direction.DESC, CMDSQuestionSet.Constants.CREATION_TIME));
		}

		query.addCriteria(Criteria.where(CMDSQuestionSet.Constants.RECORD_STATE).ne(VedantuRecordState.DELETED));

		query.skip(request.getSkip());
		query.limit(request.getLimit());

		logger.info("final query of filterQuestionSetCheckerData is {}",query);
		return runQuery(query, CMDSQuestionSet.class);
    }

    public List<CMDSQuestionSet> getQuestionIdsOfQuestionSet(List<String> ids){
		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSQuestionSet.Constants._ID).in(ids));
		query.fields().include(CMDSQuestionSet.Constants.QUESTION_IDS);

		return runQuery(query,CMDSQuestionSet.class);
	}
}
