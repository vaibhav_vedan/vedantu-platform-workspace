package com.vedantu.cmds.dao.sql;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.cmds.constants.ConstantsGlobal;
import com.vedantu.cmds.entities.challenges.ChallengeAttemptRD;
import com.vedantu.cmds.entities.challenges.ClanMember;
import com.vedantu.cmds.pojo.challenges.ChallengeAttemptsCount;
import com.vedantu.cmds.pojo.challenges.ClanMemberCount;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractSqlDAO;

@Service
public class ClanMemberDAO extends AbstractSqlDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ClanMemberDAO.class);

	public ClanMemberDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}

	public void create(ClanMember clanMember, String callingUserId) throws ConflictException {
		try {
			if (clanMember != null) {
				saveEntity(clanMember, callingUserId);
			}
		}catch(Exception e){
			throw new ConflictException(ErrorCode.USER_MEMBER_OF_CLAN,"User is already part of clan");
		}
	}

	public List<ClanMember> getByUserId(Long userId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ClanMember> clanMembers = null;
		try {
			clanMembers = getByUserId(userId,session);
		} catch (Exception ex) {
			logger.error("getByUserId ClanMember: " + ex.getMessage());
			clanMembers = null;
		} finally {
			session.close();
		}
		return clanMembers;
	}

	public List<ClanMember> getByUserId(Long userId, Session session) {
		ClanMember clanMember = null;
		if (userId != null) {
			Criteria cr = session.createCriteria(ClanMember.class);
			cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
			cr.addOrder(Order.desc(ConstantsGlobal.CREATION_TIME));
			List<ClanMember> results = runQuery(session, cr, ClanMember.class);
			return results;

			} else {
				logger.info("getByUserId ClanMember : no ClanMember found " + userId);
			}		
		return new ArrayList<>();
	}
	
	public List<ClanMember> getByClanId(String clanId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ClanMember> clanMembers = null;
		try {
			clanMembers = getByClanId(clanId,session);
		} catch (Exception ex) {
			logger.error("getByclanId ClanMember: " + ex.getMessage());
			clanMembers = null;
		} finally {
			session.close();
		}
		return clanMembers;
	}

	public List<ClanMember> getByClanId(String clanId, Session session) {
		ClanMember clanMember = null;
		if (StringUtils.isNotEmpty(clanId)) {
			Criteria cr = session.createCriteria(ClanMember.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, clanId));
			
			List<ClanMember> results = runQuery(session, cr, ClanMember.class);
			return results;

			} else {
				logger.info("getByclanId ClanMember : no ClanMember found " + clanId);
			}		
		return new ArrayList<>();
	}

	public void update(ClanMember p, Session session, String callingUserId) {
		if (p != null) {
			logger.info("update ClanMember: " + p.toString());
			updateEntity(p, session, callingUserId);
		}
	}
	
	public Long getClanMemberCount(String clanId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Long count = null;
		try {
			Criteria cr = session.createCriteria(ClanMember.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, clanId));
			count = getCount(session, cr, false);
		} catch (Exception e) {
			logger.error("getClanMemberCount failed : " + e);
		} finally {
			session.close();
		}
		return count;
	}
	
	   public List<ClanMemberCount> getAttemptsCount(Set<String> clanIds) {
	    	SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
			Session session = sessionFactory.openSession();
			List<ClanMemberCount> counts = new ArrayList<>();
			Criteria cr = session.createCriteria(ClanMember.class);
			try {
				
				cr.add(Restrictions.in(ConstantsGlobal.CLAN_ID, clanIds));
				cr.setProjection(Projections.projectionList()					
						.add(Projections.groupProperty(ConstantsGlobal.CLAN_ID),"clanId")
						.add(Projections.rowCount(),"count"));
				cr.setResultTransformer(Transformers.aliasToBean(ClanMemberCount.class));

				counts = cr.list();
//				List results = runQuery(session, cr, ClanMember.class);
//				if (!results.isEmpty()) {
//					logger.info("result size : " +results.size() );
//					for(int i = 0; i < results.size();i++){
//						Object[] temp = (Object[]) results.get(i);
//						ClanMemberCount countAtt = new ClanMemberCount();
//						countAtt.setClanId((String) temp[0]);
//						Long count = (Long) temp[1];
//						if(count != null){
//							countAtt.setCount(count);
//						}
//						logger.info("challengeAttempt id  : " +countAtt.getClanId() + " count : "+ countAtt.getCount());
//						counts.add(countAtt);
//					}				
//				}
				
			} catch (Exception ex) {
				logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
			} finally {
				session.close();
			}
			return counts;
	    }
	
	public Long getCount(Session session, Criteria cr, boolean rank) {
		Long count = null;
		cr.setProjection(Projections.rowCount());
		List results = runQuery(session, cr, ClanMember.class);
		if (!results.isEmpty()) {
			count = (Long) results.get(0);
			if (rank) {
				count++;
			}
		}
		return count;
	}
	
	public List<ClanMember> getByClanId(String clanId,int start,int size) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<ClanMember> clanMembers = null;
		try {
			Criteria cr = session.createCriteria(ClanMember.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, clanId));
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			clanMembers = runQuery(session, cr, ClanMember.class);

		} catch (Exception ex) {
			logger.error("getByClanId ClanMember: " + ex.getMessage());
			clanMembers = null;
		} finally {
			session.close();
		}
		return clanMembers;
	}
	
	public ClanMember getByClanIdAndUserId(Long userId, String clanId) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		ClanMember clanMember = null;
		try {
			Criteria cr = session.createCriteria(ClanMember.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, clanId));
			cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
			
			List<ClanMember> results = runQuery(session, cr, ClanMember.class);
			if(!results.isEmpty()){
				clanMember = results.get(0);
			}
		} catch (Exception ex) {
			logger.error("getByclanId ClanMember: " + ex.getMessage());
			clanMember = null;
		} finally {
			session.close();
		}
		return clanMember;
	}
}
