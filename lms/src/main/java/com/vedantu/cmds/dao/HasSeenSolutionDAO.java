/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.HasSeenSolution;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class HasSeenSolutionDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(HasSeenSolutionDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public HasSeenSolutionDAO() {
        super();
    }

    public void save(HasSeenSolution entity) {
        saveEntity(entity);
    }

    public List<HasSeenSolution> getEntities(Long userId, String questionId, int start, int size) {
        Query query = prepareQuery(userId, questionId);
        setFetchParameters(query, start, size);
        return runQuery(query, HasSeenSolution.class);
    }

    public long getEntitiesCount(Long userId, String questionId) {
        Query query = prepareQuery(userId, questionId);
        return queryCount(query, HasSeenSolution.class);
    }

    public Query prepareQuery(Long userId, String questionId) {
        Query query = new Query();
        if (userId != null) {
            query.addCriteria(Criteria.where(HasSeenSolution.Constants.USER_ID).is(userId));
        }
        if (StringUtils.isNotEmpty(questionId)) {
            query.addCriteria(Criteria.where(HasSeenSolution.Constants.QUESTION_ID).is(questionId));
        }
        return query;
    }

}
