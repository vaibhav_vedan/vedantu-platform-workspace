package com.vedantu.cmds.dao;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.vedantu.cmds.entities.FileMetaInfo;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class FileMetaInfoDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(FileMetaInfoDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public FileMetaInfoDAO() {
		super();
	}

	public void update(FileMetaInfo fileMetaInfo) {
		try {
			Assert.notNull(fileMetaInfo);
			saveEntity(fileMetaInfo);
		} catch (Exception ex) {
			throw new RuntimeException(
					"ContentInfoUpdateError : Error updating the file meta info " + fileMetaInfo.toString(), ex);
		}
	}

	public FileMetaInfo getById(String id) {
		FileMetaInfo fileMetainfo = null;
		try {
			Assert.hasText(id);
			fileMetainfo = (FileMetaInfo) getEntityById(id, FileMetaInfo.class);
		} catch (Exception ex) {
			throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
		}
		return fileMetainfo;
	}

	public FileMetaInfo findByFileId(String fileId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(FileMetaInfo.Constants.FILE_ID).is(fileId));
		List<FileMetaInfo> results = runQuery(query, FileMetaInfo.class);
		return CollectionUtils.isEmpty(results) ? null : results.get(0);
	}

	public void delete(FileMetaInfo fileMetaInfo) {
		fileMetaInfo.setRecordState(VedantuRecordState.DELETED);
		update(fileMetaInfo);
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}
}
