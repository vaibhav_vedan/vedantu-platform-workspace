package com.vedantu.cmds.dao.sql;

import com.vedantu.cmds.entities.challenges.PictureChallengeLeaderBoardTransaction;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractSqlDAO;

import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PictureChallengeLeaderBoardTransactionDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PictureChallengeLeaderBoardTransactionDAO.class);
    
    private static int MAX_RESULTS = 10;

    public PictureChallengeLeaderBoardTransactionDAO() {
        super();
    }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void updateAll(List<PictureChallengeLeaderBoardTransaction> p, Session session) {
        updateAll(p, session, null);
    }

    public void updateAll(List<PictureChallengeLeaderBoardTransaction> p, Session session, String callingUserId) {
        try {
            if (p != null) {
                updateAllEntities(p, session, callingUserId);
            }
        } catch (Exception ex) {
        	//TODO : Handle Exception
            logger.error("updateAll PictureChallengeLeaderBoardTransaction: " + ex.getMessage());
        }
    }	
}
