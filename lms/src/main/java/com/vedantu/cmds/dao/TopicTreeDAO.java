package com.vedantu.cmds.dao;

import com.amazonaws.services.dynamodbv2.xspec.B;
import com.google.gson.Gson;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CurriculumTopicTree;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.DuplicateEntryException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class TopicTreeDAO extends AbstractMongoDAO {

    private final Gson gson = new Gson();

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TopicTreeDAO.class);

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    public TopicTreeDAO() {
        super();
    }

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private RedisDAO redisDAO;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void upsertBaseTopicTree(BaseTopicTree p, Long callingUserId, boolean isComputeChildren) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            super.saveEntity(p, callingUserIdString);

//            try {
//                Map<String, Object> payload = new HashMap<>();
//                payload.put("nodeId", p.getId());
//                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_BASE_TREE_NODE_PARENTS, payload);
//                asyncTaskFactory.executeTask(params);
//            } catch (Exception e) {
//                logger.error(e.getMessage());
//            }
            logger.info("BaseTopicTree saved is {}",p);
            updateBaseTreeNodeParents(p.getId());

//            updateBaseTreeChildren(p.getId());
            if(isComputeChildren) {
                logger.info("COMPUTING-CHILDREN");
                try {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("nodeId", p.getId());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_BASE_TREE_CHILDREN, payload);
                    asyncTaskFactory.executeTask(params);
                } catch (Exception e) {
                    logger.error(e.getMessage());
                }
            }
        }
    }

    public void upsertCurriculumTopicTree(CurriculumTopicTree p, Long callingUserId) throws DuplicateEntryException {
        if (p != null) {

            //Duplicate check for Name and Same Parent added in the CurriculumTree
            if(duplicateCurriculumTreeNodeNameAndParent(p)) {
                throw new DuplicateEntryException(ErrorCode.DUPLICATE_ENTRY, "Same entity in CurriculumTree exists");
            }

            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            super.saveEntity(p, callingUserIdString);

            try {
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_CURRICULUM_TREE_CHILDREN, null);
                asyncTaskFactory.executeTask(params);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }

    public List<BaseTopicTree> getAllTopicTreeNodes() {
        Query query = new Query();
        List<BaseTopicTree> allNodes = runQuery(query, BaseTopicTree.class);
        return allNodes;
    }

    public List<CurriculumTopicTree> getAllCurriculumTopicTreeNodes() {
        Query query = new Query();
        List<CurriculumTopicTree> allNodes = runQuery(query, CurriculumTopicTree.class);
        return allNodes;
    }

    public BaseTopicTree getBaseTopicTreeNodeByField(String fieldName, String fieldValue) {
        BaseTopicTree board = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(fieldName).is(fieldValue));
        logger.info("getBaseTopicTreeNodeByField: query: "+query);
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        if (boards != null && !boards.isEmpty()) {
            board = boards.get(0);
        }
//        logger.info("getBoardByField"+ board);
        return board;
    }

    public BaseTopicTree getBaseTopicTreeNodeById(String id) {
        BaseTopicTree baseTopicTree = null;
        if(!StringUtils.isEmpty(id)) {
            baseTopicTree = getEntityById(id, BaseTopicTree.class);
        }
        return baseTopicTree;
    }

    public CurriculumTopicTree getCurriculumTopicTreeNodeById(String id) {
        
        CurriculumTopicTree curriculumTopicTree = null;
        if (!StringUtils.isEmpty(id)) {
            logger.info("Id for search is: "+id);
            curriculumTopicTree = getEntityById(id, CurriculumTopicTree.class);
            logger.info("curreiculum : "+curriculumTopicTree);
        }
        return curriculumTopicTree;
    }

    public CurriculumTopicTree getCurriculumTopicTreeNodeByField(String fieldName, String fieldValue) {
        CurriculumTopicTree board = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(fieldName).is(fieldValue));
        List<CurriculumTopicTree> boards = runQuery(query, CurriculumTopicTree.class);
        if (boards != null && !boards.isEmpty()) {
            board = boards.get(0);
        }
//        logger.info("getBoardByField"+ board);
        return board;
    }

    public List<CurriculumTopicTree> getCurriculumTopicTreesByNames(Set<String> names){
        Query query = new Query();
        logger.info("Names : "+names);
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.NAME).in(names));
        
        return runQuery(query, CurriculumTopicTree.class);
    }
    
    public List<BaseTopicTree> getBasicTopicTreeNodesByParentId(String parentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.PARENT_ID).is(parentId));
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;
    }

    public CurriculumTopicTree getCurriculumTopicTreeNodesByParentIdAndName(String parentId, String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.PARENT_ID).is(parentId));
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.NAME).is(name));
        return findOne(query, CurriculumTopicTree.class);
    }    

    public List<CurriculumTopicTree> getCurriculumTopicTreeNodesByParentIdsAndName(Set<String> parentIds, String name) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.PARENT_ID).in(parentIds));
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.NAME).is(name));
        return runQuery(query, CurriculumTopicTree.class);
    }
    
    public List<CurriculumTopicTree> getCurriculumTopicTreeNodesByParentId(String parentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.PARENT_ID).is(parentId));
        List<CurriculumTopicTree> boards = runQuery(query, CurriculumTopicTree.class);
        return boards;
    }

    public List<BaseTopicTree> getBaseTreeNodesByLevel(Integer level) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.LEVEL).is(level));
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;
    }

    public List<BaseTopicTree> getBaseTreeNodesByLevelWithoutChildren(Integer level) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.LEVEL).is(level));
        query.fields().exclude(BaseTopicTree.Constants.CHILDREN);
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;
    }

    public List<BaseTopicTree> getBaseTreeNodesBySubject(String subject){
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.LEVEL).is(0));
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.NAME).is(subject));
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;
    }
    
    public Set<BaseTopicTree> getBaseTreeNodesByNodeNames(Set<String> nodeNames){
        Query query = new Query();
        if(!ArrayUtils.isEmpty(nodeNames))
        	query.addCriteria(Criteria.where(BaseTopicTree.Constants.NAME).in(nodeNames));
        else
        	return null;
        Set<BaseTopicTree> nodes = new HashSet<>(runQuery(query, BaseTopicTree.class));
        return nodes;
    }
    
    public List<CurriculumTopicTree> getCurriculumTreeNodesByLevel(Integer level) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.LEVEL).is(level));
        List<CurriculumTopicTree> boards = runQuery(query, CurriculumTopicTree.class);
        return boards;
    }

    public void updateBaseTreeChildren(String nodeId) {
        logger.info("in updateBaseTreeChildren");
        List<BaseTopicTree> allNodes = getAllTopicTreeNodes();
        for(BaseTopicTree b: allNodes) {
//            logger.info("PRINTING CHILD OF PARENT");
            Set<String> finalChildren = new HashSet<>();
            finalChildren = updateChildrenOfBaseTreeNode(finalChildren, b.getId());
//            logger.info(finalChildren);
            b.setChildren(finalChildren);
            saveEntity(b);
        }

        BaseTopicTree newNode = getBaseTopicTreeNodeById(nodeId);
        BaseTopicTree parentNode = getBaseTopicTreeNodeById(newNode.getParentId());
        if(parentNode != null) {
            Set<String> allParents = parentNode.getParents();
            logger.info("updateBaseTreeNodeParents");
            logger.info(allParents);
//            if(allParents == null || allParents.size() == 0) {
//                allParents = new HashSet<String>();
//            }
            allParents.add(parentNode.getName());
            logger.info(allParents);
            newNode.setParents(allParents);
            logger.info(newNode.toString());
            saveEntity(newNode);
        }


    }

    public void updateBaseTreeNodeParents(String nodeId) {
        logger.info("nodeId - {}",nodeId);
        BaseTopicTree newNode = getBaseTopicTreeNodeById(nodeId);
        logger.info("newNode - {}",newNode);
        BaseTopicTree parentNode = getBaseTopicTreeNodeById(newNode.getParentId());
        logger.info("parentNode - {}",parentNode);
        if(parentNode != null) {
            Set<String> allParents = Objects.nonNull(parentNode.getParents())?parentNode.getParents():new HashSet<>();
            logger.info("updateBaseTreeNodeParents");
            logger.info(allParents);
//            if(allParents == null || allParents.size() == 0) {
//                allParents = new HashSet<String>();
//            }
            allParents.add(parentNode.getName());
            logger.info(allParents);
            newNode.setParents(allParents);
            logger.info(newNode.toString());
            saveEntity(newNode);
        }
    }

    public void updateCurriculumTreeChildren() {
        List<CurriculumTopicTree> allNodes = getAllCurriculumTopicTreeNodes();
        for(CurriculumTopicTree c: allNodes) {
            Set<String> finalChidren = new HashSet<>();
            finalChidren = updateChildrenOfCurriculumTreeNode(finalChidren, c.getId());
            c.setChildren(finalChidren);
            saveEntity(c);
        }
    }

    public Set<String> updateChildrenOfBaseTreeNode(Set<String> finalChildren, String nodeId) {
        Set<String> childrenName;
        List<BaseTopicTree> children = getBasicTopicTreeNodesByParentId(nodeId);
        if(children != null && children.size() == 0) {
            return finalChildren;
        } else if(children != null && children.size() > 0) {
            for(BaseTopicTree b: children) {
                finalChildren.add(b.getName());
                updateChildrenOfBaseTreeNode(finalChildren, b.getId());
            }
        }
        return finalChildren;
    }

    public Set<String> updateChildrenOfCurriculumTreeNode(Set<String> finalChildren, String nodeId) {
        Set<String> childrenName;
        List<CurriculumTopicTree> children = getCurriculumTopicTreeNodesByParentId(nodeId);
        if(children != null && children.size() == 0) {
            return finalChildren;
        } else if(children != null && children.size() > 0) {
            for(CurriculumTopicTree b: children) {
                finalChildren.add(b.getName());
                updateChildrenOfCurriculumTreeNode(finalChildren, b.getId());
            }
        }
        return finalChildren;
    }

    public List<BaseTopicTree> getNodesForTopics(Set<String> inputTopics) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.NAME).in(inputTopics));
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;
    }
    
    public List<BaseTopicTree> getNodesForTopicsWithoutChildren(Set<String> inputTopics){
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.NAME).in(inputTopics));
        query.fields().exclude(BaseTopicTree.Constants.CHILDREN);        
        List<BaseTopicTree> boards = runQuery(query, BaseTopicTree.class);
        return boards;        
    }

    public List<CurriculumTopicTree> verifyCurriculumNodeAndChild(String[] cInput) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.NAME).regex(Pattern.quote(cInput[0]), "i"));
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.CHILDREN).regex(Pattern.quote(cInput[1]), "i"));
        logger.info(query);
        List<CurriculumTopicTree> boards = runQuery(query, CurriculumTopicTree.class);
        logger.info(boards);
        return boards;
    }

    public boolean duplicateCurriculumTreeNodeNameAndParent(CurriculumTopicTree p) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.NAME).regex(Pattern.quote(p.getName()), "i"));
        query.addCriteria(Criteria.where(CurriculumTopicTree.Constants.PARENT_ID).is(p.getParentId()));
        List<CurriculumTopicTree> boards = runQuery(query, CurriculumTopicTree.class);
        if(boards.size() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public void cacheEntireTopicTree() throws InternalServerErrorException {
        Query query=new Query();
        query.fields().include(BaseTopicTree.Constants.NAME);
        query.fields().include(BaseTopicTree.Constants.LEVEL);
        query.fields().include(BaseTopicTree.Constants.SLUG);
        query.fields().include(BaseTopicTree.Constants.PARENT_ID);
        query.fields().include(BaseTopicTree.Constants.PARENTS);
        query.fields().include(BaseTopicTree.Constants.CHILDREN);

        List<BaseTopicTree> baseTopicTrees=runQuery(query,BaseTopicTree.class);
        Map<String,String> baseTreeIdPojoMap=baseTopicTrees
                .stream()
                .collect(Collectors.toMap(
                        node->"BaseTopicTree:"+node.getId(),
                        gson::toJson
                ));
        Map<String,String> baseTreeNamePojoMap=baseTopicTrees
                .stream()
                .collect(Collectors.toMap(
                        node->"BaseTopicTree:"+node.getName().toLowerCase(),
                        gson::toJson
                ));
        redisDAO.setKeyPairs(baseTreeIdPojoMap);
        redisDAO.setKeyPairs(baseTreeNamePojoMap);
    }

    @Deprecated
    public void updateTopicTreeParentsAndChildrens(String patternValue,String corrPatternValue,List<BaseTopicTree> getTopicTreeErrorNodes,String topicTreeField){

        Map<String,Set<String>>topicTreeCollection=new HashMap<>();
        Set<String> topicTreeNode=new HashSet<>();

        for(BaseTopicTree baseTopicTree :getTopicTreeErrorNodes ){

            if(BaseTopicTree.Constants.PARENTS.equalsIgnoreCase(topicTreeField))
                topicTreeNode=baseTopicTree.getParents();
            else
                topicTreeNode=baseTopicTree.getChildren();

            if(ArrayUtils.isNotEmpty(topicTreeNode)) {

                Set<String> tempNodes=new HashSet<>();
                if (topicTreeNode.contains(patternValue)) {
                    for (String node : topicTreeNode) {
                        if (node.matches(patternValue))
                            tempNodes.add(corrPatternValue);
                        else
                            tempNodes.add(node);
                    }
                    topicTreeCollection.put(baseTopicTree.getId(),tempNodes);
                    logger.info("topicTreeCollection = {} --- {}",baseTopicTree.getId(),tempNodes);
                }
            }
        }

        for(Map.Entry<String,Set<String>>Node:topicTreeCollection.entrySet()) {

            Query query=new Query();
            Update update=new Update();
            query.addCriteria(Criteria.where(BaseTopicTree.Constants._ID).is(Node.getKey()));
            update.set(topicTreeField,Node.getValue());
            updateFirst(query,update,BaseTopicTree.class);
        }
    }

    @Deprecated
    public void updateTopicTreeErrorNodes(String patternValue,String corrPatternValue){

        Query query=new Query();
        Criteria orCriteria =new Criteria();
        orCriteria.orOperator(
                Criteria.where(BaseTopicTree.Constants.NAME).is(patternValue),
                Criteria.where(BaseTopicTree.Constants.PARENTS).is(patternValue),
                Criteria.where(BaseTopicTree.Constants.CHILDREN).is(patternValue));
        query.addCriteria(orCriteria);
        query.fields().include(BaseTopicTree.Constants.NAME);
        query.fields().include(BaseTopicTree.Constants.PARENTS);
        query.fields().include(BaseTopicTree.Constants.CHILDREN);
        logger.info("patternValue = {}     corrPatternValue = {}  ",patternValue,corrPatternValue);
        List<BaseTopicTree> getTopicTreeErrorNodes=runQuery(query,BaseTopicTree.class);

        logger.info("Query Response = {}",getTopicTreeErrorNodes);
        String corrSlugString=corrPatternValue.toLowerCase().replace(" ","-");

        Query queryName=new Query();
        Update update=new Update();

        queryName.addCriteria(Criteria.where(BaseTopicTree.Constants.NAME).is(patternValue));
        update.set(BaseTopicTree.Constants.NAME,corrPatternValue);
        update.set(BaseTopicTree.Constants.MAPPED_FROM,corrPatternValue);
        update.set(BaseTopicTree.Constants.SLUG,corrSlugString);
        updateMulti(queryName,update,BaseTopicTree.class);

        updateTopicTreeParentsAndChildrens(patternValue,corrPatternValue,getTopicTreeErrorNodes,BaseTopicTree.Constants.PARENTS);
        updateTopicTreeParentsAndChildrens(patternValue,corrPatternValue,getTopicTreeErrorNodes,BaseTopicTree.Constants.CHILDREN);
    }

//    public void flushEntireTopicTree(){
//        redisDAO.deleteAllMatchingKeys("BaseTopicTree:");
//    }

    public void insertNodeInTopicTree(BaseTopicTree baseTopicTree){
        if(baseTopicTree!=null){
            saveEntity(baseTopicTree);
        }
    }

    public void updateParentAndChildrenOfNodes(String id, Set<String> parents, Set<String> children){
        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants._ID).is(id));

        Update update=new Update();
        if(ArrayUtils.isNotEmpty(parents))
            update.set(BaseTopicTree.Constants.PARENTS,parents);
        if(ArrayUtils.isNotEmpty(children))
            update.set(BaseTopicTree.Constants.CHILDREN,children);
        updateFirst(query,update,BaseTopicTree.class);

    }
    public void mapNewNameToOldNames(String name){
        mapNewNameToOldNames(name,name);
    }

    public String mapNewNameToOldNames(String oldName, String newName){
        logger.info("oldName: "+oldName+" newName: "+newName);
        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.NAME).is(newName.replaceAll("\\r", "")));

        BaseTopicTree baseTopicTree=findOne(query,BaseTopicTree.class);
        if(baseTopicTree==null){
            logger.info("Base topic tree not found for the name: "+newName);
            return newName;
        }
        Update update=new Update();
        update.set(BaseTopicTree.Constants.MAPPED_FROM,oldName);
        logger.info(query);
        logger.info(update);
        updateFirst(query,update,BaseTopicTree.class);
        return null;
    }

    public List<BaseTopicTree> getMappedEntries(){
        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.MAPPED_FROM).ne(null));

        return runQuery(query,BaseTopicTree.class);
    }

    public void mapOldNameToOldName(){
        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.MAPPED_FROM).is(null));

        query.fields().include(BaseTopicTree.Constants.NAME);
        query.fields().exclude(BaseTopicTree.Constants._ID);

        List<BaseTopicTree> baseTopicTrees=runQuery(query,BaseTopicTree.class);
        baseTopicTrees.parallelStream().map(BaseTopicTree::getName).forEach(name->mapNewNameToOldNames(name));
    }

    public List<BaseTopicTree> getBaseTreeSubjectNodes() {
        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.PARENT_ID).is("0"));

        query.fields().include(BaseTopicTree.Constants.NAME);
        query.fields().include(BaseTopicTree.Constants._ID);

        return runQuery(query,BaseTopicTree.class);
    }

    public List<BaseTopicTree> getAllChildrenByParentId(String parentId) {
        if(StringUtils.isEmpty(parentId)){
            return null;
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.PARENT_ID).is(parentId));

        query.fields().include(BaseTopicTree.Constants._ID);
        query.fields().include(BaseTopicTree.Constants.NAME);

        return runQuery(query,BaseTopicTree.class);
    }

    public void updateLevelOfChildren(String parentId, int level) {
        if(StringUtils.isEmpty(parentId)){
            return;
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.PARENT_ID).is(parentId));

        Update update=new Update();
        update.set(BaseTopicTree.Constants.LEVEL,level);

        updateMulti(query,update,BaseTopicTree.class);
    }

    public List<BaseTopicTree> getNamesOfFirstLevelNodes() {

        Query query=new Query();
        query.addCriteria(Criteria.where(BaseTopicTree.Constants.LEVEL).is(0));

        query.fields().include(BaseTopicTree.Constants.NAME);
        query.fields().exclude(BaseTopicTree.Constants._ID);

        return runQuery(query,BaseTopicTree.class);
    }
}
