package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.cmds.entities.TestUser;
import com.vedantu.lms.request.AddOrEditTestUserReq;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

/**
 * Created by somil on 05/04/17.
 */
@Service
public class TestUserDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public TestUserDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(QuestionAttempt p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public TestUser getById(String id) {
        TestUser testUser = null;
        if (!StringUtils.isEmpty(id)) {
            testUser = getEntityById(id, TestUser.class);
        }
        return testUser;
    }


    public TestUser addOrEditTestUser(AddOrEditTestUserReq req) {
        TestUser testUser = null;
        if(StringUtils.isEmpty(req.getId())) {
            testUser = new TestUser(req.getName(), req.getGrade(), req.getEmail(), req.getContactNumber(), req.getPhoneCode(), req.getUserData());
        } else {
            testUser = getById(req.getId());
            testUser.setName(req.getName());
            testUser.setEmail(req.getEmail());
            testUser.setGrade(req.getGrade());
            testUser.setContactNumber(req.getContactNumber());
            testUser.setPhoneCode(req.getPhoneCode());
            testUser.setUserData(req.getUserData());
        }
        saveEntity(testUser);
        return testUser;

    }
}
