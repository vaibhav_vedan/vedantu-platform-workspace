/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;
import com.vedantu.cmds.entities.CMDSVideoPlaylistOrder;
import com.vedantu.cmds.entities.ClassroomChat;
import com.vedantu.cmds.entities.ClassroomChatMessage;
import com.vedantu.cmds.pojo.PlaylistVideoPojo;
import com.vedantu.cmds.request.GetCMDSVideoPlaylistReq;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 *
 * @author parashar
 */
@Service
public class CMDSVideoPlaylistDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSVideoPlaylistDAO.class);

    @Autowired
    private RedisDAO redisDao;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public CMDSVideoPlaylistDAO() {
        super();
    }

    public void update(CMDSVideoPlaylist videoPlaylist) {
        logger.info("Request : " + videoPlaylist.toString());
        try {
            Assert.notNull(videoPlaylist);
            saveEntity(videoPlaylist);
            logger.info("Updated id : " + videoPlaylist.getId());
        } catch (Exception ex) {
            throw new RuntimeException("CMDSTestUpdateError : Error updating the CMDS Video " + videoPlaylist.toString(), ex);
        }
    }

    public void saveClassroomChat(List<ClassroomChat> crChatList) {
    	logger.info("Request : " + crChatList.toString());
        try {
        	for(ClassroomChat crChat : crChatList) {
        		Assert.notNull(crChat);
        		saveEntity(crChat);
        	}
        } catch (Exception ex) {
            throw new RuntimeException("ClassroomChat : Error updating the Classroom chat ", ex);
        }
    }

    public List<CMDSVideoPlaylist> getVideoPlaylist(GetCMDSVideoPlaylistReq req) {
        Query query = new Query();

        if (ArrayUtils.isNotEmpty(req.getMainTags())) {
            query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(req.getMainTags()));
        }

        if (req.getPublished() != null) {
            query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.PUBLISHED).is(req.getPublished()));
        }

        query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        if (req.getStart() != null) {
            query.skip(req.getStart());
        }

        if (req.getSize() != null && req.getSize() > 0) {
            query.limit(req.getSize());
        }
        query.with(Sort.by(Sort.Direction.DESC, CMDSVideoPlaylist.Constants.CREATION_TIME));

        logger.info("query: " + query);
        return runQuery(query, CMDSVideoPlaylist.class);

    }

    public List<CMDSVideoPlaylist> getVideoPlaylistsForUpcomingVideos(GetCMDSVideoPlaylistReq req) {
        Query query = new Query();
        Long currentTime = System.currentTimeMillis();
        if (ArrayUtils.isNotEmpty(req.getMainTags())) {
            query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(req.getMainTags()));
        }

        if (req.getPublished() != null) {
            query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.PUBLISHED).is(req.getPublished()));
        }

        query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_END_TIME).gte(currentTime));
        return runQuery(query, CMDSVideoPlaylist.class);
    }

    public boolean isLiveVideoAvailable(String grade) {
		Query query = new Query();
		Long currentTime = System.currentTimeMillis();
		if (StringUtils.isNotEmpty(grade)) {
			query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(grade));
		}
		query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.PUBLISHED).is(true));

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_START_TIME).lte(currentTime), Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_END_TIME).gte(currentTime));
        query.addCriteria(andCriteria);

		List<CMDSVideoPlaylist> playlists = runQuery(query, CMDSVideoPlaylist.class);

		if(ArrayUtils.isNotEmpty(playlists)) {
			for(CMDSVideoPlaylist list : playlists) {
				if(ArrayUtils.isNotEmpty(list.getVideos())) {
					for(PlaylistVideoPojo video : list.getVideos()) {
						if(video.isLive() && video.getStartTime() != null && video.getEndTime() != null) {
							if(currentTime > video.getStartTime() && currentTime < video.getEndTime()) {
								return true;
							}
						}
					}
				}
			}
		}
		return false;
	}

    public List<CMDSVideo> getTrendingVideos(GetCMDSVideoPlaylistReq req) throws InternalServerErrorException, BadRequestException {
    	Query query;
    	String trendingVidsRedis = redisDao.get("TRENDING_VIDEOS");
    	List<CMDSVideo> vidList = null;

    	if(StringUtils.isNotEmpty(trendingVidsRedis)) {
    		String[] vidDetails = trendingVidsRedis.split(",");
    		Map<String,Float> vidDetMap  = new HashMap<>();
    		String[] temp;
    		for(String det : vidDetails) {
    			temp = det.split(":");
    			vidDetMap.put(temp[0], Float.parseFloat(temp[1]));
    		}
			/*
			 * vidList = new ArrayList<>(); CMDSVideo cMDSVideo; for(String vidId :
			 * vidDetMap.keySet()) { query = new Query();
			 * query.addCriteria(Criteria.where(CMDSVideo.Constants.ID).is(vidId)); if
			 * (ArrayUtils.isNotEmpty(req.getMainTags())) {
			 * query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(
			 * req.getMainTags())); } logger.info("Trending video search query :: " +
			 * query); cMDSVideo = findOne(query, CMDSVideo.class); if(cMDSVideo!=null) {
			 * vidList.add(cMDSVideo); logger.info("found video :: " + cMDSVideo.getId()); }
			 * }
			 */
    		query = new Query();
    		query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(req.getMainTags()));
    		query.addCriteria(Criteria.where(CMDSVideo.Constants._ID).in(vidDetMap.keySet()));
    		vidList = runQuery(query, CMDSVideo.class);
    	} else {
    		logger.info("Setting up redis values!!!...");
    		Long currentTime = System.currentTimeMillis();
    		query = new Query();
    		if (ArrayUtils.isNotEmpty(req.getMainTags())) {
    			query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(req.getMainTags()));
    		}
			query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.PUBLISHED).is(true));

    		List<CMDSVideoPlaylist> playlistColl =  runQuery(query, CMDSVideoPlaylist.class);
    		logger.info("Query for playlists :: " + query);
    		if(playlistColl==null || !(playlistColl.size()>0)) {
    			logger.info("No playlists found !!!");
    			return null;
    		}

    		CMDSVideo cMDSVideo;
    		float trendingScore;
    		StringBuilder pastViewDetails = new StringBuilder();
    		float perViewsInc = 0;
    		Map<String,Float> videoTrendScore = new HashMap<>();

    		String viewsDetailRedis = redisDao.get("PAST_VIEW_DETAILS");
    		Map<String,Float> vidViewsMap = new HashMap<>();

    		if(StringUtils.isNotEmpty(viewsDetailRedis)) {
    			String[] viewsDetail = viewsDetailRedis.split(",");
    			String[] temp;
    			for(String det: viewsDetail) {
    				temp = det.split(":");
    				if(temp.length == 2) {
						vidViewsMap.put(temp[0], Float.parseFloat(temp[1]));
					}
    			}
    		}

    		float prevViews;
    		query = null;
    		for(CMDSVideoPlaylist playlist : playlistColl) {
    			for(PlaylistVideoPojo video : playlist.getVideos()) {
    				if(!video.isLive()) {
    					query = new Query();
    					query.addCriteria(Criteria.where("_id").is(video.getEntityId()));
    					cMDSVideo = findOne(query, CMDSVideo.class);
    					if(cMDSVideo!=null) {
    						prevViews = 0;
    						if(vidViewsMap.containsKey(cMDSVideo.getId())){
    							prevViews = vidViewsMap.get(cMDSVideo.getId());
    						}
    						if(prevViews>500) {
    							perViewsInc = cMDSVideo.getVideoViews()/prevViews;
    						}else {
    							perViewsInc = 0;
    						}
    						trendingScore = (float) ((cMDSVideo.getCreationTime()>(currentTime-(12*60*60*1000))?1:0)*0.5 + perViewsInc*0.5);
    						videoTrendScore.put(cMDSVideo.getId(), trendingScore);
    						pastViewDetails.append(cMDSVideo.getId()+":"+cMDSVideo.getVideoViews()+",");
    						logger.info("Trend score details :: " + cMDSVideo.getId()+" ----> "+trendingScore);
    					}
    				}
    			}
    		}

    		//storing current views in Redis for future reference
    		pastViewDetails = new StringBuilder(pastViewDetails.substring(0, pastViewDetails.length()-2));
    		redisDao.set("PAST_VIEW_DETAILS", pastViewDetails.toString());

    		//sort map according to trendScore
            Set<Entry<String, Float>> set = videoTrendScore.entrySet();
            List<Entry<String, Float>> list = new ArrayList<Entry<String, Float>>(set);
            Collections.sort( list, new Comparator<Map.Entry<String, Float>>()
            {
                public int compare( Map.Entry<String, Float> o1, Map.Entry<String, Float> o2 )
                {
                    return (o1.getValue()).compareTo( o2.getValue() );
                }
            } );

    		//query to get list in order for trending videos
            StringBuilder trendingVidDetails = new StringBuilder();
    		vidList = new ArrayList<>();
    		query = null;
    		int counter = 0;
    		List<String> vidIds = new ArrayList<>();
    		for(Map.Entry<String, Float> entry:list) {
    			vidIds.add(entry.getKey());
    			counter++;
    			if(counter==20) {
    				break;
    			}
    		}
    		query = new Query();
    		query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(req.getMainTags()));
    		query.addCriteria(Criteria.where(CMDSVideo.Constants._ID).in(vidIds));
    		vidList = runQuery(query, CMDSVideo.class);
			/*
			 * for(Map.Entry<String, Float> entry:list) { query = new Query();
			 * query.addCriteria(Criteria.where("_id").is(entry.getKey()));
			 * logger.info("Video id :: " + entry.getKey() + " Trend score :: " +
			 * entry.getValue());
			 * trendingVidDetails.append(entry.getKey()+":"+entry.getValue()+","); if
			 * (ArrayUtils.isNotEmpty(req.getMainTags())) {
			 * query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(
			 * req.getMainTags())); } cMDSVideo = findOne(query, CMDSVideo.class);
			 * logger.info("Video :: " + cMDSVideo); if(cMDSVideo!=null) {
			 * vidList.add(cMDSVideo); } counter++; if(counter == 20) { break; } }
			 */

    		for(Map.Entry<String, Float> entry:list) {
    			trendingVidDetails.append(entry.getKey()+":"+entry.getValue()+",");
    		}

    		redisDao.setex("TRENDING_VIDEOS", trendingVidDetails.toString().substring(0,trendingVidDetails.length()-2),43200);

    	}

    	return vidList;
    }

    public List<CMDSVideo> getMostViwedVideos(GetCMDSVideoPlaylistReq req) {
    	/*ArrayList<CMDSVideo> videosList = new ArrayList<>();
		 * Criteria criteria = new Criteria(); if
		 * (ArrayUtils.isNotEmpty(req.getMainTags())) {
		 * criteria.andOperator(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).
		 * all(req.getMainTags())); } //top 20 most viewed videos
		 * //db.getCollection('CMDSVideoPlaylist').aggregate([{$unwind: '$videos'}, {
		 * '$sort': { 'videos.videoViews': -1 }}, {'$limit':20}]) Aggregation agg =
		 * newAggregation(Aggregation.unwind("$videos"),sort(Sort.Direction.DESC,
		 * "videos.videoViews"),match(criteria),Aggregation.limit(20));
		 * AggregationResults<CMDSVideoPlaylist> groupResults =
		 * getMongoOperations().aggregate(agg,CMDSVideoPlaylist.class.getName(),
		 * CMDSVideoPlaylist.class);
		 *
		 * //Query query = null; CMDSVideo vid; //getting videos using playlist details
		 * for(CMDSVideoPlaylist playlist :groupResults.getMappedResults()) { query =
		 * new Query();
		 * query.addCriteria(Criteria.where("_id").is(playlist.getVideos().get(0).
		 * getEntityId())); vid = findOne(query, CMDSVideo.class);
		 * logger.info("Video :: " + vid); if(vid!=null) { videosList.add(vid); } }
		 * return videosList;
		 */

    	//getting all the published playlists
    	Query query = new Query();
    	query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.PUBLISHED).is(true));
    	query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.MAIN_TAGS).all(req.getMainTags()));
    	List<CMDSVideoPlaylist> playlists = runQuery(query, CMDSVideoPlaylist.class);
    	Map<String, Long> vidDetails = new HashMap<>();
    	for(CMDSVideoPlaylist playlist : playlists) {
    		for(PlaylistVideoPojo video : playlist.getVideos()) {
    			if(!video.isLive()) {
    				vidDetails.put(video.getEntityId(), video.getVideoViews());
    			}
    		}
    	}

    	//Sorting videos based on views
    	Set<Entry<String, Long>> set = vidDetails.entrySet();
        List<Entry<String, Long>> list = new ArrayList<Entry<String, Long>>(set);
        Collections.sort( list, new Comparator<Map.Entry<String, Long>>()
        {
            public int compare( Map.Entry<String, Long> o1, Map.Entry<String, Long> o2 )
            {
                return (o1.getValue()).compareTo( o2.getValue() );
            }
        } );

        ArrayList<String> vidIds = new ArrayList<>();
        int counter = 0;
        for(Map.Entry<String, Long> entry:list) {
        	vidIds.add(entry.getKey());
        	if(counter==19) {
        		break;
        	}
        	counter++;
        }

        //returning 20 most viewed videos
        Query videoQuery = new Query();
        videoQuery.addCriteria(Criteria.where(CMDSVideo.Constants._ID).in(vidIds));
        return runQuery(videoQuery, CMDSVideo.class);
    }

	public List<CMDSVideoPlaylist> getUserPlaylists(List<String> contextIds, GetCMDSVideoPlaylistReq req){
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSVideoPlaylistOrder.Constants.GRADE).in(req.getMainTags()));

		logger.info("Query - all videos: " + query);

		CMDSVideoPlaylistOrder playListOrder = findOne(query, CMDSVideoPlaylistOrder.class);

		logger.info("playListOrder: " + playListOrder);

		List<CMDSVideoPlaylist> playlists = new ArrayList<>();
		CMDSVideoPlaylist playlist;
		//get playlists
		if(playListOrder != null && ArrayUtils.isNotEmpty(playListOrder.getPlaylists())) {
			Query getPlaylist;
			for(String id: playListOrder.getPlaylists()) {
				getPlaylist = new Query();
				getPlaylist.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants._ID).is(id));
				logger.info("Query - getPlaylist: " + getPlaylist);
				playlist = findOne(getPlaylist, CMDSVideoPlaylist.class);
				playlists.add(playlist);
			}
		}


//		List<CMDSVideoPlaylist> playListCopy = new ArrayList<>();
//		playListCopy.addAll(playlists);

//		//remove unnecessary videos
//		int playlistCount = 0;
//		int videoCount = 0;
//		for(CMDSVideoPlaylist playlist : playlists) {
//			for(PlaylistVideoPojo video : playlist.getVideos()) {
//				if(contextIds.contains(video.getEntityId())) {
//					playListCopy.get(playlistCount).getVideos().remove(videoCount);
//					videoCount--;
//				}
//				videoCount++;
//			}
//			playlistCount++;
//		}

		return playlists;
	}

	public void updateClassroomChat(CMDSVideoPlaylist playlist) {
		Query query;
		ClassroomChat crChat;

		for(PlaylistVideoPojo video : playlist.getVideos()) {
			if(video.isLive()) {
				for(String grade : playlist.gettGrades()){
					query = new Query();
					query.addCriteria(Criteria.where("videoId").is(video.getEntityId()));
					query.addCriteria(Criteria.where("grade").is(grade));
					logger.info("Query :: " + query);
					crChat = findOne(query, ClassroomChat.class);

					if(crChat==null) {
						crChat = new ClassroomChat();
						List<Long> members = new ArrayList<Long>();
						crChat.setMembersList(members);
					}

					crChat.setStartTime(video.getStartTime());
					crChat.setEndTime(video.getEndTime());
					crChat.setGrade(grade);
					crChat.setDuration(video.getDuration());
					crChat.setVideoId(video.getEntityId());
					crChat.setTitle(video.getTitle());
					saveEntity(crChat);
				}
			}
		}
	}

	public List<ClassroomChatMessage> getChatDetails(String videoId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("videoId").is(videoId));
		query.with(Sort.by(Sort.Direction.DESC, CMDSVideo.Constants.CREATION_TIME));
		return runQuery(query, ClassroomChatMessage.class);
	}

	public List<CMDSVideoPlaylist> getCMDSPlaylistsfromVideoID(String videoId) {

		Query query = new Query();
		query.addCriteria(Criteria.where("videos.entityId").is(videoId));
		query.with(Sort.by(Sort.Direction.DESC, CMDSVideo.Constants.CREATION_TIME));

		List<CMDSVideoPlaylist> reqPlaylists = runQuery(query, CMDSVideoPlaylist.class);

		return reqPlaylists;
	}

    public List<CMDSVideo> getMostViewedVideos(GetCMDSVideoPlaylistReq req, int limit) {
        Query mostViewedVideosQuery = new Query();
        mostViewedVideosQuery.addCriteria(Criteria.where(CMDSVideo.Constants.MAIN_TAGS).in(req.getMainTags()))
                .with(Sort.by(Sort.Direction.DESC, CMDSVideo.Constants.VIDEO_VIEWS))
                .limit(limit);
        return runQuery(mostViewedVideosQuery, CMDSVideo.class);
    }

	public List<CMDSVideoPlaylist> getCMDSPlaylistsByGrade(String grade) {
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.GRADES).is(grade));
		query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		query.with(Sort.by(Sort.Direction.DESC, CMDSVideo.Constants.CREATION_TIME));

		List<CMDSVideoPlaylist> reqPlaylists = runQuery(query, CMDSVideoPlaylist.class);

		return reqPlaylists;
	}

}
