package com.vedantu.cmds.dao;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Objects;
import java.util.Set;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTestListOrder;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import org.apache.logging.log4j.Level;
import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.pojo.QuestionApprovalPojo;
import com.vedantu.cmds.pojo.TestMetadata;
import com.vedantu.cmds.request.CheckerQuestionSetOrTestFilterRequest;
import com.vedantu.cmds.request.QuestionApprovalRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.DateTimeUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.pojo.CMDSTestState;
import com.vedantu.cmds.request.GetCMDSTestReq;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

@Service
public class CMDSTestDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CMDSTestDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public CMDSTestDAO() {
		super();
	}

	public void update(CMDSTest test) {
		logger.info("Request : " + test.toString());
		try {
			Assert.notNull(test);
			logger.info("The test is created by {}",test.getCreatedBy());
			saveEntity(test,test.getCreatedBy());
			logger.info("Updated id : " + test.getId());
		} catch (Exception ex) {
			throw new RuntimeException("CMDSTestUpdateError : Error updating the CMDS Test " + test.toString(), ex);
		}
	}

	public void update(CMDSTestListOrder test) {
		logger.info("Request : " + test.toString());
		try {
			Assert.notNull(test);
			saveEntity(test);
			logger.info("Updated id : " + test.getId());
		} catch (Exception ex) {
			throw new RuntimeException("CMDSTestListOrderUpdateError : Error updating the CMDS Test List Order " + test.toString(), ex);
		}
	}
	private void setQueryMaxTimeout(Query query) {
		if (query != null) {
			query.maxTimeMsec(86400000l);
		}
	}

	public CMDSTest getById(String id) throws BadRequestException {
		return getById(id,null);
	}
	public CMDSTest getById(String id,List<String> includeFields) throws BadRequestException {
		logger.info("getById - id: {}", id);
		if(StringUtils.isEmpty(id)){
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Test id cann't be null", Level.ERROR);
		}
		return getEntityById(id, CMDSTest.class,includeFields);
	}

	public List<CMDSTest> getByIds(List<String> testIds) {

		Query testQuery = new Query();
		testQuery.addCriteria(Criteria.where(CMDSTest.Constants._ID).in(testIds));
		List<CMDSTest> results = runQuery(testQuery, CMDSTest.class);
		return results;
	}

	public List<CMDSTest> getByIds(List<String> testIds, List<String> includeFields, int limit) {

		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants._ID).in(testIds));
		query.limit(limit);

		return runQuery(query, CMDSTest.class, includeFields);
	}

	public List<CMDSTest> getTests(GetCMDSTestReq req, List<String> includeFields) {
		Query query = new Query();
		if (!StringUtils.isEmpty(req.getQuestionSetId())) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.QUESTION_SET_ID).is(req.getQuestionSetId()));
		}
		if (req.getVersionNo() != null && req.getVersionNo() > 0) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.VERSION_NUMBER).is(req.getVersionNo()));
		}
		if (!CollectionUtils.isEmpty(req.getTestStates())) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.TEST_STATE).in(req.getTestStates()));
		}
		if (!CollectionUtils.isEmpty(req.getRecordStates())) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.RECORD_STATE).in(req.getRecordStates()));
		}

		if (!CollectionUtils.isEmpty(req.getBoardIds())) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.BOARD_IDS).in(req.getBoardIds()));
		}

		if (!CollectionUtils.isEmpty(req.getTargetIds())) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.TARGET_IDS).in(req.getTargetIds()));
		}

		if (!CollectionUtils.isEmpty(req.getGrades())) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.GRADES).in(req.getGrades()));
		}

		if (!CollectionUtils.isEmpty(req.getTags())) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.TAGS).in(req.getTags()));
		}

		if(req.isVisibleInApp()) {
			query.addCriteria(Criteria.where(CMDSTest.Constants.VISIBLE_IN_APP).is(true));
		}

		query.addCriteria(Criteria.where(CMDSTest.Constants.APPROVAL_STATUS).nin(Arrays.asList(CMDSApprovalStatus.MAKER_APPROVED,CMDSApprovalStatus.TEMPORARY)));

		if (req.getStart() != null) {
			query.skip(req.getStart());
		}

		if (req.getSize() != null && req.getSize() > 0) {
			query.limit(req.getSize());
		}

		query.with(Sort.by(Sort.Direction.DESC, CMDSTest.Constants.CREATION_TIME));

		if(ArrayUtils.isNotEmpty(includeFields)){
			includeFields.forEach(field->query.fields().include(field));
		}
		logger.info("query: "+query);
		return runQuery(query, CMDSTest.class);
	}

	public void deleteTestsByQuestionSet(String questionSetId, String callingUserId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants.QUESTION_SET_ID).is(questionSetId));
		query.addCriteria(Criteria.where(CMDSTest.Constants.TEST_STATE).is(CMDSTestState.PUBLIC));

		List<CMDSTest> results = runQuery(query, CMDSTest.class);
		if (!CollectionUtils.isEmpty(results)) {
			for (CMDSTest entry : results) {
				logger.info("entry : " + entry.toString());
				entry.setLastUpdatedBy(callingUserId);
				entry.setLastUpdated(System.currentTimeMillis());
				entry.setTestState(CMDSTestState.PRIVATE);
				update(entry);
				logger.info("entry updated with id : " + entry.getId());
			}
		}
	}

	public CMDSTest getTestByCode(String testCode) {
		CMDSTest cMDSTest = null;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where(CMDSTest.Constants.CODE).is(testCode));
			List<CMDSTest> results = runQuery(query, CMDSTest.class);
			if (results != null && results.size() > 0) {
				cMDSTest = results.get(0);
			}
		} catch (Exception ex) {
			throw new RuntimeException("getById : Error fetch the test with testCode " + testCode, ex);
		}
		return cMDSTest;
	}

	public List<CMDSTest> getTestByIds(List<String> testIds) {
		return getTestByIds(testIds,null);
	}
	public List<CMDSTest> getTestByIds(List<String> testIds,List<String> includeFields) {
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants._ID).in(testIds));
		return runQuery(query, CMDSTest.class,includeFields);
	}

	public List<CMDSTest> getTestByIdsAndMaxStartTime(List<String> testIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(testIds));
		query.addCriteria(Criteria.where(CMDSTest.Constants.MAX_START_TIME).gt(System.currentTimeMillis()));
		query.with(Sort.by(Sort.Direction.ASC, CMDSTest.Constants.MIN_START_TIME));
		List<CMDSTest> results = runQuery(query, CMDSTest.class);
		return results;
	}

    
    public List<CMDSTest> getCMDSTestsByIdsForValidation(Set<String> testIds){
    	Query query = new Query();
		query.addCriteria(Criteria.where("_id").in(testIds));
		query.fields().include(CMDSTest.Constants.ID);
		query.fields().include(CMDSTest.Constants._ID);
		query.fields().include(CMDSTest.Constants.BOARD_IDS);
		List<CMDSTest> results = runQuery(query, CMDSTest.class);
		return results;   	
    }
        public List<CMDSTest> getObjectTestWithMaxStartTime(Long afterStartTime, Long beforeStartTime){
                Query query = new Query();
                if(beforeStartTime == null){
                    beforeStartTime = System.currentTimeMillis();
                }
                if(afterStartTime == null){
                    return new ArrayList<>();
                }
		query.addCriteria(Criteria.where(CMDSTest.Constants.MAX_START_TIME).gte(afterStartTime)
					.lte(beforeStartTime));
                query.addCriteria(Criteria.where(CMDSTest.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE));
		List<CMDSTest> results = runQuery(query, CMDSTest.class);
		return results;   	
        }

        
	public List<CMDSTest> getTestsWithQuestionId(String questionId, int start, int size){
		return getTestsWithQuestionId(questionId,start,size,null);
	}
	public List<CMDSTest> getTestsWithQuestionId(String questionId, int start, int size,List<String> includeFields){
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants.QUESTION_ID).is(questionId));
		setFetchParameters(query, start, size);

		return runQuery(query, CMDSTest.class, includeFields);

	}

	public List<CMDSTest> getPhaseTestsWithQuestionId(List<String> testIds){
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants.ID).in(testIds));
		query.addCriteria(Criteria.where(CMDSTest.Constants.TEST_TAG).in(Arrays.asList(EnumBasket.TestTagType.PHASE, EnumBasket.TestTagType.UNIT)));
		query.addCriteria(Criteria.where(CMDSTest.Constants.MAX_START_TIME).exists(true));
		return runQuery(query, CMDSTest.class);
	}

	public List<CMDSTest> getCompetitiveTestsBetween(Long startTime, Long endTime, int start, int size){
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE));
		query.addCriteria(Criteria.where(CMDSTest.Constants.TEST_TAG).in(Arrays.asList(EnumBasket.TestTagType.PHASE, EnumBasket.TestTagType.UNIT)));
		if(startTime != null && endTime != null){
			query.addCriteria(Criteria.where(CMDSTest.Constants.MAX_START_TIME).gte(startTime).lt(endTime));
		}
		setFetchParameters(query, start, size);
		return runQuery(query, CMDSTest.class);
	}

	public List<CMDSTest> getCompetitiveTestsIgnoringTagsBetween(Long startTime, Long endTime, int start, int size) throws BadRequestException {
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants.CONTENT_INFO_TYPE).is(ContentInfoType.OBJECTIVE));
		if(startTime != null && endTime != null){
			query.addCriteria(Criteria.where(CMDSTest.Constants.MAX_START_TIME).gte(startTime).lt(endTime));
		}else{
			throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"startTime or endTime parameters cann't be null",Level.ERROR);
		}
		query.addCriteria(Criteria.where(CMDSTest.Constants.ANALYTICS).is(null));
		query.fields().include(CMDSTest.Constants._ID).include(CMDSTest.Constants.MAX_START_TIME).include(CMDSTest.Constants.DURATION);
		setFetchParameters(query, start, size);
		logger.info("getCompetitiveTestsIgnoringTagsBetween - query - {}",query);
		return runQuery(query, CMDSTest.class);
	}

	public List<CMDSTest> getCompetitiveTests(int start, int size){
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants.TEST_TAG).in(Arrays.asList(EnumBasket.TestTagType.PHASE, EnumBasket.TestTagType.UNIT)));
		setFetchParameters(query, start, size);
		return runQuery(query, CMDSTest.class);
	}

	public CMDSTestListOrder getOrderedTestListByGrade(String grade) {
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTestListOrder.Constants.GRADE).is(grade));
		return findOne(query, CMDSTestListOrder.class);
	}

	public List<CMDSTest> getTestsBetween(long fromTime, long toTime) {
		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants.MAX_START_TIME).gte(fromTime).lt(toTime));
		return runQuery(query, CMDSTest.class);
	}

	public void deleteUnnecessaryTests(){
		long currentTime=System.currentTimeMillis();
		long fifteenDaysBefore=currentTime-15*DateTimeUtils.MILLIS_PER_DAY;
		long sixteenDaysBefore=currentTime-16*DateTimeUtils.MILLIS_PER_DAY;
		Query query=new Query();
		query.addCriteria(
				new Criteria().andOperator(
						Criteria.where(CMDSTest.Constants.CREATION_TIME).lte(fifteenDaysBefore),
						Criteria.where(CMDSTest.Constants.CREATION_TIME).gte(sixteenDaysBefore)
				)
		);
		query.addCriteria(
				Criteria.where(CMDSTest.Constants.APPROVAL_STATUS).in(
						Arrays.asList(
								CMDSApprovalStatus.REJECTED,
								CMDSApprovalStatus.TEMPORARY,
								CMDSApprovalStatus.DUPLICATE
						)
				)
		);
		Update update=new Update();
		update.set(CMDSTest.Constants.RECORD_STATE, VedantuRecordState.DELETED);
		updateMulti(query,update,CMDSTest.class);
	}

	public void updateApprovalStatus(QuestionApprovalRequest request) throws BadRequestException {
        if(StringUtils.isEmpty(request.getTestId()) || Objects.isNull(request.getOverallApprovalStatus()) || Objects.isNull(request.getCallingUserId())){
            try {
                throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION,"Please specify valid parameters");
            } catch (BadRequestException e) {
                logger.error("updateApprovalStatus",e);
            }
        }

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants._ID).is(request.getTestId()));


		Update update=new Update();
		update.set(CMDSTest.Constants.APPROVAL_STATUS,request.getOverallApprovalStatus());
		update.set(CMDSTest.Constants.LAST_UPDATED_BY,request.getCallingUserId());

		// Assigning checker for maker
		if(Objects.nonNull(request.getCheckerId()) && CMDSApprovalStatus.CHECKER_APPROVED.equals(request.getOverallApprovalStatus())) {
			CMDSTest cmdsTest=getById(request.getTestId());

			if(Objects.isNull(cmdsTest)){
				try {
					throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION,"Invalid test id "+request.getTestId());
				} catch (BadRequestException e) {
					logger.error("Error in test id ",e);
				}
			}

			Map<String, String> questionDuplicateMap =
					request.getApprovalPojo()
							.stream()
							.filter(question->CMDSApprovalStatus.DUPLICATE.equals(question.getStatus()))
							.collect(Collectors.toMap(
									QuestionApprovalPojo::getId,
									QuestionApprovalPojo::getIsACopyOf
							));

			Map<String, Marks> questionMarksChangedMap =
					request.getApprovalPojo()
							.stream()
							.filter(QuestionApprovalPojo::isMarksChanged)
							.collect(
									Collectors.toMap(
											QuestionApprovalPojo::getId,
											QuestionApprovalPojo::getMarks
									)
							);

			List<TestMetadata> cmdsTestMetadata = cmdsTest.getMetadata();
			cmdsTestMetadata.forEach(metadata->metadata.mutateMetadata(questionDuplicateMap,questionMarksChangedMap));
			float totalMarks = (float)cmdsTestMetadata.stream().mapToDouble(TestMetadata::getTotalMarks).sum();

			update.set(CMDSTest.Constants.TOTAL_MARKS,totalMarks);
			update.set(CMDSTest.Constants.METADATA,cmdsTestMetadata);
			update.set(CMDSTest.Constants.ASSIGNED_CHECKER, request.getCheckerId());
		}

		logger.info("updating approval status of test with `query` {} and `update` {}",query,update);

		updateFirst(query,update,CMDSTest.class);
	}

	public List<CMDSTest> filterTestCheckerData(CheckerQuestionSetOrTestFilterRequest request) {
		logger.info("Request for filtering test checker data is {} ",request);
		Query query=new Query();

		if(Objects.nonNull(request.getTestId()))
			query.addCriteria(Criteria.where(CMDSTest.Constants._ID).is(request.getTestId()));

		if(Objects.nonNull(request.getApprovalStatus())) {
			logger.info("Required approval status is there so going for the same.");
			query.addCriteria(Criteria.where(CMDSTest.Constants.APPROVAL_STATUS).is(request.getApprovalStatus()));
		}else {
			logger.info("Required approval status is not there so going for default ones.");
			query.addCriteria(Criteria.where(CMDSTest.Constants.APPROVAL_STATUS).in(Arrays.asList(
					CMDSApprovalStatus.MAKER_APPROVED,
					CMDSApprovalStatus.CHECKER_APPROVED,
					CMDSApprovalStatus.REJECTED
			)));
		}

		if(Objects.nonNull(request.getCheckerId())){
			logger.info("As the checker id is given going for checker id data only.");
			query.addCriteria(Criteria.where(CMDSTest.Constants.ASSIGNED_CHECKER).is(request.getCheckerId()));
		}else{
			query.addCriteria(Criteria.where(CMDSTest.Constants.ASSIGNED_CHECKER).ne(null));
		}

		if(Objects.nonNull(request.getTitle())){
			logger.info("Filtering also with the basis of title");
			query.addCriteria(Criteria.where(CMDSTest.Constants.NAME).is(request.getTitle()));
		}

		if(Objects.nonNull(request.getMakerId())){
			logger.info("Filtering on the basis of maker id also");
			query.addCriteria(Criteria.where(CMDSTest.Constants.CREATED_BY).is(request.getMakerId()));
		}

		if(request.isActionTimeSort()){
			logger.info("Sorting on the basis of last updated");
			query.with(Sort.by(Sort.Direction.DESC, CMDSTest.Constants.LAST_UPDATED));
		}else{
			logger.info("Sorting on the basis of creation time");
			query.with(Sort.by(Sort.Direction.DESC, CMDSTest.Constants.CREATION_TIME));
		}

		query.addCriteria(Criteria.where(CMDSTest.Constants.RECORD_STATE).ne(VedantuRecordState.DELETED));

		query.fields().include(CMDSTest.Constants.QUESTION_SET_ID);
		query.fields().include(CMDSTest.Constants.ASSIGNED_CHECKER);
		query.fields().include(CMDSTest.Constants.APPROVAL_STATUS);
		query.fields().include(CMDSTest.Constants.NAME);
		query.fields().include(CMDSTest.Constants.CREATED_BY);
		query.fields().include(CMDSTest.Constants.CREATION_TIME);
		query.fields().include(CMDSTest.Constants.LAST_UPDATED);

		query.skip(request.getSkip());
		query.limit(request.getLimit());

		logger.info("final query of filterTestCheckerData is {}",query);
		return runQuery(query, CMDSTest.class);

	}

	public CMDSTest getOnlyCheckerApprovedData(String testId) {
		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTest.Constants._ID).is(testId));
		query.addCriteria(Criteria.where(CMDSTest.Constants.APPROVAL_STATUS).in(Arrays.asList(CMDSApprovalStatus.CHECKER_APPROVED,null)));
		return findOne(query,CMDSTest.class);
	}
}
