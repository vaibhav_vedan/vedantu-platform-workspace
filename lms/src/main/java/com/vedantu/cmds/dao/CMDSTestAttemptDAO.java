package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.pojo.TestFeedback;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.board.pojo.Board;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@Service
public class CMDSTestAttemptDAO extends AbstractMongoDAO {

	private Logger logger = LogFactory.getLogger(CMDSTestAttemptDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Autowired
	private FosUtils fosUtils;

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public CMDSTestAttemptDAO() {
		super();
	}

	public void update(CMDSTestAttempt testAttempt) {
		logger.info("Request for test attempt id {} ", testAttempt.getId());
		try {
			saveEntity(testAttempt);
			logger.info("Updated id : " + testAttempt.getId());
		} catch (Exception ex) {
			throw new RuntimeException(
					"testAttemptUpdateError : Error updating the CMDS Test attempt " + testAttempt.toString(), ex);
		}
	}

	public CMDSTestAttempt getById(String id) {
		logger.info("id:" + id);
		CMDSTestAttempt questionSet = null;
		try {
			questionSet = getEntityById(id, CMDSTestAttempt.class);
		} catch (Exception ex) {
			throw new RuntimeException("CMDSTestFetchError : Error fetch the CMDS Test with id " + id, ex);
		}
		return questionSet;
	}

//	public void insertAllEntities(List<CMDSTestAttempt> testAttempts) {
//		if(ArrayUtils.isNotEmpty(testAttempts)){
//			insertAllEntities(testAttempts,CMDSTestAttempt.class.getSimpleName());
//		}
//	}

	public void saveAllTheAttemptsIfNecessary(ContentInfo contentInfo) {
		logger.info("saveAllTheAttemptsIfNecessary");
		TestContentInfoMetadata metadata = contentInfo.getMetadata();
		if (Objects.nonNull(metadata)) {
			List<CMDSTestAttempt> testAttempts = metadata.getTestAttempts();
			if (ArrayUtils.isNotEmpty(testAttempts)) {
				String studentId = contentInfo.getStudentId();
				String contentInfoId = contentInfo.getId();
				ContentInfoType contentInfoType = contentInfo.getContentInfoType();
				Set<String> contentInfoSubjects = contentInfo.getSubjects();
				if (ArrayUtils.isEmpty(contentInfoSubjects) && ArrayUtils.isNotEmpty(contentInfo.getBoardIds())) {
					Map<Long, Board> boardInfoMap = fosUtils.getBoardInfoMap(contentInfo.getBoardIds());
					contentInfoSubjects =
							Optional.ofNullable(boardInfoMap.values())
									.map(Collection::stream)
									.orElseGet(Stream::empty)
									.map(Board::getName)
									.collect(Collectors.toSet());

				}
				int attemptIndex = 0;
				for (CMDSTestAttempt cmdsTestAttempt : testAttempts) {
					cmdsTestAttempt.setContentInfoId(contentInfoId);
					cmdsTestAttempt.setStudentId(studentId);
					cmdsTestAttempt.setAttemptIndex(++attemptIndex);
					cmdsTestAttempt.setContentInfoType(contentInfoType);
					cmdsTestAttempt.setSubjects(contentInfoSubjects);
					update(cmdsTestAttempt);
				}


			}
		}
	}

	public void save(CMDSTestAttempt cmdsTestAttempt) {
		if(Objects.nonNull(cmdsTestAttempt)){
			saveEntity(cmdsTestAttempt);
		}
	}

	public List<CMDSTestAttempt> getTestAttemptsInProgress(Set<String> contentInfoIds, final List<String> includeFields) throws BadRequestException {
		if (contentInfoIds.size() > 250) {
			throw new BadRequestException(ErrorCode.IMPERMISSIBLE_SIZE_PER_REQUEST, "Max fetch size limit exceeded");
		}

		Query query = new Query();

		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.CONTENT_INFO_ID).in(contentInfoIds));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).is(CMDSAttemptState.ATTEMPT_STARTED));

		return runQuery(query, CMDSTestAttempt.class, includeFields);
	}


	public void insertAllEntities(List<CMDSTestAttempt> testAttempts) {
		if(ArrayUtils.isNotEmpty(testAttempts)){
			insertAllEntities(testAttempts,CMDSTestAttempt.class.getSimpleName());
		}
	}

	public CMDSTestAttempt getLatestAttempt(String contentInfoId) {

		if(StringUtils.isEmpty(contentInfoId)){
			return null;
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.CONTENT_INFO_ID).is(contentInfoId));
		query.with(Sort.by(Sort.Direction.DESC, CMDSTestAttempt.Constants.CREATION_TIME));
		return findOne(query,CMDSTestAttempt.class);
	}

	public CMDSTestAttempt getTestAttemptById(String testAttemptId, List<String> includeFields){
		if(StringUtils.isEmpty(testAttemptId)){
			return null;
		}
		return getEntityById(testAttemptId,CMDSTestAttempt.class,includeFields);

	}

	public List<ObjectId> getAttemptsForExpiration(Long tillTime, List<ContentInfoType> contentInfoTypes) throws BadRequestException {

		if(Objects.isNull(tillTime) || ArrayUtils.isEmpty(contentInfoTypes)){
			throwInvalidParameterException();
		}

		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).is(CMDSAttemptState.ATTEMPT_STARTED));

		// Query Optimisation added from time as the query was fetching all the tests upto tillTime.
		Long fromTime = tillTime - DateTimeUtils.MILLIS_PER_HOUR;
		Criteria andCriteria = new Criteria().andOperator(
				Criteria.where(CMDSTestAttempt.Constants.END_TIME).lt(tillTime),
				Criteria.where(CMDSTestAttempt.Constants.END_TIME).gt(fromTime)
		);
		query.addCriteria(andCriteria);
		if (ArrayUtils.isNotEmpty(contentInfoTypes)) {
			query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.CONTENT_INFO_TYPE).in(contentInfoTypes));
		}
		logger.info("getAttemptsForExpiration - query {}", query);
		return findDistinct(query, CMDSTestAttempt.Constants._ID,CMDSTestAttempt.class,ObjectId.class);
	}

	public List<CMDSTestAttempt> getEndedAttemptsForContentInfoId(String contentInfoId,Integer start, Integer size, List<String> includeFields) throws BadRequestException {

		if(StringUtils.isEmpty(contentInfoId) || Objects.isNull(start) || Objects.isNull(size)){
			throwInvalidParameterException();
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.CONTENT_INFO_ID).is(contentInfoId));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).is(CMDSAttemptState.EVALUATE_COMPLETE));

		query.skip(start);
		query.limit(size);

		return runQuery(query,CMDSTestAttempt.class,includeFields);
	}

	public void updateResultEntriesForTestAttempt(String id, List<QuestionAnalytics> resultEntries) throws BadRequestException {

		if(StringUtils.isEmpty(id) || ArrayUtils.isEmpty(resultEntries)){
			throwInvalidParameterException();
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants._ID).is(id));

		Update update=new Update();
		update.set(CMDSTestAttempt.Constants.RESULT_ENTRIES,resultEntries);

		updateFirst(query,update,CMDSTestAttempt.class);
	}

	private void throwInvalidParameterException() throws BadRequestException {
		throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"None of the parameter could be null valued");
	}

	public List<CMDSTestAttempt> getTestAttemptsByContentInfoId(String contentInfoId, List<String> includeFields) throws BadRequestException {
		if(StringUtils.isEmpty(contentInfoId)){
			throwInvalidParameterException();
		}
		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.CONTENT_INFO_ID).is(contentInfoId));

		query.limit(10);

		query.with(Sort.by(Sort.Direction.DESC,CMDSTestAttempt.Constants._ID));

		return runQuery(query,CMDSTestAttempt.class,includeFields);
	}

	public List<CMDSTestAttempt> getTestAttemptsByContentInfoIdsOnlyForTestIdsAndMarksAchieved(List<String> contentInfoIds) throws BadRequestException {
		if(ArrayUtils.isEmpty(contentInfoIds)){
			throwInvalidParameterException();
		}
		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.CONTENT_INFO_ID).in(contentInfoIds));

		query.fields().include(CMDSTestAttempt.Constants.TEST_ID);
		query.fields().include(CMDSTestAttempt.Constants.MARKS_ACHIEVED);

		query.fields().exclude(CMDSTestAttempt.Constants._ID);

		return runQuery(query,CMDSTestAttempt.class);
	}

	public void updateTeacherIdForTestAttempt(String testId, String teacherId) throws BadRequestException {
		if(StringUtils.isEmpty(teacherId) || StringUtils.isEmpty(testId)){
			throwInvalidParameterException();
		}
		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).in(Arrays.asList(CMDSAttemptState.ATTEMPT_STARTED,CMDSAttemptState.ATTEMPT_COMPLETE)));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.TEST_ID).is(testId));

		Update update=new Update();
		update.set(CMDSTestAttempt.Constants.TEACHER_ID,teacherId);


		updateMulti(query,update,CMDSTestAttempt.class);
	}

	public List<CMDSTestAttempt> getAttemptDetailsForAnalyticsCalculation(String testId, Long maxStartTime, int start, int size) throws BadRequestException {

		if(StringUtils.isEmpty(testId) || Objects.isNull(maxStartTime) || size==0 || start<0){
			throwInvalidParameterException();
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.TEST_ID).is(testId));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).is(CMDSAttemptState.EVALUATE_COMPLETE));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.STARTED_AT).lte(maxStartTime));

		query.fields().include(CMDSTestAttempt.Constants.CATEGORY_ANALYTICS_LIST);
		query.fields().exclude(CMDSTestAttempt.Constants._ID);

		query.skip(start);
		query.limit(size);

		return runQuery(query,CMDSTestAttempt.class);
	}


	public List<CMDSTestAttempt> getFirstValidEndedTestAttemptsForTestId(String testId, Long maxStartTime, int skip, int limit) throws BadRequestException {

		if(StringUtils.isEmpty(testId) || Objects.isNull(maxStartTime) || skip<0 || limit==0){
			throwInvalidParameterException();
		}


		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.TEST_ID).is(testId));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.STARTED_AT).lte(maxStartTime));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).is(CMDSAttemptState.EVALUATE_COMPLETE));

		query.skip(skip);
		query.limit(limit);

		query.with(Sort.by(Sort.Direction.DESC,CMDSTestAttempt.Constants.MARKS_ACHIEVED));

		query.fields().include(CMDSTestAttempt.Constants.MARKS_ACHIEVED)
				.include(CMDSTestAttempt.Constants.TIME_TAKEN_BY_STUDENT)
				.include(CMDSTestAttempt.Constants.TOTAL_MARKS);

		return runQuery(query,CMDSTestAttempt.class);
	}

	public long getAttemptsforRankCalculation(String testId, Long maxStartTime) throws BadRequestException {
		if(StringUtils.isEmpty(testId) || Objects.isNull(maxStartTime)){
			throwInvalidParameterException();
		}


		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.TEST_ID).is(testId));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.STARTED_AT).lte(maxStartTime));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).is(CMDSAttemptState.EVALUATE_COMPLETE));

		return queryCount(query,CMDSTestAttempt.class);
	}

	public void updateBatchAvgScore(String testId, Long maxStartTime, int batchAvg) throws BadRequestException {
		if(StringUtils.isEmpty(testId) || Objects.isNull(maxStartTime)){
			throwInvalidParameterException();
		}


		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.TEST_ID).is(testId));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.STARTED_AT).lte(maxStartTime));
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.ATTEMPT_STATE).is(CMDSAttemptState.EVALUATE_COMPLETE));


		Update update=new Update();
		update.set(CMDSTestAttempt.Constants.BATCH_AVG,batchAvg);

		logger.info("updateBatchAvgScore - query - {}",query);
		logger.info("updateBatchAvgScore - update - {}",update);

		updateMulti(query,update,CMDSTestAttempt.class);

	}

	public void updateRankRelatedDataForAttemptId(String id, Float attemptMarksPercent, int currentRank, float testPercentile, int totalAttempts) throws BadRequestException {
		if(StringUtils.isEmpty(id) || Objects.isNull(attemptMarksPercent) || currentRank<0 || testPercentile<0 || totalAttempts==0){
			throwInvalidParameterException();
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants._ID).is(id));


		Update update=new Update();
		update.set(CMDSTestAttempt.Constants.ATTEMPT_MARKS_PERCENTAGE,attemptMarksPercent);
		update.set(CMDSTestAttempt.Constants.RANK,currentRank);
		update.set(CMDSTestAttempt.Constants.TEST_PERCENTILE,testPercentile);
		update.set(CMDSTestAttempt.Constants.TOTAL_STUDENTS,totalAttempts);

		logger.info("updateRankRelatedDataForAttemptId - query - {}",query);
		logger.info("updateRankRelatedDataForAttemptId - update - {}",update);

		updateFirst(query,update,CMDSTestAttempt.class);
	}

	public void updateTestAttemptFeedbackForAttemptId(String id, TestFeedback feedback) throws BadRequestException {
		if(StringUtils.isEmpty(id) || Objects.isNull(feedback)){
			throwInvalidParameterException();
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants._ID).is(id));


		Update update=new Update();
		update.set(CMDSTestAttempt.Constants.FEEDBACK,feedback);

		updateFirst(query,update,CMDSTestAttempt.class);
	}

	public int getNoOfAttemptsForContentInfo(String contentInfoId) throws BadRequestException {
		if(StringUtils.isEmpty(contentInfoId)){
			throwInvalidParameterException();
		}

		Query query=new Query();
		query.addCriteria(Criteria.where(CMDSTestAttempt.Constants.CONTENT_INFO_ID).is(contentInfoId));

		return (int)queryCount(query,CMDSTestAttempt.class);
	}
}

