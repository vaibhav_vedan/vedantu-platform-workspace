/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.VideoSchedule;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class VideoScheduleDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VideoScheduleDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public VideoScheduleDAO() {
        super();
    }

    public void replaceAll(List<VideoSchedule> schedules, String callingUserId) {
        logger.info("deleting all entities");
        Query query = new Query();
        Update update = new Update();
        update.set(VideoSchedule.Constants.ENTITY_STATE, EntityState.DELETED);
        update.set(VideoSchedule.Constants.LAST_UPDATED_BY, callingUserId);
        updateMulti(query, update, VideoSchedule.class);
        logger.info("inserting new ones");
        insertAllEntities(schedules, VideoSchedule.class.getSimpleName(), callingUserId);
    }

    public List<VideoSchedule> getVideoSchedules(Set<String> mainTags, Long fromTime,
            Long tillTime, Integer start, Integer size) {
        Query query = new Query();
        if (ArrayUtils.isNotEmpty(mainTags)) {
            query.addCriteria(Criteria.where(VideoSchedule.Constants.MAIN_TAGS).all(mainTags));
        }
        if (fromTime == null && tillTime == null) {
            query.addCriteria(Criteria.where(VideoSchedule.Constants.SCHEDULE_TIME).gte(System.currentTimeMillis()));
        } else {
            if (fromTime != null && tillTime != null) {
                query.addCriteria(Criteria.where(VideoSchedule.Constants.SCHEDULE_TIME).gte(fromTime)
                        .lte(tillTime));
            } else if (fromTime != null) {
                query.addCriteria(Criteria.where(VideoSchedule.Constants.SCHEDULE_TIME).gte(fromTime));
            } else if (tillTime != null) {
                query.addCriteria(Criteria.where(VideoSchedule.Constants.SCHEDULE_TIME).lte(tillTime));
            }
        }
        query.addCriteria(Criteria.where(VideoSchedule.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.with(Sort.by(Sort.Direction.ASC, VideoSchedule.Constants.SCHEDULE_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, VideoSchedule.class);
    }
}
