/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.Book;
import com.vedantu.cmds.request.GetBooksReq;
import com.vedantu.cmds.utils.CMDSCommonUtils;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class BookDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BookDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public BookDAO() {
        super();
    }

    public void save(Book book) {
        try {
            saveEntity(book);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "Error creating book " + book, ex);
        }
    }
    
    public Book getBook(String id, String bookName){
    	return getBook(id,bookName,null);
    }

    public Book getBook(String id, String bookName,List<VedantuRecordState> states) {
        if (StringUtils.isNotEmpty(id)) {
            return getEntityById(id, Book.class);
        } else {
            Query query = new Query();
            query.addCriteria(Criteria.where(Book.Constants.BOOK_SLUG).is(CMDSCommonUtils.makeSlug(bookName)));
            if(ArrayUtils.isNotEmpty(states)){
            	query.addCriteria(Criteria.where(Book.Constants.RECORD_STATE).in(states));
            }else {
            	query.addCriteria(Criteria.where(Book.Constants.RECORD_STATE).is(VedantuRecordState.ACTIVE));
            }
            logger.info("Query " + query);
            return findOne(query, Book.class);
        }
    }

    public List<Book> getBooks(GetBooksReq req,boolean fectchAll) {
        req.setSize(100);//will hard code this
        Query query = new Query();
        if (StringUtils.isNotEmpty(req.getGrade())) {
            query.addCriteria(Criteria.where(Book.Constants.GRADES).is(req.getGrade()));
        }
        if (StringUtils.isNotEmpty(req.getTarget())) {
            query.addCriteria(Criteria.where(Book.Constants.TARGETS).is(req.getTarget()));
        }
        if (StringUtils.isNotEmpty(req.getSubject())) {
            query.addCriteria(Criteria.where(Book.Constants.SUBJECT).is(req.getSubject()));
        }
        if(!fectchAll){
            query.addCriteria(Criteria.where(Book.Constants.RECORD_STATE).in(Arrays.asList(VedantuRecordState.ACTIVE,VedantuRecordState.DELETED)));
        }
        setFetchParameters(query, req);
        logger.info("Query " + query);
        return runQuery(query, Book.class);
    }

}
