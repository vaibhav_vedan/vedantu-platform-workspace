
package com.vedantu.cmds.dao.challenges;

import java.util.List;

import com.vedantu.cmds.enums.Difficulty;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.entities.challenges.PictureChallenge;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.pojo.challenges.ChallengeStatusAggResult;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SortOrder;
import java.util.Set;
import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Update;

@Service
public class PictureChallengeDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PictureChallengeDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public PictureChallengeDAO() {
        super();
    }

    public void savePictureChallenge(PictureChallenge pictureChallenge) {
        try {
            saveEntity(pictureChallenge);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ContentInfoUpdateError : Error updating the content info " + pictureChallenge, ex);
        }
    }

    public PictureChallenge getById(String id) {
        PictureChallenge pictureChallenge = null;
        try {
            pictureChallenge = (PictureChallenge) getEntityById(id, PictureChallenge.class);
        } catch (Exception ex) {
            throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
        }
        return pictureChallenge;
    }

    public List<PictureChallenge> getByIds(List<String> pictureChallengeIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.ID).in(pictureChallengeIds));

        List<PictureChallenge> results = runQuery(query, PictureChallenge.class);
        return results;
    }

    public void delete(String id) {
        deleteEntityById(id, PictureChallenge.class);
    }

    public List<PictureChallenge> getPictureChallenges(Integer start, Integer size,
            ChallengeStatus status, String sortOrder,
            String orderBy, Difficulty difficulty,
            Set<String> takenPictureChallengeIds, String category, String challengeId) {

        Query query = new Query();
        if (status != null) {
            query.addCriteria(Criteria.where(PictureChallenge.Constants.STATUS).is(status));
        }

        if (difficulty != null) {
            query.addCriteria(Criteria.where(PictureChallenge.Constants.DIFFICULTY).is(difficulty));
        }

        if (ArrayUtils.isNotEmpty(takenPictureChallengeIds)) {
            query.addCriteria(Criteria.where(AbstractMongoEntity.Constants._ID).in(takenPictureChallengeIds));
        }else if(StringUtils.isNotEmpty(challengeId)) {
        	query.addCriteria(Criteria.where(AbstractMongoEntity.Constants._ID).is(challengeId));
        }

        if (StringUtils.isNotEmpty(category)) {
            query.addCriteria(Criteria.where(PictureChallenge.Constants.CATEGORY).is(category));
        }    
        setFetchParameters(query, start, size);

        if (StringUtils.isNotEmpty(orderBy) && StringUtils.isNotEmpty(sortOrder)) {
            SortOrder sortOrder1 = SortOrder.valueQuitentOf(sortOrder);
            if(orderBy.equals("lastUpdated")){
				orderBy = "lastUpdatedTime";
			}
            if (Sort.Direction.ASC.name().equals(sortOrder1.name())) {
                query.with(Sort.by(Sort.Direction.ASC, orderBy));
            } else {
                query.with(Sort.by(Sort.Direction.DESC, orderBy));
            }
        } else {
            query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.LAST_UPDATED));
        }

        List<PictureChallenge> results = runQuery(query, PictureChallenge.class);
        return results;
    }

    public List<PictureChallenge> getBurstablePictureChallenges() {
        Query query = new Query();
        query.addCriteria(Criteria.where(PictureChallenge.Constants.STATUS).is(ChallengeStatus.ACTIVE));
        query.addCriteria(Criteria.where(PictureChallenge.Constants.BURST_TIME).lt(System.currentTimeMillis()));
        setFetchParameters(query, 0, 100);
        List<PictureChallenge> results = runQuery(query, PictureChallenge.class);
        if (ArrayUtils.isNotEmpty(results) && results.size() >= 100) {
            logger.error("Too many burstable pictureChallenges, change logic");
        }
        return results;
    }

    public void markPictureChallengesActive() {
        Query query = new Query();
        query.addCriteria(Criteria.where(PictureChallenge.Constants.STATUS).is(ChallengeStatus.DRAFT));
        query.addCriteria(Criteria.where(PictureChallenge.Constants.START_TIME).lt(System.currentTimeMillis()));
        Update update = new Update();
        update.set(PictureChallenge.Constants.STATUS, ChallengeStatus.ACTIVE);
        updateMulti(query, update, PictureChallenge.class);
    }
    
    public List<ChallengeStatusAggResult> getCounts() {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(
                group("status").count().as("count").sum("maxPoints").as("points"),
                project("count", "points").and("status").previousOperation()).withOptions(aggregationOptions);
        AggregationResults<ChallengeStatusAggResult> groupResults
                = getMongoOperations().aggregate(agg, PictureChallenge.class.getSimpleName(), ChallengeStatusAggResult.class);
        List<ChallengeStatusAggResult> result = groupResults.getMappedResults();
        return result;
    }
    
//    public List<ActivePictureChallengeIds> getActiveSinglePictureChallenges() {
//    	Criteria criteria = new Criteria();
//    	criteria = Criteria.where(PictureChallenge.Constants.STATUS).is(PictureChallengeStatus.ACTIVE.name());
//        Aggregation agg = newAggregation(Aggregation.match(criteria),
//        		Aggregation.sort(Sort.Direction.DESC, "startTime"),
//                group(PictureChallenge.Constants.CATEGORY).first(Aggregation.ROOT).as("pictureChallenge"));
//        AggregationResults<ActivePictureChallengeIds> groupResults
//                = getMongoOperations().aggregate(agg, PictureChallenge.class.getSimpleName(), ActivePictureChallengeIds.class);
//        List<ActivePictureChallengeIds> result = groupResults.getMappedResults();
//        return result;
//    }
  
}
