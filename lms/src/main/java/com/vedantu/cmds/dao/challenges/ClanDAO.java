package com.vedantu.cmds.dao.challenges;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.entities.challenges.Clan;
import com.vedantu.cmds.request.clans.GetClansReq;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SortOrder;

@Service
public class ClanDAO extends AbstractMongoDAO{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ClanDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public ClanDAO() {
        super();
    }

    public void saveClan(Clan Clan) {
        try {
            saveEntity(Clan);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ContentInfoUpdateError : Error updating the content info " + Clan, ex);
        }
    }

    public Clan getById(String id) {
        Clan clan = null;
        try {
        	clan = (Clan) getEntityById(id, Clan.class);
        } catch (Exception ex) {
            throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
        }
        return clan;
    }

    public List<Clan> getByIds(List<String> clanIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.ID).in(clanIds));

        List<Clan> results = runQuery(query, Clan.class);
        return results;
    }

    public void delete(String id) {
        deleteEntityById(id, Clan.class);
    }
    
    public Clan getByRefCode(String id) {
        Clan clan = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(Clan.Constants.SHARE_CODE).is(id));

        List<Clan> results = runQuery(query, Clan.class);
        if(ArrayUtils.isNotEmpty(results)){
        	clan = results.get(0);
        }
        return clan;
    }
    
    public List<Clan> getByCategories(String category) {
        Query query = new Query();
        List<Clan> clans = new ArrayList<>();
        boolean process = true;
        int start = 0;
        int size = 200;
        while(process){
        	List<Clan> results = getByCategories(category,start,size);
        	if(ArrayUtils.isNotEmpty(results)){
        		clans.addAll(results);
        		start += results.size();
        	}else {
        		process = false;
        	}
        }
        return clans;
    }
    
    public List<Clan> getByCategories(String category,int start,int size){
    	Query query = new Query();
        query.addCriteria(Criteria.where(Clan.Constants.ALLOWED_CATEGORIES).in(category)); 
        
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.ID));
        setFetchParameters(query,start,size);
        List<Clan> results = runQuery(query, Clan.class);
        return results;
    }
    
    public List<Clan> getClans(GetClansReq req) {
        Query query = new Query();
        if(req.getCategory() != null){
        	query.addCriteria(Criteria.where(Clan.Constants.ALLOWED_CATEGORIES).is(req.getCategory())); 
        }
        if(ArrayUtils.isNotEmpty(req.getShareCodes())){
        	query.addCriteria(Criteria.where(Clan.Constants.SHARE_CODE).in(req.getShareCodes()));
        }
        setFetchParameters(query, req);

        if (StringUtils.isNotEmpty(req.getOrderBy()) && StringUtils.isNotEmpty(req.getSortOrder())) {
            SortOrder sortOrder1 = SortOrder.valueQuitentOf(req.getSortOrder());
            if (Sort.Direction.ASC.name().equals(sortOrder1.name())) {
                query.with(Sort.by(Sort.Direction.ASC, req.getOrderBy()));
            } else {
                query.with(Sort.by(Sort.Direction.DESC, req.getOrderBy()));
            }
        } else {
            query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.LAST_UPDATED));
        }      
        List<Clan> results = runQuery(query, Clan.class);
        return results;
    }
}
