/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.CompetitiveTestStats;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 *
 * @author parashar
 */
@Service
public class CompetitiveTestStatsDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;
    
    public CompetitiveTestStatsDAO(){
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CompetitiveTestStatsDAO.class);

    public void save(CompetitiveTestStats CompetitiveStats) {
        try {
            Assert.notNull(CompetitiveStats);
            saveEntity(CompetitiveStats);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ContentInfoUpdateError : Error updating the content info " + CompetitiveStats.toString(), ex);
        }
    }

    public CompetitiveTestStats getById(String id) {
        CompetitiveTestStats competitiveStats = null;
        try {
            Assert.hasText(id);
            competitiveStats = (CompetitiveTestStats) getEntityById(id, CompetitiveTestStats.class);
        } catch (Exception ex) {
            throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
        }
        return competitiveStats;
    }

    public List<CompetitiveTestStats> getCompetitiveTestStatses(List<String> testIds){
        
        if(ArrayUtils.isEmpty(testIds)){
            return new ArrayList<>();
        }
        
        List<CompetitiveTestStats> competitiveTestStatses = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where("cmdsTestId").in(testIds));
        competitiveTestStatses = runQuery(query, CompetitiveTestStats.class);
        if(competitiveTestStatses == null){
            return new ArrayList<>();
        }
        return competitiveTestStatses;
    }

    public CompetitiveTestStats getCompetitiveTestStats(String testId){
        Query query = new Query();
        query.addCriteria(Criteria.where("cmdsTestId").is(testId));
        return findOne(query, CompetitiveTestStats.class);
    }
    
}
