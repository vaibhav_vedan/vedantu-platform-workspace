/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.StudyEntry;
import com.vedantu.cmds.entities.StudyEntryItem;
import com.vedantu.cmds.pojo.ChapterPdf;
import com.vedantu.cmds.pojo.StudySubjectOrBook;
import com.vedantu.exception.ErrorCode;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Sort;

/**
 *
 * @author ajith
 */
@Service
public class StudyDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(StudyDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public StudyDAO() {
        super();
    }

    public void replaceAll(List<StudyEntry> entities, String callingUserId, boolean replaceAll) {
        if (replaceAll) {
            logger.info("deleting all entities");
            Query query = new Query();
            query.addCriteria(Criteria.where(StudyEntry.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
            Update update = new Update();
            update.set(StudyEntry.Constants.ENTITY_STATE, EntityState.DELETED);
            update.set(StudyEntry.Constants.LAST_UPDATED_BY, callingUserId);
            updateMulti(query, update, StudyEntry.class);
        }
        logger.info("inserting new ones " + entities.size());
        insertAllEntities(entities, StudyEntry.class.getSimpleName(), callingUserId);
    }

    public void replaceAllStudyEntryItems(List<StudyEntryItem> studyEntryItems, String callingUserId, boolean replaceAll) {
        if (replaceAll) {
            logger.info("replaceAllStudyEntryItems deleting all entities");
            Query query = new Query();
            query.addCriteria(Criteria.where(StudyEntry.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
            Update update = new Update();
            update.set(StudyEntry.Constants.ENTITY_STATE, EntityState.DELETED);
            update.set(StudyEntry.Constants.LAST_UPDATED_BY, callingUserId);
            updateMulti(query, update, StudyEntryItem.class);
        }
        logger.info("replaceAllStudyEntryItems inserting new ones " + studyEntryItems.size());
        insertAllEntities(studyEntryItems, StudyEntryItem.class.getSimpleName(), callingUserId);
    }

    public List<StudyEntry> getStudyWithSubjects(Set<String> mainTags) {
        Query query = new Query();
        if (ArrayUtils.isNotEmpty(mainTags)) {
            query.addCriteria(Criteria.where(StudyEntry.Constants.MAIN_TAGS).all(mainTags));
        }
        query.addCriteria(Criteria.where(StudyEntry.Constants.ENTITY_STATE).is(EntityState.ACTIVE.name()));
        query.fields().include(StudyEntry.Constants.ID);
        query.fields().include(StudyEntry.Constants.GRADES);
        query.fields().include(StudyEntry.Constants.TARGETS);
        query.fields().include(StudyEntry.Constants.TITLE);
        query.fields().include(StudyEntry.Constants.TOP_HEADING_NAME);
        query.fields().include(StudyEntry.Constants.SUBJECT_OR_BOOKS_CHAPTER_COUNT);
        query.fields().include(StudyEntry.Constants.SUBJECT_OR_BOOKS_PDF_COUNT);
        query.fields().include(StudyEntry.Constants.SUBJECT_OR_BOOKS_TITLE);
        query.fields().include(StudyEntry.Constants.BOTTOM_TEXT);
        query.with(Sort.by(Sort.Direction.ASC, StudyEntry.Constants.CREATION_TIME));
        logger.info("Query : {}", query);
        return runQuery(query, StudyEntry.class);
    }

    public List<ChapterPdf> getChapterPdfs(String studyEntryId, String title) throws NotFoundException {
        StudyEntry entry = getEntityById(studyEntryId, StudyEntry.class);
        if (entry == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No study entry found with id");
        }
        //hackish way
        List<ChapterPdf> chapterPdfs = new ArrayList<>();

        if (ArrayUtils.isNotEmpty(entry.getSubjectOrBooks())) {
            for (StudySubjectOrBook subjectOrBook : entry.getSubjectOrBooks()) {
                logger.info(subjectOrBook.getTitle());
                logger.info(subjectOrBook.getChapters());
                if (StringUtils.equalsIgnoreCase(title, subjectOrBook.getTitle())
                        && ArrayUtils.isNotEmpty(subjectOrBook.getChapters())) {
                    chapterPdfs = subjectOrBook.getChapters();
                    break;
                }
            }
        } else {
            if (ArrayUtils.isNotEmpty(entry.getChapters())) {
                return entry.getChapters();
            }
        }
        return chapterPdfs;
    }

    public List<StudyEntryItem> getStudyEntryItems(List<String> ids) {
        if (ids == null) {
            return new ArrayList<>();
        }
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(StudyEntryItem.Constants.ID).in(ids));
        return runQuery(query, StudyEntryItem.class);
    }

    public StudyEntryItem getStudyEntryItem(String seoUrl) {
        Query query = new Query();
        query.addCriteria(Criteria.where(StudyEntryItem.Constants.SEO_URL).is(seoUrl));
        query.with(Sort.by(Sort.Direction.DESC, StudyEntryItem.Constants.CREATION_TIME));
        return findOne(query, StudyEntryItem.class);
    }

    public List<StudyEntryItem> getTopMostViewedPdfs(String grade, int limit) {
        Query topMostViewedPdfsQuery = new Query();
        topMostViewedPdfsQuery.addCriteria(Criteria.where(StudyEntryItem.Constants.BOARD_GRADE).regex(Pattern.quote(grade), "i"))
                .with(Sort.by(Sort.Direction.DESC, StudyEntryItem.Constants.VIEWS))
                .limit(limit);

        logger.info("HOME-FEED-MOST-VIEWED-PDF-QUERY");
        logger.info(topMostViewedPdfsQuery);

        return runQuery(topMostViewedPdfsQuery, StudyEntryItem.class);
    }

    public List<StudyEntry> getStudyEntriesForNCERT(List<String> titles, String grade)
    {
        Query query = new Query();
        logger.info("Getting Query for NCERT Solutions for title : {} and grades : {}", titles, grade);
        query.addCriteria(Criteria.where(StudyEntry.Constants.SUBJECT_OR_BOOKS_TITLE).in(titles))
                .addCriteria(Criteria.where(StudyEntry.Constants.TITLE).is("NCERT Solutions"))
                .addCriteria(Criteria.where(StudyEntry.Constants.MAIN_TAGS).all(grade))
                .addCriteria(Criteria.where(StudyEntry.Constants.ENTITY_STATE).is("ACTIVE"));
        logger.info("Query for getting Study Entry : {}", query);
        getProjectionQuery(query);
        logger.info("Projection Query : {}", query);
        return runQuery(query, StudyEntry.class);
    }

    private void getProjectionQuery(Query query)
    {
        query.fields().include(StudyEntry.Constants._ID);
    }

    
    public void updateStudyEntryOrder(List<String> targetGradeTitleOrder, String callingUserId) throws RuntimeException{
		targetGradeTitleOrder.forEach(eachString -> {
			String[] titleGradeTitle = eachString.split("\\:");
			if(null != titleGradeTitle && titleGradeTitle.length == 2) {
				Query query = new Query();
				query.addCriteria(Criteria.where(StudyEntry.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
				query.addCriteria(Criteria.where(StudyEntry.Constants.TARGETGRADES).is(titleGradeTitle[0].trim()));
				query.addCriteria(Criteria.where(StudyEntry.Constants.TITLE).is(titleGradeTitle[1].trim()));
				Update update = new Update();
				update.set(StudyEntry.Constants.LAST_UPDATED_BY, callingUserId);
				update.set(StudyEntry.Constants.LAST_UPDATED, System.currentTimeMillis());
				update.set(StudyEntry.Constants.CREATIONTIME,  System.currentTimeMillis());
				updateMulti(query, update, StudyEntry.class);
			}else {
	            throw new RuntimeException("Study Entry title not found for the target grade specified, Please check the entered data and try again!" + " "+eachString.toString()) ;
 
			}
		});
		
		
	}
    

	public List<StudyEntry> getStudyEntryByGrades(Set<String> mainTags) {
	
		Query query = new Query();
        if (ArrayUtils.isNotEmpty(mainTags)) {
            query.addCriteria(Criteria.where(StudyEntry.Constants.MAIN_TAGS).all(mainTags));
        }
        query.addCriteria(Criteria.where(StudyEntry.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.with(Sort.by(Sort.Direction.ASC, StudyEntry.Constants.CREATION_TIME));
        return runQuery(query, StudyEntry.class);
   
	}

	public void updateStudyEntryById(List<String> ids, String callingUserId) throws NotFoundException, VException{
		Query query = new Query();
		query.addCriteria(Criteria.where(StudyEntry.Constants._ID).in(ids));
        query.addCriteria(Criteria.where(StudyEntry.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        Update update = new Update();
        update.set(StudyEntry.Constants.ENTITY_STATE, EntityState.DELETED);
        update.set(StudyEntry.Constants.LAST_UPDATED_BY, callingUserId);
        updateMulti(query, update, StudyEntry.class);
		
	}
}
