package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.cmds.enums.QuestionAttemptContext;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by somil on 23/03/17.
 */
@Service
public class QuestionAttemptDAO extends AbstractMongoDAO {

    @Autowired
    private CMDSQuestionDAO cMDSQuestionDAO;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public QuestionAttemptDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(QuestionAttempt p) {
        if (p != null) {
            boolean updateCount = false;
            if (p.getId() == null) {
                updateCount = true;
            }
            saveEntity(p);
            if (updateCount) {
                cMDSQuestionDAO.incCount(Arrays.asList(p.getId()), CMDSQuestion.Constants.ATTEMPTS);
            }
        }
    }

    public QuestionAttempt getById(String id) {
        QuestionAttempt QuestionAttempt = null;
        if (!StringUtils.isEmpty(id)) {
            QuestionAttempt = getEntityById(id, QuestionAttempt.class);
        }
        return QuestionAttempt;
    }

    public void insertAll(List<QuestionAttempt> questionAttempts) {
        if (ArrayUtils.isNotEmpty(questionAttempts)) {
            insertAllEntities(questionAttempts, QuestionAttempt.class.getSimpleName());
            List<String> questionIds =
                    questionAttempts
                            .stream()
                            .map(QuestionAttempt::getQuestionId)
                            .collect(Collectors.toList());
            if (ArrayUtils.isNotEmpty(questionIds)) {
                cMDSQuestionDAO.incCount(questionIds, CMDSQuestion.Constants.ATTEMPTS);
            }
        }
    }

    public List<QuestionAttempt> getByAttemptId(String attemptId) {
        Query query = new Query();
        query.addCriteria(Criteria.where("attemptId").is(attemptId));
        List<QuestionAttempt> results = runQuery(query, QuestionAttempt.class);
        return results;
    }
    
    public void upsert(QuestionAttempt p) {
        if (p != null) {
            boolean updateCount = false;
            if (p.getId() == null) {
                updateCount = true;
            }
            Query query = new Query();
            query.addCriteria(Criteria.where("attemptId").is(p.getAttemptId()));
            p.setEntityDefaultProperties(null);
            Update update = getUpdateForQuestionAttemptUpsert(p);
            upsertEntity(query, update, QuestionAttempt.class);
            if (updateCount) {
                cMDSQuestionDAO.incCount(Arrays.asList(p.getId()), CMDSQuestion.Constants.ATTEMPTS);
            }
        }
    }
    
    public Update getUpdateForQuestionAttemptUpsert(QuestionAttempt attempt) {
    	
        Update update = new Update();
        update.set("userId", attempt.getUserId());
        update.set("duration", attempt.getDuration());
        update.set("questionId", attempt.getQuestionId());
        update.set("contextType", attempt.getContextType());
        update.set("contextId", attempt.getContextId());
        update.set("attemptId", attempt.getAttemptId());
        update.set("answerGiven", attempt.getAnswerGiven());
        update.set("creationTime", attempt.getCreationTime());
        update.set("createdBy", attempt.getCreatedBy());
        update.set("lastUpdated", attempt.getLastUpdated());
        update.set("lastUpdatedBy", attempt.getLastUpdatedBy());
        update.set("entityState", attempt.getEntityState());        

        return update;
    }
    
    public QuestionAttempt getQuestionAttemptForAttemptAndQuestionId(String questionId, String attemptId){
        Query query = new Query();
        query.addCriteria(Criteria.where(QuestionAttempt.Constants.ATTEMPT_ID).is(attemptId));
        query.addCriteria(Criteria.where(QuestionAttempt.Constants.QUESTION_ID).is(questionId));
        
        return findOne(query, QuestionAttempt.class);
        
    }
    
}
