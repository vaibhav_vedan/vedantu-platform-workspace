package com.vedantu.cmds.dao.sql;

import com.vedantu.cmds.constants.ConstantsGlobal;
import com.vedantu.cmds.entities.challenges.ClanLeaderBoard;
import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractSqlDAO;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;


@Service
public class ClanLeaderBoardDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ClanLeaderBoardDAO.class);

    public ClanLeaderBoardDAO() {
        super();
    }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void create(ClanLeaderBoard clanLeaderBoard, String callingUserId) {
        try {
            if (clanLeaderBoard != null) {
                saveEntity(clanLeaderBoard, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("Create clanLeaderBoard: " + ex.getMessage());
        }
    }
    
    public void save(ClanLeaderBoard p, Session session, String callingUserId) {
        if (p != null) {
            logger.info("save/update ClanLeaderBoard: " + p.toString());
            saveEntity(p, session, callingUserId);
        }
    }

    public ClanLeaderBoard getByClanId(String id) {
        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        ClanLeaderBoard clanLeaderBoard = null;
        try {
            clanLeaderBoard = getByClanId(id, session);
        } catch (Exception ex) {
            logger.error("getByClanId clanLeaderBoard: " + ex.getMessage());
            clanLeaderBoard = null;
        } finally {
            session.close();
        }
        return clanLeaderBoard;
    }

    public ClanLeaderBoard getByClanId(String id, Session session) {
        ClanLeaderBoard clanLeaderBoard = null;
        if (id != null) {
            Criteria cr = session.createCriteria(ClanLeaderBoard.class);
            cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, id));
            List<ClanLeaderBoard> results = runQuery(session, cr, ClanLeaderBoard.class);
            if (!CollectionUtils.isEmpty(results)) {
                if (results.size() > 1) {
                    logger.error("Duplicate entries found for id : " + id);
                }
                clanLeaderBoard = results.get(0);
            } else {
                logger.info("getByClanId ClanLeaderBoard : no ClanLeaderBoard found " + id);
            }
        }
        return clanLeaderBoard;
    }

    public void update(ClanLeaderBoard p, Session session, String callingUserId) {
        if (p != null) {
            logger.info("update ClanLeaderBoard: " + p.toString());
            updateEntity(p, session, callingUserId);
        }
    }
    
	public List<ClanLeaderBoard> getClanLeaderBoardForCategory(String category, LeaderBoardType type,
			Long startTime, int start, int size) {
		List<ClanLeaderBoard> leaderBoards = new ArrayList<>();
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(ClanLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.EVENT_CATEGORY, category));
			cr.add(Restrictions.eq(ConstantsGlobal.LEADER_BOARD_TYPE, type));
			if(!type.equals(LeaderBoardType.OVERALL)){
				cr.add(Restrictions.eq(ConstantsGlobal.START_TIME, startTime));
			}
			//Need Clarification
			cr.addOrder(Order.desc(ConstantsGlobal.POINTS));
			
			cr.setFirstResult(start);
			cr.	setMaxResults(size);
			List<ClanLeaderBoard> results = runQuery(session, cr, ClanLeaderBoard.class);
			return results;
		} catch (Exception e) {
			logger.error("getClanLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return leaderBoards;
	}

	public Integer getClanRank(String category, LeaderBoardType type, Long startTime, long clanPoints
			) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Integer rank = null;
		try {
			Criteria cr = session.createCriteria(ClanLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.EVENT_CATEGORY, category));
			cr.add(Restrictions.eq(ConstantsGlobal.LEADER_BOARD_TYPE, type));
			if(!type.equals(LeaderBoardType.OVERALL)){
				cr.add(Restrictions.eq(ConstantsGlobal.START_TIME, startTime));
			}
			//Disjunction disjunction = Restrictions.disjunction();
			cr.add(Restrictions.gt(ConstantsGlobal.POINTS, clanPoints));
			//Need Clarification
//			disjunction.add(Restrictions.and(Restrictions.eq(ConstantsGlobal.POINTS, clanPoints),
//					Restrictions.lt("timeRate", clanTimeRate)));
//			cr.add(disjunction);

			cr.setProjection(Projections.rowCount());

			List results = runQuery(session, cr, ClanLeaderBoard.class);
			if (!results.isEmpty()) {
				rank = (Integer) results.get(0);
				rank++;
			}
		} catch (Exception e) {
			logger.error("getClanLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return rank;
	}
	
	public ClanLeaderBoard getByClanId(String clanId, LeaderBoardType type, Long startTime,
			String category){
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        ClanLeaderBoard clanLeaderBoard = null;
        try {
            clanLeaderBoard = getByClanId(clanId, session,type,startTime,category);
        } catch (Exception ex) {
            logger.error("getByClanId clanLeaderBoard: " + ex.getMessage());
            clanLeaderBoard = null;
        } finally {
            session.close();
        }
        return clanLeaderBoard;
	}
	
	public ClanLeaderBoard getByClanId(String clanId, Session session, LeaderBoardType type, Long startTime,
			String category) {
		ClanLeaderBoard clanLeaderBoard = null;
		if (StringUtils.isNotEmpty(clanId)) {
			Criteria cr = session.createCriteria(ClanLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, clanId));
			cr.add(Restrictions.eq(ConstantsGlobal.LEADER_BOARD_TYPE, type));
			if(!type.equals(LeaderBoardType.OVERALL)){
				cr.add(Restrictions.eq(ConstantsGlobal.START_TIME, startTime));
			}
			
			cr.add(Restrictions.eq(ConstantsGlobal.EVENT_CATEGORY, category));
						
			List<ClanLeaderBoard> results = runQuery(session, cr, ClanLeaderBoard.class);
			if (!CollectionUtils.isEmpty(results)) {
				if (results.size() > 1) {
					logger.info("Duplicate entries found for id : " + clanId);//if hint taken will always be multiple//need to relook
				}
				clanLeaderBoard = results.get(0);
			} else {
				logger.info("getByUserId UserLeaderBoard : no UserLeaderBoard found " + clanId);
			}
		}
		return clanLeaderBoard;
	}
		
	public List<ClanLeaderBoard> getByClanIds(List<String> clanIds, LeaderBoardType type, Long startTime,
			String category){
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        List<ClanLeaderBoard> clanLeaderBoards = new ArrayList<>();
        try {
        	Criteria cr = session.createCriteria(ClanLeaderBoard.class);
			cr.add(Restrictions.in(ConstantsGlobal.CLAN_ID, clanIds));
			cr.add(Restrictions.eq(ConstantsGlobal.LEADER_BOARD_TYPE, type));
			if(!type.equals(LeaderBoardType.OVERALL)){
				cr.add(Restrictions.eq(ConstantsGlobal.START_TIME, startTime));
			}
			
			cr.add(Restrictions.eq(ConstantsGlobal.EVENT_CATEGORY, category));
//			cr.addOrder(Order.desc(ConstantsGlobal.POINTS));
			clanLeaderBoards = runQuery(session, cr, ClanLeaderBoard.class);
        } catch (Exception ex) {
            logger.error("getByClanId clanLeaderBoard: " + ex.getMessage());

        } finally {
            session.close();
        }
        return clanLeaderBoards;
	}

}
