package com.vedantu.cmds.dao.sql;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.cmds.constants.ConstantsGlobal;
import com.vedantu.cmds.entities.challenges.PictureChallengeAttempt;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractSqlDAO;

@Service
public class PictureChallengeAttemptDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PictureChallengeAttemptDAO.class);

    public PictureChallengeAttemptDAO() {
        super();
    }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void create(PictureChallengeAttempt pictureChallengeAttempt, String callingUserId) {
        try {
            if (pictureChallengeAttempt != null) {
                saveEntity(pictureChallengeAttempt, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("Create pictureChallengeAttempt: " + ex.getMessage());
        }
    }
    
    public void save(PictureChallengeAttempt p, Session session, String callingUserId) {
        if (p != null) {
            logger.info("update PictureChallengeAttempt: " + p.toString());
            saveEntity(p, session, callingUserId);
        }
    }

    public PictureChallengeAttempt getByClanIdAndChallengeId(String id,String challengeId) {
        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        PictureChallengeAttempt pictureChallengeAttempt = null;
        try {
            pictureChallengeAttempt = getByClanIdAndChallengeId(id,challengeId, session);
        } catch (Exception ex) {
            logger.error("getByPictureChallengeId pictureChallengeAttempt: " + ex.getMessage());
            pictureChallengeAttempt = null;
        } finally {
            session.close();
        }
        return pictureChallengeAttempt;
    }

    public PictureChallengeAttempt getByClanIdAndChallengeId(String id,String challengeId, Session session) {
    	PictureChallengeAttempt pictureChallengeAttempt = null;
        if (id != null) {
            Criteria cr = session.createCriteria(PictureChallengeAttempt.class);
            cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, id));
            cr.add(Restrictions.eq(ConstantsGlobal.PICTURE_CHALLENGE_ID, challengeId));
            List<PictureChallengeAttempt> results = runQuery(session, cr, PictureChallengeAttempt.class);
            if (!CollectionUtils.isEmpty(results)) {
                if (results.size() > 1) {
                    logger.error("Duplicate entries found for id : " + id);
                }
                pictureChallengeAttempt = results.get(0);
            } else {
                logger.info("getByPictureChallengeId PictureChallengeAttempt : no PictureChallengeAttempt found " + id);
            }
        }
        return pictureChallengeAttempt;
    }

    public void update(PictureChallengeAttempt p, String callingUserId) {
        if (p != null) {
            logger.info("update PictureChallengeAttempt: " + p.toString());
            updateEntity(p, callingUserId);
        }
    }
    
    public List<PictureChallengeAttempt> getByClanId(String id) {
        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        List<PictureChallengeAttempt> pictureChallengeAttempt = null;
        try {
            pictureChallengeAttempt = getByClanId(id, session);
        } catch (Exception ex) {
            logger.error("getByPictureChallengeId pictureChallengeAttempt: " + ex.getMessage());
            pictureChallengeAttempt = null;
        } finally {
            session.close();
        }
        return pictureChallengeAttempt;
    }

    public List<PictureChallengeAttempt> getByClanId(String id, Session session) {
    	List<PictureChallengeAttempt> pictureChallengeAttempt = null;
        if (id != null) {
            Criteria cr = session.createCriteria(PictureChallengeAttempt.class);
            cr.add(Restrictions.eq(ConstantsGlobal.CLAN_ID, id));
            pictureChallengeAttempt = runQuery(session, cr, PictureChallengeAttempt.class);
                
            } else {
                logger.info("getByPictureChallengeId PictureChallengeAttempt : no PictureChallengeAttempt found " + id);
            }
        
        return pictureChallengeAttempt;
    }
    
    public List<PictureChallengeAttempt> getByChallengeId(String id,int start,int size) {
        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        List<PictureChallengeAttempt> pictureChallengeAttempts = new ArrayList<>();
        try {
        	if (StringUtils.isNotEmpty(id)) {
                Criteria cr = session.createCriteria(PictureChallengeAttempt.class);
                cr.add(Restrictions.eq(ConstantsGlobal.PICTURE_CHALLENGE_ID, id));
                cr.setFirstResult(start);
                cr.setFetchSize(size);
                pictureChallengeAttempts = runQuery(session, cr, PictureChallengeAttempt.class);                
        	}
        } catch (Exception ex) {
            logger.error("getByPictureChallengeId pictureChallengeAttempt: " + ex.getMessage());
            pictureChallengeAttempts = new ArrayList<>();
        } finally {
            session.close();
        }
        return pictureChallengeAttempts;
    }
}
