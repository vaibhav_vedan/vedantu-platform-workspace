package com.vedantu.cmds.dao;

import com.amazonaws.services.lightsail.model.RecordState;
import com.google.gson.Gson;
import com.vedantu.cmds.dao.es.CMDSQuestionESDAO;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.managers.CurriculumManager;
import com.vedantu.cmds.pojo.CMDSQuestionBasicInfoESPojo;
import com.vedantu.cmds.pojo.QuestionApprovalPojo;
import com.vedantu.cmds.request.ChangeQuestionTagsReq;
import com.vedantu.cmds.request.GetAdminQuestionsReq;
import com.vedantu.cmds.request.GetQuestionsReq;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.cmds.utils.CMDSCommonUtils;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CMDSQuestionDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSQuestionDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private TopicTreeDAO topicTreeDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private CMDSQuestionESDAO cmdsQuestionESDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    @Override
    public <T extends AbstractMongoEntity> int updateFirst(Query q, Update u, Class<T> clazz) {
        return super.updateFirst(q, u, clazz);
    }

    public static final Gson gson = new Gson();


    public CMDSQuestionDAO() {
        super();
    }

    public void save(CMDSQuestion question) {
        try {
            saveEntity(question);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "Update question error " + question.getId(), ex);
        }
    }

    public CMDSQuestion getById(String id) {
        CMDSQuestion question;
        try {
            Assert.hasText(id);
            question = getEntityById(id, CMDSQuestion.class);
        } catch (Exception ex) {
            throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
        }
        return question;
    }

    public List<CMDSQuestion> getByIdsOrdered(List<String> questionIds, VedantuRecordState recordState) {
        List<CMDSQuestion> results = new ArrayList<>();
        if (!questionIds.isEmpty()) {
            for (String question : questionIds) {
                Query questionQuery = new Query();
                questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.ID).is(question));
                if (recordState != null) {
                    questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.RECORD_STATE).is(recordState));
                }
                List<CMDSQuestion> result = runQuery(questionQuery, CMDSQuestion.class);
                if(ArrayUtils.isNotEmpty(result)) {
                    results.add(result.get(0));
                }
            }
        }
        return results;
    }

    public List<CMDSQuestion> getByIds(List<String> questionIds, VedantuRecordState recordState) {

        Query questionQuery = new Query();
        questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.ID).in(questionIds));
        if (recordState != null) {
            questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.RECORD_STATE).is(recordState));
        }
        List<CMDSQuestion> results = runQuery(questionQuery, CMDSQuestion.class);
        return results;

    }

    public void markDeleted(String questionId) throws VException {
        logger.info("Request : " + questionId);
        CMDSQuestion question = getById(questionId);
        if (question == null) {
            throw new VException(ErrorCode.NOT_FOUND_ERROR, "Question not found for id : " + questionId);
        }

        markDeleted(question);
    }

    public void markDeleted(CMDSQuestion question) {
        logger.info("Request : " + question.toString());
        question.setRecordState(VedantuRecordState.DELETED);
        save(question);
        logger.info("Question updated with id :" + question.toString());
    }

    public CMDSQuestion getQuestion(String questionId, List<String> fields) {
        Query query = new Query(Criteria.where("_id").is(questionId));
        if (fields != null) {
            for (String field : fields) {
                query.fields().include(field);
            }
        }
        List<CMDSQuestion> results = runQuery(query, CMDSQuestion.class);
        if (ArrayUtils.isNotEmpty(results)) {
            if (results.size() > 1) {
                logger.error("Multiple question records found for " + questionId);
            }
            return results.get(0);
        } else {
            return null;
        }
    }

    public void incCount(List<String> qids, String queryField) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).in(qids));
        Update update = new Update();
        update.inc(queryField, 1);
        updateMulti(query, update, CMDSQuestion.class);
    }

    public void setCount(String qid, String queryField, long count) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(qid));
        Update update = new Update();
        update.set(queryField, count);
        updateMulti(query, update, CMDSQuestion.class);
    }

    public List<CMDSQuestion> getQuestions(GetQuestionsReq req) {

        logger.info("GetAdminQuestions");
        logger.info(req);

        Query questionQuery = new Query();
        if (StringUtils.isNotEmpty(req.getBook())) {
            questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.BOOK_SLUG).is(CMDSCommonUtils.makeSlug(req.getBook())));
        }
        if (StringUtils.isNotEmpty(req.getChapter())) {
            questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.CHAPTER_SLUG).is(CMDSCommonUtils.makeSlug(req.getChapter())));
        }
        if (StringUtils.isNotEmpty(req.getChapterNo())) {
            questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.CHAPTER_NO).is(req.getChapterNo()));
        }
        if (StringUtils.isNotEmpty(req.getExercise())) {
            questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.EXERCISE_SLUG).is(CMDSCommonUtils.makeSlug(req.getExercise())));
        }
        if (StringUtils.isNotEmpty(req.getEdition())) {
            questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.EDITION_SLUG).is(CMDSCommonUtils.makeSlug(req.getEdition())));
        }
        if (req.getPageNos() != null && !req.getPageNos().isEmpty()) {
            Set<String> slugs = CMDSCommonUtils.makeSlugs(req.getPageNos());
            questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.PAGENOS_SLUGS).in(slugs));
        }
        questionQuery.addCriteria(Criteria.where(CMDSQuestion.Constants.RECORD_STATE).is(VedantuRecordState.ACTIVE));
        questionQuery.with(Sort.by(Sort.Direction.ASC, CMDSQuestion.Constants.SLNO_IN_BOOK));
        setFetchParameters(questionQuery, req);
        logger.info("Query " + questionQuery);
        List<CMDSQuestion> results = runQuery(questionQuery, CMDSQuestion.class);
        return results;
    }

    public List<CMDSQuestion> getAdminQuestions(GetAdminQuestionsReq req) {
        Query query = new Query();
        if (req.getMainTags() != null && !req.getMainTags().isEmpty()) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.MAIN_TAGS).all(req.getMainTags()));
        }
        if (StringUtils.isNotEmpty(req.getSource())) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.SOURCE).is(req.getSource()));
        }
        if (StringUtils.isNotEmpty(req.getCognitiveLevel())) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.COGNITIVE_LEVEL).is(req.getCognitiveLevel().toUpperCase()));
        }
        if (req.getDifficulty() != null) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.DIFFICULTY).is(req.getDifficulty()));
        }
        if (req.getType() != null) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.TYPE).is(req.getType()));
        }
        if (req.getIndexable() != null) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.INDEXABLE).is(req.getIndexable()));
        }
        if (req.isUsed() != null && req.isUsed()) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.USED).gt(0));
        } else if (req.isUsed() != null && !req.isUsed()) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.USED).is(0));
        } else {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.USED).gte(0));
        }
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.APPROVAL_STATUS).is(CMDSApprovalStatus.UNIQUE));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.OVERALL_APPROVAL_STATUS).in(Arrays.asList(CMDSApprovalStatus.CHECKER_APPROVED,null)));
        if(Objects.nonNull(req.getStart()) && Objects.nonNull(req.getSize())){
            query.skip(req.getStart());
            query.limit(req.getSize());
        }else{
            query.limit(100);
        }
        
        logger.info(query);
        List<CMDSQuestion> results = runQuery(query, CMDSQuestion.class);
        return results;
    }

    public List<CMDSQuestion> getIndexableQuestions(int page, int max) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.INDEXABLE).is("YES"));
        query.with(Sort.by(Sort.Direction.DESC, CMDSQuestion.Constants.LAST_UPDATED))
                .with(PageRequest.of(page, max));
        return runQuery(query, CMDSQuestion.class);
    }

    public void migrateQuestionToNewTopicTree(){
        logger.info("migrateQuestionToNewTopicTree DEBUG START");
        List<BaseTopicTree> mappedBaseTopicTree=topicTreeDAO.getMappedEntries();
        Map<String, Set<String>> oldToNewMapping = mappedBaseTopicTree
                .parallelStream()
                .collect(Collectors.groupingBy(BaseTopicTree::getMappedFrom, Collectors.mapping(BaseTopicTree::getName, Collectors.toSet())));
        logger.info("mappedBaseTopicTree: "+mappedBaseTopicTree);
        Query query=new Query();
        int skip=0;
        int limit=300;
        query.skip(skip);
        query.limit(limit);
        List<CMDSQuestion> cmdsQuestions=runQuery(query,CMDSQuestion.class);
        while (ArrayUtils.isNotEmpty(cmdsQuestions)){
            logger.info("Loop number "+skip/limit);
            logger.info("query to get questions is: "+query);
            logger.info("cmdsQuestions: "+cmdsQuestions);
            logger.info("Topic tags of all cmds questions are: START");
            cmdsQuestions.parallelStream().map(CMDSQuestion::getTopics).forEach(logger::info);
            logger.info("Topic tags of all cmds questions are: END");

            // update all the questions one by one
            cmdsQuestions
                    .parallelStream()
                    .filter(cmdsQuestion -> ArrayUtils.isNotEmpty(cmdsQuestion.getTopics()))
                    .filter(cmdsQuestion -> topicsChanged(cmdsQuestion,oldToNewMapping))
                    .forEach(cmdsQuestion -> updateTagsOfQuestion(cmdsQuestion,oldToNewMapping));


            // Get the new set of data
            skip+=limit;
            query.skip(skip);
            query.limit(limit);
            cmdsQuestions=runQuery(query,CMDSQuestion.class);
        }
        logger.info("migrateQuestionToNewTopicTree DEBUG END");
    }

    private void updateTagsOfQuestion(CMDSQuestion cmdsQuestion, Map<String, Set<String>> oldToNewMapping){

        logger.info("updateTagsOfQuestion for question id: "+cmdsQuestion.getId());

        Set<String> newTopics = cmdsQuestion.getTopics()
                .stream()
                .map(topic -> oldToNewMapping.containsKey(topic) ? oldToNewMapping.get(topic) : new ArrayList<>(Arrays.asList(topic)))
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());


        AbstractTargetTopicEntity abstractTargetTopicEntity=new AbstractTargetTopicEntity();
        abstractTargetTopicEntity.setTargetGrades(cmdsQuestion.getTargetGrade());
        abstractTargetTopicEntity.settTopics(newTopics);
        AbstractTargetTopicEntity newTargetTopicEntity=new AbstractTargetTopicEntity();
        try {
            newTargetTopicEntity = fosUtils.setAndGenerateTags(abstractTargetTopicEntity);
        } catch (Exception e) {
            logger.error("updateTagsOfQuestion: ",e);
        }
        logger.info("newTargetTopicEntity for question id: "+cmdsQuestion.getId()+" is "+newTargetTopicEntity);
        if(ArrayUtils.isNotEmpty(newTargetTopicEntity.getMainTags())){
            logger.info("Updating for question id "+cmdsQuestion.getId());

            Query query=new Query();
            query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(cmdsQuestion.getId()));

            Update update=new Update();
            update.set(CMDSQuestion.Constants.TOPICS, newTopics);
            update.set(CMDSQuestion.Constants.MAIN_TAGS, newTargetTopicEntity.getMainTags());
            update.set(CMDSQuestion.Constants.LAST_UPDATED,System.currentTimeMillis());
            update.set(CMDSQuestion.Constants.LAST_UPDATED_BY,"MIGRATION");
            logger.info("Query: "+query);
            logger.info("Update: "+update);
            updateFirst(query,update,CMDSQuestion.class);
        }
    }

    private boolean topicsChanged(CMDSQuestion question, Map<String, Set<String>> oldToNewMapping){
        String tag = question.getTopics()
                .stream()
                .filter(oldToNewMapping::containsKey)
                .findFirst()
                .orElse(null);

        return StringUtils.isNotEmpty(tag);
    }


    public List<CMDSQuestion> getQuestionsInChunks(int skip, int limit) {
        logger.info("getQuestions in chunks for skip {} limit {} ",skip,limit);
        Query query=new Query();
        query.skip(skip);
        query.limit(limit);
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.UPLOADED_TO_ES).ne(Boolean.TRUE));
        return runQuery(query,CMDSQuestion.class);
    }

    public void updateCMDSQuestionDataUploadedToES(String id){
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(id));

        Update update=new Update();
        update.set(CMDSQuestion.Constants.UPLOADED_TO_ES,Boolean.TRUE);
        update.set(CMDSQuestion.Constants.LAST_UPDATED,System.currentTimeMillis());

        updateFirst(query,update,CMDSQuestion.class);
    }


    public void removeDuplicates() throws IOException {
        List<CMDSQuestion> cmdsQuestions=getUnapprovedQuestionDataOneByOne();
        while(ArrayUtils.isNotEmpty(cmdsQuestions)){
            if(cmdsQuestions.size()==1) {
                String id=cmdsQuestions.get(0).getId();
                List<CMDSQuestionBasicInfoESPojo> duplicates = cmdsQuestionESDAO.searchCMDSQuestionDataFromES(id);
                Set<String> duplicateIds = duplicates
                        .stream()
                        .map(CMDSQuestionBasicInfoESPojo::get_id)
                        .collect(Collectors.toSet());
                duplicateIds.remove(id);
                removeDuplicatesForOneTime(duplicateIds,id);
                markCurrentDataOriginal(id);
                logger.info("The question with question id {} has duplicates {}",id,duplicateIds);
                cmdsQuestions = getUnapprovedQuestionDataOneByOne();
            }else{
                logger.error(cmdsQuestions.size()+" becuase of which error occured.");
            }
        }
    }

    private void markCurrentDataOriginal(String id) {

        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(id));

        Update update=new Update();
        update.set(CMDSQuestion.Constants.APPROVAL_STATUS,CMDSApprovalStatus.UNIQUE);
        update.set(CMDSQuestion.Constants.LAST_UPDATED,System.currentTimeMillis());

        updateFirst(query,update,CMDSQuestion.class);
    }

    private void removeDuplicatesForOneTime(Set<String> duplicateIds,String id) {
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).in(duplicateIds));

        Update update=new Update();
        update.set(CMDSQuestion.Constants.APPROVAL_STATUS,CMDSApprovalStatus.ARCHIVED);
        update.set(CMDSQuestion.Constants.IS_A_COPY_OF,id);
        update.set(CMDSQuestion.Constants.LAST_UPDATED,System.currentTimeMillis());

        updateMulti(query,update,CMDSQuestion.class);

        cmdsQuestionESDAO.deleteQuestionInESByIds(duplicateIds);
    }

    public List<CMDSQuestion> getUnapprovedQuestionDataOneByOne(){
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.APPROVAL_STATUS).is(null));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.QUESTION_BODY).ne(null));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.RECORD_STATE).is(VedantuRecordState.ACTIVE));
        query.fields().include(CMDSQuestion.Constants._ID);
        query.limit(1);
        return runQuery(query,CMDSQuestion.class);
    }

    public void deleteUnnecessaryQuestions(){
        long currentTime=System.currentTimeMillis();
        long fifteenDaysBefore=currentTime-15* DateTimeUtils.MILLIS_PER_DAY;
        long sixteenDaysBefore=currentTime-16*DateTimeUtils.MILLIS_PER_DAY;
        Query query=new Query();
        query.addCriteria(
                new Criteria().andOperator(
                        Criteria.where(CMDSQuestion.Constants.CREATION_TIME).lte(fifteenDaysBefore),
                        Criteria.where(CMDSQuestion.Constants.CREATION_TIME).gte(sixteenDaysBefore)
                )
        );
        query.addCriteria(
                Criteria.where(CMDSQuestion.Constants.APPROVAL_STATUS).in(
                        Arrays.asList(
                            CMDSApprovalStatus.REJECTED,
                            CMDSApprovalStatus.TEMPORARY,
                            CMDSApprovalStatus.DUPLICATE
                        )
                )
        );
        List<CMDSQuestion> cmdsQuestions=runQuery(query,CMDSQuestion.class);
        Set<String> questionIds =
                Optional.ofNullable(cmdsQuestions)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(CMDSQuestion::getId)
                        .collect(Collectors.toSet());
        cmdsQuestionESDAO.deleteQuestionInESByIds(questionIds);

        Update update=new Update();
        update.set(CMDSQuestion.Constants.RECORD_STATE, VedantuRecordState.DELETED);
        updateMulti(query,update, CMDSQuestion.class);
    }

    public List<CMDSQuestion> getByIds(List<String> questionIds) {
        return getByIds(questionIds,null);
    }

    public void updateApprovalStatus(List<String> questionIds, CMDSApprovalStatus approvalStatus) {
        updateApprovalStatus(questionIds,approvalStatus,null);
    }
    public void updateApprovalStatus(List<String> questionIds, CMDSApprovalStatus approvalStatus, Long callingUserId) {
        if(ArrayUtils.isEmpty(questionIds) || Objects.isNull(approvalStatus)){
            try {
                throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION,"Please specify valid parameters");
            } catch (BadRequestException e) {
                logger.error("updateApprovalStatus",e);
            }
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).in(questionIds));
        // Don't delete those questions which are already checker approved
        if(CMDSApprovalStatus.REJECTED.equals(approvalStatus)) {
            query.addCriteria(Criteria.where(CMDSQuestion.Constants.OVERALL_APPROVAL_STATUS).ne(CMDSApprovalStatus.CHECKER_APPROVED));
        }
        Update update=new Update();
        update.set(CMDSQuestion.Constants.APPROVAL_STATUS,approvalStatus);
        if(Objects.nonNull(callingUserId)) {
            update.set(CMDSQuestion.Constants.LAST_UPDATED_BY, callingUserId);
        }
        updateMulti(query,update,CMDSQuestion.class);
    }

    public void updateApprovedQuestion(QuestionApprovalPojo approvalReq, CMDSApprovalStatus overallApprovalStatus, Long checkerId, Long callingUserId, String questionSetId, List<String> parentNames) {
        if(Objects.isNull(approvalReq) || Objects.isNull(overallApprovalStatus) || Objects.isNull(checkerId) || Objects.isNull(callingUserId) || Objects.isNull(questionSetId)){
            try {
                throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION,"Please specify valid parameters");
            } catch (BadRequestException e) {
                logger.error("updateApprovedQuestion",e);
            }
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(approvalReq.getId()));

        Update update=new Update();

        // Don't change the approval status of already changed questions
        CMDSQuestion cmdsQuestion = getById(approvalReq.getId());
        Set<String> mainTags=new HashSet<>();
        if(!CMDSApprovalStatus.CHECKER_APPROVED.equals(cmdsQuestion.getApprovalStatus())){
            update.set(CMDSQuestion.Constants.OVERALL_APPROVAL_STATUS,overallApprovalStatus);
        }
        if(Objects.nonNull(approvalReq.getStatus())){
            update.set(CMDSQuestion.Constants.APPROVAL_STATUS,approvalReq.getStatus());
        }
        if(Objects.nonNull(approvalReq.getSubject())){
            update.set(CMDSQuestion.Constants.SUBJECT,approvalReq.getSubject());
        }
        if(ArrayUtils.isNotEmpty(approvalReq.getTopics())){
            List<String> redisKeys =
                    approvalReq.getTopics()
                            .stream()
                            .map(String::toLowerCase)
                            .map(CurriculumManager::getTopicKey)
                            .collect(Collectors.toList());
            Map<String, String> redisValues=new HashMap<>();
            try {
                redisValues=redisDAO.getValuesForKeys(redisKeys);
            } catch (InternalServerErrorException e) {
                logger.info("InternalServerErrorException while getting topics from redis {}",e);
            }
            mainTags.addAll(approvalReq.getTopics());
            mainTags.addAll(
                    redisValues.values()
                            .stream()
                            .filter(Objects::nonNull)
                            .map(this::getBaseTopicTreeFromValue)
                            .map(BaseTopicTree::getParents)
                            .filter(ArrayUtils::isNotEmpty)
                            .flatMap(Collection::stream)
                            .collect(Collectors.toSet())
            );
            update.set(CMDSQuestion.Constants.TOPICS,approvalReq.getTopics());
        }
        if(ArrayUtils.isNotEmpty(approvalReq.getSubtopics())){
            List<String> redisKeys =
                    approvalReq.getSubtopics()
                            .stream()
                            .map(String::toLowerCase)
                            .map(CurriculumManager::getTopicKey)
                            .collect(Collectors.toList());
            Map<String, String> redisValues=new HashMap<>();
            try {
                redisValues=redisDAO.getValuesForKeys(redisKeys);
            } catch (InternalServerErrorException e) {
                logger.info("InternalServerErrorException while getting topics from redis {}",e);
            }
            mainTags.addAll(
                    redisValues.values()
                            .stream()
                            .filter(Objects::nonNull)
                            .map(this::getBaseTopicTreeFromValue)
                            .map(BaseTopicTree::getParents)
                            .filter(ArrayUtils::isNotEmpty)
                            .flatMap(Collection::stream)
                            .collect(Collectors.toSet())
            );
            mainTags.addAll(approvalReq.getSubtopics());
            update.set(CMDSQuestion.Constants.SUBTOPICS,approvalReq.getSubtopics());
        }else{
            update.unset(CMDSQuestion.Constants.SUBTOPICS);
        }
        if(ArrayUtils.isNotEmpty(approvalReq.getGrades())){
            update.set(CMDSQuestion.Constants.GRADES,approvalReq.getGrades());
        }else{
            update.unset(CMDSQuestion.Constants.GRADES);
        }
        if(ArrayUtils.isNotEmpty(approvalReq.getTargets())){
            update.set(CMDSQuestion.Constants.TARGETS,approvalReq.getTargets());
        }else{

        }
        if(Objects.nonNull(approvalReq.getDifficultyValue())){
            update.set(CMDSQuestion.Constants.DIFFICULTY_VALUE,approvalReq.getDifficultyValue());
        }else{
            update.unset(CMDSQuestion.Constants.DIFFICULTY_VALUE);
        }
        if(Objects.nonNull(approvalReq.getCognitiveLevel())){
            update.set(CMDSQuestion.Constants.COGNITIVE_LEVEL,approvalReq.getCognitiveLevel());
        }else{
            update.unset(CMDSQuestion.Constants.COGNITIVE_LEVEL);
        }
        if(ArrayUtils.isNotEmpty(approvalReq.getSource())){
            update.set(CMDSQuestion.Constants.SOURCE,approvalReq.getSource());
        }
        if(Objects.nonNull(approvalReq.getType())){
            update.set(CMDSQuestion.Constants.TYPE,approvalReq.getType());
        }
        if(Objects.nonNull(approvalReq.getIsACopyOf())){
            update.set(CMDSQuestion.Constants.IS_A_COPY_OF,approvalReq.getIsACopyOf());
        }
        if(ArrayUtils.isNotEmpty(approvalReq.getAnalysisTags())){
            update.set(CMDSQuestion.Constants.ANALYSIS_TAGS,approvalReq.getAnalysisTags());
        }else{
            update.unset(CMDSQuestion.Constants.ANALYSIS_TAGS);
        }
        if(ArrayUtils.isNotEmpty(approvalReq.getTargetGrade())){
            update.set(CMDSQuestion.Constants.TARGET_GRADE,approvalReq.getTargetGrade());
        }else{
            update.unset(CMDSQuestion.Constants.TARGET_GRADE);
        }
        if(ArrayUtils.isNotEmpty(mainTags)){
            if(ArrayUtils.isNotEmpty(approvalReq.getTargetGrade())){
                mainTags.addAll(
                        approvalReq.getTargetGrade()
                                .stream()
                                .filter(Objects::nonNull)
                                .map(this::getSplittedTargetGrades)
                                .filter(ArrayUtils::isNotEmpty)
                                .flatMap(Collection::stream)
                                .collect(Collectors.toSet())
                );
            }
            update.set(CMDSQuestion.Constants.MAIN_TAGS,mainTags);
            List<String> tempParentNames=new ArrayList<>(parentNames);
            tempParentNames.retainAll(mainTags);
            update.set(CMDSQuestion.Constants.SUBJECTS,tempParentNames);
        }
        logger.info("Updating questions for update with `query` {} and `update` {}",query,update);
        updateFirst(query,update,CMDSQuestion.class);

        // Change the subject of original question is needed as we are subject for sections
        // (ideally it's a bad design, need to be changed ASAP)
        if(CMDSApprovalStatus.DUPLICATE.equals(approvalReq.getStatus())){
            query=new Query();
            query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(approvalReq.getIsACopyOf()));

            update=new Update();
            update.set(CMDSQuestion.Constants.SUBJECT,approvalReq.getSubject());

            updateFirst(query,update,CMDSQuestion.class);
        }
    }

    private BaseTopicTree getBaseTopicTreeFromValue(String value) {
        return gson.fromJson(value, BaseTopicTree.class);
    }

    private Set<String> getSplittedTargetGrades(String targetGrade) {
        Set<String> targetGradeSplit=Arrays.stream(targetGrade.split("-")).collect(Collectors.toSet());
        targetGradeSplit.add(targetGrade);
        return targetGradeSplit;
    }

    public CMDSQuestion updateChangeQuestionTags(ChangeQuestionTagsReq req){

        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(req.getQuestionId()));

        Update update=new Update();
        if(StringUtils.isNotEmpty(req.getSubject())) {
            update.set(CMDSQuestion.Constants.SUBJECT, req.getSubject());
            update.set(CMDSQuestion.Constants.SUBJECT_SLUG, CMDSCommonUtils.makeSlug(req.getSubject()));
        }
        if(ArrayUtils.isNotEmpty(req.getTopics())) {
            update.set(CMDSQuestion.Constants.TOPICS, req.getTopics());
            update.set(CMDSQuestion.Constants.TOPICS_SLUGS,CMDSCommonUtils.makeSlugs(req.getTopics()));
        }
        if(ArrayUtils.isNotEmpty(req.getSubtopics())) {
            update.set(CMDSQuestion.Constants.SUBTOPICS, req.getSubtopics());
            update.set(CMDSQuestion.Constants.SUBTOPICS_SLUGS,CMDSCommonUtils.makeSlugs(req.getSubtopics()));
        }
        if(ArrayUtils.isNotEmpty(req.getGrades())) {
            update.set(CMDSQuestion.Constants.GRADES, req.getGrades());
            update.set(CMDSQuestion.Constants.GRADES_SLUGS,CMDSCommonUtils.makeSlugs(req.getGrades()));
        }
        if(ArrayUtils.isNotEmpty(req.getTargets())) {
            update.set(CMDSQuestion.Constants.TARGETS, req.getTargets());
            update.set(CMDSQuestion.Constants.TARGETS_SLUGS,CMDSCommonUtils.makeSlugs(req.getTargets()));
        }
        if(Objects.nonNull(req.getDifficultyValue())) {
            update.set(CMDSQuestion.Constants.DIFFICULTY_VALUE, req.getDifficultyValue());
        }
        if(Objects.nonNull(req.getType())) {
            update.set(CMDSQuestion.Constants.TYPE, req.getType());
        }
        if(ArrayUtils.isNotEmpty(req.getAnalysisTags())){
            update.set(CMDSQuestion.Constants.ANALYSIS_TAGS,req.getAnalysisTags());
        }

        updateMulti(query,update,CMDSQuestion.class);

        query.fields().include(CMDSQuestion.Constants.SUBJECT);
        query.fields().include(CMDSQuestion.Constants.TOPICS);
        query.fields().include(CMDSQuestion.Constants.SUBTOPICS);
        query.fields().include(CMDSQuestion.Constants.GRADES);
        query.fields().include(CMDSQuestion.Constants.TARGETS);
        query.fields().include(CMDSQuestion.Constants.DIFFICULTY_VALUE);
        query.fields().include(CMDSQuestion.Constants.TYPE);

        return findOne(query,CMDSQuestion.class);
    }

    public void updateOriginalQuestionReferenceToCurrentQuestionSet(List<String> questionIds,String questionSetId) {
        for (String questionId : questionIds) {
            CMDSQuestion cmdsQuestion=getById(questionId);

            Set<String> mappedToQuestionSetIds;
            if(ArrayUtils.isEmpty(cmdsQuestion.getMappedToQuestionSetIds())){
                mappedToQuestionSetIds=new HashSet<>();
            }else{
                mappedToQuestionSetIds=cmdsQuestion.getMappedToQuestionSetIds();
            }
            mappedToQuestionSetIds.add(questionSetId);

            Query query=new Query();
            query.addCriteria(Criteria.where(CMDSQuestion.Constants._ID).is(cmdsQuestion.getId()));

            Update update=new Update();
            update.set(CMDSQuestion.Constants.MAPPED_TO_QUESTION_SET_IDS,mappedToQuestionSetIds);

            updateFirst(query,update,CMDSQuestion.class);
        }

    }

    public List<CMDSQuestion> getDirtyData(){
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.APPROVAL_STATUS).is(CMDSApprovalStatus.UNIQUE));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.IS_A_COPY_OF).ne(null));
        query.limit(1000);
        return runQuery(query,CMDSQuestion.class);
    }

    public List<CMDSQuestion> getQuestionsToBeUploadedToES(List<String> questionIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.ID).in(questionIds));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.UPLOADED_TO_ES).is(false));
        List<CMDSQuestion> results = runQuery(query, CMDSQuestion.class);
        return results;
    }

    public List<CMDSQuestion> getIdsIfOrNotUploadedToES(List<String> questionIds, Boolean uploadedToES) {
        if(ArrayUtils.isEmpty(questionIds)){
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.ID).in(questionIds));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.UPLOADED_TO_ES).is(uploadedToES));
        query.fields().include(CMDSQuestion.Constants.ID);
        logger.info("Query for getIdsIfOrNotUploadedToES {}",query);
        List<CMDSQuestion> results = runQuery(query, CMDSQuestion.class);
        return results;
    }

    public List<CMDSQuestion> getInactiveDirtyQuestions() {
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.UPLOADED_TO_ES).is(true));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.RECORD_STATE).in(Arrays.asList(VedantuRecordState.DELETED,VedantuRecordState.TEMPORARY)));
        query.addCriteria(Criteria.where(CMDSQuestion.Constants.APPROVAL_STATUS).is(CMDSApprovalStatus.UNIQUE));
        query.limit(1000);
        return runQuery(query,CMDSQuestion.class);
    }
}
