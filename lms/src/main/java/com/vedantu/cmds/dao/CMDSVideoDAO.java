package com.vedantu.cmds.dao;

import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;
import com.vedantu.cmds.entities.CMDSVideoPlaylistOrder;
import com.vedantu.cmds.enums.OnboardingVideoType;
import com.vedantu.cmds.request.GetAdminVideosReq;
import com.vedantu.cmds.response.FiltersRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Service
public class CMDSVideoDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSVideoDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public CMDSVideoDAO() {
        super();
    }

    public void update(CMDSVideo video) {
        logger.info("Request : " + video.toString());
        try {
            Assert.notNull(video);
            saveEntity(video);
            logger.info("Updated id : " + video.getId());
        } catch (Exception ex) {
            
           
            logger.info("Exception in saving: "+ex.getMessage());
            throw new RuntimeException("CMDSTestUpdateError : Error updating the CMDS Video " + video.toString(), ex);
        }
    }

    public void update(CMDSVideoPlaylistOrder order) {
        logger.info("Request : " + order.toString());
        try {
            Assert.notNull(order);
            saveEntity(order);
            logger.info("Updated id : " + order.getId());
        } catch (Exception ex) {
            logger.info("Exception in saving: "+ex.getMessage());
            throw new RuntimeException("CMDSTestUpdateError : Error updating the CMDS Playlist Order " + order.toString(), ex);
        }
    }

    public CMDSVideoPlaylistOrder getOrderedPlaylistByGrade(String grade) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSVideoPlaylistOrder.Constants.GRADE).is(grade));
        return findOne(query, CMDSVideoPlaylistOrder.class);
    }

    public List<CMDSVideoPlaylistOrder> getOrderedPlaylistWithPlaylist(String playlistId){
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSVideoPlaylistOrder.Constants.PLAYLISTS).in(playlistId));
        List<CMDSVideoPlaylistOrder> list = runQuery(query, CMDSVideoPlaylistOrder.class);
        return list;
    }

    public List<CMDSVideo> getAdminVideos(GetAdminVideosReq req) {
        Query query = new Query();
        if(Objects.nonNull(req.getMainTags()) && !req.getMainTags().isEmpty()) {
            query.addCriteria(Criteria.where(CMDSVideo.Constants.MAIN_TAGS).all(req.getMainTags()));
        }

        if(Objects.nonNull(req.getTeacherIds()) && !req.getMainTags().isEmpty()) {
            query.addCriteria(Criteria.where(CMDSVideo.Constants.TEACHER_IDS).in(req.getMainTags()));
        }

        if(req.isOnboardingVideosRequired()){

            query.addCriteria(Criteria.where(CMDSVideo.Constants.SOCIAL_CONTEXT_TYPE).is(SocialContextType.ONBOARDING));

            if(Objects.nonNull(req.getOnboardingVideoType())){
                query.addCriteria(Criteria.where(CMDSVideo.Constants.ONBOARDING_VIDEO_TYPE).is(req.getOnboardingVideoType()));
            }
        }

        query.addCriteria(Criteria.where(CMDSVideo.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        if (Objects.nonNull(req.getStart())) {
            query.skip(req.getStart());
        }

        if (Objects.nonNull(req.getSize()) && req.getSize() > 0) {
            query.limit(req.getSize());
        }

        query.with(Sort.by(Sort.Direction.DESC, CMDSVideo.Constants.CREATION_TIME));

        return runQuery(query, CMDSVideo.class);
    }
    
    public CMDSVideo getByUrl(String url){
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSVideo.Constants.VIDEO_URL).is(url));
        return findOne(query, CMDSVideo.class);
    }

    public CMDSVideo getCMDSVideoByUrl(String url){
        
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSVideo.Constants.VIDEO_URL).is(url));
        
        return findOne(query, CMDSVideo.class);
        
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public List<CMDSVideo> getVideosFromIds(List<String> ids) {
        if (ids == null) {
            return new ArrayList<>();
        }
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(CMDSVideo.Constants.ID).in(ids));
        return runQuery(query, CMDSVideo.class);
    }    

    public List<CMDSVideo> getVideosViewsFromIds(Set<String> ids){
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSVideo.Constants.ID).in(ids));
        query.fields().include(CMDSVideo.Constants.VIDEO_VIEWS);
        query.fields().include(CMDSVideo.Constants.TITLE);
        query.fields().include(CMDSVideo.Constants.DESCRIPTION);
        query.fields().include(CMDSVideo.Constants.DURATION);
        query.fields().include(CMDSVideo.Constants.THUMBNAIL);
        query.fields().include(CMDSVideo.Constants.TOTAL_LIKES);
        query.fields().include(CMDSVideo.Constants.VIDEO_URL);
        return runQuery(query, CMDSVideo.class);
    }

    public List<CMDSVideo> getMostViewedVideos(String grade, int limit) {
        Query mostViewedVideosQuery = new Query();
        mostViewedVideosQuery.addCriteria(Criteria.where(CMDSVideo.Constants.MAIN_TAGS).in(grade))
                .with(Sort.by(Sort.Direction.DESC, CMDSVideo.Constants.VIDEO_VIEWS))
                .limit(limit);
        return runQuery(mostViewedVideosQuery, CMDSVideo.class);
    }

    public void setCount(String qid, String queryField, long count, String queryField2, long dislikeCount) {
    	//db.getCollection('CMDSVideoPlaylist').update({"videos.entityId":"5c1333b060b288f86020fa26"},{$set:{"videos.$.totalLikes":12}})
        Query query = new Query();
        query.addCriteria(Criteria.where(CMDSVideo.Constants._ID).is(qid));
        Update update = new Update();
        update.set(queryField, count);
        update.set(queryField2, dislikeCount);
        updateMulti(query, update, CMDSVideo.class);
    }

    public List<CMDSVideoPlaylist> getLiveVideos() {
        Long currentTime = System.currentTimeMillis();
    	Query query = new Query();

    	//Criteria criteria = new Criteria();
    	//criteria.andOperator(Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_START_TIME).lte(currentTime),
    	//Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_END_TIME).gte(currentTime),
    	//Criteria.where(CMDSVideo.Constants.VIDEOS_LIVE).is(true));

    	//Aggregation agg = newAggregation(sort(Sort.Direction.DESC, CMDSVideo.Constants.CREATION_TIME),Aggregation.unwind("$videos"),match(criteria));
        //logger.info("Aggregation for videos :: " + agg);
    	//AggregationResults<CMDSVideoPlaylist> liveVideos = getMongoOperations().aggregate(agg,CMDSVideoPlaylist.class.getName(),CMDSVideoPlaylist.class);
    	//List<CMDSVideoPlaylist> videoList = liveVideos.getMappedResults();
    	//logger.info("video count :: " + videoList.size());
    	//logger.info("videos :: " + videoList);

//        Criteria andCriteria = new Criteria().andOperator(Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_START_TIME).lte(currentTime), Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_END_TIME).gte(currentTime));
//        query.addCriteria(andCriteria);

        query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_START_TIME).lte(currentTime));
        query.addCriteria(Criteria.where(CMDSVideoPlaylist.Constants.VIDEO_END_TIME).gte(currentTime));
        query.addCriteria(Criteria.where(CMDSVideo.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.addCriteria(Criteria.where(CMDSVideo.Constants.VIDEOS_LIVE).is(true));
        query.with(Sort.by(Sort.Direction.DESC, CMDSVideo.Constants.CREATION_TIME));
        List<CMDSVideoPlaylist> playlists = runQuery(query, CMDSVideoPlaylist.class);

        return playlists;
    }
	/*
	 * public List<DummyMessages> getDummyChat() { Query query = new Query(); return
	 * runQuery(query, DummyMessages.class); }
	 */

    public ArrayList<FiltersRes> getFilters() {
    	Query query = new Query();
        query.addCriteria(Criteria.where("level").is(0));
        List<BaseTopicTree> subjects = runQuery(query, BaseTopicTree.class);

        ArrayList<FiltersRes> filters = new ArrayList<>();
        FiltersRes filter;
        List<BaseTopicTree> topics;
        ArrayList<String> topicList = new ArrayList<>();
        Query getTopics;
        for(BaseTopicTree subject : subjects) {
        	getTopics = new Query();
        	getTopics.addCriteria(Criteria.where(subject.getName()).in("parents"));
        	getTopics.addCriteria(Criteria.where("level").in(3));
        	topics = runQuery(query, BaseTopicTree.class);
        	for(BaseTopicTree topic : topics) {
        		topicList.add(topic.getName());
        	}
        	filter = new FiltersRes();
        	filter.setTopics(topicList);
        	filter.setSubject(subject.getName());
        	topicList.clear();
        	filters.add(filter);
        }

        return filters;
    }

    public CMDSVideo saveOrUpdateVideo(CMDSVideo cmdsVideo) {
        if(Objects.isNull(cmdsVideo.getId())){
            logger.info("Saving video for information - {}",cmdsVideo);
            saveEntity(cmdsVideo);
        }else{
            Query query=new Query();
            query.addCriteria(Criteria.where(CMDSVideo.Constants._ID).is(cmdsVideo.getId()));
            query.addCriteria(Criteria.where(CMDSVideo.Constants.ONBOARDING_VIDEO_TYPE).is(cmdsVideo.getOnboardingVideoType()));

            CMDSVideo currentVideo=findOne(query,CMDSVideo.class);
            if(Objects.nonNull(currentVideo)) {
                if(!currentVideo.getVideoUrl().equals(cmdsVideo.getVideoUrl())) {
                    Update update = new Update();
                    update.set(CMDSVideo.Constants.VIDEO_URL, cmdsVideo.getVideoUrl());
                    logger.info("saveOrUpdateVideo - query - {} - update - {}", query, update);
                    updateFirst(query, update, CMDSVideo.class);
                }
            }

        }
        return cmdsVideo;
    }

    public List<CMDSVideo> getOnboardingVideoForGrade(String grade) {
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSVideo.Constants.SOCIAL_CONTEXT_TYPE).is(SocialContextType.ONBOARDING));
        query.addCriteria(Criteria.where(CMDSVideo.Constants.MAIN_TAGS).is(grade));
        logger.info("getOnboardingVideoForGrade - query - {}",query);
        return runQuery(query,CMDSVideo.class);
    }

    public List<CMDSVideo> getOnboardingVideoForGradeAndOnboardingVideoType(Set<String> grades,List<OnboardingVideoType> onboardingVideoTypes) {
        Query query=new Query();
        query.addCriteria(Criteria.where(CMDSVideo.Constants.SOCIAL_CONTEXT_TYPE).is(SocialContextType.ONBOARDING));
        query.addCriteria(Criteria.where(CMDSVideo.Constants.ONBOARDING_VIDEO_TYPE).in(onboardingVideoTypes));
        query.addCriteria(Criteria.where(CMDSVideo.Constants.MAIN_TAGS).in(grades));
        logger.info("getOnboardingVideoForGrade - query - {}",query);
        return runQuery(query,CMDSVideo.class);
    }
}
