/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.dao.challenges;

import com.vedantu.cmds.entities.challenges.ChallengeAttempt;
import com.vedantu.cmds.pojo.challenges.ChallengeAttemptsCount;
import com.vedantu.lms.cmds.pojo.AttemptedUserIds;
import com.vedantu.lms.cmds.pojo.ChallengeUserPoints;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SortOrder;
import java.util.ArrayList;
import java.util.Set;
import org.bson.Document;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;

@Service
public class ChallengeAttemptDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ChallengeAttemptDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public ChallengeAttemptDAO() {
        super();
    }

    public void save(ChallengeAttempt challengeTaken) {
        try {
            saveEntity(challengeTaken);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ContentInfoUpdateError : Error updating the content info " + challengeTaken, ex);
        }
    }

    public ChallengeAttempt getByUserId(Long userId, String challengeId) {

        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.USERID).is(userId));
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(challengeId));

        List<ChallengeAttempt> results = runQuery(query, ChallengeAttempt.class);
        if (ArrayUtils.isNotEmpty(results)) {
            if (results.size() > 1) {
                logger.error("Multiple ChallengeTaken records found for " + userId);
            }
            return results.get(0);
        } else {
            return null;
        }
    }

    public List<ChallengeAttempt> getChallengesTakenByUserId(Long userId, List<String> fields,
            Integer start, Integer size, String sortOrder,
            String orderBy) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.USERID).is(userId));
        if (fields != null) {
            for (String field : fields) {
                query.fields().include(field);
            }
        }
        setFetchParameters(query, start, size);
        if (StringUtils.isNotEmpty(orderBy) && StringUtils.isNotEmpty(sortOrder)) {
            SortOrder sortOrder1 = SortOrder.valueQuitentOf(sortOrder);
            if (Sort.Direction.ASC.name().equals(sortOrder1.name())) {
                query.with(Sort.by(Sort.Direction.ASC, orderBy));
            } else {
                query.with(Sort.by(Sort.Direction.DESC, orderBy));
            }
        } else {
            query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.LAST_UPDATED));
        }
        logger.info("query " + query);
        return runQuery(query, ChallengeAttempt.class);
    }

    public List<ChallengeAttempt> getChallengeLeaderBoard(String challengeId, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(challengeId));
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.SUCCESS).is(true));
        setFetchParameters(query, start, size);
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, ChallengeAttempt.Constants.TOTAL_POINTS));
        orderList.add(new Sort.Order(Sort.Direction.ASC, ChallengeAttempt.Constants.TIME_TAKEN));
        query.with(Sort.by(orderList));
        return runQuery(query, ChallengeAttempt.class);
    }

    public Long getMyRank(ChallengeAttempt attempt) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(attempt.getChallengeId()));
        query.addCriteria(new Criteria().orOperator(Criteria.where(ChallengeAttempt.Constants.TOTAL_POINTS).gt(attempt.totalPoints),
                new Criteria().andOperator(Criteria.where(ChallengeAttempt.Constants.TOTAL_POINTS).is(attempt.totalPoints), Criteria.where(ChallengeAttempt.Constants.TIME_TAKEN).lt(attempt.getTimeTaken()))));
        return queryCount(query, ChallengeAttempt.class) + 1;
    }

    public Long getChallengeAttemptCount(String challengeId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(challengeId));
        return queryCount(query, ChallengeAttempt.class);
    }

    public Long getChallengesTakenByUserIdCount(Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.USERID).is(userId));
        return queryCount(query, ChallengeAttempt.class);
    }

    public List<ChallengeAttempt> getChallengeTakensByChallengeId(String challengeId, int start, int size) {
        return getChallengeTakensByChallengeId(challengeId, start, size, null);
    }

    public List<ChallengeAttempt> getChallengeTakensByChallengeId(String challengeId, int start, int size, List<String> fields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(challengeId));
        if (fields != null) {
            for (String field : fields) {
                query.fields().include(field);
            }
        }
        setFetchParameters(query, start, size);
        query.with(Sort.by(Sort.Direction.ASC, ChallengeAttempt.Constants.END_TIME));
        return runQuery(query, ChallengeAttempt.class);
    }

    public List<ChallengeAttempt> getChallengeAttempts(Set<String> challengeIds, Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.USERID).is(userId));
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).in(challengeIds));
        return runQuery(query, ChallengeAttempt.class);
    }

    public List<ChallengeAttemptsCount> getAttemptsCount(Set<String> challengeIds) {
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = newAggregation(
                match(Criteria.where("challengeId").in(challengeIds)),
                group("challengeId").count().as("attempts"),
                project("attempts").and("challengeId").previousOperation()).withOptions(aggregationOptions);
        AggregationResults<ChallengeAttemptsCount> groupResults
                = getMongoOperations().aggregate(agg, ChallengeAttempt.class.getSimpleName(), ChallengeAttemptsCount.class);
        List<ChallengeAttemptsCount> result = groupResults.getMappedResults();
        return result;
    }
    
    public List<ChallengeUserPoints> getDailyScoreUsers(String category,Long startTime, Long endTime) {
        Criteria categoryCrit = Criteria.where("category").in(category);
        Criteria time = Criteria.where(ChallengeAttempt.Constants.CREATION_TIME).gte(startTime).lt(endTime);
        Criteria criteria = new Criteria();
        criteria.andOperator(categoryCrit,time);
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
    	Aggregation agg = newAggregation(
                match(criteria),
                group("userId").sum("totalPoints").as("points"),
                project("points").and("userId").previousOperation()).withOptions(aggregationOptions);
        AggregationResults<ChallengeUserPoints> groupResults
                = getMongoOperations().aggregate(agg, ChallengeAttempt.class.getSimpleName(), ChallengeUserPoints.class);
        List<ChallengeUserPoints> result = groupResults.getMappedResults();
        return result;
    }
    
    //TEMPORARY FOR NOW : Will Break on large datasets
    public List<AttemptedUserIds> getUsersWhoAttemptedChallenge(){
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
    	Aggregation agg = newAggregation(group(ChallengeAttempt.Constants.CATEGORY)
				.addToSet(ChallengeAttempt.Constants.USERID).as("attemptedUserIds"),
				project("attemptedUserIds")
                                        .and(ChallengeAttempt.Constants.CATEGORY).previousOperation()).withOptions(aggregationOptions);
    	
    	AggregationResults<AttemptedUserIds> groupResults = 
    			getMongoOperations().aggregate(agg, ChallengeAttempt.class.getSimpleName(), AttemptedUserIds.class);

    	List<AttemptedUserIds> result = groupResults.getMappedResults();
    	return result;
    }
    
    public long countAttemptsForChallengeId(String challengeId, Long beforeTime){
        
        Query query = new Query();
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(challengeId));
        query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CREATION_TIME).gte(beforeTime));
        
        return queryCount(query, ChallengeAttempt.class);
        
    }

}
