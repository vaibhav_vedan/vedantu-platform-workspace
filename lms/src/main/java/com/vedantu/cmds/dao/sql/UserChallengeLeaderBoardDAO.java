package com.vedantu.cmds.dao.sql;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.cmds.constants.ConstantsGlobal;
import com.vedantu.cmds.entities.challenges.UserChallengeLeaderBoard;
import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractSqlDAO;

@Service
public class UserChallengeLeaderBoardDAO extends AbstractSqlDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UserChallengeLeaderBoardDAO.class);

	public UserChallengeLeaderBoardDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;

	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}

	public void create(UserChallengeLeaderBoard userChallengeLeaderBoard, String callingUserId) {
		try {
			if (userChallengeLeaderBoard != null) {
				saveEntity(userChallengeLeaderBoard, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create UserLeaderBoard: " + ex.getMessage());
		}
	}

	// TODO : category clarify
	public UserChallengeLeaderBoard getByUserId(Long userId, LeaderBoardType type, Long startTime, String category) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		UserChallengeLeaderBoard userChallengeLeaderBoard = null;
		try {
			userChallengeLeaderBoard = getByUserId(userId, session, type, startTime, category);
		} catch (Exception ex) {
			logger.error("getByUserId UserLeaderBoard: " + ex.getMessage());
			userChallengeLeaderBoard = null;
		} finally {
			session.close();
		}
		return userChallengeLeaderBoard;
	}

	public UserChallengeLeaderBoard getByUserId(Long userId, Session session, LeaderBoardType type, Long startTime,
			String category) {
		UserChallengeLeaderBoard userChallengeLeaderBoard = null;
		if (userId != null) {
			Criteria cr = session.createCriteria(UserChallengeLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.USER_ID, userId));
			cr.add(Restrictions.eq(ConstantsGlobal.LEADER_BOARD_TYPE, type));
			if(!type.equals(LeaderBoardType.OVERALL)){
				cr.add(Restrictions.eq(ConstantsGlobal.START_TIME, startTime));
			}
			
			cr.add(Restrictions.eq(ConstantsGlobal.EVENT_CATEGORY, category));
						
			cr.setFetchMode("hintsCountMap", FetchMode.JOIN);
			List<UserChallengeLeaderBoard> results = runQuery(session, cr, UserChallengeLeaderBoard.class);
			if (!CollectionUtils.isEmpty(results)) {
				if (results.size() > 1) {
					logger.info("Duplicate entries found for id : " + userId);//if hint taken will always be multiple//need to relook
				}
				userChallengeLeaderBoard = results.get(0);
			} else {
				logger.info("getByUserId UserLeaderBoard : no UserLeaderBoard found " + userId);
			}
		}
		return userChallengeLeaderBoard;
	}

	public void update(UserChallengeLeaderBoard p, Session session, String callingUserId) {
		if (p != null) {
			logger.info("update UserLeaderBoard: " + p.toString());
			updateEntity(p, session, callingUserId);
		}
	}

	public List<UserChallengeLeaderBoard> getUserLeaderBoardForCategory(String category, LeaderBoardType type,
			Long startTime, int start, int size) {
		List<UserChallengeLeaderBoard> leaderBoards = new ArrayList<>();
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(UserChallengeLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.EVENT_CATEGORY, category));
			cr.add(Restrictions.eq(ConstantsGlobal.LEADER_BOARD_TYPE, type));
			if(!type.equals(LeaderBoardType.OVERALL)){
				cr.add(Restrictions.eq(ConstantsGlobal.START_TIME, startTime));
			}
			cr.addOrder(Order.desc(ConstantsGlobal.POINTS)).addOrder(Order.asc("timeRate"));
			cr.setFetchMode("hintsCountMap", FetchMode.DEFAULT);
			cr.setFirstResult(start);
			cr.	setMaxResults(size);
			List<UserChallengeLeaderBoard> results = runQuery(session, cr, UserChallengeLeaderBoard.class);
			return results;
		} catch (Exception e) {
			logger.error("getUserLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return leaderBoards;
	}

	public Long getUserRank(String category, LeaderBoardType type, Long startTime, long userPoints,
			double userTimeRate) {
		SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Long rank = null;
		try {
			Criteria cr = session.createCriteria(UserChallengeLeaderBoard.class);
			cr.add(Restrictions.eq(ConstantsGlobal.EVENT_CATEGORY, category));
			cr.add(Restrictions.eq(ConstantsGlobal.LEADER_BOARD_TYPE, type));
			if(!type.equals(LeaderBoardType.OVERALL)){
				cr.add(Restrictions.eq(ConstantsGlobal.START_TIME, startTime));
			}
			Disjunction disjunction = Restrictions.disjunction();
			disjunction.add(Restrictions.gt(ConstantsGlobal.POINTS, userPoints));
			disjunction.add(Restrictions.and(Restrictions.eq(ConstantsGlobal.POINTS, userPoints),
					Restrictions.lt("timeRate", userTimeRate)));
			cr.add(disjunction);

			cr.setFetchMode("hintsCountMap", FetchMode.DEFAULT);
			cr.setProjection(Projections.rowCount());

			List results = runQuery(session, cr, UserChallengeLeaderBoard.class);
			if (!results.isEmpty()) {
				rank = (Long) results.get(0);
				rank++;
			}
		} catch (Exception e) {
			logger.error("getUserLeaderBoardForCategory failed : " + e);
		} finally {
			session.close();
		}
		return rank;
	}

}
