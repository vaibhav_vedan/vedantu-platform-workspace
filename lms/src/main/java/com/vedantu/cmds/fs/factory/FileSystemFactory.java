package com.vedantu.cmds.fs.factory;

import org.apache.commons.lang.StringUtils;

import com.vedantu.cmds.fs.handler.IFileSystemHandler;
import com.vedantu.cmds.fs.handler.LocalFileSystemHandler;
import com.vedantu.util.ConfigUtils;

public class FileSystemFactory {

	public static final FileSystemFactory INSTANCE = new FileSystemFactory();

	private final IFileSystemHandler fs;
	private final LocalFileSystemHandler localFs;
	private final LocalFileSystemHandler tempFs;

	private FileSystemFactory() {
		// TODO : Check wtf is wrong with initializing it as a member
		String fsToUse = ConfigUtils.INSTANCE.getStringValue("fs.class");
		IFileSystemHandler fs = null;
		try {
			Class<?> clazz = Class.forName(fsToUse);
			fs = (IFileSystemHandler) clazz.newInstance();
		} catch (ClassNotFoundException e) {
		} catch (InstantiationException e) {
		} catch (IllegalAccessException e) {
		}
		this.fs = fs;
		this.localFs = StringUtils.equals(fsToUse, LocalFileSystemHandler.class.getName()) ? (LocalFileSystemHandler) fs
				: new LocalFileSystemHandler();
		this.tempFs = new LocalFileSystemHandler(false, ConfigUtils.INSTANCE.getStringValue("util.temp_dir"));
	}

	public IFileSystemHandler getFS() {

		return fs;
	}

	public LocalFileSystemHandler getLocalFS() {

		return localFs;
	}

	public LocalFileSystemHandler getTempFS() {

		return tempFs;
	}

}
