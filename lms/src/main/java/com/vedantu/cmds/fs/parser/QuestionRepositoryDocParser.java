package com.vedantu.cmds.fs.parser;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;

import com.vedantu.cmds.enums.Indexable;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.wmf.tosvg.WMFTranscoder;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.apache.poi.xwpf.usermodel.Document;
import org.apache.poi.xwpf.usermodel.UnderlinePatterns;
import org.apache.poi.xwpf.usermodel.VerticalAlign;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSym;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.constants.QuestionSetFileConstants;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.enums.MetadataParts;
import com.vedantu.cmds.managers.AmazonS3Manager;
import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.cmds.pojo.EntireQuestion;
import com.vedantu.cmds.pojo.ParseQuestionMetadata;
import com.vedantu.cmds.pojo.QuestionParts;
import com.vedantu.cmds.pojo.QuestionTable;
import com.vedantu.cmds.pojo.QuestionTableRow;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.SolutionFormat;
import com.vedantu.cmds.pojo.SymbolMap;
import com.vedantu.cmds.utils.CMDSCommonUtils;
import com.vedantu.cmds.utils.ImageDisplayURLUtil;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.lms.cmds.enums.StorageType;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.poi.xwpf.usermodel.IBodyElement;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.dozer.DozerBeanMapper;

@Service
public class QuestionRepositoryDocParser {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(QuestionRepositoryDocParser.class);

    public static final String TEMP_QUESTION_IMAGES_DIR = "images";
    private static SymbolMap symbols = SymbolMap.getSymbolMap();

    private static final String UNICODE_REGEX = "([\\x20-\\xFE]*)([^\\x20-\\xFE]?)([\\x20-\\xFE]*)";
    private static final String PRINTABLE_UNICODE_REGEX = "([\\x20-\\xFE]?)";
    static final Pattern p = Pattern.compile(UNICODE_REGEX, Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);
    static final Pattern printablePattern = Pattern.compile(PRINTABLE_UNICODE_REGEX,
            Pattern.UNICODE_CASE | Pattern.CASE_INSENSITIVE);

    private static final String IMAGE_ASPECT_RATIO = "IMAGE_ASPECT_RATIO";

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    @SuppressWarnings("resource")
    public List<CMDSQuestion> getContent(String providedQuestionSetFileName,
            File file, String userId, ParseQuestionMetadata globalParseMetadata,
            AccessLevel accessLevel) throws Exception {
        return getContent(providedQuestionSetFileName, file, userId, globalParseMetadata, accessLevel, null);
    }
    @SuppressWarnings("resource")
    public List<CMDSQuestion> getContent(String providedQuestionSetFileName,
            File file, String userId, ParseQuestionMetadata globalParseMetadata,
            AccessLevel accessLevel, ParseQuestionMetadata overrideMetadata) throws Exception {

        XWPFDocument doc = new XWPFDocument(new FileInputStream(file));

        List<EntireQuestion> allQuestions = processIBodyElements(doc.getBodyElements(), accessLevel, globalParseMetadata, overrideMetadata);

        List<CMDSQuestion> questions = new ArrayList<>();
        for (int questionIndexInQuestionSet = 0; questionIndexInQuestionSet < allQuestions.size(); questionIndexInQuestionSet++) {
            try {
                EntireQuestion eq = allQuestions.get(questionIndexInQuestionSet);

                String questionSetFileName = providedQuestionSetFileName;
                if (StringUtils.isEmpty(questionSetFileName)) {
                    questionSetFileName = file.getName();
                }

                CMDSQuestion question = eq.toQuestion(eq, userId, questionSetFileName);

                if (question != null) {
                    question.addHook();
                    questions.add(question);
                }
            } catch (BadRequestException exception) {
                logger.warn(exception.getErrorCode() + " For Question " + (questionIndexInQuestionSet + 1) + ": " + exception.getMessage());
                throw exception;
            }

        }
        if (globalParseMetadata != null) {
            logger.info("global metadata is : " + globalParseMetadata);
        }

        if (ArrayUtils.isNotEmpty(questions)) {
            boolean hasslno = false;
            boolean hasquestionno = false;
            for (CMDSQuestion question : questions) {
                if (question.getSlNoInBook() > 0) {
                    hasslno = true;
                }
                if (StringUtils.isNotEmpty(question.getQuestionNo())) {
                    hasquestionno = true;
                }
            }
            if (!hasslno) {
                for (int slno = 0; slno < questions.size(); slno++) {
                    CMDSQuestion question = questions.get(slno);
                    question.setSlNoInBook((slno + 1));
                }
            }
            if (!hasquestionno) {
                for (int slno = 0; slno < questions.size(); slno++) {
                    CMDSQuestion question = questions.get(slno);
                    question.setQuestionNo(question.getSlNoInBook() + "");
                }
            }
        }
        return questions;
    }

    
    
    @SuppressWarnings("resource")
    public String getHtmlFromDocFile(File file, UploadTarget target) throws Exception {
        String resp = "";
        XWPFDocument doc = new XWPFDocument(new FileInputStream(file));
        List<IBodyElement> iBodyElements = doc.getBodyElements();
        int i = 0;
        Map<String, Integer> listCounterMap = new HashMap<>();
        for (IBodyElement elem : iBodyElements) {
            i++;
            if (elem instanceof XWPFParagraph) {
                XWPFParagraph para = (XWPFParagraph) elem;
                logger.info("list " + para.getNumLevelText() + ", " + para.getNumID() + ", "
                        + para.getNumFmt() + ", " + para.getNumIlvl()
                        + ", " + para.getNumStartOverride());
                logger.info("paraNo : " + i + " and para text : " + para.getParagraphText()
                        + " ,  getIndentationFirstLine: " + para.getIndentationFirstLine()
                        + " ,  getIndentationHanging: " + para.getIndentationHanging()
                        + " ,  getIndentationLeft : " + para.getIndentationLeft());
                List<CMDSImageDetails> cmdsImageDetailsList = new ArrayList<>(); // Holds
                String htmlText = "";

                String docParaText = processDocPara(para, cmdsImageDetailsList,target, AccessLevel.PUBLIC, true,true);
                if (para.getNumID() != null) {
                    String listCountKey = para.getNumID() + "-" + para.getNumIlvl();
                    if (!listCounterMap.containsKey(listCountKey)) {
                        listCounterMap.put(listCountKey, 0);
                    }
                    listCounterMap.put(listCountKey, listCounterMap.get(listCountKey) + 1);
                    String listItemText = getListItemText(para.getNumLevelText(),
                            listCounterMap.get(listCountKey), para.getNumFmt().toLowerCase(),
                            para.getNumIlvl().intValue());
                    logger.info(para.getNumLevelText()+":"+listCounterMap.get(listCountKey)+":"+para.getNumFmt().toLowerCase()+":"+para.getNumIlvl().intValue());
                    htmlText += "<li class='listitem-"
                            + listCounterMap.get(listCountKey)
                            + " parent-ul-" + listCountKey + " listitem'>"
                            + listItemText + " " + docParaText + "</li>";
                } else {
                    htmlText += docParaText;
                }
                htmlText = CMDSCommonUtils.handleStartingSpacesAndTabs(htmlText);
                htmlText = "<br>" + htmlText;
                resp+=htmlText;
            } else if (elem instanceof XWPFTable) {
                logger.info("paraNo : " + i + " found a table, looping through it");
                QuestionTable questionTable = new QuestionTable();
                List<CMDSImageDetails> cellcmdsImageDetailsList = new ArrayList<>();
                List<XWPFTableRow> rows = ((XWPFTable) elem).getRows();
                logger.info("total rows in the table " + rows.size());
                boolean foundQuestionsInsideTable = false;
                int rowno = 0;
                String allRowsText = "";
                for (XWPFTableRow row : rows) {
                    rowno++;
                    QuestionTableRow questionTableRow = new QuestionTableRow();
                    List<XWPFTableCell> cells = row.getTableCells();
                    logger.info("total cells in the row " + cells.size());
                    String allCellsText = "";
                    for (XWPFTableCell cell : cells) {
                        List<IBodyElement> cellItems = cell.getBodyElements();
                        int cellParaNo = 0;
                        String singleCellText = "";
                        for (IBodyElement cellElem : cellItems) {
                            cellParaNo++;
                            if (cellElem instanceof XWPFParagraph) {
                                XWPFParagraph cellpara = (XWPFParagraph) cellElem;
                                logger.info("global paraNo : " + i + " rowno " + rowno
                                        + " cellParaNo " + cellParaNo
                                        + " and cell para text : " + cellpara.getParagraphText()
                                        + " ,  IndentationFirstLine: " + cellpara.getIndentationFirstLine());
                                String htmlText = processDocPara(cellpara, cellcmdsImageDetailsList,target, AccessLevel.PUBLIC, true, true);
                                if (CMDSCommonUtils.doesStartsWith(htmlText, QuestionSetFileConstants.PREFIX_QUESTION)) {
                                    logger.info("found a question prefix, processing the entire table as source of questions");
                                    break;
                                }
                                singleCellText += htmlText;
                            } else {
                                logger.warn("non para found inside cell " + cellElem.getClass());
                            }
                        }
                        if (foundQuestionsInsideTable) {
                            break;
                        }
                        logger.info("cell text " + singleCellText);
                        allCellsText += "<td>" + singleCellText + "</td>";
                    }
                    if (foundQuestionsInsideTable) {
                        continue;
                    }
                    allRowsText += "<tr>" + allCellsText + "</tr>";
                }
                allRowsText = "<div class='uploaded-question-table-par'><table class='uploaded-question-table'>" + allRowsText + "</table></div>";
                logger.info("adding table to the question");
               resp+="<br>"+allRowsText;
            } else {
                logger.warn("paraNo : " + i + " non para/table found " + elem.getClass());
            }
        }
        return resp;
    }

     private List<EntireQuestion> processIBodyElements(List<IBodyElement> iBodyElements,
            AccessLevel accessLevel, ParseQuestionMetadata globalParseMetadata)
            throws Exception {
         return processIBodyElements(iBodyElements, accessLevel, globalParseMetadata, null);
     }

    private List<EntireQuestion> processIBodyElements(List<IBodyElement> iBodyElements,
            AccessLevel accessLevel, ParseQuestionMetadata globalParseMetadata, ParseQuestionMetadata overrideMetadata)
            throws Exception {
        logger.info("found set iBodyElements, will parse for questions ");
        List<EntireQuestion> allQuestions = new ArrayList<>();
        EntireQuestion entireQuestion = null;
        boolean seenQuestion = false;
        ParseQuestionMetadata questionParseMetadata = null;
        QuestionParts oldQuestionPart = null;
        int i = 0;
        Map<String, Integer> listCounterMap = new HashMap<>();
        try {
            for (IBodyElement elem : iBodyElements) {
                i++;
                if (elem instanceof XWPFParagraph) {
                    XWPFParagraph para = (XWPFParagraph) elem;
                    logger.info("list " + para.getNumLevelText() + ", " + para.getNumID() + ", "
                            + para.getNumFmt() + ", " + para.getNumIlvl()
                            + ", " + para.getNumStartOverride());
                    logger.info("paraNo : " + i + " and para text : " + para.getParagraphText()
                            + " ,  getIndentationFirstLine: " + para.getIndentationFirstLine()
                            + " ,  getIndentationHanging: " + para.getIndentationHanging()
                            + " ,  getIndentationLeft : " + para.getIndentationLeft());

                    boolean newPara = true;
                    List<CMDSImageDetails> cmdsImageDetailsList = new ArrayList<>(); // Holds
                    String htmlText = "";

                    String docParaText = processDocPara(para, cmdsImageDetailsList, accessLevel, false);
                    if (para.getNumID() != null) {
                        String listCountKey = para.getNumID() + "-" + para.getNumIlvl();
                        if (!listCounterMap.containsKey(listCountKey)) {
                            listCounterMap.put(listCountKey, 0);
                        }
                        listCounterMap.put(listCountKey, listCounterMap.get(listCountKey) + 1);
                        String listItemText = getListItemText(para.getNumLevelText(),
                                listCounterMap.get(listCountKey), para.getNumFmt().toLowerCase(),
                                para.getNumIlvl().intValue());
                        htmlText += "<li class='listitem-"
                                + listCounterMap.get(listCountKey)
                                + " parent-ul-" + listCountKey + " listitem'>"
                                + listItemText + " " + docParaText + "</li>";
                    } else {
                        htmlText += docParaText;
                    }
                    int leftMargin = 0;
                    if (para.getIndentationFirstLine() > 0) {
                        leftMargin += para.getIndentationFirstLine() / 15;
                    }
                    if (para.getIndentationLeft() > 0) {
                        leftMargin += para.getIndentationLeft() / 15;
                    }

                    // read metadata here
//            if ((htmlText.replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").trim().toLowerCase()
//                    .startsWith(QuestionSetFileConstants.PREFIX_TITLE))) {
//                logger.info("Creating for global metadata : ");
//                globalParseMetadata = new ParseQuestionMetadata();
//            }
                    if (globalParseMetadata != null && !seenQuestion) {
                        logger.info("parsing for global metadata : ");
                        //using para text to read metadata
                        globalParseMetadata.accumulateMetadataInfo(para.getParagraphText(), false);
                    }

                    logger.info("html text : " + htmlText);
                    if (CMDSCommonUtils.doesStartsWith(htmlText, QuestionSetFileConstants.PREFIX_QUESTION)) {
                        seenQuestion = true;
                        logger.info("previous entire question object : " + entireQuestion);
                        entireQuestion = new EntireQuestion();
                        logger.info("Cloning  global metadata : ");
                        questionParseMetadata = mapper.map(globalParseMetadata, ParseQuestionMetadata.class);

                        logger.info("Original  global metadata : " + globalParseMetadata);
                        logger.info("cloned  global metadata : " + questionParseMetadata);

                        allQuestions.add(entireQuestion);
                    }
                    if (null != entireQuestion) {
                        // this will parse the local metadata
                        logger.info("Parsing local question metadata : ");
                        MetadataParts parsedMetadataPart = questionParseMetadata.accumulateMetadataInfo(para.getParagraphText(), true);
                        if (parsedMetadataPart == null) {
                            logger.info("accumulating question data like question,solution,answer,cola,colb,options");
                            QuestionParts newQuestionPart = entireQuestion.accumulateQuestionInfo(seenQuestion,
                                    htmlText, htmlText, cmdsImageDetailsList,
                                    false, false, newPara, leftMargin);
                            logger.info("new questionPart " + newQuestionPart + ", old QuestionPart " + oldQuestionPart);

                            oldQuestionPart = newQuestionPart;
                            if(questionParseMetadata != null && overrideMetadata != null){
                                questionParseMetadata.overrideMetadata(overrideMetadata);
                            }
                            entireQuestion.metadata = questionParseMetadata;
                        } else {
                            logger.info("found parsedMetadataPart "
                                    + parsedMetadataPart.name() + ", resetting entireQuestion state");
                            entireQuestion.resetState();
                        }
                    }
                    logger.info("after accumulation entire question object : " + entireQuestion);
                } else if (elem instanceof XWPFTable) {
                    logger.info("paraNo : " + i + " found a table, looping through it");
                    QuestionTable questionTable = new QuestionTable();
                    List<CMDSImageDetails> cellcmdsImageDetailsList = new ArrayList<>();
                    List<XWPFTableRow> rows = ((XWPFTable) elem).getRows();
                    logger.info("total rows in the table " + rows.size());
                    boolean foundQuestionsInsideTable = false;
                    int rowno = 0;
                    String allRowsText = "";
                    for (XWPFTableRow row : rows) {
                        rowno++;
                        QuestionTableRow questionTableRow = new QuestionTableRow();
                        List<XWPFTableCell> cells = row.getTableCells();
                        logger.info("total cells in the row " + cells.size());
                        String allCellsText = "";
                        for (XWPFTableCell cell : cells) {
                            List<IBodyElement> cellItems = cell.getBodyElements();
                            int cellParaNo = 0;
                            String singleCellText = "";
                            for (IBodyElement cellElem : cellItems) {
                                cellParaNo++;
                                if (cellElem instanceof XWPFParagraph) {
                                    XWPFParagraph cellpara = (XWPFParagraph) cellElem;
                                    logger.info("global paraNo : " + i + " rowno " + rowno
                                            + " cellParaNo " + cellParaNo
                                            + " and cell para text : " + cellpara.getParagraphText()
                                            + " ,  IndentationFirstLine: " + cellpara.getIndentationFirstLine());
                                    String htmlText = processDocPara(cellpara, cellcmdsImageDetailsList, accessLevel, false);
                                    if (CMDSCommonUtils.doesStartsWith(htmlText, QuestionSetFileConstants.PREFIX_QUESTION)) {
                                        logger.info("found a question prefix, processing the entire table as source of questions");
                                        List<EntireQuestion> _entireQuestions = processIBodyElements(cellItems, accessLevel, globalParseMetadata,overrideMetadata);
                                        allQuestions.addAll(_entireQuestions);
                                        foundQuestionsInsideTable = true;
                                        break;
                                    }
                                    singleCellText += htmlText;
                                } else {
                                    logger.warn("non para found inside cell " + cellElem.getClass());
                                }
                            }
                            if (foundQuestionsInsideTable) {
                                break;
                            }
                            logger.info("cell text " + singleCellText);
                            allCellsText += "<td>" + singleCellText + "</td>";
//                        QuestionTableCell questionTableCell = new QuestionTableCell();
//                        questionTableCell.setNewText(htmlText);
//                        questionTableCell.setUuidImages(new HashSet<>(cellcmdsImageDetailsList));
//                        questionTableRow.addCell(questionTableCell);
                        }
                        if (foundQuestionsInsideTable) {
                            continue;
                        }
                        allRowsText += "<tr>" + allCellsText + "</tr>";
//                    questionTable.addRow(questionTableRow);
                    }
//                if (ArrayUtils.isNotEmpty(questionTable.getRows()) && null != entireQuestion) {
//                    logger.info("inserting table into question " + questionTable);
//                    entireQuestion.addTable(questionTable);
//                } else {
//                    logger.warn("no question to add table data into");
//                }
                    allRowsText = "<div class='uploaded-question-table-par'><table class='uploaded-question-table'>" + allRowsText + "</table></div>";
                    logger.info("adding table to the question");
                    if (Objects.nonNull(entireQuestion)) {
                        entireQuestion.accumulateQuestionInfo(false, null, allRowsText, cellcmdsImageDetailsList,
                                false, false, true, 0);
                    }
                } else {
                    logger.warn("paraNo : " + i + " non para/table found " + elem.getClass());
                }
            }
        } catch (Exception e) {
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage() + ", line no " + i + " line text " );
        }
        if (ArrayUtils.isNotEmpty(allQuestions) && !listCounterMap.keySet().isEmpty()) {
            for (EntireQuestion _entireQuestion : allQuestions) {
                QuestionParts.QUESTION.addParentUlTags(_entireQuestion, listCounterMap);
                QuestionParts.SOLUTION.addParentUlTags(_entireQuestion, listCounterMap);
            }
        }
        return allQuestions;
    }
    private String processDocPara(XWPFParagraph para,
                                  List<CMDSImageDetails> cmdsImageDetailsList,
                                  AccessLevel accessLevel,
                                  boolean imageEmbeded)
            throws TranscoderException, BadRequestException, IOException {
        return processDocPara(para,cmdsImageDetailsList, null, accessLevel,imageEmbeded, false);
    }
    
    private String processDocPara(XWPFParagraph para,
                                  List<CMDSImageDetails> cmdsImageDetailsList,
                                  UploadTarget target,
                                  AccessLevel accessLevel,
                                  boolean imageEmbeded,
                                  boolean forceTextStyle)
            throws TranscoderException, BadRequestException, IOException {
        String htmlText = "";
        for (XWPFRun run : para.getRuns()) {
            logger.info("isbold: " + run.isBold());
            logger.info("isitalic: " + run.isItalic());
            htmlText += annotateHtmlStyle(run,forceTextStyle);
            if (Objects.nonNull(run.getEmbeddedPictures())) {
                for (XWPFPicture p : run.getEmbeddedPictures()) {
                    String imgType = null;
                    byte[] imgBytes = p.getPictureData().getData();
                    if (Document.PICTURE_TYPE_JPEG == p.getPictureData().getPictureType()
                            || Document.PICTURE_TYPE_PNG == p.getPictureData().getPictureType()
                            || Document.PICTURE_TYPE_GIF == p.getPictureData().getPictureType()) {
                        imgType = Document.PICTURE_TYPE_PNG == p.getPictureData().getPictureType() ? "png" : "jpg";
                    } else if (Document.PICTURE_TYPE_WMF == p.getPictureData().getPictureType()) {
                        WMFTranscoder wmfTranscoder = new WMFTranscoder();
                        wmfTranscoder.addTranscodingHint(WMFTranscoder.KEY_HEIGHT, 100f);
                        wmfTranscoder.addTranscodingHint(WMFTranscoder.KEY_WIDTH, 100f);
                        TranscoderInput wmfTranscoderInput = new TranscoderInput(
                                new ByteArrayInputStream(imgBytes));
                        ByteArrayOutputStream svgBAOS = new ByteArrayOutputStream();
                        TranscoderOutput wmfTranscoderOutput = new TranscoderOutput(svgBAOS);
                        wmfTranscoder.transcode(wmfTranscoderInput, wmfTranscoderOutput);

                        JPEGTranscoder jpegTranscoder = new JPEGTranscoder();
                        jpegTranscoder.addTranscodingHint(JPEGTranscoder.KEY_HEIGHT, 100f);
                        jpegTranscoder.addTranscodingHint(JPEGTranscoder.KEY_WIDTH, 100f);
                        jpegTranscoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, (float) 0.8);
                        TranscoderInput jpegInput = new TranscoderInput(
                                new ByteArrayInputStream(svgBAOS.toByteArray()));
                        ByteArrayOutputStream jpegBAOS = new ByteArrayOutputStream();
                        TranscoderOutput jpegOutput = new TranscoderOutput(jpegBAOS);
                        jpegTranscoder.transcode(jpegInput, jpegOutput);

                        imgType = "jpg";

                    } else {
                        logger.info("GET UNICODE CHARACTERS HERE else: " + run.toString());
                    }

                    if (Objects.isNull(imgType)) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "unsupported image type : " + p.getPictureData().getPictureType()
                                + " for " + p.getPictureData().getFileName());
                    }


                    logger.info("The image's extension is {} and file name is {}",p.getPictureData().suggestFileExtension(),p.getPictureData().getFileName());

                    byte[] imageData=p.getPictureData().getData();

                    String randomUUID = UUID.randomUUID().toString();
                    File outputfile = File.createTempFile(randomUUID, ".png");

                    InputStream in = new ByteArrayInputStream(imageData);
                    BufferedImage bImageFromConvert = ImageIO.read(in);
                    ImageIO.write(bImageFromConvert, "png", outputfile);

                    /*
                    // Didn't work
                    OutputStream os =new FileOutputStream(outputfile);
                    Iterator<ImageWriter> writers =  ImageIO.getImageWritersByFormatName("png");
                    ImageWriter writer = writers.next();

                    ImageOutputStream ios = ImageIO.createImageOutputStream(os);
                    writer.setOutput(ios);

                    ImageWriteParam param = writer.getDefaultWriteParam();
                    if(param.canWriteCompressed()) {
                        param.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
                        param.setCompressionQuality(0.5f);
                        writer.write(null, new IIOImage(bImageFromConvert, null, null), param);
                    }else{
                        logger.info("No write compression supported for image id {}",outputfile.getName());
                    }
                    os.close();
                    ios.close();
                    writer.dispose();
                    */

                    // Calculate aspect ratio of the image
                    Map<String, String> params = new HashMap<>();
                    Float aspectRatio = null;
                    try {
                        Long cX = p.getCTPicture().getSpPr().getXfrm().getExt().getCx();
                        Long cY = p.getCTPicture().getSpPr().getXfrm().getExt().getCy();
                        logger.info("cx " + cX + ", cY " + cY);
                        if (cY != 0l) {
                            aspectRatio = Float.valueOf(cX) / Float.valueOf(cY);
                            params.put(IMAGE_ASPECT_RATIO, String.valueOf(aspectRatio));
                        }
                    } catch (Exception ex) {
                        logger.info("Error capturing the aspect ratio : " + ex.toString());
                    }

                    String fileName = outputfile.getName();
                    UploadTarget uploadTarget = target;
                    if(target == null){
                        uploadTarget = UploadTarget.QUESTIONSETS;
                        if (AccessLevel.PUBLIC.equals(accessLevel)) {
                            uploadTarget = UploadTarget.PUBLIC_QUESTIONSETS;
                        }
                    }
                    amazonS3Manager.uploadImage(fileName, outputfile, params, uploadTarget);
                    String publicUrl = amazonS3Manager.getImageUrl(fileName, uploadTarget);

                    // clean up
                    logger.info("The size of output file is {} kb",outputfile.getTotalSpace()*0.001);
                    outputfile.delete();

                    logger.debug("random uuid" + randomUUID + " public url : " + publicUrl);
                    CMDSImageDetails cmdsImageDetails = new CMDSImageDetails(fileName, publicUrl, null, null, null,
                            StorageType.AMAZON_S3, aspectRatio, uploadTarget);
                    cmdsImageDetailsList.add(cmdsImageDetails);

                    logger.info("input image type : " + imgType + ", and image will be saved as jpg");
                    htmlText += ImageDisplayURLUtil.getEmbededHtml(publicUrl, fileName, imageEmbeded);
                }
            }
        }
        return htmlText;
    }

    private static final String SPACE_UNICODE = "\\uF020";
    private String annotateHtmlStyle(XWPFRun run) {
        return annotateHtmlStyle(run);
    }
    
    @SuppressWarnings("deprecation")
    private String annotateHtmlStyle(XWPFRun run, boolean forceStyle) throws BadRequestException{

        String htmlRunText = "";
        logger.info("<span style=\"font-family:" + run.getFontFamily() + ";\">" + "run string: " + run.toString());
        if (run.getFontFamily() != null) {
            String rnTxt = run.toString();
            if (StringUtils.isEmpty(rnTxt)) {
                rnTxt = getRunTextFromDOM(run);
            }
            if (rnTxt == null) {
                rnTxt = "";
            }
            String unscapeString = rnTxt.replaceAll(SPACE_UNICODE, "").replaceAll(" ", "");
            boolean hasSpace = (run.toString().length() != unscapeString.length());
            try {
                if (isSpecialCharacter(unscapeString)) {
                    htmlRunText = StringEscapeUtils.unescapeJava(symbols.symbol.get(unscapeString));
                    htmlRunText += hasSpace ? " " : "";
                } else {
                    StringBuilder sb = new StringBuilder();
                    Matcher m = p.matcher(run.toString());
                    while (m.find()) {
                        sb.append(m.group(1))
                                .append(StringUtils.isNotEmpty(m.group(2))
                                        && symbols.symbol.containsKey(StringEscapeUtils.unescapeJava(m.group(2)))
                                        ? StringEscapeUtils.unescapeJava(symbols.symbol.get(m.group(2)))
                                        : m.group(2))
                                .append(m.group(3));
                    }
                    htmlRunText += sb.toString();
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new BadRequestException(ErrorCode.INVALID_PARAMETER, e.getMessage());
            }
        } else {
            String rnTxt = run.toString();
            if (StringUtils.isEmpty(rnTxt)) {
                rnTxt = getRunTextFromDOM(run);
            }
            if (rnTxt == null) {
                rnTxt = "";
            }
            StringBuilder sb = new StringBuilder();
            logger.info("is run text empty: " + StringUtils.isEmpty(run.toString()));
            Matcher m = p.matcher(rnTxt);
            while (m.find()) {
                sb.append(m.group(1))
                        .append(StringUtils.isNotEmpty(m.group(2))
                                && symbols.symbol.containsKey(StringEscapeUtils.unescapeJava(m.group(2)))
                                ? StringEscapeUtils.unescapeJava(symbols.symbol.get(m.group(2)))
                                : m.group(2))
                        .append(m.group(3));
            }
            htmlRunText += sb.toString();
        }
        String matchString = htmlRunText.trim().toLowerCase();

        List<String> allPrefixes = QuestionSetFileConstants.INSTANCE.allPrefixes;
        boolean doNothing = false;
        if (ArrayUtils.isNotEmpty(allPrefixes)) {
            for (String prefix : allPrefixes) {
                if (matchString.startsWith(prefix.substring(0, prefix.indexOf(":")))) {
                    doNothing = true;
                    break;
                }
            }
        }
        if (CMDSCommonUtils.doesStartsWith(matchString, QuestionSetFileConstants.PREFIX_QUESTION)
                || CMDSCommonUtils.doesStartsWith(matchString, QuestionSetFileConstants.PREFIX_SOLUTION)) {
            doNothing = true;
        }

//
//        if (matchString.startsWith(QuestionSetFileConstants.PREFIX_OPTIONS)
//                || matchString.startsWith(QuestionSetFileConstants.PREFIX_TYPE)
//                || matchString.startsWith(QuestionSetFileConstants.PREFIX_ANSWER)
//                || CMDSCommonUtils.doesStartsWith(matchString, QuestionSetFileConstants.PREFIX_QUESTION)
//                || CMDSCommonUtils.doesStartsWith(matchString, QuestionSetFileConstants.PREFIX_SOLUTION)
//                || matchString.startsWith(QuestionSetFileConstants.PREFIX_COLUMNA)
//                || matchString.startsWith(QuestionSetFileConstants.PREFIX_COLUMNB)) {
//            // do nothing
//        } else {
        if (!doNothing || forceStyle) {
            VerticalAlign verticalAlign = run.getSubscript();
            if (VerticalAlign.BASELINE == verticalAlign) {
                // TODO: Fix this by maintaining global/previous baseline
                int textPosWrtBaseline = run.getTextPosition() + VerticalAlign.BASELINE.getValue();
                verticalAlign = (textPosWrtBaseline > 0) ? VerticalAlign.SUPERSCRIPT
                        : (textPosWrtBaseline < 0 ? VerticalAlign.SUBSCRIPT : VerticalAlign.BASELINE);
            }
            switch (verticalAlign) {
                case SUBSCRIPT:
                    htmlRunText = "<sub>" + htmlRunText + "</sub>";
                    break;
                case SUPERSCRIPT:
                    htmlRunText = "<sup>" + htmlRunText + "</sup>";
                    break;
                case BASELINE:
                default:
                    break;
            }
            logger.info("run text after alignment mapping: " + htmlRunText);

            if (run.isStrike()) {
                htmlRunText = "<strike>" + htmlRunText + "</strike>";
            }
            if (run.isBold()) {
                htmlRunText = "<b>" + htmlRunText + "</b>";
            }
            if (run.isItalic()) {
                htmlRunText = "<i>" + htmlRunText + "</i>";
            }
            if (run.getUnderline() != UnderlinePatterns.NONE) {
                htmlRunText = "<u>" + htmlRunText + "</u>";
            }
            if (run.getColor() != null && !run.getColor().equals("000000")) {
                htmlRunText = "<span color='#" + run.getColor() + "'>" + htmlRunText + "</span>";
            }
        }
        logger.info("htmlRunText after all annotations : " + htmlRunText);
        return htmlRunText;
    }

    private boolean isSpecialCharacter(String unscapeString) throws BadRequestException {

        try {
            return symbols.symbol.containsKey(StringEscapeUtils.unescapeJava(unscapeString));
        }catch (Exception e){
            logger.warn(e.getMessage(), e);
        }

        return false;
    }

    private String getRunTextFromDOM(XWPFRun run) {

        @SuppressWarnings("deprecation")
        CTSym[] ctSym = run.getCTR().getSymArray();
        String rnTxt = null;
        if (ctSym != null) {
            for (CTSym s : ctSym) {
                String str = new String(s.getChar());
                logger.info("symbol string: " + str);
                if (s.getDomNode().hasAttributes() && StringUtils.equalsIgnoreCase(s.getFont(), "Symbol")) {
                    str = s.getDomNode().getAttributes().getNamedItem("w:char").getNodeValue();
                    Matcher m = printablePattern.matcher(str);
                    try {
                        if (m.find()) {
                            str = StringEscapeUtils.unescapeJava("\\uF0" + str.substring(2));
                        } else {
                            str = StringEscapeUtils.unescapeJava("\\u" + str);
                        }
                    } catch (Exception e) {
                        logger.error(e.getMessage(), e);
                    }
                }
                rnTxt = str;
            }
        }
        return rnTxt;
    }

    public void populateAWSUrls(List<CMDSQuestion> questions) {
        // Generate AWS urls
        if (!CollectionUtils.isEmpty(questions)) {
            for (CMDSQuestion question : questions) {
                if (question.getQuestionBody() != null && !CollectionUtils.isEmpty(question.getQuestionBody().getUuidImages())) {
                    for (CMDSImageDetails imageDetails : question.getQuestionBody().getUuidImages()) {
//                        if(Indexable.YES.equals(question.getIndexable())){
//                            imageDetails.setUploadTarget(UploadTarget.QUESTIONSETS);
//                        }
                        String publicUrl = amazonS3Manager.getImageUrl(imageDetails);
                        imageDetails.setPublicUrl(publicUrl);
                        if (!StringUtils.isEmpty(question.getQuestionBody().getNewText())
                                && question.getQuestionBody().getNewText().contains(imageDetails.getFileName())) {
                            question.getQuestionBody().setNewText(question.getQuestionBody().getNewText()
                                    .replace(imageDetails.getFileName(), publicUrl));
                        }
                    }
                }

                if (question.getSolutionInfo() != null && !CollectionUtils.isEmpty((question.getSolutionInfo().getSolutions()))) {
                    for (SolutionFormat solutionFormat : question.getSolutionInfo().getSolutions()) {
                        if (!StringUtils.isEmpty(solutionFormat.getNewText())
                                && !CollectionUtils.isEmpty(solutionFormat.getUuidImages())) {
                            for (CMDSImageDetails imageDetails : solutionFormat.getUuidImages()) {
//                                if(Indexable.YES.equals(question.getIndexable())){
//                                    imageDetails.setUploadTarget(UploadTarget.QUESTIONSETS);
//                                }
                                if (solutionFormat.getNewText().contains(imageDetails.getFileName())) {
                                    String publicUrl = amazonS3Manager.getImageUrl(imageDetails);
                                    solutionFormat.setNewText(solutionFormat.getNewText().replace(imageDetails.getFileName(),
                                            publicUrl));
                                }
                            }
                        }
                    }
                }

                if (question.getSolutionInfo() != null && question.getSolutionInfo().getOptionBody() != null) {
                    if (!CollectionUtils.isEmpty(question.getSolutionInfo().getOptionBody().newOptions)
                            && !CollectionUtils.isEmpty(question.getSolutionInfo().getOptionBody().uuidImages)) {
                        for (CMDSImageDetails imageDetails : question.getSolutionInfo().getOptionBody().uuidImages) {
                            for (int i = 0; i < question.getSolutionInfo().getOptionBody().newOptions.size(); i++) {
                                String optionText = question.getSolutionInfo().getOptionBody().newOptions.get(i);
                                if (StringUtils.isNotEmpty(optionText) && optionText.contains(imageDetails.getFileName())) {
                                    String publicUrl = amazonS3Manager.getImageUrl(imageDetails);
                                    question.getSolutionInfo().getOptionBody().newOptions.set(i, optionText.replace(imageDetails.getFileName(),
                                            publicUrl));
                                }
                            }
                        }
                    }
                }

            }
        }
    }

    public void _populateAWSUrls(RichTextFormat richContent) {
        if (richContent != null && !CollectionUtils.isEmpty(richContent.getUuidImages())) {
            for (CMDSImageDetails imageDetails : richContent.getUuidImages()) {
                String publicUrl = amazonS3Manager.getImageUrl(imageDetails);
                imageDetails.setPublicUrl(publicUrl);
                if (!StringUtils.isEmpty(richContent.getNewText())
                        && richContent.getNewText().contains(imageDetails.getFileName())) {
                    richContent.setNewText(richContent.getNewText()
                            .replace(imageDetails.getFileName(), publicUrl));
                }
            }
        }
    }

    private String getListItemText(String numLevelText,
            int currentCount, String listItemFormat, int currentgetNumIlvl) {
        String currentCountStr = currentCount + "";
        if (StringUtils.isEmpty(listItemFormat)) {
            return currentCountStr;
        }
        logger.info("getListItemText numLevelText " + numLevelText
                + ", currentCount " + currentCount
                + ", listItemFormat " + listItemFormat
                + ", currentgetNumIlvl " + currentgetNumIlvl);
        String[] alphabets = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j",
            "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w",
            "x", "y", "z"};
        String[] romans = {"i", "ii", "iii", "iv", "v", "vi", "vii", "viii", "ix", "x",
            "xi", "xii", "xiii", "xiv", "xv", "xvi", "xvii", "xviii", "xix", "xx"};
        //numLevelText eg (%1),%1. etc
        String finalText;
        switch (listItemFormat) {
            case "lowerletter":
                finalText = numLevelText.replace(("%" + (1 + currentgetNumIlvl)), alphabets[currentCount - 1]);
                break;
            case "upperletter":
                finalText = numLevelText.replace(("%" + (1 + currentgetNumIlvl)), alphabets[currentCount - 1].toUpperCase());
                break;
            case "decimal":
                finalText = numLevelText.replace(("%" + (1 + currentgetNumIlvl)), currentCountStr);
                break;
            case "bullet":
                finalText = "\u2022";
                break;
            case "upperroman":
                finalText = numLevelText.replace(("%" + (1 + currentgetNumIlvl)), romans[currentCount - 1].toUpperCase());
                break;
            case "lowerroman":
                finalText = numLevelText.replace(("%" + (1 + currentgetNumIlvl)), romans[currentCount - 1]);
                break;
            default:
                finalText = currentCountStr + ".";
                break;
        }
        logger.info("getListItemText exit " + finalText);
        return finalText;
    }

    public static void main(String[] args) throws IOException {
        int p = 1;
        System.err.println(">>> " + p);
//        XWPFDocument doc = new XWPFDocument(new FileInputStream(new File("/Users/ajith/Downloads/test.docx")));
//        int i = 0;
//        Iterator<IBodyElement> iter = doc.getBodyElementsIterator();
//        while (iter.hasNext()) {
//            i++;
//            IBodyElement elem = iter.next();
//            if (elem instanceof XWPFParagraph) {
//                XWPFParagraph para = (XWPFParagraph) elem;
//                System.err.println("paraNo : " + i + " and para text : " + para.getParagraphText()
//                        + " ,  IndentationFirstLine: " + para.getIndentationFirstLine());
//            } else if (elem instanceof XWPFTable) {
//                List<XWPFTableRow> rows = ((XWPFTable) elem).getRows();
//                for (XWPFTableRow row : rows) {
//                    List<XWPFTableCell> cells = row.getTableCells();
//                    for (XWPFTableCell cell : cells) {
//                        for (XWPFParagraph para : cell.getParagraphs()) {
//                            System.err.println("table paraNo : " + i + " and para text : " + para.getParagraphText()
//                                    + " ,  IndentationFirstLine: " + para.getIndentationFirstLine());
//                        }
//                    }
//                }
//            }
//        }
//        System.out.print(">>>>>done");
    }

}
