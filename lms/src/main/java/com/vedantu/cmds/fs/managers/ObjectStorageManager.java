package com.vedantu.cmds.fs.managers;

import java.io.IOException;

import com.vedantu.cmds.fs.objectstorage.Container;
import com.vedantu.cmds.fs.objectstorage.ObjectFile;
import com.vedantu.cmds.fs.objectstorage.ObjectFileEx;
import com.vedantu.util.ConfigUtils;

public class ObjectStorageManager {

	private static ObjectStorageManager instance = null;
	public String username = null;
	public String password = null;
	public String baseUrl = null;

	private ObjectStorageManager() {
		username = ConfigUtils.INSTANCE.getStringValue("objectstorage.username");
		password = ConfigUtils.INSTANCE.getStringValue("objectstorage.password");
		baseUrl = ConfigUtils.INSTANCE.getStringValue("objectstorage.baseurl");
	}

	public static ObjectStorageManager get() {

		if (instance == null) {
			synchronized (ObjectStorageManager.class) {
				if (instance == null) {
					instance = new ObjectStorageManager();
				}

			}
		}
		return instance;
	}

	public String getName() {

		return username;
	}

	public String getPassword() {

		return password;
	}

	public String getBaseUrl() {

		return baseUrl;
	}

	public ObjectFile getObjectFile(String containerName, String fileName) throws IOException {

		return new ObjectFile(fileName, containerName, baseUrl, username, password, true);
	}

	public Container getContainer(String containerName) throws IOException {

		return new Container(containerName, baseUrl, username, password, true);

	}

	public ObjectFileEx getObjectFileExtended(String containerName, String fileName) throws IOException {

		return new ObjectFileEx(fileName, containerName, baseUrl, username, password, true);
	}

}
