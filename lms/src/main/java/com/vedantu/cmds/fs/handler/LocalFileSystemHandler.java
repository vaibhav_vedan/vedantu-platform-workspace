package com.vedantu.cmds.fs.handler;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.input.BoundedInputStream;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.cmds.dao.FileMetaInfoDAO;
import com.vedantu.cmds.entities.AbstractEntityFileStorage;
import com.vedantu.cmds.entities.FileMetaInfo;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.enums.StorageIdentification;
import com.vedantu.cmds.exceptions.FileStoreException;
import com.vedantu.cmds.pojo.FileData;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;

public class LocalFileSystemHandler implements IFileSystemHandler {

	@Autowired
	private FileMetaInfoDAO fileMetaInfoDAO;

	public static final String PATH_SEPARATOR = "/";
	public static final String FILE_EXTENSION_DOT = ".";

	String baseDirectory = null;
	public boolean metadataNeeded = false;

	public LocalFileSystemHandler() {

		this(true);
	}

	public LocalFileSystemHandler(boolean metadataNeeded) {
		this(metadataNeeded, ConfigUtils.INSTANCE.getStringValue("fs.local.basedir"));
	}

	public LocalFileSystemHandler(boolean metadataNeeded, String directoryLocation) {
		this.baseDirectory = directoryLocation;
		File storeDirectory = new File(baseDirectory);
		if (!storeDirectory.exists()) {
			storeDirectory.mkdir();
		}
		this.metadataNeeded = metadataNeeded;
	}

	public String getDirectory() {

		return baseDirectory;
	}

	public String getFilePath(String directoryName, String fileName) {

		if (directoryName != null) {
			File directory = new File(baseDirectory + PATH_SEPARATOR + directoryName);
			if (!directory.exists()) {
				directory.mkdir();
			}
			return baseDirectory + PATH_SEPARATOR + directoryName + PATH_SEPARATOR + fileName;
		}
		return baseDirectory + PATH_SEPARATOR + fileName;
	}

	/**
	 * gives back new file with random id
	 * 
	 * @param directoryName
	 * @param fileExtension
	 * @return
	 */
	public File getNewFile(String directoryName, String fileExtension) {

		return new File(getFilePath(directoryName, UUID.randomUUID().toString() + FILE_EXTENSION_DOT + fileExtension));

	}

	/**
	 * Takes file name w/o extension
	 * 
	 * @param directoryName
	 * @param fileName
	 * @param fileExtensionWithoutDot
	 * @return
	 */
	public File getFileWithSpecifiedName(String directoryName, String fileName, String fileExtensionWithoutDot) {

		return new File(getFilePath(directoryName, fileName + FILE_EXTENSION_DOT + fileExtensionWithoutDot));
	}

	@Override
	public boolean createParent(String dirPath) throws VException {

		if (baseDirectory != null) {
			File createDirectory = new File(baseDirectory + File.separator + dirPath);
			try {
				FileUtils.forceMkdir(createDirectory);
			} catch (IOException e) {
				throw new FileStoreException("Could not create directory " + dirPath, e);
			}
		}
		return true;
	}

	@Override
	public boolean removeParent(String dirPath) {

		if (baseDirectory != null) {
			File createDirectory = new File(baseDirectory + File.separator + dirPath);

			return FileUtils.deleteQuietly(createDirectory);

		}
		return false;
	}

	@Override
	public boolean store(File localFile, String destDir, String destFileName, Map<String, String> tags)
			throws VException {

		FileMetaInfo fileInfo = new FileMetaInfo(destFileName);
		fileInfo.add(tags);
		fileInfo.add("name", destFileName);
		if (this.metadataNeeded) {
			fileMetaInfoDAO.update(fileInfo);
		}
		File destDirectory = new File(baseDirectory + File.separator + destDir);
		try {
			FileUtils.forceMkdir(destDirectory);
		} catch (IOException e) {
			throw new FileStoreException("Could not create directory " + destDir, e);
		}

		File destFile = new File(destDirectory, destFileName);
		try {
			fileInfo.setSize(localFile.length());
			if (localFile.isDirectory()) {
				FileUtils.copyDirectory(localFile, destFile);
			} else {
				FileUtils.deleteQuietly(destFile);
				FileUtils.copyFile(localFile, destFile);
			}
		} catch (IOException e) {
			throw new FileStoreException(
					"Could not copy file " + localFile.getAbsolutePath() + " to directory " + destFileName, e);
		}

		return true;
	}

	@Override
	public FileData get(String sourceDir, String srcFileName) throws VException {

		// FileMetaInfo result =
		// FileMetaInfoDAO.INSTANCE.findByFileId(srcFileName);
		// if (result == null && metadataNeeded) {
		// LOGGER.debug("File not found for name " + srcFileName);
		// throw new VException("File metadata not found", new
		// FileNotFoundException());
		// }
		File file = new File(baseDirectory + File.separator + sourceDir + File.separator + srcFileName);

		InputStream instream = null;
		FileData fileData = null;
		try {
			instream = new FileInputStream(file);

			// LOGGER.debug("File found for name " + file.getAbsolutePath() +
			// result.getTags());
			// fileData = new FileData( fileInfo.getDbObject().toMap(), instream
			// );
			fileData = new FileData(null, instream);
			long fileSize = file.length();
			if (fileSize == -1) {
				try {
					fileSize = instream.available();
					// result.setSize(fileSize);
					// if (metadataNeeded) {
					// FileMetaInfoDAO.INSTANCE.save(result);
					// }
				} catch (IOException e) {
					fileSize = AbstractEntityFileStorage.MAXIMUM_FILE_SIZE_ALLOWED;
				}
			}
			fileData.setFileSize(fileSize);
			fileData.setContentLength(fileSize);
			fileData.setTotalContentLength(fileSize);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			IOUtils.closeQuietly(instream);
			throw new VException(ErrorCode.FILE_NOT_FOUND,
					"Could not read file " + srcFileName + " from " + sourceDir + e.getMessage());

		}
		return fileData;
	}

	@Override
	public boolean delete(String sourceDir, String srcFileName) throws VException {

		FileMetaInfo result = fileMetaInfoDAO.findByFileId(srcFileName);
		if (result == null && metadataNeeded) {
			throw new VException(ErrorCode.FILE_NOT_FOUND,
					"File metadata not found for : " + sourceDir + " srcFileName : " + srcFileName);
		}

		return FileUtils
				.deleteQuietly(new File(baseDirectory + File.separator + sourceDir + File.separator + srcFileName));
	}

	@Override
	public boolean copy(String srcDir, String destDir, String srcFileName, String destFileName) throws VException {

		File srcFile = new File(baseDirectory + File.separator + srcDir + File.separator + srcFileName);
		File destFile = new File(baseDirectory + File.separator + destDir + File.separator + destFileName);

		FileMetaInfo srcfileInfo = fileMetaInfoDAO.findByFileId(srcFileName);
		if (srcfileInfo == null && metadataNeeded) {
			throw new VException(ErrorCode.FILE_NOT_FOUND,
					"File metadata not found for srcFile : " + srcFileName + " destFile : " + destFileName);
		}

		if (metadataNeeded) {
			FileMetaInfo destFileInfo = new FileMetaInfo(destFileName);
			destFileInfo.setTags(srcfileInfo.getTags());
			fileMetaInfoDAO.update(destFileInfo);
		}

		try {
			FileUtils.copyFile(srcFile, destFile, true);
		} catch (IOException e) {
			throw new VException(ErrorCode.SERVICE_ERROR,
					"Could not copy file " + srcFile.getPath() + " to  " + destFile.getPath() + e.getMessage());
		}
		return true;
	}

	@Override
	public boolean move(String srcDir, String destDir, String srcFileName, String destFileName) throws VException {

		File srcFile = new File(baseDirectory + File.separator + srcDir + File.separator + srcFileName);
		File destFile = new File(baseDirectory + File.separator + destDir + File.separator + destFileName);
		FileMetaInfo srcfileInfo = fileMetaInfoDAO.findByFileId(srcFileName);
		if (srcfileInfo == null && metadataNeeded) {
			throw new VException(ErrorCode.FILE_NOT_FOUND,
					"File metadata not found " + srcFileName + " dest : " + destFileName);
		}

		if (metadataNeeded) {
			FileMetaInfo destFileInfo = new FileMetaInfo(destFileName);
			destFileInfo.setTags(srcfileInfo.getTags());
			fileMetaInfoDAO.update(destFileInfo);
		}
		try {
			FileUtils.moveFile(srcFile, destFile);
		} catch (IOException e) {
			// ODO Auto-generated catch block
			throw new VException(ErrorCode.SERVICE_ERROR,
					"Could not copy file " + srcFile.getPath() + " to  " + destFile.getPath() + e.getMessage());
		}
		if (metadataNeeded) {
			fileMetaInfoDAO.delete(srcfileInfo);
		}
		return true;
	}

	@Override
	public String getParentName(CMDSEntityType entityType, String fwkId) {
		return fwkId.toLowerCase() + com.vedantu.cmds.utils.FileUtils.SEPARATOR_UNDERSCORE
				+ entityType._getStorageId().toLowerCase();
	}

	public static void main(String args[]) {

		try {

			LocalFileSystemHandler handler = new LocalFileSystemHandler(false, FileUtils.getTempDirectoryPath());
			System.out.println(FileUtils.getTempDirectoryPath());
			handler.createParent("documents");
			Map<String, String> tags = new HashMap<String, String>();
			tags.put("id", "myphoto");
			tags.put("date", "jan25");
			handler.store(new File("/home/vikram/Documents/1.jpg"), "documents", "abcd.jpeg", tags);
			FileData data = handler.get("documents", "abcd.jpeg");
			System.out.println(data.getFileMetaInfo());
			handler.delete("documents", "abcd.jpeg");
		} catch (Exception exp) {
			exp.printStackTrace();
		}

	}

	@Override
	public boolean exists(String dir, String fileName) throws VException {

		File testFile = new File(baseDirectory + File.separator + dir + File.separator + fileName);
		return testFile.exists();

	}

	@Override
	public StorageIdentification getIdentification() throws VException {

		return StorageIdentification.LOCAL;
	}

	@Override
	public FileData get(String sourceDir, String srcFileName, long index, long size) throws VException {

		// TODO Auto-generated method stub

		FileMetaInfo fileInfo = fileMetaInfoDAO.findByFileId(srcFileName);

		if (fileInfo == null) {
			throw new FileStoreException("File metadata not found", new FileNotFoundException());
		}

		File file = new File(baseDirectory + File.separator + sourceDir + File.separator + srcFileName);

		InputStream instream = null;

		FileData fileData = null;
		long fileSize = metadataNeeded ? fileInfo.getSize() : 0;
		try {
			// instream = new FileInputStream(file);
			instream = new FileInputStream(file);

			if (index != 0) {
				instream.skip(index);
			}

			if (fileSize == -1) {
				try {
					fileSize = instream.available();
					fileInfo.setSize(fileSize);
					if (metadataNeeded) {
						fileMetaInfoDAO.update(fileInfo);
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					fileSize = AbstractEntityFileStorage.MAXIMUM_FILE_SIZE_ALLOWED;
				}
			}

			size = instream.available() < size ? instream.available() : size;
			instream = new BoundedInputStream(instream, size);
			fileData = new FileData(fileInfo.getFileTags(), instream);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new FileStoreException("Could not read file " + srcFileName + " from " + sourceDir, e);
		}


		fileData.setTotalContentLength(fileSize);
		if (size == -1) {
			fileData.setContentLength(fileSize);
		} else {

			fileData.setContentLength(size);
		}

		return fileData;

	}
}
