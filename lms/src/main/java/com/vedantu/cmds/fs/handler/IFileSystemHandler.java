package com.vedantu.cmds.fs.handler;

import java.io.File;
import java.util.Map;

import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.enums.StorageIdentification;
import com.vedantu.cmds.pojo.FileData;
import com.vedantu.exception.VException;

public interface IFileSystemHandler {

	public boolean createParent(String dir) throws VException;

	public boolean store(File file, String destDir, String destFileName, Map<String, String> tags) throws VException;

	public FileData get(String sourceDir, String srcFileName) throws VException;

	public boolean delete(String sourceDir, String srcFileName) throws VException;

	public boolean copy(String sourceDir, String destDir, String srcFileName, String destFileName) throws VException;

	public boolean move(String sourceDir, String destDir, String srcFileName, String destFileName) throws VException;

	public FileData get(String sourceDir, String srcFileName, long index, long size) throws VException;

	public String getParentName(CMDSEntityType entityType, String fwkId);
	//
	// public SignUploadFileRes signContentUpload(CMDSEntityType entityType,
	// String bucketName, String fileName,
	// String contentType) throws VException;

	public boolean exists(String sourceDir, String srcFileName) throws VException;

	public StorageIdentification getIdentification() throws VException;

	boolean removeParent(String dirPath) throws VException;
}
