package com.vedantu.cmds.utils;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.StringUtils;

public class FileUtils {

	public final static String HTML_EXTENSION = ".html";
	public final static String HTML_EXTENSION_WITHOUT_DOT = "html";
	public final static String HTM_EXTENSION = ".htm";
	public final static String HTM_EXTENSION_WITHOUT_DOT = "htm";

	public static final String PDF_EXTENSTION_WITHOUT_DOT = "pdf";
	public static final String PDF_EXTENSTION = ".pdf";

	public static final String JPG_EXTENTION = ".jpg";
	public static final String JPG_EXTENTION_WITHOUT_DOT = "jpg";

	public static final String ZIP_EXTENTION = ".zip";
	public static final String ZIP_EXTENTION_WITHOUT_DOT = "zip";

	public static final String WEBM_EXTENTION = ".webm";
	public static final String WEBM_EXTENTION_WITHOUT_DOT = "webm";

	public static final String SEPARATOR_DOT = ".";
	public static final String SEPARATOR_FWDSLASH = "/";
	public static final String SEPARATOR_UNDERSCORE = "_";
	public static final String SEPARATOR_HYPHEN = "-";

	public static String getExtension(File file) {

		return getExtension(file.getName());
	}

	public static String getExtension(String fileName) {

		return fileName.toLowerCase().substring(fileName.lastIndexOf("."));
	}

	public static String getExtensionWithoutDOT(String fileName) {

		return fileName.toLowerCase().substring(fileName.lastIndexOf(".") + 1);
	}

	public static String getFileName(File file) {

		return getFileName(file.getName());
	}

	public static long getFileSize(File file) {
		if (!file.exists()) {
			return 0;
		}
		return org.apache.commons.io.FileUtils.sizeOf(file);
	}

	public static String getFileName(String fileName) {

		if (fileName.lastIndexOf(".") != -1) {
			return fileName.substring(0);
		}
		return fileName.substring(0, fileName.lastIndexOf("."));
	}

	public static List<String> getTags(String fileName) {

		String[] fileNameParts = StringUtils.split(fileName.substring(0, fileName.lastIndexOf('.')), '.');

		return Arrays.asList(fileNameParts);
	}

	public static void deleteFile(String fileName, File file) {

		if (null == file) {
			return;
		}
		if (!file.exists()) {
			return;
		}
		if (!file.isFile()) {
			return;
		}
		boolean deleted = file.delete();
	}

	public static boolean moveFile(File srcFile, File destFile) {

		try {
			org.apache.commons.io.FileUtils.moveFile(srcFile, destFile);
		} catch (IOException e) {
			return false;
		}
		return true;

	}

}
