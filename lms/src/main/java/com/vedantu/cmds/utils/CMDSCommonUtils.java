package com.vedantu.cmds.utils;

import com.vedantu.cmds.constants.QuestionSetFileConstants;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.cmds.pojo.TestMetadata;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.StringUtils;

public class CMDSCommonUtils {

    //reference:https://txt2re.com/index-java.php3?s=Question%206:&-1&-17
    final static String re1 = "(Question)";	// Word 1
    final static String re2 = ".*?";	// Non-greedy match on filler
    final static String re3 = "(:)";	// Any Single Character 1

    final static Pattern questionStartsWithPattern = Pattern.compile(re1 + re2 + re3, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
    final static String soln = "(Solution)";	// Word 1

    final static Pattern solutionStartsWithPattern = Pattern.compile(soln + re2 + re3, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

    public static List<String> getQuestionIds(List<CMDSTestQuestion> tests) {
        List<String> ids = new ArrayList<String>();
        if (!CollectionUtils.isEmpty(tests)) {
            for (CMDSTestQuestion test : tests) {
                ids.add(test.getQuestionId());
            }
        }

        return ids;
    }

    public static Map<String, CMDSQuestion> getQuestionIdMap(List<CMDSQuestion> questions) {
        Map<String, CMDSQuestion> map = new HashMap<>();
        if (!CollectionUtils.isEmpty(questions)) {
            for (CMDSQuestion cmdsQuestion : questions) {
                map.put(cmdsQuestion.getId(), cmdsQuestion);
            }
        }

        return map;
    }

//	public static void populateQuestionIndex(CMDSTest cmdsTest) {
//		List<CMDSTestQuestion> questions = cmdsTest.getQuestions();
//		if (!CollectionUtils.isEmpty(questions)) {
//			int index = 1;
//			for (CMDSTestQuestion cmdsTestQuestion : questions) {
//				cmdsTestQuestion.setQuestionIndex(index);
//				index++;
//			}
//		}
//	}
    public static void populateQuestionIndex(List<CMDSTestQuestion> questions) {
        if (!CollectionUtils.isEmpty(questions)) {
            int index = 1;
            for (CMDSTestQuestion cmdsTestQuestion : questions) {
                cmdsTestQuestion.setQuestionIndex(index);
                index++;
            }
        }
    }

    public static List<QuestionAnalytics> createTestResultEntries(CMDSTest cmdsTest) {
        List<QuestionAnalytics> results = new ArrayList<>();
        for (TestMetadata testMetadata : cmdsTest.getMetadata()) {
            List<CMDSTestQuestion> questions = testMetadata.getQuestions();
            if (!CollectionUtils.isEmpty(questions)) {
                for (CMDSTestQuestion cmdsTestQuestion : questions) {
                    results.add(new QuestionAnalytics(cmdsTestQuestion));
                }
            }
        }

        return results;
    }

    public static boolean doesStartsWith(String txt, String startsWithPrefix) {
        /*
            String s = "Question 9: akjfhd jf";
            String s = "Question9: akjfhd jf";
            String s = "Question: akjfhd jf";
            all above will return true
            String s="skj  Question: sdsv";
            this will return false
         */

        if (StringUtils.isEmpty(txt) || StringUtils.isEmpty(startsWithPrefix)) {
            return false;
        }
        txt = txt.trim().toLowerCase();
        startsWithPrefix = startsWithPrefix.toLowerCase();
        Matcher m = null;
        switch (startsWithPrefix) {
            case QuestionSetFileConstants.PREFIX_QUESTION:
                m = questionStartsWithPattern.matcher(txt);
                break;
            case QuestionSetFileConstants.PREFIX_SOLUTION:
                m = solutionStartsWithPattern.matcher(txt);
                break;
            default:
                break;
        }
        if (m != null) {
            if (m.find()) {
                String matchedstr = m.group(1) + m.group(2);
                return matchedstr.equals(startsWithPrefix) && txt.startsWith(startsWithPrefix.substring(0, startsWithPrefix.length() - 1));
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public static String makeSlug(String tag) {
        if (StringUtils.isNotEmpty(tag)) {
            tag = tag.trim();
            tag = tag.toLowerCase();
            tag = tag.replaceAll(" ", "-");
        }
        return tag;
    }

    public static Set<String> makeSlugs(Set<String> tags) {
        Set<String> slugs = new HashSet<>();
        if (ArrayUtils.isNotEmpty(tags)) {
            for (String tag : tags) {
                String slug = makeSlug(tag);
                if (StringUtils.isNotEmpty(slug)) {
                    slugs.add(slug);
                }
            }
        }
        return slugs;
    }

    public static String _addParentUlTags(String text, Map<String, Integer> listCounterMap) {
        System.out.println("_addParentUlTags>> " + text);
        System.out.println("_addParentUlTags>> " + listCounterMap);
        Iterator<Map.Entry<String, Integer>> itr = listCounterMap.entrySet().iterator();
        while (itr.hasNext()) {
            Map.Entry<String, Integer> entry = itr.next();
            String parentUlKey = entry.getKey();
            Integer liCount = entry.getValue();
            String liTagStart = "<li class='listitem-1 parent-ul-" + parentUlKey + " listitem'>";
            String lastliTagStart = "<li class='listitem-" + liCount + " parent-ul-" + parentUlKey + " listitem'>";
            if (text.contains(liTagStart)) {
                text = text.replace(liTagStart, "<ul class='uploaded-question-list'>" + liTagStart);
                text = text.replace("</li><br><li", "</li><li");
                int indexOfLastliTagStart = text.indexOf(lastliTagStart);
                if (indexOfLastliTagStart > -1) {
                    int indexOfLastliTagEnd = text.indexOf("</li>", indexOfLastliTagStart);
                    if (indexOfLastliTagEnd > -1) {
                        text = text.substring(0, indexOfLastliTagEnd)
                                + "</li></ul>" + text.substring(indexOfLastliTagEnd + 5);
                    }
                }
                itr.remove();
            }
        }
        System.out.println("_addParentUlTags exit >> " + text);
        return text;
    }

    public static String handleStartingSpacesAndTabs(String s) {
        String finalString = "";
        if (StringUtils.isNotEmpty(s)) {
            String sTrimmed = s.trim();
            while (s.contains("\t") && s.indexOf("\t") < s.indexOf(sTrimmed)) {
                System.err.println(">> replacing tab");
                s = s.replaceFirst("\t", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            }
            while (s.contains(" ") && s.indexOf(" ") < s.indexOf(sTrimmed)) {
                System.err.println(">> replacing space");
                s = s.replaceFirst(" ", "&nbsp;");
            }
            finalString = s;
        }
        return finalString;
    }

    public static void main(String[] args) {
//        String s = "Question 9: akjfhd jf";
//        System.err.println(">> "
//                + CMDSCommonUtils.doesStartsWith(s, QuestionSetFileConstants.PREFIX_QUESTION));
        String text = "sss<li class='listitem-1 parent-ul-1-0 listitem'>sdcdcdf</li><li class='listitem-2 parent-ul-1-0 listitem'>2mdit</li>";
        Map<String, Integer> _m = new HashMap<>();
        _m.put("1-0", 2);
        String s = _addParentUlTags(text, _m);
        System.err.println(">>> " + s);
    }
}
