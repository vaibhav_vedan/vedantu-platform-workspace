package com.vedantu.cmds.utils;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.dao.challenges.ChallengeAttemptDAO;
import org.apache.commons.lang.time.DateUtils;

import com.vedantu.cmds.dao.challenges.MultiplierPowerDAO;
import com.vedantu.cmds.dao.sql.ChallengeAttemptRDDAO;
import com.vedantu.cmds.entities.challenges.ChallengeAttemptRD;
import com.vedantu.cmds.entities.challenges.MultiplierPower;
import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.cmds.enums.challenges.MultiplierPowerValidityType;
import com.vedantu.cmds.response.challenges.ChallengeLeaderBoardInfo;
import com.vedantu.cmds.response.challenges.ChallengeLeaderBoardRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChallengeUtilCommon {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ChallengeUtilCommon.class);

    @Autowired
    private MultiplierPowerDAO multiplierPowerDAO;

    @Autowired
    private ChallengeAttemptRDDAO challengeAttemptDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
private DozerBeanMapper mapper;

    public static final int DEFAULT_BATCH_SIZE = 100;

//    private int getUserGlobalChallengeRank(Long userId,
//            UserChallengeStats userChallengeStats, RankType rankType) {
//
//        Query query = new Query();
//        query.addCriteria(Criteria.where(ConstantsGlobal.RANK_IDENTIFIER).is(rankType.identifier()));
//        query.addCriteria(Criteria.where(ConstantsGlobal.POINTS).gt(userChallengeStats.getPoints()));
//
//        // this query count total no of users who has point greater than user
//        long previousBucketsTotalSize = challengeUserInfoDAO.queryCount(query, UserChallengeStats.class);
//        query.addCriteria(Criteria.where(ConstantsGlobal.POINTS).is(userChallengeStats.getPoints()));
//        query.with(new Sort(Sort.Direction.DESC, ConstantsGlobal.STRIKE_RATE));
//        query.fields().include(ConstantsGlobal.USER_ID);
//        //TODO max records is set to 1000, this can create problem even when we are sending limit0
//        challengeUserInfoDAO.setFetchParameters(query, 0, 1000);
//        List<UserChallengeStats> results = challengeUserInfoDAO.runQuery(query, UserChallengeStats.class);
//        int globalRank = 0;
//
//        if (ArrayUtils.isNotEmpty(results)) {
//            for (UserChallengeStats challengeUserInfo : results) {
//                globalRank++;
//                if (challengeUserInfo.getUserId() != null
//                        && userId.equals(challengeUserInfo.getUserId())) {
//                    break;
//                }
//            }
//        }
//        globalRank += previousBucketsTotalSize;
//        logger.info("challenge global rank[type:" + rankType + "] of user["
//                + userId + "] : " + globalRank);
//        return globalRank;
//    }
//    public static ChallengeRankBucket getChallengeBucket(int bucketNo) {
//        DBObject result = MongoDBCollection.findOne(new BasicDBObject(
//                ConstantsGlobal.BUCKET_NO, bucketNo), MongoDBCollection
//                .getFieldsDBObject(ConstantsGlobal._ID, MongoDBCollection.INCLUDE_FIELD),
//                ChallengeRankBucket.class);
//        return ObjectMapperUtil.convertToMongoModel(result, ChallengeRankBucket.class);
//    }
//    public static long getChallengePreviousBucketsTotalSize(int bucketNo) {
//        DBCursor cur = MongoDBCollection.getMongoDBResult(new BasicDBObject(
//                ConstantsGlobal.BUCKET_NO, new BasicDBObject("$lt", bucketNo)),
//                MongoDBCollection.getFieldsDBObject(ConstantsGlobal.SIZE,
//                        MongoDBCollection.INCLUDE_FIELD), ChallengeRankBucket.class);
//        long previousBucketsTotalSize = 0;
//        if (cur != null && cur.size() > 0) {
//            while (cur.hasNext()) {
//                ChallengeRankBucket crBucket = ObjectMapperUtil.convertToMongoModel(
//                        cur.next(), ChallengeRankBucket.class);
//                previousBucketsTotalSize += crBucket.size;
//            }
//        }
//        return previousBucketsTotalSize;
//    }
    public MultiplierPower getActiveMultiplierPower(Long userId) {
        MultiplierPower activeMultiplierPower = null;
        for (MultiplierPowerType powerType : MultiplierPowerType.values()) {
            MultiplierPower multiplierPower = multiplierPowerDAO.getByUserIdAndPowerType(userId, powerType);
            activeMultiplierPower = getSuperMultiplierPower(
                    activeMultiplierPower, multiplierPower);
        }
        return activeMultiplierPower;
    }

    private static MultiplierPower getSuperMultiplierPower(MultiplierPower activePower,
            MultiplierPower comparablePower) {
        // comparablePower can not be null
        if (activePower == null) {
            return isActivePower(comparablePower) ? comparablePower : null;
        }
        return isActivePower(comparablePower)
                && comparablePower.type.getMultiplier() > activePower.type
                .getMultiplier() ? comparablePower : activePower;
    }

    private static boolean isActivePower(MultiplierPower multiplierPower) {
        boolean active = false;
        if (multiplierPower == null) {
            return active;
        }
        if (multiplierPower.validityType == MultiplierPowerValidityType.CHALLENGE) {
            active = multiplierPower.useCount < multiplierPower.validFor;
        } else if (multiplierPower.validityType == MultiplierPowerValidityType.DAYS) {
            long activeTime = multiplierPower.validFor * DateUtils.MILLIS_PER_DAY;
            active = System.currentTimeMillis() < (multiplierPower.getCreationTime() + activeTime);
        }
        return active;
    }

    public ChallengeLeaderBoardRes getChallengeLeaderBoard(Long userId, String challengeId,
            Integer start, Integer size, boolean addMyRank) {
        if (start == null) {
            start = 0;
        }
        if (size == null) {
            size = 20;
        }
        List<ChallengeAttemptRD> attempts = challengeAttemptDAO.getChallengeLeaderBoard(challengeId, start, size);
        List<ChallengeLeaderBoardInfo> leaders = new ArrayList<>();
        List<Long> userIds = new ArrayList<>();
        boolean partOfLeaderBoard = false;
        if (ArrayUtils.isNotEmpty(attempts)) {
            int count = 1;
            for (ChallengeAttemptRD attempt : attempts) {
                userIds.add(attempt.getUserId());
                if (attempt.getUserId().equals(userId)) {
                    partOfLeaderBoard = true;
                }
                ChallengeLeaderBoardInfo leader = mapper.map(attempt, ChallengeLeaderBoardInfo.class);
                leader.setRank(count + start);
                count++;
                leaders.add(leader);
            }
        }

        if (addMyRank && !partOfLeaderBoard) {
            ChallengeAttemptRD myAttempt = challengeAttemptDAO.getByUserId(userId, challengeId);
            logger.info("myAttempt " + myAttempt);
            if (myAttempt != null && myAttempt.processed && myAttempt.success) {
                Long myRank = challengeAttemptDAO.getMyRank(myAttempt);
                if (myRank != null) {
                    userIds.add(userId);
                    ChallengeLeaderBoardInfo leader = mapper.map(myAttempt, ChallengeLeaderBoardInfo.class);
                    leader.setRank(myRank.intValue());
                    leaders.add(leader);
                }
            }
        }
        Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        for (ChallengeLeaderBoardInfo leader : leaders) {
            leader.setUser(usersMap.get(leader.getUserId()));
        }
        ChallengeLeaderBoardRes res = new ChallengeLeaderBoardRes();
        res.setList(leaders);
        return res;
    }

//    public ChallengeGlobalLeaderBoardRes getChallengeGlobalLeaderBoard(Long userId, int start,
//            int size, boolean addMyRank, RankType rankType) {
//        Query query = new Query();
//        if (rankType != null) {
//            query.addCriteria(Criteria.where(ConstantsGlobal.RANK_IDENTIFIER).is(rankType.identifier()));
//        }
//        query.addCriteria(Criteria.where(ConstantsGlobal.POINTS).ne(0));
//        challengeUserInfoDAO.setFetchParameters(query, start, size);
//        List<Sort.Order> orderList = new ArrayList<>();
//        orderList.add(new Sort.Order(Sort.Direction.DESC, ConstantsGlobal.POINTS));
//        orderList.add(new Sort.Order(Sort.Direction.DESC, ConstantsGlobal.STRIKE_RATE));
//        query.with(new Sort(orderList));
//
//        List<UserChallengeStats> results = challengeUserInfoDAO.runQuery(query, UserChallengeStats.class);
//
//        List<ChallengeUserInfoRes> leaders = new ArrayList<>();
//        if (ArrayUtils.isNotEmpty(results)) {
//            Set<Long> userIds = new HashSet<>();
//            boolean userAppeared = false;
//            ChallengeUserInfoRes lastUser = null;
//            int i = 0;
//            for (UserChallengeStats userChallengeStats : results) {
//                i++;
//                if (userChallengeStats.getUserId().equals(userId)) {
//                    userAppeared = true;
//                }
//                if (i == size && !userAppeared && addMyRank) {
//                    lastUser = mapper.map(userChallengeStats, ChallengeUserInfoRes.class);
//                    lastUser.setRank(i);
//                    continue;
//                }
//                userIds.add(userChallengeStats.getUserId());
//                ChallengeUserInfoRes obj = mapper.map(userChallengeStats, ChallengeUserInfoRes.class);
//                obj.setRank(i);
//                leaders.add(obj);
//            }
//            if (addMyRank && !userIds.contains(userId)) {
//                Query query2 = new Query();
//                query2.addCriteria(Criteria.where(ConstantsGlobal.USER_ID).is(userId));
//                query2.addCriteria(Criteria.where(ConstantsGlobal.RANK_IDENTIFIER).is(rankType.identifier()));
//                UserChallengeStats userChallengeUserInfo = challengeUserInfoDAO.findOne(query2, UserChallengeStats.class);
//                if (userChallengeUserInfo != null) {
//                    userIds.add(userChallengeUserInfo.getUserId());
//                    lastUser = mapper.map(userChallengeUserInfo, ChallengeUserInfoRes.class);
//                    lastUser.setRank(getUserGlobalChallengeRank(userId, userChallengeUserInfo,
//                            rankType));
//                }
//                if (lastUser != null) {
//                    leaders.add(lastUser);
//                }
//            }
//            Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds);
//            for (ChallengeUserInfoRes leader : leaders) {
//                if (usersMap.containsKey(leader.getUserId())) {
//                    leader.setUser(usersMap.get(leader.getUserId()));
//                }
//            }
//        }
//
//        ChallengeGlobalLeaderBoardRes res = new ChallengeGlobalLeaderBoardRes();
//        res.setList(leaders);
//        return res;
//    }
}
