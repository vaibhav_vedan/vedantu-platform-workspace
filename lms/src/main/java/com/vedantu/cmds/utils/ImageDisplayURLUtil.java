package com.vedantu.cmds.utils;

import java.io.File;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;

import com.vedantu.cmds.entities.storage.FileCategory;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.enums.ImageSize;
import com.vedantu.cmds.enums.MediaType;
import com.vedantu.cmds.factory.EntityStorageFactory;
import com.vedantu.cmds.interfaces.IEntityFileStorage;
import com.vedantu.util.ConfigUtils;

public class ImageDisplayURLUtil {

	public static final String JPG_EXTENTION = ".jpg";
	private static boolean fileSecurityEnabled = true;
	private static String host = null;

	@PostConstruct
	public void init() {
		try {
			// TODO : Add the config params
			host = ConfigUtils.INSTANCE.getStringValue("img.host");
			fileSecurityEnabled = ConfigUtils.INSTANCE.getBooleanValue("environment.file.security.enabled");
		} catch (Exception e) {
			// TODO : Add the config params
		}
	}

	public static String getImgHost() {
		return ConfigUtils.INSTANCE.getStringValue("img.host");
	}

	public static final String DEFAULT_FILE_SERVING_HOST_URL = getImgHost() + "/viewer/view/";

	public static final String DEFAULT_FILE_STREAMING_HOST_URL = getImgHost() + "/viewer/stream/";
	public static final String DEFAULT_FILE_ESTREAMING_HOST_URL = getImgHost() + "/viewer/estream/";

	public static final String DEFAULT_FILE_DOWNLOAD_HOST_URL = getImgHost() + "/viewer/download/";

	public static final String DEFAULT_FILE_SERVING_STATIC_URL = getImgHost() + "/static";

	public static final String DEFAULT_HOST_URL_PROFILE_PIC = getImgHost() + "/profiles/pic/";
	public static final String DEFAULT_HOST_URL_PLAYLIST_IMAGES = getImgHost() + "/playlists/";
	public static final String DEFAULT_HOST_URL_QUESTION_EMBED_IMAGES = getImgHost() + "/questions/";
	public static final String DEFAULT_HOST_URL_QUESTION_EMBED_TEMP_IMAGES = getImgHost() + "/temp/questions/";

	public static final String DEFAULT_HOST_URL_SOLUTION_EMBED_IMAGES = getImgHost() + "/solutions/";
	public static final String DEFAULT_HOST_URL_SOLUTION_EMBED_TEMP_IMAGES = getImgHost() + "/temp/solutions/";

	public static final String DEFAULT_HOST_URL_STATUS_FEED_EMBED_TEMP_IMAGES = getImgHost() + "/temp/statusfeed/";

	public static final String DEFAULT_HOST_URL_UPLOADED_FILE = getImgHost() + "/Diagrams/get/";

	public static String getStatuFeedOrginalImageURL(String uuid) {
		return getEntityImageURL(CMDSEntityType.STATUSFEED, uuid, ImageSize.ORIGINAL);
	}

	public static File getEmbededFileName(String directory, String url) {

		int indexOfEmbededPart = url.indexOf(DEFAULT_HOST_URL_STATUS_FEED_EMBED_TEMP_IMAGES);
		if (indexOfEmbededPart != -1) {
			String fileName = url
					.substring(indexOfEmbededPart + DEFAULT_HOST_URL_STATUS_FEED_EMBED_TEMP_IMAGES.length());
			return new File(directory + "/" + fileName);
		}
		return null;

	}

	public static String getOrganizationThumbnail(String thumbnailId) {

		return getEntityImageURL(CMDSEntityType.ORGANIZATION, thumbnailId, ImageSize.EXTRA_SMALL);

	}

	public static String getUserPicThumbnail(String profilePic) {

		return getEntityImageURL(CMDSEntityType.USER, profilePic, ImageSize.SMALL);
	}

	public static String getEntityThumbnail(CMDSEntityType entityType, String uid) {

		return getEntityImageURL(entityType, uid, ImageSize.SMALL);
	}

	public static String getEntityStaticThumbnail(CMDSEntityType entityType, List<String> suffixComponents) {

		final String fileName = StringUtils.lowerCase(StringUtils.join(suffixComponents, "-") + JPG_EXTENTION);
		return StringUtils
				.join(Arrays.asList(DEFAULT_FILE_SERVING_STATIC_URL, entityType.name().toLowerCase(), fileName), "/");
	}

	public static String getEntityImageURL(CMDSEntityType entityType, String uid) {

		return getEntityImageURL(entityType, uid, ImageSize.ORIGINAL);
	}

	public static String getEntityImageURL(CMDSEntityType entityType, String uid, ImageSize size) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		return DEFAULT_FILE_SERVING_HOST_URL + fileEntityStorage.computeDisplayUrlComponent(uid,
				FileUtils.JPG_EXTENTION_WITHOUT_DOT, MediaType.IMAGE, FileCategory.CONVERTED, size);
	}

	public static String getEntityVideoURL(CMDSEntityType entityType, String uid) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		return DEFAULT_FILE_STREAMING_HOST_URL + fileEntityStorage.computeDisplayUrlComponent(uid,
				FileUtils.WEBM_EXTENTION_WITHOUT_DOT, MediaType.VIDEO, FileCategory.CONVERTED, null);
	}

	public static String getEntityVideoSecureURL(CMDSEntityType entityType, String uid,
			Map<String, String> sessionParamsMap, boolean isWebReq) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);

		String componentUrl = fileEntityStorage.computeDisplayUrlComponent(uid, FileUtils.WEBM_EXTENTION_WITHOUT_DOT,
				MediaType.VIDEO, FileCategory.CONVERTED, null);

		return isWebReq && fileSecurityEnabled
				? (DEFAULT_FILE_ESTREAMING_HOST_URL + getEncryptedEntityUrl(componentUrl, sessionParamsMap))
				: (DEFAULT_FILE_STREAMING_HOST_URL + fileEntityStorage.computeDisplayUrlComponent(uid,
						FileUtils.WEBM_EXTENTION_WITHOUT_DOT, MediaType.VIDEO, FileCategory.CONVERTED, null));
	}

	public static String getEntityVideoURL(CMDSEntityType entityType, String uid, String fileExt,
			FileCategory fileCategory) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);

		return DEFAULT_FILE_STREAMING_HOST_URL
				+ fileEntityStorage.computeDisplayUrlComponent(uid, fileExt, MediaType.VIDEO, fileCategory, null);
	}

	public static String getEntityVideoSecureURL(CMDSEntityType entityType, String uid, String fileExt,
			FileCategory fileCategory, Map<String, String> sessionParamsMap, boolean isWebReq) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		String componentUrl = fileEntityStorage.computeDisplayUrlComponent(uid, fileExt, MediaType.VIDEO, fileCategory,
				null);

		return isWebReq && fileSecurityEnabled
				? (DEFAULT_FILE_ESTREAMING_HOST_URL + getEncryptedEntityUrl(componentUrl, sessionParamsMap))
				: (DEFAULT_FILE_STREAMING_HOST_URL + componentUrl);
	}

	public static String getEntityDocumentURL(CMDSEntityType entityType, String uid) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		return DEFAULT_FILE_STREAMING_HOST_URL + fileEntityStorage.computeDisplayUrlComponent(uid,
				FileUtils.PDF_EXTENSTION_WITHOUT_DOT, MediaType.DOC, FileCategory.CONVERTED, null);
	}

	public static String getEntityDocumentSecureURL(CMDSEntityType entityType, String uid,
			Map<String, String> sessionParamsMap, boolean isWebReq) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);

		String componentUrl = fileEntityStorage.computeDisplayUrlComponent(uid, FileUtils.PDF_EXTENSTION_WITHOUT_DOT,
				MediaType.DOC, FileCategory.CONVERTED, null);

		return isWebReq && fileSecurityEnabled
				? (DEFAULT_FILE_ESTREAMING_HOST_URL + getEncryptedEntityUrl(componentUrl, sessionParamsMap))
				: (DEFAULT_FILE_STREAMING_HOST_URL + fileEntityStorage.computeDisplayUrlComponent(uid,
						FileUtils.PDF_EXTENSTION_WITHOUT_DOT, MediaType.DOC, FileCategory.CONVERTED, null));

	}

	public static String getEntityDocumentURL(CMDSEntityType entityType, String uid, String fileExt,
			FileCategory fileCategory) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		return DEFAULT_FILE_STREAMING_HOST_URL
				+ fileEntityStorage.computeDisplayUrlComponent(uid, fileExt, MediaType.DOC, fileCategory, null);
	}

	public static String getEntityDocumentSecureURL(CMDSEntityType entityType, String uid, String fileExt,
			FileCategory fileCategory, Map<String, String> sessionParamsMap, boolean isWebReq) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);

		String componentUrl = fileEntityStorage.computeDisplayUrlComponent(uid, fileExt, MediaType.DOC, fileCategory,
				null);

		return isWebReq && fileSecurityEnabled
				? (DEFAULT_FILE_ESTREAMING_HOST_URL + getEncryptedEntityUrl(componentUrl, sessionParamsMap))
				: (DEFAULT_FILE_STREAMING_HOST_URL + componentUrl);
	}

	public static String getEntityFileURL(CMDSEntityType entityType, String uid, String fileExt,
			FileCategory fileCategory) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		return DEFAULT_FILE_SERVING_HOST_URL
				+ fileEntityStorage.computeDisplayUrlComponent(uid, fileExt, MediaType.FILE, fileCategory, null);
	}

	public static String getEntityDownloadURL(CMDSEntityType entityType, String uid, String fileExt,
			MediaType mediaType, FileCategory fileCategory) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		return DEFAULT_FILE_STREAMING_HOST_URL
				+ fileEntityStorage.computeDisplayUrlComponent(uid, fileExt, mediaType, fileCategory, null);
	}

	public static String getEntityDownloadURL(CMDSEntityType entityType, String uid, String fileExt,
			MediaType mediaType, FileCategory fileCategory, String id) {

		IEntityFileStorage fileEntityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		return DEFAULT_FILE_DOWNLOAD_HOST_URL
				+ fileEntityStorage.computeDisplayUrlComponent(uid, fileExt, mediaType, fileCategory, null) + "/" + id;
	}

	public static String getPlaylistImageURL(String uuid) {

		return getEntityImageURL(CMDSEntityType.PLAYLIST, uuid, ImageSize.ORIGINAL);
	}

	public static String getPlaylistImageThumbnailURL(String uuid) {

		return getEntityThumbnail(CMDSEntityType.PLAYLIST, uuid);

	}

	public static String getQRQuestionEmbedImage(String imageName, String questionSetId) {

		return DEFAULT_HOST_URL_QUESTION_EMBED_IMAGES + questionSetId + File.separator + "img" + File.separator
				+ imageName;
	}

	public static String getQuestionEmbedImageURL(String imageName) {

		return DEFAULT_HOST_URL_QUESTION_EMBED_IMAGES + imageName;
	}

	public static String getQuestionEmbedTempImageURL(String imageName) {

		return DEFAULT_HOST_URL_QUESTION_EMBED_TEMP_IMAGES + imageName;
	}

	public static String getSolutionEmbedImageURL(String imageName) {

		return DEFAULT_HOST_URL_SOLUTION_EMBED_IMAGES + imageName;
	}

	//
	public static String getSolutionEmbedTempImageURL(String imageName) {

		return DEFAULT_HOST_URL_SOLUTION_EMBED_TEMP_IMAGES + imageName;
	}

	public static String getStatusFeedTempImageURL(String imageName) {

		return DEFAULT_HOST_URL_STATUS_FEED_EMBED_TEMP_IMAGES + imageName;
	}

	public static String getTempImageURL(String image) {

		return getImgHost() + "/temp/images/" + image;
	}

	public static String getGlobalTypeImageFormat(String content, String uuid) {

		// chekcking it already processed or content is empty
		if (StringUtils.isEmpty(content) || content.contains("v-uid=\"" + uuid)) {
			return content;
		}
		// replacing all uuids with embeded html

		return content.replaceAll(uuid, "<img v-uid=\"" + uuid + "\" src=\"\" class=\"vUrl\" v-perm=\"true\"/>");
	}

	public static String getEmbededHtml(String url, String uuid, boolean embedded) {

		// "<img src=\"" + uuid + "\"/>";
                if(embedded){
                    return "<img src=\"" + url + "\" v-uid=\"" + uuid + "\"/>";
                }

		return "<img  src=\"" + uuid + "\"/>";
	}

	private static String getEncryptedEntityUrl(String urlComponent, Map<String, String> sessionParamsMap) {

		if (sessionParamsMap == null) {
			return StringUtils.EMPTY;
		}
		String userId = sessionParamsMap.get("userId");
		String sessionId = sessionParamsMap.get("___ID");

		if (StringUtils.isEmpty(sessionId) || StringUtils.isEmpty(userId)) {
			return StringUtils.EMPTY;
		}
		String passphrase = sessionId + userId;
		FileMaskProcessor processor = new FileMaskProcessor(passphrase, passphrase.getBytes().length);
		byte[] actual = urlComponent.getBytes();
		byte[] result = new byte[actual.length];
		processor.process(actual, 0, actual.length, result);

		String encryptedUrl = Base64.getEncoder().encodeToString(result);

		return encryptedUrl;
	}

}
