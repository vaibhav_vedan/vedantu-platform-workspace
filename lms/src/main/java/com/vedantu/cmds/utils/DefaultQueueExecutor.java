/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.User.Role;
import com.vedantu.cmds.managers.*;
import com.vedantu.cmds.request.CurriculumTemplateInsertionRequest;
import com.vedantu.lms.cmds.pojo.ContentInfo;
import com.vedantu.dashboard.managers.LOAMAmbassadorLMSManager;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.util.ConfigUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.vedantu.doubts.managers.DoubtsManager;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.entities.challenges.PictureChallenge;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.managers.challenges.ChallengesManager;
import com.vedantu.cmds.managers.challenges.ClansManager;
import com.vedantu.cmds.managers.challenges.CommunicationManager;
import com.vedantu.cmds.managers.challenges.PostChallengeProcessor;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.lms.cmds.enums.ReprocessType;
import com.vedantu.lms.cmds.request.ShareCurriculumContentReq;
import com.vedantu.lms.cmds.response.ClanRes;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSTopic;

import javax.mail.internet.InternetAddress;
import org.apache.commons.lang3.StringUtils;

/**
 * @author ajith
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Autowired
    private PostChallengeProcessor postChallengeProcessor;

    @Autowired
    private ChallengesManager challengesManager;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private ClansManager clansManager;

    @Autowired
    private CMDSTestManager cMDSTestManager;

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    private ExportManager exportManager;

    @Autowired
    private CMDSTestAttemptManager cMDSTestAttemptManager;

    @Autowired
    private TopicTreeManager topicTreeManager;

    @Autowired
    private CMDSVideoManager videoManager;

    @Autowired
    private CurriculumManager curriculumManager;


    @Autowired
    private DoubtsManager doubtsManager;

    @Autowired
    private CurriculumTemplateManager curriculumTemplateManager;

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case END_CHALLENGE_CRON:
                postChallengeProcessor.endChallengeCron();
                break;
            case MARK_CHALLENGE_ACTIVE_CRON:
                postChallengeProcessor.markChallengeActiveCron();
                break;
            case POINTS_RESET_CRON:
                //  postChallengeProcessor.movePointsOfLeaderBoard();
                break;
            case UPDATE_CHALLENGE_ATTEMPTS:
                String cid = (String) payload.get("challengeId");
                Long userId = (Long) payload.get("userId");
                challengesManager.updateChallengeAttempts(cid, userId);
                break;
            case SEND_MAIL_CHALLENGE_ATTEMPTS:
                String category = String.valueOf(payload.get("category"));
                Long startTime = (Long) payload.get("startTime");
                Long endTime = (Long) payload.get("endTime");
                postChallengeProcessor.sendDailyScoreUserstoSendMail(category, startTime, endTime);
                break;
            case SEND_MAIL_UNATTEMPTED_USERS:
                postChallengeProcessor.sendMailToUnattemptedUsers();
                break;
            case SEND_MAIL_CHALLENGE_ATTEMPTS_CRON:
                postChallengeProcessor.sendDailyChallengeAttemptMail();
                break;
            case REPROCESS_CHALLENGE:
                Challenge challenge = (Challenge) payload.get("challenge");
                boolean reprocess = (boolean) payload.get("reprocess");
                ReprocessType reprocessType = (ReprocessType) payload.get("reprocessType");
                postChallengeProcessor.endChallenge(challenge, reprocess, reprocessType);
                break;
            case CLAN_CREATION_MAIL:
                ClanRes clanRes = (ClanRes) payload.get("clanRes");
                communicationManager.sendClanCreationMail(clanRes);
                break;
            case CLAN_JOINED_MAIL:
                ClanRes clan = (ClanRes) payload.get("clanRes");
                UserBasicInfo user = (UserBasicInfo) payload.get("user");
                communicationManager.sendClanJoinedMail(clan, user);
                break;
            case END_PICTURE_CHALLENGE:
                PictureChallenge pictureChallenge = (PictureChallenge) payload.get("pictureChallenge");
                clansManager.endPictureChallenge(pictureChallenge);
                break;
            case EXPORT_TEST_ATTEMPTS:
                String testId = (String) payload.get("testId");
                HttpSessionData currentSessionData = (HttpSessionData) payload.get("sessionData");
                //exportManager.getTestAttemptsExport(testId, currentSessionData);
                break;
            case CALCULATE_TEST_RANKS:
                cMDSTestManager.calculateTestRanks();
                break;
            case TRIGGER_SNS:
                SNSTopic topic = (SNSTopic) payload.get("topic");
                String snsSubject = (String) payload.get("subject");
                String snsMessage = (String) payload.get("message");
                awsSNSManager.triggerSNS(topic, snsSubject, snsMessage);
            case END_TEST_ATTEMPT:
                cMDSTestAttemptManager.endTests(Arrays.asList(ContentInfoType.OBJECTIVE));
                cMDSTestAttemptManager.endTests(Arrays.asList(ContentInfoType.SUBJECTIVE, ContentInfoType.MIXED));
                break;
            case UPDATE_BASE_TREE_CHILDREN:
                String nodeId = (String) payload.get("nodeId");
                topicTreeManager.updateBaseTreeChildren(nodeId);
                break;
            case UPDATE_BASE_TREE_NODE_PARENTS:
                String pNodeId = (String) payload.get("nodeId");
                topicTreeManager.updateBaseTreeNodeParents(pNodeId);
                break;
            case UPDATE_CURRICULUM_TREE_CHILDREN:
                topicTreeManager.updateCurriculumTreeChildren();
                break;
            case UPDATE_CMDS_VIDEO_VIMEO_METADATA:
                String vimeoUrl = (String) payload.get("vimeoUrl");
                String vimeoId = (String) payload.get("vimeoId");
                videoManager.updateVimeoMetadata(vimeoUrl, vimeoId);
                break;
            case UPDATE_CMDS_VIDEO_YOUTUBE_METADATA:
                String youTubeUrl = (String) payload.get("youTubeUrl");
                String youTubeId = (String) payload.get("youTubeId");
                videoManager.updateYouTubeMetadata(youTubeUrl, youTubeId);
                break;
            case CHANGE_TEACHER_FOR_ATTEMPT:
                String testIdForTeacherChange = (String) payload.get("testId");
                String teacherId = (String) payload.get("teacherId");
                String callingUserId = (String) payload.get("callingUserId");
                contentInfoManager.changeTeacherIdForAttempt(teacherId, testIdForTeacherChange);
                break;
            case COMPUTE_COMPETITIVE_TEST_STATS:
                CMDSTest cMDSTest = (CMDSTest) payload.get("test");
                cMDSTestManager.processCompetitiveTest(cMDSTest);
                break;
            case UPDATE_CLASSROOM_CHAT:
                videoManager.updateClassroomChat((CMDSVideoPlaylist) payload.get("videoPlaylist"));
                break;
            case SEND_DOUBT_REOPEN_NOTIFICATION:
                String dexId = (String) payload.get("dexId");
                String doubtId = (String) payload.get("doubtId");
                doubtsManager.sendDoubtReopenNotifcationToDex(dexId, doubtId);
                break;
            case SIMULATE_LIVE_CHAT:
                videoManager.startChat((String) payload.get(CMDSVideoPlaylist.Constants.ENTITY_ID));
                break;
            case SHARE_ASSIGNMENT:
                /*
                BatchBasicInfo batchBasicInfo = (BatchBasicInfo) payload.get("batchBasicInfo");
                List<UserBasicInfo> users = (List<UserBasicInfo>) payload.get("users");
                List<ContentInfo> contents = (List<ContentInfo>) payload.get("contents");
                String NOTIFICATION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");

                // Details about email
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setType(CommunicationType.ASSIGNMENT_SHARE);
                emailRequest.setClickTrackersEnabled(true);
                emailRequest.setIncludeHeaderFooter(false);

                // Details about SMS
                TextSMSRequest textSMSRequest = new TextSMSRequest();
                textSMSRequest.setType(CommunicationType.ASSIGNMENT_SHARE);

                // Sending Email and SMS
                for (UserBasicInfo userBasicInfo : users) {// Populating details about email
                    InternetAddress internetAddress = new InternetAddress();
                    internetAddress.setAddress(userBasicInfo.getEmail());
                    internetAddress.setPersonal(userBasicInfo.getFullName());
                    List<InternetAddress> emailTo = new ArrayList<>();
                    emailTo.add(internetAddress);
                    Map<String, Object> bodyScopes = new HashMap<>();
                    Map<String, Object> subjectScopes = new HashMap<>();
                    emailRequest.setTo(emailTo);

                    // Populating details about SMS
                    textSMSRequest.setTo(userBasicInfo.getContactNumber());
                    textSMSRequest.setPhoneCode(userBasicInfo.getPhoneCode());
                    textSMSRequest.setRole(Role.STUDENT);

                    for (ContentInfo content : contents) {
                        // Populating body scopes for SMS
                        bodyScopes.put("UserName", userBasicInfo.getFirstName());
                        bodyScopes.put("Assignment Title", content.getTitle());
                        bodyScopes.put("Course title", batchBasicInfo.getCourseInfo().getTitle());
                        textSMSRequest.setScopeParams(bodyScopes);
                        communicationManager.sendSMS(textSMSRequest);

                        if (StringUtils.isNotEmpty(userBasicInfo.getEmail())) {
                            // Populating body scopes for Email
                            bodyScopes.put("link", content.getUrl());
                            emailRequest.setBodyScopes(bodyScopes);

                            // Populating subject scopes for Email
                            subjectScopes.put("Assignment Title", content.getTitle());
                            subjectScopes.put("Course title", batchBasicInfo.getCourseInfo().getTitle());
                            emailRequest.setSubjectScopes(subjectScopes);
                            communicationManager.sendEmail(emailRequest);
                        }
                    }
                }
*/
                break;
            case POPULATE_CURRICULUM_STRUCTURE_TABLE:
                CurriculumTemplateInsertionRequest request = (CurriculumTemplateInsertionRequest) payload.get("request");
                curriculumTemplateManager.insertCurriculumTemplate(request);
                break;
            case SHARE_CONTENT_CURRICULUM_FOR_BATCH:
                ShareCurriculumContentReq shareCurriculumContentReq = (ShareCurriculumContentReq) payload.get("shareCurriculumContentReq");
                curriculumManager.shareContentForCurriculumForBatchAsync(shareCurriculumContentReq);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
