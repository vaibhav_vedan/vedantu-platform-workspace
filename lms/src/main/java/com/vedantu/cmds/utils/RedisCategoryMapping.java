package com.vedantu.cmds.utils;

import com.vedantu.cmds.enums.RedisKeyCategory;

import java.util.HashMap;
import java.util.Map;

import static com.vedantu.cmds.enums.RedisKeyCategory.*;

public class RedisCategoryMapping {

    private static Map<RedisKeyCategory, String> categoryMap;

    static {
        categoryMap = new HashMap<>();
        categoryMap.put(AMPLIFY_EVENTS, "AMPLIFY-EVENTS");
        categoryMap.put(FIREBASE_EVENTS, "FIREBASE-EVENTS");
        categoryMap.put(DEX, "DEX_DOUBT_ALLOWED");
        categoryMap.put(USER_LEVEL_DOUBT, "MAX_USER_DOUBT_ALLOWED");
        categoryMap.put(GLOBAL_LEVEL_DOUBT, "GLOBAL_USER_DOUBT_ALLOWED");
        categoryMap.put(BANNER, "PAGE_BANNER");
        categoryMap.put(ACTIVE_TABS, "ACTIVE_TABS");
        categoryMap.put(APP_RATING_AND_CHAT, "MOBILE_APP");
        categoryMap.put(PAID_USER, "PAID_USERS_MAX_DOUBTS");
        categoryMap.put(STRIP, "STRIP");
    }

    public static String getKeyPattern(RedisKeyCategory category) {
        return categoryMap.get(category);
    }
}

