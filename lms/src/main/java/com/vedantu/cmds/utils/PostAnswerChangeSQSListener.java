/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.utils;

import com.google.gson.Gson;
import com.vedantu.cmds.managers.CMDSTestAttemptManager;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.moodle.pojo.PostChangeAnswerSQSPojo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SQSMessageType;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author parashar
 */
@Component
public class PostAnswerChangeSQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory; 
    
    @Autowired
    private CMDSTestAttemptManager cMDSTestAttemptManager;
    
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PostAnswerChangeSQSListener.class);
    
    @Override
    public void onMessage(Message message) {
        
        TextMessage textMessage = (TextMessage) message;
        
        try{
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message "+ textMessage.getText());
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(sqsMessageType, textMessage.getText());
            message.acknowledge();            
        }catch(Exception ex){
            logger.error("Error processing message ",ex);            
        }
        
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void handleMessage(SQSMessageType sqsMessageType, String text) throws BadRequestException {
        PostChangeAnswerSQSPojo postChangeAnswerSQSPojo = new Gson().fromJson(text, PostChangeAnswerSQSPojo.class);
        switch(sqsMessageType){
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "UNKNOWN MESSAGE TYPE FOR SQS - " + sqsMessageType);

        }
    }
    
}
