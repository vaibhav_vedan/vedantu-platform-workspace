package com.vedantu.cmds.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.vedantu.cmds.enums.MediaType;
import com.vedantu.util.ConfigUtils;

public class MediaTypeMapper {

	private Map<String, MediaType> mediaTypeMap;
	private String mediaTypeConfFileName;
	private static MediaTypeMapper instance;

	private MediaTypeMapper() throws Exception {

		mediaTypeConfFileName = ConfigUtils.INSTANCE.getStringValue("media.types.file.path");
		mediaTypeMap = new HashMap<String, MediaType>();
		String playApplicationPath = ConfigUtils.INSTANCE.getStringValue("play.application.path");
		FileReader fr;
		fr = new FileReader(new File(playApplicationPath + "/" + mediaTypeConfFileName));
		BufferedReader br = new BufferedReader(fr);
		String line;
		while ((line = br.readLine()) != null) {
			String[] keyValue = StringUtils.split(line, ' ');
			String key = keyValue[0];
			MediaType value = MediaType.valueOfKey(keyValue[1]);
			mediaTypeMap.put(key, value);
		}
	}

	public static MediaTypeMapper INSTANCE() {

		// Not autowiring this because of reading the data from a file.
		if (instance == null) {
			synchronized (MediaTypeMapper.class) {
				if (instance == null) {
					try {
						instance = new MediaTypeMapper();
					} catch (IOException e) {
					} catch (Exception e) {
					}
				}

			}
		}
		return instance;
	}

	public MediaType getMediaType(String fileExtension) {

		if (!this.mediaTypeMap.containsKey(fileExtension)) {
			return MediaType.UNKNOWN;
		}
		return this.mediaTypeMap.get(fileExtension);
	}

}
