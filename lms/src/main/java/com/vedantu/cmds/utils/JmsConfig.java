/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.utils;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSSession;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.vedantu.aws.AwsCloudWatchManager;
import com.vedantu.listeners.SQSListener;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.MessageListener;
import javax.jms.Session;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * http://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/getting-started.html
 */
@Configuration
@EnableJms
public class JmsConfig {

    @Autowired
    private SQSListener sqsListener;

    private SQSConnection connection;

    private static final Logger logger = LogManager.getRootLogger();

    private Set<SQSQueue> queueListToCreateAlarm = new HashSet<>();

    @Autowired
    private AwsCloudWatchManager awsCloudWatchManager;
    private List<DefaultMessageListenerContainer> defaultMessageListenerContainers = new ArrayList<>();
    final SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
            .withRegion(Region.getRegion(Regions.AP_SOUTHEAST_1))
            .withNumberOfMessagesToPrefetch(10).build();
    final String env = ConfigUtils.INSTANCE.getStringValue("environment");
    final boolean useSqs = (!(StringUtils.isEmpty(env) || env.equals("LOCAL")));


    @Bean
    public SQSConnection sqsConnection() throws JMSException {
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
//            SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
//                    .withRegion(Region.getRegion(Regions.AP_SOUTHEAST_1))
//                    .withNumberOfMessagesToPrefetch(10).build();

            connection = sqsConnectionFactory.createConnection();
            Session session = connection.createSession(false, SQSSession.CLIENT_ACKNOWLEDGE);


//
//
//        DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
//        dmlc.setConnectionFactory(sqsConnectionFactory);
            //dmlc.setDestinationName(queueName);
            //dmlc.setSessionAcknowledgeMode(Session.DUPS_OK_ACKNOWLEDGE);
            //dmlc.setConcurrency("3-10");


            //Add queues to create alarm
            queueListToCreateAlarm.add(SQSQueue.CHALLENGES_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.POST_CHANGE_ANSWER_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.POST_CHANGE_ANSWER_QUEUE_DL);
            queueListToCreateAlarm.add(SQSQueue.POST_CHANGE_QUESTION_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.POST_CHANGE_QUESTION_QUEUE_DL);
            queueListToCreateAlarm.add(SQSQueue.POST_TEST_ATTEMPT_OPS);
            queueListToCreateAlarm.add(SQSQueue.POST_TEST_ATTEMPT_OPS_DL);

            queueListToCreateAlarm.add(SQSQueue.SHARE_TEST_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.SHARE_TEST_DL_QUEUE);

            queueListToCreateAlarm.add(SQSQueue.SHARE_ASSIGNMENT_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.SHARE_ASSIGNMENT_DL_QUEUE);

            queueListToCreateAlarm.add(SQSQueue.VGROUP_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.VGROUP_QUEUE_DL_QUEUE);

            queueListToCreateAlarm.add(SQSQueue.TEST_END_OPS);
            queueListToCreateAlarm.add(SQSQueue.TEST_END_OPS_DL);

            queueListToCreateAlarm.add(SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_QUEUE);
            queueListToCreateAlarm.add(SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_DL_QUEUE);

            queueListToCreateAlarm.add(SQSQueue.POST_TEST_END_OPS);
            queueListToCreateAlarm.add(SQSQueue.POST_TEST_END_OPS_DL);

            queueListToCreateAlarm.add(SQSQueue.MIGRATION_NON_FIFO_OPS);
            queueListToCreateAlarm.add(SQSQueue.MIGRATION_NON_FIFO_OPS_DL);

            connection.start();
            awsCloudWatchManager.createAlarms(queueListToCreateAlarm);
            logger.info("JMS Bean created");

            return connection;
        } else {
            return null;
        }
    }

    @Bean
    public DefaultMessageListenerContainer challengesQueueListenerContainer() {

        return generateDMLC(SQSQueue.CHALLENGES_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer postChangeAnswerQueueListenerContainer() {

        return generateDMLC(SQSQueue.POST_CHANGE_ANSWER_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer postChangeQuestionQueueListenerContainer() {

        return generateDMLC(SQSQueue.POST_CHANGE_QUESTION_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer postTestAttemptOpsQueueListenerContainer() {

        return generateDMLC(SQSQueue.POST_TEST_ATTEMPT_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer shareAssignmentQueueListenerContainer() {

        return generateDMLC(SQSQueue.SHARE_ASSIGNMENT_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer shareTestQueueListenerContainer() {

        return generateDMLC(SQSQueue.SHARE_TEST_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer bundleContentShareOpsContainer() {
        return generateDMLC(SQSQueue.BUNDLE_CONTENT_SHARE_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer vGroupQueueListener() {

        return generateDMLC(SQSQueue.VGROUP_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer testEndOpsQueueListener() {
        return generateDMLC(SQSQueue.TEST_END_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer teacherDashboardOpsQueueListener() {
        return generateDMLC(SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_QUEUE, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer postTestEndOpsListener() {
        return generateDMLC(SQSQueue.POST_TEST_END_OPS, sqsListener);
    }

    @Bean
    public DefaultMessageListenerContainer migrationNonFifoOpsListener() {
        return generateDMLC(SQSQueue.MIGRATION_NON_FIFO_OPS, sqsListener);
    }

    public DefaultMessageListenerContainer generateDMLC(SQSQueue queue, MessageListener _sqsListener) {
        if (useSqs) {
            logger.info("creating dmlc for " + queue);

            DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
            dmlc.setConnectionFactory(sqsConnectionFactory);
            dmlc.setDestinationName(queue.getQueueName(env));
            dmlc.setMessageListener(_sqsListener);
            dmlc.setConcurrency(queue.getMaxConcurrency());//dmlc.setConcurrency("3-10");
            dmlc.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
            defaultMessageListenerContainers.add(dmlc);

            return dmlc;
        } else {
            return null;
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (connection != null) {
                connection.close();
            }

            if (CollectionUtils.isNotEmpty(defaultMessageListenerContainers)) {
                defaultMessageListenerContainers.stream()
                        .filter(Objects::nonNull)
                        .forEachOrdered((dmlc) -> {
                            logger.info("destroying dmlc for " + dmlc.getDestinationName());
                            dmlc.destroy();
                        });
            }

            com.amazonaws.http.IdleConnectionReaper.shutdown();
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in closing sqs connection ", e);
        }
    }

}
