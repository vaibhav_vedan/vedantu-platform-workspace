package com.vedantu.cmds.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSTest;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Data
@NoArgsConstructor
public class CMDSTestCheckerResponse extends CMDSTest {
    private List<String> questionIds;
    private boolean editable =false;
    private String makerName="SYSTEM";
    private String checkerName;
    public CMDSTestCheckerResponse populateDetails(Map<String, List<String>> questionIdsMap, Map<String, UserBasicInfo> userBasicInfosMap, Long currentChecker){
        this.questionIds=questionIdsMap.get(this.getQuestionSetId());
        if(userBasicInfosMap.containsKey(this.getCreatedBy()) &&
                Objects.nonNull(userBasicInfosMap.get(this.getCreatedBy()))){
            this.makerName=userBasicInfosMap.get(this.getCreatedBy()).getFirstName();
        }
        if(userBasicInfosMap.containsKey(this.getAssignedChecker().toString()) &&
                Objects.nonNull(userBasicInfosMap.get(this.getAssignedChecker().toString()))){
            this.checkerName=userBasicInfosMap.get(this.getAssignedChecker().toString()).getFirstName();
        }
        if(currentChecker.equals(this.getAssignedChecker())){
            this.editable =true;
        }
        return this;
    }
}
