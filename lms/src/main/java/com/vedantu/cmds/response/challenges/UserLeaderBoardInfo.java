package com.vedantu.cmds.response.challenges;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserMinimalInfo;

public class UserLeaderBoardInfo {

    private Long userId;
    private UserMinimalInfo user;
    private long timeTaken;
    private int hintTakenIndex;
    private int rank;
    private long totalPoints;
	private double timeRate;
	private Long startTime;
	private Long endTime;

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public UserMinimalInfo getUser() {
		return user;
	}

	public void setUser(UserMinimalInfo user) {
		this.user = user;
	}

	public long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public int getHintTakenIndex() {
		return hintTakenIndex;
	}

	public void setHintTakenIndex(int hintTakenIndex) {
		this.hintTakenIndex = hintTakenIndex;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public long getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(long totalPoints) {
		this.totalPoints = totalPoints;
	}

	public double getTimeRate() {
		return timeRate;
	}

	public void setTimeRate(double timeRate) {
		this.timeRate = timeRate;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}
}
