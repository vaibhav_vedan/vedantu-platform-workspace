package com.vedantu.cmds.response.clans;

import com.vedantu.lms.cmds.response.ClanRes;

public class PictureChallengeLeaderBoardRes {

	private String clanId;
	private String pictureChallengeId;
	private long points;
	private long burstChallengePoints;
	private long musclePoints;
	private long pictureChallengePoints;
	private String category;
	private int rank;
	private ClanRes clan;

	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public String getPictureChallengeId() {
		return pictureChallengeId;
	}

	public void setPictureChallengeId(String pictureChallengeId) {
		this.pictureChallengeId = pictureChallengeId;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public long getBurstChallengePoints() {
		return burstChallengePoints;
	}

	public void setBurstChallengePoints(long burstChallengePoints) {
		this.burstChallengePoints = burstChallengePoints;
	}

	public long getMusclePoints() {
		return musclePoints;
	}

	public void setMusclePoints(long musclePoints) {
		this.musclePoints = musclePoints;
	}

	public long getPictureChallengePoints() {
		return pictureChallengePoints;
	}

	public void setPictureChallengePoints(long pictureChallengePoints) {
		this.pictureChallengePoints = pictureChallengePoints;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public ClanRes getClan() {
		return clan;
	}

	public void setClan(ClanRes clan) {
		this.clan = clan;
	}
}
