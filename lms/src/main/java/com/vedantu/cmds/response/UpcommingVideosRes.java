package com.vedantu.cmds.response;

import java.util.List;
import java.util.Set;

import com.vedantu.cmds.pojo.PlaylistVideoPojo;
import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;

public class UpcommingVideosRes extends AbstractTargetTopicEntity implements HomeFeedCardData{

    private String title;
    private String description;
    private Set<String> teacherIds;
    private boolean published = false;
    private List<PlaylistVideoPojo> videos;
    private boolean playButton = false;
    private String classInfoHeader;
    private List<String> classInfoPoints;

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the teacherIds
     */
    public Set<String> getTeacherIds() {
        return teacherIds;
    }

    /**
     * @param teacherIds the teacherIds to set
     */
    public void setTeacherIds(Set<String> teacherIds) {
        this.teacherIds = teacherIds;
    }

    /**
     * @return the published
     */
    public boolean isPublished() {
        return published;
    }

    /**
     * @param published the published to set
     */
    public void setPublished(boolean published) {
        this.published = published;
    }

    /**
     * @return the videos
     */
    public List<PlaylistVideoPojo> getVideos() {
        return videos;
    }

    /**
     * @param videos the videos to set
     */
    public void setVideos(List<PlaylistVideoPojo> videos) {
        this.videos = videos;
    }
 

    public boolean isPlayButton() {
		return playButton;
	}

	public void setPlayButton(boolean playButton) {
		this.playButton = playButton;
	}


	public String getClassInfoHeader() {
		return classInfoHeader;
	}

	public void setClassInfoHeader(String classInfoHeader) {
		this.classInfoHeader = classInfoHeader;
	}

	public List<String> getClassInfoPoints() {
		return classInfoPoints;
	}

	public void setClassInfoPoints(List<String> classInfoPoints) {
		this.classInfoPoints = classInfoPoints;
	}



	public static class Constants extends AbstractTargetTopicEntity.Constants {
        public static final String TEACHER_IDS = "teacherIds";
        public static final String PUBLISHED = "published"; 
        public static final String VIDEO_START_TIME = "videos.startTime";
        public static final String VIDEO_END_TIME = "videos.endTime";        
    }
}
