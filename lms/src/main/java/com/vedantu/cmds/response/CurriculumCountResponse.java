package com.vedantu.cmds.response;

import com.vedantu.lms.cmds.pojo.SubjectFileCountPojo;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.session.pojo.EntityType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CurriculumCountResponse{
    // Details about entity
    private EntityType entityType;
    private String entityId;
    private String entityName;

    // Details about batch
    private String batchId;
    private EntityStatus entityStatus;

    // Details about course
    private String courseId;
    private String courseName;

    // Count details
    private List<SubjectFileCountPojo> subjectFileCount;

    // Details of response in tree structure
    private List<CurriculumCountResponse> countResponses;
}
