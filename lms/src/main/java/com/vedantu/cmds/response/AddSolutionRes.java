package com.vedantu.cmds.response;

public class AddSolutionRes {
	public boolean isUpdated;
	public String updatedSolution;

	public AddSolutionRes() {
		super();
	}

	public AddSolutionRes(boolean isUpdated, String updatedSolution) {
		super();
		this.isUpdated = isUpdated;
		this.updatedSolution = updatedSolution;
	}

	public boolean isUpdated() {
		return isUpdated;
	}

	public void setUpdated(boolean isUpdated) {
		this.isUpdated = isUpdated;
	}

	public String getUpdatedSolution() {
		return updatedSolution;
	}

	public void setUpdatedSolution(String updatedSolution) {
		this.updatedSolution = updatedSolution;
	}

	@Override
	public String toString() {
		return "AddSolutionRes [isUpdated=" + isUpdated + ", updatedSolution=" + updatedSolution + "]";
	}
}
