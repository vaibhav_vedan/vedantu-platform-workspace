package com.vedantu.cmds.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BaseTreeResponse{
    private String nodeName;
    private List<BaseTreeResponse> baseTreeResponse;
}
