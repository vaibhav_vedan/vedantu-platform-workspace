/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.fos.response.AbstractRes;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class BookBasicInfo extends AbstractRes{

    private String id;
    private String displayName;
    private List<CloudStorageEntity> thumbnails = new ArrayList<>();
    private String name;
    private String edition;
    private int chapterCount;
    public Set<String> grades;
    private String subject;
    private VedantuRecordState recordState;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<CloudStorageEntity> getThumbnails() {
        if (thumbnails == null) {
            return new ArrayList<>();
        }
        return thumbnails;
    }

    public void setThumbnails(List<CloudStorageEntity> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public int getChapterCount() {
        return chapterCount;
    }

    public void setChapterCount(int chapterCount) {
        this.chapterCount = chapterCount;
    }

    public Set<String> getGrades() {
        return grades;
    }

    public void setGrades(Set<String> grades) {
        this.grades = grades;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public VedantuRecordState getRecordState() {
        return recordState;
    }

    public void setRecordState(VedantuRecordState recordState) {
        this.recordState = recordState;
    }

}
