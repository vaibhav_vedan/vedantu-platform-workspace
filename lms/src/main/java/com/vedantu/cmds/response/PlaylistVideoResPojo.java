package com.vedantu.cmds.response;

import org.springframework.data.mongodb.core.index.Indexed;

import com.vedantu.cmds.enums.PlaylistEntityType;

public class PlaylistVideoResPojo {

    private String entityId;
    private PlaylistEntityType playlistEntityType;
    private String title;
    private String description;
    private String url;
    private boolean live = false;
    @Indexed(background = true)
    private Long startTime;
    @Indexed(background = true)
    private Long endTime;
    private String thumbnailUrl;
    private Long videoViews = 0l;
    private Integer duration;
    private Long addTime;
    private boolean isNew;

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityid to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the playlistEntityType
     */
    public PlaylistEntityType getPlaylistEntityType() {
        return playlistEntityType;
    }

    /**
     * @param playlistEntityType the playlistEntityType to set
     */
    public void setPlaylistEntityType(PlaylistEntityType playlistEntityType) {
        this.playlistEntityType = playlistEntityType;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the live
     */
    public boolean isLive() {
        return live;
    }

    /**
     * @param live the live to set
     */
    public void setLive(boolean live) {
        this.live = live;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @param url the url to set
     */
    public void setUrl(String url) {
        this.url = url;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the thumbnailUrl
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * @param thumbnailUrl the thumbnailUrl to set
     */
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    /**
     * @return the videoViews
     */
    public Long getVideoViews() {
        return videoViews;
    }

    /**
     * @param videoViews the videoViews to set
     */
    public void setVideoViews(Long videoViews) {
        this.videoViews = videoViews;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Long getAddTime() {
		return addTime;
	}

	public void setAddTime(Long addTime) {
		this.addTime = addTime;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	@Override
    public String toString() {
        return "PlaylistVideoResPojo{" + "entityId=" + entityId + ", playlistEntityType=" + playlistEntityType + ", title=" + title + ", description=" + description + ", url=" + url + ", live=" + live + ", startTime=" + startTime + ", endTime=" + endTime + ", thumbnailUrl=" + thumbnailUrl + ", videoViews=" + videoViews + ", duration=" + duration + '}';
    }
}
