/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response.challenges;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author ajith
 */
public class StartChallengeRes extends AbstractRes {

    private String token;
    private CMDSQuestion question;
    private List<HintFormat> hints;
    private String challengeTitle;
    private Long endTime;
    private Integer maxPoints;
    private Integer hintTakenIndex = -1;
    private Long currentTime;

    public List<HintFormat> getHints() {
        return hints;
    }

    public void setHints(List<HintFormat> hints) {
        this.hints = hints;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public CMDSQuestion getQuestion() {
        return question;
    }

    public void setQuestion(CMDSQuestion question) {
        this.question = question;
    }

    public String getChallengeTitle() {
        return challengeTitle;
    }

    public void setChallengeTitle(String challengeTitle) {
        this.challengeTitle = challengeTitle;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Integer getHintTakenIndex() {
        return hintTakenIndex;
    }

    public void setHintTakenIndex(Integer hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
    }

    public Long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Long currentTime) {
        this.currentTime = currentTime;
    }

}
