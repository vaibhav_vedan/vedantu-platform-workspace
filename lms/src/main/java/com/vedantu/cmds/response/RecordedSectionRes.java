package com.vedantu.cmds.response;

import java.util.List;

import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;

public class RecordedSectionRes {
	List<CMDSVideo> trendingVideos;
	List<CMDSVideo> mostViewedVideos;
	List<CMDSVideoPlaylist> playlist;
	
	public List<CMDSVideo> getTrendingVideos() {
		return trendingVideos;
	}
	public void setTrendingVideos(List<CMDSVideo> trendingVideos) {
		this.trendingVideos = trendingVideos;
	}
	public List<CMDSVideo> getMostViewedVideos() {
		return mostViewedVideos;
	}
	public void setMostViewedVideos(List<CMDSVideo> mostViewedVideos) {
		this.mostViewedVideos = mostViewedVideos;
	}
	public List<CMDSVideoPlaylist> getPlaylist() {
		return playlist;
	}
	public void setPlaylist(List<CMDSVideoPlaylist> playlist) {
		this.playlist = playlist;
	}	
}