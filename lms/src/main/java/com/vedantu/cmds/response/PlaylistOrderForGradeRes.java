package com.vedantu.cmds.response;

import com.vedantu.cmds.entities.CMDSVideoPlaylist;

import java.util.List;

public class PlaylistOrderForGradeRes {

    private String playlistOrderId;
    private List<CMDSVideoPlaylist> list;

    public String getPlaylistOrderId() {
        return playlistOrderId;
    }

    public void setPlaylistOrderId(String playlistOrderId) {
        this.playlistOrderId = playlistOrderId;
    }

    public List<CMDSVideoPlaylist> getList() {
        return list;
    }

    public void setList(List<CMDSVideoPlaylist> list) {
        this.list = list;
    }
}
