/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response.challenges;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

/**
 *
 * @author ajith
 */
public class ChallengeLeaderBoardInfo extends AbstractMongoStringIdEntityBean {

    private Long userId;
    private UserBasicInfo user;
    private String challengeId;
    private long timeTaken;
    private int hintTakenIndex;
    private int rank;
    private Integer totalPoints;

    public ChallengeLeaderBoardInfo() {
    }
    
    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public UserBasicInfo getUser() {
        return user;
    }

    public void setUser(UserBasicInfo user) {
        this.user = user;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public int getHintTakenIndex() {
        return hintTakenIndex;
    }

    public void setHintTakenIndex(int hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public Integer getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(Integer totalPoints) {
        this.totalPoints = totalPoints;
    }

}
