/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response.challenges;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.enums.challenges.RankType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import java.util.Map;

/**
 *
 * @author ajith
 */
public class ChallengeUserInfoRes extends AbstractMongoStringIdEntityBean{
    private Long userId;
    private UserBasicInfo user;
    private long points;
    private int totalAttempts;
    private int correctAttempts;
    private double strikeRate;
    private Map<Integer, Integer> hintsCountMap;
    private String rankBucketId;
    private int bucketNo;
    private RankType type;
    private String rankIdentifier;
    private int rank;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public UserBasicInfo getUser() {
        return user;
    }

    public void setUser(UserBasicInfo user) {
        this.user = user;
    }
    

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }

    public void setTotalAttempts(int totalAttempts) {
        this.totalAttempts = totalAttempts;
    }

    public int getCorrectAttempts() {
        return correctAttempts;
    }

    public void setCorrectAttempts(int correctAttempts) {
        this.correctAttempts = correctAttempts;
    }

    public double getStrikeRate() {
        return strikeRate;
    }

    public void setStrikeRate(double strikeRate) {
        this.strikeRate = strikeRate;
    }

    public Map<Integer, Integer> getHintsCountMap() {
        return hintsCountMap;
    }

    public void setHintsCountMap(Map<Integer, Integer> hintsCountMap) {
        this.hintsCountMap = hintsCountMap;
    }

    public String getRankBucketId() {
        return rankBucketId;
    }

    public void setRankBucketId(String rankBucketId) {
        this.rankBucketId = rankBucketId;
    }

    public int getBucketNo() {
        return bucketNo;
    }

    public void setBucketNo(int bucketNo) {
        this.bucketNo = bucketNo;
    }

    public RankType getType() {
        return type;
    }

    public void setType(RankType type) {
        this.type = type;
    }

    public String getRankIdentifier() {
        return rankIdentifier;
    }

    public void setRankIdentifier(String rankIdentifier) {
        this.rankIdentifier = rankIdentifier;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
    
}
