package com.vedantu.cmds.response;

import lombok.Data;

@Data
public class RedisKeyValuePair {
    private String key;
    private String value;
}
