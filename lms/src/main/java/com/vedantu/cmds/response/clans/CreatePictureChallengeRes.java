/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response.clans;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.util.List;

/**
 *
 * @author ajith
 */
public class CreatePictureChallengeRes extends AbstractMongoStringIdEntityBean{
    private List<CloudStorageEntity> maskedImages;

    public List<CloudStorageEntity> getMaskedImages() {
        return maskedImages;
    }

    public void setMaskedImages(List<CloudStorageEntity> maskedImages) {
        this.maskedImages = maskedImages;
    }
}
