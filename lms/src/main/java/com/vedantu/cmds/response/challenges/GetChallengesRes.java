/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response.challenges;

import com.vedantu.util.fos.response.AbstractListRes;

/**
 *
 * @author ajith
 */
public class GetChallengesRes extends AbstractListRes<ChallengeRes> {

    private int attempted;
    private int active;
    private int pointsForGrabs;
    private int ended;

    public int getAttempted() {
        return attempted;
    }

    public void setAttempted(int attempted) {
        this.attempted = attempted;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    public int getPointsForGrabs() {
        return pointsForGrabs;
    }

    public void setPointsForGrabs(int pointsForGrabs) {
        this.pointsForGrabs = pointsForGrabs;
    }

    public int getEnded() {
        return ended;
    }

    public void setEnded(int ended) {
        this.ended = ended;
    }
}
