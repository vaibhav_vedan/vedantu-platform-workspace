package com.vedantu.cmds.response;

import com.vedantu.cmds.pojo.ParseQuestionMetadata;
import com.vedantu.util.fos.response.AbstractRes;
import lombok.AllArgsConstructor;

public class UploadQuestionSetFileRes extends AbstractRes {

    public ParseQuestionMetadata metadata;
    public String questionSetName;
    public String filePrefix;
    public String questionSetId;
    public CMDSQuestionSetInfo questionSetInfo;

    public UploadQuestionSetFileRes(ParseQuestionMetadata metadata,
            String questionSetName, String filePrefix, String questionSetId) {
        super();
        this.metadata = metadata;
        this.questionSetName = questionSetName;
        this.filePrefix = filePrefix;
        this.questionSetId = questionSetId;
    }

    public UploadQuestionSetFileRes(ParseQuestionMetadata metadata,
                                    String questionSetName, String filePrefix, String questionSetId, CMDSQuestionSetInfo cmdsQuestionSetInfo) {
        super();
        this.metadata = metadata;
        this.questionSetName = questionSetName;
        this.filePrefix = filePrefix;
        this.questionSetId = questionSetId;
        this.questionSetInfo = cmdsQuestionSetInfo;
    }

    public String getQuestionSetName() {
        return questionSetName;
    }

    public void setQuestionSetName(String questionSetName) {
        this.questionSetName = questionSetName;
    }

    public String getFilePrefix() {
        return filePrefix;
    }

    public void setFilePrefix(String filePrefix) {
        this.filePrefix = filePrefix;
    }

    public String getQuestionSetId() {
        return questionSetId;
    }

    public void setQuestionSetId(String questionSetId) {
        this.questionSetId = questionSetId;
    }

    public CMDSQuestionSetInfo getQuestionSetInfo() {
        return questionSetInfo;
    }

    public void setQuestionSetInfo(CMDSQuestionSetInfo questionSetInfo) {
        this.questionSetInfo = questionSetInfo;
    }

}
