package com.vedantu.cmds.response;

import com.vedantu.cmds.entities.CMDSTestList;

import java.util.List;

public class TestListOrderForGradeRes {

    private String playlistOrderId;
    private List<CMDSTestList> list;

    public String getPlaylistOrderId() {
        return playlistOrderId;
    }

    public void setPlaylistOrderId(String playlistOrderId) {
        this.playlistOrderId = playlistOrderId;
    }

    public List<CMDSTestList> getList() {
        return list;
    }

    public void setList(List<CMDSTestList> list) {
        this.list = list;
    }
}
