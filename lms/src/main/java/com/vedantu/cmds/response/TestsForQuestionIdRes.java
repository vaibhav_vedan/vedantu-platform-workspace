/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class TestsForQuestionIdRes{
    
    private List<CMDSTest> cmdsTests = new ArrayList<>();
    private List<CMDSQuestion> cmdsQuestions = new ArrayList<>();

    /**
     * @return the cmdsTests
     */
    public List<CMDSTest> getCmdsTests() {
        return cmdsTests;
    }

    /**
     * @param cmdsTests the cmdsTests to set
     */
    public void setCmdsTests(List<CMDSTest> cmdsTests) {
        this.cmdsTests = cmdsTests;
    }

    /**
     * @return the cmdsQuestions
     */
    public List<CMDSQuestion> getCmdsQuestions() {
        return cmdsQuestions;
    }

    /**
     * @param cmdsQuestions the cmdsQuestions to set
     */
    public void setCmdsQuestions(List<CMDSQuestion> cmdsQuestions) {
        this.cmdsQuestions = cmdsQuestions;
    }
    
    
    
}
