/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.SolutionInfo;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import java.util.List;

/**
 *
 * @author ajith
 */
public class QuestionFullInfo extends AbstractMongoStringIdEntityBean{

    private SolutionInfo solutionInfo;
    private RichTextFormat questionBody;
    private QuestionType type;
    private String questionSetName;
    private String questionSetId;
    private Marks marks;

    public List<HintFormat> refHints;
    private Difficulty difficulty;

    //extra params
    private String subject;

    //social Layer
    private long upVotes;
    private long downVotes;
    private long attempts;

    private boolean hasAns;
    private boolean hasSeenSolution;
    private boolean upVoted;
    private boolean downVoted;
    private boolean attempted;
    private boolean attemptable;
    private List<QuestionAttempt> userAttempts;
    private List<String> challengeIds;

    public QuestionFullInfo() {
    }

    public QuestionFullInfo(CMDSQuestion cMDSQuestion) {
        super(cMDSQuestion);
        this.solutionInfo = cMDSQuestion.getSolutionInfo();
        this.questionBody = cMDSQuestion.getQuestionBody();
        this.type = cMDSQuestion.getType();
        this.questionSetName = cMDSQuestion.getQuestionSetName();
        this.questionSetId = cMDSQuestion.getQuestionSetId();
        this.marks = cMDSQuestion.getMarks();
        this.refHints = cMDSQuestion.getRefHints();
        this.difficulty = cMDSQuestion.getDifficulty();
        this.subject = cMDSQuestion.getSubject();
        this.upVotes = cMDSQuestion.getUpVotes();
        this.downVotes = cMDSQuestion.getDownVotes();
        this.attempts=cMDSQuestion.getAttempts();
    }

    public SolutionInfo getSolutionInfo() {
        return solutionInfo;
    }

    public void setSolutionInfo(SolutionInfo solutionInfo) {
        this.solutionInfo = solutionInfo;
    }

    public RichTextFormat getQuestionBody() {
        return questionBody;
    }

    public void setQuestionBody(RichTextFormat questionBody) {
        this.questionBody = questionBody;
    }

    public QuestionType getType() {
        return type;
    }

    public void setType(QuestionType type) {
        this.type = type;
    }

    public String getQuestionSetName() {
        return questionSetName;
    }

    public void setQuestionSetName(String questionSetName) {
        this.questionSetName = questionSetName;
    }

    public String getQuestionSetId() {
        return questionSetId;
    }

    public void setQuestionSetId(String questionSetId) {
        this.questionSetId = questionSetId;
    }

    public Marks getMarks() {
        return marks;
    }

    public void setMarks(Marks marks) {
        this.marks = marks;
    }

    public List<HintFormat> getRefHints() {
        return refHints;
    }

    public void setRefHints(List<HintFormat> refHints) {
        this.refHints = refHints;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public boolean isHasSeenSolution() {
        return hasSeenSolution;
    }

    public void setHasSeenSolution(boolean hasSeenSolution) {
        this.hasSeenSolution = hasSeenSolution;
    }

    public boolean isUpVoted() {
        return upVoted;
    }

    public void setUpVoted(boolean upVoted) {
        this.upVoted = upVoted;
    }

    public boolean isDownVoted() {
        return downVoted;
    }

    public void setDownVoted(boolean downVoted) {
        this.downVoted = downVoted;
    }

    public List<String> getChallengeIds() {
        return challengeIds;
    }

    public void setChallengeIds(List<String> challengeIds) {
        this.challengeIds = challengeIds;
    }
    
    public boolean isHasAns() {
        return hasAns;
    }

    public void setHasAns(boolean hasAns) {
        this.hasAns = hasAns;
    }


    public boolean isAttempted() {
        return attempted;
    }

    public void setAttempted(boolean attempted) {
        this.attempted = attempted;
    }

    public List<QuestionAttempt> getUserAttempts() {
        return userAttempts;
    }

    public void setUserAttempts(List<QuestionAttempt> userAttempts) {
        this.userAttempts = userAttempts;
    }

    public boolean isAttemptable() {
        return attemptable;
    }

    public void setAttemptable(boolean attemptable) {
        this.attemptable = attemptable;
    }

    public long getUpVotes() {
        return upVotes;
    }

    public void setUpVotes(long upVotes) {
        this.upVotes = upVotes;
    }

    public long getDownVotes() {
        return downVotes;
    }

    public void setDownVotes(long downVotes) {
        this.downVotes = downVotes;
    }

    public long getAttempts() {
        return attempts;
    }

    public void setAttempts(long attempts) {
        this.attempts = attempts;
    }
    
}
