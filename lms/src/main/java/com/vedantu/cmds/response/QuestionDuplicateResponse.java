package com.vedantu.cmds.response;

import com.vedantu.cmds.entities.CMDSQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionDuplicateResponse {
    private String questionId;
    private Set<String> duplicates;
    private CMDSQuestion cmdsQuestion;

    public QuestionDuplicateResponse(CMDSQuestion cmdsQuestion){
        this.questionId=cmdsQuestion.getId();
        this.duplicates=new HashSet<>();
        this.cmdsQuestion=cmdsQuestion;
    }
}
