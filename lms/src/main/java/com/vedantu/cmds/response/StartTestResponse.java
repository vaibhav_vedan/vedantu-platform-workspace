package com.vedantu.cmds.response;

import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.pojo.CategoryQuestions;

import java.util.List;

/**
 * Created by somil on 23/03/17.
 */
public class StartTestResponse extends CMDSTestAttempt {

    private Long currentTime;
    private String testName;
    private List<CategoryQuestions> categories;
    private String notRegisteredUserId;
    private Long doNotShowResultsTill;
    private Boolean isExpired;
    private Long expiryDate;

    public String getNotRegisteredUserId() {
        return notRegisteredUserId;
    }

    public void setNotRegisteredUserId(String notRegisteredUserId) {
        this.notRegisteredUserId = notRegisteredUserId;
    }

    public Long getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(Long currentTime) {
        this.currentTime = currentTime;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public List<CategoryQuestions> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryQuestions> categories) {
        this.categories = categories;
    }

    public Long getDoNotShowResultsTill() {
        return doNotShowResultsTill;
    }

    public void setDoNotShowResultsTill(Long doNotShowResultsTill) {
        this.doNotShowResultsTill = doNotShowResultsTill;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public void setExpired(Boolean expired) {
        isExpired = expired;
    }

    public Long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Long expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Override
    public String toString() {
        return "StartTestResponse{" +
                "currentTime=" + currentTime +
                ", testName='" + testName + '\'' +
                ", categories=" + categories +
                ", doNotShowResultsTill=" + doNotShowResultsTill +
                '}';
    }

}

