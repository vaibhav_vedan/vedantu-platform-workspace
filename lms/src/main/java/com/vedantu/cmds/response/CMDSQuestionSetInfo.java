package com.vedantu.cmds.response;

import java.util.List;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSQuestionSet;
import com.vedantu.cmds.enums.Difficulty;
import lombok.Data;

@Data
public class CMDSQuestionSetInfo {
	private List<CMDSQuestion> questions;

	public String type;
	public int numberOfQuestionsComplete;

	public String fileName;
	public Difficulty difficulty;

	public CMDSQuestionSetInfo(List<CMDSQuestion> questions, String type, int numberOfQuestionsComplete,
			String fileName, Difficulty difficulty) {
		super();
		this.questions = questions;
		this.type = type;
		this.numberOfQuestionsComplete = numberOfQuestionsComplete;
		this.fileName = fileName;
		this.difficulty = difficulty;
	}

	public CMDSQuestionSetInfo(List<CMDSQuestion> questions, CMDSQuestionSet questionSet) {
		this(questions, questionSet.type, questionSet.numberOfQuestionsComplete, questionSet.fileName,
				questionSet.difficulty);
	}
}
