package com.vedantu.cmds.response;

import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class BatchCurriculamMergeNode {
    private Set<String> ids = new HashSet<>();
    private String parentId;
    private String id;
    private String title;
    private Integer childOrder;
    private int level;
    private BatchCurriculamMergeNode child;
}
