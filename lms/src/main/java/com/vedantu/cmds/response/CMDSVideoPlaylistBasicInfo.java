/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;

/**
 *
 * @author parashar
 */
public class CMDSVideoPlaylistBasicInfo extends CMDSVideoPlaylist{
    private UserBasicInfo createdByBasicInfo;

    /**
     * @return the createdBy
     */
    public UserBasicInfo getCreatedByBasicInfo() {
        return createdByBasicInfo;
    }

    /**
     * @param createdBy the createdBy to set
     */
    public void setCreatedByBasicInfo(UserBasicInfo createdByBasicInfo) {
        this.createdByBasicInfo = createdByBasicInfo;
    }
}
