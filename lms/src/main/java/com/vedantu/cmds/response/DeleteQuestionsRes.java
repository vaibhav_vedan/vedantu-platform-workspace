/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author ajith
 */
public class DeleteQuestionsRes extends AbstractRes {

    private List<String> deletedQuestionIds;
    private List<String> notDeletedQuestionIds;
    private List<String> errorsForNotDeletedQuestionIds;

    public List<String> getDeletedQuestionIds() {
        return deletedQuestionIds;
    }

    public void setDeletedQuestionIds(List<String> deletedQuestionIds) {
        this.deletedQuestionIds = deletedQuestionIds;
    }

    public List<String> getNotDeletedQuestionIds() {
        return notDeletedQuestionIds;
    }

    public void setNotDeletedQuestionIds(List<String> notDeletedQuestionIds) {
        this.notDeletedQuestionIds = notDeletedQuestionIds;
    }

    public List<String> getErrorsForNotDeletedQuestionIds() {
        return errorsForNotDeletedQuestionIds;
    }

    public void setErrorsForNotDeletedQuestionIds(List<String> errorsForNotDeletedQuestionIds) {
        this.errorsForNotDeletedQuestionIds = errorsForNotDeletedQuestionIds;
    }

}
