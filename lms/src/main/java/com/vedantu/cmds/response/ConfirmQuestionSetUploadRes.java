package com.vedantu.cmds.response;

public class ConfirmQuestionSetUploadRes {

	public String questionsSetName;
	public boolean isConfirmed;
	public String id;

	public ConfirmQuestionSetUploadRes() {
		super();
	}

	public ConfirmQuestionSetUploadRes(String questionsSetName, boolean isConfirmed, String id) {
		super();
		this.questionsSetName = questionsSetName;
		this.isConfirmed = isConfirmed;
		this.id = id;
	}

	public String getQuestionsSetName() {
		return questionsSetName;
	}

	public void setQuestionsSetName(String questionsSetName) {
		this.questionsSetName = questionsSetName;
	}

	public boolean isConfirmed() {
		return isConfirmed;
	}

	public void setConfirmed(boolean isConfirmed) {
		this.isConfirmed = isConfirmed;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "ConfirmQuestionSetUploadRes [questionsSetName=" + questionsSetName + ", isConfirmed=" + isConfirmed
				+ ", id=" + id + "]";
	}
}