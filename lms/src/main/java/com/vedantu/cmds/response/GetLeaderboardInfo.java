/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import java.util.List;

/**
 *
 * @author parashar
 */
public class GetLeaderboardInfo {
    private List<StudentScoreInfo> studentScoreInfos;
    private Integer index;
    private Integer total;
    private StudentScoreInfo userScoreInfo;

    /**
     * @return the studentScoreInfos
     */
    public List<StudentScoreInfo> getStudentScoreInfos() {
        return studentScoreInfos;
    }

    /**
     * @param studentScoreInfos the studentScoreInfos to set
     */
    public void setStudentScoreInfos(List<StudentScoreInfo> studentScoreInfos) {
        this.studentScoreInfos = studentScoreInfos;
    }

    /**
     * @return the index
     */
    public Integer getIndex() {
        return index;
    }

    /**
     * @param index the index to set
     */
    public void setIndex(Integer index) {
        this.index = index;
    }

    /**
     * @return the total
     */
    public Integer getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Integer total) {
        this.total = total;
    }

    /**
     * @return the userScoreInfo
     */
    public StudentScoreInfo getUserScoreInfo() {
        return userScoreInfo;
    }

    /**
     * @param userScoreInfo the userScoreInfo to set
     */
    public void setUserScoreInfo(StudentScoreInfo userScoreInfo) {
        this.userScoreInfo = userScoreInfo;
    }
}
