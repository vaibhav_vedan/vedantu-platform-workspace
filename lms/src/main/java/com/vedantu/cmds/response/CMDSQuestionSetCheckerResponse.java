package com.vedantu.cmds.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSQuestionSet;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;
import java.util.Objects;

@Data
@NoArgsConstructor
public class CMDSQuestionSetCheckerResponse extends CMDSQuestionSet {
    private boolean editable =false;
    private String makerName="SYSTEM";
    private String checkerName;

    public CMDSQuestionSetCheckerResponse populateMakerCheckerData(Map<String, UserBasicInfo> userBasicInfosMap, Long currentChecker) {
        if(userBasicInfosMap.containsKey(this.getCreatedBy()) &&
                Objects.nonNull(userBasicInfosMap.get(this.getCreatedBy()))){
            this.makerName=userBasicInfosMap.get(this.getCreatedBy()).getFirstName();
        }
        if(userBasicInfosMap.containsKey(this.getAssignedChecker().toString()) &&
                Objects.nonNull(userBasicInfosMap.get(this.getAssignedChecker().toString()))){
            this.checkerName=userBasicInfosMap.get(this.getAssignedChecker().toString()).getFirstName();
        }
        if(currentChecker.equals(this.getAssignedChecker())){
            this.editable =true;
        }
        return this;
    }
}
