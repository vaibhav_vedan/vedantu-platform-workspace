/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import com.vedantu.cmds.pojo.TestMarksSection;
import java.util.List;

/**
 *
 * @author parashar
 */
public class TestChartStatsRes {
    
    private List<TestMarksSection> testMarksSections;

    /**
     * @return the testMarksSections
     */
    public List<TestMarksSection> getTestMarksSections() {
        return testMarksSections;
    }

    /**
     * @param testMarksSections the testMarksSections to set
     */
    public void setTestMarksSections(List<TestMarksSection> testMarksSections) {
        this.testMarksSections = testMarksSections;
    }
    
    
}
