package com.vedantu.cmds.response;

import com.vedantu.cmds.enums.TestListType;
import com.vedantu.cmds.pojo.CMDSTestInfo;
import com.vedantu.homefeed.entity.HomeFeedCardData;

import java.util.List;
import java.util.Set;

public class CMDSTestListRes implements HomeFeedCardData {

    private String title;
    private String description;
    private TestListType type;
    private boolean published = false;
    private List<String> tests;
    private List<CMDSTestInfo> testsPojo;
    public Set<String> targetGrades; // CBSE-10
    public Set<String> tSubjects;
    public Set<String> tTopics;
    public Set<String> inputTopics;
    public Set<String> tGrades;
    public Set<String> tTargets;
    public Set<String> mainTags;
    private Long creationTime;
    private String createdBy;
    private Long lastUpdated;
    private String lastUpdatedBy; // For tracking last updated by
    private String id;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public List<String> getTests() {
        return tests;
    }

    public void setTests(List<String> tests) {
        this.tests = tests;
    }

    public List<CMDSTestInfo> getTestsPojo() {
        return testsPojo;
    }

    public void setTestsPojo(List<CMDSTestInfo> testsPojo) {
        this.testsPojo = testsPojo;
    }

    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    public Set<String> gettSubjects() {
        return tSubjects;
    }

    public void settSubjects(Set<String> tSubjects) {
        this.tSubjects = tSubjects;
    }

    public Set<String> gettTopics() {
        return tTopics;
    }

    public void settTopics(Set<String> tTopics) {
        this.tTopics = tTopics;
    }

    public Set<String> getInputTopics() {
        return inputTopics;
    }

    public void setInputTopics(Set<String> inputTopics) {
        this.inputTopics = inputTopics;
    }

    public Set<String> gettGrades() {
        return tGrades;
    }

    public void settGrades(Set<String> tGrades) {
        this.tGrades = tGrades;
    }

    public Set<String> gettTargets() {
        return tTargets;
    }

    public void settTargets(Set<String> tTargets) {
        this.tTargets = tTargets;
    }

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getLastUpdatedBy() {
        return lastUpdatedBy;
    }

    public void setLastUpdatedBy(String lastUpdatedBy) {
        this.lastUpdatedBy = lastUpdatedBy;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TestListType getType() {
        return type;
    }

    public void setType(TestListType type) {
        this.type = type;
    }

}
