/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response.challenges;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.challenges.ChallengeAttempt;
//import com.vedantu.cmds.entities.challenges.ChallengeAttempt;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.challenges.ChallengeType;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.challenges.ChallengeQuestion;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.ReprocessType;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import java.util.List;
import java.util.Set;

/**
 *
 * @author ajith
 */
public class ChallengeRes extends AbstractMongoStringIdEntityBean {

    private String title;
    private ChallengeType type;
    private ChallengeStatus status;
    private Long startTime;
    private Long burstTime;
    private Integer duration = 0;
    private Integer maxPoints = 0;
    private Difficulty difficulty;
    private List<ChallengeQuestion> questions;
    private Set<QuestionType> questionTypes;
    private String category;
    private List<UserBasicInfo> lastAttemptedUsers;
    private ChallengeLeaderBoardInfo leader;
    private ReprocessType reprocess;

    //extra fields other than db entity
    private int attempts;
    private boolean attempted;
    private ChallengeAttempt attemptInfo;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ChallengeType getType() {
        return type;
    }

    public void setType(ChallengeType type) {
        this.type = type;
    }

    public ChallengeStatus getStatus() {
        return status;
    }

    public void setStatus(ChallengeStatus status) {
        this.status = status;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(Long burstTime) {
        this.burstTime = burstTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public List<ChallengeQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<ChallengeQuestion> questions) {
        this.questions = questions;
    }

    public Set<QuestionType> getQuestionTypes() {
        return questionTypes;
    }

    public void setQuestionTypes(Set<QuestionType> questionTypes) {
        this.questionTypes = questionTypes;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    public boolean isAttempted() {
        return attempted;
    }

    public void setAttempted(boolean attempted) {
        this.attempted = attempted;
    }

    public ChallengeAttempt getAttemptInfo() {
        return attemptInfo;
    }

    public void setAttemptInfo(ChallengeAttempt attemptInfo) {
        this.attemptInfo = attemptInfo;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<UserBasicInfo> getLastAttemptedUsers() {
        return lastAttemptedUsers;
    }

    public void setLastAttemptedUsers(List<UserBasicInfo> lastAttemptedUsers) {
        this.lastAttemptedUsers = lastAttemptedUsers;
    }

    public ChallengeLeaderBoardInfo getLeader() {
        return leader;
    }

    public void setLeader(ChallengeLeaderBoardInfo leader) {
        this.leader = leader;
    }

    public ReprocessType getReprocess() {
        return reprocess;
    }

    public void setReprocess(ReprocessType reprocess) {
        this.reprocess = reprocess;
    }

}
