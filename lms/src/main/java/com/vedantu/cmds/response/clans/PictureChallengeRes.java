package com.vedantu.cmds.response.clans;

import java.util.List;

import com.vedantu.cmds.entities.challenges.PictureChallengeAttempt;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.challenges.PictureChallengeDayDeductions;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import com.vedantu.util.pojo.CloudStorageEntity;

public class PictureChallengeRes extends AbstractMongoStringIdEntityBean{
	
    private String title;
    private ChallengeStatus status;
    private Long startTime;
    private Long burstTime;
    private int maxPointsToWin = 0;
    private Difficulty difficulty;
    private String category;
    private List<PictureChallengeDayDeductions> dayWisePointsToWin;
    private CloudStorageEntity displayImage;
    private int percentagePoints;

	private PictureChallengeAttempt attemptInfo;
	private PictureChallengeLeaderBoardRes pointsInfo;
	private String solutionString;

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public ChallengeStatus getStatus() {
		return status;
	}

	public void setStatus(ChallengeStatus status) {
		this.status = status;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getBurstTime() {
		return burstTime;
	}

	public void setBurstTime(Long burstTime) {
		this.burstTime = burstTime;
	}

	public int getMaxPointsToWin() {
		return maxPointsToWin;
	}

	public void setMaxPointsToWin(int maxPointsToWin) {
		this.maxPointsToWin = maxPointsToWin;
	}

	public Difficulty getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(Difficulty difficulty) {
		this.difficulty = difficulty;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<PictureChallengeDayDeductions> getDayWisePointsToWin() {
		return dayWisePointsToWin;
	}

	public void setDayWisePointsToWin(List<PictureChallengeDayDeductions> dayWisePointsToWin) {
		this.dayWisePointsToWin = dayWisePointsToWin;
	}

	public PictureChallengeAttempt getAttemptInfo() {
		return attemptInfo;
	}

	public void setAttemptInfo(PictureChallengeAttempt attemptInfo) {
		this.attemptInfo = attemptInfo;
	}

	public PictureChallengeLeaderBoardRes getPointsInfo() {
		return pointsInfo;
	}

	public void setPointsInfo(PictureChallengeLeaderBoardRes pointsInfo) {
		this.pointsInfo = pointsInfo;
	}

	public CloudStorageEntity getDisplayImage() {
		return displayImage;
	}

	public void setDisplayImage(CloudStorageEntity displayImage) {
		this.displayImage = displayImage;
	}

	public int getPercentagePoints() {
		return percentagePoints;
	}

	public void setPercentagePoints(int percentagePoints) {
		this.percentagePoints = percentagePoints;
	}

	public String getSolutionString() {
		return solutionString;
	}

	public void setSolutionString(String solutionString) {
		this.solutionString = solutionString;
	}

}
