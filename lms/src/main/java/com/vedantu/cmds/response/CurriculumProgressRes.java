package com.vedantu.cmds.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CurriculumProgressRes {
    // private String contextId;
    private Long boardId;
    private int totalChapters = 0;
    private int completedChapters = 0;

    public void incrTotalChapters() {
        this.totalChapters += 1;
    }

    public void incrCompletedChapters() {
        this.completedChapters += 1;
    }
}