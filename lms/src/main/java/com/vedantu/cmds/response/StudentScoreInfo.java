/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.response;

import com.vedantu.moodle.pojo.SectionResultInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @author parashar
 */
@Data
@NoArgsConstructor
public class StudentScoreInfo {
    private String fullName;
    private String userId;
    private List<SectionResultInfo> sectionResultInfos;
    private Float marksAchieved;
    private Float totalMarks;
    private Long duration;
    private Integer rank;
    
}
