package com.vedantu.cmds.response.challenges;

import com.vedantu.cmds.entities.challenges.ClanLeaderBoard;
import com.vedantu.lms.cmds.response.ClanRes;

public class ClanLeaderBoardRes extends ClanLeaderBoard{

	private ClanRes clan;
	private int rank;

	public ClanRes getClan() {
		return clan;
	}

	public void setClan(ClanRes clan) {
		this.clan = clan;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}
}

