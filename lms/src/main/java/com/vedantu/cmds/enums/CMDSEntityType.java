package com.vedantu.cmds.enums;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public enum CMDSEntityType implements Serializable {
	// @formatter:off
	UNKNOWN(null, null, null, null),

	USER("usr", "u", "users", "users"), MODULE("mod", "m", "module", "modules"), DOCUMENT("doc", "d", "document",
			"documents"), VIDEO("vid", "v", "video", "videos"), QUESTION("qus", "q", "question",
					"questions"), CHALLENGE("chall", "chall", "challenge", "challenges"), SOLUTION("sol", "sol", null,
							null), PLAYLIST(null, null, null, null), TEST("test", "t", "test", "tests"), ASSIGNMENT(
									"assignment", "asnm", "assignment", "assignments"), DISCUSSION("diss", "diss",
											"discussion", "discussions"), FILE("fl", "file", "file", "files"),

	COMMENT("cmnt", "cmnt", null, null), FOLDER(null, null, null, null), REMARK("rmk", "rmk", null,
			null), DIAGRAM("dia", "dia", null, null), MESSAGE("msg", "msg", null, null),

	INVOICE("iv", "iv", null, null), PLAN("plan", "plan", null, null),

	CONTENTGROUP("cg", "cg", null, null),

	// / this is only needed for internal/ cmds purpose
	CMDSRESOURCE("cmdres", "cr", "resource", "cmdsresources"), CMDSMODULE("mod", "cmdsmod", "cmdsmodule",
			"cmdsmodules"), CMDSDOCUMENT("doc", "cmdsdoc", "cmdsdocument", "cmdsdocuments"), CMDSFILE("fl", "file",
					"cmdsfile",
					"cmdsfiles"), CMDSQUESTION("qus", "cmdsqs", "cmdsquestion", "cmdsquestions"), CMDSASSIGNMENT(
							"cmdsa", "cmdsasnm", "cmdsassignment",
							"cmdsassignments"), CMDSTEST("cmdst", "cmdstes", "cmdstest", "cmdstests"), CMDSQUESTIONSET(
									"cmdsqs", "cmdsqs", "cmdsquestionset", "cmdsquestionsets"), CMDSVIDEO("vid",
											"cmdsvid", "cmdsvideo", "cmdsvideos"), STATUSFEED("sf", "sf", null,
													null), CHANNEL, ORGANIZATION("org", "org", null, null), DEPARTMENT(
															"dept", "dept", null, null), PROGRAM("prg", "prog", null,
																	null), SECTION("sect", "sect", null,
																			null), CENTER("cntr", "cntr", null, null),

	EXPORTRECORD("exports", "exports", null, null), COMPOUNDMEDIA;
	// @formatter:on

	private static Set<CMDSEntityType> cmdsLibrarySupportedType = new HashSet<CMDSEntityType>();
	private static Set<CMDSEntityType> orgEntityTypes = new HashSet<CMDSEntityType>();
	private static Set<CMDSEntityType> supportedPublishedType = new HashSet<CMDSEntityType>();
	private static Set<CMDSEntityType> attemptableEntities = new HashSet<CMDSEntityType>();
	static {
		supportedPublishedType.add(DOCUMENT);
		supportedPublishedType.add(VIDEO);
		supportedPublishedType.add(QUESTION);
		supportedPublishedType.add(PLAYLIST);
		supportedPublishedType.add(TEST);
		supportedPublishedType.add(ASSIGNMENT);
		supportedPublishedType.add(STATUSFEED);
		supportedPublishedType.add(REMARK);
		supportedPublishedType.add(DISCUSSION);
		supportedPublishedType.add(CHALLENGE);
		supportedPublishedType.add(FILE);
		supportedPublishedType.add(MODULE);

		cmdsLibrarySupportedType.add(CMDSTEST);
		cmdsLibrarySupportedType.add(CMDSASSIGNMENT);
		cmdsLibrarySupportedType.add(CMDSQUESTION);
		cmdsLibrarySupportedType.add(CMDSVIDEO);
		cmdsLibrarySupportedType.add(CMDSDOCUMENT);
		cmdsLibrarySupportedType.add(CMDSFILE);
		cmdsLibrarySupportedType.add(CMDSMODULE);

		orgEntityTypes.add(CENTER);
		orgEntityTypes.add(PROGRAM);
		orgEntityTypes.add(ORGANIZATION);
		orgEntityTypes.add(SECTION);

		attemptableEntities.add(QUESTION);
		attemptableEntities.add(CHALLENGE);
		attemptableEntities.add(TEST);
		attemptableEntities.add(ASSIGNMENT);
	}

	private String acronym;
	private String qualifierChar;
	private String indexType;
	private String indexName;

	// private Class<?> classAbstractBoardSearchEntityTagDetails;

	private static Map<String, CMDSEntityType> mapAcronym = null;

	private CMDSEntityType() {

		this(null, null, null, null);
	}

	private CMDSEntityType(final String acronym, final String qualifierChar, String indexType, final String indexName
	// , final Class<?> classAbstractBoardSearchEntityTagDetails
	) {

		this.acronym = acronym;
		this.qualifierChar = qualifierChar;
		this.indexType = indexType;
		this.indexName = indexName;
		// if (null != classAbstractBoardSearchEntityTagDetails
		// && !AbstractBoardSearchEntityTagDetails.class
		// .isAssignableFrom(classAbstractBoardSearchEntityTagDetails)) {
		// throw new RuntimeException("illegal class specification: "
		// + classAbstractBoardSearchEntityTagDetails);
		// }
		// this.classAbstractBoardSearchEntityTagDetails =
		// classAbstractBoardSearchEntityTagDetails;
	}

	public String getAcronym() {

		return acronym;
	}

	public String getQualifierChar() {

		return qualifierChar;
	}

	public String getIndexType() {

		return indexType;
	}

	public String getIndexName() {

		return indexName;
	}

	public static boolean isValidEntityType(String entityTypeValue) {

		CMDSEntityType entityType = null;
		try {
			entityType = CMDSEntityType.valueOf(entityTypeValue);
		} catch (Exception e) {
			// Swallow
		}
		return null != entityType;
	}

	public static CMDSEntityType valueOfKey(String value) {

		CMDSEntityType entityType = UNKNOWN;
		try {
			entityType = CMDSEntityType.valueOf(value.toUpperCase());
		} catch (Exception e) {
			// Swallow
		}
		return entityType;
	}

	public static boolean isValidQualifierChar(String q) {

		boolean result = false;
		for (CMDSEntityType e : CMDSEntityType.values()) {
			if (e.qualifierChar.equalsIgnoreCase(q)) {

				result = true;
				break;
			}
		}
		return result;
	}

	public static CMDSEntityType getByAcronym(String requestedAcronym) {

		if (null == mapAcronym) {
			synchronized (MediaType.class) {
				if (null == mapAcronym) {
					mapAcronym = new HashMap<String, CMDSEntityType>();
					for (CMDSEntityType entityType : CMDSEntityType.values()) {
						mapAcronym.put(entityType.acronym, entityType);
					}
				}
			}
		}
		return null != requestedAcronym && null != mapAcronym ? mapAcronym.get(requestedAcronym) : null;
	}

	public static boolean isValidOrgEntity(CMDSEntityType orgEntityType) {

		return orgEntityTypes.contains(orgEntityType);
	}

	public static boolean isSupportedCMDSLibraryEntityType(CMDSEntityType contentTYpe) {

		return cmdsLibrarySupportedType.contains(contentTYpe);
	}

	public static boolean isSupportedContentType(CMDSEntityType contentType) {

		return supportedPublishedType.contains(contentType);
	}

	public static boolean isAttemptableEntity(CMDSEntityType entityType) {

		return attemptableEntities.contains(entityType);
	}

	public String _getStorageId() {

		switch (this) {
		case CMDSQUESTION:
			return QUESTION.name().toLowerCase();
		case CMDSVIDEO:
			return VIDEO.name().toLowerCase();
		case CMDSDOCUMENT:
			return DOCUMENT.name().toLowerCase();
		case CMDSFILE:
			return FILE.name().toLowerCase();
		case EXPORTRECORD:
			return "exports";
		}

		return this.name().toLowerCase();
	}
}
