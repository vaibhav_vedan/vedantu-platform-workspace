package com.vedantu.cmds.enums.challenges;

public enum ChallengeType {
    BURST_IMMEDIATELY, BURST_LATER;
}
