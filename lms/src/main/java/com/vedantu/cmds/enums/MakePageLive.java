package com.vedantu.cmds.enums;

import org.apache.commons.lang.StringUtils;

public enum MakePageLive {
    YES, NO;

    public static MakePageLive valueOfKey(String value) {
        MakePageLive indexable = NO;
        for (MakePageLive ind : values()) {
            if (StringUtils.equalsIgnoreCase(value, ind.name())) {
                indexable = ind;
                break;
            }
        }
        return indexable;
    }
}
