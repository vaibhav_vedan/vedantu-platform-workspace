package com.vedantu.cmds.enums.challenges;

public enum CreditType {
    POSITIVE, NEGATIVE;
}
