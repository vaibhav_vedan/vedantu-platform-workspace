package com.vedantu.cmds.enums;

/**
 * Created by somil on 20/03/17.
 */
public enum TestMode {
    ONLINE, OFFLINE;

    public static TestMode valueOfKey(String key) {
        TestMode mode = ONLINE;
        try {
            mode = valueOf(key.trim().toUpperCase());
        } catch (Exception e) {
        }
        return mode;
    }
}
