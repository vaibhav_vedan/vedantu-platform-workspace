package com.vedantu.cmds.enums;

import java.util.Arrays;
import java.util.HashSet;

import com.vedantu.exception.BadRequestException;
import org.apache.commons.lang.StringUtils;

import com.vedantu.cmds.constants.QuestionSetFileConstants;
import com.vedantu.cmds.pojo.EntireQuestion;
import com.vedantu.cmds.pojo.ParseQuestionMetadata;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.QuestionType;
import java.util.Set;

public enum MetadataParts {

    TITLE {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            if (questionMetadata.title == null) {
                questionMetadata.title = StringUtils.EMPTY;
            }
            questionMetadata.title += runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_TITLE)
                    ? StringUtils.substringAfter(runText, ":").trim()
                    : runText.trim();

        }
    },
    TYPE {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws BadRequestException {
            String type = runText.toLowerCase().replaceAll(EntireQuestion.REGEX_HTML_STRIP, "").startsWith("type:")
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;
            QuestionType temporaryType = QuestionType.valueOfKey(type.trim());
            questionMetadata.type = temporaryType;
        }
    },
    INDEXABLE {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            String levels = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_INDEXABLE)
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;

            questionMetadata.indexable = Indexable.valueOfKey(levels.trim().toLowerCase());
        }
    },
    DIFFICULTY {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            String levels = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_DIFFICULTY)
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;

            questionMetadata.difficulty = Difficulty.valueOfKey(levels.trim().toLowerCase());
        }
    },
    SUBJECT {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception, VException {
            String subjects = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_SUBJECT)
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;
            questionMetadata.subject = subjects.trim();
        }
    },
    TOPICS {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            Set<String> topics = questionMetadata.getTopics();
            if (topics == null) {
                topics = new HashSet<>();
                questionMetadata.setTopics(topics);
            }
            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_TOPICS, topics);
        }
    },
    ANALYSIS_TAGS {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            Set<String> analysisTags = questionMetadata.getAnalysisTags();
            if (analysisTags == null) {
                analysisTags = new HashSet<>();
                questionMetadata.setTopics(analysisTags);
            }
            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_ANALYSIS_TAGS, analysisTags);
        }
    },
//    SUBTOPICS {
//        @Override
//        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
//                throws Exception {
//            Set<String> subtopics = questionMetadata.getSubTopics();
//            if (subtopics == null) {
//                subtopics = new HashSet<>();
//                questionMetadata.setSubTopics(subtopics);
//            }
//            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_SUBTOPICS, subtopics);
//        }
//    },
    TAGS {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            Set<String> tags = questionMetadata.getTags();
            if (tags == null) {
                tags = new HashSet<>();
                questionMetadata.setTags(tags);
            }
            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_TAGS, tags);
        }
    },
//    GRADES {
//        @Override
//        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
//                throws Exception {
//            Set<String> grades = questionMetadata.getGrades();
//            if (grades == null) {
//                grades = new HashSet<>();
//                questionMetadata.setGrades(grades);
//            }
//            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_GRADES, grades);
//        }
//    },
    TARGETGRADE {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            Set<String> targetgrade = questionMetadata.getTargetgrade();
            if (targetgrade == null) {
                targetgrade = new HashSet<>();
                questionMetadata.setGrades(targetgrade);
            }
            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_TARGET_GRADE, targetgrade);
        }
    },
    DIFFICULTY_VALUE {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            String difficultyValue = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_DIFFICULTY_VALUE)
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;
            questionMetadata.difficultyValue = Integer.parseInt(difficultyValue.trim());
        }
    },
    COGNITIVE_LEVEL {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            String cognitiveLevel = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_COGNITIVE_LEVEL)
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;
            CognitiveLevel cLevel = CognitiveLevel.valueOf(cognitiveLevel.trim());
            questionMetadata.cognitiveLevel = cLevel;
        }
    },
    SOURCE {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            String source = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_SOURCE)
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;
            questionMetadata.source = Arrays.asList( source.trim().split(","));
        }
    },
    TARGETS {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            Set<String> targets = questionMetadata.getTargets();
            if (targets == null) {
                targets = new HashSet<>();
                questionMetadata.setTargets(targets);
            }
            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_TARGETS, targets);
        }
    },
    MARKS {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {

//            if (StringUtils.isNotEmpty(v.trim())) {
//                String htmlStrippedString = v.trim().replaceAll(EntireQuestion.REGEX_HTML_STRIP, "");
//                String marksString = htmlStrippedString.substring(htmlStrippedString.indexOf(":") + 1).trim();
//                if (!StringUtils.isEmpty(marksString)) {
//                    q.marks.setPositive(Float.parseFloat(marksString));
//                }
//            }
            if (StringUtils.isNotEmpty(runText)) {
                String marksString = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_MARKS)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                if (StringUtils.isNotEmpty(marksString)) {
                    questionMetadata.marks.setPositive(Float.parseFloat(marksString));
                }
            }
        }
    },
    NEGATIVE_MARKS {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
            if (StringUtils.isNotEmpty(runText)) {
                String marksString = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_NEGATIVE_MARKS)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                if (StringUtils.isNotEmpty(marksString)) {
                    questionMetadata.marks.setNegative(Float.parseFloat(marksString));
                }
            }
        }
    },
    BOOK {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
            if (StringUtils.isNotEmpty(runText)) {
                String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_BOOK)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                str = StringUtils.trimToEmpty(str);
                if (StringUtils.isNotEmpty(str)) {
                    questionMetadata.setBook(str);
                }
            }
        }
    },
    EDITION {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
            if (StringUtils.isNotEmpty(runText)) {
                String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_EDITION)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                str = StringUtils.trimToEmpty(str);
                if (StringUtils.isNotEmpty(str)) {
                    questionMetadata.setEdition(str);
                }
            }
        }
    },
    CHAPTER {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
            if (StringUtils.isNotEmpty(runText)) {
                String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_CHAPTER)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                str = StringUtils.trimToEmpty(str);
                if (StringUtils.isNotEmpty(str)) {
                    questionMetadata.setChapter(str);
                }
            }
        }
    },
    CHAPTER_NO {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
            if (StringUtils.isNotEmpty(runText)) {
                String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_CHAPTER_NO)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                str = StringUtils.trimToEmpty(str);
                if (StringUtils.isNotEmpty(str)) {
                    questionMetadata.setChapterNo(str);
                }
            }
        }
    },
    EXERCISE {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
            if (StringUtils.isNotEmpty(runText)) {
                String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_EXERCISE)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                str = StringUtils.trimToEmpty(str);
                if (StringUtils.isNotEmpty(str)) {
                    questionMetadata.setExercise(str);
                }
            }
        }
    },
    PAGE_NOS {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
                throws Exception {
            Set<String> pagenos = questionMetadata.getPageNos();
            if (pagenos == null) {
                pagenos = new HashSet<>();
                questionMetadata.setPageNos(pagenos);
            }
            addCommaSeparatedValuesAsSlugs(runText, QuestionSetFileConstants.PREFIX_PAGE_NOS, pagenos);
        }
    },
    SLNOIN_BOOK {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) throws BadRequestException {
            if (StringUtils.isNotEmpty(runText)) {
                String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_SLNOIN_BOOK)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                if (StringUtils.isNotEmpty(str)) {
                    try {
                        questionMetadata.setSlNoInBook(Integer.parseInt(str.trim()));
                    } catch (Exception e) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "slNoinBook should be integer " + e.getMessage());
                    }
                }
            }
        }
    },
    QUESTION_NO_IN_BOOK {
        @Override
        public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
            if (StringUtils.isNotEmpty(runText)) {
                String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_QUESTION_NO_IN_BOOK)
                        ? StringUtils.substringAfter(runText, ":")
                        : runText;
                if (StringUtils.isNotEmpty(str)) {
                    questionMetadata.setQuestionNo(str.trim());
                }
            }
        }
    },
    MAKE_PAGE_LIVE {
         @Override
         public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
             String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_MAKE_PAGE_LIVE)
                     ? StringUtils.substringAfter(runText, ":")
                     : runText;

             if (StringUtils.isNotEmpty(str)) {
                 questionMetadata.setMakePageLive(MakePageLive.valueOfKey(str.trim()));
             }
         }
    },
    VIDEO {
         @Override
         public void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override) {
             String str = runText.toLowerCase().startsWith(QuestionSetFileConstants.PREFIX_VIDEO)
                     ? StringUtils.substringAfter(runText, ":")
                     : runText;

             if (StringUtils.isNotEmpty(str)) {
                 questionMetadata.setVideo(str);
             }
         }
    };

    public abstract void accumulate(ParseQuestionMetadata questionMetadata, String runText, boolean override)
            throws Exception, VException;

    public void addCommaSeparatedValuesAsSlugs(String runText, String prefix, Set<String> addToSet) {
        if (StringUtils.isNotEmpty(runText) && addToSet != null) {
            String tagString = runText.toLowerCase().startsWith(prefix)
                    ? StringUtils.substringAfter(runText, ":")
                    : runText;
            if (!StringUtils.isEmpty(tagString)) {
                String[] tags = tagString.split(",");
                if (tags.length > 0) {
                    for (String tag : tags) {
                        String slug = StringUtils.trimToEmpty(tag);
                        if (StringUtils.isNotEmpty(slug)) {
                            addToSet.add(slug);
                        }
                    }
                }
            }
        }

    }

}
