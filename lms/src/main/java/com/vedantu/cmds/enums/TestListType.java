package com.vedantu.cmds.enums;

public enum TestListType {
    UPCOMING, ONGOING, NORMAL
}
