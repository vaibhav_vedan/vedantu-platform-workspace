/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.enums;

import com.vedantu.async.IAsyncTaskName;

/**
 *
 * @author ajith
 */
public enum AsyncTaskName implements IAsyncTaskName {

	UPDATE_CMDS_VIDEO_VIMEO_METADATA(AsyncQueueName.DEFAULT_QUEUE),
	UPDATE_CMDS_VIDEO_YOUTUBE_METADATA(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_BASE_TREE_CHILDREN(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_BASE_TREE_NODE_PARENTS(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_CURRICULUM_TREE_CHILDREN(AsyncQueueName.DEFAULT_QUEUE),
    END_CHALLENGE_CRON(AsyncQueueName.DEFAULT_QUEUE),
    MARK_CHALLENGE_ACTIVE_CRON(AsyncQueueName.DEFAULT_QUEUE),
    CREATE_CHALLENGE_LEADER_BOARD(AsyncQueueName.DEFAULT_QUEUE),
    CALCULATE_CHALLENGE_RANKS(AsyncQueueName.DEFAULT_QUEUE),
    CALCULATE_CHALLENGE_POINTS(AsyncQueueName.DEFAULT_QUEUE),
    RECALCULATE_VOTES(AsyncQueueName.DEFAULT_QUEUE),
    POINTS_RESET_CRON(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_CHALLENGE_ATTEMPTS(AsyncQueueName.DEFAULT_QUEUE),
    SEND_MAIL_CHALLENGE_ATTEMPTS_CRON(AsyncQueueName.DEFAULT_QUEUE),
    SEND_MAIL_CHALLENGE_ATTEMPTS(AsyncQueueName.DEFAULT_QUEUE), 
    SEND_MAIL_UNATTEMPTED_USERS(AsyncQueueName.DEFAULT_QUEUE), 
    REPROCESS_CHALLENGE(AsyncQueueName.DEFAULT_QUEUE), 
    CLAN_CREATION_MAIL(AsyncQueueName.DEFAULT_QUEUE),
    CLAN_JOINED_MAIL(AsyncQueueName.DEFAULT_QUEUE),
    END_PICTURE_CHALLENGE(AsyncQueueName.DEFAULT_QUEUE), 
    EXPORT_TEST_ATTEMPTS(AsyncQueueName.DEFAULT_QUEUE),
    CALCULATE_TEST_RANKS(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_SNS(AsyncQueueName.DEFAULT_QUEUE),
    END_TEST_ATTEMPT(AsyncQueueName.DEFAULT_QUEUE),
    CHANGE_TEACHER_FOR_ATTEMPT(AsyncQueueName.DEFAULT_QUEUE),
    COMPUTE_COMPETITIVE_TEST_STATS(AsyncQueueName.DEFAULT_QUEUE),
	UPDATE_CLASSROOM_CHAT(AsyncQueueName.DEFAULT_QUEUE),
	SIMULATE_LIVE_CHAT(AsyncQueueName.DEFAULT_QUEUE),
    SHARE_ASSIGNMENT(AsyncQueueName.DEFAULT_QUEUE),
    SEND_DOUBT_REOPEN_NOTIFICATION(AsyncQueueName.DEFAULT_QUEUE),
    POPULATE_CURRICULUM_STRUCTURE_TABLE(AsyncQueueName.DEFAULT_QUEUE),
    SHARE_CONTENT_CURRICULUM_FOR_BATCH(AsyncQueueName.DEFAULT_QUEUE);


    private AsyncQueueName queue;

    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }

    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
