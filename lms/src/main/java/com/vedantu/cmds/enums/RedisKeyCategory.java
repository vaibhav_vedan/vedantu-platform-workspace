package com.vedantu.cmds.enums;

public enum RedisKeyCategory {
    AMPLIFY_EVENTS,
    FIREBASE_EVENTS,
    DEX,
    USER_LEVEL_DOUBT,
    GLOBAL_LEVEL_DOUBT,
    BANNER,
    ACTIVE_TABS,
    APP_RATING_AND_CHAT,
    PAID_USER,
    STRIP
}
