package com.vedantu.cmds.enums;

public enum Scope {

	UNKNOWN, PUBLIC, PRIVATE, ORG, LIBRARY;

	public static Scope valueOfKey(String name) {
		Scope scope = UNKNOWN;
		try {
			scope = valueOf(name);
		} catch (Throwable t) {
		}
		return scope;
	}
}
