package com.vedantu.cmds.enums;

public enum ReportType {

	INCORRECT_QUESTION,INCORRECT_ANSWER,INCORRECT_SOLUTION,INCOMPLETE_SOLUTION,OTHERS;

}
