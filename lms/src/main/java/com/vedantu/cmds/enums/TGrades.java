package com.vedantu.cmds.enums;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public enum TGrades {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7),
    EIGHT(8),
    NINE(9),
    TEN(10),
    ELEVEN(11),
    TWELVE(12),
    THIRTEEN(13);

    private int value;
    private static Map map = new HashMap<>();

    private TGrades(int value) {
        this.value = value;
    }

    static {
        for (TGrades tGrade : TGrades.values()) {
            map.put(tGrade.value, tGrade);
        }
    }

    public static TGrades valueOf(int tGrade) {
        return (TGrades) map.get(tGrade);
    }

    public int getValue() {
        return value;
    }

    public static Set<String> getGradeValues() {
        Set<String> gradeValues = new HashSet<>();
        for (TGrades tGrade : TGrades.values()) {
            gradeValues.add(Integer.toString(tGrade.value));
        }
        return gradeValues;
    }

}
