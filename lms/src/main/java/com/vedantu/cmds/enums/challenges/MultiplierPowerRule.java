package com.vedantu.cmds.enums.challenges;

public enum MultiplierPowerRule {
//    TOP_LEADER_BOARD(
//        5, 1, MultiplierPowerValidityType.CHALLENGE, MultiplierPowerType.TRIPLE) {
//        @Override
//        public MultiplierPower allowatMulitplierPower(String userId, String challengeId) {
//            DBObject query = new BasicDBObject(ConstantsGlobal.CHALLENGE_ID, challengeId);
//            DBCursor cursor = MongoDBCollection.getMongoDBResult(query,
//                    ChallengeLeaderBoard.class, MongoDBCollection.NO_START, 1,
//                    ConstantsGlobal.RANKER, MongoDBCollection.ORDER_ASC);
//            MultiplierPower multiplierPower = null;
//            if (cursor != null && cursor.size() > 0) {
//                ChallengeLeaderBoard leaderBoard = null;
//                while (cursor.hasNext()) {
//                    DBObject result = cursor.next();
//                    leaderBoard = ObjectMapperUtil.convertToMongoModel(result,
//                            ChallengeLeaderBoard.class);
//                    break;
//                }
//                if (StringUtils.equals(userId, leaderBoard.userId)) {
//                    multiplierPower = new MultiplierPower(userId, this.getPower(),
//                            this.getValidFor(), this.getValidityType(), new SrcEntity(
//                                    CMDSEntityType.CHALLENGE, challengeId), this);
//                }
//            }
//            return multiplierPower;
//        }
//    },
//    SOLVE_THREE_CHALLENGE_CONTINUOUSLY_WITH_NO_HINT(
//        5, 3, MultiplierPowerValidityType.DAYS, MultiplierPowerType.DOUBLE) {
//        @Override
//        public MultiplierPower allowatMulitplierPower(String userId, String challengeId) {
//            int continousCorrectCount = getContinousZeroHintSuccessChallengeTakenCount(
//                    userId, this.getMinZeroHintSuccessCount());
//            Logger.log4j.info("in last [" + this.getMinZeroHintSuccessCount()
//                    + "] corectly answered challenge count with no hints : "
//                    + continousCorrectCount);
//            if (continousCorrectCount == this.getMinZeroHintSuccessCount()) {
//                return new MultiplierPower(userId, this.getPower(), this.getValidFor(),
//                        this.getValidityType(), new SrcEntity(CMDSEntityType.CHALLENGE,
//                                challengeId), this);
//            }
//            return null;
//        }
//    },
//    SOLVE_FIVE_CHALLENGE_CONTINUOUSLY_WITH_NO_HINT(
//        5, 5, MultiplierPowerValidityType.DAYS, MultiplierPowerType.TRIPLE) {
//        @Override
//        public MultiplierPower allowatMulitplierPower(String userId, String challengeId) {
//            int continousCorrectCount = getContinousZeroHintSuccessChallengeTakenCount(
//                    userId, this.getMinZeroHintSuccessCount());
//            Logger.log4j.info("in last [" + this.getMinZeroHintSuccessCount()
//                    + "] corectly answered challenge count with no hints : "
//                    + continousCorrectCount);
//            if (continousCorrectCount == this.getMinZeroHintSuccessCount()) {
//                return new MultiplierPower(userId, this.getPower(), this.getValidFor(),
//                        this.getValidityType(), new SrcEntity(CMDSEntityType.CHALLENGE,
//                                challengeId), this);
//            }
//            return null;
//        }
//    };
//
//    private int validFor;
//    private int minZeroHintSuccessCount;
//    private MultiplierPowerValidityType validityType;
//    private MultiplierPowerType power;
//
//    private MultiplierPowerRule(int validFor, int minZeroHintSuccessCount,
//            MultiplierPowerValidityType validityType, MultiplierPowerType power) {
//        this.validFor = validFor;
//        this.minZeroHintSuccessCount = minZeroHintSuccessCount;
//        this.validityType = validityType;
//        this.power = power;
//    }
//
//    public int getValidFor() {
//        return validFor;
//    }
//
//    public MultiplierPowerValidityType getValidityType() {
//        return validityType;
//    }
//
//    public MultiplierPowerType getPower() {
//        return power;
//    }
//
//    public int getMinZeroHintSuccessCount() {
//        return minZeroHintSuccessCount;
//    }
//
//    private static int getContinousZeroHintSuccessChallengeTakenCount(String userId,
//            int size) {
//        DBObject query = new BasicDBObject(ConstantsGlobal.USER_ID, userId);
//        query.put(ConstantsGlobal.PROCESSED, true);
//        DBCursor cursor = MongoDBCollection.getMongoDBResult(query, MongoDBCollection
//                .getFieldsDBObject(
//                        Arrays.asList(ConstantsGlobal.HINT, ConstantsGlobal.SUCCESS),
//                        MongoDBCollection.INCLUDE_FIELD), ChallengeAttempt.class,
//                MongoDBCollection.NO_START, size, ConstantsGlobal.TIME_CREATED,
//                MongoDBCollection.ORDER_DESC);
//        int continousCorrectCount = 0;
//        if (cursor != null && cursor.size() > 0) {
//            List<ChallengeTaken> challTakens = new ArrayList<ChallengeTaken>();
//            while (cursor.hasNext()) {
//                ChallengeAttempt challengeTaken = ObjectMapperUtil.convertToMongoModel(
//                        cursor.next(), ChallengeAttempt.class);
//                challTakens.add(challengeTaken);
//            }
//            for (ChallengeAttempt challengeTaken : challTakens) {
//                if (challengeTaken.hint == 0 && challengeTaken.success) {
//                    continousCorrectCount++;
//                }
//            }
//        }
//        return continousCorrectCount;
//    }
//
//    public abstract MultiplierPower allowatMulitplierPower(String userId,
//            String challengeId);

}
