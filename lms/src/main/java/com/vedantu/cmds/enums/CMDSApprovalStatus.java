package com.vedantu.cmds.enums;

public enum CMDSApprovalStatus {
    ARCHIVED, // Only used for one time when overall duplicates were found
    UNIQUE, DUPLICATE, // Use for CMDSQuestions
    MAKER_APPROVED, CHECKER_APPROVED, // Use for CMDSQuestionSet and CMDSTest
    TEMPORARY, REJECTED // Use for all and will be deleted by cron if needed
}
