package com.vedantu.cmds.enums.challenges;

public enum ChallengeStatus {
    DRAFT, ACTIVE, ENDED;
}
