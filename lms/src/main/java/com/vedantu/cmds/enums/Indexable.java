package com.vedantu.cmds.enums;

import org.apache.commons.lang.StringUtils;

public enum  Indexable {
    YES, NO;

    public static Indexable valueOfKey(String value) {
        Indexable indexable = NO;
        for (Indexable ind : values()) {
            if (StringUtils.equalsIgnoreCase(value, ind.name())) {
                indexable = ind;
                break;
            }
        }
        return indexable;
    }
}
