package com.vedantu.cmds.enums;

/**
 * Created by somil on 22/03/17.
 */
public enum QuestionAttemptInfo {
    ATTEMPTED, NOT_ATTEMPTED
}
