/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.entities.AbstractCMDSEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "Clan")
public class Clan extends AbstractMongoStringIdEntity {

    private String title;
    private String tagLine;
    private String event;
    private List<String> allowedCategories;
    private CloudStorageEntity clanPic;
    @Indexed(unique = true,background = true)
    private String shareRefCode;
    private Long leader;
    private String shareLink;
    private int memberCount=1;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTagLine() {
        return tagLine;
    }

    public void setTagLine(String tagLine) {
        this.tagLine = tagLine;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public List<String> getAllowedCategories() {
        return allowedCategories;
    }

    public void setAllowedCategories(List<String> allowedCategories) {
        this.allowedCategories = allowedCategories;
    }

    public CloudStorageEntity getClanPic() {
        return clanPic;
    }

    public void setClanPic(CloudStorageEntity clanPic) {
        this.clanPic = clanPic;
    }

	public String getShareRefCode() {
		return shareRefCode;
	}

	public void setShareRefCode(String shareRefCode) {
		this.shareRefCode = shareRefCode;
	}

	public Long getLeader() {
		return leader;
	}

	public void setLeader(Long leader) {
		this.leader = leader;
	}

	public String getShareLink() {
		return shareLink;
	}

	public void setShareLink(String shareLink) {
		this.shareLink = shareLink;
	}

	 public int getMemberCount() {
		return memberCount;
	}

	public void setMemberCount(int memberCount) {
		this.memberCount = memberCount;
	}

	public static class Constants extends AbstractCMDSEntity.Constants {

	        public static final String SHARE_CODE = "shareRefCode";
	        public static final String ALLOWED_CATEGORIES = "allowedCategories";
	    }
}
