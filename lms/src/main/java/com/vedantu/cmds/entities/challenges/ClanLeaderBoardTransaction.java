/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.enums.challenges.ClanPointsContextType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ajith
 */
@Entity
@Table(name = "ClanLeaderBoardTransaction", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"clanLeaderBoardId", "contextType", "contextId"})})
public class ClanLeaderBoardTransaction extends AbstractSqlLongIdEntity implements Serializable {

    private String clanId;//just for ref
    private Long clanLeaderBoardId;//mapped to userLeaderBoard - default-overall
    @Enumerated(EnumType.STRING)
    private ClanPointsContextType contextType;
    private String contextId;//for CHALLENGE_BURST it is challengeId,
    //for MUSCLE_POINTS it is picchallengeId, for PICTURE_CHALLENGE_BURST it is picchallengeId
//    @Enumerated(EnumType.STRING)
    private String category;
    private int pointsAdded;
    private long closingPoints;
    private int burstChallengePointsAdded;
    private long closingBurstChallengePoints;
    private int musclePointsAdded;
    private long closingMusclePointsAdded;
    private int pictureChallengePointsAdded;
    private long closingPictureChallengePoints;

    public ClanLeaderBoardTransaction(String clanId, Long clanLeaderBoardId,
			ClanPointsContextType contextType, String contextId, String category, int pointsAdded,
			long closingPoints, int burstChallengePointsAdded, long closingBurstChallengePoints, int musclePointsAdded,
			long closingMusclePointsAdded, int pictureChallengePointsAdded, long closingPictureChallengePoints) {

    	this.clanId = clanId;
		this.clanLeaderBoardId = clanLeaderBoardId;
		this.contextType = contextType;
		this.contextId = contextId;
		this.category = category;
		this.pointsAdded = pointsAdded;
		this.closingPoints = closingPoints;
		this.burstChallengePointsAdded = burstChallengePointsAdded;
		this.closingBurstChallengePoints = closingBurstChallengePoints;
		this.musclePointsAdded = musclePointsAdded;
		this.closingMusclePointsAdded = closingMusclePointsAdded;
		this.pictureChallengePointsAdded = pictureChallengePointsAdded;
		this.closingPictureChallengePoints = closingPictureChallengePoints;
	}

	public ClanLeaderBoardTransaction(String clanId, Long clanLeaderBoardId,
			ClanPointsContextType contextType, String contextId, String category, int pointsAdded) {
				this.clanId = clanId;
		this.clanLeaderBoardId = clanLeaderBoardId;
		this.contextType = contextType;
		this.contextId = contextId;
		this.category = category;
		this.pointsAdded = pointsAdded;

	}

	public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public Long getClanLeaderBoardId() {
        return clanLeaderBoardId;
    }

    public void setClanLeaderBoardId(Long clanLeaderBoardId) {
        this.clanLeaderBoardId = clanLeaderBoardId;
    }

    public ClanPointsContextType getContextType() {
        return contextType;
    }

    public void setContextType(ClanPointsContextType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public int getPointsAdded() {
        return pointsAdded;
    }

    public void setPointsAdded(int pointsAdded) {
        this.pointsAdded = pointsAdded;
    }

    public long getClosingPoints() {
        return closingPoints;
    }

    public void setClosingPoints(long closingPoints) {
        this.closingPoints = closingPoints;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getBurstChallengePointsAdded() {
        return burstChallengePointsAdded;
    }

    public void setBurstChallengePointsAdded(int burstChallengePointsAdded) {
        this.burstChallengePointsAdded = burstChallengePointsAdded;
    }

    public long getClosingBurstChallengePoints() {
        return closingBurstChallengePoints;
    }

    public void setClosingBurstChallengePoints(long closingBurstChallengePoints) {
        this.closingBurstChallengePoints = closingBurstChallengePoints;
    }

    public int getMusclePointsAdded() {
        return musclePointsAdded;
    }

    public void setMusclePointsAdded(int musclePointsAdded) {
        this.musclePointsAdded = musclePointsAdded;
    }

    public long getClosingMusclePointsAdded() {
        return closingMusclePointsAdded;
    }

    public void setClosingMusclePointsAdded(long closingMusclePointsAdded) {
        this.closingMusclePointsAdded = closingMusclePointsAdded;
    }

    public int getPictureChallengePointsAdded() {
        return pictureChallengePointsAdded;
    }

    public void setPictureChallengePointsAdded(int pictureChallengePointsAdded) {
        this.pictureChallengePointsAdded = pictureChallengePointsAdded;
    }

    public long getClosingPictureChallengePoints() {
        return closingPictureChallengePoints;
    }

    public void setClosingPictureChallengePoints(long closingPictureChallengePoints) {
        this.closingPictureChallengePoints = closingPictureChallengePoints;
    }

}
