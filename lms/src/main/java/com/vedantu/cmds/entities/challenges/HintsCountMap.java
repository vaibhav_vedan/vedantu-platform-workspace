/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author ajith
 */
@Entity
@Table(name = "HintsCountMap")
public class HintsCountMap extends AbstractSqlLongIdEntity implements Serializable {

    @ManyToOne
    private UserChallengeStats userChallengeStats;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int takenCount;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int hintTakenIndex;

    public HintsCountMap() {
    }

    public HintsCountMap(int hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
        this.takenCount = 1;
    }
    
    public void setParent(UserChallengeStats userChallengeStats) {
        this.userChallengeStats = userChallengeStats;
    }

    public int getTakenCount() {
        return takenCount;
    }

    public void setTakenCount(int takenCount) {
        this.takenCount = takenCount;
    }

    public int getHintTakenIndex() {
        return hintTakenIndex;
    }

    public void setHintTakenIndex(int hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
    }

}
