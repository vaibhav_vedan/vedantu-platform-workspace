package com.vedantu.cmds.entities;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.enums.TestMode;
import com.vedantu.cmds.pojo.CMDSTestState;
import com.vedantu.cmds.pojo.CustomSectionEvaluationRule;
import com.vedantu.cmds.pojo.PerformanceReportSection;
import com.vedantu.cmds.pojo.SectionWiseInstruction;
import com.vedantu.cmds.pojo.TestMetadata;
import com.vedantu.cmds.pojo.TypeOfObjectiveTest;
import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.CollectionUtils;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import static com.vedantu.platform.enums.SocialContextType.CMDSTEST;

@Data
@NoArgsConstructor
@Document(collection = "CMDSTest")
public class CMDSTest extends AbstractCMDSEntity implements HomeFeedCardData {

    private CMDSTestState testState;
    private int versionNo = 1;
    private String questionSetId;
    private SocialContextType socialContextType = CMDSTEST;
    public String description;
    public int qusCount;
    public Long duration;
    public Float totalMarks = 0f;
    public ContentInfoType contentInfoType;
    @Indexed(background = true)
    private boolean visibleInApp = false;
    public List<TestMetadata> metadata;
    public EnumBasket.TestType type;
    public EnumBasket.TestTagType testTag;
    public TestMode mode;
    public String code; // unique code for test inside an organization(if applicable)
    public long attempts;
    public boolean published;
    public List<String> childrenIds; // if this is testGroup then members of this group
    public String parentId; // if this is part of a testGroup (testGroupId)
    public TestResultVisibility resultVisibility;
    public String resultVisibilityMessage;
    private Long doNotShowResultsTill;
    private boolean hardStop=false;
    public Long minStartTime;
    @Indexed(background = true)
    public Long maxStartTime;
    public boolean reattemptAllowed = false;
    public boolean globalRankRequired = false;
    private SignUpRestrictionLevel signupHook;
    private List<String> instructions;
    private List<SectionWiseInstruction> sectionWiseInstructions;
    private List<CustomSectionEvaluationRule> customSectionEvaluationRules;
    private String marketingRedirectUrl; // Marketing related dummy fields for now
    private Long expiryDate;// for public subjective test
    private Long expiryDays;// for private subjective test
    private TypeOfObjectiveTest typeOfObjectiveTest; // for objective test only
    private CMDSApprovalStatus approvalStatus=CMDSApprovalStatus.MAKER_APPROVED;

    private Long assignedChecker;
    private List<PerformanceReportSection> analytics; //for competitive only.
    private Set<String> analyticsTags; //for competitive only.


    public void populateTest(CMDSTest other) {
        this.testState = other.testState;
        this.versionNo = other.versionNo;
        this.questionSetId = other.questionSetId;
        this.socialContextType = other.socialContextType;
        this.description = other.description;
        this.qusCount = other.qusCount;
        this.duration = other.duration;
        this.totalMarks = other.totalMarks;
        this.contentInfoType = other.contentInfoType;
        this.visibleInApp = other.visibleInApp;
        this.metadata = other.metadata;
        this.type = other.type;
        this.testTag = other.testTag;
        this.mode = other.mode;
        this.code = other.code;
        this.attempts = other.attempts;
        this.published = other.published;
        this.childrenIds = other.childrenIds;
        this.parentId = other.parentId;
        this.resultVisibility = other.resultVisibility;
        this.resultVisibilityMessage = other.resultVisibilityMessage;
        this.doNotShowResultsTill = other.doNotShowResultsTill;
        this.hardStop = other.hardStop;
        this.minStartTime = other.minStartTime;
        this.maxStartTime = other.maxStartTime;
        this.reattemptAllowed = other.reattemptAllowed;
        this.globalRankRequired = other.globalRankRequired;
        this.signupHook = other.signupHook;
        this.instructions = other.instructions;
        this.sectionWiseInstructions = other.sectionWiseInstructions;
        this.customSectionEvaluationRules = other.customSectionEvaluationRules;
        this.marketingRedirectUrl = other.marketingRedirectUrl;
        this.expiryDate = other.expiryDate;
        this.expiryDays = other.expiryDays;
    }

    public List<String> collectErrors() {
        List<String> errors = new ArrayList<>();
        if (this.testState == null) {
            errors.add("state");
        }

        return errors;
    }

    public float calculateTotalMarks() {
        float totalMarks = 0;
        if (!CollectionUtils.isEmpty(this.metadata)) {
            for (TestMetadata testMetadata : this.metadata) {
                for (CMDSTestQuestion cmdsTestQuestion : testMetadata.getQuestions()) {
                    totalMarks += cmdsTestQuestion.getMarks().getPositive();
                }
            }
        }

        return totalMarks;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String _ID = "_id";
        public static final String TEST_STATE = "testState";
        public static final String VERSION_NUMBER = "versionNo";
        public static final String QUESTION_SET_ID = "questionSetId";
        public static final String CODE = "code";
        public static final String TEST_TAG = "testTag";
        public static final String MAX_START_TIME = "maxStartTime";
        public static final String MIN_START_TIME = "minStartTime";
        public static final String CONTENT_INFO_TYPE = "contentInfoType";
        public static final String QUESTION_ID = "metadata.questions.questionId";
        public static final String VISIBLE_IN_APP = "visibleInApp";
        public static final String METADATA_DURATION  = "metadata.duration";
        public static final String METADATA  = "metadata";
        public static final String DURATION  = "duration";
        public static final String QUS_COUNT  = "qusCount";
        public static final String APPROVAL_STATUS = "approvalStatus";
        public static final String ASSIGNED_CHECKER = "assignedChecker";
        public static final String TOTAL_MARKS = "totalMarks";
        public static final String REATTEMPT_ALLOWED = "reattemptAllowed";
        public static final String ANALYTICS = "analytics";
    }

}
