package com.vedantu.cmds.entities;

import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;

@Data
@NoArgsConstructor
@CompoundIndexes({
        @CompoundIndex(useGeneratedName = true,def = "{'teacherId': 1, 'contentInfoType': 1, 'contentState': -1}",unique = true,background = true)
})
public class TeacherContentDashboardInfo extends AbstractMongoStringIdEntity {
    private String teacherId;
    private ContentInfoType contentInfoType;
    private ContentState contentState;
    private Long count;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String TEACHER_ID = "teacherId";
        public static final String CONTENT_INFO_TYPE = "contentInfoType";
        public static final String CONTENT_STATE = "contentState";
        public static final String COUNT = "count";
    }

}
