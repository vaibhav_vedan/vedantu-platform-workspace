package com.vedantu.cmds.entities;

import com.vedantu.cmds.enums.QuestionAttemptContext;
import com.vedantu.lms.cmds.enums.AttemptStateOfQuestion;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by somil on 23/03/17.
 */
@Data
@Document(collection = "QuestionAttempt")
public class QuestionAttempt extends AbstractMongoStringIdEntity {

    private String userId;
    private Integer duration;
    private String questionId;
    private List<String> answerGiven;
    private QuestionAttemptContext contextType;
    private String contextId;
    @Indexed(background = true)
    private String attemptId;//testattemptId|challengeTakenId
    private Boolean correct;
    private AttemptStateOfQuestion attemptStateOfQuestion;

    public QuestionAttempt() {
        super();
    }

    public QuestionAttempt(String userId, Integer duration, 
            QuestionAttemptContext contextType,
            String contextId, String attemptId, 
            String questionId, List<String> answerGiven) {
        this.userId = userId;
        this.duration = duration;
        this.contextType=contextType;
        this.contextId = contextId;
        this.attemptId = attemptId;
        this.questionId = questionId;
        this.answerGiven =
                Optional.ofNullable(answerGiven)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .filter(QuestionAttempt::notAnEmptyString)
                        .collect(Collectors.toList());
    }
    public QuestionAttempt(String userId, Integer duration,
                           QuestionAttemptContext contextType,
                           String contextId, String attemptId,
                           String questionId, List<String> answerGiven,AttemptStateOfQuestion attemptStateOfQuestion) {
        this.userId = userId;
        this.duration = duration;
        this.contextType=contextType;
        this.contextId = contextId;
        this.attemptId = attemptId;
        this.questionId = questionId;
        this.attemptStateOfQuestion=attemptStateOfQuestion;
        this.answerGiven =
                Optional.ofNullable(answerGiven)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .filter(QuestionAttempt::notAnEmptyString)
                        .collect(Collectors.toList());
    }

    private static boolean notAnEmptyString(String s){
        String emptyString="";
        return !emptyString.equals(s);
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String QUESTION_ID = "questionId";
        public static final String ATTEMPT_ID = "attemptId";
        public static final String CONTEXT_ID = "contextId";
        public static final String ANSWER_GIVEN = "answerGiven";
    }
    
}
