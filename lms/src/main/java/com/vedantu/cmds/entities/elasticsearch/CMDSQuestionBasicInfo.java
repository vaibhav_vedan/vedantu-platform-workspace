package com.vedantu.cmds.entities.elasticsearch;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.ArrayUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@NoArgsConstructor
@Data
public class CMDSQuestionBasicInfo {

    private String questionText;
    private List<String> options;
    private QuestionType type;

    public CMDSQuestionBasicInfo(CMDSQuestion cmdsQuestion){

        this.questionText=refactorQuestionText(cmdsQuestion);
        this.type=cmdsQuestion.getType();

        if(ArrayUtils.isNotEmpty(cmdsQuestion.getSolutionInfo().getOptionBody().getNewOptions())) {
            this.options =refactorOptionList(cmdsQuestion);
        }
    }

    private List<String> refactorOptionList(CMDSQuestion cmdsQuestion) {

        Set<String> searchOptionImages=
                Optional.ofNullable(cmdsQuestion.getSolutionInfo().getOptionBody().getUuidImages())
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(CMDSImageDetails::getFileName)
                        .collect(Collectors.toSet());

        List<String> refactoredOptionTextList =
                cmdsQuestion
                        .getSolutionInfo()
                        .getOptionBody()
                        .getNewOptions()
                        .stream()
                        .map(option -> refactorOption(searchOptionImages,option))
                        .collect(Collectors.toList());

        return refactoredOptionTextList;
    }

    private String refactorOption(Set<String>searchOptionImages , String refactoredOptionText){

        refactoredOptionText=refactoredOptionText.replaceAll("\\\\","");
        if(ArrayUtils.isNotEmpty(searchOptionImages)){
            for(String questionImageText:searchOptionImages){
                questionImageText="<img  src=\""+questionImageText+"\"/>";
                refactoredOptionText=refactoredOptionText.replaceAll(questionImageText," Image");
            }
        }
        return refactoredOptionText;
    }

    private String refactorQuestionText(CMDSQuestion cmdsQuestion) {

        String refactoredQuestionText=cmdsQuestion.getQuestionBody().getNewText().replaceAll("\\\\", "");
        Set<String> questionTextImages=
                Optional.ofNullable(cmdsQuestion.getQuestionBody().getUuidImages())
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(CMDSImageDetails::getFileName)
                        .collect(Collectors.toSet());

        if(ArrayUtils.isNotEmpty(questionTextImages)){
            for(String questionImageText:questionTextImages){
                questionImageText="<img  src=\""+questionImageText+"\"/>";
                refactoredQuestionText=refactoredQuestionText.replaceAll(questionImageText," Image");
            }
        }
        return refactoredQuestionText;
    }

    public static class Constants {

        public static final String QUESTION_TEXT = "questionText";
        public static final String OPTIONS = "options";
        public static final String TYPE = "type";
    }
}
