package com.vedantu.cmds.entities;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Document(collection = "ClassroomChat")
public class ClassroomChat extends AbstractMongoStringIdEntity {

    private String videoId;
    private String title;
    private Long startTime;
    private Long endTime;
    private Integer duration;
    private String grade;
    private List<Long> membersList;

    public ClassroomChat() {
        super();
    }

    /**
     * @return the videoId
     */
    public String getVideoId() {
        return videoId;
    }

    /**
     * @param entityid the videoId to set
     */
    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public List<Long> getMembersList() {
		return membersList;
	}

	public void setMembersList(List<Long> membersList) {
		this.membersList = membersList;
	}

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }


    @Override
    public String toString() {
        return "ClassroomChat{" + "videoId=" + videoId + ", title=" + title + ", startTime=" + startTime + ", endTime=" + endTime + ", duration=" + duration + '}';
    }

}
