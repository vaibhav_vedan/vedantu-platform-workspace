package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.entities.AbstractCMDSEntity;
import com.vedantu.cmds.enums.challenges.MultiplierPowerRule;
import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.cmds.enums.challenges.MultiplierPowerValidityType;
import com.vedantu.cmds.pojo.SrcEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "MultiplierPower")
public class MultiplierPower extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    public Long userId;
    public MultiplierPowerType type;
    public long validFor;
    public int useCount;
    public MultiplierPowerValidityType validityType;
    public SrcEntity src;
    public MultiplierPowerRule powerRule;

    public MultiplierPower() {
    }

    public MultiplierPower(Long userId, MultiplierPowerType type, long validFor,
            MultiplierPowerValidityType validityType, SrcEntity src,
            MultiplierPowerRule powerRule) {
        super();
        this.userId = userId;
        this.type = type;
        this.validFor = validFor;
        this.validityType = validityType;
        this.src = src;
        this.powerRule = powerRule;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public MultiplierPowerType getType() {
        return type;
    }

    public void setType(MultiplierPowerType type) {
        this.type = type;
    }

    public long getValidFor() {
        return validFor;
    }

    public void setValidFor(long validFor) {
        this.validFor = validFor;
    }

    public int getUseCount() {
        return useCount;
    }

    public void setUseCount(int useCount) {
        this.useCount = useCount;
    }

    public MultiplierPowerValidityType getValidityType() {
        return validityType;
    }

    public void setValidityType(MultiplierPowerValidityType validityType) {
        this.validityType = validityType;
    }

    public SrcEntity getSrc() {
        return src;
    }

    public void setSrc(SrcEntity src) {
        this.src = src;
    }

    public MultiplierPowerRule getPowerRule() {
        return powerRule;
    }

    public void setPowerRule(MultiplierPowerRule powerRule) {
        this.powerRule = powerRule;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String USERID = "userId";
        public static final String TYPE = "type";
    }

}
