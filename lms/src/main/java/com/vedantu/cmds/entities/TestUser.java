package com.vedantu.cmds.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;

/**
 * Created by somil on 04/04/17.
 */
@Document(collection = "TestUser")
public class TestUser extends AbstractMongoStringIdEntity {

    private String name;
    private String grade;
    private String email;
    private String contactNumber;
    private String phoneCode;
    private String registeredUserId;
    private Map<String, String> userData;

    public TestUser() {
        super();
    }

    public TestUser(String name, String grade, String email, String contactNumber, String phoneCode, Map<String, String> userData) {
        this.name = name;
        this.grade = grade;
        this.email = email;
        this.contactNumber = contactNumber;
        this.phoneCode = phoneCode;
        this.userData = userData;
    }

    public String getRegisteredUserId() {
        return registeredUserId;
    }

    public void setRegisteredUserId(String registeredUserId) {
        this.registeredUserId = registeredUserId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public Map<String, String> getUserData() {
        return userData;
    }

    public void setUserData(Map<String, String> userData) {
        this.userData = userData;
    }


}

