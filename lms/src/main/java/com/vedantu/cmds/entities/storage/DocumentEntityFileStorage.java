package com.vedantu.cmds.entities.storage;

import java.io.File;
import java.util.Map;

import com.vedantu.cmds.entities.AbstractEntityFileStorage;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.enums.ImageSize;
import com.vedantu.cmds.enums.MediaType;
import com.vedantu.cmds.pojo.FileData;
import com.vedantu.exception.VException;

public class DocumentEntityFileStorage extends AbstractEntityFileStorage {

	public DocumentEntityFileStorage() {
		super(CMDSEntityType.DOCUMENT);
	}

	@Override
	public StorageResult storeImage(final String uid, final File file, final FileCategory fileCategory,
			final ImageSize imageSize, final Map<String, String> tags) throws VException {
		return super.storeImage(uid, file, fileCategory, imageSize, tags);
	}

	public StorageResult store(final String uid, final File file, final FileCategory fileCategory,
			final Map<String, String> tags) throws VException {
		// Commented By Shankhoneer: Even if image is uploaded as a doc it
		// should get moved to OS as a doc not image

		// ImageFilter imageFilter = new ImageFilter();
		// if( imageFilter.accept(file) ){
		// return super.storeImage(uid, file, fileCategory, ImageSize.ORIGINAL,
		// tags);
		// }

		return super.store(uid, file, MediaType.DOC, fileCategory, tags);
	}

	@Override
	public FileData getData(String entityType, String mediaType, String fileName) throws VException {
		return super.getData(entityType, mediaType, fileName);
	}
}
