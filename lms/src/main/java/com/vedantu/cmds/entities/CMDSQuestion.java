package com.vedantu.cmds.entities;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.vedantu.cmds.enums.*;
import com.vedantu.cmds.pojo.ParseQuestionMetadata;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.SolutionInfo;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.cmds.pojo.metadata.MCQsolutionInfo;
import com.vedantu.cmds.utils.CMDSCommonUtils;
import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.interfaces.ILatexProcessor;
import com.vedantu.lms.cmds.pojo.AnswerChangeRecord;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.ArrayUtils;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Document(collection = "CMDSQuestion")
@NoArgsConstructor
@Data
public class CMDSQuestion extends AbstractCMDSEntity implements ILatexProcessor, HomeFeedCardData {

    @JsonDeserialize(as = MCQsolutionInfo.class)
    private SolutionInfo solutionInfo;
    private RichTextFormat questionBody;
    private QuestionType type;
    private String questionSetName;
    private String questionSetId;
    private Marks marks;
    private List<HintFormat> refHints;
    @Indexed(background = true)
    private Indexable indexable;
    private String changedFromId;
    private List<AnswerChangeRecord> answerChangeRecords = new ArrayList<>();

    private Difficulty difficulty= Difficulty.getDefaultValue();


    // New Question Tagging related
    private Integer difficultyValue;
    private CognitiveLevel cognitiveLevel;
    private List<String> source;
    @Indexed(background = true)
    private Set<String> mainTags;

    private Set<String> analysisTags;

    //book related
    @Setter(AccessLevel.NONE)
    private String book;
    @Setter(AccessLevel.NONE)
    private String edition;
    @Setter(AccessLevel.NONE)
    private String chapter;
    @Setter(AccessLevel.NONE)
    private String exercise;
    @Setter(AccessLevel.NONE)
    private Set<String> pageNos;
    @Indexed(background = true)
    private String chapterNo;
    private int slNoInBook;//used for ordering only
    private String questionNo;

    //social Layer
    private long upVotes;
    private long downVotes;
    private long attempts;

    //slug data for queries
    @Indexed(background = true)
    private String bookSlug;
    @Indexed(background = true)
    private String editionSlug;
    @Indexed(background = true)
    private String chapterSlug;
    @Indexed(background = true)
    private String exerciseSlug;
    private Set<String> targetGrade;
    @Indexed(background = true)
    private Set<String> pageNosSlugs;
    @Indexed(background = true)
    private Integer used = 0;

    // question duplicate removal variables
    private Boolean uploadedToES=Boolean.FALSE;
    private String isACopyOf;
    @Indexed(background = true)
    private CMDSApprovalStatus approvalStatus= CMDSApprovalStatus.TEMPORARY;
    private CMDSApprovalStatus overallApprovalStatus= CMDSApprovalStatus.TEMPORARY;
    private Long assignedChecker;
    private Set<String> mappedToQuestionSetIds;

    private MakePageLive makePageLive;
    private String video;

    // There's something called subject also, as a part of AbstractCMDSEntity, which is used to store
    // the names of section where the question will be, adding this field so that things could be done
    // with minimum confusion. Also, don't reomve this comment or else future guys might get confused
    private List<String> subjects;

    public CMDSQuestion(RichTextFormat q, QuestionType type, SolutionInfo solutionInfo, String userId,
            String questionSetName, String assignedTo, VedantuRecordState recordState, ParseQuestionMetadata metadata) {

        super(userId);
        this.questionBody = q;
        this.type = type;
        this.solutionInfo = solutionInfo;
        this.questionSetName = questionSetName;
        this.recordState = recordState;
        this.questionSetId = "";
        if (metadata != null) {
            this.difficulty = metadata.difficulty;
            this.indexable = metadata.indexable;
            super.setSubject(metadata.getSubject());
            super.setTopics(metadata.getTopics());
            super.setSubtopics(metadata.getSubTopics());
            super.setTags(metadata.getTags());
            super.setGrades(metadata.getGrades());
            super.setTargets(metadata.getTargets());
            this.marks = metadata.getMarks();
            this.book = metadata.getBook();
            this.edition = metadata.getEdition();
            this.chapter = metadata.getChapter();
            this.chapterNo = metadata.getChapterNo();
            this.exercise = metadata.getExercise();
            this.pageNos = metadata.getPageNos();
            this.slNoInBook = metadata.getSlNoInBook();

            this.questionNo = metadata.getQuestionNo();

            this.targetGrade = metadata.getTargetgrade();
            this.source = metadata.getSource();
            this.difficultyValue = metadata.getDifficultyValue();
            this.analysisTags = metadata.getAnalysisTags();
            this.cognitiveLevel = metadata.getCognitiveLevel();

            this.makePageLive = metadata.getMakePageLive();
            this.video = metadata.getVideo();

            //adding slug data
            this.bookSlug = CMDSCommonUtils.makeSlug(metadata.getBook());
            this.editionSlug = CMDSCommonUtils.makeSlug(metadata.getEdition());
            this.chapterSlug = CMDSCommonUtils.makeSlug(metadata.getChapter());
            this.exerciseSlug = CMDSCommonUtils.makeSlug(metadata.getExercise());
            this.pageNosSlugs = CMDSCommonUtils.makeSlugs(metadata.getPageNos());
        }

        this.refHints = new ArrayList<>();
    }

    @Override
    public void addHook() {

        if (solutionInfo != null) {
            solutionInfo.addHook();
        }
        if (questionBody != null) {
            questionBody.addHook();
        }
        if (ArrayUtils.isNotEmpty(refHints)) {
            for (HintFormat hintFormat : refHints) {
                hintFormat.addHook();
            }
        }
    }

    public void setBook(String book) {
        this.book = book;
        this.bookSlug = CMDSCommonUtils.makeSlug(book);
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
        this.chapterSlug = CMDSCommonUtils.makeSlug(chapter);
    }

    public void setExercise(String exercise) {
        this.exercise = exercise;
        this.exerciseSlug = CMDSCommonUtils.makeSlug(exercise);
    }

    public void setPageNos(Set<String> pageNos) {
        this.pageNos = pageNos;
        this.pageNosSlugs = CMDSCommonUtils.makeSlugs(pageNos);
    }

    public void setEdition(String edition) {
        this.edition = edition;
        this.editionSlug = CMDSCommonUtils.makeSlug(edition);
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String QUESTION_BODY = "questionBody";
        public static final String TYPE = "type";
        public static final String DIFFICULTY = "difficulty";
        public static final String SOLUTION_INFO = "solutionInfo";
        public static final String ATTEMPTS = "attempts";
        public static final String UP_VOTES = "upVotes";
        public static final String DOWN_VOTES = "downVotes";
        public static final String BOOK = "book";
        public static final String EDITION = "edition";
        public static final String CHAPTER = "chapter";
        public static final String CHAPTER_NO = "chapterNo";
        public static final String EXERCISE = "exercise";
        public static final String PAGENOS = "pageNos";
        public static final String BOOK_SLUG = "bookSlug";
        public static final String EDITION_SLUG = "editionSlug";
        public static final String CHAPTER_SLUG = "chapterSlug";
        public static final String EXERCISE_SLUG = "exerciseSlug";
        public static final String PAGENOS_SLUGS = "pageNosSlugs";
        public static final String SLNO_IN_BOOK = "slNoInBook";
        public static final String QUESTION_NO = "questionNo";
        public static final String MAIN_TAGS = "mainTags";
        public static final String SOURCE = "source";
        public static final String DIFFICULTY_VALUE = "difficultyValue";
        public static final String COGNITIVE_LEVEL = "cognitiveLevel";
        public static final String RECORD_STATE = "recordState";
        public static final String USED = "used";
        public static final String INDEXABLE = "indexable";
        public static final String MARKS = "marks";
        public static final String UPLOADED_TO_ES ="uploadedToES";
        public static final String APPROVAL_STATUS ="approvalStatus";
        public static final String OVERALL_APPROVAL_STATUS ="overallApprovalStatus";
        public static final String IS_A_COPY_OF ="isACopyOf";
        public static final String ASSIGNED_CHECKER ="assignedChecker";
        public static final String MAPPED_TO_QUESTION_SET_IDS ="mappedToQuestionSetIds";
        public static final String ANALYSIS_TAGS ="analysisTags";
        public static final String TARGET_GRADE ="targetGrade";
        public static final String SUBJECTS = "subjects";
    }



}
