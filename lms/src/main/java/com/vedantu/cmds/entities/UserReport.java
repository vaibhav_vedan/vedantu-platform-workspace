package com.vedantu.cmds.entities;

import com.vedantu.cmds.enums.ReportEntityType;
import com.vedantu.cmds.enums.ReportStatus;
import com.vedantu.cmds.enums.ReportType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;


public class UserReport  {
    @Indexed(background = true)
    private String userId;
    private String documentId;
    private String documentType;
    private String documentName;
    private ReportType reportType;
    private String reportDesc;
    @Indexed(background = true)
    private Long raisedOn;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public ReportType getReportType() {
        return reportType;
    }

    public void setReportType(ReportType reportType) {
        this.reportType = reportType;
    }

    public String getReportDesc() {
        return reportDesc;
    }

    public void setReportDesc(String reportDesc) {
        this.reportDesc = reportDesc;
    }

    public Long getRaisedOn() {
        return raisedOn;
    }

    public void setRaisedOn(Long raisedOn) {
        this.raisedOn = raisedOn;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentName() {
        return documentName;
    }

    public void setDocumentName(String documentName) {
        this.documentName = documentName;
    }
}
