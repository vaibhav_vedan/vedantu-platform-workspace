
package com.vedantu.cmds.entities.challenges;

import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author ajith
 */
@Entity
@Table(name = "HintsCountMapLeaderBoard")
public class HintsCountMapLeaderBoard extends AbstractSqlLongIdEntity implements Serializable {

    @ManyToOne
    private UserChallengeLeaderBoard userChallengeLeaderBoard;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int takenCount;

    @Column(nullable = false, columnDefinition = "int default 0")
    private int hintTakenIndex;

    public HintsCountMapLeaderBoard() {
    }

    public HintsCountMapLeaderBoard(int hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
        this.takenCount = 1;
    }
    
    public void setParent(UserChallengeLeaderBoard userChallengeLeaderBoard) {
        this.userChallengeLeaderBoard = userChallengeLeaderBoard;
    }

    public int getTakenCount() {
        return takenCount;
    }

    public void setTakenCount(int takenCount) {
        this.takenCount = takenCount;
    }

    public int getHintTakenIndex() {
        return hintTakenIndex;
    }

    public void setHintTakenIndex(int hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
    }

}
