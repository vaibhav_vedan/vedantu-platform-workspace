package com.vedantu.cmds.entities;

import com.vedantu.cmds.pojo.CourseDetails;
import com.vedantu.cmds.request.CurriculumTemplateInsertionRequest;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.TextIndexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@Document(collection = "CurriculumTemplate")
@CompoundIndexes({
    @CompoundIndex(name = "sharedCourseDetails.courseId",def = "{'sharedCourseDetails.courseId': 1}",background = true),
    @CompoundIndex(name = "sharedCourseDetails.batchIds",def = "{'sharedCourseDetails.batchIds': 1}",background = true)
})
public class CurriculumTemplate extends AbstractTargetTopicEntity {

    private String parentId;
    @TextIndexed
    private String title;
    private String rootId;
    private Integer year;
    private Integer childOrder;
    private Long boardId;
    private Set<CourseDetails> sharedCourseDetails=new HashSet<>();

    public CurriculumTemplate(Curriculum curriculum, CurriculumTemplateInsertionRequest request) {

        // Data from the request file to be replicated to all the child nodes
        this.title=request.getTitle();
        this.year=request.getYear();
        this.childOrder=curriculum.getChildOrder();
        this.tGrades=request.getGrades();
        this.tTargets=request.getTargets();
        this.boardId=curriculum.getBoardId();

        // Node specific data to be stored in the same
        if(ArrayUtils.isNotEmpty(curriculum.getTopicTags())) {
            this.mainTags = curriculum.getTopicTags();
        }
    }

    public CurriculumTemplate(Curriculum curriculum, CurriculumTemplate parentCurriculumTemplate) {

        // Data from parent to be replicated to all the children
        this.year= parentCurriculumTemplate.getYear();
        this.tGrades= parentCurriculumTemplate.gettGrades();
        this.tTargets= parentCurriculumTemplate.gettTargets();
        this.sharedCourseDetails= parentCurriculumTemplate.getSharedCourseDetails();
        this.parentId= parentCurriculumTemplate.getId();
        this.rootId= parentCurriculumTemplate.getRootId();

        // Data from the newly made curriculum
        this.title=curriculum.getTitle();
        this.childOrder=curriculum.getChildOrder();
        this.boardId=curriculum.getBoardId();
        if(ArrayUtils.isNotEmpty(curriculum.getTopicTags())){
            this.mainTags=curriculum.getTopicTags();
        }
    }

    public CurriculumTemplate(Node node, CurriculumTemplateInsertionRequest request) {

        // Data from request to be replicated to all the children
        this.year=request.getYear();
        this.tGrades=request.getGrades();
        this.tTargets=request.getTargets();
        this.title=request.getTitle();

        // Data from node specific to the same
        if(ArrayUtils.isNotEmpty(node.getTopicTags())) {
            this.mainTags = node.getTopicTags();
        }
        this.boardId=node.getBoardId();
    }

    public CurriculumTemplate(Node node, CurriculumTemplate parentNode) {
        this.year=parentNode.getYear();
        this.tGrades=parentNode.gettGrades();
        this.tTargets=parentNode.gettTargets();
        this.parentId=parentNode.getId();
        this.rootId=parentNode.getRootId();

        this.title=node.getTitle();
        this.childOrder=node.getChildOrder();
        if(ArrayUtils.isNotEmpty(node.getTopicTags())){
            this.mainTags=node.getTopicTags();
        }
        this.boardId=node.getBoardId();
    }


    public CurriculumTemplate(CurriculumTemplate curriculumTemplate){
        
        // Private variables, so need getters and setters
        this.setId(curriculumTemplate.getId());
        this.setEntityState(curriculumTemplate.getEntityState());

        // Public variables, so can be accessed directly
        this.parentId = curriculumTemplate.parentId;
        this.title = curriculumTemplate.title;
        this.rootId = curriculumTemplate.rootId;
        this.year = curriculumTemplate.year;
        this.childOrder = curriculumTemplate.childOrder;
        this.sharedCourseDetails = curriculumTemplate.sharedCourseDetails;
        this.targetGrades = curriculumTemplate.targetGrades;
        this.tSubjects = curriculumTemplate.tSubjects;
        this.tTopics = curriculumTemplate.tTopics;
        this.inputTopics = curriculumTemplate.inputTopics;
        this.tGrades = curriculumTemplate.tGrades;
        this.tTargets = curriculumTemplate.tTargets;
        this.mainTags = curriculumTemplate.mainTags;
        this.boardId=curriculumTemplate.boardId;
    }

    public CurriculumTemplate insertCourseDetail(CourseDetails courseDetails){
        if(sharedCourseDetails.contains(courseDetails)){
            sharedCourseDetails.remove(courseDetails);
            sharedCourseDetails.add(courseDetails);
        }else{
            sharedCourseDetails.add(courseDetails);
        }
        return this;
    }

    public static class Constants extends AbstractTargetTopicEntity.Constants {
        public static final String PARENT_ID = "parentId";
        public static final String ROOT_ID = "rootId";
        public static final String CHILD_ORDER = "childOrder";
        public static final String TITLE = "title";
        public static final String YEAR = "year";
        public static final String COURSE_ID = "sharedCourseDetails.courseId";
        public static final String BATCH_IDS = "sharedCourseDetails.batchIds";
        public static final String SHARED_COURSE_DETAILS = "sharedCourseDetails";
        public static final String BOARD_ID = "boardId";

    }
}
