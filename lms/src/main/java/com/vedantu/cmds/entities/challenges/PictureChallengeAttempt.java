/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ajith
 */
@Entity
@Table(name = "PictureChallengeAttempt", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"clanId", "pictureChallengeId"})})
public class PictureChallengeAttempt extends AbstractSqlLongIdEntity implements Serializable {

    private String clanId;
    private String pictureChallengeId;
    private String answer;//for SCQ,MCQ join the answers by ,
    public Long userId;//only clan creator
    // startTime will be timeCreated
    public long endTime;
    public long answerTime;
    public boolean success;
    public boolean processed;
    public int basePoints;
    public int totalPoints;//after multiplying with multiplierPower
//    @Enumerated(EnumType.STRING)
    private String category;
    
    public PictureChallengeAttempt(){
    	super();
    }
    
    public PictureChallengeAttempt(String clanId, String pictureChallengeId,
			Long userId, long endTime, String category) {
    	super();
		this.clanId = clanId;
		this.pictureChallengeId = pictureChallengeId;
		this.userId = userId;
		this.endTime = endTime;
		this.category = category;
	}

//    public PictureChallengeAttempt(String clanId,String pictureChallengeId,
//    		Long userId,Long endTime,EventCategory category){
//    	
//    }

	public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public String getPictureChallengeId() {
        return pictureChallengeId;
    }

    public void setPictureChallengeId(String pictureChallengeId) {
        this.pictureChallengeId = pictureChallengeId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(long answerTime) {
        this.answerTime = answerTime;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public int getBasePoints() {
        return basePoints;
    }

    public void setBasePoints(int basePoints) {
        this.basePoints = basePoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
}
