package com.vedantu.cmds.entities;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ravneet
 */
@Document(collection = "CurriculumTopicTree")
public class CurriculumTopicTree extends AbstractMongoStringIdEntity {

    public static final String DEFAULT_PARENT_ID = "0";

    private String name;

    private String baseTreeNodeId;
    private String baseTreeNodeName;

    private Integer level;

    private String slug;
    private String parentId;
    private VisibilityState state;

    private Set<String> children;

    private Long userId;

    public CurriculumTopicTree() {
        super();
    }

    public CurriculumTopicTree(String name, String slug, String parentId,
                         VisibilityState state, Long userId) {
        super();
        this.name = name;
        this.slug = slug;
        this.parentId = parentId == null ? DEFAULT_PARENT_ID : parentId;
        this.state = state == null ? VisibilityState.VISIBLE : state;
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBaseTreeNodeId() {
        return baseTreeNodeId;
    }

    public void setBaseTreeNodeId(String baseTreeNodeId) {
        this.baseTreeNodeId = baseTreeNodeId;
    }

    public String getBaseTreeNodeName() {
        return baseTreeNodeName;
    }

    public void setBaseTreeNodeName(String baseTreeNodeName) {
        this.baseTreeNodeName = baseTreeNodeName;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public VisibilityState getState() {
        return state;
    }

    public void setState(VisibilityState state) {
        this.state = state;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Set<String> getChildren() {
        return children;
    }

    public void setChildren(Set<String> children) {
        this.children = children;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return String
                .format("{id=%s, name=%s, slug=%s, parentId=%s, state=%s, userId=%s, creationTime=%s, lastUpdated=%s}",
                        getId(), name, slug, parentId, state, userId, getCreationTime(),
                        getLastUpdated());
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String SLUG = "slug";
        public static final String NAME = "name";
        public static final String PARENT_ID = "parentId";
        public static final String CHILDREN = "children";
        public static final String LEVEL = "level";

    }

    public String _getComparableValue() {
        return slug;
    }

}
