package com.vedantu.cmds.entities;

import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author ravneet
 */
@Document(collection = "BaseTopicTree")
@Data
public class BaseTopicTree extends AbstractMongoStringIdEntity {

    public static final String DEFAULT_PARENT_ID = "0";
    public static final Integer DEFAULT_LEVEL = 0;

    @Indexed(unique=true, background=true)
    private String name;
    private String mappedFrom;
    private Integer level;
    private String slug;
    private String parentId;
    private VisibilityState state;

    private Set<String> children;
    private Set<String> parents;

    private Long userId;

    public BaseTopicTree() {
        super();
    }

    public BaseTopicTree(String name, String slug, String parentId,
                 VisibilityState state, Long userId) {
        super();
        this.name = name;
        this.slug = slug;
        this.parentId = parentId == null ? DEFAULT_PARENT_ID : parentId;
        this.state = state == null ? VisibilityState.VISIBLE : state;
        this.userId = userId;
        this.parents = new HashSet<>();
        this.state=VisibilityState.VISIBLE;
    }

    public BaseTopicTree(String title, String slug, String parentId,Integer level){
        this.name=title;
        this.slug=slug;
        this.parentId=parentId==null?DEFAULT_PARENT_ID:parentId;
        this.level=level==null?DEFAULT_LEVEL:level;
        this.state=VisibilityState.VISIBLE;
    }

    @Override
    public String toString() {
        return String
                .format("{id=%s, name=%s, slug=%s, parentId=%s, state=%s, userId=%s, creationTime=%s, lastUpdated=%s}",
                        getId(), name, slug, parentId, state, userId, getCreationTime(),
                        getLastUpdated());
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String SLUG = "slug";
        public static final String NAME = "name";
        public static final String PARENT_ID = "parentId";
        public static final String LEVEL = "level";
        public static final String PARENTS = "parents";
        public static final String CHILDREN = "children";
        public static final String MAPPED_FROM = "mappedFrom";
    }

}
