package com.vedantu.cmds.entities;

public class QuestionStats {
	
	private Long correct = 0l;
	private Long wrong = 0l;
	private Long unattempted = 0l;
	private Double marksAchieved = 0.0;
	private Double totalMarks = 0.0;
	
	public QuestionStats() {
		super();
	}
	
	public Long getCorrect() {
		return correct;
	}
	public void setCorrect(Long correct) {
		this.correct = correct;
	}
	public Long getWrong() {
		return wrong;
	}
	public void setWrong(Long wrong) {
		this.wrong = wrong;
	}
	public Long getUnattempted() {
		return unattempted;
	}
	public void setUnattempted(Long unattempted) {
		this.unattempted = unattempted;
	}
	public Double getMarksAchieved() {
		return marksAchieved;
	}
	public void setMarksAchieved(Double marksAchieved) {
		this.marksAchieved = marksAchieved;
	}
	public Double getTotalMarks() {
		return totalMarks;
	}
	public void setTotalMarks(Double totalMarks) {
		this.totalMarks = totalMarks;
	}
	public void incrementCorrect() {
		this.correct++;
	}
	public void incrementWrong() {
		this.wrong++;
	}
	public void incrementUnattempted() {
		this.unattempted++;
	}
	public void addToMarksAchieved(Float marks) {
		this.marksAchieved += marks;
	}
	public void subtractFromMarksAchieved(Float marks) {
		this.marksAchieved -= marks;
	}
	public void addToTotalMarks(Float marks) {
		this.totalMarks += marks;
	}
}
