/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ajith
 */
@Entity
@Table(name = "ClanLeaderBoard", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"clanId", "leaderBoardType", "category", "startTime"})})
public class ClanLeaderBoard extends AbstractSqlLongIdEntity implements Serializable {

    private String clanId;
//    @Enumerated(EnumType.STRING)
    private String category;
    @Enumerated(EnumType.STRING)
    private LeaderBoardType leaderBoardType;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long startTime;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long endTime;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long points;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long burstChallengePoints;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long musclePoints;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long pictureChallengePoints;

    public ClanLeaderBoard(){
    	super();
    }
    
    public ClanLeaderBoard(String clanId, String category,
			LeaderBoardType leaderBoardType, long startTime, long points, long burstChallengePoints,
			long musclePoints, long pictureChallengePoints) {
		
    	this.clanId = clanId;
		this.category = category;
		this.leaderBoardType = leaderBoardType;
		this.startTime = startTime;
		this.points = points;
		this.burstChallengePoints = burstChallengePoints;
		this.musclePoints = musclePoints;
		this.pictureChallengePoints = pictureChallengePoints;
	}

	public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public LeaderBoardType getLeaderBoardType() {
        return leaderBoardType;
    }

    public void setLeaderBoardType(LeaderBoardType leaderBoardType) {
        this.leaderBoardType = leaderBoardType;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public long getBurstChallengePoints() {
        return burstChallengePoints;
    }

    public void setBurstChallengePoints(long burstChallengePoints) {
        this.burstChallengePoints = burstChallengePoints;
    }

    public long getMusclePoints() {
        return musclePoints;
    }

    public void setMusclePoints(long musclePoints) {
        this.musclePoints = musclePoints;
    }

    public long getPictureChallengePoints() {
        return pictureChallengePoints;
    }

    public void setPictureChallengePoints(long pictureChallengePoints) {
        this.pictureChallengePoints = pictureChallengePoints;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

	@Override
	public String toString() {
		return "ClanLeaderBoard [clanId=" + clanId + ", category=" + category + ", leaderBoardType=" + leaderBoardType
				+ ", startTime=" + startTime + ", endTime=" + endTime + ", points=" + points + ", burstChallengePoints="
				+ burstChallengePoints + ", musclePoints=" + musclePoints + ", pictureChallengePoints="
				+ pictureChallengePoints + "]";
	}
    

}
