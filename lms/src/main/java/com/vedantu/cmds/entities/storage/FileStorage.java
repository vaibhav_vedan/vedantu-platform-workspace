package com.vedantu.cmds.entities.storage;

import java.io.File;
import java.util.Map;

import org.apache.logging.log4j.Logger;

import com.vedantu.cmds.entities.AbstractEntityFileStorage;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.enums.ImageSize;
import com.vedantu.cmds.enums.MediaType;
import com.vedantu.cmds.pojo.FileData;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;

public class FileStorage extends AbstractEntityFileStorage {

	public FileStorage() {
		super(CMDSEntityType.FILE);
	}

	@Override
	public StorageResult storeImage(final String uid, final File file, final FileCategory fileCategory,
			final ImageSize imageSize, final Map<String, String> tags) throws VException {
		return super.storeImage(uid, file, fileCategory, imageSize, tags);
	}

	@Override
	public FileData getData(String entityType, String mediaType, String fileName) throws VException {
		return super.getData(entityType, mediaType, fileName);
	}

	@Override
	public StorageResult storeVideo(final String uid, final File file, final FileCategory fileCategory,
			final Map<String, String> tags, MediaType mediaType) throws VException {
		return super.storeVideo(uid, file, fileCategory, tags, mediaType);
	}
}
