package com.vedantu.cmds.entities;

import com.vedantu.cmds.enums.CMDSVideoSourceType;
import com.vedantu.cmds.enums.OnboardingVideoType;
import com.vedantu.cmds.pojo.OnboardingVideoData;
import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.vedantu.platform.enums.SocialContextType.CMDSVIDEO;

/**
 *
 * @author ravneet
 */
@Data
@NoArgsConstructor
@Document(collection = "CMDSVideo")
public class CMDSVideo extends AbstractTargetTopicEntity implements HomeFeedCardData {

    @Indexed(unique = true, background = true)
    private String videoUrl;

    private String hlsVideoUrl;
    private String title;
    private String description;
    private Set<String> teacherIds;
    private SocialContextType socialContextType = CMDSVIDEO;
    private Integer duration;
    private String thumbnail;
    private CMDSVideoSourceType videoSourceType = CMDSVideoSourceType.YOUTUBE;
    private Long videoViews = 0l;
    private Long totalLikes = 0l;
    private Long totalUnlikes = 0l;
    private OnboardingVideoType onboardingVideoType;

    public CMDSVideo(String videoUrl, String title, String description, Set<String> targetGrades, Set<String> topics, Set<String> teacherIds) {
        this.videoUrl = videoUrl;
        this.title = title;
        this.description = description;
        this.teacherIds = teacherIds;
        super.settTopics(topics);
        super.setTargetGrades(targetGrades);
    }

    public CMDSVideo(OnboardingVideoData onboardingVideoData) {
        this.socialContextType=SocialContextType.ONBOARDING;
        this.videoSourceType=CMDSVideoSourceType.VIMEO;
        this.videoUrl=onboardingVideoData.getVideoUrl();
        this.onboardingVideoType=onboardingVideoData.getOnboardingVideoType();
        if(StringUtils.isNotEmpty(onboardingVideoData.getVideoId())){
            this.setId(onboardingVideoData.getVideoId());
        }
    }

    public CMDSVideo setOnboardingVideoGrades(Set<String> grades){
        this.settGrades(grades);
        this.setMainTags(grades);
        return this;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String MAIN_TAGS = "mainTags";
        public static final String TEACHER_IDS = "teacherIds";
        public static final String VIDEO_VIEWS = "videoViews";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String DURATION = "duration";
        public static final String THUMBNAIL = "thumbnail";
        public static final String VIDEO_URL = "videoUrl";
        public static final String TOTAL_LIKES = "totalLikes";
        public static final String TOTAL_UNLIKES = "totalUnlikes";
        public static final String LIVE = "live";
        public static final String VIDEOS_LIVE = "videos.live";
        public static final String ONBOARDING_VIDEO_TYPE = "onboardingVideoType";
        public static final String SOCIAL_CONTEXT_TYPE = "socialContextType";

    }
}
