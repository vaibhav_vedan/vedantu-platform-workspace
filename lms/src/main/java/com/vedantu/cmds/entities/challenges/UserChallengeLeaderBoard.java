package com.vedantu.cmds.entities.challenges;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;

@Entity
@Table(name = "UserChallengeLeaderBoard", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "userId", "leaderBoardType", "category", "startTime" }) })
public class UserChallengeLeaderBoard extends AbstractSqlLongIdEntity implements Serializable {

	private Long userId;
	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long points;
	@Column(nullable = false, columnDefinition = "int default 0")
	private int totalAttempts;
	@Column(nullable = false, columnDefinition = "int default 0")
	private int correctAttempts;
	private double strikeRate;
	// TODO: Clarity on hints
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, 
			mappedBy = "userChallengeLeaderBoard", orphanRemoval = true)
	private List<HintsCountMapLeaderBoard> hintsCountMap;
	@Enumerated(EnumType.STRING)
	private MultiplierPowerType multiplierPowerType;// TODO,added it now may be
													// remove or will maintain
													// this rather than fetching
													// it runtime

//	@Enumerated(EnumType.STRING)
	private String category;
	@Enumerated(EnumType.STRING)
	private LeaderBoardType leaderBoardType;
	private double timeRate;
	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long startTime;
	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long endTime;

	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long timeTaken;

	@Column(nullable = false, columnDefinition = "bigint(20) default 0")
	private long totalDuration;

	public UserChallengeLeaderBoard() {

	}

	public UserChallengeLeaderBoard(Long userId, LeaderBoardType type, String category,
			MultiplierPowerType multiplierPowerType, Long startTime) {

		this.userId = userId;
		this.leaderBoardType = type;
		this.category = category;
		this.multiplierPowerType = multiplierPowerType;
		this.startTime = startTime;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public long getPoints() {
		return points;
	}

	public void setPoints(long points) {
		this.points = points;
	}

	public int getTotalAttempts() {
		return totalAttempts;
	}

	public void setTotalAttempts(int totalAttempts) {
		this.totalAttempts = totalAttempts;
	}

	public int getCorrectAttempts() {
		return correctAttempts;
	}

	public void setCorrectAttempts(int correctAttempts) {
		this.correctAttempts = correctAttempts;
	}

	public double getStrikeRate() {
		return strikeRate;
	}

	public void setStrikeRate(double strikeRate) {
		this.strikeRate = strikeRate;
	}

	public List<HintsCountMapLeaderBoard> getHintsCountMap() {
		return hintsCountMap;
	}

	public void setHintsCountMap(List<HintsCountMapLeaderBoard> hintsCountMap) {
		this.hintsCountMap = hintsCountMap;
	}

	public MultiplierPowerType getMultiplierPowerType() {
		return multiplierPowerType;
	}

	public void setMultiplierPowerType(MultiplierPowerType multiplierPowerType) {
		this.multiplierPowerType = multiplierPowerType;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public LeaderBoardType getLeaderBoardType() {
		return leaderBoardType;
	}

	public void setLeaderBoardType(LeaderBoardType leaderBoardType) {
		this.leaderBoardType = leaderBoardType;
	}

	public double getTimeRate() {
		return timeRate;
	}

	public void setTimeRate(double timeRate) {
		this.timeRate = timeRate;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public long getTimeTaken() {
		return timeTaken;
	}

	public void setTimeTaken(long timeTaken) {
		this.timeTaken = timeTaken;
	}

	public long getTotalDuration() {
		return totalDuration;
	}

	public void setTotalDuration(long totalDuration) {
		this.totalDuration = totalDuration;
	}

	public void updateHintCount(int hintTakenIndex) {
		// TODO: Handle the hints count map
		if (this.hintsCountMap == null) {
			this.hintsCountMap = new ArrayList<>();
		}
		boolean added = false;
		for (HintsCountMapLeaderBoard hintCount : this.hintsCountMap) {
			if (hintCount.getHintTakenIndex() == hintTakenIndex) {
				hintCount.setTakenCount(hintCount.getTakenCount() + 1);
				added = true;
				break;
			}
		}
		if (!added) {
			HintsCountMapLeaderBoard newmap = new HintsCountMapLeaderBoard(hintTakenIndex);
			this.hintsCountMap.add(newmap);
			newmap.setParent(this);
		}
	}

	public void calculateStrikeRate() {
		this.strikeRate = this.totalAttempts == 0 ? 0 : this.correctAttempts * 100 / this.totalAttempts;
	}

	public void calculateTimeRate() {
		this.timeRate = this.totalDuration == 0 ? 100 : this.timeTaken * 100.00 / this.totalDuration;
	}

}
