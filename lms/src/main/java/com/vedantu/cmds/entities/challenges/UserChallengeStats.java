package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.enums.challenges.MultiplierPowerType;

import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

//reference:https://vladmihalcea.com/2015/03/05/a-beginners-guide-to-jpa-and-hibernate-cascade-types/
@Entity
@Table(name = "UserChallengeStats", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"userId"})})
public class UserChallengeStats extends AbstractSqlLongIdEntity implements Serializable {

    @Version
    private int version;

    private Long userId;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long points;
    @Column(nullable = false, columnDefinition = "int default 0")
    private int totalAttempts;
    @Column(nullable = false, columnDefinition = "int default 0")
    private int correctAttempts;
    private double strikeRate;
    //userChallengeStats is the name of the field of type UserChallengeStats
    //in the class HintsCountMap
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER,
            mappedBy = "userChallengeStats", orphanRemoval = true)
    private List<HintsCountMap> hintsCountMap;
    @Enumerated(EnumType.STRING)
    private MultiplierPowerType multiplierPowerType;//TODO,added it now may be remove or will maintain this rather than fetching it runtime

    public UserChallengeStats() {

    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public int getTotalAttempts() {
        return totalAttempts;
    }

    public void setTotalAttempts(int totalAttempts) {
        this.totalAttempts = totalAttempts;
    }

    public int getCorrectAttempts() {
        return correctAttempts;
    }

    public void setCorrectAttempts(int correctAttempts) {
        this.correctAttempts = correctAttempts;
    }

    public double getStrikeRate() {
        return strikeRate;
    }

    public void setStrikeRate(double strikeRate) {
        this.strikeRate = strikeRate;
    }

    public List<HintsCountMap> getHintsCountMap() {
        return hintsCountMap;
    }

    public void setHintsCountMap(List<HintsCountMap> hintsCountMap) {
        this.hintsCountMap = hintsCountMap;
    }

    public MultiplierPowerType getMultiplierPowerType() {
        return multiplierPowerType;
    }

    public void setMultiplierPowerType(MultiplierPowerType multiplierPowerType) {
        this.multiplierPowerType = multiplierPowerType;
    }

    public void updateHintCount(int hintTakenIndex) {
        if (this.hintsCountMap == null) {
            this.hintsCountMap = new ArrayList<>();
        }
        boolean added = false;
        for (HintsCountMap hintCount : this.hintsCountMap) {
            if (hintCount.getHintTakenIndex() == hintTakenIndex) {
                hintCount.setTakenCount(hintCount.getTakenCount() + 1);
                added = true;
                break;
            }
        }
        if (!added) {
            HintsCountMap newmap = new HintsCountMap(hintTakenIndex);
            this.hintsCountMap.add(newmap);
//       	TODO: Review later based on leaderboard change etc
//            newmap.setParent(this);
        }
    }

    public void calculateStrikeRate() {
        this.strikeRate = this.totalAttempts == 0 ? 0 : this.correctAttempts * 100 / this.totalAttempts;
    }
}
