package com.vedantu.cmds.entities;

import com.vedantu.cmds.enums.ReportEntityType;
import com.vedantu.cmds.enums.ReportStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Set;

@Document(collection = "Report")
@CompoundIndexes({
        @CompoundIndex(name = "creationTime", def = "{'creationTime':1}", background = true)})
public class Report extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String entityId;
    private ReportEntityType entityType;
    private String newEntityId;
    @Indexed(background = true)
    private ReportStatus status;
    private String reviewBy;
    private List<UserReport> userReports;
    @Indexed(background = true)
    private int count = 0;
    @Indexed(background = true)
    private Set<String> mainTags;
    private String reviewComment;
    private String subject;
    public Set<String> targetGrade;

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public ReportEntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(ReportEntityType entityType) {
        this.entityType = entityType;
    }

    public ReportStatus getStatus() {
        return status;
    }

    public void setStatus(ReportStatus status) {
        this.status = status;
    }

    public String getReviewBy() {
        return reviewBy;
    }

    public void setReviewBy(String reviewBy) {
        this.reviewBy = reviewBy;
    }

    public List<UserReport> getUserReports() {
        return userReports;
    }

    public void setUserReports(List<UserReport> userReports) {
        this.userReports = userReports;
    }

    public String getNewEntityId() {
        return newEntityId;
    }

    public void setNewEntityId(String newEntityId) {
        this.newEntityId = newEntityId;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
    public void increaseCount() {
        count++;
    }

    public Set<String> getMainTags() {
        return mainTags;
    }

    public void setMainTags(Set<String> mainTags) {
        this.mainTags = mainTags;
    }

    public String getReviewComment() {
        return reviewComment;
    }

    public void setReviewComment(String reviewComment) {
        this.reviewComment = reviewComment;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Set<String> getTargetGrade() {
        return targetGrade;
    }

    public void setTargetGrade(Set<String> targetGrade) {
        this.targetGrade = targetGrade;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String ENTITYID = "entityId";
        public static final String STATUS = "status";
        public static final String COUNT= "count";
        public static final String MAIN_TAGS= "mainTags";
        public static final String USERREPORTS_USERID = "userReports.userId";
        public static final String USERREPORTS_RAISEDON = "userReports.raisedOn";

    }

}
