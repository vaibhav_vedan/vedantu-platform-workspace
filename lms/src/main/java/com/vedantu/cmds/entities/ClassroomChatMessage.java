package com.vedantu.cmds.entities;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Document(collection = "ClassroomChatMessage")
public class ClassroomChatMessage extends AbstractMongoStringIdEntity {

	@Indexed(background=true)
	private String videoId;
	private Long userId;
	private String picUrl;
	private String message;
	private String userName;

	public ClassroomChatMessage() {
		super();
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getVideoId() {
		return videoId;
	}
	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}
	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}
	public String getPicUrl() {
		return picUrl;
	}
	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
}
