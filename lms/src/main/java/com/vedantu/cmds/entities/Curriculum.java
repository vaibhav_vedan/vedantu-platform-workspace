package com.vedantu.cmds.entities;

import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.enums.CurriculumStatus;
import com.vedantu.lms.cmds.enums.SessionType;
import com.vedantu.lms.cmds.pojo.ContentInfo;
import com.vedantu.lms.cmds.pojo.CurriculumStatusChangeTime;
import com.vedantu.lms.cmds.pojo.Node;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Data
@NoArgsConstructor
@Document(collection = "Curriculum")
@CompoundIndexes({
		@CompoundIndex(name = "statusChangeTime.changeTime_1",def = "{'statusChangeTime.changeTime':1}",background = true)

})
public class Curriculum extends AbstractMongoStringIdEntity {

	@Indexed(background = true)
	private String parentId;
	private Long startTime;
	private Long endTime;
	private String title;
	private String note;
	private CurriculumStatus status = CurriculumStatus.PENDING;
	private SessionType sessionType;
	private List<ContentInfo> contents;
	private Set<String> sessionIds;
	private CurriculumEntityName contextType;
	@Indexed(background = true)
	private String contextId;
	private String rootId;
	private Long boardId;
	private Integer childOrder;
	private List<CurriculumStatusChangeTime> statusChangeTime;
	private String parentCourseId;
	private Float expectedHours;
	@Indexed(background = true)
	private String courseNodeId;
	private Set<String> topicTags;

	public Curriculum(Node node) {
		super();
		this.startTime = node.getStartTime();
		this.endTime = node.getEndTime();
		this.title = node.getTitle();
		this.note = node.getNote();
		if (node.getStatus() != null) {
			this.status = node.getStatus();
		} else {
			this.status = CurriculumStatus.PENDING;
		}
		this.sessionType = node.getSessionType();
		this.contents = node.getContents();
		this.sessionIds =  node.getSessionIds();
		if (node.getParentId() != null) {
			this.parentId = node.getParentId();
		}
		this.boardId = node.getBoardId();
		this.childOrder = node.getChildOrder();
		this.expectedHours = node.getExpectedHours();
		this.topicTags=node.getTopicTags();

	}

	public Curriculum(Curriculum curriculum) {
		super();
		this.startTime = curriculum.getStartTime();
		this.endTime = curriculum.getEndTime();
		this.title = curriculum.getTitle();
		this.note = curriculum.getNote();
		if (curriculum.getStatus() != null) {
			this.status = curriculum.getStatus();
		} else {
			this.status = CurriculumStatus.PENDING;
		}
		this.sessionType = curriculum.getSessionType();
		this.contents = curriculum.getContents();
		if (curriculum.getParentId() != null)
			this.parentId = curriculum.getParentId();

	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String PARENT_ID = "parentId";
		public static final String CONTEXT_ID = "contextId";
		public static final String CONTEXT_TYPE = "contextType";
		public static final String ROOT_ID = "rootId";
		public static final String CHILD_ORDER = "childOrder";
		public static final String COURSE_NODE_ID = "courseNodeId";
		public static final String CONTENTS = "contents";
		public static final String CONTENT_INFOID = "contentInfoId";//part of contents
		public static final String TITLE = "title";
		public static final String NOTE = "note";
		public static final String EXPECTED_HOURS = "expectedHours";
		public static final String BOARD_ID = "boardId";
		public static final String SESSION_IDS = "sessionIds";
		public static final String STATUS = "status";
		public static final String CONTENT_TYPE = "contents.contentType";
		public static final String STATUS_CHANGE_TIME="statusChangeTime.changeTime";
		public static final String TOPIC_TAGS="topicTags";

	}

}
