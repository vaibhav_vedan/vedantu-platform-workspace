/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.entities.AbstractCMDSEntity;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.challenges.PictureChallengeAnswer;
import com.vedantu.cmds.pojo.challenges.PictureChallengeDayDeductions;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.util.List;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "PictureChallenge")
public class PictureChallenge extends AbstractMongoStringIdEntity {

    private String title;
    private ChallengeStatus status = ChallengeStatus.DRAFT;
    private Long startTime;
    private Long burstTime;
    private Difficulty difficulty;
    private String category;
    private List<PictureChallengeDayDeductions> dayWisePointsToWin;
    private CloudStorageEntity image;
    private int pointsToRevealFullImage;
    private List<CloudStorageEntity> maskedImages;
    private List<PictureChallengeAnswer> answer;
    private String answerString;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ChallengeStatus getStatus() {
        return status;
    }

    public void setStatus(ChallengeStatus status) {
        this.status = status;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(Long burstTime) {
        this.burstTime = burstTime;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<PictureChallengeDayDeductions> getDayWisePointsToWin() {
        return dayWisePointsToWin;
    }

    public void setDayWisePointsToWin(List<PictureChallengeDayDeductions> dayWisePointsToWin) {
        this.dayWisePointsToWin = dayWisePointsToWin;
    }

    public CloudStorageEntity getImage() {
        return image;
    }

    public void setImage(CloudStorageEntity image) {
        this.image = image;
    }

    public int getPointsToRevealFullImage() {
        return pointsToRevealFullImage;
    }

    public void setPointsToRevealFullImage(int pointsToRevealFullImage) {
        this.pointsToRevealFullImage = pointsToRevealFullImage;
    }

    public List<CloudStorageEntity> getMaskedImages() {
        return maskedImages;
    }

    public void setMaskedImages(List<CloudStorageEntity> maskedImages) {
        this.maskedImages = maskedImages;
    }

    public List<PictureChallengeAnswer> getAnswer() {
        return answer;
    }

    public void setAnswer(List<PictureChallengeAnswer> answer) {
        this.answer = answer;
    }
    
    public String getAnswerString() {
		return answerString;
	}

	public void setAnswerString(String answerString) {
		this.answerString = answerString;
	}

	public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String STATUS = "status";

        public static final String DIFFICULTY = "difficulty";
        public static final String CATEGORY = "category";
        public static final String BURST_TIME = "burstTime";
        public static final String START_TIME = "startTime";
    }

}
