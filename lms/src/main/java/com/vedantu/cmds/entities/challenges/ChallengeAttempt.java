package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.pojo.challenges.ChallengeTakenHint;
import com.vedantu.cmds.entities.AbstractCMDSEntity;
import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ChallengeAttempt")
@CompoundIndexes({
    @CompoundIndex(name = "userId_challengeId", def = "{ 'userId': 1,'challengeId' : 1}", unique = true, background = true),
    @CompoundIndex(name = "creationTime_category", def = "{ 'creationTime': 1,'category' : 1}", background = true)
})
public class ChallengeAttempt extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    public String challengeId;
    @Indexed(background = true)
    public Long userId;
    // startTime will be timeCreated
    public long endTime;
//    public int bid;
//    public boolean bidded;
    public int hintTakenIndex = -1;
    public long answerTime;
    public long timeTaken;
    public boolean success;
    public boolean processed;
    public MultiplierPowerType multiplierPower;
    public int basePoints;
    public int totalPoints;//after multiplying with multiplierPower
    public List<String> answer;
    private List<ChallengeTakenHint> hintsTaken;
    private String category;
    private Boolean grace;

    public ChallengeAttempt() {

    }

    public ChallengeAttempt(String challengeId, Long userId, long endTime,String category) {
        super();
        this.challengeId = challengeId;
        this.userId = userId;
        this.endTime = endTime;
        this.answer = new ArrayList<>();
        this.category=category;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getHintTakenIndex() {
        return hintTakenIndex;
    }

    public void setHintTakenIndex(int hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
    }

    public long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(long answerTime) {
        this.answerTime = answerTime;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public MultiplierPowerType getMultiplierPower() {
        return multiplierPower;
    }

    public void setMultiplierPower(MultiplierPowerType multiplierPower) {
        this.multiplierPower = multiplierPower;
    }

    public int getBasePoints() {
        return basePoints;
    }

    public void setBasePoints(int basePoints) {
        this.basePoints = basePoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public List<String> getAnswer() {
        return answer;
    }

    public void setAnswer(List<String> answer) {
        this.answer = answer;
    }

    public void addAnswer(List<String> answer) {
        if (CollectionUtils.isEmpty(answer)) {
            return;
        }
        if (this.answer == null) {
            this.answer = new ArrayList<>();
        }
        this.answer.addAll(answer);
        this.answerTime = System.currentTimeMillis();
        this.timeTaken = this.answerTime - super.getCreationTime();
    }

    public List<ChallengeTakenHint> getHintsTaken() {
        if (this.hintsTaken == null) {
            return new ArrayList<>();
        }
        return hintsTaken;
    }

    public void setHintsTaken(List<ChallengeTakenHint> hintsTaken) {
        this.hintsTaken = hintsTaken;
    }

    public void addHint(ChallengeTakenHint challengeTakenHint) {
        if (this.hintsTaken == null) {
            this.hintsTaken = new ArrayList<>();
        }
        this.hintsTaken.add(challengeTakenHint);
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
    
    public Boolean getGrace() {
		return grace;
	}

	public void setGrace(Boolean grace) {
		this.grace = grace;
	}

	public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String USERID = "userId";
        public static final String CHALLENGEID = "challengeId";
        public static final String TOTAL_POINTS = "totalPoints";
        public static final String TIME_TAKEN = "timeTaken";
        public static final String END_TIME = "endTime";
        public static final String SUCCESS = "success";
        public static final String CATEGORY = "category";
    }

}
