package com.vedantu.cmds.entities;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.TestEndType;
import com.vedantu.cmds.utils.CMDSCommonUtils;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptImage;
import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import com.vedantu.lms.cmds.pojo.TestFeedback;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "CMDSTestAttempt")
@CompoundIndexes({
		@CompoundIndex(useGeneratedName = true,def = "{'contentInfoId' : -1, '_id': -1}",background = true)
})
public class CMDSTestAttempt extends AbstractMongoStringIdEntity {
	@Indexed(background = true)
	private String testId;
	private String contentInfoId;
	private ContentInfoType contentInfoType;
	private String teacherId;
	private String studentId;
	private Long evaluatedViewTime;
	private Long evaluatedTime;
	private Long reportViewed;
	private CMDSAttemptState attemptState;
	private Set<String> subjects = new HashSet<>();
	private float totalMarks;
	@Indexed(background = true)
	private Float marksAcheived = 0f;
	private Long timeTakenByStudent;
	private String attemptRemarks;
	private List<CMDSTestAttemptImage> imageDetails;
	private Long duration;
	private Long startTime;
	@Indexed(background = true)
	private Long endTime;
	private Long startedAt;
	private Long endedAt;
	private TestEndType testEndType;
	private List<QuestionAnalytics> resultEntries;
	private List<CategoryAnalytics> categoryAnalyticsList;
	private Double percentile;
	private Integer rank;
	private Integer totalStudents;
	private Integer batchAvg;
	private Integer attempted = 0;
	private Integer unattempted = 0;
	private Integer correct = 0;
	private Integer incorrect = 0;
	private float attemptMarksPercent;
	private float testPercentile;
	private Long doNotShowResultsTill;
	private TestFeedback feedback;
	private int attemptIndex=0;

	// Reevaluation parameters
	private Integer reevaluationNumber = 0;
	private Long reevaluationTime;

	// Marketing related temporary field
	private String marketingRedirectUrl;

	public CMDSTestAttempt() {
		super();
		setId(ObjectId.get().toString());
	}

	public CMDSTestAttempt(CMDSTestAttempt cmdsTestAttempt){
		super();
		setId(ObjectId.get().toString());
		this.testId = cmdsTestAttempt.getTestId();
		this.contentInfoId = cmdsTestAttempt.getContentInfoId();
		this.teacherId = cmdsTestAttempt.getTeacherId();
		this.evaluatedViewTime = cmdsTestAttempt.getEvaluatedViewTime();
		this.evaluatedTime = cmdsTestAttempt.getEvaluatedTime();
		this.reportViewed = cmdsTestAttempt.getReportViewed();
		this.attemptState = cmdsTestAttempt.getAttemptState();
		this.totalMarks = cmdsTestAttempt.getTotalMarks();
		this.marksAcheived = cmdsTestAttempt.getMarksAcheived();
		this.timeTakenByStudent = cmdsTestAttempt.getTimeTakenByStudent();
		this.attemptRemarks = cmdsTestAttempt.getAttemptRemarks();
		this.resultEntries = cmdsTestAttempt.getResultEntries();
		this.imageDetails = cmdsTestAttempt.getImageDetails();
		this.percentile = cmdsTestAttempt.getPercentile();
		this.rank = cmdsTestAttempt.getRank();
		this.totalStudents = cmdsTestAttempt.getTotalStudents();
		this.batchAvg = cmdsTestAttempt.getBatchAvg();
		this.attempted = cmdsTestAttempt.getAttempted();
		this.unattempted = cmdsTestAttempt.getUnattempted();
		this.correct = cmdsTestAttempt.getCorrect();
		this.incorrect = cmdsTestAttempt.getIncorrect();
		this.attemptMarksPercent = cmdsTestAttempt.getAttemptMarksPercent();
		this.testPercentile = cmdsTestAttempt.getTestPercentile();
		this.reevaluationNumber = cmdsTestAttempt.getReevaluationNumber();
		this.reevaluationTime = cmdsTestAttempt.getReevaluationTime();
		this.duration = cmdsTestAttempt.getDuration();
		this.startTime = cmdsTestAttempt.getStartTime();
		this.endTime = cmdsTestAttempt.getEndTime();
		this.startedAt = cmdsTestAttempt.getStartedAt();
		this.endedAt = cmdsTestAttempt.getEndedAt();
		this.testEndType = cmdsTestAttempt.getTestEndType();
		this.categoryAnalyticsList = cmdsTestAttempt.getCategoryAnalyticsList();
		if (Objects.nonNull(cmdsTestAttempt.getFeedback())) {
			this.feedback = cmdsTestAttempt.getFeedback();
		}
		this.setCreationTime(cmdsTestAttempt.getCreationTime());
		this.setCreatedBy(cmdsTestAttempt.getCreatedBy());
		this.setLastUpdated(cmdsTestAttempt.getLastUpdated());
		this.setLastUpdatedBy(cmdsTestAttempt.getLastUpdatedBy());
	}

	public CMDSTestAttempt(ContentInfo contentInfo, CMDSTest cmdsTest) {
		super();
		setId(ObjectId.get().toString());
		Long currentTime = System.currentTimeMillis();
		this.testId = cmdsTest.getId();
		this.contentInfoId = contentInfo.getId();
		this.teacherId = contentInfo.getTeacherId();
		this.attemptState = CMDSAttemptState.ATTEMPT_STARTED;

		if (cmdsTest.getTotalMarks() == null) {
			this.totalMarks = cmdsTest.calculateTotalMarks();
		} else {
			this.totalMarks = cmdsTest.getTotalMarks();
		}
		this.duration = cmdsTest.getDuration();
		this.startTime = currentTime;
		this.startedAt = currentTime;
		if (cmdsTest.getDuration() != null && cmdsTest.getDuration() != 0l) {
			this.endTime = currentTime + cmdsTest.getDuration();
		}
		setCreationTime(currentTime);

		// Populate result entries
		this.resultEntries = CMDSCommonUtils.createTestResultEntries(cmdsTest);
		this.imageDetails = new ArrayList<>();
	}

	public CMDSTestAttempt(String testId, String contentInfoId, String studentId, String teacherId,
						   Long evaluatedViewTime, Long evaluatedTime, Long reportViewed, CMDSAttemptState attemptState,
						   float totalMarks, Float marksAcheived, Long timeTakenByStudent, String attemptRemarks,
						   List<QuestionAnalytics> resultEntries, List<CMDSTestAttemptImage> imageDetails, TestFeedback feedback) {
		super();
		setId(ObjectId.get().toString());
		this.testId = testId;
		this.contentInfoId = contentInfoId;
		this.teacherId = teacherId;
		this.evaluatedViewTime = evaluatedViewTime;
		this.evaluatedTime = evaluatedTime;
		this.reportViewed = reportViewed;
		this.attemptState = attemptState;
		this.totalMarks = totalMarks;
		this.marksAcheived = marksAcheived;
		this.timeTakenByStudent = timeTakenByStudent;
		this.attemptRemarks = attemptRemarks;
		this.resultEntries = resultEntries;
		this.imageDetails = imageDetails;
		this.feedback = feedback;
	}

	public void removeImageUrls() {
		if (!CollectionUtils.isEmpty(this.imageDetails)) {
			for (CMDSTestAttemptImage image : imageDetails) {
				image.setEvaluatedPublicUrl(null);
				image.setPublicUrl(null);
			}
		}
	}

	public void resetValues(){
		this.marksAcheived=0f;
		this.unattempted=0;
		this.attempted=0;
		this.correct=0;
		this.incorrect=0;
	}

	public void incrementAnalyticsValues(CategoryAnalytics categoryAnalytics){
		if(categoryAnalytics!=null){
			this.marksAcheived += categoryAnalytics.getMarks();
			this.unattempted += categoryAnalytics.getUnattempted();
			this.attempted += categoryAnalytics.getAttempted();
			this.correct += categoryAnalytics.getCorrect();
			this.incorrect += categoryAnalytics.getIncorrect();
		}
	}
	public static class Constants extends AbstractMongoStringIdEntity.Constants {

		public static final String TEST_ID = "testId";
		public static final String CONTENT_INFO_ID = "contentInfoId";
		public static final String END_TIME = "endTime";
		public static final String ATTEMPT_STATE = "attemptState";
		public static final String RESULT_ENTRIES = "resultEntries";
		public static final String MARKS_ACHIEVED = "marksAcheived";
		public static final String STARTED_AT = "startedAt";
		public static final String ENDED_AT = "endedAt";
		public static final String START_TIME = "startTime";
		public static final String TEACHER_ID = "teacherId";
		public static final String TIME_TAKEN_BY_STUDENT = "timeTakenByStudent";
		public static final String DURATION = "duration";
		public static final String CATEGORY_ANALYTICS_LIST = "categoryAnalyticsList";
		public static final String BATCH_AVG = "batchAvg";
		public static final String ATTEMPT_MARKS_PERCENTAGE = "attemptMarksPercent";
		public static final String RANK = "rank";
		public static final String TEST_PERCENTILE = "testPercentile";
		public static final String TOTAL_STUDENTS = "totalStudents";
		public static final String FEEDBACK = "feedback";
		public static final String CONTENT_INFO_TYPE = "contentInfoType";
		public static final String TOTAL_MARKS = "totalMarks";
		public static final String ATTEMPT_INDEX = "attemptIndex";
	}
}
