/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.cmds.enums.challenges.UserChallengeStatsContextType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ajith
 */
@Entity
@Table(name = "UserChallengeStatsTransaction", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"userChallengeStatsId", "contextType", "contextId"})})
public class UserChallengeStatsTransaction extends AbstractSqlLongIdEntity implements Serializable {

    private Long userId;//just for ref
    private Long userChallengeStatsId;//mapped to userLeaderBoard - default-overall
    @Enumerated(EnumType.STRING)
    private UserChallengeStatsContextType contextType;
    private String contextId;
    private int pointsAdded;
    private long closingPoints;
    private int closingAttempts;
    private int closingCorrectAttempts;
    private int hintIndexTakenAdded;
    @Enumerated(EnumType.STRING)
    private MultiplierPowerType closingMultiplierPowerType;
    private String category;

    public UserChallengeStatsTransaction() {
    }

    public UserChallengeStatsTransaction(Long userId, Long userChallengeStatsId,
            UserChallengeStatsContextType contextType, String contextId,
            int pointsAdded, long closingPoints, int closingAttempts,
            int closingCorrectAttempts, int hintIndexTakenAdded,
            MultiplierPowerType closingMultiplierPowerType,
            String category) {
        this.userId = userId;
        this.userChallengeStatsId = userChallengeStatsId;
        this.contextType = contextType;
        this.contextId = contextId;
        this.pointsAdded = pointsAdded;
        this.closingPoints = closingPoints;
        this.closingAttempts = closingAttempts;
        this.closingCorrectAttempts = closingCorrectAttempts;
        this.hintIndexTakenAdded = hintIndexTakenAdded;
        this.closingMultiplierPowerType = closingMultiplierPowerType;
        this.category = category;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUserChallengeStatsId() {
        return userChallengeStatsId;
    }

    public void setUserChallengeStatsId(Long userChallengeStatsId) {
        this.userChallengeStatsId = userChallengeStatsId;
    }

    public UserChallengeStatsContextType getContextType() {
        return contextType;
    }

    public void setContextType(UserChallengeStatsContextType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public int getPointsAdded() {
        return pointsAdded;
    }

    public void setPointsAdded(int pointsAdded) {
        this.pointsAdded = pointsAdded;
    }

    public long getClosingPoints() {
        return closingPoints;
    }

    public void setClosingPoints(long closingPoints) {
        this.closingPoints = closingPoints;
    }

    public int getClosingAttempts() {
        return closingAttempts;
    }

    public void setClosingAttempts(int closingAttempts) {
        this.closingAttempts = closingAttempts;
    }

    public int getClosingCorrectAttempts() {
        return closingCorrectAttempts;
    }

    public void setClosingCorrectAttempts(int closingCorrectAttempts) {
        this.closingCorrectAttempts = closingCorrectAttempts;
    }

    public int getHintIndexTakenAdded() {
        return hintIndexTakenAdded;
    }

    public void setHintIndexTakenAdded(int hintIndexTakenAdded) {
        this.hintIndexTakenAdded = hintIndexTakenAdded;
    }

    public MultiplierPowerType getClosingMultiplierPowerType() {
        return closingMultiplierPowerType;
    }

    public void setClosingMultiplierPowerType(MultiplierPowerType closingMultiplierPowerType) {
        this.closingMultiplierPowerType = closingMultiplierPowerType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
