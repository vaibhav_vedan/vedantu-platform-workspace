/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities.challenges;

import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author ajith at clan level contains points obtained when burst challenge -
 * top points obtained by members of clan picture unravel- x*number of people in
 * clan at that time, take x from config file picture unravel- points from
 * solving the picture challenge correctly
 */
@Entity
@Table(name = "PictureChallengeLeaderBoard", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"clanId", "pictureChallengeId"})})
public class PictureChallengeLeaderBoard extends AbstractSqlLongIdEntity implements Serializable {

    private String clanId;
    private String pictureChallengeId;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long points;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long burstChallengePoints;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long musclePoints;
    @Column(nullable = false, columnDefinition = "bigint(20) default 0")
    private long pictureChallengePoints;
//    @Enumerated(EnumType.STRING)
    private String category;
    
    public PictureChallengeLeaderBoard(String clanId, String pictureChallengeId,
    		String category) {
		this.clanId = clanId;
		this.pictureChallengeId = pictureChallengeId;
		this.category = category;

	}

	public PictureChallengeLeaderBoard(){
    	super();
    }

    public String getClanId() {
        return clanId;
    }

    public void setClanId(String clanId) {
        this.clanId = clanId;
    }

    public String getPictureChallengeId() {
        return pictureChallengeId;
    }

    public void setPictureChallengeId(String pictureChallengeId) {
        this.pictureChallengeId = pictureChallengeId;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public long getBurstChallengePoints() {
        return burstChallengePoints;
    }

    public void setBurstChallengePoints(long burstChallengePoints) {
        this.burstChallengePoints = burstChallengePoints;
    }

    public long getMusclePoints() {
        return musclePoints;
    }

    public void setMusclePoints(long musclePoints) {
        this.musclePoints = musclePoints;
    }

    public long getPictureChallengePoints() {
        return pictureChallengePoints;
    }

    public void setPictureChallengePoints(long pictureChallengePoints) {
        this.pictureChallengePoints = pictureChallengePoints;
    }

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

}
