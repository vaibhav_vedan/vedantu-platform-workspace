package com.vedantu.cmds.entities;

import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Set;

@Document(collection = "CMDSTestList")
public class CMDSTestList extends AbstractTargetTopicEntity {

    private String title;
    private String description;
    private boolean published = false;
    private List<String> tests;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public List<String> getTests() {
        return tests;
    }

    public void setTests(List<String> tests) {
        this.tests = tests;
    }

    public static class Constants extends AbstractTargetTopicEntity.Constants {
        public static final String PUBLISHED = "published";
        public static final String TESTS = "tests";
    }
}