/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities;

import com.vedantu.cmds.pojo.BookNode;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "Book")
public class Book extends AbstractCMDSEntity {

    private String displayName;
    private String description;
    private List<CloudStorageEntity> thumbnails = new ArrayList<>();
    private String edition;
    private List<BookNode> nodes = new ArrayList<>();
    @Indexed(background = true)
    private String bookSlug;
    @Indexed(background = true)
    private String editionSlug;

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CloudStorageEntity> getThumbnails() {
        if (thumbnails == null) {
            return new ArrayList<>();
        }
        return thumbnails;
    }

    public void setThumbnails(List<CloudStorageEntity> thumbnails) {
        this.thumbnails = thumbnails;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public List<BookNode> getNodes() {
        if (nodes == null) {
            return new ArrayList<>();
        }
        return nodes;
    }

    public void setNodes(List<BookNode> nodes) {
        this.nodes = nodes;
    }

    public String getBookSlug() {
        return bookSlug;
    }

    public void setBookSlug(String bookSlug) {
        this.bookSlug = bookSlug;
    }

    public String getEditionSlug() {
        return editionSlug;
    }

    public void setEditionSlug(String editionSlug) {
        this.editionSlug = editionSlug;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String BOOK_SLUG = "bookSlug";
        public static final String EDITION_SLUG = "editionSlug";

    }

}
