package com.vedantu.cmds.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "CMDSTestListOrder")
public class CMDSTestListOrder extends AbstractMongoStringIdEntity {

    @Indexed(unique = true, background = true)
    private String grade;
    private List<String> playlists;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<String> getPlaylists() {
        return playlists;
    }

    public void setPlaylists(List<String> playlists) {
        this.playlists = playlists;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String GRADE = "grade";
        public static final String PLAYLISTS = "playlists";

    }

    @Override
    public String toString() {
        return "CMDSTestListOrder{" +
                "grade='" + grade + '\'' +
                ", playlists=" + playlists +
                '}';
    }
}