package com.vedantu.cmds.entities;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.QuestionType;
import lombok.Data;

import static com.vedantu.util.CommonUtils.not;

@Data
public class CMDSQuestionSet extends AbstractCMDSEntity {

	public List<String> questionIds;

	public String type;

	public int numberOfQuestionsComplete;
	public QuestionType questionSetType;
	public String fileName;
	public String originalRefName;
	public Difficulty difficulty;
	public int versionCount = 0;
	public CMDSApprovalStatus approvalStatus=CMDSApprovalStatus.TEMPORARY;
	private Long assignedChecker;

	public CMDSQuestionSet() {
		super();
	}

	public CMDSQuestionSet(List<String> questionIds, String type, int numberOfQuestionsComplete,
			QuestionType questionSetType, String fileName, String originalRefName, Difficulty difficulty) {
		super();
		this.questionIds = questionIds;
		this.type = type;
		this.numberOfQuestionsComplete = numberOfQuestionsComplete;
		this.questionSetType = questionSetType;
		this.fileName = fileName;
		this.originalRefName = originalRefName;
		this.difficulty = difficulty;
	}

	public void removeAndReplaceDuplicates(Map<String, String> questionDuplicateMap) {
		List<String> questionsForRemoval = this.questionIds.stream().filter(questionDuplicateMap::containsKey).collect(Collectors.toList());
		this.questionIds.removeAll(questionsForRemoval);
		this.questionIds.addAll(questionsForRemoval.stream().map(questionDuplicateMap::get).collect(Collectors.toList()));
	}

	public void removeUnusedQuestion(List<String> collect) {
		List<String> questionsForRemoval=this.questionIds.stream().filter(not(collect::contains)).collect(Collectors.toList());
		this.questionIds.removeAll(questionsForRemoval);
	}

    public static class Constants extends AbstractCMDSEntity.Constants {
		public static final String QUESTION_IDS = "questionIds";
		public static final String DIFFICULTY = "difficulty";
		public static final String STATUS = "status";
        public static final String APPROVAL_STATUS = "approvalStatus";
        public static final String FILE_NAME = "fileName";
		public static final String ASSIGNED_CHECKER = "assignedChecker";

	}
}
