package com.vedantu.cmds.entities.storage;

import java.io.File;
import java.util.Map;

import com.vedantu.cmds.entities.AbstractEntityFileStorage;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.enums.ImageSize;
import com.vedantu.cmds.enums.MediaType;
import com.vedantu.exception.VException;

public class CMDSQuestionEntityFileStorage extends AbstractEntityFileStorage {

	public CMDSQuestionEntityFileStorage() {
		super(CMDSEntityType.CMDSQUESTION);
	}

	@Override
	public StorageResult storeImage(final String uid, final File file, final FileCategory fileCategory,
			final ImageSize imageSize, final Map<String, String> tags) throws VException {
		return super.storeImage(uid, file, fileCategory, imageSize, tags);
	}

	@Override
	public StorageResult storeVideo(final String uid, final File file, final FileCategory fileCategory,
			final Map<String, String> tags, MediaType mediaType) throws VException {
		return super.storeVideo(uid, file, fileCategory, tags, mediaType);
	}
}
