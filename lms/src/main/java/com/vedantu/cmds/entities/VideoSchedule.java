/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities;

import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "VideoSchedule")
public class VideoSchedule extends AbstractTargetTopicEntity {

    //TODO add index to entityState later based on api performance
    private Long scheduleTime;
    private String scheduleTimeStr;
    private String title;

    public Long getScheduleTime() {
        return scheduleTime;
    }

    public void setScheduleTime(Long scheduleTime) {
        this.scheduleTime = scheduleTime;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getScheduleTimeStr() {
        return scheduleTimeStr;
    }

    public void setScheduleTimeStr(String scheduleTimeStr) {
        this.scheduleTimeStr = scheduleTimeStr;
    }

    public static class Constants extends AbstractTargetTopicEntity.Constants {

        public static final String SCHEDULE_TIME = "scheduleTime";
    }

}
