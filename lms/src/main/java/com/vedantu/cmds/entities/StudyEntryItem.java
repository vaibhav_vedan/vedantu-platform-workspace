/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities;

import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import org.bson.types.ObjectId;

import static com.vedantu.platform.enums.SocialContextType.STUDY_ENTRY_ITEM;

/**
 *
 * @author ajith
 */
public class StudyEntryItem extends AbstractTargetTopicEntity implements HomeFeedCardData {

    private String curriculumName; //ncert solutions, previous yrs papers, syllabus,forumale title, this data pt is used on screen1
    private String subjectOrBookName;//this data pt is used on screen2
    private String chapterTitle; // this data pt is used on screen3
    private String pdfLink;// this data pt is used on screen4
    private String seoUrl;// this data pt is used on screen4
    private String boardGrade;//CBSE - 10- , this data pt is used on screen1
    private boolean bookmark;// will only be used in response -- hackish way
    private Long views = 0l;
    private SocialContextType socialContextType = STUDY_ENTRY_ITEM;

    public StudyEntryItem() {
    }

    public StudyEntryItem(String curriculumName, String subjectOrBookName, String chapterTitle, String pdfLink, String seoUrl, String boardGrade) {
        this.curriculumName = curriculumName;
        this.subjectOrBookName = subjectOrBookName;
        this.chapterTitle = chapterTitle;
        this.pdfLink = pdfLink;
        this.seoUrl = seoUrl;
        this.boardGrade = boardGrade;
        this.setId(new ObjectId().toString());//bad but no time, no choice
        this.setCreationTime(System.currentTimeMillis());
    }

    public String getCurriculumName() {
        return curriculumName;
    }

    public void setCurriculumName(String curriculumName) {
        this.curriculumName = curriculumName;
    }

    public String getSubjectOrBookName() {
        return subjectOrBookName;
    }

    public void setSubjectOrBookName(String subjectOrBookName) {
        this.subjectOrBookName = subjectOrBookName;
    }

    public String getChapterTitle() {
        return chapterTitle;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }

    public String getPdfLink() {
        return pdfLink;
    }

    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }

    public String getSeoUrl() {
        return seoUrl;
    }

    public void setSeoUrl(String seoUrl) {
        this.seoUrl = seoUrl;
    }

    public String getBoardGrade() {
        return boardGrade;
    }

    public void setBoardGrade(String boardGrade) {
        this.boardGrade = boardGrade;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public static class Constants extends AbstractTargetTopicEntity.Constants {

        public static final String SEO_URL = "seoUrl";
        public static final String VIEWS = "views";
        public static final String BOARD_GRADE = "boardGrade";
    }
}
