package com.vedantu.cmds.entities.challenges;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.apache.commons.collections.CollectionUtils;

import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.util.dbentities.mysql.AbstractSqlStringIdEntity;
import org.apache.commons.lang.StringUtils;

@Entity
@Table(name = "ChallengeAttemptRD", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"userId", "challengeId"})})
public class ChallengeAttemptRD extends AbstractSqlStringIdEntity implements Serializable {

//  @Indexed(background = true)
    public String challengeId;
    //   @Indexed(background = true)
    public Long userId;
    // startTime will be timeCreated
    public long endTime;
    public int hintTakenIndex = -1;
    public long answerTime;
    public long timeTaken;
    public boolean success;
    public boolean processed;
    @Enumerated(EnumType.STRING)
    public MultiplierPowerType multiplierPower;
    public int basePoints;
    public int totalPoints;
    public String answerString;
//    @Enumerated(EnumType.STRING)
    private String category;
    private boolean grace;

    public ChallengeAttemptRD() {
        super();

    }

    public ChallengeAttemptRD(String challengeId, Long userId, long endTime, String category) {
        super();
        this.challengeId = challengeId;
        this.userId = userId;
        this.endTime = endTime;

        this.category = category;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public int getHintTakenIndex() {
        return hintTakenIndex;
    }

    public void setHintTakenIndex(int hintTakenIndex) {
        this.hintTakenIndex = hintTakenIndex;
    }

    public long getAnswerTime() {
        return answerTime;
    }

    public void setAnswerTime(long answerTime) {
        this.answerTime = answerTime;
    }

    public long getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(long timeTaken) {
        this.timeTaken = timeTaken;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public boolean isProcessed() {
        return processed;
    }

    public void setProcessed(boolean processed) {
        this.processed = processed;
    }

    public MultiplierPowerType getMultiplierPower() {
        return multiplierPower;
    }

    public void setMultiplierPower(MultiplierPowerType multiplierPower) {
        this.multiplierPower = multiplierPower;
    }

    public int getBasePoints() {
        return basePoints;
    }

    public void setBasePoints(int basePoints) {
        this.basePoints = basePoints;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    public String getAnswerString() {
        return answerString;
    }

    public void setAnswerString(String answerString) {
        this.answerString = answerString;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isGrace() {
        return grace;
    }

    public void setGrace(boolean grace) {
        this.grace = grace;
    }

    public void addAnswer(List<String> answer) {
        if (CollectionUtils.isEmpty(answer)) {
            return;
        }
        this.answerString = (toAnswerString(answer));
        this.answerTime = System.currentTimeMillis();
        this.timeTaken = this.answerTime - super.getCreationTime();
    }

    public String toAnswerString(List<String> answer) {
        if (!CollectionUtils.isEmpty(answer)) {
            String answers = answer.get(0);
            for (int i = 1; i < answer.size(); i++) {
                answers += "," + answer.get(i);
            }
            return answers;
        } else {
            return "";
        }
    }

    public List<String> getAnswerList() {
        if (StringUtils.isEmpty(this.answerString)) {
            return null;
        }
        String[] answerArr = this.answerString.split("\\,");

        return new ArrayList<>(Arrays.asList(answerArr));
    }
}
