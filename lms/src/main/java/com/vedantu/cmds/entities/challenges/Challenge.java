package com.vedantu.cmds.entities.challenges;

import com.vedantu.cmds.entities.AbstractCMDSEntity;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.challenges.ChallengeType;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.challenges.ChallengeQuestion;
import com.vedantu.cmds.response.challenges.ChallengeLeaderBoardInfo;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.ReprocessType;

import java.util.List;
import java.util.Set;
import org.springframework.data.mongodb.core.index.Indexed;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Challenge")
public class Challenge extends AbstractCMDSEntity {

    private String title;
    private ChallengeType type = ChallengeType.BURST_LATER;
    @Indexed(background = true)
    private ChallengeStatus status = ChallengeStatus.DRAFT;
    private Long startTime;
    private Long burstTime;
    private Integer duration = 0;
    private Integer maxPoints = 0;
    private Difficulty difficulty;
    private List<ChallengeQuestion> questions;
    private Set<QuestionType> questionTypes;
    private String category;
    private List<Long> lastAttemptedUserIds;
    private ChallengeLeaderBoardInfo leader;
    private ReprocessType reprocess;

    //public BidType bidType;
//    public int minTargets;
//    public int bidPool;
    //public int initailBidPool;
    //public int maxBid;
    public Challenge() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ChallengeType getType() {
        return type;
    }

    public void setType(ChallengeType type) {
        this.type = type;
    }

    public ChallengeStatus getStatus() {
        return status;
    }

    public void setStatus(ChallengeStatus status) {
        this.status = status;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    public Long getBurstTime() {
        return burstTime;
    }

    public void setBurstTime(Long burstTime) {
        this.burstTime = burstTime;
    }

    public List<ChallengeQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<ChallengeQuestion> questions) {
        this.questions = questions;
    }

    public Set<QuestionType> getQuestionTypes() {
        return questionTypes;
    }

    public void setQuestionTypes(Set<QuestionType> questionTypes) {
        this.questionTypes = questionTypes;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<Long> getLastAttemptedUserIds() {
        return lastAttemptedUserIds;
    }

    public void setLastAttemptedUserIds(List<Long> lastAttemptedUserIds) {
        this.lastAttemptedUserIds = lastAttemptedUserIds;
    }

    public ChallengeLeaderBoardInfo getLeader() {
        return leader;
    }

    public void setLeader(ChallengeLeaderBoardInfo leader) {
        this.leader = leader;
    }

    public ReprocessType getReprocess() {
        return reprocess;
    }

    public void setReprocess(ReprocessType reprocess) {
        this.reprocess = reprocess;
    }

    public static class Constants extends AbstractCMDSEntity.Constants {

        public static final String STATUS = "status";
        public static final String TYPE = "type";
        public static final String QUESTION_TYPES = "questionTypes";
        public static final String DIFFICULTY = "difficulty";
        public static final String CATEGORY = "category";
        public static final String BURST_TIME = "burstTime";
        public static final String START_TIME = "startTime";
    }

}
