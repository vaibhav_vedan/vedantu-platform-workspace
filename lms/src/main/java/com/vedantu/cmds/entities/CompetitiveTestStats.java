/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities;

import com.vedantu.cmds.pojo.UserCompetitiveTestInfo;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class CompetitiveTestStats extends AbstractCMDSEntity {
    
    private String cmdsTestId;
    private Float zScore;
    private Integer studentsAbove65;
    private Integer studentsAbove75;
    private Integer studentsAboveZScore;
    private Float averageScore;
    private List<UserCompetitiveTestInfo> userCompetitiveTestInfos = new ArrayList<>();

    /**
     * @return the cmdsTestId
     */
    public String getCmdsTestId() {
        return cmdsTestId;
    }

    /**
     * @param cmdsTestId the cmdsTestId to set
     */
    public void setCmdsTestId(String cmdsTestId) {
        this.cmdsTestId = cmdsTestId;
    }

    /**
     * @return the zScore
     */
    public Float getzScore() {
        return zScore;
    }

    /**
     * @param zScore the zScore to set
     */
    public void setzScore(Float zScore) {
        this.zScore = zScore;
    }

    /**
     * @return the studentsAbove65
     */
    public Integer getStudentsAbove65() {
        return studentsAbove65;
    }

    /**
     * @param studentsAbove65 the studentsAbove65 to set
     */
    public void setStudentsAbove65(Integer studentsAbove65) {
        this.studentsAbove65 = studentsAbove65;
    }

    /**
     * @return the studentsAbove75
     */
    public Integer getStudentsAbove75() {
        return studentsAbove75;
    }

    /**
     * @param studentsAbove75 the studentsAbove75 to set
     */
    public void setStudentsAbove75(Integer studentsAbove75) {
        this.studentsAbove75 = studentsAbove75;
    }

    /**
     * @return the studentsAboveZScore
     */
    public Integer getStudentsAboveZScore() {
        return studentsAboveZScore;
    }

    /**
     * @param studentsAboveZScore the studentsAboveZScore to set
     */
    public void setStudentsAboveZScore(Integer studentsAboveZScore) {
        this.studentsAboveZScore = studentsAboveZScore;
    }

    /**
     * @return the userCompetitiveTestInfos
     */
    public List<UserCompetitiveTestInfo> getUserCompetitiveTestInfos() {
        return userCompetitiveTestInfos;
    }

    /**
     * @param userCompetitiveTestInfos the userCompetitiveTestInfos to set
     */
    public void setUserCompetitiveTestInfos(List<UserCompetitiveTestInfo> userCompetitiveTestInfos) {
        this.userCompetitiveTestInfos = userCompetitiveTestInfos;
    }

    /**
     * @return the averageScore
     */
    public Float getAverageScore() {
        return averageScore;
    }

    /**
     * @param averageScore the averageScore to set
     */
    public void setAverageScore(Float averageScore) {
        this.averageScore = averageScore;
    }

}
