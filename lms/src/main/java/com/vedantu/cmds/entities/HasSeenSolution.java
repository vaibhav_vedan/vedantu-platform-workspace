/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.entities;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */

@Document(collection = "HasSeenSolution")
public class HasSeenSolution extends AbstractMongoStringIdEntity {

    private String questionId;
    private String solutionId;
    @Indexed(background = true)
    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public String getSolutionId() {
        return solutionId;
    }

    public void setSolutionId(String solutionId) {
        this.solutionId = solutionId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String USER_ID = "userId";
        public static final String QUESTION_ID = "questionId";
    }

}
