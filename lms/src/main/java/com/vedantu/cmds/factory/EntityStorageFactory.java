package com.vedantu.cmds.factory;

import java.util.HashMap;
import java.util.Map;

import com.vedantu.cmds.entities.AbstractEntityFileStorage;
import com.vedantu.cmds.entities.storage.CMDSQuestionEntityFileStorage;
import com.vedantu.cmds.entities.storage.DocumentEntityFileStorage;
import com.vedantu.cmds.entities.storage.FileStorage;
import com.vedantu.cmds.enums.CMDSEntityType;

public class EntityStorageFactory {

	public static EntityStorageFactory INSTANCE = new EntityStorageFactory();

	private Map<CMDSEntityType, AbstractEntityFileStorage> entityTypeStorageFactory = new HashMap<CMDSEntityType, AbstractEntityFileStorage>();

	private EntityStorageFactory() {
		entityTypeStorageFactory.put(CMDSEntityType.DOCUMENT, new DocumentEntityFileStorage());
		entityTypeStorageFactory.put(CMDSEntityType.CMDSQUESTION, new CMDSQuestionEntityFileStorage());
		entityTypeStorageFactory.put(CMDSEntityType.FILE, new FileStorage());
	}

	public AbstractEntityFileStorage get(CMDSEntityType entityType) {

		if (entityTypeStorageFactory.containsKey(entityType)) {
			return entityTypeStorageFactory.get(entityType);
		}

		return null;
	}
}
