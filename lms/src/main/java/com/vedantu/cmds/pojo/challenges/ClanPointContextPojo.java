package com.vedantu.cmds.pojo.challenges;

import com.vedantu.cmds.enums.challenges.ClanPointsContextType;

public class ClanPointContextPojo {

	private int burstPoints;
	private int musclePoints;
	private int picChallengePoints;
	private ClanPointsContextType contextType;

	public ClanPointsContextType getContextType() {
		return contextType;
	}

	public void setContextType(ClanPointsContextType contextType) {
		this.contextType = contextType;
	}

	public int getBurstPoints() {
		return burstPoints;
	}

	public void setBurstPoints(int burstPoints) {
		this.burstPoints = burstPoints;
	}

	public int getMusclePoints() {
		return musclePoints;
	}

	public void setMusclePoints(int musclePoints) {
		this.musclePoints = musclePoints;
	}

	public int getPicChallengePoints() {
		return picChallengePoints;
	}

	public void setPicChallengePoints(int picChallengePoints) {
		this.picChallengePoints = picChallengePoints;
	}
}
