/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo.challenges;

/**
 *
 * @author ajith
 */
public class ChallengeAttemptsCount {
    private String challengeId;
    private int attempts;

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public int getAttempts() {
        return attempts;
    }

    public void setAttempts(int attempts) {
        this.attempts = attempts;
    }

    @Override
    public String toString() {
        return "ChallengeAttemptsCount{" + "challengeId=" + challengeId + ", attempts=" + attempts + '}';
    }
}
