/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import com.vedantu.cmds.entities.CMDSVideo;

/**
 *
 * @author parashar
 */
public class CMDSVideoBasicInfo extends CMDSVideo {
    
    private int likeCount = 0;
    private boolean liked = false;
    private boolean disliked = false;
    private int dislikeCount = 0;
    private boolean bookmarked = false;

    /**
     * @return the likeCount
     */
    public int getLikeCount() {
        return likeCount;
    }

    /**
     * @param likeCount the likeCount to set
     */
    public void setLikeCount(int likeCount) {
        this.likeCount = likeCount;
    }

    /**
     * @return the liked
     */
    public boolean isLiked() {
        return liked;
    }

    /**
     * @param liked the liked to set
     */
    public void setLiked(boolean liked) {
        this.liked = liked;
    }

    /**
     * @return the disliked
     */
    public boolean isDisliked() {
        return disliked;
    }

    /**
     * @param disliked the disliked to set
     */
    public void setDisliked(boolean disliked) {
        this.disliked = disliked;
    }

    /**
     * @return the dislikeCount
     */
    public int getDislikeCount() {
        return dislikeCount;
    }

    /**
     * @param dislikeCount the dislikeCount to set
     */
    public void setDislikeCount(int dislikeCount) {
        this.dislikeCount = dislikeCount;
    }

    /**
     * @return the bookmarked
     */
    public boolean isBookmarked() {
        return bookmarked;
    }

    /**
     * @param bookmarked the bookmarked to set
     */
    public void setBookmarked(boolean bookmarked) {
        this.bookmarked = bookmarked;
    }
    
}
