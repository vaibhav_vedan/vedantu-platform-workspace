package com.vedantu.cmds.pojo;

import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.moodle.entity.ContentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ContentDetails{
    private ContentInfo contentInfo;
    private CMDSTest cmdsTest;
    private String userId;
}
