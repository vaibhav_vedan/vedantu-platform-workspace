package com.vedantu.cmds.pojo;

public class ExportTestAttemptPojo {

    public String courseName;
    public String courseId;
    public String batchId;
    public String contextId;
    public String engagementType;
    public String studentName;
    public String studentId;
    public String studentEmail;
    public String teacherId;
    public String teacherName;
    public String testStatus;
    public Float marksScored;
    public Integer attempted;
    public Integer unattempted;
    public Integer correct;
    public Integer inCorrect;
    public Long attemptedOn;
    public Long duration;
    public Integer rank;
    public Float percentile;
    public Integer attemptNumber;
    public String attemptedOnString;
    public String testEndType;
    public Float maxMarks;
    public int timeTaken;
    public String grade;
    public String contactNumber;


    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Float getMaxMarks() {
        return maxMarks;
    }

    public void setMaxMarks(Float maxMarks) {
        this.maxMarks = maxMarks;
    }

    public int getTimeTaken() {
        return timeTaken;
    }

    public void setTimeTaken(int timeTaken) {
        this.timeTaken = timeTaken;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getEngagementType() {
        return engagementType;
    }

    public void setEngagementType(String engagementType) {
        this.engagementType = engagementType;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentEmail() {
        return studentEmail;
    }

    public void setStudentEmail(String studentEmail) {
        this.studentEmail = studentEmail;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

    public Float getMarksScored() {
        return marksScored;
    }

    public void setMarksScored(Float marksScored) {
        this.marksScored = marksScored;
    }

    public Integer getAttempted() {
        return attempted;
    }

    public void setAttempted(Integer attempted) {
        this.attempted = attempted;
    }

    public Integer getUnattempted() {
        return unattempted;
    }

    public void setUnattempted(Integer unattempted) {
        this.unattempted = unattempted;
    }

    public Integer getCorrect() {
        return correct;
    }

    public void setCorrect(Integer correct) {
        this.correct = correct;
    }

    public Integer getInCorrect() {
        return inCorrect;
    }

    public void setInCorrect(Integer inCorrect) {
        this.inCorrect = inCorrect;
    }

    public Long getAttemptedOn() {
        return attemptedOn;
    }

    public void setAttemptedOn(Long attemptedOn) {
        this.attemptedOn = attemptedOn;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Float getPercentile() {
        return percentile;
    }

    public void setPercentile(Float percentile) {
        this.percentile = percentile;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public Integer getAttemptNumber() {
        return attemptNumber;
    }

    public void setAttemptNumber(Integer attemptNumber) {
        this.attemptNumber = attemptNumber;
    }

    public String getAttemptedOnString() {
        return attemptedOnString;
    }

    public void setAttemptedOnString(String attemptedOnString) {
        this.attemptedOnString = attemptedOnString;
    }

    public String getTestEndType() {
        return testEndType;
    }

    public void setTestEndType(String testEndType) {
        this.testEndType = testEndType;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

}
