package com.vedantu.cmds.pojo;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.CollectionUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import static com.vedantu.platform.enums.SocialContextType.CMDSTEST;

@Data
@NoArgsConstructor
public class CMDSTestInfo implements HomeFeedCardData{
	private String id;
	private String testUrl;
	private Long duration;
	private CMDSTestState testState;
	private List<CMDSQuestion> questions = new ArrayList<CMDSQuestion>();
	private int versionNo = 1;
	private String questionSetId;
    private SocialContextType socialContextType = CMDSTEST;
	public Set<String> boardIds;
	public Set<String> targetIds;
	public Set<String> grades;
	public Set<String> tags;
	public VedantuRecordState recordState = VedantuRecordState.ACTIVE;
	public String name;
	public float totalMarks;
	public ContentInfoType contentInfoType;
	private AccessLevel accessLevel;
	private SignUpRestrictionLevel signupHook;
	private List<String> instructions;
	private List<SectionWiseInstruction> sectionWiseInstructions;
	public EnumBasket.TestTagType testTag;
	public Long minStartTime;
	public Long maxStartTime;
	public Long creationTime;
	private boolean savedState = false;
	private long lastUpdated;
	private boolean isAttempted;
	private TypeOfObjectiveTest typeOfObjectiveTest;
	public boolean reattemptAllowed;

	public CMDSTestInfo(CMDSTest cmdsTest) {
		super();
		this.id = cmdsTest.getId();
		this.duration = cmdsTest.getDuration();
		this.testState = cmdsTest.getTestState();
		this.versionNo = cmdsTest.getVersionNo();
		this.questionSetId = cmdsTest.getQuestionSetId();
		this.boardIds = cmdsTest.boardIds;
		this.targetIds = cmdsTest.targetIds;
		this.grades = cmdsTest.grades;
		this.tags = cmdsTest.tags;
		this.recordState = cmdsTest.recordState;
		this.name = cmdsTest.getName();
		this.totalMarks = cmdsTest.getTotalMarks();
		this.contentInfoType = cmdsTest.getContentInfoType();
		this.accessLevel = cmdsTest.getAccessLevel();
		this.signupHook = cmdsTest.getSignupHook();
		this.instructions=cmdsTest.getInstructions();
		this.sectionWiseInstructions = cmdsTest.getSectionWiseInstructions();
		this.testTag = cmdsTest.getTestTag();
		this.minStartTime = cmdsTest.getMinStartTime();
		this.maxStartTime = cmdsTest.getMaxStartTime();
		this.creationTime = cmdsTest.getCreationTime();
		this.typeOfObjectiveTest=cmdsTest.getTypeOfObjectiveTest();
		this.reattemptAllowed=cmdsTest.isReattemptAllowed();
	}

	public float calculateTotalMarks() {
		float totalMarks = 0;
		if (!CollectionUtils.isEmpty(this.questions)) {
			for (CMDSQuestion cmdsQuestion : this.questions) {
				totalMarks += cmdsQuestion.getMarks().getPositive();
			}
		}

		String formattedString = String.format("%.02f", totalMarks);
		return Float.parseFloat(formattedString);
	}

	public void addQuestion(CMDSQuestion question) {
		this.questions.add(question);
	}
}
