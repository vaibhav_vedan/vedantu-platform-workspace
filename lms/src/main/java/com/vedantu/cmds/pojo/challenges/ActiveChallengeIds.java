package com.vedantu.cmds.pojo.challenges;

import com.vedantu.cmds.entities.challenges.Challenge;

public class ActiveChallengeIds {

	private String category;
	private String challengeId;
	private Challenge challenge;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getChallengeId() {
		return challengeId;
	}

	public void setChallengeId(String challengeId) {
		this.challengeId = challengeId;
	}

	public Challenge getChallenge() {
		return challenge;
	}

	public void setChallenge(Challenge challenge) {
		this.challenge = challenge;
	}

}
