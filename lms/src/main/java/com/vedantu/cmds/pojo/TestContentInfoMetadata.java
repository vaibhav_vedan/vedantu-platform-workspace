package com.vedantu.cmds.pojo;

import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import org.bson.types.ObjectId;

/**
 * Created by somil on 22/03/17.
 */
@Data
public class TestContentInfoMetadata extends AbstractMongoStringIdEntity {
    private String testId;
    private Long duration;
    private Float totalMarks;
    private Integer noOfQuestions;

    @Deprecated
    private Integer rank;
    @Deprecated
    private Float percentage;
    @Deprecated
    private Float percentile;

    @Deprecated
    private List<CMDSTestAttempt> testAttempts;
    private Long minStartTime;
    private Long maxStartTime;
    private boolean hardStop=false;
    private boolean reattemptAllowed = false;
    private TestResultVisibility displayResultOnEnd = TestResultVisibility.VISIBLE;
    private String displayMessageOnEnd;
    private SignUpRestrictionLevel signupHook;

    private Long expiryDate;// Only for subjective public test
    private Long expiryDays;// Only for subjective private test

    private TypeOfObjectiveTest typeOfObjectiveTest; // for objective test only

    public TestContentInfoMetadata() {
        super();
        setId(ObjectId.get().toString());
    }
    public TestContentInfoMetadata(CMDSTest cmdsTest) {
        super();
        setId(ObjectId.get().toString());
        testId = cmdsTest.getId();
        minStartTime = cmdsTest.getMinStartTime();
        maxStartTime = cmdsTest.getMaxStartTime();
        reattemptAllowed = cmdsTest.isReattemptAllowed();
        duration = cmdsTest.getDuration();
        totalMarks = cmdsTest.getTotalMarks();
        noOfQuestions = cmdsTest.getQusCount();
        displayResultOnEnd = cmdsTest.getResultVisibility();
        displayMessageOnEnd = cmdsTest.getResultVisibilityMessage();
        signupHook = cmdsTest.getSignupHook();
        hardStop=cmdsTest.isHardStop();
        expiryDate=cmdsTest.getExpiryDate();
        expiryDays=cmdsTest.getExpiryDays();
        typeOfObjectiveTest=cmdsTest.getTypeOfObjectiveTest();
    }
}
