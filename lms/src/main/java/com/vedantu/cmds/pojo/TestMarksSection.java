/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

/**
 *
 * @author parashar
 */
public class TestMarksSection {

    private Float minMarks;
    private Float maxMarks;
    private boolean userPresent = false;
    private Integer numOfUsers;

    /**
     * @return the minMarks
     */
    public Float getMinMarks() {
        return minMarks;
    }

    /**
     * @param minMarks the minMarks to set
     */
    public void setMinMarks(Float minMarks) {
        this.minMarks = minMarks;
    }

    /**
     * @return the maxMarks
     */
    public Float getMaxMarks() {
        return maxMarks;
    }

    /**
     * @param maxMarks the maxMarks to set
     */
    public void setMaxMarks(Float maxMarks) {
        this.maxMarks = maxMarks;
    }

    /**
     * @return the userPresent
     */
    public boolean isUserPresent() {
        return userPresent;
    }

    /**
     * @param userPresent the userPresent to set
     */
    public void setUserPresent(boolean userPresent) {
        this.userPresent = userPresent;
    }

    /**
     * @return the numOfUsers
     */
    public Integer getNumOfUsers() {
        return numOfUsers;
    }

    /**
     * @param numOfUsers the numOfUsers to set
     */
    public void setNumOfUsers(Integer numOfUsers) {
        this.numOfUsers = numOfUsers;
    }

}