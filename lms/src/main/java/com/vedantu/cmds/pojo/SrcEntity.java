package com.vedantu.cmds.pojo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.utils.ObjectIdUtils;

public class SrcEntity implements Serializable, Comparable<SrcEntity> {

	private static final long serialVersionUID = 1L;

	public CMDSEntityType type;
	public String entityId;

	public SrcEntity() {

	}

	public SrcEntity(CMDSEntityType type, String id) {

		this.type = type;
		this.entityId = id;
	}

	public SrcEntity(SrcEntity toBeCopied) {

		this.type = toBeCopied.type;
		this.entityId = toBeCopied.entityId;
	}

        public CMDSEntityType getType() {
            return type;
        }

        public void setType(CMDSEntityType type) {
            this.type = type;
        }

        public String getEntityId() {
            return entityId;
        }

        public void setEntityId(String entityId) {
            this.entityId = entityId;
        }
        
        

	@Override
	public boolean equals(Object o) {

		if (null == o || !(o instanceof SrcEntity)) {
			return false;
		}
		SrcEntity e = (SrcEntity) o;
		return type != null && type == e.type && entityId != null && entityId.equals(e.entityId);
	}

	@Override
	public int hashCode() {

		// commented below by Shankhoneer
		// return type == null ? null : (type.name() + ":" + entityId).hashCode();
		// if the type is null hash code will be zero.Now in case the type is
		// null there will be no NPE when JVM tries to
		// compute hash
		return type == null ? 0 : (type.name() + ":" + entityId).hashCode();
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("{type:").append(type).append(", id:").append(entityId).append("}");
		return builder.toString();
	}

	@Override
	public int compareTo(SrcEntity o) {

		return this.entityId.compareTo(o.entityId);
	}

	public String validate() {

		if (type == null || ObjectIdUtils.hasInvalidId(entityId)) {
			return "entity is invalid";
		}
		return null;
	}

	public static List<String> toIds(Collection<SrcEntity> entities) {

		List<String> ids = new ArrayList<String>();
		for (SrcEntity entity : entities) {
			ids.add(entity.entityId);
		}
		return ids;
	}

	public static Map<CMDSEntityType, ArrayList<SrcEntity>> convertToMap(List<SrcEntity> contents) {

		Map<CMDSEntityType, ArrayList<SrcEntity>> mappedEntities = new HashMap<>();
		for (SrcEntity content : contents) {

			if (!mappedEntities.containsKey(content.type)) {
				ArrayList<SrcEntity> entities = new ArrayList<>();

				mappedEntities.put(content.type, entities);
			}

			mappedEntities.get(content.type).add(content);
		}
		return mappedEntities;
	}
}