package com.vedantu.cmds.pojo.metadata;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.cmds.pojo.OptionFormat;
import com.vedantu.cmds.pojo.SolutionInfo;
import com.vedantu.lms.cmds.utils.LatexProcessor;
import com.vedantu.util.ArrayUtils;

@SuppressWarnings("serial")
public class MCQsolutionInfo extends SolutionInfo {

	public List<String> answer;

	public MCQsolutionInfo() {
		this(new OptionFormat(), new ArrayList<String>());
	}

	public MCQsolutionInfo(OptionFormat op, List<String> answers) {
		super(op);
		this.answer = answers;
	}

	@Override
	public void addHook() {
		super.addHook();
		if (answer != null && !answer.isEmpty()) {
			List<String> newAnswers = new ArrayList<String>();
			for (String option : answer) {
				newAnswers.add(LatexProcessor.addHookToLatex(option));
			}
			answer = newAnswers;
		}

	}

	@Override
	public List<String> getAnswer() {
		return answer;
	}
        @Override
        public void setAnswer(List<String> answer) {
            if(ArrayUtils.isNotEmpty(answer)){
                this.answer=answer;
            }else{
                this.answer=null;
            }
        }        
}
