package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.pojo.Marks;

import java.util.Set;

public class CMDSQuestionIdInfo {
	private String questionId;
	private Set<String> tags;
	private Marks defaultMarks;

	public CMDSQuestionIdInfo() {
		super();
	}

	public CMDSQuestionIdInfo(String questionId, Marks defaultMarks) {
		super();
		this.questionId = questionId;
		this.defaultMarks = defaultMarks;
	}

	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	public Marks getDefaultMarks() {
		return defaultMarks;
	}

	public void setDefaultMarks(Marks defaultMarks) {
		this.defaultMarks = defaultMarks;
	}

	public Set<String> getTags() {
		return tags;
	}

	public void setTags(Set<String> tags) {
		this.tags = tags;
	}

	@Override
	public String toString() {
		return "CMDSQuestionIdInfo [questionId=" + questionId + ", tags=" + tags + ", defaultMarks=" + defaultMarks
				+ ", toString()=" + super.toString() + "]";
	}
}
