package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.ArrayUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by somil on 20/03/17.
 */
@Data
public class TestDetails {

    private QuestionType type;
    private int qusCount;

    private Marks marks;

    private List<String> qids;

    public TestDetails() {
        this.qids=new ArrayList<>();
    }

    public TestDetails(QuestionType type,Marks marks) {
        this.type = type;
        this.marks=marks;
        this.qids=new ArrayList<>();
    }

    public void addQid(String qid){
        if(ArrayUtils.isEmpty(qids)){
            this.qids=new ArrayList<>();
        }
        this.qids.add(qid);
    }

    public void removeQid(String qid){
        if(ArrayUtils.isNotEmpty(qids)){
            qids.remove(qid);
        }
    }

    public void mutateDetailData(Map<String, String> questionDuplicateMap) {
        if(ArrayUtils.isNotEmpty(qids)){
            List<String> toBeRemoved =
                    this.qids
                            .stream()
                            .filter(questionDuplicateMap::containsKey)
                            .collect(Collectors.toList());
            this.qids.removeAll(toBeRemoved);
            this.qids.addAll(
                    toBeRemoved
                            .stream()
                            .map(questionDuplicateMap::get)
                            .collect(Collectors.toList())
            );
        }
    }
}
