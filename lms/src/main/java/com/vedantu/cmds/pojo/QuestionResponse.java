package com.vedantu.cmds.pojo;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.lms.cmds.enums.AttemptStateOfQuestion;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.pojo.Marks;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

/**
 * Created by somil on 23/03/17.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionResponse {
    private String questionId;
    private String questionText;
    private List<Option> options;
    private List<String> answerGiven;
    private Set<String> topics;
    private Set<String> mainTags;
    private Difficulty difficulty;
    private int questionIndex;
    private QuestionType questionType;
    private Marks marks;
    private SolutionInfo solution;
    private AttemptStateOfQuestion attemptStateOfQuestion;

	@Override
    public String toString() {
        return "QuestionResponse{" +
                "questionId='" + questionId + '\'' +
                ", questionText='" + questionText + '\'' +
                ", options=" + options +
                ", questionType=" + questionType +
                ", marksGiven=" + marks +
                '}';
    }
}
