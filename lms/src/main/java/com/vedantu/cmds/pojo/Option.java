package com.vedantu.cmds.pojo;

/**
 * Created by somil on 24/03/17.
 */
public class Option {
    private String index;
    private String label;
    private String text;


    public String getIndex() {
        return index;
    }

    public void setIndex(String index) {
        this.index = index;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        return "Option{" +
                "index='" + index + '\'' +
                ", label='" + label + '\'' +
                ", text='" + text + '\'' +
                '}';
    }
}
