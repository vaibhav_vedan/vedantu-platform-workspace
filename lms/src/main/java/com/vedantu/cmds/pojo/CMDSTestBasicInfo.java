/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import com.vedantu.cmds.entities.AbstractCMDSEntity;
import com.vedantu.cmds.enums.TestMode;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.SignUpRestrictionLevel;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @author ajith
 */
@Data
@NoArgsConstructor
public class CMDSTestBasicInfo extends AbstractCMDSEntity {

    private CMDSTestState testState;
    private int versionNo = 1;

    public String description;
    public int qusCount;
    public Long duration;
    public Float totalMarks = 0f;
    public ContentInfoType contentInfoType;

    public EnumBasket.TestType type;

    public TestMode mode;
    public String code;
    private boolean hardStop=false;
    public Long minStartTime;
    public Long maxStartTime;
    public boolean reattemptAllowed = false;
    private SignUpRestrictionLevel signupHook;
    private List<String> instructions;
    private List<SectionWiseInstruction> sectionWiseInstructions;
    private List<CustomSectionEvaluationRule> customSectionEvaluationRules;
    private TypeOfObjectiveTest typeOfObjectiveTest; // for objective test only
}
