package com.vedantu.cmds.pojo;

import com.vedantu.cmds.entities.CMDSQuestion;

public class AMQuestionAttempt {
	
	private CMDSQuestion question;
	private String attempt;
	
	public AMQuestionAttempt() {
		super();
	}
	
	public AMQuestionAttempt(CMDSQuestion question) {
		super();
		this.question = question;
	}
	
	public AMQuestionAttempt(CMDSQuestion question, String attempt) {
		super();
		this.question = question;
		this.attempt = attempt;
	}
	
	public CMDSQuestion getQuestion() {
		return question;
	}
	public void setQuestion(CMDSQuestion question) {
		this.question = question;
	}
	public String getAttempt() {
		return attempt;
	}
	public void setAttempt(String attempt) {
		this.attempt = attempt;
	}

}
