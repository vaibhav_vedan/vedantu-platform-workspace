package com.vedantu.cmds.pojo.metadata;

import org.apache.commons.lang.StringUtils;

import com.vedantu.cmds.pojo.OptionFormat;
import com.vedantu.cmds.pojo.SolutionInfo;
import com.vedantu.lms.cmds.utils.LatexProcessor;
import com.vedantu.util.ArrayUtils;

import java.util.Arrays;
import java.util.List;

//@SuppressWarnings("serial")
//@JsonTypeInfo(use=JsonTypeInfo.Id.NAME, include=JsonTypeInfo.As.PROPERTY, property="tclass")
public class SCQSolutionInfo extends SolutionInfo {

	private static final long serialVersionUID = 1L;
	public String answer;

	public SCQSolutionInfo() {
		this(new OptionFormat(), new String());
	}

	public SCQSolutionInfo(OptionFormat op, String answer) {
		super(op);
		this.answer = answer;
	}

	@Override
	public void addHook() {
		super.addHook();
		if (StringUtils.isNotEmpty(answer)) {
			answer = LatexProcessor.addHookToLatex(answer);
		}

	}

	@Override
	public List<String> getAnswer() {
            if(StringUtils.isEmpty(answer)){
                return null;
            }else{
                return Arrays.asList(answer);
            }
	}

        @Override
        public void setAnswer(List<String> answer) {
            if(ArrayUtils.isNotEmpty(answer)){
                this.answer=answer.get(0);
            }else{
                this.answer=null;
            }
        }
}
