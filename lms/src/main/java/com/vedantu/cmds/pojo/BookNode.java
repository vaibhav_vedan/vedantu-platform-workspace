/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import com.vedantu.cmds.enums.BookNodeType;
import java.util.List;

/**
 *
 * @author ajith
 */
public class BookNode {

    private BookNodeType type;
    private String name;
    private String displayName;
    private List<String> pageNos;
    private List<BookNode> nodes;
    private String serialNumber;

    public BookNodeType getType() {
        return type;
    }

    public void setType(BookNodeType type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getPageNos() {
        return pageNos;
    }

    public void setPageNos(List<String> pageNos) {
        this.pageNos = pageNos;
    }

    public List<BookNode> getNodes() {
        return nodes;
    }

    public void setNodes(List<BookNode> nodes) {
        this.nodes = nodes;
    }

    public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	@Override
    public String toString() {
        return "BookNode{" + "type=" + type + ", name=" + name + ", pageNos=" + pageNos + ", nodes=" + nodes + '}';
    }
    


}
