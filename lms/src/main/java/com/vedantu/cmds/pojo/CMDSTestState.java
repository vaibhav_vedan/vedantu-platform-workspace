package com.vedantu.cmds.pojo;

public enum CMDSTestState {
	DRAFT, PRIVATE, PUBLIC
}
