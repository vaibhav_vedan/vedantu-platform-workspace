package com.vedantu.cmds.pojo;

import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.enums.OnboardingVideoType;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@NoArgsConstructor
public class OnboardingVideoData {
    private String videoId;
    private String videoUrl;
    private OnboardingVideoType onboardingVideoType;
    private String thumbnail;
    private ArrayList<String> grades;

    public OnboardingVideoData(CMDSVideo cmdsVideo){
        this.videoId=cmdsVideo.getId();
        this.videoUrl=cmdsVideo.getVideoUrl();
        this.onboardingVideoType=cmdsVideo.getOnboardingVideoType();
        this.grades=new ArrayList<>(cmdsVideo.gettGrades());
        this.thumbnail=cmdsVideo.getThumbnail();
    }
}
