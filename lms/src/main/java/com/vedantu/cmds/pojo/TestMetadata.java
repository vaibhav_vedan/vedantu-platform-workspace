package com.vedantu.cmds.pojo;

import com.vedantu.cmds.enums.OrderType;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.StringUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Created by somil on 20/03/17.
 */
@Data
public class TestMetadata {

    public String brdId; // board id
    public String name;// board name
    public String slug;
    public int qusCount;

    public List<TestDetails> details;
    public Float totalMarks;

    public List<BoardQus> children;

    public List<CMDSTestQuestion> questions;

    public OrderType qOrderType;

    public TestMetadata() {
        this.details=new ArrayList<>();
        this.children=new ArrayList<>();
        this.questions=new ArrayList<>();
    }

    public static String convertToSlug(String name) {
        String slug = null;
        if(StringUtils.isNotEmpty(name)) {
            slug = name.trim().toLowerCase().replace(' ', '_');
        }
        return slug;
    }

    public void mutateMetadata(Map<String, String> questionDuplicateMap, Map<String, Marks> questionMarksChangedMap){

        float toBeAddedToTotal=0;
        for (CMDSTestQuestion question : questions) {
            String questionId = question.getQuestionId();

            // Change the marks if it is necessary(to be done first, as in the next step question ids are going to be changed)
            if(questionMarksChangedMap.containsKey(questionId)){
                toBeAddedToTotal += questionMarksChangedMap.get(questionId).getPositive();
                toBeAddedToTotal-=question.getMarks().getPositive();
                question.setMarks(questionMarksChangedMap.get(questionId));
            }

            // Change the question ids if necessary
            if(questionDuplicateMap.containsKey(questionId)){
                question.setQuestionId(questionDuplicateMap.get(questionId));
            }

        }

        // Add the required value to the total marks
        totalMarks+=toBeAddedToTotal;

        // Mutate the details field
        this.details
            .stream()
            .forEach(detail->detail.mutateDetailData(questionDuplicateMap));

    }

}
