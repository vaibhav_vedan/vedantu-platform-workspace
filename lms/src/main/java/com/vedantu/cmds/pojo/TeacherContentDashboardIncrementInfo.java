package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherContentDashboardIncrementInfo {
    @NonNull
    private String teacherId;
    @NonNull
    private ContentInfoType contentInfoType;
    @NonNull
    private ContentState contentState;
    private long incrementCount=1;
}
