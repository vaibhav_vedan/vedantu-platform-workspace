/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 *
 * @author ashok
 */
@Data
@NoArgsConstructor
public class QuestionDurationAggregation {
    
    private String questionId;
    private Float averageDuration;

}
