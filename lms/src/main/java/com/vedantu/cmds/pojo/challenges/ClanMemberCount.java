package com.vedantu.cmds.pojo.challenges;

public class ClanMemberCount {

	private String clanId;
	private Integer count;

	public String getClanId() {
		return clanId;
	}

	public void setClanId(String clanId) {
		this.clanId = clanId;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}
}
