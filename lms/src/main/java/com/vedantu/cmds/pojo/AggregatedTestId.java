/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import java.util.List;

/**
 *
 * @author parashar
 */
public class AggregatedTestId {
    
    private String _id;
    private List<String> testIds;

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(String _id) {
        this._id = _id;
    }

    /**
     * @return the testIds
     */
    public List<String> getTestIds() {
        return testIds;
    }

    /**
     * @param testIds the testIds to set
     */
    public void setTestIds(List<String> testIds) {
        this.testIds = testIds;
    }
    
}
