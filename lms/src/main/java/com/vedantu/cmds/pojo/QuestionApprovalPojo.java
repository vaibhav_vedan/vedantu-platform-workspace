package com.vedantu.cmds.pojo;

import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.enums.CognitiveLevel;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionApprovalPojo {
    private String id;
    private CMDSApprovalStatus status; // UNIQUE, DUPLICATE
    private String subject;
    private Set<String> topics;
    private Set<String> subtopics;
    private Set<String> grades;
    private Set<String> targets;
    private Set<String> analysisTags;
    private Integer difficultyValue;
    private Set<String> targetGrade;
    private CognitiveLevel cognitiveLevel; // UNKNOWN, KNOWLEDGE, APPLICATION, ANALYSIS, HOTS , MEMORY,UNDERSTANDING
    private List<String> source;
    private QuestionType type;
    private String isACopyOf;
    private Marks marks;
    private boolean marksChanged=Boolean.FALSE;
    private String rejectionComment;

    public static Boolean isDirtyData(QuestionApprovalPojo question){
        if(StringUtils.isEmpty(question.getId()) || Objects.isNull(question.getType()) || Objects.isNull(question.getStatus()) ||
                ArrayUtils.isEmpty(question.getTopics()) || StringUtils.isEmpty(question.getSubject()) ){
            return Boolean.TRUE;
        }
        if(CMDSApprovalStatus.DUPLICATE.equals(question.getStatus()) && StringUtils.isEmpty(question.getIsACopyOf())
                && StringUtils.isEmpty(question.getSubject())){
            return Boolean.TRUE;
        }
        if(CMDSApprovalStatus.REJECTED.equals(question.getStatus()) && StringUtils.isEmpty(question.getRejectionComment())){
            return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }
    public void cleanTopicAndSubTopicAndAnalysisTopicData(){
        if(ArrayUtils.isNotEmpty(topics)){
            this.topics=topics.stream().map(this::replaceDollarWithComma).collect(Collectors.toSet());
        }
        if(ArrayUtils.isNotEmpty(subtopics)){
            this.subtopics=subtopics.stream().map(this::replaceDollarWithComma).collect(Collectors.toSet());
        }
        if(ArrayUtils.isNotEmpty(analysisTags)){
            this.analysisTags=analysisTags.stream().map(this::replaceDollarWithComma).collect(Collectors.toSet());
        }
    }

    private String replaceDollarWithComma(String topic) {
        return topic.replaceAll("\\$", ",");
    }
}
