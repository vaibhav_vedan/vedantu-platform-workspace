package com.vedantu.cmds.pojo;

import com.vedantu.cmds.entities.elasticsearch.CMDSQuestionBasicInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CMDSQuestionBasicInfoESPojo {
    private String _index;
    private String _type;
    private String _id;
    private Float _score;
    CMDSQuestionBasicInfo _source;
}
