/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

/**
 *
 * @author pranavm
 */
public class TeacherChangeTime {

    private Long changeTime;
    private Long changedBy;
    private String reason;
    private String previousTeacherId;
    private String newTeacherId;

    public TeacherChangeTime() {

    }

    public TeacherChangeTime(Long changeTime, Long changedBy, String reason, String previousTeacherId, String newTeacherId) {
        this.changeTime = changeTime;
        this.changedBy = changedBy;
        this.reason = reason;
        this.previousTeacherId = previousTeacherId;
        this.newTeacherId = newTeacherId;
    }

    public Long getChangeTime() {
        return changeTime;
    }

    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    public Long getChangedBy() {
        return changedBy;
    }

    public void setChangedBy(Long changedBy) {
        this.changedBy = changedBy;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPreviousTeacherId() {
        return previousTeacherId;
    }

    public void setPreviousTeacherId(String previousTeacherId) {
        this.previousTeacherId = previousTeacherId;
    }

    public String getNewTeacherId() {
        return newTeacherId;
    }

    public void setNewTeacherId(String newTeacherId) {
        this.newTeacherId = newTeacherId;
    }

    @Override
    public String toString() {
        return "TeacherChangeTime{" + "changeTime=" + changeTime + ", changedBy=" + changedBy + ", reason=" + reason + ", previousTeacherId=" + previousTeacherId + ", newTeacherId=" + newTeacherId + '}';
    }
    
    

}
