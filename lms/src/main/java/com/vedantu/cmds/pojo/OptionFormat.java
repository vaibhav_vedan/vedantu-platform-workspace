package com.vedantu.cmds.pojo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.lms.cmds.interfaces.ILatexProcessor;
import com.vedantu.lms.cmds.utils.LatexProcessor;
import com.vedantu.util.CollectionUtils;
import lombok.Data;

@Data
public class OptionFormat implements ILatexProcessor {

    public Set<CMDSImageDetails> uuidImages;
    public List<String> originalOptions;
    public List<String> newOptions;
    public List<String> optionOrder;

    public OptionFormat() {

        this.uuidImages = new HashSet<>();
        this.originalOptions = new ArrayList<>();
        this.newOptions = new ArrayList<>();
        this.optionOrder = new ArrayList<>();
    }

    @Override
    public void addHook() {

        if (CollectionUtils.isNotEmpty(newOptions)) {
            List<String> finalOption = new ArrayList<>();
            for (String option : this.newOptions) {
                finalOption.add(LatexProcessor.addHookToLatex(option));
            }

            this.newOptions = finalOption;
        }

    }

}
