package com.vedantu.cmds.pojo;

import java.util.List;
import java.util.Map;

public class VimeoVideoMetadata {

    private Integer duration;
//    private String thumbnail_url_with_play_button;
//    private Long video_id;
    private Map<String, String> pictures;

    public Map<String, String> getPictures() {
        return pictures;
    }

    public void setPictures(Map<String, String> pictures) {
        this.pictures = pictures;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

//    public String getThumbnail_url_with_play_button() {
//        return thumbnail_url_with_play_button;
//    }
//
//    public void setThumbnail_url_with_play_button(String thumbnail_url_with_play_button) {
//        this.thumbnail_url_with_play_button = thumbnail_url_with_play_button;
//    }
//
//    public Long getVideo_id() {
//        return video_id;
//    }
//
//    public void setVideo_id(Long video_id) {
//        this.video_id = video_id;
//    }


    @Override
    public String toString() {
        return String
                .format("{duration=%s, thumbnail_url_with_play_button=%s}",
                        getDuration());
    }
}
