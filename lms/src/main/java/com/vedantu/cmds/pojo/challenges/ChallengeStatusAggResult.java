/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo.challenges;

import com.vedantu.cmds.enums.challenges.ChallengeStatus;

/**
 *
 * @author ajith
 */
public class ChallengeStatusAggResult {
    private ChallengeStatus status;
    private int points;
    private int count;

    public ChallengeStatus getStatus() {
        return status;
    }

    public void setStatus(ChallengeStatus status) {
        this.status = status;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "ChallengeStatusAggResult{" + "status=" + status + ", points=" + points + ", count=" + count + '}';
    }
    
}
