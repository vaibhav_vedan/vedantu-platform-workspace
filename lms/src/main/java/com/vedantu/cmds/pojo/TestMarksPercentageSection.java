/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashMap;

/**
 *
 * @author ashok
 */
@Data
@NoArgsConstructor

public class TestMarksPercentageSection {
    
    private Float minMarksPct;
    private Float maxMarksPct;
    private boolean userPresent = false;
    private Integer pctOfUsers;
    private HashMap<String,?> subjects;

}
