package com.vedantu.cmds.pojo.challenges;

import java.util.List;

public class PictureChallengeAnswer {

	private String keyword;
	private List<String> variations;

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<String> getVariations() {
		return variations;
	}

	public void setVariations(List<String> variations) {
		this.variations = variations;
	}
}
