package com.vedantu.cmds.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CourseDetails {
    private String courseId;
    private String courseName;
    private Set<String> batchIds=new HashSet<>();

    @Override
    public int hashCode(){
        return courseId.hashCode()+courseName.hashCode();
    }

    @Override
    public boolean equals(Object courseDetailObject){
        if(!(courseDetailObject instanceof CourseDetails))
            return false;
        CourseDetails courseDetails=(CourseDetails)courseDetailObject;
        return courseId.equals(courseDetails.courseId);
    }
}
