package com.vedantu.cmds.pojo;

import java.util.Set;

import com.vedantu.lms.cmds.enums.AccessLevel;

/**
 * Created by somil on 24/03/17.
 */
public class AccessInfo {
    private AccessLevel accessLevel;
    Set<String> grades;

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    public Set<String> getGrades() {
        return grades;
    }

    public void setGrades(Set<String> grades) {
        this.grades = grades;
    }

    @Override
    public String toString() {
        return "AccessInfo{" +
                "accessLevel=" + accessLevel +
                ", grades=" + grades +
                '}';
    }
}
