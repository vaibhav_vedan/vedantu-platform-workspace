package com.vedantu.cmds.pojo.challenges;

import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;

public class ChallengeTakenHint extends AbstractMongoStringIdEntityBean {

    public String challengeId;
    public Long userId;
    public HintFormat hint;

    public ChallengeTakenHint() {

    }

    public ChallengeTakenHint(String challengeId, Long userId, HintFormat hint) {
        super();
        this.challengeId = challengeId;
        this.userId = userId;
        this.hint = hint;
    }

    public String getChallengeId() {
        return challengeId;
    }

    public void setChallengeId(String challengeId) {
        this.challengeId = challengeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public HintFormat getHint() {
        return hint;
    }

    public void setHint(HintFormat hint) {
        this.hint = hint;
    }

}
