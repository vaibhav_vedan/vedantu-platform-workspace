package com.vedantu.cmds.pojo.challenges;

import com.vedantu.cmds.enums.challenges.CreditCategory;

public class CreditInfo {

    public CreditCategory category;
    public int credits;

    public CreditInfo(CreditCategory category, int credits) {
        this.category = category;
        this.credits = credits;
    }

    @Override
    public int hashCode() {
        return (category == null) ? 0 : category.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass())
            return false;
        CreditInfo other = (CreditInfo) obj;
        return category == other.category;
    }

    public CreditCategory getCategory() {
        return category;
    }

    public void setCategory(CreditCategory category) {
        this.category = category;
    }

    public int getCredits() {
        return credits;
    }

    public void setCredits(int credits) {
        this.credits = credits;
    }

    @Override
    public String toString() {
        return "CreditInfo{" + "category=" + category + ", credits=" + credits + '}';
    }
    
}
