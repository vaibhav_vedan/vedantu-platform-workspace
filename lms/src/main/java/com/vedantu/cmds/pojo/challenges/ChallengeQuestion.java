/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo.challenges;

import com.vedantu.cmds.entities.CMDSQuestion;
import java.util.List;

/**
 *
 * @author ajith
 */
public class ChallengeQuestion {

    private String qid;
    private CMDSQuestion question;//will be used in response only
    private List<HintFormat> hints;
    private Integer maxPoints = 0;

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public List<HintFormat> getHints() {
        return hints;
    }

    public void setHints(List<HintFormat> hints) {
        this.hints = hints;
    }

    public Integer getMaxPoints() {
        return maxPoints;
    }

    public void setMaxPoints(Integer maxPoints) {
        this.maxPoints = maxPoints;
    }

    public CMDSQuestion getQuestion() {
        return question;
    }

    public void setQuestion(CMDSQuestion question) {
        this.question = question;
    }

    @Override
    public String toString() {
        return "ChallengeQuestion{" + "qid=" + qid + ", question=" + question + ", hints=" + hints + ", maxPoints=" + maxPoints + '}';
    }

}
