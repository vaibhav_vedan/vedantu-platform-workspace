/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo.challenges;

/**
 *
 * @author ajith
 */
public class PictureChallengeDayDeductions {

    private Long fromTime;
    private Long tillTime;
    private int pointsToWin;

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public Long getTillTime() {
        return tillTime;
    }

    public void setTillTime(Long tillTime) {
        this.tillTime = tillTime;
    }

    public int getPointsToWin() {
        return pointsToWin;
    }

    public void setPointsToWin(int pointsToWin) {
        this.pointsToWin = pointsToWin;
    }

    @Override
    public String toString() {
        return "PictureChallengeDayDeductions{" + "fromTime=" + fromTime + ", tillTime=" + tillTime + ", pointsToWin=" + pointsToWin + '}';
    }

    
}
