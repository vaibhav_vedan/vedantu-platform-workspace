/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.pojo;

/**
 *
 * @author parashar
 */
public class UserCompetitiveTestInfo implements Comparable<UserCompetitiveTestInfo> {

    private String userId;
    private Float percentile;
    private Float marksAchieved;

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the percentile
     */
    public Float getPercentile() {
        return percentile;
    }

    /**
     * @param percentile the percentile to set
     */
    public void setPercentile(Float percentile) {
        this.percentile = percentile;
    }

    /**
     * @return the marksAchieved
     */
    public Float getMarksAchieved() {
        return marksAchieved;
    }

    /**
     * @param marksAchieved the marksAchieved to set
     */
    public void setMarksAchieved(Float marksAchieved) {
        this.marksAchieved = marksAchieved;
    }

    @Override
    public int compareTo(UserCompetitiveTestInfo o) {
        if( this.marksAchieved <= o.getMarksAchieved()){
            return -1;
        }
        
        //this.marksAchieved > o.getMarksAchieved()){
        return 1;
        
        
        // throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
