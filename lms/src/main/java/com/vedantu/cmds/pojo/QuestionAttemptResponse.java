package com.vedantu.cmds.pojo;

import java.util.List;

/**
 * Created by somil on 24/03/17.
 */
public class QuestionAttemptResponse {
    String questionId;
    List<String> answersGiven;


    public String getQuestionId() {
        return questionId;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    public List<String> getAnswersGiven() {
        return answersGiven;
    }

    public void setAnswersGiven(List<String> answersGiven) {
        this.answersGiven = answersGiven;
    }


}
