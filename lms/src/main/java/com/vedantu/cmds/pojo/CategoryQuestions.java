package com.vedantu.cmds.pojo;

import java.util.List;

/**
 * Created by somil on 23/03/17.
 */
public class CategoryQuestions {
    private String category;
    private List<QuestionResponse> questions;

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public List<QuestionResponse> getQuestions() {
        return questions;
    }

    public void setQuestions(List<QuestionResponse> questions) {
        this.questions = questions;
    }


    @Override
    public String toString() {
        return "CategoryQuestions{" +
                "category='" + category + '\'' +
                ", questions=" + questions +
                '}';
    }
}
