package com.vedantu.cmds.pojo;

import com.vedantu.lms.cmds.enums.ContentState;

public class StudentContentStateCountPojo {

	private String studentId;
	private String subject;
	private ContentState contentState;
	private Integer count;
	private Long boardId;
	public String getStudentId() {
		return studentId;
	}
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Integer getCount() {
		return count;
	}
	public void setCount(Integer count) {
		this.count = count;
	}
	public Long getBoardId() {
		return boardId;
	}
	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}
	public ContentState getContentState() {
		return contentState;
	}
	public void setContentState(ContentState contentState) {
		this.contentState = contentState;
	}
}
