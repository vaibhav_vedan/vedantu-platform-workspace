package com.vedantu.cmds.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.vedantu.cmds.pojo.OnboardingVideoData;
import com.vedantu.cmds.request.*;
import com.vedantu.cmds.response.*;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;
import com.vedantu.cmds.entities.CMDSVideoPlaylistOrder;
import com.vedantu.cmds.entities.ClassroomChatMessage;
import com.vedantu.cmds.managers.CMDSVideoManager;
import com.vedantu.cmds.pojo.CMDSVideoBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.request.SetVideoCountReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

@CrossOrigin
@RestController
@RequestMapping("cmds/video")
public class CMDSVideoController {

    @Autowired
    private CMDSVideoManager videoManager;

    @Autowired
    public LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

    public Logger logger = logFactory.getLogger(CMDSVideoController.class);

    @RequestMapping(value = "/addEditCMDSVideo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSVideo addEditCMDSVideo(@RequestBody AddCMDSVideoReq req)
            throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.addEditCMDSVideo(req);
    }

    @RequestMapping(value = "/updateDurationAndThumbnailVimeo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse updateDurationAndThumbnailVimeo(@RequestParam(value = "id", required = true) String id) throws VException, IOException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        videoManager.updateVimeoMetadata(null, id);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getAdminCMDSVideos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSVideo> getAdminCMDSVideos(GetAdminVideosReq req)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.getAdminVideos(req);
    }

    @RequestMapping(value = "/getAdminCMDSVideosById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSVideo getAdminCMDSVideosById(@RequestParam(value = "id", required = true) String videoId)
            throws VException, inti.ws.spring.exception.client.BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.getAdminCMDSVideosById(videoId);
    }

    @RequestMapping(value = "/addEditCMDSPlaylist", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSVideoPlaylist addEditCMDSPlaylist(@RequestBody AddVideoPlaylistReq addVideoPlaylistReq) throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.addEditCMDSVideoPlaylist(addVideoPlaylistReq);
    }

    @RequestMapping(value = "/getVideoPlaylists", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSVideoPlaylist> getVideoPlaylists(GetCMDSVideoPlaylistReq req) throws VException {
        req.removeTargetBoardsFromMainTags();
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        if(Role.STUDENT.equals(sessionData.getRole())){
            req.setPublished(true);
        }
        return videoManager.getCMDSVideoPlaylists(req);
    }


    @RequestMapping(value = "/getVideoPlaylistsAdmin", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSVideoPlaylist> getVideoPlaylistsAdmin(GetCMDSVideoPlaylistReq req) throws VException {
        req.removeTargetBoardsFromMainTags();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.getCMDSVideoPlaylistsAdmin(req);
    }

    @RequestMapping(value = "/getAdminCMDSVideoPlaylistById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSVideoPlaylist getAdminCMDSVideoPlaylistById(@RequestParam(name = "id", required = true) String playlistId) throws VException {
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
    	Long userId = sessionData.getUserId();
        return videoManager.getCMDSVideoPlaylistById(playlistId, userId);
    }

    @RequestMapping(value = "/getCMDSVideoPlaylistById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSVideoPlaylist getCMDSVideoPlaylistById(@RequestParam(name = "id", required = true) String playlistId) throws VException {
    	HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        Long userId = sessionData.getUserId();
    	return videoManager.getCMDSVideoPlaylistById(playlistId, userId);
    }    


    @RequestMapping(value = "/getUpcomingCMDSVideoPlaylist", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UpcommingVideosRes getUpcomingCMDSVideoPlaylist(GetCMDSVideoPlaylistReq req) throws VException, NotFoundException, ForbiddenException, InternalServerErrorException, BadRequestException {
        req.removeTargetBoardsFromMainTags();
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        if(Role.STUDENT.equals(sessionData.getRole())){
            req.setPublished(true);
        }
        Long userId = sessionData.getUserId();
        UpcommingVideosRes res = videoManager.getUpcomingCMDSVideoPlaylist(req);
        if(sessionData.getUserId()%2==0) {
        	res.setPlayButton(true);
        }
        return res;
    }

    @RequestMapping(value = "/liveVideos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse liveVideos(@RequestParam(value = "grade", required = true) String grade) throws ForbiddenException {
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        if(Role.STUDENT.equals(sessionData.getRole())){
            return new PlatformBasicResponse(videoManager.isLiveVideo(grade), null, null);
        } else {
            return new PlatformBasicResponse(false, null, null);
        }
    }

    @RequestMapping(value = "/getRecordedSection", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RecordedSectionRes getRecordedSection(GetCMDSVideoPlaylistReq req) throws ForbiddenException, InternalServerErrorException, BadRequestException, NotFoundException, VException {
        req.removeTargetBoardsFromMainTags();
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        if(Role.STUDENT.equals(sessionData.getRole())){
            req.setPublished(true);
        }
        return videoManager.getRecordedSection(req);
    }

    @RequestMapping(value = "/getCMDSVideoById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSVideoBasicInfo getCMDSVideoById(@RequestParam(value = "id", required = true) String videoId)
            throws VException, inti.ws.spring.exception.client.BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.STUDENT, Boolean.TRUE);
        return videoManager.getCMDSVideosById(videoId, sessionUtils.getCallingUserId());
    }
    
    @RequestMapping(value = "/getCMDSVideoByUrl", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSVideoBasicInfo getCMDSVideoByUrl(@RequestParam(value = "url", required = true) String videoUrl)
            throws VException, inti.ws.spring.exception.client.BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.STUDENT, Boolean.TRUE);
        return videoManager.getCMDSVideosByUrl(videoUrl, sessionUtils.getCallingUserId());
    }    
    
    @RequestMapping(value = "/incrementCMDSVideoView", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void incrementCMDSVideo(@RequestParam(value = "id", required = true) String videoId){
        videoManager.incrementCMDSVideoViews(videoId);
    }    

    @RequestMapping(value = "/getSavedBookmarks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSVideo> getSavedBookmarks(@RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size) throws VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return videoManager.getSavedBookmarks(httpSessionData.getUserId(), start, size);
    }

    @RequestMapping(value = "/savePlaylistOrderForGrade", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSVideoPlaylistOrder savePlaylistOrderForGrade(@RequestBody GradeCMDSPlaylistOrder req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.savePlaylistOrderForGrade(req);
    }

    @RequestMapping(value = "/getOrderedPlaylistByGrade", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlaylistOrderForGradeRes getOrderedPlaylistByGrade(@RequestParam(value = "grade", required = true) String grade) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.getOrderedPlaylistByGrade(grade);
    }

    @RequestMapping(value = "/setVideoVoteCount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse setVideoVoteCount(@RequestBody SetVideoCountReq req) throws com.vedantu.exception.BadRequestException, VException {
        return videoManager.setVideoVoteCount(req);
    }

    @RequestMapping(value = "/getChatData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ClassroomChatMessage> getChatData(@RequestParam(value = "videoId", required = true) String videoId) {
        return videoManager.getChatData(videoId);
    }

//    @RequestMapping(value = "/simulateChatTest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    public void simulateChatTest() {
//           videoManager.simulateChat();
//    }

//    @RequestMapping(value = "/simulateChat", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public void simulateChat(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) {
//        logger.info(request);
//        Gson gson = new Gson();
//        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
//        if (messageType.equals("SubscriptionConfirmation")) {
//            String json = null;
//            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
//            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
//            logger.info(resp.getEntity(String.class));
//        } else if (messageType.equals("Notification")) {
//            logger.info("Notification received - SNS");
//            logger.info(subscriptionRequest.toString());
//            videoManager.simulateChat();
//        }
//    }

    @RequestMapping(value = "/getFilters", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FiltersRes> getFilters() {
    	return videoManager.getFilters();
    }

    @RequestMapping(value = "/getCMDSPlaylistsfromVideoID", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSVideoPlaylist> getCMDSPlaylistsfromVideoID(@RequestParam(value = "videoId", required = true)String videoId) {
		return videoManager.getCMDSPlaylistsfromVideoID(videoId);
    }
    
    @RequestMapping(value = "/getCMDSPlaylistfromVideoID", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSVideoPlaylist getCMDSPlaylistfromVideoID(@RequestParam(value = "videoId", required = true)String videoId) {
		return videoManager.getCMDSPlaylistfromVideoID(videoId);
    }
    
    @RequestMapping(value = "/getCMDSPlaylistsByGrade", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSVideoPlaylist> getCMDSPlaylistsByGrade(@RequestParam(value = "grade", required = true)String grade) {
		return videoManager.getCMDSPlaylistsByGrade(grade);
    }

    @RequestMapping(value = "/addEditOnboardingVideo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSVideo> addEditOnboardingVideo(@RequestBody AddEditOnboardingVideoRequest req)
            throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return videoManager.addEditOnboardingVideo(req);
    }

    @RequestMapping(value = "/getOnboardingVideo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<OnboardingVideoData> getOnboardingVideo(@RequestParam(value = "grade", required =  false)String grade)
            throws Exception {
        return videoManager.getOnboardingVideo(grade);
    }


}
