/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.entities.challenges.ChallengeAttempt;
import com.vedantu.cmds.entities.challenges.UserChallengeLeaderBoard;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.managers.challenges.ChallengesManager;
import com.vedantu.cmds.managers.challenges.PostChallengeProcessor;
import com.vedantu.cmds.managers.challenges.UserLeaderBoardManager;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.cmds.request.challenges.AddChallengeReq;
import com.vedantu.cmds.request.challenges.GetChallengesReq;
import com.vedantu.cmds.request.challenges.StartChallengeReq;
import com.vedantu.cmds.request.challenges.SubmitAnswerReq;
import com.vedantu.cmds.response.challenges.ChallengeLeaderBoardRes;
import com.vedantu.cmds.response.challenges.ChallengeRes;
import com.vedantu.cmds.response.challenges.GetChallengesRes;
import com.vedantu.cmds.response.challenges.StartChallengeRes;
import com.vedantu.cmds.response.challenges.UserLeaderBoardInfoRes;
import com.vedantu.cmds.utils.ChallengeUtilCommon;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.ReprocessType;
import com.vedantu.lms.cmds.request.GetLeaderBoardForChallengeReq;

import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import java.util.HashMap;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/challenges")
public class ChallengesController {

    @Autowired
    private ChallengesManager challengesManager;

    @Autowired
    private ChallengeUtilCommon challengeUtilCommon;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ChallengesController.class);

    @Autowired
    private PostChallengeProcessor postChallengeProcessor;

    @Autowired
    private UserLeaderBoardManager userLeaderBoardManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

//    @RequestMapping(value = "/createQuestionAndThenChallenge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public Challenge createQuestionAndThenChallenge(@RequestBody CreateQuestionAndThenChallengeReq req) throws BadRequestException, VException {
//        return challengesManager.createQuestionAndThenChallenge(req);
//    }
    @RequestMapping(value = "/add", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Challenge addChallenge(@RequestBody AddChallengeReq addChallengeReq) throws BadRequestException, VException {
        return challengesManager.addChallenge(addChallengeReq);
    }

    @RequestMapping(value = "/getChallengeInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ChallengeRes getChallengeInfo(@RequestParam(value = "challengeId") String challengeId,
            @RequestParam(value = "userId", required = false) Long userId) throws VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        return challengesManager.getChallengeInfo(challengeId, userId, httpSessionData.getRole());
    }

    @RequestMapping(value = "/startChallenge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public StartChallengeRes startChallenge(@RequestBody StartChallengeReq startChallengeReq)
            throws BadRequestException, VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        if (!Role.STUDENT.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ONLY_STUDENT_CAN_ATTEMPT_CHALLENGE,
                    "Only students can attempt challenges");
        }

        if (startChallengeReq.getUserId() != null
                && !startChallengeReq.getUserId().equals(httpSessionData.getUserId())) {
            throw new ForbiddenException(ErrorCode.ILLEGAL_CHALLENGE_ATTEMPT,
                    "Logged in user " + httpSessionData.getUserId() + " different from " + startChallengeReq.getUserId());
        }

        return challengesManager.startChallenge(startChallengeReq);
    }

    @RequestMapping(value = "/getHint", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HintFormat getHint(@RequestParam(value = "userId") Long userId,
            @RequestParam(value = "token") String token,
            @RequestParam(value = "hintToBeTakenIndex") Integer hintToBeTakenIndex) throws VException {
        return challengesManager.getHintWhileAttempting(userId, token, hintToBeTakenIndex);
    }

    @RequestMapping(value = "/submitAnswer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ChallengeAttempt submitAnswer(@RequestBody SubmitAnswerReq submitAnswerReq)
            throws BadRequestException, VException {
        return challengesManager.submitAnswer(submitAnswerReq);
    }

    @RequestMapping(value = "/getChallenges", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetChallengesRes getChallenges(GetChallengesReq getChallengesReq) throws VException {
        return challengesManager.getChallenges(getChallengesReq);
    }

    @RequestMapping(value = "/getChallengeUserAttemptInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ChallengeAttempt getChallengeUserAttemptInfo(@RequestParam(value = "userId") Long userId,
            @RequestParam(value = "challengeId") String challengeId) throws VException {
        return challengesManager.getChallengeUserAttemptInfo(userId, challengeId);
    }

    @RequestMapping(value = "/getUserChallengeStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserChallengeLeaderBoard getUserChallengeStats(@RequestParam(value = "userId", required = true) Long userId,
    		@RequestParam(value = "category", required = false) String category)
            throws VException {
    	sessionUtils.checkIfAllowed(userId, Role.STUDENT, Boolean.TRUE);
    	if(StringUtils.isEmpty(category)){
    		category = "DEFAULT";
    	}
        return userLeaderBoardManager.getUserChallengeStats(userId,category);
    }

    @RequestMapping(value = "/getChallengeLeaderBoard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ChallengeLeaderBoardRes getChallengeLeaderBoard(@RequestParam(value = "userId") Long userId,
            @RequestParam(value = "challengeId") String challengeId,
            @RequestParam(value = "start") Integer start,
            @RequestParam(value = "size") Integer size) throws VException {

        return challengeUtilCommon.getChallengeLeaderBoard(userId, challengeId,
                start, size, (start == 0));
    }

    @RequestMapping(value = "/endChallenge", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse endChallenge(@RequestParam(value = "challengeId") String challengeId,
            @RequestParam(value = "reprocess", required = false) boolean reprocess) throws VException {
        postChallengeProcessor.endChallenge(challengeId, reprocess);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/endChallengeCron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse endChallengeCron(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Throwable {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            Map<String, Object> payload = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.END_CHALLENGE_CRON, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/markChallengeActiveCron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markChallengeActiveCron(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Throwable {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            Map<String, Object> payload = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.MARK_CHALLENGE_ACTIVE_CRON, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getChallengeLeaderBoardForDuration", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UserLeaderBoardInfoRes getChallengeLeaderBoardForDuration(GetLeaderBoardForChallengeReq req) throws BadRequestException {
        return userLeaderBoardManager.getChallengeLeaderBoardForDuration(req);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse deleteChallenge(@RequestParam(value = "challengeId", required = true) String challengeId)
            throws VException {
        return challengesManager.deleteChallenge(challengeId);
    }

    @RequestMapping(value = "/migrateUserChallengeLeaderBoard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void migrateUserChallengeLeaderBoard(@RequestParam(value = "challengeId", required = true) String challengeId,
            @RequestParam(value = "startTime") Long startTime) throws BadRequestException, ForbiddenException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        if (!Role.ADMIN.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                    "Only Admin can access challenges");
        }

        userLeaderBoardManager.migrateUserChallengeLeaderBoard(challengeId, startTime);
    }

    @RequestMapping(value = "/getActiveChallengesForAllCategories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetChallengesRes getActiveChallengesForAllCategories() throws NotFoundException {
//    	GetChallengesRes res = new GetChallengesRes();
//    	return res;    	
        return challengesManager.getActiveChallengesForAllCategories();
    }

    @RequestMapping(value = "/sendMailToUnattemptedUsers", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendMailToUnattemptedUsers(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Throwable {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            Map<String, Object> payload = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_MAIL_UNATTEMPTED_USERS, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/sendDailyChallengeAttemptMail", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendDailyChallengeAttemptMail(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request)
            throws Throwable {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            Map<String, Object> payload = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_MAIL_CHALLENGE_ATTEMPTS_CRON, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/reprocessChallenge", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse reprocessChallenge(@RequestParam(value = "challengeId") String challengeId,
            @RequestParam(value = "reprocess") boolean reprocess, 
            @RequestParam(value = "reprocessType") ReprocessType reprocessType) throws VException {
    	sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
    	postChallengeProcessor.reprocessChallenge(challengeId, reprocess, reprocessType);
        return new PlatformBasicResponse();
    }
}
