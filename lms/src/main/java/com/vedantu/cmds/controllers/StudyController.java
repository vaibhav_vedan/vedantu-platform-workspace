/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.cmds.entities.StudyEntry;
import com.vedantu.cmds.entities.StudyEntryItem;
import com.vedantu.cmds.managers.StudyManager;
import com.vedantu.cmds.pojo.ChapterPdf;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/study")
public class StudyController {

    @Autowired
    private StudyManager studyManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(StudyController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse create(@RequestBody List<StudyEntry> req,
            @RequestParam(name = "replaceAll", required = false) boolean replaceAll) throws BadRequestException, VException, Exception {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (!Role.ADMIN.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Non admin role not allowed to create schedule");
        }
        studyManager.create(req, httpSessionData.getUserId().toString(), replaceAll);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getStudyWithSubjects", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StudyEntry> getStudyWithSubjects(@RequestParam(name = "mainTags", required = false) Set<String> mainTags) throws VException {
        //TODO store response in redis for the initial start,size
        return studyManager.getStudyWithSubjects(mainTags);
    }

    @RequestMapping(value = "/getStudyWithSubjectsById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudyEntry getStudyWithSubjectsById(@RequestParam(name = "studyId", required = true) String studyId) throws VException {
        //TODO store response in redis for the initial start,size
        return studyManager.getStudyWithSubjectsById(studyId);
    }

    @RequestMapping(value = "/getChapterPdfs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ChapterPdf> getChapterPdfs(@RequestParam(name = "studyEntryId", required = true) String studyEntryId,
            @RequestParam(name = "title", required = false) String title) throws VException {
        //TODO store response in redis for the initial start,size
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return studyManager.getChapterPdfs(studyEntryId, title, httpSessionData.getUserId());
    }

    @RequestMapping(value = "/getSavedBookmarks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StudyEntryItem> getSavedBookmarks(@RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size) throws VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return studyManager.getSavedBookmarks(httpSessionData.getUserId(), start, size);
    }

    @RequestMapping(value = "/getStudyEntryItem", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudyEntryItem getStudyEntryItem(@RequestParam(value = "seoUrl", required = true) String seoUrl) throws VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        return studyManager.getStudyEntryItem(seoUrl, httpSessionData.getUserId());
    }

    @RequestMapping(value = "/incrementStudyView", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public void incrementCMDSVideo(@RequestParam(value = "id", required = true) String studyEntryId){
        studyManager.incrementStudyView(studyEntryId);
    }
    
    @PostMapping(value = "/updateStudyEntryOrder", produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse updateStudyEntryOrder(@RequestBody List<String> targetGradeTitleOrder) throws RuntimeException, ForbiddenException {
    	HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (!Role.ADMIN.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Non admin role not allowed to update study entry order");
        }
    	studyManager.updateStudyEntryOrder(targetGradeTitleOrder, httpSessionData.getUserId().toString());
    	return new PlatformBasicResponse(true, "success", "");
    }
    
    
    @RequestMapping(value = "/getStudyEntryByGrades", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StudyEntry> getStudyEntryByGrades(@RequestParam(name = "mainTags", required = true) Set<String> mainTags) throws VException {
		return studyManager.getStudyEntryByGrades(mainTags);    	
    }
    
    @RequestMapping(value = "/updateStudyEntry", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse updateStudyEntryById(@RequestParam(name = "ids", required = true)List<String>  ids) throws BadRequestException, VException, Exception {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (!Role.ADMIN.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Non admin role not allowed to create schedule");
        }
        studyManager.updateStudyEntryById(ids, httpSessionData.getUserId().toString());
        return new PlatformBasicResponse(true,"success","");
    }
    
    @RequestMapping(value = "/getStudyEntriesForNCERT", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StudyEntry> getStudyEntriesForNCERT(@RequestParam(name = "grade", required = true) String grade, @RequestBody List<String> titles) throws VException{
    	return studyManager.getStudyEntriesForNCERT(titles, grade);
    	
    }
    
    
}
