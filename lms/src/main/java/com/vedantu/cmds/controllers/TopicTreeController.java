package com.vedantu.cmds.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.board.requests.AddToBaseTreeRequest;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CurriculumTopicTree;
import com.vedantu.cmds.managers.TopicTreeManager;
import com.vedantu.cmds.request.TopicNamesRequest;
import com.vedantu.cmds.response.BaseTreeResponse;
import com.vedantu.cmds.response.GetBaseTreeNodesByParentRes;
import com.vedantu.exception.VException;
import com.vedantu.lms.request.ValidateAndSetTopicTargetTagsReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import com.vedantu.util.security.HttpSessionUtils;
import inti.ws.spring.exception.client.BadRequestException;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@CrossOrigin
@RestController
@RequestMapping("tree")
public class TopicTreeController {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(TopicTreeController.class);

    @Autowired
    private TopicTreeManager topicTreeManager;

    @RequestMapping(value = "/addToBaseTree", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseTopicTree addToBaseTree(@RequestBody AddToBaseTreeRequest req) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return topicTreeManager.addToBaseTree(req);
    }

    @RequestMapping(value = "/addToBaseTreeByName", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseTopicTree addToBaseTreeByName(@RequestBody AddToBaseTreeRequest req) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return topicTreeManager.addToBaseTreeByName(req);
    }

    @RequestMapping(value = "/addToCurriculumTree", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CurriculumTopicTree addToCurriculumTree(@RequestBody AddToBaseTreeRequest req) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return topicTreeManager.addToCurriculumTree(req);
    }

    @RequestMapping(value = "/addToCurriculumTreeByName", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CurriculumTopicTree addToCurriculumTreeByName(@RequestBody AddToBaseTreeRequest req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return topicTreeManager.addToCurriculumTreeByName(req);
    }

    @RequestMapping(value = "/getBaseTreeNodesByParent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetBaseTreeNodesByParentRes gegetBaseTreeNodesByParent(@RequestParam(name = "parentId", required = true) String parentId) throws VException, BadRequestException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT, Role.TEACHER), Boolean.TRUE);
        return topicTreeManager.getBaseTreeNodesByParent(parentId);
    }

    @RequestMapping(value = "/getBaseTreeNodesByLevel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BaseTopicTree> getBaseTreeNodesByLevel(@RequestParam(name = "level", required = true) Integer level) throws VException, BadRequestException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT, Role.TEACHER), Boolean.TRUE);
        return topicTreeManager.getBaseTreeNodesByLevel(level);
    }

    @RequestMapping(value = "/getBaseTreeNodesBySubject", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BaseTopicTree> getBaseTreeNodesBySubject(@RequestParam(name = "subject", required = true) String subject) throws VException, BadRequestException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT, Role.TEACHER), Boolean.TRUE);
        return topicTreeManager.getBaseTreeNodesBySubject(subject);
    }    
    
    // Level 0 is for Boards/Targets
    @RequestMapping(value = "/getCurriculumTreeNodesByLevel", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CurriculumTopicTree> getCurriculumTreeNodesByLevel(@RequestParam(name = "level", required = true) Integer level) throws VException, BadRequestException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT, Role.TEACHER), false);
        return topicTreeManager.getCurriculumTreeNodesByLevel(level);
    }

    @RequestMapping(value = "/validateAndSetTopicTargetTags", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public AbstractTargetTopicEntity validateAndSetTopicTargetTags(@RequestBody ValidateAndSetTopicTargetTagsReq req) throws VException {
        return topicTreeManager.validateAndSetTopicTargetTags(req);
    }

    @RequestMapping(value = "/cacheEntireTopicTreeStructure",method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void cacheEntireTopicTreeStructure(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
                                          @RequestBody String request) throws VException {
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info("ClientResponse : " + resp.toString());
        } else if (messgaetype.equals("Notification")) {
            topicTreeManager.cacheEntireTopicTreeStructure();
        }
    }

    @RequestMapping(value = "/getMatchingTopicNamesOfTopicTree",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getMatchingTopicNamesOfTopicTree(@RequestBody TopicNamesRequest request) throws VException {
        request.verify();
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return topicTreeManager.getMatchingTopicNamesOfTopicTree();
    }

    @RequestMapping(value = "/getAllChildrenByTopicNames",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Set<String> getAllChildrenByTopicNames(@RequestBody TopicNamesRequest request) throws VException {
        request.verify();
        sessionUtils.checkIfAllowed(request.getCallingUserId(),Role.ADMIN,Boolean.TRUE);
        return topicTreeManager.getAllChildrenByTopicNames(request);
    }

    @RequestMapping(value = "/insertNewBaseTreeForSubject",method = RequestMethod.POST,consumes = MediaType.TEXT_PLAIN_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse insertNewBaseTreeForSubject(@RequestBody String baseTreeStructureForSubject){
        return topicTreeManager.insertNewBaseTreeForSubject(baseTreeStructureForSubject);
    }

    @RequestMapping(value = "/mapOldDataToNewData",method = RequestMethod.POST,consumes = MediaType.TEXT_PLAIN_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> mapOldDataToNewData(@RequestBody String mapping){
        return topicTreeManager.mapOldDataToNewData(mapping);
    }

    @RequestMapping(value = "/setCorrectLevelsInTopicTree",method = RequestMethod.POST)
    @ResponseBody
    public void setCorrectLevelsInTopicTree(){
        topicTreeManager.setCorrectLevelsInTopicTree();
    }

    @Deprecated
    @RequestMapping(value="/updateTopicTreeErrorNode",method=RequestMethod.POST,consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void updateTopicTreeErrorNode(@RequestBody String patternValue){
         logger.info("PassedValue = {}",patternValue);
        topicTreeManager.updateTopicTreeErrorNodes(patternValue);
    }

    @RequestMapping(value = "/uploadEntireCommerceTree",method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse uploadEntireCommerceTree(){
        return topicTreeManager.uploadEntireCommerceTree();
    }
}
