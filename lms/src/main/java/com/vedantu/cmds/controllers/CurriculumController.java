package com.vedantu.cmds.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.util.*;

import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.cmds.response.CurriculumCountResponse;
import com.vedantu.cmds.response.CurriculumProgressRes;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.pojo.LMSStatsForSession;
import com.vedantu.moodle.pojo.SubscriptionRequest;
import com.vedantu.moodle.response.HomepageTestResponse;
import com.vedantu.onetofew.enums.SessionEventsOTF;
import com.vedantu.subscription.response.CurriculumProgressInfo;
import com.vedantu.subscription.response.StudentAttemptInfo;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSSubject;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.cmds.managers.CurriculumManager;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.lms.cmds.pojo.SubjectFileCountPojo;
import com.vedantu.lms.cmds.request.AddContentCurriculumReq;
import com.vedantu.lms.cmds.request.EditFromCourseToBatchRes;
import com.vedantu.lms.cmds.request.AddCurriculumReq;
import com.vedantu.lms.cmds.request.ChangeCurriculumStatusReq;
import com.vedantu.lms.cmds.request.CreateBatchCurriculumReq;
import com.vedantu.lms.cmds.request.AddOrDeleteNodeReq;
import com.vedantu.lms.cmds.request.EditCurriculumReq;
import com.vedantu.lms.cmds.request.GetCurriculumBySessionIdReq;
import com.vedantu.lms.cmds.request.GetCurriculumReq;
import com.vedantu.lms.cmds.request.ShareContentAndSendMailRes;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.lms.cmds.request.ShareCurriculumContentReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;

import javax.mail.internet.AddressException;


@RestController
@RequestMapping("/curriculum")
public class CurriculumController {
	
	@Autowired
	private CurriculumManager curriculumManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CurriculumController.class);
        
    @Autowired
    HttpSessionUtils sessionUtils;
	
    @RequestMapping(value = "/createCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node createCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException {
        sessionUtils.checkIfAllowedList(addCurriculumReq.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        addCurriculumReq.verify();
        return curriculumManager.createCurriculum(addCurriculumReq);
    }

    @RequestMapping(value = "/getCurriculumByEntity", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Node> getCurriculumByEntity(GetCurriculumReq getCurriculumReq) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return curriculumManager.getCurriculumByEntity(getCurriculumReq);
    }

    @RequestMapping(value = "/getCurriculumInfo/{userId}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Node> getCurriculumByBatchesAll(@PathVariable("userId") String userId) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return curriculumManager.getCurriculumByUser(userId);
    }


    @RequestMapping(value = "/getCurriculumBySessionId", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Node> getCurriculumBySessionId(GetCurriculumBySessionIdReq getCurriculumReq) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException {
        return curriculumManager.getCurriculumBySessionId(getCurriculumReq);
    }
	
	
    @RequestMapping(value = "/editCourseCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node editCourseCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException {
        sessionUtils.checkIfAllowedList(addCurriculumReq.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        return curriculumManager.editCourseCurriculum(addCurriculumReq);
    }
	
    @RequestMapping(value = "/createBatchCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node createBatchCurriculum(@RequestBody CreateBatchCurriculumReq createBatchCurriculumReq) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException {
        return curriculumManager.createBatchCurriculum(createBatchCurriculumReq);
    }
	
	@RequestMapping(value = "/changeCurriculumStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node changeCurriculumStatus(@RequestBody ChangeCurriculumStatusReq changeCurriculumStatusReq) throws VException, MalformedURLException {
        return curriculumManager.changeCurriculumStatus(changeCurriculumStatusReq);
    }
	
	@RequestMapping(value = "/changeAnyCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node changeAnyCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException {
        return curriculumManager.changeAnyCurriculum(addCurriculumReq);
    }
	
    @RequestMapping(value = "/shareContentForCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ShareContentAndSendMailRes shareContentForCurriculum(@RequestBody ShareCurriculumContentReq shareCurriculumContentReq) throws VException, MalformedURLException {
        return curriculumManager.shareContentForCurriculum(shareCurriculumContentReq);
    }
    
    @RequestMapping(value = "/shareContentForCurriculumForBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse shareContentForCurriculumForBatch(@RequestBody ShareCurriculumContentReq shareCurriculumContentReq) throws VException, MalformedURLException {
        return curriculumManager.shareContentForCurriculumForBatch(shareCurriculumContentReq);
    }    
	
    @RequestMapping(value = "/addNodeInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public Node addNodeInCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException{
		return curriculumManager.addNodeInCurriculum(addCurriculumReq);
	}
	
	@RequestMapping(value = "/deleteNodeInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public Node deleteNodeInCurriculum(@RequestBody AddOrDeleteNodeReq req) throws VException{
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        return curriculumManager.deleteNodeInCurriculum(req);
	}
	
	@RequestMapping(value = "/addContentToNode", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node addContentToNode(@RequestBody AddContentCurriculumReq req) throws VException, MalformedURLException {
		return curriculumManager.addContentToNode(req);
	}
	
	@RequestMapping(value = "/addContentToCourseAndBatchNodes", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditFromCourseToBatchRes addContentToCourseAndBatchNodes(@RequestBody AddContentCurriculumReq req) throws VException{
		return curriculumManager.addContentToCourseAndBatchNodes(req);
	}
	
	@RequestMapping(value = "/editNodeInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node editNodeInCurriculum(@RequestBody EditCurriculumReq req) throws VException{
		return curriculumManager.editNodeInCurriculum(req);
	}
	
	@RequestMapping(value = "/shareContentOnEnrollment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void shareContentOnEnrollment(@RequestBody ShareContentEnrollmentReq req) throws VException{
		curriculumManager.shareContentOnEnrollment(req);
	}
	
	@RequestMapping(value = "/getListOfBatchesToInheritCourse", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public EditFromCourseToBatchRes getListOfBatchesToInheritCourse(EditCurriculumReq req) throws BadRequestException{
		return curriculumManager.getListOfBatchesToInheritCourse(req);
	}
	
	@RequestMapping(value = "/editNodeInCourseAndBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditFromCourseToBatchRes editNodeInCourseAndBatch(@RequestBody EditCurriculumReq req) throws VException{
		return curriculumManager.editNodeInCourseAndBatch(req);
	}
	
	@RequestMapping(value = "/deleteNodeInCourseAndBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public PlatformBasicResponse deleteNodeInCourseAndBatch(@RequestBody AddOrDeleteNodeReq req) throws VException{
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        curriculumManager.deleteNodeInCourseAndBatch(req);
        return new PlatformBasicResponse();
	}
	
	@RequestMapping(value = "/addNodeInCourseAndBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EditFromCourseToBatchRes addNodeInCourseAndBatch(@RequestBody AddOrDeleteNodeReq req) throws VException{
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
		return curriculumManager.addNodeInCourseAndBatch(req);
	}
	
	@RequestMapping(value = "/checkIfCurriculumExists", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public List<String> checkIfCurriculumExists(@RequestParam(value = "contextIds") List<String> contextIds) throws BadRequestException{
		return curriculumManager.checkIfCurriculumExists(contextIds);
	}

	@RequestMapping(value = "/getTopicsStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public CurriculumProgressInfo getTopicsStats(@RequestParam(name = "batchId", required = true) String batchId){
		return curriculumManager.getTopicsStats(batchId);
	}

	@RequestMapping(value = "/getLMSStatsForSession", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<String, LMSStatsForSession> getLMSStatsForSession (@RequestBody List<String> sessionIds){
		return curriculumManager.getLMSStatsMap(new HashSet<>(sessionIds));
	}

	@RequestMapping(value = "/getContentStatsForState", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<StudentAttemptInfo> getOTMStudentAttemptInfo(@RequestParam(name = "sessionId", required = true) String sessionId,
															 @RequestParam(name = "start", required = false) Integer start,
															 @RequestParam(name = "size", required = false) Integer size,
															 @RequestParam(name = "contentState", required = true)ContentState contentState) throws VException{
             sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return curriculumManager.getStudentAttemptInfoForSession(sessionId, start, size, contentState);
	}

    @RequestMapping(value = "/resetContentInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node resetContentInCurriculum(@RequestBody AddContentCurriculumReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return curriculumManager.resetContentInCurriculum(req);
    }
    
    @RequestMapping(value = "/resetContentInCurriculumBatches", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void resetContentInCurriculumBatches(@RequestBody AddContentCurriculumReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        curriculumManager.resetContentInCurriculumBatches(req.getBatchIds(),req.getNodeId(),req.getContents(),req.getCallingUserId(),Boolean.TRUE);
    }
    
    @RequestMapping(value = "/removeContentInCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Node removeContentInCurriculum(@RequestBody AddContentCurriculumReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return curriculumManager.removeContentInCurriculum(req);
    }
    
    @RequestMapping(value = "/getCurriculumFilesForBatch", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, List<SubjectFileCountPojo>> getCurriculumFilesForBatch(@RequestParam(name="batchIds") List<String> batchIds) throws VException {
        return curriculumManager.getCurriculumFilesInBatch(batchIds);
    }
    @RequestMapping(value = "/appendCurriculum", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void appendCurriculum(@RequestBody AddCurriculumReq addCurriculumReq) throws VException,BadRequestException, ForbiddenException, NotFoundException, ConflictException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        curriculumManager.appendCurriculumCourseAndBatch(addCurriculumReq);
    }

    @RequestMapping(value = "/getCurriculumForBatchBoardAndLastUpdated/batchId/{batchId}/boardId/{boardId}/lastUpdated/{lastUpdated}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Node> getCurriculumForBatchBoardAndLastUpdated(@PathVariable("batchId") String batchId, @PathVariable("boardId") Long boardId,@PathVariable("lastUpdated") Long lastUpdated){
        return curriculumManager.getCurriculumForBatchBoardAndLastUpdated(batchId,boardId,lastUpdated);
    }

    @RequestMapping(value = "/migrateExistingData/dataSize/{dataSize}",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Deprecated
    public List<String> migrateExistingData(@PathVariable("dataSize") int dataSize) {
        return curriculumManager.migrateExistingData(dataSize);
    }

    @RequestMapping(value = "/getCurriculumCountForUser",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CurriculumCountResponse> getCurriculumCount() throws VException {
        return curriculumManager.getCurriculumCount();
    }


    @RequestMapping(value = "/getTopicsByEntity", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public  List<String> getTopicsByEntity(GetCurriculumReq getCurriculumReq) throws VException {
        return curriculumManager.getTopicsByEntity(getCurriculumReq);
    }

    @RequestMapping(value = "/getNodeNamesByIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public  Map<String,String> getNodeNamesByIds(@RequestBody GetCurriculumReq getCurriculumReq) throws VException {
        return curriculumManager.getNodeNamesByIds(getCurriculumReq);
    }

    @ApiOperation("get completed number of chapters vs total number of chapters for the given set of batches")
    @RequestMapping(value = "/getBatchCurriculumProgress", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, List<CurriculumProgressRes>> getBatchCurriculumProgress(@RequestParam(name = "batchIds") List<String> batchIds) throws VException {
        if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.STUDENT), false);
        return curriculumManager.getBatchCurriculumProgress(batchIds);
    }

    @RequestMapping(value = "/removeBatchCurriculumProgressCache", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void removeBatchCurriculumProgressCache(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                          @RequestBody String request) throws VException {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, null, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            String message = subscriptionRequest.getMessage();
            String subject = subscriptionRequest.getSubject();
            SNSSubject snsSubject = SNSSubject.valueOf(subject);

            logger.info("deblog-- removing cached progress request received");
            curriculumManager.removeBatchCurriculumProgressCache(message);
        }
    }

}
