package com.vedantu.cmds.controllers;

import com.vedantu.util.StringUtils;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ajith
 */
@RestController
//@CrossOrigin
public class RequestForwarderController {

    @RequestMapping(path = "/postforwarder", method = RequestMethod.POST)
    public void postforwarder(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String forwardTo = getForwardTo(request);
        if (!forwardTo.equals("/postforwarder")) {
            request.getRequestDispatcher(forwardTo).forward(request, response);
        }
    }

    @RequestMapping(path = "/getforwarder", method = RequestMethod.GET)
    public void getforwarder(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String forwardTo = getForwardTo(request);
        if (!forwardTo.equals("/getforwarder")) {
            request.getRequestDispatcher(forwardTo).forward(request, response);
        }
    }

    private String getForwardTo(HttpServletRequest request) throws UnsupportedEncodingException {
        String forwardTo = request.getParameter("forwardTo");
        if (StringUtils.isNotEmpty(forwardTo)) {
            forwardTo = URLDecoder.decode(forwardTo, "UTF-8");
            forwardTo = forwardTo.replace("/lms", "");//adding this hard check to not confuse the front end
        }
        return forwardTo;
    }
}
