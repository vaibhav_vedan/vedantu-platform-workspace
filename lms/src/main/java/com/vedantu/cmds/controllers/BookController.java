/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.cmds.entities.Book;
import com.vedantu.cmds.managers.BookManager;
import com.vedantu.cmds.request.ChangeBookStateReq;
import com.vedantu.cmds.request.CreateEditBookReq;
import com.vedantu.cmds.request.GetBooksReq;
import com.vedantu.cmds.response.BookBasicInfo;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/books")
public class BookController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BookController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private BookManager bookManager;

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Book create(@RequestBody CreateEditBookReq req) throws BadRequestException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return bookManager.create(req);
    }

    @RequestMapping(value = "/changeState", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse changeState(@RequestBody ChangeBookStateReq req) throws BadRequestException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return bookManager.changeState(req);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Book> getBooks(GetBooksReq req) throws BadRequestException, VException {
        return bookManager.getBooks(req);
    }

    @RequestMapping(value = "/getBooksBasicInfos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BookBasicInfo> getBooksBasicInfos(GetBooksReq req) throws BadRequestException, VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        boolean fetchAll = false;
        if (sessionData != null && Role.ADMIN.equals(sessionData.getRole())) {
            fetchAll = true;
        }
        return bookManager.getBooksBasicInfos(req, fetchAll);
    }

    @RequestMapping(value = "/getBookInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Book getBook(@RequestParam(name = "bookId", required = false) String bookId,
            @RequestParam(name = "bookName", required = false) String bookName,
            @RequestParam(name = "lastFetchedTime", required = false) Long lastFetchedTime) throws BadRequestException, VException {
        return bookManager.getBook(bookId, bookName, lastFetchedTime);
    }

}
