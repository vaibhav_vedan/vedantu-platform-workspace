/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.managers.CMDSQuestionManager;
import com.vedantu.cmds.pojo.CMDSQuestionBasicInfoESPojo;
import com.vedantu.cmds.pojo.SeoQuestion;
import com.vedantu.cmds.request.*;
import com.vedantu.cmds.response.DeleteQuestionsRes;
import com.vedantu.cmds.response.QuestionDuplicateResponse;
import com.vedantu.cmds.response.QuestionFullInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.request.SetQuestionVoteCountReq;
import com.vedantu.platform.seo.dto.CMDSQuestionDto;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("cmds/question")
public class CMDSQuestionController {

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(CMDSTestController.class);

    @Autowired
    private CMDSQuestionManager cMDSQuestionManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @RequestMapping(value = "/addEditQuestion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSQuestion addEditQuestion(@RequestBody AddEditQuestionReq req) throws VException {
        return cMDSQuestionManager.addQuestion(req);
    }
    
    @RequestMapping(value = "/addEditQuestionSeo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public String addEditQuestionSeo(@RequestBody AddEditQuestionReq req) throws VException {
        return cMDSQuestionManager.addQuestionSeo(req);
    }

    @RequestMapping(value = "/getQuestionFullInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public QuestionFullInfo getQuestionFullInfo(@RequestParam(name = "userId", required = false) Long userId,
            @RequestParam(name = "qid", required = true) String questionId)
            throws VException {
        return cMDSQuestionManager.getQuestionFullInfo(questionId, userId);
    }

    @RequestMapping(value = "/setQuestionVoteCount", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse setQuestionVoteCount(@RequestBody SetQuestionVoteCountReq req) throws VException {
        return cMDSQuestionManager.setQuestionVoteCount(req);
    }

    @RequestMapping(value = "/editAnswer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSQuestion editAnswer(@RequestBody EditAnswerReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.editAnswer(req);
    }

    //TODO allow admin and node app
    // Modified By : Akhilesh Joshi - Purpose : Added testId parameter for getting Question Marks from Question in CMDSTest -
    @RequestMapping(value = "/getQuestion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSQuestion getQuestion(@RequestParam(name = "qid", required = true) String questionId, @RequestParam(name = "testId", required = false) String testId) throws VException {
//        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        if(testId != null){
            return cMDSQuestionManager.getQuestion(questionId, testId);
        }
        return cMDSQuestionManager.getQuestion(questionId);
    }
    
    @RequestMapping(value = "/getQuestionSeo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeoQuestion getQuestionSeo(@RequestParam(name = "qid", required = true) String questionId) throws VException {
//        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.getQuestionSeo(questionId);
    }

    @RequestMapping(value = "/getQuestionsSeo", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SeoQuestion> getQuestionsSeo(@RequestBody List<String> questionIds, HttpServletRequest request) throws VException {

        sessionUtils.isAllowedApp(request);

        return cMDSQuestionManager.getQuestionsSeo(questionIds);
    }

    @RequestMapping(value = "/deleteQuestions", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public DeleteQuestionsRes deleteQuestions(@RequestBody DeleteQuestionsReq req)
            throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.deleteQuestions(req);
    }
    
    @RequestMapping(value = "/getQuestions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSQuestion> getQuestions(GetQuestionsReq req)
            throws VException {
        return cMDSQuestionManager.getQuestions(req);
    }

    @RequestMapping(value = "/getAdminQuestions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSQuestion> getAdminQuestions(GetAdminQuestionsReq req)
            throws VException {
        sessionUtils.checkIfUserLoggedIn();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.getAdminQuestions(req);
    }

    @RequestMapping(value = "/changeQuestion", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSQuestion changeQuestion(@RequestBody ChangeQuestionRequest changeQuestionRequest) throws VException{
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        changeQuestionRequest.verify();
        return cMDSQuestionManager.changeQuestion(changeQuestionRequest);
    }
    
    @RequestMapping(value = "/evaluateAnswer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse evaluateAnswer(@RequestBody EvaluateAnswerReq req) throws VException {
        return cMDSQuestionManager.evaluateAnswer(req);
    }

    @RequestMapping(value = "/getQuizAnswer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<String> getQuizAnswer(@RequestBody EvaluateAnswerReq req) throws VException {
        return cMDSQuestionManager.getQuizAnswer(req);
    }

    @RequestMapping(value = "/seo/indexable", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSQuestionDto> getIndexableQuestions(@RequestParam(name = "page") int page, @RequestParam(name = "max") int max) throws VException {
        return cMDSQuestionManager.getIndexableQuestions(page, max);
    }
/*

    @RequestMapping(value = "/insertAllQuestionFromMongoDBToElasticSearch",method = RequestMethod.POST)
    @ResponseBody
    public void insertAllQuestionFromMongoDBToElasticSearch() {
        logger.info("inside controller: insertAllQuestionFromMongoDBToElasticSearch");
        cMDSQuestionManager.insertAllQuestionFromMongoDBToElasticSearch();
    }
*/

    @RequestMapping(value = "/migrateQuestionToNewTopicTree",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void migrateQuestionToNewTopicTree(){
        cMDSQuestionManager.migrateQuestionToNewTopicTree();
    }

    @RequestMapping(value = "/searchCMDSQuestionDataFromES/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSQuestionBasicInfoESPojo> searchCMDSQuestionDataFromES(@PathVariable("id") String id) throws IOException{
        return cMDSQuestionManager.searchCMDSQuestionDataFromES(id);
    }

/*
    @RequestMapping(value = "/removeDuplicates",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void removeDuplicates() {
        cMDSQuestionManager.removeDuplicates();
    }
*/

    @RequestMapping(value = "/approveQuestionStatus",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse approveQuestionStatus(@RequestBody QuestionApprovalRequest request) throws VException {
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.approveQuestionStatus(request);
    }

    @RequestMapping(value = "/getQuestionDuplicates",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<QuestionDuplicateResponse>  getQuestionDuplicates(@RequestBody QuestionDuplicationFindingRequest request) throws IOException, VException {
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.getQuestionDuplicates(request);
    }

    @RequestMapping(value = "/unarchiveQuestions",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSQuestion> unarchiveQuestions(@RequestBody QuestionDetailsOrUnarchiveRequest request) throws VException {
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.unarchiveQuestions(request);
    }

    @RequestMapping(value = "/getQuestionData",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSQuestion> getQuestionData(@RequestBody QuestionDetailsOrUnarchiveRequest request) throws VException {
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.getQuestionData(request);
    }

    @RequestMapping(value = "/getQuestionDuplicatesInSameDocument",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<QuestionDuplicateResponse>  getQuestionDuplicatesInSameDocument(@RequestBody QuestionDetailsOrUnarchiveRequest request) throws VException {
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.getQuestionDuplicatesInSameDocument(request);
    }

    @RequestMapping(value = "/changeQuestionTagsById", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE ,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSQuestion changeQuestionTagsById(@RequestBody ChangeQuestionTagsReq req) throws VException{
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return cMDSQuestionManager.changeQuestionTagsField(req);
    }

    @RequestMapping(value = "/deleteRemainingDirtyQuestionIds", method = RequestMethod.GET)
    @ResponseBody
    public void deleteRemainingDirtyQuestionIds(){
        cMDSQuestionManager.deleteRemainingDirtyQuestionIds();
    }

}
