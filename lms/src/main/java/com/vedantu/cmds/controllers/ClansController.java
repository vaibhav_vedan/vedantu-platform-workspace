/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.cmds.entities.challenges.Clan;
import com.vedantu.cmds.entities.challenges.PictureChallengeAttempt;
import com.vedantu.cmds.managers.challenges.ClansManager;
import com.vedantu.cmds.request.challenges.GetPictureChallengesReq;
import com.vedantu.cmds.request.challenges.SubmitPictureChallengeAnswerReq;
import com.vedantu.cmds.request.clans.CreateClanReq;
import com.vedantu.cmds.request.clans.CreatePictureChallengeReq;
import com.vedantu.cmds.request.clans.GetClansReq;
import com.vedantu.cmds.response.challenges.ClanLeaderBoardRes;
import com.vedantu.cmds.response.clans.CreatePictureChallengeRes;
import com.vedantu.cmds.response.clans.GetPictureChallengesRes;
import com.vedantu.cmds.response.clans.PictureChallengeLeaderBoardRes;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.request.GetLeaderBoardForChallengeReq;
import com.vedantu.lms.cmds.request.InviteClanMemberReq;
import com.vedantu.lms.cmds.request.JoinClanForEventReq;
import com.vedantu.lms.cmds.response.ClanRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/clans")
public class ClansController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ClansController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    ClansManager clansManager;

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Clan create(@RequestBody CreateClanReq req) throws VException {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        if (Role.STUDENT.equals(httpSessionData.getRole())) {
            if (req.getUserId() == null) {
                req.setUserId(httpSessionData.getUserId());
            }
        }
        Clan clan = clansManager.createClan(req);
        //TODO:should i validate categories for user?
        return clan;
    }
    
    @RequestMapping(value = "/inviteClanMembers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void inviteClanMembers(@RequestBody InviteClanMemberReq req) throws BadRequestException{
    	clansManager.inviteClanMembers(req);
    }
    @RequestMapping(value = "/joinClanForEvent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ClanRes joinClanForEvent(@RequestBody JoinClanForEventReq req) throws VException{
    	return clansManager.joinClanForEvent(req);
    }
    
    @RequestMapping(value = "/getClanMembers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ClanRes getClanMembers(@RequestParam(value = "clanId")String clanId) throws BadRequestException, NotFoundException{
    	return clansManager.getClanMembers(clanId);
    }
    
    @RequestMapping(value = "/getUsersClans", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ClanRes> getUsersClans(@RequestParam(value = "userId")Long userId) throws VException{
    	return clansManager.getUsersClans(userId);
    }

    @RequestMapping(value = "/createPictureChallenge", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CreatePictureChallengeRes createPictureChallenge(@RequestBody CreatePictureChallengeReq req) throws BadRequestException, VException {
        //  throw new UnsupportedOperationException();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (!Role.ADMIN.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Not allowed to createPictureChallenge ");
        }
        return clansManager.createPictureChallenge(req);
    }
    
    @RequestMapping(value = "/getClanLeaderBoardForDuration", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ClanLeaderBoardRes> getClanLeaderBoardForDuration(GetLeaderBoardForChallengeReq req) throws VException {
        return clansManager.getClanLeaderBoardForDuration(req);
    }
    
    @RequestMapping(value = "/getClansList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ClanRes> getClansList(GetClansReq req) throws VException{
    	return clansManager.getClansList(req);
    }
    
    @RequestMapping(value = "/getClanPointsOverall", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ClanLeaderBoardRes getClanPointsOverall(@RequestParam(value = "clanId")String clanId) throws VException{
    	return clansManager.getClanPointsOverall(clanId);
    }
    
    @RequestMapping(value = "/getPictureChallenges", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetPictureChallengesRes getPictureChallenges(GetPictureChallengesReq req) throws VException{
    	return clansManager.getPictureChallenges(req);
    }
    
    @RequestMapping(value = "/submitPictureChallengeAttempt", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PictureChallengeAttempt submitPictureChallengeAttempt(@RequestBody SubmitPictureChallengeAnswerReq req) throws BadRequestException, VException {
        //  throw new UnsupportedOperationException();
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (!Role.STUDENT.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Not allowed to submitPictureChallengeAttempt ");
        }
        req.setUserId(httpSessionData.getUserId());
        return clansManager.submitPictureChallengeAttempt(req);
    }
    
    @RequestMapping(value = "/getPictureChallengeLeaderBoard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<PictureChallengeLeaderBoardRes> getPictureChallengeLeaderBoard(@RequestParam(value = "clanId",required = false) String clanId,
            @RequestParam(value = "pictureChallengeId",required = true) String pictureChallengeId,
            @RequestParam(value = "start",required = false) Integer start,
            @RequestParam(value = "size",required = false) Integer size,
            @RequestParam(value = "addMyRank",required = false) Boolean addMyRank) throws VException {

    	if(addMyRank == null && StringUtils.isNotEmpty(clanId) && start==0){
    		addMyRank = Boolean.TRUE;
    	}
        return clansManager.getPictureChallengeLeaderBoard(clanId, pictureChallengeId,
                start, size, (start == 0));
    }
    
    @RequestMapping(value = "/markPictureChallengeActiveCron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markPictureChallengeActiveCron(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            clansManager.markPictureChallengeActiveCron();
        }
        return new PlatformBasicResponse();
    }
    
    @RequestMapping(value = "/endPictureChallengeCron", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse endPictureChallengeCron(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            clansManager.endPictureChallengeCron();
        }
        return new PlatformBasicResponse();
    }
    
    @RequestMapping(value = "/endPictureChallenge", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse endPictureChallenge(@RequestParam(value = "pictureChallengeId") String pictureChallengeId) throws VException {
    	HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (!Role.ADMIN.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Not allowed to endPictureChallenge ");
        }
    	clansManager.endPictureChallenge(pictureChallengeId);
        return new PlatformBasicResponse();
    }

}
