package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.cmds.entities.CurriculumTemplate;
import com.vedantu.cmds.managers.CurriculumTemplateManager;
import com.vedantu.cmds.request.CurriculumTemplateInsertionRequest;
import com.vedantu.cmds.request.CurriculumTemplateResponse;
import com.vedantu.cmds.request.CurriculumTemplateRetrievalFilterRequest;
import com.vedantu.cmds.request.CurriculumTemplateRetrievalRequest;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/curriculumTemplate")
public class CurriculumTemplateController {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CurriculumTemplateController.class);

    @Autowired
    private CurriculumTemplateManager curriculumTemplateManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/insertCurriculumTemplate",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse insertCurriculumTemplate(@RequestBody CurriculumTemplateInsertionRequest request) throws VException {
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        request.verify();
        return curriculumTemplateManager.insertCurriculumTemplate(request);

    }

    @RequestMapping(value = "/getFilteredCurriculumTemplates",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CurriculumTemplate> getFilteredCurriculumTemplates(@RequestBody CurriculumTemplateRetrievalFilterRequest request) throws VException{
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        request.verify();
        return curriculumTemplateManager.getFilteredCurriculumTemplates(request);
    }

    @RequestMapping(value = "/getCurriculumTemplateStructure",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CurriculumTemplateResponse getCurriculumTemplateStructure(@RequestBody CurriculumTemplateRetrievalRequest request) throws VException{
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        request.verify();
        return curriculumTemplateManager.getCurriculumTemplateStructure(request);
    }


}
