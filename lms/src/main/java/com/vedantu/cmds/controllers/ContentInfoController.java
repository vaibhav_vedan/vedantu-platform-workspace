package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.cmds.request.ChangeTeacherReq;
import com.vedantu.cmds.request.GenericEndTestReq;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.lms.cmds.pojo.ContentStats;
import com.vedantu.lms.request.ContentInfoReq;
import com.vedantu.moodle.response.HomepageTestResponse;
import io.swagger.annotations.ApiOperation;
import lombok.NonNull;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.pojo.StudentTestBasicInfo;
import com.vedantu.lms.cmds.pojo.TestInfoForBatchProgress;
import com.vedantu.lms.cmds.request.ShareContentReq;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.vedantu.cmds.dao.CMDSQuestionDAO.gson;

@RestController
@RequestMapping("/cmds/contentInfo")
public class ContentInfoController {

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    HttpSessionUtils sessionUtils;

    private Logger logger = LogFactory.getLogger(ContentInfoController.class);

    @RequestMapping(value = "/shareContentByTeacher", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> shareContentByTeacher(@RequestBody ShareContentReq req) throws MalformedURLException, InternalServerErrorException {
        List<ContentInfo> contentInfos = contentInfoManager.shareContentByTeacher(req);
        List<String> contentInfoIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(contentInfos)) {
            for (ContentInfo contentInfo : contentInfos) {
                contentInfoIds.add(contentInfo.getId());
                //sendContentInfoNotifications(contentInfo, ContentInfoCommunicationType.SHARED, null);
            }
        }
        return contentInfoIds;
    }

    @RequestMapping(value = "/shareContentByTeacherForMigration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse shareContentByTeacherForMigration(@RequestBody ShareContentReq req) throws MalformedURLException, InternalServerErrorException, BadRequestException {
        return contentInfoManager.shareContentByTeacherForMigrationToSQS(req);
    }

    @RequestMapping(value = "/getTestInfoForStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TestInfoForBatchProgress getTestInfoForStudent(StudentBatchProgressReq req) throws BadRequestException {
        return contentInfoManager.getTestInfoForStudent(req);
    }

    @RequestMapping(value = "/getContentStatsForBatches", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, ContentStats> getContentStats(@RequestParam(value = "batchIds", required = true) List<String> batchIds,
                                                     @RequestParam(value = "time", required = true) Long time) {
        return contentInfoManager.getContentStats(batchIds, time);
    }

    @RequestMapping(value = "/getBatchTestScoreInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, List<StudentTestBasicInfo>> getBatchTestScoreInfo(TeacherBatchProgressReq req) throws BadRequestException {
        return contentInfoManager.getBatchTestScoreInfo(req);
    }

    @RequestMapping(value = "/getTestInfoForBatches", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getTestInfoForBatches(@RequestParam(value = "batchIds", required = true) List<String> batchIds,
                                                   @RequestParam(value = "userId", required = true) String userId) throws BadRequestException {
        return contentInfoManager.getTestInfoForBatches(batchIds, userId);
    }

    @RequestMapping(value = "/getContentInfosByContextId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getContentInfosByContextId(@RequestParam(value = "contextId", required = false) List<String> contextId,
                                                        @RequestParam(value = "beforeCreationTime", required = false) Long beforeCreationTime, @RequestParam(value = "afterCreationTime", required = false) Long afterCreationTime) throws BadRequestException {
        return contentInfoManager.getContentInfosByContextId(contextId, afterCreationTime, beforeCreationTime);
    }

    @RequestMapping(value = "/getContentInfosByFilter", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getContentInfosByFilter(@RequestBody ContentInfoReq req) throws BadRequestException {
        return contentInfoManager.getContentInfosByFilter(req);
    }

    @RequestMapping(value = "/changeTeacherIdForContent", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse changeTeacherIdForContent(@RequestBody ChangeTeacherReq req) throws VException {

        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        req.verify();
        if (req.getCallingUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "calling user id is null");
        }
        contentInfoManager.changeTeacherIdForContent(req.getTeacherId().toString(), req.getTestId(), req.getCallingUserId().toString());
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getRecentlyAttemptedTests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Long> getRecentlyAttemptedTests(@RequestParam("userId") String userId, @RequestParam("releaseDate") Long releaseDate)
    {
        return ResponseEntity.ok(contentInfoManager.getRecentlyAttemptedTests(userId, releaseDate));
    }

//    @RequestMapping(value = "/migrateTestAttemptsFromContentInfo",method = RequestMethod.GET)
//    public void migrateTestAttemptsFromContentInfo(){
//        contentInfoManager.migrateTestAttemptsFromContentInfo();
//    }

    @ApiOperation("get upcoming tests for the user")
    @RequestMapping(value = "/getHomepageUpcomingTests", method = RequestMethod.GET)
    @ResponseBody
    public List<HomepageTestResponse> getHomepageUpcomingTests(@RequestParam(name = "studentId") String studentId) throws VException {
        if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.STUDENT), false);
        return contentInfoManager.getHomepageUpcomingTests(studentId);
    }

    @ApiOperation("get past unattempted tests for the user")
    @RequestMapping(value = "/getHomepageLiveTests", method = RequestMethod.GET)
    @ResponseBody
    public List<HomepageTestResponse> getHomepagePastTests(@NonNull @RequestParam(name = "studentId") String studentId) throws VException {
        Long callingUserId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(Role.STUDENT.equals(role) && (Objects.isNull(callingUserId) || !callingUserId.equals(Long.valueOf(studentId)))){
            throw new BadRequestException(ErrorCode.ACCESS_NOT_ALLOWED , callingUserId+" is not allowed to access "+ studentId +"'s data");
        }
        return contentInfoManager.getLiveAndPastUnattemptedTests(studentId);
    }
}
