package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.cmds.enums.RedisKeyCategory;
import com.vedantu.cmds.managers.AdminService;
import com.vedantu.cmds.request.AdminRedisSetReq;
import com.vedantu.cmds.response.RedisKeyValuePair;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
@RequestMapping("/admin")
public class AdminController {

    private Logger LOGGER = LogFactory.getLogger(AdminController.class);
    private AdminService adminService;
    private HttpSessionUtils sessionUtils;

    @Autowired
    public AdminController(AdminService adminService, HttpSessionUtils sessionUtils) {
        this.adminService = adminService;
        this.sessionUtils = sessionUtils;
    }

    @ResponseBody
    @RequestMapping(value = "/setRedisKey", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse setRedisKey(@RequestBody AdminRedisSetReq adminRedisSetReq) throws VException {
        LOGGER.info("method=setRedisKey, class=AdminController");
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, true);
        adminService.setRedisKey(adminRedisSetReq.getKey(), adminRedisSetReq.getValue());
        return new PlatformBasicResponse();
    }

    @ResponseBody
    @RequestMapping(value = "/setMultipleRedisKeys", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse setMultipleRedisKeys(@RequestBody List<AdminRedisSetReq> adminRedisSetReq) throws VException {
        LOGGER.info("method=setMultipleRedisKeys, class=AdminController");
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, true);
        for (AdminRedisSetReq redisSetReq : adminRedisSetReq)
            adminService.setRedisKey(redisSetReq.getKey(), redisSetReq.getValue());
        return new PlatformBasicResponse();
    }

    @ResponseBody
    @RequestMapping(value = "/getRedisValueForKey", method = GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String getRedisValueForKey(@RequestParam(value = "key") String key) throws VException {
        LOGGER.info("method=getRedisValueForKey, class=AdminController");
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, true);
        return adminService.getRedisKey(key);
    }

    @ResponseBody
    @RequestMapping(value = "/getAllRedisCategories", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RedisKeyCategory> getAllRedisCategories() throws VException {
        LOGGER.info("method=getAllRedisCategories, class=AdminController");
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), Role.ADMIN, true);
        return adminService.getAllRedisCategories();
    }
}
