/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.WebUtils;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/ping")
public class PingController {

    @RequestMapping(value = "/lms_noti_list", method = RequestMethod.GET)
    @ResponseBody
    public String lms_noti_list() throws VException {
        String listResp = pingSubSystem("listing");
        String notResp = pingSubSystem("notification-centre");
        return listResp + notResp;
    }

    private String pingSubSystem(String subsystem) throws VException {
        String userEndpoint = "http://localhost/"+subsystem;
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/index.jsp", HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return jsonString;
    }
}
