package com.vedantu.cmds.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import com.vedantu.User.enums.SubRole;
import com.vedantu.cmds.entities.*;
import com.vedantu.cmds.request.*;
import com.vedantu.cmds.response.*;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.moodle.response.HomepageTestResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.lms.cmds.pojo.TestAndAttemptDetails;
import com.vedantu.lms.cmds.pojo.TestUserInfo;
import com.vedantu.lms.cmds.request.*;
import com.vedantu.lms.request.AddOrEditTestUserReq;
import com.vedantu.lms.request.UnshareContentReq;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.subscription.request.GetBundleTestDetailsReq;
import com.vedantu.util.PlatformBasicResponse;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.managers.CMDSTestAttemptManager;
import com.vedantu.cmds.managers.CMDSTestManager;
import com.vedantu.cmds.managers.ExportManager;
import com.vedantu.cmds.pojo.CMDSTestBasicInfo;
import com.vedantu.cmds.pojo.CMDSTestInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.pojo.UserTestResultPojo;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.pojo.request.ChangeAnswerRequest;
import com.vedantu.moodle.pojo.response.BasicRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

import inti.ws.spring.exception.client.BadRequestException;
import inti.ws.spring.exception.client.ForbiddenException;
import inti.ws.spring.exception.server.InternalServerError;

import java.util.Objects;
import java.util.Set;

import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.ResponseBody;

@CrossOrigin
@RestController
@RequestMapping("cmds/test")
public class CMDSTestController {

    @Autowired
    private CMDSTestManager testManager;

    @Autowired
    private CMDSTestAttemptManager testAttemptManager;

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    public LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private ExportManager exportManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(CMDSTestController.class);

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSTest createTest(@RequestBody CreateTestReq cmdsTest) throws VException, BadRequestException {
        logger.info("Request : " + cmdsTest.toString());
        return testManager.createCMDSTest(cmdsTest);
    }

    @Deprecated
    @RequestMapping(value = "/getTests", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSTestInfo> getTests(@ModelAttribute GetCMDSTestReq req) throws VException, BadRequestException {
        logger.info("Request : " + req.toString());
        return testManager.getCMDSTests(req);
    }

    @RequestMapping(value = "/getTestsInfos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSTest> getTestsInfos(@ModelAttribute GetCMDSTestReq req) throws VException, BadRequestException {
        logger.info("Request : " + req.toString());
        return testManager.getCMDSTestsInfos(req);
    }

    @RequestMapping(value = "/getTestDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSTestInfo getTestDetails(@RequestParam(name = "entityType", required = true) CMDSEntityType entityType,
            @RequestParam(name = "entityId", required = true) String entityId,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole)
            throws VException, BadRequestException {
        return testManager.getTestDetails(entityType, entityId, callingUserId, callingUserRole);
    }

    @RequestMapping(value = "/addOrEditTestUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public TestUserInfo addOrEditTestUser(@RequestBody AddOrEditTestUserReq req)
            throws VException, BadRequestException, MalformedURLException, ForbiddenException, InternalServerError {
        logger.info("Request : " + req.toString());
        return testAttemptManager.addOrEditTestUser(req);
    }

    @RequestMapping(value = "/startAttempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public StartTestResponse startAttempt(@RequestBody CMDSStartTestAttemptReq req)
            throws VException, MalformedURLException {
        logger.info("Request : " + req.toString());
        if (Objects.nonNull(sessionUtils.getCurrentSessionData()) && Objects.nonNull(sessionUtils.getCurrentSessionData().getRole()) && !Role.STUDENT.equals(sessionUtils.getCurrentSessionData().getRole())) {
            throw new com.vedantu.exception.ForbiddenException(ErrorCode.STUDENT_ATTEMPT_ALLOWED_ONLY, "Non student role not allowed to attempt");
        }
        req.verify();
        return testAttemptManager.startCMDSTest(req);
    }

    @RequestMapping(value = "/cacheTestAttemptProgress", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse cacheTestAttemptProgress(@RequestBody TestProgressCacheRequest req) throws VException {
        return testAttemptManager.cacheTestAttemptProgress(req);
    }

    @RequestMapping(value = "/getCachedTestAttemptProgress", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse getCachedTestAttemptProgress(@RequestParam(name = "attemptId") String attemptId) throws VException {
        return testAttemptManager.getCachedTestAttemptProgress(attemptId);
    }

    @RequestMapping(value = "/submitQuestionAttempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse submitQuestionAttempt(@RequestBody BulkSubmitQuestionAttemptRequest req) throws VException {
        logger.info("Request : " + req.toString());
        return testAttemptManager.submitQuestionAttempt(req);
    }

    @RequestMapping(value = "/submitTestFeedback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse submitTestFeedback(@RequestBody TestFeedbackRequest req) throws VException {
        logger.info("Request : " + req.toString());
        return testAttemptManager.submitTestFeedback(req);
    }

    @RequestMapping(value = "/submitAttempt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSTestAttempt submitAttempt(@RequestBody CMDSSubmitTestAttemptReq req)
            throws VException, UnsupportedEncodingException {
        return testAttemptManager.submitCMDSTest(req);
    }

    @RequestMapping(value = "/linkAttemptToUser", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse linkAttemptToUser(@RequestBody CMDSLinkAttemptToUserReq req)
            throws MalformedURLException, InternalServerErrorException, VException, UnsupportedEncodingException {
        logger.info("Request : " + req.toString());
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        testAttemptManager.linkAttemptToUser(req, httpSessionData);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/evaluateTest", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSTestAttempt evaluateTest(@RequestBody CMDSEvaluateTestAttemptReq req)
            throws VException, UnsupportedEncodingException {
        return testAttemptManager.evaluateCMDSTest(req);
    }

    @RequestMapping(value = "/getAttemptDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public StartTestResponse getAttemptDetails(@RequestParam(name = "attemptId") String attemptId,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole)
            throws VException {
        return testAttemptManager.getTestAttemptById(attemptId, callingUserId, callingUserRole);
    }

    @RequestMapping(value = "/getAttemptLeaderBoard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetLeaderboardInfo getAttemptLeaderBoardInfo(@RequestParam(name = "attemptId", required = true) String attemptId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "callingUserId", required = false) Long callingUserId)
            throws VException, BadRequestException, ForbiddenException {
        Role role = sessionUtils.getCallingUserRole();
        if (Role.STUDENT.equals(role)) {
            callingUserId = sessionUtils.getCallingUserId();
        }
        if (callingUserId == null) {
            throw new com.vedantu.exception.BadRequestException(ErrorCode.USER_LOGIN_REQUIRED, "");
        }
        return testAttemptManager.getLeaderboardInfoForAttempt(attemptId, callingUserId.toString(), start, size);
    }

    @RequestMapping(value = "/attemptViewed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicRes attemptViewed(@RequestParam(name = "attemptId", required = true) String attemptId,
            @RequestParam(name = "callingUserId", required = false) String callingUserId,
            @RequestParam(name = "callingUserRole", required = false) Role callingUserRole)
            throws VException, BadRequestException, ForbiddenException {
        testAttemptManager.attemptViewed(attemptId, callingUserId, callingUserRole);
        return new BasicRes();
    }

    @RequestMapping(value = "/shareBulk", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicRes shareBulk(@RequestBody CMDSShareOTFContentsReq req)
            throws VException, BadRequestException, ForbiddenException {
        logger.info("Request : " + req.toString());
        testManager.shareBulk(req);
        return new BasicRes();
    }

    @RequestMapping(value = "/endTests", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void endTests(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {

            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());

            testAttemptManager.sendEndTestsRequestAsync();

            //This is a 5min cron so we are piggybacking on this.
            testManager.sendCalculateTestAnalyticsRequestAsync();
        }

    }

    @RequestMapping(value = "/unshareContent", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse unshareContent(@RequestBody UnshareContentReq req) {
        return contentInfoManager.unshareContent(req);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicRes deleteTest(@RequestBody DeleteTestReq request)
            throws VException {
        logger.info("Request : " + request.toString());
        testManager.deleteTestById(request);
        return new BasicRes();
    }

    @RequestMapping(value = "/getTestAndAttemptDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, TestAndAttemptDetails> getTestAndAttemptDetails(@ModelAttribute GetBundleTestDetailsReq req) throws com.vedantu.exception.BadRequestException {
        return testManager.getTestAndAttemptDetails(req);
    }

    // TODO : make sure we don't send attemptId in response
    @RequestMapping(value = "/getTestAttempts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ContentInfo> getTestAttempts(@RequestParam(name = "testId", required = true) String testId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size) throws VException, BadRequestException {
        sessionUtils.checkIfUserLoggedIn();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testManager.getTestAttempts(testId, start, size);
    }

    @RequestMapping(value = "/getTestAttemptsExport", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse getTestAttemptsExportAsync(@RequestParam(name = "testId", required = true) String testId
    ) throws VException, BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
        exportManager.getTestAttemptsExportAsync(testId, httpSessionData);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getTestBasicDetailsByContentInfoId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSTestBasicInfo getTestBasicDetailsByContentInfoId(@RequestParam(name = "contentInfoId", required = true) String contentInfoId)
            throws VException, BadRequestException {
        return testManager.getTestBasicDetailsByContentInfoId(contentInfoId);
    }

    @RequestMapping(value = "/getTestBasicDetails", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSTestBasicInfo getTestBasicDetails(@RequestParam(name = "testId", required = true) String testId)
            throws VException, BadRequestException {
        return testManager.getTestBasicDetails(testId);
    }

    @RequestMapping(value = "/getTestsBasicDetails", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSTest> getTestsBasicDetails(@RequestBody List<String> testIds)
            throws VException {
        return testManager.getTestsBasicDetails(testIds);
    }

    @RequestMapping(value = "/getEvaluatedAttempts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ContentInfo> getEvaluatedAttempts(@RequestParam(name = "testId") String testId,
            @RequestParam(name = "userId") String userId)
            throws com.vedantu.exception.BadRequestException {
        return testManager.getEvaluatedAttempts(testId, userId);
    }

    @RequestMapping(value = "/calculateTestRank", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse calculateTestRank(@RequestParam(name = "testId", required = true) String testId)
            throws VException {
        testManager.calculateTestRank(testId);
        return new PlatformBasicResponse();
    }
//    
//    @RequestMapping(value = "/calculateTestRanks", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public void calculateTestRanks(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
//            @RequestBody String request) throws Exception {
//        Gson gson = new Gson();
//        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
//        if (messageType.equals("SubscriptionConfirmation")) {
//            String json = null;
//            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
//            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
//            logger.info(resp.getEntity(String.class));
//        } else if (messageType.equals("Notification")) {
//            logger.info("Notification received - SNS");
//            logger.info(subscriptionRequest.toString());
//            testManager.calculateTestRanksAsync();
//        }
//    }
//    

    @RequestMapping(value = "/getTestForQuestionId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSTest> getTestForQuestionId(@RequestParam(name = "questionId", required = true) String questionId,
            @RequestParam(name = "beforeTime", required = false) Long beforeTime) throws VException, BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testManager.getTestsWithQuestion(questionId, beforeTime, true);
    }

    @RequestMapping(value = "/getTestForChangeQuestion", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public TestsForQuestionIdRes getTestForChangeQuestion(@RequestParam(name = "questionId", required = true) String questionId,
            @RequestParam(name = "beforeTime", required = false) Long beforeTime) throws VException, BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testManager.getTestsForChangeQuestion(questionId, beforeTime);
    }

    @RequestMapping(value = "/getChallengesForQuestionId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Challenge> getChallengesForQuestionId(@RequestParam(name = "questionId", required = true) String questionId,
            @RequestParam(name = "beforeTime", required = false) Long beforeTime) throws VException, BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testManager.getChallengesWithQuestion(questionId, beforeTime);
    }

    @RequestMapping(value = "/changeAnswer", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void changeAnswerForQuestion(@RequestBody ChangeAnswerRequest req) throws com.vedantu.exception.BadRequestException, NotFoundException, com.vedantu.exception.ForbiddenException, VException {
        req.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        testManager.editAnswerFlow(req);

    }

    /*@RequestMapping(value = "/sendNotification", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendNotificationForTest(@RequestParam(name = "testId", required=true) String testId, @RequestParam(name = "beforeTime", required=true) Long beforeTime) throws com.vedantu.exception.BadRequestException, NotFoundException, com.vedantu.exception.ForbiddenException, VException{

        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);        
        //testManager.editAnswerFlow(req);
        
    }    */
 /*@RequestMapping(value = "/addEvalLogic", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicRes addEvalLogic(@RequestBody AddEvalLogicRequest addEvalLogicRequest)
            throws VException, BadRequestException {
        addEvalLogicRequest.verify();
        logger.info("Request : " + addEvalLogicRequest.toString());
        testManager.addEvalLogic(addEvalLogicRequest);
        //testManager.deleteTestById(request);
        return new BasicRes();
    }*/
    @RequestMapping(value = "/getUserTestResultPojos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserTestResultPojo> getUserTestResultPojos(@RequestParam(name = "batchIds", required = true) List<String> batchIds, @RequestParam(name = "userId", required = true) String userId) throws com.vedantu.exception.BadRequestException {
        logger.info("In lms getUserTestResultPojos");
        return testManager.getUserTestResultPojos(batchIds, userId);

    }

    @RequestMapping(value = "/getUserTestResultPojosForOTO", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<UserTestResultPojo> getOTOUserTestResultPojos(@RequestParam(name = "coursePlanIds", required = true) List<String> coursePlanIds, @RequestParam(name = "userId", required = true) String userId) throws com.vedantu.exception.BadRequestException {

        return testManager.getUserTestResultPojosForOTO(coursePlanIds, userId);

    }

    @RequestMapping(value = "/getAMTestStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AMTestStats> getAMTestStats(@RequestParam(name = "groupName", required = true) String groupName) throws com.vedantu.exception.ForbiddenException, VException {
        if (!(Role.ADMIN.equals(sessionUtils.getCallingUserRole()) || Role.STUDENT_CARE.equals(sessionUtils.getCallingUserRole()) || Role.TEACHER.equals(sessionUtils.getCallingUserRole()))) {
            sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
            throw new com.vedantu.exception.ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "only admin or student care or teacher role allowed");
        }

        Long userId = sessionUtils.getCallingUserId();
        return testManager.getAMTestStatsForGroup(groupName, userId.toString());

    }

    @RequestMapping(value = "/preCalculateTestStats", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void preCalculateTestStats(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            testManager.processCompetitiveTestAsync();
        }
    }

    @RequestMapping(value = "/migrateTestsStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void migrateTestStats() {
        testManager.processCompetitiveTestAsyncForMigration();
    }

    @RequestMapping(value = "/getTableStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TestTableStatsRes getTableStats(@RequestParam(name = "attemptId", required = true) String attemptId) throws com.vedantu.exception.BadRequestException, NotFoundException, com.vedantu.exception.ForbiddenException {
        return testAttemptManager.getTestTableStats(sessionUtils.getCallingUserId(), attemptId);
    }

    @RequestMapping(value = "/getChartStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Deprecated
    public TestChartStatsRes getChartStats(@RequestParam(name = "attemptId", required = true) String attemptId) throws com.vedantu.exception.BadRequestException, NotFoundException, com.vedantu.exception.ForbiddenException {
        return testAttemptManager.getTestChartStats(sessionUtils.getCallingUserId(), attemptId);
    }

    @RequestMapping(value = "/getChartAnalytics", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PerformanceReportStats getChartAnalytics(@RequestParam(name = "attemptId", required = true) String attemptId) throws com.vedantu.exception.BadRequestException, NotFoundException, com.vedantu.exception.ForbiddenException {
        return testAttemptManager.getChartAnalytics(sessionUtils.getCallingUserId(), attemptId);
    }

    @RequestMapping(value = "/addEditCMDSTestList", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSTestList addEditCMDSTestList(@RequestBody AddTestListReq addTestListReq) throws Exception {
        return testManager.addEditCMDSTestList(addTestListReq);
    }

    @RequestMapping(value = "/getTestLists", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSTestListRes> getTestLists(GetCMDSTestListReq req) throws com.vedantu.exception.ForbiddenException, BadRequestException, VException {
        req.removeTargetBoardsFromMainTags();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        if (Role.STUDENT.equals(sessionData.getRole())) {
            req.setPublished(true);
        }
        return testManager.getCMDSTestLists(req);
    }

    @RequestMapping(value = "/saveTestListOrderForGrade", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSTestListOrder savePlaylistOrderForGrade(@RequestBody GradeCMDSPlaylistOrder req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testManager.savePlaylistOrderForGrade(req);
    }

    @RequestMapping(value = "/getOrderedTestListByGrade", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TestListOrderForGradeRes getOrderedPlaylistByGrade(@RequestParam(value = "grade", required = true) String grade) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testManager.getOrderedTestListByGrade(grade);
    }

    @RequestMapping(value = "/getTestSeriesById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CMDSTestList getTestSeriesById(@RequestParam(value = "id", required = true) String id) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return testManager.getTestSeriesById(id);
    }

    @RequestMapping(value = "/getTestMainListsMobile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSTestListRes> getTestMainLists(GetCMDSTestListReq req) throws com.vedantu.exception.ForbiddenException, com.vedantu.exception.BadRequestException, VException {
        req.removeTargetBoardsFromMainTags();
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        if (Role.STUDENT.equals(sessionData.getRole())) {
            req.setPublished(true);
        }
        return testManager.getTestMainLists(req, sessionData.getUserId());
    }

    @RequestMapping(value = "/getAttemptedTestsMobile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getAttemptedTestsMobile(GetTestsMobileReq req) throws com.vedantu.exception.ForbiddenException, com.vedantu.exception.BadRequestException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        return testManager.getAttemptedTestsMobile(sessionData.getUserId(), req);
    }

    @RequestMapping(value = "/getSavedTestsMobile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSTestInfo> getSavedTestsMobile(GetTestsMobileReq req) throws com.vedantu.exception.ForbiddenException, VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        return testManager.getSavedTestsMobile(sessionData.getUserId(), req);
    }

    @RequestMapping(value = "/getOngoingTestsMobile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getOngoingTestsMobile(GetTestsMobileReq req) throws com.vedantu.exception.ForbiddenException, VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        return testManager.getOngoingTestsMobile(sessionData.getUserId(), req);
    }

    @RequestMapping(value = "/getFinishedTestsMobile", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getFinishedTestsMobile(GetTestsMobileReq req) throws com.vedantu.exception.ForbiddenException, VException {
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        return testManager.getFinishedTestsMobile(sessionData.getUserId(), req);
    }

    @RequestMapping(value = "/filterTestCheckerData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSTestCheckerResponse> filterTestCheckerData(@RequestBody CheckerQuestionSetOrTestFilterRequest request) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return testManager.filterTestCheckerData(request);
    }

    @RequestMapping(value = "/deleteUnnecessaryData", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void deleteUnnecessaryData(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
            @RequestBody String request) {
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info("ClientResponse : " + resp.toString());
        } else if (messgaetype.equals("Notification")) {
            testManager.deleteUnnecessaryData();
        }
    }

    @RequestMapping(value = "/calculateAnalytics", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void calculateAnalytics(@RequestParam(name = "testId", required = true) String testId) throws VException {
        testManager.calculateTestAnalytics(testId);
    }

    @ApiOperation("get a map of key- testId - and value - a list of absent session response objects - ")
    @RequestMapping(value = "/getSubjectsForTests", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Set<String>> getSubjectsForTests(@RequestParam(name = "testIds") List<String> testIds) throws VException {
        if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.STUDENT), false);
        return testManager.getSubjectsForTests(testIds);
    }
}
