/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import com.vedantu.cmds.managers.AmazonS3Manager;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.PlatformBasicResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("cmds/upload")
public class CMDSUploadController {

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    @RequestMapping(value = "/getpresignedurl", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getPreSignedUrl(
            @RequestParam(value = "uploadTarget", required = true) UploadTarget uploadTarget,
            @RequestParam(value = "contentType") String contentType,
            @RequestParam(value = "midFolderEntityId", required = false) String midFolderEntityId) throws Exception {

        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            String preSignedUrl = amazonS3Manager.getPresignedUrl(uploadTarget, contentType, midFolderEntityId);
            platformBasicResponse.setResponse(preSignedUrl);
            platformBasicResponse.setSuccess(true);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    @RequestMapping(value = "/getsignedurl", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getsignedurl(
            @RequestParam(value = "uploadTarget", required = true) UploadTarget uploadTarget,
            @RequestParam(value = "fileName") String fileName) throws Exception {

        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            String preSignedUrl = amazonS3Manager.getImageUrl(fileName, uploadTarget);
            platformBasicResponse.setResponse(preSignedUrl);
            platformBasicResponse.setSuccess(true);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

}
