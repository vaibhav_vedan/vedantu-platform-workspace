/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import com.vedantu.cmds.fs.parser.QuestionRepositoryDocParser;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import java.io.File;
import java.io.IOException;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/cmds/contentparser")
public class ContentParserController {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;
    
    @Autowired
    private QuestionRepositoryDocParser questionRepositoryDocParser;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ContentParserController.class);

    @RequestMapping(value = "/getHtmlFromDocFile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getHtmlFromDocFile(@RequestParam(name = "file", required = true) MultipartFile multipart, @RequestParam(name = "target", required = true) UploadTarget target) throws VException, IOException, Exception {
        PlatformBasicResponse res= new PlatformBasicResponse();
        File file = CommonUtils.multipartToFile(multipart);
        String html=questionRepositoryDocParser.getHtmlFromDocFile(file, target);
        res.setResponse(html);
        return res;
    }
  
}
