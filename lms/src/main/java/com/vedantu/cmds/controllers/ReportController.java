package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.cmds.entities.Report;
import com.vedantu.cmds.enums.RedisKeyCategory;
import com.vedantu.cmds.managers.AdminService;
import com.vedantu.cmds.managers.ReportManager;
import com.vedantu.cmds.request.AdminRedisSetReq;
import com.vedantu.cmds.request.GetReportsReq;
import com.vedantu.cmds.request.ReportReq;
import com.vedantu.cmds.request.ReviewReportReq;
import com.vedantu.cmds.response.RedisKeyValuePair;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@CrossOrigin
@RestController
@RequestMapping("/report")
public class ReportController {

    @Autowired
    private ReportManager reportManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private HttpSessionUtils sessionUtils;


    @ResponseBody
    @RequestMapping(value = "/reportEntity", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Report reportEntity(@RequestBody ReportReq req) throws VException {
        Report report = mapper.map(req, Report.class);
        reportManager.reportEntity(report);
        return report;
    }
    @ResponseBody
    @RequestMapping(value = "/getReports", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Report>  getReports(GetReportsReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return reportManager.getReports(req);
    }
    @ResponseBody
    @RequestMapping(value = "/reviewReports", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void reviewReports(@RequestBody ReviewReportReq req) throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        reportManager.reviewReports(req);

    }

    @ResponseBody
    @RequestMapping(value = "/populateCount", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void populateCount() throws Exception {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        reportManager.populateCount();

    }


    @ResponseBody
    @RequestMapping(value = "/getReportsForCsv", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Report>  getReportsForCsv(GetReportsReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return reportManager.getReportsForCsv(req);
    }
}
