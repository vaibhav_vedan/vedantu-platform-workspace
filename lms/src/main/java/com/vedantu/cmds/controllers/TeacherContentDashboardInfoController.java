package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.cmds.managers.TeacherContentDashboardInfoManager;
import com.vedantu.cmds.request.TeacherIdsMigrationRequest;
import com.vedantu.exception.VException;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacherContentDashboardInfo")
public class TeacherContentDashboardInfoController {

    @Autowired
    private TeacherContentDashboardInfoManager dashboardInfoManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/migrateTeacherContentInfo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void migrateTeacherContentInfo(@RequestBody TeacherIdsMigrationRequest request) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        dashboardInfoManager.migrateTeacherContentInfo(request.getTeacherIds());
    }

}
