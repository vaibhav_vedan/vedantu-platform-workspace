/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.controllers;

import com.vedantu.User.Role;
import com.vedantu.cmds.entities.VideoSchedule;
import com.vedantu.cmds.managers.VideoScheduleManager;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vedantu.util.StringUtils;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/videoschedule")
public class VideoScheduleController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VideoScheduleController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    VideoScheduleManager videoScheduleManager;

    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse create(@RequestBody List<VideoSchedule> req) throws BadRequestException, VException, Exception {
        HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData(true);
        if (!Role.ADMIN.equals(httpSessionData.getRole())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Non admin role not allowed to create schedule");
        }
        videoScheduleManager.create(req, httpSessionData.getUserId().toString());
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<VideoSchedule> get(@RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size,
            @RequestParam(name = "fromTime", required = false) Long fromTime,
            @RequestParam(name = "tillTime", required = false) Long tillTime,
            @RequestParam(name = "mainTags", required = false) Set<String> mainTags) throws VException {
        //TODO store response in redis for the initial start,size
        mainTags=StringUtils.removeTargetBoardsFromMainTags(mainTags);
        return videoScheduleManager.get(mainTags, fromTime, tillTime, start, size);
    }
}
