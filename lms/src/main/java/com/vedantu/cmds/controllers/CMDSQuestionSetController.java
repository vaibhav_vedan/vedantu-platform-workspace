package com.vedantu.cmds.controllers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.request.*;
import com.vedantu.cmds.response.CMDSQuestionSetCheckerResponse;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.vedantu.cmds.entities.CMDSQuestionSet;
import com.vedantu.cmds.managers.CMDSQuestionSetManager;
import com.vedantu.cmds.managers.CMDSTestManager;
import com.vedantu.cmds.pojo.ParseQuestionMetadata;
import com.vedantu.cmds.response.CMDSQuestionSetInfo;
import com.vedantu.cmds.response.ConfirmQuestionSetUploadRes;
import com.vedantu.cmds.response.UploadQuestionSetFileRes;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.moodle.pojo.response.BasicRes;

import inti.ws.spring.exception.client.BadRequestException;

@CrossOrigin
@RestController
@RequestMapping("cmds/questionSet")
public class CMDSQuestionSetController {

    @Autowired
    private CMDSQuestionSetManager questionSetManager;

    @Autowired
    private CMDSTestManager testManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private RedisDAO redisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSQuestionSetController.class);

    private static final Gson gson = new Gson();


    @RequestMapping(value = "/createQuestionSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UploadQuestionSetFileRes createQuestionSet(@RequestBody CreateQuestionSetReq createQuestionSetReq) throws Exception {
        return questionSetManager.createQuestionSet(createQuestionSetReq.getQuestionIds());
    }

    @RequestMapping(value = "/uploadQuestionSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> uploadQuestionSet(
            @RequestParam(name = "file") MultipartFile multipart,
            @RequestParam(name = "questionSetFileName") String questionSetFileName,
            @RequestParam(name = "userId") String userId,
            @RequestParam(name = "accessLevel", required = false) AccessLevel accessLevel,
            @RequestParam(name = "book", required = false) String book,
            @RequestParam(name = "title", required = false) String title,
            @RequestParam(name = "subject", required = false) String subject,
            @RequestParam(name = "edition", required = false) String edition,
            @RequestParam(name = "chapter", required = false) String chapter,
            @RequestParam(name = "chapterNo", required = false) String chapterNo,
            @RequestParam(name = "exercise", required = false) String exercise,
            @RequestParam(name = "pageNos", required = false) List<String> pageNos,
            @RequestParam(name = "topics", required = false) List<String> topics,
            @RequestParam(name = "subTopics", required = false) List<String> subTopics,
            @RequestParam(name = "tags", required = false) List<String> tags,
            @RequestParam(name = "grades", required = false) List<String> grades,
            @RequestParam(name = "targetgrade", required = false) List<String> targetgrade,
            @RequestParam(name = "targets", required = false) List<String> targets,
            @RequestParam(name = "type", required = false) VedantuRecordState type)
            throws Exception {
        logger.info("Request : uploadQuestionSet: " + questionSetFileName);

        logger.info("In case of access level not present will assign it to private. Present `accessLevel` has value {}",accessLevel);
        if (accessLevel == null) {
            accessLevel = AccessLevel.PRIVATE;
        }

        File file = CommonUtils.multipartToFile(multipart);
        logger.info("After conversion from multipart to file the size of the file is " + file.length());

        ParseQuestionMetadata metadata = new ParseQuestionMetadata(title, subject, topics, subTopics, tags, grades, targetgrade, targets, book, edition, chapter, chapterNo, exercise, pageNos);
        logger.info("After parsing the metadata value is {}",metadata);

        UploadQuestionSetFileReq uploadQuestionSetFileReq = new UploadQuestionSetFileReq(file,
                questionSetFileName, userId, accessLevel, metadata, type);
        logger.info("After making a question set file request it looks like {}",uploadQuestionSetFileReq);

        String token = questionSetManager.generateToken();
        logger.info("Generated token is {}",token);

        Map<String, Object> response = new HashMap<>();
        response.put("token", token);

        Map<String, String> saveProcessingMap = new HashMap<>();
        saveProcessingMap.put("processing", "true");

        Map<String, String> redisProcessingMap = new HashMap<>();
        redisProcessingMap.put(token, gson.toJson(saveProcessingMap));
        redisDAO.setexKeyPairs(redisProcessingMap, 600);

        ExecutorService executorService = Executors.newFixedThreadPool(1);

        CompletableFuture.runAsync(() -> {
            try {

                logger.info("Starting to upload the question set file...");
                UploadQuestionSetFileRes res = questionSetManager.uploadQuestionFile(uploadQuestionSetFileReq);

                Map<String, String> redisMap = new HashMap<>();
                Map<String, String> saveMap = new HashMap<>();

                saveMap.put("processing", "false");
                saveMap.put("success", res.getQuestionSetId());
                saveMap.put("error", null);

                redisMap.put(token, gson.toJson(saveMap));
                redisDAO.setexKeyPairs(redisMap, 600);
            }
            catch(Exception e) {
                Map<String, String> redisErrorMap = new HashMap<>();
                Map<String, String> saveMap = new HashMap<>();

                saveMap.put("processing", "false");
                saveMap.put("success", null);
                saveMap.put("error", e.getMessage());
                redisErrorMap.put(token, gson.toJson(saveMap));

                logger.info("Exception during questions set upload is ",e);
                logger.info("Error Map " + redisErrorMap.values());
            try {
                redisDAO.setexKeyPairs(redisErrorMap, 600);
                logger.info("Error value set to redis " + redisErrorMap.values());
            } catch (InternalServerErrorException ex) {
                logger.info("Error in saving exception values to Redis" + ex);
            }
        }
        finally {
            executorService.shutdownNow();
        }
        }, executorService);

        return response;
    }

    @RequestMapping(value = "/uploadQuestionSet_Poll", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object> uploadQuestionSet_Poll(@RequestParam(name = "token", required = true) String token)
            throws VException, IllegalStateException, IOException, ClassNotFoundException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);


        Map<String, Object> response = new HashMap<>();

        Type filterListType = new TypeToken<Map<String, String>>() {
        }.getType();

        Map<String, String> redisMap = new Gson().fromJson(redisDAO.get(token), filterListType);

        if(redisMap.containsKey("success") && redisMap.get("success") != null){
            response.put("processing", "false");
            response.put("success", redisMap.get("success"));
            response.put("error", null);
        }
        else if(redisMap.containsKey("error") && redisMap.get("error") != null) {
            String errorMessage = redisMap.get("error");
            response.put("processing", "false");
            response.put("success", null);
            response.put("error", errorMessage);
            throw new com.vedantu.exception.BadRequestException( ErrorCode.NOT_FOUND_ERROR,
                    errorMessage );

        }
        else{
            response.put("processing", "true");
            response.put("success", null);
            response.put("error", null);
        }

        return response;
    }


    @RequestMapping(value = "/uploadQuestionSet_Success", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public UploadQuestionSetFileRes uploadQuestionSet_Success(@RequestParam(name = "questionSetId", required = true) String questionSetId)
            throws VException, IllegalStateException, IOException, ClassNotFoundException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);

        return questionSetManager.getQuestionSetUploadResponse(questionSetId);
    }

    @RequestMapping(value = "/bulkUploadConfirmQuestionSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object>  bulkUploadConfirmQuestionSet(@RequestBody ConfirmQuestionsReq confirmQuestionsReq)
            throws VException, IllegalStateException, IOException, ClassNotFoundException, BadRequestException {

        Map<String, Object> response = new HashMap<>();

        try{
            ConfirmQuestionSetUploadRes res = questionSetManager.confirmQuestions(confirmQuestionsReq);
            response.put("success", res);
            response.put("error", null);
        }
        catch(Exception e){
            response.put("success", null);
            response.put("error", e.getMessage());
        }

        return response;
    }


    @RequestMapping(value = "/confirmQuestionSet", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, Object>  confirmQuestionSet(@RequestBody ConfirmQuestionsReq confirmQuestionsReq)
            throws VException, IllegalStateException {
        sessionUtils.checkIfAllowed(confirmQuestionsReq.getCallingUserId(), Role.ADMIN, Boolean.TRUE);

        logger.info("Request : " + confirmQuestionsReq);

        String token = questionSetManager.generateToken();

        Map<String, Object> response = new HashMap<>();
        response.put("token", token);
        Map<String, String> redisProcessingMap = new HashMap<>();
        Map<String, String> saveProcessingMap = new HashMap<>();

        saveProcessingMap.put("processing", "true");

        redisProcessingMap.put(token, gson.toJson(saveProcessingMap));
        redisDAO.setexKeyPairs(redisProcessingMap, 600);


        ExecutorService executorService = Executors.newFixedThreadPool(1);

        CompletableFuture.runAsync(() -> {
            try {
                ConfirmQuestionSetUploadRes res = questionSetManager.confirmQuestions(confirmQuestionsReq);
                if (Objects.nonNull(confirmQuestionsReq.getCmdsTest())) {

                    confirmQuestionsReq.populateDataForCMDSTest();

                    CMDSTest cmdsTest = testManager.createCMDSTest(confirmQuestionsReq.getCmdsTest());

                    Map<String, String> redisMap = new HashMap<>();
                    Map<String, String> saveMap = new HashMap<>();

                    saveMap.put("processing", "false");
                    saveMap.put("success", "true");
                    saveMap.put("questionSetName", res.getQuestionsSetName());
                    saveMap.put("error", null);
                    saveMap.put("testId",cmdsTest.getId());

                    redisMap.put(token, gson.toJson(saveMap));
                    redisDAO.setexKeyPairs(redisMap, 600);
                }
            } catch (Exception e){
                Map<String, String> redisErrorMap = new HashMap<>();
                Map<String, String> saveMap = new HashMap<>();

                saveMap.put("processing", "false");
                saveMap.put("success", null);
                saveMap.put("error", e.getMessage());
                redisErrorMap.put(token, gson.toJson(saveMap));

                logger.info("Error Map " + redisErrorMap.values());
                logger.info("Error in test creation {}",e);
                try {
                    redisDAO.setexKeyPairs(redisErrorMap, 600);
                    logger.info("Error value set to redis " + redisErrorMap.values());
                } catch (InternalServerErrorException ex) {
                    logger.error("Error in saving exception values to Redis" + ex);
                }
            } finally {
                executorService.shutdownNow();
            }}, executorService);

        return response;
    }


    @RequestMapping(value = "/getQuestionSetById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CMDSQuestionSetInfo getQuestionSetById(@ModelAttribute GetQuestionSetReq request) throws VException {
        logger.info("Request : " + request.toString());
        return questionSetManager.getQuestionSetById(request);
    }

    @RequestMapping(value = "/getQuestionSets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CMDSQuestionSet> getQuestions(@ModelAttribute GetQuestionSetsReq request) throws VException {
        logger.info("Request : " + request.toString());
        return questionSetManager.getQuestionSets(request);
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public BasicRes deleteQuestionSet(@RequestBody DeleteQuestionSetsReq request)
            throws VException, BadRequestException {
        logger.info("Request : " + request.toString());
        questionSetManager.deleteQuestionSet(request);
        return new BasicRes();
    }

    @RequestMapping(value = "/filterQuestionSetCheckerData",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSQuestionSetCheckerResponse> filterQuestionSetCheckerData(@RequestBody CheckerQuestionSetOrTestFilterRequest request) throws VException {
        sessionUtils.checkIfAllowed(request.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return questionSetManager.filterQuestionSetCheckerData(request);
    }

}
