/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.security.redis;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.response.CurriculumProgressRes;
import com.vedantu.doubts.managers.DoubtsManager;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.redis.AbstractRedisDAO;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.annotation.PreDestroy;

import com.vedantu.util.scheduling.CommonCalendarUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPubSub;


@Service
public class RedisDAO extends AbstractRedisDAO{

    
    @Autowired
    DoubtsManager doubtsManager;    
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RedisDAO.class);    
    
    private static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static final Gson gson = new Gson();

    // currently 40k batches - this can withstand 1 million batches for the value of 1000 buckets
    private static final int BATCH_CURRICULUM_PROGRESS_HASH_BUCKETS = 1000;
    private static final String BATCH_CURRICULUM_PROGRESS_HASH_PREFIX = "BATCH_CURRICULUM_PROGRESS:";
    private static final String BATCH_CURRICULUM_PROGRESS_PREFIX = "BATCH_CURRICULUM_PROGRESS_";

    public RedisDAO() {
    }

    public void setBatchCurriculumProgressHash(final Map<String, List<CurriculumProgressRes>> batchCurriculumProgress) {
        if (Objects.isNull(batchCurriculumProgress)) {
            return;
        }

        for (Map.Entry<String, List<CurriculumProgressRes>> entry : batchCurriculumProgress.entrySet()) {
            String bucketKey = getBucketKeyForObjectIdStr(BATCH_CURRICULUM_PROGRESS_HASH_PREFIX, entry.getKey(), BATCH_CURRICULUM_PROGRESS_HASH_BUCKETS);
            try {
                hset(bucketKey, entry.getKey(), gson.toJson(entry.getValue()));
            } catch (BadRequestException e) {
                logger.warn("Error while setting batch curriculum progress for batchId {}", entry.getKey());
            }
        }
    }

    public Map<String, List<CurriculumProgressRes>> getBatchCurriculumProgressHash(final List<String> contextIds) {
        Map<String, List<CurriculumProgressRes>> cachedProgress = new HashMap<>();
        if (Objects.isNull(contextIds)) {
            return cachedProgress;
        }

        for (String contextId : contextIds) {
            final String bucketKey = getBucketKeyForObjectIdStr(BATCH_CURRICULUM_PROGRESS_HASH_PREFIX, contextId, BATCH_CURRICULUM_PROGRESS_HASH_BUCKETS);
            final Type setType = new TypeToken<List<CurriculumProgressRes>>() {
            }.getType();
            List<CurriculumProgressRes> batchProgress = new ArrayList<>();

            try {
                String jsonString = hget(bucketKey, contextId);
                if (StringUtils.isNotEmpty(jsonString)) {
                    batchProgress = gson.fromJson(jsonString, setType);
                    cachedProgress.putIfAbsent(contextId, batchProgress);
                }
            } catch (BadRequestException e) {
                logger.warn("Error while getting batch curriculum progress for batchId {}", contextId);
            }
        }

        return cachedProgress;
    }

    public void deleteBatchCurriculumProgressHash(final List<String> contextIds) {
        if (CollectionUtils.isEmpty(contextIds)) {
            return;
        }

        logger.info("deblog-- removing cached progress for contextIds-- {}", contextIds);
        for (String contextId: contextIds) {
            String bucketKey = getBucketKeyForObjectIdStr(BATCH_CURRICULUM_PROGRESS_HASH_PREFIX, contextId, BATCH_CURRICULUM_PROGRESS_HASH_BUCKETS);
            try {
                hdel(bucketKey, contextId);
            } catch (BadRequestException e) {
                logger.warn("Error while deleting batch curriculum progress for batchId {}", contextId);
            }
        }
    }

    public Map<String, List<CurriculumProgressRes>> getBatchCurriculumProgress(final List<String> contextIds) throws BadRequestException {
        Map<String, List<CurriculumProgressRes>> cachedProgress = new HashMap<>();
        if (CollectionUtils.isEmpty(contextIds)) {
            return cachedProgress;
        } else if (contextIds.size() > 5) {
            throw new BadRequestException(ErrorCode.IMPERMISSIBLE_SIZE_PER_REQUEST, "impermissible size per request");
        }

        List<String> batchProgressKeys = getBatchProgressKeys(contextIds);
        Map<String, String> fetchedValues=new HashMap<>();
        try {
            fetchedValues = getValuesForKeys(batchProgressKeys);
        } catch (InternalServerErrorException e) {
            logger.warn("Error while fetching progress values for keys {} ", contextIds);
        }

        final Type listType = new TypeToken<List<CurriculumProgressRes>>() {
        }.getType();

        fetchedValues.forEach((key, val) -> {
            if (StringUtils.isEmpty(val)) {
                return;
            }
            List<CurriculumProgressRes> cachedBatchProgress = gson.fromJson(val, listType);
            // String contextId = cachedBatchProgress.get(0).getContextId();
            String contextId = key.replaceFirst(BATCH_CURRICULUM_PROGRESS_PREFIX, "");
            cachedProgress.putIfAbsent(contextId, cachedBatchProgress);
        });

        return cachedProgress;
    }

    public void setBatchCurriculumProgress(final Map<String, List<CurriculumProgressRes>> batchCurriculumProgress) {
        if (Objects.isNull(batchCurriculumProgress)) {
            return;
        }

        Map<String, String> saveMap = new HashMap<>();
        batchCurriculumProgress.forEach((key, val) -> {
            String progressKey = BATCH_CURRICULUM_PROGRESS_PREFIX + key;
            String progressVal = gson.toJson(val);
            saveMap.put(progressKey, progressVal);
        });

        try {
            setexKeyPairs(saveMap, (int) (12 * CommonCalendarUtils.MILLIS_PER_HOUR));
        } catch (InternalServerErrorException e) {
            logger.warn("Error while setting batch curriculum progress for batchId");
        }
    }

    public void deleteBatchCurriculumProgress(final List<String> contextIds) {
        if (CollectionUtils.isEmpty(contextIds)) {
            return;
        }

        logger.info("deblog-- removing cached progress for contextIds-- {}", contextIds);

        try {
            String[] keys = getBatchProgressKeys(contextIds).toArray(new String[0]);
            logger.info("deblog-- deleting keys {}", keys);
            deleteKeys(keys);
        } catch (InternalServerErrorException e) {
            logger.warn("Error while deleting batch curriculum progress for contextIds {}", contextIds);
            }
    }



     private List<String> getBatchProgressKeys(List<String> contextIds) {
        return Optional.ofNullable(contextIds).orElseGet(ArrayList::new)
                .stream()
                .filter(StringUtils::isNotEmpty)
                .map(id -> BATCH_CURRICULUM_PROGRESS_PREFIX + id)
                .collect(Collectors.toList());
    }

}
