package com.vedantu.cmds.interfaces;

import java.util.List;

import org.apache.commons.lang.mutable.MutableLong;

import com.vedantu.lms.cmds.enums.VedantuRecordState;

public interface IContainable<T> {

	public List<T> getContainers(String id, int start, int size, VedantuRecordState state, MutableLong totalHits);
}
