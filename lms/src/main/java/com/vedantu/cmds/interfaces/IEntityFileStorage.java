package com.vedantu.cmds.interfaces;

import java.io.File;
import java.util.Map;

import org.restlet.data.Range;

import com.vedantu.cmds.entities.storage.FileCategory;
import com.vedantu.cmds.entities.storage.StorageResult;
import com.vedantu.cmds.enums.ImageSize;
import com.vedantu.cmds.enums.MediaType;
import com.vedantu.cmds.pojo.FileData;
import com.vedantu.exception.VException;

public interface IEntityFileStorage {

	public StorageResult storeImage(final String uid, final File file, final FileCategory fileCategory,
			final ImageSize imageSize, final Map<String, String> tags) throws VException;

	public String computeDisplayUrlComponent(final String uid, final String fileExt, final MediaType mediaType,
			final FileCategory fileCategory, final ImageSize imageSize);

	public FileData getData(String entityType, String mediaType, String fileName) throws VException;

	public FileData getData(String entityType, String mediaType, String fileName, Range ranges) throws VException;

	public String getStorageId();

	public boolean doesFileExist(String entityType, String mediaType, String fileName) throws VException;

	public boolean remove(String entityType, String mediaType, String fileName) throws VException;

}
