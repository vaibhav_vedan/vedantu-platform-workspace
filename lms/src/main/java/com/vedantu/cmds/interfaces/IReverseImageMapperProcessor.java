package com.vedantu.cmds.interfaces;

import java.io.IOException;

import com.vedantu.exception.VException;

public interface IReverseImageMapperProcessor {

	public void addImageSrcUrl();

	public void removeImageSrc(boolean moveImages) throws IOException, VException;
}
