package com.vedantu.cmds.interfaces;

import java.io.IOException;

import com.vedantu.exception.VException;

public interface IImageUrlProcessor {
	public void convertUuidsToImageUrls(boolean permanentImageUrl, String questionSetId);

	public void convertImageUrlToUuids(boolean saveImages, boolean permanentImageUrl, String questionSetId)
			throws IOException, VException;
}