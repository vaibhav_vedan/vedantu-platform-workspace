package com.vedantu.cmds.exceptions;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;

public class EntityFileStorageException extends VException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityFileStorageException(String message) {
		super(ErrorCode.ENTITY_FILE_STORAGE_EXCEPTION, message);
	}

	public EntityFileStorageException(Throwable cause) {
		super(ErrorCode.ENTITY_FILE_STORAGE_EXCEPTION, cause.getMessage());
	}

	public EntityFileStorageException(String message, Throwable cause) {
		super(ErrorCode.ENTITY_FILE_STORAGE_EXCEPTION, message);
	}

}
