package com.vedantu.cmds.exceptions;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;

public class FileStoreException extends VException {

	private static final long serialVersionUID = 1L;

	public FileStoreException(String message, Throwable cause) {
		super(ErrorCode.FILE_STORE_EXCEPTION, cause.getMessage());
		// Logger.log4j.debug(message);
	}

	public FileStoreException(String message) {
		super(ErrorCode.FILE_STORE_EXCEPTION, message);
	}

}
