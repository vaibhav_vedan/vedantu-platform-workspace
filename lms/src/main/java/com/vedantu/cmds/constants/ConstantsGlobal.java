package com.vedantu.cmds.constants;

public class ConstantsGlobal {

    public static final String PROGRAM                         = "program";
    public static final String PROGRAM_ID                      = "programId";
    public static final String PROGRAM_IDS                     = "programIds";
    public static final String PROGRAMS                        = "programs";
    public static final String CENTER_ID                       = "centerId";

    public static final String _ID                             = "_id";
    public static final String ID                              = "id";
    public static final String RECORD_STATE                    = "recordState";
    public static final String NAME                            = "name";
    public static final String NAME_LOWERCASE                  = "nameLowerCase";
    public static final String USERNAME                        = "username";
    public static final String PASS                            = "pass";
    public static final String FIRST_NAME                      = "firstName";
    public static final String LAST_NAME                       = "lastName";
    public static final String PROFILE_PIC                     = "profilePic";
    public static final String AGENT_OF                        = "agentOf";
    public static final String EMAIL                           = "email";
    public static final String SINCE                           = "since";
    public static final String EXPERT                          = "expert";
    public static final String ACTION_TYPE                     = "actionType";

    public static final String UPLOADED_BY_NAME                = "uploadedByName";
    public static final String AUTHOR_NAME                     = "authorName";
    public static final String RATING_INFO                     = "ratingInfo";
    public static final String RATING                          = "rating";
    public static final String AVG_RATING                      = "avgRating";
    public static final String DOC_TYPE                        = "docType";
    public static final String DOCUMENTS                       = "documents";

    public static final String USER                            = "user";
    public static final String USER_ID                         = "userId";
    public static final String USER_IDS                        = "userIds";
    public static final String FOLLOWING                       = "following";
    public static final String FOLLOWER                        = "follower";
    public static final String FOLLOWERS                       = "followers";
    public static final String FOLLOW_TYPE                     = "followType";
    public static final String DOC_ID                          = "docId";

    public static final String THUMBNAIL                       = "thumbnail";
    public static final String THUMB                           = "thumb";

    public static final String PAGE_ID                         = "pageId";
    public static final String PAGE_IDS                        = "pageIds";
    public static final String PAGE_COUNT                      = "pageCount";
    public static final String DOC_IDS                         = "docIds";
    public static final String TITLE                           = "title";
    public static final String TOCS                            = "tocs";
    public static final String REVIEWS                         = "reviews";
    public static final String COMMENT                         = "comment";
    public static final String COMMENTS                        = "comments";
    public static final String PAGES                           = "pages";
    public static final String VIEWS                           = "views";
    public static final String FOLDER_IDS                      = "folderIds";
    public static final String ROOT_TYPE                       = "rootType";
    public static final String ROOT_ID                         = "rootId";
    public static final String DEPTH                           = "depth";
    public static final String SCPOE                           = "scope";
    public static final String STATUSFEED_ID                   = "statusFeedId";
    public static final String PLAYLIST_ID                     = "playlistId";
    public static final String PLAYLISTS                       = "playlists";
    public static final String TOPIC                           = "topic";
    public static final String TOPICS                          = "topics";
    public static final String TOPIC_IDS                       = "topicIds";
    public static final String INFO                            = "info";
    public static final String CONTENT                         = "content";
    public static final String SECTION_ID                      = "sectionId";
    public static final String SECTION_IDS                     = "sectionIds";
    public static final String SECTION                         = "section";
    public static final String SECTIONS                        = "sections";
    public static final String HEADING                         = "heading";
    public static final String CAPTION                         = "caption";
    public static final String LINK_INFO                       = "linkInfo";
    public static final String IMAGES                          = "images";
    public static final String IMAGE                           = "image";
    public static final String SECTION_TYPE                    = "secType";
    public static final String TOPIC_ID                        = "topicId";
    public static final String ADD_AT_START                    = "-1";
    public static final String START                           = "start";
    public static final String END                             = "end";

    public static final String STATUS_FEED_ID                  = "statusFeedId";

    public static final String FOLDER_NAME                     = "name";

    public static final String COMMENT_ID                      = "commentId";
    public static final String TAGS                            = "tags";
    public static final String UNCATEGORIZED_TAG               = "uncategorizedTag";

    public static final String TIME                            = "time";
    public static final String RECENTLY_ADDED                  = "recentlyAdded";
    public static final String TIME_CREATED                    = "timeCreated";
    public static final String LAST_UPDATED                    = "lastUpdated";
    public static final String CREATION_TIME                   = "creationTime";

    public static final String ORGANIZATION_NAME               = "organizationName";
    public static final String ORGANIZATION_LOCATION           = "organizationLocation";
    public static final String HOME_TOWN                       = "homeTown";
    public static final String BRANCH                          = "branch";
    public static final String COMMON_FOLLOWING                = "commonFollowing";
    public static final String LAST_LOGIN                      = "lastlogin";

    public static final String ORG_INFO                        = "orgInfo";
    public static final String ORG_ID                          = "orgId";

    public static final String DEPARTMENT                      = "department";
    public static final String DEPARTMENT_ID                   = "departmentId";
    public static final String DEPARTMENTS                     = "departments";
    public static final String DEPARTMENT_IDS                  = "departmentIds";
    public static final String PROGRAMME                       = "programme";
    public static final String PROGRAMME_ID                    = "programmeId";
    public static final String PROGRAMME_IDS                   = "programmeIds";
    public static final String PROGRAMMES                      = "programmes";
    public static final String PROFESSOR_IDS                   = "professorIds";
    public static final String PROFESSORS                      = "professors";
    public static final String COURSE                          = "course";
    public static final String COURSE_ID                       = "courseId";
    public static final String COURSE_IDS                      = "courseIds";
    public static final String COURSES                         = "courses";
    public static final String PROF_COURSES                    = "profCourses";
    public static final String MEMBERS                         = "members";
    public static final String MEMBER_ID                       = "memberId";
    public static final String MEMBER_IDS                      = "memberIds";
    public static final String USER_ROLE                       = "userRole";

    public static final String VIDEO_EMBED_URL                 = "videoEmbedUrl";
    public static final String VIDEO_ID                        = "videoId";
    public static final String DESCRIPTION                     = "description";
    public static final String DESC                            = "desc";
    public static final String DURATION                        = "duration";

    public static final String ACRONYM                         = "acronym";
    public static final String STREAM                          = "stream";
    public static final String STREAMS                         = "streams";

    public static final String FACET                           = "facet";
    public static final String TOTAL_HITS                      = "totalHits";
    public static final String TOTAL_RESULTS                   = "totalResult";
    public static final String QUERY_RESULTS                   = "queryResult";

    public static final String REMOVED                         = "removed";
    public static final String APPROVED                        = "approved";
    public static final String YEAR                            = "year";
    public static final String UP_VOTE                         = "upVote";
    public static final String UP_VOTES                        = "upVotes";

    public static final String TYPE                            = "type";
    public static final String MODE                            = "mode";

    public static final String BOARD                           = "board";
    public static final String BOARDS                          = "boards";
    public static final String CHILDREN                        = "children";
    public static final String CHILD_COUNT                     = "childCount";
    public static final String TARGETS                         = "targets";
    public static final String VIEW_TYPE                       = "viewType";
    public static final String GRADES                          = "grades";

    public static final String TEST_ID                         = "testId";
    public static final String RESULT_VISIBILITY               = "resultVisibility";
    public static final String RESULT_VISIBILITY_MESSAGE       = "resultVisibilityMessage";

    public static final String TESTS                           = "tests";
    public static final String CMDS_ID                         = "cmdsId";
    public static final String GLOBAL_ID                       = "globalId";

    public static final String SOURCES                         = "sources";

    public static final String ENTITY                          = "entity";
    public static final String ENTITY_DOT_ID                   = "entity.id";
    public static final String ENTITY_DOT_TYPE                 = "entity.type";
    public static final String ENTITY_ID                       = "entityId";
    public static final String ENTITY_TYPE                     = "entityType";
    public static final String ENTITIES                        = "entities";
    public static final String SRC_ENTITY                      = "srcEntity";
    public static final String USERS                           = "users";
    public static final String CODE                            = "code";
    public static final String CENTER                          = "center";
    public static final String CENTERS                         = "centers";
    public static final String SUB_TOPICS                      = "subTopics";

    public static final String TARGETS_ID                      = "targets.id";
    public static final String TARGETS_NAME                    = "targets.name";
    public static final String BOARDS_ID                       = "boards.id";
    public static final String BOARDS_NAME                     = "boards.name";
    public static final String DISCUSSIONS                     = "discussions";
    public static final String DISS_ID                         = "dissId";
    public static final String DISCUSSION                      = "discussion";
    public static final String EVENT_TYPE                      = "eventType";
    public static final String PARENT                          = "parent";
    public static final String PARENT_ID                       = "parentId";
    public static final String SUCCESS                         = "success";

    // sharing related constants comes here
    public static final String SHARE_ID                        = "shareId";
    public static final String SHARED_BY                       = "sharedBy";
    public static final String WITH                            = "with";
    public static final String WITH_COUNT                      = "withCount";
    public static final String USER_ACTION                     = "userAction";
    public static final String USER_ACTION_TYPE                = "userActionType";
    public static final String IS_NOTIFICATION_ENABLED         = "isNotificationEnabled";
    public static final String NOTIFICATION_ENABLED            = "notificationEnabled";

    // challenge related constants
    public static final String CHALLENGE_ID                    = "challengeId";
    public static final String STATUS                          = "status";
    public static final String STATE                           = "state";
    public static final String LIFE_TIME                       = "lifeTime";
    public static final String LIMIT                           = "limit";
    public static final String MULTIPLIER                      = "multiplier";
    public static final String MAX_BID                         = "maxBid";
    public static final String BID_TYPE                        = "bidType";
    public static final String PUBLISH_TYPE                    = "publishType";
    public static final String END_TIME                        = "endTime";
    public static final String TOKEN                           = "token";
    public static final String HINT_TAKEN_INDEX                = "hintTakenIndex";
    public static final String HINTS                           = "hints";
    public static final String BID_POOL                        = "bidPool";

    public static final String PROCESSED                       = "processed";
    public static final String MIN_TARGETS                     = "minTargets";
    public static final String ANSWER_TIME                     = "answerTime";
    public static final String RANKER                          = "ranker";
    public static final String RANK                            = "rank";
    public static final String POWER_RULE                      = "powerRule";

    public static final String POINTS                          = "points";
    public static final String SIZE                            = "size";
    public static final String RANK_BUCKET_ID                  = "rankBucketId";
    public static final String BUCKET_NO                       = "bucketNo";
    public static final String TOPPER_ID                       = "topperId";
    public static final String TOPPER_IDS                      = "topperIds";
    public static final String TOPPERS                         = "toppers";
    public static final String STRIKE_RATE                     = "strikeRate";
    public static final String START_TIME                      = "startTime";
    public static final String TIME_TAKEN                      = "timeTaken";
    public static final String TOTAL_POINTS                    = "totalPoints";
    public static final String BASE_POINTS                     = "basePoints";
    public static final String Q_TYPES                         = "qTypes";
    public static final String LEGAL                           = "legal";
    public static final String EVENT_CATEGORY                  = "category";
    public static final String LEADER_BOARD_TYPE               = "leaderBoardType";
    // share constants
    public static final String PERMISSION                      = "permission";
    // groups constants
    public static final String ADMIN_IDS                       = "adminIds";
    public static final String ADMINS                          = "admins";

    public static final String QUESTION_TYPE                   = "QuestionType";
    public static final String DIFFICULTY                      = "difficulty";
    public static final String SUBJECT                         = "subject";

    public static final String DST_DOT_ID                      = "dst.id";
    public static final String DST_ID                          = "dstId";
    public static final String CONTENT_SRC                     = "contentSrc";

    // QR
    public static final String ISADDED                         = "isAdded";
    public static final String SOURCEID                        = "sourceId";
    public static final String ISEDITED                        = "isEdited";
    public static final String QUESTIONIDS                     = "questionIds";
    public static final String LEVELS                          = "levels";
    public static final String SOURCE                          = "source";
    public static final String SUB_SUB_TOPICS                  = "subSubTopics";
    public static final String SUB_SUB_TOPICS_TO_BRD_ID_MAP    = "subSubTopicToBrdIdMap";
    public static final String QID                             = "qId";
    public static final String QUESTION_SET_NAME               = "questionSetName";
    public static final String ASSIGNED_TO                     = "assignedTo";
    public static final String QUESTION_SET_ID                 = "questionSetId";
    public static final String PUBLISHED                       = "published";
    public static final String METADATA                        = "metadata";

    public static final String TARGET_IDS                      = "targetIds";
    public static final String TEST_GROUP_IDS                  = "testGroupIds";
    public static final String TOTAL_ATTEMPTS                  = "totalAttempts";
    public static final String CHILDREN_TEST_IDS               = "childrenTestIds";
    public static final String TEST_GROUP                      = "testGroup";

    // ticker constants
    public static final String TIME_BUCKET                     = "timeBucket";
    public static final String TICKER_TYPE                     = "tickerType";
    public static final String COUNT                           = "count";
    public static final String RANK_IDENTIFIER                 = "rankIdentifier";
    public static final String QUESTIONS                       = "questions";
    public static final String EDITION                         = "edition";
    public static final String NUM_CHILDREN_SOURCE_IDS         = "numChildrenSourceIds";
    public static final String NUM_QUESTION_IDS                = "numQuestionIds";
    public static final String AUTHORS                         = "authors";
    public static final String NEW_TEXT                        = "newText";
    public static final String ORIGINAL_TEXT                   = "originalText";
    public static final String END_INDEX_OF_LATEX_OR_IMAGE     = "endIndexOfLatexOrImage";
    public static final String START_INDEX_OF_LATEX_OR_IMAGE   = "startIndexOfLatexOrImage";
    public static final String UUID_IMAGES                     = "uuidImages";
    public static final String QUESTION_BODY                   = "questionBody";
    public static final String ASSIGNED_GROUP_ID               = "assignedGroupId";
    public static final String DEADLINE                        = "deadline";
    public static final String NOTE                            = "note";
    public static final String ISUPDATED                       = "isUpdated";
    public static final String ASSIGNED_TO_ID                  = "assignedToId";
    public static final String ASSIGNED_BY_ID                  = "assignedById";
    public static final String ASSIGNED_TO_USER_ID             = "assignedToUserId";
    public static final String ASSIGNED_BY_USER_ID             = "assignedByUserId";
    public static final String ASSIGNED_BY                     = "assignedBy";
    public static final String TOTAL_QUESTIONS                 = "totalQuestions";
    public static final String ASSIGNED_BY_USER                = "assignedByUser";
    public static final String ASSIGNED_TO_USER                = "assignedToUser";
    public static final String CHILDREN_SOURCE_IDS             = "childrenSourceIds";
    public static final String SOURCEIDS                       = "sourceIds";
    public static final String COURSEDATAS                     = "courseDatas";
    public static final String ASSIGNEEIDS                     = "assigneeIds";
    public static final String ADMIN                           = "admin";
    public static final String PROFESSOR                       = "professor";
    public static final String ROLEID                          = "roleId";
    public static final String ROLE_NAME                       = "roleName";
    public static final String USER_LIST                       = "userList";
    public static final String ORIG_REF_NO                     = "origRefNo";
    public static final String ASSIGNEDGROUP                   = "assignedGroup";
    public static final String ASSIGNEDGROUPS                  = "assignedGroups";
    public static final String QUESTIONID                      = "questionId";
    public static final String ISFLAGGED                       = "isFlagged";
    public static final String QRTESTID                        = "qrTestId";
    public static final String QRTEST                          = "qrtest";
    public static final String TOTAL_MARKS                     = "totalMarks";
    public static final String MARKS                           = "marks";
    public static final String QUS_COUNT                       = "qusCount";
    public static final String AVG_MARKS                       = "avgMarks";
    public static final String GLOBAL_TEST_ID                  = "globalTestId";
    public static final String LEVEL                           = "level";
    public static final String ASSIGNED_STATUS                 = "assignedStatus";
    public static final String ISPUBLISHED                     = "isPublished";
    public static final String SOLUTION_INFO                   = "solutionInfo";
    public static final String TEST                            = "test";
    public static final String GLOBAL_MODULE_ID                = "globalModuleId";
    public static final String COMPLETION_RULE                 = "completionRule";
    public static final String PREREQUSITE_MODULE_ID           = "prerequsiteModuleId";
    public static final String COLA                            = "cola";
    public static final String COLB                            = "colb";
    public static final String OPTIONORDERA                    = "optionOrdera";
    public static final String OPTIONORDERB                    = "optionOrdera";
    public static final String ORIGINALCOLA                    = "originalCola";
    public static final String PRODUCTS                        = "products";
    public static final String PRODUCT_TYPE                    = "productType";
    public static final String DESTINATIONIDS                  = "destinationIds";
    public static final String DATE_CREATED                    = "dateCreated";
    public static final String NUM_CONTENT_ADDED               = "numContentAdded";
    public static final String QUESTION                        = "question";
    public static final String CONTENT_INFO                    = "contentInfo";
    public static final String TOTAL_HITS_SOURCE               = "totalHitsSource";
    public static final String TOTAL_HITS_QUESTION             = "totalHitsQuestion";
    public static final String TEST_SERIES                     = "testSeries";
    public static final String NUM_CHILDREN_TESTS              = "numChildrenTests";
    public static final String TEST_SERIES_ID                  = "testSeriesId";
    public static final String TEST_SERIES_IDS                 = "testSeriesIds";
    public static final String TARGET_EXAM                     = "targetExam";
    public static final String ACTIVE_TESTS                    = "activeTests";
    public static final String INACTIVE_TESTS                  = "inactiveTests";
    public static final String ISTESTSERIES                    = "isTestSeries";
    public static final String TOTAL_HITS_SOURCES              = "totalHitsSources";
    public static final String TOTAL_HITS_QUESTIONS            = "totalHitsQuestions";
    public static final String CLAER_NAME                      = "clearName";
    public static final String QORDERTYPE                      = "qOrderType";
    public static final String VERIFY_ONLY                     = "verifyOnly";
    public static final String VERIFICATION_KEY                = "verificationKey";
    public static final String KEY                             = "key";
    public static final String SCHEDULE                        = "schedule";
    public static final String SCHEDULES                       = "schedules";
    public static final String SCHEDULEID                      = "scheduleId";
    public static final String ISREMOVED                       = "isRemoved";
    public static final String ISLABTEST                       = "isLabTest";
    public static final String RESULT                          = "result";
    public static final String MOBILE_NO                       = "mobileNo";
    public static final String CONTACTNO                       = "contactNo";
    public static final String TARGET                          = "target";
    public static final String CONTEXT                         = "context";
    public static final String TARGET_DOT_ID                   = "target.id";
    public static final String TARGET_DOT_TYPE                 = "target.type";
    public static final String TARGET_EXAMS                    = "targetExams";
    public static final String ISATTEMPTED                     = "isAttempted";
    public static final String ISEXPIRED                       = "isExpired";
    public static final String ISSHARABLE                      = "isSharable";
    public static final String ISKEYREQUIRED                   = "isKeyRequired";
    public static final String ERROR_CODE                      = "errorCode";
    public static final String INVALID_COURSE_ID               = "INVALID_COURSE_ID";
    public static final String FRONT_DESK_USER                 = "FRONT_DESK_USER";
    public static final String CONTENT_EXECUTIVE               = "CONTENT_EXECUTIVE";
    public static final String SUBSCRIPTION_ID                 = "subscriptionId";
    public static final String VERIFIED                        = "verified";
    public static final String DOWNTIME                        = "downTime";
    public static final String QUESTIONS_INFO                  = "questionsInfo";
    public static final String ISTESTCOMPLETED                 = "isTestCompleted";
    public static final String ANSWERS_GIVEN                   = "answersGiven";
    public static final String EXTRATIME                       = "extraTime";
    public static final String ISCOMPLETED                     = "isCompleted";
    public static final String COMPLETED                       = "completed";
    public static final String OPTIONS                         = "options";
    public static final String ACTIVITY                        = "activity";
    public static final String TARGET_ID                       = "targetId";
    public static final String SRC_TEST_CODE                   = "srcTestCode";
    public static final String CORDER_TYPE                     = "cOrderType";
    public static final String QORDER_TYPE                     = "cOrderType";
    public static final String QIDS                            = "qIds";
    public static final String ECSTATUSID                      = "ecStatusId";
    public static final String ISCOMPLETE                      = "isComplete";
    public static final String ISSUCCESS                       = "isSuccess";
    public static final String JOB_ID                          = "jobId";
    public static final String CHILD_STATUS                    = "childStatus";
    public static final String IS_NEW_USER                     = "isNewUser";
    public static final String SOLUTION                        = "solution";
    public static final String SOLUTIONS                       = "solutions";
    public static final String HAS_ANS                         = "hasAns";
    public static final String HAS_SEEN_SOLUTION               = "hasSeenSolution";
    public static final String PROCESS_TIME                    = "processTime";
    public static final String IS_LAST_SCHEDULE                = "isLastSchedule";
    public static final String EVENT_ID                        = "eventId";
    public static final String TEST_NAME                       = "testName";
    public static final String TOTAL_STUDENTS                  = "totalStudents";
    public static final String TEST_SERIES_NAME                = "testSeriesName";
    public static final String TEST_URL                        = "testUrl";
    public static final String BOARD_IDS                       = "boardIds";
    public static final String TOTAL_HITS_DOCUMENTS            = "totalHitsDocuments";
    public static final Object DOCUMENT                        = "document";
    public static final String HOCR                            = "hocr";
    public static final String TEXT                            = "text";

    // Content Delivery Plan
    public static final String CONTENT_DELIVERY_PLAN_ID        = "cdpId";
    public static final String CONTENT_DELIVERY_PLAN_ANCESTORS = "ancestors";

    public static final String QRCHALLENGE_ID                  = "qrChallengeId";
    public static final String QRQUESTIONID                    = "qrQid";
    public static final String INITIAL_BID_POOL                = "initialBidPool";
    public static final String GLOBAL_ENTITIES                 = "globalEntities";
    public static final String QRCHALLENGE                     = "qrchallenge";
    public static final String CHALLENGE                       = "challenge";
    public static final String CHALLENGES                      = "challenges";
    public static final String NUM_DOC_IDS                     = "numDocIds";
    public static final String PRODUCT_IDS                     = "productIds";
    public static final String VIDEO_UUID                      = "videoUUID";
    public static final String VIDEO_EXTENSION                 = "videoExt";
    public static final String VIDEO_URL                       = "videoUrl";
    public static final String TOTAL_HITS_VIDEOS               = "totalHitsVideos";
    public static final String VIDEOS                          = "videos";
    public static final String LIBRARY                         = "library";
    public static final String PACKAGE_ID                      = "packageId";
    public static final String FOLDER_ID                       = "folderId";
    public static final String GLOBAL_DOC_ID                   = "globalDocId";
    public static final String SRC_ENTITIES                    = "srcEntities";
    public static final String PACKAGES                        = "packages";
    public static final String CONTENTS                        = "contents";
    public static final String FOLDERS                         = "folders";
    public static final String QRQID                           = "qrQid";
    public static final String QR_DOC_ID                       = "qrDocId";
    public static final String SEPARATOR                       = "#";

    // Content Delivery Plan
    public static final String CONTENT_DELIVERY_CONTENTS       = "contents";
    public static final String CONTENT_DELIVERY_CHILD_CDP_IDS  = "cdpIds";
    public static final String CONTENT_DELIVERY_CHILD_CDPS     = "cdps";
    public static final String CONTENT_DELIVERY_CREATOR        = "creator";

    public static final String QR_CONTENT_DELIVERY_PLAN_ID     = "qrCdpId";
    public static final String CDPS                            = "cdps";
    public static final String CONTENT_DELIVERY_SRC_ENTITIES   = "srcEntities";
    public static final String CONTENT_DELIVERY_LIBRARY        = "library";
    public static final String CONTENT_DELIVERY_GLOBAL_CDP_ID  = "globalCDPId";
    public static final String CONTENT_DELIVERY_IS_PUBLISHED   = "isPublished";
    public static final String QR_PACKAGE_ID                   = "qrPackageId";
    public static final String DOWNLOAD_LINK                   = "downloadLink";
    public static final String TOCELEMENT                      = "tocelement";
    public static final String ANSWER                          = "answer";
    public static final String ANSWERS                         = "answers";
    public static final String MATRIX_ANSWER                   = "matrixAnswer";
    public static final String VERSION_NUMBER                  = "versionNumber";
    public static final String EXPORTED_TIME                   = "exportedTime";
    public static final String DOWNLOAD_LINKS                  = "downloadLinks";
    public static final String PACKAGE_VERSION                 = "packageVersion";
    public static final String ALPHABETICAL                    = "alphabetical";
    public static final String EXPORTED                        = "exported";
    public static final String EXPORT_INFO                     = "exportInfo";
    public static final String TOTAL_FRAGMENTS                 = "totalFragments";
    public static final String FRAGMENT_NUMBER                 = "fragmentNumber";
    public static final String IS_DIRTY                        = "isDirty";
    public static final String EXPORTED_AFTER_MODIFICATION     = "exportedAfterModification";
    public static final String ENC_LEVEL                       = "encLevel";
    public static final String PASSPHRASE                      = "passphrase";
    public static final String ENC_FILE                        = "encFile";
    public static final String MEDIA_TYPE                      = "mediaType";

    public static final String LINK                            = "link";
    public static final String LINK_TYPE                       = "linkType";
    public static final String SWF                             = "swf";
    public static final String PUBLISHED_ON                    = "publishedOn";
    public static final String PUBLISHED_BY                    = "publishedBy";

    public static final String MISCELLANEOUS                   = "misc";
    public static final String IS_TEST_FINALIZED               = "isTestFinalized";

    public static final String NEWS_FEED_ID                    = "newsFeedId";
    public static final String NEWS_ACITIVITY_REASON_WHY       = "why";
    public static final String NEWS_ACITIVITY_ID               = "newsActivityId";
    public static final String NEWS_ACITIVITY_ETYPE            = "eType";
    public static final String VOTE_TYPE                       = "voteType";
    public static final String ORGANIZATION_PIC                = "organizationPic";

    // questions constants
    public static final String ATTEMPTS                        = "attempts";
    public static final String ATTEMPT_ID                      = "attemptId";
    public static final String SCORE                           = "score";

    public static final String FLAGS                           = "flags";
    public static final String PRECISION                       = "precision";
    public static final String CHANNEL_ID                      = "channelId";
    public static final String GLOBAL_QID                      = "globalQid";

    public static final String ACAD_DIM_ID                     = "acadDimId";
    public static final String ACAD_DIM_DOT_ID                 = "acadDim.id";
    public static final String ACAD_DIM_DOT_TYPE               = "acadDim.type";
    public static final String ACTION_ID                       = "actionId";
    public static final String CMDS_VIDEO_ID                   = "cmdsVideoId";
    public static final String URL                             = "url";
    public static final String ORGINAL_FILE_NAME               = "originalFileName";
    public static final String UUID                            = "uuid";
    public static final String EXTENSION                       = "extension";
    public static final String FILE_ID                         = "fileId";

    public static final String PARENT_DOT_ID                   = "parent.id";
    public static final String PARENT_DOT_TYPE                 = "parent.type";
    public static final String CLAN_ID             			   = "clanId";
    public static final String PICTURE_CHALLENGE_ID 		   = "pictureChallengeId";
	

}
