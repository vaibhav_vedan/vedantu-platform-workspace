package com.vedantu.cmds.managers;

import com.vedantu.cmds.esconfig.ESConfig;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

@Service
public class ESManager {
    @Autowired
    private LogFactory logFactory;

    @Autowired
    private ESConfig eSConfig;

    @Autowired
    private DozerBeanMapper mapper;

    private RestHighLevelClient client;
    private Logger logger;

    @PostConstruct
    public void init() {
        client = eSConfig.getClient();
        logger = logFactory.getLogger(ESManager.class);
    }

    public IndexResponse doIndexRequest(IndexRequest indexRequest) throws IOException {
        return doIndexRequest(indexRequest,RequestOptions.DEFAULT);

    }

    public IndexResponse doIndexRequest(IndexRequest indexRequest, RequestOptions requestOptions) throws IOException {
        IndexResponse indexResponse = client.index(indexRequest, requestOptions);
        printESIndexResponse(indexResponse);
        return indexResponse;
    }

    public SearchResponse doSearchRequest(SearchRequest searchRequest) throws IOException {
        return doSearchRequest(searchRequest,RequestOptions.DEFAULT);
    }

    public SearchResponse doSearchRequest(SearchRequest searchRequest, RequestOptions requestOptions) throws IOException {
        SearchResponse searchResponse = client.search(searchRequest, requestOptions);
        return searchResponse;
    }

    public DeleteResponse doDeleteRequest(DeleteRequest deleteRequest) {
        return doDeleteRequest(deleteRequest,RequestOptions.DEFAULT);
    }

    public DeleteResponse doDeleteRequest(DeleteRequest deleteRequest, RequestOptions requestOptions) {
        DeleteResponse deleteResponse = null;
        try {
            deleteResponse = client.delete(deleteRequest, requestOptions);
        } catch (IOException e) {
            logger.error("Error in deleting for deleteRequest {} with error {}",deleteRequest,e);
        }
        printESDeleteResponse(deleteResponse);
        return deleteResponse;
    }


    private void printESUpdateResponse(UpdateResponse response) {
        if (response != null) {
            logger.info("id: " + response.getId());
            logger.info("index: " + response.getIndex());
            logger.info("type: " + response.getType());
            logger.info("version: " + response.getVersion());
        }
    }

    private void printESIndexResponse(IndexResponse response) {
        if (response != null) {
            logger.info("id: " + response.getId());
            logger.info("index: " + response.getIndex());
            logger.info("type: " + response.getType());
            logger.info("version: " + response.getVersion());
        }
    }

    private void printESDeleteResponse(DeleteResponse response) {
        if (response != null) {
            logger.info("id: " + response.getId());
            logger.info("index: " + response.getIndex());
            logger.info("type: " + response.getType());
            logger.info("version: " + response.getVersion());
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing es connection ", e);
        }
    }
}
