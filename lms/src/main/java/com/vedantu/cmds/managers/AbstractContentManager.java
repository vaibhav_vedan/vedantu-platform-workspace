package com.vedantu.cmds.managers;

import java.io.File;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.vedantu.cmds.entities.storage.FileCategory;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.cmds.enums.ImageSize;
import com.vedantu.cmds.factory.EntityStorageFactory;
import com.vedantu.cmds.fs.factory.FileSystemFactory;
import com.vedantu.cmds.fs.handler.LocalFileSystemHandler;
import com.vedantu.cmds.interfaces.IEntityFileStorage;
import com.vedantu.cmds.utils.FileUtils;
import com.vedantu.cmds.utils.ImageDisplayURLUtil;
import com.vedantu.cmds.utils.ImageHTMLUtils;
import com.vedantu.exception.VException;

public abstract class AbstractContentManager {

	protected static final int ELASTIC_SEARCH_REFRESH_TIME = 500;
	protected static final int ES_ENSURE_QUERY_STATE_MAX_TRY_COUNT = 5;

	public static String removeTempImageSrcAndSaveToFS(CMDSEntityType entityType, String content, boolean moveImage)
			throws VException {

		return removeTempImageSrcAndSaveToFS(entityType, content, moveImage, null, null);
	}

	public static String removeTempImageSrcAndSaveToFS(CMDSEntityType entityType, String content, boolean moveImage,
			Set<String> uuids, Map<String, String> tags) throws VException {

		IEntityFileStorage entityStorage = EntityStorageFactory.INSTANCE.get(entityType);
		LocalFileSystemHandler tempFs = FileSystemFactory.INSTANCE.getTempFS();
		if (uuids == null) {
			uuids = new HashSet<String>();
		}
		content = ImageHTMLUtils.removeImageSrcUrl(content, uuids);
		if (moveImage) {

			for (String uuid : uuids) {
				String filePath = tempFs.getFilePath("images", uuid + ImageDisplayURLUtil.JPG_EXTENTION);
				File file = new File(filePath);
				entityStorage.storeImage(uuid, file, FileCategory.CONVERTED, ImageSize.ORIGINAL, null);
				FileUtils.deleteFile(filePath, file);
			}
		}

		return content;
	}
}
