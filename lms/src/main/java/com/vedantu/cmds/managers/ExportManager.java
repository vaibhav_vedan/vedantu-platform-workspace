package com.vedantu.cmds.managers;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.mail.internet.AddressException;

import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.managers.challenges.CommunicationManager;
import com.vedantu.cmds.pojo.ExportTestAttemptPojo;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.bson.types.ObjectId;

@Service
public class ExportManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ExportManager.class);

    @Autowired
    private ContentInfoDAO contentInfoDAO;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FosUtils fosUtils;

    private final Gson gson = new Gson();
    private final String subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    public void getTestAttemptsExportAsync(String testId, HttpSessionData httpSessionData) throws BadRequestException {
        if (StringUtils.isEmpty(testId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "testId is null");
        }
        Map<String, Object> payload = new HashMap<>();
        payload.put("testId", testId);
        payload.put("sessionData", httpSessionData);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_TEST_ATTEMPTS, payload);
        asyncTaskFactory.executeTask(params);
    }

    @Deprecated
    public void getTestAttemptsExport(String testId, HttpSessionData httpSessionData) throws VException, NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, AddressException, IOException {

        int size = 100;
        String lastFetchedId=null;
        boolean count = true;
        String title = "";
        Integer quetionCount = 0;
        Long duration = 0l;

        Float maxMarks = 0f;
        List<ContentInfo> contentInfos = new ArrayList<>();
        while (count) {
            List<ContentInfo> tempContentInfos = contentInfoDAO.getContentInfos(testId, lastFetchedId, size);
            if (ArrayUtils.isEmpty(tempContentInfos)) {
                count = false;
                break;
            }else{
                lastFetchedId = tempContentInfos.get(tempContentInfos.size() - 1).getId();
            }
            contentInfos.addAll(tempContentInfos);
        }
        if (ArrayUtils.isEmpty(contentInfos)) {
            logger.info("no tests shared for " + testId);
        }

        logger.info("exporting contentInfo size : " + contentInfos.size());
        Set<String> batchIds = new HashSet<>();
        Set<String> coursePlanIds = new HashSet<>();
        Set<String> bundlIds = new HashSet<>();
        Set<String> userIds = new HashSet<>();
        List<ExportTestAttemptPojo> exportPojos = new ArrayList<>();
        Map<String,List<ExportTestAttemptPojo>>  testAttempts = new HashMap<String, List<ExportTestAttemptPojo>>();
        for (ContentInfo contentInfo : contentInfos) {
            if (StringUtils.isNotEmpty(contentInfo.getContextId())) {
                if (EntityType.OTF.equals(contentInfo.getContextType())) {
                    batchIds.add(contentInfo.getContextId());
                } else if (EntityType.BUNDLE.equals(contentInfo.getContextType())) {
                    bundlIds.add(contentInfo.getContextId());
                } else if (EntityType.COURSE_PLAN.equals(contentInfo.getContextType())) {
                    coursePlanIds.add(contentInfo.getContextId());
                }
            }
            if (StringUtils.isNotEmpty(contentInfo.getStudentId())
                    && !ObjectId.isValid(contentInfo.getStudentId())) {//excluding non logged in dummy studentIds
                userIds.add(contentInfo.getStudentId());
            }
            if (StringUtils.isNotEmpty(contentInfo.getTeacherId())) {
                userIds.add(contentInfo.getTeacherId());
            }
            title = contentInfo.getContentTitle();
            if (contentInfo.getMetadata() != null) {

                TestContentInfoMetadata infoMetaData = contentInfo.getMetadata();

                quetionCount = infoMetaData.getNoOfQuestions();
                if (infoMetaData.getDuration() != null) {
                    duration = (infoMetaData.getDuration() / 60000);
                }

                maxMarks = infoMetaData.getTotalMarks();
                if (ArrayUtils.isNotEmpty(infoMetaData.getTestAttempts())) {
                    int attemptCount = 1;
                    for (CMDSTestAttempt attempt : infoMetaData.getTestAttempts()) {
                        ExportTestAttemptPojo pojo = new ExportTestAttemptPojo();
                        pojo.setStudentId(contentInfo.getStudentId());
                        pojo.setAttemptNumber(attemptCount++);
                        if (contentInfo.getContentState() != null) {
                            pojo.setTestStatus(contentInfo.getContentState().name());
                        }

                        pojo.setTeacherId(contentInfo.getTeacherId());
                        pojo.setCorrect(attempt.getCorrect());
                        pojo.setInCorrect(attempt.getIncorrect());
                        pojo.setAttempted(attempt.getAttempted());
                        pojo.setUnattempted(attempt.getUnattempted());
                        pojo.setMarksScored(attempt.getMarksAcheived());
                        pojo.setAttemptedOn(attempt.getStartedAt());
                        pojo.setAttemptedOnString(getAttemptedOnString(attempt.getStartedAt()));
                        pojo.setContextId(contentInfo.getContextId());
                        if (attempt.getTestEndType() != null) {
                            pojo.setTestEndType(attempt.getTestEndType().name());
                        }
                        if (contentInfo.getContextType() != null) {
                            pojo.setEngagementType(contentInfo.getContextType().name());
                        } else if (contentInfo.getEngagementType() != null) {
                            pojo.setEngagementType(contentInfo.getEngagementType().name());
                        }
                        if (attempt.getDuration() != null) {
                            pojo.setDuration((attempt.getDuration() / 60000));
                        }

                        exportPojos.add(pojo);
                        for(CategoryAnalytics categoryAnalytics:attempt.getCategoryAnalyticsList()) {
                            ExportTestAttemptPojo categoryPojo = new ExportTestAttemptPojo();
                            categoryPojo.setAttempted(categoryAnalytics.getAttempted());
                            categoryPojo.setCorrect(categoryAnalytics.getCorrect());
                            categoryPojo.setInCorrect(categoryAnalytics.getIncorrect());
                            categoryPojo.setUnattempted(categoryAnalytics.getUnattempted());
                            categoryPojo.setMarksScored(categoryAnalytics.getMarks());
                            categoryPojo.setStudentId(contentInfo.getStudentId());
                            categoryPojo.setTimeTaken(categoryAnalytics.getDuration());
                            categoryPojo.setMaxMarks(categoryAnalytics.getMaxMarks());
                            if(testAttempts.containsKey(categoryAnalytics.getName())){
                                testAttempts.get(categoryAnalytics.getName()).add(categoryPojo);
                            }
                            else {
                               List<ExportTestAttemptPojo> testAttemptPojos= new ArrayList<>();
                                testAttemptPojos.add(categoryPojo);
                                testAttempts.put(categoryAnalytics.getName(),testAttemptPojos);
                            }
                        }
                    }
                } else {
                    ExportTestAttemptPojo pojo = new ExportTestAttemptPojo();
                    pojo.setStudentId(contentInfo.getStudentId());
                    pojo.setAttemptNumber(0);
                    pojo.setTeacherId(contentInfo.getTeacherId());
                    pojo.setContextId(contentInfo.getContextId());
                    if (contentInfo.getContentState() != null) {
                        pojo.setTestStatus(contentInfo.getContentState().name());
                    }
                    if (contentInfo.getContextType() != null) {
                        pojo.setEngagementType(contentInfo.getContextType().name());
                    } else if (contentInfo.getEngagementType() != null) {
                        pojo.setEngagementType(contentInfo.getEngagementType().name());
                    }
                    exportPojos.add(pojo);
                }
            }

        }
        if (ArrayUtils.isEmpty(exportPojos)) {
            logger.info("no tests shared for " + testId);
            return;
        }
        Map<String, UserBasicInfo> usersmap = fosUtils.getUserBasicInfosMap(userIds, true);
        Map<String, BatchBasicInfo> batchMap = getBatches(batchIds);
        Map<String, BundleInfo> bundleMap = getBundles(bundlIds);

        Collections.sort(exportPojos, new Comparator<ExportTestAttemptPojo>() {
            @Override
            public int compare(ExportTestAttemptPojo o1, ExportTestAttemptPojo o2) {
                if (o1.getMarksScored() == null && o2.getMarksScored() != null) {
                    return 1;
                } else if (o1.getMarksScored() != null && o2.getMarksScored() == null) {
                    return -1;
                } else if (o1.getMarksScored() == null && o2.getMarksScored() == null) {
                    return 0;
                }
                return o2.getMarksScored().compareTo(o1.getMarksScored());
            }
        });
        int rank = 1;
        Float marks = null;
        int totalShared = exportPojos.size();
        for (ExportTestAttemptPojo pojo : exportPojos) {
            if (marks == null && pojo.getMarksScored() != null) {
                marks = pojo.getMarksScored();
            }
            if (pojo.getMarksScored() != null && "EVALUATED".equals(pojo.getTestStatus())) {
                if (pojo.getMarksScored() < marks) {
                    rank++;
                }
                pojo.setRank(rank);
            }
            if (pojo.getRank() != null) {
                float perc = ((totalShared - pojo.getRank() + 1) * 100f) / totalShared;
                pojo.setPercentile(perc);
            }
            if (StringUtils.isNotEmpty(pojo.getContextId())) {
                if ("OTF".equals(pojo.getEngagementType()) && batchMap.containsKey(pojo.getContextId())) {
                    BatchBasicInfo basicInfo = batchMap.get(pojo.getContextId());
                    if (basicInfo != null && basicInfo.getCourseInfo() != null) {
                        pojo.setCourseId(basicInfo.getCourseInfo().getId());
                        pojo.setCourseName(basicInfo.getCourseInfo().getTitle());
                        pojo.setBatchId(basicInfo.getBatchId());
                    }
                } else if ("BUNDLE".equals(pojo.getEngagementType()) && bundleMap.containsKey(pojo.getContextId())) {
                    BundleInfo basicInfo = bundleMap.get(pojo.getContextId());
                    if (basicInfo != null) {

                        pojo.setCourseName(basicInfo.getTitle());
                    }
                }
//	    			else if (EntityType.COURSE_PLAN.equals(contentInfo.getContextType())){
//	    				coursePlanIds.add(contentInfo.getContextId());
//	    			}
            }
            if (StringUtils.isNotEmpty(pojo.getStudentId()) && usersmap.containsKey(pojo.getStudentId())) {
                UserBasicInfo info = usersmap.get(pojo.getStudentId());
                if (info != null) {
                    pojo.setStudentName(info.getFullName());
                    pojo.setStudentEmail(info.getEmail());
                    pojo.setGrade(info.getGrade());
                    if(StringUtils.isNotEmpty(info.getContactNumber())) {
                        pojo.setContactNumber(info.getContactNumber());
                    }
                }

            }
            if (StringUtils.isNotEmpty(pojo.getTeacherId()) && usersmap.containsKey(pojo.getTeacherId())) {
                UserBasicInfo info = usersmap.get(pojo.getTeacherId());
                if (info != null) {
                    pojo.setTeacherName(info.getFullName());
                }
            }
        }
        Map<String,String> exportUserData = new HashMap<>();
        String[] fieldsArray = {"studentName", "studentId", "studentEmail", "contactNumber", "courseName", "courseId", "batchId", "contextId",
            "engagementType", "testStatus", "rank", "percentile", "marksScored", "attempted", "unattempted",
            "correct", "inCorrect", "attemptedOnString", "duration",
            "attemptNumber", "attemptedOn", "testEndType", "teacherId",
            "teacherName","grade"
        };
        List<String> fields = Arrays.asList(fieldsArray);

        String data = "";
        data += "testId" + "," + testId;
        data += "\n";
        data += "testTitle" + "," + title;
        data += "\n";
        data += "testDuration" + "," + duration;
        data += "\n";
        data += "questionCount" + "," + quetionCount;
        data += "\n";
        data += "maxMarks" + "," + maxMarks;
        data += "\n";
        data += "\n";
        for (String field : fields) {
            data += field + ",";
        }
        data += "\n";
        for (ExportTestAttemptPojo pojo : exportPojos) {
            if (CollectionUtils.isNotEmpty(fields)) {
                for (String fieldName : fields) {
                    Field field = ExportTestAttemptPojo.class.getField(fieldName);
                    Object fieldObj = field.get(pojo);
                    data += (fieldObj != null ? fieldObj.toString() : " ") + ",";

                }
            }
            data += "\n";
        }
        exportUserData.put("TestAttemptData",data);
        String[] categoryFieldsArray = {"studentName", "studentId", "studentEmail", "contactNumber", "marksScored", "maxMarks", "timeTaken", "correct", "inCorrect", "unAttempted", "totalQuestions"};
        String categoryFieldData= "";
        for (String field : categoryFieldsArray) {
            categoryFieldData += field + ",";
        }


for(String category: testAttempts.keySet()){
    String categoryData= "";
    List <ExportTestAttemptPojo> categoryTests = testAttempts.get(category);
    for (ExportTestAttemptPojo categoryTest:categoryTests){
        UserBasicInfo info = usersmap.get(categoryTest.getStudentId());
        if (info != null) {
            categoryData += info.getFullName() + "," + categoryTest.getStudentId() + "," + info.getEmail() + "," + info.getContactNumber() + ",";
        }
        else{
            categoryData +=  "," + categoryTest.getStudentId() + "," + "," + "," ;
        }

        int totalQuestion =categoryTest.getUnattempted() + categoryTest.getAttempted();
                categoryData += categoryTest.getMarksScored()  + "," + categoryTest.getMaxMarks()  + "," + categoryTest.getTimeTaken() + "," + categoryTest.getCorrect()  + "," +  categoryTest.getInCorrect()   + "," + categoryTest.getUnattempted()  + "," + totalQuestion;
        categoryData += "\n";

    }
    categoryData = categoryFieldData + "\n" + categoryData;
    exportUserData.put(category,categoryData);
}
        communicationManager.sendUserEmailWithMultipleAttachments(httpSessionData,exportUserData, "TestAttemptData");
        //
        //communicationManager.sendUserDataEmail(httpSessionData, data, "TestAttemptData", "TestAttemptData");

    }

    public String getAttemptedOnString(Long startTime) {
        if (startTime == null) {
            return "";
        }
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
        String startTimeString = sdf.format(new Date(startTime));
        return startTimeString;
    }

    public Map<String, BatchBasicInfo> getBatches(Set<String> batchIds) throws VException {
        String req = subscriptionEndpoint + "/batch/getBatchBasicInfoForBatchIds?";
        Map<String, BatchBasicInfo> map = new HashMap<>();
        if (ArrayUtils.isEmpty(batchIds)) {
            return map;
        }
        for (String batchId : batchIds) {
            req += "&batchIds=" + batchId;
        }
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(req, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type otfMapType = new TypeToken<Map<String, BatchBasicInfo>>() {
            }.getType();

            map = gson.fromJson(jsonString, otfMapType);
        } catch (Exception e) {
            map = new HashMap<>();
        }
        return map;
    }

    public Map<String, BundleInfo> getBundles(Set<String> bundleIds) throws VException {
        Map<String, BundleInfo> map = new HashMap<>();
        String req = subscriptionEndpoint + "bundle/get/getPublicBundleInfosForBundleIds?";
        if (ArrayUtils.isEmpty(bundleIds)) {
            return map;
        }
        for (String bundleId : bundleIds) {
            req += "&bundleIds=" + bundleId;
        }
        try {
            ClientResponse resp = WebUtils.INSTANCE.doCall(req, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type otfListType = new TypeToken<List<BundleInfo>>() {
            }.getType();

            List<BundleInfo> list = gson.fromJson(jsonString, otfListType);
            if (ArrayUtils.isNotEmpty(list)) {
                for (BundleInfo info : list) {
                    map.put(info.getId(), info);
                }
            }
        } catch (Exception e) {
            return new HashMap<>();
        }

        return map;
    }
}
