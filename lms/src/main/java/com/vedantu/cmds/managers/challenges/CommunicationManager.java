package com.vedantu.cmds.managers.challenges;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.managers.CMDSTestManager;
import com.vedantu.cmds.managers.AwsSQSManager;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import com.vedantu.lms.cmds.pojo.ChallengeUserPoints;
import com.vedantu.lms.cmds.response.ClanRes;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionData;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.http.HttpMethod;

@Service
public class CommunicationManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CommunicationManager.class);

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    private CMDSTestManager cmdsTestManager;

    private final Gson gson = new Gson();

    public static final String TIME_ZONE_IN = "Asia/Kolkata";

    private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");

    public String sendEmail(EmailRequest request) throws VException {
        logger.info("Inside sendEmail" + gson.toJson(request));
        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, gson.toJson(request));
        return null;
    }

    public String sendSMS(TextSMSRequest request) throws VException {
        if (StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: " + request);
            return null;
        }
        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(request));
        return null;
    }

    
    public String sendEmailViaRest(EmailRequest request) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public void sendMailToChallengAttemptedUsers(ChallengeUserPoints req) throws VException, UnsupportedEncodingException {
        logger.info("Sending challenge Attempt mail for userId : " + req.getUserId());
        CommunicationType emailType = CommunicationType.CHALLENGE_ATTEMPTED_DAILY;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("points", req.getPoints());
        bodyScopes.put("date", sdf.format(new Date(req.getTime())));

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", req.getUser().getFullName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(req.getUser().getEmail(), req.getUser().getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendMailToNotAttemptedUsers(UserBasicInfo user) throws VException, UnsupportedEncodingException {
        logger.info("Sending challenge Attempt mail for NotAttempted userId : " + user.getUserId());
        CommunicationType emailType = CommunicationType.CHALLENGE_ATTEMPT_PROMPT;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        HashMap<String, Object> bodyScopes = new HashMap<>();

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFullName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendClanCreationMail(ClanRes clan) throws UnsupportedEncodingException, VException {
        logger.info("Sending clan creation mail for userId : " + clan.getLeader());
        CommunicationType emailType = CommunicationType.CLAN_CREATION;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", clan.getLeaderInfo().getFullName());
        bodyScopes.put("clanName", clan.getTitle());

        String clanBaseUrl = ConfigUtils.INSTANCE.getStringValue("isl.web.baseurl");
        bodyScopes.put("clanUrl", clanBaseUrl);
        String shareUrl = clanBaseUrl + "/share?shareToken=" + clan.getShareRefCode();
        bodyScopes.put("shareUrl", shareUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("studentName", clan.getLeaderInfo().getFullName());
        subjectScopes.put("clanName", clan.getTitle());

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(clan.getLeaderInfo().getEmail(), clan.getLeaderInfo().getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendClanJoinedMail(ClanRes clan, UserBasicInfo userInfo) throws UnsupportedEncodingException, VException {
        logger.info("Sending clan joined mail for userId : " + clan.getLeader());
        CommunicationType emailType = CommunicationType.CLAN_JOINED;
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMMM yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userInfo.getFullName());
        bodyScopes.put("clanName", clan.getTitle());

        String clanBaseUrl = ConfigUtils.INSTANCE.getStringValue("isl.web.baseurl");
        bodyScopes.put("clanUrl", clanBaseUrl);
        String shareUrl = clanBaseUrl + "/share?shareToken=" + clan.getShareRefCode();
        bodyScopes.put("shareUrl", shareUrl);
        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("studentName", clan.getLeaderInfo().getFullName());
        subjectScopes.put("clanName", clan.getTitle());

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userInfo.getEmail(), userInfo.getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendUserDataEmail(HttpSessionData httpSessionData, String exportUserData, String fileName,
            String subject) throws IOException, AddressException, VException {

        EmailRequest email = new EmailRequest();
        email.setBody("Please find the attached doc");
        if (StringUtils.isEmpty(subject)) {
            subject = "Here is the system details in the attached document";
        }

        subject = (ConfigUtils.INSTANCE.getBooleanValue("email.SUBJECT.env.append")
                ? ConfigUtils.INSTANCE.getStringValue("environment") + ": "
                : "") + subject;

        email.setSubject(subject);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(httpSessionData.getEmail(), httpSessionData.getFirstName()));
        email.setTo(toList);
        email.setType(CommunicationType.SYSTEM_INFO);

        EmailAttachment attachment = new EmailAttachment();
        attachment.setApplication("application/csv");
        attachment.setAttachmentData(exportUserData);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(System.currentTimeMillis());

        attachment.setFileName(fileName + ":" + dateTime + ".csv");
        email.setAttachment(attachment);

//        sendEmail(email);
        sendEmailViaRest(email);
    }

    public void sendPostTestEmail(CMDSTestAttempt testAttempt, CMDSTest test, ContentInfo contentInfo) throws UnsupportedEncodingException, VException {
        if (contentInfo == null) {
            contentInfo = contentInfoManager.getContentById(testAttempt.getContentInfoId(),false);
        }
        if (StringUtils.isEmpty(contentInfo.getStudentId())) {
            return;
        }
        User userBasicInfo = fosUtils.getUserInfo(Long.valueOf(contentInfo.getStudentId()), true);
        if (userBasicInfo == null) {
            return;
        }
        if (test == null) {
            String testId = testAttempt.getTestId();
            if (StringUtils.isNotEmpty(testId)) {
                test = cmdsTestManager.getCMDSTestById(testId);
            }
        }
        if (test == null) {
            logger.info("no test found for the attempt not sending any emails:" + testAttempt);
            return;
        }
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("title", contentInfo.getContentTitle());
        SimpleDateFormat sdf = new SimpleDateFormat("E, dd MMM yyyy");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String date = sdf.format(testAttempt.getCreationTime());
        sdf = new SimpleDateFormat("hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String time = sdf.format(testAttempt.getCreationTime());
        bodyScopes.put("date", date);
        bodyScopes.put("time", time);
        bodyScopes.put("studentName", userBasicInfo.getFullName());
        String courseName = getCourseName(contentInfo);
        bodyScopes.put("courseName", courseName);
        Long duration = test.getDuration();
        Double dur = (Double) (duration * 1.0 / DateTimeUtils.MILLIS_PER_HOUR);
        bodyScopes.put("duration", String.format("%.1f", dur));
        String subjects = "";
        List<CategoryAnalytics> categoryAnalyticsList = testAttempt.getCategoryAnalyticsList();
        List<HashMap<String, Object>> categories = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(categoryAnalyticsList)) {
            for (CategoryAnalytics analytics : categoryAnalyticsList) {
                subjects += analytics.getName() + ", ";
                HashMap<String, Object> categorie = new HashMap<>();
                String name = analytics.getName();
                name = name.substring(0, 3);
                categorie.put("name", name);
                categorie.put("marksAcheived", String.format("%.0f", analytics.getMarks()));
                categorie.put("totalMarks", String.format("%.0f", analytics.getMaxMarks()));
                Integer questionTotal = analytics.getAttempted() + analytics.getUnattempted();
                categorie.put("questionTotal", String.valueOf(questionTotal));
                categorie.put("questionAttempted", String.valueOf(analytics.getAttempted()));
                categorie.put("questionLeft", String.valueOf(analytics.getUnattempted()));
                categorie.put("questionCorrect", String.valueOf(analytics.getCorrect()));
                categorie.put("questionIncorrect", String.valueOf(analytics.getIncorrect()));
                categories.add(categorie);
            }
            subjects = subjects.substring(0, subjects.length() - 2);
        }
        bodyScopes.put("categories", categories);
        bodyScopes.put("subjects", subjects);
        bodyScopes.put("marksAcheived", String.format("%.0f", testAttempt.getMarksAcheived()));
        bodyScopes.put("totalMarks", String.format("%.0f", testAttempt.getTotalMarks()));
        bodyScopes.put("questionAttempted", String.valueOf(testAttempt.getAttempted()));
        Integer questionTotal = testAttempt.getAttempted() + testAttempt.getUnattempted();
        bodyScopes.put("questionTotal", String.valueOf(questionTotal));
        bodyScopes.put("questionLeft", String.valueOf(testAttempt.getUnattempted()));
        bodyScopes.put("questionCorrect", String.valueOf(testAttempt.getCorrect()));
        bodyScopes.put("questionIncorrect", String.valueOf(testAttempt.getIncorrect()));

        bodyScopes.put("rank", String.valueOf(testAttempt.getRank()));
        bodyScopes.put("totalStudents", String.valueOf(testAttempt.getTotalStudents()));
        bodyScopes.put("batchAvg", String.valueOf(testAttempt.getBatchAvg()));

        bodyScopes.put("detailLink", FOS_ENDPOINT + "/contents/test/result/" + contentInfo.getId() + "?attemptId=" + testAttempt.getId());
        CommunicationType communicationType = CommunicationType.LMS_POST_TEST_EMAIL_SUBJECTIVE;
        if (ContentInfoType.OBJECTIVE.equals(test.getContentInfoType())) {
            communicationType = CommunicationType.LMS_POST_TEST_EMAIL_OBJECTIVE;
        }
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes,
                communicationType, userBasicInfo.getRole());
        sendEmail(request);
    }

    public String getCourseName(ContentInfo contentInfo) {
        if (contentInfo.getEngagementType() != null && StringUtils.isNotEmpty(contentInfo.getContextId())
                && EngagementType.OTF.equals(contentInfo.getEngagementType()) && StringUtils.isEmpty(contentInfo.getCourseName())) {
            Set<String> contextId = new HashSet<>();
            contextId.add(contentInfo.getContextId());
            try {
                Map<String, String> courseMap = getCourseTitleForBatchIds(contextId);
                if (courseMap != null) {
                    String courseName = courseMap.get(contentInfo.getContextId());
                    if (StringUtils.isNotEmpty(courseName)) {
                        return courseName;
                    }
                }
            } catch (Exception e) {
                logger.error("Error in getting courseTitle" + e.getMessage());
            }
        }
        return null;
    }

    public Map<String, String> getCourseTitleForBatchIds(Set<String> batchIds) throws VException {
        String url = SUBSCRIPTION_ENDPOINT + "/batch/getCourseTitleForBatchIds?batchIds=";
        if (ArrayUtils.isNotEmpty(batchIds)) {
            url += batchIds.iterator().next();
            for (String batchId : batchIds) {
                url += "&batchIds=" + batchId;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type otfMapType = new TypeToken<Map<String, String>>() {
            }.getType();

            Map<String, String> response = new Gson().fromJson(jsonString, otfMapType);
            return response;
        } else {
            logger.info("batchIds is null.");
            return new HashMap<>();
        }
    }
    
    public void sendReevaluateMail(HashMap<String, Object> bodyScope, UserBasicInfo userBasicInfo) throws UnsupportedEncodingException, VException{
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));
        
        EmailRequest emailRequest = new EmailRequest(toList, bodyScope, bodyScope, CommunicationType.LMS_REEVALUATE_TEST, Role.STUDENT);
        sendEmail(emailRequest);
    }

    public void sendUserEmailWithMultipleAttachments(HttpSessionData httpSessionData, Map<String,String> exportUserData,
                                  String subject) throws IOException, AddressException, VException {

        EmailRequest email = new EmailRequest();
        email.setBody("Please find the attached doc");
        if (StringUtils.isEmpty(subject)) {
            subject = "Here is the system details in the attached document";
        }

        subject = (ConfigUtils.INSTANCE.getBooleanValue("email.SUBJECT.env.append")
                ? ConfigUtils.INSTANCE.getStringValue("environment") + ": "
                : "") + subject;

        email.setSubject(subject);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(httpSessionData.getEmail(), httpSessionData.getFirstName()));
        email.setTo(toList);
        email.setType(CommunicationType.SYSTEM_INFO);
List<EmailAttachment> attachmentList = new ArrayList<EmailAttachment>();
    for(String fileName : exportUserData.keySet()) {

        EmailAttachment attachment = new EmailAttachment();
    attachment.setApplication("application/csv");
    attachment.setAttachmentData(exportUserData.get(fileName));

    SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
    sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
    String dateTime = sdf.format(System.currentTimeMillis());

    attachment.setFileName(fileName + ":" + dateTime + ".csv");
        attachmentList.add(attachment);
    }
//        sendEmail(email);
        email.setExtraAttachments(attachmentList);
        sendEmailViaRest(email);
    }


}
