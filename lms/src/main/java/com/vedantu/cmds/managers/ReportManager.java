package com.vedantu.cmds.managers;

import com.vedantu.cmds.dao.CMDSQuestionDAO;
import com.vedantu.cmds.dao.ReportDAO;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.Report;
import com.vedantu.cmds.entities.UserReport;
import com.vedantu.cmds.enums.ReportStatus;
import com.vedantu.cmds.request.GetReportsReq;
import com.vedantu.cmds.request.ReviewReportReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class ReportManager {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private CMDSQuestionDAO questionDAO;

	public static int reportBufferDays = 3;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ReportManager.class);

	@Autowired
	private ReportDAO reportDAO;

	public void reportEntity(Report report) throws BadRequestException {
		// verify
		canUserReport(report);
		Query query = new Query();
		query.addCriteria(Criteria.where(Report.Constants.ENTITYID).is(report.getEntityId()));
		query.addCriteria(Criteria.where(Report.Constants.STATUS).is(ReportStatus.OPEN));
		query.with(Sort.by(Sort.Direction.DESC, Report.Constants.CREATION_TIME));
		List<Report> reportList= reportDAO.runQuery(query, Report.class);
		UserReport userReport =  report.getUserReports().get(0);
		userReport.setRaisedOn( System.currentTimeMillis());
		if(!ArrayUtils.isEmpty(reportList)){
			report=reportList.get(0);
			List<UserReport> userReports =  report.getUserReports();
			userReports.add(userReport);
		}
		else{
			CMDSQuestion   cmdsQuestion =questionDAO.getById(report.getEntityId());
			report.setSubject(cmdsQuestion.getSubject());
			report.setTargetGrade(cmdsQuestion.getTargetGrade());
			report.setMainTags(cmdsQuestion.getMainTags());
		}
		report.setStatus(ReportStatus.OPEN);
		report.increaseCount();
		reportDAO.update(report);
	}
	public List<Report> getEntity() {
		Query query = new Query();
		query.with(Sort.by(Sort.Direction.DESC, Report.Constants.CREATION_TIME));
		return reportDAO.runQuery(query, Report.class);
	}

	public List<Report> getReports(GetReportsReq req) {
		return reportDAO.getReports(req);
	}


	public void reviewReports(ReviewReportReq req) throws Exception {
		Report report = reportDAO.getEntityById(req.getReportId(),Report.class);
		if(!ReportStatus.OPEN.equals(report.getStatus())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Report is not in open state");
		}
		if(ReportStatus.CHANGE.equals(req.getStatus()) && req.getNewEntityId() !=  null ){
			report.setNewEntityId(req.getNewEntityId());
		}
		report.setReviewBy(req.getCallingUserId().toString());
		report.setStatus(req.getStatus());
		report.setReviewComment(req.getReviewComment());
		reportDAO.update(report);
	}

	public void canUserReport(Report report) throws BadRequestException {
		Query query = new Query();
		query.addCriteria(Criteria.where(Report.Constants.ENTITYID).is(report.getEntityId()));
		query.addCriteria(Criteria.where(Report.Constants.USERREPORTS_USERID).is(report.getUserReports().get(0).getUserId()));
		query.addCriteria(Criteria.where(Report.Constants.USERREPORTS_RAISEDON).gt(System.currentTimeMillis() - reportBufferDays*DateTimeUtils.MILLIS_PER_DAY ));
		List<Report> reportList= reportDAO.runQuery(query, Report.class);
		if(!ArrayUtils.isEmpty(reportList)){
			throw new BadRequestException(ErrorCode.ALREADY_REPORTED_ENTITY, "You have already reported this Entity");
		}

	}
	public void populateCount() throws BadRequestException {
		List<Report> reportList=	getEntity();
		for( Report report : reportList){
			report.setCount(report.getUserReports().size());
			CMDSQuestion   cmdsQuestion =questionDAO.getById(report.getEntityId());
			report.setSubject(cmdsQuestion.getSubject());
			report.setTargetGrade(cmdsQuestion.getTargetGrade());
			report.setMainTags(cmdsQuestion.getMainTags());
			reportDAO.update(report);
		}
	}
	public List<Report> getReportsForCsv(GetReportsReq req) {
		return reportDAO.getReportsForCsv(req);
	}
}
