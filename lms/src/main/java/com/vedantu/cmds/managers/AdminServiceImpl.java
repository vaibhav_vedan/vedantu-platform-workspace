package com.vedantu.cmds.managers;

import com.vedantu.cmds.enums.RedisKeyCategory;
import com.vedantu.cmds.response.RedisKeyValuePair;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.cmds.utils.RedisCategoryMapping;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AdminServiceImpl implements AdminService {

    private Logger LOGGER = LogFactory.getLogger(AdminServiceImpl.class);
    private RedisDAO redisDAO;

    @Autowired
    public AdminServiceImpl(RedisDAO redisDAO) {
        this.redisDAO = redisDAO;
    }


    @Override
    public String setRedisKey(String key, String value) throws InternalServerErrorException {
        LOGGER.info("Setting redis key={}, value={}", key, value);
        return redisDAO.set(key, value);
    }

    @Override
    public String getRedisKey(String key) throws BadRequestException, InternalServerErrorException {
        LOGGER.info("Getting value for redis key={}", key);
        return redisDAO.get(key);
    }

    @Override
    public List<RedisKeyCategory> getAllRedisCategories() {
        return Arrays.asList(RedisKeyCategory.values());
    }

    private RedisKeyValuePair getRedisKeyValuePair(String key) {
        LOGGER.info("method=getRedisKeyValuePair, class=AdminServiceImpl.");
        RedisKeyValuePair keyValuePair = new RedisKeyValuePair();
        keyValuePair.setKey(key);
        try {
            keyValuePair.setValue(redisDAO.get(key));
        } catch (InternalServerErrorException | BadRequestException e) {
            LOGGER.error("Could not get value from redis key={}", key);
        }
        return keyValuePair;
    }
}
