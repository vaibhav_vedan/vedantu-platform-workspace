package com.vedantu.cmds.managers;

import com.google.gson.Gson;
import com.vedantu.board.requests.AddToBaseTreeRequest;
import com.vedantu.cmds.dao.TopicTreeDAO;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CurriculumTopicTree;
import com.vedantu.cmds.enums.TGrades;
import com.vedantu.cmds.request.TopicNamesRequest;
import com.vedantu.cmds.response.GetBaseTreeNodesByParentRes;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.*;
import com.vedantu.lms.request.ValidateAndSetTopicTargetTagsReq;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;

import java.util.*;

import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class TopicTreeManager {

    @Autowired
    public LogFactory logFactory;
    
    @Autowired
    public FosUtils fosUtils;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(TopicTreeManager.class);

    @Autowired
    private TopicTreeDAO topicTreeDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    private static final Gson gson = new Gson();

    public BaseTopicTree addToBaseTree(AddToBaseTreeRequest req) throws VException {
        logger.info("createBoard "+req);
        req.verify();

        String slug = convertToSlug(req.getName());

        BaseTopicTree board = new BaseTopicTree(req.getName().trim(), slug.trim(), req.getParentId(), req.getState(), req.getCallingUserId());

        if(StringUtils.isEmpty(req.getParentId()) || "0".equals(req.getParentId())) {
            board.setLevel(0);
        } else {
            BaseTopicTree btt = topicTreeDAO.getBaseTopicTreeNodeById(req.getParentId());
            if(btt != null) {
                Integer level = btt.getLevel();
                board.setLevel(level + 1);
            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Parent not found");
            }
        }

        topicTreeDAO.upsertBaseTopicTree(board, req.getCallingUserId(), req.isComputeChildren());

//        BoardInfoRes res = new BoardInfoRes(pojoUtils.convertToBoardPojo(board));
//        logger.info("createBoard "+res);
//        return res;

//        BoardInfoRes res = new BoardInfoRes(pojoUtils.convertToBoardPojo(board));
//        logger.info("createBoard "+res);
//        return res;

        return board;
    }

    public BaseTopicTree addToBaseTreeByName(AddToBaseTreeRequest req) throws VException {
        logger.info("createBoard "+req);
        req.verify();

        String slug = convertToSlug(req.getName());

        BaseTopicTree board = new BaseTopicTree(req.getName().trim(), slug.trim(), req.getParentId(), req.getState(), req.getCallingUserId());

        if(StringUtils.isEmpty(req.getParentName())) {
            board.setLevel(0);
        } else {
            BaseTopicTree btt = topicTreeDAO.getBaseTopicTreeNodeByField("name", req.getParentName());
            if(btt != null) {
                Integer level = btt.getLevel();
                board.setLevel(level + 1);
                board.setParentId(btt.getId());
            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Parent not found");
            }
        }

        topicTreeDAO.upsertBaseTopicTree(board, req.getCallingUserId(), req.isComputeChildren());

        return board;
    }

    public CurriculumTopicTree addToCurriculumTree(AddToBaseTreeRequest req) throws VException {
        req.verify();

        String slug = convertToSlug(req.getName());

        CurriculumTopicTree board = new CurriculumTopicTree(req.getName().trim(), slug.trim(), req.getParentId(), req.getState(), req.getCallingUserId());

        if(StringUtils.isEmpty(req.getParentId()) || "0".equals(req.getParentId())) {
            board.setLevel(0);
        } else {
            CurriculumTopicTree c = topicTreeDAO.getCurriculumTopicTreeNodeById(req.getParentId());
            if(c != null) {
                Integer level = c.getLevel();
                board.setLevel(level + 1);
            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Parent not found");
            }
        }

        if(StringUtils.isNotEmpty(req.getBaseTreeNodeName())) {
            BaseTopicTree topicTreeBoard = topicTreeDAO.getBaseTopicTreeNodeByField("name", req.getBaseTreeNodeName());
            if(topicTreeBoard == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Node in BaseTopicTree does not exist");
            }
            board.setBaseTreeNodeId(topicTreeBoard.getId());
            board.setBaseTreeNodeName(topicTreeBoard.getName());
        }

        topicTreeDAO.upsertCurriculumTopicTree(board, req.getCallingUserId());
        return board;
    }

    public CurriculumTopicTree addToCurriculumTreeByName(AddToBaseTreeRequest req) throws VException {
        req.verify();

        String slug = convertToSlug(req.getName());

        CurriculumTopicTree board = new CurriculumTopicTree(req.getName().trim(), slug.trim(), req.getParentId(), req.getState(), req.getCallingUserId());

        if(StringUtils.isEmpty(req.getParentId()) || "0".equals(req.getParentId())) {
            board.setLevel(0);
        } else {
            CurriculumTopicTree c = topicTreeDAO.getCurriculumTopicTreeNodeByField("name", req.getParentName());
            if(c != null) {
                Integer level = c.getLevel();
                board.setLevel(level + 1);
                board.setParentId(c.getId());
            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Parent not found");
            }
        }

        if(StringUtils.isNotEmpty(req.getBaseTreeNodeName())) {
            BaseTopicTree topicTreeBoard = topicTreeDAO.getBaseTopicTreeNodeByField("name", req.getBaseTreeNodeName());
            if(topicTreeBoard == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Node in BaseTopicTree does not exist");
            }
            board.setBaseTreeNodeId(topicTreeBoard.getId());
            board.setBaseTreeNodeName(topicTreeBoard.getName());
        }

        topicTreeDAO.upsertCurriculumTopicTree(board, req.getCallingUserId());
        return board;
    }

    public GetBaseTreeNodesByParentRes getBaseTreeNodesByParent(String parentId) {
        GetBaseTreeNodesByParentRes res = new GetBaseTreeNodesByParentRes();
        List<BaseTopicTree> baseTreeList = topicTreeDAO.getBasicTopicTreeNodesByParentId(parentId);
        res.setList(baseTreeList);
        res.setCount(baseTreeList.size());
        return res;
    }

    public List<BaseTopicTree> getBaseTreeNodesByLevel(int level) {
        return topicTreeDAO.getBaseTreeNodesByLevelWithoutChildren(level);
    }
    
    public List<BaseTopicTree> getBaseTreeNodesBySubject(String subject){
        return topicTreeDAO.getBaseTreeNodesBySubject(subject);
    }
    
    public Set<BaseTopicTree> getBaseTreeNodesByNodeNames(Set<String> nodeNames){
        return topicTreeDAO.getBaseTreeNodesByNodeNames(nodeNames);
    }

    public List<CurriculumTopicTree> getCurriculumTreeNodesByLevel(int level) {
        return topicTreeDAO.getCurriculumTreeNodesByLevel(level);
    }

    public void updateBaseTreeChildren(String nodeId) {
        topicTreeDAO.updateBaseTreeChildren(nodeId);
    }

    public void updateBaseTreeNodeParents(String nodeId) {
        topicTreeDAO.updateBaseTreeNodeParents(nodeId);
    }

    public void updateCurriculumTreeChildren() {
        topicTreeDAO.updateCurriculumTreeChildren();
    }

    @Deprecated
    public void updateTopicTreeErrorNodes(String patternValues){

        String[] patternValue=patternValues.split("\n");

        for(String mappingArray:patternValue) {

            String[] correctValues=mappingArray.split("\\$");
            String keyValue=correctValues[0].trim();
            String correctValue=correctValues[1].trim();
            logger.info("correctValue = {}, keyValue = {}, and if they are equal {} ",correctValue,keyValue,correctValue.equals(keyValue));
            topicTreeDAO.updateTopicTreeErrorNodes(keyValue,correctValue);
        }
    }

    public static String convertToSlug(String name) {
        String slug = null;
        if(StringUtils.isNotEmpty(name)) {
            slug = name.trim().toLowerCase().replace(' ', '-');
        }
        return slug;
    }

    public AbstractTargetTopicEntity validateAndSetTopicTargetTags(ValidateAndSetTopicTargetTagsReq req) throws BadRequestException, NotFoundException {
        req.verify();

        Set<String> overallMainTags = new HashSet<>();
        Set<String> overallSubjects = new HashSet<>();
        Set<String> overallTargets = new HashSet<>();
        Set<String> overallGrades = new HashSet<>();
        AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();

        atte.setInputTopics(req.getTopics());

        Set<String> parentTopics = new HashSet<>();
        for (String topic : req.getTopics()) {
            BaseTopicTree treeForParents = topicTreeDAO.getBaseTopicTreeNodeByField("name", topic);
            if (treeForParents != null) {
                overallMainTags.add(topic);
                logger.info(treeForParents.getName()+" has parent "+treeForParents.getParents());
                if(ArrayUtils.isNotEmpty(treeForParents.getParents())){
                    parentTopics.addAll(treeForParents.getParents());
                }
            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Topic: " + topic + " not present in BaseTree");
            }
        }
        
        overallMainTags.addAll(parentTopics);
        atte.settTopics(parentTopics);
        atte.gettTopics().addAll(req.getTopics());
        // Adding to subjects
        List<BaseTopicTree> allSystemSubjects = getBaseTreeNodesByLevel(0);
        for(BaseTopicTree btt : allSystemSubjects) {
            if(overallMainTags.contains(btt.getName())) {
                overallSubjects.add(btt.getName());
            }
        }

        if (req.getTargetGrades().size() > 0) {
            for (String slug : req.getTargetGrades()) {
                String[] subTags = slug.split("-");
                if (subTags.length == 2) {
                    // Not checking with Curriculum for now
                    List<CurriculumTopicTree> cT = topicTreeDAO.verifyCurriculumNodeAndChild(subTags);
                    if (cT.size() != 1) {
                        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "TargetGrade does not exist in CurriculumTree");
                    }
                    overallMainTags.add(slug);
                    for (String subTag : subTags) {
                        String subSlug = org.apache.commons.lang.StringUtils.trimToEmpty(subTag);
                        if (org.apache.commons.lang.StringUtils.isNotEmpty(subSlug)) {
                            overallMainTags.add(subSlug);
                        }
                    }
                } else {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Each TargetGrade should have - separated values");
                }
            }
        } else {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "TargetGrade not present");
        }

        // Adding to targets
        List<CurriculumTopicTree> allSystemTargets = getCurriculumTreeNodesByLevel(0);

        logger.info(allSystemTargets);

        for(CurriculumTopicTree ctt: allSystemTargets) {
            if(overallMainTags.contains(ctt.getName())) {
                overallTargets.add(ctt.getName());
            }
        }
        logger.info(overallTargets);

        // Adding to grades
        Set<String> allSystemGrades = TGrades.getGradeValues();

        logger.info(allSystemGrades);

        for(String asg: allSystemGrades) {
            if(overallMainTags.contains(asg)) {
                overallGrades.add(asg);
            }
        }
        logger.info(overallGrades);

        atte.setTargetGrades(req.getTargetGrades());
        atte.setMainTags(overallMainTags);
        atte.settSubjects(overallSubjects);
        atte.settGrades(overallGrades);
        atte.settTargets(overallTargets);
        return atte;
    }

    public void cacheEntireTopicTreeStructure() throws InternalServerErrorException {
        //topicTreeDAO.flushEntireTopicTree();
        topicTreeDAO.cacheEntireTopicTree();
    }

    public List<String> getMatchingTopicNamesOfTopicTree() {
        // TODO: Added a part of hotfix, to be changed soon
        List<BaseTopicTree> baseTopicTrees=topicTreeDAO.getNamesOfFirstLevelNodes();
        List<String> keys=baseTopicTrees.stream().map(BaseTopicTree::getName).collect(Collectors.toList());
        return keys;
    }


    public Set<String> getAllChildrenByTopicNames(TopicNamesRequest request) throws BadRequestException, InternalServerErrorException {

        if(ArrayUtils.isEmpty(request.getNames())){
            return new HashSet<>();
        }

        List<String> names=
                request.getNames()
                .stream()
                .map(String::toLowerCase)
                .map(TopicTreeManager::getTopicKey)
                .collect(Collectors.toList());

        Map<String, String> topicTreeResponse=redisDAO.getValuesForKeys(names);

        if(topicTreeResponse==null){
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,"Please specify a valid topic name", Level.ERROR);
        }

        List<BaseTopicTree> baseTopicTrees=
                topicTreeResponse
                    .values()
                    .parallelStream()
                    .map(TopicTreeManager::getBaseTopicTreeFromJSONObject)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toList());

        Set<String> children=
                baseTopicTrees
                    .parallelStream()
                    .map(BaseTopicTree::getChildren)
                    .filter(ArrayUtils::isNotEmpty)
                    .flatMap(Collection::parallelStream)
                    .collect(Collectors.toSet());

        Set<String> self=
                baseTopicTrees
                    .parallelStream()
                    .map(BaseTopicTree::getName)
                    .filter(StringUtils::isNotEmpty)
                    .collect(Collectors.toSet());

        /*
        Set<String> parents=
                baseTopicTrees
                    .parallelStream()
                    .map(BaseTopicTree::getParents)
                    .filter(ArrayUtils::isNotEmpty)
                    .flatMap(Collection::parallelStream)
                    .filter(Objects::nonNull)
                    .collect(Collectors.toSet());


        children.addAll(parents);
        */
        children.addAll(self);

        return children;
    }

    public static String matchingKeyRegex(String key){
        return "BaseTopicTree:*"+key.toLowerCase()+"*";
    }

    public static String getTopicKey(String key){
        return "BaseTopicTree:"+key;
    }

    public static String extractTopicName(String key){
        int size="BaseTopicTree:".length();
        return key.substring(size);
    }

    public static Set<String> getChildrenFromJSONObjectOfBaseTopicTree(String jsonObject){
        BaseTopicTree tree=gson.fromJson(jsonObject,BaseTopicTree.class);
        return ArrayUtils.isNotEmpty(tree.getChildren())?tree.getChildren():new HashSet<>();
    }

    public static BaseTopicTree getBaseTopicTreeFromJSONObject(String jsonObject){
        BaseTopicTree tree=gson.fromJson(jsonObject,BaseTopicTree.class);
        return tree;
    }

    public PlatformBasicResponse insertNewBaseTreeForSubject(String baseTreeStructureForSubject){
        logger.info("insertNewBaseTreeForSubject DEBUG START");
        String[] lines=baseTreeStructureForSubject.split("\n");
        logger.info("Number of lines are :"+lines.length);
        Map<String,String> topicTreeIndexMapping=new HashMap<>();
        Map<String,Integer> currentParentChildOrder=new HashMap<>();
        Map<String,Set<String>> nodeIdParentMap=new HashMap<>();
        Map<String,Set<String>> nodeIdChildrenMap=new HashMap<>();
        Arrays.stream(lines).forEach(line->{
            logger.info(line);
            String[] titles=line.split("\\$");
            logger.info("No of elements in current token: "+titles.length);
            AtomicInteger index=new AtomicInteger(0);
            Arrays.stream(titles).map(String::trim).filter(StringUtils::isNotEmpty).forEach(title->{
                int idx=index.getAndIncrement();
                if(StringUtils.isNotEmpty(title) && !topicTreeIndexMapping.containsKey(title)){
                    String parentId= idx!=0 ? topicTreeIndexMapping.get(titles[idx-1].trim()) : null;
                    String slug=title.toLowerCase().replace(" ","-");
                    BaseTopicTree baseTopicTree=new BaseTopicTree(title,slug,parentId,currentParentChildOrder.get(parentId));
                    topicTreeDAO.insertNodeInTopicTree(baseTopicTree);
                    logger.info("Node inserted is "+baseTopicTree);
                    topicTreeIndexMapping.put(title,baseTopicTree.getId());
                    currentParentChildOrder.put(baseTopicTree.getId(),0);
                    if(StringUtils.isNotEmpty(parentId)){
                        currentParentChildOrder.put(parentId, currentParentChildOrder.get(parentId) + 1);
                        Set<String> parentTitles= Arrays.stream(titles).map(String::trim).limit(idx).collect(Collectors.toSet());
                        nodeIdParentMap.put(baseTopicTree.getId(),parentTitles);
                        Set<String> childTitles=ArrayUtils.isNotEmpty(nodeIdChildrenMap.get(parentId))?nodeIdChildrenMap.get(parentId):new HashSet<>();
                        childTitles.add(title);
                        nodeIdChildrenMap.put(parentId,childTitles);
                    }
                }
                logger.info(title);
            });
        });
        logger.info("nodeIdParentMap: \n");
        nodeIdParentMap.keySet().stream().forEach(key->logger.info(key+" -- "+nodeIdParentMap.get(key)));
        populateAllChildren(topicTreeIndexMapping,nodeIdChildrenMap,topicTreeIndexMapping.get(lines[0].split("\\$")[0].trim()));
        logger.info("nodeIdChildrenMap: \n");
        nodeIdChildrenMap.keySet().stream().forEach(key->logger.info(key+" -- "+nodeIdChildrenMap.get(key)));
        logger.info("insertNewBaseTreeForSubject DEBUG END");
        List<BaseTopicTree> allTopicTreeNodes = topicTreeDAO.getAllTopicTreeNodes();
        allTopicTreeNodes
                .stream()
                .map(BaseTopicTree::getId)
                .forEach(id-> topicTreeDAO.updateParentAndChildrenOfNodes(id,nodeIdParentMap.get(id),nodeIdChildrenMap.get(id)));
        return new PlatformBasicResponse();
    }

    public Set<String> populateAllChildren(Map<String,String> topicTreeIndexMapping,Map<String,Set<String>> nodeIdChildrenMap,String parentId){
        logger.info("populateAllChildren for parent id: "+parentId);
        Set<String> currentChildrenTitles=ArrayUtils.isNotEmpty(nodeIdChildrenMap.get(parentId))?nodeIdChildrenMap.get(parentId):new HashSet<>();
        logger.info("currentChildrenTitles for parent id "+parentId+" is "+currentChildrenTitles);
        Set<String> children=currentChildrenTitles
                .parallelStream()
                .map(title->topicTreeIndexMapping.get(title))
                .map(childId->populateAllChildren(topicTreeIndexMapping,nodeIdChildrenMap,childId))
                .flatMap(allChildren->allChildren.stream())
                .collect(Collectors.toSet());
        children.addAll(currentChildrenTitles);
        logger.info("currentChildrenTitles for parent id "+parentId+" after populating all data is "+currentChildrenTitles);
        nodeIdChildrenMap.put(parentId,children);
        return children;
    }

    public List<String> mapOldDataToNewData(String mapping){
        logger.info("mapOldDataToNewData DEBUG START");
        logger.info("`mapping` data is "+mapping);
        String lines[]=mapping.split("\n");
        List<String> listOfInvalidStringsToMap = Arrays.stream(lines)
                .parallel()
                .map(line -> line.split(",", 2))
                .map(oldNew -> topicTreeDAO.mapNewNameToOldNames(oldNew[0], oldNew[1]))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        topicTreeDAO.mapOldNameToOldName();
        logger.info("mapOldDataToNewData DEBUG END");
        return listOfInvalidStringsToMap;
    }

    public void setCorrectLevelsInTopicTree() {
        // Get all the subjects of topic tree
        List<BaseTopicTree> baseTopicTrees=topicTreeDAO.getBaseTreeSubjectNodes();
        logger.info("Subjects received are ");
        baseTopicTrees.stream().map(BaseTopicTree::getName).forEach(logger::info);
        List<String> parentIds=baseTopicTrees.stream().map(BaseTopicTree::getId).collect(Collectors.toList());

        // For each parents increase the level of their children
        int level=1;
        parentIds.forEach(parentId->setCorrectLevelOfChildren(parentId,level));

    }

    private void setCorrectLevelOfChildren(String parentId, int level) {
        if(StringUtils.isEmpty(parentId)){
            return;
        }
        logger.info("Updating for parent id "+parentId+" for level "+level);
        List<BaseTopicTree> children=topicTreeDAO.getAllChildrenByParentId(parentId);
        if(ArrayUtils.isNotEmpty(children)){
            topicTreeDAO.updateLevelOfChildren(parentId,level);
            children
                .parallelStream()
                .map(BaseTopicTree::getId)
                .forEach(id->setCorrectLevelOfChildren(id,level+1));
        }
    }

    public PlatformBasicResponse uploadEntireCommerceTree() {

        Long callingUserId=4102386386494267l;//httpSessionUtils.getCallingUserId();
        String Accountancy = "Accountancy";
        String EconomicStudies="Economic Studies";
        String BusinessStudies="Business Studies";


        Set<BaseTopicTree> parentNodes = getBaseTreeNodesByNodeNames(new HashSet<>(Arrays.asList(Accountancy, EconomicStudies, BusinessStudies)));
        for(BaseTopicTree parentNode:parentNodes){
            if(Accountancy.equals(parentNode.getName())){
                logger.info("Uploading for {}",Accountancy);
                List<String> accountancy=new ArrayList<>(Arrays.asList("Introduction to Accounting and Basic Accounting Terms","Theory Base of Accounting-AS-IFRS and Bases","Accounting Equation and Procedure-Rules of Dr and Cr","Origin of Transactions-Source Documents and Vouchers","Journal-Including Trade Discount and Cash Discount","Ledger","Special Purpose Book I - Other Subsidiary Books","Special Purpose Book II - Cash Book","Special Purpose Book II - Petty Cash Book","Bank Reconciliation Statement","Trial Balance","Rectification of Errors","Depreciation","Bills of Exchange","Financial Statements 1 and 2","Single Entry","NPO","Computer Awareness"));
                populateAllSubjectData(callingUserId, parentNode, accountancy);
                topicTreeDAO.updateParentAndChildrenOfNodes(parentNode.getId(),null,new HashSet<>(accountancy));
            }else if(EconomicStudies.equals(parentNode.getName())){
                logger.info("Uploading for {}",EconomicStudies);
                List<String> economics=new ArrayList<>(Arrays.asList("Significance of Statistics In Economics","Collection of Data","Data Organisation","Presentation of Data","Introduction to Micro Economics","Production and Cost","Market Equilibrium","Theory of Consumers Behaviour"));
                populateAllSubjectData(callingUserId,parentNode,economics);
                topicTreeDAO.updateParentAndChildrenOfNodes(parentNode.getId(),null,new HashSet<>(economics));
            }else if(BusinessStudies.equals(parentNode.getName())){
                logger.info("Uploading for {}",BusinessStudies);
                List<String> businessStudies=new ArrayList<>(Arrays.asList("Nature and Purpose of Business","Forms of Business Organisation","Private Public and Global Enterprises","Business Service","Emerging Modes of Business","Social Responsibility of Business","Formation of Joint Stock Company","Sources of Business Finance","Small Business","Internal Trade","International Business - I","International Business - II"));
                populateAllSubjectData(callingUserId,parentNode,businessStudies);
                topicTreeDAO.updateParentAndChildrenOfNodes(parentNode.getId(),null,new HashSet<>(businessStudies));
            }else {
                logger.error("There's some issue...");
            }
        }

        return new PlatformBasicResponse();
    }

    private void populateAllSubjectData(Long callingUserId, BaseTopicTree parentNode, List<String> nodes) {
        logger.info("callingUserId - {}\nBaseTopicTree - {}\nnodes - {}",callingUserId,parentNode,nodes);
        for(String node: nodes){
            BaseTopicTree board = new BaseTopicTree(node.trim(), convertToSlug(node).trim(), parentNode.getId(), VisibilityState.VISIBLE,callingUserId);
            board.setLevel(parentNode.getLevel()+ 1);
            topicTreeDAO.upsertBaseTopicTree(board, callingUserId, true);
        }
    }
}
