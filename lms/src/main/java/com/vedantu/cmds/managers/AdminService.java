package com.vedantu.cmds.managers;

import com.vedantu.cmds.enums.RedisKeyCategory;
import com.vedantu.cmds.response.RedisKeyValuePair;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;

import java.util.List;

public interface AdminService {

    String setRedisKey(String key, String value) throws InternalServerErrorException;

    String getRedisKey(String key) throws BadRequestException, InternalServerErrorException;

    List<RedisKeyCategory> getAllRedisCategories();
}
