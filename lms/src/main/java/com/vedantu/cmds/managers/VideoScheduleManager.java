/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers;

import com.vedantu.cmds.dao.VideoScheduleDAO;
import com.vedantu.cmds.entities.VideoSchedule;
import com.vedantu.exception.VException;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class VideoScheduleManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(VideoScheduleManager.class);

    @Autowired
    private VideoScheduleDAO videoScheduleDAO;

    @Autowired
    private FosUtils fosUtils;

    public void create(List<VideoSchedule> schedules, String callingUserId) throws VException, Exception {
        //too much but since admin side, it is ok, won't be more than 10000 entries
        if (ArrayUtils.isNotEmpty(schedules)) {
            for (VideoSchedule schedule : schedules) {
                AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
                atte.setTargetGrades(schedule.getTargetGrades());
                atte.settTopics(schedule.gettTopics());
                AbstractTargetTopicEntity newTags = fosUtils.setAndGenerateTags(atte);
                schedule.setTags(newTags);
            }
        }
        videoScheduleDAO.replaceAll(schedules, callingUserId);
    }

    public List<VideoSchedule> get(Set<String> mainTags, Long fromTime, Long tillTime, Integer start, Integer size) {
        return videoScheduleDAO.getVideoSchedules(mainTags, fromTime, tillTime, start, size);
    }
}
