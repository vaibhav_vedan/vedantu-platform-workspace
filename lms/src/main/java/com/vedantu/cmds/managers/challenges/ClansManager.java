package com.vedantu.cmds.managers.challenges;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserMinimalInfo;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.dao.challenges.ClanDAO;
import com.vedantu.cmds.dao.challenges.PictureChallengeDAO;
import com.vedantu.cmds.dao.sql.ClanLeaderBoardDAO;
import com.vedantu.cmds.dao.sql.ClanMemberDAO;
import com.vedantu.cmds.dao.sql.PictureChallengeAttemptDAO;
import com.vedantu.cmds.dao.sql.PictureChallengeLeaderBoardDAO;
import com.vedantu.cmds.entities.challenges.Clan;
import com.vedantu.cmds.entities.challenges.ClanLeaderBoard;
import com.vedantu.cmds.entities.challenges.ClanMember;
import com.vedantu.cmds.entities.challenges.PictureChallenge;
import com.vedantu.cmds.entities.challenges.PictureChallengeAttempt;
import com.vedantu.cmds.entities.challenges.PictureChallengeLeaderBoard;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.managers.AmazonS3Manager;
import com.vedantu.cmds.pojo.challenges.PictureChallengeAnswer;
import com.vedantu.cmds.pojo.challenges.PictureChallengeDayDeductions;
import com.vedantu.cmds.request.challenges.GetPictureChallengesReq;
import com.vedantu.cmds.request.challenges.PictureChallengePointsIncrementReq;
import com.vedantu.cmds.request.challenges.SubmitPictureChallengeAnswerReq;
import com.vedantu.cmds.request.clans.CreateClanReq;
import com.vedantu.cmds.request.clans.CreatePictureChallengeReq;
import com.vedantu.cmds.request.clans.GetClansReq;
import com.vedantu.cmds.response.challenges.ClanLeaderBoardRes;
import com.vedantu.cmds.response.clans.CreatePictureChallengeRes;
import com.vedantu.cmds.response.clans.GetPictureChallengesRes;
import com.vedantu.cmds.response.clans.PictureChallengeLeaderBoardRes;
import com.vedantu.cmds.response.clans.PictureChallengeRes;
import com.vedantu.cmds.utils.ChallengeUtilCommon;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.lms.cmds.request.GetLeaderBoardForChallengeReq;
import com.vedantu.lms.cmds.request.InviteClanMemberReq;
import com.vedantu.lms.cmds.request.JoinClanForEventReq;
import com.vedantu.lms.cmds.response.ClanRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.pojo.CloudStorageEntity;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import javax.imageio.ImageIO;

@Service
public class ClansManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private ClanDAO clanDAO;

    @Autowired
    private ClanMemberDAO clanMemberDAO;

    @Autowired
    private PictureChallengeDAO pictureChallengeDAO;

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    @Autowired
    private UserLeaderBoardManager userLeaderBoardManager;

    @Autowired
    private ClanLeaderBoardDAO clanLeaderBoardDAO;

    @Autowired
    private PictureChallengeLeaderBoardDAO pictureChallengeLeaderBoardDAO;

    @Autowired
    private PictureChallengeAttemptDAO pictureChallengeAttemptDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ClansManager.class);

    @Autowired
    private FosUtils fosUtils;

    private final Gson gson = new Gson();
    @Autowired
    private DozerBeanMapper mapper;

    private static final double IMAGE_PARTITION_COUNT = 100.0;
    private static final Long BUFFER_FOR_SUBMIT_ANSWER = ConfigUtils.INSTANCE.getLongValue("buffer.for.challenge.submit.answer");
    private static final int DEFAULT_MUSCLE_POINTS = ConfigUtils.INSTANCE.getIntValue("clans.default.muscle.points");
    private static final int MAX_MUSCLE_POINT_COUNT = ConfigUtils.INSTANCE.getIntValue("clans.max.muscle.point.count");

    public Clan createClan(CreateClanReq req) throws BadRequestException, ConflictException {
        req.verify();
        Clan clan = new Clan();
        clan.setClanPic(req.getClanPic());
        clan.setEvent(req.getEvent());
        clan.setTagLine(req.getTagLine());
        clan.setTitle(req.getTitle());
        clan.setLeader(req.getUserId());
        UserDetailsInfo user = null;
        if (ArrayUtils.isNotEmpty(req.getAllowedCategories()) && req.getAllowedCategories().size() <= 1) {
            user = validateCategory(req.getUserId(), req.getAllowedCategories().get(0));
            clan.setAllowedCategories(req.getAllowedCategories());
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Allowed Categories is empty or more than 1");
        }
        String refId = generateClanShareCode(req.getTitle(), req.getUserId());
        Clan check = clanDAO.getByRefCode(refId);
        if (check != null) {
            int counter = 0;
            while (check != null) {
                refId = generateClanShareCode(req.getTitle(), req.getUserId());
                check = clanDAO.getByRefCode(refId);
                counter++;
                if (counter > 5) {
                    refId = generateClanShareCode("", System.currentTimeMillis());
                    break;
                }
            }

            // RElook req
        }
        clan.setShareRefCode(refId);
        clanDAO.saveClan(clan);
        ClanMember mem = addClanMember(clan.getId(), req.getUserId());

        if (user != null && user.getStudent() != null) {
            ClanRes clanRes = mapper.map(clan, ClanRes.class);
            UserMinimalInfo info = mapper.map(user.getStudent(), UserMinimalInfo.class);
            clanRes.setLeaderInfo(info);
            Map<String, Object> payload = new HashMap<>();
            payload.put("clanRes", clanRes);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CLAN_CREATION_MAIL, payload);
            asyncTaskFactory.executeTask(params);
        }

        // if(mem == null && mem.getId() != null){
        //
        // }
        return clan;
    }

    public ClanMember addClanMember(String clanId, Long userId) throws ConflictException, BadRequestException {
        return addClanMember(clanId, userId, null);
    }

    public ClanMember addClanMember(String clanId, Long userId, Long invitedBy)
            throws ConflictException, BadRequestException {
        if (StringUtils.isEmpty(clanId) || userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "clanId or userId is Empty. clan : " + clanId + " user " + userId);
        }
        ClanMember mem = clanMemberDAO.getByClanIdAndUserId(userId, clanId);
        if (mem != null) {
            throw new ConflictException(ErrorCode.USER_MEMBER_OF_CLAN, "User is already part of clan");
        }
        ClanMember member = new ClanMember();
        member.setClanId(clanId);
        member.setUserId(userId);
        if (invitedBy != null) {
            member.setInvitedBy(invitedBy);
        }

        clanMemberDAO.create(member, userId.toString());

        return member;
    }

    public void inviteClanMembers(InviteClanMemberReq req) throws BadRequestException {
        req.verify();
        Clan clan = clanDAO.getById(req.getClanId());
        if (ArrayUtils.isNotEmpty(req.getEmailIds())) {
            for (String email : req.getEmailIds()) {
                logger.info("sending mail to " + email + " with refCode " + clan.getShareRefCode());
                // sendEmail with RefId
            }
        }
    }

    public ClanRes joinClanForEvent(JoinClanForEventReq req)
            throws BadRequestException, NotFoundException, ConflictException {
        req.verify();
        ClanRes resp = null;
        Clan clan = clanDAO.getByRefCode(req.getShareRefCode());
        ClanMember member = null;
        if (clan != null) {
            UserDetailsInfo user = validateCategory(req.getUserId(), clan.getAllowedCategories().get(0));
            if (clan.getAllowedCategories().contains(req.getCategory())) {
                member = addClanMember(clan.getId(), req.getUserId());
                if (member != null && member.getId() != null) {
                    clan.setMemberCount(clan.getMemberCount() + 1);
                    clanDAO.saveClan(clan);
                    resp = mapper.map(clan, ClanRes.class);
                    resp.setClanId(clan.getId());
                    UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(clan.getLeader(), false);
                    if (userBasicInfo != null) {
                        resp.setLeaderInfo(
                                mapper.map(fosUtils.getUserBasicInfo(clan.getLeader(), false), UserMinimalInfo.class));
                    }
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("clanRes", resp);
                    payload.put("user", user.getStudent());
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CLAN_JOINED_MAIL, payload);
                    asyncTaskFactory.executeTask(params);
                }
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "User Category not in allowed category " + req.getCategory());
            }
        } else {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Clan Not found for code " + req.getShareRefCode());
        }
        return resp;
    }

    public ClanRes getClanMembers(String clanId) throws BadRequestException, NotFoundException {
        if (StringUtils.isEmpty(clanId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "ClanId is null " + clanId);
        }
        ClanRes resp = null;
        Clan clan = clanDAO.getById(clanId);
        List<ClanMember> members = new ArrayList<>();
        List<UserMinimalInfo> userInfos = new ArrayList<>();
        Set<Long> userIds = new HashSet<>();
        if (clan != null) {
            resp = mapper.map(clan, ClanRes.class);
            resp.setClanId(clan.getId());
            members = clanMemberDAO.getByClanId(clanId);
            if (ArrayUtils.isNotEmpty(members)) {
                for (ClanMember member : members) {
                    userIds.add(member.getUserId());
                }
                Map<Long, UserBasicInfo> usersmap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
                userInfos = getUserMinimalInfos(usersmap.values());
                resp.setClanMembers(userInfos);
                if (clan.getLeader() != null && usersmap != null) {
                    resp.setLeaderInfo(mapper.map(usersmap.get(clan.getLeader()), UserMinimalInfo.class));
                }
            }
        } else {
            // clan not found
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Clan Not found for code " + clanId);
        }
        return resp;
    }

    public List<ClanRes> getUsersClans(Long userId) throws BadRequestException, NotFoundException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null " + userId);
        }
        List<ClanRes> resps = new ArrayList<>();
        List<ClanMember> members = clanMemberDAO.getByUserId(userId);
        List<String> clanIds = new ArrayList<>();
        Set<Long> userIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(members)) {
            for (ClanMember member : members) {
                clanIds.add(member.getClanId());
            }
            List<Clan> clans = clanDAO.getByIds(clanIds);
            Map<String, Long> clanPointMap = getClanPointsOverallMap(clanIds);
            Map<String, List<UserMinimalInfo>> clanMemberMap = getClanMembersMap(clanIds);
            if (ArrayUtils.isNotEmpty(clans)) {
                logger.info("Number of clans - " + clans.size());
                for (Clan clan : clans) {
                    ClanRes res = mapper.map(clan, ClanRes.class);
                    res.setClanId(clan.getId());
                    res.setPoints(clanPointMap.get(clan.getId()));
                    res.setClanMembers(clanMemberMap.get(clan.getId()));
                    userIds.add(clan.getLeader());
                    resps.add(res);
                }
                if (ArrayUtils.isNotEmpty(userIds)) {
                    Map<Long, UserBasicInfo> usersmap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
                    for (ClanRes res : resps) {
                        if (res.getLeader() != null && usersmap != null && usersmap.containsKey(res.getLeader())) {
                            res.setLeaderInfo(mapper.map(usersmap.get(res.getLeader()), UserMinimalInfo.class));
                        }
                    }
                }
                Collections.sort(resps, new Comparator<ClanRes>() {
                    @Override
                    public int compare(ClanRes o1, ClanRes o2) {
                        if (o1 == null || o1.getPoints() == null) {
                            return 1;
                        } else if (o2 == null || o2.getPoints() == null) {
                            return -1;
                        } else {
                            return o2.getPoints().compareTo(o1.getPoints());
                        }

                    }
                });
            }
        } else {
            logger.info("User not part of any clan - " + userId);
        }
        return resps;
    }

    public List<ClanRes> getClansList(GetClansReq req) throws BadRequestException, NotFoundException {
        // if(req.getUserId() != null){
        // return getUsersClans(req.getUserId());
        // }
        List<ClanRes> resps = new ArrayList<>();
        Set<Long> userIds = new HashSet<>();
        List<String> clanIds = new ArrayList<>();
        List<Clan> clans = clanDAO.getClans(req);
        if (ArrayUtils.isNotEmpty(clans)) {
            logger.info("Number of clans - " + clans.size());
            for (Clan clan : clans) {
                ClanRes res = mapper.map(clan, ClanRes.class);
                res.setClanId(clan.getId());
                userIds.add(clan.getLeader());
                clanIds.add(clan.getId());
                resps.add(res);
            }
            Map<String, Long> clanPointMap = new HashMap<>();
            if (req.isAddPoints()) {
                clanPointMap = getClanPointsOverallMap(clanIds);
            }
            Map<String, List<UserMinimalInfo>> clanMemberMap = new HashMap<>();
            if (req.isAddMembers()) {
                clanMemberMap = getClanMembersMap(clanIds);
            }
            if (ArrayUtils.isNotEmpty(userIds)) {
                Map<Long, UserBasicInfo> usersmap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
                for (ClanRes res : resps) {
                    if (res.getLeader() != null && usersmap != null && usersmap.containsKey(res.getLeader())) {
                        res.setLeaderInfo(mapper.map(usersmap.get(res.getLeader()), UserMinimalInfo.class));
                    }
                    res.setPoints(clanPointMap.get(res.getClanId()));
                    res.setClanMembers(clanMemberMap.get(res.getClanId()));
                }
            }
        }
        return resps;
    }

    public List<UserMinimalInfo> getUserMinimalInfos(Collection<UserBasicInfo> users) {
        List<UserMinimalInfo> userInfos = new ArrayList<>();
        if (users != null) {
            for (UserBasicInfo user : users) {
                UserMinimalInfo userInfo = mapper.map(user, UserMinimalInfo.class);
                userInfos.add(userInfo);
            }
        }
        return userInfos;
    }

    // TODO :RELOOK
    public String generateClanShareCode(String title, Long userId) {
        String randomString = StringUtils.randomAlphaNumericStringWithLowerCase(6);
        if (StringUtils.isEmpty(title)) {
            title = "";
        }
        String stringSecret = DigestUtils.sha256Hex(randomString + userId + title);
        String refCode = "";
        title = title.replaceAll("\\s", "");

        if (title.length() < 3) {
            refCode = title.toLowerCase() + stringSecret.substring(3, 15 - title.length());
        } else {
            refCode = title.substring(0, 3).toLowerCase() + stringSecret.substring(3, 12);
        }
        return refCode;
    }

    public CreatePictureChallengeRes createPictureChallenge(CreatePictureChallengeReq req) throws BadRequestException {
        req.verify();
        PictureChallenge picChallenge = mapper.map(req, PictureChallenge.class);
        if (req.getStartTime() == null || req.getStartTime() < System.currentTimeMillis()) {
            picChallenge.setStatus(ChallengeStatus.ACTIVE);
        }
        createImageRevealBlockInfos(picChallenge);
        pictureChallengeDAO.savePictureChallenge(picChallenge);
        CreatePictureChallengeRes res = mapper.map(picChallenge, CreatePictureChallengeRes.class);
        return res;
    }

    private void createImageRevealBlockInfos(PictureChallenge pictureChallenge) throws BadRequestException {
        CloudStorageEntity imageDetails = pictureChallenge.getImage();
        InputStream inputStream = amazonS3Manager.getObject(imageDetails.getFileName(),
                UploadTarget.PICTURE_CHALLENGES);
        if (inputStream == null) {
            throw new BadRequestException(ErrorCode.PICTURE_CHALLENGE_IMAGE_NOT_FOUND, "image not found or corrupt");
        }
        try {
            BufferedImage bimg = ImageIO.read(inputStream);
            List<CloudStorageEntity> maskedImages = createAndUploadPictureChallegeFiles(bimg,
                    imageDetails.getFileNameWithoutExtension());
            pictureChallenge.setMaskedImages(maskedImages);
        } catch (Exception e) {
            logger.error("Error in creating imageRevealBlockInfos " + e.getMessage());
        }

    }

    private List<CloudStorageEntity> createAndUploadPictureChallegeFiles(BufferedImage bimg,
            String fileNameWithoutExtension) throws IOException {
        int imageWidth = bimg.getWidth();
        int imageHeight = bimg.getHeight();
        logger.info("imageWidth: " + imageWidth + ", imageHeight: " + imageHeight);
        double eachBlockWidth = imageWidth / IMAGE_PARTITION_COUNT;
        double eachBlockHeight = imageHeight / IMAGE_PARTITION_COUNT;
        logger.info(" eachBlockWidth " + eachBlockWidth + ", eachBlockHeight:" + eachBlockHeight);
        List<CloudStorageEntity> maskedImages = new ArrayList<>();
        List<ImageRevealBlockInfo> blocks = new ArrayList<>();
        for (double x = 0; x < imageWidth; x += eachBlockWidth) {
            for (double y = 0; y < imageHeight; y += eachBlockHeight) {
                if ((imageHeight - y < eachBlockHeight) || (imageWidth - x < eachBlockWidth)) {
                    continue;
                }
                double w = eachBlockWidth;
                if (imageWidth - (x + eachBlockWidth) < eachBlockWidth) {
                    w = imageWidth - x;
                }
                double x2 = x + w;
                double h = eachBlockHeight;
                if (imageHeight - (y + eachBlockHeight) < eachBlockHeight) {
                    h = imageHeight - y;
                }
                double y2 = y + h;
                ImageRevealBlockInfo info = new ImageRevealBlockInfo(x, y, x2, y2);
                blocks.add(info);
            }
        }
        logger.info("Total blocks count " + blocks.size());
        Collections.shuffle(blocks);
        int filecount = 1;
        for (double f = 1; f <= IMAGE_PARTITION_COUNT; f++) {
            BufferedImage bi = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
            Graphics2D ig2 = bi.createGraphics();
            int blockToReveal = (int) (f * blocks.size() / 100);
            for (int n = 0; n < blockToReveal; n++) {
                ImageRevealBlockInfo info = blocks.get(n);
                ig2.drawImage(bimg, info.getX1(), info.getY1(), info.getX2(), info.getY2(), info.getX1(), info.getY1(),
                        info.getX2(), info.getY2(), null);
            }
            File file = new File("/tmp/fileNameWithoutExtension_" + filecount + ".jpg");
            ImageIO.write(bi, "PNG", file);
            String fileName = fileNameWithoutExtension + java.io.File.separator + filecount + ".jpg";
            logger.info("uploading " + fileName);
            amazonS3Manager.uploadImage(fileName, file, null, UploadTarget.PICTURE_CHALLENGES);
            CloudStorageEntity entity = new CloudStorageEntity();
            entity.setFileName(fileName);
            maskedImages.add(entity);
            logger.info("uploaded file " + fileName);
            file.delete();
            ig2.dispose();
            filecount++;
        }
        return maskedImages;
    }

    public static void main(String[] args) throws IOException {
        BufferedImage bimg = ImageIO.read(new File("/Users/ajith/Downloads/picchallengedev2.jpg"));
        int imageWidth = bimg.getWidth();
        int imageHeight = bimg.getHeight();
        System.err.println(">> width " + imageWidth + ", height " + imageHeight);
        ClansManager clansManager = new ClansManager();
        clansManager.createAndUploadPictureChallegeFiles(bimg, "pichallenge");
    }

    public class ImageRevealBlockInfo {

        private double x1;
        private double y1;
        private double x2;
        private double y2;

        public ImageRevealBlockInfo() {
        }

        public ImageRevealBlockInfo(double x1, double y1, double x2, double y2) {
            this.x1 = x1;
            this.y1 = y1;
            this.x2 = x2;
            this.y2 = y2;
        }

        public int getX1() {
            return (int) x1;
        }

        public void setX1(double x1) {
            this.x1 = x1;
        }

        public int getY1() {
            return (int) y1;
        }

        public void setY1(double y1) {
            this.y1 = y1;
        }

        public int getX2() {
            return (int) x2;
        }

        public void setX2(double x2) {
            this.x2 = x2;
        }

        public int getY2() {
            return (int) y2;
        }

        public void setY2(double y2) {
            this.y2 = y2;
        }

        @Override
        public String toString() {
            return "ImageRevealBlockInfo{" + "x1=" + x1 + ", y1=" + y1 + ", x2=" + x2 + ", y2=" + y2 + '}';
        }

    }

    public UserDetailsInfo validateCategory(Long userId, String category) throws BadRequestException {
        String url = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT")
                + "/getUserDetailsForISLInfo?event=ISL_2017&userId=" + userId;
        logger.info("sending request to " + url);
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        String output = response.getEntity(String.class);

        UserDetailsInfo userDetails = new Gson().fromJson(output, UserDetailsInfo.class);
        if (userDetails == null || !category.equals(userDetails.getCategory())) {
            throw new BadRequestException(ErrorCode.CATEGORY_NOT_ALLOWED,
                    "category of student doesnot match allowed categories");
        }
        return userDetails;
    }

    public List<ClanLeaderBoardRes> getClanLeaderBoardForDuration(GetLeaderBoardForChallengeReq req) throws VException {
        req.verify();
        if (req.getStart() == null) {
            req.setStart(0);
        }
        if (req.getSize() == null) {
            req.setSize(10);
        }
        if (req.getStartTime() == null) {
            req.setStartTime(System.currentTimeMillis());
        }
        List<ClanLeaderBoardRes> leaderBoardRes = new ArrayList<>();
        Long startTime = userLeaderBoardManager.fetchStartTime(req.getType(), req.getStartTime());

        List<ClanLeaderBoard> leaders = clanLeaderBoardDAO.getClanLeaderBoardForCategory(req.getCategory(),
                req.getType(), startTime, req.getStart(), req.getSize());
        boolean partOfLeaderBoard = false;
        int rank = 1;
        List<String> clanIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(leaders)) {
            for (ClanLeaderBoard leader : leaders) {
                clanIds.add(leader.getClanId());
                if (StringUtils.isNotEmpty(req.getClanId()) && req.getClanId().equals(leader.getClanId())) {
                    partOfLeaderBoard = true;
                }
                ClanLeaderBoardRes res = mapper.map(leader, ClanLeaderBoardRes.class);
                res.setRank(rank++);
                leaderBoardRes.add(res);
            }
        }

        if (req.getAddMyRank() && !partOfLeaderBoard && StringUtils.isNotEmpty(req.getClanId())) {
            ClanLeaderBoardRes clanRe = getClanLeaderBoardForClanId(req.getClanId(), req.getCategory(), req.getType(),
                    startTime);
            if (clanRe != null) {
                clanIds.add(clanRe.getClanId());
                if (!leaderBoardRes.isEmpty()) {
                    leaderBoardRes.remove(leaderBoardRes.size() - 1);
                }
                leaderBoardRes.add(clanRe);
            }
        }

        Map<String, ClanRes> clanMap = getClanMap(Clan.Constants.ID, clanIds);

        if (ArrayUtils.isNotEmpty(leaderBoardRes)) {
            for (ClanLeaderBoardRes leader : leaderBoardRes) {
                leader.setClan(clanMap.get(leader.getClanId()));
            }
        }
        return leaderBoardRes;
    }

    public Map<String, ClanRes> getClanMap(String property, List<String> ids) {
        Query query = new Query();
        query.addCriteria(Criteria.where(property).in(ids));
        List<Clan> results = clanDAO.runQuery(query, Clan.class);
        Map<String, ClanRes> clanMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(results)) {
            for (Clan clan : results) {
                clanMap.put(clan.getId(), mapper.map(clan, ClanRes.class));
            }
        }
        return clanMap;
    }

    public ClanLeaderBoardRes getClanLeaderBoardForClanId(String clanId, String category, LeaderBoardType type,
            Long startTime) {
        ClanLeaderBoardRes resp = null;
        if (StringUtils.isNotEmpty(clanId) && type != null && category != null && startTime != null) {
            ClanLeaderBoard clanBoard = clanLeaderBoardDAO.getByClanId(clanId, type, startTime, category);
            if (clanBoard != null) {
                int rank = clanLeaderBoardDAO.getClanRank(category, type, startTime, clanBoard.getPoints());
                ClanLeaderBoardRes res = mapper.map(clanBoard, ClanLeaderBoardRes.class);
                res.setRank(rank);
                return res;
            }
        }
        return resp;
    }

    public ClanLeaderBoardRes getClanPointsOverall(String clanId) {
        ClanLeaderBoardRes resp = null;
        ClanLeaderBoard clanBoard = clanLeaderBoardDAO.getByClanId(clanId, LeaderBoardType.OVERALL,
                System.currentTimeMillis(), "DEFAULT");
        if (clanBoard != null) {
            resp = mapper.map(clanBoard, ClanLeaderBoardRes.class);
        } else {
            clanBoard = new ClanLeaderBoard(clanId, "DEFAULT", LeaderBoardType.OVERALL,
                    System.currentTimeMillis(), 0, 0, 0, 0);
            logger.info("ClanLeaderBoard not found for clanId : " + clanId);
        }
        return resp;
    }

    public Map<String, Long> getClanPointsOverallMap(List<String> clanIds) {
        Map<String, Long> clanPointsMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(clanIds)) {
            List<ClanLeaderBoard> clanBoards = clanLeaderBoardDAO.getByClanIds(clanIds, LeaderBoardType.OVERALL,
                    System.currentTimeMillis(), "DEFAULT");
            if (ArrayUtils.isNotEmpty(clanBoards)) {
                for (ClanLeaderBoard board : clanBoards) {
                    clanPointsMap.put(board.getClanId(), board.getPoints());
                }
            }

        }
        return clanPointsMap;
    }

    public Map<String, List<UserMinimalInfo>> getClanMembersMap(List<String> clanIds) {
        Map<String, List<Long>> memberLongMap = new HashMap<>();
        Map<String, List<UserMinimalInfo>> memberMap = new HashMap<>();
        Set<Long> userSet = new HashSet<>();
        if (ArrayUtils.isNotEmpty(clanIds)) {
            for (String clanId : clanIds) {
                List<Long> userIds = new ArrayList<>();
                List<ClanMember> clans = clanMemberDAO.getByClanId(clanId, 0, 6);
                if (ArrayUtils.isNotEmpty(clans)) {
                    for (ClanMember member : clans) {
                        userIds.add(member.getUserId());
                        userSet.add(member.getUserId());
                    }
                }
                memberLongMap.put(clanId, userIds);
            }
            Map<Long, UserBasicInfo> userBasicMap = fosUtils.getUserBasicInfosMapFromLongIds(userSet, false);
            Map<Long, UserMinimalInfo> userMap = getUserMinimalInfoMap(userBasicMap);

            for (String clanId : clanIds) {
                List<UserMinimalInfo> list = new ArrayList<>();
                if (memberLongMap.containsKey(clanId) && ArrayUtils.isNotEmpty(memberLongMap.get(clanId))) {
                    for (Long id : memberLongMap.get(clanId)) {
                        if (userMap.containsKey(id)) {
                            list.add(userMap.get(id));
                        }
                    }
                }
                memberMap.put(clanId, list);
            }
        }

        return memberMap;
    }

    public Map<Long, UserMinimalInfo> getUserMinimalInfoMap(Map<Long, UserBasicInfo> userMap) {
        Map<Long, UserMinimalInfo> userMinMap = new HashMap<>();
        if (!userMap.isEmpty()) {
            for (Long id : userMap.keySet()) {
                userMinMap.put(id, mapper.map(userMap.get(id), UserMinimalInfo.class));
            }
        }
        return userMinMap;
    }

    public GetPictureChallengesRes getPictureChallenges(GetPictureChallengesReq req) throws BadRequestException {
        req.verify();
        Set<String> attemptedChallengeIds = new HashSet<>();
        Map<String, PictureChallengeAttempt> _attemptMap = new HashMap<>();
        List<PictureChallengeRes> resps = new ArrayList<>();
        if (StringUtils.isNotEmpty(req.getClanId())) {
            List<PictureChallengeAttempt> pictureChallengeAttempts = new ArrayList<>();
            if (StringUtils.isNotEmpty(req.getPictureChallengeId())) {
                PictureChallengeAttempt att = pictureChallengeAttemptDAO.getByClanIdAndChallengeId(req.getClanId(), req.getPictureChallengeId());
                pictureChallengeAttempts.add(att);
            } else {
                pictureChallengeAttempts = pictureChallengeAttemptDAO.getByClanId(req.getClanId());
            }
            if (ArrayUtils.isNotEmpty(pictureChallengeAttempts)) {
                logger.info("Attempted Challenges for clanId : " + req.getClanId() + " size : " + pictureChallengeAttempts.size());
                for (PictureChallengeAttempt attempt : pictureChallengeAttempts) {
                    attemptedChallengeIds.add(attempt.getPictureChallengeId());
                    _attemptMap.put(attempt.getPictureChallengeId(), attempt);
                }
            }
        }
        List<String> _challengeIds = new ArrayList<>();
        List<PictureChallenge> pictureChallenges = new ArrayList<>();
        if (Boolean.TRUE.equals(req.getAttemptedOnly())) {
            pictureChallenges = pictureChallengeDAO.getPictureChallenges(req.getStart(), req.getSize(),
                    req.getStatus(), req.getSortOrder(), req.getOrderBy(), req.getDifficulty(),
                    attemptedChallengeIds, req.getCategory(), req.getPictureChallengeId());
        } else {
            pictureChallenges = pictureChallengeDAO.getPictureChallenges(req.getStart(), req.getSize(),
                    req.getStatus(), req.getSortOrder(), req.getOrderBy(), req.getDifficulty(),
                    null, req.getCategory(), req.getPictureChallengeId());
        }
        if (ArrayUtils.isNotEmpty(pictureChallenges)) {
            logger.info("Picture Challenge size : " + pictureChallenges.size());
            for (PictureChallenge challenge : pictureChallenges) {
                _challengeIds.add(challenge.getId());
            }
            Map<String, PictureChallengeLeaderBoardRes> _leaderBoardMap = new HashMap<>();
            if (StringUtils.isNotEmpty(req.getClanId()) && ArrayUtils.isNotEmpty(_challengeIds)) {
                List<PictureChallengeLeaderBoard> leaderBoards = pictureChallengeLeaderBoardDAO.getByClanIdAndChallengeIds(req.getClanId(), _challengeIds);
                if (ArrayUtils.isNotEmpty(leaderBoards)) {
                    for (PictureChallengeLeaderBoard leaderBoard : leaderBoards) {
                        _leaderBoardMap.put(leaderBoard.getPictureChallengeId(), mapper.map(leaderBoard, PictureChallengeLeaderBoardRes.class));
                    }
                }
            }

            for (PictureChallenge picChallenge : pictureChallenges) {
                logger.info("Return picChallengeId : " + picChallenge.getId());
                boolean lastDay = false;
                PictureChallengeRes res = mapper.map(picChallenge, PictureChallengeRes.class);
                if (ArrayUtils.isNotEmpty(picChallenge.getDayWisePointsToWin()) && picChallenge.getDayWisePointsToWin().get(0) != null) {
                    //Delete
                    res.setMaxPointsToWin(picChallenge.getDayWisePointsToWin().get(0).getPointsToWin());
                    PictureChallengeDayDeductions last = picChallenge.getDayWisePointsToWin().get(picChallenge.getDayWisePointsToWin().size() - 1);
                    if (last != null && last.getFromTime() < System.currentTimeMillis()) {
                        lastDay = true;
                    }
                }
                if (_attemptMap.containsKey(res.getId())) {
                    res.setAttemptInfo(_attemptMap.get(res.getId()));
                }
                if (_leaderBoardMap.containsKey(res.getId())) {
                    res.setPointsInfo(_leaderBoardMap.get(res.getId()));
                } else {
                    res.setPointsInfo(new PictureChallengeLeaderBoardRes());
                }
                int percentage = 0;
                if (lastDay && ArrayUtils.isNotEmpty(picChallenge.getMaskedImages())) {
                    res.setDisplayImage(picChallenge.getMaskedImages().get(picChallenge.getMaskedImages().size() - 1));
                    res.setPercentagePoints(100);
                } else {
                    if (picChallenge.getPointsToRevealFullImage() > 0) {
                        percentage = (int) res.getPointsInfo().getPoints() * 100 / picChallenge.getPointsToRevealFullImage();
                    }
                    if (percentage >= 1 && ArrayUtils.isNotEmpty(picChallenge.getMaskedImages())) {
                        logger.info("Display Image No: " + percentage);
                        if (percentage - 1 < picChallenge.getMaskedImages().size()) {
                            res.setDisplayImage(picChallenge.getMaskedImages().get(percentage - 1));
                            res.setPercentagePoints(percentage);
                        } else {
                            res.setDisplayImage(picChallenge.getMaskedImages().get(picChallenge.getMaskedImages().size() - 1));
                            res.setPercentagePoints(100);
                        }
                    } else {
                        res.setPercentagePoints(0);
                    }
                }

                //preparing the display url
                if (res.getDisplayImage() != null) {
                    String displayUrl = amazonS3Manager.getImageUrl(res.getDisplayImage().getFileName(),
                            UploadTarget.PICTURE_CHALLENGES);
                    res.getDisplayImage().setDisplayUrl(displayUrl);
                }
                if (ChallengeStatus.ENDED.equals(picChallenge.getStatus())) {
                    res.setSolutionString(picChallenge.getAnswerString());
                }
                resps.add(res);
            }
        }
        GetPictureChallengesRes resp = new GetPictureChallengesRes();
        resp.setList(resps);
        resp.setCount(resps.size());
        return resp;

    }

    public PictureChallengeAttempt submitPictureChallengeAttempt(SubmitPictureChallengeAnswerReq req) throws BadRequestException, InternalServerErrorException, ConflictException, NotFoundException, ForbiddenException {
        req.verify();
        PictureChallengeAttempt attempt = null;
        long cTime = System.currentTimeMillis();
        if (req.getToken() != null) {
            attempt = pictureChallengeAttemptDAO.getEntityById(req.getToken(), null, PictureChallengeAttempt.class);
            if (attempt == null) {
                throw new ConflictException(ErrorCode.INVALID_TOKEN, "no PictureChallengeAttempt found for token : " + req.getToken());
            }
        } else {
            attempt = pictureChallengeAttemptDAO.getByClanIdAndChallengeId(req.getClanId(), req.getPictureChallengeId());
        }

        if (attempt == null) {
            Clan clan = clanDAO.getById(req.getClanId());
            if (clan == null) {
                throw new NotFoundException(ErrorCode.CLAN_NOT_FOUND, "clan not found for clanId" + req.getClanId());
            }

            PictureChallenge challenge = pictureChallengeDAO.getById(req.getPictureChallengeId());

            if (challenge == null) {
                throw new NotFoundException(ErrorCode.CHALLENGE_NOT_FOUND, "challenge not found " + req.getPictureChallengeId());
            }
            if (!clan.getAllowedCategories().contains(challenge.getCategory())) {
                throw new BadRequestException(ErrorCode.CATEGORY_NOT_ALLOWED, "Clan category not allowed for this PictureChallenge");
            }
            if (!(ChallengeStatus.ACTIVE.equals(challenge.getStatus())
                    || (challenge.getStartTime() != null && cTime >= challenge.getStartTime()))) {
                throw new ForbiddenException(ErrorCode.CHALLENGE_NOT_ACTIVE, "challenge[" + req.getPictureChallengeId() + "] already ended");
            }

            if (ChallengeStatus.ENDED.equals(challenge.getStatus())
                    || (challenge.getBurstTime() != null && cTime + BUFFER_FOR_SUBMIT_ANSWER >= challenge.getBurstTime())) {
                throw new ForbiddenException(ErrorCode.CHALLENGE_ENDED, "challenge[" + req.getPictureChallengeId() + "] already ended");
            }

            attempt = new PictureChallengeAttempt(req.getClanId(), challenge.getId(), clan.getLeader(), challenge.getBurstTime(), challenge.getCategory());
        }
        if (!req.getUserId().equals(attempt.getUserId())) {
            throw new BadRequestException(ErrorCode.USER_NOT_LEADER, "User not leader of clan");
        }
        if (cTime + BUFFER_FOR_SUBMIT_ANSWER > attempt.getEndTime()) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_ENDED, "challenge[" + req.getPictureChallengeId() + "] already ended");
        }

        attempt.setAnswerTime(cTime);
        attempt.setAnswer(req.getAnswer());
        String callingUserId;
        if (req.getCallingUserId() != null) {
            callingUserId = req.getCallingUserId().toString();
        } else {
            callingUserId = req.getUserId().toString();
        }

        pictureChallengeAttemptDAO.create(attempt, callingUserId);
        return attempt;
    }

    public List<PictureChallengeLeaderBoardRes> getPictureChallengeLeaderBoard(String clanId, String pictureChallengeId, Integer start, Integer size, boolean addMyRank) throws BadRequestException {
        if (StringUtils.isEmpty(pictureChallengeId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "PictureChallengeId is null");
        }
        if (start == null) {
            start = 0;
        }
        if (size == null) {
            size = 20;
        }
        List<String> clanIds = new ArrayList<>();
        Set<Long> leaderIds = new HashSet<>();
        List<PictureChallengeLeaderBoardRes> leaderBoardResponse = new ArrayList<>();
        List<PictureChallengeLeaderBoard> leaderboards = pictureChallengeLeaderBoardDAO.getPictureChallengeLeaderBoardForChallenge(pictureChallengeId, start, size);
        boolean partOfLeaderBoard = false;
        if (ArrayUtils.isNotEmpty(leaderboards)) {
            int count = 1;
            for (PictureChallengeLeaderBoard leaderBoard : leaderboards) {
                clanIds.add(leaderBoard.getClanId());
                if (leaderBoard.getClanId().equals(clanId)) {
                    partOfLeaderBoard = true;
                }
                PictureChallengeLeaderBoardRes leader = mapper.map(leaderBoard, PictureChallengeLeaderBoardRes.class);
                leader.setRank(count + start);
                count++;
                leaderBoardResponse.add(leader);
            }
        }

        if (addMyRank && !partOfLeaderBoard && StringUtils.isNotEmpty(clanId)) {
            PictureChallengeLeaderBoard leader = pictureChallengeLeaderBoardDAO.getByClanId(clanId, pictureChallengeId);
            if (leader != null) {
                Long rank = pictureChallengeLeaderBoardDAO.getPictureChallengeRank(pictureChallengeId, leader.getPoints());
                if (rank != null) {
                    clanIds.add(clanId);
                    PictureChallengeLeaderBoardRes resp = mapper.map(leader, PictureChallengeLeaderBoardRes.class);
                    resp.setRank(rank.intValue());
                    leaderBoardResponse.add(resp);
                }
            }
        }
        Map<String, ClanRes> clanMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(clanIds)) {
            clanMap = getClanMap(Clan.Constants._ID, clanIds);
        }
        for (PictureChallengeLeaderBoardRes res : leaderBoardResponse) {
            res.setClan(clanMap.get(res.getClanId()));
        }

        return leaderBoardResponse;
    }

    public void markPictureChallengeActiveCron() {
        pictureChallengeDAO.markPictureChallengesActive();
    }

    public void endPictureChallengeCron() {
        List<PictureChallenge> challenges = pictureChallengeDAO.getBurstablePictureChallenges();
        if (ArrayUtils.isNotEmpty(challenges)) {
            int counter = 0;
            int threadSize = 4;
            Long time5 = new Long(DateTimeUtils.MILLIS_PER_MINUTE * 5);
            for (PictureChallenge challenge : challenges) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("pictureChallenge", challenge);
                Long timeDelay = time5 * (counter / threadSize);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.END_PICTURE_CHALLENGE, payload,
                        timeDelay);
                asyncTaskFactory.executeTask(params);
                counter++;
            }
        }
    }

    public void endPictureChallenge(String pictureChallengeId) throws ForbiddenException, NotFoundException, BadRequestException {
        if (StringUtils.isEmpty(pictureChallengeId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "ChallengeId is null");
        }
        PictureChallenge challenge = pictureChallengeDAO.getById(pictureChallengeId);
        if (challenge == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Challenge not found");
        }
        endPictureChallenge(challenge);
    }

    public void endPictureChallenge(PictureChallenge challenge) throws ForbiddenException, BadRequestException {
        if (challenge == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Challenge is null");
        }
        if (ChallengeStatus.ENDED.equals(challenge.getStatus())) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_ALREADY_PROCESSED, "challenge already processed ");
        }
        challenge.setStatus(ChallengeStatus.ENDED);
        pictureChallengeDAO.savePictureChallenge(challenge);
        modifyAnswers(challenge);
        boolean process = true;
        int start = 0;
        while (process) {
            List<PictureChallengeAttempt> challengeAttempts = pictureChallengeAttemptDAO.getByChallengeId(challenge.getId(),
                    start, ChallengeUtilCommon.DEFAULT_BATCH_SIZE);

            if (ArrayUtils.isNotEmpty(challengeAttempts)) {
                logger.info("challengeTakens to process " + challengeAttempts.size());
                for (PictureChallengeAttempt challengeTaken : challengeAttempts) {
                    try {
                        if (challengeTaken.processed) {
                            continue;
                        }
                        logger.info("marking " + challengeTaken + " processed");
                        challengeTaken.processed = true;

                        logger.info("checking  challengeTaken["
                                + challengeTaken.getId()
                                + "] status {attempted correctly or not}");
                        boolean isCorrect = judgePictureChallengeAnswer(challengeTaken.getAnswer(), challenge.getAnswer());
                        logger.info("user[" + challengeTaken.userId
                                + "] has given answer[" + challengeTaken.getAnswer()
                                + "] for challenge[" + challengeTaken.getPictureChallengeId()
                                + "] isCorrect: "
                                + isCorrect);

                        challengeTaken.success = isCorrect;

                        pictureChallengeAttemptDAO.create(challengeTaken, "SYSTEM");
                        logger.info("user[" + challengeTaken.userId
                                + "] over all challenge[" + challengeTaken.getPictureChallengeId()
                                + "] status : " + challengeTaken.success);
                    } catch (Exception e) {
                        logger.error("Error in marking the attempt "
                                + challengeTaken.getId() + " processed,  error " + e.getMessage());
                    }
                }
                start += challengeAttempts.size();
            } else {
                process = false;
            }
        }
        pictureChallengePointProcessor(challenge);
    }

    //Make queue
    private void pictureChallengePointProcessor(PictureChallenge challenge) {
        // Nov 12 . Stop calculating clan points with current categories
//		List<Clan> clans = clanDAO.getByCategories(challenge.getCategory());
//    	List<ClanMemberCount> clanIds = new ArrayList<>();
//    	if(ArrayUtils.isNotEmpty(clans)){
//    		logger.info("Clans size : " +clans.size());
//    		int counter = 0;
//    		for(Clan clan : clans){
//    			counter++;
//    			ClanMemberCount count = new ClanMemberCount();
//    			count.setClanId(clan.getId());
//    			count.setCount(clan.getMemberCount());
//    			clanIds.add(count);
//    			if(counter==clans.size() || counter %50 == 0){
//    				PictureChallengePointsIncrementReq req = new PictureChallengePointsIncrementReq();
//    				req.setPictureChallengeId(challenge.getId());
//    				req.setCategory(challenge.getCategory());
//    				req.setClans(clanIds);
//    				awsSQSManager.sendToSQS(SQSQueue.CHALLENGES_QUEUE, SQSMessageType.PICTURE_CHALLENGE_PROCESS, gson.toJson(req));
//    				clanIds = new ArrayList<>();
//    			}
//    		}
//    	}
    }

    public void processPictureChallengePoints(PictureChallengePointsIncrementReq req) {
        // Nov 12 . Stop calculating clan points with current categories
//		PictureChallenge picChallenge = pictureChallengeDAO.getById(req.getPictureChallengeId());
//		if(ArrayUtils.isNotEmpty(req.getClans())){
//			//calcuate points and add checks
//			for(ClanMemberCount clan : req.getClans()){
//				try{
//					ClanPointContextPojo pojo = new ClanPointContextPojo();
//	            	pojo.setContextType(ClanPointsContextType.PICTURE_CHALLENGE_BURST);
//					PictureChallengeAttempt attempt = pictureChallengeAttemptDAO.getByClanIdAndChallengeId(clan.getClanId(), picChallenge.getId());
//					if(attempt != null){
//						int picPoints = calculatePictureChallengePoints(attempt,picChallenge);
//						logger.info("attemptId : "+ attempt.getId() +"time[" + attempt.getAnswerTime()
//	                            + "] over all challenge[" + attempt.getPictureChallengeId()
//	                            + "] status : " + attempt.success
//	                            + "] points : " + picPoints);
//						if(picPoints>0){
//							attempt.setBasePoints(picPoints);
//							attempt.setTotalPoints(picPoints);
//							pictureChallengeAttemptDAO.update(attempt, "SYSTEM");
//						}
//						pojo.setPicChallengePoints(picPoints);
//					}
//					if(clan.getCount() != null){
//						int count = (clan.getCount() <= MAX_MUSCLE_POINT_COUNT ? clan.getCount() : MAX_MUSCLE_POINT_COUNT);
//						pojo.setMusclePoints(count * DEFAULT_MUSCLE_POINTS);
//					}
//					postChallengeProcessor.addPointsToClanLeaderBoard(clan.getClanId(), picChallenge.getId(), 
//							picChallenge.getCategory(), pojo, picChallenge);
//				}catch(Exception e){
//					logger.error("Error in marking the attempt "
//                            + clan.getClanId() + " processed,  error " + e.getMessage());
//				}
//			}
//			
//		}
    }

    public int calculatePictureChallengePoints(PictureChallengeAttempt attempt, PictureChallenge picChallenge) {
        int points = 0;
        if (attempt.success && attempt.getAnswerTime() != 0 && ArrayUtils.isNotEmpty(picChallenge.getDayWisePointsToWin())) {
            for (PictureChallengeDayDeductions day : picChallenge.getDayWisePointsToWin()) {
                if (attempt.getAnswerTime() <= day.getTillTime()) {
                    return day.getPointsToWin();
                }
            }
        }
        return points;
    }

    private boolean judgePictureChallengeAnswer(String answer, List<PictureChallengeAnswer> answerList) throws BadRequestException {

        if (StringUtils.isEmpty(answer)) {
            return false;
        }
        answer = answer.toLowerCase();
        String[] answerArray = answer.split("[^a-zA-Z0-9]+");

        if (answerArray == null || answerArray.length == 0) {
            return false;
        }
        if (ArrayUtils.isEmpty(answerList)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "answerMap is null");
        }
        int key = 0;
        int count = 0;
        for (PictureChallengeAnswer keyWord : answerList) {
            if (keyWord != null) {
                while (key < answerArray.length) {
                    if ((StringUtils.isNotEmpty(keyWord.getKeyword()) && keyWord.getKeyword().equalsIgnoreCase(answerArray[key].trim()))
                            || (ArrayUtils.isNotEmpty(keyWord.getVariations()) && keyWord.getVariations().contains(answerArray[key].trim()))) {
                        count++;
                        key++;
                        break;
                    }
                    key++;
                }
            }

        }
        return count == answerList.size();
    }

    private void modifyAnswers(PictureChallenge challenge) {
        if (challenge != null && ArrayUtils.isNotEmpty(challenge.getAnswer())) {
            for (PictureChallengeAnswer answer : challenge.getAnswer()) {
                if (StringUtils.isNotEmpty(answer.getKeyword())) {
                    answer.setKeyword(answer.getKeyword().toLowerCase().trim());
                }

                List<String> variations = new ArrayList<>();
                if (ArrayUtils.isNotEmpty(answer.getVariations())) {
                    for (String var : answer.getVariations()) {
                        if (StringUtils.isNotEmpty(var)) {
                            variations.add(var.toLowerCase().trim());
                        }

                    }
                    answer.setVariations(variations);
                }
            }
        }
    }
}
