package com.vedantu.cmds.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import java.net.MalformedURLException;
import java.util.*;

import com.vedantu.cmds.dao.*;
import com.vedantu.cmds.entities.*;
import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.enums.TestListType;
import com.vedantu.cmds.pojo.*;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.cmds.request.*;
import com.vedantu.cmds.response.*;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.UserType;
import com.vedantu.lms.cmds.pojo.*;
import com.vedantu.lms.cmds.request.DeleteTestReq;
import com.vedantu.moodle.pojo.request.GetTestContentReq;
import com.vedantu.moodle.response.HomepageTestResponse;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.subscription.request.GetBundleTestDetailsReq;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.dao.challenges.ChallengeAttemptDAO;
import com.vedantu.cmds.managers.challenges.PostChallengeProcessor;
import com.vedantu.cmds.dao.challenges.ChallengeDAO;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.enums.CMDSEntityType;
import com.vedantu.lms.cmds.enums.ChangeQuestionActionType;
import com.vedantu.cmds.fs.parser.QuestionRepositoryDocParser;
import com.vedantu.cmds.utils.CMDSCommonUtils;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.request.CMDSShareOTFContentsReq;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.ReprocessType;
import com.vedantu.moodle.pojo.PostChangeAnswerSQSPojo;
import com.vedantu.moodle.pojo.request.ChangeAnswerRequest;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.moodle.pojo.PostChangeQuestionSQSPojo;
import com.vedantu.moodle.pojo.SectionResultInfo;
import java.lang.reflect.Type;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.http.HttpMethod;

import static com.vedantu.lms.cmds.enums.ContentState.ATTEMPTED;
import static com.vedantu.lms.cmds.enums.ContentState.EVALUATED;

@Service
public class CMDSTestManager {

    private static final String DEFAULT_TESTMETADATA_KEY = "DEFAULT_CATEGORY_NAME";
    @Autowired
    private CMDSQuestionSetDAO questionSetDAO;

    @Autowired
    private CMDSQuestionDAO questionDAO;

    @Autowired
    private CMDSTestDAO testDAO;

    @Autowired
    private ChallengeDAO challengeDAO;

    @Autowired
    private ChallengeAttemptDAO challengeAttemptDAO;

    @Autowired
    private CurriculumDAO curriculumDAO;

    @Autowired
    private PostChallengeProcessor postChallengeProcessor;

    @Autowired
    private CMDSQuestionManager cMDSQuestionManager;

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private ContentInfoDAO contentInfoDAO;

    @Autowired
    private QuestionRepositoryDocParser questionRepositoryDocParser;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private CompetitiveTestStatsDAO competitiveTestStatsDAO;

    @Autowired
    private CMDSTestListDAO cmdsTestListDAO;

    @Autowired
    private CMDSTestDAO cmdsTestDAO;

    @Autowired
    private CMDSTestAttemptDAO cmdsTestAttemptDAO;

    private Logger logger = LogFactory.getLogger(CMDSTestManager.class);
    @Autowired
    private AwsSNSManager awsSNSManager;

    public final static Long CALCULATE_TEST_RANK_CRON_RUNTIME = 30 * Long.valueOf(DateTimeUtils.MILLIS_PER_MINUTE);
    public final static Long DEFAULT_DAYS_BEFORE_TEST_TO_BE_CONSIDERED = 30l;
    private static final String ENTITY_ID = "entityId";
    private static final String ENTITY_TYPE = "entityType";
    private static final String POST_ANSWER_CHANGE_GROUP_ID = "post_answer_change";
    private static final String POST_QUESTION_CHANGE_GROUP_ID = "post_question_change";
    private final String subscriptionEndpoint = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private static final Gson gson = new Gson();
    private static final int MAX_FETCH_SIZE_FOR_TESTS = 250;


    @Autowired
    private DozerBeanMapper mapper;

    public CMDSTest createCMDSTest(CreateTestReq cmdsTest) throws BadRequestException, VException {
        logger.info("Request : " + cmdsTest.toString());

        // Validate request
        cmdsTest.validate();

        // Cannot edit a not
        if (!StringUtils.isEmpty(cmdsTest.getId())) {
            throw new VException(ErrorCode.ACTION_NOT_ALLOWED, "CMDS content cannot be edited through this api");
        }

        if (StringUtils.isNotEmpty(cmdsTest.getCode())) {
            CMDSTest testByTestCode = testDAO.getTestByCode(cmdsTest.getCode());
            if (testByTestCode != null) {
                throw new ConflictException(ErrorCode.BAD_REQUEST_ERROR, "CMDS test with given code already exists");
            }
        }

        // Validate question set and version count
//		CMDSQuestionSet cmdsQuestionSet = questionSetDAO.getById(cmdsTest.getQuestionSetId());
//		if (cmdsQuestionSet == null) {
//			throw new VException(ErrorCode.CMDS_QUESTION_SET_NOT_FOUND,
//					"Question set not found for id : " + cmdsTest.toString());
//		}
//		if (!VedantuRecordState.ACTIVE.equals(cmdsQuestionSet.recordState)) {
//			throw new VException(ErrorCode.CMDS_QUESTION_SET_NOT_ACTIVE,
//					"CMDS question set not active : " + cmdsTest.toString());
//		}
//		if (cmdsTest.getVersionNo() != cmdsQuestionSet.calculateNextVersion()) {
//			throw new VException(ErrorCode.CMDS_CONTENT_INVALID_VERSION, "Versions :" + cmdsTest.getVersionNo()
//					+ " is invalid. Latest version of question set found is :" + cmdsQuestionSet.versionCount);
//		}
        // Populate indexes
        //CMDSCommonUtils.populateQuestionIndex(cmdsTest);
        // CMDS update question set version
//		cmdsQuestionSet.incrementVersion();
//		questionSetDAO.update(cmdsQuestionSet);
        Map<String, TestMetadata> metadata = new LinkedHashMap<>();
        List<String> addedQuestions = new ArrayList<>();
        List<String> questionIds = new ArrayList<>();
        Map<String, CMDSTestQuestion> testQuestionMap = new HashMap<>();
        for (CMDSTestQuestion _cmdsQuestionIdInfo : cmdsTest.getQuestions()) {
            questionIds.add(_cmdsQuestionIdInfo.getQuestionId());
            testQuestionMap.put(_cmdsQuestionIdInfo.getQuestionId(), _cmdsQuestionIdInfo);
        }

        List<CMDSQuestion> questions = questionDAO.getByIdsOrdered(questionIds, VedantuRecordState.ACTIVE);
        Map<String, CMDSQuestion> questionsMap = getQuestionsMap(questions);

        int judgeable = 0;
        int notJudgeable = 0;

        for (CMDSQuestion cmdsQuestion : questions) {
            TestMetadata _testMetadata;
            String metadataKey = DEFAULT_TESTMETADATA_KEY;
            //TODO: check if metadata can have null name in case of default
            if (StringUtils.isNotEmpty(cmdsQuestion.getSubject())) {
                metadataKey = TestMetadata.convertToSlug(cmdsQuestion.getSubject());
            }
            if (metadata.containsKey(metadataKey)) {
                _testMetadata = metadata.get(metadataKey);
            } else {
                _testMetadata = new TestMetadata();
                _testMetadata.setSlug(metadataKey);
                String subjectName = cmdsQuestion.getSubject();
                if (StringUtils.isEmpty(cmdsQuestion.getSubject())) {
                    subjectName = metadataKey;
                }
                _testMetadata.setBrdId(subjectName);
                _testMetadata.setName(subjectName);
                metadata.put(metadataKey, _testMetadata);
            }
            _testMetadata.getQuestions().add(testQuestionMap.get(cmdsQuestion.getId()));
            _testMetadata.setQusCount(_testMetadata.getQusCount() + 1);
            addedQuestions.add(cmdsQuestion.getId());

            if (cmdsQuestion.getType().isJudgeable()) {
                judgeable++;
            } else {
                notJudgeable++;
            }

        }

        ContentInfoType calculatedType;
        if (judgeable == 0 && notJudgeable != 0) {
            calculatedType = ContentInfoType.SUBJECTIVE;
        } else if (judgeable != 0 && notJudgeable == 0) {
            calculatedType = ContentInfoType.OBJECTIVE;
        } else {
            calculatedType = ContentInfoType.MIXED;
        }

        if (!calculatedType.equals(cmdsTest.getContentInfoType())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "ContentInfoType should be " + calculatedType+" but is "+cmdsTest.getContentInfoType());
        }

        List<TestMetadata> metadataPojos = new ArrayList<>(metadata.values());
        Float totalMarks = 0f;
        for (TestMetadata testMetadata : metadataPojos) {
            Float marksInSubject = 0f;
            Map<QuestionType, TestDetails> details = new HashMap<>();
            //creating testdetails testMetadata
            if (testMetadata.getQuestions() != null) {
                CMDSCommonUtils.populateQuestionIndex(testMetadata.getQuestions());
                for (CMDSTestQuestion cmdsTestQuestion : testMetadata.getQuestions()) {
                    if (questionsMap.containsKey(cmdsTestQuestion.getQuestionId())) {
                        CMDSQuestion cMDSQuestion = questionsMap.get(cmdsTestQuestion.getQuestionId());
                        if (cMDSQuestion != null) {
                            //creating testdetails testMetadata
                            QuestionType questionType = cMDSQuestion.getType();
                            if (questionType != null) {
                                if (!details.containsKey(questionType)) {
                                    //Marks marks = new Marks();
                                    //marks.positive = configUtils.getIntValue("marks.positive");
                                    //marks.negative = configUtils.getIntValue("marks.negative");
                                    details.put(questionType, new TestDetails(questionType, null));
                                }
                                TestDetails _details = details.get(questionType);
                                _details.setQusCount(_details.getQusCount() + 1);
                                _details.addQid(cmdsTestQuestion.getQuestionId());
                                marksInSubject += cmdsTestQuestion.getMarks().getPositive();
                            } else {
                                logger.error("question does not have question type " + cmdsTestQuestion.getQuestionId());
                            }
                        }
                    }
                }
            }
            List<TestDetails> testDetails = new ArrayList<>(details.values());
            testMetadata.setTotalMarks(marksInSubject);
            testMetadata.setDetails(testDetails);
            totalMarks += marksInSubject;
        }

        logger.info("metadataPojos " + metadataPojos);

        cmdsTest.setQusCount(addedQuestions.size());

        cmdsTest.setTotalMarks(totalMarks);
        cmdsTest.setMetadata(metadataPojos);
        //TODO: Add support for SELECTIVE Access level
        if (cmdsTest.getAccessLevel() == null) {
            cmdsTest.setAccessLevel(AccessLevel.PRIVATE);
        }

        //cmdsTest.setDuration(cmdsTest.getDuration());
        //cmdsTest.setCode(cmdsTest.getCode());
        //cmdsTest.setName(cmdsTest.getName());
        //cmdsTest.setReattemptAllowed(cmdsTest.isReattemptAllowed());
        //cmdsTest.setGlobalRankRequired(cmdsTest.isGlobalRankRequired());
        //cmdsTest.setMinStartTime(cmdsTest.getMinStartTime());
        //cmdsTest.setMaxStartTime(cmdsTest.getMaxStartTime());
        logger.info("saving " + cmdsTest);
        CMDSTest cmdsTestEntity = mapper.map(cmdsTest, CMDSTest.class);

        if (ArrayUtils.isNotEmpty(cmdsTestEntity.getTags()) && cmdsTestEntity.getTags().contains("KVPY")) {

            if (ArrayUtils.isEmpty(cmdsTestEntity.getMetadata())) {
                throw new BadRequestException(ErrorCode.SECTIONS_CANT_BE_ZERO, "For KVPY sections can't be zero");
            }

            List<CustomSectionEvaluationRule> customSectionEvaluationRules = new ArrayList<>();
            CustomSectionEvaluationRule customSectionEvaluationRulePart1 = new CustomSectionEvaluationRule();
            customSectionEvaluationRulePart1.setBestOf(3);
            customSectionEvaluationRulePart1.setTestMetadataKey(new HashSet<>());
            customSectionEvaluationRulePart1.getTestMetadataKey().add("Part-1-Biology");
            customSectionEvaluationRulePart1.getTestMetadataKey().add("Part-1-Physics");
            customSectionEvaluationRulePart1.getTestMetadataKey().add("Part-1-Chemistry");
            customSectionEvaluationRulePart1.getTestMetadataKey().add("Part-1-Mathematics");
            customSectionEvaluationRules.add(customSectionEvaluationRulePart1);

            CustomSectionEvaluationRule customSectionEvaluationRulePart2 = new CustomSectionEvaluationRule();
            customSectionEvaluationRulePart2.setBestOf(2);
            customSectionEvaluationRulePart2.setTestMetadataKey(new HashSet<>());
            customSectionEvaluationRulePart2.getTestMetadataKey().add("Part-2-Biology");
            customSectionEvaluationRulePart2.getTestMetadataKey().add("Part-2-Physics");
            customSectionEvaluationRulePart2.getTestMetadataKey().add("Part-2-Chemistry");
            customSectionEvaluationRulePart2.getTestMetadataKey().add("Part-2-Mathematics");
            customSectionEvaluationRules.add(customSectionEvaluationRulePart2);
            cmdsTestEntity.setCustomSectionEvaluationRules(customSectionEvaluationRules);
        }

        if (ArrayUtils.isNotEmpty(cmdsTestEntity.getCustomSectionEvaluationRules())) {
            cmdsTestEntity.setTotalMarks(0f);
            for (CustomSectionEvaluationRule customSectionEvaluationRule : cmdsTestEntity.getCustomSectionEvaluationRules()) {

                for (TestMetadata testMetadata : cmdsTestEntity.getMetadata()) {
                    if (customSectionEvaluationRule.getTestMetadataKey().contains(testMetadata.getName())) {
                        cmdsTestEntity.setTotalMarks(cmdsTestEntity.getTotalMarks() + customSectionEvaluationRule.getBestOf() * testMetadata.getTotalMarks());
                        break;
                    }
                }

            }

        }

        // Marking questions as USED = true
        for (CMDSQuestion question : questions) {
            Integer usedNo = question.getUsed();
            if (usedNo == null) {
                usedNo = 1;
            } else {
                usedNo = usedNo + 1;
            }
            question.setUsed(usedNo);
            questionDAO.save(question);
        }

        Set<String> tags = new HashSet<>();
        for (CMDSQuestion question : questions) {
            Set<String> aTags = question.getAnalysisTags();
            if (!ArrayUtils.isEmpty(aTags)) {
                tags.addAll(aTags);
            }
        }
        cmdsTestEntity.setAnalyticsTags(tags);

        logger.info("The test is created by {}",cmdsTestEntity.getCreatedBy());
        logger.info("The entire test element is {}",cmdsTestEntity);

        testDAO.update(cmdsTestEntity);

        logger.info("Updated entry : " + cmdsTestEntity.toString());
        return cmdsTestEntity;
    }

    @Deprecated
    public List<CMDSTestInfo> getCMDSTests(GetCMDSTestReq getCMDSTestReq) throws BadRequestException, VException {
        logger.info("Request : " + getCMDSTestReq.toString());
        List<CMDSTest> results = testDAO.getTests(getCMDSTestReq, null);

        List<CMDSTestInfo> response = new ArrayList<>();
        if (!CollectionUtils.isEmpty(results)) {
            for (CMDSTest test : results) {
                response.add(getTestInfo(test, false));
            }
        }

        logger.info("response : " + response.size());
        return response;
    }


    public CMDSTest getCMDSTestById(String testId) throws BadRequestException {
        logger.info("Request - getCMDSTestById: {}", testId);
        return testDAO.getById(testId);
    }

    public String generateCMDSTestLink(CMDSEntityType entityType, String id) throws BadRequestException, VException {
        String testId = getCMDSTestId(entityType, id);
        return ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT")
                + ConfigUtils.INSTANCE.getStringValue("cmds.test.url.prefix") + "/" + testId;
    }

    public String getCMDSTestId(CMDSEntityType entityType, String id) throws BadRequestException, VException {
        logger.info("Request : " + entityType + " id :" + id);
        if (StringUtils.isEmpty(id)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "id not found");
        }
        if (entityType == null) {
            entityType = CMDSEntityType.TEST;
        }

        switch (entityType) {
            case TEST:
                return id;
            case CMDSQUESTIONSET:
                // Fetch the latest test
                CMDSTest cmdsTest = getLatestCMDSTestFromQuestionSet(id);
                return cmdsTest.getId();
            default:
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "CMDS entity type not supported for type :" + entityType);
        }
    }

    public CMDSTest getLatestCMDSTestFromQuestionSet(String id) throws BadRequestException, VException {
        logger.info("Request : " + id);
        CMDSQuestionSet cmdsQuestionSet = questionSetDAO.getById(id);
        if (cmdsQuestionSet == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_SET_NOT_FOUND, "CMDS question set not found for id :" + id);
        }
        if (cmdsQuestionSet.versionCount < 1) {
            throw new VException(ErrorCode.CMDS_CONTENT_NOT_FOUND,
                    "Version number of question set is zero. No tests found for id :" + id + " cmdsQuestionSet :"
                            + cmdsQuestionSet.toString());
        }

        GetCMDSTestReq testReq = new GetCMDSTestReq();
        testReq.setQuestionSetId(id);
        testReq.setVersionNo(cmdsQuestionSet.versionCount);
        List<CMDSTest> results = testDAO.getTests(testReq, null);
        if (CollectionUtils.isEmpty(results)) {
            throw new VException(ErrorCode.CMDS_CONTENT_NOT_FOUND,
                    "content is not found :" + id + " cmdsQuestionSet :" + cmdsQuestionSet.toString());
        }

        if (results.size() > 1) {
            logger.error("Duplicate content found for same version no. Question set id : " + id + " version no. : "
                    + cmdsQuestionSet.versionCount);
        }
        return results.get(0);
    }

    public CMDSTestInfo getTestDetails(CMDSEntityType entityType, String entityId, String callingUserId,
                                       Role callingUserRole) throws BadRequestException, VException {
        logger.info("Request : " + entityType + " entityId : " + entityId + " callingUserId : " + callingUserId
                + " callingUserRole : " + callingUserRole);
        String testId = getCMDSTestId(entityType, entityId);
        CMDSTest cmdsTest = getCMDSTestById(testId);
        if (cmdsTest == null) {
            throw new NotFoundException(ErrorCode.CMDS_CONTENT_NOT_FOUND, "Test not found for id : " + testId);
        }
        boolean maskAnswers = false;
        if (Role.STUDENT.equals(callingUserRole)) {
            maskAnswers = true;
        }

        return getTestInfo(cmdsTest, maskAnswers);
    }

    public CMDSTestInfo getTestInfo(CMDSTest cmdsTest) {
        CMDSTestInfo cmdsTestInfo = new CMDSTestInfo(cmdsTest);
        return cmdsTestInfo;
    }

    public CMDSTestInfo getTestInfo(CMDSTest cmdsTest, boolean maskAnswers) throws BadRequestException, VException {
        CMDSTestInfo cmdsTestInfo = new CMDSTestInfo(cmdsTest);

        // Populate question with test marks
        if (!CollectionUtils.isEmpty(cmdsTest.getMetadata())) {
            for (TestMetadata testMetadata : cmdsTest.getMetadata()) {
                if (!CollectionUtils.isEmpty(testMetadata.getQuestions())) {
                    List<CMDSQuestion> questions = questionDAO.getByIds(CMDSCommonUtils.getQuestionIds(testMetadata.getQuestions()),
                            null);
                    Map<String, CMDSQuestion> questionMap = CMDSCommonUtils.getQuestionIdMap(questions);
                    for (CMDSTestQuestion testQuestion : testMetadata.getQuestions()) {
                        CMDSQuestion question = questionMap.get(testQuestion.getQuestionId());
                        if (maskAnswers && question.getSolutionInfo() != null) {
                            OptionFormat optionBody = question.getSolutionInfo().getOptionBody();
                            SolutionInfo solutionInfo = new SolutionInfo() {
                                @Override
                                public List<String> getAnswer() {
                                    return null;
                                }

                                @Override
                                public void setAnswer(List<String> answer) {
                                    //
                                }
                            };
                            solutionInfo.setOptionBody(optionBody);
                            question.setSolutionInfo(solutionInfo);
                        }
                        question.setMarks(testQuestion.getMarks());
                        cmdsTestInfo.addQuestion(question);
                    }
                }
            }
        }

        // Calculating the marks again as the questions are populated later on
        cmdsTestInfo.calculateTotalMarks();

        // Generate test link
        cmdsTestInfo.setTestUrl(generateCMDSTestLink(CMDSEntityType.TEST, cmdsTestInfo.getId()));

        // Generate AWS image urls
        questionRepositoryDocParser.populateAWSUrls(cmdsTestInfo.getQuestions());

        return cmdsTestInfo;
    }

    public String getCMDSTestIdByContentUrl(ContentInfo contentInfo)
            throws VException, MalformedURLException, InternalServerErrorException, BadRequestException {
        logger.info("Request : " + contentInfo.toString());
        String contentLink = contentInfo.getContentLink();
        if (StringUtils.isEmpty(contentLink)) {
            logger.error("CMDS Conent link is not found for content info : " + contentInfo.toString());
            throw new VException(ErrorCode.CMDS_CONTENT_INFO_LINK_NOT_FOUND,
                    "Content link is not found for CMDS event : " + contentInfo.toString());
        }

        return getCMDSTestIdByContentUrl(contentLink);
    }

    public String getCMDSTestIdByContentUrl(String contentLink)
            throws MalformedURLException, InternalServerErrorException, BadRequestException, VException {
        // Parse the url to figure out the question set or the test link
        Map<String, String> queryParams = CommonUtils.getParameterMapFromUrl(contentLink);
        if (!queryParams.containsKey(ENTITY_ID) || !queryParams.containsKey(ENTITY_TYPE)) {
            String errorMessage = "CMDS Entity id or entity type not found for url : " + contentLink;
            logger.error(errorMessage);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
        }

        String entityId = queryParams.get(ENTITY_ID);
        String entityType = queryParams.get(ENTITY_TYPE);

        CMDSEntityType cmdsEntityType = CMDSEntityType.valueOf(entityType);
        return getCMDSTestId(cmdsEntityType, entityId);
    }

    public void deleteTests(String questionSetId, String callingUserId) {
        logger.info("QuestionSet id : " + questionSetId + " callingUserId:" + callingUserId);
        testDAO.deleteTestsByQuestionSet(questionSetId, callingUserId);
    }

    public void deleteTestById(DeleteTestReq req) throws BadRequestException {
        String testId = req.getTestId();
        CMDSTest test = testDAO.getById(testId);
        Long callingUserId = req.getCallingUserId();
        if (callingUserId != null) {
            test.setLastUpdatedBy(callingUserId.toString());
        }
        logger.info("entry : " + test.toString());
        test.setLastUpdated(System.currentTimeMillis());
        test.setTestState(CMDSTestState.PRIVATE);
        testDAO.update(test);
        logger.info("entry updated with id : " + test.getId());
    }

    public void shareBulk(CMDSShareOTFContentsReq req) {
        List<UserBasicInfo> students = req.getStudents();
        List<CMDSShareContentinfo> shareInfos = req.getShareInfo();
        if (!CollectionUtils.isEmpty(shareInfos) && !CollectionUtils.isEmpty(shareInfos)) {
            for (CMDSShareContentinfo shareContentinfo : shareInfos) {
                String contentLink = shareContentinfo.getContentLink();
                UserBasicInfo teacher = shareContentinfo.getTeacherInfo();
                if ((com.vedantu.util.StringUtils.isNotEmpty(contentLink) || StringUtils.isNotEmpty(shareContentinfo.getTestId())) && teacher != null) {
                    try {
                        // Parse the url to figure out the question set or the
                        // test link
                        String testId = shareContentinfo.getTestId();
                        if (StringUtils.isEmpty(testId)) {
                            Map<String, String> queryParams = CommonUtils.getParameterMapFromUrl(contentLink);
                            if (!queryParams.containsKey(ENTITY_ID) || !queryParams.containsKey(ENTITY_TYPE)) {
                                String errorMessage = "CMDS Entity id or entity type not found for url : " + contentLink;
                                logger.error(errorMessage);
                                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
                            }

                            testId = queryParams.get(ENTITY_ID);
                        }
                        CMDSTest cmdsTest = getCMDSTestById(testId);
                        if (cmdsTest == null) {
                            logger.error("Test not found for id : " + testId + " req : " + req.toString());
                            continue;
                        }

                        List<String> studentIds = new ArrayList<>();
                        for (UserBasicInfo student : students) {
                            studentIds.add(student.getUserId().toString());
                        }
                        //TODO: Check about contextType and contextId here
                        List<ContentInfo> existingContentInfoList = contentInfoManager.getContentInfos(studentIds, teacher.getUserId().toString(), null, null, cmdsTest.getId());
                        List<String> alreadySharedStudents = new ArrayList<>();

                        if (existingContentInfoList != null) {
                            for (ContentInfo contentInfo : existingContentInfoList) {
                                alreadySharedStudents.add(contentInfo.getStudentId());
                            }
                        }

                        List<ContentInfo> contentInfoList = new ArrayList<>();

                        for (UserBasicInfo student : students) {
                            try {
                                if (!alreadySharedStudents.contains(student.getUserId().toString())) {
                                    EngagementType engagementType = null;
                                    if (req.getEngagementType() != null) {
                                        engagementType = EngagementType.valueOf(req.getEngagementType().name());
                                    }
                                    ContentInfo contentInfo = contentInfoManager.createCMDSContentInfo(UserType.REGISTERED, student.getUserId().toString(), student.getFullName(), teacher, contentLink, cmdsTest,
                                            req.getCourseName(), engagementType, req.getContextType(), req.getContextId());
                                    if (contentInfo != null) {
                                        contentInfoList.add(contentInfo);
                                    }
                                }
                            } catch (Exception ex) {
                                logger.error("Error creating contentInfo", ex);
                            }
                        }

                        if (CollectionUtils.isNotEmpty(contentInfoList)) {
                            contentInfoManager.insertContentInfos(contentInfoList);
                            if(Objects.nonNull(teacher.getUserId())) {
                                TeacherContentDashboardIncrementInfo info = new TeacherContentDashboardIncrementInfo(teacher.getUserId().toString(), cmdsTest.getContentInfoType(),ContentState.SHARED,contentInfoList.size());
                                awsSNSManager.triggerSNS(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE, SNSSubject.UPDATE_TEACHER_CONTENT_DASHBOARD_INFO.name(),gson.toJson(info));
                            }
//                            if (Boolean.TRUE.equals(req.getSendCommunication())) {
//                                for (ContentInfo contentInfo : contentInfoList) {
//                                    contentInfoManager.sendContentInfoNotifications(contentInfo, ContentInfoCommunicationType.SHARED, null);
//                                }
//                            }
                        } else {
                            logger.warn("contentInfoList is empty");
                        }

                    } catch (Exception ex) {
                        logger.error("Error with parsing the contetnt url : " + req.toString() + " contentLink : "
                                + contentLink, ex);
                        continue;
                    }

                }
            }
        }

    }

    private Map<String, CMDSQuestion> getQuestionsMap(List<CMDSQuestion> questions) {
        Map<String, CMDSQuestion> _map = new HashMap<>();
        if (questions != null) {
            for (CMDSQuestion cMDSQuestion : questions) {
                if (cMDSQuestion != null) {
                    _map.put(cMDSQuestion.getId(), cMDSQuestion);
                }
            }
        }
        return _map;
    }

    public Map<String, TestAndAttemptDetails> getTestAndAttemptDetails(GetBundleTestDetailsReq req) throws BadRequestException {
        List<CMDSTest> cmdsTests = testDAO.getTestByIds(req.getTestIds(),Arrays.asList(CMDSTest.Constants.QUS_COUNT,CMDSTest.Constants.DURATION,CMDSTest.Constants.REATTEMPT_ALLOWED));
        Map<String, TestAndAttemptDetails> responseMap=
                Optional.ofNullable(cmdsTests)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .collect(Collectors.toMap(
                                CMDSTest::getId,
                                test->new TestAndAttemptDetails(test.getQusCount(),test.isReattemptAllowed(),test.getDuration(),null)
                        ));

        if (Objects.nonNull(req.getUserId())) {
            List<ContentInfo> contentInfos = contentInfoManager.getContentInfosForUserAndContextId(req.getUserId().toString(), req.getTestIds(), req.getBundleId(), Collections.singletonList(ContentInfo.Constants.METADATA_TESTID));
            if (CollectionUtils.isNotEmpty(contentInfos)) {
                for (ContentInfo contentInfo : contentInfos) {
                    String testId = contentInfo.getMetadata().getTestId();
                    TestAndAttemptDetails testAndAttemptDetails = responseMap.get(testId);
                    if (Objects.nonNull(testAndAttemptDetails)) {
                        List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(), Collections.singletonList(CMDSTestAttempt.Constants.ATTEMPT_STATE));
                        if (CollectionUtils.isNotEmpty(testAttempts)) {
                            List<TestAttemptInfo> testAttemptInfos = new ArrayList<>();
                            testAndAttemptDetails.setAttemptDetails(testAttemptInfos);
                            for (CMDSTestAttempt cmdsTestAttempt : testAttempts) {
                                TestAttemptInfo testAttemptInfo = new TestAttemptInfo();
                                testAttemptInfo.setAttemptId(cmdsTestAttempt.getId());
                                testAttemptInfo.setAttemptState(cmdsTestAttempt.getAttemptState());
                                testAttemptInfos.add(testAttemptInfo);
                            }
                        }
                    }

                }
            }
        }

        return responseMap;

    }

    public List<ContentInfo> getTestAttempts(String testId, Integer start, Integer size) throws VException {
        return contentInfoDAO.getContentInfosUsingStart(testId, start, size);
    }

    public ContentInfo getContentInfoById(String contentInfoId) {
        logger.info("Request : " + contentInfoId);
        ContentInfo contentInfo = contentInfoDAO.getById(contentInfoId);
        return contentInfo;
    }

    public CMDSTestBasicInfo getTestBasicDetailsByContentInfoId(String contentInfoId)
            throws VException {
        ContentInfo contentInfo = getContentInfoById(contentInfoId);
        if (contentInfo == null) {
            throw new NotFoundException(ErrorCode.CMDS_CONTENT_INFO_NOT_FOUND, "Content info not found for id : " + contentInfoId);
        }
        if (ContentType.TEST.equals(contentInfo.getContentType()) || ContentType.ASSIGNMENT.equals(contentInfo.getContentType())) {
            TestContentInfoMetadata metadata = (TestContentInfoMetadata) contentInfo.getMetadata();
            if (metadata == null) {
                throw new NotFoundException(ErrorCode.INVALID_METADATA, "Meta data not found for contentInfoId : " + contentInfoId);
            }
            return getTestBasicDetails(metadata.getTestId());
        } else {
            throw new NotFoundException(ErrorCode.INVALID_CONTENT_TYPE, "This contentInfoId does not belongs to any test/assignment: " + contentInfoId);
        }
    }

    public CMDSTestBasicInfo getTestBasicDetails(String testId)
            throws VException {
        CMDSTest cmdsTest = getCMDSTestById(testId);
        if (cmdsTest == null) {
            throw new NotFoundException(ErrorCode.CMDS_CONTENT_NOT_FOUND, "Test not found for id : " + testId);
        }
        return mapper.map(cmdsTest, CMDSTestBasicInfo.class);
    }

    public List<CMDSTest> getTestsBasicDetails(List<String> testIds)
            throws VException {
        return getTestsByIds(testIds);
    }

    public List<ContentInfo> getEvaluatedAttempts(String testId, String userId) throws BadRequestException {
        List<ContentInfo> contentInfos = contentInfoDAO.getEvaluatedAttempts(testId, userId);
        return filterSelectedContentInfos(contentInfos);
    }

    private List<ContentInfo> filterSelectedContentInfos(List<ContentInfo> contentInfos) throws BadRequestException {
        for(ContentInfo contentInfo : contentInfos){
            Map<Long, CMDSTestAttempt> filterMap = new LinkedHashMap<>();
            TestContentInfoMetadata metadata = contentInfo.getMetadata();
            List<CMDSTestAttempt> attemptsForCurrentContentInfo = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),null);
            if(ArrayUtils.isNotEmpty(attemptsForCurrentContentInfo)) {
                for(CMDSTestAttempt currentAttempt : attemptsForCurrentContentInfo) {
                    if(Objects.nonNull(currentAttempt.getStartTime()) && filterMap.containsKey(currentAttempt.getStartTime())) {
                        CMDSTestAttempt storedTestAttempt = filterMap.get(currentAttempt.getStartTime());
                        if(Objects.isNull(storedTestAttempt.getEvaluatedTime()) || Objects.isNull(currentAttempt.getEvaluatedTime())) {
                            continue;
                        }
                        CMDSTestAttempt toBeSavedAttempt = (storedTestAttempt.getEvaluatedTime() > currentAttempt.getEvaluatedTime())  ? storedTestAttempt : currentAttempt;
                        filterMap.put(currentAttempt.getStartTime(), toBeSavedAttempt);
                    }
                    else {
                        filterMap.put(currentAttempt.getStartTime(), currentAttempt);
                    }
                }
                List<CMDSTestAttempt> testAttempts = new ArrayList<>(filterMap.values());
                metadata.setTestAttempts(testAttempts);
            }
        }
        return contentInfos;
    }

    public void calculateTestRanksAsync() {
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CALCULATE_TEST_RANKS, new HashMap<>());
        asyncTaskFactory.executeTask(params);
    }

    public void calculateTestRank(String testId) throws NotFoundException, BadRequestException {
        CMDSTest cmdsTest = getCMDSTestById(testId);
        if (cmdsTest == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, testId);
        }
        contentInfoManager.calculateTestRank(cmdsTest);
    }

    public void calculateTestRanks() {
        logger.info("calculateTestRanks");
        try {
            Long currentTime = System.currentTimeMillis();
            logger.info("calculateTestRanks starting at time : " + currentTime);
            currentTime = currentTime - currentTime % DateTimeUtils.MILLIS_PER_MINUTE;
            Long afterStartTime = currentTime - 3 * DateTimeUtils.MILLIS_PER_HOUR - CALCULATE_TEST_RANK_CRON_RUNTIME;
            Long beforeStartTime = afterStartTime + CALCULATE_TEST_RANK_CRON_RUNTIME - 1;
            logger.info("calculateTestRanks afterStartTime " + afterStartTime + " beforeStartTime " + beforeStartTime);
            List<CMDSTest> tests = testDAO.getObjectTestWithMaxStartTime(afterStartTime, beforeStartTime);
            if (ArrayUtils.isNotEmpty(tests)) {
                for (CMDSTest test : tests) {
                    try {
                        contentInfoManager.calculateTestRank(test);
                    } catch (Exception ex) {
                        logger.error("error in Creating rank for test: " + test + " error:" + ex.getMessage());
                    }
                }
            }
        } catch (Exception ex) {
            logger.error("error in calculateTestRanks " + ex.getMessage());
        }
    }

    public List<CMDSTest> getTestsWithQuestion(String questionId, Long time, Boolean beforeTime) {

        logger.info("Params: " + questionId + " beforeTime: " + time);


        if (time == null) {
            time = System.currentTimeMillis() - (DEFAULT_DAYS_BEFORE_TEST_TO_BE_CONSIDERED * DateTimeUtils.MILLIS_PER_DAY);
        }
        int start = 0;
        int size = 100;
        List<String> allTestIds = new ArrayList<>();

        while (true) {
            List<CMDSTest> results = testDAO.getTestsWithQuestionId(questionId, start, size, Collections.singletonList(CMDSTest.Constants._ID));

            if (ArrayUtils.isEmpty(results)) {
                break;
            }

            allTestIds.addAll(results.stream().map(CMDSTest::getId).collect(Collectors.toList()));

            start+=size;

        }

        List<String> testIds = new ArrayList<>();
        start=0;
        size=1000;
        while (true){
            List<String> intermediateTestIds = contentInfoDAO.getTestIdsForQuestion(allTestIds, time, beforeTime, start, size);
            if(ArrayUtils.isEmpty(intermediateTestIds)){
                break;
            }
            testIds.addAll(intermediateTestIds);
            start+=size;
        }

        return ArrayUtils.isEmpty(testIds)?new ArrayList<>():testDAO.getTestByIds(testIds);

    }

    public TestsForQuestionIdRes getTestsForChangeQuestion(String questionId, Long beforeTime) {
        if (beforeTime == null) {
            beforeTime = System.currentTimeMillis() - (DEFAULT_DAYS_BEFORE_TEST_TO_BE_CONSIDERED * DateTimeUtils.MILLIS_PER_DAY);
        }

        List<CMDSTest> cmdstests = new ArrayList<>();
        int start = 0;
        int size = 200;
        while (true) {
            List<CMDSTest> results = testDAO.getTestsWithQuestionId(questionId, start, size);
            if (ArrayUtils.isEmpty(results)) {
                break;
            }

            cmdstests.addAll(results);

            if (results.size() < size) {
                break;
            }

            start = start + size;

        }

        Set<String> questionIds = new HashSet<>();
        questionIds.add(questionId);

        for (CMDSTest cMDSTest : cmdstests) {
            if (ArrayUtils.isNotEmpty(cMDSTest.getMetadata())) {

                for (TestMetadata testMetadata : cMDSTest.getMetadata()) {

                    if (ArrayUtils.isNotEmpty(testMetadata.getQuestions())) {
                        for (CMDSTestQuestion cMDSTestQuestion : testMetadata.getQuestions()) {

                            if (questionId.equals(cMDSTestQuestion.getQuestionId())) {
                                for (QuestionChangeRecord questionChangeRecord : cMDSTestQuestion.getQuestionChangeRecords()) {
                                    questionIds.add(questionChangeRecord.getPrevQuestionId());
                                }
                                break;
                            }

                        }
                    }

                }

            }
        }

        List<CMDSQuestion> questions = questionDAO.getByIds(new ArrayList<String>(questionIds), null);

        TestsForQuestionIdRes testsForQuestionIdRes = new TestsForQuestionIdRes();

        testsForQuestionIdRes.setCmdsQuestions(questions);
        testsForQuestionIdRes.setCmdsTests(cmdstests);

        return testsForQuestionIdRes;

    }

    public List<Challenge> getChallengesWithQuestion(String questionId, Long beforeTime) {

        logger.info("Params: " + questionId + " beforeTime: " + beforeTime);


        if (beforeTime == null) {
            beforeTime = System.currentTimeMillis() - (DEFAULT_DAYS_BEFORE_TEST_TO_BE_CONSIDERED * DateTimeUtils.MILLIS_PER_DAY);
        }

        List<Challenge> challenges = challengeDAO.getChallengesByQuestionId(questionId);

        List<Challenge> results = new ArrayList<>();
        for (Challenge challenge : challenges) {
            long attemptsCount = challengeAttemptDAO.countAttemptsForChallengeId(challenge.getId(), beforeTime);

            if (attemptsCount > 0) {
                results.add(challenge);
            }

        }

        return results;

    }

    public void editAnswerFlow(ChangeAnswerRequest req) throws BadRequestException, NotFoundException, ForbiddenException {

        boolean questionUpdated = cMDSQuestionManager.changeAnswer(req);

        if (!questionUpdated) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Answer Change failed");
        }

        //List<String> testIds = contentInfoDAO.getTestIdsForQuestion(req.getQuestionId(), 1l);
        List<CMDSTest> cMDSTests = getTestsWithQuestion(req.getQuestionId(), 1l, false);
        List<String> testIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(cMDSTests)) {
            for (CMDSTest cMDSTest : cMDSTests) {
                testIds.add(cMDSTest.getId());
            }
        }
        Set<String> testIdsForReEvaluation = new HashSet<>(ArrayUtils.isEmpty(req.getTestIdsForReEvaluation())?new ArrayList<>():req.getTestIdsForReEvaluation());
        for (String testId : testIds) {
            PostChangeAnswerSQSPojo postChangeAnswerSQSPojo = new PostChangeAnswerSQSPojo(req.getQuestionId(),testId,false,testIdsForReEvaluation.contains(testId));
            awsSQSManager.sendToSQS(SQSQueue.POST_CHANGE_ANSWER_QUEUE, SQSMessageType.UPDATE_CHANGE_ANSWER_RESULT_ENTRIES_AND_REEVALUATE_TEST_IF_NECESSARY, gson.toJson(postChangeAnswerSQSPojo), testId);
        }

        if (ArrayUtils.isNotEmpty(req.getChallengeIdsForReEvaluation())) {
            for (String challengeId : req.getChallengeIdsForReEvaluation()) {
                postChallengeProcessor.reprocessChallenge(challengeId, true, ReprocessType.RECHECK);
            }
        }

    }

    private void updateCMDSTestWithNewQuestionId(CMDSTest cMDSTest, String prevQuestionId, String newQuestionId, QuestionChangeRecord questionChangeRecord) {

        for (TestMetadata testMetadata : cMDSTest.getMetadata()) {

            for (TestDetails testDetails : testMetadata.getDetails()) {

                if (testDetails.getQids().contains(prevQuestionId)) {
                    int questionIndex = testDetails.getQids().indexOf(prevQuestionId);
                    testDetails.getQids().set(questionIndex, newQuestionId);
                }

            }

            for (CMDSTestQuestion cMDSTestQuestion : testMetadata.getQuestions()) {

                if (cMDSTestQuestion.getQuestionId().equals(prevQuestionId)) {
                    cMDSTestQuestion.setQuestionId(newQuestionId);
                    if (cMDSTestQuestion.getQuestionChangeRecords() == null) {
                        cMDSTestQuestion.setQuestionChangeRecords(new ArrayList<>());
                    }
                    cMDSTestQuestion.setQuestionChangedTime(System.currentTimeMillis());
                    cMDSTestQuestion.getQuestionChangeRecords().add(questionChangeRecord);
                    if (ChangeQuestionActionType.GRACE_MARKS.equals(questionChangeRecord.getChangeQuestionActionType())) {
                        cMDSTestQuestion.setNoMarks(false);
                        cMDSTestQuestion.setGraceMarks(true);
                        break;
                    }

                    if (ChangeQuestionActionType.NO_MARKS.equals(questionChangeRecord.getChangeQuestionActionType())) {
                        cMDSTestQuestion.setGraceMarks(false);
                        cMDSTestQuestion.setNoMarks(true);
                        break;
                    }
                    cMDSTestQuestion.setGraceMarks(false);
                    cMDSTestQuestion.setNoMarks(false);
                }

            }


        }

        testDAO.update(cMDSTest);

    }

    private void updateCMDSTestWithNewQuestionMarks(CMDSTest cMDSTest, ChangeQuestionRequest req, QuestionChangeRecord questionChangeRecord) {

        String prevQuestionId = req.getQuestionId();
        for (TestMetadata testMetadata : cMDSTest.getMetadata()) {

            for (CMDSTestQuestion cMDSTestQuestion : testMetadata.getQuestions()) {

                if (cMDSTestQuestion.getQuestionId().equals(prevQuestionId)) {
                    if (cMDSTestQuestion.getQuestionChangeRecords() == null) {
                        cMDSTestQuestion.setQuestionChangeRecords(new ArrayList<>());
                    }
                    cMDSTestQuestion.setQuestionChangedTime(System.currentTimeMillis());

                    if (ChangeQuestionActionType.GRACE_MARKS.equals(questionChangeRecord.getChangeQuestionActionType())) {
                        cMDSTestQuestion.setNoMarks(false);
                        cMDSTestQuestion.setGraceMarks(true);
                        break;
                    }

                    if (ChangeQuestionActionType.NO_MARKS.equals(questionChangeRecord.getChangeQuestionActionType())) {
                        cMDSTestQuestion.setGraceMarks(false);
                        cMDSTestQuestion.setNoMarks(true);
                        break;
                    }

                    if (ChangeQuestionActionType.NO_ACTION.equals(questionChangeRecord.getChangeQuestionActionType())) {
                        cMDSTestQuestion.setGraceMarks(false);
                        cMDSTestQuestion.setNoMarks(false);
                        cMDSTestQuestion.setChangeMarks(true);
                        questionChangeRecord.setPositiveMarks(cMDSTestQuestion.getMarks().getPositive());
                        questionChangeRecord.setNegativeMarks(cMDSTestQuestion.getMarks().getNegative());
                        cMDSTestQuestion.getQuestionChangeRecords().add(questionChangeRecord);
                        cMDSTest.setTotalMarks(cMDSTest.getTotalMarks() - cMDSTestQuestion.getMarks().getPositive());
                        Marks marks = new Marks();
                        if(null != req.getPositiveMarks() && null != req.getNegativeMarks()){
                            marks.setPositive(req.getPositiveMarks());
                            marks.setNegative(req.getNegativeMarks());
                            cMDSTestQuestion.setMarks(marks);
                        }
                        cMDSTest.setTotalMarks(cMDSTest.getTotalMarks() + req.getPositiveMarks());

                        break;
                    }
                    cMDSTestQuestion.setGraceMarks(false);
                    cMDSTestQuestion.setNoMarks(false);
                }
            }

        }

        testDAO.update(cMDSTest);

    }

    public void addEvalLogic(AddEvalLogicRequest addEvalLogicRequest) throws ConflictException, BadRequestException {

        CMDSTest cMDSTest = testDAO.getById(addEvalLogicRequest.getTestId());

        if (ArrayUtils.isNotEmpty(cMDSTest.getCustomSectionEvaluationRules())) {
            throw new ConflictException(ErrorCode.EVAL_LOGIC_ALREADY_DEFINED, "Eval logic already defined for test");
        }

        cMDSTest.setCustomSectionEvaluationRules(addEvalLogicRequest.getCustomSectionEvaluationRules());

        testDAO.update(cMDSTest);

    }

    void processChangeQuestion(ChangeQuestionRequest req) {

        List<CMDSTest> cmdstests = new ArrayList<>();
        int start = 0;
        int size = 200;
        while (true) {
            List<CMDSTest> results = testDAO.getTestsWithQuestionId(req.getQuestionId(), start, size);
            if (ArrayUtils.isEmpty(results)) {
                break;
            }

            cmdstests.addAll(results);

            if (results.size() < size) {
                break;
            }

            start = start + size;


        }
        QuestionChangeRecord questionChangeRecord = new QuestionChangeRecord();
        questionChangeRecord.setNewQuestionId(req.getNewQuestionId());
        questionChangeRecord.setPrevQuestionId(req.getQuestionId());
        questionChangeRecord.setChangedAt(System.currentTimeMillis());
        questionChangeRecord.setChangedBy(req.getCallingUserId().toString());
        if (ArrayUtils.isNotEmpty(cmdstests)) {

            if(cmdstests.size()>50){
                logger.error("No of tests are exceeding the size of the required ones, need to optimize it");
            }

            for (CMDSTest cMDSTest : cmdstests) {
                String testId = cMDSTest.getId();
                logger.info("Updating for the test id {} and question id {}", testId,req.getNewQuestionId());

                updateCMDSTestWithNewQuestionId(cMDSTest, req.getQuestionId(), req.getNewQuestionId(), questionChangeRecord);

                awsSQSManager.sendToSQS(SQSQueue.POST_CHANGE_QUESTION_QUEUE, SQSMessageType.CHANGE_ANSWER_IN_TEST_RESULT_ENTRIES, gson.toJson(new PostChangeQuestionSQSPojo(testId,questionChangeRecord)), testId);

            }

        }

        logger.info("Checking if there is a question change actions {}",req.getChangeQuestionActions());
        for (ChangeQuestionAction changeQuestionAction : req.getChangeQuestionActions()) {

            if ((ChangeQuestionActionType.GRACE_MARKS.equals(changeQuestionAction.getChangeQuestionActionType()) || ChangeQuestionActionType.NO_MARKS.equals(changeQuestionAction.getChangeQuestionActionType()))
                    && ArrayUtils.isNotEmpty(req.getTestIdsForAction())) {
                for (String testId : req.getTestIdsForAction()) {
                    logger.info("Reevaluating for test id {}",testId);

                    questionChangeRecord.setPrevQuestionId(changeQuestionAction.getQuestionId());
                    questionChangeRecord.setChangeQuestionActionType(changeQuestionAction.getChangeQuestionActionType());

                    PostChangeQuestionSQSPojo postChangeQuestionSQSPojo = new PostChangeQuestionSQSPojo(testId,questionChangeRecord,changeQuestionAction.getChangeQuestionActionType(),false);
                    awsSQSManager.sendToSQS(SQSQueue.POST_CHANGE_QUESTION_QUEUE, SQSMessageType.REEVALUATE_TESTS, gson.toJson(postChangeQuestionSQSPojo), testId);

                }
            }
        }

    }

    private UserTestResultPojo getUserTestResultForTest(List<CMDSTestAttempt> testAttempts, CMDSTest cmdsTest) {
        UserTestResultPojo userTestResultPojo = new UserTestResultPojo();
        for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {

            if (cmdsTest != null && cmdsTest.getMaxStartTime() != null && (cMDSTestAttempt.getStartedAt() > cmdsTest.getMaxStartTime())) {
                continue;
            }

            if (userTestResultPojo.getMarksAchieved() == null) {
                userTestResultPojo.setMarksAchieved(cMDSTestAttempt.getMarksAcheived());
                List<TestSectionResult> testSectionResults = new ArrayList<>();
                if (ArrayUtils.isNotEmpty(cMDSTestAttempt.getCategoryAnalyticsList())) {
                    for (CategoryAnalytics categoryAnalytics : cMDSTestAttempt.getCategoryAnalyticsList()) {
                        TestSectionResult testSectionResult = new TestSectionResult();
                        testSectionResult.setMarksAchieved(categoryAnalytics.getMarks());
                        testSectionResult.setSectionName(categoryAnalytics.getName());
                        testSectionResult.setTotalMarks(categoryAnalytics.getMaxMarks());
                        testSectionResult.setCorrect(categoryAnalytics.getCorrect());
                        testSectionResult.setTotalQuestions(categoryAnalytics.getUnattempted() + categoryAnalytics.getAttempted());
                        testSectionResult.setAttemptedQuestion(categoryAnalytics.getAttempted());
                        testSectionResult.setUnAttemptedQuestion(categoryAnalytics.getUnattempted());
                        testSectionResult.setInCorrect(categoryAnalytics.getIncorrect());
                        testSectionResults.add(testSectionResult);
                    }
                }
                userTestResultPojo.setTotalMarks(cMDSTestAttempt.getTotalMarks());
                userTestResultPojo.setAttemptedQuestion(cMDSTestAttempt.getAttempted());
                userTestResultPojo.setUnAttemptedQuestion(cMDSTestAttempt.getUnattempted());
                userTestResultPojo.setTotalQuestions(cMDSTestAttempt.getAttempted() + cMDSTestAttempt.getUnattempted());
                userTestResultPojo.setCorrect(cMDSTestAttempt.getCorrect());
                userTestResultPojo.setInCorrect(cMDSTestAttempt.getIncorrect());
                userTestResultPojo.setTestSectionResults(testSectionResults);
                userTestResultPojo.setTime(cMDSTestAttempt.getStartedAt());
            }

            if (cMDSTestAttempt.getMarksAcheived() > userTestResultPojo.getMarksAchieved()) {
                userTestResultPojo.setMarksAchieved(cMDSTestAttempt.getMarksAcheived());
                List<TestSectionResult> testSectionResults = new ArrayList<>();
                if (ArrayUtils.isNotEmpty(cMDSTestAttempt.getCategoryAnalyticsList())) {
                    for (CategoryAnalytics categoryAnalytics : cMDSTestAttempt.getCategoryAnalyticsList()) {
                        TestSectionResult testSectionResult = new TestSectionResult();
                        testSectionResult.setMarksAchieved(categoryAnalytics.getMarks());
                        testSectionResult.setSectionName(categoryAnalytics.getName());
                        testSectionResult.setTotalMarks(categoryAnalytics.getMaxMarks());
                        testSectionResult.setCorrect(categoryAnalytics.getCorrect());
                        testSectionResult.setTotalQuestions(categoryAnalytics.getUnattempted() + categoryAnalytics.getAttempted());
                        testSectionResult.setAttemptedQuestion(categoryAnalytics.getAttempted());
                        testSectionResult.setUnAttemptedQuestion(categoryAnalytics.getUnattempted());
                        testSectionResult.setInCorrect(categoryAnalytics.getIncorrect());
                        testSectionResults.add(testSectionResult);
                    }
                }
                userTestResultPojo.setTotalMarks(cMDSTestAttempt.getTotalMarks());
                userTestResultPojo.setAttemptedQuestion(cMDSTestAttempt.getAttempted());
                userTestResultPojo.setUnAttemptedQuestion(cMDSTestAttempt.getUnattempted());
                userTestResultPojo.setTotalQuestions(cMDSTestAttempt.getAttempted() + cMDSTestAttempt.getUnattempted());
                userTestResultPojo.setCorrect(cMDSTestAttempt.getCorrect());
                userTestResultPojo.setInCorrect(cMDSTestAttempt.getIncorrect());
                userTestResultPojo.setTestSectionResults(testSectionResults);
                userTestResultPojo.setTime(cMDSTestAttempt.getStartedAt());
            }
            userTestResultPojo.setAttempted(true);
        }
        if (userTestResultPojo.getTime() == null && cmdsTest != null) {
            userTestResultPojo.setTime(cmdsTest.getMinStartTime());
        }
        logger.info(userTestResultPojo);

        return userTestResultPojo;
    }

    public List<UserTestResultPojo> getUserTestResultPojos(List<String> batchIds, String userId) throws BadRequestException {
        logger.info("In CMDSTestManager getUserTestResultPojos");
        List<ContentInfo> contentInfos = contentInfoDAO.getTestInfoForBatches(batchIds, userId);

        if (ArrayUtils.isEmpty(contentInfos)) {
            return new ArrayList<>();
        }


        Map<String, UserTestResultPojo> testResultMap = new HashMap<>();
        Set<String> testIds = new HashSet<>();
        Map<String, CMDSTest> testMap = new HashMap<>();


        for (ContentInfo contentInfo : contentInfos) {
            TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) contentInfo.getMetadata();

            if (testContentInfoMetadata == null) {
                continue;
            }
            testIds.add(testContentInfoMetadata.getTestId());
        }
        List<CMDSTest> tests = testDAO.getTestByIds(new ArrayList<>(testIds));
        List<CompetitiveTestStats> competitiveTestStatses = competitiveTestStatsDAO.getCompetitiveTestStatses(new ArrayList<>(testIds));
        for (CMDSTest cMDSTest : tests) {
            testMap.put(cMDSTest.getId(), cMDSTest);
        }

        Map<String, CompetitiveTestStats> testStatsMap = new HashMap<>();

        if (ArrayUtils.isNotEmpty(competitiveTestStatses)) {
            for (CompetitiveTestStats competitiveTestStats : competitiveTestStatses) {
                testStatsMap.put(competitiveTestStats.getCmdsTestId(), competitiveTestStats);
            }
        }


        contentInfos.sort(Comparator.comparing(AbstractMongoEntity::getCreationTime));

        Long lastUnitTime = null;
        for (ContentInfo contentInfo : contentInfos) {
            TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) contentInfo.getMetadata();

            if (testContentInfoMetadata == null) {
                continue;
            }

            if (ContentInfoType.SUBJECTIVE.equals(contentInfo.getContentInfoType()) && ATTEMPTED.equals(contentInfo.getContentState())) {
                continue;
            }

            CMDSTest test = testMap.get(testContentInfoMetadata.getTestId());
            updateTestResultMap(contentInfo, testContentInfoMetadata, testResultMap, lastUnitTime, testMap);

            if (EnumBasket.TestTagType.PHASE.equals(test.getTestTag()) ||
                    EnumBasket.TestTagType.UNIT.equals(test.getTestTag())) {
                if (contentInfo.getAttemptedTime() != null) {
                    lastUnitTime = contentInfo.getAttemptedTime();
                } else {
                    lastUnitTime = contentInfo.getCreationTime();
                }
            }
            logger.info(testResultMap);

        }

        List<UserTestResultPojo> result = new ArrayList<>();
        for (CMDSTest test : tests) {
            if (testResultMap.containsKey(test.getId())) {
                UserTestResultPojo userTestResultPojo = testResultMap.get(test.getId());

                userTestResultPojo.setTag(test.getTestTag());
                userTestResultPojo.setTestName(test.getName());
                logger.info(userTestResultPojo);
                if (EnumBasket.TestTagType.PHASE.equals(test.getTestTag()) || EnumBasket.TestTagType.UNIT.equals(test.getTestTag())) {
                    CompetitiveTestStats competitiveTestStats = testStatsMap.get(test.getId());
                    if (competitiveTestStats != null) {
                        userTestResultPojo.setzScore(competitiveTestStats.getzScore());
                        userTestResultPojo.setAverageMarks(((float) (Math.round(competitiveTestStats.getAverageScore() * 100))) / 100);
                        if (ArrayUtils.isNotEmpty(competitiveTestStats.getUserCompetitiveTestInfos())) {
                            for (UserCompetitiveTestInfo userCompetitiveTestInfo : competitiveTestStats.getUserCompetitiveTestInfos()) {
                                if (userCompetitiveTestInfo.getUserId().equals(userId)) {
                                    userTestResultPojo.setPercentile(((float) (Math.round(userCompetitiveTestInfo.getPercentile() * 100))) / 100);
                                }
                            }
                        }
                    }
                }

                result.add(userTestResultPojo);

            }
        }

        result.sort(Comparator.comparing(UserTestResultPojo::getTime));

        logger.info("Results of getUserTestResultPojos " + result.toString());
        return result;
    }

    private void updateTestResultMap(ContentInfo contentInfo, TestContentInfoMetadata testContentInfoMetadata, Map<String, UserTestResultPojo> testResultMap, Long lastUnitTime, Map<String, CMDSTest> testMap) throws BadRequestException {

        CMDSTest test = testMap.get(testContentInfoMetadata.getTestId());
        List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),null);
        if (testResultMap.containsKey(testContentInfoMetadata.getTestId())) {
            UserTestResultPojo originalUserTestResultPojo = testResultMap.get(testContentInfoMetadata.getTestId());
            UserTestResultPojo userTestResultPojo = new UserTestResultPojo();

            if (lastUnitTime != null && originalUserTestResultPojo.getTime() < lastUnitTime && (!(EnumBasket.TestTagType.PHASE.equals(test.getTestTag()) ||
                    EnumBasket.TestTagType.UNIT.equals(test.getTestTag())))) {
                return;
            }

            if (ArrayUtils.isNotEmpty(testAttempts)) {
                userTestResultPojo = getUserTestResultForTest(testAttempts, testMap.get(testContentInfoMetadata.getTestId()));
            } else {
                userTestResultPojo.setAttempted(false);
                userTestResultPojo.setTime(contentInfo.getCreationTime());
            }
            userTestResultPojo.setBatchId(contentInfo.getContextId());
            if (originalUserTestResultPojo.isAttempted() && userTestResultPojo.isAttempted() && originalUserTestResultPojo.getMarksAchieved() < userTestResultPojo.getMarksAchieved()) {
                userTestResultPojo.setBatchId(contentInfo.getContextId());
                testResultMap.put(testContentInfoMetadata.getTestId(), userTestResultPojo);
            } else if (!originalUserTestResultPojo.isAttempted()) {
                userTestResultPojo.setBatchId(contentInfo.getContextId());
                testResultMap.put(testContentInfoMetadata.getTestId(), userTestResultPojo);
            }

        } else {
            UserTestResultPojo userTestResultPojo = new UserTestResultPojo();
            if (ArrayUtils.isNotEmpty(testAttempts)) {
                userTestResultPojo = getUserTestResultForTest(testAttempts, testMap.get(testContentInfoMetadata.getTestId()));
            } else {
                userTestResultPojo.setAttempted(false);
                userTestResultPojo.setTime(contentInfo.getCreationTime());
            }
            userTestResultPojo.setBatchId(contentInfo.getContextId());
            testResultMap.put(testContentInfoMetadata.getTestId(), userTestResultPojo);

        }


    }

    private Float marksAchievedInUnitTest(List<CMDSTestAttempt> testAttempts, CMDSTest cmdsTest) {

        if (ArrayUtils.isEmpty(testAttempts)) {
            return 0f;
        }

        float maxMarks = testAttempts.get(0).getMarksAcheived();
        for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {

            if(Objects.isNull(cMDSTestAttempt.getStartedAt())) {
                continue;
            }
            if (cMDSTestAttempt.getStartedAt() <= cmdsTest.getMaxStartTime() && cMDSTestAttempt.getStartedAt() >= cmdsTest.getMinStartTime()) {

                if (maxMarks < cMDSTestAttempt.getMarksAcheived()) {
                    maxMarks = cMDSTestAttempt.getMarksAcheived();
                }
            }
        }

        return maxMarks;
    }

    public Float getAverageMarks(List<Float> marks) {

        if (ArrayUtils.isEmpty(marks)) {
            return 0f;
        }
        float sum = 0f;
        int count = 0;

        for (Float mark : marks) {
            count++;
            sum = sum + mark;
        }
        return (sum / count);
    }

    public Integer getNumOfMarksGreaterThan(List<Float> marks, Float thresholMark) {

        if (ArrayUtils.isEmpty(marks)) {
            return 0;
        }

        int count = 0;

        for (Float studentMark : marks) {
            if (studentMark >= thresholMark) {
                count++;
            }
        }

        return count;
    }

    public Float getPercentile(List<Float> marks, double percentile) {
        int index = (int) Math.ceil(percentile * (double) marks.size() / 100);
        return marks.get(index - 1);

    }

    public Float getZScore(List<Float> marks) {

        if (ArrayUtils.isEmpty(marks)) {
            return 0f;
        }

        Collections.sort(marks);
        Float seventyPercentile = getPercentile(marks, 70);
        Float ninetyPercentile = getPercentile(marks, 90);
        float sum = 0f;
        int count = 0;
        for (Float studentMark : marks) {
            if (studentMark >= seventyPercentile && studentMark <= ninetyPercentile) {
                sum = sum + studentMark;
                count++;
            }
        }
        return (sum / count);
    }

    public List<AMTestStats> getAMTestStatsForGroup(String groupName, String amId) throws VException {


        String url = subscriptionEndpoint + "/batch/getBatchForAM?groupName=" + WebUtils.getUrlEncodedValue(groupName) + "&amId=" + amId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type batchIdsListType = new TypeToken<ArrayList<String>>() {
        }.getType();
        List<String> batchIds = gson.fromJson(jsonString, batchIdsListType);
        if (ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }
        List<Curriculum> curriculums = curriculumDAO.getTestForBatchIds(batchIds);

        Set<String> testIds = new HashSet<>();

        if (ArrayUtils.isEmpty(curriculums)) {
            return new ArrayList<>();
        }

        for (Curriculum curriculum : curriculums) {
            if (ArrayUtils.isNotEmpty(curriculum.getContents())) {

                for (com.vedantu.lms.cmds.pojo.ContentInfo contentInfo : curriculum.getContents()) {

                    if (ContentType.TEST.equals(contentInfo.getContentType())) {

                        testIds.add(contentInfo.getContentId());

                    }

                }

            }
        }

        List<CMDSTest> cMDSTests = testDAO.getPhaseTestsWithQuestionId(new ArrayList<>(testIds));

        if (ArrayUtils.isEmpty(cMDSTests)) {
            return new ArrayList<>();
        }
        testIds = new HashSet<>();
        Map<String, CMDSTest> cmdsTestMap = new HashMap<>();
        for (CMDSTest cMDSTest : cMDSTests) {
            testIds.add(cMDSTest.getId());
            cmdsTestMap.put(cMDSTest.getId(), cMDSTest);
        }

        Map<String, List<ContentInfo>> testMap = new HashMap<>();

        List<ContentInfo> contentInfos = new ArrayList<>();

        int start = 0;
        int size = 100;
        while (true) {

            List<ContentInfo> tempContents = contentInfoDAO.getContentInfosForPhaseTests(batchIds, new ArrayList<>(testIds), start, size);

            if (ArrayUtils.isEmpty(tempContents)) {
                break;
            }

            contentInfos.addAll(tempContents);

            if (tempContents.size() < size) {
                break;
            }
            start = start + size;

        }

        if (ArrayUtils.isEmpty(contentInfos)) {
            return new ArrayList<>();
        }

        for (ContentInfo contentInfo : contentInfos) {
            TestContentInfoMetadata metadata = (TestContentInfoMetadata) contentInfo.getMetadata();
            if (!testMap.containsKey(metadata.getTestId())) {
                testMap.put(metadata.getTestId(), new ArrayList<>());
            }
            testMap.get(metadata.getTestId()).add(contentInfo);
        }
        List<AMTestStats> aMTestStatses = new ArrayList<>();
        for (String testId : testIds) {
            AMTestStats aMTestStats = new AMTestStats();
            aMTestStats.setGroupName(groupName);
            aMTestStats.setTestId(testId);
            aMTestStats.setTestName(cmdsTestMap.get(testId).getName());
            if (!testMap.containsKey(testId)) {
                continue;
            }
            CMDSTest test = cmdsTestMap.get(testId);
            Map<String, Float> userContentInfoMap = new HashMap<>();
            List<ContentInfo> attemptedTests = testMap.get(testId);
            for (ContentInfo contentInfo : attemptedTests) {
                List<CMDSTestAttempt> attempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(), Arrays.asList(CMDSTestAttempt.Constants.STARTED_AT, CMDSTestAttempt.Constants.MARKS_ACHIEVED));
                if (userContentInfoMap.containsKey(contentInfo.getStudentId())) {
                    if (userContentInfoMap.get(contentInfo.getStudentId()) < marksAchievedInUnitTest(attempts, test)) {
                        userContentInfoMap.put(contentInfo.getStudentId(), marksAchievedInUnitTest(attempts, test));
                    }
                } else {
                    userContentInfoMap.put(contentInfo.getStudentId(), marksAchievedInUnitTest(attempts, test));
                }
            }

            List<Float> marksList = new ArrayList<>();

            for (Map.Entry<String, Float> entry : userContentInfoMap.entrySet()) {
                marksList.add(entry.getValue());
            }
            Float totalMarks = cmdsTestMap.get(testId).getTotalMarks();
            aMTestStats.setAverageScore(getAverageMarks(marksList));
            aMTestStats.setNumOfStudentsAttempted(marksList.size());
            aMTestStats.setNumOfStudentsAbove65(getNumOfMarksGreaterThan(marksList, totalMarks * 0.65f));
            aMTestStats.setNumOfStudentsAbove75(getNumOfMarksGreaterThan(marksList, totalMarks * 0.75f));
            aMTestStats.setGroupName(groupName);
            aMTestStats.setzScore(getZScore(marksList));
            aMTestStats.setNumOfStudentsAboveZScore(getNumOfMarksGreaterThan(marksList, aMTestStats.getzScore()));
            aMTestStatses.add(aMTestStats);
        }

        return aMTestStatses;
    }


    public void processCompetitiveTest(CMDSTest cMDSTest) throws VException {

        /*String url = subscriptionEndpoint+"/batch/getBatchForAM?groupName="+WebUtils.getUrlEncodedValue(groupName);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);     
        Type batchIdsListType = new TypeToken<ArrayList<String>>(){}.getType();
        List<String> batchIds = gson.fromJson(jsonString, batchIdsListType);
        if(ArrayUtils.isEmpty(batchIds)){
            return;
        }*/

        CompetitiveTestStats competitiveTestStats = competitiveTestStatsDAO.getCompetitiveTestStats(cMDSTest.getId());

        if (competitiveTestStats == null) {
            competitiveTestStats = new CompetitiveTestStats();
        }

        List<ContentInfo> contentInfos = new ArrayList<>();

        int start = 0;
        int size = 100;
        while (true) {

            List<ContentInfo> tempContents = contentInfoDAO.getContentInfosForPhaseTests(new ArrayList<>(), Arrays.asList(cMDSTest.getId()), start, size);

            if (ArrayUtils.isEmpty(tempContents)) {
                break;
            }

            contentInfos.addAll(tempContents);

            if (tempContents.size() < size) {
                break;
            }
            start = start + size;

        }

        if (ArrayUtils.isEmpty(contentInfos)) {
            return; // new ArrayList<>();
        }

        competitiveTestStats.setCmdsTestId(cMDSTest.getId());
        Map<String, UserCompetitiveTestInfo> userTestInfoMap = new HashMap<>();
        for (ContentInfo contentInfo : contentInfos) {
            List<CMDSTestAttempt> attempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(), Arrays.asList(CMDSTestAttempt.Constants.STARTED_AT, CMDSTestAttempt.Constants.MARKS_ACHIEVED));
            if (!userTestInfoMap.containsKey(contentInfo.getStudentId())) {
                UserCompetitiveTestInfo userCompetitiveTestInfo = new UserCompetitiveTestInfo();
                userCompetitiveTestInfo.setUserId(contentInfo.getStudentId());
                userCompetitiveTestInfo.setMarksAchieved(marksAchievedInUnitTest(attempts, cMDSTest));
                userTestInfoMap.put(contentInfo.getStudentId(), userCompetitiveTestInfo);
            } else {
                if (marksAchievedInUnitTest(attempts, cMDSTest) > userTestInfoMap.get(contentInfo.getStudentId()).getMarksAchieved()) {
                    userTestInfoMap.get(contentInfo.getStudentId()).setMarksAchieved(marksAchievedInUnitTest(attempts, cMDSTest));
                }
            }
        }

        List<UserCompetitiveTestInfo> userCompetitiveTestInfos = new ArrayList<>();
        List<Float> marksList = new ArrayList<>();
        for (Map.Entry<String, UserCompetitiveTestInfo> entry : userTestInfoMap.entrySet()) {
            userCompetitiveTestInfos.add(entry.getValue());
            marksList.add(entry.getValue().getMarksAchieved());
        }
        Collections.sort(userCompetitiveTestInfos);
        competitiveTestStats.setAverageScore(getAverageMarks(marksList));
        competitiveTestStats.setStudentsAbove65(getNumOfMarksGreaterThan(marksList, cMDSTest.getTotalMarks() * 0.65f));
        competitiveTestStats.setStudentsAbove75(getNumOfMarksGreaterThan(marksList, cMDSTest.getTotalMarks() * 0.75f));
        competitiveTestStats.setzScore(getZScore(marksList));
        competitiveTestStats.setStudentsAboveZScore(getNumOfMarksGreaterThan(marksList, competitiveTestStats.getzScore()));


        for (UserCompetitiveTestInfo userCompetitiveTestInfo : userCompetitiveTestInfos) {
            userCompetitiveTestInfo.setPercentile(getPercentileScores(userCompetitiveTestInfos, userCompetitiveTestInfo.getMarksAchieved()));
        }

        competitiveTestStats.setUserCompetitiveTestInfos(userCompetitiveTestInfos);

        competitiveTestStatsDAO.save(competitiveTestStats);
    }

    public float getPercentileScores(List<UserCompetitiveTestInfo> userCompetitiveTestInfos, Float value) {

        int lowerCount = 0;
        int sameCount = 0;
        int n = userCompetitiveTestInfos.size();
        int i = 0;
        for (UserCompetitiveTestInfo userCompetitiveTestInfo : userCompetitiveTestInfos) {
            if (userCompetitiveTestInfo.getMarksAchieved() < value) {
                lowerCount++;
            } else if (userCompetitiveTestInfo.getMarksAchieved() == value) {
                sameCount++;
            } else {
                break;
            }
        }

        return (float) (lowerCount + 0.5 * sameCount) * 100 / n;

    }


    public List<UserTestResultPojo> getUserTestResultPojosForOTO(List<String> coursePlanIds, String userId) throws BadRequestException {

        List<ContentInfo> contentInfos = new ArrayList<>();
        int start = 0;
        int size = 100;
        while (true) {

            List<ContentInfo> tempContentInfos = contentInfoDAO.getTestInfoForCoursePlan(coursePlanIds, userId, start, size);

            if (ArrayUtils.isEmpty(tempContentInfos)) {
                break;
            }

            contentInfos.addAll(tempContentInfos);

            if (tempContentInfos.size() < size) {
                break;
            }

            start = start + size;

        }

        if (ArrayUtils.isEmpty(contentInfos)) {
            return new ArrayList<>();
        }

        List<UserTestResultPojo> userTestResultPojos = new ArrayList<>();
        for (ContentInfo contentInfo : contentInfos) {
            TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();

            if (ContentInfoType.SUBJECTIVE.equals(contentInfo.getContentInfoType()) && ATTEMPTED.equals(contentInfo.getContentState())) {
                continue;
            }

            List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),null);
            if (Objects.isNull(testContentInfoMetadata) || ArrayUtils.isEmpty(testAttempts)) {
                UserTestResultPojo userTestResultPojo = new UserTestResultPojo();
                userTestResultPojo.setAttempted(false);
                userTestResultPojo.setTime(contentInfo.getCreationTime());
                userTestResultPojo.setTestName(contentInfo.getContentTitle());
                userTestResultPojo.setBatchId(contentInfo.getContextId());
                userTestResultPojos.add(userTestResultPojo);
                continue;
            }

            UserTestResultPojo userTestResultPojo = getUserTestResultForTest(testAttempts, null);
            userTestResultPojo.setBatchId(contentInfo.getContextId());
            userTestResultPojo.setTestName(contentInfo.getContentTitle());
            userTestResultPojos.add(userTestResultPojo);
        }

        return userTestResultPojos;
    }

    public void processCompetitiveTestAsync() {

        List<CMDSTest> cMDSTests = new ArrayList<>();
        Long currentTime = System.currentTimeMillis();
        Long dayStartTime = DateTimeUtils.getISTDayStartTime(currentTime);
        Long prevDayStartTime = dayStartTime - DateTimeUtils.MILLIS_PER_DAY;
        int start = 0;
        int size = 100;

        while (true) {
            List<CMDSTest> tests = testDAO.getCompetitiveTestsBetween(prevDayStartTime, dayStartTime, start, size);
            if (ArrayUtils.isEmpty(tests)) {
                break;
            }

            cMDSTests.addAll(tests);

            if (tests.size() < size) {
                break;
            }

            start = start + size;

        }

        if (ArrayUtils.isNotEmpty(cMDSTests)) {
            for (CMDSTest cMDSTest : cMDSTests) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("test", cMDSTest);
                AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.COMPUTE_COMPETITIVE_TEST_STATS, payload);
                asyncTaskFactory.executeTask(asyncTaskParams);
            }
        }

    }

    public void processCompetitiveTestAsyncForMigration() {

        List<CMDSTest> cMDSTests = new ArrayList<>();

        int start = 0;
        int size = 100;

        while (true) {
            List<CMDSTest> tests = testDAO.getCompetitiveTests(start, size);
            if (ArrayUtils.isEmpty(tests)) {
                break;
            }

            cMDSTests.addAll(tests);

            if (tests.size() < size) {
                break;
            }

            start = start + size;

        }

        if (ArrayUtils.isNotEmpty(cMDSTests)) {
            for (CMDSTest cMDSTest : cMDSTests) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("test", cMDSTest);
                AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.COMPUTE_COMPETITIVE_TEST_STATS, payload);
                asyncTaskFactory.executeTask(asyncTaskParams);
            }
        }

    }

    public StudentScoreInfo convertTestAttemptToScore(CMDSTestAttempt testAttempt) {

        StudentScoreInfo studentScoreInfo = new StudentScoreInfo();

        studentScoreInfo.setMarksAchieved(testAttempt.getMarksAcheived());
        studentScoreInfo.setTotalMarks(testAttempt.getTotalMarks());
        List<SectionResultInfo> sectionResultInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(testAttempt.getCategoryAnalyticsList())) {
            for (CategoryAnalytics categoryAnalytics : testAttempt.getCategoryAnalyticsList()) {
                SectionResultInfo sectionResultInfo = new SectionResultInfo();
                sectionResultInfo.setMarksAchieved(categoryAnalytics.getMarks());
                sectionResultInfo.setTotalMarks(categoryAnalytics.getMaxMarks());
                sectionResultInfo.setName(categoryAnalytics.getName());
                sectionResultInfo.setCategoryType(categoryAnalytics.getCategoryType());
                sectionResultInfo.setSubject(categoryAnalytics.getSubject());
                sectionResultInfos.add(sectionResultInfo);
            }
        }
        studentScoreInfo.setDuration(testAttempt.getTimeTakenByStudent());
        studentScoreInfo.setSectionResultInfos(sectionResultInfos);

        return studentScoreInfo;

    }

    List<StudentScoreInfo> getStudentScoreInfos(CMDSTest cMDSTest) throws BadRequestException {

        List<ContentInfo> contentInfos = new ArrayList<>();
        int size = 100;
        String lastFetchedId=null;
        while (true) {
            List<ContentInfo> tempList = contentInfoManager.getContentInfosForTestId(cMDSTest.getId(), lastFetchedId, size);
            if (ArrayUtils.isEmpty(tempList)) {
                break;
            }else{
                lastFetchedId = tempList.get(tempList.size() - 1).getId();
            }
            contentInfos.addAll(tempList);
            if (tempList.size() < size) {
                break;
            }

        }

        Map<String, CMDSTestAttempt> userContentInfos = new HashMap<>();

        if (ArrayUtils.isEmpty(contentInfos)) {
            return new ArrayList<>();
        }

        Set<String> studentIds = new HashSet<>();

        for (ContentInfo contentInfo : contentInfos) {

            if (!EVALUATED.equals(contentInfo.getContentState())) {
                continue;
            }

            List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),null);
            if (ArrayUtils.isEmpty(testAttempts)) {
                continue;
            }
            Float marksAchieved = null;
            CMDSTestAttempt attemptToBeConsidered = null;
            for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {

                if (!CMDSAttemptState.EVALUATE_COMPLETE.equals(cMDSTestAttempt.getAttemptState())) {
                    continue;
                }

                if (cMDSTestAttempt.getStartedAt() <= cMDSTest.getMaxStartTime() && cMDSTestAttempt.getStartedAt() >= cMDSTest.getMinStartTime()) {

                    if (marksAchieved == null) {
                        marksAchieved = cMDSTestAttempt.getMarksAcheived();
                        attemptToBeConsidered = cMDSTestAttempt;
                    }

                }
            }

            if (marksAchieved != null) {
                if (!userContentInfos.containsKey(contentInfo.getStudentId())) {
                    studentIds.add(contentInfo.getStudentId());
                    userContentInfos.put(contentInfo.getStudentId(), attemptToBeConsidered);
                } else {
                    CMDSTestAttempt previousAttempt = userContentInfos.get(contentInfo.getStudentId());
                    if (previousAttempt.getMarksAcheived() < marksAchieved) {
                        userContentInfos.put(contentInfo.getStudentId(), attemptToBeConsidered);
                    }
                }
            }

        }
        Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(studentIds, false);
        List<StudentScoreInfo> studentScoreInfos = new ArrayList<>();
        for (Map.Entry<String, CMDSTestAttempt> entry : userContentInfos.entrySet()) {
            String key = entry.getKey();
            CMDSTestAttempt value = entry.getValue();
            UserBasicInfo userBasicInfo = userMap.get(key);
            if (userBasicInfo == null) {
                continue;
            }
            StudentScoreInfo studentScoreInfo = convertTestAttemptToScore(value);
            studentScoreInfo.setFullName(userBasicInfo.getFullName());
            studentScoreInfo.setUserId(key);
            studentScoreInfos.add(studentScoreInfo);
        }

        return studentScoreInfos;

    }


    public CMDSTestList addEditCMDSTestList(AddTestListReq req) throws VException, Exception {
        req.verify();
        if (com.vedantu.util.StringUtils.isNotEmpty(req.getTestListId())) {
            CMDSTestList cMDSTestList = testDAO.getEntityById(req.getTestListId(), CMDSTestList.class);

            cMDSTestList.setTitle(req.getTitle());
            cMDSTestList.setPublished(req.isPublished());
            cMDSTestList.setTests(req.getList());

            AbstractTargetTopicEntity topicTargetTagsPojo = new AbstractTargetTopicEntity();
            if(req.getTargetGrades() == null || req.getTopics() == null) {
                // ignore
            } else {
                if (!req.getTargetGrades().isEmpty() && !req.getTopics().isEmpty()) {

                    AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
                    atte.setTargetGrades(req.getTargetGrades());
                    atte.settTopics(req.getTopics());
                    topicTargetTagsPojo = fosUtils.setAndGenerateTags(atte);

                    if (topicTargetTagsPojo != null) {
                        if (req.getMainTags() != null) {
                            topicTargetTagsPojo.getMainTags().addAll(req.getMainTags());
                        }
                        cMDSTestList.setTags(topicTargetTagsPojo);
                    } else {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid TargetGrades or Topics");
                    }
                }
            }
            cMDSTestList.setTags(topicTargetTagsPojo);
            cmdsTestListDAO.update(cMDSTestList);
            return cMDSTestList;

        } else {
            CMDSTestList cMDSTestList = new CMDSTestList();
            cMDSTestList.setTitle(req.getTitle());
            cMDSTestList.setPublished(req.isPublished());
            cMDSTestList.setTests(req.getList());

            AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
            atte.setTargetGrades(req.getTargetGrades());
            atte.settTopics(req.getTopics());
            AbstractTargetTopicEntity abstractTargetTopicEntity = fosUtils.setAndGenerateTags(atte);
            logger.info("Target grades are : " + abstractTargetTopicEntity.getTargetGrades());

            cMDSTestList.setTags(abstractTargetTopicEntity);
            cmdsTestListDAO.update(cMDSTestList);
            return cMDSTestList;
        }
    }

    public List<CMDSTestListRes> getTestMainLists(GetCMDSTestListReq req, Long userId) throws BadRequestException, VException {
        String grade = "";

        if(req.getMainTags() != null && req.getMainTags().size() > 0) {
            grade = (String) req.getMainTags().toArray()[0];
        }

        List<CMDSTestList> cMDSTestLists = new ArrayList<>();

        req.setPublished(true);
        if(ArrayUtils.isEmpty(cMDSTestLists)) {
            cMDSTestLists = cmdsTestListDAO.getTestList(req);
        }

        List<CMDSTestListRes> response = new ArrayList<>();

        CMDSTestListRes upcomingResp = new CMDSTestListRes();

        List<String> tests = new ArrayList<>();
        List<String> upcomingTests = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(cMDSTestLists)) {

            for (CMDSTestList cmdsTestList : cMDSTestLists) {
                tests.addAll(cmdsTestList.getTests());
            }

        }


        // Getting test in attempted state
        GetTestContentReq reqQuery = new GetTestContentReq();
        reqQuery.setContentState(ContentState.SHARED);
        reqQuery.setUserId(userId);
        List<ContentInfo> contentInfos = contentInfoDAO.getTestContentInfo(reqQuery);
        populateTestAttemptsForContentInfos(contentInfos);

        List<String> ongoingTests = new ArrayList<>();
        for(ContentInfo c : contentInfos) {
            // only adding tests in overall published test list for that grade
            TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) c.getMetadata();
            if(testContentInfoMetadata != null && StringUtils.isNotEmpty(testContentInfoMetadata.getTestId())) {
                String testId = testContentInfoMetadata.getTestId();
                if (!ongoingTests.contains(testId) && tests.contains(testId)) {
                    ongoingTests.add(testId);
                }
            }
        }


        CMDSTestListRes ongoingResp = new CMDSTestListRes();
        List<CMDSTest> fullTests = testDAO.getTestByIds(ongoingTests);
        if(ArrayUtils.isNotEmpty(fullTests)) {
            List<CMDSTestInfo> testInfos = new ArrayList<>();
            for (CMDSTest test : fullTests) {
                CMDSTestInfo testInfoWithSavedTest = getTestInfo(test);
                testInfoWithSavedTest.setQuestions(null);
                testInfos.add(testInfoWithSavedTest);
            }

            ongoingResp.setTitle("ONGOING TESTS");
            ongoingResp.setType(TestListType.ONGOING);
            ongoingResp.setPublished(true);
            ongoingResp.setTestsPojo(testInfos);

            logger.info("ONGOING-TEST-POJO: " + testInfos);
        }


        // removing ongoing from upcoming
        tests.removeAll(ongoingTests);

        // saved tests from platform bookmarks
        logger.info("USER-SAVE-ID: " + userId.toString());
        List<String> savedUpcomingTests = getSavedTests(userId);
        logger.info("SAVED-UPCOMING-TESTS: " + savedUpcomingTests);

        List<CMDSTest> upComingFullTests = testDAO.getTestByIdsAndMaxStartTime(tests);
        List<CMDSTestInfo> testInfos = new ArrayList<>();
        for (CMDSTest test : upComingFullTests) {
            upcomingTests.add(test.getId());
            CMDSTestInfo testInfoWithSavedTest = getTestInfo(test);
            testInfoWithSavedTest.setQuestions(null);
            logger.info("TEST-ID-SAVE-CHECK: " + test.getId());

            if(ArrayUtils.isNotEmpty(savedUpcomingTests) && savedUpcomingTests.contains(test.getId())) {
                logger.info("INSIDE-SAVE-TEST");
                testInfoWithSavedTest.setSavedState(true);
            } else {
                testInfoWithSavedTest.setSavedState(false);
            }
            testInfos.add(testInfoWithSavedTest);
        }

        upcomingResp.setTitle("UPCOMING TESTS");
        upcomingResp.setType(TestListType.UPCOMING);
        upcomingResp.setPublished(true);
        upcomingResp.setTestsPojo(testInfos);
        logger.info("UPCOMING-TEST-POJO: " + testInfos);

        if(upcomingResp != null && ArrayUtils.isNotEmpty(upcomingResp.getTestsPojo())) {
            response.add(upcomingResp);
        }
        if(ongoingResp != null && ArrayUtils.isNotEmpty(ongoingResp.getTestsPojo())) {
            response.add(ongoingResp);
        }



        // Getting ordered lists here
        logger.info("ORDERED-LIST-GRADE:" + grade);
        CMDSTestListOrder order = testDAO.getOrderedTestListByGrade(grade);
        logger.info("ORDERED-LIST-ORDER:" + order);
        List<String> orderedList = new ArrayList<>();
        if(order != null) {
            if (ArrayUtils.isNotEmpty(order.getPlaylists())) {
                orderedList = order.getPlaylists();
            }
        }

        logger.info("ORDERED-LIST-TEST:" + orderedList);

        logger.info("UPCOMING-LIST-FULL: " + upcomingTests);
        logger.info("ONGOING-LIST-FULL: " + ongoingTests);

        for(String l : orderedList) {
            CMDSTestList playlist = testDAO.getEntityById(l, CMDSTestList.class);
            if(playlist != null && playlist.isPublished()) {
                List<CMDSTest> nFullTests = testDAO.getTestByIds(playlist.getTests());
                List<CMDSTestInfo> normalTestInfos = new ArrayList<>();

                for(String te : playlist.getTests()) {
                    for (CMDSTest test : nFullTests) {
                        logger.info("PLAYLIST-LIST-TEST: " + test.getId());
                        if (!upcomingTests.contains(test.getId()) && !ongoingTests.contains(test.getId()) && te.equals(test.getId())) {
                            logger.info("CONTAINS-TEST_LIST");
                            CMDSTestInfo testInfoWithSavedTest = getTestInfo(test);
                            testInfoWithSavedTest.setQuestions(null);
                            normalTestInfos.add(testInfoWithSavedTest);
                            break;
                        }
                    }
                }
                CMDSTestListRes nResp = new CMDSTestListRes();
                nResp.setTitle(playlist.getTitle());
                nResp.setType(TestListType.NORMAL);
                nResp.setId(playlist.getId());
                nResp.setPublished(true);
                logger.info("ORDERED-LIST-TESTINFO:" + orderedList);
                nResp.setTestsPojo(normalTestInfos);
                response.add(nResp);
            }
        }


        return response;

    }

    @Deprecated
    private void populateTestAttemptsForContentInfos(List<ContentInfo> contentInfos) throws BadRequestException {
        if(ArrayUtils.isNotEmpty(contentInfos)){
            int size=contentInfos.size();
            for(int i=0;i<size;++i){
                ContentInfo contentInfo=contentInfos.get(i);
                if(contentInfo.getMetadata()!=null){
                    contentInfo.getMetadata().setTestAttempts(cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),null));
                }
            }
        }
    }

    public CMDSTestListRes getTestSeriesForHomeFeed(Long userId, String testId) throws VException {
        logger.info("method=getTestSeriesForHomeFeed, class={}", CMDSTestManager.class.toString());
        CMDSTestList testList = testDAO.getEntityById(testId, CMDSTestList.class);
        if (testList != null && testList.isPublished()) {
            logger.info("Getting CMDSTest list for testIds={}", testList.getTests());
            List<CMDSTest> fullTests = testDAO.getTestByIds(testList.getTests());

            logger.info("FULL-TESTS-TEST-SERIES : {}", fullTests);

//            List<String> testIds = fullTests.stream()
//                    .map(CMDSTest::getId)
//                    .collect(Collectors.toList());
//            List<ContentInfo> contentInfos = contentInfoManager.getContentInfoForTests(userId.toString(), testIds);
            List<CMDSTestInfo> testInfoList = new ArrayList<>();
//            int counter = 0;
            for (CMDSTest test : fullTests) {
//                testInfoList.add(getTestInfoWithSavedTests(test, contentInfos.get(counter++).getContentState()));
                testInfoList.add(getTestInfo(test));
            }

            logger.info("TESTLIST-TEST-SERIES : {}", testInfoList);
            return getTestResponse(testList, testInfoList);
        } else {
            logger.info("No Tests found for testId= {}", testId);
            return null;
        }
    }

    private CMDSTestInfo getTestInfoWithSavedTests(CMDSTest test, ContentState contentState) throws VException {
        CMDSTestInfo testInfo = getTestInfo(test);
        testInfo.setQuestions(null);
        testInfo.setAttempted(ATTEMPTED.equals(contentState) || EVALUATED.equals(contentState));
        return testInfo;
    }

    private CMDSTestListRes getTestResponse(CMDSTestList testList, List<CMDSTestInfo> testInfoList) {
        CMDSTestListRes testListResponse = new CMDSTestListRes();
        testListResponse.setTitle(testList.getTitle());
        testListResponse.setType(TestListType.NORMAL);
        testListResponse.setId(testList.getId());
        testListResponse.setPublished(true);
        testListResponse.setTestsPojo(testInfoList);
        return testListResponse;
    }

    public List<ContentInfo> getOngoingTestsMobile(Long userId, GetTestsMobileReq req) throws BadRequestException {
        // Getting test in attempted state
        GetTestContentReq reqQuery = new GetTestContentReq();
        reqQuery.setContentState(ContentState.SHARED);
        reqQuery.setUserId(userId);
        if(req.getStart() != null) {
            reqQuery.setStart(req.getStart());
        }
        if(req.getSize() != null) {
            reqQuery.setSize(req.getSize());
        }
        List<ContentInfo> contentInfos = contentInfoDAO.getTestContentInfo(reqQuery);
        populateTestAttemptsForContentInfos(contentInfos);
        return contentInfos;
    }

    public List<ContentInfo> getFinishedTestsMobile(Long userId, GetTestsMobileReq req) throws BadRequestException {
        // Getting test in attempted state
        GetTestContentReq reqQuery = new GetTestContentReq();
        reqQuery.setContentState(EVALUATED);
        reqQuery.setUserId(userId);
        if(req.getStart() != null) {
            reqQuery.setStart(req.getStart());
        }
        if(req.getSize() != null) {
            reqQuery.setSize(req.getSize());
        }
        List<ContentInfo> contentInfos = contentInfoDAO.getTestContentInfo(reqQuery);
        populateTestAttemptsForContentInfos(contentInfos);
        return contentInfos;
    }

    public List<CMDSTestInfo> getSavedTestsMobile(Long userId, GetTestsMobileReq req) throws VException {
        // call platform for getting save tests list
        if(StringUtils.isEmpty(req.getGrade())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty Grade");
        }
        List<String> allPublishedTests = getPublishedTests(req.getGrade());

        logger.info("UPCOMING-TEST-LIST" + allPublishedTests);

        if(ArrayUtils.isNotEmpty(allPublishedTests)) {
            List<CMDSTest> tests = testDAO.getTestByIdsAndMaxStartTime(allPublishedTests);
            logger.info("FULL-TEST-LIST" + tests);
            List<String> upcomingTestIds = new ArrayList<>();
            for(CMDSTest c : tests) {
                upcomingTestIds.add(c.getId());
            }
            if(ArrayUtils.isEmpty(upcomingTestIds)) {
                return new ArrayList<>();
            } else {
                List<String> savedUpcomingTests = getSavedTests(userId);
                logger.info("SAVEDUPCOMING-TEST-LIST" + savedUpcomingTests);
                upcomingTestIds.retainAll(savedUpcomingTests);

                logger.info("FINAL-UPCOMING-TEST-LIST" + upcomingTestIds);
                List<CMDSTestInfo> testInfos = new ArrayList<>();
                for (CMDSTest test : tests) {
                    if(upcomingTestIds.contains(test.getId())) {
                        CMDSTestInfo testInfoWithSavedTest = getTestInfo(test);
                        testInfoWithSavedTest.setSavedState(true);
                        testInfoWithSavedTest.setQuestions(null);
                        testInfos.add(testInfoWithSavedTest);
                    }
                }

                return testInfos;
            }
        } else {
            return new ArrayList<>();
        }
    }

    public List<ContentInfo> getAttemptedTestsMobile(Long userId, GetTestsMobileReq req) throws BadRequestException {
        if(StringUtils.isEmpty(req.getGrade())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty Grade");
        }
//        List<String> tests = getPublishedTests(req.getGrade());

//        logger.info("MOBILE-ATTEMPT-TESTS: " + tests.toString());

        GetTestContentReq reqQuery = new GetTestContentReq();
        reqQuery.setContentState(EVALUATED);
        reqQuery.setUserId(userId);
        //        List<ContentInfo> responseContentInfosList = new ArrayList<>();
//        if(ArrayUtils.isNotEmpty(contentInfos)) {
//            for (ContentInfo c : contentInfos) {
//                logger.info("ATTEMPTED-TESTS: " + c.toString());
//                TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) c.getMetadata();
//                String testId = testContentInfoMetadata.getTestId();
//                if (ArrayUtils.isNotEmpty(tests) && tests.contains(testId)) {
//                    responseContentInfosList.add(c);
//                }
//            }
//        }
        List<ContentInfo> contentInfos = contentInfoDAO.getTestContentInfo(reqQuery);
        populateTestAttemptsForContentInfos(contentInfos);
        return contentInfos;
    }

    public List<CMDSTestListRes> getCMDSTestLists(GetCMDSTestListReq req) throws BadRequestException, VException{
        String grade = "";
        if(req.getMainTags() != null && req.getMainTags().size() > 0) {
            grade = (String) req.getMainTags().toArray()[0];
        }


        List<CMDSTestList> cMDSTestLists = new ArrayList<>();

        // for ordering the list

//        if(StringUtils.isNotEmpty(grade)) {
//            CMDSVideoPlaylistOrder orderedPlaylist = cmdsVideoDAO.getOrderedPlaylistByGrade(grade);
//            if(orderedPlaylist != null) {
//                List<String> orderedList = orderedPlaylist.getPlaylists();
//                for(String playlistId : orderedList) {
//                    CMDSVideoPlaylist playlist = cMDSVideoPlaylistDAO.getEntityById(playlistId, CMDSVideoPlaylist.class);
//                    if(playlist.isPublished()) {
//                        cMDSVideoPlaylists.add(playlist);
//                    }
//                }
//            }
//        }

        if(ArrayUtils.isEmpty(cMDSTestLists)) {
            cMDSTestLists = cmdsTestListDAO.getTestList(req);
        }


        List<CMDSTestListRes> response = new ArrayList<>();

        if (ArrayUtils.isEmpty(cMDSTestLists)) {
            return new ArrayList<>();
        } else {
            //hackish but effective
            for (CMDSTestList cmdsTestList : cMDSTestLists) {
                List<String> tests = cmdsTestList.getTests();
                List<CMDSTest> fullTests = testDAO.getTestByIds(tests);
                List<CMDSTest> orderedFullTests = new ArrayList<>();

                for(String test: tests) {
                    for(CMDSTest te : fullTests) {
                        if(test.equals(te.getId())) {
                            orderedFullTests.add(te);
                            break;
                        }
                    }
                }

                CMDSTestListRes resp = mapper.map(cmdsTestList, CMDSTestListRes.class);
                List<CMDSTestInfo> testInfos = new ArrayList<>();
                for (CMDSTest test : orderedFullTests) {
                    testInfos.add(getTestInfo(test));
                }
                resp.setTitle(cmdsTestList.getTitle());
                resp.setDescription(cmdsTestList.getDescription());
                resp.setTestsPojo(testInfos);
                response.add(resp);

                if (ArrayUtils.isNotEmpty(tests)) {
                    cmdsTestList.setTests(tests);
                }
            }
        }

        return response;
    }

    public CMDSTestListOrder savePlaylistOrderForGrade(GradeCMDSPlaylistOrder req) throws BadRequestException, NotFoundException {
        req.verify();

        if(StringUtils.isNotEmpty(req.getPlaylistOrderId())) {
            CMDSTestListOrder order = testDAO.getEntityById(req.getPlaylistOrderId(), CMDSTestListOrder.class);
            if(order == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Didn't find playlists for this grade");
            }
            order.setPlaylists(req.getPlaylistIds());
            order.setGrade(req.getGrade());
            testDAO.update(order);
            return order;
        } else {
            CMDSTestListOrder order = new CMDSTestListOrder();
            order.setGrade(req.getGrade());
            order.setPlaylists(req.getPlaylistIds());
            testDAO.update(order);
            return order;
        }
    }


    public CMDSTestList getTestSeriesById(String id) throws NotFoundException {
        CMDSTestList testList = testDAO.getEntityById(id, CMDSTestList.class);
        if (testList == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Didn't find playlist by Id");
        }
        return testList;
    }


    public TestListOrderForGradeRes getOrderedTestListByGrade(String grade) throws BadRequestException, NotFoundException {

        if(StringUtils.isEmpty(grade)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty Grade");
        }

        CMDSTestListOrder order = testDAO.getOrderedTestListByGrade(grade);

        if(order == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Didn't find playlists for this grade");
        }

        List<CMDSTestList> list = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(order.getPlaylists())) {
            for(String playListId : order.getPlaylists()) {
                CMDSTestList playlist = testDAO.getEntityById(playListId, CMDSTestList.class);
                if(playlist != null) {
                    list.add(playlist);
                }
            }
        }

        TestListOrderForGradeRes res = new TestListOrderForGradeRes();
        res.setPlaylistOrderId(order.getId());
        res.setList(list);
        return res;
    }

    public List<String> getPublishedTests(String grade) {
        List<CMDSTestList> cMDSTestLists = new ArrayList<>();
        GetCMDSTestListReq req = new GetCMDSTestListReq();
        req.setPublished(true);
        Set<String> setA = new HashSet<>();
        setA.add(grade);
        req.setMainTags(setA);

        cMDSTestLists = cmdsTestListDAO.getTestList(req);

        List<CMDSTestListRes> response = new ArrayList<>();

        List<String> tests = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(cMDSTestLists)) {
            for (CMDSTestList cmdsTestList : cMDSTestLists) {
                tests.addAll(cmdsTestList.getTests());
            }
        }
        return tests;
    }

    public List<String> getSavedTests(Long userId) throws VException {

        String url = PLATFORM_ENDPOINT + "/social/getBookmarkedContextIds?";

        url = url + "userId=" + userId.toString() + "&socialContextType=" + SocialContextType.CMDSTEST;
//        if (start != null) {
//            url = url + "&start=" + start;
//        }
//        if (size != null) {
//            url = url + "&size=" + size;
//        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        Type _type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(respString, _type);
    }

    public List<CMDSTest> getTestsByIds(List<String> testIds){
        List<CMDSTest> tests = testDAO.getTestByIds(new ArrayList<>(testIds));

        return tests;
    }

    public List<CMDSTest> getCMDSTestsInfos(GetCMDSTestReq getCMDSTestReq) {

        return testDAO.getTests( getCMDSTestReq, Arrays.asList( CMDSTest.Constants.NAME, CMDSTest.Constants.CONTENT_INFO_TYPE,
                CMDSTest.Constants.ACCESS_LEVEL, CMDSTest.Constants.DURATION, CMDSTest.Constants.TARGET_IDS,
                CMDSTest.Constants.GRADES, CMDSTest.Constants.BOARD_IDS ) );
    }

    private PerformanceReportSection calculateAnalytics(List<CategoryAnalytics> categoryAnalytics) {
        PerformanceReportSection p = new PerformanceReportSection();
        if(ArrayUtils.isEmpty(categoryAnalytics)){
            return p;
        }
        p.setSubject(categoryAnalytics.get(0).getSubject());
        p.setDifficulty(categoryAnalytics.get(0).getDifficulty());
        p.setQuestionType(categoryAnalytics.get(0).getQuestionType());
        p.setTopic(categoryAnalytics.get(0).getTopic());
        p.setTitle(categoryAnalytics.get(0).getName());
        p.setType(categoryAnalytics.get(0).getCategoryType());
        p.setSubject(categoryAnalytics.get(0).getSubject());
        p.setTotal(categoryAnalytics.get(0).getMaxMarks());

        int topperIndex = categoryAnalytics.size() - 1;
        p.setTopper(categoryAnalytics.get(topperIndex).getMarks());
        p.setTopperDuration(categoryAnalytics.get(topperIndex).getDuration());

        float sum = 0f;
        Integer sumDuration = 0;
        for (CategoryAnalytics c: categoryAnalytics) {
            sum += c.getMarks();
            sumDuration += c.getDuration();
        }
        float average = sum / categoryAnalytics.size();
        p.setAverage(average);
        Integer averageDuration = sumDuration / categoryAnalytics.size();
        p.setAverageDuration(averageDuration);
        if(categoryAnalytics.size() < 10 ){
            p.setTop10(average);
            p.setTop10Duration(averageDuration);
        }else {
            int top10Rank = (int) Math.ceil((double)categoryAnalytics.size() / 10f);
            p.setTop10(categoryAnalytics.get(top10Rank-1).getMarks());
            p.setTop10Duration(categoryAnalytics.get(top10Rank-1).getDuration());
        }
        return p;
    }

    public void calculateTestAnalytics(String testId) throws BadRequestException {
        logger.info("CMDS test ID: " + testId);
        CMDSTest cmdsTest = getCMDSTestById(testId);

        if (Objects.isNull(cmdsTest)) {
            logger.info("CMDSTest Not Found");
            return;
        }
        if (Objects.isNull(cmdsTest.getMaxStartTime()) || Objects.isNull(cmdsTest.getDuration())) {
            logger.info("Non competitive test");
            return;
        }
        long buffer = 5 * DateTimeUtils.MILLIS_PER_MINUTE;
        long currentTime = System.currentTimeMillis();

        if (Objects.isNull(cmdsTest.getDuration())) {
            cmdsTest.setDuration(0L);
        }

        if (cmdsTest.getMaxStartTime() + cmdsTest.getDuration() + buffer > currentTime) {
            logger.info("test end time not happened");
            return;
        }
        if (Objects.nonNull(cmdsTest.getCustomSectionEvaluationRules())) {
            logger.info("Custom Evaluation Rules do no support Analytics");
            return;
        }
        HashMap<String, List<CategoryAnalytics>> analyticsList = new HashMap<>();

        int start = 0;
        int size = 1000;
        while (true){
            List<CMDSTestAttempt> cmdsTestAttempts = cmdsTestAttemptDAO.getAttemptDetailsForAnalyticsCalculation(testId, cmdsTest.getMaxStartTime(), start, size);
            if(ArrayUtils.isEmpty(cmdsTestAttempts)){
                break;
            }
            for (CMDSTestAttempt cmdsTestAttempt : cmdsTestAttempts) {
                for (CategoryAnalytics categoryAnalytics : cmdsTestAttempt.getCategoryAnalyticsList()) {
                    String name = categoryAnalytics.getName();
                    analyticsList.computeIfAbsent(name, k -> new ArrayList<>());
                    analyticsList.get(name).add(categoryAnalytics);
                }
            }
            start+=size;
        }

        List<PerformanceReportSection> analyticsData = new ArrayList<>();
        Comparator<CategoryAnalytics> categoryAnalyticsComparator = (o1, o2) -> o1.getMarks().compareTo(o2.getMarks())!=0 ? o1.getMarks().compareTo(o2.getMarks()) : o1.getDuration().compareTo(o2.getDuration());
        for (String name : analyticsList.keySet()) {
            List<CategoryAnalytics> categoryAnalytics = analyticsList.get(name);
            categoryAnalytics.sort(categoryAnalyticsComparator);
            analyticsData.add(calculateAnalytics(categoryAnalytics));
        }
        cmdsTest.setAnalytics(analyticsData);
        cmdsTestDAO.update(cmdsTest);
    }

    public void sendCalculateTestAnalyticsRequestAsync() throws BadRequestException {
        logger.info("Calculate Test Analytics request");

        Long starTime = System.currentTimeMillis() - 180 * DateTimeUtils.MILLIS_PER_MINUTE;
        Long endTime = System.currentTimeMillis() - 10 * DateTimeUtils.MILLIS_PER_MINUTE;
        Optional.ofNullable(testDAO.getCompetitiveTestsIgnoringTagsBetween(starTime, endTime, 0, 1000))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .filter(this::isTestEligibleForAnalyticsCalculation)
                .map(CMDSTest::getId)
                .peek(logger::info)
                .forEach(testId->awsSQSManager.sendToSQS(SQSQueue.POST_TEST_END_OPS, SQSMessageType.CALCULATE_TEST_ANALYTICS, testId));
    }

    private boolean isTestEligibleForAnalyticsCalculation(CMDSTest cmdsTest) {
        long timeTillWhenAllTestOperationShouldBeEnded = cmdsTest.getMaxStartTime() + cmdsTest.getDuration() + 20 * DateTimeUtils.MILLIS_PER_MINUTE;
        long currentTime = System.currentTimeMillis();
        boolean isEligibleForAnalyticsCalculation = timeTillWhenAllTestOperationShouldBeEnded < currentTime;
        logger.info("timeTillWhenAllTestOperationShouldBeEnded - {}",timeTillWhenAllTestOperationShouldBeEnded);
        logger.info("currentTime - {}",currentTime);
        logger.info("The test id {} eligibility criteria for analytics calculation is {}",cmdsTest.getId(),isEligibleForAnalyticsCalculation);
        return isEligibleForAnalyticsCalculation;
    }

    void updateApprovalStatus(QuestionApprovalRequest request) throws BadRequestException {
        testDAO.updateApprovalStatus(request);

        if(CMDSApprovalStatus.CHECKER_APPROVED.equals(request.getOverallApprovalStatus())) {
            CMDSTest cmdsTest = getCMDSTestById(request.getTestId());
            request.setQuestionSetId(cmdsTest.getQuestionSetId());
            questionSetDAO.updateApprovalStatus(request);
        }
    }

    public List<CMDSTestCheckerResponse> filterTestCheckerData(CheckerQuestionSetOrTestFilterRequest request) throws BadRequestException {
        request.verify();
        List<CMDSTest> cmdsTests = testDAO.filterTestCheckerData(request);
        logger.info("{} tests recieved",cmdsTests);
        List<String> questionSetIds = cmdsTests.stream().map(CMDSTest::getQuestionSetId).collect(Collectors.toList());
        List<CMDSQuestionSet> questionSet = questionSetDAO.getQuestionIdsOfQuestionSet(questionSetIds);
        Map<String, List<String>> questionIdsMap = questionSet.stream().collect(Collectors.toMap(CMDSQuestionSet::getId, CMDSQuestionSet::getQuestionIds));

        Set<String> userIds=new HashSet<>();
        Set<String> makers = cmdsTests.stream().map(CMDSTest::getCreatedBy).filter(StringUtils::isNotEmpty).collect(Collectors.toSet());
        Set<String> checkers=cmdsTests.stream().map(CMDSTest::getAssignedChecker).map(String::valueOf).filter(StringUtils::isNotEmpty).collect(Collectors.toSet());
        userIds.addAll(makers);
        userIds.addAll(checkers);

        Long currentChecker=request.getCallingUserId();

        Map<String, UserBasicInfo> userBasicInfosMap = fosUtils.getUserBasicInfosMap(userIds, false);

        return cmdsTests
                .stream()
                .map(this::getCMDSTestCheckerResponse)
                .map(res -> res.populateDetails(questionIdsMap,userBasicInfosMap,currentChecker))
                .collect(Collectors.toList());
    }

    private CMDSTestCheckerResponse getCMDSTestCheckerResponse(CMDSTest cmdsTest) {
        CMDSTestCheckerResponse checkerResponse = mapper.map(cmdsTest, CMDSTestCheckerResponse.class);
        logger.info("Test created by {} with checker id {} with test id {}",checkerResponse.getCreatedBy(),checkerResponse.getAssignedChecker(),checkerResponse.getId());
        return checkerResponse;
    }

    public void deleteUnnecessaryData() {
        testDAO.deleteUnnecessaryTests();
        questionSetDAO.deleteUnnecessaryQuestionSets();
        questionDAO.deleteUnnecessaryQuestions();
    }

    void processChangeQuestionForMarks(ChangeQuestionRequest req) {

        logger.info("Processing the details for change marks for req {}",req);

        List<CMDSTest> cmdstests = new ArrayList<>();
        int start = 0;
        int size = 200;
        while (true) {
            logger.info("start - {} -- size - {}",start,size);
            List<CMDSTest> results = testDAO.getTestsWithQuestionId(req.getQuestionId(), start, size);
            if (ArrayUtils.isEmpty(results)) {
                break;
            }

            cmdstests.addAll(results);

            if (results.size() < size) {
                break;
            }

            start = start + size;


        }
        QuestionChangeRecord questionChangeRecord = new QuestionChangeRecord();
        questionChangeRecord.setPrevQuestionId(req.getQuestionId());
        questionChangeRecord.setChangedAt(System.currentTimeMillis());
        questionChangeRecord.setChangedBy(req.getCallingUserId().toString());

        if (ArrayUtils.isNotEmpty(cmdstests)) {

            for (CMDSTest cMDSTest : cmdstests) {
                if(req.getTestIdsForAction().contains(cMDSTest.getId())){
                    logger.info("Processing for test id {} for changing the marks in metadata",cMDSTest.getId());
                    updateCMDSTestWithNewQuestionMarks(cMDSTest, req, questionChangeRecord);
                }
            }
        }else{
            logger.info("No test found for changing marks in metadata");
        }

        if (req.getChangeQuestionActionType().equals(ChangeQuestionActionType.NO_ACTION)
                && ArrayUtils.isNotEmpty(req.getTestIdsForAction())) {
            logger.info("Test ids for which action needs to be taken are {}",req.getTestIdsForAction());
            float prevPositiveMarks = questionChangeRecord.getPositiveMarks();
            for (String testId : req.getTestIdsForAction()) {
                questionChangeRecord.setPrevQuestionId(req.getQuestionId());
                questionChangeRecord.setNewQuestionId(req.getQuestionId());
                questionChangeRecord.setChangeQuestionActionType(req.getChangeQuestionActionType());
                questionChangeRecord.setprevQuestionPositiveMarks(prevPositiveMarks);
                questionChangeRecord.setPositiveMarks(req.getPositiveMarks());
                questionChangeRecord.setNegativeMarks(req.getNegativeMarks());

                logger.info("Sending test id for test reevaluation {}",testId);
                PostChangeQuestionSQSPojo postChangeQuestionSQSPojo = new PostChangeQuestionSQSPojo(testId,questionChangeRecord,req.getChangeQuestionActionType(),false);
                awsSQSManager.sendToSQS(SQSQueue.POST_CHANGE_QUESTION_QUEUE, SQSMessageType.REEVALUATE_TESTS_AFTER_CHANGE_MARKS, gson.toJson(postChangeQuestionSQSPojo), testId);
            }
        }else{
            logger.info("No test ids are there for action.");
        }

    }

    public Map<String, Set<String>> getSubjectsForTests(List<String> testIds) throws BadRequestException {

        if (CollectionUtils.isEmpty(testIds)) {
            return new HashMap<>();
        } else if (testIds.size() > MAX_FETCH_SIZE_FOR_TESTS) {
            throw new BadRequestException(ErrorCode.IMPERMISSIBLE_SIZE_PER_REQUEST, "Max fetch size limit exceeded");
        }

        List<CMDSTest> fetchedTests = cmdsTestDAO.getByIds(testIds, Collections.singletonList(CMDSTest.Constants.BOARD_IDS), MAX_FETCH_SIZE_FOR_TESTS);
        return Optional.ofNullable(fetchedTests).orElseGet(ArrayList::new).stream()
                .filter(test -> CollectionUtils.isNotEmpty(test.getBoardIds()))
                .collect(Collectors.toMap(AbstractMongoStringIdEntity::getId, AbstractCMDSEntity::getBoardIds));

    }

}