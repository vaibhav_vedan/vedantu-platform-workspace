/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.dao.StudyDAO;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.entities.StudyEntry;
import com.vedantu.cmds.entities.StudyEntryItem;
import com.vedantu.cmds.pojo.ChapterPdf;
import com.vedantu.cmds.pojo.StudySubjectOrBook;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

/**
 *
 * @author ajith
 */
@Service
public class StudyManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(StudyManager.class);

    @Autowired
    private StudyDAO studyDAO;

    private final static String PLATFORM_END_PONT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");

    public void create(List<StudyEntry> entries, String callingUserId, boolean replaceAll) throws VException, Exception {
        //too much but since admin side, it is ok, won't be more than 10000 entries
        List<StudyEntryItem> studyEntryItems = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(entries)) {
            for (StudyEntry entry : entries) {
                String curriculumName = entry.getTitle();
                String boardGrade = entry.getBottomText();
                if (ArrayUtils.isNotEmpty(entry.getSubjectOrBooks())) {
                    for (StudySubjectOrBook book : entry.getSubjectOrBooks()) {
                        String subjectOrBookName = book.getTitle();
                        if (ArrayUtils.isNotEmpty(book.getChapters())) {
                            for (ChapterPdf chapterPdf : book.getChapters()) {
                                StudyEntryItem item = new StudyEntryItem(curriculumName, subjectOrBookName, chapterPdf.getChapterTitle(), chapterPdf.getPdfLink(),
                                        chapterPdf.getSeoUrl(), boardGrade);
                                studyEntryItems.add(item);
                                chapterPdf.setId(item.getId());
                            }
                        }
                    }
                }
                if (ArrayUtils.isNotEmpty(entry.getChapters())) {
                    for (ChapterPdf chapterPdf : entry.getChapters()) {
                        StudyEntryItem item = new StudyEntryItem(curriculumName, null, chapterPdf.getChapterTitle(), chapterPdf.getPdfLink(),
                                chapterPdf.getSeoUrl(), boardGrade);
                        studyEntryItems.add(item);
                        chapterPdf.setId(item.getId());
                    }
                }
            }
        }
        studyDAO.replaceAll(entries, callingUserId, replaceAll);
        studyDAO.replaceAllStudyEntryItems(studyEntryItems, callingUserId, replaceAll);
    }

    public List<StudyEntry> getStudyWithSubjects(Set<String> mainTags) {
        List<StudyEntry> studyEntries = studyDAO.getStudyWithSubjects(mainTags);
        logger.info("Fetched PDFs : {}", studyEntries);
        return studyEntries;
    }

    public StudyEntry getStudyWithSubjectsById(String stringId) {
        return studyDAO.getEntityById(stringId, StudyEntry.class);
    }


    public List<ChapterPdf> getChapterPdfs(String studyEntryId, String title, Long userId) throws NotFoundException, VException {
        List<ChapterPdf> chapterPdfs = studyDAO.getChapterPdfs(studyEntryId, title);
        if (ArrayUtils.isNotEmpty(chapterPdfs)) {
            List<String> ids = new ArrayList<>();
            for (ChapterPdf chapterPdf : chapterPdfs) {
                if (StringUtils.isNotEmpty(chapterPdf.getId())) {
                    ids.add(chapterPdf.getId());
                }
            }

            //Need to revisit bookmark implementation

//            Map<String, Boolean> map = getBookmarkedMap(userId, ids);
//            if (map != null) {
//                for (ChapterPdf chapterPdf : chapterPdfs) {
//                    if (Boolean.TRUE.equals(map.get(chapterPdf.getId()))) {
//                        chapterPdf.setBookmark(true);
//                    }
//                }
//            }
        }

        return chapterPdfs;
    }

    public List<StudyEntryItem> getSavedBookmarks(Long userId, Integer start, Integer size) throws NotFoundException, VException {
        List<String> studyEntryItemIds = getBookmarkedContextIds(userId, start, size);
        List<StudyEntryItem> items= studyDAO.getStudyEntryItems(studyEntryItemIds);
        if(ArrayUtils.isNotEmpty(items)){
            for(StudyEntryItem item:items){
                item.setBookmark(true);
            }
        }
        return items;
    }

    public StudyEntryItem getStudyEntryItemById(String id) throws VException {
        StudyEntryItem item = studyDAO.getEntityById(id, StudyEntryItem.class);
        if(item == null) {
            throw new NotFoundException(ErrorCode.STUDY_ENTRY_ITEM_NOT_FOUND, "STUDY_ENTRY_ITEM_NOT_FOUND");
        }
        return item;
    }

    public StudyEntryItem getStudyEntryItem(String seoUrl, Long userId) throws VException {
        StudyEntryItem item = studyDAO.getStudyEntryItem(seoUrl);
        if (item == null) {
            throw new NotFoundException(ErrorCode.STUDY_ENTRY_ITEM_NOT_FOUND, "STUDY_ENTRY_ITEM_NOT_FOUND");
        }
        PlatformBasicResponse platformBasicResponse = isBookmarked(userId, item.getId());
        if (platformBasicResponse.isSuccess()) {
            item.setBookmark(true);
        }
        return item;
    }

    public void incrementStudyView(String studyEntryId){
        Query query = new Query();
        query.addCriteria(Criteria.where(StudyEntryItem.Constants._ID).is(studyEntryId));
        Update update = new Update();
        update.inc(StudyEntryItem.Constants.VIEWS, 1);
        studyDAO.upsertEntity(query, update, StudyEntryItem.class);
    }

    public StudyEntryItem getMostViewedStudyEntryItem() {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, StudyEntryItem.Constants.VIEWS));
        return studyDAO.findOne(query, StudyEntryItem.class);
    }

    public List<String> getBookmarkedContextIds(Long userId, Integer start, Integer size) throws VException {
        String url = PLATFORM_END_PONT + "/social/getBookmarkedContextIds?";

        url = url + "userId=" + userId + "&socialContextType=" + SocialContextType.STUDY_ENTRY_ITEM;
        if (start != null) {
            url = url + "&start=" + start;
        }
        if (size != null) {
            url = url + "&size=" + size;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        Type _type = new TypeToken<ArrayList<String>>() {
        }.getType();
        List<String> studyEntryItemIds = new Gson().fromJson(respString, _type);

        return studyEntryItemIds;
    }

    public Map<String, Boolean> getBookmarkedMap(Long userId, List<String> itemIds) throws VException {
        String url = PLATFORM_END_PONT + "/social/getBookmarkedMap?";

        url = url + "userId=" + userId;
        if (ArrayUtils.isNotEmpty(itemIds)) {
            for (String id : itemIds) {
                url = url + "&entityIds=" + id;
            }
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        Type _type = new TypeToken<Map<String, Boolean>>() {
        }.getType();

        Map<String, Boolean> response = new Gson().fromJson(respString, _type);
        return response;
    }

    public PlatformBasicResponse isBookmarked(Long userId, String contextId) throws VException {
        String url = PLATFORM_END_PONT + "/social/isBookmarked?userId=" + userId + "&contextId=" + contextId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        PlatformBasicResponse response = new Gson().fromJson(respString, PlatformBasicResponse.class);
        return response;
    }

    public List<StudyEntryItem> getTopMostViewedPdfs(String grade, int limit) {
        logger.info("method=getTopMostViewedPdfs, class={}", StudyManager.class.toString());
        return studyDAO.getTopMostViewedPdfs(grade, limit);
    }

    public List<StudyEntry> getStudyEntriesForNCERT(List<String> titles, String grade)
    {
        return studyDAO.getStudyEntriesForNCERT(titles, grade);
    }

	public void updateStudyEntryOrder(List<String> targetGradeTitleOrder, String callingUserId) throws RuntimeException {
		
		studyDAO.updateStudyEntryOrder(targetGradeTitleOrder, callingUserId);
		
	}
	
	public List<StudyEntry> getStudyEntryByGrades(Set<String> mainTags) {
		return studyDAO.getStudyEntryByGrades(mainTags);
	}


	public void updateStudyEntryById(List<String> ids, String callingUserId) throws NotFoundException, VException{
		
		studyDAO.updateStudyEntryById(ids, callingUserId);
	}
	

}
