package com.vedantu.cmds.managers;

import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.ConfigUtils;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.io.InputStream;
import java.util.UUID;

@Service
public class AmazonS3Manager {

    @Autowired
    private AmazonClient amazonClient;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonS3Manager.class);

    public boolean uploadImage(String key, File file, Map<String, String> metadata, UploadTarget uploadTarget) throws FileNotFoundException {
        if (uploadTarget == null) {
            uploadTarget = UploadTarget.QUESTIONSETS;//for backward compatibility
        }
        String bucket = uploadTarget.getBucketName();
        if (StringUtils.isEmpty(bucket)) {
            bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        }
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String s3location = bucket + java.io.File.separator + env + java.io.File.separator + uploadTarget.getFolderPath();
        return amazonClient.uploadImage(key, file, metadata, s3location);
    }

    public String getImageUrl(String fileName, String bucketName) {
        //logger.info("Request : " + fileName);
        logger.debug("Request : " + fileName);
        String url = amazonClient.getImageUrl(fileName, bucketName);
        //logger.info("Response : " + url);
        logger.debug("Response : " + url);
        return url;
    }

    public String getImageUrl(CMDSImageDetails imageDetails) {
        UploadTarget uploadTarget = imageDetails.getUploadTarget();
        if (uploadTarget == null) {
            uploadTarget = UploadTarget.QUESTIONSETS;//for backward compatibility
            imageDetails.setUploadTarget(uploadTarget);
        }
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();

        if (UploadTarget.QUESTIONSETS.equals(uploadTarget)
                || UploadTarget.QUESTIONS.equals(uploadTarget)) {
            String cloudfront = ConfigUtils.INSTANCE.getStringValue("QUESTION_PICS_CLOUDFRONT");
            if (env.equalsIgnoreCase("prod")) {
                return cloudfront + uploadTarget.getFolderPath() + java.io.File.separator + imageDetails.getFileName();
            } else {
                return cloudfront + env.toLowerCase() + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + imageDetails.getFileName();
            }
        }

        String bucket = uploadTarget.getBucketName();
        if (StringUtils.isEmpty(bucket)) {
            bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        }

        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + imageDetails.getFileName();
        return amazonClient.getImageUrl(fullKey, bucket);
    }

    public String getImageUrl(String fileName, UploadTarget uploadTarget) {
        if (uploadTarget == null) {
            uploadTarget = UploadTarget.QUESTIONSETS;//for backward compatibility
        }
        String bucket = uploadTarget.getBucketName();
        if (StringUtils.isEmpty(bucket)) {
            bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        }
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + fileName;
        return amazonClient.getImageUrl(fullKey, bucket);
    }

    public String getPresignedUrl(UploadTarget uploadTarget, String contentType, String midFolderEntityId) {
        String bucket = uploadTarget.getBucketName();
        if (StringUtils.isEmpty(bucket)) {
            bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        }

        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath();
        if (StringUtils.isNotEmpty(midFolderEntityId)) {
            fullKey += java.io.File.separator + midFolderEntityId;
        }
        fullKey += java.io.File.separator + UUID.randomUUID();
        return amazonClient.getPreSignedUrl(bucket, fullKey, contentType);
    }

    public InputStream getObject(String fileName, UploadTarget uploadTarget) {
        String bucket = uploadTarget.getBucketName();
        if (StringUtils.isEmpty(bucket)) {
            bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        }
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + fileName;
        return amazonClient.getObject(fullKey, bucket);
    }

    public String getDoubtsImageUrl(String fileName) {
        UploadTarget uploadTarget = UploadTarget.DOUBTS_PICS;
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + fileName;
        String doubtPicsCloudfront = ConfigUtils.INSTANCE.getStringValue("DOUBT_PICS_CLOUDFRONT");
        if (env.equals("prod") || env.equals("mobile")) {
            return doubtPicsCloudfront + fileName;
        }
        return amazonClient.getImageUrlForDoubts(fullKey, bucket);
    }

    public String uploadFile(File file, String key, String bucketName)
    {
        String publicUrl = amazonClient.uploadFile(file, key, bucketName);
        logger.info("publicUrl : {}", publicUrl);
        return publicUrl;
    }

}
