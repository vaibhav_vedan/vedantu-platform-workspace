package com.vedantu.cmds.managers;

import java.io.IOException;
import java.lang.reflect.Type;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.vedantu.cmds.enums.OnboardingVideoType;
import com.vedantu.cmds.pojo.OnboardingVideoData;
import com.vedantu.cmds.request.*;
import com.vedantu.cmds.response.*;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.dao.CMDSVideoDAO;
import com.vedantu.cmds.dao.CMDSVideoPlaylistDAO;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.entities.CMDSVideoPlaylist;
import com.vedantu.cmds.entities.CMDSVideoPlaylistOrder;
import com.vedantu.cmds.entities.ClassroomChat;
import com.vedantu.cmds.entities.ClassroomChatMessage;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.enums.CMDSVideoSourceType;
import com.vedantu.cmds.pojo.CMDSVideoBasicInfo;
import com.vedantu.cmds.pojo.LastViewTime;
import com.vedantu.cmds.pojo.PlaylistVideoPojo;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.request.SetVideoCountReq;
import com.vedantu.lms.cmds.response.GetUserVoteRes;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;

import java.io.IOException;
import java.lang.reflect.Type;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import springfox.documentation.spring.web.json.Json;

@Service
public class CMDSVideoManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CMDSVideoManager.class);

	@Autowired
	private CMDSVideoDAO cmdsVideoDAO;

	@Autowired
	private CMDSVideoPlaylistDAO cMDSVideoPlaylistDAO;

	@Autowired
	private DozerBeanMapper mapper;

	@Autowired
	AsyncTaskFactory asyncTaskFactory;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private RedisDAO redisDAO;

	private final static String NODE_END_PONT = ConfigUtils.INSTANCE.getStringValue("DOUBT_NODE_SERVER");
	private final static String PLATFORM_END_PONT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
	private final static String YOUTUBE_API_KEY = "AIzaSyD-JF7LDErvuu5wSE05kB8taNEd5xEighM";

	private static final Gson gson = new Gson();

	public CMDSVideo addEditCMDSVideo(AddCMDSVideoReq req) throws Exception, BadRequestException, NotFoundException {
		req.verify();

		if (com.vedantu.util.StringUtils.isNotEmpty(req.getVideoId())) {
			CMDSVideo video = cmdsVideoDAO.getEntityById(req.getVideoId(), CMDSVideo.class);

			if (!video.getVideoUrl().equals(req.getVideoUrl())) {
				video.setVideoUrl(req.getVideoUrl());
			}
			video.setTitle(req.getTitle());
			video.setDescription(req.getDescription());

			// ToDo verify teacherIds before saving
			if (ArrayUtils.isNotEmpty(req.getTeacherIds())) {
				video.setTeacherIds(req.getTeacherIds());
			}
			if (ArrayUtils.isNotEmpty(req.getTargetGrades()) && ArrayUtils.isNotEmpty(req.getTopics())) {

				AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
				atte.setTargetGrades(req.getTargetGrades());
				atte.settTopics(req.getTopics());
				AbstractTargetTopicEntity topicTargetTagsPojo = fosUtils.setAndGenerateTags(atte);

				if (topicTargetTagsPojo != null) {
					video.setTags(topicTargetTagsPojo);
				} else {
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid TargetGrades or Topics");
				}

			}
			cmdsVideoDAO.update(video);

			if(req.getVideoSourceType().equals(CMDSVideoSourceType.VIMEO)) {
				updateVimeoVideoMetadata(video);
			}
			else {
				updateYouTubeVideoMetadata(video);
			}
			
			return video;

		} else {

			CMDSVideo cMDSVideo = cmdsVideoDAO.getByUrl(req.getVideoUrl());

			if (cMDSVideo != null) {
				throw new ConflictException(ErrorCode.DUPLICATE_VIDEO_NOT_ALLOWED, "");
			}

			if (ArrayUtils.isNotEmpty(req.getTopics())) {
				//Create CMDSVideo
				CMDSVideo video = new CMDSVideo(req.getVideoUrl(), req.getTitle(), req.getDescription(), req.getTargetGrades(), req.getTopics(), req.getTeacherIds());

				logger.info("TopicTargetTagsPojo-123");
				logger.info(video.getVideoUrl());

				AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
				atte.setTargetGrades(req.getTargetGrades());
				atte.settTopics(req.getTopics());
				AbstractTargetTopicEntity topicTargetTagsPojo = fosUtils.setAndGenerateTags(atte);

				if (topicTargetTagsPojo != null) {
					video.setTags(topicTargetTagsPojo);
				} else {
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid TargetGrades or Topics");
				}

				logger.info("TopicTargetTagsPojo-123");
				logger.info(topicTargetTagsPojo);

				cmdsVideoDAO.update(video);

				if(req.getVideoSourceType().equals(CMDSVideoSourceType.VIMEO)) {
					updateVimeoVideoMetadata(video);
				}
				else {
					updateYouTubeVideoMetadata(video);
				}

				return video;
			} else {
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Topics cannot be empty");
			}
		}
	}

	public List<CMDSVideo> getAdminVideos(GetAdminVideosReq req) {
		List<CMDSVideo> cMDSVideos = cmdsVideoDAO.getAdminVideos(req);

		if (ArrayUtils.isEmpty(cMDSVideos)) {
			return new ArrayList<>();
		}

		return cMDSVideos;
	}

	public CMDSVideo getAdminCMDSVideosById(String videoId) throws NotFoundException {

		CMDSVideo cMDSVideo = cmdsVideoDAO.getEntityById(videoId, CMDSVideo.class);

		if (cMDSVideo == null) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "");
		}

		/*
		 * UserBasicInfo userBasicInfo =
		 * fosUtils.getUserBasicInfo(cMDSVideo.getCreatedBy(), true); CMDSVideoBasicInfo
		 * cMDSVideoBasicInfo = mapper.map(cMDSVideo, CMDSVideoBasicInfo.class);
		 * cMDSVideoBasicInfo.setCreatedByBasicInfo(userBasicInfo);
		 */
		return cMDSVideo;
	}

	public CMDSVideo getCMDSVideosById(String videoId) throws NotFoundException {
		CMDSVideo cMDSVideo = cmdsVideoDAO.getEntityById(videoId, CMDSVideo.class);
		if (cMDSVideo == null) {
//			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "");
			return null;
		}
		return cMDSVideo;
	}

	public CMDSVideoBasicInfo getCMDSVideosById(String videoId, Long userId) throws NotFoundException, VException {

		CMDSVideo cMDSVideo = cmdsVideoDAO.getEntityById(videoId, CMDSVideo.class);

		if (cMDSVideo == null) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "");
		}

		CMDSVideoBasicInfo cMDSVideoBasicInfo = mapper.map(cMDSVideo, CMDSVideoBasicInfo.class);

//		cMDSVideoBasicInfo.setLikeCount(cMDSVideo.getTotalLikes().intValue());
//		cMDSVideoBasicInfo.setDislikeCount(cMDSVideo.getTotalUnlikes().intValue());

		try {
			// TODO precalculate info rather than aggregate and get on the fly
			GetUserVoteRes getUserVoteRes = getUserVotes(SocialContextType.CMDSVIDEO, videoId, userId);
			cMDSVideoBasicInfo.setDislikeCount(getUserVoteRes.getDislikeCount());
			cMDSVideoBasicInfo.setLikeCount(getUserVoteRes.getLikeCount());
			cMDSVideoBasicInfo.setLiked(getUserVoteRes.isLiked());
			cMDSVideoBasicInfo.setDisliked(getUserVoteRes.isDisliked());
		} catch (Exception ex) {
			logger.error("Error in getting votes for video " + ex.getMessage());
		}

		boolean bookmarked = isBookmarked(SocialContextType.CMDSVIDEO, videoId, userId);
		cMDSVideoBasicInfo.setBookmarked(bookmarked);

		return cMDSVideoBasicInfo;
	}

	public boolean isBookmarked(SocialContextType socialContextType, String contextId, Long userId) throws VException {
		String url = PLATFORM_END_PONT + "/social/isBookmarked?";

		url = url + "socialContextType=" + socialContextType + "&contextId=" + contextId + "&userId=" + userId.toString();

		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String respString = resp.getEntity(String.class);
		PlatformBasicResponse res = gson.fromJson(respString, PlatformBasicResponse.class);
		return res != null && res.isSuccess();
	}

	public GetUserVoteRes getUserVotes(SocialContextType socialContextType, String contextId, Long userId) throws VException {
		String url = PLATFORM_END_PONT + "/social/getUserVotes?";

		url = url + "socialContextType=" + socialContextType + "&contextId=" + contextId + "&userId="+ userId.toString();

		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String respString = resp.getEntity(String.class);
		GetUserVoteRes getUserVoteRes = gson.fromJson(respString, GetUserVoteRes.class);
		return getUserVoteRes;
	}

	public void updateVimeoMetadata(String url, String id) throws VException, IOException {

		CMDSVideo video = cmdsVideoDAO.getEntityById(id, CMDSVideo.class);

		video.setVideoSourceType(CMDSVideoSourceType.VIMEO);
		if (StringUtils.isEmpty(url)) {
			url = video.getVideoUrl();
		}

		String[] videoId = url.split("/");
		Integer len = videoId.length;
		ClientResponse vimeoMetadata = WebUtils.INSTANCE.doCall("https://api.vimeo.com/videos/" + videoId[len - 1], HttpMethod.GET, null, "bearer f0e95baeae39e06a3234f7a7c5c63477");
		VExceptionFactory.INSTANCE.parseAndThrowException(vimeoMetadata);
		String jsonString = vimeoMetadata.getEntity(String.class);
		JsonObject vimeoVideoMetadata = new Gson().fromJson(jsonString, JsonObject.class);

		Integer duration = vimeoVideoMetadata.get("duration").getAsInt();
		if (duration != null) {
			video.setDuration(duration);
		}
		try {
			String link = vimeoVideoMetadata.get("pictures").getAsJsonObject().get("sizes").getAsJsonArray().get(3).getAsJsonObject().get("link").getAsString();
			if (StringUtils.isNotEmpty(link)) {
				video.setThumbnail(link);
			}
		} catch (Exception ex) {
			//
		}
		try {
			JsonArray fileLinks = vimeoVideoMetadata.get("files").getAsJsonArray();
			for (int i = 0; i < fileLinks.size(); i++) {
				JsonObject jo = fileLinks.get(i).getAsJsonObject();
				if (StringUtils.isNotEmpty(jo.get("quality").getAsString()) && jo.get("quality").getAsString().equals("hls")) {
					String secureLink = jo.get("link_secure").getAsString();
					if (StringUtils.isEmpty(secureLink)) {
						secureLink = jo.get("link").getAsString();
					}
					if (StringUtils.isNotEmpty(secureLink)) {
						video.setHlsVideoUrl(secureLink);
					}
				}
			}
		} catch (Exception ex) {
			//
		}
		cmdsVideoDAO.update(video);
	}

	public void updateYouTubeMetadata(String url, String id) throws VException, IOException {

		CMDSVideo video = cmdsVideoDAO.getEntityById(id, CMDSVideo.class);

		video.setVideoSourceType(CMDSVideoSourceType.YOUTUBE);
		if (StringUtils.isEmpty(url)) {
			url = video.getVideoUrl();
		}

		video.setHlsVideoUrl(url);
		
		String[] videoId = url.split("/");
		Integer len = videoId.length;
		ClientResponse youTubeMetadata = WebUtils.INSTANCE.doCall("https://www.googleapis.com/youtube/v3/videos?"
				+ "part=contentDetails,statistics"
				+ "&id=" + videoId[len - 1]
				+ "&key=" + YOUTUBE_API_KEY, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(youTubeMetadata);
		String jsonString = youTubeMetadata.getEntity(String.class);
		JsonObject youTubeVideoMetadata = new Gson().fromJson(jsonString, JsonObject.class);
		
		if(youTubeVideoMetadata.get("pageInfo").getAsJsonObject()
				.get("totalResults").getAsInt() == 0) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Youtube Url entry does not exists");
		}
		
		String parsedDuration = youTubeVideoMetadata.get("items").getAsJsonArray()
								.get(0).getAsJsonObject()
								.get("contentDetails").getAsJsonObject()
								.get("duration").getAsString();
		Integer duration = Math.toIntExact(Duration.parse(parsedDuration).getSeconds());

		if (duration != null) {
			video.setDuration(duration);
		}
		
		String parsedViewCount = youTubeVideoMetadata.get("items").getAsJsonArray()
								.get(0).getAsJsonObject()
								.get("statistics").getAsJsonObject()
								.get("viewCount").getAsString();
		
		if (StringUtils.isNotEmpty(parsedViewCount)) {
			video.setVideoViews(Long.parseLong(parsedViewCount));
		}
		
		try {
			String link = "https://i.ytimg.com/vi/" + videoId[len -1] + "/hqdefault.jpg";
			if (StringUtils.isNotEmpty(link)) {
				video.setThumbnail(link);
			}
		} catch (Exception ex) {
			//
		}
		cmdsVideoDAO.update(video);
	}

	public void updateVimeoVideoMetadata(CMDSVideo video) {
		try {
			Map<String, Object> payload = new HashMap<>();
			payload.put("vimeoUrl", video.getVideoUrl());
			payload.put("vimeoId", video.getId());
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_CMDS_VIDEO_VIMEO_METADATA, payload);
			asyncTaskFactory.executeTask(params);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public void updateYouTubeVideoMetadata(CMDSVideo video) {
		try {
			Map<String, Object> payload = new HashMap<>();
			payload.put("youTubeUrl", video.getVideoUrl());
			payload.put("youTubeId", video.getId());
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_CMDS_VIDEO_YOUTUBE_METADATA, payload);
			asyncTaskFactory.executeTask(params);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	public CMDSVideoPlaylist addEditCMDSVideoPlaylist(AddVideoPlaylistReq req) throws VException, Exception {
		req.verify();
		if (com.vedantu.util.StringUtils.isNotEmpty(req.getVideoPlaylistId())) {
			CMDSVideoPlaylist cMDSVideoPlaylist = cMDSVideoPlaylistDAO.getEntityById(req.getVideoPlaylistId(), CMDSVideoPlaylist.class);
			List<String> vidEntityIdList = new ArrayList<>();

			for (PlaylistVideoPojo video : cMDSVideoPlaylist.getVideos()) {
				vidEntityIdList.add(video.getEntityId());
			}

			for (PlaylistVideoPojo video : req.getPlaylist()) {
				if (!vidEntityIdList.contains(video.getEntityId())) {
					video.setAddTime(System.currentTimeMillis());
				}
			}

			boolean changeInGrade = false;

			logger.info("ADD-EDIT-PLAYLIST");
			logger.info("inputTargetGrades: " + req.getTargetGrades());

			Set<String> currentTGrades = cMDSVideoPlaylist.gettGrades();

			if (req.getTargetGrades() != null) {
				for (String s : req.getTargetGrades()) {
					if (currentTGrades != null && currentTGrades.size() != 0) {
						currentTGrades.remove(s.split("-")[1]);
					}
				}
			}

			if (currentTGrades != null && currentTGrades.size() > 0) {
				logger.info("UPDATING-LIST");
				changeInGrade = true;
			}

			if (!req.isPublished() || changeInGrade) {
				List<CMDSVideoPlaylistOrder> list = cmdsVideoDAO.getOrderedPlaylistWithPlaylist(req.getVideoPlaylistId());
				logger.info("ORDERED-LIST");
				logger.info(list);
				if (ArrayUtils.isNotEmpty(list)) {
					for (CMDSVideoPlaylistOrder c : list) {
						List<String> newOrderedList = c.getPlaylists();
						if (changeInGrade == true) {
							for (String grade : currentTGrades) {
								if (c.getGrade().equals(grade)) {
									logger.info("Editing for: " + grade);
									newOrderedList.remove(req.getVideoPlaylistId());
									c.setPlaylists(newOrderedList);
									logger.info("NEW-ORDER");
									logger.info(newOrderedList);
									cmdsVideoDAO.update(c);
								}
							}
						} else {
							newOrderedList.remove(req.getVideoPlaylistId());
							c.setPlaylists(newOrderedList);
							logger.info("NEW-ORDER");
							logger.info(newOrderedList);
							cmdsVideoDAO.update(c);
						}
					}
				}
			}

			cMDSVideoPlaylist.setTitle(req.getTitle());
			cMDSVideoPlaylist.setDescription(req.getDescription());
			cMDSVideoPlaylist.setPublished(req.isPublished());

			// ToDo verify teacherIds before saving
			if (req.getTeacherIds() != null) {
				cMDSVideoPlaylist.setTeacherIds(req.getTeacherIds());
			}

			cMDSVideoPlaylist.setVideos(req.getPlaylist());

			// commenting the code As we are using directly main tags of video
			AbstractTargetTopicEntity topicTargetTagsPojo = new AbstractTargetTopicEntity();
			if (req.getTargetGrades() == null || req.getTopics() == null) {
				// ignore
			} else {
				if (!req.getTargetGrades().isEmpty() && !req.getTopics().isEmpty()) {

					AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
					atte.setTargetGrades(req.getTargetGrades());
					atte.settTopics(req.getTopics());
					topicTargetTagsPojo = fosUtils.setAndGenerateTags(atte);

					if (topicTargetTagsPojo != null) {
						if (req.getMainTags() != null) {
							topicTargetTagsPojo.getMainTags().addAll(req.getMainTags());
						}
						cMDSVideoPlaylist.setTags(topicTargetTagsPojo);
					} else {
						throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid TargetGrades or Topics");
					}
				}
			}
//            AbstractTargetTopicEntity abstractTargetTopicEntity = new AbstractTargetTopicEntity ();
//            Set <String> mainTags = new HashSet<String>();
//            for(PlaylistVideoPojo  videoPojo  : req.getPlaylist()){
//                List <String> ids = new ArrayList<String>();
//                ids.add(videoPojo.getEntityId());
//                List<CMDSVideo> videoList= cmdsVideoDAO.getVideosFromIds(ids);
//                CMDSVideo video=videoList.get(0);
//                mainTags.addAll(video.getMainTags());
//            }
//            abstractTargetTopicEntity.setMainTags(mainTags);
			cMDSVideoPlaylist.setTags(topicTargetTagsPojo);
			cMDSVideoPlaylistDAO.update(cMDSVideoPlaylist);

			// update classroom chat document
			Map<String, Object> payload = new HashMap<>();
			payload.put("videoPlaylist", cMDSVideoPlaylist);
			AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_CLASSROOM_CHAT, payload);
			asyncTaskFactory.executeTask(params);

			return cMDSVideoPlaylist;

		} else {

			List<ClassroomChat> crChatList = new ArrayList<>();
			ClassroomChat classroomChat;

			// Create CMDSVideoPlaylist, and chatDocument for live videos
			for (PlaylistVideoPojo video : req.getPlaylist()) {
				video.setAddTime(System.currentTimeMillis());
				if (video.isLive()) {
					List<Long> members = new ArrayList<>();
					classroomChat = new ClassroomChat();
					classroomChat.setVideoId(video.getEntityId());
					classroomChat.setStartTime(video.getStartTime());
					classroomChat.setEndTime(video.getEndTime());
					classroomChat.setDuration(video.getDuration());
					classroomChat.setMembersList(members);
					crChatList.add(classroomChat);
				}
			}

			cMDSVideoPlaylistDAO.saveClassroomChat(crChatList);

			CMDSVideoPlaylist cMDSVideoPlaylist = new CMDSVideoPlaylist();
			cMDSVideoPlaylist.setDescription(req.getDescription());
			cMDSVideoPlaylist.setTitle(req.getTitle());
			cMDSVideoPlaylist.setPublished(req.isPublished());
			cMDSVideoPlaylist.setVideos(req.getPlaylist());
			cMDSVideoPlaylist.setTeacherIds(req.getTeacherIds());

			AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
			atte.setTargetGrades(req.getTargetGrades());
			atte.settTopics(req.getTopics());
			AbstractTargetTopicEntity abstractTargetTopicEntity = fosUtils.setAndGenerateTags(atte);
			logger.info("Target grades are : " + abstractTargetTopicEntity.getTargetGrades());
//                Set <String> mainTags = new HashSet<String>();
//                for(PlaylistVideoPojo  videoPojo  : req.getPlaylist()){
//                    List <String> ids = new ArrayList<String>();
//                    ids.add(videoPojo.getEntityId());
//                    List<CMDSVideo> videoList= cmdsVideoDAO.getVideosFromIds(ids);
//                    CMDSVideo video=videoList.get(0);
//                    mainTags.addAll(video.getMainTags());
//                }
//                abstractTargetTopicEntity.setMainTags(mainTags);
			cMDSVideoPlaylist.setTags(abstractTargetTopicEntity);
			cMDSVideoPlaylistDAO.update(cMDSVideoPlaylist);

			return cMDSVideoPlaylist;
		}
	}

	public void updatedVideoViews(CMDSVideoPlaylist cMDSVideoPlaylist) throws NotFoundException, VException{
		if(cMDSVideoPlaylist == null){
			return;
		}
		Set<String> videoIds = new HashSet<>();

		if(ArrayUtils.isNotEmpty(cMDSVideoPlaylist.getVideos())){
			for(PlaylistVideoPojo playlistVideoPojo : cMDSVideoPlaylist.getVideos()){
				videoIds.add(playlistVideoPojo.getEntityId());
			}
		}

		List<CMDSVideo> cMDSVideos = cmdsVideoDAO.getVideosViewsFromIds(videoIds);
		Map<String, CMDSVideo> videoMap = new HashMap<>();

		if (ArrayUtils.isNotEmpty(cMDSVideos)) {

			for (CMDSVideo cMDSVideo : cMDSVideos) {
				videoMap.put(cMDSVideo.getId(), cMDSVideo);
			}

			if (ArrayUtils.isNotEmpty(cMDSVideoPlaylist.getVideos())) {
				for (PlaylistVideoPojo video : cMDSVideoPlaylist.getVideos()) {
					if (videoMap.containsKey(video.getEntityId())) {
						video.setVideoViews(videoMap.get(video.getEntityId()).getVideoViews());
					}
					/*
					 * Get all videos for a playlist
					 * Get IDs for same
					 * Update Request per videoID using getVideosByID
					 */

					String videoId = video.getEntityId();
					CMDSVideo videoInfo = videoMap.get(videoId);
//					CMDSVideo videoInfo = getCMDSVideosById(videoId);
					if(videoInfo != null) {
						video.setTitle(videoInfo.getTitle());
						video.setDescription(videoInfo.getDescription());
						video.setUrl(videoInfo.getVideoUrl());
						video.setDuration(videoInfo.getDuration());
						video.setThumbnailUrl(videoInfo.getThumbnail());
						video.setLikeCount(videoInfo.getTotalLikes());
					}
				}
			}
		}

	}

	public void updatedVideoViews(List<CMDSVideoPlaylist> cMDSVideoPlaylists) throws NotFoundException, VException{
		Set<String> videoIds = new HashSet<>();

		if (ArrayUtils.isEmpty(cMDSVideoPlaylists)) {
			return;
		}

		for (CMDSVideoPlaylist cMDSVideoPlaylist : cMDSVideoPlaylists) {
			if (ArrayUtils.isNotEmpty(cMDSVideoPlaylist.getVideos())) {
				for (PlaylistVideoPojo playlistVideoPojo : cMDSVideoPlaylist.getVideos()) {
					videoIds.add(playlistVideoPojo.getEntityId());
				}
			}
		}

		List<CMDSVideo> cMDSVideos = cmdsVideoDAO.getVideosViewsFromIds(videoIds);

		Map<String, CMDSVideo> videoMap = new HashMap<>();

		if (ArrayUtils.isNotEmpty(cMDSVideos)) {

			for (CMDSVideo cMDSVideo : cMDSVideos) {
				videoMap.put(cMDSVideo.getId(), cMDSVideo);
			}

			for (CMDSVideoPlaylist cMDSVideoPlaylist : cMDSVideoPlaylists) {
				if (ArrayUtils.isNotEmpty(cMDSVideoPlaylist.getVideos())) {
					for (PlaylistVideoPojo video : cMDSVideoPlaylist.getVideos()) {
						if (videoMap.containsKey(video.getEntityId())) {
							video.setVideoViews(videoMap.get(video.getEntityId()).getVideoViews());
						}
						/*
	                     * Get all videos for a playlist
	                     * Get IDs for same
	                     * Update Request per videoID using getVideosByID
	                     */
						String videoId = video.getEntityId();
						CMDSVideo videoInfo = videoMap.get(videoId);
//						CMDSVideo videoInfo = getCMDSVideosById(videoId);
						if(videoInfo != null) {
							video.setTitle(videoInfo.getTitle());
							video.setDescription(videoInfo.getDescription());
							video.setUrl(videoInfo.getVideoUrl());
							video.setDuration(videoInfo.getDuration());
							video.setThumbnailUrl(videoInfo.getThumbnail());
							video.setLikeCount(videoInfo.getTotalLikes());
						}
					}
				}
			}

		}

	}

	/*
	 * Get all videos for a playlist
	 * Get IDs for same
	 * Update Request per videoID using getVideosByID
	 *
	 * @param CMDSVideoPlaylist object playlist
	 * @param Boolean object isViewUpdate
	 * @param Long object userId
	 */
//	private void updatePlaylist(CMDSVideoPlaylist playlist, Boolean isViewUpdate) throws NotFoundException, VException {
//		for(PlaylistVideoPojo video : playlist.getVideos()) {
//			String videoId = video.getEntityId();
//			CMDSVideoBasicInfo videoInfo = getCMDSVideosById(videoId, userId);
//			video.setTitle(videoInfo.getTitle());
//			video.setDescription(videoInfo.getDescription());
//			video.setUrl(videoInfo.getVideoUrl());
//			video.setDuration(videoInfo.getDuration());
//			video.setLikeCount(videoInfo.getTotalLikes());
//			if(!isViewUpdate) video.setVideoViews(videoInfo.getVideoViews());
//		}
//	}

	public Long getLastView(Long userId) throws VException {
		logger.info("inside getLastView");
		String url = PLATFORM_END_PONT + "/social/lastView?";
		url = url + "&userId=" + userId + "&socialContextType=" + SocialContextType.CMDSVIDEO;
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
		logger.info("Last view response :: " + resp);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String respString = resp.getEntity(String.class);
		LastViewTime res = gson.fromJson(respString, LastViewTime.class);
		return res.getViewTime();
	}

	public List<CMDSVideoPlaylist> getCMDSVideoPlaylists(GetCMDSVideoPlaylistReq req) throws NotFoundException, VException {

		String grade = "";
		if (req.getMainTags().size() > 0) {
			grade = (String) req.getMainTags().toArray()[0];
		}

		Long lastViewTime = Long.MAX_VALUE;
		try {
			lastViewTime = getLastView(req.getCallingUserId());
			if(lastViewTime == null) {
				lastViewTime = Long.MAX_VALUE;
			}
		} catch (VException vex) {

		}

		List<CMDSVideoPlaylist> cMDSVideoPlaylists = new ArrayList<>();

		if (StringUtils.isNotEmpty(grade)) {
			CMDSVideoPlaylistOrder orderedPlaylist = cmdsVideoDAO.getOrderedPlaylistByGrade(grade);
			if (orderedPlaylist != null) {
				List<String> orderedList = orderedPlaylist.getPlaylists();
				for (String playlistId : orderedList) {
					CMDSVideoPlaylist playlist = cMDSVideoPlaylistDAO.getEntityById(playlistId, CMDSVideoPlaylist.class);
					if (playlist.isPublished()) {
						cMDSVideoPlaylists.add(playlist);
					}
				}
			}
		}

		if (ArrayUtils.isEmpty(cMDSVideoPlaylists)) {
			cMDSVideoPlaylists = cMDSVideoPlaylistDAO.getVideoPlaylist(req);
		}

		if (ArrayUtils.isEmpty(cMDSVideoPlaylists)) {
			return new ArrayList<>();
		} else {
			// hackish but effective
			for (CMDSVideoPlaylist cMDSVideoPlaylist : cMDSVideoPlaylists) {
				List<PlaylistVideoPojo> videos = cMDSVideoPlaylist.getVideos();
				if (ArrayUtils.isNotEmpty(videos) && videos.size() >= 5) {
					cMDSVideoPlaylist.setVideos(videos.subList(0, 5));
				}
			}
		}
		updatedVideoViews(cMDSVideoPlaylists);

		for (CMDSVideoPlaylist playlist : cMDSVideoPlaylists) {
			for (PlaylistVideoPojo video : playlist.getVideos()) {
				logger.info("Last view time :: " + lastViewTime + " Video add time :: " + video.getAddTime());
				if (video.getAddTime() != null && lastViewTime < video.getAddTime()) {
					video.setNew(true);
				} else {
					video.setNew(false);
				}
			}
		}

		return cMDSVideoPlaylists;
	}

	public List<CMDSVideoPlaylist> getCMDSVideoPlaylistsAdmin(GetCMDSVideoPlaylistReq req) throws NotFoundException, VException {
		List<CMDSVideoPlaylist> cMDSVideoPlaylists = cMDSVideoPlaylistDAO.getVideoPlaylist(req);

		if (ArrayUtils.isEmpty(cMDSVideoPlaylists)) {
			return new ArrayList<>();
		}

		updatedVideoViews(cMDSVideoPlaylists);

		return cMDSVideoPlaylists;
	}

	public CMDSVideoPlaylist getCMDSVideoPlaylistById(String id, Long userId) throws VException {
		CMDSVideoPlaylist cMDSVideoPlaylist = cMDSVideoPlaylistDAO.getEntityById(id, CMDSVideoPlaylist.class);

		if (cMDSVideoPlaylist == null) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "");
		}
		updatedVideoViews(cMDSVideoPlaylist);

		Long lastViewTime = Long.MAX_VALUE;
		try {
			lastViewTime = getLastView(userId);
			if(lastViewTime == null) {
				lastViewTime = Long.MAX_VALUE;
			}
		} catch (VException vex) {

		}

		logger.info("User's last visit :: " + lastViewTime);

		for (PlaylistVideoPojo video : cMDSVideoPlaylist.getVideos()) {
			if(video.getAddTime() == null) {
				//ignore
			} else {
				if (video.getAddTime() != null && video.getAddTime() > lastViewTime) {
					video.setNew(true);
				} else {
					video.setNew(false);
				}
			}
		}

		/*
		 * UserBasicInfo userBasicInfo =
		 * fosUtils.getUserBasicInfo(cMDSVideoPlaylist.getCreatedBy(), true);
		 * CMDSVideoPlaylistBasicInfo cMDSVideoPlaylistBasicInfo =
		 * mapper.map(cMDSVideoPlaylist, CMDSVideoPlaylistBasicInfo.class);
		 * cMDSVideoPlaylistBasicInfo.setCreatedByBasicInfo(userBasicInfo);
		 */
		return cMDSVideoPlaylist;
	}

	public List<CMDSVideo> getLiveVideoForHomeFeed(GetCMDSVideoPlaylistReq playlistReq) throws NotFoundException {
		logger.info("method=getLiveVideoForHomeFeed, class=CMDSVideoManager.");
		List<CMDSVideoPlaylist> videoPlaylists = cMDSVideoPlaylistDAO.getVideoPlaylistsForUpcomingVideos(playlistReq);

		logger.info("HOME-FEED-LIVE-VIDEO-PLAYLIST");
		logger.info(videoPlaylists);

		if (ArrayUtils.isEmpty(videoPlaylists)) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No Live video found presently.");
		}
		List<PlaylistVideoPojo> playlistVideoPojos = getVideoPojosFromPlaylist(videoPlaylists);

		logger.info("HOME-FEED-LIVE-VIDEO-PLAYLIST-VIDEO-POJO");
		logger.info(playlistVideoPojos);

		if (ArrayUtils.isEmpty(playlistVideoPojos)) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No Live video found presently.");
		}
		List<String> videoPojoOptional = getOldestLiveVideoPojo(playlistVideoPojos);

		logger.info("HOME-FEED-LIVE-VIDEO-OPTIONAL");
		logger.info(videoPojoOptional);

		if (ArrayUtils.isNotEmpty(videoPojoOptional)) {
			List<CMDSVideo> videoOptional = cmdsVideoDAO.getVideosFromIds(videoPojoOptional);
			if (ArrayUtils.isNotEmpty(videoOptional)) {
				return videoOptional;
			} else {
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No Live video found presently.");
			}
		} else {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No Live video found presently.");
		}
	}

	private List<PlaylistVideoPojo> getVideoPojosFromPlaylist(List<CMDSVideoPlaylist> videoPlaylists) {
		return videoPlaylists.stream().filter(e -> ArrayUtils.isNotEmpty(e.getVideos()))
				.map(CMDSVideoPlaylist::getVideos).flatMap(Collection::stream).collect(Collectors.toList());
	}

	private List<String> getOldestLiveVideoPojo(List<PlaylistVideoPojo> playlistVideoPojos) {
		Long currentTime = System.currentTimeMillis();
		return playlistVideoPojos.stream()
				.filter(e -> e.getStartTime() != null && e.getEndTime() != null && e.getStartTime() < currentTime
						&& currentTime < e.getEndTime())
				.sorted(Comparator.comparing(PlaylistVideoPojo::getStartTime)).map(e -> e.getEntityId())
				.collect(Collectors.toList());
	}

	public boolean isLiveVideo(String grade) {
		return cMDSVideoPlaylistDAO.isLiveVideoAvailable(grade);
	}

	public UpcommingVideosRes getUpcomingCMDSVideoPlaylist(GetCMDSVideoPlaylistReq req) throws InternalServerErrorException, BadRequestException, VException {

		List<CMDSVideoPlaylist> cMDSVideoPlaylists = cMDSVideoPlaylistDAO.getVideoPlaylistsForUpcomingVideos(req);
		UpcommingVideosRes upcommingVideosRes = null;
		logger.info("PRINTING-MAIN-LIST");
//        logger.info(cMDSVideoPlaylists.get(0));
		Long currentTime = System.currentTimeMillis();

		CMDSVideoPlaylist cMDSVideoPlaylist = new CMDSVideoPlaylist();
		if (ArrayUtils.isEmpty(cMDSVideoPlaylists)) {
//            throw new NotFoundException(ErrorCode.NO_UPCOMING_VIDEOS, "No upcoming video available");
			List<PlaylistVideoPojo> emptyVideos = new ArrayList<>();
			cMDSVideoPlaylist.setVideos(emptyVideos);
			upcommingVideosRes = mapper.map(cMDSVideoPlaylist, UpcommingVideosRes.class);
			return upcommingVideosRes;
		}

		cMDSVideoPlaylist.setTitle("Upcoming Live Classes");
		List<PlaylistVideoPojo> videos = new ArrayList<>();
		for (CMDSVideoPlaylist cMDSVideoPlaylist1 : cMDSVideoPlaylists) {
			if (ArrayUtils.isNotEmpty(cMDSVideoPlaylist1.getVideos())) {
				for (PlaylistVideoPojo playlistVideoPojo : cMDSVideoPlaylist1.getVideos()) {
					logger.info("PRINTING-VIDEO");
					logger.info(playlistVideoPojo);
					logger.info(playlistVideoPojo.getDuration());
					if (playlistVideoPojo.isLive() && playlistVideoPojo.getEndTime() != null && playlistVideoPojo.getEndTime() > currentTime) {
						videos.add(playlistVideoPojo);
					}
				}
			}
		}

		videos.sort(new Comparator<PlaylistVideoPojo>() {
			@Override
			public int compare(PlaylistVideoPojo o1, PlaylistVideoPojo o2) {
				return o1.getStartTime().compareTo(o2.getStartTime());
			}
		});

		cMDSVideoPlaylist.setPublished(true);
		// TODO find a better way of managing upcoming videos, aggregation query can help
		if (req.getSize() != null && videos.size() >= req.getSize()) {
			videos = videos.subList(0, req.getSize());
		}
		cMDSVideoPlaylist.setVideos(videos);
		updatedVideoViews(cMDSVideoPlaylist);

		upcommingVideosRes = mapper.map(cMDSVideoPlaylist, UpcommingVideosRes.class);

		upcommingVideosRes.setClassInfoHeader(redisDAO.get("LIVE_CLASSES_HEADER"));

		String liveClassesPoints = redisDAO.get("LIVE_CLASSES_POINTS");
		if (StringUtils.isNotEmpty(liveClassesPoints)) {
			upcommingVideosRes.setClassInfoPoints(Arrays.asList(redisDAO.get("LIVE_CLASSES_POINTS").split("::")));
		}

		return upcommingVideosRes;
	}

	public void incrementCMDSVideoViews(String videoId) {

		Query query = new Query();
		query.addCriteria(Criteria.where(CMDSVideo.Constants._ID).is(videoId));

		Update update = new Update();
		update.inc(CMDSVideo.Constants.VIDEO_VIEWS, 1);

		cmdsVideoDAO.upsertEntity(query, update, CMDSVideo.class);

	}

	public CMDSVideoBasicInfo getCMDSVideosByUrl(String videoUrl, Long userId) throws NotFoundException, VException {

		CMDSVideo cMDSVideo = cmdsVideoDAO.getCMDSVideoByUrl(videoUrl);

		if (cMDSVideo == null) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "");
		}

		CMDSVideoBasicInfo cMDSVideoBasicInfo = mapper.map(cMDSVideo, CMDSVideoBasicInfo.class);
		try {
			// TODO precalculate info rather than aggregate and get on the fly
			GetUserVoteRes getUserVoteRes = getUserVotes(SocialContextType.CMDSVIDEO, videoUrl, userId);
			cMDSVideoBasicInfo.setDislikeCount(getUserVoteRes.getDislikeCount());
			cMDSVideoBasicInfo.setLikeCount(getUserVoteRes.getLikeCount());
			cMDSVideoBasicInfo.setLiked(getUserVoteRes.isLiked());
			cMDSVideoBasicInfo.setDisliked(getUserVoteRes.isDisliked());
		} catch (Exception ex) {
			logger.error("Error in getting votes for video " + ex.getMessage());
		}

		boolean bookmarked = isBookmarked(SocialContextType.CMDSVIDEO, videoUrl, userId);
		cMDSVideoBasicInfo.setBookmarked(bookmarked);

		return cMDSVideoBasicInfo;
	}

	public List<CMDSVideo> getSavedBookmarks(Long userId, Integer start, Integer size) throws VException {
		List<String> videoIds = getBookmarkedContextIds(userId, start, size);
		return cmdsVideoDAO.getVideosFromIds(videoIds);
	}

	public List<String> getBookmarkedContextIds(Long userId, Integer start, Integer size) throws VException {
		String url = PLATFORM_END_PONT + "/social/getBookmarkedContextIds?";

		url = url + "userId=" + userId + "&socialContextType=" + SocialContextType.CMDSVIDEO;
		if (start != null) {
			url = url + "&start=" + start;
		}
		if (size != null) {
			url = url + "&size=" + size;
		}

		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String respString = resp.getEntity(String.class);
		Type _type = new TypeToken<ArrayList<String>>() {
		}.getType();
		return new Gson().fromJson(respString, _type);
	}

	public CMDSVideoPlaylistOrder savePlaylistOrderForGrade(GradeCMDSPlaylistOrder req) throws BadRequestException, NotFoundException {
		req.verify();

		if (StringUtils.isNotEmpty(req.getPlaylistOrderId())) {
			CMDSVideoPlaylistOrder order = cmdsVideoDAO.getEntityById(req.getPlaylistOrderId(), CMDSVideoPlaylistOrder.class);
			if (order == null) {
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Didn't find playlists for this grade");
			}
			order.setPlaylists(req.getPlaylistIds());
			order.setGrade(req.getGrade());
			cmdsVideoDAO.update(order);
			return order;
		} else {
			CMDSVideoPlaylistOrder order = new CMDSVideoPlaylistOrder();
			order.setGrade(req.getGrade());
			order.setPlaylists(req.getPlaylistIds());
			cmdsVideoDAO.update(order);
			return order;
		}
	}

	public PlaylistOrderForGradeRes getOrderedPlaylistByGrade(String grade) throws BadRequestException, NotFoundException {

		if (StringUtils.isEmpty(grade)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty Grade");
		}

		CMDSVideoPlaylistOrder order = cmdsVideoDAO.getOrderedPlaylistByGrade(grade);

		if (order == null) {
			throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Didn't find playlists for this grade");
		}

		List<CMDSVideoPlaylist> list = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(order.getPlaylists())) {
			for (String playListId : order.getPlaylists()) {
				CMDSVideoPlaylist playlist = cMDSVideoPlaylistDAO.getEntityById(playListId, CMDSVideoPlaylist.class);
				if (playlist != null) {
					list.add(playlist);
				}
			}
		}

		PlaylistOrderForGradeRes res = new PlaylistOrderForGradeRes();
		res.setPlaylistOrderId(order.getId());
		res.setList(list);
		return res;
	}

	public List<CMDSVideo> getMostViewedVideosForGrade(String grade, int limit) {
		logger.info("method=getMostViewedVideosForGrade, class=CMDSVideoManager.");
		return cmdsVideoDAO.getMostViewedVideos(grade, limit);
	}

	public RecordedSectionRes getRecordedSection(GetCMDSVideoPlaylistReq req)
			throws InternalServerErrorException, BadRequestException, NotFoundException, VException {
		RecordedSectionRes response = new RecordedSectionRes();
		// getTrendingVideos
//		List<CMDSVideo> trendingVids = cMDSVideoPlaylistDAO.getTrendingVideos(req);
//		logger.info("Trending video count :: " + trendingVids.size());
//		response.setTrendingVideos(trendingVids);

		// getMostViewedVideos
//		List<CMDSVideo> mostViewedVids = cMDSVideoPlaylistDAO.getMostViewedVideos(req, 20);//cMDSVideoPlaylistDAO.getMostViwedVideos(req);
//		logger.info("Most viewed video count :: " + mostViewedVids.size());
//		response.setMostViewedVideos(mostViewedVids);

		ArrayList<String> contextIds = new ArrayList<>();
//		for (CMDSVideo video : trendingVids) {
//			contextIds.add(video.getId());
//		}
//		for (CMDSVideo video : mostViewedVids) {
//			contextIds.add(video.getId());
//		}

		logger.info("Selected IDs :: " + contextIds);

		// getRegularPlaylist
		List<CMDSVideoPlaylist> playlistVids = cMDSVideoPlaylistDAO.getUserPlaylists(contextIds, req);
		updatedVideoViews(playlistVids);


		Long lastViewTime = Long.MAX_VALUE;
		try {
			lastViewTime = getLastView(req.getCallingUserId());
			if(lastViewTime == null) {
				lastViewTime = Long.MAX_VALUE;
			}
		} catch (VException vex) {

		}

		for (CMDSVideoPlaylist playlist : playlistVids) {
			for (PlaylistVideoPojo video : playlist.getVideos()) {
				if (video.getAddTime() != null && lastViewTime < video.getAddTime()) {
					video.setNew(true);
				} else {
					video.setNew(false);
				}
			}
		}


		response.setPlaylist(playlistVids);

		return response;
	}

	public PlatformBasicResponse setVideoVoteCount(SetVideoCountReq req) throws VException {
		req.verify();
		cmdsVideoDAO.setCount(req.getVideoId(), CMDSVideo.Constants.TOTAL_LIKES, req.getCount(), CMDSVideo.Constants.TOTAL_UNLIKES, req.getDisLikeCount());
		return new PlatformBasicResponse();
	}

	public List<ClassroomChatMessage> getChatData(String videoId) {
		return cMDSVideoPlaylistDAO.getChatDetails(videoId);
	}

	public void simulateChat() {
		// get live videos
		List<CMDSVideoPlaylist> vidList = cmdsVideoDAO.getLiveVideos();
		// get available chats for the same
		// async task for starting chats
		logger.info("playlist count :: " + vidList.size());
		Map<String, Object> videoDetails;
		for (CMDSVideoPlaylist playlist : vidList) {
			for(PlaylistVideoPojo video : playlist.getVideos()) {
				videoDetails = new HashMap<>();
				logger.info("Starting for Video ID :: " + video.getEntityId());
				videoDetails.put(CMDSVideoPlaylist.Constants.ENTITY_ID, video.getEntityId());
				AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SIMULATE_LIVE_CHAT, videoDetails);
				asyncTaskFactory.executeTask(params);
			}
		}
	}

	public void startChat(String videoId) throws InterruptedException {
		// get chat details
		// start chat
		double randTime;
		int counter = 10;

		// TO DO DEPENDING UPON DATA RECIEVED FOLLOWING BLOCK WILL BE UPDATED
		ClassroomChatMessage msg = new ClassroomChatMessage();
		msg.setVideoId(videoId);
		msg.setUserId(new Long("4102376723343945"));
		msg.setUserName("TEST_USER_NAME");
		msg.setMessage("HOW YOU DOIN??");

		for (int i = 0; i < counter; i++) {
			// hit api with message details
			String url = NODE_END_PONT + "classRommChatMessage";
			ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(msg), true);
			logger.info("response :: " + resp);
			logger.info("user ::" + msg.getUserName() + " msg :: " + msg.getMessage());
			randTime = getRandTime();
			Thread.sleep((int) randTime); // 5000 - 10000 millis
		}
	}

	public double getRandTime() {
		Double rand = Math.random() % 5;
		return (rand + 5)*1000;
	}

	public ArrayList<FiltersRes> getFilters() {
		return cmdsVideoDAO.getFilters();
	}

	public void updateClassroomChat(CMDSVideoPlaylist playlist) {
		cMDSVideoPlaylistDAO.updateClassroomChat(playlist);
	}

	public List<CMDSVideoPlaylist> getCMDSPlaylistsfromVideoID(String videoId) {
		return cMDSVideoPlaylistDAO.getCMDSPlaylistsfromVideoID(videoId);
	}

	public CMDSVideoPlaylist getCMDSPlaylistfromVideoID(String videoId) {
		List<CMDSVideoPlaylist> playlists = cMDSVideoPlaylistDAO.getCMDSPlaylistsfromVideoID(videoId);
		
		return playlists.stream()
                .filter(x -> true == x.isPublished())
                .findFirst().get();
	}

	public List<CMDSVideoPlaylist> getCMDSPlaylistsByGrade(String grade) {
		return cMDSVideoPlaylistDAO.getCMDSPlaylistsByGrade(grade);
	}

	public List<CMDSVideo> addEditOnboardingVideo(AddEditOnboardingVideoRequest req) throws BadRequestException {
		req.verify();
		Set<String> grades=new HashSet<>();
		String grade = req.getGrade();
		grades.add(grade);

		List<OnboardingVideoType> onboardingVideoTypes =
				req.getOnboardingVideoData()
						.stream()
						.filter(Objects::nonNull)
						.filter(this::isNewVideo)
						.map(OnboardingVideoData::getOnboardingVideoType)
						.collect(Collectors.toList());
		if(ArrayUtils.isNotEmpty(onboardingVideoTypes)){
			List<CMDSVideo> cmdsVideos=cmdsVideoDAO.getOnboardingVideoForGradeAndOnboardingVideoType(grades,onboardingVideoTypes);
			if(ArrayUtils.isNotEmpty(cmdsVideos)){
				throw new BadRequestException(ErrorCode.DUPLICATE_VIDEO_NOT_ALLOWED,"Duplicate video not allowed for grade "+ grade +" with contexts "+onboardingVideoTypes);
			}
		}

		List<CMDSVideo> cmdsVideos = req.getOnboardingVideoData()
				.stream()
				.map(CMDSVideo::new)
				.map(cmdsVideo -> cmdsVideo.setOnboardingVideoGrades(grades))
				.map(cmdsVideoDAO::saveOrUpdateVideo)
				.collect(Collectors.toList());
		cmdsVideos.forEach(this::updateVimeoVideoMetadata);
		return cmdsVideos;
	}

	private boolean isNewVideo(OnboardingVideoData onboardingVideoData) {
		logger.info("onboarding video is of type {} and has id {}",onboardingVideoData.getOnboardingVideoType(),onboardingVideoData.getVideoId());
		return Objects.isNull(onboardingVideoData.getVideoId());
	}


	public List<OnboardingVideoData> getOnboardingVideo(String grade) throws BadRequestException {
		if(StringUtils.isEmpty(grade)){
			//throw new BadRequestException(ErrorCode.MISSING_PARAMETER,"[grade] cann't be null or empty");
			grade = "1";
		}
		List<CMDSVideo> cmdsVideos=cmdsVideoDAO.getOnboardingVideoForGrade(grade);
		if(ArrayUtils.isNotEmpty(cmdsVideos)){
			return cmdsVideos.stream()
					.map(OnboardingVideoData::new)
					.sorted(Comparator.comparingInt(o -> o.getOnboardingVideoType().getIndex()))
					.collect(Collectors.toList());
		}
		return null;
	}
}
