/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers.challenges;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.EventCategory;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.dao.CMDSQuestionDAO;
import com.vedantu.cmds.dao.QuestionAttemptDAO;
import com.vedantu.cmds.dao.challenges.ChallengeDAO;
import com.vedantu.cmds.dao.challenges.ClanDAO;
import com.vedantu.cmds.dao.challenges.PictureChallengeDAO;
import com.vedantu.cmds.dao.sql.ChallengeAttemptRDDAO;
import com.vedantu.cmds.dao.sql.ClanLeaderBoardDAO;
import com.vedantu.cmds.dao.sql.ClanLeaderBoardTransactionDAO;
import com.vedantu.cmds.dao.sql.PictureChallengeLeaderBoardDAO;
import com.vedantu.cmds.dao.sql.PictureChallengeLeaderBoardTransactionDAO;
import com.vedantu.cmds.dao.sql.SqlSessionFactory;
import com.vedantu.cmds.dao.sql.UserChallengeStatsDAO;
import com.vedantu.cmds.dao.sql.UserChallengeStatsTransactionDAO;
import com.vedantu.cmds.dao.sql.UserChallengeLeaderBoardDAO;
import com.vedantu.cmds.dao.sql.UserChallengeLeaderBoardTransactionDAO;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.entities.challenges.ChallengeAttemptRD;
import com.vedantu.cmds.entities.challenges.ClanLeaderBoard;
import com.vedantu.cmds.entities.challenges.ClanLeaderBoardTransaction;
import com.vedantu.cmds.entities.challenges.PictureChallenge;
import com.vedantu.cmds.entities.challenges.PictureChallengeLeaderBoard;
import com.vedantu.cmds.entities.challenges.PictureChallengeLeaderBoardTransaction;
import com.vedantu.cmds.entities.challenges.UserChallengeLeaderBoard;
import com.vedantu.cmds.entities.challenges.UserChallengeLeaderBoardTransaction;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.challenges.ClanPointsContextType;
import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.cmds.enums.challenges.UserChallengeStatsContextType;
import com.vedantu.cmds.pojo.challenges.ChallengeQuestion;
import com.vedantu.cmds.pojo.challenges.ClanPointContextPojo;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.cmds.request.clans.ProcessClanChallengeAttemptsReq;
import com.vedantu.cmds.response.challenges.ChallengeLeaderBoardInfo;
import com.vedantu.cmds.managers.AwsSQSManager;
import com.vedantu.cmds.utils.ChallengeUtilCommon;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.EnumBasket.Judgement;
import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.lms.cmds.enums.ReprocessType;
import com.vedantu.lms.cmds.pojo.AttemptedUserIds;
import com.vedantu.lms.cmds.pojo.ChallengeUserPoints;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

//mark the challenge ended --endChallenge
//update whether the answer given is correct in challengetaken--endChallenge
//create ranks for the challenge --challengeRankCalculatorProcessor
//calculate points and update this in challengeuserinfo --challengePointProcessor
//change the rank bucket for the users based the new points won --challengePointProcessor
/**
 *
 * @author ajith
 */
@Service
public class PostChallengeProcessor {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PostChallengeProcessor.class);

    @Autowired
    private ChallengeDAO challengeDAO;

    @Autowired
    private CMDSQuestionDAO cMDSQuestionDAO;

    @Autowired
    private QuestionAttemptDAO questionAttemptDAO;

    @Autowired
    private PictureChallengeDAO pictureChallengeDAO;

    @Autowired
    private ChallengeAttemptRDDAO challengeAttemptDAO;

    @Autowired
    private SqlSessionFactory sqlSessionfactory;

    @Autowired
    private UserChallengeStatsDAO userChallengeStatsDAO;

    @Autowired
    private UserChallengeStatsTransactionDAO userChallengeStatsTransactionDAO;

    @Autowired
    private UserChallengeLeaderBoardTransactionDAO userChallengeLeaderBoardTransactionDAO;

    @Autowired
    private UserChallengeLeaderBoardDAO userChallengeLeaderBoardDAO;

    @Autowired
    private UserLeaderBoardManager userLeaderBoardManager;

    @Autowired
    private ClanLeaderBoardDAO clanLeaderBoardDAO;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private PictureChallengeLeaderBoardDAO pictureChallengeLeaderBoardDAO;

    @Autowired
    private PictureChallengeLeaderBoardTransactionDAO pictureChallengeLeaderBoardTransactionDAO;

    @Autowired
    private ClanLeaderBoardTransactionDAO clanLeaderBoardTransactionDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private ClanDAO clanDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FosUtils fosUtils;

    private final Gson gson = new Gson();
    @Autowired
    private DozerBeanMapper mapper;

    public void markChallengeActiveCron() {
        challengeDAO.markChallengesActive();
    }

    public void endChallengeCron() throws ForbiddenException {
        List<Challenge> burstableChallenges = challengeDAO.getBurstableChallenges();
        if (ArrayUtils.isNotEmpty(burstableChallenges)) {
            for (Challenge challenge : burstableChallenges) {
                try {
                    endChallenge(challenge, false);
                } catch (Exception e) {
                    logger.error("Error in processing end challenge for "
                            + challenge.getId() + ", Error: " + e.getMessage());
                }
            }
        }
    }

    public void endChallenge(String challengeId) throws NotFoundException, ForbiddenException, BadRequestException {
        endChallenge(challengeId, false);
    }

    public void endChallenge(String challengeId, boolean reprocess) throws NotFoundException, ForbiddenException, BadRequestException {
        endChallenge(challengeId, false, null);
    }

    public void endChallenge(String challengeId, boolean reprocess, ReprocessType reprocessType) throws NotFoundException, ForbiddenException, BadRequestException {
        Challenge challenge = challengeDAO.getById(challengeId);
        if (challenge == null) {
            throw new NotFoundException(ErrorCode.CHALLENGE_NOT_FOUND, "challenge not found " + challengeId);
        }
        endChallenge(challenge, reprocess, reprocessType);
    }

    public void endChallenge(Challenge challenge, boolean reprocess) throws ForbiddenException, BadRequestException {
        endChallenge(challenge, reprocess, null);
    }

    public void endChallenge(Challenge challenge, boolean reprocess, ReprocessType reprocessType) throws ForbiddenException, BadRequestException {
        //TODO reprocess will it create problems
        if (challenge.getReprocess() != null || (ChallengeStatus.ENDED.equals(challenge.getStatus()) && !reprocess)) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_ALREADY_PROCESSED, "challenge already processed ");
        }
        ChallengeStatus status = ChallengeStatus.ENDED;
        if (reprocess || ChallengeStatus.ENDED.equals(challenge.getStatus())) {
            if (reprocessType == null) {
                throw new BadRequestException(ErrorCode.CHALLENGE_REPROCESS_TYPE_NOT_FOUND, "CHALLENGE_REPROCESS_TYPE_NOT_FOUND in request");
            }
        }
        if (reprocessType != null) {
            challenge.setReprocess(reprocessType);
        }
        challenge.setStatus(status);
        challenge.setLeader(null);
        challengeDAO.saveChallenge(challenge);
        boolean process = true;
        int start = 0;

        while (process) {
            List<ChallengeAttemptRD> challengeAttempts = challengeAttemptDAO.getChallengeTakensByChallengeId(challenge.getId(),
                    start, ChallengeUtilCommon.DEFAULT_BATCH_SIZE);
            logger.info("challengeTakens to process " + challengeAttempts.size());
            if (ArrayUtils.isNotEmpty(challengeAttempts)) {
                for (ChallengeAttemptRD challengeTaken : challengeAttempts) {
                    try {
                        logger.info("marking " + challengeTaken + " processed");
                        challengeTaken.processed = true;

                        if (challenge.getReprocess() != null && ReprocessType.GRACE.equals(challenge.getReprocess())) {
                            challengeTaken.setGrace(Boolean.TRUE);
                            challengeAttemptDAO.save(challengeTaken);
                            logger.info("user[" + challengeTaken.userId
                                    + "] over all challenge[" + challengeTaken.challengeId
                                    + "] grace : " + challengeTaken.isGrace());
                            continue;
                        }
                        logger.info("checking  challengeTaken["
                                + challengeTaken.getId()
                                + "] status {attempted correctly or not}");
                        int correctCount = 0;
                        for (ChallengeQuestion entity : challenge.getQuestions()) {
                            CMDSQuestion question = cMDSQuestionDAO.getById(entity.getQid());
                            List<String> answer = question.getSolutionInfo().getAnswer();
                            logger.info("answer of the question " + answer);
                            if (ArrayUtils.isEmpty(answer)) {
                                continue;
                            }
                            boolean isCorrect = ArrayUtils
                                    .isNotEmpty(challengeTaken.getAnswerList())
                                    && question.getType()
                                            .isCorrect(
                                                    Judgement.JUDGE,
                                                    challengeTaken.getAnswerList(),
                                                    answer,
                                                    EnumBasket.Status.COMPLETE);
                            logger.info("user[" + challengeTaken.userId
                                    + "] has given answer[" + challengeTaken.answerString
                                    + "] for challenge[" + challengeTaken.challengeId
                                    + "] and entity[" + entity + "], isCorrect: "
                                    + isCorrect);
                            if (isCorrect) {
                                correctCount++;
                            }
                            List<QuestionAttempt> questionAttempts = questionAttemptDAO.getByAttemptId(challengeTaken.getId());
                            if (ArrayUtils.isNotEmpty(questionAttempts)) {
                                if (questionAttempts.size() > 1) {
                                    logger.error("more than 1 attempt found for challengeTaken Id " + challengeTaken.getId());
                                }
                                questionAttempts.get(0).setCorrect(isCorrect);
                                questionAttemptDAO.create(questionAttempts.get(0));
                            }
                        }
                        // TODO: make it >=details.minTargets
                        if (correctCount >= 1) {
                            challengeTaken.success = true;
                        } else {
                            challengeTaken.success = false;
                        }
                        challengeAttemptDAO.save(challengeTaken);
                        logger.info("user[" + challengeTaken.userId
                                + "] over all challenge[" + challengeTaken.challengeId
                                + "] status : " + challengeTaken.success);
                    } catch (Exception e) {
                        logger.error("Error in marking the attempt "
                                + challengeTaken.getId() + " processed,  error " + e.getMessage());
                    }
                }
                start += challengeAttempts.size();
            } else {
                process = false;
            }
        }
        challengePointProcessor(challenge);
        if (challenge.getReprocess() == null) {
            // Stopped calculating clan points on Nov 12.
            // Therefore commenting it
//        	awsSQSManager.sendToSQS(SQSQueue.CHALLENGES_QUEUE, SQSMessageType.CHALLENGE_PROCESSED, gson.toJson(challenge));
        }
        try {
            userLeaderBoardManager.deleteLeaderBoardCache(challenge.getCategory());
        } catch (Exception e) {
            logger.error("error in deleting cache for id: " + challenge.getId());
        }

    }

    //calculating the points for the particular challenge for each user
    private void challengePointProcessor(Challenge challenge) {
        String challengeId = challenge.getId();
        boolean process = true;
        int start = 0;
        while (process) {
            List<ChallengeAttemptRD> challengeAttempts = challengeAttemptDAO
                    .getChallengeTakensByChallengeId(challengeId, start,
                            ChallengeUtilCommon.DEFAULT_BATCH_SIZE);
            if (ArrayUtils.isNotEmpty(challengeAttempts)) {
                start += challengeAttempts.size();
                for (ChallengeAttemptRD challengeAttempt : challengeAttempts) {
                    logger.info("incrementing points for user["
                            + challengeAttempt.userId + "] for challenge["
                            + challengeAttempt.challengeId + "]");
                    try {
                        incrementChallengePoint(challengeAttempt, challenge);
                    } catch (Exception e) {
                        logger.error("Error in marking the attempt "
                                + challengeAttempt.getId() + " processed,  error " + e.getMessage());
                    }
                }
            } else {
                process = false;
            }
        }
        List<ChallengeAttemptRD> toppers = challengeAttemptDAO.getChallengeLeaderBoard(challenge.getId(), 0, 1);
        if (ArrayUtils.isNotEmpty(toppers)) {
            ChallengeAttemptRD topper = toppers.get(0);
            ChallengeLeaderBoardInfo leader = mapper.map(topper, ChallengeLeaderBoardInfo.class);
            challenge.setLeader(leader);
            challengeDAO.saveChallenge(challenge);
        }
    }

    private void incrementChallengePoint(ChallengeAttemptRD challengeAttempt,
            Challenge challenge) {
        int basePoints = 0;
        int totalPoints = 0;
        int initPoints = 0;
        boolean initSuccess = challengeAttempt.success;
        long initTimeTaken = challengeAttempt.timeTaken;

        if (challengeAttempt.totalPoints != 0) {
            initPoints = challengeAttempt.totalPoints;
        }
        boolean reprocess = false;
        if (challenge.getReprocess() != null) {
            reprocess = true;
        }

        if (challengeAttempt.success && !(challengeAttempt.isGrace())) {
            basePoints = getEarnedChallengePointsAfterHintTaken(challengeAttempt.hintTakenIndex,
                    challenge);
            logger.info("user[" + challengeAttempt.userId + "], has earned ["
                    + basePoints + "] basePoints for challenge[" + challengeAttempt.challengeId
                    + "] with [" + challengeAttempt.hintTakenIndex + "] hints");

            challengeAttempt.basePoints = basePoints;
            totalPoints = basePoints;
//        challengeAttempt.totalPoints = basePoints
//                * challengeAttempt.multiplierPower.getMultiplier();
            challengeAttempt.totalPoints = totalPoints;
            challengeAttemptDAO.save(challengeAttempt);
        } else if (reprocess && !challengeAttempt.success && !(challengeAttempt.isGrace())) {
            challengeAttempt.basePoints = totalPoints;
            challengeAttempt.totalPoints = totalPoints;
            challengeAttemptDAO.save(challengeAttempt);
        }

        if ((challengeAttempt.isGrace())) {
            totalPoints = challenge.getMaxPoints();
            challengeAttempt.basePoints = totalPoints;
            challengeAttempt.totalPoints = totalPoints;
            challengeAttempt.success = true;
            challengeAttempt.timeTaken = 0;
            challengeAttemptDAO.save(challengeAttempt);
        }
        //updating totalPoints for reprocess
        totalPoints = totalPoints - initPoints;

        //updating user challengestats
        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();

        long timeTaken = challengeAttempt.getTimeTaken();
        int NUMBER_OF_TRIES = 2;
        for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();
                logger.info("try :" + retry);
                // if (retry != 0)
//                UserChallengeStats userChallengeStats = userChallengeStatsDAO
//                        .getByUserId(challengeAttempt.getUserId(), session);
//
//                if (userChallengeStats == null) {
//                    userChallengeStats = new UserChallengeStats();
//                    userChallengeStats.setUserId(challengeAttempt.getUserId());
//                    userChallengeStats.setHintsCountMap(new ArrayList<>());
//                    userChallengeStats.setMultiplierPowerType(MultiplierPowerType.SINGLE);
//                    userChallengeStatsDAO.create(userChallengeStats, "SYSTEM");
//                }
//                logger.info("userChallengeStats " + userChallengeStats);
//
//                userChallengeStats.setPoints(userChallengeStats.getPoints() + totalPoints);
//                userChallengeStats.setTotalAttempts(userChallengeStats.getTotalAttempts() + 1);
//                if (challengeAttempt.success) {
//                    userChallengeStats.setCorrectAttempts(userChallengeStats.getCorrectAttempts() + 1);
//                    userChallengeStats.updateHintCount(challengeAttempt.getHintTakenIndex());
//                }
//                userChallengeStats.calculateStrikeRate();
//                userChallengeStatsDAO.update(userChallengeStats, session, "SYSTEM");
//
//                UserChallengeStatsTransaction statsTransaction = new UserChallengeStatsTransaction(challengeAttempt.getUserId(),
//                        userChallengeStats.getId(), UserChallengeStatsContextType.CHALLENGE_ATTEMPT,
//                        challengeAttempt.getId(),
//                        totalPoints, userChallengeStats.getPoints(),
//                        userChallengeStats.getTotalAttempts(),
//                        userChallengeStats.getCorrectAttempts(),
//                        challengeAttempt.getHintTakenIndex(),
//                        userChallengeStats.getMultiplierPowerType(),
//                        challengeAttempt.getCategory());
//                userChallengeStatsTransactionDAO.updateAll(Arrays.asList(statsTransaction), session);

                Long weekStartTime = userLeaderBoardManager.getWeekStartTime(challenge.getBurstTime());
                Long monthStartTime = userLeaderBoardManager.getMonthStartTime(challenge.getBurstTime());
                Long biWeekStartTime = userLeaderBoardManager.getBiWeekStartTime(challenge.getBurstTime());
                UserChallengeLeaderBoard userLeaderBoardWeek = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.WEEK, challengeAttempt.getCategory(),
                        challenge.getDuration(), weekStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardWeek != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardWeek, session, "SYSTEM");
                    logger.info("userLeaderBoardWeek " + userLeaderBoardWeek);
                }
                UserChallengeLeaderBoard userLeaderBoardDefaultWeek = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.WEEK, "DEFAULT",
                        challenge.getDuration(), weekStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardDefaultWeek != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardDefaultWeek, session, "SYSTEM");
                    logger.info("userLeaderBoardDefaultWeek " + userLeaderBoardDefaultWeek);
                }
                /////////////////////////////////////
                UserChallengeLeaderBoard userLeaderBoardMonth = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.MONTH, challengeAttempt.getCategory(),
                        challenge.getDuration(), monthStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardMonth != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardMonth, session, "SYSTEM");
                    logger.info("userLeaderBoardMonth " + userLeaderBoardMonth);
                }
                UserChallengeLeaderBoard userLeaderBoardDefaultMonth = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.MONTH, "DEFAULT",
                        challenge.getDuration(), monthStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardDefaultMonth != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardDefaultMonth, session, "SYSTEM");
                    logger.info("userLeaderBoardDefaultMonth " + userLeaderBoardDefaultMonth);
                }
                ////////////////////////////
                UserChallengeLeaderBoard userLeaderBoardBiWeek = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.BI_WEEK, challengeAttempt.getCategory(),
                        challenge.getDuration(), biWeekStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardWeek != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardBiWeek, session, "SYSTEM");
                    logger.info("userLeaderBoardWeek " + userLeaderBoardBiWeek);
                }
                UserChallengeLeaderBoard userLeaderBoardDefaultBiWeek = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.BI_WEEK, "DEFAULT",
                        challenge.getDuration(), biWeekStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardDefaultWeek != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardDefaultBiWeek, session, "SYSTEM");
                    logger.info("userLeaderBoardDefaultWeek " + userLeaderBoardDefaultBiWeek);
                }

                UserChallengeLeaderBoard userLeaderBoardOverall = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.OVERALL, challengeAttempt.getCategory(),
                        challenge.getDuration(), weekStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardOverall != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardOverall, session, "SYSTEM");
                    logger.info("userLeaderBoardOverall " + userLeaderBoardOverall);
                }
                UserChallengeLeaderBoard userLeaderBoardDefaultOverall = incrementUserChallengeLeaderBoard(
                        challengeAttempt, totalPoints, LeaderBoardType.OVERALL, "DEFAULT",
                        challenge.getDuration(), weekStartTime, session, reprocess, initSuccess, initTimeTaken);
                if (userLeaderBoardDefaultOverall != null) {
                    userChallengeLeaderBoardDAO.update(userLeaderBoardDefaultOverall, session, "SYSTEM");
                    logger.info("userLeaderBoardDefaultOverall " + userLeaderBoardDefaultOverall);
                }
                ////////////////////////////////////////////////
                UserChallengeStatsContextType contType = UserChallengeStatsContextType.CHALLENGE_ATTEMPT;
                if (reprocess) {
                    contType = UserChallengeStatsContextType.RE_PROCESS_ATTEMPT;
                }
                UserChallengeLeaderBoardTransaction statsTransaction = new UserChallengeLeaderBoardTransaction(
                        challengeAttempt.getUserId(), userLeaderBoardDefaultOverall.getId(),
                        contType, challengeAttempt.getId(), totalPoints,
                        userLeaderBoardDefaultOverall.getPoints(), userLeaderBoardDefaultOverall.getTotalAttempts(),
                        userLeaderBoardDefaultOverall.getCorrectAttempts(), challengeAttempt.getHintTakenIndex(),
                        userLeaderBoardDefaultOverall.getMultiplierPowerType(), challengeAttempt.getCategory());
                userChallengeLeaderBoardTransactionDAO.updateAll(Arrays.asList(statsTransaction), session);

                transaction.commit();
                break;
            } catch (Exception e) {
                if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                    logger.info("UserLeaderBoard Rollback: " + e.getMessage());
                    session.getTransaction().rollback();
                }
                if (retry == (NUMBER_OF_TRIES - 1)) {
                    throw e;
                }
            } finally {
                session.close();
            }
        }
        //updating the leader in the challenge
        List<ChallengeAttemptRD> toppers = challengeAttemptDAO.getChallengeLeaderBoard(challenge.getId(), 0, 1);
        if (ArrayUtils.isNotEmpty(toppers)) {
            ChallengeAttemptRD topper = toppers.get(0);
            ChallengeLeaderBoardInfo leader = mapper.map(topper, ChallengeLeaderBoardInfo.class);
            challenge.setLeader(leader);
            challengeDAO.saveChallenge(challenge);
        }
    }

    private int getEarnedChallengePointsAfterHintTaken(int hintTakenIndex,
            Challenge details) {
        logger.info("hintstakenIndex " + hintTakenIndex);
        if (ArrayUtils.isEmpty(details.getQuestions())
                || ArrayUtils.isEmpty(details.getQuestions().get(0).getHints())
                || hintTakenIndex == -1) {
            return details.getMaxPoints();
        }
        List<HintFormat> hints = details.getQuestions().get(0).getHints();
        int deductions = 0;
        for (int i = 0; i <= hintTakenIndex; i++) {
            deductions += hints.get(i).getDeduction();
        }
        logger.info("deductions " + deductions);
        int finalEarnedPoints = details.getMaxPoints() - deductions;
        logger.info("finalEarnedPoints " + finalEarnedPoints);
        return (finalEarnedPoints < 0 ? 0 : finalEarnedPoints);
    }

//    public UserChallengeLeaderBoard incrementUserChallengeLeaderBoard(ChallengeAttempt challengeAttempt,
//    		int totalPoints,LeaderBoardType type,EventCategory category,Integer duration,Long startTime,Session session){
//    	return incrementUserChallengeLeaderBoard(
//				challengeAttempt,totalPoints,type,category,
//				duration,startTime,session,false);
//    }
    public UserChallengeLeaderBoard incrementUserChallengeLeaderBoard(ChallengeAttemptRD challengeAttempt,
            int totalPoints, LeaderBoardType type, String category, Integer duration, Long startTime,
            Session session, boolean reprocess, boolean initSuccess, long initTimeTaken) {

        UserChallengeLeaderBoard leaderBoard = userChallengeLeaderBoardDAO.getByUserId(challengeAttempt.getUserId(), session,
                type, startTime, category);

        if (leaderBoard == null) {
            leaderBoard = new UserChallengeLeaderBoard(challengeAttempt.getUserId(),
                    type, category, MultiplierPowerType.SINGLE, startTime);
            leaderBoard.setHintsCountMap(new ArrayList<>());
            userChallengeLeaderBoardDAO.create(leaderBoard, "SYSTEM");
        }

        leaderBoard.setPoints(leaderBoard.getPoints() + totalPoints);

        if (reprocess) {
            if ((challengeAttempt.isGrace())) {
                if (!initSuccess) {
                    leaderBoard.setCorrectAttempts(leaderBoard.getCorrectAttempts() + 1);
                    leaderBoard.updateHintCount(challengeAttempt.getHintTakenIndex());
                }
                leaderBoard.setTimeTaken(leaderBoard.getTimeTaken() - initTimeTaken);
                leaderBoard.calculateTimeRate();
            } else if (challengeAttempt.success) {
                leaderBoard.setCorrectAttempts(leaderBoard.getCorrectAttempts() + 1);
                leaderBoard.updateHintCount(challengeAttempt.getHintTakenIndex());
            } else if (initSuccess && !challengeAttempt.success) {
                leaderBoard.setCorrectAttempts(leaderBoard.getCorrectAttempts() - 1);
            }
            leaderBoard.calculateStrikeRate();

            ///////
        } else {
            leaderBoard.setTotalAttempts(leaderBoard.getTotalAttempts() + 1);
            if (challengeAttempt.success) {
                leaderBoard.setCorrectAttempts(leaderBoard.getCorrectAttempts() + 1);
                leaderBoard.updateHintCount(challengeAttempt.getHintTakenIndex());
            }
            leaderBoard.calculateStrikeRate();
            leaderBoard.setTimeTaken(leaderBoard.getTimeTaken() + challengeAttempt.getTimeTaken());
            leaderBoard.setTotalDuration(leaderBoard.getTotalDuration() + duration);
            leaderBoard.calculateTimeRate();
        }

        return leaderBoard;
    }

    public void sendDailyChallengeAttemptMail() throws BadRequestException {
        Long currentTime = System.currentTimeMillis();
        Long endTime = getMailEndTime(currentTime);
        Long startTime = endTime - DateTimeUtils.MILLIS_PER_DAY;
        for (EventCategory category : EventCategory.values()) {
            if (EventCategory.DEFAULT.equals(category)) {
                continue;
            }
            //ASYNC Task
            Map<String, Object> payload = new HashMap<>();
            payload.put("category", category.toString());
            payload.put("startTime", startTime);
            payload.put("endTime", endTime);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_MAIL_CHALLENGE_ATTEMPTS, payload);
            asyncTaskFactory.executeTask(params);
        }
    }

    public void sendDailyScoreUserstoSendMail(String category, Long startTime, Long endTime) throws BadRequestException {
        if (category == null || startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "category : " + category + " startTime : " + startTime);
        }
        List<ChallengeUserPoints> userPoints = getDailyScoreUserstoSendMail(category, startTime, endTime);
        Set<Long> userIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(userPoints)) {
            logger.info("attempted mail category " + category + " size " + userPoints.size());
            for (ChallengeUserPoints point : userPoints) {
                if (point.getUserId() != null) {
                    userIds.add(point.getUserId());
                }
            }
            Map<Long, UserBasicInfo> usermap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            for (ChallengeUserPoints point : userPoints) {
                if (point.getUserId() != null && usermap.containsKey(point.getUserId())) {
                    point.setUser(usermap.get(point.getUserId()));
                    point.setTime(startTime);
                    try {
                        communicationManager.sendMailToChallengAttemptedUsers(point);
                    } catch (Exception e) {
                        logger.error("Error in sending mail to userId : " + point.getUserId() + " Exception :" + e);
                    }
                }
            }

        }
    }

    public List<ChallengeUserPoints> getDailyScoreUserstoSendMail(String category, Long startTime, Long endTime) throws BadRequestException {
        if (category == null || startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "category : " + category + " startTime : " + startTime);
        }
        List<ChallengeUserPoints> challengeUserPoints = challengeAttemptDAO.getDailyScoreUsers(category, startTime, endTime);
        return challengeUserPoints;
    }

    public void sendMailToUnattemptedUsers() {
//    	return challengeAttemptDAO.getUsersWhoAttemptedChallenge();
        String url = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT") + "/getUserIdsRegisteredForISL";
        logger.info("sending request to " + url);
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        String output = response.getEntity(String.class);
        Type listType1 = new TypeToken<ArrayList<AttemptedUserIds>>() {
        }.getType();
        List<AttemptedUserIds> userDetails = new Gson().fromJson(output, listType1);

        Set<Long> user = new HashSet<>();
        if (ArrayUtils.isNotEmpty(userDetails)) {

            for (AttemptedUserIds ids : userDetails) {
                if (ids != null && ArrayUtils.isNotEmpty(ids.getAttemptedUserIds())) {
                    logger.info("userDetails category : " + ids.getCategory() + " count :" + ids.getAttemptedUserIds().size());
                    user.addAll(ids.getAttemptedUserIds());
                }
            }
            List<AttemptedUserIds> attemptedUsers = challengeAttemptDAO.getUsersWhoAttemptedChallenge();
            if (ArrayUtils.isNotEmpty(attemptedUsers)) {
                for (AttemptedUserIds attemptIds : attemptedUsers) {
                    Set<Long> attempted = attemptIds.getAttemptedUserIds();
                    if (ArrayUtils.isNotEmpty(user) && ArrayUtils.isNotEmpty(attempted)) {
                        logger.info("userDetails category : " + attemptIds.getCategory() + " count :" + attempted.size());
                        logger.info("Pre - UserDetails size -" + user.size());
                        getIdsNotAttempted(user, attempted);
                        logger.info("Post - UserDetails size -" + user.size());
                    }
                }
            }
        }
        if (ArrayUtils.isNotEmpty(user)) {
            List<UserBasicInfo> usersToMail = fosUtils.getUserBasicInfosFromLongIds(user, true);
            if (ArrayUtils.isNotEmpty(usersToMail)) {
                for (UserBasicInfo userInfo : usersToMail) {
                    try {
                        communicationManager.sendMailToNotAttemptedUsers(userInfo);
                    } catch (Exception e) {
                        logger.error("Error in sending mail to userId : " + userInfo.getUserId() + " Exception :" + e);
                    }
                }
            }
        }
    }

    public boolean getIdsNotAttempted(Set<Long> registered, Set<Long> attempted) {
        return registered.removeAll(attempted);
    }

    public Long getMailEndTime(Long currTime) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        calendar.setTimeInMillis(currTime);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTimeInMillis();
    }

    public void reprocessChallenge(String challengeId, boolean reprocess, ReprocessType reprocessType) throws NotFoundException, ForbiddenException {
        Challenge challenge = challengeDAO.getById(challengeId);
        if (challenge == null) {
            throw new NotFoundException(ErrorCode.CHALLENGE_NOT_FOUND, "challenge not found " + challengeId);
        }
        if (challenge.getReprocess() != null || (ChallengeStatus.ENDED.equals(challenge.getStatus()) && !reprocess)) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_ALREADY_PROCESSED, "challenge already processed ");
        }
        if (!ChallengeStatus.ENDED.equals(challenge.getStatus())) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_STILL_ACTIVE, "challenge is still active ");
        }
        Map<String, Object> payload = new HashMap<>();
        payload.put("challenge", challenge);
        payload.put("reprocess", reprocess);
        payload.put("reprocessType", reprocessType);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.REPROCESS_CHALLENGE, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void populateClanPointsForChallenge(String challengeId) throws NotFoundException, ConflictException {
        Challenge challenge = challengeDAO.getById(challengeId);
        if (challenge == null) {
            throw new NotFoundException(ErrorCode.CHALLENGE_NOT_FOUND, "challenge not found " + challengeId);
        }
        populateClanPointsForChallenge(challenge);
    }

    public void populateClanPointsForChallenge(Challenge challenge) throws ConflictException {
        /*
 * Uncomment method when clanPoints need to be added.
 * Stopped as on Nov 12.
 * 
         */

//    	if(!ChallengeStatus.ENDED.equals(challenge.getStatus())){
//    		throw new ConflictException(ErrorCode.CHALLENGE_STILL_ACTIVE,"Challenge is still active");
//    	}
//    	logger.info("challenge category : " +challenge.getCategory());
//    	List<Clan> clans = clanDAO.getByCategories(challenge.getCategory());
//    	List<String> clanIds = new ArrayList<>();
//    	if(ArrayUtils.isNotEmpty(clans)){
//    		logger.info("Clans size : " +clans.size());
//    		int counter = 0;
//    		for(Clan clan : clans){
//    			counter++;
//    			clanIds.add(clan.getId());
//    			if(counter==clans.size() || counter %100 == 0){
//    				ProcessClanChallengeAttemptsReq req = new ProcessClanChallengeAttemptsReq();
//    				req.setChallengeId(challenge.getId());
//    				req.setClanIds(clanIds);
//    				req.setCategory(challenge.getCategory());
//    				awsSQSManager.sendToSQS(SQSQueue.CHALLENGES_QUEUE, SQSMessageType.PROCESS_CHALLENGE_ATTEMPTS, gson.toJson(req));
//    				clanIds = new ArrayList<>();
//    			}
//    		}
//    	}
    }

    public void addPointsToClanLeaderBoard(ProcessClanChallengeAttemptsReq req) {
        if (StringUtils.isEmpty(req.getChallengeId()) || ArrayUtils.isEmpty(req.getClanIds())) {
            logger.error("Bad RequestException for challengeId : " + req.getChallengeId());
            return;
        }
        for (String clanId : req.getClanIds()) {
            logger.info("incrementing points for clan["
                    + clanId + "] for challenge["
                    + req.getChallengeId() + "]");
            try {
                int points = challengeAttemptDAO.getPointsForClan(clanId, req.getChallengeId());
                ClanPointContextPojo pojo = new ClanPointContextPojo();
                pojo.setContextType(ClanPointsContextType.CHALLENGE_BURST);
                pojo.setBurstPoints(points);
                addPointsToClanLeaderBoard(clanId, req.getChallengeId(), req.getCategory(), pojo, null);
            } catch (Exception e) {
                logger.error("Error in marking the attempt "
                        + clanId + " processed,  error " + e.getMessage());
            }
        }
    }

    //Make sure picturechallengeburst is always the first
    public void addPointsToClanLeaderBoard(String clanId, String challengeId, String category, ClanPointContextPojo points, PictureChallenge picChallenge) {
        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();

        int NUMBER_OF_TRIES = 2;
        for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();

                Long weekStartTime = userLeaderBoardManager.getWeekStartTime(System.currentTimeMillis());
                ClanLeaderBoard weekLeaderBoard = incrementClanLeaderBoard(clanId, session, LeaderBoardType.WEEK,
                        category, weekStartTime, points);
                logger.info("weekLeaderBoard " + weekLeaderBoard);

                ClanLeaderBoard weekDefaultLeaderBoard = incrementClanLeaderBoard(clanId, session, LeaderBoardType.WEEK,
                        "DEFAULT", weekStartTime, points);

                ClanLeaderBoard overallLeaderBoard = incrementClanLeaderBoard(clanId, session, LeaderBoardType.OVERALL,
                        category, weekStartTime, points);
                logger.info("overallLeaderBoard " + overallLeaderBoard);
                ClanLeaderBoard overallDefaultLeaderBoard = incrementClanLeaderBoard(clanId, session, LeaderBoardType.OVERALL,
                        "DEFAULT", weekStartTime, points);
                int totalPoints = points.getMusclePoints() + points.getPicChallengePoints() + points.getBurstPoints();

                ClanLeaderBoardTransaction clanLeaderBoardTransaction = new ClanLeaderBoardTransaction(clanId, overallDefaultLeaderBoard.getId(),
                        points.getContextType(), challengeId, category, totalPoints, overallDefaultLeaderBoard.getPoints(),
                        points.getBurstPoints(), overallDefaultLeaderBoard.getBurstChallengePoints(), points.getMusclePoints(),
                        overallDefaultLeaderBoard.getMusclePoints(), points.getPicChallengePoints(),
                        overallDefaultLeaderBoard.getPictureChallengePoints());

                clanLeaderBoardTransactionDAO.updateAll(Arrays.asList(clanLeaderBoardTransaction), session);
                if (ClanPointsContextType.CHALLENGE_BURST.equals(points.getContextType())) {
                    incrementPictureChallengePoints(clanId, category, session, points, challengeId, null);
                } else {
                    incrementPictureChallengePoints(clanId, category, session, points, challengeId, picChallenge);
                }
                logger.info("incrementing points for challengeId : " + challengeId + " with context : " + points.getContextType() + "and points : " + totalPoints);

                transaction.commit();
                break;
            } catch (Exception e) {
                if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                    logger.info("ClanLeaderBoard Rollback: " + e.getMessage());
                    session.getTransaction().rollback();
                }
                if (retry == (NUMBER_OF_TRIES - 1)) {
                    throw e;
                }
            } finally {
                session.close();
            }
        }
    }

    // CHALLENGE_BURST, MUSCLE_POINTS, PICTURE_CHALLENGE_BURST
    public ClanLeaderBoard incrementClanLeaderBoard(String clanId, Session session, LeaderBoardType type, String category,
            Long startTime, ClanPointContextPojo points) {
        ClanLeaderBoard leaderBoard = clanLeaderBoardDAO.getByClanId(clanId, session, type, startTime, category);

        if (leaderBoard == null) {
            leaderBoard = new ClanLeaderBoard(clanId, category, type, startTime, 0, 0, 0, 0);
        }
        int totalPoints = points.getMusclePoints() + points.getPicChallengePoints() + points.getBurstPoints();
        leaderBoard.setMusclePoints(leaderBoard.getMusclePoints() + points.getMusclePoints());
        leaderBoard.setPictureChallengePoints(leaderBoard.getPictureChallengePoints() + points.getPicChallengePoints());
        leaderBoard.setBurstChallengePoints(leaderBoard.getBurstChallengePoints() + points.getBurstPoints());

        leaderBoard.setPoints(leaderBoard.getPoints() + totalPoints);
        clanLeaderBoardDAO.save(leaderBoard, session, "SYSTEM");
        return leaderBoard;

    }

    public void incrementPictureChallengePoints(String clanId, String category, Session session, ClanPointContextPojo points, String challengeId, PictureChallenge picChallenge) {
        //TODO : Fetch active picturechallengeAttempt/pictureChallenge and increment it
        String contextId = challengeId;
        List<PictureChallenge> picChallenges = new ArrayList<>();
        if (ClanPointsContextType.CHALLENGE_BURST.equals(points.getContextType())) {
            picChallenges = pictureChallengeDAO.getPictureChallenges(0, 20,
                    ChallengeStatus.ACTIVE, "", "", null, null, category, "");
        } else {
            picChallenges.add(picChallenge);
        }

        if (ArrayUtils.isNotEmpty(picChallenges)) {
            for (PictureChallenge challenge : picChallenges) {
                PictureChallengeLeaderBoard leaderBoard = pictureChallengeLeaderBoardDAO.getByClanId(clanId, session, challenge.getId());
                if (leaderBoard == null) {
                    leaderBoard = new PictureChallengeLeaderBoard(clanId, challenge.getId(), category);
                }
                int totalPoints = points.getMusclePoints() + points.getPicChallengePoints() + points.getBurstPoints();
                leaderBoard.setMusclePoints(leaderBoard.getMusclePoints() + points.getMusclePoints());
                leaderBoard.setPictureChallengePoints(leaderBoard.getPictureChallengePoints() + points.getPicChallengePoints());
                leaderBoard.setBurstChallengePoints(leaderBoard.getBurstChallengePoints() + points.getBurstPoints());

                leaderBoard.setPoints(leaderBoard.getPoints() + totalPoints);
                pictureChallengeLeaderBoardDAO.save(leaderBoard, session, "SYSTEM");

                logger.info("incrementing picture challenge points for challengeId : " + contextId + " with context : " + points.getContextType() + "and points : " + totalPoints);
                PictureChallengeLeaderBoardTransaction transaction = new PictureChallengeLeaderBoardTransaction(clanId, leaderBoard.getId(),
                        points.getContextType(), contextId, category, totalPoints, leaderBoard.getPoints(),
                        points.getBurstPoints(), leaderBoard.getBurstChallengePoints(), points.getMusclePoints(),
                        leaderBoard.getMusclePoints(), points.getPicChallengePoints(),
                        leaderBoard.getPictureChallengePoints());
                pictureChallengeLeaderBoardTransactionDAO.updateAll(Arrays.asList(transaction), session);
            }
        }
    }
    /*

    @Deprecated
    public void challengeLeaderBoardProcessor(Challenge challenge) {
        String challengeId = challenge.getId();
        boolean process = true;
        int start = 0;
        while (process) {
            Query query = new Query();
            query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(challenge.getId()));
            query.addCriteria(Criteria.where(ConstantsGlobal.SUCCESS).is(true));
            challengeAttemptDAO.setFetchParameters(query, start, ChallengeUtilCommon.DEFAULT_BATCH_SIZE);
            List<ChallengeAttempt> challengeAttempts = challengeAttemptDAO.runQuery(query,
                    ChallengeAttempt.class, Arrays.asList(
                            ConstantsGlobal.CHALLENGE_ID, ConstantsGlobal.USER_ID,
                            ConstantsGlobal.HINT_TAKEN_INDEX, ConstantsGlobal.TIME_TAKEN));

            if (ArrayUtils.isNotEmpty(challengeAttempts)) {
                start += challengeAttempts.size();
                if (challengeAttempts.size() < MIN_SUCCESSFULL_ATTEMPT) {
                    logger.info("challeneg[" + challenge.getId() + "] has only["
                            + start
                            + "] successful attempts hence not creating leaderBoard ");
                    break;
                }
                logger.info("challengeTakens to process " + challengeAttempts.size());
                for (ChallengeAttempt challengeAttempt : challengeAttempts) {
                    ChallengeLeaderBoard leaderBoard = challengeLeaderBoardDAO
                            .getByUserId(challengeAttempt.getUserId(), challengeId);
                    if (leaderBoard == null) {
                        leaderBoard = new ChallengeLeaderBoard(challengeAttempt.userId,
                                challengeAttempt.challengeId, challengeAttempt.timeTaken,
                                challengeAttempt.hintTakenIndex);
                        challengeLeaderBoardDAO.save(leaderBoard);
                    }
                    logger.info("saved challenge[" + challengeAttempt.challengeId
                            + "] leader board info for user[" + challengeAttempt.userId
                            + "]");
                }
            } else {
                process = false;
            }
        }
        challengeRankCalculatorProcessor(challenge);
    }

    @Deprecated
    //calculating the ranks for the particular challenge for each user
    private void challengeRankCalculatorProcessor(Challenge challenge) {
        boolean process = true;
        int start = 0;
        int rank = 0;
        long previousRanker = -1;
        List<Long> topperIds = new ArrayList<>();
        while (process) {
            Query query = new Query();
            query.addCriteria(Criteria.where(ChallengeAttempt.Constants.CHALLENGEID).is(challenge.getId()));
            challengeLeaderBoardDAO.setFetchParameters(query, start, ChallengeUtilCommon.DEFAULT_BATCH_SIZE);
            query.with(new Sort(Sort.Direction.ASC, ConstantsGlobal.RANKER));
            List<ChallengeLeaderBoard> challengesLeaderBoards = challengeLeaderBoardDAO.runQuery(query,
                    ChallengeLeaderBoard.class);
            if (ArrayUtils.isNotEmpty(challengesLeaderBoards)) {
                logger.info("challengesLeaderBoards to process " + challengesLeaderBoards.size());
                for (ChallengeLeaderBoard challengeLeaderBoard : challengesLeaderBoards) {
                    if (previousRanker != challengeLeaderBoard.ranker) {
                        rank++;
                    }
                    challengeLeaderBoard.rank = rank;
                    if (challengeLeaderBoard.rank == 1) {
                        topperIds.add(challengeLeaderBoard.userId);
                    }
                    previousRanker = challengeLeaderBoard.ranker;
                    challengeLeaderBoardDAO.save(challengeLeaderBoard);
                    logger.info("saved challengeLeaderBoard["
                            + challengeLeaderBoard.challengeId + "] rank[" + rank
                            + "] for user[" + challengeLeaderBoard.userId + "]");
                }
                start += challengesLeaderBoards.size();
            } else {
                process = false;
            }
        }

        if (CollectionUtils.isNotEmpty(topperIds)) {
            logger.info("saving topperIds[" + topperIds + "], for challenge["
                    + challenge.getId() + "]");
            challenge.setTopperIds(topperIds);
            challengeDAO.saveChallenge(challenge);
        }

//        challengePointProcessor(challenge);
    }

    //this update challenge weekly, monthly, overall leaderboard
    @Deprecated
    private void updateUserRankBuckets(ChallengeAttempt challengeAttempt, int totalPoints) {
        for (RankType rankType : RankType.values()) {

            ChallengeUserInfo challengeUserInfo = challengeUserInfoDAO
                    .getChallengeUserInfo(challengeAttempt.userId, rankType);

            logger.info("current " + rankType.name() + " points of user["
                    + challengeAttempt.userId + "] [" + challengeUserInfo.points
                    + "], and total points earned  for challenge["
                    + challengeAttempt.challengeId + "]: " + totalPoints);

            challengeUserInfo.updateHintCount(challengeAttempt.hintTakenIndex);
            challengeUserInfo.points += totalPoints;
            challengeUserInfo.correctAttempts++;
            challengeUserInfo.calculateStrikeRate();
            challengeUserInfoDAO.save(challengeUserInfo);
            if (rankType == RankType.OVERALL) {
                // this will update the rankBucket for challengeUserInfo
                // TODO: this might not need when new model from mongo query are
                // used
                globalChallengeRankCalculator(challengeUserInfo);
            }

        }
    }

    @Deprecated
    private void globalChallengeRankCalculator(ChallengeUserInfo challengeUserInfo) {
        Query query = new Query();
        query.addCriteria(Criteria.where("minPoint").lt(challengeUserInfo.points));
        query.addCriteria(Criteria.where("maxPoint").gte(challengeUserInfo.points));

        ChallengeRankBucket challengeRankBucket = challengeRankBucketDAO.findOne(query, ChallengeRankBucket.class);
        if (challengeRankBucket == null) {
            challengeRankBucket = createNewChallengeRankBucket(challengeUserInfo.points);
        }
        if (StringUtils.isEmpty(challengeUserInfo.rankBucketId)) {
            challengeUserInfo.bucketNo = challengeRankBucket.bucketNo;
            challengeUserInfo.rankBucketId = challengeRankBucket.getId();
            challengeRankBucket.size++;
            challengeRankBucketDAO.save(challengeRankBucket);
            challengeUserInfoDAO.save(challengeUserInfo);
        } else if (!StringUtils.equals(challengeRankBucket.getId(),
                challengeUserInfo.rankBucketId)) {
            // move the user from one bucket to other
            logger.info("moving user[" + challengeUserInfo.userId
                    + "] form bucket[" + challengeUserInfo.rankBucketId + "] to bucket["
                    + challengeRankBucket.getId() + "]");
            Query query2 = new Query();
            query2.addCriteria(Criteria.where(AbstractMongoEntity.Constants._ID)
                    .is(challengeUserInfo.rankBucketId));
            Update update = new Update();
            update.inc(ConstantsGlobal.SIZE, -1);
            challengeRankBucketDAO.updateFirst(query2, update, ChallengeRankBucket.class);

            challengeUserInfo.rankBucketId = challengeRankBucket.getId();
            challengeUserInfo.bucketNo = challengeRankBucket.bucketNo;
            Query query3 = new Query();
            query3.addCriteria(Criteria.where(AbstractMongoEntity.Constants._ID)
                    .is(challengeUserInfo.rankBucketId));
            Update update1 = new Update();
            update.inc(ConstantsGlobal.SIZE, 1);
            challengeRankBucketDAO.updateFirst(query3, update1, ChallengeRankBucket.class);
            challengeUserInfoDAO.save(challengeUserInfo);
        }

    }

    @Deprecated
    private ChallengeRankBucket createNewChallengeRankBucket(long userPoint) {
        int basketNo = (int) (userPoint / RANK_BASKED_POINT_SIZE);
        long min = basketNo == 0 ? 0 : (basketNo * RANK_BASKED_POINT_SIZE) + 1;
        long max = min + RANK_BASKED_POINT_SIZE;
        String name = "RankBasket-" + basketNo + ": " + min + "-" + max;
        ChallengeRankBucket challengeRankBucket = new ChallengeRankBucket(min, max, name);
        challengeRankBucket.bucketNo = basketNo + 1;
        challengeRankBucketDAO.save(challengeRankBucket);
        logger.info("created new challenge rank basket : " + challengeRankBucket);
        return challengeRankBucket;
    }
     */
 /*    public ChallengeLeaderBoardRes getChallengeLeaderBoardForDuration(Long userId, LeaderBoardType type,
            Integer start, Integer size, Boolean addMyRank, EventCategory category) throws BadRequestException {
        if (start == null) {
            start = 0;
        }
        if (size == null) {
            size = 20;
        }
        List<String> typeOfLeaderBoard = getTypeString(type);
        String pointString = typeOfLeaderBoard.get(0);
        String timeString = typeOfLeaderBoard.get(1);
        UserLeaderBoard userBoard = null;
        if (userId != null) {
            userBoard = userLeaderBoardDAO.getByUserId(userId);
        }
        EventCategory cat = category;
        if (userBoard != null && userBoard.getCategory() != null) {
            cat = userBoard.getCategory();
        }
        if (cat == null) {
            UserBasicInfo user = fosUtils.getUserBasicInfo(userId);
            if (user != null && StringUtils.isNotEmpty(user.getGrade())) {
                try {
                    Integer grade = Integer.parseInt(user.getGrade());
                    if (grade >= 3 && grade <= 5) {
                        cat = (EventCategory.ISL_2017_CAT_3_5);
                    } else if (grade >= 6 && grade <= 8) {
                        cat = (EventCategory.ISL_2017_CAT_6_8);
                    } else if (grade >= 9) {
                        cat = (EventCategory.ISL_2017_CAT_9_10);
                    }
                } catch (NumberFormatException e) {    				//
                    throw new BadRequestException(ErrorCode.USER_GRADE_NOT_FOUND, "User grade not found in table for " + userId);
                }
            } else {
                throw new BadRequestException(ErrorCode.USER_NOT_FOUND, "User not found in table for " + userId);
            }
        }
        List<UserLeaderBoard> leaderBoards
                = userLeaderBoardDAO.getUserLeaderBoardForCategory(cat, pointString, timeString, start, size);

        List<ChallengeLeaderBoardInfo> leaders = new ArrayList<>();
        List<Long> userIds = new ArrayList<>();
        boolean partOfLeaderBoard = false;
        if (ArrayUtils.isNotEmpty(leaderBoards)) {
            int count = 1;
            for (UserLeaderBoard leaderBoard : leaderBoards) {
                userIds.add(leaderBoard.getUserId());
                if (leaderBoard.getUserId().equals(userId)) {
                    partOfLeaderBoard = true;
                }
                ChallengeLeaderBoardInfo leader = new ChallengeLeaderBoardInfo();
                getChallengeLeaderBoardInfo(leader, leaderBoard, type);
                leader.setRank(count + start);
                leader.setUserId(leaderBoard.getUserId());
                count++;
                leaders.add(leader);
            }
        }

        if (addMyRank && !partOfLeaderBoard && userBoard != null) {
            ChallengeLeaderBoardInfo leader = new ChallengeLeaderBoardInfo();
            getChallengeLeaderBoardInfo(leader, userBoard, type);
            Long myRank = userLeaderBoardDAO.getUserRank(cat, pointString, (long) leader.getTotalPoints(), timeString, leader.getTimeTaken());
            if (myRank != null) {
                userIds.add(userId);
                leader.setRank(myRank.intValue());
                leader.setUserId(userBoard.getUserId());
                if (!leaders.isEmpty()) {
                    leaders.remove(leaders.size() - 1);
                }
                leaders.add(leader);
            }
        }
        Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds);
        for (ChallengeLeaderBoardInfo leader : leaders) {
            leader.setUser(usersMap.get(leader.getUserId()));
        }
        ChallengeLeaderBoardRes res = new ChallengeLeaderBoardRes();
        res.setList(leaders);
        return res;
    }

    public void getChallengeLeaderBoardInfo(ChallengeLeaderBoardInfo info, UserLeaderBoard user, LeaderBoardType type) {
        switch (type) {
            case CURRENT_WEEK:
                info.setTotalPoints((int) user.getCurrWeekPoints());
                info.setTimeTaken(user.getCurrWeekTime());
                break;
            case CURRENT_MONTH:
                info.setTotalPoints((int) user.getCurrMonthPoints());
                info.setTimeTaken(user.getCurrMonthTime());
                break;
            case PREVIOUS_WEEK:
                info.setTotalPoints((int) user.getPrevWeekPoints());
                info.setTimeTaken(user.getPrevWeekTime());
                break;
            case PREVIOUS_MONTH:
                info.setTotalPoints((int) user.getPrevMonthPoints());
                info.setTimeTaken(user.getPrevMonthTime());
                break;
            case GLOBAL:
                info.setTotalPoints((int) user.getTotalPoints());
                info.setTimeTaken(user.getTotalTime());
                break;
            default:
                info.setTotalPoints((int) user.getCurrWeekPoints());
                info.setTimeTaken(user.getCurrWeekTime());
                break;
        }
    }

    public List<String> getTypeString(LeaderBoardType type) {
        List<String> list = new ArrayList<>();
        switch (type) {
            case CURRENT_WEEK:
                list.add("currWeekPoints");
                list.add("currWeekTime");
                break;
            case CURRENT_MONTH:
                list.add("currMonthPoints");
                list.add("currMonthTime");
                break;
            case PREVIOUS_WEEK:
                list.add("prevWeekPoints");
                list.add("prevWeekTime");
                break;
            case PREVIOUS_MONTH:
                list.add("prevMonthPoints");
                list.add("prevMonthTime");
                break;
            case GLOBAL:
                list.add("totalPoints");
                list.add("totalTime");
                break;
            default:
                list.add("currWeekPoints");
                list.add("currWeekTime");
                break;
        }
        return list;
    }

    public void movePointsOfLeaderBoard() {
        boolean week = false;
        boolean month = false;
        int day = getDayOfTheWeek(System.currentTimeMillis());
        if (day == 1) {
            week = true;
        } else {
            logger.info("Not Changing of Week");
        }
        int dayMonth = getDayOfTheMonth(System.currentTimeMillis());
        if (dayMonth == 1) {
            month = true;
        } else {
            logger.info("Not Changing of Month");
        }
        if (!week && !month) {
            return;
        }
        int start = 0;
        int size = 100;
        List<UserLeaderBoard> leaderBoards = new ArrayList<>();

        while (true) {
            List<UserLeaderBoard> userLeaderBoards = userLeaderBoardDAO.getAllUserLeaderBoards(start, size);
            if (ArrayUtils.isEmpty(userLeaderBoards)) {
                break;
            }
            start = start + size;
            if (week) {
                shiftPointsForAllWeek(userLeaderBoards);
            }
            if (month) {
                shiftPointsForAllMonth(userLeaderBoards);
            }
            userLeaderBoardDAO.updateAll(userLeaderBoards, "SYSTEM");
        }
    }

    public void shiftPointsForAllWeek(List<UserLeaderBoard> userLeaderBoards) {

        if (ArrayUtils.isNotEmpty(userLeaderBoards)) {
            for (UserLeaderBoard leaderBoard : userLeaderBoards) {
                leaderBoard.setPrevWeekPoints(leaderBoard.getCurrWeekPoints());
                leaderBoard.setPrevWeekTime(leaderBoard.getCurrWeekTime());
                leaderBoard.setCurrWeekPoints(0);
                leaderBoard.setCurrWeekTime(0);
            }
        }
    }

    public void shiftPointsForAllMonth(List<UserLeaderBoard> userLeaderBoards) {

        if (ArrayUtils.isNotEmpty(userLeaderBoards)) {
            for (UserLeaderBoard leaderBoard : userLeaderBoards) {
                leaderBoard.setPrevMonthPoints(leaderBoard.getCurrMonthPoints());
                leaderBoard.setPrevMonthTime(leaderBoard.getCurrMonthTime());
                leaderBoard.setCurrMonthPoints(0);
                leaderBoard.setCurrMonthTime(0);
            }
        }
    }

    public int getDayOfTheMonth(Long currentTime) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        calendar.setTimeInMillis(currentTime);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        return day;
    }

    public int getDayOfTheWeek(Long currentTime) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
        calendar.setTimeInMillis(currentTime);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return day;
    }
     */

}
