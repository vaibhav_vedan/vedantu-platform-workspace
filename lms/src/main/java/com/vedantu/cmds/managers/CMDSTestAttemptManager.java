package com.vedantu.cmds.managers;

import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.EventName;
import com.vedantu.User.enums.SubRole;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.board.pojo.Board;
import com.vedantu.cmds.dao.CMDSQuestionDAO;
import com.vedantu.cmds.dao.CMDSTestAttemptDAO;
import com.vedantu.cmds.dao.CMDSTestDAO;
import com.vedantu.cmds.dao.QuestionAttemptDAO;
import com.vedantu.cmds.dao.TestUserDAO;
import com.vedantu.cmds.dao.TopicTreeDAO;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.cmds.entities.TestUser;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.enums.QuestionAttemptContext;
import com.vedantu.cmds.fs.parser.QuestionRepositoryDocParser;
import com.vedantu.cmds.managers.challenges.CommunicationManager;
import com.vedantu.cmds.pojo.CategoryQuestions;
import com.vedantu.cmds.pojo.ContentDetails;
import com.vedantu.cmds.pojo.CustomSectionEvaluationRule;
import com.vedantu.cmds.pojo.Option;
import com.vedantu.cmds.pojo.PerformanceReportSection;
import com.vedantu.cmds.pojo.QuestionAnalytics;
import com.vedantu.cmds.pojo.QuestionResponse;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.cmds.pojo.TestMarksPercentageSection;
import com.vedantu.cmds.pojo.TestMarksSection;
import com.vedantu.cmds.pojo.TestMetadata;
import com.vedantu.cmds.response.GetLeaderboardInfo;
import com.vedantu.cmds.response.PerformanceReportStats;
import com.vedantu.cmds.response.StartTestResponse;
import com.vedantu.cmds.response.StudentScoreInfo;
import com.vedantu.cmds.response.TestChartStatsRes;
import com.vedantu.cmds.response.TestTableStatsRes;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.lms.cmds.enums.AttemptStateOfQuestion;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.ChangeQuestionActionType;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.LMSType;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.TestEndType;
import com.vedantu.lms.cmds.enums.TestResultVisibility;
import com.vedantu.lms.cmds.enums.UserType;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.pojo.AnalyticsTypes;
import com.vedantu.lms.cmds.pojo.AnswerChangeRecord;
import com.vedantu.lms.cmds.pojo.CMDSTestAttemptImage;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.lms.cmds.pojo.CategoryAnalytics;
import com.vedantu.lms.cmds.pojo.QuestionAttemptDetails;
import com.vedantu.lms.cmds.pojo.QuestionAttemptState;
import com.vedantu.lms.cmds.pojo.QuestionChangeRecord;
import com.vedantu.lms.cmds.pojo.TestUserInfo;
import com.vedantu.lms.cmds.pojo.TestProgressCache;
import com.vedantu.lms.cmds.request.BulkSubmitQuestionAttemptRequest;
import com.vedantu.lms.cmds.request.CMDSEvaluateTestAttemptReq;
import com.vedantu.lms.cmds.request.CMDSLinkAttemptToUserReq;
import com.vedantu.lms.cmds.request.CMDSStartTestAttemptReq;
import com.vedantu.lms.cmds.request.CMDSSubmitTestAttemptReq;
import com.vedantu.lms.cmds.request.TestFeedbackRequest;
import com.vedantu.lms.cmds.request.TestProgressCacheRequest;
import com.vedantu.lms.request.AddOrEditTestUserReq;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.pojo.PostChangeAnswerSQSPojo;
import com.vedantu.moodle.pojo.PostChangeQuestionSQSPojo;
import com.vedantu.moodle.pojo.SectionResultInfo;
import com.vedantu.moodle.pojo.UpdateTestAttemptPojoForTestReevaluation;
import com.vedantu.moodle.pojo.UpdateTestAttemptSQSPojoForChangeAnswer;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CMDSTestAttemptManager {

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private CMDSTestManager cmdsTestManager;

    @Autowired
    private CMDSTestAttemptDAO cmdsTestAttemptDAO;

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    private ContentInfoDAO contentInfoDAO;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private AmazonS3Manager s3Manager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private CMDSQuestionDAO cmdsQuestionDAO;

    @Autowired
    private QuestionAttemptDAO questionAttemptDAO;

    @Autowired
    private TestUserDAO testUserDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private QuestionRepositoryDocParser questionRepositoryDocParser;

    @Autowired
    private CMDSTestDAO testDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private TopicTreeDAO topicTreeDAO;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private RedisDAO redisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSTestAttemptManager.class);

    public static final String AWS_CMDS_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
    public static final String AWS_QUESTION_SET_FOLDER = "question-sets";
    public static final String AWS_CMDS_ANSWERS_FOLDER = "answers";
    private static final Gson gson = new Gson();
    private static final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");
    public static final String TIME_ZONE_IN = "Asia/Kolkata";

    private static final int BUCKET_COUNT = 5;
    private static final int TABLE_BUCKET_COUNT = 6;
    private static final int STEP_PCT = 15;
    private static final int LOWEST_MIN_PCT = 0;
    private static final int MAX_FETCH_SIZE = 250;
    private static final boolean DRAFT_STATE_DEFAULT = false;

    @Autowired
    private DozerBeanMapper mapper;

    public TestUserInfo addOrEditTestUser(AddOrEditTestUserReq addOrEditTestUserReq) {
        TestUser testUser = testUserDAO.addOrEditTestUser(addOrEditTestUserReq);
        TestUserInfo response = mapper.map(testUser, TestUserInfo.class);
        return response;
    }

    public StartTestResponse startCMDSTest(CMDSStartTestAttemptReq req)
            throws MalformedURLException, VException {

        if (StringUtils.isEmpty(req.getContentInfoId()) && Objects.nonNull(req.getCallingUserId())) { //public test registered user
            UserBasicInfo studentInfo = fosUtils.getUserBasicInfo(req.getCallingUserId(), false);
            req.setStudentInfo(studentInfo);
        }

        // Getting or creating the content info entry according to the condition
        logger.info("Creating content info...");
        ContentDetails contentDetails = getContentInfoForTestAttemptAndPopulateDetails(req);

        // Initialize all the required variables
        Boolean isExpired = Boolean.FALSE;
        Long expiryDate = null;
        ContentInfo contentInfo = contentDetails.getContentInfo();
        CMDSTest cmdsTest = contentDetails.getCmdsTest();
        String userId = contentDetails.getUserId();

        // Validating the contentInfo object formed or retrieved from the database
        logger.info("Validation of contentInfo details...");
        validateContentInfoDetails(contentInfo, req);

        // Get the metadata(or say details of the shared/attempted test) from the contentInfo collection
        logger.info("Getting test's metadata from contentInfo collection data");
        TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();

        // Populate test id and test details conditionally
        logger.info("Populating test id and test details");
        String testId;
        testId = testContentInfoMetadata.getTestId();
        if (StringUtils.isEmpty(testId)) {
            testId = cmdsTestManager.getCMDSTestIdByContentUrl(contentInfo);
        }
        if (Objects.isNull(cmdsTest)) {
            cmdsTest = cmdsTestManager.getCMDSTestById(testId);
        }

        // Validate the test response
        logger.info("Validating test response");
        int buffer = 0;
        if (ContentInfoType.SUBJECTIVE.equals(contentInfo.getContentInfoType())
                || ContentInfoType.MIXED.equals(contentInfo.getContentInfoType())) {
            buffer = DateTimeUtils.MILLIS_PER_HOUR;
        }
        CMDSTestAttempt cmdsTestAttempt = validateTestAttemptAndReturnOngoingTest(testContentInfoMetadata, buffer, contentInfo.getId());

        /**
         * <p>Algorithm:</p>
         * Check if the test is subjective or not and in case it is then ensure
         * that the correct value is returned to the front-end so that they can
         * either chose to let the user to attempt the test accordingly.
         *
         * Business Requirement: As for subjective test, teacher has to check
         * them manually so it is better to give a timeline to student to
         * attempt the test. If they don't attempt within that timeline then
         * just let them see the question but don't upload the answer.
         */
        if (ContentInfoType.SUBJECTIVE.equals(cmdsTest.getContentInfoType())) {
            if (AccessLevel.PUBLIC.equals(cmdsTest.getAccessLevel()) && Objects.nonNull(cmdsTest.getExpiryDate())) {
                expiryDate = cmdsTest.getExpiryDate();
                if (System.currentTimeMillis() > cmdsTest.getExpiryDate()) {
                    isExpired = Boolean.TRUE;
                }
            } else if (AccessLevel.PRIVATE.equals(cmdsTest.getAccessLevel()) && Objects.nonNull(cmdsTest.getExpiryDays())) {
                expiryDate = cmdsTest.getExpiryDays() + contentInfo.getCreationTime();
                if (System.currentTimeMillis() > cmdsTest.getExpiryDays() + contentInfo.getCreationTime()) {
                    isExpired = Boolean.TRUE;
                }
            }
        }

        // In case it is a fresh attempt create a fresh attempt out of the required information
        if (Objects.isNull(cmdsTestAttempt)) {
            logger.info("Creating attempt details in case of fresh attempts");
            cmdsTestAttempt = new CMDSTestAttempt(contentInfo, cmdsTest);

            // Populate the data inside contentInfoManager only if the test is attempted
            if (!isExpired) {
                contentInfoManager.updateContent(contentInfo);
                int attemptIndex=cmdsTestAttemptDAO.getNoOfAttemptsForContentInfo(contentInfo.getId())+1;

                cmdsTestAttempt.setContentInfoId(contentInfo.getId());
                cmdsTestAttempt.setStudentId(contentInfo.getStudentId());
                cmdsTestAttempt.setContentInfoType(contentInfo.getContentInfoType());
                Set<String> contentInfoSubjects = contentInfo.getSubjects();
                if(ArrayUtils.isEmpty(contentInfoSubjects) && ArrayUtils.isNotEmpty(contentInfo.getBoardIds())){
                    Map<Long, Board> boardInfoMap = fosUtils.getBoardInfoMap(contentInfo.getBoardIds());
                    contentInfoSubjects=
                            Optional.ofNullable(boardInfoMap.values())
                                    .map(Collection::stream)
                                    .orElseGet(Stream::empty)
                                    .map(Board::getName)
                                    .collect(Collectors.toSet());

                }
                cmdsTestAttempt.setSubjects(contentInfoSubjects);
                cmdsTestAttempt.setAttemptIndex(attemptIndex);
                cmdsTestAttemptDAO.save(cmdsTestAttempt);
            }
        }

        // Populate the return response according to the attempt and test information
        logger.info("Populating response object.");
        StartTestResponse response = createStartTestResponse(cmdsTestAttempt, cmdsTest);
        response.setExpired(isExpired);
        response.setExpiryDate(expiryDate);
        if (StringUtils.isNotEmpty(userId)) {
            response.setNotRegisteredUserId(userId);
        }

        // Comment the logs when putting in production(only use it for debugging in test environments)
        /*
        logger.info("req: "+req);
        logger.info("contentInfo: "+contentInfo);
        logger.info("cmdsTest: "+cmdsTest);
        logger.info("userId: "+userId);
        logger.info("testId: "+testId);
        logger.info("cmdsTestAttempt: "+cmdsTestAttempt);
        logger.info("expiryDate: "+expiryDate);
        logger.info("isExpired: "+isExpired);
        logger.info("response: "+response);
         */
        // return the response
        try {

            List<String> reviseJeeTestIds = redisDAO.getList("REVISE_TARGET_JEE_NEET_TEST_IDS", 0, 200);

            if (reviseJeeTestIds != null && reviseJeeTestIds.contains(req.getTestId())) {
                JSONObject j = new JSONObject();
                j.put("secretKey", "dP98/mBH}yWwWQ=<");
                j.put("testId", req.getTestId());
                j.put("eventName", EventName.TARGET_JEE_NEET);
                j.put("userId", req.getCallingUserId());
                awsSNSManager.triggerSNS(SNSTopic.TARGET_JEE_NEET_POSTTEST, SNSTopic.TARGET_JEE_NEET_POSTTEST.name(), j.toString());
            }

        } catch (Exception e) {
            logger.warn("error in sending revise Jee inc to sns " + e.getMessage());
        }

        return response;
    }

    private void validateContentInfoDetails(ContentInfo contentInfo, CMDSStartTestAttemptReq req)
            throws ForbiddenException, BadRequestException, ConflictException {

        if (UserType.REGISTERED.equals(contentInfo.getUserType())
                && (Objects.isNull(req.getCallingUserId())
                || !String.valueOf(req.getCallingUserId()).equals(contentInfo.getStudentId()))) {
            logger.warn("Illegal resource access : userId -" + req.getCallingUserId() + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for the current role");
        }
        if (!LMSType.CMDS.equals(contentInfo.getLmsType())) {
            logger.error("LMS type is not CMDS - req : " + req.toString() + " contentInfo : " + contentInfo.toString());
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Requested content is not part of CMDS");
        }
        if (ContentType.TEST != contentInfo.getContentType() && ContentType.ASSIGNMENT != contentInfo.getContentType()) {
            throw new ConflictException(ErrorCode.INVALID_CONTENT_TYPE, "Content type is not test");
        }
    }

    private ContentDetails getContentInfoForTestAttemptAndPopulateDetails(CMDSStartTestAttemptReq req)
            throws ForbiddenException, BadRequestException, MalformedURLException, InternalServerErrorException {
        ContentInfo contentInfo;
        CMDSTest cmdsTest = null;
        String userId = null;

        if (StringUtils.isEmpty(req.getContentInfoId())) {
            logger.info("It is a public test since the content info id is not present");
            String testId = req.getTestId();
            cmdsTest = cmdsTestManager.getCMDSTestById(testId);

            if (Objects.isNull(cmdsTest)) {
                throw new BadRequestException(ErrorCode.INVALID_TEST_ID, testId + " is an invalid test id.");
            }

            if (AccessLevel.PRIVATE.equals(cmdsTest.getAccessLevel())) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Content is private");
            }

            if (Objects.nonNull(req.getStudentInfo())) {
                logger.info("Since the student info exists so the user is a registered one i.e. entry is already in the system.");
                String studentId = req.getStudentInfo().getUserId().toString();

                // Check if already an attempt is present for the same user and test
                contentInfo=contentInfoDAO.getLatestContentInfoForTestForStudent(studentId,cmdsTest.getId(),null);

                // In case no attempt is present, then create one
                if (Objects.isNull(contentInfo)) {
                    logger.info("Attempt is not present for the user for the test, so creating new one");
                    if (ContentInfoType.SUBJECTIVE.equals(cmdsTest.getContentInfoType())) {
                        logger.info("Creating attempt for subjective test");

                        // Usually subjective tests are private, if they are public then the creator is taken as the evaluator
                        UserBasicInfo evaluatorInfo = fosUtils.getUserBasicInfo(cmdsTest.getCreatedBy(), true);

                        contentInfo = contentInfoManager.createCMDSContentInfo(
                                UserType.REGISTERED,
                                studentId,
                                req.getStudentInfo().getFullName(),
                                evaluatorInfo,
                                null,
                                cmdsTest,
                                null,
                                null,
                                req.getContextType(),
                                req.getContextId());

                    } else {
                        logger.info("Creating attempt for non subjective test, usually objective one.");
                        contentInfo = contentInfoManager.createCMDSContentInfo(
                                UserType.REGISTERED,
                                studentId,
                                req.getStudentInfo().getFullName(),
                                null,
                                null,
                                cmdsTest,
                                null,
                                null,
                                req.getContextType(),
                                req.getContextId());
                    }
                }else{
                    logger.info("Attempt already present for the user for that test.");
                }
            } else {
                userId = req.getNotRegisteredUserId();
                String name = null;
                if (StringUtils.isEmpty(userId)) {
                    AddOrEditTestUserReq addOrEditTestUserReq = new AddOrEditTestUserReq();
                    TestUser testUser = testUserDAO.addOrEditTestUser(addOrEditTestUserReq);
                    userId = testUser.getId();
                    name = testUser.getName();
                }
                contentInfo=contentInfoDAO.getLatestContentInfoForTestForStudent(userId,cmdsTest.getId(),null);
                if (Objects.isNull(contentInfo)) {
                    contentInfo = contentInfoManager.createCMDSContentInfo(
                            UserType.NOT_REGISTERED,
                            userId,
                            name,
                            null,
                            null,
                            cmdsTest,
                            null,
                            null,
                            req.getContextType(),
                            req.getContextId());
                }
            }
        } else {
            logger.info("It is not a public test since the content info id is present");
            contentInfo = contentInfoManager.getContentById(req.getContentInfoId());
        }

        if (Objects.isNull(contentInfo)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Content info not found for request : " + req.toString());
        }

        return new ContentDetails(contentInfo, cmdsTest, userId);
    }

    private StartTestResponse createStartTestResponse(CMDSTestAttempt cmdsTestAttempt, CMDSTest cmdsTest) {
        StartTestResponse response = mapper.map(cmdsTestAttempt, StartTestResponse.class);
        response.setTestName(cmdsTest.getName());

        List<QuestionAttempt> questionAttempts = questionAttemptDAO.getByAttemptId(cmdsTestAttempt.getId());
        HashMap<String, QuestionAttempt> uniqueQuestionsMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(questionAttempts)) {
            for (QuestionAttempt questionAttempt : questionAttempts) {
                QuestionAttempt existingValueInMap = uniqueQuestionsMap.get(questionAttempt.getQuestionId());
                if (existingValueInMap == null
                        || questionAttempt.getCreationTime() > existingValueInMap.getCreationTime()) {
                    uniqueQuestionsMap.put(questionAttempt.getQuestionId(), questionAttempt);
                }
            }
        }

        Boolean testEnded = Boolean.FALSE;
        if (cmdsTestAttempt.getAttemptState() != null && (!CMDSAttemptState.ATTEMPT_STARTED.equals(cmdsTestAttempt.getAttemptState())
                || (cmdsTestAttempt.getEndTime() != null && System.currentTimeMillis() > cmdsTestAttempt.getEndTime()))) {
            testEnded = Boolean.TRUE;
        }

        List<CategoryQuestions> categories = new ArrayList<>();
        List<TestMetadata> testMetadatas = cmdsTest.getMetadata();
        for (TestMetadata testMetadata : testMetadatas) {
            CategoryQuestions categoryQuestions = new CategoryQuestions();
            categoryQuestions.setCategory(testMetadata.getName());
            List<QuestionResponse> questionResponses = new ArrayList<>();
            List<CMDSTestQuestion> cmdsTestQuestionList = testMetadata.getQuestions();
            List<String> qidList = new ArrayList<>();
            for (CMDSTestQuestion cmdsTestQuestion : cmdsTestQuestionList) {
                qidList.add(cmdsTestQuestion.getQuestionId());
            }

            List<CMDSQuestion> questions = cmdsQuestionDAO.getByIds(qidList, null);
            if (questions != null) {
                questionRepositoryDocParser.populateAWSUrls(questions);
                Map<String, CMDSQuestion> cmdsQuestionMap = new HashMap<>();
                for (CMDSQuestion cmdsQuestion : questions) {
                    cmdsQuestionMap.put(cmdsQuestion.getId(), cmdsQuestion);
                }
                for (CMDSTestQuestion cmdsTestQuestion : cmdsTestQuestionList) {
                    CMDSQuestion question = cmdsQuestionMap.get(cmdsTestQuestion.getQuestionId());
                    QuestionResponse questionResponse = new QuestionResponse();
                    questionResponse.setQuestionId(question.getId());
                    questionResponse.setQuestionIndex(cmdsTestQuestion.getQuestionIndex());
                    questionResponse.setQuestionText(question.getQuestionBody().getNewText());
                    questionResponse.setDifficulty(question.getDifficulty());
                    questionResponse.setTopics(question.getAnalysisTags());
                    questionResponse.setMainTags(question.getMainTags());
                    setOptions(questionResponse, question.getSolutionInfo().getOptionBody().getNewOptions());
                    questionResponse.setQuestionType(question.getType());
                    questionResponse.setMarks(cmdsTestQuestion.getMarks());
                    QuestionAttempt questionAttempt = uniqueQuestionsMap.get(question.getId());
                    if (questionAttempt != null && questionAttempt.getAnswerGiven() != null && !questionAttempt.getAnswerGiven().isEmpty()) {
                        questionResponse.setAnswerGiven(questionAttempt.getAnswerGiven());
                        questionResponse.setAttemptStateOfQuestion(questionAttempt.getAttemptStateOfQuestion());
                    }
                    // In a to be reviewed question the answer is empty and state is TO_BE_REVIEWED
                    if(questionAttempt != null && Objects.nonNull(questionAttempt.getAttemptStateOfQuestion()) &&
                        questionAttempt.getAttemptStateOfQuestion().equals(AttemptStateOfQuestion.TO_BE_REVIEWED)) {
                        questionResponse.setAttemptStateOfQuestion(questionAttempt.getAttemptStateOfQuestion());
                    }
                    /* In normal objective test there is no question state for now
                       making this temporary fix where if both answer attempt list is empty and question attempt state is null
                       then it a clear response option in normal objective test
                     */
                    if(questionAttempt != null && ArrayUtils.isEmpty(questionAttempt.getAnswerGiven()) && Objects.isNull(questionAttempt.getAttemptStateOfQuestion())){
                        questionResponse.setAttemptStateOfQuestion(AttemptStateOfQuestion.CLEAR_RESPONSE);
                    }
                    // Returing solution in case of test end state or assignment.
                    if (testEnded || ((cmdsTest.getDuration() == null) || (cmdsTest.getDuration() == 0))) {
                        questionResponse.setSolution(question.getSolutionInfo());
                    }

                    questionResponses.add(questionResponse);
                }
            }
            categoryQuestions.setQuestions(questionResponses);
            categories.add(categoryQuestions);
        }

        response.setCategories(categories);
        response.setCurrentTime(System.currentTimeMillis());
        response.setDoNotShowResultsTill(cmdsTest.getDoNotShowResultsTill());

        if (cmdsTest.getDoNotShowResultsTill() != null && System.currentTimeMillis() < cmdsTest.getDoNotShowResultsTill()) {
            response.setResultEntries(null);
        }

        // In case of hardstop set the end duration accordingly
        if (cmdsTest.isHardStop()
                && cmdsTest.getMinStartTime() <= System.currentTimeMillis()
                && cmdsTest.getMaxStartTime() <= cmdsTest.getMinStartTime() + cmdsTest.getDuration()
                && cmdsTest.getMinStartTime() + cmdsTest.getDuration() >= System.currentTimeMillis()
                && cmdsTestAttempt.getStartedAt() <= cmdsTest.getMaxStartTime()) {
            response.setDuration(cmdsTest.getDuration() + cmdsTest.getMinStartTime() - System.currentTimeMillis());
            if (response.getStartedAt() != null) {
                logger.info("The test is started at " + getFormattedDate(response.getStartedAt()));
            } else {
                logger.info("Starting test for the first time ");
            }
            response.setStartTime(cmdsTest.getMinStartTime());
            logger.info("Setting start time to the original start time of test " + getFormattedDate(response.getStartTime()));
            logger.info("The test has the duration " + response.getDuration() / (1000 * 60) + " min " + (response.getDuration() / (1000)) % 60 + " sec ");
            response.setEndTime(cmdsTest.getDuration() + cmdsTest.getMinStartTime());
            logger.info("The test has the end time " + getFormattedDate(response.getEndTime()));
        }
        if (cmdsTest.getMaxStartTime() != null
                && CMDSAttemptState.EVALUATE_COMPLETE.equals(cmdsTestAttempt.getAttemptState())
                && ArrayUtils.isNotEmpty(response.getResultEntries())
                && !cmdsTest.getId().equals("5ec5047dd079632139a24eed")) {
//            AggregationResults<QuestionDurationAggregation> averageTimeSpent = contentInfoDAO.getAverageTimeSpent(cmdsTest.getId(), cmdsTest.getMaxStartTime());
//            HashMap<String, Float> avgMap = new HashMap<>();
//            averageTimeSpent.forEach(questionDurationAggregation -> avgMap.put(questionDurationAggregation.getQuestionId(), questionDurationAggregation.getAverageDuration()));
//            response.getResultEntries().forEach(questionAnalytics -> questionAnalytics.setAverageAttemptDuration(avgMap.get(questionAnalytics.getQuestionId())));
        }

        return response;
    }

    private void setOptions(QuestionResponse questionResponse, List<String> optionTextList) {
        List<Option> options = new ArrayList<>();
        int index = 1;
        char label = 'a';
        for (String optionText : optionTextList) {
            Option option = new Option();
            option.setIndex(String.valueOf(index++));
            option.setLabel(String.valueOf(label++));
            option.setText(optionText);
            options.add(option);

        }
        questionResponse.setOptions(options);
    }

    private CMDSTestAttempt validateTestAttemptAndReturnOngoingTest(TestContentInfoMetadata testContentInfoMetadata, int endTimeBuffer,String contentInfoId) throws ForbiddenException {
        Long minTestStartTime = testContentInfoMetadata.getMinStartTime();
        Long maxTestStartTime = testContentInfoMetadata.getMaxStartTime();
        Long duration = testContentInfoMetadata.getDuration();
        Long currentTime = System.currentTimeMillis();

        CMDSTestAttempt latestTestAttempt = getLatestTestAttempt(contentInfoId);
        if (Objects.isNull(latestTestAttempt)) {
            logger.info("This is the first attempt, so checking for necessary condition if the test has started or not");
            checkMinStartTime(minTestStartTime, currentTime);
        } else {
            logger.info("The test is already attempted");
            if (!CMDSAttemptState.ATTEMPT_STARTED.equals(latestTestAttempt.getAttemptState())
                    || (Objects.nonNull(latestTestAttempt.getEndTime()) && currentTime > (endTimeBuffer + latestTestAttempt.getEndTime()))) {
                logger.info("Checking for conditions where test is already attempted(non-first attempt)");
                if (testContentInfoMetadata.isReattemptAllowed()) {
                    if (testContentInfoMetadata.isHardStop()) {
                        if (Objects.nonNull(minTestStartTime) && currentTime <= minTestStartTime + duration) {
                            logger.info("For this case reattempt is allowed but is hardstop, so the user can attempt the test when it is finally over.");
                            throw new ForbiddenException(ErrorCode.CMDS_TEST_START_TIME_OVER, "The max start time is over, kindly take the test after it ends", Level.INFO);
                        }
                    } else {
                        if (Objects.nonNull(maxTestStartTime) && currentTime <= maxTestStartTime + duration) {
                            logger.info("For this case reattempt is allowed but is not hardstop, so the user can attempt the test when it is finally over.");
                            throw new ForbiddenException(ErrorCode.CMDS_TEST_START_TIME_OVER, "The max start time is over, kindly take the test after it ends", Level.INFO);
                        }
                    }
                } else {
                    logger.info("For this case reattempt is not allowed");
                    throw new ForbiddenException(ErrorCode.CMDS_TEST_ENDED, "Test already over");
                }
            } else {
                logger.info("So far so good(ongoing attempt), as the attempt is already started, so sending the data of current attempt.");
                return latestTestAttempt;
            }
        }
        return null;
    }

    private void checkMinStartTime(Long minTestStartTime, Long currentTime) throws ForbiddenException {
        if (minTestStartTime != null && currentTime < minTestStartTime) {
            throw new ForbiddenException(ErrorCode.CMDS_TEST_NOT_STARTED, "Test not started yet");
        }
    }

    private CMDSTestAttempt getLatestTestAttempt(String contentInfoId) {
        return cmdsTestAttemptDAO.getLatestAttempt(contentInfoId);
    }

    public PlatformBasicResponse submitQuestionAttempt(BulkSubmitQuestionAttemptRequest req) throws InternalServerErrorException, ForbiddenException, NotFoundException , BadRequestException{
        req.verify();
        CMDSTestAttempt testAttempt =  getTestAttemptByAttemptId(req.getTestAttemptId());;
        if (Objects.isNull(testAttempt)) {
            throw new NotFoundException(ErrorCode.ATTEMPT_NOT_FOUND, "Test Attempt not found for attempt Id " + req.getTestAttemptId());
        }

        ContentInfo contentInfo=contentInfoManager.getContentById(testAttempt.getContentInfoId(),false);
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (req.getCallingUserId() == null || !String.valueOf(req.getCallingUserId()).equals(contentInfo.getStudentId()))) {
            logger.warn("Illegal resource access : userId -" + req.getCallingUserId() + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for the current role");
        }

        Long currentTime = System.currentTimeMillis();

        if (!CMDSAttemptState.ATTEMPT_STARTED.equals(testAttempt.getAttemptState())
                || (testAttempt.getEndTime() != null && currentTime > testAttempt.getEndTime())) {
            throw new ForbiddenException(ErrorCode.CMDS_TEST_ENDED, "Test already over");
        }

        List<QuestionAttempt> questionAttemptList = new ArrayList<>();

        for (Map.Entry<String, QuestionAttemptDetails> entry : req.getQuestionAttemptsMap().entrySet()) {
            QuestionAttempt questionAttempt = new QuestionAttempt(contentInfo.getStudentId(),
                    entry.getValue().getDuration(), QuestionAttemptContext.CMDSTEST, testAttempt.getTestId(),
                    req.getTestAttemptId(), entry.getKey(), entry.getValue().getAttempt(), entry.getValue().getAttemptStateOfQuestion());
            questionAttemptList.add(questionAttempt);
        }

        questionAttemptDAO.insertAll(questionAttemptList);
        return new PlatformBasicResponse(true, null, null);
    }

    public PlatformBasicResponse submitTestFeedback(TestFeedbackRequest req) throws BadRequestException, NotFoundException, ForbiddenException {
        req.verify();
        CMDSTestAttempt testAttempt = cmdsTestAttemptDAO.getTestAttemptById(req.getTestAttemptId(),Collections.singletonList(CMDSTestAttempt.Constants.CONTENT_INFO_ID));
        if(Objects.nonNull(testAttempt)){
            ContentInfo contentInfo = contentInfoManager.getContentById(testAttempt.getContentInfoId());
            if (contentInfo == null) {
                logger.info("Content Info not found");
                throw new NotFoundException(ErrorCode.ATTEMPT_NOT_FOUND, "Test Attempt not found for attempt Id " + req.getTestAttemptId());
            }
            if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (req.getCallingUserId() == null || !String.valueOf(req.getCallingUserId()).equals(contentInfo.getStudentId()))) {
                logger.warn("Illegal resource access : userId -" + req.getCallingUserId() + " contentInfo :"
                        + contentInfo.toString());
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for the current role");
            }
        }else{
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"No test attempt found for the id "+req.getTestAttemptId());
        }
        logger.info("Adding feedback " + req.getFeedback() + " to test attempt with Id " + req.getTestAttemptId());

        cmdsTestAttemptDAO.updateTestAttemptFeedbackForAttemptId(testAttempt.getId(),req.getFeedback());

        return new PlatformBasicResponse(true, null, null);
    }

    public PlatformBasicResponse cacheTestAttemptProgress(TestProgressCacheRequest req) throws BadRequestException, NotFoundException {
        req.verify();
        CMDSTestAttempt cmdsTestAttempt = cmdsTestAttemptDAO.getTestAttemptById(req.getTestAttemptId(),Collections.singletonList(CMDSTestAttempt.Constants._ID));
        if (Objects.isNull(cmdsTestAttempt)) {
            logger.error("Content Info not found");
            throw new NotFoundException(ErrorCode.ATTEMPT_NOT_FOUND, "Test Attempt not found for attempt Id " + req.getTestAttemptId());
        }
        TestProgressCache testProgressCache = req.getTestProgressCache();
        if(Objects.isNull(testProgressCache)) {
            logger.error("Test progress parameters not given!");
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Test progress parameters for attempt not given! " + req.getTestAttemptId());
        }
        Long testDuration = cmdsTestAttempt.getDuration();
        int duration = 3 * DateTimeUtils.SECONDS_PER_HOUR;
        if(Objects.nonNull(testDuration) && testDuration > 0) {
            duration = (int)(testDuration/1000);
        }
        try {
            redisDAO.setex(req.getTestAttemptId(), gson.toJson(testProgressCache), duration);
        } catch(Exception e){
            logger.error("Error while writing to Redis: ", e);
            return new PlatformBasicResponse(false, null, "Error while writing to Redis");
        }
        return new PlatformBasicResponse(true, null, null);
    }

    public PlatformBasicResponse getCachedTestAttemptProgress(String attemptId) throws NotFoundException {
        CMDSTestAttempt cmdsTestAttempt = cmdsTestAttemptDAO.getTestAttemptById(attemptId,Collections.singletonList(CMDSTestAttempt.Constants._ID));
        if (Objects.isNull(cmdsTestAttempt)) {
            logger.error("Content Info not found");
            throw new NotFoundException(ErrorCode.ATTEMPT_NOT_FOUND, "Test Attempt not found for attempt Id " + attemptId);
        }
        String testCacheProgressString;
        try {
            testCacheProgressString = redisDAO.get(attemptId);
        } catch (Exception e) {
            logger.info("Can't fetch test progress for attemptId: {} and message {} ", attemptId, e);
            return new PlatformBasicResponse(true, null, null);
        }
        return new PlatformBasicResponse(true, gson.fromJson(testCacheProgressString, TestProgressCache.class), null);
    }

    private void validateTestAttemptForSubmission(CMDSTestAttempt testAttempt) throws NotFoundException, ForbiddenException {
        if (testAttempt == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Test not found");
        }

        if (!CMDSAttemptState.ATTEMPT_STARTED.equals(testAttempt.getAttemptState())) {
            throw new ForbiddenException(ErrorCode.CMDS_TEST_ENDED, "Test already over");
        }
    }

    private CMDSTestAttempt getTestAttemptByAttemptId(String testAttemptId) {
        return cmdsTestAttemptDAO.getTestAttemptById(testAttemptId,null);
    }

    public CMDSTestAttempt submitCMDSTest(CMDSSubmitTestAttemptReq req)
            throws VException, UnsupportedEncodingException {

        // Debug start
        logger.info("submitCMDSTest DEBUT START: " + req.hashCode());
        logger.info("submitCMDSTest: request file : " + req);

        // Validate request
        req.verify();


        // Filtering out only the current attempt from retrieved ContentInfo DB and
        // validate if they are ready for submission by checking if the state is in
        // `ATTEMPT_STARTED` and `null` value is not returned after filtering from the DB
        // object(i.e. cmdsTestAttempt(below variable))
        String attemptId = req.getAttemptId();
        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(attemptId);
        logger.info("Current test attempt is " + cmdsTestAttempt);
        validateTestAttemptForSubmission(cmdsTestAttempt);
        logger.info("Validation of test attempt is done successfully.");

        // Validate the data after getting the attempt details from ContentInfo database
        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);
        logger.info("Received content info for attempt with attempt id " + attemptId + " is " + contentInfo);
        if (Objects.isNull(contentInfo)) {
            logger.info("The attempt not found for submitting" + attemptId);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "The attempt is not found");

        }
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (Objects.isNull(req.getCallingUserId()) || !String.valueOf(req.getCallingUserId()).equals(contentInfo.getStudentId()))) {
            logger.warn("Illegal resource access : userId -" + req.getCallingUserId() + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for the current role");
        }
        if (!LMSType.CMDS.equals(contentInfo.getLmsType())) {
            logger.error("LMS type is not cmds. Req : " + req.toString() + " contentInfo : " + contentInfo.toString());
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Requested content is not part of CMDS");
        }
        logger.info("ContentInfo validated successfully");


        // Update details of submission like endedAt, endTime, image details data(in case of subjective test),
        // the way test is in case it is etc.(all the cleanup data after a test ends)
        updateSubmitDetails(req, cmdsTestAttempt);
        //setting attempt type as ATTEMPT_COMPLETE_SOLUTIONS_NOT_UPLOADED and ContentState as NOT_UPLOADED for subjective tests with no files uploaded
        checkForFileInfosInSubjectiveTest(contentInfo, cmdsTestAttempt, req.isDraftState());
        logger.info("After populating all the submission details for the attempt, the attempt details for test of type : " + contentInfo.getContentInfoType() + " are : " + cmdsTestAttempt);

        logger.info("Adding test feedback " + req.getFeedback() + " to test attempt with Id " + attemptId);
        if(Objects.isNull(req.getFeedback())){
            logger.info("Feedback is captured as null, so adding information for debugging - attemptId "+req.getAttemptId()+" with total request object "+req);
        }else{
            cmdsTestAttempt.setFeedback(req.getFeedback());
        }
        // If there is any data in the question map or say if the student has attempted any question then
        // increment the attempt count in CMDSQuestionAttempt table
        if (req.getQuestionAttemptsMap() != null && ArrayUtils.isNotEmpty(req.getQuestionAttemptsMap().keySet())) {
            logger.info("The student has attempted the test, so question attempt present");
            List<QuestionAttempt> questionAttemptList
                    = req.getQuestionAttemptsMap().entrySet()
                            .parallelStream()
                            .map(entry -> getQuestionAttemptObject(contentInfo, cmdsTestAttempt, entry))
                            .collect(Collectors.toList());

            questionAttemptDAO.insertAll(questionAttemptList);
        }

        // If the attempt submitted is not in draft state(which is deprecated as of now, attempt is always in non-draft state)
        // then populate more info for the test attempt like `contentState`, `resultEntries`(for the attempt) etc.
        if (!req.isDraftState() && (!contentInfo.getContentState().equals(ContentState.NOT_UPLOADED))) {

            contentInfo.setContentState(ContentState.ATTEMPTED);
            TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) contentInfo.getMetadata();

            CMDSTest test = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());

            // Set total marks from test
            if (Objects.nonNull(test.getTotalMarks())) {
                cmdsTestAttempt.setTotalMarks(test.getTotalMarks());
            }

            // For objective test various evaluation type are there for various type of
            // questions, so evaluate the same and put the data into db below
            if (ContentInfoType.OBJECTIVE.equals(test.getContentInfoType())
                    && TestResultVisibility.VISIBLE.equals(testContentInfoMetadata.getDisplayResultOnEnd())) {
                computeAnalytics(cmdsTestAttempt, test, contentInfo.getStudentId(), contentInfo.getId());
                contentInfo.setContentState(ContentState.EVALUATED);
                cmdsTestAttempt.setEvaluatedTime(System.currentTimeMillis());
            }
            contentInfo.setStudentActionLink(contentInfoManager.getCMDSReportLink(contentInfo));
        }

        // Store all the updated content inside the contentInfo
        logger.info("Updated content inside content info is " + contentInfo);
        contentInfoManager.updateContent(contentInfo);
        cmdsTestAttemptDAO.save(cmdsTestAttempt);
        // todo enable for gamification
        // contentInfoManager.processContentInfoForGameAsync(contentInfo);

        // Post Test Marketing changes
        List<String> testIds = new ArrayList<>();
        testIds.add(cmdsTestAttempt.getTestId());
        List<CMDSTest> tests = testDAO.getTestByIds(testIds);
        if (CollectionUtils.isNotEmpty(tests)) {
            CMDSTest test = tests.get(0);
            if (test != null) {
                if (StringUtils.isNotEmpty(test.getMarketingRedirectUrl())) {
                    Float marksLost = cmdsTestAttempt.getTotalMarks() - cmdsTestAttempt.getMarksAcheived();
                    Float finalMarksLost = marksLost * 923;
                    String redirectUrl = test.getMarketingRedirectUrl() + "?t=" + cmdsTestAttempt.getTotalMarks() + "&a=" + cmdsTestAttempt.getMarksAcheived() + "&r=" + finalMarksLost;
                    cmdsTestAttempt.setMarketingRedirectUrl(redirectUrl);
                    cmdsTestAttempt.setDoNotShowResultsTill(test.getDoNotShowResultsTill());
                }
            }
        }
        logger.info("submitCMDSTest DEBUG END: " + req.hashCode());

        //flush redis entry for test attempt progress
        try {
            redisDAO.del(attemptId);
        } catch (Exception e) {
            logger.warn("Cannot flush Redis entry for attemptId: " + attemptId);
        }
        return cmdsTestAttempt;
    }

    private QuestionAttempt getQuestionAttemptObject(ContentInfo contentInfo, CMDSTestAttempt cmdsTestAttempt, Map.Entry<String, QuestionAttemptDetails> entry) {
        return new QuestionAttempt(
                contentInfo.getStudentId(),
                entry.getValue().getDuration(),
                QuestionAttemptContext.CMDSTEST,
                cmdsTestAttempt.getTestId(),
                cmdsTestAttempt.getId(),
                entry.getKey(),
                entry.getValue().getAttempt());
    }

    // Calculate Total Marks from Test Question.
    private float calculateTotalMarks(CMDSTest test) {
        float totalMarks = 0;

        TestMetadata testMetadata = null;
        if (test.getMetadata() != null) {
            testMetadata = test.getMetadata().get(0);
            List<CMDSTestQuestion> cmdsTestQuestions = testMetadata.getQuestions();
            for (CMDSTestQuestion question : cmdsTestQuestions) {
                if (question.getMarks() != null) {
                    totalMarks += question.getMarks().getPositive();
                }
            }
        }
        return totalMarks;
    }

    public void linkAttemptToUser(CMDSLinkAttemptToUserReq req, HttpSessionData httpSessionData)
            throws MalformedURLException, VException, UnsupportedEncodingException {
        logger.info("linkAttemptToUser Request : " + req.toString());

        // Validate request
        req.validate();

        Long userId = httpSessionData.getUserId();
        if (userId == null) {
            logger.info("User not logged in");
            throw new BadRequestException(ErrorCode.NOT_LOGGED_IN, "User not logged in");
        }

        // Check access
        CMDSTestAttempt attempt=cmdsTestAttemptDAO.getTestAttemptById(req.getAttemptId(),Collections.singletonList(CMDSTestAttempt.Constants.CONTENT_INFO_ID));
        ContentInfo contentInfo = contentInfoManager.getContentById(attempt.getContentInfoId());
        if (contentInfo == null) {
            logger.info("The attempt not found for submitting" + req.getAttemptId());
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "The attempt is not found");

        }
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (userId == null || !String.valueOf(userId).equals(contentInfo.getStudentId()))) {
            logger.warn("Illegal resource access : userId -" + req.getUserId() + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for the current role");
        }
        if (UserType.NOT_REGISTERED.equals(contentInfo.getUserType())) {
            contentInfo.setAttemptId(req.getAttemptId());
            contentInfo.setStudentId(userId.toString());
            if (StringUtils.isNotEmpty(httpSessionData.getFirstName())) {
                if (StringUtils.isNotEmpty(httpSessionData.getLastName())) {
                    contentInfo.setStudentFullName(httpSessionData.getFirstName() + " " + httpSessionData.getLastName());
                } else {
                    contentInfo.setStudentFullName(httpSessionData.getFirstName());
                }
            }
            contentInfo.setUserType(UserType.REGISTERED);
            contentInfoDAO.save(contentInfo);
        }
    }

    public CMDSTestAttempt evaluateCMDSTest(CMDSEvaluateTestAttemptReq req)
            throws VException, UnsupportedEncodingException {

        // Validate request
        req.verify();

        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(req.getAttemptId());

        if (Objects.isNull(cmdsTestAttempt)) {
            String errorMessage = "Attempt event is not found for id : " + req.getAttemptId() + " req: "
                    + req.toString();
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        // Check the attempt state
        if (!CMDSAttemptState.ATTEMPT_COMPLETE.equals(cmdsTestAttempt.getAttemptState())
                && !CMDSAttemptState.EVALUATE_STARTED.equals(cmdsTestAttempt.getAttemptState())
                && !CMDSAttemptState.EVALUATE_COMPLETE.equals(cmdsTestAttempt.getAttemptState())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Invalid attempt state preventing the operation : " + req.toString() + " attempt : "
                    + cmdsTestAttempt.toString());
        }

        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);
        if (req.getCallingUserId() == null || !String.valueOf(req.getCallingUserId()).equals(contentInfo.getTeacherId())) {
            logger.warn("Illegal resource access : userId -" + req.getCallingUserId() + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Action not allowed for the current role");
        }
        if (!LMSType.CMDS.equals(contentInfo.getLmsType())) {
            logger.error("LMS type is not cmds. Req : " + req.toString() + " contentInfo : " + contentInfo.toString());
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Requested content is not part of CMDS");
        }

        updateEvaluateDetails(req, cmdsTestAttempt);
        logger.info("Response : " + cmdsTestAttempt.toString());

        // Update content info
        logger.info("ContentInfo pre update : " + contentInfo.toString());
        cmdsTestAttempt.setEvaluatedTime(System.currentTimeMillis());
        contentInfo.setContentState(ContentState.EVALUATED);
        contentInfoManager.updateContent(contentInfo);
        cmdsTestAttemptDAO.save(cmdsTestAttempt);

        logger.info("Updated contentinfo : " + contentInfo.toString());
        sendPostTestEmailFromAttempt(cmdsTestAttempt, contentInfo, null);

        if (ContentInfoType.SUBJECTIVE.equals(contentInfo.getContentInfoType())) {
            CMDSTest cMDSTest = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());

            if (cMDSTest != null) {

                if (EnumBasket.TestTagType.PHASE.equals(cMDSTest.getTestTag()) || EnumBasket.TestTagType.UNIT.equals(cMDSTest.getTestTag())) {

                    if (cMDSTest.getMinStartTime() != null && cMDSTest.getMaxStartTime() != null && cmdsTestAttempt.getStartedAt() >= cMDSTest.getMinStartTime() && cmdsTestAttempt.getStartedAt() <= cMDSTest.getMaxStartTime()) {
                        cmdsTestManager.processCompetitiveTest(cMDSTest);
                    }
                }
            }
        }

        return cmdsTestAttempt;
    }

    public StartTestResponse getTestAttemptById(String attemptId, String callingUserId, Role callingUserRole)
            throws VException {
        logger.info("Request : " + attemptId + " callingUserId : " + callingUserId + " callingUserRole : "
                + callingUserRole);
        if (StringUtils.isEmpty(attemptId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad request error. Missing params : id - "
                    + attemptId + " callingUserId : " + callingUserId + " callingUserRole : " + callingUserRole);
        }

        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(attemptId);
        if (Objects.isNull(cmdsTestAttempt)) {
            String errorMessage = "Attempt event is not found for id : " + attemptId + " callingUserId: "
                    + callingUserId;
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);

        if(Objects.isNull(contentInfo))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"No contentInfo exists");

        // Check if the user is acadmentor / supermentor . AM / Supermentor is allowed to see the result.
        boolean userIsAcadMentor = checkIfUserIdAcadMentor();

        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (callingUserId == null ||
                (!callingUserId.equals(contentInfo.getStudentId()) && !callingUserId.equals(contentInfo.getTeacherId())
                        && !userIsAcadMentor))) {
            logger.warn("Illegal resource access : userId -" + callingUserId + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Action not allowed for the current user");
        }

        // Create aws signe urls for attempt/evaluate images
        try {
            createSignedUrls(cmdsTestAttempt);
        } catch (Exception ex) {
            logger.error("Error generating signed url : " + ex.toString() + " cmdsTestAttempt :"
                    + cmdsTestAttempt.toString());
        }
        CMDSTest cmdsTest = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());

        return createStartTestResponse(cmdsTestAttempt, cmdsTest);
    }

    private boolean checkIfUserIdAcadMentor() throws VException {
        if (sessionUtils.getCurrentSessionData() != null) {
            Set<SubRole> subRoles = sessionUtils.getCurrentSessionData().getSubRoles();

            Role role = sessionUtils.getCurrentSessionData().getRole();

            if (subRoles == null || !(subRoles.contains(SubRole.ACADMENTOR) || subRoles.contains(SubRole.SUPERMENTOR))) {
                return false;
            }
            return true;
        }
        return false;
    }

    public void attemptViewed(String attemptId, String callingUserId, Role callingUserRole)
            throws BadRequestException, ForbiddenException, NotFoundException {
        logger.info("Request : " + attemptId + " callingUserId : " + callingUserId + " callingUserRole : "
                + callingUserRole);
        if (StringUtils.isEmpty(attemptId) || StringUtils.isEmpty(callingUserId) || (callingUserRole == null)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad request error. Missing params : id - "
                    + attemptId + " callingUserId : " + callingUserId + " callingUserRole : " + callingUserRole);
        }

        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(attemptId);
        if (Objects.isNull(cmdsTestAttempt)) {
            String errorMessage = "Attempt event is not found for id : " + attemptId + " callingUserId: "
                    + callingUserId;
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }


        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && !callingUserId.equals(contentInfo.getStudentId()) && !callingUserId.equals(contentInfo.getTeacherId())) {
            logger.warn("Illegal resource access : userId -" + callingUserId + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Action not allowed for the current user");
        }

        switch (callingUserRole) {
            case STUDENT:
                if (CMDSAttemptState.EVALUATE_COMPLETE.equals(cmdsTestAttempt.getAttemptState())
                        && cmdsTestAttempt.getReportViewed() == null) {
                    cmdsTestAttempt.setReportViewed(System.currentTimeMillis());
                    contentInfoManager.updateContent(contentInfo);
                    cmdsTestAttemptDAO.save(cmdsTestAttempt);
                }
                break;
            case TEACHER:
                if (CMDSAttemptState.ATTEMPT_COMPLETE.equals(cmdsTestAttempt.getAttemptState())
                        && cmdsTestAttempt.getEvaluatedViewTime() == null) {
                    cmdsTestAttempt.setEvaluatedViewTime(System.currentTimeMillis());
                    contentInfoManager.updateContent(contentInfo);
                    cmdsTestAttemptDAO.save(cmdsTestAttempt);
                }
                break;
            default:
                // This wont be executed
                break;
        }
        logger.info("cmdsTestAttempt: " + cmdsTestAttempt.toString());
    }

    private void updateSubmitDetails(CMDSSubmitTestAttemptReq req, CMDSTestAttempt cmdsTestAttempt) {
        Long currentTime = System.currentTimeMillis();

        cmdsTestAttempt.setLastUpdated(currentTime);
        cmdsTestAttempt.setLastUpdatedBy(String.valueOf(req.getCallingUserId()));
        cmdsTestAttempt.setImageDetails(req.getFileInfos());
        if (!req.isDraftState()) {
            cmdsTestAttempt.setAttemptState(CMDSAttemptState.ATTEMPT_COMPLETE);
            if (Objects.nonNull(req.getTestEndType())) {
                cmdsTestAttempt.setTestEndType(req.getTestEndType());
            } else {
                cmdsTestAttempt.setTestEndType(TestEndType.AUTOMATIC);
            }
            Long endTime = currentTime;
            if (Objects.nonNull(cmdsTestAttempt.getEndTime()) && endTime > cmdsTestAttempt.getEndTime()) {
                endTime = cmdsTestAttempt.getEndTime();
            }
            cmdsTestAttempt.setEndedAt(endTime);
            if (Objects.nonNull(cmdsTestAttempt.getStartedAt()) && Objects.nonNull(cmdsTestAttempt.getEndedAt())) {
                cmdsTestAttempt.setTimeTakenByStudent(cmdsTestAttempt.getEndedAt() - cmdsTestAttempt.getStartedAt());
            } else if (Objects.nonNull(cmdsTestAttempt.getStartTime()) && Objects.nonNull(cmdsTestAttempt.getEndTime())) {
                cmdsTestAttempt.setTimeTakenByStudent(cmdsTestAttempt.getEndTime() - cmdsTestAttempt.getStartTime());
            }
        }
        updateImageTimeDetails(cmdsTestAttempt);
    }

    private void checkForFileInfosInSubjectiveTest(ContentInfo contentInfo, CMDSTestAttempt cmdsTestAttempt, boolean draftState) throws BadRequestException {
        if(contentInfo.getContentInfoType().equals(ContentInfoType.SUBJECTIVE)
                && (cmdsTestAttempt.getImageDetails() == null || cmdsTestAttempt.getImageDetails().size() == 0)
                && (cmdsTestAttempt.getEndTime() != null)) {

            if(cmdsTestAttempt.getEndTime() <= System.currentTimeMillis()) {
                cmdsTestAttempt.setAttemptState(CMDSAttemptState.ATTEMPT_COMPLETE_SOLUTIONS_NOT_UPLOADED);
                contentInfo.setContentState(ContentState.NOT_UPLOADED);
            } else if (cmdsTestAttempt.getEndTime() > System.currentTimeMillis() && !draftState) {
                logger.info("No images have been uploaded as part of submission for contentInfo : "
                        + contentInfo.toString()
                        + ", with current time as : "
                        + System.currentTimeMillis()
                        + " and end time as : "
                        + cmdsTestAttempt.getEndTime());
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No images have been uploaded as part of submission");
            }

        }
    }

    private void updateImageTimeDetails(CMDSTestAttempt cmdsTestAttempt) {
        if (!CollectionUtils.isEmpty(cmdsTestAttempt.getImageDetails())) {
            for (CMDSTestAttemptImage testAttemptImage : cmdsTestAttempt.getImageDetails()) {
                if (testAttemptImage.getCreationTime() == null || testAttemptImage.getCreationTime() <= 0l) {
                    testAttemptImage.setCreationTime(System.currentTimeMillis());
                }
                testAttemptImage.setLastUpdatedTime(System.currentTimeMillis());
            }
        }
    }

    private void updateEvaluateDetails(CMDSEvaluateTestAttemptReq req, CMDSTestAttempt cmdsTestAttempt)
            throws VException, UnsupportedEncodingException {
        cmdsTestAttempt.setLastUpdated(System.currentTimeMillis());
        cmdsTestAttempt.setLastUpdatedBy((Objects.nonNull(req.getCallingUserId())) ? req.getCallingUserId().toString() : null);

        cmdsTestAttempt.setEvaluatedTime(System.currentTimeMillis());
        if (Objects.isNull(cmdsTestAttempt.getEvaluatedViewTime()) || cmdsTestAttempt.getEvaluatedViewTime() <= 0l) {
            cmdsTestAttempt.setEvaluatedViewTime(cmdsTestAttempt.getEvaluatedTime());
        }

        cmdsTestAttempt.setAttemptRemarks(req.getAttemptRemarks());
        updateEvaluateResultEntries(req.getResults(), cmdsTestAttempt);
        updateEvaluateImageDetails(req.getFileInfos(), cmdsTestAttempt);
    }

    private void calculateMarksAcheived(CMDSTestAttempt cmdsTestAttempt) {
        float marksAcheived = 0f;
        if (!CollectionUtils.isEmpty(cmdsTestAttempt.getResultEntries())) {
            for (QuestionAnalytics resultEntry : cmdsTestAttempt.getResultEntries()) {
                if (resultEntry.getMarksGiven() != null
                        && QuestionAttemptState.EVALUATED.equals(resultEntry.getQuestionAttemptState())) {
                    marksAcheived += resultEntry.getMarksGiven();
                }
            }
        }

        cmdsTestAttempt.setMarksAcheived(marksAcheived);
    }

    private void updateEvaluateResultEntries(List<QuestionAnalytics> newEntries, CMDSTestAttempt cmdsTestAttempt)
            throws VException, UnsupportedEncodingException {
        if (!CollectionUtils.isEmpty(newEntries)) {
            // Save the update the entries in this list
            List<QuestionAnalytics> updatedEntries = new ArrayList<>();

            // Create existing entries map to update from the request entries
            List<QuestionAnalytics> existingEntries = cmdsTestAttempt.getResultEntries();
            Map<String, QuestionAnalytics> existingEntriesMap = createQuestionIdMap(existingEntries);

            // Evaluate time that need to be updated in the questions. If the
            // evaluated time is not set, pick current time. Set this field only
            // if client does not set it.
            Long evaluatedTime = cmdsTestAttempt.getEvaluatedTime();
            if (evaluatedTime == null) {
                evaluatedTime = System.currentTimeMillis();
            }
            for (QuestionAnalytics newEntry : newEntries) {
                QuestionAnalytics existingEntry = existingEntriesMap.get(newEntry.getQuestionId());
                if (existingEntry == null) {
                    String errorMessage = "CMDS result entry question id not found : " + newEntry.toString();
                    logger.error(errorMessage);
                    throw new VException(ErrorCode.CMDS_RESULT_ENTRY_NOT_FOUND, errorMessage);
                }
                // TODO max marks field should not be in CMDSTestResultEntry,
                // fetch it from Test Details
                // if (existingEntry.getMaxMarks() == null) {
                // logger.error("Max marks not found for test entry : " +
                // cmdsTestAttempt.toString()
                // + " existingEntry : " + existingEntry.toString());
                // } else if (newEntry.getMaxMarks() != null
                // &&
                // !existingEntry.getMaxMarks().equals(newEntry.getMaxMarks()))
                // {
                // String errorMessage = "Max marks mismatch with existing entry
                // : " + existingEntry.toString()
                // + " newEntry : " + newEntry.toString();
                // logger.error(errorMessage);
                // throw new
                // BadRequestException(ErrorCode.CMDS_MAX_MARKS_MISMATCH,
                // errorMessage);
                // }

//				else if (newEntry.getMarksGiven() != null) {
//					if (!QuestionAttemptState.EVALUATED.equals(newEntry.getQuestionAttemptState())) {
//						String errorMessage = "Invalid question state for new entry : " + newEntry.toString()
//								+ " existingEntry : " + existingEntry.toString()
//								+ ". State is not evaluated and marks are provided.";
//						throw new BadRequestException(ErrorCode.CMDS_INVALID_QUESTION_STATE, errorMessage);
//					}
//					// TODO max marks field should not be in
//					// CMDSTestResultEntry, fetch it from Test Details
//					// else if (newEntry.getMarksGiven() >
//					// existingEntry.getMaxMarks()) {
//					// String errorMessage = "Given marks greater than max marks
//					// for new entry : "
//					// + newEntry.toString() + " existingEntry : " +
//					// existingEntry.toString()
//					// + ". State is not evaluated and marks are provided.";
//					// throw new
//					// BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
//					// errorMessage);
//					// }
//
//				}
                // Update the existing entry with the new details
                if (existingEntry.getEvaluatedTime() == null) {
                    existingEntry.setEvaluatedTime(evaluatedTime);
                }

                QuestionAttemptState questionAttemptState = QuestionAttemptState.NOT_EVALUATED;
                if (newEntry.getQuestionAttemptState() != null) {
                    questionAttemptState = newEntry.getQuestionAttemptState();
                }
                existingEntry.setQuestionAttemptState(questionAttemptState);
                existingEntry.setMarksGiven(newEntry.getMarksGiven());
                existingEntry.setQuestionRemarks(newEntry.getQuestionRemarks());
                existingEntry.setCorrectness(EnumBasket.Correctness.UNJUDGEABLE);
                updatedEntries.add(existingEntry);
            }
            cmdsTestAttempt.setResultEntries(updatedEntries);
            CMDSTest test = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());
            if (ContentInfoType.SUBJECTIVE.equals(test.getContentInfoType())) {
                computeOverallAnalytics(cmdsTestAttempt, test);
            } else {
                computeAnalytics(cmdsTestAttempt, test);
            }

        }
    }

    private void updateEvaluateImageDetails(List<CMDSTestAttemptImage> newEntries, CMDSTestAttempt cmdsTestAttempt) {
        if (ArrayUtils.isNotEmpty(newEntries) && ArrayUtils.isNotEmpty(cmdsTestAttempt.getImageDetails())) {
            Map<String, CMDSTestAttemptImage> imageMap = createAttemptImageMap(newEntries);
            List<CMDSTestAttemptImage> updatedImages = new ArrayList<>();
            List<CMDSTestAttemptImage> existingEntries = cmdsTestAttempt.getImageDetails();
            for (CMDSTestAttemptImage existingEntry : existingEntries) {
                CMDSTestAttemptImage newEntry = imageMap.get(existingEntry.getKeyName());
                if (Objects.nonNull(newEntry)) {
                    logger.info("newEntry:" + newEntry + " existing entry:" + existingEntry);
                    existingEntry.updateEvaluateDetails(newEntry);
                    logger.info("entry updated");
                }
                logger.info("Updated entry : " + existingEntry.toString());
                updatedImages.add(existingEntry);
            }
            cmdsTestAttempt.setImageDetails(updatedImages);
            updateImageTimeDetails(cmdsTestAttempt);
        }
    }

    private static Map<String, QuestionAnalytics> createQuestionIdMap(List<QuestionAnalytics> entries) {
        // QuestionId, ResultEntry
        Map<String, QuestionAnalytics> map = new HashMap<>();
        if (!CollectionUtils.isEmpty(entries)) {
            for (QuestionAnalytics entry : entries) {
                map.put(entry.getQuestionId(), entry);
            }
        }

        return map;
    }

    private static Map<String, CMDSTestAttemptImage> createAttemptImageMap(List<CMDSTestAttemptImage> images) {
        // KeyName, imageDetails
        Map<String, CMDSTestAttemptImage> imageMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(images)) {
            for (CMDSTestAttemptImage image : images) {
                imageMap.put(image.getKeyName(), image);
            }
        }

        return imageMap;
    }

    private void createSignedUrls(CMDSTestAttempt cmdsTestAttempt) throws VException {
        List<CMDSTestAttemptImage> images = cmdsTestAttempt.getImageDetails();
        if (!CollectionUtils.isEmpty(images)) {
            for (CMDSTestAttemptImage image : images) {
                if (!StringUtils.isEmpty(image.getKeyName())) {
                    String signedUrl = s3Manager.getImageUrl(image.getKeyName(),
                            getS3Location(UploadFileSourceType.CMDS_ANSWER_UPLOAD));
                    logger.info("key :" + image.getKeyName() + " signedUrl : " + signedUrl);
                    image.setPublicUrl(signedUrl);
                }

                if (!StringUtils.isEmpty(image.getEvaluatedKeyName())) {
                    String signedUrl = s3Manager.getImageUrl(image.getEvaluatedKeyName(),
                            getS3Location(UploadFileSourceType.CMDS_ANSWER_UPLOAD));
                    logger.info("evaluatedKey : " + image.getEvaluatedKeyName() + " signedUrl : " + signedUrl);
                    image.setEvaluatedPublicUrl(signedUrl);
                }
            }
        }
    }

    private String getS3Location(UploadFileSourceType sourceType) throws VException {
        switch (sourceType) {
            case CMDS_ANSWER_UPLOAD:
                return AWS_CMDS_BUCKET + "/" + ConfigUtils.INSTANCE.getEnvironmentSlug() + "/" + AWS_CMDS_ANSWERS_FOLDER;
            default:
                throw new VException(ErrorCode.NOT_FOUND_ERROR, "Source type not found");
        }
    }

    private void computeAnalytics(CMDSTestAttempt testAttempt, CMDSTest cmdsTest) throws InternalServerErrorException, UnsupportedEncodingException {
        computeAnalytics(testAttempt, cmdsTest, null, null);
    }

    private void computeAnalytics(CMDSTestAttempt testAttempt, CMDSTest cmdsTest, String studentId, String contenInfoId) throws InternalServerErrorException, UnsupportedEncodingException {
        // Get all the metadata of questions
        List<TestMetadata> testMetadataList = cmdsTest.getMetadata();

        // Collect all the questions one by one from each and every question set
        List<String> questionIds
                = testMetadataList
                        .stream()
                        .map(TestMetadata::getQuestions)
                        .flatMap(Collection::stream)
                        .map(CMDSTestQuestion::getQuestionId)
                        .collect(Collectors.toList());

        // Collect all the previous question ids if present in case there are some changes in question
        List<String> previousQuestionIds
                = testMetadataList
                        .stream()
                        .map(TestMetadata::getQuestions)
                        .flatMap(Collection::stream)
                        .map(CMDSTestQuestion::getQuestionChangeRecords)
                        .filter(ArrayUtils::isNotEmpty)
                        .flatMap(Collection::stream)
                        .filter(questionChangeRecord -> questionChangeRecord.getChangedAt() > testAttempt.getEndedAt())
                        .map(QuestionChangeRecord::getPrevQuestionId)
                        .collect(Collectors.toList());

        // Get all the questions
        if (ArrayUtils.isNotEmpty(previousQuestionIds)) {
            questionIds.addAll(previousQuestionIds);
        }

        // Get all the question and if the questions are present then put them in a map corresponding to question id
        List<CMDSQuestion> questions = cmdsQuestionDAO.getByIds(questionIds);
        Map<String, CMDSQuestion> questionMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(questions)) {
            questionMap.putAll(
                    questions.stream().filter(Objects::nonNull).collect(Collectors.toMap(CMDSQuestion::getId, Function.identity()))
            );
        }

        // compute the result
        computeResultForTestAttempt(testAttempt, cmdsTest, questionMap);

    }

    private void computeResultForTestAttempt(CMDSTestAttempt testAttempt, CMDSTest test, Map<String, CMDSQuestion> questionMap) {
        // NOTE: The `_id` inside `tesAttempts` object of `metadata` object inside ContentInfo collection
        // is mapped to `attemptId` of QuestionAttempt collection
        List<QuestionAttempt> questionAttempts = questionAttemptDAO.getByAttemptId(testAttempt.getId());
        logger.info("computeResultForTestAttempt: questionAttempts " + questionAttempts);
        Long currentTime = System.currentTimeMillis();

        // Collect unique question attempts and in case a question is attempted multiple
        // times then add the duration of multiple attempts for the same question
        HashMap<String, QuestionAttempt> uniqueQuestionAttemptsMap = new HashMap<>();
        if (Objects.nonNull(questionAttempts)) {
            for (QuestionAttempt questionAttempt : questionAttempts) {
                QuestionAttempt existingValueInMap = uniqueQuestionAttemptsMap.get(questionAttempt.getQuestionId());
                if (Objects.isNull(existingValueInMap) || questionAttempt.getCreationTime() > existingValueInMap.getCreationTime()) {
                    if (Objects.nonNull(existingValueInMap)) {
                        questionAttempt.setDuration(questionAttempt.getDuration() + existingValueInMap.getDuration());
                    }
                    uniqueQuestionAttemptsMap.put(questionAttempt.getQuestionId(), questionAttempt);
                }
            }
        }

        logger.info("computeResultForTestAttempt: uniqueQuestionAttemptsMap " + uniqueQuestionAttemptsMap);

        List<TestMetadata> testMetadataList = test.getMetadata(); // getting list of questions sets of test
        List<QuestionAnalytics> questionAnalyticsList = testAttempt.getResultEntries();// getting the attempt details for the test
        Map<String, List<QuestionChangeRecord>> changedQuestionRecordMap = new HashMap<>();
        Map<String, QuestionAnalytics> noMarksQuestions = new HashMap<>();
        Map<String, QuestionAnalytics> subjectiveAnalyticsMap = new HashMap<>();

        // Go inside this if statement if attempt(s) are present for the test
        if (ArrayUtils.isNotEmpty(questionAnalyticsList)) {

            // For each question attempted or not attempted by the user
            for (QuestionAnalytics questionAnalytics : questionAnalyticsList) {

                // Get the details of question
                CMDSQuestion cmdsQuestion = questionMap.get(questionAnalytics.getQuestionId());

                // In case the question is changed in any way, store them in a map corresponding to question id
                if (ArrayUtils.isNotEmpty(questionAnalytics.getQuestionChangeRecords())) {
                    changedQuestionRecordMap.put(questionAnalytics.getQuestionId(), questionAnalytics.getQuestionChangeRecords());
                }

                // The question has no marks(may be an assignment)
                if (questionAnalytics.isNoMarks()) {
                    noMarksQuestions.put(questionAnalytics.getNewQuestionId(), questionAnalytics);
                    continue;
                }

                // In case the question is not a part of question map(which is a near impossible case then)
                // log an error
                if (Objects.isNull(cmdsQuestion)) {
                    logger.error("question not present in questionMap :" + questionAnalytics.getQuestionId());
                    continue;
                }

                // Subjective questions need evaluator so they can't be judged by the system, so are
                // not judgeable by default
                if (!cmdsQuestion.getType().isJudgeable()) {
                    subjectiveAnalyticsMap.put(questionAnalytics.getQuestionId(), questionAnalytics);
                }

            }
        }

        // object to be stored in database
        questionAnalyticsList = new ArrayList<>();

        // Go through the questions of each metadata question set
        for (TestMetadata testMetadata : testMetadataList) {

            List<CMDSTestQuestion> cmdsTestQuestionList = testMetadata.getQuestions(); // Get the list of questions of the metadata

            // Get each of the question object of the list of questions
            for (CMDSTestQuestion cmdsTestQuestion : cmdsTestQuestionList) {
                String qid = cmdsTestQuestion.getQuestionId(); // get the question id

                // Check if the question id present in the question map of metadata
                CMDSQuestion cmdsQuestion = questionMap.get(qid);
                if (Objects.isNull(cmdsQuestion) && !noMarksQuestions.containsKey(qid)) {
                    logger.error("question not present in questionMap :" + qid);
                    continue;
                }

                // Get the question analytics or the attempt details and set the required values inside it
                QuestionAnalytics questionAnalytics;
                if (noMarksQuestions.containsKey(qid)) { // no marks questions are not judgeable, so simply give them the answer
                    questionAnalytics = noMarksQuestions.get(qid);
                    questionAnalytics.setCorrectAnswer(cmdsQuestion.getSolutionInfo().getAnswer());

                } else if (cmdsQuestion.getType().isJudgeable()) { // judge the judgeable questions(not subjective at least)

                    // analytics object for the question
                    questionAnalytics = new QuestionAnalytics();

                    // In case there is some changes in the question then there is no meaning of putting any
                    // analytics data for the question, so the reference of new question is given along with
                    // change record
                    if (ArrayUtils.isNotEmpty(cmdsTestQuestion.getQuestionChangeRecords())) {
                        for (QuestionChangeRecord questionChangeRecord : cmdsTestQuestion.getQuestionChangeRecords()) {
                            if (testAttempt.getEndedAt() < questionChangeRecord.getChangedAt()) {
                                questionAnalytics.setNewQuestionId(qid);
                                qid = questionChangeRecord.getPrevQuestionId();
                                if (changedQuestionRecordMap.containsKey(qid)) {
                                    questionAnalytics.setQuestionChangeRecords(changedQuestionRecordMap.get(qid));
                                }
                                break;
                            }
                        }
                    }

                    // Set the obvious data for analytics
                    questionAnalytics.setQuestionId(qid);
                    questionAnalytics.setEvaluatedTime(currentTime);
                    questionAnalytics.setMaxMarks(cmdsTestQuestion.getMarks().getPositive());

                    // In case due to any reason the answer is changed then put them in record
                    // along with the correct answer to be used in future for evaluation
                    List<AnswerChangeRecord> answerChangeRecords = new ArrayList<>();
                    if (ArrayUtils.isNotEmpty(cmdsQuestion.getAnswerChangeRecords())) {
                        for (AnswerChangeRecord answerChangeRecord : cmdsQuestion.getAnswerChangeRecords()) {
                            logger.info("Answer Change Record: " + answerChangeRecord);
                            if (answerChangeRecord.getChangedTime() > testAttempt.getEndedAt()) {
                                answerChangeRecords.add(answerChangeRecord);
                            }
                        }
                    }
                    questionAnalytics.setAnswerChangeRecords(answerChangeRecords);
                    questionAnalytics.setCorrectAnswer(cmdsQuestion.getSolutionInfo().getAnswer());

                    // Get the question attempt of the user from the map for result evaluation
                    QuestionAttempt questionAttempt = uniqueQuestionAttemptsMap.get(qid);
                    if (Objects.isNull(questionAttempt)
                            || Objects.isNull(questionAttempt.getAnswerGiven())
                            || questionAttempt.getAnswerGiven().isEmpty()) { // User didn't attempt the question

                        questionAnalytics.setQuestionAttemptState(QuestionAttemptState.NOT_ATTEMPTED);
                        if (Objects.nonNull(questionAttempt)) {
                            questionAnalytics.setDuration(questionAttempt.getDuration().longValue());
                        }
                    } else { // User did attempt the question
                        questionAnalytics.setQuestionAttemptState(QuestionAttemptState.EVALUATED);
                        questionAnalytics.setDuration(questionAttempt.getDuration().longValue());
                        questionAnalytics.setAnswerGiven(questionAttempt.getAnswerGiven());
                        boolean isCorrect = isCorrect(cmdsQuestion, questionAttempt.getAnswerGiven());
                        Float marksToBeGiven;
                        if (isCorrect) {
                            questionAnalytics.setCorrectness(EnumBasket.Correctness.CORRECT);
                            marksToBeGiven = cmdsTestQuestion.getMarks().getPositive();
                        } else {
                            questionAnalytics.setCorrectness(EnumBasket.Correctness.INCORRECT);
                            marksToBeGiven = -1 * cmdsTestQuestion.getMarks().getNegative();
                        }
                        questionAnalytics.setMarksGiven(marksToBeGiven);
                    }
                } else {
                    questionAnalytics = subjectiveAnalyticsMap.get(qid);
                }
                if (Objects.nonNull(questionAnalytics)) {
                    logger.info("computeResultForTestAttempt - questionAnalytics: " + questionAnalytics);
                    questionAnalyticsList.add(questionAnalytics);
                }
            }

        }
        testAttempt.setResultEntries(questionAnalyticsList);

        // Calculates the overall analytics using the resulting entries(say categorical analytics of question)
        computeOverallAnalytics(testAttempt, test);
    }

    public void sendPostTestEmailFromAttempt(CMDSTestAttempt testAttempt, ContentInfo contentInfo, CMDSTest cMDSTest) throws BadRequestException {
        if (testAttempt == null) {
            return;
        }
        if (cMDSTest == null) {
            String testId = testAttempt.getTestId();
            if (StringUtils.isNotEmpty(testId)) {
                cMDSTest = cmdsTestManager.getCMDSTestById(testId);
            }
        }
        if (cMDSTest == null) {
            logger.info("no test found for the attempt not sending any emails:" + testAttempt);
            return;
        }
        if (cMDSTest.getMaxStartTime() == null) {
            logger.info("test found but not a timebound test, not sending any emails:" + testAttempt);
            return;
        }
        if (EnumBasket.TestTagType.NORMAL.equals(cMDSTest.getTestTag()) || cMDSTest.getTestTag() == null) {
            logger.info("no email should be sent for this test mode, not sending any emails:" + testAttempt);
            return;
        }
        try {
            communicationManager.sendPostTestEmail(testAttempt, cMDSTest, contentInfo);
        } catch (Exception ex) {
            logger.error("error in sending post test email for attempt:" + testAttempt + " error: " + ex.getMessage());
        }
    }

    private void computeOverallAnalytics(CMDSTestAttempt testAttempt, CMDSTest test) {

        // Get the resulting entries populated previously and in case nothing is present
        // there is no way you can compute the analytics
        List<QuestionAnalytics> questionAnalyticsList = testAttempt.getResultEntries();
        logger.info("Calculating overall analytics for attempt with attempt id {}", testAttempt.getId());

        if (Objects.isNull(questionAnalyticsList)) {
            return;
        }

        Map<String, QuestionAnalytics> questionAnalyticsMap = new HashMap<>(); // to store the data of all the questions

        // Get the data of all the questions which are originally there
        Map<String, QuestionAnalytics> originalQuestionAnalyticsMap
                = questionAnalyticsList
                        .parallelStream()
                        .collect(
                                Collectors.toMap(
                                        QuestionAnalytics::getQuestionId,
                                        Function.identity()
                                )
                        );
        if (ArrayUtils.isNotEmpty(originalQuestionAnalyticsMap.keySet())) {
            questionAnalyticsMap.putAll(originalQuestionAnalyticsMap);
        }

        // Get the data of all the questions which are changed
        Map<String, QuestionAnalytics> newQuestionAnalyticsMap
                = questionAnalyticsList
                        .parallelStream()
                        .filter(CMDSTestAttemptManager::containsNewQuestionId)
                        .collect(
                                Collectors.toMap(
                                        QuestionAnalytics::getQuestionId,
                                        Function.identity()
                                )
                        );

        if (ArrayUtils.isNotEmpty(newQuestionAnalyticsMap.keySet())) {
            questionAnalyticsMap.putAll(newQuestionAnalyticsMap);
        }

        // resetting the cumulative values so as to populate them because
        // by default they have the value `null` as wrapper classes are used
        testAttempt.resetValues();

        Map<String, CategoryAnalytics> testMetadataMarksMap = new HashMap<>();
        List<CategoryAnalytics> categoryAnalyticsList = new ArrayList<>();
        List<String> questionIds = new ArrayList<>();

        // For each categorical metadata inside test
        for (TestMetadata testMetadata : test.getMetadata()) {

            // For all the questions in the metadata
            // get all the question ids
            // get all the questions from question map of question analytics from test attempt
            // only filter those objects which exists
            // convert the QuestionAnalytics to CategoryAnalytics for smooth accumulation
            // accumulate all the CategoryAnalytics value to one object
            // in case nothing could be accumulated return a new object
            CategoryAnalytics categoryInfo
                    = testMetadata.getQuestions()
                            .stream()
                            .map(CMDSTestQuestion::getQuestionId)
                            .map(questionAnalyticsMap::get)
                            .filter(Objects::nonNull)
                            .map(CategoryAnalytics::new)
                            .reduce(CategoryAnalytics::accumulateValues)
                            .orElse(new CategoryAnalytics());
            questionIds.addAll(testMetadata.getQuestions().stream().map(CMDSTestQuestion::getQuestionId).collect(Collectors.toList()));

            // insert all the mandatory values
            categoryInfo.setCategoryType(AnalyticsTypes.SECTION);
            categoryInfo.setName(testMetadata.getName());
            categoryInfo.setMaxMarks(testMetadata.getTotalMarks());

            // set up all the values for further use
            testMetadataMarksMap.put(categoryInfo.getName(), categoryInfo);
            categoryAnalyticsList.add(categoryInfo);
            testAttempt.incrementAnalyticsValues(categoryInfo);
            logger.info("Computing category Analytics" + categoryInfo);
        }

        //Usually used for KVPY Tests
        if (ArrayUtils.isNotEmpty(test.getCustomSectionEvaluationRules())) {

            testAttempt.resetValues();
            questionIds.clear();
            List<String> selectedSectionsForEvaluation = new ArrayList<>();

            for (CustomSectionEvaluationRule customSectionEvaluationRule : test.getCustomSectionEvaluationRules()) {
                List<CategoryAnalytics> sections
                        = customSectionEvaluationRule.getTestMetadataKey()
                                .stream()
                                .filter(testMetadataMarksMap::containsKey)
                                .map(testMetadataMarksMap::get)
                                .sorted(Comparator.comparingDouble(CategoryAnalytics::getMarks).reversed())
                                .collect(Collectors.toList())
                                .stream()
                                .limit(customSectionEvaluationRule.getBestOf())
                                .collect(Collectors.toList());
                sections.forEach(testAttempt::incrementAnalyticsValues);
                selectedSectionsForEvaluation.addAll(sections.stream().map(CategoryAnalytics::getName).collect(Collectors.toList()));
            }
            logger.info("Selected sections for Evaluation" + selectedSectionsForEvaluation);

            List<String> questions
                    = test.getMetadata()
                            .stream()
                            .filter(testMetadata -> selectedSectionsForEvaluation.contains(testMetadata.getName()))
                            .map(TestMetadata::getQuestions)
                            .flatMap(Collection::stream)
                            .map(CMDSTestQuestion::getQuestionId)
                            .collect(Collectors.toList());
            questionIds.addAll(questions);

        }
        //Log all the questions which needs to be analysed
        logger.info("Question List For test" + questionIds);

        //Topic Subject Question Wise analysis starts here
        Set<String> curriculumSubjectList
                = topicTreeDAO.getNamesOfFirstLevelNodes()
                        .stream()
                        .map(BaseTopicTree::getName)
                        .collect(Collectors.toSet());

        //Get all the questions for this test.
        List<CMDSQuestion> questionList = cmdsQuestionDAO.getByIds(questionIds, VedantuRecordState.ACTIVE);

        // Corresponding to each subject find all the question ids mapping to a certain difficulty
        HashMap<String, HashMap<Difficulty, List<String>>> difficultyMap = new HashMap<>();
        // Corresponding to each subject find all the question ids mapping to a certain question type
        HashMap<String, HashMap<QuestionType, List<String>>> questionTypeMap = new HashMap<>();
        // Corresponding to each subject find all the question ids mapping to a certain analysis topic
        HashMap<String, HashMap<String, List<String>>> topicsMap = new HashMap<>();
        // Corresponding to each subject find all the question ids mapping to the subject
        HashMap<String, List<String>> subjectMap = new HashMap<>();

        //Sort the questions as per questionTopics which needs to be analysed
        for (CMDSQuestion cmdsQuestion : questionList) {
            logger.info("Populating all the required data for the question id - {}", cmdsQuestion.getId());

            //get all the main tags from the question
            Set<String> questionTopics = cmdsQuestion.getMainTags();
            logger.info("questionTopics - {}", questionTopics);

            //get the subjects using set intersection which should give a single element set
            Set<String> subjectSet = new HashSet<>();
            if (ArrayUtils.isNotEmpty(questionTopics) && ArrayUtils.isNotEmpty(curriculumSubjectList)) {
                subjectSet = Sets.intersection(questionTopics, curriculumSubjectList);
            }
            //Convert to a List!!
            ArrayList<String> subjects = new ArrayList<>();
            subjects.add("Overall");
            subjects.addAll(subjectSet);

            // Add Overall to the bucket as all questions belong to it.
            Set<String> topicsForAnalysis = cmdsQuestion.getAnalysisTags();
            for (String subject : subjects) {

                questionTypeMap.putIfAbsent(subject, new HashMap<>());
                difficultyMap.putIfAbsent(subject, new HashMap<>());
                topicsMap.putIfAbsent(subject, new HashMap<>());
                subjectMap.putIfAbsent(subject, new ArrayList<>());

                //Filter the questions as per topic type
                HashMap<String, List<String>> subjectTopicMap = topicsMap.get(subject);
                logger.info("subjectTopicMap before population- {}", subjectTopicMap);
                logger.info("subjectTopicMap logs");
                if (ArrayUtils.isNotEmpty(questionTopics)) {
                    Optional.ofNullable(topicsForAnalysis)
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .filter(questionTopics::contains)
                            .collect(Collectors.toList())
                            .stream()
                            .peek(logger::info)
                            .forEach(topic -> {
                                subjectTopicMap.putIfAbsent(topic, new ArrayList<>());
                                subjectTopicMap.get(topic).add(cmdsQuestion.getId());
                            });
                }
                logger.info("subjectTopicMap after population - {}", subjectTopicMap);

                //Filter the questions as per difficulty type
                HashMap<Difficulty, List<String>> subjectDifficultyMap = difficultyMap.get(subject);
                logger.info("subjectDifficultyMap before population - {}", subjectDifficultyMap);
                Difficulty difficulty = cmdsQuestion.getDifficulty();
                subjectDifficultyMap.putIfAbsent(difficulty, new ArrayList<>());
                subjectDifficultyMap.get(difficulty).add(cmdsQuestion.getId());
                logger.info("subjectDifficultyMap after population - {}", subjectDifficultyMap);

                //Filter the questions as per question type
                HashMap<QuestionType, List<String>> questionTypeSubjectMap = questionTypeMap.get(subject);
                logger.info("questionTypeSubjectMap before population - {}", questionTypeSubjectMap);
                QuestionType questionType = cmdsQuestion.getType();
                questionTypeSubjectMap.putIfAbsent(questionType, new ArrayList<>());
                questionTypeSubjectMap.get(questionType).add(cmdsQuestion.getId());
                logger.info("questionTypeSubjectMap after population - {}", questionTypeSubjectMap);

                // Question ids mapped to respective subjects
                logger.info("subjectMap before population - {}", subjectMap);
                subjectMap.get(subject).add(cmdsQuestion.getId());
                logger.info("subjectMap after population - {}", subjectMap);
            }
        }

        // For each analysis topic compute analytics
        for (String subject : topicsMap.keySet()) {
            HashMap<String, List<String>> topicQuestionIdsMap = topicsMap.get(subject);
            for (String analysisTopic : topicQuestionIdsMap.keySet()) {
                List<String> questions = topicQuestionIdsMap.get(analysisTopic);
                CategoryAnalytics categoryInfo
                        = questions.stream()
                                .map(questionAnalyticsMap::get)
                                .filter(Objects::nonNull)
                                .map(CategoryAnalytics::new)
                                .reduce(CategoryAnalytics::accumulateValues)
                                .orElse(new CategoryAnalytics());

                // insert all the mandatory values
                categoryInfo.setTopic(analysisTopic);
                categoryInfo.setSubject(subject);
                categoryInfo.setName("Subject:" + subject + "_Topic:" + analysisTopic);
                categoryInfo.setCategoryType(AnalyticsTypes.TOPIC);
                categoryAnalyticsList.add(categoryInfo);
            }
        }

        // For each Difficulty type compute analytics
        for (String subject : difficultyMap.keySet()) {
            HashMap<Difficulty, List<String>> difficultyQuestionIdsMap = difficultyMap.get(subject);
            for (Difficulty difficulty : difficultyQuestionIdsMap.keySet()) {
                List<String> questions = difficultyQuestionIdsMap.get(difficulty);
                CategoryAnalytics categoryInfo
                        = questions.stream()
                                .map(questionAnalyticsMap::get)
                                .filter(Objects::nonNull)
                                .map(CategoryAnalytics::new)
                                .reduce(CategoryAnalytics::accumulateValues)
                                .orElse(new CategoryAnalytics());

                categoryInfo.setSubject(subject);
                categoryInfo.setName("Subject:" + subject + "_Difficulty:" + difficulty);
                categoryInfo.setDifficulty(difficulty);
                categoryAnalyticsList.add(categoryInfo);
                categoryInfo.setCategoryType(AnalyticsTypes.DIFFICULTY);
            }
        }

        // For each QuestionType compute analytics
        for (String subject : questionTypeMap.keySet()) {
            HashMap<QuestionType, List<String>> questionTypeQuestionIdsMap = questionTypeMap.get(subject);
            for (QuestionType questionType : questionTypeQuestionIdsMap.keySet()) {
                List<String> questions = questionTypeQuestionIdsMap.get(questionType);
                CategoryAnalytics categoryInfo
                        = questions.stream()
                                .map(questionAnalyticsMap::get)
                                .filter(Objects::nonNull)
                                .map(CategoryAnalytics::new)
                                .reduce(CategoryAnalytics::accumulateValues)
                                .orElse(new CategoryAnalytics());
                categoryInfo.setSubject(subject);
                categoryInfo.setQuestionType(questionType);
                categoryInfo.setName("Subject:" + subject + "_Type:" + questionType);
                categoryAnalyticsList.add(categoryInfo);
                categoryInfo.setCategoryType(AnalyticsTypes.QUESTION_TYPE);
            }

        }

        // For each subject compute analytics
        for (String subject : subjectMap.keySet()) {
            List<String> questions = subjectMap.get(subject);
            CategoryAnalytics categoryInfo
                    = questions.stream()
                            .map(questionAnalyticsMap::get)
                            .filter(Objects::nonNull)
                            .map(CategoryAnalytics::new)
                            .reduce(CategoryAnalytics::accumulateValues)
                            .orElse(new CategoryAnalytics());

            // insert all the mandatory values
            categoryInfo.setSubject(subject);
            categoryInfo.setName("Subject:" + subject);
            categoryAnalyticsList.add(categoryInfo);
            categoryInfo.setCategoryType(AnalyticsTypes.SUBJECT);
        }

        logger.info("Computed All Analytics" + categoryAnalyticsList);

        testAttempt.setCategoryAnalyticsList(categoryAnalyticsList);
        testAttempt.setAttemptState(CMDSAttemptState.EVALUATE_COMPLETE);
    }

    private boolean questionHasMarks(CMDSTestQuestion cmdsTestQuestion) {
        return Objects.nonNull(cmdsTestQuestion) && Objects.nonNull(cmdsTestQuestion.getMarks());
    }

    private boolean isCorrect(CMDSQuestion cmdsQuestion, List<String> answerGiven) {
        List<String> answer = cmdsQuestion.getSolutionInfo().getAnswer();
        return cmdsQuestion.getType().isCorrect(EnumBasket.Judgement.JUDGE, answerGiven, answer, EnumBasket.Status.COMPLETE);
    }

    public void sendEndTestsRequestAsync() {
        logger.info("End Test Attempt request");
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.END_TEST_ATTEMPT, new HashMap<>());
        asyncTaskFactory.executeTask(params);

    }

    public void endTests(List<ContentInfoType> contentInfoTypes) throws BadRequestException {
        long tillTime = System.currentTimeMillis() - 5 * DateTimeUtils.MILLIS_PER_MINUTE;
        if (ArrayUtils.isNotEmpty(contentInfoTypes) && (contentInfoTypes.contains(ContentInfoType.SUBJECTIVE) || contentInfoTypes.contains(ContentInfoType.MIXED))) {
            tillTime = System.currentTimeMillis() - 65 * DateTimeUtils.MILLIS_PER_MINUTE;
        }
        List<String> cmdsTestAttemptsForExpiration=
                Optional.ofNullable(cmdsTestAttemptDAO.getAttemptsForExpiration(tillTime, contentInfoTypes))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(ObjectId::toString)
                        .collect(Collectors.toList());
        logger.info("Ending test for attemptIds - {}",cmdsTestAttemptsForExpiration);
        cmdsTestAttemptsForExpiration.forEach(this::sendToSQSForAttemptEndingOperation);
        if(ArrayUtils.isNotEmpty(cmdsTestAttemptsForExpiration) && cmdsTestAttemptsForExpiration.size()>100000){
            logger.error("The size is going beyond 10000, try batch processing.");
        }
    }

    private void sendToSQSForAttemptEndingOperation(String attemptId) {
        awsSQSManager.sendToSQS(SQSQueue.TEST_END_OPS,SQSMessageType.END_TEST_CRON_OPS,gson.toJson(attemptId));
    }

    public void endAttemptWithRelatedOperation(String attemptId) {
        logger.info("endAttemptWithRelatedOperation - attemptId - {}",attemptId);
        CMDSTestAttempt cmdsTestAttempt=cmdsTestAttemptDAO.getTestAttemptById(attemptId,null);
        try{
            ContentInfo contentInfo=contentInfoDAO.getById(cmdsTestAttempt.getContentInfoId());
            CMDSSubmitTestAttemptReq req = new CMDSSubmitTestAttemptReq(cmdsTestAttempt.getId(), TestEndType.AUTOMATIC);
            updateSubmitDetails(req, cmdsTestAttempt);
            contentInfo.setContentState(ContentState.ATTEMPTED);
            CMDSTest test = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());
            //setting attempt type as ATTEMPT_COMPLETE_SOLUTIONS_NOT_UPLOADED and ContentState as NOT_UPLOADED for subjective tests with no files uploaded
            checkForFileInfosInSubjectiveTest(contentInfo, cmdsTestAttempt, DRAFT_STATE_DEFAULT);
            if (ContentInfoType.OBJECTIVE.equals(test.getContentInfoType()) && TestResultVisibility.VISIBLE.equals(contentInfo.getMetadata().getDisplayResultOnEnd())) {
                computeAnalytics(cmdsTestAttempt, test, contentInfo.getStudentId(), contentInfo.getId());
                contentInfo.setContentState(ContentState.EVALUATED);
            }
            contentInfo.setStudentActionLink(contentInfoManager.getCMDSReportLink(contentInfo));

            cmdsTestAttemptDAO.save(cmdsTestAttempt);
            contentInfoManager.updateContent(contentInfo);
                // todo enable for gamification
                // contentInfoManager.processContentInfoForGameAsync(contentInfo);
        }catch (Exception ex){
            logger.error("Error while ending attempt with attempt id {} and the error is {}",cmdsTestAttempt.getId(),ex);
        }
    }

    public void changeAnswerInResultEntries(PostChangeAnswerSQSPojo postChangeAnswerSQSPojo) {
        int size = 100;
        String lastFetchedId = null;
        while (true) {
            List<ContentInfo> contentInfos = contentInfoManager.getContentInfosForTestId(postChangeAnswerSQSPojo.getTestId(), lastFetchedId, size, Collections.singletonList(ContentInfo.Constants._ID));
            if (ArrayUtils.isEmpty(contentInfos)) {
                break;
            }else{
                lastFetchedId = contentInfos.get(contentInfos.size() - 1).getId();
            }
            for (ContentInfo contentInfo : contentInfos) {
                try {
                    UpdateTestAttemptSQSPojoForChangeAnswer updateTestAttemptSQSPojoForChangeAnswer =new UpdateTestAttemptSQSPojoForChangeAnswer(postChangeAnswerSQSPojo.getQuestionId(),contentInfo.getId());
                    awsSQSManager.sendToSQS(SQSQueue.POST_CHANGE_ANSWER_QUEUE,SQSMessageType.UPDATE_TEST_ATTEMPT_AFTER_CHANGE_ANSWER,gson.toJson(updateTestAttemptSQSPojoForChangeAnswer),postChangeAnswerSQSPojo.getTestId());
                } catch (Exception ex) {
                    logger.error("Error in updating Result Entries : " + contentInfo.getId() + " error: ", ex);
                }
            }

            if (contentInfos.size() < size) {
                break;
            }


        }

    }

    public void updateTestAttemptForChangeAnswer(String questionId, String contentInfoId) throws BadRequestException {

        CMDSQuestion cmdsQuestion = cmdsQuestionDAO.getById(questionId);
        List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getEndedAttemptsForContentInfoId(contentInfoId,0,10,Collections.singletonList(CMDSTestAttempt.Constants.RESULT_ENTRIES));

        for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {
            for (QuestionAnalytics questionAnalytics : cMDSTestAttempt.getResultEntries()) {
                if (questionAnalytics.getQuestionId().equals(cmdsQuestion.getId()) || (questionAnalytics.getNewQuestionId() != null && questionAnalytics.getNewQuestionId().equals(cmdsQuestion.getId()))) {
                    questionAnalytics.setCorrectAnswer(cmdsQuestion.getSolutionInfo().getAnswer());
                    questionAnalytics.setAnswerChangeRecords(cmdsQuestion.getAnswerChangeRecords());
                    break;
                }
            }
            try {
                cmdsTestAttemptDAO.updateResultEntriesForTestAttempt(cMDSTestAttempt.getId(), cMDSTestAttempt.getResultEntries());
            }catch (Exception e){
                logger.error("Error in updating result entries for id {} and exception {}", cMDSTestAttempt.getId(),e);
            }
        }
    }

    public void reevaluateTest(PostChangeAnswerSQSPojo postChangeAnswerSQSPojo) throws BadRequestException {

        CMDSTest test = cmdsTestManager.getCMDSTestById(postChangeAnswerSQSPojo.getTestId());
        int questionIndex = 0;
        String section = "";
        if (ArrayUtils.isNotEmpty(test.getMetadata())) {
            for (TestMetadata testMetadata : test.getMetadata()) {
                if (ArrayUtils.isNotEmpty(testMetadata.getQuestions())) {
                    for (CMDSTestQuestion cMDSTestQuestion : testMetadata.getQuestions()) {
                        if (cMDSTestQuestion.getQuestionId().equals(postChangeAnswerSQSPojo.getQuestionId())) {
                            questionIndex = cMDSTestQuestion.getQuestionIndex();
                            if (test.getMetadata().size() > 1) {
                                section = testMetadata.getName();
                            }
                            break;
                        }
                    }
                }
            }
        }

        CMDSQuestion cMDSQuestion = cmdsQuestionDAO.getById(postChangeAnswerSQSPojo.getQuestionId());

        int size = 100;
        String lastFetchedId=null;
        while (true) {

            List<ContentInfo> contentInfos = contentInfoManager.getContentInfosForTestId(postChangeAnswerSQSPojo.getTestId(), lastFetchedId, size);

            if (ArrayUtils.isEmpty(contentInfos)) {
                break;
            }else{
                lastFetchedId = contentInfos.get(contentInfos.size() - 1).getId();
            }

            for (ContentInfo contentInfo : contentInfos) {
                UpdateTestAttemptPojoForTestReevaluation updateTestAttemptPojoForTestReevaluation=new UpdateTestAttemptPojoForTestReevaluation(postChangeAnswerSQSPojo, test, questionIndex, section, cMDSQuestion.getId(), contentInfo);
                awsSQSManager.sendToSQS(SQSQueue.POST_CHANGE_ANSWER_QUEUE,SQSMessageType.UPDATE_TEST_ATTEMPT_AFTER_REEVALUATION,gson.toJson(updateTestAttemptPojoForTestReevaluation),postChangeAnswerSQSPojo.getTestId());
                udpateTestAttemptDueToReevaluation(postChangeAnswerSQSPojo, test, questionIndex, section, cMDSQuestion.getId(), contentInfo);
            }

            if (contentInfos.size() < size) {
                break;
            }
        }
        awsSQSManager.sendToSQS(SQSQueue.POST_CHANGE_ANSWER_QUEUE,SQSMessageType.UPDATE_RANK_AFTER_REEVALUATION,gson.toJson(test), postChangeAnswerSQSPojo.getTestId());
    }

    public void udpateTestAttemptDueToReevaluation(PostChangeAnswerSQSPojo postChangeAnswerSQSPojo, CMDSTest test, int questionIndex, String section,
                                                   String questionId, ContentInfo contentInfo) {
        CMDSTestAttempt latestAttempt = null;
        try {

            if (ContentInfoType.OBJECTIVE.equals(contentInfo.getContentInfoType()) || ContentInfoType.MIXED.equals(contentInfo.getContentInfoType())) {

                List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getEndedAttemptsForContentInfoId(contentInfo.getId(),0,20,null);
                if (ArrayUtils.isEmpty(testAttempts)) {
                    return;
                }

                for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {
                    if (latestAttempt == null) {
                        latestAttempt = cMDSTestAttempt;
                    }

                    if (latestAttempt.getEndedAt() < cMDSTestAttempt.getEndedAt()) {
                        latestAttempt = cMDSTestAttempt;
                    }

                    cMDSTestAttempt.setMarksAcheived(0f);
                    cMDSTestAttempt.setCorrect(0);
                    cMDSTestAttempt.setIncorrect(0);
                    cMDSTestAttempt.setAttempted(0);
                    cMDSTestAttempt.setUnattempted(0);
                    computeAnalytics(cMDSTestAttempt, test, contentInfo.getStudentId(), contentInfo.getId());
                    cmdsTestAttemptDAO.save(cMDSTestAttempt);
                }
            }

        } catch (Exception ex) {
            logger.error("Error in updating Result Entries : " + contentInfo.getId() + " error: ", ex);
        }
        if (latestAttempt != null) {
            CMDSQuestion cmdsQuestion = cmdsQuestionDAO.getById(questionId);
            emailForChangeAnswer(cmdsQuestion, latestAttempt, postChangeAnswerSQSPojo, contentInfo.getStudentId(), questionIndex, section, contentInfo.getContentTitle(), contentInfo.getStudentActionLink());
        }
    }

    public AnswerChangeRecord replaceNumericOptionWithAlphabet(AnswerChangeRecord answerChangeRecord) {
        List<String> prevAnswer = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(answerChangeRecord.getPreviousAnswers())) {

            for (String answer : answerChangeRecord.getPreviousAnswers()) {
                if (answer.equals("1")) {
                    prevAnswer.add("a");
                } else if (answer.equals("2")) {
                    prevAnswer.add("b");
                } else if (answer.equals("3")) {
                    prevAnswer.add("c");
                } else if (answer.equals("4")) {
                    prevAnswer.add("d");
                }
            }
        }
        answerChangeRecord.setPreviousAnswers(prevAnswer);
        List<String> newAnswer = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(answerChangeRecord.getNewAnswers())) {
            for (String answer : answerChangeRecord.getNewAnswers()) {
                if (answer.equals("1")) {
                    newAnswer.add("a");
                } else if (answer.equals("2")) {
                    newAnswer.add("b");
                } else if (answer.equals("3")) {
                    newAnswer.add("c");
                } else if (answer.equals("4")) {
                    newAnswer.add("d");
                }
            }
        }
        answerChangeRecord.setNewAnswers(newAnswer);
        return answerChangeRecord;
    }

    private void emailForChangeAnswer(CMDSQuestion cMDSQuestion, CMDSTestAttempt cMDSTestAttempt, PostChangeAnswerSQSPojo postChangeAnswerSQSPojo, String userId, int questionIndex, String section, String contentTitle, String testLink) {
        if (ArrayUtils.isNotEmpty(cMDSTestAttempt.getResultEntries())) {

            for (QuestionAnalytics questionAnalytics : cMDSTestAttempt.getResultEntries()) {
                if (questionAnalytics.getQuestionId().equals(postChangeAnswerSQSPojo.getQuestionId())) {
                    logger.info("analytics : " + questionAnalytics);
                    HashMap<String, Object> payload = new HashMap<String, Object>();
                    if (ArrayUtils.isEmpty(questionAnalytics.getAnswerChangeRecords())) {
                        return;
                    }
                    int size = questionAnalytics.getAnswerChangeRecords().size();
                    AnswerChangeRecord answerChangeRecord = questionAnalytics.getAnswerChangeRecords().get(size - 1);

                    String correctionType = "";
                    String reevaluationType = "";
                    if (QuestionType.MCQ.equals(cMDSQuestion.getType()) || QuestionType.SCQ.equals(cMDSQuestion.getType())) {
                        answerChangeRecord = replaceNumericOptionWithAlphabet(answerChangeRecord);
                        correctionType = "Correct Option(s) changed from " + StringUtils.join(answerChangeRecord.getPreviousAnswers().toArray(), ",") + " to " + StringUtils.join(answerChangeRecord.getNewAnswers().toArray(), ",");
                        reevaluationType = "Marks awarded for correct option(s)";
                    } else if (QuestionType.NUMERIC.equals(cMDSQuestion.getType())) {
                        correctionType = "Correct Answer Changed from " + StringUtils.join(answerChangeRecord.getPreviousAnswers().toArray(), ",") + " to " + StringUtils.join(answerChangeRecord.getNewAnswers().toArray(), ",");
                        reevaluationType = "Marks awarded for correct answer";
                    }
                    payload.put("correctionType", correctionType);
                    payload.put("reevaluationType", reevaluationType);
                    payload.put("testLink", testLink + "?attemptId=" + cMDSTestAttempt.getId());
                    payload.put("testName", contentTitle);
                    payload.put("testId", cMDSTestAttempt.getTestId());
                    if (StringUtils.isEmpty(section)) {
                        payload.put("questionNumber", String.valueOf(questionIndex));
                    } else {
                        payload.put("questionNumber", questionIndex + " of " + section);
                    }

                    UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);
                    if(Objects.nonNull(userBasicInfo)) {
                        if(StringUtils.isNotEmpty(userBasicInfo.getFullName())) {
                            payload.put("studentName", userBasicInfo.getFullName());
                            try {
                                communicationManager.sendReevaluateMail(payload, userBasicInfo);
                            } catch (Exception ex) {
                                logger.error("Error in sending email for reevaluation");
                            }
                        }
                    }
                    break;
                }
            }

        }

    }

    public void changeQuestionInResultEntries(PostChangeQuestionSQSPojo postChangeQuestionSQSPojo) {
        int size = 100;
        String lastFetchedId=null;

        while (true) {
            logger.info("Processing for changeQuestionInResultEntries");
            List<ContentInfo> contentInfos = contentInfoManager.getContentInfosForTestId(postChangeQuestionSQSPojo.getTestId(), lastFetchedId, size,Collections.singletonList(ContentInfo.Constants._ID));

            if (ArrayUtils.isEmpty(contentInfos)) {
                logger.info("No contentInfos found");
                break;
            }else{
                lastFetchedId = contentInfos.get(contentInfos.size() - 1).getId();
                logger.info("lastFetchedId - {}",lastFetchedId);
            }

            for (ContentInfo contentInfo : contentInfos) {
                try {
                    logger.info("Updating for contentInfoId {}",contentInfo.getId());
                    updateContentInfoForChangeQuestion(postChangeQuestionSQSPojo.getQuestionChangeRecord(),contentInfo.getId());
                } catch (Exception ex) {
                    logger.error("Error in updating Result Entries : " + contentInfo.getId() + " error: ", ex);
                }
            }

            if (contentInfos.size() < size) {
                break;
            }

        }

    }

    private void updateContentInfoForChangeQuestion(QuestionChangeRecord questionChangeRecord, String contentInfoId) throws BadRequestException {


        List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getEndedAttemptsForContentInfoId(contentInfoId,0,20,Collections.singletonList(CMDSTestAttempt.Constants.RESULT_ENTRIES));
        if (ArrayUtils.isEmpty(testAttempts)) {
            logger.info("No test attempts found for contentInfoId {}",contentInfoId);
            return;
        }

        for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {
            logger.info("Changing for test attempt id {}",cMDSTestAttempt.getId());

            for (QuestionAnalytics questionAnalytics : cMDSTestAttempt.getResultEntries()) {

                if (questionAnalytics.getQuestionId().equals(questionChangeRecord.getPrevQuestionId())
                        || (questionAnalytics.getNewQuestionId() != null && questionAnalytics.getNewQuestionId().equals(questionChangeRecord.getPrevQuestionId()))) {
                    questionAnalytics.setNewQuestionId(questionChangeRecord.getNewQuestionId());
                    if (ArrayUtils.isEmpty(questionAnalytics.getQuestionChangeRecords())) {
                        questionAnalytics.setQuestionChangeRecords(new ArrayList<>());
                    }
                    questionAnalytics.getQuestionChangeRecords().add(questionChangeRecord);
                    questionAnalytics.setAnswerChangeRecords(new ArrayList<>());
                    break;
                }
            }
            logger.info("Updating new data for test attempt with id {} and data {}",cMDSTestAttempt.getId(),cMDSTestAttempt.getResultEntries());
            cmdsTestAttemptDAO.updateResultEntriesForTestAttempt(cMDSTestAttempt.getId(),cMDSTestAttempt.getResultEntries());
        }

    }

    public void processTestPostChangeQuestion(PostChangeQuestionSQSPojo postChangeQuestionSQSPojo) throws BadRequestException {
        CMDSTest cMDSTest = cmdsTestManager.getCMDSTestById(postChangeQuestionSQSPojo.getTestId());

        int size = 100;
        String section = "";
        int questionIndex = 0;
        String lastFetchedId=null;

        while (true) {
            logger.info("processTestPostChangeQuestion - lastFetchedId - {}",lastFetchedId);
            List<ContentInfo> contentInfos = contentInfoManager.getContentInfosForTestId(postChangeQuestionSQSPojo.getTestId(), lastFetchedId, size);
            if (ArrayUtils.isEmpty(contentInfos)) {
                break;
            }else{
                lastFetchedId = contentInfos.get(contentInfos.size() - 1).getId();
            }

            for (ContentInfo contentInfo : contentInfos) {
                try {

                    if (ContentInfoType.OBJECTIVE.equals(contentInfo.getContentInfoType()) || ContentInfoType.MIXED.equals(contentInfo.getContentInfoType())) {
                        logger.info("Reevaluating for contentInfo with id {}",contentInfo.getId());

                        TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();

                        if (Objects.isNull(testContentInfoMetadata)) {
                            continue;
                        }

                        List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getEndedAttemptsForContentInfoId(contentInfo.getId(),0,20,null);
                        if (ArrayUtils.isEmpty(testAttempts)) {
                            logger.info("No attempts present for contentInfo");
                            continue;
                        }

                        CMDSTestAttempt latestAttempt = null;

                        logger.info("Processing for test attempts");
                        for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {

                            if (latestAttempt == null) {
                                latestAttempt = cMDSTestAttempt;
                            }

                            if (latestAttempt.getEndedAt() < cMDSTestAttempt.getEndedAt()) {
                                latestAttempt = cMDSTestAttempt;
                            }

                            logger.info("Processing status for test attempt id {}",cMDSTestAttempt.getId());
                            if (ChangeQuestionActionType.NO_MARKS.equals(postChangeQuestionSQSPojo.getChangeQuestionActionType())) {
                                logger.info("postQuestionChangeNoMarksReEvaluate");
                                postQuestionChangeNoMarksReEvaluate(cMDSTest, cMDSTestAttempt, postChangeQuestionSQSPojo.getQuestionChangeRecord().getPrevQuestionId(), questionIndex, section);
                            } else if (ChangeQuestionActionType.GRACE_MARKS.equals(postChangeQuestionSQSPojo.getChangeQuestionActionType())) {
                                logger.info("postQuestionChangeGraceMarksReEvaluate");
                                postQuestionChangeGraceMarksReEvaluate(cMDSTest, cMDSTestAttempt, postChangeQuestionSQSPojo.getQuestionChangeRecord().getPrevQuestionId());
                            }else{
                                logger.info("no processing");
                            }
                        }

                        if (latestAttempt != null && !UserType.NOT_REGISTERED.equals(contentInfo.getUserType())) {
                            emailForChangeQuestion(latestAttempt.getId(), cMDSTest.getId(), questionIndex, section, contentInfo.getStudentId(), contentInfo.getContentTitle(), contentInfo.getStudentActionLink());
                        }

                    }else{
                        logger.info("No reevaluation is done for contentInfo with id {}",contentInfo.getId());
                    }

                } catch (Exception ex) {
                    logger.error("Error in updating Result Entries : " + contentInfo.getId() + " error: ", ex);
                }

            }

            if (contentInfos.size() < size) {
                break;
            }


        }
        if (ChangeQuestionActionType.GRACE_MARKS.equals(postChangeQuestionSQSPojo.getChangeQuestionActionType())
                || ChangeQuestionActionType.NO_MARKS.equals(postChangeQuestionSQSPojo.getChangeQuestionActionType())) {
            contentInfoManager.calculateTestRank(cMDSTest);
        }

    }

    public void processTestPostChangeQuestionMarks(PostChangeQuestionSQSPojo postChangeQuestionSQSPojo) throws BadRequestException {

        CMDSTest cMDSTest = cmdsTestManager.getCMDSTestById(postChangeQuestionSQSPojo.getTestId());
        logger.info("Reevaluating all the content infos for the test id {}",postChangeQuestionSQSPojo.getTestId());
        int size = 100;
        String lastFetchedId=null;

        while (true) {

            logger.info("Last fetched id is {}",lastFetchedId);

            List<ContentInfo> contentInfos = contentInfoManager.getContentInfosForTestId(postChangeQuestionSQSPojo.getTestId(), lastFetchedId, size);
            if (ArrayUtils.isEmpty(contentInfos)) {
                break;
            }else{
                lastFetchedId = contentInfos.get(contentInfos.size() - 1).getId();
            }

            for (ContentInfo contentInfo : contentInfos) {
                try {

                    if (ContentInfoType.OBJECTIVE.equals(contentInfo.getContentInfoType()) || ContentInfoType.MIXED.equals(contentInfo.getContentInfoType())) {

                        TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();

                        if (testContentInfoMetadata == null) {
                            continue;
                        }


                        List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getEndedAttemptsForContentInfoId(contentInfo.getId(),0,20,null);


                        if (ArrayUtils.isEmpty(testAttempts)) {
                            continue;
                        }

                        if (ChangeQuestionActionType.NO_ACTION.equals(postChangeQuestionSQSPojo.getChangeQuestionActionType())) {
                            for(CMDSTestAttempt cmdsTestAttempt:testAttempts) {
                                postQuestionChangeMarksReEvaluate(cMDSTest, cmdsTestAttempt, postChangeQuestionSQSPojo);
                            }

                        }

                    }

                } catch (Exception ex) {
                    logger.error("Error in updating Result Entries : " + contentInfo.getId() + " error: ", ex);
                }
            }
            if (contentInfos.size() < size) {
                break;
            }
        }
        if (ChangeQuestionActionType.NO_ACTION.equals(postChangeQuestionSQSPojo.getChangeQuestionActionType())
                || ChangeQuestionActionType.NO_MARKS.equals(postChangeQuestionSQSPojo.getChangeQuestionActionType())) {
            awsSQSManager.sendToSQS(SQSQueue.POST_TEST_END_OPS, SQSMessageType.CALCULATE_TEST_ANALYTICS, cMDSTest.getId());
            contentInfoManager.calculateTestRank(cMDSTest);
        }
    }

    private void postQuestionChangeMarksReEvaluate(CMDSTest cMDSTest, CMDSTestAttempt cMDSTestAttempt, PostChangeQuestionSQSPojo postChangeQuestionSQSPojo)
            throws UnsupportedEncodingException, InternalServerErrorException {

        logger.info("Reevaluating for test attempt id {}",cMDSTestAttempt.getId());

        QuestionChangeRecord questionChangeRecord = postChangeQuestionSQSPojo.getQuestionChangeRecord();
        String questionId = questionChangeRecord.getNewQuestionId();

        float positiveMarks = questionChangeRecord.getPositiveMarks();
        float negativeMarks = questionChangeRecord.getNegativeMarks();

        float prevPositiveMarks = questionChangeRecord.getprevQuestionPositiveMarks();

        CMDSTestQuestion cMDSTestQuestion = null;

        if (ArrayUtils.isEmpty(cMDSTest.getMetadata())) {
            return;
        }

        for (TestMetadata testMetadata : cMDSTest.getMetadata()) {

            if (ArrayUtils.isEmpty(testMetadata.getQuestions())) {
                continue;
            }

            for (CMDSTestQuestion question : testMetadata.getQuestions()) {

                if (question.getQuestionId().equals(questionId)) {
                    cMDSTestQuestion = question;
                }

            }

        }

        if (cMDSTestQuestion == null) {
            logger.error("Question not found : " + questionId);
            return;
        }

        if (ArrayUtils.isEmpty(cMDSTestAttempt.getResultEntries())) {
            return;
        }

        for (QuestionAnalytics questionAnalytics : cMDSTestAttempt.getResultEntries()) {

            if (questionAnalytics.getQuestionId().equals(questionId)) {

                cMDSTestAttempt.setEvaluatedTime(System.currentTimeMillis());
                cMDSTestAttempt.setLastUpdated(System.currentTimeMillis());
                if (Objects.nonNull(cMDSTestAttempt.getReevaluationNumber())) {
                    cMDSTestAttempt.setReevaluationNumber(cMDSTestAttempt.getReevaluationNumber() + 1);
                } else {
                    cMDSTestAttempt.setReevaluationNumber(1);
                }
                cMDSTestAttempt.setReevaluationTime(System.currentTimeMillis());
                questionAnalytics.setEvaluatedTime(System.currentTimeMillis());
                cMDSTestAttempt.setMarksAcheived(cMDSTestAttempt.getMarksAcheived() - questionAnalytics.getMarksGiven());
                cMDSTestAttempt.setTotalMarks(cMDSTestAttempt.getTotalMarks() - prevPositiveMarks);

                if (EnumBasket.Correctness.CORRECT.equals(questionAnalytics.getCorrectness())) {
                    questionAnalytics.setMarksGiven(positiveMarks);
                } else if (EnumBasket.Correctness.INCORRECT.equals(questionAnalytics.getCorrectness())) {
                    negativeMarks = -1 * negativeMarks;
                    questionAnalytics.setMarksGiven(negativeMarks);
                }
                cMDSTestAttempt.setTotalMarks(cMDSTestAttempt.getTotalMarks() + positiveMarks);
                cMDSTestAttempt.setMarksAcheived(cMDSTestAttempt.getMarksAcheived() + questionAnalytics.getMarksGiven());
                break;
            }
        }
        if(ArrayUtils.isNotEmpty(cMDSTestAttempt.getCategoryAnalyticsList())){
            computeAnalytics(cMDSTestAttempt, cMDSTest);
        }
        cmdsTestAttemptDAO.save(cMDSTestAttempt);
    }


    private void emailForChangeQuestion(String attemptId, String testId, int questionIndex, String section, String userId, String testName, String testLink) {

        HashMap<String, Object> bodyScopes = new HashMap<>();
        String correctionType = "Question is identified to be in-correct";
        String reevaluationType = "No marks given (Total marks of test reduced)";
        if (StringUtils.isEmpty(section)) {
            bodyScopes.put("questionNumber", String.valueOf(questionIndex));
        } else {
            bodyScopes.put("questionNumber", questionIndex + " of " + section);
        }
        bodyScopes.put("correctionType", correctionType);
        bodyScopes.put("reevaluationType", reevaluationType);
        bodyScopes.put("testName", testName);
        bodyScopes.put("testLink", testLink + "?attemptId=" + attemptId);
        bodyScopes.put("testId", testId);
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);
        bodyScopes.put("studentName", userBasicInfo.getFullName());
        try {
            communicationManager.sendReevaluateMail(bodyScopes, userBasicInfo);
        } catch (Exception ex) {
            logger.error("Error in sending email for reevaluation");
        }
        //break;
        //questionIndex
        //testName
        //studentName
        //correctionType
        //reevaluationType
        //testLink
    }

    private void postQuestionChangeNoMarksReEvaluate(CMDSTest cMDSTest, CMDSTestAttempt cMDSTestAttempt, String questionId, int questionIndex, String section) throws InternalServerErrorException, UnsupportedEncodingException {

        CMDSTestQuestion cMDSTestQuestion = null;

        if (ArrayUtils.isEmpty(cMDSTest.getMetadata())) {
            return;
        }

        for (TestMetadata testMetadata : cMDSTest.getMetadata()) {

            if (ArrayUtils.isEmpty(testMetadata.getQuestions())) {
                continue;
            }

            for (CMDSTestQuestion question : testMetadata.getQuestions()) {

                if (question.getQuestionId().equals(questionId)) {
                    cMDSTestQuestion = question;
                    break;
                }

                if (ArrayUtils.isNotEmpty(question.getQuestionChangeRecords())) {
                    for (QuestionChangeRecord questionChangeRecord : question.getQuestionChangeRecords()) {
                        if (questionId.equals(questionChangeRecord.getPrevQuestionId())) {
                            cMDSTestQuestion = question;
                        }
                    }
                }

            }

        }

        if (Objects.isNull(cMDSTestQuestion)) {
            logger.error("Question not found : " + questionId);
            return;
        }

        if (ArrayUtils.isEmpty(cMDSTestAttempt.getResultEntries())) {
            return;
        }

        for (QuestionAnalytics questionAnalytics : cMDSTestAttempt.getResultEntries()) {
            if (questionAnalytics.getQuestionId().equals(questionId) && !questionAnalytics.isNoMarks()) {

                if (EnumBasket.Correctness.CORRECT.equals(questionAnalytics.getCorrectness())) {
                    cMDSTestAttempt.setCorrect(cMDSTestAttempt.getCorrect() - 1);

                } else if (EnumBasket.Correctness.INCORRECT.equals(questionAnalytics.getCorrectness())) {
                    cMDSTestAttempt.setIncorrect(cMDSTestAttempt.getIncorrect() - 1);

                }
                cMDSTestAttempt.setAttempted(cMDSTestAttempt.getAttempted() - 1);
                cMDSTestAttempt.setMarksAcheived(cMDSTestAttempt.getMarksAcheived() - questionAnalytics.getMarksGiven());
                questionAnalytics.setCorrectness(null);
                questionAnalytics.setMarksGiven(0f);
                questionAnalytics.setNoMarks(true);
                cMDSTestAttempt.setTotalMarks(cMDSTestAttempt.getTotalMarks() - cMDSTestQuestion.getMarks().getPositive());

                break;
            }
        }

        cMDSTestAttempt.setMarksAcheived(0f);
        cMDSTestAttempt.setUnattempted(0);
        cMDSTestAttempt.setAttempted(0);
        cMDSTestAttempt.setCorrect(0);
        cMDSTestAttempt.setIncorrect(0);
        cMDSTestAttempt.setReevaluationNumber(cMDSTestAttempt.getReevaluationNumber()+1);
        computeAnalytics(cMDSTestAttempt, cMDSTest);

        cmdsTestAttemptDAO.save(cMDSTestAttempt);
    }

    private void postQuestionChangeGraceMarksReEvaluate(CMDSTest cMDSTest, CMDSTestAttempt cMDSTestAttempt, String questionId) {

        CMDSTestQuestion cMDSTestQuestion = null;

        if (ArrayUtils.isEmpty(cMDSTest.getMetadata())) {
            return;
        }

        for (TestMetadata testMetadata : cMDSTest.getMetadata()) {

            if (ArrayUtils.isEmpty(testMetadata.getQuestions())) {
                continue;
            }

            for (CMDSTestQuestion question : testMetadata.getQuestions()) {

                if (question.getQuestionId().equals(questionId)) {
                    cMDSTestQuestion = question;
                }

            }

        }

        if (cMDSTestQuestion == null) {
            logger.error("Question not found : " + questionId);
            return;
        }

        if (ArrayUtils.isEmpty(cMDSTestAttempt.getResultEntries())) {
            return;
        }

        for (QuestionAnalytics questionAnalytics : cMDSTestAttempt.getResultEntries()) {

            if (questionAnalytics.getQuestionId().equals(questionId)) {

                if (EnumBasket.Correctness.CORRECT.equals(questionAnalytics.getCorrectness())) {
                    cMDSTestAttempt.setCorrect(cMDSTestAttempt.getCorrect() - 1);

                } else if (EnumBasket.Correctness.INCORRECT.equals(questionAnalytics.getCorrectness())) {
                    cMDSTestAttempt.setIncorrect(cMDSTestAttempt.getIncorrect() - 1);

                }

                if (questionAnalytics.isNoMarks()) {
                    cMDSTestAttempt.setTotalMarks(cMDSTestAttempt.getTotalMarks() + cMDSTestQuestion.getMarks().getPositive());
                    questionAnalytics.setNoMarks(false);
                }

                cMDSTestAttempt.setMarksAcheived(cMDSTestAttempt.getMarksAcheived() - questionAnalytics.getMarksGiven());
                questionAnalytics.setCorrectness(null);
                questionAnalytics.setMarksGiven(cMDSTestQuestion.getMarks().getPositive());
                cMDSTestAttempt.setMarksAcheived(cMDSTestAttempt.getMarksAcheived() + questionAnalytics.getMarksGiven());

                break;
            }
        }
        cMDSTestAttempt.setReevaluationNumber(cMDSTestAttempt.getReevaluationNumber()+1);
        cmdsTestAttemptDAO.save(cMDSTestAttempt);

    }

    public GetLeaderboardInfo getLeaderboardInfoForAttempt(String attemptId, String callingUserId, Integer start, Integer size) throws BadRequestException, ForbiddenException, NotFoundException {

        logger.info("Request : " + attemptId + " callingUserId : " + callingUserId);
        if (StringUtils.isEmpty(attemptId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad request error. Missing params : id - " + attemptId + " callingUserId : " + callingUserId);
        }

        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(attemptId);
        if (Objects.isNull(cmdsTestAttempt)) {
            String errorMessage = "Attempt event is not found for id : " + attemptId + " callingUserId: "
                    + callingUserId;
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (callingUserId == null || (!callingUserId.equals(contentInfo.getStudentId()) && !callingUserId.equals(contentInfo.getTeacherId())))) {
            logger.warn("Illegal resource access : userId -" + callingUserId + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Action not allowed for the current user");
        }
        String studentId = contentInfo.getStudentId();


        if (start == null || size == null) {
            start = 0;
            size = 100;
        }

        CMDSTest cmdsTest = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());

        if (cmdsTest.getMaxStartTime() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Non competitve test");
        }

        List<StudentScoreInfo> studentScoreInfos = cmdsTestManager.getStudentScoreInfos(cmdsTest);
        long buffer = (long) 5 * DateTimeUtils.MILLIS_PER_MINUTE;
        long currentTime = System.currentTimeMillis();

        if (cmdsTest.getDuration() == null) {
            cmdsTest.setDuration(0l);
        }

        if (cmdsTest.getMaxStartTime() + cmdsTest.getDuration() + buffer > currentTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "test end time not happened");
        }

        if (ArrayUtils.isNotEmpty(studentScoreInfos)) {

            studentScoreInfos.sort(new Comparator<StudentScoreInfo>() {
                @Override
                public int compare(StudentScoreInfo o1, StudentScoreInfo o2) {
                    Float o1Percent = o1.getMarksAchieved() * 100 / o1.getTotalMarks();
                    Float o2Percent = o2.getMarksAchieved() * 100 / o2.getTotalMarks();
                    int sComp = o2Percent.compareTo(o1Percent);
                    if (sComp != 0) {
                        return sComp;
                    }
                    return o1.getDuration().compareTo(o2.getDuration());
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            });

            Integer rank = 1;
            int lastSame = 0;
            Float lastMarksPercent = null;
            Long lastStudentDuration = null;
            StudentScoreInfo userScoreInfo = null;
            GetLeaderboardInfo getLeaderboardInfo = new GetLeaderboardInfo();
            for (StudentScoreInfo studentScoreInfo : studentScoreInfos) {
                Float studentMarksPercent = studentScoreInfo.getMarksAchieved() / studentScoreInfo.getTotalMarks();
                Long studentDuration = studentScoreInfo.getDuration();
                if (lastMarksPercent == null) {

                    lastMarksPercent = studentMarksPercent;
                    lastStudentDuration = studentDuration;
                    studentScoreInfo.setRank(rank);
                    rank++;
                    lastSame++;
                    studentScoreInfo.setRank(rank - lastSame);

                    if (studentScoreInfo.getUserId().equals(studentId)) {

                        userScoreInfo = studentScoreInfo;
                        getLeaderboardInfo.setIndex(rank - lastSame);

                        //break;
                    }

                } else {

                    if (lastMarksPercent.equals(studentMarksPercent) && (lastStudentDuration != null && lastStudentDuration.equals(studentDuration))) {
                        lastSame++;
                    } else {
                        lastMarksPercent = studentMarksPercent;
                        lastStudentDuration = studentDuration;
                        lastSame = 1;
                    }
                    rank++;
                    studentScoreInfo.setRank(rank - lastSame);

                    if (studentScoreInfo.getUserId().equals(studentId)) {
                        userScoreInfo = studentScoreInfo;
                        getLeaderboardInfo.setIndex(rank - lastSame);
                    }
                }

            }

            getLeaderboardInfo.setStudentScoreInfos(studentScoreInfos.subList(start, start + size > studentScoreInfos.size() ? studentScoreInfos.size() : start + size));
            if (cmdsTestAttempt.getStartedAt() >= cmdsTest.getMinStartTime() && cmdsTestAttempt.getStartedAt() <= cmdsTest.getMaxStartTime()) {
                getLeaderboardInfo.setUserScoreInfo(userScoreInfo);
            } else {
                getLeaderboardInfo.setIndex(null); //in case of reattempt not sending the rank, which is set in the above code
            }
            getLeaderboardInfo.setTotal(studentScoreInfos.size());

            return getLeaderboardInfo;

        }

        return new GetLeaderboardInfo();

    }

    public TestTableStatsRes getTestTableStats(Long callingUserId, String attemptId) throws NotFoundException, ForbiddenException, BadRequestException {

        logger.info("Request : " + attemptId + " callingUserId : ");
        if (StringUtils.isEmpty(attemptId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad request error. Missing params : id - "
                    + attemptId + " callingUserId : " + callingUserId);
        }

        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(attemptId);
        if (Objects.isNull(cmdsTestAttempt)) {
            String errorMessage = "Attempt event is not found for id : " + attemptId + " callingUserId: " + callingUserId;
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (Objects.isNull(callingUserId)
                || (!String.valueOf(callingUserId).equals(contentInfo.getStudentId())
                && !String.valueOf(callingUserId).equals(contentInfo.getTeacherId())))) {
            logger.info("Illegal resource access : userId -" + callingUserId + " contentInfo :" + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Action not allowed for the current user");
        }

        CMDSTest cmdsTest = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());
        long buffer = (long) 5 * DateTimeUtils.MILLIS_PER_MINUTE;
        long currentTime = System.currentTimeMillis();
        if (cmdsTest.getMaxStartTime() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Non competitive test");
        }

        if (cmdsTest.getCustomSectionEvaluationRules() != null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Non Supported in Custom Evaluation Rule(KVPY)");
        }

        if (cmdsTest.getDuration() == null) {
            cmdsTest.setDuration(0l);
        }

        if (cmdsTest.getMaxStartTime() + cmdsTest.getDuration() + buffer > currentTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "test end time not happened");
        }
        if (cmdsTest.getDoNotShowResultsTill() != null && System.currentTimeMillis() < cmdsTest.getDoNotShowResultsTill()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Results will be available later.");
        }
        if (cmdsTest.getMaxStartTime() < cmdsTestAttempt.getStartTime()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Test was attempted after max start time and wont be ranked.");
        }

        List<StudentScoreInfo> studentScoreInfos = cmdsTestManager.getStudentScoreInfos(cmdsTest);

        studentScoreInfos.sort(new Comparator<StudentScoreInfo>() {
            @Override
            public int compare(StudentScoreInfo o1, StudentScoreInfo o2) {
                Float o1Marks = o1.getMarksAchieved();
                Float o2Marks = o2.getMarksAchieved();
                int sComp = o1Marks.compareTo(o2Marks);
                if (sComp != 0) {
                    return sComp;
                }
                return o1.getDuration().compareTo(o2.getDuration());
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });
        List<TestMarksPercentageSection> testMarksPercentageSections = new ArrayList<>();

        if (ArrayUtils.isEmpty(studentScoreInfos)) {
            TestTableStatsRes testTableStatsRes = new TestTableStatsRes();
            testTableStatsRes.setTestMarksPercentageSections(testMarksPercentageSections);
            return testTableStatsRes;
        }

        Float maxTotalMarks = null;
        Float maxAchievedMarks = null;
        for (StudentScoreInfo studentScoreInfo : studentScoreInfos) {
            if (maxTotalMarks == null) {
                maxTotalMarks = studentScoreInfo.getTotalMarks();
            } else {
                if (maxTotalMarks < studentScoreInfo.getTotalMarks()) {
                    maxTotalMarks = studentScoreInfo.getTotalMarks();
                }
            }
            if (maxAchievedMarks == null) {
                maxAchievedMarks = studentScoreInfo.getMarksAchieved();
            } else {
                if (maxAchievedMarks < studentScoreInfo.getMarksAchieved()) {
                    maxAchievedMarks = studentScoreInfo.getMarksAchieved();
                }
            }
        }

        Float firstMinPct = (float) LOWEST_MIN_PCT;
        Float firstMin = firstMinPct * maxTotalMarks * 0.01f;
        Float step = (float) STEP_PCT * (maxTotalMarks / 100);
        Float currentBucketMin = 0f;
        Float currentBucketMax = firstMin + step;
        Float userMarks = cmdsTestAttempt.getMarksAcheived();

        logger.info("User marks: " + userMarks);
        logger.info("First max: " + currentBucketMax);
        logger.info("First Min: " + firstMin);
        logger.info("step: " + step);

        Integer numOfStudents = 0;
        Integer totalNumOfStudents = studentScoreInfos.size();

        boolean userIncluded = false;
        boolean setUserIncluded = false;

        //setting userIncluded data only if the user attempt falls between min, max time
        if (cmdsTestAttempt.getStartedAt() >= cmdsTest.getMinStartTime()
                && cmdsTestAttempt.getStartedAt() <= cmdsTest.getMaxStartTime()
                && (String.valueOf(callingUserId).equals(contentInfo.getStudentId()))) {
            setUserIncluded = true;
        }

        for (int i = 0; i < studentScoreInfos.size(); i++) {
            StudentScoreInfo studentScoreInfo = studentScoreInfos.get(i);
            Float marks = studentScoreInfo.getMarksAchieved();
//            logger.info("counter loop currentBucketMax" + currentBucketMax);
//            logger.info("counter loop marks" + marks);
//            logger.info("counter loop currentBucketMin" + currentBucketMin);
//            logger.info("Buckt counter" + testMarksPercentageSections.size());
            if (TABLE_BUCKET_COUNT - testMarksPercentageSections.size() == 1) {
                //Last bucket;
                currentBucketMax = maxTotalMarks;
            }
            if (marks < currentBucketMax || Objects.equals(marks, currentBucketMax)) {
                numOfStudents++;
                logger.info("counter loop count" + numOfStudents);
            } else {
                TestMarksPercentageSection testMarksPercentageSection = new TestMarksPercentageSection();
                testMarksPercentageSection.setMinMarksPct((currentBucketMin / maxTotalMarks) * 100);
                testMarksPercentageSection.setMaxMarksPct((currentBucketMax / maxTotalMarks) * 100);
                testMarksPercentageSection.setPctOfUsers((int) (((float) numOfStudents / (float) totalNumOfStudents) * 100));
                logger.info("Calculating Pct" + numOfStudents + "/" + totalNumOfStudents);
                if ((((userMarks > currentBucketMin)
                        && Objects.equals(userMarks, currentBucketMax))) && !userIncluded
                        && setUserIncluded) {
                    testMarksPercentageSection.setUserPresent(true);
                    userIncluded = true;
                }
                testMarksPercentageSections.add(testMarksPercentageSection);
                //Add step to jump buckets.
                currentBucketMin = currentBucketMax;
                currentBucketMax = currentBucketMax + step;
                numOfStudents = 0;
                i--;
            }
        }

        while (testMarksPercentageSections.size() < TABLE_BUCKET_COUNT) {
            //Zero fill buckets.
            TestMarksPercentageSection testMarksPercentageSection = new TestMarksPercentageSection();
            testMarksPercentageSection.setMaxMarksPct((currentBucketMax / maxTotalMarks) * 100);
            testMarksPercentageSection.setMinMarksPct((currentBucketMin / maxTotalMarks) * 100);
            testMarksPercentageSection.setPctOfUsers((int) (((float) numOfStudents / (float) totalNumOfStudents) * 100));
//            logger.info("Calculating Pct ZF" + numOfStudents + "/" + totalNumOfStudents);
            if (TABLE_BUCKET_COUNT - testMarksPercentageSections.size() == 1) {
                //Last bucket
                testMarksPercentageSection.setMaxMarksPct(100f);
            }
            if ((((userMarks > currentBucketMin) && userMarks < currentBucketMax)
                    || (Objects.equals(userMarks, currentBucketMax))) && !userIncluded && setUserIncluded) {
                testMarksPercentageSection.setUserPresent(true);
                userIncluded = true;
            }
            testMarksPercentageSections.add(testMarksPercentageSection);
            //Add step to jump buckets.
            currentBucketMin = currentBucketMax;
            currentBucketMax = currentBucketMax + step;
            numOfStudents = 0;
        }
//        List<String> subjects = cmdsTest.getMetadata().stream().map(TestMetadata::getName).collect(Collectors.toList());
        List<String> subjects = cmdsTestAttempt.getCategoryAnalyticsList().stream()
                .filter(sectionResultInfo -> sectionResultInfo.getCategoryType().equals(AnalyticsTypes.SUBJECT) && !sectionResultInfo.getSubject().equals("Overall"))
                .map(CategoryAnalytics::getSubject)
                .collect(Collectors.toList());

        if (!ArrayUtils.isNotEmpty(cmdsTest.getCustomSectionEvaluationRules())) {
            //NON KVPY tests
            HashMap<String, List<SectionResultInfo>> subjectSectionMap = new HashMap<String, List<SectionResultInfo>>() {
            };
            HashMap<String, CategoryAnalytics> userSubjectMap = new HashMap<String, CategoryAnalytics>() {
            };
            for (String subject : subjects) {
                CategoryAnalytics userSubjectSectionInfo = cmdsTestAttempt.getCategoryAnalyticsList().stream()
                        .filter(sectionResultInfo -> subject.equals(sectionResultInfo.getSubject()) && sectionResultInfo.getCategoryType().equals(AnalyticsTypes.SUBJECT))
                        .findFirst().orElse(null);
                userSubjectMap.put(subject, userSubjectSectionInfo);
                List<SectionResultInfo> subjectSectionInfos = studentScoreInfos.stream()
                        .map(studentScoreInfo
                                -> studentScoreInfo.getSectionResultInfos().stream()
                                .filter(sectionResultInfo -> subject.equals(sectionResultInfo.getSubject()) && sectionResultInfo.getCategoryType().equals(AnalyticsTypes.SUBJECT))
                                .findFirst().orElse(null))
                        .collect(Collectors.toList());
                subjectSectionMap.put(subject, subjectSectionInfos);
            }
            for (TestMarksPercentageSection testMarksPercentageSection : testMarksPercentageSections) {
                HashMap<String, HashMap<String, ?>> subjectMap = new HashMap<String, HashMap<String, ?>>() {
                };
                for (String s : subjects) {
                    HashMap<String, Object> userMap = new HashMap<String, Object>() {
                    };
                    List<SectionResultInfo> subjectSectionInfos = subjectSectionMap.get(s);
                    CategoryAnalytics userSubjectSectionInfo = userSubjectMap.get(s);
                    Float subjectMaxMark = userSubjectSectionInfo.getMaxMarks();
                    Float subjectPctSectionMinMark = testMarksPercentageSection.getMinMarksPct() * subjectMaxMark * 0.01f;
                    Float subjectPctSectionMaxMark = testMarksPercentageSection.getMaxMarksPct() * subjectMaxMark * 0.01f;
                    if (subjectPctSectionMinMark == 0) {
                        //Stupid Hack for -ve score, Percentage can't go -ve;
                        subjectPctSectionMinMark = -2147483648f;
                    }
                    Float finalSubjectPctSectionMinMark = subjectPctSectionMinMark;
                    int userCount = (int) subjectSectionInfos.stream().filter(sectionResultInfo -> sectionResultInfo.getMarksAchieved() > finalSubjectPctSectionMinMark && sectionResultInfo.getMarksAchieved() <= subjectPctSectionMaxMark).count();
                    userMap.put("userPresent", userSubjectSectionInfo.getMarks() > finalSubjectPctSectionMinMark && userSubjectSectionInfo.getMarks() <= subjectPctSectionMaxMark);
                    userMap.put("pctOfUsers", (int) (((float) userCount / (float) totalNumOfStudents) * 100));
                    subjectMap.put(s, userMap);
                }
                testMarksPercentageSection.setSubjects(subjectMap);
            }
        }

        TestTableStatsRes testTableStatsRes = new TestTableStatsRes();
        testTableStatsRes.setTestMarksPercentageSections(testMarksPercentageSections);
        return testTableStatsRes;
    }

    @Deprecated
    public TestChartStatsRes getTestChartStats(Long callingUserId, String attemptId) throws NotFoundException, ForbiddenException, BadRequestException {
        logger.info("Request : " + attemptId + " callingUserId : ");
        if (StringUtils.isEmpty(attemptId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad request error. Missing params : id - " + attemptId + " callingUserId : " + callingUserId);
        }

        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(attemptId);
        if (cmdsTestAttempt == null) {
            String errorMessage = "Attempt event is not found for id : " + attemptId + " callingUserId: "
                    + callingUserId;
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (callingUserId == null || (!String.valueOf(callingUserId).equals(contentInfo.getStudentId()) && !String.valueOf(callingUserId).equals(contentInfo.getTeacherId())))) {
            logger.info("Illegal resource access : userId -" + callingUserId + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Action not allowed for the current user");
        }


        CMDSTest cmdsTest = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());
        long buffer = (long) 5 * DateTimeUtils.MILLIS_PER_MINUTE;
        long currentTime = System.currentTimeMillis();
        if (cmdsTest.getMaxStartTime() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Non competitve test");
        }

        if (cmdsTest.getDuration() == null) {
            cmdsTest.setDuration(0l);
        }

        if (cmdsTest.getMaxStartTime() + cmdsTest.getDuration() + buffer > currentTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "test end time not happened");
        }
        if (cmdsTest.getDoNotShowResultsTill() != null && System.currentTimeMillis() < cmdsTest.getDoNotShowResultsTill()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Results will be available later.");
        }

        List<StudentScoreInfo> studentScoreInfos = cmdsTestManager.getStudentScoreInfos(cmdsTest);

        studentScoreInfos.sort(new Comparator<StudentScoreInfo>() {
            @Override
            public int compare(StudentScoreInfo o1, StudentScoreInfo o2) {
                Float o1Marks = o1.getMarksAchieved();
                Float o2Marks = o2.getMarksAchieved();
                int sComp = o1Marks.compareTo(o2Marks);
                if (sComp != 0) {
                    return sComp;
                }
                return o1.getDuration().compareTo(o2.getDuration());
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });
        List<TestMarksSection> testMarksSections = new ArrayList<>();

        if (ArrayUtils.isEmpty(studentScoreInfos)) {
            TestChartStatsRes testChartStatsRes = new TestChartStatsRes();
            testChartStatsRes.setTestMarksSections(testMarksSections);
            return testChartStatsRes;
        }

        Float maxTotalMarks = null;
        for (StudentScoreInfo studentScoreInfo : studentScoreInfos) {
            if (maxTotalMarks == null) {
                maxTotalMarks = studentScoreInfo.getTotalMarks();
            } else {
                if (maxTotalMarks < studentScoreInfo.getTotalMarks()) {
                    maxTotalMarks = studentScoreInfo.getTotalMarks();
                }
            }
        }
//        logger.info("maxTotalMarks " + maxTotalMarks);
        Float minMarks = studentScoreInfos.get(0).getMarksAchieved();
//        logger.info("minMarks " + minMarks);
        Float prevMax = getMinFlooredToX(minMarks, 10);
//        logger.info("prevMax " + prevMax);
        Float max = getMaxCeiledToX(maxTotalMarks, 10);
//        logger.info("max " + max);
        Float step = (max - prevMax) / BUCKET_COUNT;
        Float firstMax = prevMax + step;
//        logger.info("step " + step);

        Float userMarks = cmdsTestAttempt.getMarksAcheived();
//        logger.info("User marks: " + userMarks);
        Integer numOfStudents = 0;

        boolean userIncluded = false;
        boolean setUserIncluded = false;

        //setting userIncluded data only if the user attempt falls between min, max time
        if (cmdsTestAttempt.getStartedAt() >= cmdsTest.getMinStartTime()
                && cmdsTestAttempt.getStartedAt() <= cmdsTest.getMaxStartTime()
                && (String.valueOf(callingUserId).equals(contentInfo.getStudentId()))) {
            setUserIncluded = true;
        }

        for (int i = 0; i < studentScoreInfos.size(); i++) {
            StudentScoreInfo studentScoreInfo = studentScoreInfos.get(i);
            Float marks = studentScoreInfo.getMarksAchieved();

            if (marks < firstMax || Objects.equals(firstMax, max)) {
                numOfStudents++;
            } else {

                TestMarksSection testMarksSection = new TestMarksSection();
                testMarksSection.setMaxMarks(firstMax);
                testMarksSection.setMinMarks(prevMax);
                testMarksSection.setNumOfUsers(numOfStudents);
                if ((((userMarks > prevMax || Objects.equals(userMarks, prevMax))
                        && userMarks < firstMax) || (Objects.equals(userMarks, max)
                        && Objects.equals(userMarks, firstMax))) && !userIncluded
                        && setUserIncluded) {
                    testMarksSection.setUserPresent(true);
                    userIncluded = true;
                }
                testMarksSections.add(testMarksSection);
                prevMax = firstMax;
                firstMax = firstMax + step;
                numOfStudents = 0;
                i--;
            }
        }

        if (Objects.equals(firstMax, max)) {
            TestMarksSection testMarksSection = new TestMarksSection();
            testMarksSection.setMaxMarks(firstMax);
            testMarksSection.setMinMarks(prevMax);
            testMarksSection.setNumOfUsers(numOfStudents);
            if (userMarks <= firstMax && !userIncluded && setUserIncluded) {
                testMarksSection.setUserPresent(true);
                userIncluded = true;
            }
            testMarksSections.add(testMarksSection);
        }

        while (testMarksSections.size() < BUCKET_COUNT) {

            TestMarksSection testMarksSection = new TestMarksSection();
            testMarksSection.setMaxMarks(firstMax);
            testMarksSection.setMinMarks(prevMax);
            testMarksSection.setNumOfUsers(numOfStudents);
            if ((((userMarks > prevMax || Objects.equals(userMarks, prevMax))
                    && userMarks < firstMax) || (Objects.equals(userMarks, max)
                    && Objects.equals(userMarks, firstMax))) && !userIncluded && setUserIncluded) {
                testMarksSection.setUserPresent(true);
                userIncluded = true;
            }
            testMarksSections.add(testMarksSection);
            prevMax = firstMax;
            firstMax = firstMax + step;
            numOfStudents = 0;
        }

        TestChartStatsRes testChartStatsRes = new TestChartStatsRes();
        testChartStatsRes.setTestMarksSections(testMarksSections);
        return testChartStatsRes;
    }

    public PerformanceReportStats getChartAnalytics(Long callingUserId, String attemptId) throws NotFoundException, ForbiddenException, BadRequestException {
        logger.info("Request : " + attemptId + " callingUserId : ");
        if (StringUtils.isEmpty(attemptId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bad request error. Missing params : id - " + attemptId + " callingUserId : " + callingUserId);
        }

        CMDSTestAttempt cmdsTestAttempt = getTestAttemptByAttemptId(attemptId);
        if (Objects.isNull(cmdsTestAttempt)) {
            String errorMessage = "Attempt event is not found for id : " + attemptId + " callingUserId: "
                    + callingUserId;
            logger.error(errorMessage);
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorMessage);
        }

        ContentInfo contentInfo = contentInfoManager.getContentById(cmdsTestAttempt.getContentInfoId(),false);
        if (UserType.REGISTERED.equals(contentInfo.getUserType()) && (Objects.isNull(callingUserId) || (!String.valueOf(callingUserId).equals(contentInfo.getStudentId()) && !String.valueOf(callingUserId).equals(contentInfo.getTeacherId())))) {
            logger.warn("Illegal resource access : userId -" + callingUserId + " contentInfo :"
                    + contentInfo.toString());
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Action not allowed for the current user");
        }

        CMDSTest cmdsTest = cmdsTestManager.getCMDSTestById(cmdsTestAttempt.getTestId());
        long buffer = (long) 5 * DateTimeUtils.MILLIS_PER_MINUTE;
        long currentTime = System.currentTimeMillis();

        if (Objects.isNull(cmdsTest.getDuration())) {
            cmdsTest.setDuration(0l);
        }
        if (cmdsTest.getDoNotShowResultsTill() != null && System.currentTimeMillis() < cmdsTest.getDoNotShowResultsTill()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Results will be available later.");
        }

        List<CategoryAnalytics> selfAnalytics = cmdsTestAttempt.getCategoryAnalyticsList();

        List<PerformanceReportSection> performanceReportSections = cmdsTest.getAnalytics();
        if (Objects.isNull(performanceReportSections) //Analytics was not calculated
                || (cmdsTest.getMaxStartTime() + cmdsTest.getDuration() + buffer > currentTime) //Test is not ended
                || (cmdsTest.getMaxStartTime() < cmdsTestAttempt.getStartTime()) //Test attempted after max start time
                || Objects.isNull(cmdsTest.getMaxStartTime()) //Test is non competitive
                ) {
            List<PerformanceReportSection> performanceReportStats
                    = Optional.ofNullable(selfAnalytics)
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .filter(Objects::nonNull)
                            .map(PerformanceReportSection::new)
                            .collect(Collectors.toList());
            PerformanceReportStats performanceReportSelf = new PerformanceReportStats();
            performanceReportSelf.setPerformanceReportSections(performanceReportStats);
            return performanceReportSelf;
        }
        performanceReportSections.forEach(performanceReportSection -> {
            String name = performanceReportSection.getTitle();
            selfAnalytics.forEach(categoryAnalytics -> {
                if (categoryAnalytics.getName().equals(name)) {
                    performanceReportSection.setSelf(categoryAnalytics.getMarks());
                    performanceReportSection.setSelfDuration(categoryAnalytics.getDuration());
                }
            });
        });

        PerformanceReportStats performanceReportStats = new PerformanceReportStats();
        performanceReportStats.setPerformanceReportSections(performanceReportSections);
        return performanceReportStats;
    }

    private static float getMinFlooredToX(float a, int x) {
        int floor = (int) Math.floor(a);
        if (floor % x == 0) {
            return (float) floor;
        } else {
            return (float) ((floor - floor % x) - x);
        }
    }

    private static float getMaxCeiledToX(float a, int x) {
        int ceil = (int) Math.ceil(a);
        if (ceil % x == 0) {
            return (float) ceil;
        } else {
            return (float) ((ceil - ceil % x) + x);
        }
    }

    private String getFormattedDate(Long epoch) {
        Date date = new Date(epoch);
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        String formattedDate = format.format(date);
        return formattedDate;
    }

    private static Boolean containsNewQuestionId(QuestionAnalytics questionAnalytics) {
        return Objects.nonNull(questionAnalytics.getNewQuestionId());
    }

    public Map<String, CMDSTestAttempt> getAttemptsForContentInfoIds(Set<String> contentInfoIds) throws BadRequestException{

        if (CollectionUtils.isEmpty(contentInfoIds)) {
            return new HashMap<>();
        } else if (contentInfoIds.size() > MAX_FETCH_SIZE) {
            throw new BadRequestException(ErrorCode.IMPERMISSIBLE_SIZE_PER_REQUEST, "Fetch size more than permissible limits");
        }

        logger.info("fetching live attempts for content ids : {}", contentInfoIds);
        final List<String> includeFields = Arrays.asList(CMDSTestAttempt.Constants._ID, CMDSTestAttempt.Constants.STARTED_AT,
                CMDSTestAttempt.Constants.ENDED_AT, CMDSTestAttempt.Constants.CONTENT_INFO_ID, CMDSTestAttempt.Constants.ATTEMPT_STATE);
        List<CMDSTestAttempt> fetchedAttempts =
                cmdsTestAttemptDAO.getTestAttemptsInProgress(contentInfoIds, includeFields);

        return Optional.of(fetchedAttempts)
                .orElseGet(ArrayList::new)
                .stream()
                .collect(Collectors.toMap(CMDSTestAttempt::getContentInfoId, Function.identity()));
    }

}
