/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers;

import com.vedantu.cmds.dao.BookDAO;
import com.vedantu.cmds.entities.Book;
import com.vedantu.cmds.enums.BookNodeType;
import com.vedantu.cmds.pojo.BookNode;
import com.vedantu.cmds.request.ChangeBookStateReq;
import com.vedantu.cmds.request.CreateEditBookReq;
import com.vedantu.cmds.request.GetBooksReq;
import com.vedantu.cmds.response.BookBasicInfo;
import com.vedantu.cmds.utils.CMDSCommonUtils;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class BookManager {

    @Autowired
    private BookDAO bookDAO;

    @Autowired
private DozerBeanMapper mapper;

    public Book create(CreateEditBookReq req) throws BadRequestException, VException {
        req.verify();
        Book book = mapper.map(req, Book.class);
        if (StringUtils.isNotEmpty(req.getBookId())) {
            book.setId(req.getBookId());
        }
        book.setBookSlug(CMDSCommonUtils.makeSlug(book.getName()));
        book.setEditionSlug(CMDSCommonUtils.makeSlug(book.getEdition()));
        setBookNodeDisplayName(book.getNodes());
        bookDAO.save(book);
        return book;
    }

    public PlatformBasicResponse changeState(ChangeBookStateReq req)
            throws BadRequestException, VException {
        req.verify();
        Book book = bookDAO.getBook(req.getBookId(), null);
        if (book == null) {
            throw new NotFoundException(ErrorCode.BOOK_NOT_FOUND,
                    "book not found " + req.getBookId());
        }
        book.setRecordState(req.getRecordState());
        bookDAO.save(book);
        return new PlatformBasicResponse();
    }

    public List<Book> getBooks(GetBooksReq req) {
        return bookDAO.getBooks(req, true);
    }

    public List<BookBasicInfo> getBooksBasicInfos(GetBooksReq req, boolean fetchAll) {
        List<Book> books = bookDAO.getBooks(req, false);
        List<BookBasicInfo> bookBasicInfos = new ArrayList<>();
        boolean booksChanged = false;
        for (Book book : books) {
            BookBasicInfo basicInfo = mapper.map(book, BookBasicInfo.class);
            int chaptercount = 0;
            if (ArrayUtils.isNotEmpty(book.getNodes())) {
                for (BookNode node : book.getNodes()) {//checking at the top level only
                    if (BookNodeType.CHAPTER.equals(node.getType())) {
                        chaptercount++;
                    }
                }
            }
            basicInfo.setChapterCount(chaptercount);
            bookBasicInfos.add(basicInfo);
            if (req.getLastFetchedTime() != null
                    && req.getLastFetchedTime() > 0l && book.getLastUpdated() > req.getLastFetchedTime()) {
                booksChanged = true;
            }
        }
        if (booksChanged || req.getLastFetchedTime() == null || req.getLastFetchedTime().equals(0l)) {
            return bookBasicInfos;
        } else {
            return new ArrayList<>();
        }
    }

    public Book getBook(String id, String bookName, Long lastFetchedTime) throws BadRequestException {
        if (StringUtils.isEmpty(id) && StringUtils.isEmpty(bookName)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bookName or bookId");
        }
        Book book = bookDAO.getBook(id, bookName);
        if (book == null) {
            return null;
        } else if (lastFetchedTime != null && lastFetchedTime > 0l && book.getLastUpdated() <= lastFetchedTime) {
            return null;
        }
        return book;
    }

    public void setBookNodeDisplayName(List<BookNode> bookNodes) {
        if (ArrayUtils.isNotEmpty(bookNodes)) {
            for (BookNode node : bookNodes) {
                if (node != null && StringUtils.isEmpty(node.getDisplayName()) && StringUtils.isNotEmpty(node.getName())) {
                    node.setDisplayName(node.getName());
                }
                setBookNodeDisplayName(node.getNodes());
            }
        }
    }
}
