/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.lms.cmds.pojo.SubjectFileCountPojo;
import com.vedantu.util.ConfigUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class CacheManager {
    
    @Autowired
    private RedisDAO redisDAO;
    
    private String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private final static Gson gson = new Gson();
    
    public CacheManager(){
        
    }
    
    public String getCachedKeyForBatchContents(String batchId){
        return env + "_BATCH_CONTENT_CACHE_" + batchId;
    }
    
    public List<SubjectFileCountPojo> getCachedNumberOfContentSharedInBatch(String batchId) throws InternalServerErrorException, BadRequestException{
        String respData = redisDAO.get(getCachedKeyForBatchContents(batchId));
        Type listType = new TypeToken<ArrayList<SubjectFileCountPojo>>(){}.getType();
        return gson.fromJson(respData, listType);
        
    }
    
    public void incrementCachedContentSharedInBatch(String batchId) throws InternalServerErrorException{
        redisDAO.increment(getCachedKeyForBatchContents(batchId), 1);
    }
    
    public void setCachedContentSharedInBatch(String batchId, List<SubjectFileCountPojo> subjectFileCountPojo) throws InternalServerErrorException{
        redisDAO.set(getCachedKeyForBatchContents(batchId), gson.toJson(subjectFileCountPojo));
    }
    
    public void deleteCachedContentSharedInBatch(String batchId) throws InternalServerErrorException, BadRequestException{
        redisDAO.del(getCachedKeyForBatchContents(batchId));
    }    
    
}
