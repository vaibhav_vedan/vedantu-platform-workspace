package com.vedantu.cmds.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.cmds.dao.CMDSQuestionDAO;
import com.vedantu.cmds.dao.CMDSTestDAO;
import com.vedantu.cmds.dao.HasSeenSolutionDAO;
import com.vedantu.cmds.dao.TopicTreeDAO;
import com.vedantu.cmds.dao.challenges.ChallengeDAO;
import com.vedantu.cmds.dao.es.CMDSQuestionESDAO;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSQuestionSet;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.enums.CMDSApprovalStatus;
import com.vedantu.cmds.enums.Indexable;
import com.vedantu.cmds.enums.MakePageLive;
import com.vedantu.cmds.fs.parser.QuestionRepositoryDocParser;
import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.cmds.pojo.CMDSQuestionBasicInfoESPojo;
import com.vedantu.cmds.pojo.CMDSTestInfo;
import com.vedantu.cmds.pojo.OptionFormat;
import com.vedantu.cmds.pojo.QuestionApprovalPojo;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.SeoQuestion;
import com.vedantu.cmds.pojo.SolutionFormat;
import com.vedantu.cmds.pojo.SolutionInfo;
import com.vedantu.cmds.pojo.TestMetadata;
import com.vedantu.cmds.pojo.challenges.ChallengeQuestion;
import com.vedantu.cmds.pojo.metadata.GridSolutionInfo;
import com.vedantu.cmds.pojo.metadata.MCQsolutionInfo;
import com.vedantu.cmds.pojo.metadata.NumericSolutionInfo;
import com.vedantu.cmds.pojo.metadata.ParaSolutionInfo;
import com.vedantu.cmds.pojo.metadata.SCQSolutionInfo;
import com.vedantu.cmds.pojo.metadata.ShortTextSolutionInfo;
import com.vedantu.cmds.pojo.metadata.TextSolutionInfo;
import com.vedantu.cmds.request.AddEditQuestionReq;
import com.vedantu.cmds.request.ChangeQuestionRequest;
import com.vedantu.cmds.request.ChangeQuestionTagsReq;
import com.vedantu.cmds.request.DeleteQuestionsReq;
import com.vedantu.cmds.request.EditAnswerReq;
import com.vedantu.cmds.request.EvaluateAnswerReq;
import com.vedantu.cmds.request.GetAdminQuestionsReq;
import com.vedantu.cmds.request.GetQuestionsReq;
import com.vedantu.cmds.request.QuestionApprovalRequest;
import com.vedantu.cmds.request.QuestionDetailsOrUnarchiveRequest;
import com.vedantu.cmds.request.QuestionDuplicationFindingRequest;
import com.vedantu.cmds.response.DeleteQuestionsRes;
import com.vedantu.cmds.response.QuestionDuplicateResponse;
import com.vedantu.cmds.response.QuestionFullInfo;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.ChangeQuestionActionType;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.lms.cmds.pojo.AnswerChangeRecord;
import com.vedantu.lms.cmds.pojo.CMDSTestQuestion;
import com.vedantu.lms.cmds.pojo.Marks;
import com.vedantu.lms.cmds.request.GetVotesReq;
import com.vedantu.lms.cmds.request.SetQuestionVoteCountReq;
import com.vedantu.lms.cmds.response.GetVotesRes;
import com.vedantu.lms.cmds.response.VoteRes;
import com.vedantu.moodle.pojo.request.ChangeAnswerRequest;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.platform.seo.dto.CMDSQuestionDto;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.vedantu.util.CommonUtils.not;

/**
 *
 */
@Service
public class CMDSQuestionManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private CMDSQuestionDAO questionDAO;

    @Autowired
    private CMDSTestManager cMDSTestManager;

    @Autowired
    private HasSeenSolutionDAO hasSeenSolutionDAO;

    @Autowired
    private QuestionRepositoryDocParser questionRepositoryDocParser;

    @Autowired
    private ChallengeDAO challengeDAO;

    @Autowired
    private CMDSTestDAO testDAO;

    @Autowired
    private CMDSQuestionSetManager questionSetManager;

    @Autowired
    private CMDSTestManager cmdsTestManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private CMDSQuestionESDAO cmdsQuestionESDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private TopicTreeDAO topicTreeDAO;

    private static final Integer ZERO_VALUE = 0;
    public static final Gson GSON = new Gson();

    public String tempQuestionDir;
    private String PLATFORM_ENDPOINT;
    private String NOTIFICATION_ENDPOINT;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSQuestionManager.class);

    public CMDSQuestionManager() {
        tempQuestionDir = "questions";
        PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
        NOTIFICATION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
    }

    public String getQuestionUploadDirectory() {

        return tempQuestionDir;
    }

    public CMDSQuestion addQuestion(AddEditQuestionReq request) throws VException {
        CMDSQuestion question;
        if (StringUtils.isNotEmpty(request.getQuestionId())) {
            question = questionDAO.getById(request.getQuestionId());
            if (question == null) {
                throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found "
                        + request.getQuestionId());
            }
            if (QuestionType.MATCHING.equals(question.getType())) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                        "Matching question editing is not supported");
            }
        } else {
            question = new CMDSQuestion();
        }
        question.setSubject(request.getSubject());
        question.setDifficulty(request.getDifficulty());
        question.setQuestionBody(request.getQuestionBody());
        question.setType(request.getQuestionType());
        question.setRefHints(request.getRefHints());

        SolutionInfo solInfo = null;
        // parse answers
        if (QuestionType.SCQ.equals(question.getType())) {
            // validations?
            SCQSolutionInfo scqsolInfo = new SCQSolutionInfo();
            scqsolInfo.answer = request.getAnswers().get(0);
            solInfo = scqsolInfo;
        }
        if (QuestionType.MCQ.equals(question.getType())) {
            MCQsolutionInfo mcqSolInfo = new MCQsolutionInfo();
            // validations?
            mcqSolInfo.answer = request.getAnswers();
            solInfo = mcqSolInfo;
        }

        if ((QuestionType.MCQ.equals(question.getType())
                || QuestionType.SCQ.equals(question.getType())) && solInfo != null) {
            OptionFormat optionFormat = new OptionFormat();
            List<String> newOptions = new ArrayList<>();
            Set<CMDSImageDetails> uuidImages = new HashSet<>();
            if (ArrayUtils.isNotEmpty(request.getOptions())) {
                for (RichTextFormat option : request.getOptions()) {
                    newOptions.add(option.getNewText());
                    uuidImages.addAll(option.getUuidImages());
                }
            }
            optionFormat.setOptionOrder(request.getOptionOrder());
            optionFormat.setNewOptions(newOptions);
            optionFormat.setUuidImages(uuidImages);
            solInfo.setOptionBody(optionFormat);
        }

//        if (QuestionType.TEXT.equals(question.type)) {
//
//            TextSolutionInfo textSolInfo = new TextSolutionInfo();
//            textSolInfo.answer = request.answers != null ? request.answers.get(ZERO_VALUE) : null;
//            solInfo = textSolInfo;
//
//        }

        if (QuestionType.SHORT_TEXT.equals(question.getType())) {
            ShortTextSolutionInfo shortTextSolutionInfo = new ShortTextSolutionInfo();
            shortTextSolutionInfo.answer = request.getAnswers() != null ? request.getAnswers().get(ZERO_VALUE) : null;
            solInfo = shortTextSolutionInfo;
        }

        if (QuestionType.PARA.equals(question.getType())) {
            solInfo = new ParaSolutionInfo();
        }

        if (QuestionType.MATCHING.equals(question.getType())) {
            GridSolutionInfo gridSOLInfo = new GridSolutionInfo();
//            gridSOLInfo.cola = request.columnA;
//            gridSOLInfo.colb = request.columnB;
//            gridSOLInfo.gridAnswer = request.gridAnswer;
            solInfo = gridSOLInfo;
        }
        if (QuestionType.NUMERIC.equals(question.getType())) {
            NumericSolutionInfo numSOLInfo = new NumericSolutionInfo();
            numSOLInfo.answer = request.getAnswers().get(0);
            solInfo = numSOLInfo;
        }

        if (ArrayUtils.isNotEmpty(request.getSolutions()) && solInfo != null) {
            List<SolutionFormat> solutions = new ArrayList<>();
            for (RichTextFormat solution : request.getSolutions()) {
                SolutionFormat solutionFormat = new SolutionFormat();
                solutionFormat.setNewText(solution.getNewText());
                solutionFormat.setUuidImages(solution.getUuidImages());
                solutionFormat.setVideos(solution.getVideos());
                solutions.add(solutionFormat);
            }
            solInfo.setSolutions(solutions);
        }

        question.setSolutionInfo(solInfo);
        question.setBoardIds(request.getBoardIds());
        question.setTargets(request.getTargets());
        question.setGrades(request.getGrades());
        question.setTags(request.getTags());
        question.setSubject(request.getSubject());
        question.setAnalysisTags(request.getAnalysisTags());
        question.setTopics(request.getTopics());
        question.setSubtopics(request.getSubtopics());
        question.addHook();

        question.setBook(request.getBook());
        question.setEdition(request.getEdition());
        question.setChapter(request.getChapter());
        question.setChapterNo(request.getChapterNo());
        question.setExercise(request.getExercise());
        question.setPageNos(request.getPageNos());
        question.setSlNoInBook(request.getSlNoInBook());
        question.setQuestionNo(request.getQuestionNo());

        // CMDSImageUtil.convertImageUrlToUuidAndSaveImage(question, true,
        // false);
        questionDAO.save(question);
        return question;
    }

    public CMDSQuestion changeQuestionTagsField(ChangeQuestionTagsReq req)
            throws BadRequestException, NotFoundException, InternalServerErrorException {

        req.verify();
        req.cleanTopicAndSubTopicAndAnalysisTopicData(); // clean overall topic tags data

        // validation on topic tags
        Set<String> topicTags=new HashSet<>();
        if(ArrayUtils.isNotEmpty(req.getTopics())){
            topicTags.addAll(req.getTopics());
        }
        if(ArrayUtils.isNotEmpty(req.getSubtopics())){
            topicTags.addAll(req.getSubtopics());
        }
        if(ArrayUtils.isNotEmpty(req.getAnalysisTags())){
            topicTags.addAll(req.getAnalysisTags());
        }
        if(ArrayUtils.isNotEmpty(topicTags)){
            validateTopicTags(topicTags);
        }

        CMDSQuestion cmdsQuestion = questionDAO.getById(req.getQuestionId());

        if (Objects.nonNull(cmdsQuestion)) {
            cmdsQuestion = questionDAO.updateChangeQuestionTags(req);
        } else {
            throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found :" + req.getQuestionId());
        }
        return cmdsQuestion;
    }

    private CMDSQuestion updateQuestion(AddEditQuestionReq request, CMDSQuestion question) throws VException {

        question.setSubject(request.getSubject());
        question.setDifficulty(request.getDifficulty());
        question.setQuestionBody(request.getQuestionBody());
        question.setType(request.getQuestionType());
        question.setRefHints(request.getRefHints());

        SolutionInfo solInfo = null;
        // parse answers
        if (QuestionType.SCQ.equals(question.getType())) {
            // validations?
            SCQSolutionInfo scqsolInfo = new SCQSolutionInfo();
            scqsolInfo.answer = request.getAnswers().get(0);
            solInfo = scqsolInfo;
        }
        if (QuestionType.MCQ.equals(question.getType())) {
            MCQsolutionInfo mcqSolInfo = new MCQsolutionInfo();
            // validations?
            mcqSolInfo.answer = request.getAnswers();
            solInfo = mcqSolInfo;
        }

        if ((QuestionType.MCQ.equals(question.getType())
                || QuestionType.SCQ.equals(question.getType())) && solInfo != null) {
            OptionFormat optionFormat = new OptionFormat();
            List<String> newOptions = new ArrayList<>();
            Set<CMDSImageDetails> uuidImages = new HashSet<>();
            if (ArrayUtils.isNotEmpty(request.getOptions())) {
                for (RichTextFormat option : request.getOptions()) {
                    newOptions.add(option.getNewText());
                    uuidImages.addAll(option.getUuidImages());
                }
            }
            optionFormat.setOptionOrder(request.getOptionOrder());
            optionFormat.setNewOptions(newOptions);
            optionFormat.setUuidImages(uuidImages);
            solInfo.setOptionBody(optionFormat);
        }

        if (QuestionType.SHORT_TEXT.equals(question.getType())) {
            ShortTextSolutionInfo shortTextSolutionInfo = new ShortTextSolutionInfo();
            shortTextSolutionInfo.answer = request.getAnswers() != null ? request.getAnswers().get(ZERO_VALUE) : null;
            solInfo = shortTextSolutionInfo;
        }

        if (QuestionType.PARA.equals(question.getType())) {
            solInfo = new ParaSolutionInfo();
        }

        if (QuestionType.MATCHING.equals(question.getType())) {
            GridSolutionInfo gridSOLInfo = new GridSolutionInfo();
            solInfo = gridSOLInfo;
        }
        if (QuestionType.NUMERIC.equals(question.getType())) {
            NumericSolutionInfo numSOLInfo = new NumericSolutionInfo();
            numSOLInfo.answer = request.getAnswers().get(0);
            solInfo = numSOLInfo;
        }

        if (ArrayUtils.isNotEmpty(request.getSolutions()) && solInfo != null) {
            List<SolutionFormat> solutions = new ArrayList<>();
            for (RichTextFormat solution : request.getSolutions()) {
                SolutionFormat solutionFormat = new SolutionFormat();
                solutionFormat.setNewText(solution.getNewText());
                solutionFormat.setUuidImages(solution.getUuidImages());
                solutionFormat.setVideos(solution.getVideos());
                solutions.add(solutionFormat);
            }
            solInfo.setSolutions(solutions);
        }

        question.setSolutionInfo(solInfo);
        question.setBoardIds(request.getBoardIds());
        question.setTargets(request.getTargets());
        question.setGrades(request.getGrades());
        question.setTags(request.getTags());
        question.setSubject(request.getSubject());
        question.setTopics(request.getTopics());
        question.setAnalysisTags(request.getAnalysisTags());
        question.setSubtopics(request.getSubtopics());
        question.addHook();

        question.setBook(request.getBook());
        question.setEdition(request.getEdition());
        question.setChapter(request.getChapter());
        question.setChapterNo(request.getChapterNo());
        question.setExercise(request.getExercise());
        question.setPageNos(request.getPageNos());
        question.setSlNoInBook(request.getSlNoInBook());
        question.setQuestionNo(request.getQuestionNo());

        return question;
    }

    public String addQuestionSeo(AddEditQuestionReq request) throws VException {
        CMDSQuestion question;
        if (StringUtils.isNotEmpty(request.getQuestionId())) {
            question = questionDAO.getById(request.getQuestionId());
            if (question == null) {
                throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found "
                        + request.getQuestionId());
            }
            if (QuestionType.MATCHING.equals(question.getType())) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                        "Matching question editing is not supported");
            }
        } else {
            question = new CMDSQuestion();
        }

        question.setQuestionBody(request.getQuestionBody());
        question.setType(request.getQuestionType());
        question.setDifficulty(request.getDifficulty());

        SolutionInfo solInfo = null;
        if (QuestionType.PARA.equals(question.getType())) {
            solInfo = new ParaSolutionInfo();
        }

        if (ArrayUtils.isNotEmpty(request.getSolutions()) && solInfo != null) {
            List<SolutionFormat> solutions = new ArrayList<>();
            for (RichTextFormat solution : request.getSolutions()) {
                SolutionFormat solutionFormat = new SolutionFormat();
                solutionFormat.setNewText(solution.getNewText());
                solutionFormat.setUuidImages(solution.getUuidImages());
                solutionFormat.setVideos(solution.getVideos());
                solutions.add(solutionFormat);
            }
            solInfo.setSolutions(solutions);
        }

        question.setSolutionInfo(solInfo);

        question.setTags(request.getTags());
        question.setSubject(request.getSubject());
        question.setTopics(request.getTopics());
        question.setSubtopics(request.getSubtopics());
        question.addHook();

        questionDAO.save(question);

        logger.info("question: " + question.toString());
        logger.info("questionID: " + question.getId());

        return question.getId();
    }

    public CMDSQuestion addQuestion(String userId, CMDSQuestion question) throws Exception {
        question.setLastUpdated(System.currentTimeMillis());
        question.setLastUpdatedBy(userId);
        questionDAO.save(question);

        return question;
    }

    public boolean addSolution(String userId, String questionId, String solution, boolean delete, String newSolution,
                               boolean edit) throws VException {

        if (edit && StringUtils.isEmpty(newSolution)) {
            logger.error("empty target solution");
            throw new VException(ErrorCode.CMDS_NEW_SOLUTION_IS_MISSING, "New Solution is missing");
        }

        CMDSQuestion question = questionDAO.getById(questionId);
        if (question == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_NOT_FOUND, "Question not found for id : " + questionId);
        }
        boolean isUpdated = false;

        if (delete || edit) {

            if (question.getSolutionInfo() != null) {
                SolutionInfo solutionInfo = question.getSolutionInfo();
                List<SolutionFormat> solutions = solutionInfo.getSolutions();
                if (CollectionUtils.isNotEmpty(solutions)) {

                    for (int i = 0; i < solutions.size(); i++) {
                        if (org.apache.commons.lang3.StringUtils.equalsIgnoreCase(solutions.get(i).getNewText(), solution)) {
                            isUpdated = true;
                            if (delete) {
                                solutions.remove(i);

                            } else if (edit) {
                                SolutionFormat sf = solutions.get(i);
                                sf.setNewText(newSolution);
                            }
                            break;
                        }
                    } // Looking solution to be deleted or updated

                } // solutions are not empty
            }

        } else {
            if (StringUtils.isEmpty(solution)) {
                logger.info("empty solution!");
                throw new VException(ErrorCode.CMDS_SOLUTION_EMPTY, "Solution is empty");
            }
            SolutionInfo solutionInfo = question.getSolutionInfo();
            List<SolutionFormat> solutions = solutionInfo.getSolutions();
            if (solutions == null) {
                solutions = new ArrayList<>();
            }
            SolutionFormat solutionFormat = new SolutionFormat();
            solutionFormat.setNewText(solution);
            solutions.add(solutionFormat);
            isUpdated = true;
        }
        if (isUpdated) {
            logger.info("empty solution!");
            questionDAO.save(question);
            return true;
        }
        return false;

    }

    public CMDSQuestion getQuestion(String qid) throws VException {
        CMDSQuestion response = questionDAO.getById(qid);
        if (response == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_NOT_FOUND, "Question not found : " + qid);
        }

        List<CMDSQuestion> questionList = new ArrayList<>();
        questionList.add(response);

        questionRepositoryDocParser.populateAWSUrls(questionList);

        return response;
    }

    public SeoQuestion getQuestionSeo(String qid) throws VException {
        CMDSQuestion question = questionDAO.getById(qid);
        if (question == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_NOT_FOUND, "Question not found : " + qid);
        }

        //NOTE: can be removed once we clean the data
        if(Indexable.YES.equals(question.getIndexable())){
            if (question.getQuestionBody() != null && !CollectionUtils.isEmpty(question.getQuestionBody().getUuidImages())) {
                for (CMDSImageDetails imageDetails : question.getQuestionBody().getUuidImages()) {
                    imageDetails.setUploadTarget(UploadTarget.QUESTIONSETS);
                }
            }

            if (question.getSolutionInfo() != null && !CollectionUtils.isEmpty((question.getSolutionInfo().getSolutions()))) {
                for (SolutionFormat solutionFormat : question.getSolutionInfo().getSolutions()) {
                    if (!org.apache.commons.lang.StringUtils.isEmpty(solutionFormat.getNewText())
                            && !CollectionUtils.isEmpty(solutionFormat.getUuidImages())) {
                        for (CMDSImageDetails imageDetails : solutionFormat.getUuidImages()) {
                            imageDetails.setUploadTarget(UploadTarget.QUESTIONSETS);
                        }
                    }
                }
            }
        }

        List<CMDSQuestion> list = new ArrayList<>();
        list.add(question);
        questionRepositoryDocParser.populateAWSUrls(list);

//        questionRepositoryDocParser.populateAWSUrls(Arrays.asList(question));
//        res.getQuestions().get(0).setQuestion(question);
        SeoQuestion seoquestion = new SeoQuestion();

        seoquestion.setId(list.get(0).getId());
        seoquestion.setQuestionBody(list.get(0).getQuestionBody());
        seoquestion.setSolutions(list.get(0).getSolutionInfo().getSolutions());
        seoquestion.setDifficulty(list.get(0).getDifficulty());

        if(CollectionUtils.isNotEmpty(question.getTopics())){
            String topic = ArrayUtils.isNotEmpty(question.getTopics()) ?
                    question.getTopics().toArray(new String[]{})[0] : null;
            seoquestion.setTopic(topic);
        }
        return seoquestion;
    }

    public List<SeoQuestion> getQuestionsSeo(List<String> qids) throws VException {
        List<CMDSQuestion> questions = questionDAO.getByIds(qids);

        questionRepositoryDocParser.populateAWSUrls(questions);

        List<SeoQuestion> seoQuestionList = new ArrayList<>();
        for(CMDSQuestion question : questions){

            SeoQuestion seoquestion = new SeoQuestion();
            seoquestion.setId(question.getId());
            seoquestion.setQuestionBody(question.getQuestionBody());
            seoquestion.setSolutions(question.getSolutionInfo().getSolutions());

            if(CollectionUtils.isNotEmpty(question.getTopics())){
                String topic = ArrayUtils.isNotEmpty(question.getTopics()) ?
                        question.getTopics().toArray(new String[]{})[0] : null;
                seoquestion.setTopic(topic);
            }

            seoquestion.setDifficulty(question.getDifficulty());

            seoQuestionList.add(seoquestion);
        }


        return seoQuestionList;
    }

    public QuestionFullInfo getQuestionFullInfo(String qid, Long userId) throws VException {
        CMDSQuestion response = questionDAO.getById(qid);
        if (response == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_NOT_FOUND, "Question not found : " + qid);
        }
        QuestionFullInfo questionFullInfo = new QuestionFullInfo(response);

        if (questionFullInfo.getSolutionInfo() != null
                && ArrayUtils.isNotEmpty(questionFullInfo.getSolutionInfo().getAnswer())) {
            questionFullInfo.setHasAns(true);
        }

        //if(!ques.hasAns||ques.questionType=='PARA'||ques.questionType=='TEXT')
        if (questionFullInfo.isHasAns()
                //                && !QuestionType.PARA.equals(questionFullInfo.getType())
                && !QuestionType.TEXT.equals(questionFullInfo.getType())) {
            questionFullInfo.setAttemptable(true);
        }

        long seenCount = hasSeenSolutionDAO.getEntitiesCount(userId, qid);
        if (seenCount > 0) {
            questionFullInfo.setHasSeenSolution(true);
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("questions.qid").is(qid));
        query.fields().include("entities");
        List<Challenge> challenges = challengeDAO.runQuery(query, Challenge.class);
        List<String> challengeIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(challenges)) {
            for (Challenge challenge : challenges) {
                List<ChallengeQuestion> entities = challenge.getQuestions();
                if (ArrayUtils.isNotEmpty(entities)) {
                    for (ChallengeQuestion entity : entities) {
                        if (entity.getQid().equals(qid)) {
                            challengeIds.add(challenge.getId());
                        }
                    }
                }
            }
        }
        questionFullInfo.setChallengeIds(challengeIds);
        SolutionInfo solutionInfo = questionFullInfo.getSolutionInfo();
        List<String> answer = null;
        List<SolutionFormat> solutions = null;
        if (solutionInfo != null) {
            answer = solutionInfo.getAnswer();
            solutions = solutionInfo.getSolutions();
            solutionInfo.setSolutions(null);
            solutionInfo.setAnswer(null);
        }

        //attempted/not
        Query query2 = new Query();
        query2.addCriteria(Criteria.where("questionId").is(qid));
        query2.addCriteria(Criteria.where("userId").is(userId));
        questionDAO.setFetchParameters(query2, 0, 10);
        List<QuestionAttempt> attempts = questionDAO.runQuery(query2, QuestionAttempt.class);
        if (ArrayUtils.isNotEmpty(attempts)) {
            questionFullInfo.setAttempted(true);
            questionFullInfo.setUserAttempts(attempts);
        }
        if (!questionFullInfo.isAttemptable() || questionFullInfo.isAttempted()) {
            questionFullInfo.getSolutionInfo().setAnswer(answer);
            questionFullInfo.getSolutionInfo().setSolutions(solutions);
        }

        //fetch voting info
        GetVotesReq req = new GetVotesReq();
        req.setContextId(qid);
        req.setSocialContextType(SocialContextType.CMDSQUESTION);
        req.setUserId(userId);
        String getVotesUrl = PLATFORM_ENDPOINT + "/social/getVotes?"
                + WebUtils.INSTANCE.createQueryStringOfObject(req);

        ClientResponse votesResponse = WebUtils.INSTANCE.doCall(getVotesUrl, HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(votesResponse);
        String jsonString = votesResponse.getEntity(String.class);
        GetVotesRes getVotesRes = new Gson().fromJson(jsonString, GetVotesRes.class);
        if (getVotesRes != null && ArrayUtils.isNotEmpty(getVotesRes.getList())) {
            VoteRes voteRes = getVotesRes.getList().get(0);
            if (voteRes.getVoteValue() == -1) {
                questionFullInfo.setDownVoted(true);
            } else if (voteRes.getVoteValue() == 1) {
                questionFullInfo.setUpVoted(true);
            }
        }

        return questionFullInfo;
    }

    public PlatformBasicResponse setQuestionVoteCount(SetQuestionVoteCountReq req) throws
            VException {
        req.verify();
        questionDAO.setCount(req.getQuestionId(), CMDSQuestion.Constants.UP_VOTES, req.getCount());
        return new PlatformBasicResponse();
    }

    public CMDSQuestion editAnswer(EditAnswerReq req) throws VException {
        req.verify();
        CMDSQuestion response = questionDAO.getById(req.getQuestionId());
        if (response == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_NOT_FOUND, "Question not found : " + req.getQuestionId());
        }
        if (QuestionType.NUMERIC.equals(response.getType()) && response.getSolutionInfo() != null) {
            NumericSolutionInfo numSOLInfo = (NumericSolutionInfo) response.getSolutionInfo();
            numSOLInfo.setAnswer(req.getAnswer());
            response.setSolutionInfo(numSOLInfo);
        } else if (QuestionType.MCQ.equals(response.getType()) && response.getSolutionInfo() != null) {
            MCQsolutionInfo numSOLInfo = (MCQsolutionInfo) response.getSolutionInfo();
            numSOLInfo.setAnswer(req.getAnswer());
            response.setSolutionInfo(numSOLInfo);
        } else if (QuestionType.SCQ.equals(response.getType()) && response.getSolutionInfo() != null) {
            SCQSolutionInfo numSOLInfo = (SCQSolutionInfo) response.getSolutionInfo();
            numSOLInfo.setAnswer(req.getAnswer());
            response.setSolutionInfo(numSOLInfo);
        } else if (QuestionType.TEXT.equals(response.getType()) && response.getSolutionInfo() != null) {
            TextSolutionInfo numSOLInfo = (TextSolutionInfo) response.getSolutionInfo();
            numSOLInfo.setAnswer(req.getAnswer());
            response.setSolutionInfo(numSOLInfo);
        }
        questionDAO.save(response);
        return response;
    }

    public DeleteQuestionsRes deleteQuestions(DeleteQuestionsReq req) throws BadRequestException {
        req.verify();
        List<CMDSQuestion> questions = questionDAO.getByIds(req.getQuestionIds(), null);
        List<String> deletedQIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(questions)) {
            for (CMDSQuestion question : questions) {
                question.setRecordState(VedantuRecordState.DELETED);
                deletedQIds.add(question.getId());
                questionDAO.save(question);
            }
        }
        req.getQuestionIds().removeAll(deletedQIds);
        DeleteQuestionsRes res = new DeleteQuestionsRes();
        res.setDeletedQuestionIds(deletedQIds);
        res.setNotDeletedQuestionIds(req.getQuestionIds());
        res.setErrorsForNotDeletedQuestionIds(Arrays.asList(ErrorCode.QUESTION_NOT_FOUND.name()));
        return res;
    }

    public List<CMDSQuestion> getQuestions(GetQuestionsReq req) throws BadRequestException {
        req.verify();
        return questionDAO.getQuestions(req);
    }

    public List<CMDSQuestion> getAdminQuestions(GetAdminQuestionsReq req) throws VException {
        req.verify();
        if (StringUtils.isNotEmpty(req.getTestId())) {
            CMDSTest test = testDAO.getOnlyCheckerApprovedData(req.getTestId());
            if(Objects.isNull(test)){
                logger.info("No test found which is approved for test id {}",req.getTestId());
                return new ArrayList<>();
            }
            CMDSTestInfo testInfo = cMDSTestManager.getTestInfo(test, false);
            List<CMDSQuestion> testQues = testInfo.getQuestions();
            return testQues;
        } else {
            List<CMDSQuestion> testQues = questionDAO.getAdminQuestions(req);
            questionRepositoryDocParser.populateAWSUrls(testQues);
            return testQues;
        }
    }

    public boolean changeAnswer(ChangeAnswerRequest req) throws NotFoundException, ForbiddenException {
        CMDSQuestion question = questionDAO.getById(req.getQuestionId());

        if (question == null) {
            throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found for id: " + req.getQuestionId());
        }

        if (ArrayUtils.isEmpty(req.getNewAnswer())) {
            logger.error("Answers empty: " + req.getQuestionId());
            return false;
        }

        if (!(QuestionType.MCQ.equals(question.getType()) || QuestionType.SCQ.equals(question.getType()) || QuestionType.NUMERIC.equals(question.getType()) || QuestionType.SHORT_TEXT.equals(question.getType()))) {
            throw new ForbiddenException(ErrorCode.INVALID_QUESTION_TYPE, "Edit answer not allowed for other than SCQ or MCQ or Numeric");
        }

        AnswerChangeRecord answerChangeRecord = new AnswerChangeRecord();

        answerChangeRecord.setChangedBy(req.getCallingUserId().toString());
        answerChangeRecord.setChangedTime(System.currentTimeMillis());
        answerChangeRecord.setNewAnswers(req.getNewAnswer());

        if (QuestionType.SCQ.equals(question.getType())) {

            if (req.getNewAnswer().size() > 1) {
                logger.error("Number of answers can't be more than 1 for SCQ : " + req.getQuestionId());
                return false;
            }

            SCQSolutionInfo scqSolutionInfo = (SCQSolutionInfo) question.getSolutionInfo();

            answerChangeRecord.setPreviousAnswers(scqSolutionInfo.getAnswer());

            scqSolutionInfo.setAnswer(req.getNewAnswer());
            SolutionInfo solutionInfo = scqSolutionInfo;

            question.setSolutionInfo(solutionInfo);

        } else if (QuestionType.MCQ.equals(question.getType())) {
            MCQsolutionInfo mcqSolutionInfo = (MCQsolutionInfo) question.getSolutionInfo();
            answerChangeRecord.setPreviousAnswers(mcqSolutionInfo.getAnswer());

            mcqSolutionInfo.setAnswer(req.getNewAnswer());
            SolutionInfo solutionInfo = mcqSolutionInfo;
            question.setSolutionInfo(solutionInfo);

        } else if (QuestionType.NUMERIC.equals(question.getType())) {

            if (req.getNewAnswer().size() > 1) {
                logger.error("Number of answers can't be more than 1 for Numeric : " + req.getQuestionId());
                return false;
            }

            NumericSolutionInfo numericSolutionInfo = (NumericSolutionInfo) question.getSolutionInfo();
            answerChangeRecord.setPreviousAnswers(numericSolutionInfo.getAnswer());

            numericSolutionInfo.setAnswer(req.getNewAnswer());
            SolutionInfo solutionInfo = numericSolutionInfo;
            question.setSolutionInfo(solutionInfo);

        } else if (QuestionType.SHORT_TEXT.equals(question.getType())) {
            question.getSolutionInfo().setAnswer(req.getNewAnswer());
        }

        if (question.getAnswerChangeRecords() == null) {
            question.setAnswerChangeRecords(new ArrayList<>());
        }

        question.getAnswerChangeRecords().add(answerChangeRecord);

        questionDAO.save(question);

        return true;

    }

    public CMDSQuestion changeQuestion(ChangeQuestionRequest req) throws VException {

        logger.info("The change question action type is {}",req.getChangeQuestionActionType());
        if (req.getChangeQuestionActionType().equals(ChangeQuestionActionType.NO_ACTION)) {
            logger.info("Just changing the marks of the question");
            return changeQuestionMarks(req);
        }


        CMDSQuestion prevQuestion = questionDAO.getById(req.getQuestionId());
        logger.info("The question to be changed is {}",prevQuestion);

        if (Objects.isNull(prevQuestion)) {
            throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found for questionId: " + req.getQuestionId());
        }

        logger.info("The type of question we want to edit is {}",prevQuestion.getType());

        if (!(QuestionType.SCQ.equals(prevQuestion.getType())
                || QuestionType.MCQ.equals(prevQuestion.getType())
                || QuestionType.NUMERIC.equals(prevQuestion.getType())
                || QuestionType.SHORT_TEXT.equals(prevQuestion.getType()))) {

            throw new ForbiddenException(ErrorCode.QUESTION_TYPE_NOT_ALLOWED, "Only SCQ, MCQ, Numeric, Short Text question type allowed");
        }

        if (!prevQuestion.getType().equals(req.getQuestionType())) {
            throw new ForbiddenException(ErrorCode.QUESTION_TYPE_CHANGE_NOT_ALLOWED, "Question type change not allowed");
        }

        logger.info("Creating an alternative new question for insertion.");
        AddEditQuestionReq addEditQuestionReq = mapper.map(req, AddEditQuestionReq.class);
        addEditQuestionReq.setQuestionId(null);
        CMDSQuestion newQuestion = addQuestion(addEditQuestionReq);
        newQuestion.setMarks(prevQuestion.getMarks());
        newQuestion.setUpVotes(prevQuestion.getUpVotes());
        newQuestion.setDownVotes(prevQuestion.getDownVotes());
        newQuestion.setAttempts(prevQuestion.getAttempts());
        newQuestion.setBookSlug(prevQuestion.getBookSlug());
        newQuestion.setChapterSlug(prevQuestion.getChapterSlug());
        newQuestion.setExerciseSlug(prevQuestion.getExerciseSlug());
        newQuestion.setEditionSlug(prevQuestion.getEditionSlug());
        newQuestion.setPageNosSlugs(prevQuestion.getPageNosSlugs());
        newQuestion.setChangedFromId(req.getQuestionId());
        newQuestion.setApprovalStatus(prevQuestion.getApprovalStatus());
        newQuestion.setOverallApprovalStatus(prevQuestion.getOverallApprovalStatus());
        newQuestion.setAssignedChecker(req.getCallingUserId());

        questionDAO.save(newQuestion);
        req.setNewQuestionId(newQuestion.getId());
        logger.info("The new question id is {}",newQuestion.getId());

        // Delete the previous question and upload the new question to elasticsearch
        deleteQuestions(new DeleteQuestionsReq(Collections.singletonList(req.getQuestionId())));
        if(Boolean.TRUE.equals(prevQuestion.getUploadedToES()) && CMDSApprovalStatus.UNIQUE.equals(prevQuestion.getApprovalStatus())){
            cmdsQuestionESDAO.deleteQuestionInESByIds(new HashSet<>(Collections.singletonList(prevQuestion.getId())));
            cmdsQuestionESDAO.uploadCMDSQuestionDataToES(newQuestion);
        }


        cMDSTestManager.processChangeQuestion(req);

        return newQuestion;

    }

    private CMDSQuestion changeQuestionMarks(ChangeQuestionRequest req) throws VException {

        CMDSQuestion prevQuestion = questionDAO.getById(req.getQuestionId());

        if (Objects.isNull(prevQuestion)) {
            throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found for questionId: " + req.getQuestionId());
        }

        logger.info("The type of question we want to edit is " + prevQuestion.getType());

        if (!(QuestionType.SCQ.equals(prevQuestion.getType())
                || QuestionType.MCQ.equals(prevQuestion.getType())
                || QuestionType.NUMERIC.equals(prevQuestion.getType())
                || QuestionType.SHORT_TEXT.equals(prevQuestion.getType()))) {

            throw new ForbiddenException(ErrorCode.QUESTION_TYPE_NOT_ALLOWED, "Only SCQ, MCQ, Numeric, Short Text question type allowed");
        }

        if (!prevQuestion.getType().equals(req.getQuestionType())) {
            throw new ForbiddenException(ErrorCode.QUESTION_TYPE_CHANGE_NOT_ALLOWED, "Question type change not allowed");
        }

        if (req.getNegativeMarks() != null && req.getNegativeMarks() < 0) {
            float negativeMarks = req.getNegativeMarks();
            negativeMarks = -1 * negativeMarks;
            req.setNegativeMarks(negativeMarks);
        }

        // Add Text and Image to the existing question and update.
        AddEditQuestionReq addEditQuestionReq = mapper.map(req, AddEditQuestionReq.class);
        updateQuestion(addEditQuestionReq, prevQuestion);
        questionDAO.save(prevQuestion);
        cMDSTestManager.processChangeQuestionForMarks(req);
        return prevQuestion;

    }

    public PlatformBasicResponse evaluateAnswer(EvaluateAnswerReq req) throws VException {
        req.verify();
        PlatformBasicResponse res = new PlatformBasicResponse(false, null, null);
        CMDSQuestion question = questionDAO.getById(req.getqId());

        if (question == null) {
            throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found for questionId: " + req.getqId());
        }

        if (!(QuestionType.SCQ.equals(question.getType())
                || QuestionType.MCQ.equals(question.getType())
                || QuestionType.NUMERIC.equals(question.getType()))
                || QuestionType.SHORT_TEXT.equals(question.getType())) {

            throw new ForbiddenException(ErrorCode.QUESTION_TYPE_NOT_ALLOWED, "Only SCQ, MCQ, Numeric question type allowed");
        }
        List<String> answer = question.getSolutionInfo().getAnswer();
        boolean isCorrect = question.getType().isCorrect(EnumBasket.Judgement.JUDGE, req.getAnswerGiven(), answer, EnumBasket.Status.COMPLETE);
        res.setSuccess(isCorrect);
        return res;
    }

    void setCMDSQuestionTargetGradeData(CMDSQuestion question) {

        if (ArrayUtils.isNotEmpty(question.getTargetGrade())) {
            for (String targetGrade : question.getTargetGrade()) {
                if (StringUtils.isNotEmpty(targetGrade)) {
                    String[] split = targetGrade.split("-");

                    Set<String> targets = ArrayUtils.isNotEmpty(question.getTargets()) ? question.getTargets() : new HashSet<>();
                    Set<String> grades = ArrayUtils.isNotEmpty(question.getGrades()) ? question.getGrades() : new HashSet<>();

                    if (split.length == 2) {
                        targets.add(split[0]);
                        grades.add(split[1]);
                    }
                    question.setTargets(targets);
                    question.setGrades(grades);
                }
            }
        }
    }

    public List<String> getQuizAnswer(EvaluateAnswerReq req) throws VException {
        req.verify();
        CMDSQuestion question = questionDAO.getById(req.getqId());

        if (question == null) {
            throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND, "Question not found for questionId: " + req.getqId());
        }

        if (!(QuestionType.SCQ.equals(question.getType())
                || QuestionType.MCQ.equals(question.getType())
                || QuestionType.NUMERIC.equals(question.getType()))) {

            throw new ForbiddenException(ErrorCode.QUESTION_TYPE_NOT_ALLOWED, "Only SCQ, MCQ, Numeric question type allowed");
        }
        List<String> answer = question.getSolutionInfo().getAnswer();
        return answer;
    }

    public List<CMDSQuestionDto> getIndexableQuestions(int page, int max) {
        List<CMDSQuestion> questions = questionDAO.getIndexableQuestions(page, max);
        List<CMDSQuestionDto> questionDtos = new ArrayList<>();
        for (CMDSQuestion question : questions) {
            questionDtos.add(getCMDSQuestionDto(question));
        }
        return questionDtos;
    }

    public CMDSQuestionDto getCMDSQuestionDto(CMDSQuestion question) {
        CMDSQuestionDto dto = new CMDSQuestionDto();
        dto.setId(question.getId());
        dto.setTargetGrade(question.getTargetGrade());
        dto.setTargets(question.getTargets());
        dto.setGrades(question.getGrades());
        dto.setSubject(question.getSubject());
        dto.setName(question.getName());
        dto.setQuestionBody(question.getQuestionBody());
        dto.setMakePageLive(MakePageLive.YES.equals(question.getMakePageLive()));
        dto.setVideo(question.getVideo());
        dto.setTopics(question.getTopics());
        dto.setDifficulty(question.getDifficulty());
        return dto;
    }

    public CMDSQuestion getQuestion(String questionId, String testId) throws VException {
        CMDSQuestion response = questionDAO.getById(questionId);
        if (response == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_NOT_FOUND, "Question not found : " + questionId);
        }

        CMDSTest test = cmdsTestManager.getCMDSTestById(testId);
        if(test == null) {
            throw new NotFoundException(ErrorCode.INVALID_TEST_ID, "Test is not found for : " + testId);
        }
        float positiveMarks = 0;
        float negativeMarks = 0;

        boolean marksUpdated = false;
        if (test.getMetadata() != null) {
            for (TestMetadata testMetadata : test.getMetadata()) {
                for (CMDSTestQuestion question : testMetadata.getQuestions()) {
                    if (question.getQuestionId().equals(questionId)) {
                        marksUpdated = true;
                        positiveMarks = question.getMarks().getPositive();
                        negativeMarks = question.getMarks().getNegative();
                        break;
                    }
                }
            }
        }

        if (marksUpdated) {
            Marks marks = new Marks();
            marks.setPositive(positiveMarks);
            marks.setNegative(negativeMarks);
            response.setMarks(marks);
        }

        List<CMDSQuestion> questionList = new ArrayList<>();
        questionList.add(response);

        questionRepositoryDocParser.populateAWSUrls(questionList);

        return response;
    }
/*

    public void insertAllQuestionFromMongoDBToElasticSearch() {
        logger.info("inside manager: insertAllQuestionFromMongoDBToElasticSearch");
        int skip = 0;
        int limit = 100;
        List<CMDSQuestion> cmdsQuestions = questionDAO.getQuestionsInChunks(skip, limit);
        while (ArrayUtils.isNotEmpty(cmdsQuestions)) {
            skip += limit;
            final List<CMDSQuestion> questionForAsync = cmdsQuestions;
            uploadListOfCMDSQuestionDataToES(questionForAsync);
            cmdsQuestions = questionDAO.getQuestionsInChunks(skip, limit);
        }
    }
*/

    public void uploadListOfCMDSQuestionDataToES(List<CMDSQuestion> cmdsQuestionData) {
        if (ArrayUtils.isEmpty(cmdsQuestionData)) {
            return;
        }
        cmdsQuestionData
                .parallelStream()
                .filter(Objects::nonNull)
                .forEach(cmdsQuestionESDAO::uploadCMDSQuestionDataToES);
    }

    public void migrateQuestionToNewTopicTree() {
        questionDAO.migrateQuestionToNewTopicTree();
    }

    public List<CMDSQuestionBasicInfoESPojo> searchCMDSQuestionDataFromES(String id) throws IOException {
        List<CMDSQuestionBasicInfoESPojo> cmdsQuestionDataFromES = cmdsQuestionESDAO.searchCMDSQuestionDataFromES(id,5f);
        return Optional.ofNullable(cmdsQuestionDataFromES).map(Collection::stream).orElseGet(Stream::empty).filter(question->!id.equals(question.get_id())).collect(Collectors.toList());
    }
/*

    public void removeDuplicates() {
        questionDAO.removeDuplicates();
    }
*/

    public PlatformBasicResponse approveQuestionStatus(QuestionApprovalRequest request)
            throws BadRequestException, InternalServerErrorException {
        request.verify();

        // cleaning the data of approval POJO as they contain dollars
        request.cleanApprovalPojoData();

        logger.info("approveQuestionStatus: request {}", request);

        // Get the overall approval status
        CMDSApprovalStatus overallApprovalStatus = request.getOverallApprovalStatus();
        switch (overallApprovalStatus) {
            case REJECTED:
                rejectValuesRecursively(request);
                break;

            case MAKER_APPROVED:
                updateMakerApprovedData(request);
                break;

            case CHECKER_APPROVED:
                updateCheckerApprovedData(request);
                break;

            default:
                logger.error("Something has bypassed initial check, please look into it for the request ", request);
                break;
        }
        return new PlatformBasicResponse();
    }   

    private void validateTopicTagsForApprovalRequest(QuestionApprovalRequest request)
            throws InternalServerErrorException, BadRequestException {

        // Verify if the topics and subtopics which we are using correct or not
        Set<String> topics =
                request.getApprovalPojo()
                        .stream()
                        .map(QuestionApprovalPojo::getTopics)
                        .filter(ArrayUtils::isNotEmpty)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());
        logger.info("All topics are {}",topics);
        Set<String> subTopics =
                request.getApprovalPojo()
                        .stream()
                        .map(QuestionApprovalPojo::getSubtopics)
                        .filter(ArrayUtils::isNotEmpty)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());
        logger.info("All subtopics are {}",subTopics);
        Set<String> analysisTags=
                request.getApprovalPojo()
                        .stream()
                        .map(QuestionApprovalPojo::getAnalysisTags)
                        .filter(ArrayUtils::isNotEmpty)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());
        logger.info("All analysis topics are {}",analysisTags);
        Set<String> topicTags=new HashSet<>();
        topicTags.addAll(topics);
        topicTags.addAll(subTopics);
        topicTags.addAll(analysisTags);
        //logger.info("After addition all the topics and subtopics are {}",topicTags);
        validateTopicTags(topicTags);
    }

    private void updateCheckerApprovedData(QuestionApprovalRequest request)
            throws BadRequestException, InternalServerErrorException {
        logger.info("Updating checker approved data");

        // Validate so that no 2 or more questions have same question id
        validateForQuestionIds(request);

        // Validate topic tags
        validateTopicTagsForApprovalRequest(request);

        // updating test or question set data
        updateQuestionSetOrTestAccordingly(request);

        // update approved question data
        updateApprovedQuestionData(request);

        // update to original question reference if any
        updateOriginalQuestionReferenceToCurrentQuestionSet(request);

        // If the data is proper then push it to elasticsearch
        pushBulkQuestionToElasticSearchAfterApproval(request);

        // Sending approval or rejection email
        sendRejectionOrApprovalMailAsync(request);

    }

    private void updateMakerApprovedData(QuestionApprovalRequest request)
            throws BadRequestException, InternalServerErrorException {
        logger.info("Updating maker approved data");

        // Validate so that no 2 or more questions have same question id
        validateForQuestionIds(request);

        // Validating topic tags
        validateTopicTagsForApprovalRequest(request);

        // updating test or question set data
        updateQuestionSetOrTestAccordingly(request);

        // update approved question data
        updateApprovedQuestionData(request);

        // send maker approved mail to checker
        sendRejectionOrApprovalMailAsync(request);
    }

    private void validateForQuestionIds(QuestionApprovalRequest request) throws BadRequestException {
        int uniqueQuestions =
                request.getApprovalPojo()
                        .stream()
                        .map(this::getToBeUsedQuestionId)
                        .collect(Collectors.toSet())
                        .size();
        if(uniqueQuestions!=request.getApprovalPojo().size()){
            throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,"It contains duplicate question ids");
        }

    }

    private String getToBeUsedQuestionId(QuestionApprovalPojo questionApprovalPojo) {
        if(CMDSApprovalStatus.DUPLICATE.equals(questionApprovalPojo.getStatus()) && StringUtils.isNotEmpty(questionApprovalPojo.getIsACopyOf())){
            return questionApprovalPojo.getIsACopyOf();
        }
        return questionApprovalPojo.getId();
    }

    private void rejectValuesRecursively(QuestionApprovalRequest request) throws BadRequestException {
        logger.info("Rejecting values recursively for the request {}.",request);

        // updating test or question set data
        updateQuestionSetOrTestAccordingly(request);

        // Update all the questions according to status ensuring not deleting those questions which are uploaded to ES
        List<String> questionIds = request.getApprovalPojo().stream().map(QuestionApprovalPojo::getId).collect(Collectors.toList());
        logger.info("Getting questions for deletion");
        List<String> questionIdForDeletion =
                Optional.ofNullable(questionDAO.getIdsIfOrNotUploadedToES(questionIds,Boolean.FALSE))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .peek(logger::info)
                        .map(CMDSQuestion::getId)
                        .collect(Collectors.toList());
        logger.info("Filtering from questions {}",questionIds);
        logger.info("Recursively rejecting values for the questions {}", questionIdForDeletion);
        if (ArrayUtils.isNotEmpty(questionIdForDeletion)) {
            questionDAO.updateApprovalStatus(questionIdForDeletion, CMDSApprovalStatus.REJECTED, request.getCallingUserId());
        }

        // Sending approval or rejection email
        sendRejectionOrApprovalMailAsync(request);
    }

    private void updateQuestionSetOrTestAccordingly(QuestionApprovalRequest request) throws BadRequestException {
        logger.info("Updating question set or test accordingly.");
        if (StringUtils.isNotEmpty(request.getTestId())) {
            logger.info("Updating for test id {} for approval status {}", request.getTestId(), request.getOverallApprovalStatus());
            cmdsTestManager.updateApprovalStatus(request);
        } else if (StringUtils.isNotEmpty(request.getQuestionSetId())) {
            logger.info("Update for question set id {} for approval status {}", request.getQuestionSetId(), request.getOverallApprovalStatus());
            questionSetManager.updateApprovalStatus(request);
        } else {
            logger.error("Something went wrong with `updateQuestionSetOrTestAccordingly`");
        }
    }

    private void updateApprovedQuestionData(QuestionApprovalRequest request) throws BadRequestException {
        logger.info("updating approved question data");
        if (Objects.isNull(request.getQuestionSetId())) {
            CMDSTest test = cmdsTestManager.getCMDSTestById(request.getTestId());
            if (Objects.isNull(test)) {
                try {
                    throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION, "Please specify valid test id");
                } catch (BadRequestException e) {
                    logger.error("updateApprovalStatus", e);
                }
            }
            request.setQuestionSetId(test.getQuestionSetId());
        }
        List<BaseTopicTree> parentNodes = topicTreeDAO.getNamesOfFirstLevelNodes();
        List<String> parentNames=parentNodes.stream().map(BaseTopicTree::getName).collect(Collectors.toList());
        request.getApprovalPojo()
                .forEach(approvalReq -> questionDAO.updateApprovedQuestion(approvalReq, request.getOverallApprovalStatus(), request.getCheckerId(), request.getCallingUserId(), request.getQuestionSetId(), parentNames));
    }

    private void pushBulkQuestionToElasticSearchAfterApproval(QuestionApprovalRequest request) {
        logger.info("Pushing bulk data to elasticsearch.");

        // Get all the question ids
        List<String> questionIds =
                request.getApprovalPojo()
                        .stream()
                        .filter(questionApprovalPojo -> CMDSApprovalStatus.UNIQUE.equals(questionApprovalPojo.getStatus()))
                        .map(QuestionApprovalPojo::getId)
                        .collect(Collectors.toList());
        logger.info("questions for pushing into elasticsearch is {}", questionIds);

        // If there's some unique questions then
        if (ArrayUtils.isNotEmpty(questionIds)) {

            // Get all the questions and upload only those not uploaded to elasticsearch
            List<CMDSQuestion> cmdsQuestions = questionDAO.getQuestionsToBeUploadedToES(questionIds);

            // Push them to elasticsearch one by one
            Optional.ofNullable(cmdsQuestions)
                    .map(Collection::parallelStream)
                    .orElseGet(Stream::empty)
                    .forEach(cmdsQuestionESDAO::uploadCMDSQuestionDataToES);
        }
    }

    public List<QuestionDuplicateResponse> getQuestionDuplicates(QuestionDuplicationFindingRequest request)
            throws IOException, BadRequestException {

        // verify if the request is proper
        request.verify();
        logger.info("getQuestionDuplicates: request - {}", request);

        // variable for question ids
        List<String> questionIds = new ArrayList<>();

        // if we have test id get the question set id for finding the questions
        if (StringUtils.isNotEmpty(request.getTestId())) {
            CMDSTest cmdsTest = cmdsTestManager.getCMDSTestById(request.getTestId());
            if (Objects.isNull(cmdsTest)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Please specify a valid test id not " + request.getTestId());
            }
            logger.info("This request contains only test id {} so getting the questions set ids for same  {}", cmdsTest.getId(), cmdsTest.getQuestionSetId());
            request.setQuestionSetId(cmdsTest.getQuestionSetId());
        }

        // after getting question set id find the questions
        if (StringUtils.isNotEmpty(request.getQuestionSetId())) {
            CMDSQuestionSet questionSet = questionSetManager.getQuestionSetById(request.getQuestionSetId());
            if (Objects.isNull(questionSet)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Please specify a valid question set id not " + request.getQuestionSetId());
            }
            logger.info("This request contains only question set id {} with questions {}", questionSet.getId(), questionSet.getQuestionIds());
            questionIds.addAll(questionSet.getQuestionIds());
        }


        // get all the questions
        Map<String, CMDSQuestion> questionMap =
                Optional.ofNullable(questionDAO.getByIds(questionIds))
                        .map(Collection::parallelStream)
                        .orElseGet(Stream::empty)
                        .collect(Collectors.toMap(
                                CMDSQuestion::getId,
                                Function.identity()
                        ));
        logger.info("Getting question ids from db {}", questionMap.keySet());

        if (questionMap.keySet().size() != questionIds.size()) {
            logger.warn("Some questions which were sent are invalid, look into it.");
        }

        // find the duplicate response from elasticsearch excluding the question id itself
        List<QuestionDuplicateResponse> duplicateResponses = new ArrayList<>();
        for (String questionId : questionIds) {
            List<CMDSQuestionBasicInfoESPojo> duplicates = cmdsQuestionESDAO.searchCMDSQuestionDataFromES(questionId);
            Set<String> duplicateQuestions =
                    duplicates
                        .stream()
                        .map(CMDSQuestionBasicInfoESPojo::get_id)
                        .filter(not(questionId::equals))
                        .collect(Collectors.toSet());
            logger.info("for question id {} duplicate question found are {}", questionId, duplicateQuestions);
            duplicateResponses.add(new QuestionDuplicateResponse(questionId, duplicateQuestions, questionMap.get(questionId)));
        }

        // return the same
        return duplicateResponses;
    }

    public List<CMDSQuestion> unarchiveQuestions(QuestionDetailsOrUnarchiveRequest request) throws BadRequestException {
        request.verify();

        // Validation for if the questions are there
        List<String> questionIds = request.getQuestionIds();
        List<CMDSQuestion> cmdsQuestions = questionDAO.getByIds(questionIds);
        if (questionIds.size() != cmdsQuestions.size()) {
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION, "Some of the question ids are invalid");
        }

        // Updating approval status of the questions from archived to unique
        logger.info("Updating the approval status for the request {}", request);
        questionDAO.updateApprovalStatus(questionIds, CMDSApprovalStatus.UNIQUE, request.getCallingUserId());

        // Getting them pushed to elasticsearch
        cmdsQuestions.parallelStream().forEach(cmdsQuestionESDAO::uploadCMDSQuestionDataToES);

        return cmdsQuestions;
    }

    public List<CMDSQuestion> getQuestionData(QuestionDetailsOrUnarchiveRequest request) throws BadRequestException {
        request.verify();

        // Validation for if the questions are there
        List<String> questionIds = request.getQuestionIds();
        List<CMDSQuestion> cmdsQuestions = questionDAO.getByIds(questionIds);
        if (questionIds.size() != cmdsQuestions.size()) {
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION, "Some of the question ids are invalid");
        }

        return cmdsQuestions;
    }

    public List<QuestionDuplicateResponse> getQuestionDuplicatesInSameDocument(QuestionDetailsOrUnarchiveRequest request) throws BadRequestException {

        request.verify();
        logger.info("getQuestionDuplicatesInSameDocument: request {}",request);

        // Validation for if the questions are there
        List<String> questionIds = request.getQuestionIds();
        List<CMDSQuestion> cmdsQuestions = questionDAO.getByIds(questionIds);
        if (questionIds.size() != cmdsQuestions.size()) {
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION, "Some of the question ids are invalid");
        }

        // Getting duplicates
        List<QuestionDuplicateResponse> duplicateResponses = cmdsQuestions.stream().map(QuestionDuplicateResponse::new).collect(Collectors.toList());



        // Iterate through current questions
        int size = duplicateResponses.size();
        for (int currentQuestionIdx = 0; currentQuestionIdx < size; currentQuestionIdx++) {
            Set<String> duplicateQuestionIds = new HashSet<>();

            // Get the current question's text with img file's data in it
            String patternText=duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getQuestionBody().getNewText();

            // Get all the images in the current file
            Set<String> patternTextImages=
                    Optional.ofNullable(duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getQuestionBody().getUuidImages())
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .map(CMDSImageDetails::getFileName)
                            .collect(Collectors.toSet());

            // Replace all the images of the current file with ""
            for(String patternImageText:patternTextImages){
                patternText=patternText.replaceAll(patternImageText,"");
                patternText=patternText.replaceAll("<br>","");
                patternText=patternText.replaceAll("</br>","");
            }

            // Get all t
            List<String> patternOptions = null;
            Set<String> patternOptionImages=null;
            if(QuestionType.MCQ.equals(duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getType())||
                    QuestionType.SCQ.equals(duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getType())){
                patternOptions = duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getSolutionInfo().getOptionBody().getNewOptions();

                patternOptionImages=
                        Optional.ofNullable(duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getSolutionInfo().getOptionBody().getUuidImages())
                                .map(Collection::stream)
                                .orElseGet(Stream::empty)
                                .map(CMDSImageDetails::getFileName)
                                .collect(Collectors.toSet());
            }
            logger.info("currentQuestionIdx {} - patternText {}",currentQuestionIdx,patternText);

            // Iterate through other questions
            for (int iterativeQuestionIdx = 0; iterativeQuestionIdx < size; iterativeQuestionIdx++) {
                // Check for questions other that current questions
                if (currentQuestionIdx!=iterativeQuestionIdx) {

                    // Get the iterated question's text with img file's data in it
                    String currentText=duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getQuestionBody().getNewText();

                    // Get all the images in the iterated file
                    Set<String> searchTextImages=
                            Optional.ofNullable(duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getQuestionBody().getUuidImages())
                                    .map(Collection::stream)
                                    .orElseGet(Stream::empty)
                                    .map(CMDSImageDetails::getFileName)
                                    .collect(Collectors.toSet());

                    // Replace all the images of the iterated file with ""
                    for(String searchImageText:searchTextImages){
                        currentText=currentText.replaceAll(searchImageText,"");
                        currentText=currentText.replaceAll("<br>","");
                        currentText=currentText.replaceAll("</br>","");
                    }

                    logger.info("iterativeQuestionIdx {} - patternText {}",iterativeQuestionIdx,currentText);
                    if (patternText.toLowerCase().equals(currentText.toLowerCase())) {
                        if ((QuestionType.MCQ.equals(duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getType()) &&
                                QuestionType.MCQ.equals(duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getType()))||
                                (QuestionType.SCQ.equals(duplicateResponses.get(currentQuestionIdx).getCmdsQuestion().getType()) &&
                                        QuestionType.SCQ.equals(duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getType()))) {

                            List<String> searchOptions = duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getSolutionInfo().getOptionBody().getNewOptions();

                            Set<String> searchOptionImages=
                                    Optional.ofNullable(duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getSolutionInfo().getOptionBody().getUuidImages())
                                            .map(Collection::stream)
                                            .orElseGet(Stream::empty)
                                            .map(CMDSImageDetails::getFileName)
                                            .collect(Collectors.toSet());

                            int questionOptionCount = 0;
                            if(ArrayUtils.isNotEmpty(patternOptions)){
                                for (String patternOption : patternOptions) {
                                    if(ArrayUtils.isNotEmpty(patternOptionImages)) {
                                        for (String patternOptionImageText : patternOptionImages) {
                                            patternOption = patternOption.replaceAll(patternOptionImageText, "");
                                        }
                                    }
                                        for (String searchOption : searchOptions) {
                                            if(ArrayUtils.isNotEmpty(searchOptionImages)) {
                                                for (String searchOptionImageText : searchOptionImages) {
                                                    searchOption = searchOption.replaceAll(searchOptionImageText, "");
                                                }
                                            }
                                            if (patternOption.toLowerCase().equals(searchOption.toLowerCase())) {
                                                questionOptionCount++;
                                            }
                                           // logger.info("questionOptionCount : {}",questionOptionCount);
                                            //logger.info("patternOption : {}",patternOption);
                                            //logger.info("searchOption : {}",searchOption);
                                        }

                                }
                            }
                            //logger.info("patternOptions : {}",patternOptions.size());
                            if (questionOptionCount >= patternOptions.size()) {
                                //logger.info("Duplicate Found");
                                duplicateQuestionIds.add(duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getId());
                            }
                        } else {
                            duplicateQuestionIds.add(duplicateResponses.get(iterativeQuestionIdx).getCmdsQuestion().getId());
                        }
                    }
                }
            }
            duplicateResponses.get(currentQuestionIdx).setDuplicates(duplicateQuestionIds);
        }

        return duplicateResponses;
    }


    public void sendRejectionOrApprovalMail(QuestionApprovalRequest request) throws AddressException, BadRequestException {

        logger.info("sendRejectionOrApprovalMail");

        CMDSApprovalStatus overallApprovalStatus = request.getOverallApprovalStatus();
        String name, id, email,
                approvalStatus = overallApprovalStatus.name(),
                approverName = fosUtils.getUserBasicInfo(request.getCallingUserId(), false).getFirstName(),
                comment = request.getRejectionComment();

        if (StringUtils.isNotEmpty(request.getTestId())) {
            CMDSTest test = testDAO.getById(request.getTestId());
            name = test.getName();
            id = test.getId();
            email = fosUtils.getUserBasicInfo(test.getCreatedBy(), true).getEmail();
        } else if (StringUtils.isNotEmpty(request.getQuestionSetId())) {
            CMDSQuestionSet questionSet = questionSetManager.getQuestionSetById(request.getQuestionSetId());
            name = questionSet.getName();
            id = questionSet.getId();
            email = fosUtils.getUserBasicInfo(questionSet.getCreatedBy(), true).getEmail();
        } else {
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER_VALUE_PASSED_IN_FUNCTION, "Invalid parameter passed in sendRejectionOrApprovalMail");
        }

        if(StringUtils.isEmpty(name)){
            name="approving questionSetId/testId";
        }

        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setSubject("Content request is " + approvalStatus);
        String rejectionComment = ", & the reason for rejection is : " + comment;
        String body = "Your request of " + name + " with ID " + id + " has been " + approvalStatus + " by " + approverName;
        String assignedCheckerMessage=" and you have been assigned as the checker for the same.\nKindly ensure that duplicates " +
                "are identified properly or else things will get messier in long run.";
        if(StringUtils.isNotEmpty(comment)){
            body = body.concat(rejectionComment);
        }

        if(CMDSApprovalStatus.MAKER_APPROVED.equals(overallApprovalStatus)){
            email=fosUtils.getUserBasicInfo(request.getCheckerId(),true).getEmail();
            body=body.concat(assignedCheckerMessage);
        }

        emailRequest.setBody(body);
        emailRequest.setTo(Arrays.asList(
                new InternetAddress(email) // ,
                // new InternetAddress("bibhuti.bhusan@vedantu.com"),
                // new InternetAddress("venkatesh.v@vedantu.com"),
                // new InternetAddress("avantika.choudhary@vedantu.com")
        ));
        emailRequest.setType(CommunicationType.MAKER_CHECKER_APPROVAL_REJECTION);
        logger.info("Email request inside sendRejectionOrApprovalMail is {}",emailRequest);

        ClientResponse emailResponse = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT + "/email/sendEmail",
                HttpMethod.POST, GSON.toJson(emailRequest));
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(emailResponse);
        } catch (VException e) {
            logger.error("sendRejectionOrApprovalMail: ", e);
        }
        String emailResponseEntity = emailResponse.getEntity(String.class);
        logger.info("Response from notification-centre for sendRejectionOrApprovalMail" + emailResponseEntity);
    }

    public void sendRejectionOrApprovalMailAsync(QuestionApprovalRequest request) {
        CompletableFuture.runAsync(() -> {
            try {
                sendRejectionOrApprovalMail(request);
            } catch (AddressException e) {
                logger.error("sendRejectionOrApprovalMailAsync - AddressException ", e);
            } catch (BadRequestException e) {
                logger.error("sendRejectionOrApprovalMailAsync - BadRequestException", e);
            }
        });
    }

    public void deleteRemainingDirtyQuestionIds() {
        List<CMDSQuestion> cmdsQuestions = questionDAO.getDirtyData();

        List<CMDSQuestion> inactiveQuestions=questionDAO.getInactiveDirtyQuestions();
        while (ArrayUtils.isNotEmpty(inactiveQuestions)) {
            List<String> questionIds = cmdsQuestions.stream().map(CMDSQuestion::getId).collect(Collectors.toList());
            cmdsQuestionESDAO.deleteQuestionInESByIds(questionIds.stream().collect(Collectors.toSet()));
            questionDAO.updateApprovalStatus(questionIds, CMDSApprovalStatus.REJECTED);
            cmdsQuestions = questionDAO.getInactiveDirtyQuestions();
        }
/*
        while (ArrayUtils.isNotEmpty(cmdsQuestions)) {
            List<String> questionIds = cmdsQuestions.stream().map(CMDSQuestion::getId).collect(Collectors.toList());
            cmdsQuestionESDAO.deleteQuestionInESByIds(questionIds.stream().collect(Collectors.toSet()));
            questionDAO.updateApprovalStatus(questionIds, CMDSApprovalStatus.ARCHIVED);
            cmdsQuestions = questionDAO.getDirtyData();
        }
*/
    }

    private void updateOriginalQuestionReferenceToCurrentQuestionSet(QuestionApprovalRequest request) throws BadRequestException {

        // Get the question set id
        String questionSetId=request.getQuestionSetId();
        if(StringUtils.isEmpty(questionSetId)){
            questionSetId=cmdsTestManager.getCMDSTestById(request.getTestId()).getQuestionSetId();
        }

        // Get the question set and the question ids from it
        CMDSQuestionSet questionSet = questionSetManager.getQuestionSetById(questionSetId);
        List<String> questionIds=questionSet.getQuestionIds();

        // Get only those ids which are uploaded to elasticsearch as they are the only ones with reference
        List<CMDSQuestion> questions=questionDAO.getIdsIfOrNotUploadedToES(questionIds,Boolean.TRUE);
        List<String> questionIdsToBeUpdated=
                Optional.ofNullable(questions)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(CMDSQuestion::getId)
                .collect(Collectors.toList());

        // Update the data in background by referring to their original links
        questionDAO.updateOriginalQuestionReferenceToCurrentQuestionSet(questionIdsToBeUpdated, questionSetId);

    }

    private void validateTopicTags(Set<String> topicTagsList) throws InternalServerErrorException, BadRequestException {
        if(ArrayUtils.isEmpty(topicTagsList)){
            return;
        }
        Set<String> redisKeys=
                topicTagsList
                        .stream()
                        .map(String::toLowerCase)
                        .map(CurriculumManager::getTopicKey)
                        .collect(Collectors.toSet());
        Map<String, String> redisValues = redisDAO.getValuesForKeys(redisKeys);

        // Keys which are not present in redis
        logger.info("Keys to remove are: ");
        List<String> keysToRemove =
                redisValues.keySet()
                        .stream()
                        .filter(key -> Objects.isNull(redisValues.get(key)))
                        .peek(logger::info)
                        .collect(Collectors.toList());

        // Validation on the same
        if((redisValues.size()-keysToRemove.size())!=redisKeys.size()){
            Set<String> retrievedKeys=redisValues.keySet();
            redisKeys.removeAll(retrievedKeys);
            keysToRemove=keysToRemove.stream().map(TopicTreeManager::extractTopicName).collect(Collectors.toList());
            throw new BadRequestException(ErrorCode.INVALID_TOPIC_TAGS,"Invalid tags(either from topics, subtopics or analysis tags) are "+keysToRemove, Level.INFO);
        }

    }

}
