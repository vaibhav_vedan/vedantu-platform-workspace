/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers.challenges;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.dao.CMDSQuestionDAO;
import com.vedantu.cmds.dao.QuestionAttemptDAO;
import com.vedantu.cmds.dao.challenges.ChallengeDAO;
import com.vedantu.cmds.dao.sql.ChallengeAttemptRDDAO;
import com.vedantu.cmds.dao.sql.UserChallengeStatsDAO;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.entities.challenges.ChallengeAttempt;
import com.vedantu.cmds.entities.challenges.ChallengeAttemptRD;
//import com.vedantu.cmds.entities.challenges.ChallengeAttempt;
import com.vedantu.cmds.pojo.challenges.ChallengeTakenHint;
import com.vedantu.cmds.entities.challenges.UserChallengeStats;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.enums.QuestionAttemptContext;
import com.vedantu.cmds.enums.challenges.ChallengeStatus;
import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.cmds.fs.parser.QuestionRepositoryDocParser;
import com.vedantu.cmds.managers.CMDSQuestionManager;
import com.vedantu.cmds.pojo.SolutionInfo;
import com.vedantu.cmds.pojo.challenges.ActiveChallengeIds;
import com.vedantu.cmds.pojo.challenges.ChallengeAttemptsCount;
import com.vedantu.cmds.pojo.challenges.ChallengeQuestion;
import com.vedantu.cmds.pojo.challenges.ChallengeStatusAggResult;
import com.vedantu.cmds.pojo.challenges.HintFormat;
import com.vedantu.cmds.request.challenges.AddChallengeReq;
import com.vedantu.cmds.request.challenges.StartChallengeReq;
import com.vedantu.cmds.request.challenges.SubmitAnswerReq;
import com.vedantu.cmds.response.challenges.GetChallengesRes;
import com.vedantu.cmds.response.challenges.StartChallengeRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.util.LogFactory;
import com.vedantu.cmds.request.challenges.GetChallengesReq;
import com.vedantu.cmds.response.challenges.ChallengeRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class ChallengesManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ChallengesManager.class);

    @Autowired
    private CMDSQuestionManager cMDSQuestionManager;

    @Autowired
    private ChallengeDAO challengeDAO;

    @Autowired
    private CMDSQuestionDAO cMDSQuestionDAO;

    @Autowired
    private QuestionAttemptDAO questionAttemptDAO;

//    @Autowired
//    private ChallengeAttemptDAO challengeAttemptDAO;
//    
    @Autowired
    private ChallengeAttemptRDDAO challengeAttemptDAO;

    @Autowired
    private UserChallengeStatsDAO userChallengeStatsDAO;

    @Autowired
    private QuestionRepositoryDocParser questionRepositoryDocParser;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
private DozerBeanMapper mapper;
    private static final Long BUFFER_FOR_SUBMIT_ANSWER = ConfigUtils.INSTANCE.getLongValue("buffer.for.challenge.submit.answer");

    public Challenge addChallenge(AddChallengeReq addChallengeReq) throws BadRequestException, VException {
        addChallengeReq.verify();

        //TODO check if the question has "status" : "COMPLETE","recordState" : "CONFIRMING"
        CMDSQuestion q = cMDSQuestionManager.getQuestion(addChallengeReq.getQuestions().get(0).getQid());
        if (q == null) {
            throw new NotFoundException(ErrorCode.QUESTION_NOT_FOUND,
                    "question in challenge is not found");
        }

        if (!QuestionType.SCQ.equals(q.getType()) && !QuestionType.MCQ.equals(q.getType()) 
                && !QuestionType.NUMERIC.equals(q.getType()) ) {
            throw new ForbiddenException(ErrorCode.QUESTION_TYPE_NOT_ALLOWED,
                    "question [" + addChallengeReq.getQuestions().get(0).getQid()
                    + "] has questionType[" + q.getType()
                    + "] not allowed in challenge");
        }
        Challenge challenge = new Challenge();
        challenge.setTitle(addChallengeReq.getTitle());
        challenge.setType(addChallengeReq.getType());
        challenge.setBurstTime(addChallengeReq.getBurstTime());
        Long startTime = addChallengeReq.getStartTime();
        if (startTime == null) {
            startTime = System.currentTimeMillis();
            challenge.setStatus(ChallengeStatus.ACTIVE);
        }
        challenge.setStartTime(startTime);
        challenge.setDuration(addChallengeReq.getDuration());
        challenge.setDifficulty(addChallengeReq.getDifficulty());
        challenge.setBoardIds(addChallengeReq.getBoardIds());
        challenge.setTargetIds(addChallengeReq.getTargetIds());
        challenge.setGrades(addChallengeReq.getGrades());
        challenge.setTags(addChallengeReq.getTags());
        challenge.setQuestions(addChallengeReq.getQuestions());
        challenge.setMaxPoints(addChallengeReq.getMaxPoints());
        challenge.addBoardIds(q.getBoardIds());
        challenge.addTargets(q.getTargets());
        challenge.addGrades(q.getGrades());
        challenge.addTags(q.getTags());
        challenge.setCategory(addChallengeReq.getCategory());

        Set<QuestionType> qtypes = new HashSet<>();
        qtypes.add(q.getType());
        challenge.setQuestionTypes(qtypes);

        challengeDAO.saveChallenge(challenge);
        return challenge;
    }

    public ChallengeRes getChallengeInfo(String challengeId, Long userId, Role callingUserRole) throws NotFoundException, ForbiddenException {
        Challenge challenge = challengeDAO.getById(challengeId);
        if (challenge == null) {
            throw new NotFoundException(ErrorCode.CHALLENGE_NOT_FOUND, "challenge not found " + challengeId);
        }
        if (!(ChallengeStatus.ENDED.equals(challenge.getStatus())
                || (challenge.getBurstTime() != null && System.currentTimeMillis() >= challenge.getBurstTime()))
                && !Role.ADMIN.equals(callingUserRole) && !Role.STUDENT_CARE.equals(callingUserRole)) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_STILL_ACTIVE, "challenge["
                    + challengeId + "] is still active/draft or role not sufficient");
        }

        ChallengeRes res = mapper.map(challenge, ChallengeRes.class);
        if (ArrayUtils.isNotEmpty(challenge.getQuestions())) {
            CMDSQuestion question = cMDSQuestionDAO.getQuestion(challenge.getQuestions().get(0).getQid(), null);
            questionRepositoryDocParser.populateAWSUrls(Arrays.asList(question));
            res.getQuestions().get(0).setQuestion(question);
        }
        if (userId != null && Role.STUDENT.equals(callingUserRole)) {
        	ChallengeAttemptRD challengeTaken = challengeAttemptDAO.getByUserId(userId, challengeId);
//            ChallengeAttempt challengeTaken = challengeAttemptDAO.getByUserId(userId, challengeId);
            if (challengeTaken != null) {
                res.setAttempted(true);
                ChallengeAttempt challengeAtt = mapper.map(challengeTaken, ChallengeAttempt.class);
                challengeAtt.setAnswer(challengeTaken.getAnswerList());
                List<HintFormat> hints = challenge.getQuestions().get(0).getHints();
                for(int i=-1;i<challengeTaken.getHintTakenIndex();i++){
                	ChallengeTakenHint challengeTakenHint = new ChallengeTakenHint(
                			challengeTaken.challengeId, userId, hints.get(i+1));
                	challengeAtt.addHint(challengeTakenHint);
                }
                res.setAttemptInfo(challengeAtt);
            }
            
        }
        Long attemptCount = challengeAttemptDAO.getChallengeAttemptCount(challengeId);
        if (attemptCount != null) {
            res.setAttempts(attemptCount.intValue());
        }
        return res;
    }

    public StartChallengeRes startChallenge(StartChallengeReq startChallengeReq) throws NotFoundException, ForbiddenException, ConflictException, BadRequestException {

        //TODO use include fields
        startChallengeReq.verify();
        String challengeId = startChallengeReq.getChallengeId();
        Long userId = startChallengeReq.getUserId();
        Challenge challenge = challengeDAO.getById(challengeId);
        if (challenge == null) {
            throw new NotFoundException(ErrorCode.CHALLENGE_NOT_FOUND, "challenge not found " + challengeId);
        }
        long cTime = System.currentTimeMillis();
        if (ChallengeStatus.ENDED.equals(challenge.getStatus())
                || (challenge.getBurstTime() != null && cTime >= challenge.getBurstTime())) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_ENDED, "challenge[" + challengeId + "] already ended");
        }

        //challenge should be either in active state or current time gt starttime
        //using the start time logic because the cron(of interval 1min)
        //can take time(max delay 1min) to update it to ACTIVE
        if (!(ChallengeStatus.ACTIVE.equals(challenge.getStatus())
                || (challenge.getStartTime() != null && System.currentTimeMillis() >= challenge.getStartTime()))) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_NOT_ACTIVE, "challenge[" + challengeId + "] already ended");
        }

        // TODO: as of now it's only single question on a challenge, in future
        // if we want to add multiple question/test in challenge than rendering
        // of entities should be a list insted of single question
        if (CollectionUtils.isEmpty(challenge.getQuestions())) {
            throw new ConflictException(ErrorCode.ENTITY_NOT_FOUND,
                    "no questions found for challenge[" + challengeId + "]");
        }
        ChallengeAttemptRD challengeAttempt = challengeAttemptDAO.getByUserId(userId, challengeId);
//        ChallengeAttempt challengeAttempt = challengeAttemptDAO.getByUserId(userId, challengeId);

        if (challengeAttempt != null && StringUtils.isNotEmpty(challengeAttempt.getAnswerString())) {
            throw new ForbiddenException(ErrorCode.ALREADY_ATTEMPTED,
                    "user[" + userId + "], has already taken  challenge["
                    + challengeId + "]");
        } else if (challengeAttempt == null) {
            long endTimeForAttemptingChallenge = challenge.getDuration() == 0 || challenge.getDuration() == null ? challenge.getBurstTime()
                    : (Math.min((cTime + challenge.getDuration()),
                            challenge.getBurstTime()));
            challengeAttempt = new ChallengeAttemptRD(challengeId, userId,
                    endTimeForAttemptingChallenge, challenge.getCategory());
            challengeAttemptDAO.save(challengeAttempt);
        }

        StartChallengeRes startChallengeRes = new StartChallengeRes();
        startChallengeRes.setToken(challengeAttempt.getId());

        CMDSQuestion question = cMDSQuestionDAO.getQuestion(challenge.getQuestions().get(0).getQid(),
                Arrays.asList(CMDSQuestion.Constants.QUESTION_BODY, CMDSQuestion.Constants.SOLUTION_INFO,
                        CMDSQuestion.Constants.TYPE, CMDSQuestion.Constants.DIFFICULTY));

        List<HintFormat> hintFormats = challenge.getQuestions().get(0).getHints();
        if (ArrayUtils.isNotEmpty(hintFormats)) {
            for (int k = hintFormats.size() - 1; k >= 0; k--) {
                if (k <= challengeAttempt.getHintTakenIndex()) {
                    break;
                }
                HintFormat hintFormat = hintFormats.get(k);
                hintFormat.setNewText(null);
                hintFormat.setUuidImages(null);
                hintFormat.setVideos(null);
            }
        }
        startChallengeRes.setHints(hintFormats);

        SolutionInfo solutionInfo = question.getSolutionInfo();
        if (solutionInfo != null) {
            solutionInfo.setSolutions(null);
            solutionInfo.setAnswer(null);
        }
        question.setRefHints(null);

        questionRepositoryDocParser.populateAWSUrls(Arrays.asList(question));
        startChallengeRes.setQuestion(question);
        startChallengeRes.setChallengeTitle(challenge.getTitle());
        startChallengeRes.setEndTime(challengeAttempt.getEndTime());
        startChallengeRes.setMaxPoints(challenge.getMaxPoints());
        startChallengeRes.setHintTakenIndex(challengeAttempt.getHintTakenIndex());
        startChallengeRes.setCurrentTime(System.currentTimeMillis());
        return startChallengeRes;
    }

    public HintFormat getHintWhileAttempting(Long userId, String token, Integer hintToBeTakenIndex) throws NotFoundException,
            ForbiddenException, ConflictException, BadRequestException, InternalServerErrorException {
        ChallengeAttemptRD challengeAttempt = challengeAttemptDAO.getEntityById(token,null,
                ChallengeAttemptRD.class);
        if (challengeAttempt == null || !userId.equals(challengeAttempt.getUserId())) {
            throw new ConflictException(ErrorCode.INVALID_TOKEN, "no challengeAttempt found for token : " + token
                    + " for user[" + userId + "]");
        }
        if (challengeAttempt.endTime < System.currentTimeMillis()) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_ENDED, "challenge[" + challengeAttempt.challengeId + "] token["
                    + token + "] expried for user[" + userId + "]");
        }

        //tODO include only fields HINTS,bidtype,
        Challenge challenge = challengeDAO.getById(challengeAttempt.getChallengeId());
        if (challenge == null
                || CollectionUtils.isEmpty(challenge.getQuestions())
                || CollectionUtils.isEmpty(challenge.getQuestions().get(0).getHints())
                || hintToBeTakenIndex == null
                || hintToBeTakenIndex >= challenge.getQuestions().get(0).getHints().size()) {
            throw new NotFoundException(ErrorCode.NO_HINT, "no hints found for challenge["
                    + challengeAttempt.challengeId + "]");
        }
        List<HintFormat> hints = challenge.getQuestions().get(0).getHints();
        logger.info("hints: " + hints);

        if (challengeAttempt.getHintTakenIndex() >= hintToBeTakenIndex) {
            return hints.get(hintToBeTakenIndex);
        }

        if ((challengeAttempt.hintTakenIndex + 1) != hintToBeTakenIndex) {
            throw new ForbiddenException(ErrorCode.INVALID_HINT_ORDER,
                    "invalid hint request, correct order: " + (challengeAttempt.hintTakenIndex + 1));
        }

        logger.info("getting hint [" + hintToBeTakenIndex
                + "], for challenge[" + challengeAttempt.challengeId + "], for user["
                + userId + "]");
        HintFormat hint = hints.get(hintToBeTakenIndex);
        questionRepositoryDocParser._populateAWSUrls(hint);
//        ChallengeTakenHint challengeTakenHint = new ChallengeTakenHint(
//                challengeAttempt.challengeId, userId, hint);
//        challengeTakenHint.setCreationTime(System.currentTimeMillis());
//        challengeTakenHint.setCreatedBy(userId.toString());
      //  challengeAttempt.addHint(challengeTakenHint);
        challengeAttempt.hintTakenIndex++;
        challengeAttemptDAO.save(challengeAttempt);
        logger.info("sending hint[" + hint + "], for challenge["
                + challengeAttempt.challengeId + "], to user[" + userId + "]");
        return hint;
    }

    public ChallengeAttempt submitAnswer(SubmitAnswerReq submitAnswerReq) throws NotFoundException,
            ForbiddenException, ConflictException, BadRequestException, InternalServerErrorException {
        submitAnswerReq.verify();
        String token = submitAnswerReq.getToken();
        List<String> answer = submitAnswerReq.getAnswer();
        ChallengeAttemptRD challengeAttempt = challengeAttemptDAO.getEntityById(token,null, ChallengeAttemptRD.class
        );
        if (challengeAttempt == null) {
            throw new ConflictException(ErrorCode.INVALID_TOKEN, "no ChallengeTaken found for token : " + token);
        }
        Long userId = challengeAttempt.getUserId();
        if ((challengeAttempt.endTime + BUFFER_FOR_SUBMIT_ANSWER) < System.currentTimeMillis()) {
            throw new ForbiddenException(ErrorCode.CHALLENGE_ENDED, "challenge[" + challengeAttempt.challengeId
                    + "] already ended, user[" + userId + "] can not bid now, endTime:"
                    + challengeAttempt.endTime + ", cTime:" + System.currentTimeMillis());
        }
        if (StringUtils.isNotEmpty(challengeAttempt.answerString)) {
            throw new ForbiddenException(ErrorCode.ALREADY_ATTEMPTED, "user[" + userId
                    + "] has already submited answer for challenge["
                    + challengeAttempt.challengeId + "]");
        }
        if(userId == null){
        	throw new ForbiddenException(ErrorCode.INVALID_USER_ID, "USER_ID NOT FOUND in challengeAttempt");
        }
        challengeAttempt.addAnswer(answer);
        challengeAttemptDAO.save(challengeAttempt);

        Challenge challenge = challengeDAO.getEntityById(challengeAttempt.getChallengeId(),
                Challenge.class);
        List<ChallengeQuestion> entities = challenge.getQuestions();
        if (ArrayUtils.isNotEmpty(entities)) {
            ChallengeQuestion entity = entities.get(0);
            Long duration = challengeAttempt.getTimeTaken();
            QuestionAttempt questionAttempt = new QuestionAttempt(userId.toString(), duration.intValue(),
                    QuestionAttemptContext.CHALLENGE, challengeAttempt.getChallengeId(),
                    challengeAttempt.getId(), entity.getQid(), answer);
            questionAttemptDAO.upsert(questionAttempt);
        }
        logger.info("saved answer for challenge[" + challengeAttempt.challengeId
                + "], for user[" + userId + "]");

        Map<String, Object> payload = new HashMap<>();
        payload.put("challengeId", challenge.getId());
        payload.put("userId", challengeAttempt.getUserId());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_CHALLENGE_ATTEMPTS, payload);
        asyncTaskFactory.executeTask(params);
        //CHANGE!@#
        ChallengeAttempt challengeAtt = mapper.map(challengeAttempt, ChallengeAttempt.class);
        challengeAtt.setAnswer(challengeAttempt.getAnswerList());
        List<HintFormat> hints = challenge.getQuestions().get(0).getHints();
        for(int i=-1;i<challengeAttempt.getHintTakenIndex();i++){
        	ChallengeTakenHint challengeTakenHint = new ChallengeTakenHint(
                    challengeAttempt.challengeId, userId, hints.get(i+1));
        	challengeAtt.addHint(challengeTakenHint);
        }
        //challengeAtt.addHint(HintFormat hint = hints.get(hintToBeTakenIndex);); 
        return challengeAtt;
    }

    public void updateChallengeAttempts(String challengeId, Long userId) {
        Challenge challenge = challengeDAO.getById(challengeId);
        List<Long> attemptedUserIds = challenge.getLastAttemptedUserIds();
        if (attemptedUserIds == null) {
            attemptedUserIds = new ArrayList<>();
        }
        attemptedUserIds.add(userId);
        if (attemptedUserIds.size() > 5) {
            attemptedUserIds.remove(0);
        }
        challenge.setLastAttemptedUserIds(attemptedUserIds);
        challengeDAO.saveChallenge(challenge);
    }

    public GetChallengesRes getChallenges(GetChallengesReq getChallengesReq) throws BadRequestException {
        getChallengesReq.verify();
        List<String> attemptedChallengeIds = new ArrayList<>();
        //challengeIdvschallengeAttempt map
        Map<String, ChallengeAttemptRD> attemptMap = new HashMap<>();
        if (Boolean.TRUE.equals(getChallengesReq.getAttemptedOnly())) {
            //TODO will fail once user attempts more than 1000 challenges
            List<ChallengeAttemptRD> challengesAttempted = challengeAttemptDAO.
                    getChallengesTakenByUserId(getChallengesReq.getUserId(), null, 0, 1000,
                            getChallengesReq.getSortOrder(),
                            getChallengesReq.getOrderBy());
            if (challengesAttempted.size() >= 500) {
                logger.error("ChallengesAttempted by " + getChallengesReq.getUserId() + ">= 500");
            }
            for (ChallengeAttemptRD challengeAttempt : challengesAttempted) {
                attemptedChallengeIds.add(challengeAttempt.getChallengeId());
                attemptMap.put(challengeAttempt.getChallengeId(), challengeAttempt);
            }
        }

        List<Challenge> _challenges = new ArrayList<>();

        if (!Boolean.TRUE.equals(getChallengesReq.getAttemptedOnly())) {
            _challenges = challengeDAO.getChallenges(getChallengesReq.getStart(),
                    getChallengesReq.getSize(), getChallengesReq.getStatus(),
                    getChallengesReq.getBrdIds(), getChallengesReq.getSortOrder(),
                    getChallengesReq.getOrderBy(), getChallengesReq.getType(),
                    getChallengesReq.getDifficulty(), null,
                    getChallengesReq.getqType(), getChallengesReq.getCategory());
        } else if (!attemptedChallengeIds.isEmpty()) {
            _challenges = challengeDAO.getChallenges(getChallengesReq.getStart(),
                    getChallengesReq.getSize(), getChallengesReq.getStatus(),
                    getChallengesReq.getBrdIds(), getChallengesReq.getSortOrder(),
                    getChallengesReq.getOrderBy(), getChallengesReq.getType(),
                    getChallengesReq.getDifficulty(), new HashSet<>(attemptedChallengeIds),
                    getChallengesReq.getqType(), getChallengesReq.getCategory());
        }

        List<Long> userIds = new ArrayList<>();
        Map<String, Challenge> challengesMap = new HashMap<>();
        for (Challenge challenge : _challenges) {
            challengesMap.put(challenge.getId(), challenge);
            if (challenge.getLeader() != null) {
                userIds.add(challenge.getLeader().getUserId());
            }
            if (ArrayUtils.isNotEmpty(challenge.getLastAttemptedUserIds())) {
                userIds.addAll(challenge.getLastAttemptedUserIds());
            }
        }

        List<Challenge> challenges;
        if (!attemptedChallengeIds.isEmpty()) {
            challenges = new ArrayList<>();
            for (String cid : attemptedChallengeIds) {
                if (challengesMap.containsKey(cid)) {
                    logger.info("atemptedId " + cid);
                    challenges.add(challengesMap.get(cid));
                }
            }
        } else {
            challenges = _challenges;
        }

        if (getChallengesReq.getUserId() != null
                && !Boolean.TRUE.equals(getChallengesReq.getAttemptedOnly())
                && !challengesMap.keySet().isEmpty()) {
            logger.info("fecthing challengetakens for the userId " + getChallengesReq.getUserId());
            List<ChallengeAttemptRD> challengesAttempted = challengeAttemptDAO
                    .getChallengeAttempts(challengesMap.keySet(), getChallengesReq.getUserId());

            for (ChallengeAttemptRD challengeAttempt : challengesAttempted) {
                attemptMap.put(challengeAttempt.getChallengeId(), challengeAttempt);
            }
        }

        logger.info("fetching the attempts count for each challenge");
        Map<String, ChallengeAttemptsCount> _map = new HashMap<>();
        if (!challengesMap.keySet().isEmpty()) {
            List<ChallengeAttemptsCount> results = challengeAttemptDAO.getAttemptsCount(challengesMap.keySet());
            if (ArrayUtils.isNotEmpty(results)) {
                for (ChallengeAttemptsCount challengeAttemptsCount : results) {
                    _map.put(challengeAttemptsCount.getChallengeId(), challengeAttemptsCount);
                }
            }
        }

        logger.info("attempts count map " + _map);
        Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        List<ChallengeRes> challengesRes = new ArrayList<>();
        for (Challenge challenge : challenges) {
            ChallengeRes res = mapper.map(challenge, ChallengeRes.class);
            if (attemptMap.containsKey(challenge.getId())) {
                ChallengeAttemptRD _attempt = attemptMap.get(challenge.getId());
                if (_attempt.isProcessed() || StringUtils.isNotEmpty(_attempt.answerString)
                        || (_attempt.getEndTime() != 0 && System.currentTimeMillis() > _attempt.getEndTime())) {
                    res.setAttempted(true);
                }
            }
            if (_map.containsKey(challenge.getId())) {
                res.setAttempts(_map.get(challenge.getId()).getAttempts());
            }

            if (res.getLeader() != null && usersMap.get(res.getLeader().getUserId()) != null) {
                res.getLeader().setUser(usersMap.get(res.getLeader().getUserId()));
            }
            if (ArrayUtils.isNotEmpty(challenge.getLastAttemptedUserIds())) {
                List<UserBasicInfo> attemptedUsers = new ArrayList<>();
                for (Long _uid : challenge.getLastAttemptedUserIds()) {
                    attemptedUsers.add(usersMap.get(_uid));
                }
                res.setLastAttemptedUsers(attemptedUsers);
            }
            challengesRes.add(res);
        }

        //counts
        int active = 0, ended = 0, attempted = 0, pointsForGrabs = 0;
        logger.info("fetching count of active and ended");
        List<ChallengeStatusAggResult> countresult = challengeDAO.getCounts();
        Map<ChallengeStatus, ChallengeStatusAggResult> statusMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(countresult)) {
            for (ChallengeStatusAggResult challengeStatusAggResult : countresult) {
                statusMap.put(challengeStatusAggResult.getStatus(), challengeStatusAggResult);
            }
        }
        if (statusMap.containsKey(ChallengeStatus.ACTIVE)) {
            active = statusMap.get(ChallengeStatus.ACTIVE).getCount();
            pointsForGrabs = statusMap.get(ChallengeStatus.ACTIVE).getPoints();
        }
        if (statusMap.containsKey(ChallengeStatus.ENDED)) {
            ended = statusMap.get(ChallengeStatus.ENDED).getCount();
        }
        logger.info("active " + active + ", ended " + ended + ", points " + pointsForGrabs);

        logger.info("fetching attempts count");
        if (getChallengesReq.getUserId() != null) {
            Long _attempted = challengeAttemptDAO.getChallengesTakenByUserIdCount(getChallengesReq.getUserId());
            if (_attempted != null) {
                attempted = _attempted.intValue();
            }
        }
        logger.info("attempts " + attempted);

        GetChallengesRes response = new GetChallengesRes();
        response.setList(challengesRes);
        response.setActive(active);
        response.setEnded(ended);
        response.setPointsForGrabs(pointsForGrabs);
        response.setAttempted(attempted);
        response.setAttempted(attemptedChallengeIds.size());
        return response;
    }

    public ChallengeAttempt getChallengeUserAttemptInfo(Long userId, String challengeId) throws NotFoundException {
        ChallengeAttemptRD challengeTaken = challengeAttemptDAO.getByUserId(userId, challengeId);
        if (challengeTaken == null) {
            throw new NotFoundException(ErrorCode.NOT_ATTEMPTED, "user[" + userId + "] has not attempted challenge["
                    + challengeId + "]");
        }
        logger.info("returning ChallengeTaken info for user[" + userId + "], "
                + challengeTaken);
        Challenge challenge = challengeDAO.getById(challengeTaken.getChallengeId());
      //CHANGE!@#
        ChallengeAttempt challengeAtt = mapper.map(challengeTaken, ChallengeAttempt.class);
        challengeAtt.setAnswer(challengeTaken.getAnswerList());
        List<HintFormat> hints = challenge.getQuestions().get(0).getHints();
        for(int i=-1;i<challengeTaken.getHintTakenIndex();i++){
        	ChallengeTakenHint challengeTakenHint = new ChallengeTakenHint(
        			challengeTaken.challengeId, userId, hints.get(i+1));
        	challengeAtt.addHint(challengeTakenHint);
        }
        return challengeAtt;
    }

    public UserChallengeStats getUserChallengeStats(Long userId) throws NotFoundException {
        UserChallengeStats userChallengeStats = userChallengeStatsDAO.getByUserId(userId);
        if (userChallengeStats == null) {
            userChallengeStats = new UserChallengeStats();
            userChallengeStats.setUserId(userId);
            userChallengeStats.setHintsCountMap(new ArrayList<>());
            userChallengeStats.setMultiplierPowerType(MultiplierPowerType.SINGLE);
            userChallengeStatsDAO.create(userChallengeStats, "SYSTEM");
        }
        return userChallengeStats;
    }

    //    public ChallengeAttempt bid(BidReq bidReq) throws BadRequestException, ConflictException, NotFoundException, ForbiddenException {
//        bidReq.verify();
//        int bid = bidReq.getBid();
//        Long userId = bidReq.getUserId();
//        String token = bidReq.getToken();
//        ChallengeAttempt challengeAttempt = challengeAttemptDAO.getEntityById(token, ChallengeAttempt.class);
//        if (challengeAttempt == null || !userId.equals(challengeAttempt.getUserId())) {
//            throw new ConflictException(ErrorCode.INVALID_TOKEN, "no ChallengeAttempt found for token : " + token
//                    + " for user[" + userId + "]");
//        }
//        //TODO use include fields
//        Challenge challenge = challengeDAO.getById(challengeAttempt.getChallengeId());
//        if (challenge == null) {
//            throw new NotFoundException(ErrorCode.CHALLENGE_NOT_FOUND, "no challenge found with challengeId["
//                    + challengeAttempt.challengeId + "] for user[" + userId + "]");
//        }
//        if (challenge.bidType == BidType.NON_BIDDABLE) {
//            throw new ForbiddenException(ErrorCode.NON_BIDDABLE, "challenge [" + challengeAttempt.challengeId + "] is ["
//                    + challenge.bidType + "], user[" + userId + "]");
//        }
//        if (challenge.maxBid < bid) {
//            throw new ForbiddenException(ErrorCode.BID_AMOUNT_NOT_ALLOWED, "challenge [" + challengeAttempt.challengeId
//                    + "] has maxBid [" + challenge.maxBid + "], and user[" + userId
//                    + "] is putting bid[" + bid + "] which is not allowed");
//        }
//        UserCredits userCredits = userCreditDetailDAO.getEntityById(userId, UserCredits.class);
//        //TODO userCredits ==null
//        if (userCredits.totalCredits < bid) {
//            throw new ForbiddenException(ErrorCode.INSUFFICIENT_CREDITS, "user[" + userId
//                    + "] does not have sufficient credit balance [balance:"
//                    + userCredits.totalCredits + ", bid:" + bid + "]");
//        }
//
//        if (challengeAttempt.bidded || challengeAttempt.hintTakenIndex > 0) {
//            throw new ForbiddenException(ErrorCode.ALREADY_BIDDED,
//                    "user[" + userId + "] already bidded for challenge["
//                    + challengeAttempt.challengeId + "] with token : " + token);
//        }
//        if (challengeAttempt.endTime < System.currentTimeMillis()) {
//            throw new ForbiddenException(ErrorCode.BID_ENDED,
//                    "challenge[" + challengeAttempt.challengeId
//                    + "] already ended, user[" + userId + "] can not bid now");
//        }
//        creditUtil.saveUserCreditsInfo(userCredits, new CreditInfo(CreditCategory.CHALLENGE, bid),
//                CreditType.NEGATIVE, new SrcEntity(CMDSEntityType.CHALLENGE,
//                        challengeAttempt.challengeId));
//        logger.info("placing bid for user[" + userId + "], on challenge["
//                + challengeAttempt.challengeId + "]");
//        challengeAttempt.bid = bid;
//        challengeAttempt.bidded = true;
//        challengeAttemptDAO.save(challengeAttempt);
//
//        Query query = new Query();
//        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants._ID).is(challenge.getId()));
//        Update update = new Update();
//        update.inc(ConstantsGlobal.BID_POOL, bid);
//        challengeDAO.updateFirst(query, update, Challenge.class);
//
//        logger.info("placed bid for user[" + userId + "], on challenge["
//                + challengeAttempt.challengeId + "]");
//        return challengeAttempt;
//    } 
    
    public PlatformBasicResponse deleteChallenge(String challengeId)
            throws BadRequestException, VException {
        if(StringUtils.isEmpty(challengeId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "challengeId");
        }
        challengeDAO.deleteChallenge(challengeId);
        return new PlatformBasicResponse();
    }    
    
    public GetChallengesRes getActiveChallengesForAllCategories() throws NotFoundException {
        
    	List<ChallengeRes> challengesRes = new ArrayList<>();
    	Set<Long> userIds = new HashSet<>();
    	List<ActiveChallengeIds> activechallenges = challengeDAO.getActiveSingleChallenges();
    	Map<String,Challenge> challengeMap = new HashMap<>();
    	if(ArrayUtils.isNotEmpty(activechallenges)){
    		for(ActiveChallengeIds active : activechallenges){
    			//logger.info("Active Challenge id : " + active.getChallengeId() + " category "+active.getCategory());
    			
    			if(active.getChallenge() != null){
    				Challenge challenge = active.getChallenge();
    				challengeMap.put(challenge.getId(), challenge);
    				if(ArrayUtils.isNotEmpty(challenge.getLastAttemptedUserIds())){
    					userIds.addAll(challenge.getLastAttemptedUserIds());
    				}    				
    			}
    		}
    	}

        Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        
        Map<String, ChallengeAttemptsCount> _map = new HashMap<>();
        if (!challengeMap.keySet().isEmpty()) {
            List<ChallengeAttemptsCount> results = challengeAttemptDAO.getAttemptsCount(challengeMap.keySet());
            if (ArrayUtils.isNotEmpty(results)) {
                for (ChallengeAttemptsCount challengeAttemptsCount : results) {
                    _map.put(challengeAttemptsCount.getChallengeId(), challengeAttemptsCount);
                }
            }
        }

        logger.info("attempts count map " + _map);
        if (!challengeMap.keySet().isEmpty()){
	        for (String id : challengeMap.keySet()) {
	        	Challenge challenge = challengeMap.get(id);
	            ChallengeRes res = mapper.map(challenge, ChallengeRes.class);
	            if (_map.containsKey(challenge.getId())) {
	                res.setAttempts(_map.get(challenge.getId()).getAttempts());
	            }
	            if (ArrayUtils.isNotEmpty(challenge.getLastAttemptedUserIds())) {
	                List<UserBasicInfo> attemptedUsers = new ArrayList<>();
	                for (Long _uid : challenge.getLastAttemptedUserIds()) {
	                    attemptedUsers.add(usersMap.get(_uid));
	                }
	                res.setLastAttemptedUsers(attemptedUsers);
	            }
	            challengesRes.add(res);
	        }
        }
        GetChallengesRes response = new GetChallengesRes();
        response.setList(challengesRes);
        return response;
    }
}
