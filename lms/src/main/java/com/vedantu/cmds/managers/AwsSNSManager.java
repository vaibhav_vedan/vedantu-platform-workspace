/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers;

import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author jeet
 */
@Service
public class AwsSNSManager extends AbstractAwsSNSManager {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);

    public AwsSNSManager() {
        super();
        logger.info("Initializing AwsSNSManager");
    }

    @Override
    public void createSubscriptions() {
        logger.info("Ready to create various subscriptions");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "challenges/endChallengeCron");
        createCronSubscription(CronTopic.CRON_CHIME_30_Minutes, "cmds/test/calculateTestRanks");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "challenges/markChallengeActiveCron");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "challenges/endChallengeCron");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "cmds/test/endTests");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "cmds/test/preCalculateTestStats");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY_2PM_IST, "challenges/sendDailyChallengeAttemptMail");
        createCronSubscription(SNSTopic.BATCH_CURRICULUM_UPDATE, "curriculum/removeBatchCurriculumProgressCache");
    }

    @Override
    public void createTopics() {
        logger.info("Ready to create various topics");
        createSNSTopic(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE);
        createSNSTopic(SNSTopic.BATCH_CURRICULUM_UPDATE);
    }

}
