package com.vedantu.cmds.managers.challenges;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserMinimalInfo;
import com.vedantu.cmds.dao.challenges.ChallengeDAO;
import com.vedantu.cmds.dao.sql.ChallengeAttemptRDDAO;
import com.vedantu.cmds.dao.sql.SqlSessionFactory;
import com.vedantu.cmds.dao.sql.UserChallengeLeaderBoardDAO;
import com.vedantu.cmds.dao.sql.UserChallengeLeaderBoardTransactionDAO;
import com.vedantu.cmds.entities.challenges.Challenge;
import com.vedantu.cmds.entities.challenges.ChallengeAttemptRD;
import com.vedantu.cmds.entities.challenges.UserChallengeLeaderBoard;
import com.vedantu.cmds.entities.challenges.UserChallengeLeaderBoardTransaction;
import com.vedantu.cmds.enums.challenges.MultiplierPowerType;
import com.vedantu.cmds.enums.challenges.UserChallengeStatsContextType;
import com.vedantu.cmds.response.challenges.UserLeaderBoardInfo;
import com.vedantu.cmds.response.challenges.UserLeaderBoardInfoRes;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.cmds.utils.ChallengeUtilCommon;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.lms.cmds.enums.LeaderBoardType;
import com.vedantu.lms.cmds.request.GetLeaderBoardForChallengeReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.lang.reflect.Type;
import java.util.Arrays;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.resource.transaction.spi.TransactionStatus;

@Service
public class UserLeaderBoardManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ChallengesManager.class);

	@Autowired
	private ChallengeDAO challengeDAO;

	@Autowired
	private ChallengeAttemptRDDAO challengeAttemptDAO;

	@Autowired
	private UserChallengeLeaderBoardDAO userChallengeLeaderBoardDAO;

	@Autowired
	private SqlSessionFactory sqlSessionfactory;

	@Autowired
	private UserChallengeLeaderBoardTransactionDAO userChallengeLeaderBoardTransactionDAO;

	@Autowired
	private PostChallengeProcessor postChallengeProcessor;

	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
private DozerBeanMapper mapper;
	private final Gson gson = new Gson();

	private static final Integer WEEK_START_TIME_HOUR = ConfigUtils.INSTANCE
			.getIntValue("challenge.leaderboard.week.startHour");
	private static final Integer WEEK_START_TIME_MINUTE = ConfigUtils.INSTANCE
			.getIntValue("challenge.leaderboard.week.startMinute");
	private static final Integer WEEK_START_TIME_DAY = ConfigUtils.INSTANCE
			.getIntValue("challenge.leaderboard.week.startDay");
	private static final Integer MONTH_START_TIME_HOUR = ConfigUtils.INSTANCE
			.getIntValue("challenge.leaderboard.month.startHour");
	private static final Integer MONTH_START_TIME_MINUTE = ConfigUtils.INSTANCE
			.getIntValue("challenge.leaderboard.month.startMinute");
	private static final Integer MONTH_START_TIME_DAY = ConfigUtils.INSTANCE
			.getIntValue("challenge.leaderboard.month.startDay");
	private static final Long BIWEEK_START_TIME = ConfigUtils.INSTANCE
			.getLongValue("challenge.leaderboard.biweek.startTime");
	private static final Integer BIWEEK_PERIOD = ConfigUtils.INSTANCE
			.getIntValue("challenge.leaderboard.biweek.period");

	private static final Long BIWEEK_MILLIS = (long) BIWEEK_PERIOD*DateTimeUtils.MILLIS_PER_DAY;
	
	public Long getWeekStartTime(Long currentTime) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		calendar.setTimeInMillis(currentTime);
		calendar.set(Calendar.DAY_OF_WEEK, WEEK_START_TIME_DAY);
		calendar.set(Calendar.HOUR_OF_DAY, WEEK_START_TIME_HOUR);
		calendar.set(Calendar.MINUTE, WEEK_START_TIME_MINUTE); // set minutes to
																// zero
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		if (calendar.getTimeInMillis() > currentTime) {
			if (calendar.get(Calendar.WEEK_OF_YEAR) > 1) {
				calendar.set(Calendar.WEEK_OF_YEAR, (calendar.get(Calendar.WEEK_OF_YEAR) - 1));
			} else {
				calendar.set(Calendar.WEEK_OF_YEAR, 52);
				calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
			}
		}
		return calendar.getTimeInMillis();
	}
	
	public Long getBiWeekStartTime(Long currentTime) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		calendar.setTimeInMillis(currentTime);
		currentTime = calendar.getTimeInMillis();
		Long timeSinceStart = (currentTime - BIWEEK_START_TIME) % (BIWEEK_MILLIS);
		return (currentTime - timeSinceStart);
	}

	public Long getMonthStartTime(Long currentTime) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		calendar.setTimeInMillis(currentTime);
		calendar.set(Calendar.DAY_OF_MONTH, MONTH_START_TIME_DAY);
		calendar.set(Calendar.HOUR_OF_DAY, MONTH_START_TIME_HOUR);
		calendar.set(Calendar.MINUTE, MONTH_START_TIME_MINUTE); // set minutes
																// to zero
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		if (calendar.getTimeInMillis() > currentTime) {
			if (calendar.get(Calendar.MONTH) > 0) {
				calendar.set(Calendar.MONTH, (calendar.get(Calendar.MONTH) - 1));
			} else {
				calendar.set(Calendar.MONTH, 11);
				calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR) - 1);
			}
		}
		return calendar.getTimeInMillis();
	}

	public Long getNextMonthStartTime(Long currentTime) {

		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("Asia/Calcutta"));
		calendar.setTimeInMillis(currentTime);
		calendar.setTimeInMillis(currentTime);
		int month = calendar.get(Calendar.MONTH);
		if (month == 11) {
			month = 0;
			calendar.set(Calendar.YEAR, (calendar.get(Calendar.YEAR)) + 1);
		} else {
			month = month + 1;
		}
		calendar.set(Calendar.MONTH, month);
		calendar.set(Calendar.DAY_OF_MONTH, MONTH_START_TIME_DAY);
		calendar.set(Calendar.HOUR_OF_DAY, MONTH_START_TIME_HOUR);
		calendar.set(Calendar.MINUTE, MONTH_START_TIME_MINUTE); // set minutes
																// to zero
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTimeInMillis();
	}

	public UserLeaderBoardInfoRes getChallengeLeaderBoardForDuration(GetLeaderBoardForChallengeReq req)
			throws BadRequestException {
		req.verify();
		if (req.getStart() == null) {
			req.setStart(0);
		}
		if (req.getSize() == null) {
			req.setSize(10);
		}
		if (req.getStartTime() == null) {
			req.setStartTime(System.currentTimeMillis());
		}
		Long startTime = fetchStartTime(req.getType(), req.getStartTime());
		

		switch (req.getType()) {
		case PREVIOUS_WEEK:
		case CURRENT_WEEK:
			req.setType(LeaderBoardType.WEEK);
			break;
		case GLOBAL:
			req.setType(LeaderBoardType.OVERALL);
			break;
		case PREVIOUS_BI_WEEK:
			req.setType(LeaderBoardType.BI_WEEK);
			break;
		}
		logger.info("startTime: " + startTime);
		
		List<UserLeaderBoardInfo> leaders = null;
		if(req.getStart() == 0){
			leaders = getLeaderBoardsCache(req.getCategory(), req.getType(), startTime);
		}		 
		if (ArrayUtils.isEmpty(leaders) || leaders.size() != req.getSize()) {
			List<UserChallengeLeaderBoard> leaderBoards = userChallengeLeaderBoardDAO.getUserLeaderBoardForCategory(req.getCategory(), req.getType(),
					startTime, req.getStart(), req.getSize());
			leaders = new ArrayList<>();
			if (ArrayUtils.isNotEmpty(leaderBoards)) {
				int count = 1;
				for (UserChallengeLeaderBoard leaderBoard : leaderBoards) {
					logger.info("leaderBoard - " + leaderBoard.getUserId() + "  " + count);
					UserLeaderBoardInfo leader = new UserLeaderBoardInfo();
					leader.setTotalPoints(leaderBoard.getPoints());
					leader.setTimeRate(leaderBoard.getTimeRate());
					leader.setTimeTaken(leaderBoard.getTimeTaken());
					leader.setRank(count + req.getStart());
					leader.setUserId(leaderBoard.getUserId());
					count++;
					leaders.add(leader);
				}
			}
			
			if (ArrayUtils.isNotEmpty(leaders) && leaders.size() <= 10 && req.getStart() == 0) {
				try {
					String key = getLeaderBoardCacheKey(req.getCategory(), req.getType(), startTime);
					logger.info("Key cache is :" + key);
					String value = gson.toJson(leaders);
					logger.info("Value of cache : "+value);
					if(StringUtils.isNotEmpty(key) && StringUtils.isNotEmpty(value)){
						redisDAO.setex(key, value, DateTimeUtils.SECONDS_PER_DAY);
					}					
				} catch (Exception e) {
					logger.info("Error in Marking setex for leaderboard. category : " + req.getCategory() + " type: "
							+ req.getType() + e);
				}
			}
		}

		List<Long> userIds = new ArrayList<>();
		boolean partOfLeaderBoard = false;
		
		if(ArrayUtils.isNotEmpty(leaders)){
			for(UserLeaderBoardInfo leader : leaders){
				userIds.add(leader.getUserId());
				if (req.getUserId() != null && leader.getUserId().equals(req.getUserId())) {
					partOfLeaderBoard = true;
				}
			}
		}
		

		UserChallengeLeaderBoard userBoard = null;

		if (req.getAddMyRank() && !partOfLeaderBoard) {
			if (req.getUserId() != null) {
				UserLeaderBoardInfo leader = getUserRank(req.getUserId(), req.getCategory(), req.getType(), startTime);
				if (leader != null) {
					if (!leaders.isEmpty()) {
						leaders.remove(leaders.size() - 1);
					}
					leaders.add(leader);
					userIds.add(leader.getUserId());
				}
			}
		}
		Long endTime = startTime;
		switch(req.getType()){
		case WEEK:
			endTime += 7l*DateTimeUtils.MILLIS_PER_DAY;
			break;
		case MONTH:
			endTime += 30l*DateTimeUtils.MILLIS_PER_DAY;
			break;
		case BI_WEEK:
			endTime += 14l*DateTimeUtils.MILLIS_PER_DAY;
			break;
		case OVERALL:
			startTime = null;
			endTime = null;
			break;
		}
		Map<Long, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
		for (UserLeaderBoardInfo leader : leaders) {
			leader.setStartTime(startTime);
			leader.setEndTime(endTime);
			leader.setUser(mapper.map(usersMap.get(leader.getUserId()), UserMinimalInfo.class));
		}
		UserLeaderBoardInfoRes res = new UserLeaderBoardInfoRes();
		res.setList(leaders);
		return res;
	}

	public UserLeaderBoardInfo getUserRank(Long userId, String category, LeaderBoardType type, Long startTime)
			throws BadRequestException {
		UserChallengeLeaderBoard userBoard = null;
		UserLeaderBoardInfo leader = null;
		if (startTime == null) {
			startTime = fetchStartTime(type, System.currentTimeMillis());
			logger.info("startTime: " + startTime);

		}
		if (userId != null && StringUtils.isNotEmpty(category) && type != null) {
			userBoard = userChallengeLeaderBoardDAO.getByUserId(userId, type, startTime, category);

			if (userBoard != null) {
				leader = new UserLeaderBoardInfo();
				leader.setTotalPoints(userBoard.getPoints());
				leader.setTimeRate(userBoard.getTimeRate());
				leader.setTimeTaken(userBoard.getTimeTaken());
				leader.setUserId(userBoard.getUserId());
				Long myRank = userChallengeLeaderBoardDAO.getUserRank(category, type, startTime,
						leader.getTotalPoints(), leader.getTimeRate());
				if (myRank != null) {
					leader.setRank(myRank.intValue());
					leader.setUserId(userBoard.getUserId());
				}
			}
		}
		return leader;
	}

	public Long fetchStartTime(LeaderBoardType type, Long startTime) throws BadRequestException {
		switch (type) {
		case WEEK:
			return getWeekStartTime(startTime);
		case MONTH:
			return getMonthStartTime(startTime);
		case GLOBAL:
		case OVERALL:
			return getWeekStartTime(startTime);
		case CURRENT_WEEK:
			return getWeekStartTime(startTime);
		case PREVIOUS_WEEK:
			logger.info("startTime PREVIOUS_WEEK: " + startTime);
			Long startNewTime = startTime - DateTimeUtils.MILLIS_PER_WEEK;
			logger.info("startNewTime PREVIOUS_WEEK: " + startNewTime);
			return getWeekStartTime(startNewTime);
		case BI_WEEK:
			return getBiWeekStartTime(startTime);
                case PREVIOUS_BI_WEEK:
                    Long startTimeNew = startTime - 2 * DateTimeUtils.MILLIS_PER_WEEK;
                    return getBiWeekStartTime(startTimeNew);
		default:
			logger.error("Incorrect LeaderBoardType : " + type);
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Unhandled leaderBoard type");
		}
	}

	public void migrateUserChallengeLeaderBoard(String challengeId, Long startTime) {
		Challenge challenge = challengeDAO.getById(challengeId);
		boolean process = true;
		int start = 0;
		while (process) {
			List<ChallengeAttemptRD> challengeAttempts = challengeAttemptDAO.getChallengeTakensByChallengeId(challengeId,
					start, ChallengeUtilCommon.DEFAULT_BATCH_SIZE);
			if (ArrayUtils.isNotEmpty(challengeAttempts)) {
				start += challengeAttempts.size();
				for (ChallengeAttemptRD challengeAttempt : challengeAttempts) {
					logger.info("userLeaderBoard[" + challengeAttempt.userId + "] for challenge["
							+ challengeAttempt.challengeId + "]");
					try {
						updateLeaderBoard(challengeAttempt, challenge, startTime);
					} catch (Exception e) {
						logger.error("Error in updateLeaderBoard. Attempt : " + challengeAttempt.getId()
								+ " processed,  error " + e.getMessage());
					}
				}
			} else {
				process = false;
			}
		}
	}

	private void updateLeaderBoard(ChallengeAttemptRD challengeAttempt, Challenge challenge, Long startTime) {

		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		int totalPoints = challengeAttempt.getTotalPoints();
		long timeTaken = challengeAttempt.getTimeTaken();
		int NUMBER_OF_TRIES = 2;
		for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			Transaction transaction = session.getTransaction();
			try {
				transaction.begin();
				logger.info("try :" + retry);
				Long weekStartTime = getWeekStartTime(startTime);
				Long monthStartTime = getMonthStartTime(startTime);
				UserChallengeLeaderBoard userLeaderBoardWeek = postChallengeProcessor.incrementUserChallengeLeaderBoard(
						challengeAttempt, totalPoints, LeaderBoardType.WEEK, challengeAttempt.getCategory(),
						challenge.getDuration(), weekStartTime, session,false,false,0);
				if (userLeaderBoardWeek != null) {
					userChallengeLeaderBoardDAO.update(userLeaderBoardWeek, session, "SYSTEM");
					logger.info("userLeaderBoardWeek " + userLeaderBoardWeek);
				}
				UserChallengeLeaderBoard userLeaderBoardDefaultWeek = postChallengeProcessor
						.incrementUserChallengeLeaderBoard(challengeAttempt, totalPoints, LeaderBoardType.WEEK,
								"DEFAULT", challenge.getDuration(), weekStartTime, session,false,false,0);
				if (userLeaderBoardDefaultWeek != null) {
					userChallengeLeaderBoardDAO.update(userLeaderBoardDefaultWeek, session, "SYSTEM");
					logger.info("userLeaderBoardDefaultWeek " + userLeaderBoardDefaultWeek);
				}
				/////////////////////////////////////
				UserChallengeLeaderBoard userLeaderBoardMonth = postChallengeProcessor
						.incrementUserChallengeLeaderBoard(challengeAttempt, totalPoints, LeaderBoardType.MONTH,
								challengeAttempt.getCategory(), challenge.getDuration(), monthStartTime, session,false,false,0);
				if (userLeaderBoardMonth != null) {
					userChallengeLeaderBoardDAO.update(userLeaderBoardMonth, session, "SYSTEM");
					logger.info("userLeaderBoardMonth " + userLeaderBoardMonth);
				}
				UserChallengeLeaderBoard userLeaderBoardDefaultMonth = postChallengeProcessor
						.incrementUserChallengeLeaderBoard(challengeAttempt, totalPoints, LeaderBoardType.MONTH,
								"DEFAULT", challenge.getDuration(), monthStartTime, session,false,false,0);
				if (userLeaderBoardDefaultMonth != null) {
					userChallengeLeaderBoardDAO.update(userLeaderBoardDefaultMonth, session, "SYSTEM");
					logger.info("userLeaderBoardDefaultMonth " + userLeaderBoardDefaultMonth);
				}
				////////////////////////////

				UserChallengeLeaderBoard userLeaderBoardOverall = postChallengeProcessor
						.incrementUserChallengeLeaderBoard(challengeAttempt, totalPoints, LeaderBoardType.OVERALL,
								challengeAttempt.getCategory(), challenge.getDuration(), weekStartTime, session,false,false,0);
				if (userLeaderBoardOverall != null) {
					userChallengeLeaderBoardDAO.update(userLeaderBoardOverall, session, "SYSTEM");
					logger.info("userLeaderBoardOverall " + userLeaderBoardOverall);
				}
				UserChallengeLeaderBoard userLeaderBoardDefaultOverall = postChallengeProcessor
						.incrementUserChallengeLeaderBoard(challengeAttempt, totalPoints, LeaderBoardType.OVERALL,
								"DEFAULT", challenge.getDuration(), weekStartTime, session,false,false,0);
				if (userLeaderBoardDefaultOverall != null) {
					userChallengeLeaderBoardDAO.update(userLeaderBoardDefaultOverall, session, "SYSTEM");
					logger.info("userLeaderBoardDefaultOverall " + userLeaderBoardDefaultOverall);
				}
				////////////////////////////////////////////////
				UserChallengeLeaderBoardTransaction statsTransaction = new UserChallengeLeaderBoardTransaction(
						challengeAttempt.getUserId(), userLeaderBoardDefaultOverall.getId(),
						UserChallengeStatsContextType.CHALLENGE_ATTEMPT, challengeAttempt.getId(), (int) totalPoints,
						userLeaderBoardDefaultOverall.getPoints(), userLeaderBoardDefaultOverall.getTotalAttempts(),
						userLeaderBoardDefaultOverall.getCorrectAttempts(), challengeAttempt.getHintTakenIndex(),
						userLeaderBoardDefaultOverall.getMultiplierPowerType(), challengeAttempt.getCategory());
				userChallengeLeaderBoardTransactionDAO.updateAll(Arrays.asList(statsTransaction), session);

				transaction.commit();
				break;
			} catch (Exception e) {
				if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
					logger.info(
							"UserLeaderBoard for Id :" + challengeAttempt.getUserId() + " Rollback: " + e.getMessage());
					session.getTransaction().rollback();
				}
				if (retry == (NUMBER_OF_TRIES - 1)) {
					throw e;
				}
			} finally {
				session.close();
			}
		}

	}

	public UserChallengeLeaderBoard getUserChallengeStats(Long userId, String category) throws NotFoundException {
		
		UserChallengeLeaderBoard userChallengeLeaderBoard = userChallengeLeaderBoardDAO.getByUserId(userId,
				LeaderBoardType.OVERALL, null, category);
		if (userChallengeLeaderBoard == null) {
			userChallengeLeaderBoard = new UserChallengeLeaderBoard();
			userChallengeLeaderBoard.setUserId(userId);
			userChallengeLeaderBoard.setHintsCountMap(new ArrayList<>());
			userChallengeLeaderBoard.setMultiplierPowerType(MultiplierPowerType.SINGLE);
			userChallengeLeaderBoard.setStartTime(getWeekStartTime(System.currentTimeMillis()));
			userChallengeLeaderBoard.setLeaderBoardType(LeaderBoardType.OVERALL);
			userChallengeLeaderBoard.setCategory(category);
			//userChallengeLeaderBoardDAO.create(userChallengeLeaderBoard, "SYSTEM");
		}
		return userChallengeLeaderBoard;
	}

	public List<UserLeaderBoardInfo> getLeaderBoardsCache(String category, LeaderBoardType type,
			Long startTime) {

		List<UserLeaderBoardInfo> leaderBoards = null;
		String key = getLeaderBoardCacheKey(category, type, startTime);
		logger.info("Key produced is "+key);
		if (StringUtils.isNotEmpty(key)) {
			String response = null;
			try {
				response = redisDAO.get(key);
			} catch (Exception e) {
				logger.info("redis get throws exception." + key + " " + e);
			}
			if (StringUtils.isNotEmpty(response)) {
				Type listType1 = new TypeToken<ArrayList<UserLeaderBoardInfo>>() {
				}.getType();
				leaderBoards = gson.fromJson(response, listType1);
			}
		}
		return leaderBoards;
	}

	public String getLeaderBoardCacheKey(String category, LeaderBoardType type, Long startTime) {

		if(StringUtils.isEmpty(category) || type== null || startTime == null){
			logger.info("Category or LeaderBoardType or startTime is null" + category + " " + type + " "+startTime);
			return null;
		}
		String key =  ("LEADERBOARD_" + category + "_" + type.name() + "_" + startTime);
		logger.info("key for leaderBoard cache is "+key);
		return key;
	}

	public void deleteLeaderBoardCache(String category) throws BadRequestException {
		Long currentTime = System.currentTimeMillis();
		List<String> cacheKeys = new ArrayList<>();

		for (LeaderBoardType type : LeaderBoardType.values()) {
			try {
				Long startTime = fetchStartTime(type, currentTime);
				if (type.equals(LeaderBoardType.PREVIOUS_WEEK)) {
					cacheKeys.add(getLeaderBoardCacheKey(category, LeaderBoardType.WEEK, startTime));
				} else {
					cacheKeys.add(getLeaderBoardCacheKey(category, type, startTime));
				}
			} catch (Exception e) {
				logger.info("exception while creating keys for deletion" + e);
			}
		}
		if (ArrayUtils.isNotEmpty(cacheKeys)) {
			String[] deleteKeys = new String[cacheKeys.size()];
			try {
				redisDAO.deleteKeys(cacheKeys.toArray(deleteKeys));
			} catch (Exception e) {
				logger.info("Error in deleting Keys." + e);
			}
		}
	}
}
