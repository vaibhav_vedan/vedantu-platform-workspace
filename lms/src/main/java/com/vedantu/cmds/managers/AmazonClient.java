package com.vedantu.cmds.managers;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import com.amazonaws.services.s3.model.S3Object;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.io.InputStream;
import javax.annotation.PreDestroy;


@Service
public class AmazonClient {

    public static final String AWS_CMDS_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.bucket");//
    public static final String AWS_QUESTION_SET_FOLDER = "question-sets";
    public static final String PUBLIC_CONTENT_IDENTIFIER = "/public/";

    private static AmazonS3 s3Client;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonClient.class);

    @PostConstruct
    public void init() {
        try {
            s3Client = new AmazonS3Client();
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        } catch (Exception ex) {
            logger.info("Error occured initializing s3Client : " + ex.getMessage());
        }
    }

    @Deprecated
    public boolean uploadImage(String keyName, File file, Map<String, String> metadata) throws FileNotFoundException {
        return uploadImage(keyName, new FileInputStream(file), metadata);
    }

    @Deprecated
    public boolean uploadImage(String keyName, FileInputStream fileInputStream, Map<String, String> metadata)
            throws FileNotFoundException {
        return uploadImage(keyName, fileInputStream, metadata, getS3Location());
    }

    public boolean uploadImage(String keyName, File file, Map<String, String> metadata, String s3Location) throws FileNotFoundException {
        return uploadImage(keyName, new FileInputStream(file), metadata, s3Location);
    }

    public boolean uploadImage(String keyName, FileInputStream fileInputStream, Map<String, String> metadata,
            String s3Location)
            throws FileNotFoundException {
        logger.info("Request : " + keyName + ", s3 location " + s3Location);
        ObjectMetadata objectMetadata = new ObjectMetadata();
        if (metadata != null && metadata.size() > 0) {
            for (String key : metadata.keySet()) {
                objectMetadata.addUserMetadata(key, metadata.get(key));
            }
        }

        objectMetadata.setContentType("image/jpeg");
        PutObjectRequest putObjectRequest = new PutObjectRequest(s3Location, keyName, fileInputStream,
                objectMetadata);
        PutObjectResult result = s3Client.putObject(putObjectRequest);
        logger.info("upload respodnse from s3 : " + result.toString());
        return true;
    }

    public String getImageUrl(String keyName, String bucketName) {
        //key == fully qualified key name along with folder path
        java.util.Date expiration = new java.util.Date();
        long milliSeconds = expiration.getTime();
        milliSeconds += 1000 * 60 * 60 * 12; // Add 12 hour.
        expiration.setTime(milliSeconds);
        if (keyName.contains(PUBLIC_CONTENT_IDENTIFIER)) {
            return s3Client.getUrl(bucketName, keyName).toString();
        }
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, keyName);
        return s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();
    }

    public String getImageUrlForDoubts(String keyName, String bucketName) {
        //key == fully qualified key name along with folder path
        java.util.Date expiration = new java.util.Date();
        long milliSeconds = expiration.getTime();
        milliSeconds += 12 * 1000 * 60 * 60; // Add 12 hour.
        expiration.setTime(milliSeconds);
        if (keyName.contains(PUBLIC_CONTENT_IDENTIFIER)) {
            return s3Client.getUrl(bucketName, keyName).toString();
        }
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucketName, keyName);
        generatePresignedUrlRequest.setExpiration(expiration);
        return s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();
    }
    
    
    public InputStream getObject(String keyName, String bucketName) {
        GetObjectRequest req = new GetObjectRequest(bucketName, keyName);
        S3Object object = s3Client.getObject(req);
        if (object == null) {
            return null;
        }
        return s3Client.getObject(req).getObjectContent();
    }

    @Deprecated
    private String getS3Location() {
        return AWS_CMDS_BUCKET + "/" + ConfigUtils.INSTANCE.getEnvironmentSlug() + "/" + AWS_QUESTION_SET_FOLDER;
    }

    public String getPreSignedUrl(String bucket, String key, String contentType) {
        //bucket== only bucket name
        //key == fully qualified key name along with folder path
        String presignedUrl = "";

        try {
            java.util.Date expiration = new java.util.Date();
            long milliSeconds = expiration.getTime();
            milliSeconds += 12 * 1000 * 60 * 60; // Add 12 hour.
            expiration.setTime(milliSeconds);

            GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(bucket, key);
            generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
            generatePresignedUrlRequest.setExpiration(expiration);
            generatePresignedUrlRequest.setContentType(contentType);

            presignedUrl = s3Client.generatePresignedUrl(generatePresignedUrlRequest).toString();

            logger.info("preSignedUrl:" + presignedUrl);
        } catch (Exception exception) {
            logger.error(exception.getMessage());
        }

        return presignedUrl;
    }

    public String uploadFile(File file, String key, String bucketName)
    {
        try
        {
            s3Client.putObject(bucketName, key, file);
            logger.info("Uploaded file successfully");
            return s3Client.getUrl(bucketName, key).toExternalForm();
        }
        catch (Exception e)
        {
            return "File couldn't be uploaded due to : " + e.getMessage();
        }
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }        
}
