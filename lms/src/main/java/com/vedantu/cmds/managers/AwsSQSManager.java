/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.cmds.managers;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;


/**
 *
 * @author ajith
 */
@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    private static String arn;
    private static String env;

    private AmazonSNSAsync snsClient;

    private Map<String, String> queueUrlMap = new HashMap<>();

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
            snsClient = AmazonSNSAsyncClientBuilder.standard()
                    .withRegion(Regions.AP_SOUTHEAST_1)
                    .build();
            logger.info("Initialized SNS client");

            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                try {
                    CreateQueueRequest cqr = new CreateQueueRequest(SQSQueue.CHALLENGES_QUEUE
                            .getQueueName(env));
                    cqr.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(cqr);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest changeAnswerQueue = new CreateQueueRequest(SQSQueue.POST_CHANGE_ANSWER_QUEUE
                            .getQueueName(env));
                    changeAnswerQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    changeAnswerQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.POST_CHANGE_ANSWER_QUEUE.getVisibilityTimeout());

                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(changeAnswerQueue);

                    CreateQueueRequest changeAnswerQueueDL = new CreateQueueRequest(SQSQueue.POST_CHANGE_ANSWER_QUEUE_DL
                            .getQueueName(env));
                    changeAnswerQueueDL.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(changeAnswerQueueDL);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest changeQuestionQueue = new CreateQueueRequest(SQSQueue.POST_CHANGE_QUESTION_QUEUE
                            .getQueueName(env));
                    changeQuestionQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    changeQuestionQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.POST_CHANGE_QUESTION_QUEUE.getVisibilityTimeout());

                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(changeQuestionQueue);

                    CreateQueueRequest changeQuestionQueueDL = new CreateQueueRequest(SQSQueue.POST_CHANGE_QUESTION_QUEUE_DL
                            .getQueueName(env));
                    changeQuestionQueueDL.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(changeQuestionQueueDL);


                    logger.info("created queue for challenges");
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }


                try {
                    logger.info("creating queues for post test attempt ops for "+env);
                    CreateQueueRequest postTestAttemptQueue = new CreateQueueRequest(SQSQueue.POST_TEST_ATTEMPT_OPS
                            .getQueueName(env));
                    postTestAttemptQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    postTestAttemptQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.POST_TEST_ATTEMPT_OPS.getVisibilityTimeout());
                    logger.info(postTestAttemptQueue);

                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(postTestAttemptQueue);

                    CreateQueueRequest postTestAttemptQueueDL = new CreateQueueRequest(SQSQueue.POST_TEST_ATTEMPT_OPS_DL
                            .getQueueName(env));
                    postTestAttemptQueueDL.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(postTestAttemptQueueDL);

                    logger.info("created queue for postTestAttemptQueue");
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest postTestEndOpsDl = new CreateQueueRequest(SQSQueue.POST_TEST_END_OPS_DL.
                            getQueueName(env));
                    postTestEndOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.POST_TEST_END_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(postTestEndOpsDl);
                    logger.info("created postTestEndOps dead letter queue" + SQSQueue.POST_TEST_END_OPS_DL.getQueueName(env));

                    CreateQueueRequest postTestEndOps = new CreateQueueRequest(SQSQueue.POST_TEST_END_OPS.
                            getQueueName(env));
                    postTestEndOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.POST_TEST_END_OPS.getVisibilityTimeout());
                    sqsClient.createQueue(postTestEndOps);
                    logger.info("created postTestEndOps queue:" + SQSQueue.POST_TEST_END_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.POST_TEST_END_OPS
                            .getQueueName(env), SQSQueue.POST_TEST_END_OPS_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest migrationOpsDl = new CreateQueueRequest(SQSQueue.MIGRATION_NON_FIFO_OPS_DL.
                            getQueueName(env));
                    migrationOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.MIGRATION_NON_FIFO_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(migrationOpsDl);
                    logger.info("created migrationOps dead letter queue" + SQSQueue.MIGRATION_NON_FIFO_OPS_DL.getQueueName(env));

                    CreateQueueRequest migrationOps = new CreateQueueRequest(SQSQueue.MIGRATION_NON_FIFO_OPS.
                            getQueueName(env));
                    migrationOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.MIGRATION_NON_FIFO_OPS.getVisibilityTimeout());
                    sqsClient.createQueue(migrationOps);
                    logger.info("created migrationOps queue:" + SQSQueue.MIGRATION_NON_FIFO_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.MIGRATION_NON_FIFO_OPS
                            .getQueueName(env), SQSQueue.MIGRATION_NON_FIFO_OPS_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // creating queue - testEndOpsDl
                    CreateQueueRequest testEndOpsDl = new CreateQueueRequest(SQSQueue.TEST_END_OPS_DL.getQueueName(env));
                    testEndOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.TEST_END_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(testEndOpsDl);
                    logger.info("created testEndOps dead letter queue" + SQSQueue.TEST_END_OPS_DL.getQueueName(env));

                    // creating queue - testEndOps
                    CreateQueueRequest testEndOps = new CreateQueueRequest(SQSQueue.TEST_END_OPS.getQueueName(env));
                    testEndOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.TEST_END_OPS.getVisibilityTimeout());
                    sqsClient.createQueue(testEndOps);
                    logger.info("created testEndOps queue:" + SQSQueue.TEST_END_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.TEST_END_OPS
                            .getQueueName(env), SQSQueue.TEST_END_OPS_DL
                            .getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest postTestEndOpsDl = new CreateQueueRequest(SQSQueue.SHARE_TEST_DL_QUEUE.
                            getQueueName(env));
                    postTestEndOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SHARE_TEST_DL_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(postTestEndOpsDl);
                    logger.info("created queue" + SQSQueue.SHARE_TEST_DL_QUEUE.getQueueName(env));

                    CreateQueueRequest postTestEndOps = new CreateQueueRequest(SQSQueue.SHARE_TEST_QUEUE.
                            getQueueName(env));
                    postTestEndOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SHARE_TEST_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(postTestEndOps);
                    logger.info("created queue:" + SQSQueue.SHARE_TEST_QUEUE.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.SHARE_TEST_QUEUE.getQueueName(env),
                            SQSQueue.SHARE_TEST_DL_QUEUE.getQueueName(env), 4);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest postTestEndOpsDl = new CreateQueueRequest(SQSQueue.SHARE_ASSIGNMENT_DL_QUEUE.
                            getQueueName(env));
                    postTestEndOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SHARE_ASSIGNMENT_DL_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(postTestEndOpsDl);
                    logger.info("created queue" + SQSQueue.SHARE_ASSIGNMENT_DL_QUEUE.getQueueName(env));
                    CreateQueueRequest postTestEndOps = new CreateQueueRequest(SQSQueue.SHARE_ASSIGNMENT_QUEUE.
                            getQueueName(env));
                    postTestEndOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SHARE_ASSIGNMENT_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(postTestEndOps);
                    logger.info("created queue:" + SQSQueue.SHARE_ASSIGNMENT_QUEUE.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.SHARE_ASSIGNMENT_QUEUE.getQueueName(env),
                            SQSQueue.SHARE_ASSIGNMENT_DL_QUEUE.getQueueName(env), 1);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    CreateQueueRequest vgroupQueueDl = new CreateQueueRequest(SQSQueue.VGROUP_QUEUE_DL_QUEUE.getQueueName(env));
                    vgroupQueueDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.VGROUP_QUEUE_DL_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(vgroupQueueDl);
                    logger.info("created queue" + SQSQueue.VGROUP_QUEUE_DL_QUEUE.getQueueName(env));

                    CreateQueueRequest vgroupQueue = new CreateQueueRequest(SQSQueue.VGROUP_QUEUE.getQueueName(env));
                    vgroupQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.VGROUP_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(vgroupQueue);
                    logger.info("created queue" + SQSQueue.VGROUP_QUEUE.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.VGROUP_QUEUE.getQueueName(env), SQSQueue.VGROUP_QUEUE_DL_QUEUE.getQueueName(env), 4);
                }
                catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                try {
                    logger.info("creating queues for teacher dashboard ops for "+env);

                    CreateQueueRequest teacherDashboardOpsQueue = new CreateQueueRequest(SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_QUEUE.getQueueName(env));
                    teacherDashboardOpsQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_QUEUE.getVisibilityTimeout());
                    logger.info(teacherDashboardOpsQueue);
                    CreateQueueResult result = sqsClient.createQueue(teacherDashboardOpsQueue);
                    String consumptionQueueUrl = result.getQueueUrl();
                    queueUrlMap.put(SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_QUEUE.getQueueName(env), consumptionQueueUrl);
                    logger.info("created queue for teacherDashboardOpsQueue");

                    CreateQueueRequest teacherDashboardOpsQueueDL = new CreateQueueRequest(SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_DL_QUEUE.getQueueName(env));
                    teacherDashboardOpsQueueDL.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_DL_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(teacherDashboardOpsQueueDL);
                    logger.info("created queue for teacherDashboardOpsQueueDl");
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }

    }

    public void createSubscription(String topicArn, String queueName) {
        logger.info("Trying to create subscription for topicArn - {} and queueName - {}",topicArn,queueName);
        String queueUrl = queueUrlMap.get(queueName);
        GetQueueAttributesRequest queueAttributesRequest = new GetQueueAttributesRequest(queueUrl).withAttributeNames("All");
        GetQueueAttributesResult queueAttributesResult = sqsClient.getQueueAttributes(queueAttributesRequest);
        Map<String, String> sqsAttributeMap = queueAttributesResult.getAttributes();
        String sqsArn = sqsAttributeMap.get("QueueArn");
        SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn, "sqs", sqsArn);
        SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
        logger.info("Subscription created successfully for queue: " + queueName);
    }

    public String getTopicArn(String topic) {
        return "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic + "_" + env.toUpperCase();
    }


    private void assignDeadLetterQueue(String src_queue_name,String dl_queue_name,Integer maxReceive){
        if(maxReceive==null){
            maxReceive=4;
        }
        // Get dead-letter queue ARN
        String dl_queue_url = sqsClient.getQueueUrl(dl_queue_name)
                                 .getQueueUrl();

        GetQueueAttributesResult queue_attrs = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dl_queue_url)
                    .withAttributeNames("QueueArn"));

        String dl_queue_arn = queue_attrs.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String src_queue_url = sqsClient.getQueueUrl(src_queue_name)
                                  .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(src_queue_url)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\""+maxReceive+"\", \"deadLetterTargetArn\":\""
                        + dl_queue_arn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: "+dl_queue_name+"  to:"+src_queue_name);

    }  
    
    @PostConstruct
    public void postQueueCreation(){
        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
            assignDeadLetterQueue(SQSQueue.POST_CHANGE_ANSWER_QUEUE.getQueueName(env), SQSQueue.POST_CHANGE_ANSWER_QUEUE_DL.getQueueName(env), 4);
            assignDeadLetterQueue(SQSQueue.POST_CHANGE_QUESTION_QUEUE.getQueueName(env), SQSQueue.POST_CHANGE_QUESTION_QUEUE_DL.getQueueName(env), 4);
            assignDeadLetterQueue(SQSQueue.POST_TEST_ATTEMPT_OPS.getQueueName(env), SQSQueue.POST_TEST_ATTEMPT_OPS_DL.getQueueName(env), 4);
            assignDeadLetterQueue(SQSQueue.TEST_END_OPS.getQueueName(env), SQSQueue.TEST_END_OPS_DL.getQueueName(env), 4);
            assignDeadLetterQueue(SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_QUEUE.getQueueName(env), SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_DL_QUEUE.getQueueName(env), 4);
            //createSubscription(getTopicArn(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE.toString()), SQSQueue.TEACHER_CONTENT_DASHBOARD_OPS_QUEUE.getQueueName(env));

        }

    }

}
