package com.vedantu.cmds.managers;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.board.pojo.Board;
import com.vedantu.cmds.dao.CurriculumDAO;
import com.vedantu.cmds.dao.CurriculumTemplateDAO;
import com.vedantu.cmds.entities.Curriculum;
import com.vedantu.cmds.entities.CurriculumTemplate;
import com.vedantu.cmds.pojo.CourseDetails;
import com.vedantu.cmds.request.CurriculumTemplateInsertionRequest;
import com.vedantu.cmds.request.CurriculumTemplateResponse;
import com.vedantu.cmds.request.CurriculumTemplateRetrievalFilterRequest;
import com.vedantu.cmds.request.CurriculumTemplateRetrievalRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CurriculumTemplateManager {

    @Autowired
    private CurriculumTemplateDAO curriculumTemplateDAO;

    @Autowired
    private CurriculumDAO curriculumDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private FosUtils fosUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CurriculumTemplateManager.class);

    private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    public PlatformBasicResponse insertCurriculumTemplate(CurriculumTemplateInsertionRequest request)
            throws VException{

        // Check for duplication in curriculum structure title
        checkIfCurriculumStructureTitleAlreadyPresent(request.getTitle());

        // `if` happens when request comes from csv upload
        // `else` happens when request comes from standalone curriculum template creation
        if(request.getNode()==null){
            logger.info("Insertion from curriculum update in course.");

            // Check if title is absent and throw error accordingly
            if(StringUtils.isEmpty(request.getTitle())){
                throw new BadRequestException(ErrorCode.CURRICULUM_TEMPLATE_TITLE_EMPTY,"Please specify a proper title for the curriculum template");
            }

            // Get all the curriculum
            List<Curriculum> curriculum=curriculumDAO.getCurriculumByContextIdForCurriculumStructure(request.getEntityId());
            logger.info("Getting inserted curriculum for fetching curriculum template: "+curriculum);

            // Find the root curriculum as map is unable to map null
            Curriculum parentCurriculum=curriculum.stream()
                    .filter(curricula->curricula.getParentId()==null)
                    .findFirst()
                    .get();
            parentCurriculum.setParentId("PARENT");

            // Map them according to their id
            Map<String,List<Curriculum>> curriculumMap=
                    curriculum
                            .stream()
                            .collect(Collectors.groupingBy(Curriculum::getParentId));
            logger.info("Prepared curriculum map is "+curriculumMap);

            // Getting the parent as parent has null key as its parent id
            Curriculum parentCurricula=curriculumMap.get("PARENT").get(0);
            logger.info("Getting the parent curriculum: "+parentCurricula);

            CurriculumTemplate parentNodeOfCurriculumTemplate =new CurriculumTemplate(parentCurricula,request);

            // Populate course details for the first time
            CourseDetails courseDetails=new CourseDetails();
            courseDetails.setCourseId(request.getEntityId());
            courseDetails.setCourseName(getCourseName(request.getEntityId()));
            if(ArrayUtils.isNotEmpty(request.getBatchIds())){
                courseDetails.setBatchIds(request.getBatchIds().stream().collect(Collectors.toSet()));
            }
            logger.info("Retrieved course details from database is "+courseDetails);

            parentNodeOfCurriculumTemplate.insertCourseDetail(courseDetails);

            // insert parent details
            logger.info("parent node of curriculum template before insertion is "+parentNodeOfCurriculumTemplate);
            curriculumTemplateDAO.saveEntity(parentNodeOfCurriculumTemplate);

            // this is a jugaad setup so as to retrieve root nodes for it children, not really saved to
            // database for this data
            parentNodeOfCurriculumTemplate.setRootId(parentNodeOfCurriculumTemplate.getId());
            logger.info("parent node of curriculum template after insertion is "+parentNodeOfCurriculumTemplate);

            // recursively insert all the following templates
            recursiveInsertTemplatesFromCurriculumMapping(curriculumMap,parentCurricula, parentNodeOfCurriculumTemplate);

        }else{

            logger.info("Standalone insertion into curriculum structure.");

            // Get the parent node and extract the required data to curriculum template
            Node node=request.getNode();
            
            // validate board ids for standalone curriculum template structure insertion
            validateBoardIdsForCurriculumTemplate(node);
            
            CurriculumTemplate parentCurriculumTemplate =new CurriculumTemplate(node,request);
            logger.info("parentCurriculumTemplate data for db insertion: "+parentCurriculumTemplate);

            // save the parent template
            curriculumTemplateDAO.saveEntity(parentCurriculumTemplate);

            // this is a jugaad setup so as to retrieve root nodes for it children, not really saved to
            // database for this data
            parentCurriculumTemplate.setRootId(parentCurriculumTemplate.getId());
            logger.info("parentCurriculumTemplate after saving into db and tweaking: "+parentCurriculumTemplate);

            // recursively insert all the child nodes
            recursiveInsertStandAloneChildren(node.getNodes(), parentCurriculumTemplate);
        }
        return new PlatformBasicResponse();
    }

    private void validateBoardIdsForCurriculumTemplate(Node node) throws BadRequestException{
        Set<Long> allBoardIds= getAllBoardIdsFromNodeAndValidateThemWithin(node);
        logger.info("validateBoardIdsForCurriculumTemplate: allBoardIds before removal of one null"+allBoardIds);
        allBoardIds.remove(null); // One time removal as the parent can't contain any board id
        logger.info("validateBoardIdsForCurriculumTemplate: allBoardIds after removal of one null"+allBoardIds);

        Map<Long, Board> boardMap=fosUtils.getBoardInfoMap(allBoardIds);
        logger.info("validateBoardIdsForCurriculumTemplate: boardMap"+boardMap.keySet());
        boardMap.remove(null); // One time removal as the parent can't contain any board id
        if(boardMap.size()!=allBoardIds.size() || allBoardIds.contains(null)){
            allBoardIds.removeAll(boardMap.keySet());
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,"Some of the board ids are invalid: "+allBoardIds);
        }
    }

    private static Set<Long> getAllBoardIdsFromNodeAndValidateThemWithin(Node node){
        if(node!=null){
            Set<Long> allBoardIds =
                    Optional.ofNullable(node.getNodes())
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(CurriculumTemplateManager::getAllBoardIdsFromNodeAndValidateThemWithin)
                        .flatMap(Collection::stream)
                        .collect(Collectors.toSet());
            allBoardIds.add(node.getBoardId());
            return allBoardIds;
        }
        return new HashSet<>();

    }

    private void recursiveInsertStandAloneChildren(List<Node> nodes, CurriculumTemplate parentNode) {
        logger.info("recursiveInsertStandAloneChildren: DEBUG START"+parentNode.hashCode());
        logger.info("nodes inside recursive call: "+nodes);
        logger.info("parentNode inside recursive call: "+parentNode);


        // Stopping condition when it reaches the children
        if(ArrayUtils.isEmpty(nodes)){
            return;
        }

        // Mutating internal nodes to store the child order
        AtomicInteger childOrder=new AtomicInteger(-1);
        nodes.forEach(node->node.setChildOrder(childOrder.incrementAndGet()));
        logger.info("After changing nodes are "+nodes);

        // Getting bulk curriculum templates for insertion and inserting the same
        List<CurriculumTemplate> curriculumTemplates =
                    nodes
                        .stream()
                        .map(node->new CurriculumTemplate(node,parentNode))
                        .collect(Collectors.toList());
        curriculumTemplateDAO.insertAll(curriculumTemplates);
        logger.info("After insertion into db curriculumTemplates are "+curriculumTemplates);

        // Getting nodes with child order for mapping with inserted template in the database
        Map<Integer, Node> childOrderNodeMap =
                    nodes
                        .stream()
                        .collect(Collectors.toMap(
                                node -> node.getChildOrder(),
                                node -> node
                        ));
        logger.info("childOrderNodeMap: "+childOrderNodeMap);

        // Getting templates with child order for mapping with nodes
        Map<Integer, CurriculumTemplate> childOrderCurriculumStructureMap = curriculumTemplates
                .stream()
                .collect(Collectors.toMap(
                        curriculumTemplate -> curriculumTemplate.getChildOrder(),
                        curriculumTemplate -> curriculumTemplate
                ));
        logger.info("childOrderCurriculumStructureMap: "+childOrderCurriculumStructureMap);

        // Getting required data from node and template and recursively inserting the children
        childOrderNodeMap
                .keySet()
                .parallelStream()
                .forEach(key->recursiveInsertStandAloneChildren(childOrderNodeMap.get(key).getNodes(),childOrderCurriculumStructureMap.get(key)));
        
        logger.info("recursiveInsertStandAloneChildren: DEBUG END"+parentNode.hashCode());
    }

    private void recursiveInsertTemplatesFromCurriculumMapping(Map<String, List<Curriculum>> curriculumMap, Curriculum parentCurricula, CurriculumTemplate parentCurriculumTemplate) {
        logger.info("recursiveInsertTemplatesFromCurriculumMapping: DEBUG START: "+parentCurricula.hashCode());

        // get all the child nodes of parent curriculum
        List<Curriculum> children=curriculumMap.get(parentCurricula.getId());
        logger.info("Children of parent curriculum are: "+children);

        // Stopping condition when it reaches its children, no more node are available
        if(ArrayUtils.isEmpty(children)){
            return;
        }

        // Store all the children and then change them to the required curriculum template
        List<CurriculumTemplate> curriculumTemplates =
                children
                        .parallelStream()
                        .map(child -> new CurriculumTemplate(child, parentCurriculumTemplate))
                        .collect(Collectors.toList());

        // Save all the curriculum templates
        logger.info("Curriculum templates before insertion: "+curriculumTemplates);
        curriculumTemplateDAO.insertAll(curriculumTemplates);
        logger.info("Curriculum templates after insertion: "+curriculumTemplates);


        // For each templates go for recursive insertion of its children according to child order of both
        // curriculum and curriculumTemplate(as they are mapped)
        Map<Integer, Curriculum> curriculumChildOrderMap =
                children
                        .parallelStream()
                        .collect(Collectors.toMap(
                                curriculum->curriculum.getChildOrder(),
                                curriculum->curriculum
                        ));
        logger.info("Curriculum child order map: "+curriculumChildOrderMap);

        // mapping of curriculum template according to child order
        Map<Integer, CurriculumTemplate> curriculumTemplateChildOrderMap =
                curriculumTemplates
                        .parallelStream()
                        .collect(
                                Collectors.toMap(
                                        curriculumTemplate -> curriculumTemplate.getChildOrder(),
                                        curriculumTemplate -> curriculumTemplate
                                )
                        );
        logger.info("Curriculum template child order map: "+curriculumTemplateChildOrderMap);

        // mapping of curriculum and curriculum template along with a recursive call
        curriculumChildOrderMap
                .keySet()
                .parallelStream()
                .forEach(key-> recursiveInsertTemplatesFromCurriculumMapping(curriculumMap,curriculumChildOrderMap.get(key),curriculumTemplateChildOrderMap.get(key)));

        logger.info("recursiveInsertTemplatesFromCurriculumMapping: DEBUG END: "+parentCurricula.hashCode());
    }

    
    public String getCourseName(String id) throws VException{
        String url = SUBSCRIPTION_ENDPOINT+"course/getCourseName/"+id;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String courseName = resp.getEntity(String.class);
        if(StringUtils.isEmpty(courseName)){
            throw new BadRequestException(ErrorCode.COURSE_NOT_FOUND,"Course not found for course id "+id);
        }
        return courseName;
    }

    public List<CurriculumTemplate> getFilteredCurriculumTemplates(CurriculumTemplateRetrievalFilterRequest request){
        return curriculumTemplateDAO.getFilteredCurriculumTemplates(request);
    }

    public CurriculumTemplateResponse getCurriculumTemplateStructure(CurriculumTemplateRetrievalRequest request) throws BadRequestException{

        logger.info("getCurriculumTemplateStructure: DEBUG START");
        logger.info("The request object is : "+request);

        // Get the root template and check that the id is a valid root template id
        CurriculumTemplate rootTemplateObject=curriculumTemplateDAO.getEntityById(request.getParentId());
        logger.info("Retrieved rootTemplateObject is: "+rootTemplateObject);

        if(rootTemplateObject==null || rootTemplateObject.getParentId()!=null){
            throw new BadRequestException(ErrorCode.INVALID_PARENT_ID_FOR_TEMPLATE,"Invalid parent id for template");
        }

        // Get all the child templates according to the root id
        List<CurriculumTemplate> curriculumTemplates=curriculumTemplateDAO.getCurriculumTemplateBasicDetailsByRootId(rootTemplateObject.getId());
        logger.info("Child curriculum templates retrieved are : "+curriculumTemplates);

        // Grouping templates by parent id
        Map<String,List<CurriculumTemplateResponse>> curriculumTemplatesGroupedByParentId=
                curriculumTemplates
                    .parallelStream()
                    .map(CurriculumTemplateResponse::new)
                    .collect(Collectors.groupingBy(CurriculumTemplateResponse::getParentId));

        // Recursive insertion of nodes
        logger.info("Curriculum templates are grouped by parent id and a recursive function is called " +
                "to put them in tree structure.");
        CurriculumTemplateResponse curriculumTemplate=mapper.map(rootTemplateObject,CurriculumTemplateResponse.class);
        curriculumTemplate.setNodes(getAllChildren(curriculumTemplate.getId(),curriculumTemplatesGroupedByParentId));

        return curriculumTemplate;
    }

    private List<CurriculumTemplateResponse> getAllChildren(String parentId,Map<String,List<CurriculumTemplateResponse>> curriculumTemplatesGroupedByParentId){
        logger.info("parentId received is "+parentId);

        // Check if it hasn't reached to the leaf nodes
        if(!curriculumTemplatesGroupedByParentId.containsKey(parentId)){
            logger.info("No children are present for the parent id "+parentId);
            return null;
        }

        // Get children according to parent
        List<CurriculumTemplateResponse> children=curriculumTemplatesGroupedByParentId.get(parentId);
        logger.info("Children of parent id "+parentId+" are "+children);

        // Recursively adding children in case they are present.
        children
            .parallelStream()
            .forEach(child->child.setNodes(getAllChildren(child.getId(),curriculumTemplatesGroupedByParentId)));

        return children;
    }

    private void checkIfCurriculumStructureTitleAlreadyPresent(String title) throws BadRequestException{
        if(StringUtils.isEmpty(title) || curriculumTemplateDAO.getEntityByTitle(title)!=null){
            throw new BadRequestException(ErrorCode.CURRICULUM_TEMPLATE_TITLE_ALREADY_PRESENT,"Please specify another title as this title is already present");
        }
    }
}
