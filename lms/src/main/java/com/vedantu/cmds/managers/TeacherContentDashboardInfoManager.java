package com.vedantu.cmds.managers;

import com.google.gson.Gson;
import com.vedantu.cmds.dao.TeacherContentDashboardInfoDAO;
import com.vedantu.cmds.pojo.TeacherContentDashboardIncrementInfo;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class TeacherContentDashboardInfoManager {

    @Autowired
    private TeacherContentDashboardInfoDAO dashboardInfoDAO;

    @Autowired
    private ContentInfoDAO contentInfoDAO;

    @Autowired
    private AwsSNSManager awsSNSManager;

    private static final Gson gson=new Gson();

    private Logger logger = LogFactory.getLogger(TeacherContentDashboardInfoManager.class);

    public void updateTeacherContentDashboardInfo(TeacherContentDashboardIncrementInfo info) {
        logger.info("incrementing count for info {}",info);
        dashboardInfoDAO.updateTeacherContentDashboardInfo(info);
    }

    @Deprecated
    public void migrateTeacherContentInfo(List<String> teacherIds) {
        logger.info("teacherIds - {}",teacherIds);
        if (ArrayUtils.isEmpty(teacherIds)) {
            return;
        }
        logger.info("Migrating teacherContentInfo with teacherId {}", teacherIds);
        for(String teacherId : teacherIds) {
            logger.info("Operation for teacher id {}",teacherId);
            if(Objects.nonNull(contentInfoDAO.getOneContentInfo(teacherId, null, null))) {
                for (ContentInfoType contentInfoType : ContentInfoType.values()) {
                    if(Objects.nonNull(contentInfoDAO.getOneContentInfo(teacherId, contentInfoType, null))) {
                        for (ContentState contentState : ContentState.values()) {
                            ContentInfo contentInfo = contentInfoDAO.getOneContentInfo(teacherId, contentInfoType, contentState);
                            if (Objects.isNull(contentInfo)) {
                                logger.info("No data present for teacherId {} contentInfoType {} contentState {}", teacherId, contentInfoType, contentState);
                                continue;
                            }
                            long contentInfoCount = contentInfoDAO.getContentInfosCount(teacherId, contentInfoType, contentState);
                            if (contentInfoCount != 0) {
                                logger.info("Sending SNS for TeacherId, ContentInfoType, ContentState contentInfoCount- {}, {}, {} {}", teacherId, contentInfoType.toString(), contentState, contentInfoCount);
                                TeacherContentDashboardIncrementInfo info = new TeacherContentDashboardIncrementInfo(teacherId, contentInfoType, contentState, contentInfoCount);
                                awsSNSManager.triggerSNS(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE, SNSSubject.UPDATE_TEACHER_CONTENT_DASHBOARD_INFO.name(), gson.toJson(info));
                            }
                        }
                    }else{
                        logger.info("Avoiding more db calls for teacher id {} contentInfoType {}",teacherId,contentInfoType);
                    }
                }
            }else{
                logger.info("Avoiding more db calls for teacher id {}",teacherId);
            }
        }
    }
}
