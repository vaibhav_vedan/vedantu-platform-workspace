package com.vedantu.cmds.managers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.cmds.dao.CMDSTestAttemptDAO;
import com.vedantu.cmds.dao.CMDSTestDAO;
import com.vedantu.cmds.dao.CurriculumDAO;
import com.vedantu.cmds.dao.CurriculumTemplateDAO;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.entities.Curriculum;
import com.vedantu.cmds.entities.CurriculumTemplate;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.managers.challenges.CommunicationManager;
import com.vedantu.cmds.pojo.CourseDetails;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.cmds.request.CurriculumTemplateInsertionRequest;
import com.vedantu.cmds.response.CurriculumCountResponse;
import com.vedantu.cmds.response.CurriculumProgressRes;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.lms.cmds.pojo.*;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.pojo.response.BasicRes;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.response.CurriculumProgressInfo;
import com.vedantu.subscription.response.StudentAttemptInfo;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.vedantu.lms.cmds.pojo.ContentInfo;
import com.vedantu.lms.cmds.pojo.CurriculumStatusChangeTime;
import com.vedantu.lms.cmds.pojo.Node;
import com.vedantu.lms.cmds.pojo.SubjectFileCountPojo;
import com.vedantu.lms.cmds.request.AddContentCurriculumReq;
import com.vedantu.lms.cmds.request.EditFromCourseToBatchRes;
import com.vedantu.lms.cmds.request.AddCurriculumReq;
import com.vedantu.lms.cmds.request.ChangeCurriculumStatusReq;
import com.vedantu.lms.cmds.request.CreateBatchCurriculumReq;
import com.vedantu.lms.cmds.request.AddOrDeleteNodeReq;
import com.vedantu.lms.cmds.request.EditCurriculumReq;
import com.vedantu.lms.cmds.request.GetCurriculumBySessionIdReq;
import com.vedantu.lms.cmds.request.GetCurriculumReq;
import com.vedantu.lms.cmds.request.ShareContentAndSendMailRes;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.lms.cmds.request.ShareContentReq;
import com.vedantu.lms.cmds.request.ShareCurriculumContentReq;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CurriculumManager {

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private CurriculumDAO curriculumDAO;

	@Autowired
	private ContentInfoManager contentInfoManager;

	@Autowired
	private ContentInfoDAO contentInfoDAO;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private CMDSTestDAO testDAO;

    @Autowired
    private HttpSessionUtils sessionUtils;

	@Autowired
	private RedisDAO redisDAO;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(CurriculumManager.class);

	@Autowired
	private DozerBeanMapper mapper;

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	@Autowired
	private CurriculumTemplateDAO curriculumTemplateDAO;

	@Autowired
	private CurriculumTemplateManager curriculumTemplateManager;

	@Autowired
	private CommunicationManager communicationManager;

	@Autowired
	private AwsSQSManager awsSQSManager;

	@Autowired
	private AwsSNSManager awsSNSManager;

	@Autowired
	private CMDSTestAttemptDAO cmdsTestAttemptDAO;

	private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
	private static final Gson gson = new Gson();

	public Node createCurriculum(AddCurriculumReq addCurriculumReq) throws VException{

		String courseName = curriculumTemplateManager.getCourseName(addCurriculumReq.getEntityId());

		// Get all the active curriculum for the existing course/batch/course plan when doing fresh insertion
		List<Curriculum> curriculumList = curriculumDAO.getCurriculumByCourseId(addCurriculumReq.getEntityName(),
				addCurriculumReq.getEntityId(),Arrays.asList(Curriculum.Constants._ID),null);

		// If you find that the curriculum exists for the entity
		if (CollectionUtils.isNotEmpty(curriculumList)) {
			throw new BadRequestException(ErrorCode.CURRICULUM_ID_ALREADY_EXISTS_OR_NOT_DELETED,
					"Curriculum already exists for " + addCurriculumReq.getEntityId());
		}

		// Validation around curriculum template id
		if(!addCurriculumReq.isCurriculumStructureChanged()){
			if(addCurriculumReq.getCurriculumTemplateId()==null){
				throw new BadRequestException(ErrorCode.CURRICULUM_TEMPLATE_ID_EMPTY,"Can't have empty template for curriculum");
			}
		}else{
			checkIfCurriculumStructureTitleAlreadyPresent(addCurriculumReq.getTitle());
		}

		// Fresh insertion is allowed only for course so check for the same
		if (addCurriculumReq.getEntityName() == CurriculumEntityName.OTF_COURSE) {

			// Check if there ar data for insertion
			if (addCurriculumReq.getNode() != null) {
				Node node = addCurriculumReq.getNode(); // get the node

				List<String> errors = validateContentIds(node); // validate the contents inside the node
				if(ArrayUtils.isNotEmpty(errors)){
					throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID,errors.toString());
				}

				validateTopicTags(node); // validate the topic tags(i.e. if they are present in redis or not)

				// Create a new curriculum, the root one named "Curriculum" and add it to DB
				Curriculum curriculum = new Curriculum(node);
				curriculum.setContextType(addCurriculumReq.getEntityName());
				curriculum.setContextId(addCurriculumReq.getEntityId());
				curriculumDAO.save(curriculum, addCurriculumReq.getCallingUserId());

				// root id is populated so that in the recurring nodes it could be used
				curriculum.setRootId(curriculum.getId());

				// recursively add other child nodes
				recurringAddNodes(node.getNodes(), curriculum, addCurriculumReq.getCallingUserId());

				// Code to store the structure of curriculum
				if(addCurriculumReq.isCurriculumStructureChanged()){
					CurriculumTemplateInsertionRequest request=mapper.map(addCurriculumReq, CurriculumTemplateInsertionRequest.class);
					request.setEntityId(addCurriculumReq.getEntityId());
					request.setEntityName(addCurriculumReq.getEntityName());
					request.setGrades(addCurriculumReq.getGrades());
					request.setTargets(addCurriculumReq.getTargets());
					request.setTitle(addCurriculumReq.getTitle());
					request.setYear(addCurriculumReq.getYear());
					request.setNode(null);
					request.setCallingUserId(addCurriculumReq.getCallingUserId());
					request.setCallingUserRole(addCurriculumReq.getCallingUserRole());
					Map<String, Object> payload = new HashMap<>();
					payload.put("request",request);
					AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.POPULATE_CURRICULUM_STRUCTURE_TABLE, payload);
					asyncTaskFactory.executeTask(params);
				}else{
					logger.info("Updating the curriculum template in case it is just imported and nothing done for template id "+addCurriculumReq.getCurriculumTemplateId());
					CourseDetails courseDetails=new CourseDetails();
					courseDetails.setCourseId(addCurriculumReq.getEntityId());
					courseDetails.setCourseName(getCourseName(addCurriculumReq.getEntityId()));
					Set<CourseDetails> sharedCourseDetails = curriculumTemplateDAO.getSharedCourseDetailsOfCurriculumTemplateById(addCurriculumReq.getCurriculumTemplateId());
					if(!sharedCourseDetails.contains(courseDetails)){
						sharedCourseDetails.add(courseDetails);
						curriculumTemplateDAO.updateSharedCourseDetailsForCurriculumTemplate(addCurriculumReq.getCurriculumTemplateId(),sharedCourseDetails);
					}
				}
			} else {
				throw new BadRequestException(ErrorCode.CURRICULUM_NODE_NOT_FOUND, "Curriculum Node is Null");
			}
		} else {
			throw new BadRequestException(ErrorCode.CURRICULUM_EDIT_NOT_ALLOWED,
					"curriculum course edit is not allowed");
		}

		return getAllNodes(addCurriculumReq.getEntityName(), addCurriculumReq.getEntityId());
	}

	private void checkIfCurriculumStructureTitleAlreadyPresent(String title) throws BadRequestException{
		if(StringUtils.isEmpty(title) || curriculumTemplateDAO.getEntityByTitle(title)!=null){
			throw new BadRequestException(ErrorCode.CURRICULUM_TEMPLATE_TITLE_ALREADY_PRESENT,"Please specify another title as this title is already present");
		}
	}

	private String getCourseName(String id) throws VException{
		String url = SUBSCRIPTION_ENDPOINT+"course/getCourseName/"+id;
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String courseName = resp.getEntity(String.class);
		if(StringUtils.isEmpty(courseName)){
			throw new BadRequestException(ErrorCode.COURSE_NOT_FOUND,"Course not found for course id "+id);
		}
		return courseName;
	}

	private void validateTopicTags(Node node) throws InternalServerErrorException, BadRequestException {
		List<Node> nodes = node.getNodes();
		List<String> topicTagsList = new ArrayList<>();
		validateTopicTags(nodes, topicTagsList);
		if(ArrayUtils.isEmpty(topicTagsList)){
			return;
		}
		Set<String> redisKeys=topicTagsList
				.stream()
				.map(CurriculumManager::getTopicKey)
				.collect(Collectors.toSet());
		Map<String, String> redisValues = redisDAO.getValuesForKeys(redisKeys);
		if(redisValues.size()!=redisKeys.size()){
			Set<String> retrievedKeys=redisValues.keySet();
			redisKeys.removeAll(retrievedKeys);
			throw new BadRequestException(ErrorCode.INVALID_TOPIC_TAGS,"Invalid tags are "+redisKeys,Level.INFO);
		}

	}

	public static String getTopicKey(String key){
		return "BaseTopicTree:"+key;
	}

	private void validateTopicTags(List<Node> nodes, List<String> topicTags) {
		if(ArrayUtils.isEmpty(nodes)){
			return;
		}
		Set<String> topicsSet = nodes.stream()
				.map(Node::getTopicTags)
				.filter(ArrayUtils::isNotEmpty)
				.flatMap(topics -> topics.stream())
				.collect(Collectors.toSet());
		nodes.forEach(node->validateTopicTags(node.getNodes(),topicTags));
		topicTags.addAll(topicsSet);
	}

	public Map<String, LMSStatsForSession> getLMSStatsMap(Set<String> sessionIds){
		List<Curriculum> curriculumList = curriculumDAO.getCurriculumBySessionIds(new ArrayList<>(sessionIds));

		Map<String, LMSStatsForSession> lmsStatsForSessionMap = new HashMap<>();
		Map<String, Set<String>> sessionContentInfoMap = new HashMap<>();
		Map<String, Set<String>> sessionTestMap = new HashMap<>();
		Map<String, Set<String>> sessionAssignmentMap = new HashMap<>();
		Set<String> contentInfos = new HashSet<>();
		for(Curriculum curriculum : curriculumList){
			List<ContentInfo> contentInfoList = curriculum.getContents();
			Set<String> sessionIdsForCurriculum = new HashSet<>();
			for(ContentInfo contentInfo : contentInfoList){
				int flag = 0;

				if(!(ContentType.TEST.equals(contentInfo.getContentType()) || ContentType.ASSIGNMENT.equals(contentInfo.getContentType()))){
					continue;
				}
				if(contentInfo.getSessionIds()==null){
					continue;
				}

				for(String sessionId : contentInfo.getSessionIds()){
					if(!sessionIds.contains(sessionId)){
						continue;
					}

					flag=1;

					if(ContentType.TEST.equals(contentInfo.getContentType())){
						if(!sessionTestMap.containsKey(sessionId)){
							Set<String> tests = new HashSet<>();
							tests.add(contentInfo.getTitle());
							sessionTestMap.put(sessionId, tests);
						}else{
							Set<String> tests = sessionTestMap.get(sessionId);
							tests.add(contentInfo.getTitle());
							sessionTestMap.put(sessionId, tests);
						}
					}

					if(ContentType.ASSIGNMENT.equals(contentInfo.getContentType())){
						if(!sessionAssignmentMap.containsKey(sessionId)){
							Set<String> assingments = new HashSet<>();
							assingments.add(contentInfo.getTitle());
							sessionAssignmentMap.put(sessionId, assingments);
						}else{
							Set<String> assingments = sessionAssignmentMap.get(sessionId);
							assingments.add(contentInfo.getTitle());
							sessionAssignmentMap.put(sessionId, assingments);
						}
					}



					if(!sessionContentInfoMap.containsKey(sessionId)){
						Set<String> contentInfoIds = new HashSet<>(contentInfo.getContentInfoId());
						sessionContentInfoMap.put(sessionId, contentInfoIds);
					}else{
						Set<String> contentInfoIds = sessionContentInfoMap.get(sessionId);
						contentInfoIds.addAll(contentInfo.getContentInfoId());
						sessionContentInfoMap.put(sessionId, contentInfoIds);
					}

				}

				if(flag==1){
					contentInfos.addAll(contentInfo.getContentInfoId());
				}
			}


		}

		List<Curriculum> curriculumList1 = curriculumDAO.getCurriculumDone(new ArrayList<>(sessionIds));
		Map<String, Set<String>> sessionTopicMap = new HashMap<>();
		for(Curriculum curriculum : curriculumList1){
			Set<String> sessionIdsForCurriculum = new HashSet<>();
			for(String sessionId: curriculum.getSessionIds()){
				if(!sessionIdsForCurriculum.contains(sessionId)) {
					if (!sessionTopicMap.containsKey(sessionId)) {
						Set<String> topicsList = new HashSet<>();
						topicsList.add(curriculum.getTitle());
						sessionTopicMap.put(sessionId, topicsList);
					} else {
						Set<String> topicsList = new HashSet<>();
						topicsList.add(curriculum.getTitle());
						sessionTopicMap.put(sessionId, topicsList);
					}
					sessionIdsForCurriculum.add(sessionId);
				}
			}
		}



		List<com.vedantu.moodle.entity.ContentInfo> contentInfoList = contentInfoDAO.getContentInfosById(new ArrayList<>(contentInfos));
		Map<String, com.vedantu.moodle.entity.ContentInfo> contentInfoMap = new HashMap<>();
		for(com.vedantu.moodle.entity.ContentInfo contentInfo : contentInfoList){
			contentInfoMap.put(contentInfo.getId(), contentInfo);
		}

		if(sessionIds == null){
			return lmsStatsForSessionMap;
		}

		for(String sessionId: sessionIds){
			LMSStatsForSession lmsStatsForSession = new LMSStatsForSession();

			Set<String> contentInfoIds = sessionContentInfoMap.get(sessionId);
			int testAttempted = 0;
			int testEvaluated = 0;
			int assignmentAttempted = 0;
			int assignmentEvaluated = 0;

			if(contentInfoIds != null) {

				for (String contentInfoId : contentInfoIds) {
					com.vedantu.moodle.entity.ContentInfo contentInfo = contentInfoMap.get(contentInfoId);
					if(contentInfo == null){
						continue;
					}
					if (ContentType.ASSIGNMENT.equals(contentInfo.getContentType()) && ContentState.ATTEMPTED.equals(contentInfo.getContentState())) {
						assignmentAttempted++;
					}

					if (ContentType.ASSIGNMENT.equals(contentInfo.getContentType()) && ContentState.EVALUATED.equals(contentInfo.getContentState())) {
						assignmentEvaluated++;
					}

					if (ContentType.TEST.equals(contentInfo.getContentType()) && ContentState.ATTEMPTED.equals(contentInfo.getContentState())) {
						testAttempted++;
					}

					if (ContentType.TEST.equals(contentInfo.getContentType()) && ContentState.EVALUATED.equals(contentInfo.getContentState())) {
						testEvaluated++;
					}

				}

			}
			if(sessionTestMap.get(sessionId)!=null) {
				lmsStatsForSession.setTestShared((new ArrayList<>(sessionTestMap.get(sessionId))));
			}else{
				lmsStatsForSession.setTestShared(new ArrayList<>());
			}

			if(sessionAssignmentMap.get(sessionId)!=null) {
				lmsStatsForSession.setAssingmentShared(new ArrayList<>(sessionAssignmentMap.get(sessionId)));
			}else{
				lmsStatsForSession.setAssingmentShared(new ArrayList<>());
			}
			lmsStatsForSession.setAttemptedAssignment(assignmentAttempted+assignmentEvaluated);
			lmsStatsForSession.setEvaluatedAssignment(assignmentEvaluated);
			lmsStatsForSession.setAttemptedTests(testAttempted+testEvaluated);
			lmsStatsForSession.setEvaluatedTests(testEvaluated);
			if(sessionTopicMap.get(sessionId)!=null) {
				lmsStatsForSession.setTopicDone(org.apache.commons.lang3.StringUtils.join(sessionTopicMap.get(sessionId), ","));
			}else{
				lmsStatsForSession.setTopicDone("");
			}
			lmsStatsForSessionMap.put(sessionId, lmsStatsForSession);

		}

		return lmsStatsForSessionMap;
	}


	public List<StudentAttemptInfo> getStudentAttemptInfoForSession(String sessionId, Integer start, Integer size, ContentState contentState) throws BadRequestException {
		List<String> sessionIds = new ArrayList<>();
		sessionIds.add(sessionId);
		List<Curriculum> curriculumList = curriculumDAO.getCurriculumBySessionIds(new ArrayList<>(sessionIds));
		List<String> contentInfos = new ArrayList<>();
		for(Curriculum curriculum : curriculumList){
			List<ContentInfo> contentInfoList = curriculum.getContents();
			for(ContentInfo contentInfo : contentInfoList){

				if(!ContentType.TEST.equals(contentInfo.getContentType()) && !ContentType.ASSIGNMENT.equals(contentInfo.getContentType())){
					continue;
				}

				if(contentInfo.getSessionIds() == null){
					continue;
				}

				if(!contentInfo.getSessionIds().contains(sessionId)){
					continue;
				}

				contentInfos.addAll(contentInfo.getContentInfoId());


			}


		}

		List<com.vedantu.moodle.entity.ContentInfo> contentInfoList = contentInfoDAO.getContentInfosByIdAndType(contentInfos, start, size, contentState);

		Set<String> userIds = new HashSet<>();

		for(com.vedantu.moodle.entity.ContentInfo contentInfo: contentInfoList){
			userIds.add(contentInfo.getStudentId());
		}

		Map<String, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMap(userIds, true);

		List<StudentAttemptInfo> studentAttemptInfoList = new ArrayList<>();

		for(com.vedantu.moodle.entity.ContentInfo contentInfo : contentInfoList){

			StudentAttemptInfo studentAttemptInfo = new StudentAttemptInfo();
			studentAttemptInfo.setContentTitle(contentInfo.getContentTitle());
			studentAttemptInfo.setContentId(contentInfo.getId());
			studentAttemptInfo.setContentState(contentState);
			studentAttemptInfo.setContentType(contentInfo.getContentType());
			studentAttemptInfo.setObjective(ContentInfoType.OBJECTIVE.equals(contentInfo.getContentInfoType()));
			if(studentAttemptInfo.isObjective() && ContentType.TEST.equals(contentInfo.getContentType())){
				TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();
				studentAttemptInfo.setTotalMarks(testContentInfoMetadata.getTotalMarks());
				List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),Collections.singletonList(CMDSTestAttempt.Constants.MARKS_ACHIEVED));
				studentAttemptInfo.setMarks(testAttempts.get(testAttempts.size()-1).getMarksAcheived());
			}
			UserBasicInfo userBasicInfo = userBasicInfoMap.get(contentInfo.getStudentId());
			if(userBasicInfo == null){
				logger.error("UserBasicInfo not available for userId: "+contentInfo.getStudentId());
				continue;
			}
			studentAttemptInfo.setEmail(userBasicInfo.getEmail());
			studentAttemptInfo.setPhoneCode(userBasicInfo.getPhoneCode());
			studentAttemptInfo.setPhoneNumber(userBasicInfo.getContactNumber());
			studentAttemptInfo.setsName(userBasicInfo.getFullName());

			studentAttemptInfoList.add(studentAttemptInfo);

		}

		return studentAttemptInfoList;
	}

	public CurriculumProgressInfo getTopicsStats(String batchId){
		List<String> tempList = new ArrayList<>();
		tempList.add(batchId);

		List<Curriculum> curriculumList = curriculumDAO.getCurriculumByBatchIds(tempList);


		List<String> topicToBeCovered = new ArrayList<>();
		List<String> topicDone = new ArrayList<>();

		if(curriculumList == null){
			return new CurriculumProgressInfo();
		}

		Long timestamp = System.currentTimeMillis();
		for(Curriculum curriculum : curriculumList){

			if(CurriculumStatus.DONE.equals(curriculum.getStatus())){
				topicDone.add(curriculum.getTitle());
			}


			if(curriculum.getEndTime() == null){
				continue;
			}

			if(curriculum.getEndTime() < timestamp){
				topicToBeCovered.add(curriculum.getTitle());
			}

		}

		CurriculumProgressInfo curriculumProgressInfo = new CurriculumProgressInfo();
		curriculumProgressInfo.setTopicDone(topicDone);
		curriculumProgressInfo.setTopicToBeCovered(topicToBeCovered);

		return curriculumProgressInfo;

	}

	public void recurringAddNodes(List<Node> nodes, Curriculum parent, Long callingUserId) {
		List<Curriculum> curriculumList = new ArrayList<>(); // entity to be added and saved
		int childOrder = 0; // initial child order
		HashMap<Integer,Node> nodeHashMap = new HashMap<>(); // storing node data along with its child order as its key

		// If nodes are present for insertion, proceed for the same
		if (CollectionUtils.isNotEmpty(nodes)) {
			for (Node node : nodes) {

				// Create the db object for insertion
				Curriculum curriculum = new Curriculum(node);
				curriculum.setParentId(parent.getId()); // id of parent to be stored
				curriculum.setRootId(parent.getRootId()); // id of root to be stored(i.e. node with title "Curriculum")
				if (curriculum.getChildOrder() == null) { // order of node inserted for the parent
					curriculum.setChildOrder(childOrder);
				}
				if (curriculum.getBoardId() == null && parent.getBoardId() != null) { // boardId insertion
					curriculum.setBoardId(parent.getBoardId());
				}
				if (StringUtils.isNotEmpty(node.getId())) {
					curriculum.setCourseNodeId(node.getId());
				}

				// mandatory data for the entire curriculum tree inherited from parent to all the roots
				curriculum.setContextType(parent.getContextType());
				curriculum.setContextId(parent.getContextId());

				// putting the same into hash map so that according to child order so that
				// during recursive insertion we can use map `curriculum` and `node` accordingly
				// and do a recursive insertion
				nodeHashMap.put(node.getChildOrder(),node);

				// added into curriculumList for bulk insertion
				curriculumList.add(curriculum);

				// as one child is added to the tree the child order needs to be incremented
				childOrder++;
			}

			// insert all the children in one go for optimization, and during insertion the `id`
			// field will be populated for all the curriculum
			curriculumDAO.insertAllChildren(curriculumList, callingUserId.toString());

			// Check if some data are inserted then proceed for the following operations
			if (CollectionUtils.isNotEmpty(curriculumList)) {

				// For each curriculum get the original nodes of the request object using the child order
				// and then set the field of node id same as the mapped curriculum id
				for (Curriculum curriculum : curriculumList) {
					if(CurriculumEntityName.OTF_COURSE.equals(curriculum.getContextType())) {
						nodeHashMap.get(curriculum.getChildOrder()).setId(curriculum.getId());
					}

					// recursively add all the nodes of the inserted parent
					recurringAddNodes(nodeHashMap.get(curriculum.getChildOrder()).getNodes(), curriculum, callingUserId);
				}
			}
		}
	}

	public Node getAllNodes(List<Curriculum> curriculums, Curriculum root) {
		Node node = null;
		if (ArrayUtils.isEmpty(curriculums)) {
			return node;
		}
		Map<String, List<Node>> map = new HashMap<>();
		for (Curriculum iter : curriculums) {
			if (StringUtils.isNotEmpty(iter.getParentId())) {
				if (!map.containsKey(iter.getParentId())) {
					map.put(iter.getParentId(), new ArrayList<Node>());
				}
				map.get(iter.getParentId()).add(mapper.map(iter, Node.class));
			}
		}
		node = mapper.map(root, Node.class);
		node.setNodes(structureAllNodes(map, root.getId()));

		return node;
	}

	public Node getAllNodes(CurriculumEntityName contextType, String contextId) {
		Node node = null;
		List<Curriculum> curriculums = curriculumDAO.getCurriculumByCourseId(contextType, contextId);
		if (ArrayUtils.isEmpty(curriculums)) {
			return node;
		}
		Curriculum root = findRoot(curriculums);
		return getAllNodes(curriculums, root);
	}

	private List<Node> getAllTreeNodesByBatch(List<String> contextIds) {
		String[] exclude = {Curriculum.Constants.CREATED_BY, Curriculum.Constants.CREATION_TIME, Curriculum.Constants.ENTITY_STATE,
				Curriculum.Constants.LAST_UPDATED, Curriculum.Constants.LAST_UPDATED_BY, Curriculum.Constants.CONTENTS,
				Curriculum.Constants.SESSION_IDS};

		List<Curriculum> curriculum = curriculumDAO.getCurriculumByBatchIds(contextIds, null, exclude);

		Map<String, List<Curriculum>> map = curriculum.stream().collect(Collectors.groupingBy(Curriculum::getContextId));
		List<Node> nodes = new ArrayList<>();
		for (Entry<String, List<Curriculum>> entry : map.entrySet()) {
			Curriculum root = findRoot(entry.getValue());
			Node node = getAllNodes(entry.getValue(), root);
			nodes.add(node);
		}
		return nodes;
	}


	public Node getAllNodes(CurriculumEntityName contextType, String contextId,Long boardId) {
		Node node = null;
		List<Curriculum> curriculums = curriculumDAO.getCurriculumByCourseAndBoard(contextType, contextId,boardId);
		if (ArrayUtils.isEmpty(curriculums)) {
			return node;
		}
		Curriculum root = findRootForSubTree(curriculums);
		return getAllNodes(curriculums, root);
	}

	public List<Node> structureAllNodes(Map<String, List<Node>> map, String parentId) {
		List<Node> nodes = map.get(parentId);
		if (ArrayUtils.isNotEmpty(nodes)) {
			Collections.sort(nodes, new Comparator<Node>() {
				@Override
				public int compare(Node o1, Node o2) {
					return o1.getChildOrder().compareTo(o2.getChildOrder());
				}
			});

			for (Node node : nodes) {
				node.setNodes(structureAllNodes(map, node.getId()));
			}
		}
		return nodes;
	}


	public Node getCurriculum(String curriculumId) throws BadRequestException, NotFoundException {
		if (curriculumId != null) {
			Query query = new Query();
			query.addCriteria(Criteria.where(Curriculum.Constants.ID).is(curriculumId));
			query.addCriteria(Criteria.where(Curriculum.Constants.ROOT_ID).is(curriculumId));
			query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
			List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
			if (ArrayUtils.isEmpty(curriculums)) {
				throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND,
						"curriculumId not found in DB for " + curriculumId);
			}
			Curriculum root = findRoot(curriculums);
			return getAllNodes(curriculums, root);
		} else {
			throw new BadRequestException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "curriculumId is null");
		}
	}

	public Curriculum findRoot(List<Curriculum> curriculum) {

		return Optional.ofNullable(curriculum)
				.map(Collection::stream)
				.orElseGet(Stream::empty)
				.filter(CurriculumManager::isARootNode)
				.findFirst()
				.orElse(null);
	}

	private static boolean isARootNode(Curriculum curriculuae){
		return StringUtils.isEmpty(curriculuae.getRootId());
	}

	public Curriculum findRootForSubTree(List<Curriculum> curriculums){
		if(ArrayUtils.isNotEmpty(curriculums)){
			Set<String> curriculumIds = new HashSet<>();
			for(Curriculum curriculum : curriculums){
				curriculumIds.add(curriculum.getId());
			}
			for(Curriculum curriculum : curriculums){
				if(StringUtils.isEmpty(curriculum.getParentId()) || !curriculumIds.contains(curriculum.getParentId())){
					return curriculum;
				}
			}
		}
		return null;
	}
	public Node findTreeForBoardId(List<Node> nodes, Long boardId) {
		if (ArrayUtils.isNotEmpty(nodes)) {

			// If at the first stage of nodes the root node of tree is found return the entire tree
			// for the required board id
			Node node=nodes
						.stream()
						.filter(child->boardId.equals(child.getBoardId()))
						.findFirst()
						.orElse(null);
			if(node!=null)
				return node;

			// Else go around searching in a level order traversal manner to find the root of the
			// the tree with required board id
			return nodes.stream()
					.map(Node::getNodes)
					.filter(ArrayUtils::isNotEmpty)
					.map(children -> findTreeForBoardId(children, boardId))
					.filter(Objects::nonNull)
					.findFirst()
					.orElse(null);
		}
		return null;
	}

	public Node findSubTreeForNodeId(List<Node> nodes, String nodeId) {
		if (ArrayUtils.isNotEmpty(nodes)) {
			for (Node node : nodes) {
				if (node.getId() != null && node.getId().equals(nodeId)) {
					return node;
				}
			}
			for (Node node : nodes) {
				if (ArrayUtils.isNotEmpty(node.getNodes())) {
					logger.info("node id is " + node.getId());
					Node returnNode = findSubTreeForNodeId(node.getNodes(), nodeId);
					if (returnNode != null) {
						return returnNode;
					}
				}
			}
		}
		return null;
	}

	public void getAllNodeIdsInTree(Node node, List<String> nodeIds) {
		if (StringUtils.isNotEmpty(node.getId())) {
			nodeIds.add(node.getId());
		}
		if (ArrayUtils.isNotEmpty(node.getNodes())) {
			for (Node iterNode : node.getNodes()) {
				getAllNodeIdsInTree(iterNode, nodeIds);
			}
		}
	}

	public List<Node> getCurriculumByEntity(GetCurriculumReq getCurriculumReq) throws VException {

		getCurriculumReq.verify();
		logger.info("Inside getCurriculumByEntity");
		Role role = sessionUtils.getCallingUserRole();
		Long userId = sessionUtils.getCallingUserId();
		List<Node> nodes = new ArrayList<>();
		Set<String> entityIds = new HashSet<>(getCurriculumReq.getEntityIds());
		logger.info("Role: "+role+" sessionId: "+getCurriculumReq.getSessionId()+" entity: "+getCurriculumReq.getEntityName().toString());
		if(!(StringUtils.isEmpty(getCurriculumReq.getSessionId()) && Role.STUDENT.equals(role) && ((!CurriculumEntityName.COURSE_PLAN.equals(getCurriculumReq.getEntityName())) && !checkIfUserEnrolledInEntity(getCurriculumReq.getEntityName(), String.join(",", getCurriculumReq.getEntityIds()), userId.toString())))){
			for(String entityId : entityIds){
				Node node = getAllNodes(getCurriculumReq.getEntityName(), entityId);
				if (node == null) {
					logger.info( "curriculumId not found for "
							+ getCurriculumReq.getEntityName() + " with Id " + entityId);
					continue;
				}
				if (getCurriculumReq.getBoardId() != null) {
					Node tempNode = findTreeForBoardId(Arrays.asList(node), getCurriculumReq.getBoardId());
					if (tempNode == null) {
						logger.info("node: details");
						printNodeDetailsForDebugPurpose(node,"");
						logger.info( "curriculumId not found for "
								+ getCurriculumReq.getEntityName() + " with Id " + entityId + " & boardId = "+getCurriculumReq.getBoardId());
						continue;
					}
					nodes.add(tempNode);
					continue;
				}
				nodes.add(node);
			}
		}else{
			logger.info("Not getting any nodes for the user.");
		}

		if (ArrayUtils.isEmpty(nodes)) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "curriculumId not found for "
					+ getCurriculumReq.getEntityName() + " with Id " + entityIds);
		}

		logger.info("Role found to be "+role);
		if(role!=null && !Role.ADMIN.equals(role) && !Role.TEACHER.equals(role)){
			logger.info("Removing unshared curriculum & curriculum which are shared after it got inactive in some batch");
			if(getCurriculumReq.getBatchInactiveDate()==null){
				logger.info("As inactive date of batch is not set, setting the same for the user id "+userId);
				getCurriculumReq.setBatchInactiveDate(getInactiveDateEnrollmentMap(getCurriculumReq.getEntityIds(),userId));
			}
			logger.info("List of inactive batches are "+getCurriculumReq.getBatchInactiveDate());
			removeUnsharedContentsForDisplay(nodes,getCurriculumReq.getBatchInactiveDate());
			logger.info("After removing final list of nodes are "+new Gson().toJson(nodes));
		}


		return nodes;
	}

	public void printNodeDetailsForDebugPurpose(Node node,String space){
		if(node!=null){
			logger.info(space.concat(node.toString()));
			Optional.ofNullable(node.getNodes())
					.map(Collection::stream)
					.orElseGet(Stream::empty)
					.forEach(child->printNodeDetailsForDebugPurpose(child,space+" "));
		}
	}
	public List<Node> getCurriculumByUser(String userId) throws VException {
		Role role = sessionUtils.getCallingUserRole();
		logger.info("Get Curriculum By User ID - Role: " + role + " User Id: " + userId);
		String url = SUBSCRIPTION_ENDPOINT + "/enroll/getBatchInfoForUser?userId=" + userId;
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, new Gson().toJson(userId), true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String respString = resp.getEntity(String.class);

		List<String> entityIds = new ArrayList<>();
		List<Map> maps = new Gson().fromJson(respString, new TypeToken<List<Map>>(){}.getType());
		for (Map map : maps) {
			Object batchId = map.get("batchId");
			if (Objects.nonNull(batchId)) entityIds.add(batchId.toString());
		}
		List<Node> nodes = null;
		if (ArrayUtils.isNotEmpty(entityIds)) {
			nodes = getAllTreeNodesByBatch(entityIds);
		}

		if (ArrayUtils.isEmpty(nodes)) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "curriculumId not found for User Id " + userId);
		}
		return getNodesUpto(nodes, 2);
	}


	private List<Node> getNodesUpto(List<Node> nodes, int uptoLevel) {
		List<Node> list = new ArrayList<>();
		for (Node node : nodes) {
			int level = 0;
			if (node != null) {
				node = getNodesUpto(node, level, uptoLevel);
				list.add(node);
			}
		}
		return nodes;
	}


	private Map<String, Long> getInactiveDateEnrollmentMap(List<String> entityIds, Long userId) throws VException {
		logger.info("Calculating to show actual contents to user.");
		List<EnrollmentPojo> enrollments = getActiveAndInactiveEnrollmentsForUserId(Long.toString(userId));
		Map<String, Long> batchInactiveDate=new HashMap<>();
		logger.info("All batch ids are "+entityIds);
		enrollments.forEach(enrollment->{
			logger.info("Finding enrollment details for batch id "+enrollment.getBatchId());
			if(entityIds.contains(enrollment.getBatchId())  && EntityStatus.INACTIVE.equals(enrollment.getStatus())){
				logger.info("The enrollment details is "+enrollment);
				logger.info("Inactive batch id is "+enrollment.getBatchId());
				List<StatusChangeTime> statusChangeTime=enrollment.getStatusChangeTime();
				Collections.sort(statusChangeTime, Comparator.comparing(StatusChangeTime::getChangeTime));
				batchInactiveDate.put(enrollment.getBatchId(),statusChangeTime.get(statusChangeTime.size()-1).getChangeTime());
				logger.info("The batch got " +statusChangeTime.get(statusChangeTime.size()-1).getNewStatus().toString()+ " at "+statusChangeTime.get(statusChangeTime.size()-1).getChangeTime());
			}
		});
		return batchInactiveDate;
	}

	private void removeUnsharedContentsForDisplay(List<Node> nodes, Map<String,Long> batchInactiveDate) {
		if(nodes==null)return;
		for(Node node:nodes){
			List<Node> children=node.getNodes();
			removeUnsharedContentsForDisplay(children,batchInactiveDate);
			List<ContentInfo> contents=node.getContents();
			if(contents!=null){
				List<ContentInfo> contentsToRemove=new ArrayList<>();
				for(ContentInfo content:contents){
					if(content.getShareStatus()!=null && ShareStatus.UNSHARED.equals(content.getShareStatus())){
						contentsToRemove.add(content);
						continue;
					}
					if(batchInactiveDate!=null
							&& batchInactiveDate.containsKey(node.getContextId())
								&& content.getSharedTime()!=null
									&& content.getSharedTime()>batchInactiveDate.get(node.getContextId())){
						contentsToRemove.add(content);
						continue;
					}
					logger.info("The content won't be removed is "+content.getTitle());
				}
				contents.removeAll(contentsToRemove);
			}
		}
	}
        private Node getNodesUpto(Node node, int level, int uptoLevel) {
            while (level < uptoLevel) {
                level++;
                if (node != null) {
                    for (Node child : node.getNodes()) {
                        if (level < uptoLevel && child.getNodes() != null) {
                            return getNodesUpto(child, level, uptoLevel);
                        } else {
                            child.setNodes(null);
                        }
                    }
                }
            }
            return node;
        }
	public List<Node> getCurriculumBySessionId(GetCurriculumBySessionIdReq req) throws BadRequestException, NotFoundException {
            req.verify();
            List<Curriculum> curriculumList = curriculumDAO.getCurriculumBySessionId(req.getContextType(),req.getContextIds(),req.getSessionId(),req.getAfterLastUpdated(), req.getBeforeLastUpdated());
            List<Node> nodes=new ArrayList<>();
            if(ArrayUtils.isNotEmpty(curriculumList)){
                for (Curriculum iter : curriculumList) {
                    nodes.add(mapper.map(iter, Node.class));
                }
            }
            return nodes;
	}

	public Node editCourseCurriculum(AddCurriculumReq addCurriculumReq) throws VException{

		addCurriculumReq.verify();

		// Check if the title we want for insertion is already present or not
		if(addCurriculumReq.isCurriculumStructureChanged()) {
			checkIfCurriculumStructureTitleAlreadyPresent(addCurriculumReq.getTitle());
		}

		logger.info("editCourseCurriculum addCurriculumReq: "+addCurriculumReq);
		if (addCurriculumReq.getEntityName() == CurriculumEntityName.OTF_COURSE) {
			List<Curriculum> curriculumList = curriculumDAO.getCurriculumByCourseId(addCurriculumReq.getEntityName(),
					addCurriculumReq.getEntityId(),Arrays.asList(Curriculum.Constants._ID),null);
			if (CollectionUtils.isEmpty(curriculumList)) {
				throw new BadRequestException(ErrorCode.CURRICULUM_ID_NOT_FOUND,
						"curriculumId is not found for " + addCurriculumReq.getEntityId());
			}
			List<String> curriculumIds = new ArrayList<>();
			for (Curriculum curriculum : curriculumList) {
				curriculumIds.add(curriculum.getId());
			}
			curriculumDAO.changeEntityStateToDeleted(curriculumIds);
			return createCurriculum(addCurriculumReq);

		} else {
			throw new BadRequestException(ErrorCode.CURRICULUM_EDIT_NOT_ALLOWED,
					"curriculum course edit is not allowed");
		}
	}

	public Node createBatchCurriculum(CreateBatchCurriculumReq createBatchCurriculumReq)
			throws BadRequestException {
		logger.info("createBatchCurriculum: DEBUG START: "+createBatchCurriculumReq.hashCode());
		createBatchCurriculumReq.verify();

		List<Curriculum> curriculums = curriculumDAO.getCurriculumByCourseId(createBatchCurriculumReq.getEntityName(),
				createBatchCurriculumReq.getEntityId());
		logger.info("createBatchCurriculum -- curriculums: "+curriculums);
		if (ArrayUtils.isNotEmpty(curriculums)) {
			logger.info("Curriculum Already Exists for "+createBatchCurriculumReq.getEntityId());
			return null;
		}
		if (createBatchCurriculumReq.getParentEntityName() == CurriculumEntityName.OTF_COURSE) {
			Node course = getAllNodes(createBatchCurriculumReq.getParentEntityName(),
					createBatchCurriculumReq.getParentEntityId());

			if (course == null) {
				logger.info(ErrorCode.CURRICULUM_ID_NOT_FOUND + "curriculumId is not found");
				return null;
			}
			Curriculum root = new Curriculum(course);
			root.setContextType(createBatchCurriculumReq.getEntityName());
			root.setContextId(createBatchCurriculumReq.getEntityId());
			root.setParentCourseId(createBatchCurriculumReq.getParentEntityId());
			root.setCourseNodeId(course.getId());
			curriculumDAO.save(root, createBatchCurriculumReq.getCallingUserId());
			root.setRootId(root.getId());
			recurringAddNodes(course.getNodes(), root, createBatchCurriculumReq.getCallingUserId());

			// Adding batch ids corresponding to the course ids in curriculum template
			logger.info("createBatchCurriculum: DEBUG START -- "+ createBatchCurriculumReq.hashCode());
			logger.info("Updating curriculum template for the course with course id " +
					createBatchCurriculumReq.getParentEntityId()+" for the batch with batch id "+createBatchCurriculumReq.getEntityId());
			List<CurriculumTemplate> curriculumTemplatesForCourse=
					curriculumTemplateDAO.getCurriculumTemplateForCourse(createBatchCurriculumReq.getParentEntityId());

			logger.info("Retrieved curriculum templates are "+ curriculumTemplatesForCourse);
			if(ArrayUtils.isNotEmpty(curriculumTemplatesForCourse)){
				CourseDetails sharedCourseDetail=
						curriculumTemplatesForCourse.get(0).getSharedCourseDetails()
								.stream()
								.filter(courseDetails -> courseDetails.getCourseId().equals(createBatchCurriculumReq.getParentEntityId()))
								.findFirst()
								.get();

				logger.info("Data of shared course details are "+ sharedCourseDetail);
				if(sharedCourseDetail != null){
					logger.info("As shared course details are not null so going to update the same for all the batches present.");
					Set<String> existingBatchIds = sharedCourseDetail.getBatchIds();
					existingBatchIds.add(createBatchCurriculumReq.getEntityId());
					sharedCourseDetail.setBatchIds(existingBatchIds);
					logger.info("existingBatchIds: "+existingBatchIds);
					logger.info("sharedCourseDetail: "+sharedCourseDetail);
					curriculumTemplatesForCourse
							.parallelStream()
							.map(template -> template.insertCourseDetail(sharedCourseDetail))
							.forEach(template -> curriculumTemplateDAO.saveEntity(template));

				}
			}
			logger.info("createBatchCurriculum: DEBUG END -- "+ createBatchCurriculumReq.hashCode());
			logger.info("createBatchCurriculum: DEBUG END: "+createBatchCurriculumReq.hashCode());
			
			return getAllNodes(createBatchCurriculumReq.getEntityName(), createBatchCurriculumReq.getEntityId());

		} else {
			logger.info(ErrorCode.CURRICULUM_BATCH_PARENT_NOT_COURSE + " curriculum course batch parent is not course");
		}



		return null;
	}

	public Node changeCurriculumStatus(ChangeCurriculumStatusReq req) throws VException, MalformedURLException {
		req.verify();
		Curriculum curriculum = curriculumDAO.getCurriculumById(req.getNodeId());
		if (curriculum == null) {
			throw new BadRequestException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "curriculumId is not found");
		}else if(curriculum.getContextType() != null && curriculum.getContextType().equals(CurriculumEntityName.OTF_COURSE)){
			throw new BadRequestException(ErrorCode.CURRICULUM_EDIT_NOT_ALLOWED, "curriculumId is for course");
		}
		if (!curriculum.getStatus().equals(req.getNewStatus())) {
			if (req.getNewStatus().equals(CurriculumStatus.INPROGRESS) && curriculum.getStartTime() == null) {
				curriculum.setStartTime(System.currentTimeMillis());
			}
			if (ArrayUtils.isEmpty(curriculum.getStatusChangeTime())) {
				curriculum.setStatusChangeTime(new ArrayList<>());
			}
			CurriculumStatusChangeTime statusChangeTime = new CurriculumStatusChangeTime();
			statusChangeTime.setChangedBy(req.getCallingUserId());
			statusChangeTime.setChangeTime(System.currentTimeMillis());
			statusChangeTime.setNewStatus(req.getNewStatus());
			statusChangeTime.setPreviousStatus(curriculum.getStatus());
			curriculum.getStatusChangeTime().add(statusChangeTime);
			curriculum.setStatus(req.getNewStatus());
		}
		// TODO : for pending update it accordingly
		if (ArrayUtils.isNotEmpty(req.getSessionIds())) {
			List<String> sessionIds = new ArrayList<>();
			if (curriculum.getSessionIds() == null) {
				curriculum.setSessionIds(new HashSet<String>());
			}
			for (String sessionId : req.getSessionIds()) {
				if (StringUtils.isNotEmpty(sessionId)) {
					sessionIds.add(sessionId);
				}
			}
			if (ArrayUtils.isNotEmpty(sessionIds)) {
				curriculum.getSessionIds().addAll(sessionIds);
			}

		}
		List<ContentInfo> testContents=new ArrayList<>();
		List<ContentInfo> assignmentToShare=new ArrayList<>();
		if(CurriculumStatus.DONE.equals(curriculum.getStatus())){
			List<ContentInfo> contents=curriculum.getContents();
			logger.info("Beginning of making content shared");
			if(ArrayUtils.isNotEmpty(contents)){
				for(ContentInfo content:contents){
					logger.info("contentShareType - {}, contentShareStatus - {}",content.getShareType(),content.getShareStatus());
					if(content.getShareType()!=null && ShareType.SHARE_ON_CURRICULUM_MARK_DONE.equals(content.getShareType())){
						if(content.getShareStatus()==null || ShareStatus.UNSHARED.equals(content.getShareStatus())){
							logger.info("Making content "+content.getTitle()+"as shared");
							content.setShareStatus(ShareStatus.SHARED);
							content.setSharedBy(Long.toString(req.getCallingUserId()));
							content.setSharedTime(System.currentTimeMillis());
							if(ContentType.TEST.equals(content.getContentType())){
								testContents.add(content);
							}else if(ContentType.ASSIGNMENT.equals(content.getContentType()) || ContentType.ASSIGNMENT_PDF.equals(content.getContentType())){
								assignmentToShare.add(content);							}
						}
					}
				}
			}
			logger.info("Ending of making content shared");

		}
		logger.info("So saving curriculum status as "+curriculum.getStatus());
		curriculumDAO.save(curriculum, req.getCallingUserId());
		// Added this code so as to preserve the previous data
		if(!testContents.isEmpty()){
			logger.info("Sharing tests ...");
			try {
				shareContentsToStudent(testContents, curriculum, req.getCallingUserId(), req.getCallingUserRole(), false, SQSMessageType.SHARE_TEST_TASK);
			} catch (Exception e) {
				logger.error("Couldn't share content to batch which doesn't have any batch. {}", curriculum.getId());
			}
		}else{
			logger.info("No tests to share");
		}

		if (CurriculumEntityName.OTF_BATCH.equals(curriculum.getContextType())) {
			awsSNSManager.triggerSNS(SNSTopic.BATCH_CURRICULUM_UPDATE, SNSSubject.REMOVE_BATCH_CURRICULUM_PROGRESS_KEYS.name(),
					gson.toJson(Collections.singletonList(curriculum.getContextId())));
		}

		if(!assignmentToShare.isEmpty()){
			logger.info("Sharing assignments ...");

//		if(!assignmentToShare.isEmpty()){
//			sendEmailAndSMSToShareContent(curriculum,assignmentToShare);
			try {
				shareContentsToStudent(assignmentToShare, curriculum, req.getCallingUserId(), req.getCallingUserRole(), false, SQSMessageType.SHARE_ASSIGNMENT_TASK);
			} catch (Exception e) {
				logger.error("Couldn't share content to batch which doesn't have any batch. {}", curriculum.getId());
			}
		}else{
			logger.info("No assignments to share...");
		}
		Node node = mapper.map(curriculum, Node.class);
		return node;

	}

	private void shareContentsToStudent(List<ContentInfo> contentInfos, Curriculum curriculum, Long callingUserId, Role callingUserRole, boolean newContent, SQSMessageType messageType) throws VException {
		logger.info("Start contents share - {}", messageType);
		if (CurriculumEntityName.OTF_BATCH != curriculum.getContextType() || StringUtils.isEmpty(curriculum.getContextId())) {
			throw new BadRequestException(ErrorCode.STUDENTS_NOT_FOUND_TO_SHARE_CONTENT, "Only otf batches allowed",Level.ERROR);
		}
		if (ArrayUtils.isEmpty(contentInfos)) {
			return;
		}
		logger.info("Total content infos to share is {}",contentInfos.size());
		List<String> studentIds;
		int start = 0, limit = 1000;
		int iterationNo = 0;
		do {

			String url = SUBSCRIPTION_ENDPOINT + "enroll/getEnrolledStudentsInBatch?batchId=" + curriculum.getContextId() + "&start=" + start + "&size=" + limit;
			logger.info("URL = {}", url);
			ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(response);
			String jsonString = response.getEntity(String.class);
			studentIds = gson.fromJson(jsonString, new TypeToken<List<String>>() {

			}.getType());
			start += limit;

			List<List<String>> studentIdPartitionLists = Lists.partition(studentIds, 200);

			for (List<String> studentIdBatch : studentIdPartitionLists) {
				++iterationNo;
				for (ContentInfo contentInfo : contentInfos) {
					try {
						List<Long> students = studentIdBatch.stream().map(Long::valueOf).collect(Collectors.toList());

						ShareCurriculumContentReq contentShareReq=new ShareCurriculumContentReq();
						contentShareReq.setStudentIds(students);
						contentShareReq.setNewContent(newContent);
						contentShareReq.setContents(Collections.singletonList(contentInfo));
						contentShareReq.setContextId(curriculum.getContextId());
						contentShareReq.setNodeId(curriculum.getId());
						contentShareReq.setContextType(curriculum.getContextType());
						contentShareReq.setCallingUserId(callingUserId);
						contentShareReq.setCallingUserRole(callingUserRole);
						contentShareReq.setIterationNo(iterationNo);
						String json = gson.toJson(contentShareReq);
						logger.info("Details of the request are {}", json);
						if(iterationNo==1) {
							logger.info("Direct call for first iteration for curriculum id {}",curriculum.getId());
							shareContentForCurriculum(contentShareReq, contentShareReq.isNewContent());
						}else {
							logger.info("Iteration no for curriculum id {} is {}",curriculum.getId(),iterationNo);
							awsSQSManager.sendToSQS(messageType.getQueue(), messageType, json);
						}
					} catch (Exception e) {
						logger.error(e.getMessage(), e);
					}
				}
			}
		} while (ArrayUtils.isNotEmpty(studentIds));

		logger.info("End contents share");
	}

	public BatchBasicInfo batchBasicInfo(String id) throws VException {
		String url = SUBSCRIPTION_ENDPOINT + "batch/basicInfo/" + id;

		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		logger.info(jsonString);
		BatchBasicInfo batchBasicInfo = new Gson().fromJson(jsonString,
				BatchBasicInfo.class);
		return batchBasicInfo;
	}

	public ShareContentAndSendMailRes shareContentForCurriculum(ShareCurriculumContentReq req) throws MalformedURLException, VException {
		return shareContentForCurriculum(req,false);
	}

        public PlatformBasicResponse shareContentForCurriculumForBatch(ShareCurriculumContentReq req) throws MalformedURLException, VException {
            if (req.getContextType().equals(CurriculumEntityName.OTF_BATCH)) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("shareCurriculumContentReq", req);
                AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.SHARE_CONTENT_CURRICULUM_FOR_BATCH, payload);
                asyncTaskFactory.executeTask(asyncTaskParams);
            } else {
                throw new ForbiddenException(ErrorCode.ACCESS_NOT_ALLOWED, "Only allowed to share content in a batch");
            }
            return new PlatformBasicResponse();
        }
        
	public PlatformBasicResponse shareContentForCurriculumForBatchAsync(ShareCurriculumContentReq contentShareReq) {

            logger.info("Start test share");
            try {
                if (CurriculumEntityName.OTF_BATCH != contentShareReq.getContextType() || StringUtils.isEmpty(contentShareReq.getContextId())) {
                    throw new BadRequestException(ErrorCode.STUDENTS_NOT_FOUND_TO_SHARE_CONTENT, "Only otf batches allowed", Level.ERROR);
                }
                List<String> studentIds;
                int start = 0, limit = 1000;
                int iterationNo=0;
                do {

                    String url = SUBSCRIPTION_ENDPOINT + "enroll/getEnrolledStudentsInBatch?batchId=" + contentShareReq.getContextId() + "&start=" + start + "&size=" + limit;
                    logger.info("URL = " + url);
                    ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                    VExceptionFactory.INSTANCE.parseAndThrowException(response);
                    String jsonString = response.getEntity(String.class);
                    studentIds = gson.fromJson(jsonString, new TypeToken<List<String>>() {

                    }.getType());
                    start += limit;

                    List<List<String>> lists = Lists.partition(studentIds, 200);
                    SQSMessageType messageType = SQSMessageType.SHARE_TEST_TASK;
                    for (List<String> list : lists) {
						++iterationNo;
						ShareCurriculumContentReq shareCurriculumContentReq=new ShareCurriculumContentReq(contentShareReq);
						shareCurriculumContentReq.setStudentIds(list.stream().map(Long::valueOf).collect(Collectors.toList()));
						shareCurriculumContentReq.setIterationNo(iterationNo);
                        String json = gson.toJson(shareCurriculumContentReq);
                        logger.info("Details of the request are " + json);
						if(iterationNo==1) {
							logger.info("Direct call for first iteration for shareContentForCurriculumForBatchAsync");
							shareContentForCurriculum(shareCurriculumContentReq, shareCurriculumContentReq.isNewContent());
						}else {
							logger.info("Iteration no for shareContentForCurriculumForBatchAsync is {}",iterationNo);
							awsSQSManager.sendToSQS(messageType.getQueue(), messageType, json);
						}
                    }
                } while (ArrayUtils.isNotEmpty(studentIds));

            } catch (Exception e) {
                logger.error("exception in shareContentForCurriculumForBatch ", e);
            }

            logger.info("End test share");

            return new PlatformBasicResponse();
	}
        
        
        

	public ShareContentAndSendMailRes shareContentForCurriculum(ShareCurriculumContentReq req,boolean newContent)
			throws VException, MalformedURLException {
		req.verify();
		if (ArrayUtils.isEmpty(req.getStudentIds())) {
			throw new BadRequestException(ErrorCode.STUDENTS_NOT_FOUND_TO_SHARE_CONTENT, "StudentIds are null");
		}
		logger.info("Students are found to send test");
		Curriculum curriculum = curriculumDAO.getCurriculumById(req.getNodeId());
		ShareContentAndSendMailRes response = null;
		if (curriculum == null || curriculum.getEntityState().equals(EntityState.DELETED)) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Curriculum does not exist");
		} else if (curriculum.getContextType() != null
				&& curriculum.getContextType().equals(CurriculumEntityName.OTF_COURSE)) {
			throw new BadRequestException(ErrorCode.CURRICULUM_EDIT_NOT_ALLOWED,
					"curriculumentity is course not batch");
		}
		logger.info("Curriculum is good to send");
		EntityType contextType;
		if (curriculum.getContextType() != null && curriculum.getContextType().equals(CurriculumEntityName.OTF_BATCH)) {
			contextType = EntityType.OTF;
		} else {
			contextType = EntityType.COURSE_PLAN;
		}
		logger.info("The entity type found to be "+contextType.toString());
		Map<String, ContentInfo> mapContentId = new HashMap<>();
		List<ContentInfo> otherContents=new ArrayList<>();
		if (ArrayUtils.isNotEmpty(req.getContents())) {
			for (ContentInfo contentInfo : req.getContents()) {
				logger.info("The content id is found to be {} and url is {}", contentInfo.getContentId(), contentInfo.getUrl());
				if (contentInfo.getContentId()!=null && StringUtils.isNotEmpty(contentInfo.getContentId())) {
					mapContentId.put(contentInfo.getContentId(), contentInfo);
				} else if (contentInfo.getUrl()!=null && StringUtils.isNotEmpty(contentInfo.getUrl())) {
					mapContentId.put(contentInfo.getUrl(), contentInfo);
				}else{
					otherContents.add(contentInfo);
				}
			}
		}else {
			logger.info("No content is there is request to share.");
		}
		logger.info("No of items mapped are "+mapContentId.size());
		logger.info("No of non-test items to share is "+otherContents.size());

		List<ContentInfo> contentInfos = new ArrayList<>();
		if (ArrayUtils.isEmpty(curriculum.getContents()) && !newContent) {
			throw new NotFoundException(ErrorCode.CONTENT_NOT_FOUND_CURRICULUM,
					"Content for Curriculum does not exist");
		}
		logger.info("Contents found for curriculum, yet to be decided to share");
		List<ContentInfo> contentInfoToShare;
		if(newContent){
			contentInfoToShare=req.getContents();
			contentInfos=curriculum.getContents();
		}else{
			contentInfoToShare=curriculum.getContents();
		}
		Map<String,List<String>> content_contentInfoId=new HashMap<>();
		List<ContentInfo> assignmentToShare=new ArrayList<>();
		for (ContentInfo contentInfo : contentInfoToShare) {
			logger.info("share content info - id {}, url {}", contentInfo.getContentId(), contentInfo.getUrl());
			if (contentInfo.getContentId() != null || StringUtils.isNotEmpty(contentInfo.getUrl())) {
				boolean containsId = mapContentId.containsKey(contentInfo.getContentId());
				boolean containsUrl = mapContentId.containsKey(contentInfo.getUrl());
				logger.info("content info in map - containsId {}, containsUrl {}", containsId, containsUrl);
				if ((contentInfo.getContentId() != null || contentInfo.getUrl() != null) &&
						(containsId || containsUrl)) {
					logger.info("Preparing to share content "+contentInfo.getTitle());

					if(contentInfo.getShareStatus()==null || ShareStatus.UNSHARED.equals(contentInfo.getShareStatus())){
						logger.info("The content to be shared is "+contentInfo.getTitle());
						contentInfo.setShareStatus(ShareStatus.SHARED);
						contentInfo.setSharedBy(Long.toString(req.getCallingUserId()));
						contentInfo.setSharedTime(System.currentTimeMillis());
						if(ContentType.ASSIGNMENT.equals(contentInfo.getContentType()) || ContentType.ASSIGNMENT_PDF.equals(contentInfo.getContentType())){
							assignmentToShare.add(contentInfo);
						}
					}
					ContentInfo requestContentInfo = null;
					if (containsId) {
						requestContentInfo = mapContentId.get(contentInfo.getContentId());
					} else if (containsUrl) {
						requestContentInfo = mapContentId.get(contentInfo.getUrl());
					}
					if(ArrayUtils.isNotEmpty(contentInfo.getContentInfoId())){
						logger.info("Already once the content is shared, doing for the same again as we are using queues.");
					}

					if (requestContentInfo.getTeacherId() != null) {
						contentInfo.setTeacherId(requestContentInfo.getTeacherId());
					} else if (contentInfo.getTeacherId() != null) {
						contentInfo.setTeacherId(contentInfo.getTeacherId());
					} else {
						contentInfo.setTeacherId(req.getCallingUserId());
					}
					logger.info("The content yet to be shared is "+contentInfo.getTitle());
					ShareContentReq shareContentReq = new ShareContentReq(contentInfo.getTitle(),
							contentInfo.getTeacherId(), contentInfo.getDescription(), contentInfo.getContentType(),
							contentInfo.getUrl(), contentInfo.getContentId(), req.getStudentIds(), req.getCourseName(),
							contextType, curriculum.getContextId(), curriculum.getBoardId(), contentInfo.getTopicNames(),
							contentInfo.getContentSubType());
					if (req.getTeacherName() != null) {
						shareContentReq.setTeacherName(req.getTeacherName());
					}
					response = contentInfoManager.shareContentByTeacherAndSendMail(shareContentReq);
					logger.info("Content is shared to the student");
					if(response != null && ArrayUtils.isNotEmpty(response.getContentInfoIds())){
						contentInfo.setContentInfoId(response.getContentInfoIds());
						content_contentInfoId.put(contentInfo.getContentId(),contentInfo.getContentInfoId());
						logger.info("Received contentInfoIds are "+contentInfo.getContentInfoId());
					}
					logger.info("Logging the response of contentInfoId if found");
					if (StringUtils.isNotEmpty(req.getCurrentSessionId())) {
						if (ArrayUtils.isEmpty(contentInfo.getSessionIds())) {
							contentInfo.setSessionIds(new HashSet<>());
						}
						contentInfo.getSessionIds().add(req.getCurrentSessionId());
					}
					if (containsId) {
						mapContentId.remove(contentInfo.getContentId());
					}
				}else{
					for(ContentInfo otherContent:otherContents){
						if(contentInfo.getTitle().equals(otherContent.getTitle()) &&
							contentInfo.getContentType().equals(otherContent.getContentType()) &&
							contentInfo.getUrl().equals(otherContent.getUrl()) &&
						 	contentInfo.getShareType().equals(otherContent.getShareType()) &&
							contentInfo.getShareStatus().equals(otherContent.getShareStatus())){
							contentInfo.setShareStatus(ShareStatus.SHARED);
							contentInfo.setSharedBy(Long.toString(req.getCallingUserId()));
							contentInfo.setSharedTime(System.currentTimeMillis());
							logger.info("Changing status of "+contentInfo.getTitle()+
									"to"+contentInfo.getShareStatus());
							if(ContentType.ASSIGNMENT_PDF.equals(contentInfo.getContentType()) || ContentType.ASSIGNMENT.equals(contentInfo.getContentType())){
								assignmentToShare.add(contentInfo);
							}
						}
					}
				}
			}
			logger.info("Adding content"+contentInfo.getTitle()+" with contentInfoId "+contentInfo.getContentInfoId());
			contentInfos.add(contentInfo);
		}
		if (ArrayUtils.isNotEmpty(contentInfos)) {
			curriculum.setContents(contentInfos);
		}
//		if(!ArrayUtils.isEmpty(assignmentToShare)){
//			sendEmailAndSMSToShareContent(curriculum,assignmentToShare);
//		}
		logger.info("The curriculum saved is "+curriculum.getId());
		if(req.getIterationNo()==1) {
			logger.info("Saving for iteration no 1");
			curriculumDAO.save(curriculum, req.getCallingUserId());
		}else{
			logger.info("Not saving as it's not first iteration.");
		}
		Node resp = mapper.map(curriculum, Node.class);
		if(response != null){
			response.setNode(resp);
			return response;
		}else {
			response = new ShareContentAndSendMailRes();
			response.setNode(resp);
			return response;
		}
	}

	public Node changeAnyCurriculum(AddCurriculumReq addCurriculumReq) throws NotFoundException, BadRequestException {
		addCurriculumReq.verify();
		List<String> errors = validateContentIds(addCurriculumReq.getNode());
		if(ArrayUtils.isNotEmpty(errors)){
			throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID,errors.toString());
		}
		List<Curriculum> curriculums = curriculumDAO.getCurriculumByCourseId(addCurriculumReq.getEntityName(),
				addCurriculumReq.getEntityId());
		if (CollectionUtils.isNotEmpty(curriculums)) {
			List<String> curriculumIds = new ArrayList<>();
			for (Curriculum curriculum : curriculums) {
				curriculumIds.add(curriculum.getId());
			}
			curriculumDAO.changeEntityStateToDeleted(curriculumIds);
		}
		if (addCurriculumReq.getNode() != null) {
			Node node = addCurriculumReq.getNode();

			Curriculum curriculum = new Curriculum(node);
			curriculum.setContextType(addCurriculumReq.getEntityName());
			curriculum.setContextId(addCurriculumReq.getEntityId());

			curriculumDAO.save(curriculum, addCurriculumReq.getCallingUserId());
			curriculum.setRootId(curriculum.getId());
			recurringAddNodes(node.getNodes(), curriculum, addCurriculumReq.getCallingUserId());
		} else {
			throw new BadRequestException(ErrorCode.CURRICULUM_NODE_NOT_FOUND, "Curriculum Node is Null");
		}

		return getAllNodes(addCurriculumReq.getEntityName(), addCurriculumReq.getEntityId());

	}

	public Node addNodeInCurriculum(AddCurriculumReq addCurriculumReq) throws BadRequestException, NotFoundException {
		if (addCurriculumReq.getNode() == null || StringUtils.isEmpty(addCurriculumReq.getNode().getParentId())) {
			throw new BadRequestException(ErrorCode.CURRICULUM_NODE_NOT_FOUND,
					"request does not contain Node or Node ParentId");
		}

		Query query = new Query();
		query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		Criteria parentId = Criteria.where(Curriculum.Constants.PARENT_ID).is(addCurriculumReq.getNode().getParentId());
		Criteria rootId = Criteria.where(Curriculum.Constants._ID).is(addCurriculumReq.getNode().getParentId());

		Criteria criteria = new Criteria();
		criteria.orOperator(parentId, rootId);
		query.addCriteria(criteria);
		query.with(Sort.by(Sort.Direction.DESC, Curriculum.Constants.CHILD_ORDER));

		List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
		Curriculum newCurrciulum = null;
		if (ArrayUtils.isNotEmpty(curriculums)) {
			Curriculum parent = null;
			int index = 0;
			for (Curriculum curriculum : curriculums) {
				if (curriculum.getId().equals(addCurriculumReq.getNode().getParentId())) {
					parent = curriculum;
					break;
				}
			}

			if (addCurriculumReq.getNode().getChildOrder() == null) {
				newCurrciulum = new Curriculum(addCurriculumReq.getNode());
				newCurrciulum.setContextType(parent.getContextType());
				newCurrciulum.setContextId(parent.getContextId());
				if (!curriculums.get(0).getId().equals(parent.getId())) {
					newCurrciulum.setChildOrder(curriculums.get(0).getChildOrder() + 1);
				} else if (curriculums.get(1) != null) {
					newCurrciulum.setChildOrder(curriculums.get(1).getChildOrder() + 1);
				} else {
					newCurrciulum.setChildOrder(0);
				}

				if(StringUtils.isNotEmpty(parent.getRootId())){
					newCurrciulum.setRootId(parent.getRootId());
				}else {
					newCurrciulum.setRootId(parent.getId());
				}
				newCurrciulum.setRootId(parent.getRootId());
				if (newCurrciulum.getBoardId() == null) {
					newCurrciulum.setBoardId(parent.getBoardId());
				}
				curriculumDAO.save(newCurrciulum, addCurriculumReq.getCallingUserId());
			} else {
				List<String> curriculumIds = new ArrayList<>();
				for (Curriculum curriculum : curriculums) {
					if (!curriculum.getId().equals(parent.getId())
							&& curriculum.getChildOrder() > addCurriculumReq.getNode().getChildOrder()) {
						curriculumIds.add(curriculum.getId());
					}
				}
				curriculumDAO.incrementChildOrders(curriculumIds);
				newCurrciulum = new Curriculum(addCurriculumReq.getNode());
				newCurrciulum.setContextType(parent.getContextType());
				newCurrciulum.setContextId(parent.getContextId());
				newCurrciulum.setRootId(parent.getRootId());
				if (newCurrciulum.getBoardId() == null) {
					newCurrciulum.setBoardId(parent.getBoardId());
				}
				curriculumDAO.save(newCurrciulum, addCurriculumReq.getCallingUserId());

			}

			if (CollectionUtils.isNotEmpty(addCurriculumReq.getBatchIds())) {
				logger.info("deblog-- delete node in branch {}", addCurriculumReq.getBatchIds());
				awsSNSManager.triggerSNS(SNSTopic.BATCH_CURRICULUM_UPDATE, SNSSubject.REMOVE_BATCH_CURRICULUM_PROGRESS_KEYS.name(),
						gson.toJson(addCurriculumReq.getBatchIds()));
			} else if (addCurriculumReq.getNode().getContextType().equals(CurriculumEntityName.OTF_BATCH)) {
				awsSNSManager.triggerSNS(SNSTopic.BATCH_CURRICULUM_UPDATE, SNSSubject.REMOVE_BATCH_CURRICULUM_PROGRESS_KEYS.name(),
						gson.toJson(Collections.singletonList(addCurriculumReq.getNode().getContextId())));
			}


			return mapper.map(newCurrciulum, Node.class);

		} else {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Parent Id Not Found");
		}
	}

	public Node deleteNodeInCurriculum(AddOrDeleteNodeReq req) throws BadRequestException, NotFoundException {
		req.verify();
		Node completeNode = getAllNodes(req.getNode().getContextType(), req.getNode().getContextId());
		if (completeNode == null) {
			throw new NotFoundException(ErrorCode.CURRICULUM_NODE_NOT_FOUND, "Could Not Find Node for given Node");
		}
		Node deleteNode = null;
		if (completeNode.getId().equals(req.getNode().getId())) {
			deleteNode = completeNode;
		} else if (ArrayUtils.isNotEmpty(completeNode.getNodes())) {
			deleteNode = findSubTreeForNodeId(completeNode.getNodes(), req.getNode().getId());
		}
		if (deleteNode == null) {
			throw new NotFoundException(ErrorCode.CURRICULUM_NODE_NOT_FOUND, "Could Not Find Node for given Node");
		}
		List<String> deleteNodeIds = new ArrayList<>();
		getAllNodeIdsInTree(deleteNode, deleteNodeIds);
		if (ArrayUtils.isNotEmpty(deleteNodeIds)) {
			curriculumDAO.changeEntityStateToDeleted(deleteNodeIds);
		}

		if (CollectionUtils.isNotEmpty(req.getBatchIds())) {
			logger.info("deblog-- delete node in branch {}", req.getBatchIds());
			awsSNSManager.triggerSNS(SNSTopic.BATCH_CURRICULUM_UPDATE, SNSSubject.REMOVE_BATCH_CURRICULUM_PROGRESS_KEYS.name(),
					gson.toJson(req.getBatchIds()));
		} else if (req.getNode().getContextType().equals(CurriculumEntityName.OTF_BATCH)) {
			awsSNSManager.triggerSNS(SNSTopic.BATCH_CURRICULUM_UPDATE, SNSSubject.REMOVE_BATCH_CURRICULUM_PROGRESS_KEYS.name(),
					gson.toJson(Collections.singletonList(req.getNode().getContextId())));
		}

		return getAllNodes(req.getNode().getContextType(), req.getNode().getContextId());
	}

	public Node addContentToNode(AddContentCurriculumReq req) throws VException {

		logger.info("Adding content for node id {}",req.getNodeId());
		req.verify();
		Node node;
		if (ArrayUtils.isEmpty(req.getContents())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"No content to be added in Req");
		}
		Curriculum curriculum = curriculumDAO.getCurriculumById(req.getNodeId());
		if (curriculum == null) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Node Id Not Found");
		}
		List<String> errors = validateContentIds(curriculum.getBoardId(),req.getContents());
		if(ArrayUtils.isNotEmpty(errors)){
			throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID,errors.toString());
		}
		if (ArrayUtils.isEmpty(curriculum.getContents())) {
			curriculum.setContents(new ArrayList<>());
		}
		List<ContentInfo> contents = curriculum.getContents();
		List<ContentInfo> testContents=new ArrayList<>();
		List<ContentInfo> shareContents=req.getContents();
		List<ContentInfo> assignmentToShare=new ArrayList<>();
		for(ContentInfo content:req.getContents()){
			if(CurriculumStatus.DONE.equals(curriculum.getStatus())){
				if(ShareType.SHARE_ON_CURRICULUM_MARK_DONE.equals(content.getShareType())){
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"You can't share automatically once it is done", Level.ERROR);
				}
			}
			if(ShareType.SHARE_ON_CONTENT_SAVE.equals(content.getShareType())){
				logger.info("Sharing the message as it is instant");
				if(ContentType.TEST.equals(content.getContentType())){
					logger.info("Sharing the test as it has a different flow");
					testContents.add(content);
				}else if(ContentType.ASSIGNMENT_PDF.equals(content.getContentType()) ||
						ContentType.ASSIGNMENT.equals(content.getContentType())){
					content.setShareStatus(ShareStatus.SHARED);
					assignmentToShare.add(content);
				}else{
					content.setShareStatus(ShareStatus.SHARED);
				}
				content.setSharedBy(Long.toString(req.getCallingUserId()));
				content.setSharedTime(System.currentTimeMillis());
			}
			content.setCreatedBy(Long.toString(req.getCallingUserId()));
			content.setCreationTime(System.currentTimeMillis());
		}
		// Send email in case of test or assignment
		if(!testContents.isEmpty()){
			if(shareContents.removeAll(testContents)){
				logger.info(testContents.size()+" contents are removed and remaining contents are "+shareContents.size());
			}
		}

		if(!assignmentToShare.isEmpty()){
			if(shareContents.removeAll(assignmentToShare)){
				logger.info(assignmentToShare.size()+" contents are removed and remaining contents are "+shareContents.size());
			}
		}

		// when new content flag is true these are saved again when contents are shared. check {@link shareContentsToStudent}
		// so duplicates will be created. so we are removing here
		if(!assignmentToShare.isEmpty()){
			if(shareContents.removeAll(assignmentToShare)){
				logger.info(""+assignmentToShare.size()+" contents are removed and remaining contents are "+shareContents.size());
			}
		}
		logger.info("Share content has the size {}",shareContents.size());
		logger.info("Contents has the size {}",contents.size());
		contents.addAll(shareContents);
		curriculumDAO.save(curriculum, req.getCallingUserId());
		// Added this code so as to preserve the previous data
		if(!testContents.isEmpty()){
			logger.info("Sharing test...");
			Curriculum finalCurriculum = curriculum;
			try {
				shareContentsToStudent(testContents, finalCurriculum,req.getCallingUserId(),req.getCallingUserRole(),true, SQSMessageType.SHARE_TEST_TASK);
			} catch (Exception e){
				logger.error(e.getMessage(), e);
			}
		}else{
			logger.info("Not sharing any test");
		}
		if(!assignmentToShare.isEmpty()){
			logger.info("Sharing assignment...");
//			sendEmailAndSMSToShareContent(curriculum,assignmentToShare);
			try {
				shareContentsToStudent(assignmentToShare, curriculum,req.getCallingUserId(),req.getCallingUserRole(),true, SQSMessageType.SHARE_ASSIGNMENT_TASK);
			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}else{
			logger.info("Not sharing any assignment");
		}
		node = mapper.map(curriculum, Node.class);
		return node;
	}

	private void sendEmailAndSMSToShareContent(Curriculum curriculum, List<ContentInfo> contents) throws VException {
		if (CurriculumEntityName.OTF_BATCH != curriculum.getContextType() || StringUtils.isEmpty(curriculum.getContextId())) {
			throw new BadRequestException(ErrorCode.STUDENTS_NOT_FOUND_TO_SHARE_CONTENT, "Only otf batches allowed",Level.ERROR);
		}

		List<String> studentIds;
		int start = 0, limit = 1000;
		do {

			String url = SUBSCRIPTION_ENDPOINT + "enroll/getEnrolledStudentsInBatch?batchId=" + curriculum.getContextId() + "&start=" + start + "&size=" + limit;
			logger.info("URL = " + url);
			ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
			VExceptionFactory.INSTANCE.parseAndThrowException(response);
			String jsonString = response.getEntity(String.class);
			studentIds = gson.fromJson(jsonString, new TypeToken<List<String>>() {

			}.getType());
			start += limit;

			List<String> userIds = studentIds.stream().map(String::valueOf).collect(Collectors.toList());
			if(!ArrayUtils.isEmpty(userIds)){
				List<List<String>> lists = Lists.partition(userIds, 100);
				BatchBasicInfo batchBasicInfo=batchBasicInfo(curriculum.getContextId());
				Map<String, Object> payload = new HashMap<>();
				payload.put("batchBasicInfo", batchBasicInfo);
				payload.put("contents",contents);
				SQSMessageType task = SQSMessageType.SHARE_ASSIGNMENT_TASK;
				for (List<String> list : lists) {
					payload.put("userIds",list);
					awsSQSManager.sendToSQS(task.getQueue(), task, gson.toJson(payload));
				}
			}
		} while (ArrayUtils.isNotEmpty(studentIds));
	}

	public void shareAssignmentFor(BatchBasicInfo batchBasicInfo, List<String> userIds,
								   List<ContentInfo> contents) throws VException, UnsupportedEncodingException {
            /*
		logger.info("Batch info "+ batchBasicInfo);
		logger.info("Sharing Assignments For " + userIds);
		logger.info("Content " + contents);
		List<UserBasicInfo> users=fosUtils.getUserBasicInfos(userIds,true);

		// Details about email
		EmailRequest emailRequest = new EmailRequest();
		emailRequest.setType(CommunicationType.ASSIGNMENT_SHARE);
		emailRequest.setClickTrackersEnabled(true);
		emailRequest.setIncludeHeaderFooter(false);

		// Details about SMS
		TextSMSRequest textSMSRequest = new TextSMSRequest();
		textSMSRequest.setType(CommunicationType.ASSIGNMENT_SHARE);

		// Sending Email and SMS
		for (UserBasicInfo userBasicInfo : users) {// Populating details about email
			InternetAddress internetAddress = new InternetAddress();
			internetAddress.setAddress(userBasicInfo.getEmail());
			internetAddress.setPersonal(userBasicInfo.getFullName());
			List<InternetAddress> emailTo = new ArrayList<>();
			emailTo.add(internetAddress);
			Map<String, Object> bodyScopes = new HashMap<>();
			Map<String, Object> subjectScopes = new HashMap<>();
			emailRequest.setTo(emailTo);

			// Populating details about SMS
			textSMSRequest.setTo(userBasicInfo.getContactNumber());
			textSMSRequest.setPhoneCode(userBasicInfo.getPhoneCode());
			textSMSRequest.setRole(Role.STUDENT);

			for (ContentInfo content : contents) {
				// Populating body scopes for SMS
				bodyScopes.put("UserName", userBasicInfo.getFirstName());
				bodyScopes.put("Assignment Title", content.getTitle());
				bodyScopes.put("Course title", batchBasicInfo.getCourseInfo().getTitle());
				textSMSRequest.setScopeParams(bodyScopes);
				communicationManager.sendSMS(textSMSRequest);

				if (org.apache.commons.lang3.StringUtils.isNotEmpty(userBasicInfo.getEmail())) {
					// Populating body scopes for Email
					bodyScopes.put("link", content.getUrl());
					emailRequest.setBodyScopes(bodyScopes);

					// Populating subject scopes for Email
					subjectScopes.put("Assignment Title", content.getTitle());
					subjectScopes.put("Course title", batchBasicInfo.getCourseInfo().getTitle());
					emailRequest.setSubjectScopes(subjectScopes);
					communicationManager.sendEmail(emailRequest);
				}
			}
		}
                */
	}

	public EditFromCourseToBatchRes addContentToCourseAndBatchNodes(AddContentCurriculumReq req)
			throws BadRequestException, NotFoundException {
		req.verify();

		EditFromCourseToBatchRes response = new EditFromCourseToBatchRes();
		Node node = null;
		List<String> contextIds = new ArrayList<>();
		if (ArrayUtils.isEmpty(req.getContents())) {
			return response;
		}
		Query query = new Query();
		Criteria parentId = Criteria.where(Curriculum.Constants.COURSE_NODE_ID).is(req.getNodeId());
		Criteria rootId = Criteria.where(Curriculum.Constants._ID).is(req.getNodeId());
		Criteria criteria = new Criteria();
		criteria.orOperator(parentId, rootId);
		query.addCriteria(criteria);
		query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
		if (ArrayUtils.isEmpty(curriculums)) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Node Id Not Found");
		}
		Curriculum course = null;
		for (Curriculum curriculum : curriculums) {
			if (curriculum.getId().equals(req.getNodeId())) {
				if (CurriculumEntityName.OTF_COURSE.equals(curriculum.getContextType())) {
					course = curriculum;
					break;
				} else if (CurriculumEntityName.OTF_BATCH.equals(curriculum.getContextType())) {
					throw new BadRequestException(ErrorCode.CURRICULUM_BATCH_PARENT_NOT_COURSE,
							"nodeId is for batch. Not Course");
				}
			}
		}
		List<String> errors = validateContentIds(course.getBoardId(),req.getContents());
		if(ArrayUtils.isNotEmpty(errors)){
			throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID,errors.toString());
		}
		for (Curriculum curriculum : curriculums) {
			if (ArrayUtils.isEmpty(curriculum.getContents())) {
				curriculum.setContents(new ArrayList<>());
			}
                        //logger.info("contents for "+curriculum.getId()+ " , bfore "+curriculum.getContents().toString());
			List<ContentInfo> contents = curriculum.getContents();
			contents.addAll(req.getContents());
			contextIds.add(curriculum.getContextId());
			curriculumDAO.save(curriculum, req.getCallingUserId());
                        //logger.info("contents for "+curriculum.getId()+ " , after "+curriculum.getContents().toString());
			if (curriculum.getId().equals(req.getNodeId())) {
				node = mapper.map(curriculum, Node.class);
			} else {
				contextIds.add(curriculum.getContextId());
			}
		}
		response.setNode(node);
		response.setBatchIds(contextIds);
		return response;
	}

	public Node editNodeInCurriculum(EditCurriculumReq req) throws BadRequestException, NotFoundException {
		req.verify();
		Node node;
		if (StringUtils.isEmpty(req.getTitle()) && StringUtils.isEmpty(req.getNote())
				&& req.getExpectedHours() == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Nothing to Edit");
		}
		Curriculum curriculum = curriculumDAO.getCurriculumById(req.getNodeId());
		if (curriculum == null) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND,
					"Node Id Not Found in Curriculum. ID : " + req.getNodeId());
		}

		if (StringUtils.isNotEmpty(req.getTitle())) {
			curriculum.setTitle(req.getTitle());
		}
		if (StringUtils.isNotEmpty(req.getNote())) {
			curriculum.setNote(req.getNote());
		}
		if (req.getExpectedHours() != null) {
			curriculum.setExpectedHours(req.getExpectedHours());
		}
		curriculumDAO.save(curriculum, req.getCallingUserId());
		node = mapper.map(curriculum, Node.class);
		return node;
	}

	public void shareContentOnEnrollment(ShareContentEnrollmentReq req) throws BadRequestException {
		req.verify();
		logger.info("deblog-- sharing contents off listener: {}", req);
		Query query = new Query();
		query.addCriteria(Criteria.where(Curriculum.Constants.CONTENTS + ".0").exists(true));
		query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).is(req.getContextId()));

		List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
		List<String> contentInfoIds = new ArrayList<>();
		if (ArrayUtils.isEmpty(curriculums)) {
			logger.info("No content to Share for Id " + req.getContextId());
			return;
		}
		for (Curriculum curriculum : curriculums) {
			for (ContentInfo content : curriculum.getContents()) {
				if (ArrayUtils.isNotEmpty(content.getContentInfoId())) {
					contentInfoIds.add(content.getContentInfoId().get(0));
				}
			}
		}
		if (ArrayUtils.isEmpty(contentInfoIds)) {
			logger.info("No content to share for Id " + req.getContextId());
			return;
		}
		contentInfoManager.shareContentOnEnrollment(contentInfoIds,
				req.getStudentId(), req.getContextId(), req.getStartTime(), req.getEndTime());

//		for (Curriculum curriculum : curriculums) {
//			Boolean added = Boolean.FALSE;
//			for (ContentInfo content : curriculum.getContents()) {
//				if (ArrayUtils.isNotEmpty(content.getContentInfoId())) {
//					if (contentInfoMap.containsKey(content.getContentInfoId().get(0))) {
//						List<String> contentInfoId = content.getContentInfoId();
//						contentInfoId.add(contentInfoMap.get(content.getContentInfoId().get(0)));
//						added = Boolean.TRUE;
//					}
//				}
//			}
//			if (added) {
//				curriculumDAO.save(curriculum, req.getCallingUserId());
//			}
//
//		}
	}

	public EditFromCourseToBatchRes getListOfBatchesToInheritCourse(EditCurriculumReq req) throws BadRequestException {
		req.verify();
		EditFromCourseToBatchRes resp = new EditFromCourseToBatchRes();
		Query query = new Query();
		Criteria parentId = Criteria.where(Curriculum.Constants.COURSE_NODE_ID).is(req.getNodeId());
		query.addCriteria(parentId);
		query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		query.fields().include(Curriculum.Constants.CONTEXT_ID);
		query.fields().include(Curriculum.Constants.CONTEXT_TYPE);
		List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
		List<String> batchIds = new ArrayList<>();
		if (ArrayUtils.isEmpty(curriculums)) {
			return resp;
		}
		for (Curriculum curriculum : curriculums) {
			batchIds.add(curriculum.getContextId());
		}
		resp.setBatchIds(batchIds);
		return resp;
	}

	public EditFromCourseToBatchRes editNodeInCourseAndBatch(EditCurriculumReq req)
			throws VException {
		req.verify();
		String courseNameId = curriculumTemplateManager.getCourseName(req.getEntityId());

		EditFromCourseToBatchRes response = new EditFromCourseToBatchRes();
		Node node;
		if (StringUtils.isEmpty(req.getTitle()) && StringUtils.isEmpty(req.getNote())
				&& req.getExpectedHours() == null) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Nothing to Edit");
		}

		// Check if the title we want for insertion is already present or not
		checkIfCurriculumStructureTitleAlreadyPresent(req.getTitle());

		Set<String> batchIds = new HashSet<>();
		if (ArrayUtils.isEmpty(req.getBatchIds())) {
			logger.info("Editing only at course level");
		} else {
			batchIds.addAll(req.getBatchIds());
		}
		List<String> contextIds = new ArrayList<>();
		List<Curriculum> curriculums = curriculumDAO.getActiveCurriculumNodesForParentAndChildren(req.getNodeId());
		List<String> updateCurriculumIds = new ArrayList<>();
		if (ArrayUtils.isEmpty(curriculums)) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Node Id Not Found");
		}

		Curriculum course = null;
		for (Curriculum curriculum : curriculums) {
			if (curriculum.getId().equals(req.getNodeId())) {
				if (CurriculumEntityName.OTF_COURSE.equals(curriculum.getContextType())) {
					course = curriculum;
					break;
				} else if (CurriculumEntityName.OTF_BATCH.equals(curriculum.getContextType())) {
					throw new BadRequestException(ErrorCode.CURRICULUM_BATCH_PARENT_NOT_COURSE,
							"nodeId is for batch. Not Course");
				}
			}
		}
		for (Curriculum curriculum : curriculums) {
			if (StringUtils.isNotEmpty(curriculum.getId()) && (batchIds.contains(curriculum.getContextId()))) {
				updateCurriculumIds.add(curriculum.getId());
				contextIds.add(curriculum.getContextId());
			} else if (course.getId().equals(curriculum.getId())) {
				updateCurriculumIds.add(curriculum.getId());
			}
		}
		curriculumDAO.updateNodesInCurriculum(updateCurriculumIds, req.getTitle(), req.getNote(),
				req.getExpectedHours());

		// Inserting a new curriculum template through async task
		CurriculumTemplateInsertionRequest request=mapper.map(req, CurriculumTemplateInsertionRequest.class);
		request.setEntityId(req.getEntityId());
		request.setEntityName(req.getEntityName());
		request.setGrades(req.getGrades());
		request.setTargets(req.getTargets());
		request.setTitle(req.getTemplateTitle());
		request.setYear(req.getYear());
		request.setBatchIds(req.getBatchIds());
		request.setNode(null);
		request.setCallingUserId(req.getCallingUserId());
		request.setCallingUserRole(req.getCallingUserRole());
		Map<String, Object> payload = new HashMap<>();
		payload.put("request",request);
		String courseName = curriculumTemplateManager.getCourseName(req.getEntityId());

		logger.info("request file for async task POPULATE_CURRICULUM_STRUCTURE_TABLE for editNodeInCourseAndBatch: "+request);

		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.POPULATE_CURRICULUM_STRUCTURE_TABLE, payload);
		asyncTaskFactory.executeTask(params);

		Curriculum curriculum = curriculumDAO.getCurriculumById(course.getId());
		node = mapper.map(curriculum, Node.class);
		response.setNode(node);
		response.setBatchIds(contextIds);
		return response;
	}


	public void deleteNodeInCourseAndBatch(AddOrDeleteNodeReq req) throws VException {

		// Verify if the request is proper
		req.verify();
		String courseName = curriculumTemplateManager.getCourseName(req.getEntityId());

		// Verify the data specific to this API call is valid
		if (req.getNode().getContextType() != null
				&& CurriculumEntityName.OTF_BATCH.equals(req.getNode().getContextType())) {
			throw new BadRequestException(ErrorCode.CURRICULUM_EDIT_NOT_ALLOWED, "request node is of batch not course");
		} else if (StringUtils.isEmpty(req.getNode().getContextId()) || StringUtils.isEmpty(req.getNode().getId())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "node id is null or contextId is null");
		}

		// Check if the title we want for insertion is already present or not
		checkIfCurriculumStructureTitleAlreadyPresent(req.getTitle());

		// Get the list of batch ids where these deleted node data needs to be populated
		// along with the course id
		Set<String> contextIds = new HashSet<>();
		if (ArrayUtils.isNotEmpty(req.getBatchIds())) {
			contextIds.addAll(req.getBatchIds());
		}
		contextIds.add(req.getNode().getContextId());

		// Get the active curriculum to be deleted
		List<Curriculum> curriculumList = curriculumDAO.getActiveCurriculumNodesForParentAndChildren(req.getNode().getId());

		// In case no curriculum present we don't need to do anything
		if (ArrayUtils.isEmpty(curriculumList)) {
			logger.info("curriculum not found for nodeId : " + req.getNode().getId());
			return;
		}

		// Getting the parent ids to fetch the child ids
		List<String> parentIds =
				curriculumList
					.parallelStream()
					.filter(curriculum -> contextIds.contains(curriculum.getContextId()))
					.map(Curriculum::getId)
					.collect(Collectors.toList());

		// List of ids to be marked deleted
		List<String> toDelete =
				parentIds
					.parallelStream()
					.collect(Collectors.toList());

		// Unless and until all the leave nodes are traversed collect the ids of all the children
		while (ArrayUtils.isNotEmpty(parentIds)) {

			// Get all the child nodes
			List<Curriculum> childNodes = curriculumDAO.getCurriculumIdOfAllTheActiveChildNodes(parentIds);
			logger.info("parentIds: "+parentIds);
			logger.info("childNodes: "+childNodes);

			// If child nodes are present use them as parents and prepare them for deletion
			parentIds=new ArrayList<>();
			if (ArrayUtils.isNotEmpty(childNodes)) {
				parentIds = childNodes.parallelStream().map(Curriculum::getId).collect(Collectors.toList());
				toDelete.addAll(parentIds);
			}
		}

		// Mark status of all the required nodes to deleted
		if (ArrayUtils.isNotEmpty(toDelete)) {
			logger.info("toDelete: "+toDelete);
			curriculumDAO.changeEntityStateToDeleted(toDelete);
		}

		// Inserting a new curriculum template through async task
		CurriculumTemplateInsertionRequest request=mapper.map(req, CurriculumTemplateInsertionRequest.class);
		request.setEntityId(req.getEntityId());
		request.setEntityName(req.getEntityName());
		request.setGrades(req.getGrades());
		request.setTargets(req.getTargets());
		request.setTitle(req.getTitle());
		request.setYear(req.getYear());
		request.setBatchIds(req.getBatchIds());
		request.setNode(null);
		request.setCallingUserId(req.getCallingUserId());
		request.setCallingUserRole(req.getCallingUserRole());
		Map<String, Object> payload = new HashMap<>();
		payload.put("request",request);
		logger.info("request file for async task POPULATE_CURRICULUM_STRUCTURE_TABLE for deleteNodeInCourseAndBatch: "+request);

		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.POPULATE_CURRICULUM_STRUCTURE_TABLE, payload);
		asyncTaskFactory.executeTask(params);

		if (CollectionUtils.isNotEmpty(req.getBatchIds())) {
			awsSNSManager.triggerSNS(SNSTopic.BATCH_CURRICULUM_UPDATE, SNSSubject.REMOVE_BATCH_CURRICULUM_PROGRESS_KEYS.name(),
					gson.toJson(req.getBatchIds()));
		}
	}

	public EditFromCourseToBatchRes addNodeInCourseAndBatch(AddOrDeleteNodeReq req)
			throws VException {

		// verify if the request is proper
		req.verify();
		String courseName = curriculumTemplateManager.getCourseName(req.getEntityId());

		EditFromCourseToBatchRes res = new EditFromCourseToBatchRes(); // response object to be sent

		// In case you don't specify the parent id where it could be added we can't proceed
		if (StringUtils.isEmpty(req.getNode().getParentId())) {
			throw new BadRequestException(ErrorCode.CURRICULUM_NODE_NOT_FOUND, "request does not contain ParentId");
		}

		// Check if the title we want for insertion is already present or not
		checkIfCurriculumStructureTitleAlreadyPresent(req.getTitle());

		// Get the child order of the node
		Integer childOrder = null;
		if (req.getNode().getChildOrder() != null) {
			childOrder = req.getNode().getChildOrder();
		}

		// Get the details of parents and immediate children of the same
		List<Curriculum> curriculumList = curriculumDAO.getActiveCurriculumNodesForParentAndChildren(req.getNode().getParentId());
		logger.info("getActiveCurriculumNodesForParentAndChildren: "+curriculumList);

		// If the node and its children are not present then throw an error
		if (ArrayUtils.isEmpty(curriculumList)) {
			throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND,
					"Node Id Not Found in Curriculum. ID : " + req.getNode().getParentId());
		}

		// Get all the contextIds(or batchIds) and store them in set for editing
		Set<String> contextIds = new HashSet<>();
		if (ArrayUtils.isNotEmpty(req.getBatchIds())) {
			contextIds.addAll(req.getBatchIds());
		}


		Set<String> parentIds = new HashSet<>(); // store all the parent ids
		Curriculum courseCurriculum = null; // store only the parent node of course curriculum here
		for (Curriculum curriculum : curriculumList) {

			// Condition for the course node's curriculum
			if (curriculum.getId().equals(req.getNode().getParentId())) {

				// Ensure that the parent id sent is not of batch
				if (curriculum.getContextType().equals(CurriculumEntityName.OTF_BATCH)) {
					throw new BadRequestException(ErrorCode.CURRICULUM_BATCH_PARENT_NOT_COURSE,
							"Req node is of batch not course");
				}

				// Get all the ids of retrieved curriculum and set them as parent as
				// you can't really add a new node to the leaf nodes
				parentIds.add(curriculum.getId());

				// When everything goes fine then store the data of course curriculum here
				courseCurriculum = curriculum;
				continue;
			}

			// Condition for batch nodes' curriculum
			if (contextIds.contains(curriculum.getContextId())) {
				parentIds.add(curriculum.getId());
			}

		}

		// Getting all the active children for all the parent nodes corresponding to the course node
		List<Curriculum> children = curriculumDAO.getAllActiveChildrenForParentIdsInDescendingChildOrder(parentIds);

		Map<String, Integer> parentMaxChild = new HashMap<>();

		// If children exist for the parent then do increase the child order of required child/children
		if (ArrayUtils.isNotEmpty(children)) {
			List<String> incrementIds = new ArrayList<>();
			for (Curriculum child : children) {

				// stores the id of the nodes after the place we have added so that they could be incremented
				if (childOrder != null && child.getChildOrder() > childOrder) {
					incrementIds.add(child.getId());
				}

				// stores the highest child order corresponding to the parent
				// (as they are sorted in descending order)
				if (!parentMaxChild.containsKey(child.getParentId())) {
					parentMaxChild.put(child.getParentId(), child.getChildOrder());
				}
			}

			// In case there are child/children whose child order needs to be incremented
			// do the same
			if (ArrayUtils.isNotEmpty(incrementIds)) {
				curriculumDAO.incrementChildOrders(incrementIds);
			}
		}

		// Insert the node in proper place inside the course curriculum
		Curriculum newCurriculum = new Curriculum(req.getNode());
		newCurriculum.setContextType(courseCurriculum.getContextType());
		newCurriculum.setContextId(courseCurriculum.getContextId());
		if (newCurriculum.getChildOrder() == null) {
			if (parentMaxChild.containsKey(courseCurriculum.getId())) {
				int tempOrder = parentMaxChild.get(courseCurriculum.getId());
				newCurriculum.setChildOrder(tempOrder + 1);
			} else {
				newCurriculum.setChildOrder(0);
			}
		}
		if(StringUtils.isNotEmpty(courseCurriculum.getRootId())){
			newCurriculum.setRootId(courseCurriculum.getRootId());
		}else {
			newCurriculum.setRootId(courseCurriculum.getId());
		}
		if (newCurriculum.getBoardId() == null && courseCurriculum.getBoardId() != null) {
			newCurriculum.setBoardId(courseCurriculum.getBoardId());
		}
		curriculumDAO.save(newCurriculum, req.getCallingUserId());

		List<Curriculum> insertCurriculums = new ArrayList<>();
		List<String> finalBatchIds = new ArrayList<>();
		for (Curriculum curriculum : curriculumList) {

			// Don't insert data again for the main course curriculum
			if (curriculum.getId().equals(req.getNode().getParentId())) {
				continue;
			}

			// Prepare data for insertion in selected batches
			if (contextIds.contains(curriculum.getContextId())) {

				// Prepare the new data to be inserted
				Curriculum tempCurriculum = new Curriculum(req.getNode());
				tempCurriculum.setContextType(curriculum.getContextType());
				tempCurriculum.setContextId(curriculum.getContextId());
				tempCurriculum.setParentId(curriculum.getId());
				if (parentMaxChild.containsKey(curriculum.getId())) {
					int tempOrder = parentMaxChild.get(curriculum.getId());
					if (tempOrder < newCurriculum.getChildOrder()) {
						tempCurriculum.setChildOrder(tempOrder + 1);
					} else {
						tempCurriculum.setChildOrder(newCurriculum.getChildOrder());
					}
				} else {
					tempCurriculum.setChildOrder(0);
				}
				if(StringUtils.isNotEmpty(curriculum.getRootId())){
					tempCurriculum.setRootId(curriculum.getRootId());
				}else {
					tempCurriculum.setRootId(curriculum.getId());
				}

				if (tempCurriculum.getBoardId() == null) {
					tempCurriculum.setBoardId(curriculum.getBoardId());
				}
				tempCurriculum.setCourseNodeId(newCurriculum.getId());
				finalBatchIds.add(curriculum.getContextId());

				// Collect them in a single place for efficient insertion
				insertCurriculums.add(tempCurriculum);
			}
		}

		// Insert all the new curriculum
		if (ArrayUtils.isNotEmpty(insertCurriculums) && req.getCallingUserId() != null) {
			curriculumDAO.insertAllChildren(insertCurriculums, req.getCallingUserId().toString());
		}

		// Inserting a new curriculum template through async task
		CurriculumTemplateInsertionRequest request=mapper.map(req, CurriculumTemplateInsertionRequest.class);
		request.setEntityId(req.getEntityId());
		request.setEntityName(req.getEntityName());
		request.setGrades(req.getGrades());
		request.setTargets(req.getTargets());
		request.setTitle(req.getTitle());
		request.setYear(req.getYear());
		request.setBatchIds(req.getBatchIds());
		request.setNode(null);
		Map<String, Object> payload = new HashMap<>();
		payload.put("request",request);
		request.setCallingUserId(req.getCallingUserId());
		request.setCallingUserRole(req.getCallingUserRole());
		logger.info("request file for async task POPULATE_CURRICULUM_STRUCTURE_TABLE for addNodeInCourseAndBatch: "+request);

		AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.POPULATE_CURRICULUM_STRUCTURE_TABLE, payload);
		asyncTaskFactory.executeTask(params);

		awsSNSManager.triggerSNS(SNSTopic.BATCH_CURRICULUM_UPDATE, SNSSubject.REMOVE_BATCH_CURRICULUM_PROGRESS_KEYS.name(),
				gson.toJson(finalBatchIds));

		// Prepare the response object
		Node node = mapper.map(newCurriculum, Node.class);
		res.setNode(node);
		res.setBatchIds(finalBatchIds);
		return res;
	}

	public List<String> checkIfCurriculumExists(List<String> contextIds){
		List<String> batchIds = new ArrayList<>();
		if(ArrayUtils.isNotEmpty(contextIds)){
			Set<String> batch_Ids = new HashSet<>();
			batch_Ids.addAll(contextIds);
			List<String> responses = curriculumDAO.curriculumExists(batch_Ids);
			if(ArrayUtils.isNotEmpty(responses)){
				return responses;
			}
		}
		return batchIds;
	}

	public void getAllContentIdsInTree(Node node, Set<String> contentIds) {
		if (node != null) {
			if (ArrayUtils.isNotEmpty(node.getContents())) {
				for (ContentInfo contentInfo : node.getContents()) {
					if (StringUtils.isNotEmpty(contentInfo.getContentId()) && contentInfo.getContentType() != null
							&& ContentType.TEST.equals(contentInfo.getContentType())) {
						contentIds.add(contentInfo.getContentId());
					}
				}
			}
			if (ArrayUtils.isNotEmpty(node.getNodes())) {
				for (Node iterNode : node.getNodes()) {
					getAllContentIdsInTree(iterNode, contentIds);
				}
			}
		}
	}

	public List<String> validateContentIds(Long boardId,List<ContentInfo> contentInfos){
		Node node = new Node();
		if(boardId != null && ArrayUtils.isNotEmpty(contentInfos)){
			node.setBoardId(boardId);
			node.setContents(contentInfos);
			return validateContentIds(node);
		}else {
			return new ArrayList<String>();
		}
	}

	public Map<Long,Set<String>> getBoardContentMap(Node node){
		Map<Long,Set<String>> boardContentMap = new HashMap<>();
		if(node!= null && ArrayUtils.isNotEmpty(node.getNodes())){
			for(Node iterNode : node.getNodes()){
				if(iterNode.getBoardId() != null){
					Set<String> contentIds = new HashSet<>();
					getAllContentIdsInTree(iterNode,contentIds);
					if(ArrayUtils.isNotEmpty(contentIds)){
						boardContentMap.put(iterNode.getBoardId(), contentIds);
					}
				}
			}
		}else {
			if(node!= null && ArrayUtils.isNotEmpty(node.getContents())){
				Set<String> contentIds = new HashSet<>();
				for(ContentInfo contentInfo : node.getContents()){
					if (StringUtils.isNotEmpty(contentInfo.getContentId()) && contentInfo.getContentType() != null
							&& ContentType.TEST.equals(contentInfo.getContentType())) {
						contentIds.add(contentInfo.getContentId());
					}
				}
				if(node.getBoardId() != null){
					boardContentMap.put(node.getBoardId(), contentIds);
				}
			}
		}
		return boardContentMap;
	}

	public List<String> validateContentIds(Node node) {
		List<String> errors = new ArrayList<>();
		List<String> mappingErrors = new ArrayList<>();
		List<String> invalidTestErrors = new ArrayList<>();
		if (node != null) {
			Set<String> totalContentIds = new HashSet<>();
			Map<Long,Set<String>> boardContentMap = getBoardContentMap(node);
			if(boardContentMap == null){
				return errors;
			}
			Map<String, CMDSTest> testBoardMap = new HashMap<>();
			for(Entry<Long,Set<String>> entry : boardContentMap.entrySet()){
				if(ArrayUtils.isNotEmpty(entry.getValue())){
					totalContentIds.addAll(entry.getValue());
				}
			}
			if (ArrayUtils.isNotEmpty(totalContentIds)) {
				List<CMDSTest> tests = testDAO.getCMDSTestsByIdsForValidation(totalContentIds);
				if (ArrayUtils.isEmpty(tests)) {
					errors.add("No valid test ids");
					return errors;
				}
				for (CMDSTest test : tests) {
					if (StringUtils.isNotEmpty(test.getId())) {
						testBoardMap.put(test.getId(), test);//Validate if content added at rigth place as well
					}
				}
				for(Entry<Long,Set<String>> entry : boardContentMap.entrySet()){
					Set<String> contents = entry.getValue();
					Long boardId = entry.getKey();
					String board = null;
					if(boardId != null){
						board = boardId.toString();
					}
					if(ArrayUtils.isNotEmpty(contents)){
						for (String id : contents) {
							if (!testBoardMap.containsKey(id)) {
								invalidTestErrors.add(id);
							}else if(testBoardMap.containsKey(id) && StringUtils.isNotEmpty(board)){
								CMDSTest test = testBoardMap.get(id);
								if(test != null && ArrayUtils.isNotEmpty(test.getBoardIds())){
									if(!test.getBoardIds().contains(board)){
										mappingErrors.add(id);
									}
								}
							}
						}
					}
				}
			}
		}
		if(ArrayUtils.isNotEmpty(invalidTestErrors)){
			Boolean errorPresent = Boolean.FALSE;
			for(String id : invalidTestErrors){
				if(StringUtils.isNotEmpty(id)){
					errors.add(id);
					errorPresent = Boolean.TRUE;
				}
			}
			if(errorPresent){
				errors.add(0,"The Following TestIds are invalid : ");
			}
		}
		if(ArrayUtils.isNotEmpty(mappingErrors)){
			int size = errors.size();
			Boolean errorPresent = Boolean.FALSE;
			for(String id : mappingErrors){
				if(StringUtils.isNotEmpty(id)){
					errors.add(id);
					errorPresent = Boolean.TRUE;
				}
			}
			if(errorPresent){
				errors.add(size,"TestIds not mapped correctly with subject : ");
			}
		}
		return errors;
	}


    public Node resetContentInCurriculum(AddContentCurriculumReq req) throws VException {
        if (StringUtils.isEmpty(req.getNodeId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "nodeId is null");
        }

        Curriculum curriculum = curriculumDAO.getCurriculumById(req.getNodeId());
        if (curriculum == null) {
            throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Node Id Not Found");
        }
        if (ArrayUtils.isNotEmpty(req.getContents())) {
            List<String> errors = validateContentIds(curriculum.getBoardId(), req.getContents());
            if (ArrayUtils.isNotEmpty(errors)) {
                throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID, errors.toString());
            }
        }
        curriculum.setContents(req.getContents());
        curriculumDAO.save(curriculum, req.getCallingUserId());
        logger.info("Content Reset by "+req.getCallingUserId());
        if(Boolean.TRUE.equals(req.getAddInBatch())){
            resetContentInCurriculumBatches(req.getBatchIds(),curriculum.getId(),req.getContents(),req.getCallingUserId(),Boolean.FALSE);
        }

        Node node = mapper.map(curriculum, Node.class);
        return node;

    }

    //Only for batches
    public void resetContentInCurriculumBatches(List<String> batchIds, String courseNodeId,List<ContentInfo> contents,Long callingUserId,Boolean validate) throws VException{
        if(StringUtils.isEmpty(courseNodeId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "courseNodeId is null");
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.COURSE_NODE_ID).is(courseNodeId));
        if (ArrayUtils.isNotEmpty(batchIds)) {
            query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(batchIds));
        }

        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
        if (ArrayUtils.isEmpty(curriculums)) {
            throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Node Id Not Found");
        }
        if(Boolean.TRUE.equals(validate) && ArrayUtils.isNotEmpty(contents)){
            List<String> errors = validateContentIds(curriculums.get(0).getBoardId(), contents);
            if (ArrayUtils.isNotEmpty(errors)) {
                throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID, errors.toString());
            }
        }
        for(Curriculum curriculum : curriculums){
            curriculum.setContents(contents);
            curriculumDAO.save(curriculum, callingUserId);
        }

    }


    public Node removeContentInCurriculum(AddContentCurriculumReq req) throws VException {

        req.verify();

        Curriculum curriculum = curriculumDAO.getCurriculumById(req.getNodeId());
        if (curriculum == null) {
            throw new NotFoundException(ErrorCode.CURRICULUM_ID_NOT_FOUND, "Node Id Not Found");
        }
        Set<String> contentIds = new HashSet<>();
        Set<String> contentUrls = new HashSet<>();
        List<ContentInfo> contentsToRemove=new ArrayList<>();
        contentsToRemove.addAll(req.getContents());
        List<ContentInfo> contents = new ArrayList<>();
        for(ContentInfo content : req.getContents()){
            if(StringUtils.isNotEmpty(content.getContentId())){
                contentIds.add(content.getContentId());
            }
            if(StringUtils.isNotEmpty(content.getUrl())){
                contentUrls.add(content.getUrl());
            }
        }
        if(ArrayUtils.isEmpty(contentIds) && ArrayUtils.isEmpty(contentUrls)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No contents have contentId and url");
        }
        if(ArrayUtils.isEmpty(curriculum.getContents())){
            throw new NotFoundException(ErrorCode.CONTENT_NOT_FOUND_CURRICULUM, "curriculum does not have contents");
        }
        for(ContentInfo content : curriculum.getContents()){

			if(contentsToRemove.contains(content)){
				contentsToRemove.remove(content);
				continue;
			}
//
//            if(content.getContentId() != null && contentIds.contains(content.getContentId())){
//                logger.info("removing content : "+ content.getContentId());
//                continue;
//            }
//
//            if(StringUtils.isNotEmpty(content.getUrl()) && contentUrls.contains(content.getUrl())){
//                logger.info("removing content : "+ content.getUrl());
//                continue;
//            }
//
            contents.add(content);
        }
        curriculum.setContents(contents);
        curriculumDAO.save(curriculum, req.getCallingUserId());
        logger.info("Content Reset by "+req.getCallingUserId());
        if(Boolean.TRUE.equals(req.getAddInBatch())){
            removeContentInCurriculumBatches(contentIds,req.getBatchIds(),curriculum.getId(),req.getContents(),req.getCallingUserId());

        }

        Node node = mapper.map(curriculum, Node.class);
        return node;

    }

	private void removeContentInCurriculumBatches(Set<String> contentIds, List<String> batchIds, String id, Long callingUserId) throws VException {
		removeContentInCurriculumBatches(contentIds,batchIds,id,null,callingUserId);

	}

	//Only for batches
    public void removeContentInCurriculumBatches(Set<String> contentIds, List<String> batchIds, String courseNodeId,List<ContentInfo> contentsToRemove, Long callingUserId) throws VException{
        if(StringUtils.isEmpty(courseNodeId)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "courseNodeId is null");
        }
        if (ArrayUtils.isEmpty(contentIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No contents have contentId");
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(Curriculum.Constants.COURSE_NODE_ID).is(courseNodeId));
        if (ArrayUtils.isNotEmpty(batchIds)) {
            query.addCriteria(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(batchIds));
        }

        query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
        if (ArrayUtils.isEmpty(curriculums)) {
            logger.info(" Curriculums Not Found");
            return;
        }
        for (Curriculum curriculum : curriculums) {
            if (ArrayUtils.isEmpty(curriculum.getContents())) {
                logger.info( "curriculum does not have contents");
                continue;
            }
            List<ContentInfo> contents = new ArrayList<>();
            List<ContentInfo> contentsRemovalList=new ArrayList<>();
            contentsRemovalList.addAll(contentsToRemove);
            for (ContentInfo content : curriculum.getContents()) {
            	if(contentsRemovalList!=null) {
            		if(contentsRemovalList.contains(content)){
            			contentsRemovalList.remove(content);
            			continue;
					}
				}else if (content.getContentId() != null && contentIds.contains(content.getContentId())) {
                    logger.info("removing content : " + content.getContentId());
                    continue;
                }
                contents.add(content);
            }
            curriculum.setContents(contents);
            curriculumDAO.save(curriculum, callingUserId);
        }

    }

    public int getAllNonTestContentIdsInTree(Node node) {
        int total = 0;
            if (node != null) {
                    if (ArrayUtils.isNotEmpty(node.getContents())) {
                            for (ContentInfo contentInfo : node.getContents()) {
                                    if (!(ContentType.TEST.equals(contentInfo.getContentType()))){
                                        total++;
                                    }
                            }
                    }
                    if (ArrayUtils.isNotEmpty(node.getNodes())) {
                            for (Node iterNode : node.getNodes()) {
                                    total = total+getAllNonTestContentIdsInTree(iterNode);
                            }
                    }
            }
            return total;
    }

    public Map<String, List<SubjectFileCountPojo>> getCurriculumFilesInBatch(List<String> batchIds){
		logger.info("Going for calculating files for curriculum in batch.");

        if(ArrayUtils.isEmpty(batchIds)){
            return new HashMap<>();
        }

        Map<String, List<SubjectFileCountPojo>> result = new HashMap<>();
        Set<String> nonCachedBatchIds = new HashSet<>(batchIds);
        /*
        for(String batchId : batchIds){
            Integer numOfContents = null; 
            try{
                
                numOfContents = cacheManager.getCachedNumberOfContentSharedInBatch(batchId);
                
            }catch(Exception ex){
                logger.warn("Error in fetching cached content num for batchId: "+batchId);
            }
            if(numOfContents == null){
                nonCachedBatchIds.add(batchId);
            }else{
                result.put(batchId, numOfContents);
            }

        
        }*/

        GetCurriculumReq getCurriculumReq = new GetCurriculumReq();
        getCurriculumReq.setEntityName(CurriculumEntityName.OTF_BATCH);
        getCurriculumReq.setEntityIds(new ArrayList<>(nonCachedBatchIds));
        List<Node> nodes = new ArrayList<>();
        try{
        	logger.info("Going to calculate curriculum by entity");
            nodes = getCurriculumByEntity(getCurriculumReq);
        }catch(Exception ex){
            //swallow
//            logger.error("Error in getting curriculum nodes for batch: "+ex.getMessage());
        }
        Map<String, List<Node>> batchNodeMap = new HashMap<>();
        Set<Long> boardIds = new HashSet<>();
        if(ArrayUtils.isNotEmpty(nodes)){
            for(Node node: nodes){

                if(ArrayUtils.isNotEmpty(node.getNodes())){
                    batchNodeMap.put(node.getContextId(), node.getNodes());
                    for(Node subjectNode : node.getNodes()){
                        if(subjectNode.getBoardId() != null){
                            boardIds.add(subjectNode.getBoardId());
                        }
                    }
                }
                // int contentIds = getAllNonTestContentIdsInTree(node);
                
                /*if(result.containsKey(node.getContextId())){
                    result.put(node.getContextId(), result.get(node.getContextId()) + contentIds);
                }else{
                    result.put(node.getContextId(), contentIds);
                }*/
            }
        }
        Map<Long, Board> boards = fosUtils.getBoardInfoMap(boardIds);

        logger.info("boards: "+boards);
        for(String batchId: nonCachedBatchIds){

            List<Node> subjectNodes = batchNodeMap.get(batchId);
            if(ArrayUtils.isNotEmpty(subjectNodes)){
                List<SubjectFileCountPojo> batchSubjectFileCountPojos = new ArrayList<>();
                for(Node subjectNode : subjectNodes){
                    if(boards.containsKey(subjectNode.getBoardId())){
                        Board board = boards.get(subjectNode.getBoardId());
                        SubjectFileCountPojo subjectFileCountPojo = new SubjectFileCountPojo();
                        subjectFileCountPojo.setBoardId(subjectNode.getBoardId());
                        subjectFileCountPojo.setName(board.getName());
                        subjectFileCountPojo.setCount(getAllNonTestContentIdsInTree(subjectNode));
                        batchSubjectFileCountPojos.add(subjectFileCountPojo);
                    }
                }
                result.put(batchId, batchSubjectFileCountPojos);
            }

        }
        logger.info("result: "+result);
        /*for(String batchId: nonCachedBatchIds){
            if(result.containsKey(batchId)){
                try{
                    cacheManager.setCachedContentSharedInBatch(batchId, result.get(batchId));
                }catch(Exception ex){
                    logger.error("Error in caching content shared in batch: "+ex.getMessage());
                }                
            }
        }*/

        return result;
    }

    public boolean checkIfUserEnrolledInEntity(CurriculumEntityName entityName, String entityId, String userId) throws VException{

        String queryString = SUBSCRIPTION_ENDPOINT + "/enroll/checkForUserEnrollment?entityName="+entityName+"&entityIds="+entityId+"&userId="+userId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(queryString, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PlatformBasicResponse platformBasicResponse = new Gson().fromJson(jsonString, PlatformBasicResponse.class);
        return platformBasicResponse.isSuccess();

    }

	public  List<String> getTopicsByEntity(GetCurriculumReq getCurriculumReq)  throws VException{

		Node node = null;
		List<String> topics = new ArrayList<String>();
		List<Curriculum> curriculums = curriculumDAO.getCurriculumByCourseId(getCurriculumReq.getEntityName(),getCurriculumReq.getEntityId());
		if (ArrayUtils.isEmpty(curriculums)) {
			return topics;
		}
		Curriculum root = findRoot(curriculums);
		for(Curriculum curriculum: curriculums){
			if(curriculum.getParentId()!= null &&  !curriculum.getParentId().equals(root.getId())){
				topics.add(curriculum.getId());
			}
		}

		return topics;

	}

	public  Map<String,String> getNodeNamesByIds(GetCurriculumReq getCurriculumReq)  throws VException{

		Node node = null;
		Map<String,String> topics = new HashMap<String, String>();
		if( ArrayUtils.isEmpty(getCurriculumReq.getEntityIds())){
			return topics;
		}
		List<Curriculum> curriculums = curriculumDAO.getCurriculumByIds(getCurriculumReq.getEntityIds());
		if (ArrayUtils.isEmpty(curriculums)) {
			return topics;
		}
		Curriculum root = findRoot(curriculums);
		for(Curriculum curriculum: curriculums){
				topics.put(curriculum.getId(),curriculum.getTitle());
		}
		return topics;

	}



	@Deprecated
	public List<String> migrateExistingData(int dataSize){
		return curriculumDAO.migrateExistingData(dataSize);
	}

	public void appendCurriculum(AddCurriculumReq addCurriculumReq) throws BadRequestException {
		addCurriculumReq.verify();

				Node node = addCurriculumReq.getNode();
				List<String> errors = validateContentIds(node);
				if(ArrayUtils.isNotEmpty(errors)){
					throw new BadRequestException(ErrorCode.INVALID_CONTENT_ID,errors.toString());
				}
				Curriculum curriculum = curriculumDAO.getCurriculumById(addCurriculumReq.getEntityId());
				Query query = new Query();
				query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
				query.addCriteria(Criteria.where(Curriculum.Constants.PARENT_ID).is(addCurriculumReq.getEntityId()));
				query.fields().include(Curriculum.Constants.CHILD_ORDER);
				query.fields().include(Curriculum.Constants._ID);
				List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
				int childOrder = 0;

				for(Curriculum curriculum1: curriculums){
					if( curriculum1.getChildOrder() > childOrder)
					{
						childOrder=curriculum1.getChildOrder();
					}
				}
				childOrder++;
				for (Node iterNode : node.getNodes()) {
					iterNode.setChildOrder(childOrder);
					childOrder++;
				}
				recurringAddNodes(node.getNodes(), curriculum, addCurriculumReq.getCallingUserId());


		//PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

	}

	public List<Node> getCurriculumForBatchBoardAndLastUpdated(String batchId, Long boardId, Long lastUpdated) {
		List<Curriculum> curriculums=curriculumDAO.getCurriculumForBatchBoardAndLastUpdated(batchId,boardId,lastUpdated);
		List<Node> nodes=new ArrayList<>();
		if(!ArrayUtils.isEmpty(curriculums)) {
			curriculums.forEach(curriculum -> nodes.add(mapper.map(curriculum, Node.class)));
		}
		return nodes;
	}
	public void appendCurriculumCourseAndBatch(AddCurriculumReq req) throws BadRequestException {
		Query query = new Query();
		Criteria rootId = Criteria.where(Curriculum.Constants._ID).is(req.getEntityId());
		if(!ArrayUtils.isEmpty(req.getBatchIds())) {
			Criteria criteria = new Criteria();
			Criteria parentId = Criteria.where(Curriculum.Constants.COURSE_NODE_ID).is(req.getEntityId());
			parentId.andOperator(Criteria.where(Curriculum.Constants.CONTEXT_ID).in(req.getBatchIds()));
			criteria.orOperator(parentId, rootId);
			query.addCriteria(criteria);
		}
		else {
			query.addCriteria(rootId);
		}
		appendCurriculum(req);
		query.addCriteria(Criteria.where(Curriculum.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		List<Curriculum> curriculums = curriculumDAO.runQuery(query, Curriculum.class);
		String CourseEtityId = req.getEntityId();
		for(Curriculum curriculum:curriculums ){
			if(!CourseEtityId.equals(curriculum.getId())) {
				req.setEntityId(curriculum.getId());
				appendCurriculum(req);
			}
		}
	}
//
//	public void recurringAddNodesToCourse(List<Node> nodes, Curriculum parent, Long callingUserId) {
//		List<Curriculum> curriculums = new ArrayList<>();
//		int childOrder = 0;
//		HashMap<Integer,Node> nodeHashMap = new HashMap<>();
//		if (CollectionUtils.isNotEmpty(nodes)) {
//			for (Node iterNode : nodes) {
//				Curriculum curriculum = new Curriculum(iterNode);
//				curriculum.setParentId(parent.getId());
//				curriculum.setRootId(parent.getRootId());
//				if (curriculum.getChildOrder() == null) {
//					// if(curriculum.getChildOrder() == childOrder){
//					curriculum.setChildOrder(childOrder);
//				}
//				if (curriculum.getBoardId() == null && parent.getBoardId() != null) {
//					curriculum.setBoardId(parent.getBoardId());
//				}
//				if (StringUtils.isNotEmpty(iterNode.getId())) {
//					curriculum.setCourseNodeId(iterNode.getId());
//				}
//				curriculum.setContextType(parent.getContextType());
//				curriculum.setContextId(parent.getContextId());
//				nodeHashMap.put(iterNode.getChildOrder(),iterNode);
//				curriculumDAO.save(curriculum, callingUserId);
//				iterNode.setId(curriculum.getId());
//
//			}
//			curriculumDAO.insertAllChildren(curriculums, callingUserId.toString());
//			if (CollectionUtils.isNotEmpty(curriculums)) {
//				for (Curriculum curriculum : curriculums) {
//					nodeHashMap.get(curriculum.getChildOrder()).setId(curriculum.getId());
//					recurringAddNodesToCourse(nodeHashMap.get(curriculum.getChildOrder()).getNodes(), curriculum, callingUserId);
//				}
//			}
//		}
//	}
	public List<CurriculumCountResponse> getCurriculumCount() throws VException {

		if(Objects.isNull(sessionUtils.getCallingUserId())){
			throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,"You are not authorized");
		}
		Long userId=sessionUtils.getCallingUserId();
		Role role=sessionUtils.getCallingUserRole();
		List<CurriculumCountResponse> curriculumCount=new ArrayList<>();
		logger.info("userId is "+userId+" user role is "+role);
		List<EnrollmentPojo> enrollments=getActiveAndInactiveEnrollmentsForUserId(Long.toString(userId));
		Map<String,Long> batchInactiveDate=new HashMap<>(); // to give only those contents which are active
		Map<String,String> batchCourseIdMap=new HashMap<>(); // to store the course id of the batch
		Map<String,String> courseNameMap=new HashMap<>(); // to store the course name of the batch
		Map<String,String> batchEntityIdMap=new HashMap<>(); // entity id for the batch
		Map<String,String> batchEntityTitleMap=new HashMap<>(); // get the entity title for the batch
		Map<String,EntityStatus> batchEntityStatusMap=new HashMap<>(); // to store if the batch is active or inactive
		Map<String,EntityType> batchEntityTypeMap=new HashMap<>(); // to store the entity type of batch
		Map<String,String> batchBundleMap=new HashMap<>(); // to store if the batch is a part of bundle
		Map<String,String> aioNameMap=new HashMap<>(); // to store the AIO names for the batch

		logger.info("For each enrollment populating data in batch course map and batch inactive date map");
		logger.info("No of enrollment found is "+enrollments.size());
		if(ArrayUtils.isNotEmpty(enrollments)) {
			enrollments.forEach(enrollment -> {

				// For each batch id populate the course id and course name and entity status and other details of batch
				if (!batchEntityIdMap.containsKey(enrollment.getBatchId())) {
					batchEntityIdMap.put(enrollment.getBatchId(), enrollment.getEntityId());
					batchCourseIdMap.put(enrollment.getBatchId(),enrollment.getCourseId());
					batchEntityTitleMap.put(enrollment.getBatchId(),enrollment.getEntityTitle());
					batchEntityStatusMap.put(enrollment.getBatchId(),enrollment.getStatus());
					batchEntityTypeMap.put(enrollment.getBatchId(),enrollment.getEntityType());
					if(enrollment.getPurchaseContextId()!=null) {
						batchBundleMap.put(enrollment.getBatchId(), enrollment.getPurchaseContextId());
						// Override the value so that it doesn't conflict when we are calculating for
						// bundle of batches below.
						batchEntityTypeMap.put(enrollment.getBatchId(), EntityType.BUNDLE);
					}
					courseNameMap.put(enrollment.getBatchId(),enrollment.getCourse().getTitle());
				}

				// If the batch is inactive then populate its actual inactive date and if the case is
				// opposite then put max value of long as inactive date of the batch.
				if (EntityStatus.INACTIVE.equals(enrollment.getStatus())) {
					List<StatusChangeTime> statusChangeTime = enrollment.getStatusChangeTime();
					Long changeTime = 0l;
					for (StatusChangeTime time : statusChangeTime) {
						changeTime = Math.max(changeTime, time.getChangeTime());
					}
					batchInactiveDate.put(enrollment.getBatchId(), changeTime);
				} else {
					batchInactiveDate.put(enrollment.getBatchId(), Long.MAX_VALUE);
				}
			});
		}

		logger.info("Making a tree out of all the retrieved curriculum. ");
		// Get all the curriculum for all the enrolled batch ids which are active or inactive
		logger.info("Number of batch enrollments found is "+batchEntityIdMap.keySet().size()+" and the batchids are "+batchEntityIdMap.keySet());
		List<Curriculum> curriculumList=curriculumDAO.getCurriculumByBatchIds(Lists.newArrayList(batchEntityIdMap.keySet()));
		Map<String,Curriculum> curriculumMap=new HashMap<>();// for storing details of curriculum
		Map<String,List<String>> curriculumTree=new HashMap<>();// for making a tree(Tree levels: CURRICULUM -> SUBJECT -> CHAPTER -> TOPIC )
		Set<String> curriculumRoot=new HashSet<>(); // for storing root node so that for each batch traversal is easy

		// For each curriculum of batch the student is enrolled
		if(ArrayUtils.isNotEmpty(curriculumList)) {
			curriculumList.forEach(curriculum -> {
				// Store its data to its id
				curriculumMap.put(curriculum.getId(), curriculum);

				// If the parent is already present in the tree then append the child
				// to rest of the child(ren) of the tree or insert a new child
				if (curriculumTree.containsKey(curriculum.getParentId())) {
					List<String> leaves = curriculumTree.get(curriculum.getParentId());
					leaves.add(curriculum.getId());
					curriculumTree.put(curriculum.getParentId(), leaves);
				} else {
					List<String> leaves = new ArrayList<>();
					leaves.add(curriculum.getId());
					curriculumTree.put(curriculum.getParentId(), leaves);
				}

				// A root node has no parent, so populate them in parent
				if (curriculum.getParentId() == null) {
					curriculumRoot.add(curriculum.getId());
				}
			});
		}

		logger.info("For each tree or batch, traverse(DFS) and calculate the number of contents shared");
		logger.info("Number of root found to be "+curriculumRoot.size()+" and it should be "+batchEntityIdMap.size());
		Map<String,CurriculumCountResponse> batchCountResponse=new HashMap<>(); // Store the details of batch
		if(ArrayUtils.isNotEmpty(curriculumRoot)) {
			curriculumRoot.forEach(root -> {
				Long dateOfBatchInactive=batchInactiveDate.get(curriculumMap.get(root).getContextId());
				String courseId=batchCourseIdMap.get(curriculumMap.get(root).getContextId());
				CurriculumCountResponse curriculaCountForBatch = getContentCountInBatch(root, curriculumTree, curriculumMap, dateOfBatchInactive, courseId);
				batchCountResponse.put(curriculaCountForBatch.getBatchId(),curriculaCountForBatch);
			});
		}

		// Get the remaining AIO names
		populateAIONames(aioNameMap,batchBundleMap);

		// Make the tree for response structure
		Map<String,CurriculumCountResponse> responses=new HashMap<>();
		/*
		// Uncomment in case you need proper debugging
		logger.info("Creating tree for response");
		logger.info("enrollments -- "+enrollments);
		logger.info("batchInactiveDate -- "+batchInactiveDate);
		logger.info("batchCourseIdMap -- "+batchCourseIdMap);
		logger.info("courseNameMap -- "+courseNameMap);
		logger.info("batchEntityIdMap -- "+batchEntityIdMap);
		logger.info("batchEntityTitleMap -- "+batchEntityTitleMap);
		logger.info("batchEntityStatusMap -- "+batchEntityStatusMap);
		logger.info("batchEntityTypeMap -- "+batchEntityTypeMap);
		logger.info("batchBundleMap -- "+batchBundleMap);
		logger.info("aioNameMap -- "+aioNameMap);
		*/
		for(String batchId:batchEntityTypeMap.keySet()){
			// Get the desired key and store the variables to be used in future
			logger.info("For batch id "+batchId+" processing data");
			String key=null;
			String entityId=null;
			String entityName=null;
			EntityType entityType=null;
			if(EntityType.BUNDLE.equals(batchEntityTypeMap.get(batchId))){
				key=batchBundleMap.get(batchId);
				entityId=batchBundleMap.get(batchId);
				entityName=aioNameMap.get(batchBundleMap.get(batchId));
				entityType=EntityType.BUNDLE;
			}else if(EntityType.OTM_BUNDLE.equals(batchEntityTypeMap.get(batchId))){
				key=batchEntityIdMap.get(batchId);
				entityId=batchEntityIdMap.get(batchId);
				entityName=batchEntityTitleMap.get(batchId);
				entityType=EntityType.OTM_BUNDLE;
			}else if(EntityType.OTF_COURSE.equals(batchEntityTypeMap.get(batchId))){
				key=batchCourseIdMap.get(batchId);
				entityId=batchCourseIdMap.get(batchId);
				entityName=courseNameMap.get(batchId);
				entityType=EntityType.OTF_COURSE;
			}else {
				logger.error("There is a problem with the data "+gson.toJson(batchEntityTypeMap));
			}

			// The case which should never occur
			if(key==null || entityId==null || entityName==null || entityType==null){
				throw new VException(ErrorCode.BAD_REQUEST_ERROR,"key = "+key+" -- entityId = "+entityId+" -- entityName = "+entityName+" -- entityType = "+entityType);
			}

			// Check if the response exists for the required entity or not
			CurriculumCountResponse response;
			if(responses.containsKey(key)){
				response=responses.get(key);
			}else{
				response=new CurriculumCountResponse();
				response.setEntityId(entityId);
				response.setEntityName(entityName);
				response.setEntityType(entityType);
				response.setEntityStatus(EntityStatus.INACTIVE);
			}

			// Create the current batch count response
			CurriculumCountResponse batchCountRes=batchCountResponse.get(batchId);
			logger.info("The batch count response we got is "+batchCountRes);

			// This is used because not all batches need to have curriculum in them, in that case
			// it is better not to show them
			if(batchCountRes!=null){
				batchCountRes.setEntityStatus(batchEntityStatusMap.get(batchId));
				batchCountRes.setCourseName(courseNameMap.get(batchId));

				// The entire stuff will be inactive if and only if everything is inactive
				if(EntityStatus.ACTIVE.equals(batchCountRes.getEntityStatus())){
					response.setEntityStatus(EntityStatus.ACTIVE);
				}

				// Insert into the count res or create if not necessary
				List<CurriculumCountResponse> countResponses;
				if(ArrayUtils.isNotEmpty(response.getCountResponses())){
					countResponses=response.getCountResponses();
				}else{
					countResponses=new ArrayList<>();
				}
				countResponses.add(batchCountRes);

				// Update the count response
				response.setCountResponses(countResponses);
				logger.info("Response for "+"key = "+key+" -- entityId = "+entityId+" -- entityName = "+entityName+" -- entityType = "+entityType);
				logger.info("Response is "+gson.toJson(response));
				responses.put(key,response);
			}
		}

		curriculumCount.addAll(responses.values());

		return curriculumCount;
	}

	private CurriculumCountResponse getContentCountInBatch(String root, Map<String, List<String>> curriculumTree, Map<String, Curriculum> curriculumMap, Long dateOfBatchInactive, String courseId) {

		// Populate basic data
		CurriculumCountResponse batchDetailsRes=new CurriculumCountResponse();
		batchDetailsRes.setBatchId(curriculumMap.get(root).getContextId());
		batchDetailsRes.setCourseId(courseId);

		List<String> subjectsOfCurriculum=curriculumTree.get(root); // Get the subjects from curriculum
		List<SubjectFileCountPojo> subjectFileCountPojos=new ArrayList<>(); // Initialization for storing subject file count
		if(ArrayUtils.isNotEmpty(subjectsOfCurriculum)) {
			for (String subjectId : subjectsOfCurriculum) {

				// For each subject fill in the basic details
				SubjectFileCountPojo subjectFileCount = new SubjectFileCountPojo();
				subjectFileCount.setName(curriculumMap.get(subjectId).getTitle());
				subjectFileCount.setBoardId(curriculumMap.get(subjectId).getBoardId());

				// Initialize the shared count
				Integer sharedContentCount = 0;

				// Get details of subject(retrieved above from curriculum tree of a particular batch)
				sharedContentCount += getContentCountForCurriculumNode(curriculumMap.get(subjectId),dateOfBatchInactive);

				// Get details of chapters of subject
				List<String> chapterOfSubject = curriculumTree.get(subjectId);
				if (ArrayUtils.isNotEmpty(chapterOfSubject)) {
					for (String chapterId : chapterOfSubject) {
						sharedContentCount += getContentCountForCurriculumNode(curriculumMap.get(chapterId),dateOfBatchInactive);

						// Get details of topicTags of chapter
						List<String> topicOfChapter = curriculumTree.get(chapterId);
						if (ArrayUtils.isNotEmpty(topicOfChapter)) {
							for (String topicId : topicOfChapter) {
								sharedContentCount += getContentCountForCurriculumNode(curriculumMap.get(topicId),dateOfBatchInactive);
							}
						}
					}
				}
				subjectFileCount.setCount(sharedContentCount);
				subjectFileCountPojos.add(subjectFileCount);
			}
		}
		batchDetailsRes.setSubjectFileCount(subjectFileCountPojos);
		logger.info("getContentCountInBatch - "+gson.toJson(batchDetailsRes));
		return batchDetailsRes;
	}

	private Integer getContentCountForCurriculumNode(Curriculum curriculum, Long batchInactiveDate) {
		Integer count=0;
		logger.info("Curriculum id is "+curriculum.getId());
		List<ContentInfo> contents=curriculum.getContents();
		if(ArrayUtils.isNotEmpty(contents)){
			for(ContentInfo content:contents){
				if(!ContentType.TEST.equals(content.getContentType())){
					logger.info("content share time is "+content.getSharedTime()+" batchInactiveDate is "+batchInactiveDate);
					if(ShareStatus.SHARED.equals(content.getShareStatus())){
						if(content.getSharedTime()<batchInactiveDate){
							++count;
						}
					}
				}
			}
		}

		logger.info("For curriculum node title "+curriculum.getTitle()+" no of shared content is "+count);

		return count;
	}

	public List<EnrollmentPojo> getActiveAndInactiveEnrollmentsForUserId(String userId){
		ClientResponse response = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
				+ "enroll/getActiveAndInactiveEnrollmentsForUserId?userId=" + userId,HttpMethod.GET, null);
		String jsonString = response.getEntity(String.class);
		Type listType=new TypeToken<List<EnrollmentPojo>>(){}.getType();
		List<EnrollmentPojo> enrollments=gson.fromJson(jsonString,listType);
		logger.info("getActiveAndInactiveEnrollmentsForUserId from lms is "+gson.toJson(enrollments));
		return enrollments;
	}

	private void populateAIONames(Map<String, String> aioNameMap, Map<String, String> batchBundleMap) {
		GetBundlesReq req=new GetBundlesReq();
		ArrayList<String> aioIds=new ArrayList<>();
		for(String batchId:batchBundleMap.keySet()){
			aioIds.add(batchBundleMap.get(batchId));
		}
		req.setBundleIds(aioIds);
		ClientResponse response = WebUtils.INSTANCE.doCall(SUBSCRIPTION_ENDPOINT
				+ "bundle/getBundlesNames",HttpMethod.POST, gson.toJson(req));
		String jsonString = response.getEntity(String.class);
		Type listType=new TypeToken<List<BundleInfo>>(){}.getType();
		List<BundleInfo> bundleInfos=gson.fromJson(jsonString,listType);
		bundleInfos.forEach(bundleInfo -> aioNameMap.put(bundleInfo.getId(),bundleInfo.getTitle()));
		logger.info("aio name map populated as "+aioNameMap);
		logger.info("populateAIONames from subscription is "+gson.toJson(bundleInfos));
	}

	public Map<String, List<CurriculumProgressRes>> getBatchCurriculumProgress(List<String> batchIds) throws BadRequestException {
		Map<String, List<CurriculumProgressRes>> batchCurriculumProgressRes = new HashMap<>();
		if (CollectionUtils.isEmpty(batchIds)) {
			return batchCurriculumProgressRes;
		} else if (batchIds.size() > 5) {
			throw new BadRequestException(ErrorCode.IMPERMISSIBLE_SIZE_PER_REQUEST, "Impermissible request size");
		}

		// fetch cached progress from redis
		Map<String, List<CurriculumProgressRes>> cachedProgress = redisDAO.getBatchCurriculumProgress(batchIds);
		batchIds = batchIds.stream().filter(bId -> !cachedProgress.containsKey(bId)).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(batchIds)) {
			return cachedProgress;
		}

		// fetch curriculum progress
		final List<String> includeFieldsCurriculum = Arrays.asList(Curriculum.Constants.BOARD_ID, Curriculum.Constants.PARENT_ID,
				Curriculum.Constants.ROOT_ID, Curriculum.Constants._ID, Curriculum.Constants.STATUS, Curriculum.Constants.CONTEXT_ID);
		List<Curriculum> fetchedCurricula = curriculumDAO.getCurriculumByBatchIds(batchIds, includeFieldsCurriculum);
		batchCurriculumProgressRes =  Optional.of(fetchedCurricula).orElseGet(ArrayList::new)
				.stream().filter(Objects::nonNull)
				.collect(Collectors.groupingBy(Curriculum::getContextId,
						Collectors.collectingAndThen(Collectors.toList(), this::getBatchChapterProgress)));

		// set progress in redis
		redisDAO.setBatchCurriculumProgress(batchCurriculumProgressRes);
		batchCurriculumProgressRes.putAll(cachedProgress);

		return batchCurriculumProgressRes;
	}

	private List<CurriculumProgressRes> getBatchChapterProgress(final List<Curriculum> batchCurriculum) {

		if (CollectionUtils.isEmpty(batchCurriculum)) {
			return new ArrayList<>();
		}

		Optional<String> rootNodeId = batchCurriculum.stream()
				.filter(c -> Objects.nonNull(c.getRootId()))
				.map(Curriculum::getRootId)
				.findFirst();

		Map<String, CurriculumProgressRes> subjectProgressMap = new HashMap<>();
		if (!rootNodeId.isPresent()) {
			return new ArrayList<>();
		}
		String rootId = rootNodeId.get();
		Set<String> subjectNodeIds = batchCurriculum.stream()
				.filter(c -> Objects.nonNull(c.getParentId()) && c.getParentId().equals(rootId))
				.map(Curriculum::getId)
				.collect(Collectors.toSet());

		batchCurriculum.stream()
				.filter(c -> subjectNodeIds.contains(c.getParentId()) && c.getStatus() != null)
				.forEach(chapterNode -> {
					String subjectNodeId = chapterNode.getParentId();
					CurriculumProgressRes subjectCounts;
					if (subjectProgressMap.containsKey(subjectNodeId)) {
						subjectCounts = subjectProgressMap.get(subjectNodeId);
					} else {
						subjectCounts = new CurriculumProgressRes();
						subjectCounts.setBoardId(chapterNode.getBoardId());
					}

					if (CurriculumStatus.DONE.equals(chapterNode.getStatus()))
						subjectCounts.incrCompletedChapters();
					subjectCounts.incrTotalChapters();
					subjectProgressMap.put(subjectNodeId, subjectCounts);
				});

 		return new ArrayList<>(subjectProgressMap.values());
	}

	public void removeBatchCurriculumProgressCache(String message) {
		final Type listType = new TypeToken<List<String>>() {
		}.getType();
		List<String> contextIds = gson.fromJson(message, listType);
		redisDAO.deleteBatchCurriculumProgress(contextIds);
	}

}
