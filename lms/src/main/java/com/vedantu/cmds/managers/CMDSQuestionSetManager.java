package com.vedantu.cmds.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.dao.CMDSQuestionDAO;
import com.vedantu.cmds.dao.CMDSQuestionSetDAO;
import com.vedantu.cmds.dao.TopicTreeDAO;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSQuestionSet;
import com.vedantu.cmds.entities.CurriculumTopicTree;
import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.enums.Indexable;
import com.vedantu.cmds.enums.MakePageLive;
import com.vedantu.cmds.fs.parser.QuestionRepositoryDocParser;
import com.vedantu.cmds.pojo.CMDSQuestionIdInfo;
import com.vedantu.cmds.pojo.EntireQuestion;
import com.vedantu.cmds.pojo.ParseQuestionMetadata;
import com.vedantu.cmds.request.CheckerQuestionSetOrTestFilterRequest;
import com.vedantu.cmds.request.ConfirmQuestionsReq;
import com.vedantu.cmds.request.DeleteQuestionSetsReq;
import com.vedantu.cmds.request.GetQuestionSetReq;
import com.vedantu.cmds.request.GetQuestionSetsReq;
import com.vedantu.cmds.request.QuestionApprovalRequest;
import com.vedantu.cmds.request.UpdateQuestionSetReq;
import com.vedantu.cmds.request.UploadQuestionSetFileReq;
import com.vedantu.cmds.response.CMDSQuestionSetCheckerResponse;
import com.vedantu.cmds.response.CMDSQuestionSetInfo;
import com.vedantu.cmds.response.ConfirmQuestionSetUploadRes;
import com.vedantu.cmds.response.UploadQuestionSetFileRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.VedantuRecordState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Random;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class CMDSQuestionSetManager {

    @Autowired
    private CMDSQuestionManager questionManager;

    @Autowired
    private CMDSQuestionSetDAO questionSetDAO;

    @Autowired
    private CMDSQuestionDAO questionDAO;

    @Autowired
    private QuestionRepositoryDocParser questionRepositoryDocParser;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private TopicTreeDAO topicTreeDAO;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private FosUtils fosUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CMDSQuestionManager.class);

    private String GROWTH_ENDPOINT;

    private static final Gson gson = new Gson();

    public CMDSQuestionSetManager() {

        GROWTH_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT");
    }

    public UploadQuestionSetFileRes createQuestionSet(List<String> questionIds) throws Exception {
        if (CollectionUtils.isEmpty(questionIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "QuestionIds list is empty");
        } else {
            CMDSQuestionSet questionSet = new CMDSQuestionSet();
            questionSet.numberOfQuestionsComplete = questionIds.size();
//            questionSet.setNumberOfQuestionsComplete(questionIds.size());
            List<CMDSQuestion> allQuestion = new ArrayList<>();
            for (String questionId : questionIds) {
                CMDSQuestion q = questionDAO.getById(questionId);
                allQuestion.add(q);
            }
            CMDSQuestionSetInfo questionSetInfo = new CMDSQuestionSetInfo(allQuestion, questionSet);
            questionSet.questionIds = questionIds;
//            questionSet.setQuestionIds(questionIds);
            questionSet.recordState = VedantuRecordState.TEMPORARY;
//            questionSet.setRecordState(VedantuRecordState.TEMPORARY);
            questionSetDAO.update(questionSet);
            UploadQuestionSetFileRes response = new UploadQuestionSetFileRes(null, null, null, questionSet.getId());
            questionRepositoryDocParser.populateAWSUrls(questionSetInfo.getQuestions());
            response.setQuestionSetInfo(questionSetInfo);
            return response;
        }
    }

    public UploadQuestionSetFileRes uploadQuestionFile(UploadQuestionSetFileReq request) throws Exception {

        logger.info("uploadQuestionFile: Name of the file " + request.questionSetFileName);

        List<BaseTopicTree> parentNodes = topicTreeDAO.getNamesOfFirstLevelNodes();
        List<String> parentSubjectNames=
                Optional.ofNullable(parentNodes)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(BaseTopicTree::getName)
                        .collect(Collectors.toList());


        List<CMDSQuestion> allQuestions;
        CMDSQuestionSet questionSet = new CMDSQuestionSet();
        ParseQuestionMetadata globalMetadata = new ParseQuestionMetadata();

        allQuestions = questionRepositoryDocParser.getContent(request.questionSetFileName, request.questionSetFile,
                request.userId, globalMetadata, request.getAccessLevel(), request.getMetadata());

        List<String> collectedQuestionIds = new ArrayList<>();

        questionSet.recordState = VedantuRecordState.TEMPORARY;
        questionSet.name = request.questionSetFileName;

        questionSet.difficulty = globalMetadata.difficulty;
        questionSet.name = globalMetadata.title;
        questionSet.questionSetType = globalMetadata.type;
        questionSet.tags = globalMetadata.tags;

        questionSet.fileName = request.questionSetFileName;
        questionSet.numberOfQuestionsComplete = allQuestions.size();
        questionSet.setLastUpdatedBy(request.userId);
        questionSet.setAccessLevel(request.getAccessLevel());

        Set<String> allTopics = new HashSet<>();
        for (CMDSQuestion question : allQuestions) {

            int index = allQuestions.indexOf(question) + 1;

            // Type is not mandatory
            if (question.getType() == null) {
                throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "Question Type cannot be empty for question: " + Integer.toString(index));
            }

            if (Indexable.YES.equals(question.getIndexable())) {
                question.setIndexable(Indexable.YES);

                if(MakePageLive.YES.equals(question.getMakePageLive())){
                    question.setMakePageLive(MakePageLive.YES);
                }else{
                    question.setMakePageLive(MakePageLive.NO);
                }

                if (question.getQuestionBody() == null
                        || StringUtils.isEmpty(question.getQuestionBody().getNewText().trim())) {
                    throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "Question cannot be empty for question: " + index);
                }

                if (question.getSolutionInfo() == null
                        || CollectionUtils.isEmpty(question.getSolutionInfo().getSolutions())
                        || StringUtils.isEmpty(question.getSolutionInfo().getSolutions().get(0).getNewText().trim())) {
                    throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "Solution cannot be empty for question: " + index);
                }

            } else {
                question.setIndexable(Indexable.NO);
            }

            Set<String> overallMainTags = new HashSet<>();

            Set<String> inputTopics = new HashSet<>();

            // check if topics or subtopics exist in BaseTopicTree
            logger.info(question.getSubtopics());
            logger.info(question.getTopics());

            if (question.getSubtopics().size() > 0) {
                inputTopics.addAll(question.getSubtopics());
            } else if (question.getTopics().size() > 0) {
                inputTopics.addAll(question.getTopics());
            } else {
                throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "Topic or Subtopic not present for Question No:" + index);
            }
            logger.info(inputTopics);

            if (inputTopics.size() == 0) {
                throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "Topic or Subtopic not present for Question No: " + index);
            }
            allTopics.addAll(inputTopics);

            Set<String> parentTopics = new HashSet<>();
            for (String topic : inputTopics) {
                if (topic.contains("$")) {
                    topic = topic.replace('$', ',');
                }

                BaseTopicTree treeForParents = topicTreeDAO.getBaseTopicTreeNodeByField("name", topic);
                if (treeForParents != null) {
                    if(ArrayUtils.isNotEmpty(treeForParents.getParents())) {
                        parentTopics.addAll(treeForParents.getParents());
                    }
                } else {
                    logger.info("uploadQuestionFile: Topic: " + topic + " not present in BaseTree for Question No: " + index);
                    throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "Topic: " + topic + " not present in BaseTree for Question No: " + index);
                }
            }
            Set<String> inputTopicsDollarCheck = inputTopics
                    .stream()
                    .map(this::replaceDollarWithComma)
                    .peek(logger::info)
                    .collect(Collectors.toSet());

            overallMainTags.addAll(inputTopicsDollarCheck);
            overallMainTags.addAll(parentTopics);

            logger.info("QUESTION");
            logger.info(question);


            if (question.getTargetGrade().size() > 0) {
                for (String slug : question.getTargetGrade()) {
                    String[] subTags = slug.split("-");
                    if (subTags.length == 2) {
                        // Not checking with Curriculum for now
                        List<CurriculumTopicTree> cT = topicTreeDAO.verifyCurriculumNodeAndChild(subTags);
                        if (cT.size() != 1) {
                            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "TargetGrade does not exist in CurriculumTree for Question No: " + index);
                        }
                        overallMainTags.add(slug);
                        for (String subTag : subTags) {
                            String subSlug = StringUtils.trimToEmpty(subTag);
                            if (StringUtils.isNotEmpty(subSlug)) {
                                overallMainTags.add(subSlug);
                            }
                        }
                    } else {
                        throw new EntireQuestion.InvalidMetadataException("Each TargetGrade should have - separated values. Problem in Question No: " + index);
                    }
                }

            } else {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "TargetGrade not present for Question No :" + index);
            }

            Set<String> analysisTagsDollarRemove = question.getAnalysisTags()
                    .stream()
                    .map(this::replaceDollarWithComma)
                    .peek(logger::info)
                    .collect(Collectors.toSet());
            question.setAnalysisTags(analysisTagsDollarRemove);
            logger.info("analysisTagsDollarChanges " + analysisTagsDollarRemove);
            question.setMainTags(overallMainTags);
            List<String> temporaryParentNodes=new ArrayList<>(parentSubjectNames);
            temporaryParentNodes.retainAll(overallMainTags);
            if(temporaryParentNodes.size()>1){
                throw new BadRequestException(ErrorCode.MULTIPLE_SUBJECT_FOR_QUESTION,"Multiple subject present in a question for question "+question);
            }
            question.setSubjects(temporaryParentNodes);
            if (ArrayUtils.isNotEmpty(question.getAnalysisTags()) && !question.getMainTags().containsAll(analysisTagsDollarRemove)) {
                logger.info("Analysis Tag is not from Topic Tree (Main tags)" + question.getMainTags() + "(Atags)" + analysisTagsDollarRemove + "for qno" + Integer.toString(index));
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, " Analysis Tag " + analysisTagsDollarRemove + "is/are not one of the subtopic(s) of"+ inputTopics);
            }
        }
        Set<String> allTopicsDollarRemove = allTopics
                .stream()
                .map(this::replaceDollarWithComma)
                .peek(logger::info)
                .collect(Collectors.toSet());
        logger.info("CMDS-TOPICS");
        logger.info(allTopics.size());
        List<BaseTopicTree> bT = topicTreeDAO.getNodesForTopics(allTopicsDollarRemove);
        logger.info(bT.size());

        if (bT.size() != allTopics.size()) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Some topic or sub-topic in TopicTree does not exist");
        }

        questionSetDAO.update(questionSet);

        for (CMDSQuestion question : allQuestions) {
            question.recordState = VedantuRecordState.TEMPORARY;
            question.setQuestionSetId(questionSet.getId());
            question.setQuestionSetName(questionSet.name);
            question.setLastUpdatedBy(questionSet.getLastUpdatedBy());
            question.setAccessLevel(request.getAccessLevel());
            questionDAO.save(question);
            logger.info("Saved question is {}", question);

            collectedQuestionIds.add(question.getId());
            questionSet.addGrades(question.grades);
            questionSet.addTargets(question.targets);
            questionSet.addTags(question.tags);

            if (StringUtils.isNotEmpty(question.getId()) && Objects.nonNull(question.getIndexable()) &&
                    question.getIndexable() == Indexable.YES) {
                ClientResponse resp;
                resp = WebUtils.INSTANCE.doCall(GROWTH_ENDPOINT + "/generate-category-page",
                        HttpMethod.POST, gson.toJson(questionManager.getCMDSQuestionDto(question)), true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            }
        }

        // Update AWS Image urls
        questionRepositoryDocParser.populateAWSUrls(allQuestions);

        questionSet.questionIds = collectedQuestionIds;
        questionSetDAO.update(questionSet);

        String prefixFileName = null;
/*
        try {
			LocalFileSystemHandler tempFs = FileSystemFactory.INSTANCE.getTempFS();
			prefixFileName = UUID.randomUUID().toString();
			String tempQuestionSetFileBeforeConfirmation = tempFs.getFilePath(
					CMDSEntityType.CMDSQUESTIONSET.name().toLowerCase(), prefixFileName + request.questionSetFileName);
			File nF = new File(tempQuestionSetFileBeforeConfirmation);
			FileUtils.copyFile(request.questionSetFile, nF);
			request.questionSetFile.delete();
		} catch (IOException e) {
			logger.error(e.getMessage(), e);
			throw new VException(ErrorCode.CMDS_UPLOAD_ERROR, "Error deleting the temp file : " + e.getMessage());
		}
*/

        UploadQuestionSetFileRes response = new UploadQuestionSetFileRes(globalMetadata, questionSet.fileName,
                prefixFileName, questionSet.getId());

        response.setQuestionSetInfo(new CMDSQuestionSetInfo(allQuestions, questionSet.type,
                questionSet.numberOfQuestionsComplete, questionSet.fileName, questionSet.difficulty));

        logger.info("Response : " + response);
        return response;
    }

    private String replaceDollarWithComma(String topic) {
        return topic.replaceAll("\\$", ",");
    }

    public ConfirmQuestionSetUploadRes confirmQuestions(ConfirmQuestionsReq confirmQuestionsReq)
            throws VException {
        logger.info("Request : " + confirmQuestionsReq.toString());

        // Update the question set to processing state
        CMDSQuestionSet questionSet = questionSetDAO.updateToProcessing(confirmQuestionsReq.getQuestionSetId());
        logger.debug("Updated record state to processing");
        questionSet.name = StringUtils.isNotEmpty(confirmQuestionsReq.getProvidedQuestionSetName())
                ? confirmQuestionsReq.getProvidedQuestionSetName()
                : questionSet.name;

        // Confirm questions
        List<CMDSQuestion> questions = questionDAO.getByIdsOrdered(questionSet.questionIds, null);

        logger.info("QUESTION-SET-QUESTIONS: " + questions);

        Map<String, CMDSQuestionIdInfo> requestQuestionEntries = getRequestQuestionInfoMap(
                confirmQuestionsReq.getQuestionIdInfos());
        List<String> confirmedQuestionIdList = new ArrayList<>();
        for (CMDSQuestion cmdsQuestion : questions) {
            cmdsQuestion.setQuestionSetName(StringUtils.isNotEmpty(confirmQuestionsReq.getProvidedQuestionSetName())
                    ? confirmQuestionsReq.getProvidedQuestionSetName()
                    : questionSet.name);
            cmdsQuestion.recordState = VedantuRecordState.ACTIVE;
            if (requestQuestionEntries.containsKey(cmdsQuestion.getId())) {
                CMDSQuestionIdInfo requestQuestionIdInfo = requestQuestionEntries.get(cmdsQuestion.getId());
                cmdsQuestion.addTags(requestQuestionIdInfo.getTags());
                cmdsQuestion.setMarks(requestQuestionIdInfo.getDefaultMarks());
            }

            try {
                questionManager.addQuestion(confirmQuestionsReq.getUserId(), cmdsQuestion);
                logger.info("confirmed question : " + cmdsQuestion.toString());
                confirmedQuestionIdList.add(cmdsQuestion.getId());
                logger.info("confirmed list : " + confirmedQuestionIdList);
            } catch (Exception e) {
                logger.debug("Can not save question " + cmdsQuestion);
            }
        }

        // Update question set
        questionSet.recordState = VedantuRecordState.ACTIVE;
        questionSet.boardIds = confirmQuestionsReq.getBoardIds();
        questionSet.grades = confirmQuestionsReq.getGrades();
        questionSet.targetIds = confirmQuestionsReq.getTargetIds();
        questionSet.tags = confirmQuestionsReq.getTags();
        questionSet.questionIds = new ArrayList<>(confirmedQuestionIdList);
        questionSet.numberOfQuestionsComplete = confirmedQuestionIdList.size();
        questionSetDAO.update(questionSet);
        logger.info("Updated question set : " + questionSet.toString());

        ConfirmQuestionSetUploadRes response = new ConfirmQuestionSetUploadRes();
        response.isConfirmed = true;
        response.questionsSetName = questionSet.name;
        response.id = questionSet.getId();
        logger.info("Response : " + response.toString());
        return response;
    }

    public void deleteQuestionSet(DeleteQuestionSetsReq req) throws VException {
        CMDSQuestionSet cmdsQuestionSet = questionSetDAO.getById(req.getQuestionSetId());
        if (cmdsQuestionSet == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_SET_NOT_FOUND,
                    "Question set not found for id : " + req.toString());
        }

        // Mark the question set as deleted
        cmdsQuestionSet.setLastUpdated(System.currentTimeMillis());
        cmdsQuestionSet.setLastUpdatedBy(req.getCallingUserId());
        cmdsQuestionSet.recordState = VedantuRecordState.DELETED;
        questionSetDAO.update(cmdsQuestionSet);

        // Mark the questions as inactive
        List<String> questionIds = cmdsQuestionSet.questionIds;
        List<CMDSQuestion> questions = questionDAO.getByIds(questionIds, null);
        if (!CollectionUtils.isEmpty(questions)) {
            for (CMDSQuestion question : questions) {
                question.setRecordState(VedantuRecordState.DELETED);
                question.setLastUpdated(System.currentTimeMillis());
                question.setLastUpdatedBy(req.getCallingUserId());
                questionDAO.save(question);
            }
        }

        // Mark the tests as inactive
        //testManager.deleteTests(req.getQuestionSetId(), req.getCallingUserId());
    }

    public CMDSQuestionSetInfo getQuestionSetById(GetQuestionSetReq request) throws VException {
        logger.info("Request : " + request.toString());
        VedantuRecordState state = VedantuRecordState.ACTIVE;
        if (request.getRecordState() != null) {
            state = request.getRecordState();
        }

        CMDSQuestionSet questionSet = questionSetDAO.getById(request.getQuestionSetId());

        if (questionSet == null) {
            throw new VException(ErrorCode.CMDS_QUESTION_SET_NOT_FOUND,
                    "Question not found for id : " + request.getQuestionSetId());
        }

        List<String> questionIds = questionSet.questionIds;
        if (request.validLimits()) {
            questionIds = questionSet.questionIds.subList(request.getStart(), request.getStart() + request.getLimit());
        }

        List<CMDSQuestion> questions = questionDAO.getByIds(questionIds, state);
        CMDSQuestionSetInfo questionSetInfo = new CMDSQuestionSetInfo(questions, questionSet);
        logger.info("Response : " + questionSetInfo.toString());
        return questionSetInfo;
    }

    public List<CMDSQuestionSet> getQuestionSets(GetQuestionSetsReq request) throws VException {
        logger.info("Request : " + request.toString());
        List<CMDSQuestionSet> results = questionSetDAO.getQuestionSets(request);
        return results;
    }

    public boolean updateQuestionSet(UpdateQuestionSetReq request) throws VException {
        CMDSQuestionSet questionSet = questionSetDAO.getById(request.getId());
        if (questionSet == null) {
            throw new VException(ErrorCode.CMDS_CONTENT_NOT_FOUND,
                    "Content not found for request : " + request.toString());
        }

        questionSetDAO.update(questionSet);
        return true;
    }

    private Map<String, CMDSQuestionIdInfo> getRequestQuestionInfoMap(List<CMDSQuestionIdInfo> idInfos) {
        Map<String, CMDSQuestionIdInfo> questionMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(idInfos)) {
            for (CMDSQuestionIdInfo idInfo : idInfos) {
                questionMap.put(idInfo.getQuestionId(), idInfo);
            }
        }

        return questionMap;
    }

    public CMDSQuestionSet getQuestionSetById(String id) {
        return questionSetDAO.getById(id);
    }

    public UploadQuestionSetFileRes getQuestionSetUploadResponse(String questionSetId) {
        CMDSQuestionSet cmdsQuestionSet = getQuestionSetById(questionSetId);

        String questionSetName = cmdsQuestionSet.getName();
        List<CMDSQuestion> questionList = questionDAO.getByIds(cmdsQuestionSet.getQuestionIds(), VedantuRecordState.TEMPORARY);


        String type = cmdsQuestionSet.getType();
        String fileName = cmdsQuestionSet.getFileName();
        Difficulty difficulty = cmdsQuestionSet.getDifficulty();
        Integer numberOfQuestionsComplete = cmdsQuestionSet.getNumberOfQuestionsComplete();

        CMDSQuestionSetInfo cmdsQuestionSetInfo = new CMDSQuestionSetInfo(questionList, type, numberOfQuestionsComplete, fileName, difficulty);


        ParseQuestionMetadata parseQuestionMetadata = new ParseQuestionMetadata(cmdsQuestionSet.getName(), cmdsQuestionSet.getSubject(), new ArrayList<String>(cmdsQuestionSet.getTopics()), new ArrayList<String>(cmdsQuestionSet.getSubtopics()),
                new ArrayList<String>(cmdsQuestionSet.getTags()), new ArrayList<String>(cmdsQuestionSet.getGrades()), null, new ArrayList<String>(cmdsQuestionSet.getTargets()), null, null, null, null, null, null);

        UploadQuestionSetFileRes response = new UploadQuestionSetFileRes(parseQuestionMetadata, questionSetName, "", questionSetId, cmdsQuestionSetInfo);

        return response;
    }

    public String generateToken() {
        long lowerLimit = 123456712L;
        long upperLimit = 234567892L;
        Random r = new Random();
        long number = lowerLimit + ((long) (r.nextDouble() * (upperLimit - lowerLimit)));

        return Long.toString(number);
    }

    public void updateApprovalStatus(QuestionApprovalRequest request) {
        questionSetDAO.updateApprovalStatus(request);
    }

    public List<CMDSQuestionSetCheckerResponse> filterQuestionSetCheckerData(CheckerQuestionSetOrTestFilterRequest request) throws BadRequestException {
        request.verify();

        if(Objects.nonNull(request.getTestId()))
            throw new BadRequestException(ErrorCode.TEST_ID_FILTER_NOT_APPLICABLE_FOR_QUESTION_SET,"QuestionSet cannot be filtered with testId");

        List<CMDSQuestionSet> questionSets = questionSetDAO.filterQuestionSetCheckerData(request);

        Set<String> userIds=new HashSet<>();
        Set<String> makers = questionSets.stream().map(CMDSQuestionSet::getCreatedBy).collect(Collectors.toSet());
        Set<String> checkers=questionSets.stream().map(CMDSQuestionSet::getAssignedChecker).map(String::valueOf).collect(Collectors.toSet());
        userIds.addAll(makers);
        userIds.addAll(checkers);

        Long currentChecker=request.getCallingUserId();

        Map<String, UserBasicInfo> userBasicInfosMap = fosUtils.getUserBasicInfosMap(userIds, false);

        List<CMDSQuestionSetCheckerResponse> checkerResponses =
                questionSets.stream()
                            .map(this::getCMDSTestCheckerResponse)
                            .map(res->res.populateMakerCheckerData(userBasicInfosMap,currentChecker))
                            .collect(Collectors.toList());
        return checkerResponses;
    }

    private CMDSQuestionSetCheckerResponse getCMDSTestCheckerResponse(CMDSQuestionSet questionSet) {
        return mapper.map(questionSet, CMDSQuestionSetCheckerResponse.class);
    }

    @Deprecated
    public File getFileFromBody(HttpServletRequest request) throws IOException {
        File file = new File("temp_" + UUID.randomUUID());
        logger.info("Size : " + file.length());
        @SuppressWarnings("resource")
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ServletInputStream in = request.getInputStream();
        int b = -1;
        while ((b = in.read()) != -1) {
            fileOutputStream.write(b);
        }
        return file;
    }
}
