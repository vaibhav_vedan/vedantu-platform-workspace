package com.vedantu.feed.dao;

import com.vedantu.feed.enums.FeedType;
import com.vedantu.feed.iface.BaseFeed;
import com.vedantu.feed.request.GetFeedReq;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

@Service
public class FeedDao extends AbstractMongoDAO {

    private final Logger logger = LogFactory.getLogger(FeedDao.class);

    private MongoClientFactory mongoClientFactory;



    @Autowired
    public FeedDao(MongoClientFactory mongoClientFactory) {
        this.mongoClientFactory = mongoClientFactory;
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public void save(BaseFeed feed) {
        saveEntityInAnnotatedCollection(feed, null);
    }

    public List<? extends BaseFeed> get(GetFeedReq req) {
        Query query = new Query();

        if (ArrayUtils.isNotEmpty(req.getGrades())) {
            query.addCriteria(Criteria.where(BaseFeed.Constants.GRADES).in(req.getGrades()));
        }

        if (req.getModule() != null) {
            query.addCriteria(Criteria.where(BaseFeed.Constants.MODULE).is(req.getModule()));
        }

        if (req.getFeedType() != null) {
            query.addCriteria(Criteria.where(BaseFeed.Constants.FEED_TYPE).is(req.getFeedType()));
        }

        if (req.getPublished() != null) {
            query.addCriteria(Criteria.where(BaseFeed.Constants.PUBLISHED).is(req.getPublished()));
        }
        query.addCriteria(Criteria.where(BaseFeed.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.with(Sort.by(Sort.Direction.DESC, BaseFeed.Constants.LAST_UPDATED));

        setFetchParameters(query, req);
        List<Class<? extends BaseFeed>> classes = getClasses(req.getFeedType());
        List<? extends BaseFeed> feeds = new ArrayList<>();
        for (Class<? extends BaseFeed> aClass : classes) {
            //noinspection rawtypes
            List baseFeeds = runQueryForAnnotatedCollection(query, aClass);
            //noinspection unchecked
            feeds.addAll(baseFeeds);
        }
        return feeds;
    }

    private List<Class<? extends BaseFeed>> getClasses(FeedType feedType) {
        if (feedType != null) {
            return Collections.singletonList(feedType.getFeedClass());
        } else {
            List<Class<? extends BaseFeed>> classes = new ArrayList<>();
            for (FeedType value : FeedType.values()) {
                classes.add(value.getFeedClass());
            }
            return classes;
        }
    }

    public boolean publishFeed(String id, Boolean publish) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseFeed.Constants._ID).is(id));
        Update update = new Update();
        update.set(BaseFeed.Constants.PUBLISHED, Objects.nonNull(publish) && publish);
        logger.info("Query: " + query);
        logger.info("Update: " + update);
        return updateFirstForAnnotatedCollection(query, update, BaseFeed.class) == 1;
    }

    public boolean deleteFeeds(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BaseFeed.Constants._ID).is(id));
        Update update = new Update();
        update.set(BaseFeed.Constants.ENTITY_STATE, EntityState.DELETED);
        logger.info("Query: " + query);
        logger.info("Update: " + update);
        return updateFirstForAnnotatedCollection(query, update, BaseFeed.class) == 1;
    }
}
