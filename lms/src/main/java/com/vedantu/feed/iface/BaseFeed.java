package com.vedantu.feed.iface;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.vedantu.feed.entity.Carousel;
import com.vedantu.feed.enums.FeedModule;
import com.vedantu.feed.enums.FeedType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * @author mano
 */
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.EXISTING_PROPERTY,
        property = "feedType", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = Carousel.class, name = "CAROUSEL"),
})

@Getter
@Setter
@Document(collection = "Feeds")
public abstract class BaseFeed extends AbstractMongoStringIdEntity {
    private List<Integer> grades;
    private String title;
    private FeedModule module;
    private boolean published;

    public abstract FeedType getFeedType();

    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String MODULE = "module";
        public static final String TITLE = "title";
        public static final String GRADES = "grades";
        public static final String FEED_TYPE = "feedType";
        public static final String PUBLISHED = "published";
    }
}
