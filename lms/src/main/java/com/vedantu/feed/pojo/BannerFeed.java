package com.vedantu.feed.pojo;

import lombok.Data;

@Data
public class BannerFeed {
    private String title;
    private String redirectionUrl;
    private String contextUrl;
    private String imgWeb;
    private String imgMobile;
}
