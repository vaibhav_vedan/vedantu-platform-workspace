package com.vedantu.feed.entity;

import com.vedantu.feed.enums.FeedType;
import com.vedantu.feed.iface.BaseFeed;
import com.vedantu.feed.pojo.BannerFeed;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Carousel extends BaseFeed {
    private FeedType feedType = FeedType.CAROUSEL;
    private List<BannerFeed> banners;
    private boolean autoRotate;
    private int frequency;
}
