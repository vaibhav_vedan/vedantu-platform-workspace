package com.vedantu.feed.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class PublishFeedReq extends AbstractFrontEndReq {

    private String id;
    private Boolean publish;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(id)) {
            errors.add("id_empty");
        }

        if (publish == null) {
            errors.add("publish");
        }
        return errors;
    }
}
