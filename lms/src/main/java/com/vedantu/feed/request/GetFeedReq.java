package com.vedantu.feed.request;

import com.vedantu.feed.enums.FeedModule;
import com.vedantu.feed.enums.FeedType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class GetFeedReq extends AbstractFrontEndListReq {
    private List<Integer> grades;
    private FeedType feedType;
    private FeedModule module;
    private Boolean published;

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (ArrayUtils.isEmpty(grades) && feedType == null) {
            errors.add("query empty");
        }

        if (module == null) {
            errors.add("module");
        }

        return errors;
    }
}
