package com.vedantu.feed.enums;

import com.vedantu.feed.entity.Carousel;
import com.vedantu.feed.iface.BaseFeed;

public enum FeedType {
    /**
     * Carousel for courses
     */
    CAROUSEL(Carousel.class),


    ;

    private Class<? extends BaseFeed> feedClass;

    FeedType(Class<? extends BaseFeed> feedClass) {
        this.feedClass = feedClass;
    }

    public Class<? extends BaseFeed> getFeedClass() {
        return feedClass;
    }
}
