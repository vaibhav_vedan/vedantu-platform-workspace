package com.vedantu.feed.controller;


import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.feed.iface.BaseFeed;
import com.vedantu.feed.manager.FeedsManager;
import com.vedantu.feed.request.GetFeedReq;
import com.vedantu.feed.request.PublishFeedReq;
import com.vedantu.homefeed.controllers.HomeFeedController;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/feeds")
public class FeedsController {

    private final Logger logger = LogFactory.getLogger(HomeFeedController.class);

    private HttpSessionUtils sessionUtils;
    private FosUtils fosUtils;
    private FeedsManager feedsManager;
    private Gson gson = new Gson();

    @Autowired
    public FeedsController(HttpSessionUtils sessionUtils, FosUtils fosUtils, FeedsManager feedsManager) {
        this.sessionUtils = sessionUtils;
        this.fosUtils = fosUtils;
        this.feedsManager = feedsManager;
    }


    @RequestMapping(value = {"/add", "/edit"}, method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addFeeds(@RequestBody BaseFeed feed) throws VException {
        logger.info("GSON: " + gson.toJson(feed));
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.ADMIN), true);
        feedsManager.addOrEditFeed(feed);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = {"/get"}, method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<? extends BaseFeed> getFeeds(@RequestBody GetFeedReq req) throws VException {
        logger.info("GSON: " + gson.toJson(req));
        req.verify();
        return feedsManager.getFeeds(req);
    }

    @RequestMapping(value = {"/publish"}, method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse publishFeeds(@RequestBody PublishFeedReq req) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.ADMIN), true);
        req.verify();
        logger.info("GSON: " +req.getId());
        return feedsManager.publishFeeds(req.getId(), req.getPublish());
    }

    @RequestMapping(value = {"/delete/{id}"}, method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse deleteFeeds(@PathVariable(value = "id") String id) throws VException {
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.ADMIN), true);
        logger.info("Delete: " +id);
        return feedsManager.deleteFeeds(id);
    }
}
