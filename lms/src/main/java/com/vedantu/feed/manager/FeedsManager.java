package com.vedantu.feed.manager;

import com.vedantu.feed.dao.FeedDao;
import com.vedantu.feed.iface.BaseFeed;
import com.vedantu.feed.request.GetFeedReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeedsManager {

    private final Logger logger = LogFactory.getLogger(FeedsManager.class);

    private FeedDao feedDao;

    @Autowired
    public FeedsManager(FeedDao feedDao) {
        this.feedDao = feedDao;
    }

    public void addOrEditFeed(BaseFeed feed) {
        feedDao.save(feed);
    }

    public List<? extends BaseFeed> getFeeds(GetFeedReq req) {
        return feedDao.get(req);
    }

    public PlatformBasicResponse publishFeeds(String id, Boolean publish) {
        boolean publishFeed = feedDao.publishFeed(id, publish);
        return new PlatformBasicResponse(publishFeed, "", "");
    }

    public PlatformBasicResponse deleteFeeds(String id) {
        boolean deleteFeeds = feedDao.deleteFeeds(id);
        return new PlatformBasicResponse(deleteFeeds, "", "");
    }
}
