package com.vedantu.moodle;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.dao.CMDSTestAttemptDAO;
import com.vedantu.cmds.dao.CurriculumDAO;
import com.vedantu.cmds.dao.TeacherContentDashboardInfoDAO;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.entities.Curriculum;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.managers.AwsSNSManager;
import com.vedantu.cmds.managers.AwsSQSManager;
import com.vedantu.cmds.managers.CMDSTestAttemptManager;
import com.vedantu.cmds.managers.CMDSTestManager;
import com.vedantu.cmds.managers.challenges.CommunicationManager;
import com.vedantu.cmds.pojo.CMDSTestState;
import com.vedantu.cmds.pojo.StudentContentStateCountPojo;
import com.vedantu.cmds.pojo.TeacherContentDashboardIncrementInfo;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.*;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.lms.cmds.pojo.*;
import com.vedantu.lms.cmds.request.ShareContentAndSendMailRes;
import com.vedantu.lms.cmds.request.ShareContentReq;
import com.vedantu.lms.cmds.request.StudentBatchProgressReq;
import com.vedantu.lms.cmds.request.TeacherBatchProgressReq;
import com.vedantu.lms.request.ContentInfoReq;
import com.vedantu.lms.request.UnshareContentReq;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.dao.GroupDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.entity.Group;
import com.vedantu.moodle.pojo.ContentInfoCommunicationType;
import com.vedantu.moodle.pojo.EvaluateNowLink;
import com.vedantu.moodle.pojo.request.GetContentsReq;
import com.vedantu.moodle.pojo.request.GetTeacherCourses;
import com.vedantu.moodle.pojo.request.GetTestContentReq;
import com.vedantu.moodle.pojo.request.SessionDate;
import com.vedantu.moodle.pojo.request.SessionDateReq;
import com.vedantu.moodle.pojo.response.*;
import com.vedantu.moodle.response.HomepageTestResponse;
import com.vedantu.notification.pojos.MoodleContentInfo;
import com.vedantu.notification.requests.SendMoodleNotificationsReq;
import com.vedantu.onetofew.pojo.StudentContentPojo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.BatchCurriculumStatus;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.scheduling.CommonCalendarUtils;
import com.vedantu.util.threadutil.CachedThreadPoolUtility;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static com.vedantu.moodle.entity.ContentInfo.Constants.*;

@Service
public class ContentInfoManager {

    @Autowired
    private ContentInfoDAO contentInfoDAO;

    @Autowired
    private MoodleDataManager moodleDataManager;

    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private CurriculumDAO curriculumDAO;

    @Autowired
    private CMDSTestManager cmdsTestManager;

    @Autowired
    private CMDSTestAttemptManager cmdsTestAttemptManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AwsSNSManager snsManager;

    @Autowired

    private AwsSQSManager sqsManager;

    @Autowired
    private CachedThreadPoolUtility cachedThreadPoolUtility;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private TeacherContentDashboardInfoDAO contentDashboardInfoDAO;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private CMDSTestAttemptDAO cmdsTestAttemptDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    private Logger logger = LogFactory.getLogger(ContentInfoManager.class);

    private String MOODLE_BASE_URL;
    private String FOS_ENDPOINT;
    private String PLATFORM_ENDPOINT;
    private String SUBSCRIPTION_ENDPOINT;
    private String SCHEDULING_ENDPOINT;
    private String NEXT_SESSION_API = "/onetofew/session/getNextSessionTimeForBatchIdAndBoardId?";
    private static final String ENTITY_ID = "entityId";
    private static final String ENTITY_TYPE = "entityType";
    private static final String DEFAULT_CONTENT_TITLE = "Test";
    private static final int MAX_UPCOMING_HOME_PAGE_TESTS= 5;
    private static final int MAX_LIVE_UNATTEMPTED_TESTS= 3;
    private static Gson gson = new Gson();

    @Autowired
    private DozerBeanMapper mapper;

    @PostConstruct
    public void init() {
        SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
        MOODLE_BASE_URL = ConfigUtils.INSTANCE.getStringValue("MOODLE_SERVER_URL");
        FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
        PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
        SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

//        List<Bson> pipeline = Collections.singletonList(Aggregates.match(Filters.or(Filters.in("operationType", Arrays.asList("insert", "update", "replace")))));
//        contentInfoDAO.watchDocument(this, ContentInfo.class, pipeline);
    }

    public BasicRes addContents(ContentInfo contentInfo) {
        BasicRes response = new BasicRes();
        contentInfoDAO.save(contentInfo);
        return response;
    }

    public BasicRes updateContent(ContentInfo contentInfo) {
        return addContents(contentInfo);
    }

    public BatchCurriculumStatus getBatchCurriculumStatus(List<Curriculum> curriculumList, Long timestamp) {

        if (curriculumList == null) {
            return BatchCurriculumStatus.ON_TRACK;
        }

        int notDoneByTime = 0;
        int fast = 0;
        int slow = 0;
        for (Curriculum curriculum : curriculumList) {
            if (curriculum.getEndTime() == null) {
                continue;
            }
            if (CurriculumStatus.DONE.equals(curriculum.getStatus()) && curriculum.getEndTime() > timestamp) {
                fast = 1;
            }

            if ((CurriculumStatus.PENDING.equals(curriculum.getStatus())
                    || CurriculumStatus.INPROGRESS.equals(curriculum.getStatus()))
                    && curriculum.getEndTime() < timestamp) {
                slow = 1;
            }

            if (fast == 1 && slow == 1) {
                return BatchCurriculumStatus.NOT_ON_TRACK;
            }

        }

        if (fast == 0 && slow == 0) {
            return BatchCurriculumStatus.ON_TRACK;
        }

        if (fast == 1) {
            return BatchCurriculumStatus.FAST;
        }

        return BatchCurriculumStatus.SLOW;

    }


    public void setContentStats(List<ContentInfo> contentInfoList, ContentStats contentStats) {
        int testAttempted = 0;
        int assignAttempted = 0;
        Set<String> testIds = new HashSet<>();
        Set<String> assignmentIds = new HashSet<>();
        if (contentInfoList != null) {
            for (ContentInfo contentInfo : contentInfoList) {
                TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) contentInfo.getMetadata();

                if (testContentInfoMetadata == null || testContentInfoMetadata.getTestId() == null) {
                    continue;
                }

                if (ContentType.TEST.equals(contentInfo.getContentType())) {
                    testIds.add(testContentInfoMetadata.getTestId());
                }

                if (ContentType.ASSIGNMENT.equals(contentInfo.getContentType())) {
                    assignmentIds.add(testContentInfoMetadata.getTestId());
                }

                if ((ContentState.ATTEMPTED.equals(contentInfo.getContentState())
                        || ContentState.EVALUATED.equals(contentInfo.getContentState()))
                        && ContentType.TEST.equals(contentInfo.getContentType())) {
                    testAttempted++;
                }

                if ((ContentState.ATTEMPTED.equals(contentInfo.getContentState())
                        || ContentState.EVALUATED.equals(contentInfo.getContentState()))
                        && ContentType.ASSIGNMENT.equals(contentInfo.getContentType())) {
                    assignAttempted++;
                }

            }
        }
        contentStats.setAssignAttempted(assignAttempted);
        contentStats.setAssignShared(assignmentIds.size());
        contentStats.setTestAttempted(testAttempted);
        contentStats.setTestShared(testIds.size());

    }

    public Map<String, ContentStats> getContentStats(List<String> batchIds, Long timestamp) {
        List<Curriculum> curriculumList = curriculumDAO.getCurriculumByBatchIds(batchIds);

        if (ArrayUtils.isEmpty(curriculumList)) {
            return new HashMap<>();
        }

        Map<String, List<Curriculum>> curriculumMap = new HashMap<>();
        for (Curriculum curriculum : curriculumList) {
            if (!curriculumMap.containsKey(curriculum.getContextId())) {
                List<Curriculum> curricula = new ArrayList<>();
                curricula.add(curriculum);
                curriculumMap.put(curriculum.getContextId(), curricula);
            } else {
                curriculumMap.get(curriculum.getContextId()).add(curriculum);
            }
        }

        List<ContentInfo> contentInfos = contentInfoDAO.getContentInfoForBatches(batchIds);

        Map<String, List<ContentInfo>> contentInfoMap = new HashMap<>();

        for (ContentInfo contentInfo : contentInfos) {
            if (!contentInfoMap.containsKey(contentInfo.getContextId())) {
                List<ContentInfo> contentInfoList = new ArrayList<>();
                contentInfoList.add(contentInfo);
                contentInfoMap.put(contentInfo.getContextId(), contentInfoList);
            } else {
                contentInfoMap.get(contentInfo.getContextId()).add(contentInfo);
            }
        }

        Map<String, ContentStats> contentStatsMap = new HashMap<>();

        for (String batchId : batchIds) {
            ContentStats contentStats = new ContentStats();
            contentStats.setBatchCurriculumStatus(getBatchCurriculumStatus(curriculumMap.get(batchId), timestamp));
            setContentStats(contentInfoMap.get(batchId), contentStats);
            contentStatsMap.put(batchId, contentStats);
        }

        return contentStatsMap;

    }

    public GetContentInfoListRes getContents(GetContentsReq req) throws BadRequestException {
        GetContentInfoListRes res = new GetContentInfoListRes();
        if (req.getStart() == null || req.getStart() < 0) {
            req.setStart(0);
        }
        if (req.getEnd() == null || req.getEnd() <= 0) {
            req.setEnd(req.getStart() + 10);
        }
        Query query = getContentsBasicQuery(req);
        List<ContentInfo> contentInfos = null;
        if (!StringUtils.isEmpty(req.getSortParam()) && req.getSortParam().equals("deadLine")) {
            // Fetch record with future deadline
            query.addCriteria(Criteria.where(ContentInfo.Constants.EXPIRY_TIME).gte(System.currentTimeMillis()));
            long futureRecords=
                    Optional.ofNullable(contentInfoDAO.runQuery(query, ContentInfo.class,Collections.singletonList(ContentInfo.Constants._ID)))
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .count();
            logger.info("Query:" + query + "futureRecords:" + futureRecords);
            if (futureRecords == 0) {
                // Incase of no future records
                Query newQuery = getContentsBasicQuery(req);
                newQuery.skip(req.getStart() - futureRecords);
                newQuery.limit(req.getEnd() - req.getStart());
                newQuery.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.LAST_UPDATED));

                contentInfos = contentInfoDAO.runQuery(newQuery, ContentInfo.class);
                logger.info("newQuery size:" + (contentInfos == null ? 0 : contentInfos.size()));
                res.setList(contentInfos);
            } else if (futureRecords >= req.getEnd()-req.getStart()) {
                // If future records count is sufficient for response
                logger.info("Future records sufficient");
                query.skip(req.getStart());
                query.limit(req.getEnd() - req.getStart());
                query.with(Sort.by(Sort.Direction.ASC, ContentInfo.Constants.EXPIRY_TIME));

                contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
                res.setList(contentInfos);
            } else {
                logger.info("Future records insufficient");
                List<ContentInfo> results = new ArrayList<ContentInfo>();
                query.skip(req.getStart());
                query.limit(req.getEnd() - req.getStart());
                query.with(Sort.by(Sort.Direction.ASC, ContentInfo.Constants.EXPIRY_TIME));

                contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
                logger.info("Query size:" + (contentInfos == null ? 0 : contentInfos.size()));
                if (contentInfos != null && !contentInfos.isEmpty()) {
                    results.addAll(contentInfos);
                }

                // Fetch content without exiry time
                Query newQuery = getContentsBasicQuery(req);
                Integer newLimit = req.getEnd() - req.getStart() - (contentInfos == null ? 0 : contentInfos.size());
                newQuery.addCriteria(new Criteria().orOperator(
                        Criteria.where(ContentInfo.Constants.EXPIRY_TIME).lte(System.currentTimeMillis()),
                        Criteria.where(ContentInfo.Constants.EXPIRY_TIME).exists(false),
                        Criteria.where(ContentInfo.Constants.EXPIRY_TIME).is(null)));
                newQuery.skip(0);
                newQuery.limit(newLimit);
                newQuery.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.EXPIRY_TIME));

                contentInfos = contentInfoDAO.runQuery(newQuery, ContentInfo.class);
                logger.info("newQuery size:" + (contentInfos == null ? 0 : contentInfos.size()));
                logger.info("new query size : " + contentInfos);
                if (contentInfos != null && !contentInfos.isEmpty()) {
                    results.addAll(contentInfos);
                }
                res.setList(results);
            }
        } else {
            query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.LAST_UPDATED));
            query.skip(req.getStart());
            query.limit(req.getEnd() - req.getStart());
            logger.info("Running query for contentInfo : {}", query);
            contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
            List<String> includeFields=Arrays.asList(
                    CMDSTestAttempt.Constants.START_TIME,
                    CMDSTestAttempt.Constants.ATTEMPT_STATE,
                    CMDSTestAttempt.Constants.END_TIME,
                    CMDSTestAttempt.Constants._ID,
                    CMDSTestAttempt.Constants.MARKS_ACHIEVED,
                    CMDSTestAttempt.Constants.TOTAL_MARKS,
                    CMDSTestAttempt.Constants.CONTENT_INFO_ID
            );
            if(ArrayUtils.isNotEmpty(contentInfos)){
                for (ContentInfo contentInfo : contentInfos) {
                    if(Objects.nonNull(contentInfo.getMetadata())) {
                        logger.info("Setting up test attempts for content info id {}",contentInfo.getId());
                        contentInfo.getMetadata().setTestAttempts(cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(), includeFields));
                    } else {
                        logger.info("No associated metadata with contentInfo Id : {}",contentInfo.getId());
                    }
                }
            }
            res.setList(contentInfos);
        }

        if (StringUtils.isEmpty(req.getStudentId())) {
            contentInfos = res.getList();
            if (contentInfos != null) {
                for (ContentInfo contentInfo : contentInfos) {

                    if (StringUtils.isEmpty(contentInfo.getContentTitle())) {
                        contentInfo.setContentTitle(DEFAULT_CONTENT_TITLE);
                    }

                    // Validate teacher id
                    try {
                        Long.parseLong(contentInfo.getTeacherId());
                    } catch (NumberFormatException ignore) {
                    }
                }

                res.setList(contentInfos);
            }
        }

        //long totalCount = contentInfoDAO.queryCount(query, ContentInfo.class);
        List<ContentState> contentStates = Arrays.asList(ContentState.DRAFT,ContentState.PRIVATE,ContentState.PUBLIC);
        long totalCount = contentDashboardInfoDAO.getTeacherContentCountExcludingSome(req.getTeacherId(),contentStates);
        logger.info("total count " + totalCount);
        res.setTotalCount(Long.valueOf(totalCount).intValue());

        logger.info("res later  is " + res.toString());
        return res;
    }

    public void expireContents() {
        logger.info("expiring contents:");
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.SHARED));
        query.addCriteria(Criteria.where(ContentInfo.Constants.EXPIRY_TIME).lte(System.currentTimeMillis())
                .gte(System.currentTimeMillis() - 4 * DateTimeUtils.MILLIS_PER_MINUTE));
        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
        for (ContentInfo contentInfo : contentInfos) {
            // logger.info("expiring content :" + contentInfo.getTestId());
            contentInfo.setContentState(ContentState.EXPIRED);
            contentInfoDAO.save(contentInfo);
        }
    }

    public void getPendingEvaluations(boolean all) {
        logger.info("getting pending evaluation contents:");
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.ATTEMPTED));
        if (all) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.ATTEMPTED_TIME)
                    .lte(System.currentTimeMillis() - 3 * DateTimeUtils.MILLIS_PER_DAY));
        } else {
            query.addCriteria(Criteria.where(ContentInfo.Constants.ATTEMPTED_TIME)
                    .gt(System.currentTimeMillis() - 4 * DateTimeUtils.MILLIS_PER_DAY)
                    .lte(System.currentTimeMillis() - 3 * DateTimeUtils.MILLIS_PER_DAY));
        }
        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.CREATION_TIME));
        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);

        for (ContentInfo contentInfo : contentInfos) {
            logger.info("content info pending evaluation id: " + contentInfo.getId());
            sendContentInfoNotifications(contentInfo.getId(), ContentInfoCommunicationType.PENDING_EVALUATION);
        }
    }

    public Query getTestContentQuery(GetTestContentReq getTestContentReq) {
        Query query = new Query();
        if (getTestContentReq.getUserId() != null) {
            query.addCriteria(
                    Criteria.where(ContentInfo.Constants.STUDENT_ID).is(getTestContentReq.getUserId().toString()));
        }
        if (getTestContentReq.getContentType() != null) {
            query.addCriteria(
                    Criteria.where(ContentInfo.Constants.CONTENT_TYPE).in(getTestContentReq.getContentType()));
        }
        if (getTestContentReq.getContextId() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(getTestContentReq.getContextId()));
        }

        if (ArrayUtils.isNotEmpty(getTestContentReq.getBoardIds())) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.BOARD_IDS).in(getTestContentReq.getBoardIds()));
        } else if (getTestContentReq.getBoardId() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.BOARD_IDS).is(getTestContentReq.getBoardId()));
        }

        if (!org.springframework.util.StringUtils.isEmpty(getTestContentReq.getContentQuery())) {
            int length = getTestContentReq.getContentQuery().length();
            int max = (length < ReqLimits.NAME_TYPE_MAX)
                    ? length
                    : ReqLimits.NAME_TYPE_MAX;
            getTestContentReq.setContentQuery(getTestContentReq.getContentQuery().substring(0, max));
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TITLE)
                    .regex(getTestContentReq.getContentQuery().substring(0, index), "i"));
        }

        if (getTestContentReq.getContentState() != null) {
            query.addCriteria(
                    Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(getTestContentReq.getContentState()));
        }

        if (getTestContentReq.getSelectedContentInfoType() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE)
                    .is(getTestContentReq.getSelectedContentInfoType()));
        }

        return query;
    }

    public GetContentInfoListRes getTestContent(GetTestContentReq getTestContentReq) throws VException {
        GetContentInfoListRes getContentInfoListRes = new GetContentInfoListRes();
        getContentInfoListRes.setList(filterSelectedContentInfos(contentInfoDAO.getTestContentInfo(getTestContentReq)));
        getContentInfoListRes.setTotalCount(
                Long.valueOf(contentInfoDAO.queryCount(getTestContentQuery(getTestContentReq), ContentInfo.class))
                        .intValue());
        List<Long> boardIds = new ArrayList<>();
        List<String> teacherIds = new ArrayList<>();
        List<String> batchIds = new ArrayList<>();
        for (ContentInfo contentInfo : getContentInfoListRes.getList()) {
            if (contentInfo.getBoardId() != null) {
                boardIds.add(contentInfo.getBoardId());
            }

            if (ArrayUtils.isNotEmpty(contentInfo.getBoardIds())) {
                boardIds.addAll(contentInfo.getBoardIds());
            } else {
                contentInfo.setBoardIds(new HashSet<>(boardIds));
            }

            if (contentInfo.getContextId() != null && EntityType.OTF.equals(contentInfo.getContextType())) {
                batchIds.add(contentInfo.getContextId());
            }

            if (contentInfo.getTeacherId() != null) {
                teacherIds.add(contentInfo.getTeacherId());
            }

        }

        Map<String, String> batchCourseMap = new HashMap<>();
        if (!ArrayUtils.isEmpty(batchIds)) {

            String url = SUBSCRIPTION_ENDPOINT + "/batch/getCourseTitleForBatchIds?";
            int init = 0;
            for (String batchId : batchIds) {
                if (init == 0) {
                    url = url + "batchIds=" + batchId;
                    init = 1;
                } else {
                    url = url + "&batchIds=" + batchId;
                }
            }
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            String jsonString = response.getEntity(String.class);
            Type batchCourseType = new TypeToken<Map<String, String>>() {
            }.getType();
            batchCourseMap = gson.fromJson(jsonString, batchCourseType);

        }

        Map<String, com.vedantu.User.UserBasicInfo> userBasicInfoMap = new HashMap<>();

        if (!ArrayUtils.isEmpty(teacherIds)) {
            userBasicInfoMap = fosUtils.getUserBasicInfosMap(new HashSet<>(teacherIds), false);
        }

        Map<Long, com.vedantu.board.pojo.Board> boardMap = new HashMap<>();
        if (!ArrayUtils.isEmpty(boardIds)) {
            boardMap = fosUtils.getBoardInfoMap(boardIds);
        }

        List<ContentInfo> contentInfoList = new ArrayList<>();
        for (ContentInfo contentInfo : getContentInfoListRes.getList()) {
            if (contentInfo.getBoardId() != null && boardMap.get(contentInfo.getBoardId()) != null) {
                contentInfo.setSubject(boardMap.get(contentInfo.getBoardId()).getName());

                if (contentInfo.getTeacherFullName() == null && contentInfo.getTeacherId() != null) {
                    if (userBasicInfoMap.get(contentInfo.getTeacherId()) != null) {
                        contentInfo.setTeacherFullName(userBasicInfoMap.get(contentInfo.getTeacherId()).getFullName());
                    }
                }

                if (contentInfo.getContextId() != null && EntityType.OTF.equals(contentInfo.getContextType())) {
                    if (batchCourseMap.get(contentInfo.getContextId()) != null) {
                        contentInfo.setCourseName(batchCourseMap.get(contentInfo.getContextId()));
                    }
                }

                if (ArrayUtils.isNotEmpty(contentInfo.getBoardIds())) {
                    Set<String> subjects = new HashSet<>();
                    if (ArrayUtils.isNotEmpty(contentInfo.getSubjects())) {
                        subjects.addAll(contentInfo.getSubjects());
                    }

                    for (Long boardId : contentInfo.getBoardIds()) {
                        if (boardMap.get(boardId) != null) {
                            subjects.add(boardMap.get(boardId).getName());
                        }
                    }
                    contentInfo.setSubjects(new HashSet<>(subjects));
                }

                contentInfoList.add(contentInfo);
            }

        }

        return getContentInfoListRes;
    }

    private List<ContentInfo> filterSelectedContentInfos(List<ContentInfo> contentInfos) throws BadRequestException {
        for (ContentInfo contentInfo : contentInfos) {
            Map<Long, CMDSTestAttempt> filterMap = new LinkedHashMap<>();
            TestContentInfoMetadata metadata = contentInfo.getMetadata();
            List<String> includeFields=Arrays.asList(
                    CMDSTestAttempt.Constants.START_TIME,
                    CMDSTestAttempt.Constants.ATTEMPT_STATE,
                    CMDSTestAttempt.Constants.END_TIME,
                    CMDSTestAttempt.Constants._ID,
                    CMDSTestAttempt.Constants.MARKS_ACHIEVED,
                    CMDSTestAttempt.Constants.TOTAL_MARKS,
                    CMDSTestAttempt.Constants.CONTENT_INFO_ID
            );
            List<CMDSTestAttempt> allAttemptsForContentInfo = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),includeFields);
            if (Objects.nonNull(metadata) && ArrayUtils.isNotEmpty(allAttemptsForContentInfo)) {
                for (CMDSTestAttempt currentAttempt : allAttemptsForContentInfo) {
                    if (null != currentAttempt.getStartTime() && filterMap.containsKey(currentAttempt.getStartTime())) {
                        CMDSTestAttempt cmdsTestAttempt1 = filterMap.get(currentAttempt.getStartTime());
                        if (null == cmdsTestAttempt1.getEvaluatedTime() || null == currentAttempt.getEvaluatedTime()) {
                            continue;
                        }
                        CMDSTestAttempt attemptToBeSaved = (cmdsTestAttempt1.getEvaluatedTime() > currentAttempt.getEvaluatedTime()) ? cmdsTestAttempt1 : currentAttempt;
                        filterMap.put(currentAttempt.getStartTime(), attemptToBeSaved);
                    } else {
                        filterMap.put(currentAttempt.getStartTime(), currentAttempt);
                    }
                }
                List<CMDSTestAttempt> testAttempts = new ArrayList<>(filterMap.values());
                testAttempts.sort(Comparator.comparingInt(CMDSTestAttempt::getAttemptIndex));
                metadata.setTestAttempts(testAttempts);
            }
        }
        return contentInfos;
    }

    private Query getContentsBasicQuery(GetContentsReq req) {
        Query query = new Query();

        if (StringUtils.isEmpty(req.getCourseId()) && !StringUtils.isEmpty(req.getCourseName())) {
            req.setCourseId(moodleDataManager.getCourseId(req.getCourseName()));
        }

        if (!StringUtils.isEmpty(req.getTeacherId())) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(req.getTeacherId()));
        }
        if (!StringUtils.isEmpty(req.getCourseId())) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.COURSE_ID).is(req.getCourseId()));
        }

        if (!StringUtils.isEmpty(req.getStudentId())) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(req.getStudentId()));
        }

        if (!StringUtils.isEmpty(req.getStudentName())) {
            int length = req.getStudentName().length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_FULL_NAME).regex(req.getStudentName().substring(0, index), "i"));
        }

        if (req.getContentType() != null && !req.getContentType().

                isEmpty()) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).in(req.getContentType()));
        }
        if (req.getEngagementType() != null && !req.getEngagementType().

                isEmpty()) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.ENGAGEMENT_TYPE).in(req.getEngagementType()));
        }

        if (req.getContentState() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).in(req.getContentState()));
        } else {
            List<ContentState> contentStates = new ArrayList<ContentState>();
            contentStates.add(ContentState.DRAFT);
            contentStates.add(ContentState.PRIVATE);
            contentStates.add(ContentState.PUBLIC);
            contentStates.add(ContentState.NOT_UPLOADED);
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).nin(contentStates));
        }

        if (!StringUtils.isEmpty(req.getSearchFieldValue())) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.TAGS)
                    .in(Pattern.compile(req.getSearchFieldValue().toUpperCase())));
        }

        if (req.getLmsType() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.LMS_TYPE).is(req.getLmsType()));
        }

        // include fields - metadata
        query.fields().include(ContentInfo.Constants._ID)
                .include(ContentInfo.Constants.LMS_TYPE).include(ContentInfo.Constants.STUDENT_FULL_NAME).include(ContentInfo.Constants.CONTENT_TITLE)
                .include(ContentInfo.Constants.CREATION_TIME).include(ContentInfo.Constants.COURSE_NAME)
                .include(ContentInfo.Constants.CONTENT_STATE)
                .include(ContentInfo.Constants.METADATA_DURATION);

        logger.info("queryr  is " + query.toString());

        return query;
    }

    @Deprecated
    public GetTeacherDashBoardInfoListRes getTeacherDashBoardInfoOpt(String teacherId) {

        GetTeacherDashBoardInfoListRes res = new GetTeacherDashBoardInfoListRes();
        List<TeacherDashBoardInfo> teacherDashBoardInfos = new ArrayList<TeacherDashBoardInfo>();
        if (StringUtils.isEmpty(teacherId)) {
            return res;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(teacherId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).ne(ContentState.PUBLIC));
        if (ConfigUtils.INSTANCE.getStringValue("DEMO_STUDENT_ID") != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID)
                    .ne(ConfigUtils.INSTANCE.getStringValue("DEMO_STUDENT_ID")));
        }

        // Inclusion of necessary fields only
        query.fields().include(ContentInfo.Constants.COURSE_NAME);
        query.fields().include(ContentInfo.Constants.CONTENT_INFO_TYPE);
        query.fields().include(ContentInfo.Constants.CONTENT_STATE);
        query.fields().include(ContentInfo.Constants.STUDENT_ID);
        query.fields().include(ContentInfo.Constants.STUDENT_FULL_NAME);
        query.fields().include(ContentInfo.Constants.TEACHER_ACTION_LINK);

        logger.info("query fetch teacher dashboard info is {} ", query);
        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
        List<EvaluateNowLink> evaluateNowLinks = new ArrayList<>();
        Map<ContentInfoType, TeacherDashBoardInfo> resultMap = new HashMap<>();
        if (Objects.nonNull(contentInfos)) {
            for (ContentInfo contentInfo : contentInfos) {
                if (!StringUtils.isEmpty(contentInfo.getCourseName())
                        && contentInfo.getCourseName().split("-").length == 2
                        && !contentInfo.getCourseName().contains("OTF")) {
                    continue;
                }
                TeacherDashBoardInfo teacherDashBoardInfo;
                if (resultMap.containsKey(contentInfo.getContentInfoType())) {
                    teacherDashBoardInfo = resultMap.get(contentInfo.getContentInfoType());
                } else {
                    teacherDashBoardInfo = new TeacherDashBoardInfo();
                }

                logger.info("content type " + contentInfo.getContentState());
                switch (contentInfo.getContentState()) {
                    case SHARED: {
                        teacherDashBoardInfo.incrementShared();
                    }
                    break;
                    case ATTEMPTED: {
                        teacherDashBoardInfo.incrementAttempted();
                        teacherDashBoardInfo.incrementShared();
                    }
                    break;
                    case EVALUATED: {
                        teacherDashBoardInfo.incrementEvaluated();
                        teacherDashBoardInfo.incrementAttempted();
                        teacherDashBoardInfo.incrementShared();
                    }
                    break;
                    default:
                        break;
                }

                teacherDashBoardInfo.setContentInfoType(contentInfo.getContentInfoType());
                resultMap.put(contentInfo.getContentInfoType(), teacherDashBoardInfo);

                if (ContentState.ATTEMPTED.equals(contentInfo.getContentState())) {
                    evaluateNowLinks.add(new EvaluateNowLink(contentInfo.getTeacherActionLink(),
                            contentInfo.getStudentFullName(), contentInfo.getStudentId()));
                }
            }
        }

        // Extract Results
        for (Entry<ContentInfoType, TeacherDashBoardInfo> entry : resultMap.entrySet()) {
            teacherDashBoardInfos.add(entry.getValue());
        }

        res.setList(teacherDashBoardInfos);
        res.setEvaluateNowLinks(evaluateNowLinks);
        logger.info("teacher dashboard is" + res.toString());
        return res;
    }

    public GetTeacherDashBoardInfoListRes getTeacherDashBoardInfo(String teacherId) {

        GetTeacherDashBoardInfoListRes res = new GetTeacherDashBoardInfoListRes();
        List<TeacherDashBoardInfo> teacherDashBoardInfos = new ArrayList<>();
        if (StringUtils.isEmpty(teacherId)) {
            return res;
        }

        // fetch interval -- past month
        long toTime = System.currentTimeMillis();
        long fromTime = System.currentTimeMillis() - 30 * CommonCalendarUtils.MILLIS_PER_DAY;

        int sharedCount = (int)contentDashboardInfoDAO.getTeacherContentCount(teacherId,ContentInfoType.SUBJECTIVE,ContentState.SHARED);
        int evaluatedCount = (int)contentDashboardInfoDAO.getTeacherContentCount(teacherId,ContentInfoType.SUBJECTIVE,ContentState.EVALUATED);

        List<String> includeFieldsForAttemptedQuery = Arrays.asList(ContentInfo.Constants.COURSE_NAME,
                ContentInfo.Constants.CONTENT_INFO_TYPE, ContentInfo.Constants.CONTENT_STATE, ContentInfo.Constants.STUDENT_ID,
                ContentInfo.Constants.STUDENT_FULL_NAME, ContentInfo.Constants.TEACHER_ACTION_LINK);

        Function<ContentInfo, EvaluateNowLink> evaluateNowLinkFactory = (attemptedContentInfo) -> EvaluateNowLink.builder()
                .link(attemptedContentInfo.getTeacherActionLink())
                .studentName(attemptedContentInfo.getStudentFullName())
                .studentId(attemptedContentInfo.getStudentId())
                .build();

        List<ContentInfo> attemptedContentInfos =
                contentInfoDAO.getAttemptedContentInfosForTeacher(teacherId, fromTime, toTime,1000, includeFieldsForAttemptedQuery);
        List<EvaluateNowLink> evaluateNowLinks =
                Optional.ofNullable(attemptedContentInfos)
                        .orElseGet(ArrayList::new)
                        .stream()
                        .map(evaluateNowLinkFactory)
                        .collect(Collectors.toList());

        // get counts for shared and evaluated
        int attemptedCount = CollectionUtils.isEmpty(attemptedContentInfos) ? 0 : attemptedContentInfos.size();
        attemptedCount += evaluatedCount;
        sharedCount += attemptedCount;

        TeacherDashBoardInfo teacherDashBoardInfo =
                TeacherDashBoardInfo
                        .builder()
                        .shared(sharedCount)
                        .attempted(attemptedCount)
                        .evaluated(evaluatedCount)
                        .contentInfoType(ContentInfoType.SUBJECTIVE)
                        .build();

        teacherDashBoardInfos.add(teacherDashBoardInfo);
        res.setList(teacherDashBoardInfos);
        res.setEvaluateNowLinks(evaluateNowLinks);
        return res;
    }

    public GetTeacherCoursesListRes getTeacherCourses(GetTeacherCourses req) {
        GetTeacherCoursesListRes res = new GetTeacherCoursesListRes();
        List<TeacherCoursesRes> result = new ArrayList<>();
        if (StringUtils.isEmpty(req.getTeacherId())) {
            return res;
        }
        logger.info("get teacher courses request " + req.toString());
        // shortname":"VEDANTU_CONTENT
        List<CourseInfoRes> courseInfoRes = moodleDataManager.getUserCourses(req.getTeacherId());
        Integer start = req.getStart();
        if (start == null || start < 0) {
            start = 0;
        }

        Integer end = req.getEnd();
        if (end == null || end < 0) {
            end = courseInfoRes.size();
        }

        if (courseInfoRes != null && !courseInfoRes.isEmpty() && start < courseInfoRes.size()) {
            res.setTotalCount(courseInfoRes.size());
            for (CourseInfoRes courseInfo : courseInfoRes) {
                if (courseInfo.getShortname().equals("VEDANTU_CONTENT")) {
                    continue;
                }
                Query query = new Query();
                query.addCriteria(Criteria.where(ContentInfo.Constants.COURSE_ID).is(courseInfo.getId()));
                query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(req.getTeacherId()));
                query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_FULL_NAME).ne("Demo Student"));
                logger.info("qeury is " + query.toString());
                List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
                Map<ContentInfoType, TeacherDashBoardInfo> resultMap = getContentMetaData(contentInfos);
                List<TeacherDashBoardInfo> teacherDashBoardInfos = new ArrayList<TeacherDashBoardInfo>();
                for (Entry<ContentInfoType, TeacherDashBoardInfo> entry : resultMap.entrySet()) {
                    teacherDashBoardInfos.add(entry.getValue());
                }

                int notesCount = 0;
                for (ContentInfo contentInfo : contentInfos) {
                    if (contentInfo.getContentType().equals(ContentType.NOTES)) {
                        ++notesCount;
                    }

                }

                TeacherCoursesRes teacherCourseRes = new TeacherCoursesRes();
                teacherCourseRes.setCourseInfoRes(courseInfo);
                // (" course info is "+courseInfo.toString());
                teacherCourseRes.setContentMetaData(teacherDashBoardInfos);
                logger.info("course short name :" + courseInfo.getShortname());
                if (courseInfo.getShortname().contains("-") && courseInfo.getShortname().split("-").length == 4) {
                    teacherCourseRes.setStudentId(courseInfo.getShortname().split("-")[0]);
                    teacherCourseRes.setStudentName(courseInfo.getFullname().split("-")[0]);
                    teacherCourseRes.setSubject(courseInfo.getShortname().split("-")[1]);
                    // teacherCourseRes.setSubject(courseInfo.getShortname());
                    teacherCourseRes.setEngagementType(
                            EngagementType.valueOf(courseInfo.getShortname().split("-")[3].toUpperCase()));

                    if (req.getUserIds() != null && req.getUserIds().size() > 0) {
                        if (!req.getUserIds().contains(courseInfo.getShortname().split("-")[0])) {
                            continue;
                        }
                    }
                } else if (courseInfo.getShortname().contains("-")
                        && courseInfo.getShortname().split("-").length == 2) {
                    Group group = null;
                    query = new Query();
                    query.addCriteria(Criteria.where("_id").is(courseInfo.getShortname().split("-")[0]));
                    List<Group> groups = groupDAO.runQuery(query, Group.class);
                    if (groups != null && groups.size() > 0) {
                        group = groups.get(0);
                    }
                    if (group != null) {
                        if (group.getLaunchDate() != null && group.getStartTime() != null
                                && group.getEndTime() != null) {

                            SimpleDateFormat sdf = new SimpleDateFormat("dd MMM hh:mm a");
                            String startTimeString = sdf.format(new Date(group.getStartTime()
                                    + 5 * DateTimeUtils.MILLIS_PER_HOUR + 30 * DateTimeUtils.MILLIS_PER_MINUTE));
                            courseInfo.setFullname(group.getBatchTitle() + "  " + "(" + startTimeString + ")");
                            teacherCourseRes.setCourseInfoRes(courseInfo);
                        }
                        if (req.getUserIds() != null && req.getUserIds().size() > 0) {
                            boolean match = false;
                            if (group.getEnrolledStudents() != null && group.getEnrolledStudents().size() > 0) {
                                for (UserBasicInfo student : group.getEnrolledStudents()) {
                                    if (req.getUserIds().contains(String.valueOf(student.getUserId()))) {
                                        match = true;
                                    }
                                }
                            }
                            if (match == false) {
                                continue;
                            }

                        }

                        teacherCourseRes.setEngagementType(EngagementType.OTF);
                        teacherCourseRes.setStudentName(courseInfo.getFullname().split("-")[0]);
                        if (group.getSubjects() != null && !group.getSubjects().isEmpty()) {
                            teacherCourseRes.setSubject(group.getSubjects().get(0));
                        }
                    } else {

                        continue;
                    }
                }

                teacherCourseRes.setTeacherActionLink(
                        MOODLE_BASE_URL + "/course/view.php?id=" + courseInfo.getId() + "&notifyeditingon=1");
                teacherCourseRes.setNoteCount(notesCount);
                if (result.size() < end) {
                    result.add(teacherCourseRes);
                } else {
                    break;
                }
            }

            if (end < result.size()) {
                result = result.subList(start, end);
            } else {
                result = result.subList(start, result.size());
            }
        }

        res.setList(result);
        return res;
    }

    public static Map<ContentInfoType, TeacherDashBoardInfo> getContentMetaData(List<ContentInfo> contentInfos) {
        Map<ContentInfoType, TeacherDashBoardInfo> resultMap = new HashMap<ContentInfoType, TeacherDashBoardInfo>();
        if (contentInfos != null) {
            for (ContentInfo contentInfo : contentInfos) {
                TeacherDashBoardInfo teacherDashBoardInfo;
                if (resultMap.containsKey(contentInfo.getContentInfoType())) {
                    teacherDashBoardInfo = resultMap.get(contentInfo.getContentInfoType());
                } else {
                    teacherDashBoardInfo = new TeacherDashBoardInfo();
                }

                switch (contentInfo.getContentState()) {
                    case SHARED: {
                        teacherDashBoardInfo.incrementShared();
                        break;
                    }
                    case ATTEMPTED: {
                        teacherDashBoardInfo.incrementAttempted();
                        teacherDashBoardInfo.incrementShared();
                        break;
                    }
                    case EVALUATED: {
                        teacherDashBoardInfo.incrementEvaluated();
                        teacherDashBoardInfo.incrementAttempted();
                        teacherDashBoardInfo.incrementShared();
                        break;
                    }
                    default:
                        break;
                }

                teacherDashBoardInfo.setContentInfoType(contentInfo.getContentInfoType());
                resultMap.put(contentInfo.getContentInfoType(), teacherDashBoardInfo);
            }
        }

        return resultMap;
    }

    // public handling here??
    public List<ContentInfo> getAllContents(Long startTime, Long endTime, String queryParam) {
        logger.info("getAllContents startTime : " + startTime + " endTime : " + endTime);
        Query query = new Query();
        if (StringUtils.isEmpty(queryParam)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(startTime).lte(endTime));
        } else {
            switch (queryParam) {
                case "evaluatedTime":
                    query.addCriteria(Criteria.where(ContentInfo.Constants.EVAULATED_TIME).gte(startTime).lte(endTime));
                    break;
                case "expiryTime":
                    query.addCriteria(Criteria.where(ContentInfo.Constants.EXPIRY_TIME).gte(startTime).lte(endTime));
                    break;
                case "attemptedTime":
                    query.addCriteria(Criteria.where(ContentInfo.Constants.ATTEMPTED_TIME).gte(startTime).lte(endTime));
                    break;
                default:
                    break;
            }
        }

        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
        logger.info("getAllContents response count : " + contentInfos == null ? 0 : contentInfos.size());
        return contentInfos;
    }
    
    // Get 3 latest shared assignments for the given studentId
    public List<AssignmentCardInfo> getLatestSharedAssignments(String studentId) throws BadRequestException {
    	int size = 3;
        logger.info("getLatestSharedAssignments Manager studentId : {}", studentId);
        List<AssignmentCardInfo> assignmentCardInfos = new ArrayList<>();
        List<ContentInfo> contentInfos = contentInfoDAO.getLatestSharedAssignments(studentId, size);
        if (CollectionUtils.isEmpty(contentInfos)) {
        	logger.info("getLatestSharedAssignments No data in table ContentInfo for : {}", studentId);
        	return assignmentCardInfos;
        }
        logger.info("getLatestSharedAssignments contentInfos Size : {}",contentInfos.size());
        
        for (ContentInfo contentInfo: contentInfos) {
        	AssignmentCardInfo assignmentCardInfo = new AssignmentCardInfo();
            TestContentInfoMetadata metadata = Optional.ofNullable(contentInfo.getMetadata()).orElseGet(TestContentInfoMetadata::new);
            String testId = metadata.getTestId();
        	logger.info("getLatestSharedAssignments testId {}", testId);
        	if(!StringUtils.isEmpty(testId)) {
        		CMDSTest cmdsTest = cmdsTestManager.getCMDSTestById(testId);
        		Set<String> boardIds = cmdsTest.getBoardIds();
        		assignmentCardInfo.setBoardIds(boardIds);
        	}
        	assignmentCardInfo.setContentInfoBoardIds(contentInfo.getBoardIds());
        	assignmentCardInfo.setContextId(contentInfo.getContextId());
        	assignmentCardInfo.setTestId(metadata.getTestId());
        	assignmentCardInfo.setContentTitle(contentInfo.getContentTitle());
        	assignmentCardInfo.setContentType(contentInfo.getContentType());
        	assignmentCardInfo.setContentSubType(contentInfo.getContentSubType());
        	assignmentCardInfo.setStudentActionLink(contentInfo.getStudentActionLink());
        	assignmentCardInfo.setSharedTime(contentInfo.getCreationTime());
        	assignmentCardInfo.setAccessLevel(contentInfo.getAccessLevel());
        	logger.info("getLatestSharedAssignments AssignmentCardInfo : {} ", assignmentCardInfo.toString());
        	assignmentCardInfos.add(assignmentCardInfo);
        }
        return assignmentCardInfos;
    }
    
    // Get start time of shared assignments with session subtype for the given subject(boardId) and batch(contextId)
    public List<SessionDateRes> getSessionDates(SessionDateReq sessionDateReq) throws VException {
    	
    	logger.info("getSessionDates Req {} ", sessionDateReq.toString());
    	List<SessionDate> sessionDates = sessionDateReq.getSessionDateList();

    	if(sessionDates.isEmpty())
        	throw new BadRequestException(ErrorCode.IO_ERROR, "Unable to fetch sessionDates");
    	
    	logger.info("getSessionDates size {} ", sessionDates.size());
    	ArrayList<SessionDateRes> sessionDateResponseList = new ArrayList<>();
        for(SessionDate sessionDate : sessionDates) {
        	List<Long> boardId = sessionDate.getBoardId();
        	String boardIds = "";
        	for (Long board: boardId) {
        		boardIds = boardIds + board.toString() + ',';
        	}
        	boardIds = boardIds.substring(0, boardIds.length() - 1);
	        String batchId = sessionDate.getBatchId();
	        String testId = sessionDate.getTestId();
	        Long sharedTime = sessionDate.getSharedTime();
	        String url = SCHEDULING_ENDPOINT + NEXT_SESSION_API + "boardId=" + boardIds + "&batchId=" + batchId + "&startTime=" + sharedTime;
	        logger.info("getSessionDates URL :  {}", url);
	        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
	        logger.info("getSessionDates response : {}", response);
	        VExceptionFactory.INSTANCE.parseAndThrowException(response);
	        String jsonString = response.getEntity(String.class);
	        logger.info("getSessionDates jsonString : {}", jsonString);
	        SessionDateRes sessionDateRes = new SessionDateRes();
	        sessionDateRes.setBatchId(batchId);
	        sessionDateRes.setBoardId(boardId);
	        sessionDateRes.setTestId(testId);
	        if(StringUtils.isNotEmpty(jsonString)) {
	        	Long startTimeSession = new Gson().fromJson(jsonString, Long.class);
	        	Long sharedTimeAssignment = sessionDate.getSharedTime();
	        	logger.info("getSessionDates startTime of next Session : {}", startTimeSession);
        		sessionDateRes.setCompleteBy(startTimeSession);
        		
	        	if (startTimeSession - sharedTimeAssignment < TimeUnit.DAYS.toMillis(2)) {
	        		logger.info("getSessionDates startTime of next Session : {}", (startTimeSession+ TimeUnit.DAYS.toMillis(2)));
	        		sessionDateRes.setCompleteBy(sharedTimeAssignment + TimeUnit.DAYS.toMillis(2) );
	        	}	
	       	}
	        sessionDateResponseList.add(sessionDateRes);
        }
        logger.info("getSessionDates Res {} ", sessionDateResponseList.toString());
        return sessionDateResponseList;
    }
    

    //ADITYA LO for content type as test
    private void setQueryMaxTimeout(Query query) {
        if (query != null) {
            query.maxTimeMsec(86400000l);
        }
    }

    public String getCourseUrl(String courseName) {
        String courseId = moodleDataManager.getCourseId(courseName);
        if (StringUtils.isEmpty(courseId)) {
            return MOODLE_BASE_URL + "/";
        } else {
            return MOODLE_BASE_URL + "/course/view.php?id=" + courseId + "&notifyeditingon=1";
        }
    }

    public boolean isLastThreeAssignmentsInterventionNeeded(Long studentId, Long creationTime) {
        List<ContentInfo> contentInfos = contentInfoDAO.getLastThreeAssignmentsOfStudent(studentId);
        logger.info("ContentInfos");
        if (contentInfos.size() >= 3) {
            if ((ContentState.ATTEMPTED.equals(contentInfos.get(0).getContentState())
                    || ContentState.EVALUATED.equals(contentInfos.get(0).getContentState()))
                    && contentInfos.get(0).getCreationTime() >= creationTime
                    && ContentState.SHARED.equals(contentInfos.get(1).getContentState())
                    && ContentState.SHARED.equals(contentInfos.get(2).getContentState())) {
                return true;
            }
        }
        return false;
    }

    public boolean isInLastTenPercentileInTests(Long studentId, Long creationTime) {
        List<ContentInfo> contentInfos = contentInfoDAO.getTestsOfStudentAfter(studentId, creationTime);
        for (ContentInfo contentInfo : contentInfos) {
            logger.info(
                    "ContentInfo Id: " + contentInfo.getId() + "ContentInfo StudentId" + contentInfo.getStudentId());
            if (contentInfo.getMetadata() != null) {
                if (contentInfo.getMetadata().getPercentile() <= 10.0) {
                    return true;
                }
            }
        }
        return false;
    }

    public ContentInfo getContentById(String id) {
        return getContentById(id,true);
    }
    public ContentInfo getContentById(String id,boolean processContentInfoContent) {
        if (StringUtils.isEmpty(id)) {
            return null;
        }

        ContentInfo contentInfo = contentInfoDAO.getById(id);
        if (processContentInfoContent && Objects.nonNull(contentInfo)) {
            List<String> ids = new ArrayList<>();
            if (StringUtils.isNotEmpty(contentInfo.getTeacherId())
                    && StringUtils.isEmpty(contentInfo.getTeacherFullName())) {
                ids.add(contentInfo.getTeacherId());
            }
            if (StringUtils.isNotEmpty(contentInfo.getStudentId())
                    && StringUtils.isEmpty(contentInfo.getStudentFullName())) {
                ids.add(contentInfo.getStudentId());
            }
            if (ArrayUtils.isEmpty(ids)) {
                return contentInfo;
            }
            Map<String, com.vedantu.User.UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(ids, false);
            if (StringUtils.isNotEmpty(contentInfo.getStudentId()) && userMap.containsKey(contentInfo.getStudentId())) {
                com.vedantu.User.UserBasicInfo student = userMap.get(contentInfo.getStudentId());
                if (student != null) {
                    contentInfo.setStudentFullName(student.getFullName());
                }
            }
            if (StringUtils.isNotEmpty(contentInfo.getTeacherId()) && userMap.containsKey(contentInfo.getTeacherId())) {
                com.vedantu.User.UserBasicInfo teacher = userMap.get(contentInfo.getTeacherId());
                if (teacher != null) {
                    contentInfo.setTeacherFullName(teacher.getFullName());
                }
            }
        }
        return contentInfo;
    }

    public void expireContentsBeforeTime(Long beforeTime) {

        logger.info("expiring contents:");
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.EXPIRY_TIME).lte(beforeTime));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.SHARED));
        query.addCriteria(Criteria.where(ContentInfo.Constants.EXPIRY_TIME).ne(0));
        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
        for (ContentInfo contentInfo : contentInfos) {
            logger.info("expiring content :" + contentInfo.getId());
            contentInfo.setContentState(ContentState.EXPIRED);
            contentInfoDAO.save(contentInfo);
        }

    }

    public List<ContentInfo> fetchPrivateContents() {

        Query query = new Query();

        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).ne(ContentState.PUBLIC));
        query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).exists(false));
        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);

        // for (ContentInfo contentInfo : contentInfos) {
        // String courseId = contentInfo.getCourseId();
        // if(contentInfo.getCourseName()==null || courseId.equals("68"))
        // continue;
        // // logger.info("contentInfo " + contentInfo.getCourseName());
        // List<CoursePojo> courseInfoRes =
        // moodleDataManager.getCourseWithId(courseId);
        // if (courseInfoRes.size() > 0) {
        // logger.info(
        // "course short name : " + courseInfoRes.get(0).getShortname() +
        // "for course id " + courseId);
        // contentInfo.setTeacherId(courseInfoRes.get(0).getShortname().split("-")[0]);
        // contentInfoDAO.update(contentInfo);
        // }
        // }
        return contentInfos;
    }

    public List<ContentInfo> fetchGroupContents() {

        Query query = new Query();

        query.addCriteria(Criteria.where(ContentInfo.Constants.ENGAGEMENT_TYPE).is(EngagementType.OTF));
        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
        // List<ContentInfo> resultContentInfos=new ArrayList<ContentInfo>();
        // for (ContentInfo contentInfo : contentInfos) {
        // if(contentInfo.getCourseName()==null ||
        // contentInfo.getCourseId().equals("68")) continue;
        //
        // }
        return contentInfos;

    }

    public List<ContentInfo> fetchContents(String studentId, String teacherId, String testId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId));
        if (!StringUtils.isEmpty(teacherId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(teacherId));
        }
        query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID).is(testId));

        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
        logger.info("Query results:" + contentInfos.toString());
        return contentInfos;
    }

    public ContentInfo createCMDSContentInfo(UserType userType, String userId, String studentName,
                                             com.vedantu.User.UserBasicInfo teacher, String contentLink, CMDSTest cmdsTest, String courseName,
                                             EngagementType engagementType, com.vedantu.session.pojo.EntityType contextType, String contextId)
            throws MalformedURLException, InternalServerErrorException {
        if (CMDSTestState.PRIVATE.equals(cmdsTest.getTestState())) {
            logger.info("Test is private, not sharing");
            return null;
        }
        ContentInfo contentInfo = new ContentInfo();
        contentInfo.setUserType(userType);
        contentInfo.setStudentId(userId);
        contentInfo.setStudentFullName(studentName);
        if (teacher != null) {
            contentInfo.setTeacherId(String.valueOf(teacher.getUserId()));
            contentInfo.setTeacherFullName(teacher.getFullName());
        }

        if (cmdsTest.getBoardIds() != null) {
            List<Long> boardIds = new ArrayList<>();
            for (String boardId : cmdsTest.getBoardIds()) {
                boardIds.add(Long.parseLong(boardId));
            }
            Map<Long, com.vedantu.board.pojo.Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            for (Map.Entry<Long, com.vedantu.board.pojo.Board> entry : boardMap.entrySet()) {

                if (entry.getValue().getParentId() != null && entry.getValue().getParentId() == 0) {
                    contentInfo.setBoardId(entry.getValue().getId());
                    contentInfo.setSubject(entry.getValue().getName());
                }

                if (entry.getValue().getParentId() != null && entry.getValue().getParentId() == 0) {
                    if (contentInfo.getBoardIds() == null) {
                        contentInfo.setBoardIds(new HashSet<>());
                    }

                    if (contentInfo.getSubjects() == null) {
                        contentInfo.setSubjects(new HashSet<>());
                    }

                    contentInfo.getBoardIds().add(entry.getValue().getId());
                    contentInfo.getSubjects().add(entry.getValue().getName());

                }

            }
        }

        TestContentInfoMetadata metadata = new TestContentInfoMetadata(cmdsTest);
        contentInfo.setMetadata(metadata);
        contentInfo.setContentTitle(cmdsTest.getName());
        contentInfo.setCourseName(courseName);
        contentInfo.setContentType(ContentType.TEST);
        contentInfo.setContentInfoType(cmdsTest.getContentInfoType());
        contentInfo.setEngagementType(engagementType);
        contentInfo.setContentState(ContentState.SHARED);
        contentInfo.setLmsType(LMSType.CMDS);
        contentInfo.setAccessLevel(cmdsTest.getAccessLevel());

        // Update test details
        String testLink;
        if (com.vedantu.util.StringUtils.isNotEmpty(cmdsTest.getId())) {
            testLink = getCMDSContentLinkFromTestId(cmdsTest.getId());
        } else {
            testLink = getCMDSContentLink(contentLink);
        }
        contentInfo.setContentLink(testLink);
        contentInfo.setStudentActionLink(testLink);
        contentInfo.setTeacherActionLink(testLink);
        contentInfo.setContextType(contextType);
        contentInfo.setContextId(contextId);
        return contentInfo;
    }

    private String getCMDSContentLink(String contentLink) throws MalformedURLException, InternalServerErrorException {
        // String contentLink
        // Parse the url to figure out the question set or the
        // test link
        Map<String, String> queryParams = CommonUtils.getParameterMapFromUrl(contentLink);
        if (!queryParams.containsKey(ENTITY_ID) || !queryParams.containsKey(ENTITY_TYPE)) {
            String errorMessage = "CMDS Entity id or entity type not found for url : " + contentLink;
            logger.error(errorMessage);
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
        }

        String entityId = queryParams.get(ENTITY_ID);
        return FOS_ENDPOINT + "cmds/test/" + entityId;
    }

    private String getCMDSContentLinkFromTestId(String testId) {
        return FOS_ENDPOINT + "cmds/test/" + testId;
    }

    public String getCMDSReportLink(ContentInfo contentInfo) {
        return FOS_ENDPOINT + "contents/test/result/" + contentInfo.getId();
    }

    private void sendContentInfoNotifications(String contentInfoId, ContentInfoCommunicationType communicationType,
                                              String testAttemptId) {
        ContentInfo contentInfo = getContentById(contentInfoId);

        sendContentInfoNotifications(contentInfo, communicationType, testAttemptId);
    }

    private void sendContentInfoNotifications(ContentInfo contentInfo, ContentInfoCommunicationType communicationType,
                                              String testAttemptId) {
        String moodleNotificationUrl = PLATFORM_ENDPOINT + "/notification-centre/email/sendMoodleNotifications";
        MoodleContentInfo moodleContentInfo = gson.fromJson(gson.toJson(contentInfo), MoodleContentInfo.class);
        moodleContentInfo.setAttemptId(testAttemptId);
        SendMoodleNotificationsReq req = new SendMoodleNotificationsReq(communicationType.name(), moodleContentInfo);
        logger.info("sendMoodleNotifications : " + moodleNotificationUrl);
        ClientResponse response = WebUtils.INSTANCE.doCall(moodleNotificationUrl, HttpMethod.POST,
                new Gson().toJson(req), true);
        logger.info("response : " + response);
    }

    void sendContentInfoNotifications(String contentInfoId, ContentInfoCommunicationType communicationType) {
        sendContentInfoNotifications(contentInfoId, communicationType, null);
    }

    public PlatformBasicResponse unshareContent(UnshareContentReq req) {
        contentInfoDAO.unshareContent(req);
        return new PlatformBasicResponse();
    }

    public List<ContentInfo> getContentInfos(List<String> studentIds, String teacherId, EntityType contextType,
                                             String contextId, String testId) {
        return contentInfoDAO.getContentInfos(studentIds, teacherId, contextType, contextId, testId);
    }

    public void insertContentInfos(List<ContentInfo> contentInfoList) {
        contentInfoDAO.insertAllEntities(contentInfoList, ContentInfo.class.getSimpleName());
    }

    public List<ContentInfo> getContentInfosForUserAndContextId(String studentId, List<String> testIds,
                                                                String bundleId,List<String> includeFields) {
        return contentInfoDAO.getContentInfosForUserAndContextId(studentId, testIds, bundleId,includeFields);
    }

    public Map<String, List<StudentContentPojo>> getContentInfoForDashboard(List<String> studentIds, String batchId){

        Map<String, List<StudentContentPojo>> mapIdToContent = new HashMap<>();
        if (CollectionUtils.isEmpty(studentIds)) {
            return mapIdToContent;
        }
        Map<String, Map<String, StudentContentPojo>> map = new HashMap<>();

        Criteria criteria = new Criteria();
        if (StringUtils.isNotEmpty(batchId)) {
            // TODO : for content without batches
        }
        Criteria contextId = Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(batchId);
        Criteria studentId = Criteria.where(ContentInfo.Constants.STUDENT_ID).in(studentIds);
        GroupOperation contentCount = Aggregation.group(ContentInfo.Constants.STUDENT_ID, ContentInfo.Constants.SUBJECT,
                ContentInfo.Constants.CONTENT_STATE).count().as("count");
        criteria.andOperator(contextId, studentId);
        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = Aggregation.newAggregation(Aggregation.match(criteria), contentCount,
                Aggregation.project("count").and(ContentInfo.Constants.STUDENT_ID).previousOperation()
                        .and(ContentInfo.Constants.SUBJECT).previousOperation().and(ContentInfo.Constants.CONTENT_STATE)
                        .previousOperation()).withOptions(aggregationOptions);
        AggregationResults<StudentContentCount> groupResults = contentInfoDAO.getMongoOperations().aggregate(agg,
                ContentInfo.class.getSimpleName(), StudentContentCount.class);
        List<StudentContentCount> results = groupResults.getMappedResults();
        if (CollectionUtils.isEmpty(results)) {
            return mapIdToContent;
        }
        for (StudentContentCount result : results) {
            Map<String, StudentContentPojo> temp;
            if (map.containsKey(result.getStudentId())) {
                temp = map.get(result.getStudentId());
                StudentContentPojo studentCountPojo;
                if (temp.containsKey(result.getSubject())) {
                    studentCountPojo = temp.get(result.getSubject());
                } else {
                    studentCountPojo = new StudentContentPojo();
                    studentCountPojo.setSubject(result.getSubject());
                }
                temp.put(result.getSubject(), incrementStudentContent(studentCountPojo, result));
                // map.put(result.getStudentId(), temp);
            } else {
                temp = new HashMap<String, StudentContentPojo>();
                StudentContentPojo studentCountPojo = new StudentContentPojo();
                studentCountPojo.setSubject(result.getSubject());
                temp.put(result.getSubject(), incrementStudentContent(studentCountPojo, result));
            }
            map.put(result.getStudentId(), temp);
        }

        for (Entry<String, Map<String, StudentContentPojo>> entry : map.entrySet()) {
            List<StudentContentPojo> list = new ArrayList<StudentContentPojo>(entry.getValue().values());
            mapIdToContent.put(entry.getKey(), list);
        }
        return mapIdToContent;

    }

    public StudentContentPojo incrementStudentContent(StudentContentPojo studentCountPojo, StudentContentCount count) {
        if (count.getCount() != null) {
            if (count.getContentState().equals(ContentState.SHARED)) {
                studentCountPojo.setShared(studentCountPojo.getShared() + count.getCount());
            } else if (count.getContentState().equals(ContentState.ATTEMPTED)) {
                studentCountPojo.setShared(studentCountPojo.getShared() + count.getCount());
                studentCountPojo.setAttempted(studentCountPojo.getAttempted() + count.getCount());
            } else if (count.getContentState().equals(ContentState.EVALUATED)) {
                studentCountPojo.setShared(studentCountPojo.getShared() + count.getCount());
                studentCountPojo.setAttempted(studentCountPojo.getAttempted() + count.getCount());
                studentCountPojo.setEvaluated(studentCountPojo.getEvaluated() + count.getCount());
            }
        }
        return studentCountPojo;
    }

    public Map<Long, List<TeacherContentCount>> getTeacherContentForDashboard(Long startTime, Long endTime) throws BadRequestException {

        Map<Long, List<TeacherContentCount>> mapIdToContent = new HashMap<>();

        List<ContentInfo> contentInfos = new ArrayList<>();
        int start = 0, limit = 100;
        while (true) {
            List<ContentInfo> contentInfoTemp;
            try {
                Query query = new Query();
                query.addCriteria(Criteria.where(ContentInfo.Constants.LAST_UPDATED).gte(startTime).lt(endTime));
                query.fields().include(ContentInfo.Constants.CONTEXT_TYPE);
                query.fields().include(ContentInfo.Constants.ID);
                query.fields().include(ContentInfo.Constants.CONTENT_STATE);
                query.fields().include(ContentInfo.Constants.METADATA);
                query.fields().include(ContentInfo.Constants.TEACHER_ID);
                query.fields().include(ContentInfo.Constants.CREATION_TIME);

                contentInfoDAO.setFetchParameters(query, start, limit);
                contentInfoTemp = contentInfoDAO.runQuery(query, ContentInfo.class);
            } catch (Exception e) {
                break;
            }
            if (CollectionUtils.isNotEmpty(contentInfoTemp)) {
                contentInfos.addAll(contentInfoTemp);
            } else {
                break;
            }
            start += limit;
        }
        if (CollectionUtils.isEmpty(contentInfos)) {
            return mapIdToContent;
        }
        for (ContentInfo contentInfo : contentInfos) {
            TestContentInfoMetadata testContentInfoMetadata = null;
            if (contentInfo.getMetadata() != null) {
                testContentInfoMetadata = contentInfo.getMetadata();
            }
            Long teacherId = Long.parseLong(contentInfo.getTeacherId());
            TeacherContentCount teacherContentCount = null;
            List<CMDSTestAttempt> testAttempts=cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),Arrays.asList(CMDSTestAttempt.Constants.END_TIME,CMDSTestAttempt.Constants.ENDED_AT));
            if (mapIdToContent.containsKey(teacherId)) {
                Boolean context_not_exists = Boolean.TRUE;
                List<TeacherContentCount> teacherContentCounts = mapIdToContent.get(teacherId);
                for (TeacherContentCount teacherCount : teacherContentCounts) {
                    if (teacherCount.getContextType() != null
                            && teacherCount.getContextType().equals(contentInfo.getContextType())) {
                        incrementTeacherStateCount(teacherCount, testContentInfoMetadata, testAttempts, contentInfo, startTime,
                                endTime);
                        context_not_exists = Boolean.FALSE;
                    }
                }
                if (context_not_exists) {
                    teacherContentCount = new TeacherContentCount();
                    teacherContentCount.setTeacherId(teacherId.toString());
                    teacherContentCount.setContextType(contentInfo.getContextType());
                    incrementTeacherStateCount(teacherContentCount, testContentInfoMetadata, testAttempts, contentInfo, startTime,
                            endTime);
                    teacherContentCounts.add(teacherContentCount);
                }
            } else {
                teacherContentCount = new TeacherContentCount();
                teacherContentCount.setTeacherId(teacherId.toString());
                teacherContentCount.setContextType(contentInfo.getContextType());
                incrementTeacherStateCount(teacherContentCount, testContentInfoMetadata, testAttempts, contentInfo, startTime,
                        endTime);
                List<TeacherContentCount> teacherContentCounts = new ArrayList<>();
                teacherContentCounts.add(teacherContentCount);
                mapIdToContent.put(teacherId, teacherContentCounts);
            }

        }

        return mapIdToContent;
    }
    private void incrementTeacherStateCount(TeacherContentCount teacherContentCount,
                                            TestContentInfoMetadata testContentInfoMetadata,List<CMDSTestAttempt> testAttempts, ContentInfo contentInfo, Long start, Long end) {

        if (contentInfo.getCreationTime() != null && contentInfo.getCreationTime() > start
                && contentInfo.getCreationTime() < end) {
            teacherContentCount.setShared(teacherContentCount.getShared() + 1);
        }

        boolean testContentInfoMetaDataPresent = Objects.nonNull(testContentInfoMetadata);
        if (testContentInfoMetaDataPresent && Objects.nonNull(testContentInfoMetadata.getCreationTime())
                && testContentInfoMetadata.getCreationTime() > start
                && testContentInfoMetadata.getCreationTime() < end) {
            teacherContentCount.setAttempted(teacherContentCount.getAttempted() + 1);
        } else {
            if (testContentInfoMetaDataPresent && ArrayUtils.isNotEmpty(testAttempts)) {

                testAttempts.sort((o1, o2) -> {
                    if (o1.getEndTime() == null) {
                        return 1;
                    }

                    if (o2.getEndTime() == null) {
                        return -1;
                    }

                    return o1.getEndTime().compareTo(o2.getEndTime());
                });
                if (testAttempts.size() > 0) {
                    CMDSTestAttempt cMDSTestAttempt = testAttempts
                            .get(testAttempts.size() - 1);
                    if (cMDSTestAttempt.getEndedAt() != null && cMDSTestAttempt.getEndedAt() > start
                            && cMDSTestAttempt.getEndedAt() < end) {
                        teacherContentCount.setAttempted(teacherContentCount.getAttempted() + 1);
                    }
                }
            }
        }

        if (contentInfo.getContentState() != null && contentInfo.getContentState().equals(ContentState.EVALUATED)) {
            teacherContentCount.setEvaluated(teacherContentCount.getEvaluated() + 1);

        }
    }

    private ContentInfo toContentInfo(ShareContentReq req, String userId) {
        ContentInfo contentInfo = new ContentInfo();
        EngagementType engagementType;
        if (req.getContextType().equals(EntityType.OTF)) {
            engagementType = EngagementType.OTF;
        } else {
            engagementType = EngagementType.OTO;
        }
        contentInfo.setUserType(UserType.REGISTERED);
        contentInfo.setStudentId(userId);
        if (req.getTeacherId() != null) {
            contentInfo.setTeacherId(req.getTeacherId().toString());
        } else {
            contentInfo.setTeacherId(req.getCallingUserId().toString());
        }
        if (req.getTeacherName() != null) {
            contentInfo.setTeacherFullName(req.getTeacherName());
        }
        contentInfo.setBoardId(req.getBoardId());
        if (req.getBoardId() != null) {
            contentInfo.setBoardIds(new HashSet<>());
            contentInfo.getBoardIds().add(req.getBoardId());
        }
        String testLink;

        if (req.getContentType() == ContentType.ASSIGNMENT
                && StringUtils.isEmpty(req.getContentId())
                && org.apache.commons.lang3.StringUtils.containsIgnoreCase(req.getUrl(), "PUBLIC")) {
            String[] strings = req.getUrl().split("/");
            String string = strings.length > 0 ? strings[strings.length - 1] : "";
            if (ObjectId.isValid(string)) {
                req.setContentId(string);
            }
        }

        if (((StringUtils.isNotEmpty(req.getUrl()) && req.getUrl().contains("/cmds"))
                || StringUtils.isNotEmpty(req.getContentId()))) {
            CMDSTest cmdsTest = null;
            try {
                String testId = req.getContentId();
                if (StringUtils.isEmpty(testId)) {
                    Map<String, String> queryParams = CommonUtils.getParameterMapFromUrl(req.getUrl());
                    if (!queryParams.containsKey(ENTITY_ID) || !queryParams.containsKey(ENTITY_TYPE)) {
                        String errorMessage = "CMDS Entity id or entity type not found for url : " + req.getUrl();
                        logger.error(errorMessage);
                        throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, errorMessage);
                    }
                    testId = queryParams.get(ENTITY_ID);
                }
                testId = testId.trim();
                cmdsTest = cmdsTestManager.getCMDSTestById(testId);
            } catch (Exception ex) {
                logger.error(
                        "Error with parsing the contetnt url : " + req.toString() + " contentLink : " + req.getUrl(),
                        ex);
            }

            TestContentInfoMetadata metadata = new TestContentInfoMetadata(cmdsTest);
            contentInfo.setMetadata(metadata);
            contentInfo.setContentTitle(cmdsTest.getName());
            contentInfo.setContentInfoType(cmdsTest.getContentInfoType());
            if (cmdsTest != null && req.getBoardId() == null) {
                List<Long> boardIds = new ArrayList<>();

                if (cmdsTest.getBoardIds() != null) {

                    for (String boardId : cmdsTest.getBoardIds()) {
                        boardIds.add(Long.parseLong(boardId));
                    }
                    Map<Long, com.vedantu.board.pojo.Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
                    for (Map.Entry<Long, com.vedantu.board.pojo.Board> entry : boardMap.entrySet()) {

                        if (entry.getValue().getParentId() != null && entry.getValue().getParentId() == 0) {
                            contentInfo.setBoardId(entry.getValue().getId());
                            contentInfo.setSubject(entry.getValue().getName());
                            if (contentInfo.getBoardIds() == null) {
                                contentInfo.setBoardIds(new HashSet<>());
                            }

                            if (contentInfo.getSubjects() == null) {
                                contentInfo.setSubjects(new HashSet<>());
                            }

                            contentInfo.getBoardIds().add(entry.getValue().getId());
                            contentInfo.getSubjects().add(entry.getValue().getName());
                        }
                    }
                }
            } else {
                contentInfo.setBoardId(req.getBoardId());
                contentInfo.setBoardIds(new HashSet<>(Collections.singletonList(req.getBoardId())));
            }

            contentInfo.setAccessLevel(cmdsTest.getAccessLevel());
            contentInfo.setLmsType(LMSType.CMDS);
            try {
                if (req.getContentType() == ContentType.ASSIGNMENT || req.getContentType() == ContentType.ASSIGNMENT_PDF) {
                    testLink = req.getUrl();
                } else if (StringUtils.isNotEmpty(req.getContentId())) {
                    testLink = getCMDSContentLinkFromTestId(cmdsTest.getId());
                } else {
                    testLink = getCMDSContentLink(req.getUrl());
                }
            } catch (Exception e) {
                testLink = req.getUrl();
            }
        } else {
            testLink = req.getUrl();
            contentInfo.setBoardId(req.getBoardId());
        }
        if (contentInfo.getContentTitle() == null && req.getTitle() != null) {
            contentInfo.setContentTitle(req.getTitle());
        }

        contentInfo.setEngagementType(engagementType);
        contentInfo.setCourseName(req.getCourseName());
        contentInfo.setContentType(req.getContentType());
        contentInfo.setContentSubType(req.getContentSubType());
        contentInfo.setTopicNames(req.getTopicNames());

        contentInfo.setContentState(ContentState.SHARED);
        contentInfo.setContentLink(testLink);
        contentInfo.setStudentActionLink(testLink);
        contentInfo.setTeacherActionLink(testLink);
        contentInfo.setContextType(req.getContextType());
        contentInfo.setContextId(req.getContextId());

        return contentInfo;
    }

    public List<ContentInfo> shareContentByTeacher(ShareContentReq req)
            throws MalformedURLException, InternalServerErrorException {
        logger.info("Sharing content by teacher");

        List<ContentInfo> contentInfos = new ArrayList<>();
        List<String> contentInfoIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req.getStudentIds())) {
            for (Long userId : req.getStudentIds()) {
                ContentInfo contentInfo = toContentInfo(req, userId.toString());
                if (contentInfo != null) {
                    contentInfos.add(contentInfo);
                }
            }
        }
        if (ArrayUtils.isNotEmpty(contentInfos)) {
            logger.info("Inserting data inside ContentInfo table");
            contentInfoDAO.insertAllEntities(contentInfos, ContentInfo.class.getSimpleName());
            if(Objects.nonNull(req.getTeacherId())){
                ContentInfo contentInfo=contentInfos.get(0);
                TeacherContentDashboardIncrementInfo info=mapper.map(contentInfo,TeacherContentDashboardIncrementInfo.class);
                info.setIncrementCount(contentInfos.size());
                awsSNSManager.triggerSNS(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE, SNSSubject.UPDATE_TEACHER_CONTENT_DASHBOARD_INFO.name(),gson.toJson(info));
            }
            // sendContentInfoNotifications(contentInfo,
            // ContentInfoCommunicationType.SHARED, null);

        }
        return contentInfos;
    }

    public PlatformBasicResponse shareContentByTeacherForMigrationToSQS(ShareContentReq req) throws BadRequestException {
        req.verify();
        logger.info("Sharing content for migration");
        awsSQSManager.sendToSQS(SQSQueue.MIGRATION_NON_FIFO_OPS, SQSMessageType.CONTENT_INFO_ASSIGNMENT_MIGRATION,gson.toJson(req));

        return new PlatformBasicResponse();
    }

    public void shareContentByTeacherForMigration(ShareContentReq req) {

        logger.info("Content for migration {}",req);

        long creationTime=req.getCreationTime();

        List<ContentInfo> contentInfoList = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req.getStudentIds())) {
            ContentInfo contentInfo = toContentInfo(req, Integer.toString(1));
            if(Objects.nonNull(contentInfo)){
                contentInfoList.addAll(
                        req.getStudentIds()
                                .stream()
                                .map(Object::toString)
                                .map(studentId->contentInfo.createNewContentInfo(studentId,creationTime))
                                .collect(Collectors.toList())
                );
            }
        }
        if (ArrayUtils.isNotEmpty(contentInfoList)) {
            logger.info("Inserting data inside ContentInfo table for migration");
            contentInfoDAO.insertAllEntities(contentInfoList, ContentInfo.class.getSimpleName());
            logger.info("Inserted content infos are ");
            List<String> contentInfoIds = contentInfoList.stream().map(ContentInfo::getId).collect(Collectors.toList());
            contentInfoDAO.setUpMigrationChange(contentInfoIds,req.getCreationTime());
            if (Objects.nonNull(req.getTeacherId())) {
                ContentInfo contentInfo = contentInfoList.get(0);
                TeacherContentDashboardIncrementInfo info = mapper.map(contentInfo, TeacherContentDashboardIncrementInfo.class);
                info.setIncrementCount(contentInfoList.size());
                awsSNSManager.triggerSNS(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE, SNSSubject.UPDATE_TEACHER_CONTENT_DASHBOARD_INFO.name(), gson.toJson(info));
            }
        }
    }

    public ShareContentAndSendMailRes shareContentByTeacherAndSendMail(ShareContentReq req)
            throws MalformedURLException, InternalServerErrorException {
        ShareContentAndSendMailRes res = new ShareContentAndSendMailRes();
        List<ContentInfo> contentInfos = shareContentByTeacher(req);
        List<String> contentInfoIds = new ArrayList<>();
        List<MoodleContentInfo> moodleContentInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(contentInfos)) {
            for (ContentInfo contentInfo : contentInfos) {
                contentInfoIds.add(contentInfo.getId());
                MoodleContentInfo moodleContentInfo = mapper.map(contentInfo, MoodleContentInfo.class);// gson.fromJson(gson.toJson(contentInfo),
                // MoodleContentInfo.class);
                if (moodleContentInfo != null) {
                    moodleContentInfos.add(moodleContentInfo);
                }
            }
        }
        res.setContentInfoIds(contentInfoIds);
        res.setMoodleContentInfos(moodleContentInfos);
        return res;
    }

    public List<StudentContentStateCountPojo> getStudentContentCountForContextAndBoard(String userId, String contextId,
                                                                                       Long boardId, ContentType contentType) {
        Criteria criteria = new Criteria();
        Criteria contextIdQuery = Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(contextId);
        Criteria studentIdQuery = Criteria.where(ContentInfo.Constants.STUDENT_ID).is(userId);
        Criteria boardIdQuery = Criteria.where(ContentInfo.Constants.BOARD_ID).is(boardId);
        Criteria contentTypeQuery = Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST.toString());
        GroupOperation contentCount = Aggregation.group(ContentInfo.Constants.CONTENT_STATE).count().as("count");
        criteria.andOperator(contextIdQuery, studentIdQuery, boardIdQuery, contentTypeQuery);
        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agg = Aggregation.newAggregation(Aggregation.match(criteria), contentCount,
                Aggregation.project("count").and(ContentInfo.Constants.CONTENT_STATE).previousOperation()).withOptions(aggregationOptions);
        AggregationResults<StudentContentStateCountPojo> groupResults = contentInfoDAO.getMongoOperations()
                .aggregate(agg, ContentInfo.class.getSimpleName(), StudentContentStateCountPojo.class);
        List<StudentContentStateCountPojo> results = groupResults.getMappedResults();
        return results;
    }

    private Map<String, Float> getMaxScoreForTestIdAndContextId(String contextId, List<String> testIds) throws BadRequestException {
        Map<String, Float> testToMax = new HashMap<>();
        Criteria criteria = new Criteria();
        Criteria contextIdQuery = Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(contextId);
        Criteria contentStateQuery = Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED);
        Criteria testIdQuery = Criteria.where(ContentInfo.Constants.METADATA_TESTID).in(testIds);
        criteria.andOperator(contextIdQuery, contentStateQuery, testIdQuery);
        Query query = new Query();
        query.addCriteria(criteria);
        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class,Collections.singletonList(ContentInfo.Constants._ID));
        if (ArrayUtils.isNotEmpty(contentInfos)) {
            List<List<ContentInfo>> contentInfoPartition = Lists.partition(contentInfos, 100);
            for (List<ContentInfo> contentInfoList : contentInfoPartition) {
                List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoIdsOnlyForTestIdsAndMarksAchieved(contentInfoList.stream().map(ContentInfo::getId).collect(Collectors.toList()));
                if (ArrayUtils.isNotEmpty(testAttempts)) {
                    for (CMDSTestAttempt attempt : testAttempts) {
                        if (Objects.nonNull(attempt.getMarksAcheived())) {
                            if (testToMax.containsKey(attempt.getTestId())) {
                                if (testToMax.get(attempt.getTestId()) < attempt.getMarksAcheived()) {
                                    testToMax.put(attempt.getTestId(), attempt.getMarksAcheived());
                                }
                            } else {
                                testToMax.put(attempt.getTestId(), attempt.getMarksAcheived());
                            }
                        }
                    }
                }
            }

        }
        return testToMax;
    }

    public TestInfoForBatchProgress getTestInfoForStudent(StudentBatchProgressReq req) throws BadRequestException {
        req.verify();
        TestInfoForBatchProgress testInfoForBatchProgress = new TestInfoForBatchProgress();
        testInfoForBatchProgress.setUserId(req.getUserId());
        List<StudentTestBasicInfo> tests = new ArrayList<>();

        ContentType contentType = ContentType.TEST;
        List<StudentContentStateCountPojo> totalCount = getStudentContentCountForContextAndBoard(
                req.getUserId().toString(), req.getContextId(), req.getBoardId(), contentType);

        if (ArrayUtils.isNotEmpty(totalCount)) {
            for (StudentContentStateCountPojo count : totalCount) {
                if (count.getContentState().equals(ContentState.SHARED)) {
                    testInfoForBatchProgress.setShared(testInfoForBatchProgress.getShared() + count.getCount());
                } else if (count.getContentState().equals(ContentState.ATTEMPTED)) {
                    testInfoForBatchProgress.setShared(testInfoForBatchProgress.getShared() + count.getCount());
                    testInfoForBatchProgress.setAttempted(testInfoForBatchProgress.getAttempted() + count.getCount());
                } else if (count.getContentState().equals(ContentState.EVALUATED)) {
                    testInfoForBatchProgress.setShared(testInfoForBatchProgress.getShared() + count.getCount());
                    testInfoForBatchProgress.setAttempted(testInfoForBatchProgress.getAttempted() + count.getCount());
                    testInfoForBatchProgress.setEvaluated(testInfoForBatchProgress.getEvaluated() + count.getCount());
                }
            }
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(req.getContextId()));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(req.getUserId().toString()));
        query.addCriteria(Criteria.where(ContentInfo.Constants.BOARD_ID).is(req.getBoardId()));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).exists(true));
        contentInfoDAO.setFetchParameters(query, req);
        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.CREATION_TIME));

        List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);

        List<String> testIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(contentInfos)) {
            for (ContentInfo contentInfo : contentInfos) {

                if (contentInfo.getMetadata() != null) {
                    TestContentInfoMetadata testContentInfoMetadata = contentInfo
                            .getMetadata();
                    if (StringUtils.isNotEmpty(testContentInfoMetadata.getTestId())) {
                        testIds.add(testContentInfoMetadata.getTestId());
                    }
                }
            }
            Map<String, Float> maxScore = getMaxScoreForTestIdAndContextId(req.getContextId(), testIds);
            if (maxScore == null) {
                maxScore = new HashMap<>();
            }
            for (ContentInfo contentInfo : contentInfos) {
                if (contentInfo.getMetadata() != null) {
                    TestContentInfoMetadata testContentInfoMetadata = contentInfo
                            .getMetadata();
                    StudentTestBasicInfo studentTestBasicInfo = new StudentTestBasicInfo();
                    studentTestBasicInfo.setSharedTime(contentInfo.getCreationTime());
                    studentTestBasicInfo.setTestId(testContentInfoMetadata.getTestId());
                    studentTestBasicInfo.setTitle(contentInfo.getContentTitle());
                    studentTestBasicInfo.setStudentActionLink(contentInfo.getStudentActionLink());
                    studentTestBasicInfo.setContentInfoId(contentInfo.getId());
                    studentTestBasicInfo.setTotalScore(testContentInfoMetadata.getTotalMarks());
                    studentTestBasicInfo.setHighestScore(maxScore.get(testContentInfoMetadata.getTestId()));

                    if (contentInfo.getContentInfoType() != null) {
                        studentTestBasicInfo.setTestType(contentInfo.getContentInfoType().toString());
                    }
                    studentTestBasicInfo.setTestState(contentInfo.getContentState().toString());
                    List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),Arrays.asList(CMDSTestAttempt.Constants.START_TIME,CMDSTestAttempt.Constants.MARKS_ACHIEVED,CMDSTestAttempt.Constants.ENDED_AT));
                    if (ArrayUtils.isNotEmpty(testAttempts)) {
                        testAttempts.sort((o1, o2) -> o2.getStartTime().compareTo(o1.getStartTime()));
                        CMDSTestAttempt finalTestAttempt = testAttempts.get(0);
                        studentTestBasicInfo.setScore(finalTestAttempt.getMarksAcheived());
                        studentTestBasicInfo.setLastAttemptedOn(finalTestAttempt.getEndedAt());
                    }

                    tests.add(studentTestBasicInfo);
                }
            }
        }
        testInfoForBatchProgress.setTests(tests);
        return testInfoForBatchProgress;
    }

    public List<ContentInfo> getContentInfosForStudents(Long boardId, String contextId, List<String> studentIds,
                                                        Integer start, Integer size, List<String> testIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.BOARD_ID).is(boardId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(contextId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).in(studentIds));
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA).exists(true));
        // query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).in(Arrays.asList(ContentState.ATTEMPTED,ContentState.EVALUATED)));;

        if (ArrayUtils.isNotEmpty(testIds)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).in(testIds));
        }
        if (size != null && start != null) {
            contentInfoDAO.setFetchParameters(query, start, size);
        }
        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.CREATION_TIME));
        List<ContentInfo> contents = contentInfoDAO.runQuery(query, ContentInfo.class);

        return contents;
    }

    public Map<String, List<StudentTestBasicInfo>> getBatchTestScoreInfo(TeacherBatchProgressReq req)
            throws BadRequestException {
        req.verify();
        if (ArrayUtils.isEmpty(req.getStudentIds())) {
            throw new BadRequestException(ErrorCode.STUDENTS_NOT_FOUND, "No students found in Req");
        }
        Map<String, List<StudentTestBasicInfo>> mapIdToListTests = new HashMap<>();
        Integer size = 10;
        if (req.getSize() != null && req.getSize() <= 10) {
            size = req.getSize();
        }

        Float tempScore = 0.0f;
        List<String> testIds = new ArrayList<>();
        List<ContentInfo> contentForOne = getContentInfosForStudents(req.getBoardId(), req.getContextId(),
                Arrays.asList(req.getStudentIds().get(0)), req.getStart(), req.getSize(), null);
        if (ArrayUtils.isEmpty(contentForOne)) {

            return mapIdToListTests;
        }
        Map<String, LinkedHashMap<String, StudentTestBasicInfo>> mapUserIdtoTest = new HashMap<>();
        for (ContentInfo contentInfo : contentForOne) {
            if (contentInfo.getMetadata() == null) {
                continue;
            }
            TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();
            if (testContentInfoMetadata.getTestId() != null) {
                testIds.add(testContentInfoMetadata.getTestId());
                for (String studentId : req.getStudentIds()) {
                    if (mapUserIdtoTest.containsKey(studentId)) {
                        LinkedHashMap<String, StudentTestBasicInfo> mapTestIdToInfo = mapUserIdtoTest.get(studentId);
                        if (mapTestIdToInfo.containsKey(testContentInfoMetadata.getTestId())) {
                            continue;
                        } else {
                            StudentTestBasicInfo basicInfo = new StudentTestBasicInfo();
                            basicInfo.setTestId(testContentInfoMetadata.getTestId());
                            basicInfo.setTotalScore(testContentInfoMetadata.getTotalMarks());
                            basicInfo.setTestState("SHARED");
                            basicInfo.setTitle(contentInfo.getContentTitle());
                            basicInfo.setTestType(contentInfo.getContentInfoType().toString());
                            basicInfo.setTeacherId(contentInfo.getTeacherId());
                            mapTestIdToInfo.put(testContentInfoMetadata.getTestId(), basicInfo);
                            mapUserIdtoTest.put(studentId, mapTestIdToInfo);
                        }
                    } else {
                        LinkedHashMap<String, StudentTestBasicInfo> mapTestIdToInfo = new LinkedHashMap<>();
                        StudentTestBasicInfo basicInfo = new StudentTestBasicInfo();
                        basicInfo.setTestId(testContentInfoMetadata.getTestId());
                        basicInfo.setTotalScore(testContentInfoMetadata.getTotalMarks());
                        basicInfo.setTestState("SHARED");
                        basicInfo.setTitle(contentInfo.getContentTitle());
                        basicInfo.setTestType(contentInfo.getContentInfoType().toString());
                        basicInfo.setTeacherId(contentInfo.getTeacherId());
                        mapTestIdToInfo.put(testContentInfoMetadata.getTestId(), basicInfo);
                        mapUserIdtoTest.put(studentId, mapTestIdToInfo);
                    }
                }
            }
        }
        List<ContentInfo> contents = getContentInfosForStudents(req.getBoardId(), req.getContextId(),
                req.getStudentIds(), null, null, testIds);
        for (ContentInfo contentInfo : contents) {
            LinkedHashMap<String, StudentTestBasicInfo> mapTestIdToInfo = null;
            StudentTestBasicInfo basicInfo = null;

            if (contentInfo.getMetadata() == null) {
                continue;
            }

            TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();

            // logger.info("Before Map student ID is " + contentInfo.getStudentId());
            if (mapUserIdtoTest.containsKey(contentInfo.getStudentId())) {
                mapTestIdToInfo = mapUserIdtoTest.get(contentInfo.getStudentId());
                // logger.info("Mapped student ID is " + contentInfo.getStudentId());
                if (testContentInfoMetadata.getTestId() != null
                        && mapTestIdToInfo.containsKey(testContentInfoMetadata.getTestId())) {
                    // logger.info("Mapped test ID is " + testContentInfoMetadata.getTestId());
                    basicInfo = mapTestIdToInfo.get(testContentInfoMetadata.getTestId());
                }
            }
            if (basicInfo == null) {
                logger.info("Could not map test to userId");
                continue;
            }
            basicInfo.setSharedTime(contentInfo.getCreationTime());
            List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),Arrays.asList(CMDSTestAttempt.Constants.MARKS_ACHIEVED,CMDSTestAttempt.Constants.END_TIME,CMDSTestAttempt.Constants.STARTED_AT));
            if (ArrayUtils.isEmpty(testAttempts)) {
                continue;
            }
            testAttempts.sort((o1, o2) -> {
                if (o2 == null || o2.getEndTime() == null) {
                    return -1;
                } else if (o1 == null || o1.getEndTime() == null) {
                    return 1;
                }
                return o2.getEndTime().compareTo(o1.getEndTime());
            });

            if (testAttempts.get(0).getMarksAcheived() != null) {
                basicInfo.setScore(testAttempts.get(0).getMarksAcheived());
                basicInfo.setLastAttemptedOn(testAttempts.get(0).getStartedAt());
                basicInfo.setTestState(contentInfo.getContentState().toString());
            }
        }
        for (Entry<String, LinkedHashMap<String, StudentTestBasicInfo>> entry : mapUserIdtoTest.entrySet()) {
            List<StudentTestBasicInfo> list = new ArrayList<>(entry.getValue().values());
            mapIdToListTests.put(entry.getKey(), list);
        }

        return mapIdToListTests;
    }

    public ContentInfo createContentInfoForStudent(ContentInfo currentContentInfo, String studentId) throws BadRequestException {
        ContentInfo contentInfo = new ContentInfo();

        contentInfo.setUserType(UserType.REGISTERED);
        contentInfo.setStudentId(studentId);
        contentInfo.setTeacherId(currentContentInfo.getTeacherId());
        contentInfo.setTeacherFullName(currentContentInfo.getTeacherFullName());
        contentInfo.setBoardId(currentContentInfo.getBoardId());
        if (currentContentInfo.getBoardId() != null) {
            contentInfo.setBoardIds(new HashSet<>());
            contentInfo.getBoardIds().add(currentContentInfo.getBoardId());
            if (ArrayUtils.isNotEmpty(currentContentInfo.getBoardIds())) {
                contentInfo.getBoardIds().addAll(currentContentInfo.getBoardIds());
            }
        }

        if (currentContentInfo.getSubjects() != null) {
            contentInfo.setSubjects(new HashSet<>(currentContentInfo.getSubjects()));
        }

        String testLink = null;
        if (currentContentInfo.getMetadata() != null) {
            CMDSTest cmdsTest = null;
            TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) currentContentInfo
                    .getMetadata();
            if (testContentInfoMetadata != null && StringUtils.isNotEmpty(testContentInfoMetadata.getTestId())) {
                String testId = testContentInfoMetadata.getTestId();
                cmdsTest = cmdsTestManager.getCMDSTestById(testId);

                if (cmdsTest != null) {
                    TestContentInfoMetadata metadata = new TestContentInfoMetadata(cmdsTest);
                    contentInfo.setMetadata(metadata);
                    contentInfo.setContentTitle(cmdsTest.getName());
                    contentInfo.setContentInfoType(cmdsTest.getContentInfoType());
                    if (cmdsTest.getBoardIds() != null && currentContentInfo.getBoardId() == null) {
                        List<Long> boardIds = new ArrayList<>();
                        for (String boardId : cmdsTest.getBoardIds()) {
                            boardIds.add(Long.parseLong(boardId));
                        }
                        Map<Long, com.vedantu.board.pojo.Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
                        for (Map.Entry<Long, com.vedantu.board.pojo.Board> entry : boardMap.entrySet()) {
                            if (entry.getValue().getParentId() != null && entry.getValue().getParentId() == 0) {
                                contentInfo.setBoardId(entry.getValue().getId());
                                contentInfo.setSubject(entry.getValue().getName());
                                if (contentInfo.getBoardIds() == null) {
                                    contentInfo.setBoardIds(new HashSet<>());
                                }

                                if (contentInfo.getSubjects() == null) {
                                    contentInfo.setSubjects(new HashSet<>());
                                }

                                contentInfo.getBoardIds().add(entry.getValue().getId());
                                contentInfo.getSubjects().add(entry.getValue().getName());

                            }
                        }

                    }

                    contentInfo.setAccessLevel(cmdsTest.getAccessLevel());
                    contentInfo.setLmsType(LMSType.CMDS);
                    testLink = getCMDSContentLinkFromTestId(cmdsTest.getId());
                }
            }
        } else {
            testLink = currentContentInfo.getContentLink();
        }
        if (contentInfo.getContentTitle() == null && currentContentInfo.getContentTitle() != null) {
            contentInfo.setContentTitle(currentContentInfo.getContentTitle());
        }
        contentInfo.setEngagementType(currentContentInfo.getEngagementType());
        contentInfo.setCourseName(currentContentInfo.getCourseName());
        contentInfo.setContentType(currentContentInfo.getContentType());
        contentInfo.setTopicNames(currentContentInfo.getTopicNames());
        contentInfo.setContentSubType(currentContentInfo.getContentSubType());

        contentInfo.setContentState(ContentState.SHARED);
        contentInfo.setContentLink(testLink);
        contentInfo.setStudentActionLink(testLink);
        contentInfo.setTeacherActionLink(testLink);
        contentInfo.setContextType(currentContentInfo.getContextType());
        contentInfo.setContextId(currentContentInfo.getContextId());

        return contentInfo;
    }

    public void shareContentOnEnrollment(List<String> contentInfoIds, String studentId, String contextId,
                                         Long startTime, Long endTime) throws BadRequestException {
        //Map<String, String> oldToNewContentInfo = new HashMap<>();
        if (ArrayUtils.isEmpty(contentInfoIds) || StringUtils.isEmpty(studentId) || StringUtils.isEmpty(contextId)) {
            return;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants._ID).in(contentInfoIds));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(contextId));
        if (startTime != null) {
            if (endTime != null) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(startTime).lte(endTime));
            } else {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(startTime));
            }
        }
        query.with(Sort.by(Sort.Direction.ASC, ContentInfo.Constants.CREATION_TIME));
        List<ContentInfo> contents = contentInfoDAO.runQuery(query, ContentInfo.class);
        List<ContentInfo> toShare = new ArrayList<>();
        //Map<String, String> studentLinkContentInfo = new HashMap<>();

        for (ContentInfo content : contents) {
            //studentLinkContentInfo.put(content.getContentLink(), content.getId());
            ContentInfo newContent = createContentInfoForStudent(content, studentId);
            toShare.add(newContent);
            if(StringUtils.isNotEmpty(newContent.getTeacherId())){
                TeacherContentDashboardIncrementInfo info=mapper.map(newContent,TeacherContentDashboardIncrementInfo.class);
                awsSNSManager.triggerSNS(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE,SNSSubject.UPDATE_TEACHER_CONTENT_DASHBOARD_INFO.name(),gson.toJson(info));

            }
        }
        if (ArrayUtils.isNotEmpty(toShare)) {
            contentInfoDAO.insertAllEntities(toShare, ContentInfo.class.getSimpleName());
//            for (ContentInfo contentInfo : toShare) {
//                if (studentLinkContentInfo.containsKey(contentInfo.getContentLink())) {
//                    oldToNewContentInfo.put(studentLinkContentInfo.get(contentInfo.getContentLink()),
//                            contentInfo.getId());
//                }
//            }
        }
    }

    public List<ContentInfo> getTestInfoForBatches(List<String> batchIds, String userId) throws BadRequestException {
        if (StringUtils.isEmpty(userId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userIds should be present");
        } else if (ArrayUtils.isEmpty(batchIds)) {
            return new ArrayList<>();
        }
        return contentInfoDAO.getTestInfoForBatches(batchIds, userId);
    }

    public List<ContentInfo> getContentInfosByContextId(List<String> contextId, Long afterCreationTime,
                                                        Long beforeCreationTime) throws BadRequestException {
        if (ArrayUtils.isEmpty(contextId)) {
            return new ArrayList<>();
        }

        return contentInfoDAO.getContentInfosByContextId(contextId, afterCreationTime, beforeCreationTime);
    }

    public List<ContentInfo> getContentInfosByFilter(ContentInfoReq req) throws BadRequestException {

        return contentInfoDAO.getContentInfosByFilter(req);
    }

    public void changeTeacherIdForContent(String teacherId, String testId, String callingUserId) throws VException {

        com.vedantu.User.UserBasicInfo userInfo = fosUtils.getUserBasicInfo(teacherId, false);
        if (!Role.TEACHER.equals(userInfo.getRole())) {
            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "teacherId is not teacher : " + teacherId);
        }
        contentInfoDAO.updateTeacherId(teacherId, testId, userInfo.getFullName(), callingUserId);

        Map<String, Object> payload = new HashMap<>();
        payload.put("testId", testId);
        payload.put("teacherId", teacherId);
        payload.put("callingUserId", callingUserId);
        AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.CHANGE_TEACHER_FOR_ATTEMPT, payload);
        asyncTaskFactory.executeTask(asyncTaskParams);
    }

    public void changeTeacherIdForAttempt(String teacherId, String testId)
            throws BadRequestException {
        com.vedantu.User.UserBasicInfo userInfo = fosUtils.getUserBasicInfo(teacherId, false);
        if (!Role.TEACHER.equals(userInfo.getRole())) {
            throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "teacherId is not teacher : " + teacherId);
        }

        cmdsTestAttemptDAO.updateTeacherIdForTestAttempt(testId,teacherId);

//        int size = 100;
//        String lastFetchedId = null;
//        while (true) {
//            List<ContentInfo> contentInfos = contentInfoDAO.getContentInfos(testId, lastFetchedId, size);
//
//            if (ArrayUtils.isEmpty(contentInfos)) {
//                break;
//            }else{
//                lastFetchedId = contentInfos.get(contentInfos.size() - 1).getId();
//            }
//
//            if (ArrayUtils.isNotEmpty(contentInfos)) {
//                for (ContentInfo contentInfo : contentInfos) {
//                    TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();
//                    if (testContentInfoMetadata != null && (ContentState.ATTEMPTED.equals(contentInfo.getContentState())
//                            || ContentState.SHARED.equals(contentInfo.getContentState()))) {
//
//                        List<CMDSTestAttempt> testAttempts = testContentInfoMetadata.getTestAttempts();
//                        if (ArrayUtils.isNotEmpty(testAttempts)) {
//                            for (CMDSTestAttempt cMDSTestAttempt : testAttempts) {
//                                if (CMDSAttemptState.ATTEMPT_STARTED.equals(cMDSTestAttempt.getAttemptState())
//                                        || CMDSAttemptState.ATTEMPT_COMPLETE
//                                        .equals(cMDSTestAttempt.getAttemptState())) {
//
//                                    cMDSTestAttempt.setTeacherId(teacherId);
//                                }
//                            }
//                        }
//                        contentInfoDAO.save(contentInfo);
//                    }
//                }
//            }
//
//            if (contentInfos.size() < size) {
//                break;
//            }
//
//        }
    }

    public void calculateTestRank(CMDSTest test) throws BadRequestException {
        if (Objects.isNull(test)) {
            logger.info("Test cann't be null.");
            return;
        }

        if (EnumBasket.TestTagType.NORMAL.equals(test.getTestTag()) ||
                Objects.isNull(test.getTestTag()) ||
                Objects.isNull(test.getMaxStartTime())) {
            logger.info("This test is not eligible for emails: {}", test.getId());
            return;
        }

        logger.info("Calculating test rank for test: {}", test.getId());


        int rank = 1;
        int lastRank = 1;

        float totalMarks = 0;

        int skip=0;
        int limit=1000;

        Float lastMarksPercent = null;

        long totalAttempts=cmdsTestAttemptDAO.getAttemptsforRankCalculation(test.getId(),test.getMaxStartTime());
        logger.info("totalAttempts - {}",totalAttempts);
        if(totalAttempts>0){
            List<CMDSTestAttempt> attemptsForRank = cmdsTestAttemptDAO.getFirstValidEndedTestAttemptsForTestId(test.getId(),test.getMaxStartTime(),skip,limit);
            logger.info("attemptsForRank - {}",attemptsForRank);
            while(ArrayUtils.isNotEmpty(attemptsForRank)){

                logger.info("skip - {}, limit - {}",skip, limit);

                for (CMDSTestAttempt attempt : attemptsForRank) {
                    logger.info("Processing for attempt id {}",attempt.getId());
                    Float attemptMarksPercent = (attempt.getMarksAcheived() * 100) / attempt.getTotalMarks();
                    logger.info("marksAchieved - {}",attempt.getMarksAcheived());
                    logger.info("totalMarks - {}",attempt.getTotalMarks());
                    logger.info("attemptMarksPercent - {}",attemptMarksPercent);

                    if (Objects.isNull(lastMarksPercent)) {
                        lastMarksPercent = attemptMarksPercent;
                    }

                    logger.info("lastMarksPercent - {}",lastMarksPercent);
                    int currentRank;
                    if (lastMarksPercent.equals(attemptMarksPercent)){
                        currentRank=lastRank;
                    } else {
                        currentRank=rank;
                        lastRank = rank;
                        lastMarksPercent = attemptMarksPercent;
                    }
                    logger.info("currentRank - {}",currentRank);
                    float testPercentile = ((totalAttempts - rank) * 100f) / totalAttempts;

                    ++rank;
                    totalMarks += attempt.getMarksAcheived();

                    logger.info("Updating rank for attempt id {}",attempt.getId());
                    cmdsTestAttemptDAO.updateRankRelatedDataForAttemptId(attempt.getId(),attemptMarksPercent,currentRank,testPercentile,(int)totalAttempts);
                }

                skip+=limit;
                attemptsForRank = cmdsTestAttemptDAO.getFirstValidEndedTestAttemptsForTestId(test.getId(),test.getMaxStartTime(),skip,limit);
            }

            int batchAvg = (int) (totalMarks / totalAttempts);
            cmdsTestAttemptDAO.updateBatchAvgScore(test.getId(),test.getMaxStartTime(),batchAvg);

        }
    }

    public List<ContentInfo> getContentInfosForTestId(String testId, String lastFetchedId, int size) {
        return  getContentInfosForTestId(testId,lastFetchedId,size,null);
    }
    public List<ContentInfo> getContentInfosForTestId(String testId, String lastFetchedId, int size,List<String> includeFields) {
        return contentInfoDAO.getContentInfos(testId, lastFetchedId, size,includeFields);
    }

    public List<ContentInfo> getContentInfoForTests(String userId, List<String> testIds) {
        return contentInfoDAO.getContentInfoForTests(userId, testIds);
    }

    public List<ContentInfo> getTestContentInfosForStudents(List<String> studentIds, Long fromTime, Long thruTime) {

        Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(fromTime), Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(thruTime));
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).in(studentIds));
        query.addCriteria(andCriteria);
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA).exists(true));
        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.EXPIRY_TIME));

        List<ContentInfo> contents = contentInfoDAO.runQuery(query, ContentInfo.class);

        return contents;
    }

    public List<ContentInfo> getContentsForStudentsByFilter(com.vedantu.lms.request.GetContentsReq getContentsReq) throws BadRequestException {

        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).in(getContentsReq.getStudentIds()));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).in(getContentsReq.getContentState()));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gt(getContentsReq.getFromTime()));
        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.CREATION_TIME));

        logger.info("getContentsForStudentsByFilter Query ::{}", query);

        List<ContentInfo> contents = contentInfoDAO.runQuery(query, ContentInfo.class);

        if(ArrayUtils.isNotEmpty(contents)){
            int size=contents.size();
            for(int i=0;i<size;++i){
                ContentInfo contentInfo=contents.get(i);
                contentInfo.getMetadata().setTestAttempts(cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),null));
            }
        }

        return contents;
    }

    public void processContentInfoForGameAsync(ContentInfo contentInfo) {
        cachedThreadPoolUtility.execute(() -> processContentInfoForGame(contentInfo));
    }

    private void processContentInfoForGame(ContentInfo contentInfo) {
        try {
            logger.info("WATCHED TEST ATTEMPT");
            if (contentInfo.getContentType() != ContentType.TEST) {
                logger.info("contentType is not TEST");
                return;
            }

            TestContentInfoMetadata testContentInfoMetadata = contentInfo.getMetadata();

            if (testContentInfoMetadata.getDuration() == null) {
                logger.info("no duration, its not a test");
                return;
            }

            List<CMDSTestAttempt> testAttempts = cmdsTestAttemptDAO.getTestAttemptsByContentInfoId(contentInfo.getId(),Arrays.asList(CMDSTestAttempt.Constants.TIME_TAKEN_BY_STUDENT,CMDSTestAttempt.Constants.DURATION,CMDSTestAttempt.Constants.START_TIME));
            if (ArrayUtils.isEmpty(testAttempts)) {
                logger.info("no test attempts");
                return;
            }

            logger.info("getting latest attempt for testId: " + testContentInfoMetadata.getTestId() + " for userId: " + contentInfo.getStudentId());
            CMDSTestAttempt testAttempt = testAttempts.get(testAttempts.size() - 1);
            if (testAttempt.getEndedAt() == null) {
                logger.info("test is ongoing...");
                return;
            }
            LMSTestInfo lmsTestInfo = new LMSTestInfo();
            Long timeTakenByStudent = testAttempt.getTimeTakenByStudent();
            Long testDuration = testAttempt.getDuration();
            long userId;
            try {
                userId = Long.parseLong(contentInfo.getStudentId());
            } catch (NumberFormatException e) {
                return;
            }
            lmsTestInfo.setUserId(userId);
            lmsTestInfo.setTestAttemptId(testAttempt.getId());
            logger.info("testContentInfo Id: " + testAttempt.getId());
            lmsTestInfo.setTestStartTime(testAttempt.getStartTime());
            lmsTestInfo.setTestDuration(testDuration);
            lmsTestInfo.setAttemptDuration(timeTakenByStudent);
            logger.info("sending test attemptId to game journey queue");
            String message = gson.toJson(lmsTestInfo);

            sqsManager.sendToSQS(SQSQueue.GAME_JOURNEY_TEST_OPS, SQSMessageType.GAME_TEST_ACTIVITY, message,
                    contentInfo.getStudentId());

        } catch (Exception e) {
            logger.info("exception in consumeWatchedEntity while watching tests");
            logger.error(e.getMessage(), e);
        }

    }

    public Long getRecentlyAttemptedTests(String userId, Long releaseDate) {
        Long testCount = contentInfoDAO.getTestInfoByUserId(userId, releaseDate);
        logger.info("Test attempt count : {}", testCount);
        return testCount;
    }

    public StudentDashboardInfo getTestContentCountsForStudent(Long boardId, String studentId, String contextId, ContentType contentType) {
        int sharedCount = 0;
        int attemptedCount = 0;
        int evaluatedCount = 0;
        final String contentCountsKey = "STUDENT_BATCH_PROGRESS_STATS_" + studentId + "_" + contextId + "_" + boardId;
        Map<String, Integer> contentCountsMap = new HashMap<>();

        try {
            Type setType = new TypeToken<Map<String, Integer>>() {
            }.getType();
            String contentCountsJson = redisDAO.get(contentCountsKey);

            if (!Objects.isNull(contentCountsJson)) {
                contentCountsMap = new Gson().fromJson(contentCountsJson, setType);
                logger.info("counts found in redis for batch progress S:{} A:{} E:{}",
                        sharedCount, attemptedCount, evaluatedCount);
                sharedCount = contentCountsMap.getOrDefault("sharedCount", 0);
                attemptedCount = contentCountsMap.getOrDefault("attemptedCount", 0);
                evaluatedCount = contentCountsMap.getOrDefault("evaluatedCount", 0);
            } else {
                sharedCount = (int) contentInfoDAO.getTestInfosCountForStudent(boardId, studentId,
                        contextId, contentType, ContentState.SHARED);
                attemptedCount = (int) contentInfoDAO.getTestInfosCountForStudent(boardId, studentId,
                        contextId, contentType, ContentState.ATTEMPTED);
                evaluatedCount = (int) contentInfoDAO.getTestInfosCountForStudent(boardId, studentId,
                        contextId, contentType, ContentState.EVALUATED);

                attemptedCount += evaluatedCount;
                sharedCount += attemptedCount;

                contentCountsMap.put("sharedCount", sharedCount);
                contentCountsMap.put("attemptedCount", attemptedCount);
                contentCountsMap.put("evaluatedCount", evaluatedCount);

                try {
                    redisDAO.setex(contentCountsKey, gson.toJson(contentCountsMap), 300);
                } catch (InternalServerErrorException e) {
                    logger.warn("Error while setting test content counts for {} ", studentId);
                }
            }

        } catch (BadRequestException | InternalServerErrorException e) {
            logger.warn("Error while fetching test content counts for {}", studentId);
        }

        logger.info("test stats counts for {} - shareCount: {}, attemptedCount: {}", studentId, sharedCount, attemptedCount);
        return StudentDashboardInfo.builder().sharedCount(sharedCount).attemptedCount(attemptedCount).contentType(contentType).build();
    }
//    public void migrateTestAttemptsFromContentInfo() {
//        long skip=0;
//        int limit=100;
//        while(true){
//            List<ContentInfo> contentInfos=contentInfoDAO.getContentInfosForMigration(skip,limit);
//            if(ArrayUtils.isEmpty(contentInfos)){
//                break;
//            }
//            List<String> ids=contentInfos.stream().map(ContentInfo::getId).collect(Collectors.toList());
//            logger.info("ids - {}\nskip - {}\nlimit - {}",ids,skip,limit);
//            for(ContentInfo contentInfo:contentInfos){
//                if(Objects.nonNull(contentInfo.getMetadata())){
//                    List<CMDSTestAttempt> testAttempts = contentInfo.getMetadata().getTestAttempts();
//                    int attemptIndex=0;
//                    if(ArrayUtils.isNotEmpty(testAttempts)){
//                        Set<String> contentInfoSubjects = contentInfo.getSubjects();
//                        if(ArrayUtils.isEmpty(contentInfoSubjects) && ArrayUtils.isNotEmpty(contentInfo.getBoardIds())){
//                            Map<Long, Board> boardInfoMap = fosUtils.getBoardInfoMap(contentInfo.getBoardIds());
//                            contentInfoSubjects=
//                                    Optional.ofNullable(boardInfoMap.values())
//                                            .map(Collection::stream)
//                                            .orElseGet(Stream::empty)
//                                            .map(Board::getName)
//                                            .collect(Collectors.toSet());
//
//                        }
//                        for(CMDSTestAttempt cmdsTestAttempt : testAttempts) {
//                            cmdsTestAttempt.setContentInfoId(contentInfo.getId());
//                            cmdsTestAttempt.setStudentId(contentInfo.getStudentId());
//                            cmdsTestAttempt.setAttemptIndex(++attemptIndex);
//                            cmdsTestAttempt.setContentInfoType(contentInfo.getContentInfoType());
//                            cmdsTestAttempt.setSubjects(contentInfoSubjects);
//                        }
//                        cmdsTestAttemptDAO.insertAllEntities(testAttempts);
//                        logger.info("attemptIdsMigrated - {}",testAttempts.stream().map(CMDSTestAttempt::getId).collect(Collectors.toList()));
//                    }
//                }
//            }
//            contentInfoDAO.markContentInfosMigrated(ids);
//            skip+=limit;
//        }
//
//    }


    private boolean isLiveOrBeingAttempted(HomepageTestResponse test) {
        final Long currentTime = System.currentTimeMillis();

        // live and currently being attempted if attempt state is ATTEMPT_STARTED
        if (Objects.nonNull(test.getFirstAttempt())) {
            CMDSTestAttempt firstAttempt = test.getFirstAttempt();

            // not live - if first attempt didn't fall in between min and max start
            logger.info("attempt found for test: {} state: {}", test.getTestId(), firstAttempt.getAttemptState());
            return CMDSAttemptState.ATTEMPT_STARTED.equals(firstAttempt.getAttemptState())
                    && Objects.isNull(firstAttempt.getEndedAt())
                    && firstAttempt.getStartedAt() < test.getAttemptBy();
        }

        // consider as past test - if not attempted before maxStartTime
        if (Objects.nonNull(test.getAttemptBy()) && test.getAttemptBy() < currentTime) {
            logger.info("max start time passed for: {}", test.getTestId());
            return false;
        }

        // live yet not attempted
        return true;
    }

    public List<HomepageTestResponse> getLiveAndPastUnattemptedTests(String studentId) throws BadRequestException {
        if (StringUtils.isEmpty(studentId)) {
            return new ArrayList<>();
        }

        final List<String> includeFieldsPastTests = Arrays.asList(_ID, SUBJECTS, METADATA_DURATION, METADATA_TESTID,
                CONTENT_TITLE, BOARD_IDS, CONTENT_LINK, CREATION_TIME, METADATA_MIN_START_TIME, METADATA_MAX_START_TIME,
                METADATA_EXPIRY_DATE);

        List<ContentInfo> competitiveTests = contentInfoDAO.getLiveTestsForStudent(studentId, MAX_LIVE_UNATTEMPTED_TESTS, includeFieldsPastTests);
        // <contentInfoId, TestAttempt>
        Set<String> competitiveTestContentInfoIds = Optional.ofNullable(competitiveTests).orElseGet(ArrayList::new)
                .stream()
                .map(AbstractMongoStringIdEntity::getId)
                .collect(Collectors.toSet());
        Map<String, CMDSTestAttempt> competitiveTestAttemptMap = cmdsTestAttemptManager.getAttemptsForContentInfoIds(competitiveTestContentInfoIds);

        List<HomepageTestResponse> liveOrUnattempted = HomepageTestResponse.generateCompetitiveTestResponse(competitiveTests, competitiveTestAttemptMap);
        liveOrUnattempted = Optional.of(liveOrUnattempted)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(this::isLiveOrBeingAttempted)
                .peek(logger::info)
                .sorted(Comparator.comparingLong(HomepageTestResponse::getAttemptBy))
                .collect(Collectors.toList());


        if (liveOrUnattempted.size() < 3) {
            final int toFetch = MAX_LIVE_UNATTEMPTED_TESTS - liveOrUnattempted.size();

            Set<String> liveTestIds = liveOrUnattempted.stream().map(HomepageTestResponse::getTestId).collect(Collectors.toSet());

            List<ContentInfo> pastLiveOrUnattempted =
                    contentInfoDAO.getPastLiveOrUnattemptedTests(studentId, MAX_LIVE_UNATTEMPTED_TESTS, includeFieldsPastTests);

            pastLiveOrUnattempted = pastLiveOrUnattempted
                    .stream()
                    .filter(test -> !liveTestIds.contains(test.getMetadata().getTestId()))
                    .limit(toFetch)
                    .collect(Collectors.toList());

            Set<String> contentInfoIds = pastLiveOrUnattempted
                    .stream()
                    .map(AbstractMongoStringIdEntity::getId)
                    .collect(Collectors.toSet());

            Map<String, CMDSTestAttempt> pastTestsUnderAttempt = cmdsTestAttemptManager.getAttemptsForContentInfoIds(contentInfoIds);

            List<HomepageTestResponse> pastTestsResponse = HomepageTestResponse.generateFromContentInfos(pastLiveOrUnattempted);
            pastTestsResponse.forEach(r -> {
                r.setIsPastUnattempted(Boolean.TRUE);
                if (pastTestsUnderAttempt.containsKey(r.getId())) {
                    r.setIsPastLive(Boolean.TRUE);
                }
            });

            liveOrUnattempted.addAll(pastTestsResponse);
        }

        return liveOrUnattempted;
    }

    public List<HomepageTestResponse> getHomepageUpcomingTests(String studentId) {
        if (StringUtils.isEmpty(studentId)) {
            return new ArrayList<>();
        }

        final Long twoWeeksFromNow = System.currentTimeMillis() + 2 * CommonCalendarUtils.MILLIS_PER_WEEK;
        final List<String> includeFieldsUpcomingTests = Arrays.asList(METADATA_TESTID, CONTENT_TITLE, BOARD_IDS, SUBJECTS,
                METADATA_MIN_START_TIME, METADATA_MAX_START_TIME, METADATA_EXPIRY_DATE, METADATA_DURATION);

        List<ContentInfo> fetchedTests = contentInfoDAO.getUpcomingTestsForStudent(studentId, twoWeeksFromNow,
                MAX_UPCOMING_HOME_PAGE_TESTS, includeFieldsUpcomingTests);
        List<HomepageTestResponse> upcomingTests = HomepageTestResponse.generateFromContentInfos(fetchedTests);

        return Optional.of(upcomingTests)
                .orElseGet(ArrayList::new)
                .stream()
                .sorted(Comparator.comparingLong(HomepageTestResponse::getAttemptBy))
                .collect(Collectors.toList());
    }



}
