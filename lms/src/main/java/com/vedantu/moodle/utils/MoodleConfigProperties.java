package com.vedantu.moodle.utils;

import com.vedantu.util.logger.LoggingMarkers;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.vedantu.moodle.MoodleDataManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@Service
public class MoodleConfigProperties {

	LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MoodleDataManager.class);

	public static final int NODE_ID_VEDANTU = 2;
	public static final int NODE_ID_TEACHER = 3;
	public static final int NODE_ID_STUDENT = 4;
	public static final int NODE_ID_GROUP = 12;
	public static final int NODE_ID_VEDANTUCONTENT = 68;
	public static final int NODE_ID_COMMON_REPO_TEACHERS = 2278;
	public static String ACCESS_TOKEN = "cabfbd6ec5bd7103d99c01f63bec7ff8";
	public String BASE_URL = "http://vedantu.skoolbox.in/webservice/rest/server.php";
	public static final String METHOD_CREATE_USER = "core_user_create_users";
	public static final String METHOD_CREATE_CATEGORY = "core_course_create_categories";
	public static final String METHOD_SEARCH_CATEGORY = "core_course_get_categories";
	public static final String METHOD_CREATE_COURSE = "core_course_create_courses";
	public static final String METHOD_SEARCH_COURSE = "core_course_search_courses";
	public static final String METHOD_ENROLL_TO_COURSE = "enrol_manual_enrol_users";
	public static final String METHOD_COURSE_GET_CONTENTS = "core_course_get_contents";
	public static final String METHOD_USER_GET_COURSES = "core_enrol_get_users_courses";
	public static final String METHOD_COURSE_GET_COURSE = "core_course_get_courses";
	public static final String METHOD_GET_COURSE_MARKS_FOR_USER = "core_grades_get_grades";

	public static final String METHOD_GET_USER_INFO = "core_user_get_users";
	public static final String DEFAULT_PASSWORD = "Vedantu@123";
	public static final String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";
	public static final String AUTH_TYPE = "saml";
	public static final String FORMAT_TYPE = "flexsections";
	public static final String ROLE_VEDANTU_TEACHER = "3";
	public static final String ROLE_VEDANTU_STUDENT = "5";

	private Client client = Client.create();

	public String getMoodleUrl(String function) {
		BASE_URL = ConfigUtils.INSTANCE.getStringValue("MOODLE_SERVER_URL") + "/webservice/rest/server.php";
		ACCESS_TOKEN = ConfigUtils.INSTANCE.getStringValue("moodle.webuser.access.token");
		return BASE_URL + "?wstoken=" + ACCESS_TOKEN + "&wsfunction=" + function;
	}

	public String postMoodleData(String location, Object body, String contentType, String httpMethod) {

		WebResource webResource = client.resource(location);
		client.setReadTimeout(30000);

		ClientResponse response = null;
		switch (httpMethod) {
		case "POST":
			response = webResource.acceptLanguage("en-US").type(contentType).post(ClientResponse.class, body);
			break;
		case "PUT":
			response = webResource.acceptLanguage("en-US").type(contentType).put(ClientResponse.class, body);
			break;
		case "GET":
			response = webResource.get(ClientResponse.class);
			break;
		case "DELETE":
			response = webResource.type(contentType).delete(ClientResponse.class, body);
			break;
		default:
			break;
		}

		logger.info(LoggingMarkers.JSON_MASK, "location :" + location + " type: " + httpMethod + " body: " + body.toString());
		String responseString = response.getEntity(String.class);
		logger.info("response is : " + response);
		logger.info(responseString);
		return responseString;
	}
}
