package com.vedantu.moodle.utils;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.logging.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.vedantu.moodle.MoodleEventManager;
import com.vedantu.moodle.pojo.response.XMLResponse;
import com.vedantu.util.LogFactory;

public class MoodleUtils {

	final static ObjectMapper mapper = new ObjectMapper();
	private Client client = Client.create();
	public final static MoodleUtils INSTANCE = new MoodleUtils();

	LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MoodleEventManager.class);

	public static <T> T getPojoFromMap(Map<String, String> map, Class<T> className) {
		return mapper.convertValue(map, className);
	}

	public static ObjectMapper getObjectMapperInstance() {
		return mapper;
	}

	public static <T> List<T> getObjectResponse(String response, Class<T> className) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(XMLResponse.class);
			InputStream inputStream = new ByteArrayInputStream(response.getBytes());
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			XMLResponse xmlResponse = (XMLResponse) jaxbUnmarshaller.unmarshal(inputStream);
			if (xmlResponse != null) {
				return xmlResponse.getMapping(className);
			} else {
				return null;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public String postPlatformData(String location, String body, boolean isJsonReq, String httpMethod) {
		logger.info("postPlatformData", new Object[] { location, body, isJsonReq, httpMethod });
		WebResource webResource = client.resource(location);
		client.setReadTimeout(30000);

		ClientResponse response = null;
		switch (httpMethod) {
		case "POST":
			response = webResource.type("application/json").post(ClientResponse.class, body);
			break;
		case "GET":
			response = webResource.get(ClientResponse.class);
			break;
		default:
			break;
		}

		return response.getEntity(String.class);
	}
}
