package com.vedantu.moodle;

import java.util.*;
import java.util.stream.Collectors;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.DateTimeUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.dao.CMDSTestDAO;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.lms.cmds.enums.CMDSAttemptState;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.dao.UpdateContentInfoDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.pojo.response.BasicRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;

@Service
public class UpdateContentInfoManager {

    @Autowired
    private UpdateContentInfoDAO updateContentInfoDAO;

    @Autowired
    private CMDSTestDAO cmdsTestDAO;

    @Autowired
    private ContentInfoDAO contentInfoDAO;

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UpdateContentInfoManager.class);

    // need to be greater than CRON task schedule interval. change if the CRON task interval changed.
    private final static long CALCULATE_TEST_BUFFER_TIME = (long) (1.5 * DateTimeUtils.MILLIS_PER_HOUR);

    @Deprecated
    public void updateAssignmentResultsAfter(Long creationTime) {
        List<ContentInfo> contentInfos = updateContentInfoDAO.getAssignmentData(creationTime);

        for (ContentInfo contentInfo : contentInfos) {
            if (contentInfo.getMetadata() != null) {
                TestContentInfoMetadata testContentInfoMetadata = (TestContentInfoMetadata) contentInfo.getMetadata();
                if(testContentInfoMetadata != null && testContentInfoMetadata.getTestAttempts() != null) {
                	float percentage = (testContentInfoMetadata.getTestAttempts().get(0).getMarksAcheived() / testContentInfoMetadata.getTotalMarks()) * 100;
                	testContentInfoMetadata.setPercentage(percentage);
                	contentInfo.setMetadata(testContentInfoMetadata);
                	updateContentInfoDAO.updateAssignmentData(contentInfo);
                }
            }
        }
    }

    public void updateTestResultsAfterMinStartTime(Long currentTime) throws VException {
        if (Objects.isNull(currentTime)) {
            throw new VException(ErrorCode.SERVICE_ERROR, "CurrentTime param is null");
        }
        long fromTime = currentTime - CALCULATE_TEST_BUFFER_TIME;
        List<CMDSTest> tests = cmdsTestDAO.getTestsBetween(fromTime, currentTime);
        List<CMDSTest> validTests = getValidTests(tests, currentTime);
        for(CMDSTest test : validTests) {
            contentInfoManager.calculateTestRank(test);
        }
    }

    private List<CMDSTest> getValidTests(List<CMDSTest> tests, long currentTime) {
        List<CMDSTest> validTests = new ArrayList<>();

        for (CMDSTest test : tests) {
            Long maxStartTime = test.getMaxStartTime();
            Long duration = test.getDuration();

            if (Objects.nonNull(duration) && Objects.nonNull(maxStartTime)) {
                long testEndTime = duration + maxStartTime + 20 * DateTimeUtils.MILLIS_PER_MINUTE;
                if (testEndTime <= currentTime) {
                    validTests.add(test);
                }
            }
        }
        return validTests;
    }

    public List<ContentInfo> getAssignmentByStudent(Long studentId, Long fromTime, Long thruTime) {
        return updateContentInfoDAO.getAssignmentByStudent(studentId, fromTime, thruTime);
    }

    public List<ContentInfo> getTestByStudent(Long studentId, Long fromTime, Long thruTime) {
        return updateContentInfoDAO.getTestByStudent(studentId, fromTime, thruTime);
    }



}
