package com.vedantu.moodle.entity;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.listing.pojo.EngagementType;
import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Group extends AbstractMongoStringIdEntity {

	private String batchid;
	private String batchTitle;
	private EngagementType engagementType;
	private List<String> subjects;
	private List<UserBasicInfo> teachers;
	private List<UserBasicInfo> enrolledStudents;
	private Long launchDate;
	private Long startTime;
	private Long endTime;
	

	public String getBatchTitle() {
		return batchTitle;
	}

	public void setBatchTitle(String batchTitle) {
		this.batchTitle = batchTitle;
	}

	public String getBatchid() {
		return batchid;
	}

	public void setBatchid(String batchid) {
		this.batchid = batchid;
	}

	public List<UserBasicInfo> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<UserBasicInfo> teachers) {
		this.teachers = teachers;
	}

	public List<UserBasicInfo> getEnrolledStudents() {
		return enrolledStudents;
	}

	public void setEnrolledStudents(List<UserBasicInfo> enrolledStudents) {
		this.enrolledStudents = enrolledStudents;
	}

	public List<String> getSubjects() {
		return subjects;
	}

	public void setSubjects(List<String> subjects) {
		this.subjects = subjects;
	}

	public EngagementType getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(EngagementType engagementType) {
		this.engagementType = engagementType;
	}

	
	public Long getLaunchDate() {
		return launchDate;
	}

	public void setLaunchDate(Long launchDate) {
		this.launchDate = launchDate;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}


	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String BATCH_ID = "batchid";
		public static final String BATCH_TITLE = "batchTitle";
		public static final String TEACHERS = "teachers";
		public static final String ENROLLED_STUDENTS = "enrolledStudents";
		public static final String ENGAGEMENT_TYPE = "engagementType";
		public static final String SUBJECTS = "subjects";
		public static final String LAUNCH_DATE = "launchDate";
		public static final String START_TIME = "startTime";
		public static final String END_TIME = "endTime";

	}

}
