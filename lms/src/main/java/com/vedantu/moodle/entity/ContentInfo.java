package com.vedantu.moodle.entity;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.pojo.TeacherChangeTime;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.moodle.youscore.manager.YouScoreManager;
import com.vedantu.moodle.youscore.request.YouScoreContentSharedReq;
import com.vedantu.moodle.youscore.response.YouScoreTestDetailsRes;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "ContentInfo")
@CompoundIndexes({
        @CompoundIndex(name = "studentId_contentState_creationTime", def = "{'studentId':1, 'contentState' : 1, 'creationTime': -1}", background = true),
        @CompoundIndex(name = "studentId_1_expiryTime_-1", def = "{'studentId':1,'expiryTime': -1}", background = true),
        @CompoundIndex(name = "contentState_expiryTime", def = "{'contentState' : 1, 'expiryTime': 1}", background = true),
        @CompoundIndex(name = "contentState_evaulatedTime", def = "{'contentState' : 1, 'evaulatedTime': 1}", background = true),
        @CompoundIndex(name = "metadata.testId_1_lastUpdated_-1", def = "{'metadata.testId' : 1, 'lastUpdated' : -1}", background = true),
        @CompoundIndex(name = "contextId_creationTime", def = "{'contextId':1, 'creationTime': -1}", background = true)
})
@Data
public class ContentInfo extends AbstractMongoStringIdEntity {
    private UserType userType;

    @Indexed(background = true)
    private String studentId;
    private String studentFullName;
    @Indexed(background = true)
    private String teacherId;
    private String teacherFullName;

    @Indexed(background = true)
    private String contentTitle;

    private String courseName;
    private String courseId;
    private String subject;
    private Set<String> subjects = new HashSet<>();

    private Long expiryTime;

    private ContentType contentType = ContentType.TEST;
    private ContentSubType contentSubType;
    private ContentInfoType contentInfoType = ContentInfoType.SUBJECTIVE;
    private EngagementType engagementType = EngagementType.OTO;
    private ContentState contentState = ContentState.SHARED;
    private LMSType lmsType;

    private String contentLink;
    private String studentActionLink;
    private String teacherActionLink;

    private EntityType contextType;

    private String contextId;

    private List<String> tags;

    private TestContentInfoMetadata metadata;
    private AccessLevel accessLevel = AccessLevel.PRIVATE;

    private Long boardId;
    private Set<Long> boardIds = new HashSet<>();

    private List<TeacherChangeTime> teacherChangeTime;

    private String testId;
    private Long duration;
    private Long attemptedTime;
    private Long evaulatedTime;
    private Float totalMarks;
    private Float marksAcheived;
    private Integer noOfQuestions;
    private String attemptId; // Latest attempt id in case of multiple active attempts

    private Set<String> topicNames;

    public ContentInfo() {
        super();
    }

    public ContentInfo(UserBasicInfo student, UserBasicInfo teacher, String examCode) {
        super();
        this.studentId = String.valueOf(student.getUserId());
        this.studentFullName = student.getFullName();
        this.teacherId = String.valueOf(teacher.getUserId());
        this.teacherFullName = teacher.getFullName();
        this.contentType = ContentType.TEST;
        this.engagementType = EngagementType.OTO;
        this.contentState = ContentState.SHARED;
        this.lmsType = LMSType.YOUSCORE;
    }

    public ContentInfo(YouScoreContentSharedReq youScoreContentSharedReq, UserBasicInfo student,
                       UserBasicInfo teacher) {
        // Youscore
        super();
        this.testId = youScoreContentSharedReq.getTestId();
        this.contentTitle = youScoreContentSharedReq.getContentTitle();
        this.studentId = youScoreContentSharedReq.getStudentId();
        this.studentFullName = student.getFullName();
        this.teacherId = youScoreContentSharedReq.getTeacherId();
        this.teacherFullName = teacher.getFullName();

        this.subject = youScoreContentSharedReq.getSubject();
        this.subjects = new HashSet<>();
        this.subjects.add(youScoreContentSharedReq.getSubject());
        this.duration = youScoreContentSharedReq.getDuration() * 60;
        this.expiryTime = youScoreContentSharedReq.getExpiryTime();
        this.contentType = youScoreContentSharedReq.getContentType();
        this.contentInfoType = youScoreContentSharedReq.getContentInfoType();
        this.engagementType = youScoreContentSharedReq.getEngagementType();
        this.contentState = youScoreContentSharedReq.getContentState();
        this.totalMarks = youScoreContentSharedReq.getTotalMarks();
        this.lmsType = LMSType.YOUSCORE;
    }

    public ContentInfo(ContentInfo other) {
        this.userType = other.userType;
        this.studentId = other.studentId;
        this.studentFullName = other.studentFullName;
        this.teacherId = other.teacherId;
        this.teacherFullName = other.teacherFullName;
        this.contentTitle = other.contentTitle;
        this.courseName = other.courseName;
        this.courseId = other.courseId;
        this.subject = other.subject;
        this.subjects = other.subjects;
        this.expiryTime = other.expiryTime;
        this.contentType = other.contentType;
        this.contentInfoType = other.contentInfoType;
        this.engagementType = other.engagementType;
        this.contentState = other.contentState;
        this.lmsType = other.lmsType;
        this.contentLink = other.contentLink;
        this.studentActionLink = other.studentActionLink;
        this.teacherActionLink = other.teacherActionLink;
        this.contextType = other.contextType;
        this.contextId = other.contextId;
        this.tags = other.tags;
        this.metadata = other.metadata;
        this.accessLevel = other.accessLevel;
        this.boardId = other.boardId;
        this.boardIds = other.boardIds;
        this.teacherChangeTime = other.teacherChangeTime;
        this.testId = other.testId;
        this.duration = other.duration;
        this.attemptedTime = other.attemptedTime;
        this.evaulatedTime = other.evaulatedTime;
        this.totalMarks = other.totalMarks;
        this.marksAcheived = other.marksAcheived;
        this.noOfQuestions = other.noOfQuestions;
        this.attemptId = other.attemptId;
    }

    public ContentInfo createNewContentInfo(String studentId,long creationTime) {
        ContentInfo newContentInfo=new ContentInfo(this);
        newContentInfo.setStudentId(studentId);
        newContentInfo.setCreationTime(creationTime);
        return newContentInfo;
    }

    public void addTeacherChangeTime(TeacherChangeTime changeTime) {
        if (changeTime != null) {
            if (ArrayUtils.isEmpty(teacherChangeTime)) {
                teacherChangeTime = new ArrayList<>();
            }
            teacherChangeTime.add(changeTime);
        }
    }

    public void updateTestDetails(YouScoreTestDetailsRes res) {
        if (res == null) {
            this.contentTitle = YouScoreManager.YOUSCORE_DEFAULT_CONTENT_TITLE;
            this.totalMarks = 0f;
            this.duration = 0l;
            this.expiryTime = 0l;
        } else {
            if (!StringUtils.isEmpty(res.getContentTitle())) {
                this.contentTitle = res.getContentTitle();
            } else {
                this.contentTitle = YouScoreManager.YOUSCORE_DEFAULT_CONTENT_TITLE;
            }
            if (!StringUtils.isEmpty(res.getTotalMarks())) {
                try {
                    this.totalMarks = Float.parseFloat(res.getTotalMarks());
                } catch (Exception ex) {
                }
            }
            if (!StringUtils.isEmpty(res.getDuration())) {
                try {
                    this.duration = Long.parseLong(res.getDuration()) * 60;
                } catch (Exception ex) {
                }
            }
            this.expiryTime = 0l;
        }
    }

    public void setTags() {
        List<String> tags = new ArrayList<>();
        if (!StringUtils.isEmpty(this.teacherFullName)) {
            tags.add(this.teacherFullName.toUpperCase());
        }

        if (!StringUtils.isEmpty(this.contentTitle)) {
            tags.add(this.contentTitle.toUpperCase());
        }

        if (!StringUtils.isEmpty(this.courseName)) {
            tags.add(this.courseName.toUpperCase());

            if (this.courseName.indexOf("OTF") != -1) {
                tags.add("1-TO-FEW");
            }

            if (this.courseName.indexOf("OTO") != -1) {
                tags.add("1-TO-1");
            }
        }
        if (this.contentState != null) {
            tags.add(String.valueOf(this.contentState).toUpperCase());
        }

        if (this.contentType != null) {
            tags.add(String.valueOf(this.contentType).toUpperCase());
        }

        this.tags = tags;
    }

    @Override
    public String toString() {
        return "ContentInfo{"
                + "studentId='" + studentId + '\''
                + ", studentFullName='" + studentFullName + '\''
                + ", teacherId='" + teacherId + '\''
                + ", teacherFullName='" + teacherFullName + '\''
                + ", contentTitle='" + contentTitle + '\''
                + ", courseName='" + courseName + '\''
                + ", subject='" + subject + '\''
                + ", courseId='" + courseId + '\''
                + ", expiryTime=" + expiryTime
                + ", contentType=" + contentType
                + ", engagementType=" + engagementType
                + ", contentState=" + contentState
                + ", lmsType=" + lmsType
                + ", contentLink='" + contentLink + '\''
                + ", studentActionLink='" + studentActionLink + '\''
                + ", teacherActionLink='" + teacherActionLink + '\''
                + ", tags=" + tags
                + ", metadata=" + metadata
                + '}';
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String STUDENT_ID = "studentId";
        public static final String TEACHER_ID = "teacherId";
        public static final String SUBJECT = "subject";
        public static final String SUBJECTS = "subjects";
        public static final String COURSE_ID = "courseId";
        public static final String CONTENT_TYPE = "contentType";
        public static final String CONTENT_SUB_TYPE = "contentSubType";
        public static final String STUDENT_FULL_NAME = "studentFullName";
        public static final String TEACHER_FULL_NAME = "teacherFullName";
        public static final String CONTENT_TITLE = "contentTitle";
        public static final String CONTENT_LINK = "contentLink";
        public static final String COURSE_NAME = "courseName";
        public static final String EXPIRY_TIME = "expiryTime";
        public static final String CONTENT_INFO_TYPE = "contentInfoType";
        public static final String ENGAGEMENT_TYPE = "engagementType";
        public static final String LMS_TYPE = "lmsType";
        public static final String CONTENT_STATE = "contentState";
        public static final String TEST_ID = "testId";
        public static final String DURATION = "duration";
        public static final String EVAULATED_TIME = "evaulatedTime";
        public static final String ATTEMPTED_TIME = "attemptedTime";
        public static final String TAGS = "tags";
        public static final String CONTEXT_TYPE = "contextType";
        public static final String CONTEXT_ID = "contextId";
        public static final String METADATA = "metadata";
        public static final String BOARD_ID = "boardId";
        public static final String BOARD_IDS = "boardIds";
        public static final String RANK = "rank";
        public static final String PERCENTAGE = "percentage";
        public static final String PERCENTILE = "percentile";
        public static final String USER_TYPE = "userType";
        public static final String METADATA_TESTID = "metadata.testId";
        public static final String METADATA_TESTATTEMPTS = "metadata.testAttempts";
        public static final String METADATA_EXPIRY_DATE = "metadata.expiryDate";
        public static final String METADATA_TESTATTEMPTS_RESULT_ENTRIES = "metadata.testAttempts.resultEntries";
        public static final String METADATA_TESTATTEMPTS_RESULT_ENTRIES_QUESTION_ID = "metadata.testAttempts.resultEntries.questionId";
        public static final String METADATA_TESTATTEMPTS_ATTEMPTSTATE = "metadata.testAttempts.attemptState";
        public static final String METADATA_TESTATTEMPTS_ENDTIME = "metadata.testAttempts.endTime";
        public static final String METADATA_TESTATTEMPTS_STARTTIME = "metadata.testAttempts.startTime";
        public static final String METADATA_TESTATTEMPTS_STARTEDAT = "metadata.testAttempts.startedAt";
        public static final String METADATA_TESTATTEMPTS_ENDEDDAT = "metadata.testAttempts.endedAt";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_EVALUATED_TIME = "metadata.testAttempts.0.evaluatedTime";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_ZERO = "metadata.testAttempts.0";
        public static final String METADATA_TESTATTEMPTS_FIRSTATTEMPT_RESULT_ENTRIES = "metadata.testAttempts.0.resultEntries";
        public static final String METADATA_TESTATTEMPTS_TEST_ID = "metadata.testAttempts.testId";
        public static final String METADATA_TESTATTEMPTS__ID = "metadata.testAttempts._id";
        public static final String METADATA_TESTATTEMPTS_TOTAL_MARKS = "metadata.testAttempts.totalMarks";
        public static final String METADATA_TESTATTEMPTS_MARKS_ACHEIVED = "metadata.testAttempts.marksAcheived";
        public static final String METADATA_TESTATTEMPTS_ATTEMPT_STATE = "metadata.testAttempts.attemptState";
        public static final String METADATA_TESTATTEMPTS_CONTENT_INFO_ID = "metadata.testAttempts.contentInfoId";
        public static final String METADATA_MAX_START_TIME = "metadata.maxStartTime";
        public static final String METADATA_MIN_START_TIME = "metadata.minStartTime";
        public static final String METADATA_DURATION = "metadata.duration";
        public static final String CREATION_TIME = "creationTime";
        public static final String STUDENT_ACTION_LINK = "studentActionLink";
        public static final String TEACHER_ACTION_LINK = "teacherActionLink";
    }
}
