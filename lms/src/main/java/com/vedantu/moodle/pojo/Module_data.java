package com.vedantu.moodle.pojo;

public class Module_data
{
    private String showdescription;

    private String module;

    private String added;

    private String indent;

    private String visible;

    private String score;

    private String idnumber;

    private String completionexpected;

    private String completiongradeitemnumber;

    private String completion;

    private String visibleold;

    private String section;

    private String groupmode;

    private String id;

    private String course;

    private String completionview;

    private String groupingid;

    private String instance;

    private String availability;

    public String getShowdescription ()
    {
        return showdescription;
    }

    public void setShowdescription (String showdescription)
    {
        this.showdescription = showdescription;
    }

    public String getModule ()
    {
        return module;
    }

    public void setModule (String module)
    {
        this.module = module;
    }

    public String getAdded ()
    {
        return added;
    }

    public void setAdded (String added)
    {
        this.added = added;
    }

    public String getIndent ()
    {
        return indent;
    }

    public void setIndent (String indent)
    {
        this.indent = indent;
    }

    public String getVisible ()
    {
        return visible;
    }

    public void setVisible (String visible)
    {
        this.visible = visible;
    }

    public String getScore ()
    {
        return score;
    }

    public void setScore (String score)
    {
        this.score = score;
    }

    public String getIdnumber ()
    {
        return idnumber;
    }

    public void setIdnumber (String idnumber)
    {
        this.idnumber = idnumber;
    }

    public String getCompletionexpected ()
    {
        return completionexpected;
    }

    public void setCompletionexpected (String completionexpected)
    {
        this.completionexpected = completionexpected;
    }

    public String getCompletiongradeitemnumber ()
    {
        return completiongradeitemnumber;
    }

    public void setCompletiongradeitemnumber (String completiongradeitemnumber)
    {
        this.completiongradeitemnumber = completiongradeitemnumber;
    }

    public String getCompletion ()
    {
        return completion;
    }

    public void setCompletion (String completion)
    {
        this.completion = completion;
    }

    public String getVisibleold ()
    {
        return visibleold;
    }

    public void setVisibleold (String visibleold)
    {
        this.visibleold = visibleold;
    }

    public String getSection ()
    {
        return section;
    }

    public void setSection (String section)
    {
        this.section = section;
    }

    public String getGroupmode ()
    {
        return groupmode;
    }

    public void setGroupmode (String groupmode)
    {
        this.groupmode = groupmode;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCourse ()
    {
        return course;
    }

    public void setCourse (String course)
    {
        this.course = course;
    }

    public String getCompletionview ()
    {
        return completionview;
    }

    public void setCompletionview (String completionview)
    {
        this.completionview = completionview;
    }

    public String getGroupingid ()
    {
        return groupingid;
    }

    public void setGroupingid (String groupingid)
    {
        this.groupingid = groupingid;
    }

    public String getInstance ()
    {
        return instance;
    }

    public void setInstance (String instance)
    {
        this.instance = instance;
    }

    public String getAvailability ()
    {
        return availability;
    }

    public void setAvailability (String availability)
    {
        this.availability = availability;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [showdescription = "+showdescription+", module = "+module+", added = "+added+", indent = "+indent+", visible = "+visible+", score = "+score+", idnumber = "+idnumber+", completionexpected = "+completionexpected+", completiongradeitemnumber = "+completiongradeitemnumber+", completion = "+completion+", visibleold = "+visibleold+", section = "+section+", groupmode = "+groupmode+", id = "+id+", course = "+course+", completionview = "+completionview+", groupingid = "+groupingid+", instance = "+instance+", availability = "+availability+"]";
    }
}