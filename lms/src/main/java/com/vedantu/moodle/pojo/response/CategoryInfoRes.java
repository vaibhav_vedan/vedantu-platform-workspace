package com.vedantu.moodle.pojo.response;

public class CategoryInfoRes {
	private String id;
	private String name;
	private String idnumber;
	private String description;
	private String descriptionformat;
	private String parent;
	private String sortorder;
	private String coursecount;
	private String visible;
	private String visibleold;
	
	private String timemodified;
	private String depth;
	private String path;
	private String theme;
	
	
	public String getDescriptionformat() {
		return descriptionformat;
	}

	public void setDescriptionformat(String descriptionformat) {
		this.descriptionformat = descriptionformat;
	}
	public String getVisibleold() {
		return visibleold;
	}

	public void setVisibleold(String visibleold) {
		this.visibleold = visibleold;
	}



	public CategoryInfoRes() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getSortorder() {
		return sortorder;
	}

	public void setSortorder(String sortorder) {
		this.sortorder = sortorder;
	}

	public String getCoursecount() {
		return coursecount;
	}

	public void setCoursecount(String coursecount) {
		this.coursecount = coursecount;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getTimemodified() {
		return timemodified;
	}

	public void setTimemodified(String timemodified) {
		this.timemodified = timemodified;
	}

	public String getDepth() {
		return depth;
	}

	public void setDepth(String depth) {
		this.depth = depth;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTheme() {
		return theme;
	}

	public void setTheme(String theme) {
		this.theme = theme;
	}

	@Override
	public String toString() {
		return "SeachCategoryRes [id=" + id + ", name=" + name + ", idnumber=" + idnumber + ", description="
				+ description + ", parent=" + parent + ", sortorder=" + sortorder + ", coursecount=" + coursecount
				+ ", visible=" + visible + ", timemodified=" + timemodified + ", depth=" + depth + ", path=" + path
				+ ", theme=" + theme + "]";
	}

}
