package com.vedantu.moodle.pojo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentMetaData {

    private Double marksAchievedEasy = 0.0;
    private Double marksAchievedModerate = 0.0;
    private Double marksAchievedTough = 0.0;
    private Double marksAchievedUnknown = 0.0;
}
