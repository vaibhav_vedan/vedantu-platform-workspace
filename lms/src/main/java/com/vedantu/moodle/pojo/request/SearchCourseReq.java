package com.vedantu.moodle.pojo.request;

import java.net.URLEncoder;

import org.springframework.util.StringUtils;

public class SearchCourseReq {
	private String name = "search";
	private String value;

	public SearchCourseReq() {
		super();
	}

	public SearchCourseReq(String value) {
		super();
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "SearchCourseReq [name=" + name + ", value=" + value + "]";
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(value)) {
			try {
				body = body + "criterianame=" + name + "&criteriavalue=" + URLEncoder.encode(value, "UTF-8");
			} catch (Exception ex) {
				// throw nothing
			}
		}

		return body;
	}
}
