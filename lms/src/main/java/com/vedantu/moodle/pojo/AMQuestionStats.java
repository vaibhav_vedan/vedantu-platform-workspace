package com.vedantu.moodle.pojo;

public class AMQuestionStats {

	private Long noCorrect = 0l;
	private Long noWrong = 0l;
	private Long noUnattempted = 0l;

	public AMQuestionStats() {
		super();
	}

	public AMQuestionStats(Long noCorrect, Long noWrong, Long noUnattempted) {
		super();
		this.noCorrect = noCorrect;
		this.noWrong = noWrong;
		this.noUnattempted = noUnattempted;
	}

	public Long getNoCorrect() {
		return noCorrect;
	}

	public void setNoCorrect(Long noCorrect) {
		this.noCorrect = noCorrect;
	}

	public Long getNoWrong() {
		return noWrong;
	}

	public void setNoWrong(Long noWrong) {
		this.noWrong = noWrong;
	}

	public Long getNoUnattempted() {
		return noUnattempted;
	}

	public void setNoUnattempted(Long noUnattempted) {
		this.noUnattempted = noUnattempted;
	}

	public void incrementNoCorrect() {
		this.noCorrect++;
	}

	public void incrementNoWrong() {
		this.noWrong++;
	}

	public void incrementNoUnattempted() {
		this.noUnattempted++;
	}
	
	@Override
	public String toString() {
		return "AMQuestionStats[noCorrect: "+ noCorrect + ", " + "noWrong: " + noWrong + "noUnattempted: " + noUnattempted + "]";
	}
}
