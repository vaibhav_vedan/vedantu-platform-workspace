package com.vedantu.moodle.pojo;

import java.util.Map;

public class Question_data {

	Map<String,QuestionContext> questions;

	public Map<String, QuestionContext> getQuestions() {
		return questions;
	}

	public void setQuestions(Map<String, QuestionContext> questions) {
		this.questions = questions;
	}
	
	
}
