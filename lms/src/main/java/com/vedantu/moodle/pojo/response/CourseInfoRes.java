package com.vedantu.moodle.pojo.response;

public class CourseInfoRes {
	private String id;
	private String shortname;
	private String categoryid;
	private String categorysortorder;
	private String fullname;
	private String idnumber;
	private String summary;
	private String summaryformat;
	private String format;
	private String showgrades;
	private String newsitems;
	private String startdate;
	private String numsections;
	private String maxbytes;
	private String showreports;
	private String visible;
	private String hiddensections;
	private String groupmode;
	private String groupmodeforce;
	private String defaultgroupingid;
	private String timecreated;
	private String timemodified;
	private String enablecompletion;
	private String completionnotify;
	private String lang;
	private String forcetheme;
	private String enrolledusercount;

	public CourseInfoRes() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(String categoryid) {
		this.categoryid = categoryid;
	}

	public String getCategorysortorder() {
		return categorysortorder;
	}

	public void setCategorysortorder(String categorysortorder) {
		this.categorysortorder = categorysortorder;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getIdnumber() {
		return idnumber;
	}

	public void setIdnumber(String idnumber) {
		this.idnumber = idnumber;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getSummaryformat() {
		return summaryformat;
	}

	public void setSummaryformat(String summaryformat) {
		this.summaryformat = summaryformat;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getShowgrades() {
		return showgrades;
	}

	public void setShowgrades(String showgrades) {
		this.showgrades = showgrades;
	}

	public String getNewsitems() {
		return newsitems;
	}

	public void setNewsitems(String newsitems) {
		this.newsitems = newsitems;
	}

	public String getStartdate() {
		return startdate;
	}

	public void setStartdate(String startdate) {
		this.startdate = startdate;
	}

	public String getNumsections() {
		return numsections;
	}

	public void setNumsections(String numsections) {
		this.numsections = numsections;
	}

	public String getMaxbytes() {
		return maxbytes;
	}

	public void setMaxbytes(String maxbytes) {
		this.maxbytes = maxbytes;
	}

	public String getShowreports() {
		return showreports;
	}

	public void setShowreports(String showreports) {
		this.showreports = showreports;
	}

	public String getVisible() {
		return visible;
	}

	public void setVisible(String visible) {
		this.visible = visible;
	}

	public String getHiddensections() {
		return hiddensections;
	}

	public void setHiddensections(String hiddensections) {
		this.hiddensections = hiddensections;
	}

	public String getGroupmode() {
		return groupmode;
	}

	public void setGroupmode(String groupmode) {
		this.groupmode = groupmode;
	}

	public String getGroupmodeforce() {
		return groupmodeforce;
	}

	public void setGroupmodeforce(String groupmodeforce) {
		this.groupmodeforce = groupmodeforce;
	}

	public String getDefaultgroupingid() {
		return defaultgroupingid;
	}

	public void setDefaultgroupingid(String defaultgroupingid) {
		this.defaultgroupingid = defaultgroupingid;
	}

	public String getTimecreated() {
		return timecreated;
	}

	public void setTimecreated(String timecreated) {
		this.timecreated = timecreated;
	}

	public String getTimemodified() {
		return timemodified;
	}

	public void setTimemodified(String timemodified) {
		this.timemodified = timemodified;
	}

	public String getEnablecompletion() {
		return enablecompletion;
	}

	public void setEnablecompletion(String enablecompletion) {
		this.enablecompletion = enablecompletion;
	}

	public String getCompletionnotify() {
		return completionnotify;
	}

	public void setCompletionnotify(String completionnotify) {
		this.completionnotify = completionnotify;
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getForcetheme() {
		return forcetheme;
	}

	public void setForcetheme(String forcetheme) {
		this.forcetheme = forcetheme;
	}

	/**
	 * @return the enrolledusercount
	 */
	public String getEnrolledusercount() {
		return enrolledusercount;
	}

	/**
	 * @param enrolledusercount the enrolledusercount to set
	 */
	public void setEnrolledusercount(String enrolledusercount) {
		this.enrolledusercount = enrolledusercount;
	}

	@Override
	public String toString() {
		return "CourseInfoRes [id=" + id + ", shortname=" + shortname + ", categoryid=" + categoryid
				+ ", categorysortorder=" + categorysortorder + ", fullname=" + fullname + ", idnumber=" + idnumber
				+ ", summary=" + summary + ", summaryformat=" + summaryformat + ", format=" + format + ", showgrades="
				+ showgrades + ", newsitems=" + newsitems + ", startdate=" + startdate + ", numsections=" + numsections
				+ ", maxbytes=" + maxbytes + ", showreports=" + showreports + ", visible=" + visible
				+ ", hiddensections=" + hiddensections + ", groupmode=" + groupmode + ", groupmodeforce="
				+ groupmodeforce + ", defaultgroupingid=" + defaultgroupingid + ", timecreated=" + timecreated
				+ ", timemodified=" + timemodified + ", enablecompletion=" + enablecompletion + ", completionnotify="
				+ completionnotify + ", lang=" + lang + ", forcetheme=" + forcetheme + "]";
	}

	// <KEY name="courseformatoptions">
	// <MULTIPLE>
	// <SINGLE>
	// <KEY name="name">
	// <VALUE>numsections</VALUE>
	// </KEY>
	// <KEY name="value">
	// <VALUE>1</VALUE>
	// </KEY>
	// </SINGLE>
	// </MULTIPLE>
	// </KEY>
}
