package com.vedantu.moodle.pojo;

public class Submission_data
{
    private String id;

    private String assignment;

    private String status;

    private String attemptnumber;

    private String timemodified;

    private String userid;

    private String timecreated;

    private String groupid;

    private String latest;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAssignment ()
    {
        return assignment;
    }

    public void setAssignment (String assignment)
    {
        this.assignment = assignment;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public String getAttemptnumber ()
    {
        return attemptnumber;
    }

    public void setAttemptnumber (String attemptnumber)
    {
        this.attemptnumber = attemptnumber;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getGroupid ()
    {
        return groupid;
    }

    public void setGroupid (String groupid)
    {
        this.groupid = groupid;
    }

    public String getLatest ()
    {
        return latest;
    }

    public void setLatest (String latest)
    {
        this.latest = latest;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", assignment = "+assignment+", status = "+status+", attemptnumber = "+attemptnumber+", timemodified = "+timemodified+", userid = "+userid+", timecreated = "+timecreated+", groupid = "+groupid+", latest = "+latest+"]";
    }
}