package com.vedantu.moodle.pojo.response;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractListRes<T> {

	private List<T> list = new ArrayList<T>();

	private int count;

	public List<T> getList() {

		return list;
	}

	public void setList(List<T> list) {
		this.list = list;
		calculateCount();
	}

	public void addItem(T item) {
		this.list.add(item);
		calculateCount();
	}

	public int getCount() {
		return count;
	}

	public void calculateCount() {
		if (this.list != null) {
			this.count = this.list.size();
		} else {
			this.count = 0;
		}
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {

		StringBuilder builder = new StringBuilder();
		builder.append("{list=").append(list).append(", count=").append(count).append("}");
		return builder.toString();
	}
}
