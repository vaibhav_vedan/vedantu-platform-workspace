package com.vedantu.moodle.pojo;

import lombok.Builder;

@Builder
public class EvaluateNowLink {
	private String link;
	private String studentName;
	private String studentId;

	public EvaluateNowLink() {
		super();
	}

	public EvaluateNowLink(String link, String studentName, String studentId) {
		super();
		this.link = link;
		this.studentName = studentName;
		this.studentId = studentId;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

}
