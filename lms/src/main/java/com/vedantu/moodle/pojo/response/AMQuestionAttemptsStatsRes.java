package com.vedantu.moodle.pojo.response;

import com.vedantu.cmds.entities.BaseTopicTree;

import com.vedantu.moodle.pojo.AMQuestionStats;
import com.vedantu.moodle.pojo.AMQuestionStatsAvg;
import com.vedantu.moodle.pojo.BaseTopicTreeBasic;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMQuestionAttemptsStatsRes {
	private Map<String, AMQuestionStats> labelledStats;
	private Map<String, AMQuestionStatsAvg> labelledStatsClassAvg;
	private Map<String, Object> labelledInsights;
	private Map<String, AMQuestionStats> difficultyWiseStats;
	private Map<String, AMQuestionStatsAvg> difficultyWiseStatsClassAvg;
	private Map<String, Object> difficultyWiseInsights;
	private List<BaseTopicTreeBasic> childNodesBasic;
	private String currentNodeName;
	private String currentNodeId;
}
