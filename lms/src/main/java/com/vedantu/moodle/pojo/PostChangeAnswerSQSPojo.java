/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.moodle.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author parashar
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostChangeAnswerSQSPojo {
    private String questionId;
    private String testId;
    private boolean sendNotification = false;
    private boolean testIdForReevaluation=false;
}
