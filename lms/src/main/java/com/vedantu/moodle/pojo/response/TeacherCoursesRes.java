package com.vedantu.moodle.pojo.response;

import com.vedantu.listing.pojo.EngagementType;
import java.util.List;


public class TeacherCoursesRes {
	private CourseInfoRes courseInfoRes;
	private List<TeacherDashBoardInfo> contentMetaData;
	private String subject;
	private String studentName;
	private String studentId;
	private String teacherActionLink;
	private EngagementType engagementType;
	private Integer noteCount;
	
	
	public Integer getNoteCount() {
		return noteCount;
	}

	public void setNoteCount(Integer noteCount) {
		this.noteCount = noteCount;
	}

	public EngagementType getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(EngagementType engagementType) {
		this.engagementType = engagementType;
	}

	public TeacherCoursesRes() {
		super();
	}

	public CourseInfoRes getCourseInfoRes() {
		return courseInfoRes;
	}

	public void setCourseInfoRes(CourseInfoRes courseInfoRes) {
		this.courseInfoRes = courseInfoRes;
	}

	public List<TeacherDashBoardInfo> getContentMetaData() {
		return contentMetaData;
	}

	public void setContentMetaData(List<TeacherDashBoardInfo> contentMetaData) {
		this.contentMetaData = contentMetaData;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherActionLink() {
		return teacherActionLink;
	}

	public void setTeacherActionLink(String teacherActionLink) {
		this.teacherActionLink = teacherActionLink;
	}

	@Override
	public String toString() {
		return "TeacherCoursesRes [courseInfoRes=" + courseInfoRes + ", contentMetaData=" + contentMetaData
				+ ", subject=" + subject + ", studentName=" + studentName + ", studentId=" + studentId + "]";
	}
}
