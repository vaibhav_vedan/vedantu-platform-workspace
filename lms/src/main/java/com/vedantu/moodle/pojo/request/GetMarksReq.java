package com.vedantu.moodle.pojo.request;

import java.util.List;

public class GetMarksReq {

	private String courseId;
	private List<String> userids;

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public List<String> getUserids() {
		return userids;
	}

	public void setUserids(List<String> userids) {
		this.userids = userids;
	}

	public GetMarksReq(String courseId, List<String> userids) {
		super();
		this.courseId = courseId;
		this.userids = userids;
	}

	@Override
	public String toString() {
		return "GetMarksReq [courseId=" + courseId + ", userids=" + userids + "]";
	}

	public String getPostBody() {
		String body = "";

		try {
			body = body + "courseid=" + courseId ;
			for(String userId:userids)
				 body =body+"&userids[0]=" +userId ;
		} catch (Exception ex) {
			// throw nothing
		}

		return body;
	}

}
