package com.vedantu.moodle.pojo;

import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.moodle.entity.ContentInfo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTestAttemptPojoForTestReevaluation {
    private PostChangeAnswerSQSPojo postChangeAnswerSQSPojo;
    private CMDSTest test;
    private int questionIndex;
    private String section;
    private String questionId;
    private ContentInfo contentInfo;
}
