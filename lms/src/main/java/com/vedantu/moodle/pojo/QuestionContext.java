package com.vedantu.moodle.pojo;

public class QuestionContext
{
    

    private String qtype;

    private String id;

    private String category;

    private String name;


    
    public String getQtype ()
    {
        return qtype;
    }

    public void setQtype (String qtype)
    {
        this.qtype = qtype;
    }

    
    
    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }


    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }


    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

	@Override
	public String toString() {
		return "QuestionContext [qtype=" + qtype + ", id=" + id + ", category=" + category + ", name=" + name + "]";
	}

  
}