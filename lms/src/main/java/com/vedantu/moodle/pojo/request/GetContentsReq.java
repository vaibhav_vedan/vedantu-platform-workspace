package com.vedantu.moodle.pojo.request;

import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.LMSType;
import java.util.List;
public class GetContentsReq {
	private String studentId;
	private String teacherId;
	private String courseId;
	private String courseName;
	private List<ContentType> contentType;
	private String studentName;
	private String searchFieldValue;
	private String sortParam;
	private Integer start;
	private Integer end;
	private List<ContentState> contentState;
	private List<EngagementType> engagementType;
	private LMSType lmsType;

	public GetContentsReq() {
		super();
	}

	public GetContentsReq(String studentId, String teacherId, String courseId, String courseName,
			List<ContentType> contentType, List<ContentState> contentState, List<EngagementType> engagementType,
			String studentName, String searchFieldValue, String sortParam, LMSType lmsType, Integer start,
			Integer end) {
		super();
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.contentState = contentState;
		this.courseId = courseId;
		this.courseName = courseName;
		this.contentType = contentType;
		this.studentName = studentName;
		this.searchFieldValue = searchFieldValue;
		this.sortParam = sortParam;
		this.lmsType = lmsType;
		this.start = start;
		this.end = end;
		this.courseId = courseId;
		this.engagementType = engagementType;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public List<ContentType> getContentType() {
		return contentType;
	}

	public void setContentType(List<ContentType> contentType) {
		this.contentType = contentType;
	}

	public String getSearchFieldValue() {
		return searchFieldValue;
	}

	public void setSearchFieldValue(String searchFieldValue) {
		this.searchFieldValue = searchFieldValue;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	public String getSortParam() {
		return sortParam;
	}

	public void setSortParam(String sortParam) {
		this.sortParam = sortParam;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public List<ContentState> getContentState() {
		return contentState;
	}

	public void setContentState(List<ContentState> contentState) {
		this.contentState = contentState;
	}

	public List<EngagementType> getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(List<EngagementType> engagementType) {
		this.engagementType = engagementType;
	}

	public LMSType getLmsType() {
		return lmsType;
	}

	public void setLmsType(LMSType lmsType) {
		this.lmsType = lmsType;
	}

	@Override
	public String toString() {
		return "GetContentsReq [studentId=" + studentId + ", teacherId=" + teacherId + ", courseId=" + courseId
				+ ", courseName=" + courseName + ", contentType=" + contentType + ", studentName=" + studentName
				+ ", searchFieldValue=" + searchFieldValue + ", sortParam=" + sortParam + ", start=" + start + ", end="
				+ end + ", contentState=" + contentState + ", engagementType=" + engagementType + ", lmsType=" + lmsType
				+ ", toString()=" + super.toString() + "]";
	}
}
