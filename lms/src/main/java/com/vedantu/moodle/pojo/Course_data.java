package com.vedantu.moodle.pojo;

public class Course_data
{
    private String summary;

    private String visible;

    private String legacyfiles;

    private String calendartype;

    private String lang;

    private String id;

    private String enablecompletion;

    private String cacherev;

    private String shortname;

    private String defaultgroupingid;

    private String fullname;

    private String completionnotify;

    private String newsitems;

    private String theme;

    private String timemodified;

    private String idnumber;

    private String format;

    private String visibleold;

    private String groupmode;

    private String requested;

    private String category;

    private String startdate;

    private String summaryformat;

    private String sortorder;

    private String marker;

    private String showgrades;

    private String timecreated;

    private String groupmodeforce;

    private String maxbytes;

    private String showreports;

    public String getSummary ()
    {
        return summary;
    }

    public void setSummary (String summary)
    {
        this.summary = summary;
    }

    public String getVisible ()
    {
        return visible;
    }

    public void setVisible (String visible)
    {
        this.visible = visible;
    }

    public String getLegacyfiles ()
    {
        return legacyfiles;
    }

    public void setLegacyfiles (String legacyfiles)
    {
        this.legacyfiles = legacyfiles;
    }

    public String getCalendartype ()
    {
        return calendartype;
    }

    public void setCalendartype (String calendartype)
    {
        this.calendartype = calendartype;
    }

    public String getLang ()
    {
        return lang;
    }

    public void setLang (String lang)
    {
        this.lang = lang;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getEnablecompletion ()
    {
        return enablecompletion;
    }

    public void setEnablecompletion (String enablecompletion)
    {
        this.enablecompletion = enablecompletion;
    }

    public String getCacherev ()
    {
        return cacherev;
    }

    public void setCacherev (String cacherev)
    {
        this.cacherev = cacherev;
    }

    public String getShortname ()
    {
        return shortname;
    }

    public void setShortname (String shortname)
    {
        this.shortname = shortname;
    }

    public String getDefaultgroupingid ()
    {
        return defaultgroupingid;
    }

    public void setDefaultgroupingid (String defaultgroupingid)
    {
        this.defaultgroupingid = defaultgroupingid;
    }

    public String getFullname ()
    {
        return fullname;
    }

    public void setFullname (String fullname)
    {
        this.fullname = fullname;
    }

    public String getCompletionnotify ()
    {
        return completionnotify;
    }

    public void setCompletionnotify (String completionnotify)
    {
        this.completionnotify = completionnotify;
    }

    public String getNewsitems ()
    {
        return newsitems;
    }

    public void setNewsitems (String newsitems)
    {
        this.newsitems = newsitems;
    }

    public String getTheme ()
    {
        return theme;
    }

    public void setTheme (String theme)
    {
        this.theme = theme;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getIdnumber ()
    {
        return idnumber;
    }

    public void setIdnumber (String idnumber)
    {
        this.idnumber = idnumber;
    }

    public String getFormat ()
    {
        return format;
    }

    public void setFormat (String format)
    {
        this.format = format;
    }

    public String getVisibleold ()
    {
        return visibleold;
    }

    public void setVisibleold (String visibleold)
    {
        this.visibleold = visibleold;
    }

    public String getGroupmode ()
    {
        return groupmode;
    }

    public void setGroupmode (String groupmode)
    {
        this.groupmode = groupmode;
    }

    public String getRequested ()
    {
        return requested;
    }

    public void setRequested (String requested)
    {
        this.requested = requested;
    }

    public String getCategory ()
    {
        return category;
    }

    public void setCategory (String category)
    {
        this.category = category;
    }

    public String getStartdate ()
    {
        return startdate;
    }

    public void setStartdate (String startdate)
    {
        this.startdate = startdate;
    }

    public String getSummaryformat ()
    {
        return summaryformat;
    }

    public void setSummaryformat (String summaryformat)
    {
        this.summaryformat = summaryformat;
    }

    public String getSortorder ()
    {
        return sortorder;
    }

    public void setSortorder (String sortorder)
    {
        this.sortorder = sortorder;
    }

    public String getMarker ()
    {
        return marker;
    }

    public void setMarker (String marker)
    {
        this.marker = marker;
    }

    public String getShowgrades ()
    {
        return showgrades;
    }

    public void setShowgrades (String showgrades)
    {
        this.showgrades = showgrades;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getGroupmodeforce ()
    {
        return groupmodeforce;
    }

    public void setGroupmodeforce (String groupmodeforce)
    {
        this.groupmodeforce = groupmodeforce;
    }

    public String getMaxbytes ()
    {
        return maxbytes;
    }

    public void setMaxbytes (String maxbytes)
    {
        this.maxbytes = maxbytes;
    }

    public String getShowreports ()
    {
        return showreports;
    }

    public void setShowreports (String showreports)
    {
        this.showreports = showreports;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [summary = "+summary+", visible = "+visible+", legacyfiles = "+legacyfiles+", calendartype = "+calendartype+", lang = "+lang+", id = "+id+", enablecompletion = "+enablecompletion+", cacherev = "+cacherev+", shortname = "+shortname+", defaultgroupingid = "+defaultgroupingid+", fullname = "+fullname+", completionnotify = "+completionnotify+", newsitems = "+newsitems+", theme = "+theme+", timemodified = "+timemodified+", idnumber = "+idnumber+", format = "+format+", visibleold = "+visibleold+", groupmode = "+groupmode+", requested = "+requested+", category = "+category+", startdate = "+startdate+", summaryformat = "+summaryformat+", sortorder = "+sortorder+", marker = "+marker+", showgrades = "+showgrades+", timecreated = "+timecreated+", groupmodeforce = "+groupmodeforce+", maxbytes = "+maxbytes+", showreports = "+showreports+"]";
    }
}