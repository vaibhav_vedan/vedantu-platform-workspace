package com.vedantu.moodle.pojo;

public class Grades {

	private String feedback;

    private String feedbackformat;

    private String datesubmitted;

    private String overridden;

    private String hidden;

    private String dategraded;

    private String userid;

    private String usermodified;

    private String grade;

    private String locked;

    private String str_feedback;

    private String str_long_grade;

    private String str_grade;

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getFeedbackformat() {
		return feedbackformat;
	}

	public void setFeedbackformat(String feedbackformat) {
		this.feedbackformat = feedbackformat;
	}

	public String getDatesubmitted() {
		return datesubmitted;
	}

	public void setDatesubmitted(String datesubmitted) {
		this.datesubmitted = datesubmitted;
	}

	public String getOverridden() {
		return overridden;
	}

	public void setOverridden(String overridden) {
		this.overridden = overridden;
	}

	public String getHidden() {
		return hidden;
	}

	public void setHidden(String hidden) {
		this.hidden = hidden;
	}

	public String getDategraded() {
		return dategraded;
	}

	public void setDategraded(String dategraded) {
		this.dategraded = dategraded;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getUsermodified() {
		return usermodified;
	}

	public void setUsermodified(String usermodified) {
		this.usermodified = usermodified;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public String getLocked() {
		return locked;
	}

	public void setLocked(String locked) {
		this.locked = locked;
	}

	public String getStr_feedback() {
		return str_feedback;
	}

	public void setStr_feedback(String str_feedback) {
		this.str_feedback = str_feedback;
	}

	public String getStr_long_grade() {
		return str_long_grade;
	}

	public void setStr_long_grade(String str_long_grade) {
		this.str_long_grade = str_long_grade;
	}

	public String getStr_grade() {
		return str_grade;
	}

	public void setStr_grade(String str_grade) {
		this.str_grade = str_grade;
	}
    
    

}
