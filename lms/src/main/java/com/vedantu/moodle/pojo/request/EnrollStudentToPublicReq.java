package com.vedantu.moodle.pojo.request;

import com.vedantu.moodle.pojo.MoodleUser;

public class EnrollStudentToPublicReq {
	private String contentId;
	private MoodleUser studentInfo;
	public EnrollStudentToPublicReq() {
		super();
		
	}
	
	public EnrollStudentToPublicReq(String contentId, MoodleUser studentInfo) {
		super();
		this.contentId = contentId;
		this.studentInfo = studentInfo;
	}
	public String getContentId() {
		return contentId;
	}
	public void setContentId(String contentId) {
		this.contentId = contentId;
	}
	public MoodleUser getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(MoodleUser studentInfo) {
		this.studentInfo = studentInfo;
	}
}
