package com.vedantu.moodle.pojo;

import com.vedantu.cmds.entities.CMDSQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTestAttemptSQSPojoForChangeAnswer {
    private String questionId;
    private String contentInfoId;
}
