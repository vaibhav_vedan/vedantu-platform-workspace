package com.vedantu.moodle.pojo.request;

import java.net.URLEncoder;

import org.springframework.util.StringUtils;

public class GetCourseContentsReq {
	private String courseid;

	public GetCourseContentsReq() {
		super();
	}

	public GetCourseContentsReq(String courseid) {
		super();
		this.courseid = courseid;
	}

	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}

	@Override
	public String toString() {
		return "GetCourseContentsReq [courseid=" + courseid + "]";
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(courseid)) {
			try {
				body = body + "courseid=" + URLEncoder.encode(courseid, "UTF-8");
			} catch (Exception ex) {
				// throw nothing
			}
		}

		return body;
	}
}
