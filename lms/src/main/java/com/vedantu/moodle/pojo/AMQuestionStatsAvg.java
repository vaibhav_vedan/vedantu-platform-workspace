package com.vedantu.moodle.pojo;

public class AMQuestionStatsAvg {
	
	private Double noCorrect = 0.0;

	private Double noWrong = 0.0;
	private Double noUnattempted = 0.0;

	public AMQuestionStatsAvg(Double noCorrect, Double noWrong, Double noUnattempted) {
		this.noCorrect = noCorrect;
		this.noWrong = noWrong;
		this.noUnattempted = noUnattempted;
	}

	
	public AMQuestionStatsAvg() {
		super();
	}
	
	public Double getNoCorrect() {
		return noCorrect;
	}
	public void setNoCorrect(Double noCorrect) {
		this.noCorrect = noCorrect;
	}
	public Double getNoWrong() {
		return noWrong;
	}
	public void setNoWrong(Double noWrong) {
		this.noWrong = noWrong;
	}
	public Double getNoUnattempted() {
		return noUnattempted;
	}
	public void setNoUnattempted(Double noUnattempted) {
		this.noUnattempted = noUnattempted;
	}
	
}
