package com.vedantu.moodle.pojo;

import java.util.Set;

public class CourseBasicInfo {
		@Override
	public String toString() {
		return "CourseBasicInfo [id=" + id + ", title=" + title + ", targets=" + targets + ", subjects=" + subjects
				+ "]";
	}
		private String id;
		private String orgId;
		private String title;
		private Set<String> targets;
		private Set<String> subjects;
		private Long launchDate;
		private Long batchStartTime;
		private Long batchEndTime;
		public Long getLaunchDate() {
			return launchDate;
		}
		public void setLaunchDate(Long launchDate) {
			this.launchDate = launchDate;
		}
		public Long getBatchStartTime() {
			return batchStartTime;
		}
		public void setBatchStartTime(Long batchStartTime) {
			this.batchStartTime = batchStartTime;
		}
		public Long getBatchEndTime() {
			return batchEndTime;
		}
		public void setBatchEndTime(Long batchEndTime) {
			this.batchEndTime = batchEndTime;
		}
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public Set<String> getTargets() {
			return targets;
		}
		public void setTargets(Set<String> targets) {
			this.targets = targets;
		}
		public Set<String> getSubjects() {
			return subjects;
		}
		public void setSubjects(Set<String> subjects) {
			this.subjects = subjects;
		}
		public String getOrgId() {
		return orgId;
	}
		public void setOrgId(String orgId) {
		this.orgId = orgId;
	}


}
