/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.moodle.pojo;

import com.vedantu.lms.cmds.enums.ChangeQuestionActionType;
import com.vedantu.lms.cmds.pojo.QuestionChangeRecord;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author parashar
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PostChangeQuestionSQSPojo {
    private String testId;
    private QuestionChangeRecord questionChangeRecord;
    private ChangeQuestionActionType changeQuestionActionType;
    private boolean sendNotification = false;

    public PostChangeQuestionSQSPojo(String testId, QuestionChangeRecord questionChangeRecord) {
        this.testId=testId;
        this.questionChangeRecord=questionChangeRecord;
    }
}
