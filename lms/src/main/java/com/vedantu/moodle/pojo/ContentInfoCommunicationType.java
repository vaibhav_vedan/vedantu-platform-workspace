package com.vedantu.moodle.pojo;

public enum ContentInfoCommunicationType {
	SHARED, EVALUATED, ATTEMPTED, PENDING_EVALUATION
}
