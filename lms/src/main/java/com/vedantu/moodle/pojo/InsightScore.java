package com.vedantu.moodle.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsightScore {

    private Double insight_marks;
    private String insight_grade;
}