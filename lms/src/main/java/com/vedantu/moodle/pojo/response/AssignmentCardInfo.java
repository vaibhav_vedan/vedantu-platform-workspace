package com.vedantu.moodle.pojo.response;

import com.vedantu.lms.cmds.enums.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AssignmentCardInfo {

    private String contentTitle;
    private String contextId;
    private String testId;
    private Set<Long> contentInfoBoardIds;
    private Set<String> boardIds;
    private ContentType contentType ;
    private ContentSubType contentSubType ;
    private String studentActionLink;
    private Long sharedTime;
    private AccessLevel accessLevel;

}
