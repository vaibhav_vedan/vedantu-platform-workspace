package com.vedantu.moodle.pojo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SubscriptionRequest {
		private String Type;
		private String MessageId;
		private String Token;
		private String TopicArn;
		private String Message;
		private String SubscribeURL;
		private String Timestamp;
		private String SignatureVersion;
		private String Signature;
		private String SigningCertURL;
		private String Subject;

		@JsonProperty("Subject")
		public String getSubject() {
			return Subject;
		}

		@JsonProperty("Subject")
		public void setSubject(String subject) {
			Subject = subject;
		}

		@JsonProperty("Type")
		public String getType() {
			return Type;
		}

		@JsonProperty("Type")
		public void setType(String type) {
			Type = type;
		}

		@JsonProperty("MessageId")
		public String getMessageId() {
			return MessageId;
		}

		@JsonProperty("MessageId")
		public void setMessageId(String messageId) {
			MessageId = messageId;
		}

		@JsonProperty("Token")
		public String getToken() {
			return Token;
		}

		@JsonProperty("Token")
		public void setToken(String token) {
			Token = token;
		}

		@JsonProperty("TopicArn")
		public String getTopicArn() {
			return TopicArn;
		}

		@JsonProperty("TopicArn")
		public void setTopicArn(String topicArn) {
			TopicArn = topicArn;
		}

		@JsonProperty("Message")
		public String getMessage() {
			return Message;
		}

		@JsonProperty("Message")
		public void setMessage(String message) {
			Message = message;
		}

		@JsonProperty("SubscribeURL")
		public String getSubscribeURL() {
			return SubscribeURL;
		}

		@JsonProperty("SubscribeURL")
		public void setSubscribeURL(String subscribeURL) {
			SubscribeURL = subscribeURL;
		}

		@JsonProperty("Timestamp")
		public String getTimestamp() {
			return Timestamp;
		}

		@JsonProperty("Timestamp")
		public void setTimestamp(String timestamp) {
			Timestamp = timestamp;
		}

		@JsonProperty("SignatureVersion")
		public String getSignatureVersion() {
			return SignatureVersion;
		}

		@JsonProperty("SignatureVersion")
		public void setSignatureVersion(String signatureVersion) {
			SignatureVersion = signatureVersion;
		}

		@JsonProperty("Signature")
		public String getSignature() {
			return Signature;
		}

		@JsonProperty("Signature")
		public void setSignature(String signature) {
			Signature = signature;
		}

		@JsonProperty("SigningCertURL")
		public String getSigningCertURL() {
			return SigningCertURL;
		}

		@JsonProperty("SigningCertURL")
		public void setSigningCertURL(String signingCertURL) {
			SigningCertURL = signingCertURL;
		}

		@Override
		public String toString() {
			return "SubscriptionRequest [Type=" + Type + ", MessageId=" + MessageId + ", Token=" + Token + ", TopicArn="
					+ TopicArn + ", Message=" + Message + ", SubscribeURL=" + SubscribeURL + ", Timestamp=" + Timestamp
					+ ", SignatureVersion=" + SignatureVersion + ", Signature=" + Signature + ", SigningCertURL="
					+ SigningCertURL + ", Subject=" + Subject + "]";
		}

		
	
}
