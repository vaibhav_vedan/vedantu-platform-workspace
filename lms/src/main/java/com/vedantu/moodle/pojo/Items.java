package com.vedantu.moodle.pojo;

public class Items {

	private String gradepass;

    private String grademax;

    private String grademin;

    private String itemnumber;

    private String hidden;

    private String name;

    private String activityid;

    private String scaleid;

    private String locked;

    private Grades[] grades;

    public String getGradepass ()
    {
        return gradepass;
    }

    public void setGradepass (String gradepass)
    {
        this.gradepass = gradepass;
    }

    public String getGrademax ()
    {
        return grademax;
    }

    public void setGrademax (String grademax)
    {
        this.grademax = grademax;
    }

    public String getGrademin ()
    {
        return grademin;
    }

    public void setGrademin (String grademin)
    {
        this.grademin = grademin;
    }

    public String getItemnumber ()
    {
        return itemnumber;
    }

    public void setItemnumber (String itemnumber)
    {
        this.itemnumber = itemnumber;
    }

    public String getHidden ()
    {
        return hidden;
    }

    public void setHidden (String hidden)
    {
        this.hidden = hidden;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getActivityid ()
    {
        return activityid;
    }

    public void setActivityid (String activityid)
    {
        this.activityid = activityid;
    }

    public String getScaleid ()
    {
        return scaleid;
    }

    public void setScaleid (String scaleid)
    {
        this.scaleid = scaleid;
    }

    public String getLocked ()
    {
        return locked;
    }

    public void setLocked (String locked)
    {
        this.locked = locked;
    }

    public Grades[] getGrades ()
    {
        return grades;
    }

    public void setGrades (Grades[] grades)
    {
        this.grades = grades;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [gradepass = "+gradepass+", grademax = "+grademax+", grademin = "+grademin+", itemnumber = "+itemnumber+", hidden = "+hidden+", name = "+name+", activityid = "+activityid+", scaleid = "+scaleid+", locked = "+locked+", grades = "+grades+"]";
    }
}
