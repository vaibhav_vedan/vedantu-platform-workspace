package com.vedantu.moodle.pojo.response;

public class GetTeacherCoursesListRes extends AbstractListRes<TeacherCoursesRes> {
	private Integer totalCount;

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
}
