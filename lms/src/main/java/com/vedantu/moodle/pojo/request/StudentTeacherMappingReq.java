package com.vedantu.moodle.pojo.request;

import com.vedantu.moodle.pojo.MoodleUser;

public class StudentTeacherMappingReq {
	private MoodleUser studentInfo;
	private MoodleUser teacherInfo;
	private String subject;
	private String engagementType;

	public StudentTeacherMappingReq() {
		super();
	}

	public MoodleUser getStudentInfo() {
		return studentInfo;
	}

	public void setStudentInfo(MoodleUser studentInfo) {
		this.studentInfo = studentInfo;
	}

	public MoodleUser getTeacherInfo() {
		return teacherInfo;
	}

	public void setTeacherInfo(MoodleUser teacherInfo) {
		this.teacherInfo = teacherInfo;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(String engagementType) {
		this.engagementType = engagementType;
	}

	@Override
	public String toString() {
		return "StudentTeacherMappingReq [studentInfo=" + studentInfo + ", teacherInfo=" + teacherInfo + ", subject="
				+ subject + ", engagementType=" + engagementType + "]";
	}
}
