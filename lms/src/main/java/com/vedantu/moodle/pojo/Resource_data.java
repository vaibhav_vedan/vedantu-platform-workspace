package com.vedantu.moodle.pojo;

public class Resource_data
{
    private String course;

    private String id;

    private String revision;

    private String displayoptions;

    private String legacyfileslast;

    private String tobemigrated;

    private String legacyfiles;

    private String name;

    private String timemodified;

    private String filterfiles;

    private String display;

    private String intro;

    private String introformat;

    public String getCourse ()
    {
        return course;
    }

    public void setCourse (String course)
    {
        this.course = course;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getRevision ()
    {
        return revision;
    }

    public void setRevision (String revision)
    {
        this.revision = revision;
    }

    public String getDisplayoptions ()
    {
        return displayoptions;
    }

    public void setDisplayoptions (String displayoptions)
    {
        this.displayoptions = displayoptions;
    }

    public String getLegacyfileslast ()
    {
        return legacyfileslast;
    }

    public void setLegacyfileslast (String legacyfileslast)
    {
        this.legacyfileslast = legacyfileslast;
    }

    public String getTobemigrated ()
    {
        return tobemigrated;
    }

    public void setTobemigrated (String tobemigrated)
    {
        this.tobemigrated = tobemigrated;
    }

    public String getLegacyfiles ()
    {
        return legacyfiles;
    }

    public void setLegacyfiles (String legacyfiles)
    {
        this.legacyfiles = legacyfiles;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getFilterfiles ()
    {
        return filterfiles;
    }

    public void setFilterfiles (String filterfiles)
    {
        this.filterfiles = filterfiles;
    }

    public String getDisplay ()
    {
        return display;
    }

    public void setDisplay (String display)
    {
        this.display = display;
    }

    public String getIntro ()
    {
        return intro;
    }

    public void setIntro (String intro)
    {
        this.intro = intro;
    }

    public String getIntroformat ()
    {
        return introformat;
    }

    public void setIntroformat (String introformat)
    {
        this.introformat = introformat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [course = "+course+", id = "+id+", revision = "+revision+", displayoptions = "+displayoptions+", legacyfileslast = "+legacyfileslast+", tobemigrated = "+tobemigrated+", legacyfiles = "+legacyfiles+", name = "+name+", timemodified = "+timemodified+", filterfiles = "+filterfiles+", display = "+display+", intro = "+intro+", introformat = "+introformat+"]";
    }
}