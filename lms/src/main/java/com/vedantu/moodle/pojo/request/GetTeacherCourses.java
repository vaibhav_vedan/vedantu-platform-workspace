package com.vedantu.moodle.pojo.request;

import java.util.List;

public class GetTeacherCourses {
	
	private String teacherId;
	private Integer start;
	private Integer end;
	private List<String> userIds;
	public GetTeacherCourses() {
		super();
	}


	
	
	public GetTeacherCourses(String teacherId, Integer start, Integer end, List<String> userIds) {
		super();
		this.teacherId = teacherId;
		this.start = start;
		this.end = end;
		this.userIds = userIds;
	}




	public List<String> getUserIds() {
		return userIds;
	}

	public void setUserIds(List<String> userIds) {
		this.userIds = userIds;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public Integer getStart() {
		return start;
	}

	public void setStart(Integer start) {
		this.start = start;
	}

	public Integer getEnd() {
		return end;
	}

	public void setEnd(Integer end) {
		this.end = end;
	}

	@Override
	public String toString() {
		return "GetTeacherCourses [teacherId=" + teacherId + ", start=" + start + ", end=" + end + "]";
	}
}
