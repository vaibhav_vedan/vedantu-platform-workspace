/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.moodle.pojo;

import com.vedantu.lms.cmds.pojo.AnalyticsTypes;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author parashar
 */
@Data
@NoArgsConstructor
public class SectionResultInfo {
    private Float marksAchieved;
    private Float totalMarks;
    private String name;
    private String subject;
    private AnalyticsTypes categoryType;

}
