package com.vedantu.moodle.pojo.response;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionDateRes {

	@NotNull
    @NotBlank(message = "batchId cannot be empty")
    private String batchId;
	
	@NotNull
    @NotBlank(message = "boardId cannot be empty")
	@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    private List<Long> boardId;
	
	private String testId;
	
    private Long completeBy;
}
