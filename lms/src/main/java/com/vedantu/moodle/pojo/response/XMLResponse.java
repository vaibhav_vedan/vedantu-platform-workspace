package com.vedantu.moodle.pojo.response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.vedantu.moodle.utils.MoodleUtils;

@XmlRootElement(name = "RESPONSE")
public class XMLResponse {
	private List<MULTIPLE> MULTIPLE;

	private XMLResponse() {
	}

	@XmlElement
	public List<MULTIPLE> getMULTIPLE() {
		return MULTIPLE;
	}

	public void setMULTIPLE(List<MULTIPLE> mULTIPLE) {
		MULTIPLE = mULTIPLE;
	}

	public <T> List<T> getMapping(Class<T> className) {
		List<T> returnObjects = new ArrayList<T>();
		List<MULTIPLE> multiples = this.getMULTIPLE();
		if (multiples != null) {
			for (MULTIPLE multiple : multiples) {
				List<SINGLE> singles = multiple.getSINGLE();
				if (singles != null) {
					for (SINGLE single : singles) {
						Map<String, String> map = new HashMap<String, String>();
						List<KEY> keys = single.getKEY();
						if (keys != null) {
							for (KEY key : keys) {
								map.put(key.getName(), key.getVALUE());
							}
						}

						try {
							returnObjects.add(MoodleUtils.getPojoFromMap(map, className));
						} catch (Exception ex) {
							// log exception
							ex.printStackTrace();
							continue;
						}
					}
				}
			}
		}

		return returnObjects;
	}
}
