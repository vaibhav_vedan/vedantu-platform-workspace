package com.vedantu.moodle.pojo;

public class NewSubjectUser {

	private MoodleUser user;
	private String subject;
	public MoodleUser getUser() {
		return user;
	}
	public void setUser(MoodleUser user) {
		this.user = user;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
}
