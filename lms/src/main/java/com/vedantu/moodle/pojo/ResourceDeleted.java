package com.vedantu.moodle.pojo;

public class ResourceDeleted {

	private String authtoken;

	private Course_data course_data;

	private String module_data;

	private Teacher_data teacher_data;

	private Event_data event_data;

	private String resource_data;

	public String getAuthtoken() {
		return authtoken;
	}

	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}

	public Course_data getCourse_data() {
		return course_data;
	}

	public void setCourse_data(Course_data course_data) {
		this.course_data = course_data;
	}

	public String getModule_data() {
		return module_data;
	}

	public void setModule_data(String module_data) {
		this.module_data = module_data;
	}

	public Teacher_data getTeacher_data() {
		return teacher_data;
	}

	public void setTeacher_data(Teacher_data teacher_data) {
		this.teacher_data = teacher_data;
	}

	public Event_data getEvent_data() {
		return event_data;
	}

	public void setEvent_data(Event_data event_data) {
		this.event_data = event_data;
	}

	public String getResource_data() {
		return resource_data;
	}

	public void setResource_data(String resource_data) {
		this.resource_data = resource_data;
	}

	@Override
	public String toString() {
		return "ClassPojo [authtoken = " + authtoken + ", course_data = " + course_data + ", module_data = "
				+ module_data + ", teacher_data = " + teacher_data + ", event_data = " + event_data
				+ ", resource_data = " + resource_data + "]";
	}
}
