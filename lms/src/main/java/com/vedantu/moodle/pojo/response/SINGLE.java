package com.vedantu.moodle.pojo.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class SINGLE {
	private List<KEY> KEY;

	private SINGLE() {
	}

	@XmlElement
	public List<KEY> getKEY() {
		return KEY;
	}

	public void setKEY(List<KEY> kEY) {
		KEY = kEY;
	}

}
