package com.vedantu.moodle.pojo;

import com.vedantu.lms.cmds.pojo.QuestionChangeRecord;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateTestAttemptForChangeQuestionSQSPojo {
    private QuestionChangeRecord questionChangeRecord;
    private String contentInfoId;
}
