package com.vedantu.moodle.pojo;

public class Quiz_data
{
    private String graceperiod;

    private String overduehandling;

    private String sumgrades;

    private String reviewspecificfeedback;

    private String attemptonlast;

    private String grademethod;

    private String password;

    private String questionsperpage;

    private String questiondecimalpoints;

    private String introformat;

    private String course;

    private String id;

    private String subnet;

    private String navmethod;

    private String browsersecurity;

    private String name;

    private String attempts;

    private String reviewoverallfeedback;

    private String completionattemptsexhausted;

    private String grade;

    private String canredoquestions;

    private String decimalpoints;

    private String showuserpicture;

    private String reviewcorrectness;

    private String showblocks;

    private String timeopen;

    private String reviewrightanswer;

    private String timemodified;

    private String reviewmarks;

    private String completionpass;

    private String intro;

    private String reviewattempt;

    private String timeclose;

    private String preferredbehaviour;

    private String timelimit;

    private String timecreated;

    private String shuffleanswers;

    private String delay2;

    private String reviewgeneralfeedback;

    private String delay1;

    public String getGraceperiod ()
    {
        return graceperiod;
    }

    public void setGraceperiod (String graceperiod)
    {
        this.graceperiod = graceperiod;
    }

    public String getOverduehandling ()
    {
        return overduehandling;
    }

    public void setOverduehandling (String overduehandling)
    {
        this.overduehandling = overduehandling;
    }

    public String getSumgrades ()
    {
        return sumgrades;
    }

    public void setSumgrades (String sumgrades)
    {
        this.sumgrades = sumgrades;
    }

    public String getReviewspecificfeedback ()
    {
        return reviewspecificfeedback;
    }

    public void setReviewspecificfeedback (String reviewspecificfeedback)
    {
        this.reviewspecificfeedback = reviewspecificfeedback;
    }

    public String getAttemptonlast ()
    {
        return attemptonlast;
    }

    public void setAttemptonlast (String attemptonlast)
    {
        this.attemptonlast = attemptonlast;
    }

    public String getGrademethod ()
    {
        return grademethod;
    }

    public void setGrademethod (String grademethod)
    {
        this.grademethod = grademethod;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getQuestionsperpage ()
    {
        return questionsperpage;
    }

    public void setQuestionsperpage (String questionsperpage)
    {
        this.questionsperpage = questionsperpage;
    }

    public String getQuestiondecimalpoints ()
    {
        return questiondecimalpoints;
    }

    public void setQuestiondecimalpoints (String questiondecimalpoints)
    {
        this.questiondecimalpoints = questiondecimalpoints;
    }

    public String getIntroformat ()
    {
        return introformat;
    }

    public void setIntroformat (String introformat)
    {
        this.introformat = introformat;
    }

    public String getCourse ()
    {
        return course;
    }

    public void setCourse (String course)
    {
        this.course = course;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSubnet ()
    {
        return subnet;
    }

    public void setSubnet (String subnet)
    {
        this.subnet = subnet;
    }

    public String getNavmethod ()
    {
        return navmethod;
    }

    public void setNavmethod (String navmethod)
    {
        this.navmethod = navmethod;
    }

    public String getBrowsersecurity ()
    {
        return browsersecurity;
    }

    public void setBrowsersecurity (String browsersecurity)
    {
        this.browsersecurity = browsersecurity;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getAttempts ()
    {
        return attempts;
    }

    public void setAttempts (String attempts)
    {
        this.attempts = attempts;
    }

    public String getReviewoverallfeedback ()
    {
        return reviewoverallfeedback;
    }

    public void setReviewoverallfeedback (String reviewoverallfeedback)
    {
        this.reviewoverallfeedback = reviewoverallfeedback;
    }

    public String getCompletionattemptsexhausted ()
    {
        return completionattemptsexhausted;
    }

    public void setCompletionattemptsexhausted (String completionattemptsexhausted)
    {
        this.completionattemptsexhausted = completionattemptsexhausted;
    }

    public String getGrade ()
    {
        return grade;
    }

    public void setGrade (String grade)
    {
        this.grade = grade;
    }

    public String getCanredoquestions ()
    {
        return canredoquestions;
    }

    public void setCanredoquestions (String canredoquestions)
    {
        this.canredoquestions = canredoquestions;
    }

    public String getDecimalpoints ()
    {
        return decimalpoints;
    }

    public void setDecimalpoints (String decimalpoints)
    {
        this.decimalpoints = decimalpoints;
    }

    public String getShowuserpicture ()
    {
        return showuserpicture;
    }

    public void setShowuserpicture (String showuserpicture)
    {
        this.showuserpicture = showuserpicture;
    }

    public String getReviewcorrectness ()
    {
        return reviewcorrectness;
    }

    public void setReviewcorrectness (String reviewcorrectness)
    {
        this.reviewcorrectness = reviewcorrectness;
    }

    public String getShowblocks ()
    {
        return showblocks;
    }

    public void setShowblocks (String showblocks)
    {
        this.showblocks = showblocks;
    }

    public String getTimeopen ()
    {
        return timeopen;
    }

    public void setTimeopen (String timeopen)
    {
        this.timeopen = timeopen;
    }

    public String getReviewrightanswer ()
    {
        return reviewrightanswer;
    }

    public void setReviewrightanswer (String reviewrightanswer)
    {
        this.reviewrightanswer = reviewrightanswer;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getReviewmarks ()
    {
        return reviewmarks;
    }

    public void setReviewmarks (String reviewmarks)
    {
        this.reviewmarks = reviewmarks;
    }

    public String getCompletionpass ()
    {
        return completionpass;
    }

    public void setCompletionpass (String completionpass)
    {
        this.completionpass = completionpass;
    }

    public String getIntro ()
    {
        return intro;
    }

    public void setIntro (String intro)
    {
        this.intro = intro;
    }

    public String getReviewattempt ()
    {
        return reviewattempt;
    }

    public void setReviewattempt (String reviewattempt)
    {
        this.reviewattempt = reviewattempt;
    }

    public String getTimeclose ()
    {
        return timeclose;
    }

    public void setTimeclose (String timeclose)
    {
        this.timeclose = timeclose;
    }

    public String getPreferredbehaviour ()
    {
        return preferredbehaviour;
    }

    public void setPreferredbehaviour (String preferredbehaviour)
    {
        this.preferredbehaviour = preferredbehaviour;
    }

    public String getTimelimit ()
    {
        return timelimit;
    }

    public void setTimelimit (String timelimit)
    {
        this.timelimit = timelimit;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getShuffleanswers ()
    {
        return shuffleanswers;
    }

    public void setShuffleanswers (String shuffleanswers)
    {
        this.shuffleanswers = shuffleanswers;
    }

    public String getDelay2 ()
    {
        return delay2;
    }

    public void setDelay2 (String delay2)
    {
        this.delay2 = delay2;
    }

    public String getReviewgeneralfeedback ()
    {
        return reviewgeneralfeedback;
    }

    public void setReviewgeneralfeedback (String reviewgeneralfeedback)
    {
        this.reviewgeneralfeedback = reviewgeneralfeedback;
    }

    public String getDelay1 ()
    {
        return delay1;
    }

    public void setDelay1 (String delay1)
    {
        this.delay1 = delay1;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [graceperiod = "+graceperiod+", overduehandling = "+overduehandling+", sumgrades = "+sumgrades+", reviewspecificfeedback = "+reviewspecificfeedback+", attemptonlast = "+attemptonlast+", grademethod = "+grademethod+", password = "+password+", questionsperpage = "+questionsperpage+", questiondecimalpoints = "+questiondecimalpoints+", introformat = "+introformat+", course = "+course+", id = "+id+", subnet = "+subnet+", navmethod = "+navmethod+", browsersecurity = "+browsersecurity+", name = "+name+", attempts = "+attempts+", reviewoverallfeedback = "+reviewoverallfeedback+", completionattemptsexhausted = "+completionattemptsexhausted+", grade = "+grade+", canredoquestions = "+canredoquestions+", decimalpoints = "+decimalpoints+", showuserpicture = "+showuserpicture+", reviewcorrectness = "+reviewcorrectness+", showblocks = "+showblocks+", timeopen = "+timeopen+", reviewrightanswer = "+reviewrightanswer+", timemodified = "+timemodified+", reviewmarks = "+reviewmarks+", completionpass = "+completionpass+", intro = "+intro+", reviewattempt = "+reviewattempt+", timeclose = "+timeclose+", preferredbehaviour = "+preferredbehaviour+", timelimit = "+timelimit+", timecreated = "+timecreated+", shuffleanswers = "+shuffleanswers+", delay2 = "+delay2+", reviewgeneralfeedback = "+reviewgeneralfeedback+", delay1 = "+delay1+"]";
    }
}