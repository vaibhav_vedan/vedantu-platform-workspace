package com.vedantu.moodle.pojo.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMQuestionAttemptStatsReq extends AbstractFrontEndReq {
    private Long studentId;
    private Long fromTime;
    private Long thruTime;
    private String difficulty;
    private String currentNodeId;
    private String currentNodeName;
}
