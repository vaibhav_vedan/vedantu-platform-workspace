package com.vedantu.moodle.pojo.request;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SessionDate {

	@NotNull
    @NotBlank(message = "batchId cannot be empty")
    private String batchId;
	
	@NotNull
    @NotBlank(message = "boardId cannot be empty")
    private List<Long> boardId;
	
	@NotNull
    @NotBlank(message = "sharedTime cannot be empty")
    private Long sharedTime;
	
	@NotNull
    @NotBlank(message = "testId cannot be empty")
	private String testId;

}
