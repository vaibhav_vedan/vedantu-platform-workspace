package com.vedantu.moodle.pojo.request;

import java.net.URLEncoder;

import org.springframework.util.StringUtils;

public class GetUserInfoReq {
	private String username;

	public GetUserInfoReq() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetUserInfoReq(String username) {
		super();
		this.username = username;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "GetUserInfoReq [username=" + username + "]";
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(username)) {
			try {
				body = body + "criteria[0][key]=username&criteria[0][value]=" + URLEncoder.encode(username, "UTF-8");
			} catch (Exception ex) {
				// throw nothing
			}
		}

		return body;
	}
}
