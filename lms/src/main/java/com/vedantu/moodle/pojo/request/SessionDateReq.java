package com.vedantu.moodle.pojo.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class SessionDateReq extends AbstractFrontEndListReq{
	
    @NotNull
    private List<SessionDate> sessionDateList;

}
