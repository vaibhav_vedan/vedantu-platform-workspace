package com.vedantu.moodle.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InsightResponseData {
    private Long studentId;
    private Map<String, InsightScore> insightData;
}