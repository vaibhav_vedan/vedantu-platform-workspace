package com.vedantu.moodle.pojo.response;

import com.vedantu.lms.cmds.enums.ContentInfoType;
import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor
public class TeacherDashBoardInfo {
	private Integer shared = 0;
	private Integer attempted = 0;
	private Integer evaluated = 0;
	private ContentInfoType contentInfoType;

	public TeacherDashBoardInfo() {
		super();
	}

	public Integer getShared() {
		return shared;
	}

	public void setShared(Integer shared) {
		this.shared = shared;
	}

	public Integer getAttempted() {
		return attempted;
	}

	public void setAttempted(Integer attempted) {
		this.attempted = attempted;
	}

	public Integer getEvaluated() {
		return evaluated;
	}

	public void setEvaluated(Integer evaluated) {
		this.evaluated = evaluated;
	}

	public ContentInfoType getContentInfoType() {
		return contentInfoType;
	}

	public void setContentInfoType(ContentInfoType contentInfoType) {
		this.contentInfoType = contentInfoType;
	}

	public void incrementShared() {
		this.shared++;
	}

	public void incrementAttempted() {
		this.attempted++;
	}

	public void incrementEvaluated() {
		this.evaluated++;
	}
}
