package com.vedantu.moodle.pojo.request;

import java.net.URLEncoder;

import org.springframework.util.StringUtils;

public class SearchCategoryReq {
	public String parent;
	public String categoryName;

	public SearchCategoryReq(String categoryName) {
		super();
		this.categoryName = categoryName;
	}

	public String getParent() {
		return parent;
	}

	public void setParent(String parent) {
		this.parent = parent;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	@Override
	public String toString() {
		return "SearchCategoryReq [parent=" + parent + ", categoryName=" + categoryName + "]";
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(categoryName)) {
			try {
				body = body + "criteria[0][key]=name&criteria[0][value]=" + URLEncoder.encode(categoryName, "UTF-8");
			} catch (Exception ex) {
				// throw nothing
			}
		}

		return body;
	}
}
