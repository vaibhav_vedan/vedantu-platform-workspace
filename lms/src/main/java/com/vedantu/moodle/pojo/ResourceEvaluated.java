package com.vedantu.moodle.pojo;

public class ResourceEvaluated {

	private Usergrade_data usergrade_data;

    private String authtoken;

    private Gradeitem_data gradeitem_data;
    
    private Student_data student_data;

    private Course_data course_data;
    
    private Assign_data assign_data;

    private Module_data module_data;

    private Teacher_data teacher_data;

    private Event_data event_data;

    private Assigngrade_data assigngrade_data;

    public Assign_data getAssign_data() {
		return assign_data;
	}

	public void setAssign_data(Assign_data assign_data) {
		this.assign_data = assign_data;
	}

    public Teacher_data getTeacher_data() {
		return teacher_data;
	}

	public void setTeacher_data(Teacher_data teacher_data) {
		this.teacher_data = teacher_data;
	}

	public Assigngrade_data getAssigngrade_data() {
		return assigngrade_data;
	}

	public void setAssigngrade_data(Assigngrade_data assigngrade_data) {
		this.assigngrade_data = assigngrade_data;
	}


    public Usergrade_data getUsergrade_data ()
    {
        return usergrade_data;
    }

    public void setUsergrade_data (Usergrade_data usergrade_data)
    {
        this.usergrade_data = usergrade_data;
    }

    public String getAuthtoken ()
    {
        return authtoken;
    }

    public void setAuthtoken (String authtoken)
    {
        this.authtoken = authtoken;
    }

    public Student_data getStudent_data ()
    {
        return student_data;
    }

    public void setStudent_data (Student_data student_data)
    {
        this.student_data = student_data;
    }

    public Course_data getCourse_data ()
    {
        return course_data;
    }

    public void setCourse_data (Course_data course_data)
    {
        this.course_data = course_data;
    }

    public Gradeitem_data getGradeitem_data ()
    {
        return gradeitem_data;
    }

    public void setGradeitem_data (Gradeitem_data gradeitem_data)
    {
        this.gradeitem_data = gradeitem_data;
    }

    public Module_data getModule_data ()
    {
        return module_data;
    }

    public void setModule_data (Module_data module_data)
    {
        this.module_data = module_data;
    }

    public Event_data getEvent_data ()
    {
        return event_data;
    }

    public void setEvent_data (Event_data event_data)
    {
        this.event_data = event_data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [usergrade_data = "+usergrade_data+", authtoken = "+authtoken+", student_data = "+student_data+", course_data = "+course_data+", gradeitem_data = "+gradeitem_data+", module_data = "+module_data+", event_data = "+event_data+"]";
    }
}
