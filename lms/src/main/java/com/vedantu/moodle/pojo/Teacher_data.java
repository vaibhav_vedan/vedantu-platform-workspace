package com.vedantu.moodle.pojo;

public class Teacher_data
{
    private String maildisplay;

    private String firstnamephonetic;

    private String mailformat;

    private String phone2;

    private String calendartype;

    private String phone1;

    private String password;

    private String city;

    private String username;

    private String timezone;

    private String confirmed;

    private String currentlogin;

    private String description;

    private String middlename;

    private String msn;

    private String mnethostid;

    private String descriptionformat;

    private String lastlogin;

    private String timemodified;

    private String lastip;

    private String deleted;

    private String lastaccess;

    private String url;

    private String country;

    private String picture;

    private String aim;

    private String email;

    private String emailstop;

    private String timecreated;

    private String trackforums;

    private String trustbitmask;

    private String auth;

    private String department;

    private String lastname;

    private String icq;

    private String autosubscribe;

    private String alternatename;

    private String lang;

    private String id;

    private String maildigest;

    private String lastnamephonetic;

    private String institution;

    private String yahoo;

    private String firstaccess;

    private String theme;

    private String idnumber;

    private String firstname;

    private String imagealt;

    private String suspended;

    private String address;

    private String secret;

    private String policyagreed;

    private String skype;

    public String getMaildisplay ()
    {
        return maildisplay;
    }

    public void setMaildisplay (String maildisplay)
    {
        this.maildisplay = maildisplay;
    }

    public String getFirstnamephonetic ()
    {
        return firstnamephonetic;
    }

    public void setFirstnamephonetic (String firstnamephonetic)
    {
        this.firstnamephonetic = firstnamephonetic;
    }

    public String getMailformat ()
    {
        return mailformat;
    }

    public void setMailformat (String mailformat)
    {
        this.mailformat = mailformat;
    }

    public String getPhone2 ()
    {
        return phone2;
    }

    public void setPhone2 (String phone2)
    {
        this.phone2 = phone2;
    }

    public String getCalendartype ()
    {
        return calendartype;
    }

    public void setCalendartype (String calendartype)
    {
        this.calendartype = calendartype;
    }

    public String getPhone1 ()
    {
        return phone1;
    }

    public void setPhone1 (String phone1)
    {
        this.phone1 = phone1;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword (String password)
    {
        this.password = password;
    }

    public String getCity ()
    {
        return city;
    }

    public void setCity (String city)
    {
        this.city = city;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getTimezone ()
    {
        return timezone;
    }

    public void setTimezone (String timezone)
    {
        this.timezone = timezone;
    }

    public String getConfirmed ()
    {
        return confirmed;
    }

    public void setConfirmed (String confirmed)
    {
        this.confirmed = confirmed;
    }

    public String getCurrentlogin ()
    {
        return currentlogin;
    }

    public void setCurrentlogin (String currentlogin)
    {
        this.currentlogin = currentlogin;
    }

    public String getDescription ()
    {
        return description;
    }

    public void setDescription (String description)
    {
        this.description = description;
    }

    public String getMiddlename ()
    {
        return middlename;
    }

    public void setMiddlename (String middlename)
    {
        this.middlename = middlename;
    }

    public String getMsn ()
    {
        return msn;
    }

    public void setMsn (String msn)
    {
        this.msn = msn;
    }

    public String getMnethostid ()
    {
        return mnethostid;
    }

    public void setMnethostid (String mnethostid)
    {
        this.mnethostid = mnethostid;
    }

    public String getDescriptionformat ()
    {
        return descriptionformat;
    }

    public void setDescriptionformat (String descriptionformat)
    {
        this.descriptionformat = descriptionformat;
    }

    public String getLastlogin ()
    {
        return lastlogin;
    }

    public void setLastlogin (String lastlogin)
    {
        this.lastlogin = lastlogin;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getLastip ()
    {
        return lastip;
    }

    public void setLastip (String lastip)
    {
        this.lastip = lastip;
    }

    public String getDeleted ()
    {
        return deleted;
    }

    public void setDeleted (String deleted)
    {
        this.deleted = deleted;
    }

    public String getLastaccess ()
    {
        return lastaccess;
    }

    public void setLastaccess (String lastaccess)
    {
        this.lastaccess = lastaccess;
    }

    public String getUrl ()
    {
        return url;
    }

    public void setUrl (String url)
    {
        this.url = url;
    }

    public String getCountry ()
    {
        return country;
    }

    public void setCountry (String country)
    {
        this.country = country;
    }

    public String getPicture ()
    {
        return picture;
    }

    public void setPicture (String picture)
    {
        this.picture = picture;
    }

    public String getAim ()
    {
        return aim;
    }

    public void setAim (String aim)
    {
        this.aim = aim;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String getEmailstop ()
    {
        return emailstop;
    }

    public void setEmailstop (String emailstop)
    {
        this.emailstop = emailstop;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getTrackforums ()
    {
        return trackforums;
    }

    public void setTrackforums (String trackforums)
    {
        this.trackforums = trackforums;
    }

    public String getTrustbitmask ()
    {
        return trustbitmask;
    }

    public void setTrustbitmask (String trustbitmask)
    {
        this.trustbitmask = trustbitmask;
    }

    public String getAuth ()
    {
        return auth;
    }

    public void setAuth (String auth)
    {
        this.auth = auth;
    }

    public String getDepartment ()
    {
        return department;
    }

    public void setDepartment (String department)
    {
        this.department = department;
    }

    public String getLastname ()
    {
        return lastname;
    }

    public void setLastname (String lastname)
    {
        this.lastname = lastname;
    }

    public String getIcq ()
    {
        return icq;
    }

    public void setIcq (String icq)
    {
        this.icq = icq;
    }

    public String getAutosubscribe ()
    {
        return autosubscribe;
    }

    public void setAutosubscribe (String autosubscribe)
    {
        this.autosubscribe = autosubscribe;
    }

    public String getAlternatename ()
    {
        return alternatename;
    }

    public void setAlternatename (String alternatename)
    {
        this.alternatename = alternatename;
    }

    public String getLang ()
    {
        return lang;
    }

    public void setLang (String lang)
    {
        this.lang = lang;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getMaildigest ()
    {
        return maildigest;
    }

    public void setMaildigest (String maildigest)
    {
        this.maildigest = maildigest;
    }

    public String getLastnamephonetic ()
    {
        return lastnamephonetic;
    }

    public void setLastnamephonetic (String lastnamephonetic)
    {
        this.lastnamephonetic = lastnamephonetic;
    }

    public String getInstitution ()
    {
        return institution;
    }

    public void setInstitution (String institution)
    {
        this.institution = institution;
    }

    public String getYahoo ()
    {
        return yahoo;
    }

    public void setYahoo (String yahoo)
    {
        this.yahoo = yahoo;
    }

    public String getFirstaccess ()
    {
        return firstaccess;
    }

    public void setFirstaccess (String firstaccess)
    {
        this.firstaccess = firstaccess;
    }

    public String getTheme ()
    {
        return theme;
    }

    public void setTheme (String theme)
    {
        this.theme = theme;
    }

    public String getIdnumber ()
    {
        return idnumber;
    }

    public void setIdnumber (String idnumber)
    {
        this.idnumber = idnumber;
    }

    public String getFirstname ()
    {
        return firstname;
    }

    public void setFirstname (String firstname)
    {
        this.firstname = firstname;
    }

    public String getImagealt ()
    {
        return imagealt;
    }

    public void setImagealt (String imagealt)
    {
        this.imagealt = imagealt;
    }

    public String getSuspended ()
    {
        return suspended;
    }

    public void setSuspended (String suspended)
    {
        this.suspended = suspended;
    }

    public String getAddress ()
    {
        return address;
    }

    public void setAddress (String address)
    {
        this.address = address;
    }

    public String getSecret ()
    {
        return secret;
    }

    public void setSecret (String secret)
    {
        this.secret = secret;
    }

    public String getPolicyagreed ()
    {
        return policyagreed;
    }

    public void setPolicyagreed (String policyagreed)
    {
        this.policyagreed = policyagreed;
    }

    public String getSkype ()
    {
        return skype;
    }

    public void setSkype (String skype)
    {
        this.skype = skype;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [maildisplay = "+maildisplay+", firstnamephonetic = "+firstnamephonetic+", mailformat = "+mailformat+", phone2 = "+phone2+", calendartype = "+calendartype+", phone1 = "+phone1+", password = "+password+", city = "+city+", username = "+username+", timezone = "+timezone+", confirmed = "+confirmed+", currentlogin = "+currentlogin+", description = "+description+", middlename = "+middlename+", msn = "+msn+", mnethostid = "+mnethostid+", descriptionformat = "+descriptionformat+", lastlogin = "+lastlogin+", timemodified = "+timemodified+", lastip = "+lastip+", deleted = "+deleted+", lastaccess = "+lastaccess+", url = "+url+", country = "+country+", picture = "+picture+", aim = "+aim+", email = "+email+", emailstop = "+emailstop+", timecreated = "+timecreated+", trackforums = "+trackforums+", trustbitmask = "+trustbitmask+", auth = "+auth+", department = "+department+", lastname = "+lastname+", icq = "+icq+", autosubscribe = "+autosubscribe+", alternatename = "+alternatename+", lang = "+lang+", id = "+id+", maildigest = "+maildigest+", lastnamephonetic = "+lastnamephonetic+", institution = "+institution+", yahoo = "+yahoo+", firstaccess = "+firstaccess+", theme = "+theme+", idnumber = "+idnumber+", firstname = "+firstname+", imagealt = "+imagealt+", suspended = "+suspended+", address = "+address+", secret = "+secret+", policyagreed = "+policyagreed+", skype = "+skype+"]";
    }
}