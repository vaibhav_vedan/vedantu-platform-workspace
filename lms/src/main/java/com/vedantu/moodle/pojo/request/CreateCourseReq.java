package com.vedantu.moodle.pojo.request;

import java.net.URLEncoder;

import com.vedantu.util.StringUtils;


public class CreateCourseReq {
	public String fullname;
	public String shortname;
	public String categoryid;
	public String format;

	public CreateCourseReq() {
		super();
	}

	public CreateCourseReq(String fullname, String shortname, String categoryid, String format) {
		super();
		this.fullname = fullname;
		this.shortname = shortname;
		this.categoryid = categoryid;
		this.format = format;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public String getShortname() {
		return shortname;
	}

	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	public String getCategoryid() {
		return categoryid;
	}

	public void setCategoryid(String categoryid) {
		this.categoryid = categoryid;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	@Override
	public String toString() {
		return "CreateContainer [fullname=" + fullname + ", shortname=" + shortname + ", categoryid=" + categoryid
				+ ", format=" + format + "]";
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(fullname)) {
			try {
				body = body + "courses[0][fullname]=" + URLEncoder.encode(fullname, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		if (!StringUtils.isEmpty(shortname)) {
			try {
				body = body + "courses[0][shortname]=" + URLEncoder.encode(shortname, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		if (!StringUtils.isEmpty(format)) {
			try {
				body = body + "courses[0][format]=" + URLEncoder.encode(format, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		if (!StringUtils.isEmpty(categoryid)) {
			try {
				body = body + "courses[0][categoryid]=" + URLEncoder.encode(categoryid, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		if (!StringUtils.isEmpty(body)) {
			body = body.substring(0, body.length() - 1);
		}
		return body;
	}
}
