package com.vedantu.moodle.pojo;

public class Assigngrade_data
{
    private String id;

    private String assignment;

    private String attemptnumber;

    private String timemodified;

    private String userid;

    private String grade;

    private String timecreated;

    private String grader;

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getAssignment ()
    {
        return assignment;
    }

    public void setAssignment (String assignment)
    {
        this.assignment = assignment;
    }

    public String getAttemptnumber ()
    {
        return attemptnumber;
    }

    public void setAttemptnumber (String attemptnumber)
    {
        this.attemptnumber = attemptnumber;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getGrade ()
    {
        return grade;
    }

    public void setGrade (String grade)
    {
        this.grade = grade;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getGrader ()
    {
        return grader;
    }

    public void setGrader (String grader)
    {
        this.grader = grader;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", assignment = "+assignment+", attemptnumber = "+attemptnumber+", timemodified = "+timemodified+", userid = "+userid+", grade = "+grade+", timecreated = "+timecreated+", grader = "+grader+"]";
    }
}
		