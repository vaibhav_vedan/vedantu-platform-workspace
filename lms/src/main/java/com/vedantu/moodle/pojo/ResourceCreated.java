package com.vedantu.moodle.pojo;

import java.util.List;

public class ResourceCreated {

	private String authtoken;

    private Course_data course_data;

    private Quiz_data quiz_data;
    
    private Assign_data assign_data;

    private Module_data module_data;

    private Teacher_data teacher_data;

    private Event_data event_data;

    private Resource_data resource_data;
    
   // private Question_data question_data;

    

//	public Question_data getQuestion_data() {
//		return question_data;
//	}
//
//	public void setQuestion_data(Question_data question_data) {
//		this.question_data = question_data;
//	}

	public Resource_data getResource_data() {
		return resource_data;
	}

	public void setResource_data(Resource_data resource_data) {
		this.resource_data = resource_data;
	}

	public String getAuthtoken ()
    {
        return authtoken;
    }

    public void setAuthtoken (String authtoken)
    {
        this.authtoken = authtoken;
    }

    public Course_data getCourse_data ()
    {
        return course_data;
    }

    public void setCourse_data (Course_data course_data)
    {
        this.course_data = course_data;
    }

    public Assign_data getAssign_data ()
    {
        return assign_data;
    }

    public void setAssign_data (Assign_data assign_data)
    {
        this.assign_data = assign_data;
    }
    
    public Quiz_data getQuiz_data ()
    {
        return quiz_data;
    }

    public void setQuiz_data (Quiz_data quiz_data)
    {
        this.quiz_data = quiz_data;
    }

    public Module_data getModule_data ()
    {
        return module_data;
    }

    public void setModule_data (Module_data module_data)
    {
        this.module_data = module_data;
    }

    public Teacher_data getTeacher_data ()
    {
        return teacher_data;
    }

    public void setTeacher_data (Teacher_data teacher_data)
    {
        this.teacher_data = teacher_data;
    }

    public Event_data getEvent_data ()
    {
        return event_data;
    }

    public void setEvent_data (Event_data event_data)
    {
        this.event_data = event_data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [authtoken = "+authtoken+", course_data = "+course_data+", quiz_data = "+quiz_data+", module_data = "+module_data+", teacher_data = "+teacher_data+", event_data = "+event_data+"]";
    }
}
