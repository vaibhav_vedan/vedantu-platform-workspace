package com.vedantu.moodle.pojo.response;

import java.util.List;

import javax.xml.bind.annotation.XmlElement;

public class MULTIPLE {
	private List<SINGLE> SINGLE;

	private MULTIPLE() {
	}

	@XmlElement
	public List<SINGLE> getSINGLE() {
		return SINGLE;
	}

	public void setSINGLE(List<SINGLE> sINGLE) {
		SINGLE = sINGLE;
	}
}
