package com.vedantu.moodle.pojo.request;

import java.net.URLEncoder;

import com.vedantu.util.StringUtils;



public class EnrollToCourseReq {
	public String roleid;
	public String userid;
	public String courseid;

	public EnrollToCourseReq() {
		super();
	}

	public EnrollToCourseReq(String roleid, String userid, String courseid) {
		super();
		this.roleid = roleid;
		this.userid = userid;
		this.courseid = courseid;
	}

	public String getRoleid() {
		return roleid;
	}

	public void setRoleid(String roleid) {
		this.roleid = roleid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String courseid) {
		this.courseid = courseid;
	}

	@Override
	public String toString() {
		return "EnrollToCourse [roleid=" + roleid + ", userid=" + userid + ", courseid=" + courseid + "]";
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(roleid)) {
			try {
				body = body + "enrolments[0][roleid]=" + URLEncoder.encode(roleid, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}
		if (!StringUtils.isEmpty(userid)) {
			try {
				body = body + "enrolments[0][userid]=" + URLEncoder.encode(userid, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}
		if (!StringUtils.isEmpty(courseid)) {
			try {
				body = body + "enrolments[0][courseid]=" + URLEncoder.encode(courseid, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		if (!StringUtils.isEmpty(body)) {
			body = body.substring(0, body.length() - 1);
		}
		return body;
	}
}
