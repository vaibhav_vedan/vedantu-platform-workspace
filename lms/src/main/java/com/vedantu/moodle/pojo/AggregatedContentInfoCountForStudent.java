package com.vedantu.moodle.pojo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Service;

@Getter
@Setter
public class AggregatedContentInfoCountForStudent {

    private String _id;
    private int count=0;
}
