package com.vedantu.moodle.pojo;

public class Event_data
{
    private String edulevel;

    private Other other;

    private String userid;

    private String contextid;

    private String relateduserid;

    private String anonymous;

    private String component;

    private String contextinstanceid;

    private String action;

    private String target;

    private String crud;

    private String timecreated;

    private String objectid;

    private String courseid;

    private String contextlevel;

    private String objecttable;

    private String eventname;

    public String getEdulevel ()
    {
        return edulevel;
    }

    public void setEdulevel (String edulevel)
    {
        this.edulevel = edulevel;
    }

    public Other getOther ()
    {
        return other;
    }

    public void setOther (Other other)
    {
        this.other = other;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getContextid ()
    {
        return contextid;
    }

    public void setContextid (String contextid)
    {
        this.contextid = contextid;
    }

    public String getRelateduserid ()
    {
        return relateduserid;
    }

    public void setRelateduserid (String relateduserid)
    {
        this.relateduserid = relateduserid;
    }

    public String getAnonymous ()
    {
        return anonymous;
    }

    public void setAnonymous (String anonymous)
    {
        this.anonymous = anonymous;
    }

    public String getComponent ()
    {
        return component;
    }

    public void setComponent (String component)
    {
        this.component = component;
    }

    public String getContextinstanceid ()
    {
        return contextinstanceid;
    }

    public void setContextinstanceid (String contextinstanceid)
    {
        this.contextinstanceid = contextinstanceid;
    }

    public String getAction ()
    {
        return action;
    }

    public void setAction (String action)
    {
        this.action = action;
    }

    public String getTarget ()
    {
        return target;
    }

    public void setTarget (String target)
    {
        this.target = target;
    }

    public String getCrud ()
    {
        return crud;
    }

    public void setCrud (String crud)
    {
        this.crud = crud;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getObjectid ()
    {
        return objectid;
    }

    public void setObjectid (String objectid)
    {
        this.objectid = objectid;
    }

    public String getCourseid ()
    {
        return courseid;
    }

    public void setCourseid (String courseid)
    {
        this.courseid = courseid;
    }

    public String getContextlevel ()
    {
        return contextlevel;
    }

    public void setContextlevel (String contextlevel)
    {
        this.contextlevel = contextlevel;
    }

    public String getObjecttable ()
    {
        return objecttable;
    }

    public void setObjecttable (String objecttable)
    {
        this.objecttable = objecttable;
    }

    public String getEventname ()
    {
        return eventname;
    }

    public void setEventname (String eventname)
    {
        this.eventname = eventname;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [edulevel = "+edulevel+", other = "+other+", userid = "+userid+", contextid = "+contextid+", relateduserid = "+relateduserid+", anonymous = "+anonymous+", component = "+component+", contextinstanceid = "+contextinstanceid+", action = "+action+", target = "+target+", crud = "+crud+", timecreated = "+timecreated+", objectid = "+objectid+", courseid = "+courseid+", contextlevel = "+contextlevel+", objecttable = "+objecttable+", eventname = "+eventname+"]";
    }
}
			