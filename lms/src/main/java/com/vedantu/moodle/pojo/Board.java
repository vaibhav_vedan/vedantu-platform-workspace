package com.vedantu.moodle.pojo;

import java.util.Set;

public class Board {

	private String name;
	private String slug;
	private Long parentId;
	private Set<String> categories;
	private Set<String> grades;
	private Set<String> exams;
	private Long userId;

	public Board() {
		super();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Set<String> getCategories() {
		return categories;
	}

	public void setCategories(Set<String> categories) {
		this.categories = categories;
	}

	public Set<String> getGrades() {
		return grades;
	}

	public void setGrades(Set<String> grades) {
		this.grades = grades;
	}

	public Set<String> getExams() {
		return exams;
	}

	public void setExams(Set<String> exams) {
		this.exams = exams;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
