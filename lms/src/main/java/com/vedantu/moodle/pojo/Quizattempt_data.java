package com.vedantu.moodle.pojo;

public class Quizattempt_data
{
    private String attempt;

    private String sumgrades;

    private String timefinish;

    private String userid;

    private String state;

    private String timemodified;

    private String timecheckstate;

    private String currentpage;

    private String id;

    private String uniqueid;

    private String preview;

    private String quiz;

    private String layout;

    private String timestart;

    public String getAttempt ()
    {
        return attempt;
    }

    public void setAttempt (String attempt)
    {
        this.attempt = attempt;
    }

    public String getSumgrades ()
    {
        return sumgrades;
    }

    public void setSumgrades (String sumgrades)
    {
        this.sumgrades = sumgrades;
    }

    public String getTimefinish ()
    {
        return timefinish;
    }

    public void setTimefinish (String timefinish)
    {
        this.timefinish = timefinish;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getState ()
    {
        return state;
    }

    public void setState (String state)
    {
        this.state = state;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getTimecheckstate ()
    {
        return timecheckstate;
    }

    public void setTimecheckstate (String timecheckstate)
    {
        this.timecheckstate = timecheckstate;
    }

    public String getCurrentpage ()
    {
        return currentpage;
    }

    public void setCurrentpage (String currentpage)
    {
        this.currentpage = currentpage;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getUniqueid ()
    {
        return uniqueid;
    }

    public void setUniqueid (String uniqueid)
    {
        this.uniqueid = uniqueid;
    }

    public String getPreview ()
    {
        return preview;
    }

    public void setPreview (String preview)
    {
        this.preview = preview;
    }

    public String getQuiz ()
    {
        return quiz;
    }

    public void setQuiz (String quiz)
    {
        this.quiz = quiz;
    }

    public String getLayout ()
    {
        return layout;
    }

    public void setLayout (String layout)
    {
        this.layout = layout;
    }

    public String getTimestart ()
    {
        return timestart;
    }

    public void setTimestart (String timestart)
    {
        this.timestart = timestart;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [attempt = "+attempt+", sumgrades = "+sumgrades+", timefinish = "+timefinish+", userid = "+userid+", state = "+state+", timemodified = "+timemodified+", timecheckstate = "+timecheckstate+", currentpage = "+currentpage+", id = "+id+", uniqueid = "+uniqueid+", preview = "+preview+", quiz = "+quiz+", layout = "+layout+", timestart = "+timestart+"]";
    }
}