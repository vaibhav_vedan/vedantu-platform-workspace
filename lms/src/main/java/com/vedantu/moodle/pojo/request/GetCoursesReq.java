package com.vedantu.moodle.pojo.request;

import java.net.URLEncoder;

import org.springframework.util.StringUtils;

public class GetCoursesReq {
	private String userid;

	public GetCoursesReq() {
		super();
	}

	public GetCoursesReq(String userid) {
		super();
		this.userid = userid;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Override
	public String toString() {
		return "GetCoursesReq [userid=" + userid + "]";
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(userid)) {
			try {
				body ="userid=" + URLEncoder.encode(userid, "UTF-8");
			} catch (Exception ex) {
				// throw nothing
			}
		}

		return body;
	}
}
