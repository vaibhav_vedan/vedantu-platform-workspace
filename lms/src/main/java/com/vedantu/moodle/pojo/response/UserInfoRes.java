package com.vedantu.moodle.pojo.response;

public class UserInfoRes {
	private String id;
	private String username;

	public UserInfoRes() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "UserInfoRes [id=" + id + ", username=" + username + "]";
	}
}
