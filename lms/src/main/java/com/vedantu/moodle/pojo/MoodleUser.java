package com.vedantu.moodle.pojo;

import com.vedantu.User.Role;
import java.net.URLEncoder;
import org.springframework.util.StringUtils;

import com.vedantu.moodle.utils.MoodleConfigProperties;

public class MoodleUser {
	@Override
	public String toString() {
		return "User [userId=" + userId + ", id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName="
				+ lastName + ", role=" + role + ", primaryBoard=" + primaryBoard + ", secondaryBoard=" + secondaryBoard
				+ "]";
	}

	private String userId;
	private String id;
	private String email;
	private String firstName;
	private String lastName;
	private Role role;
	private Board primaryBoard;
	private Board secondaryBoard;

	public MoodleUser() {
		super();
	}

	public String getUserId() {
		if (StringUtils.isEmpty(userId)) {
			this.userId = this.id;
		}
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getId() {
                if (StringUtils.isEmpty(id)) {
			this.id = this.userId;
		}
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Board getPrimaryBoard() {
		return primaryBoard;
	}

	public void setPrimaryBoard(Board primaryBoard) {
		this.primaryBoard = primaryBoard;
	}

	public Board getSecondaryBoard() {
		return secondaryBoard;
	}

	public void setSecondaryBoard(Board secondaryBoard) {
		this.secondaryBoard = secondaryBoard;
	}

	public String getPrimarySubject() {
		if (this.primaryBoard != null) {
			String slug = this.primaryBoard.getSlug();
			if (!StringUtils.isEmpty(slug)) {
				return getValidSlugName(slug);
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	public String getSecondarySubject() {
		if (this.secondaryBoard != null) {
			String slug = this.secondaryBoard.getSlug();
			if (!StringUtils.isEmpty(slug)) {
				return getValidSlugName(slug);
			} else {
				return "";
			}
		} else {
			return "";
		}
	}

	public String getPostBody() {
		String body = "";
		if (!StringUtils.isEmpty(getUserId())) {
			try {
				body = body + "users[0][username]=" + URLEncoder.encode(getUserId(), "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		if (!StringUtils.isEmpty(email)) {
			try {
				body = body + "users[0][email]=" + URLEncoder.encode(email, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		if (!StringUtils.isEmpty(firstName)) {
			try {
				body = body + "users[0][firstname]=" + URLEncoder.encode(firstName, "UTF-8") + "&";
			} catch (Exception ex) {
				// throw nothing
			}
		}

		try {
			body = body + "users[0][lastname]="
					+ URLEncoder.encode(StringUtils.isEmpty(lastName) ? "Student" : lastName, "UTF-8") + "&";
		} catch (Exception ex) {
			// throw nothing
		}

		try {
			if (Role.TEACHER.equals(role)) {
				body = body + "users[0][customfields][0][type]=vrole&users[0][customfields][0][value]=TEACHER&";
			} else if (Role.STUDENT.equals(role)) {
				body = body + "users[0][customfields][0][type]=vrole&users[0][customfields][0][value]=STUDENT&";
			}
		} catch (Exception ex) {
			// throw nothing
		}

		body = body + "users[0][password]=" + MoodleConfigProperties.DEFAULT_PASSWORD + "&users[0][auth]="
				+ MoodleConfigProperties.AUTH_TYPE;
		return body;
	}

	public String getValidSlugName(String slug) {
		slug = slug.replaceAll(" ", "_");
		return slug.replaceAll("-", "_");
	}
}
