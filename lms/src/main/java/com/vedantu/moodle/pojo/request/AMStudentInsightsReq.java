package com.vedantu.moodle.pojo.request;

import com.vedantu.moodle.pojo.InsightSortBy;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AMStudentInsightsReq {

    private String amEmail;
    private List<String> insightList;
    private InsightSortBy sortBy;
}
