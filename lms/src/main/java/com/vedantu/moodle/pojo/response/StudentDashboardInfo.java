package com.vedantu.moodle.pojo.response;

import com.vedantu.lms.cmds.enums.ContentType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Builder
@Getter
@AllArgsConstructor
@ToString
public class StudentDashboardInfo {
    private Integer sharedCount;
    private Integer attemptedCount;
    private Integer evaluatedCount;
    private ContentType contentType;
}
