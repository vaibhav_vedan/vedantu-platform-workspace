package com.vedantu.moodle.pojo.response;

import com.vedantu.moodle.entity.ContentInfo;

public class GetContentInfoListRes extends AbstractListRes<ContentInfo> {
	private Integer totalCount;

	public Integer getTotalCount() {
		return totalCount;
	}

	@Override
	public String toString() {
		return "GetContentInfoListRes [totalCount=" + totalCount + "]";
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}
}
