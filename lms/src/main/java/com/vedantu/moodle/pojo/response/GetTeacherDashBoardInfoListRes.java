package com.vedantu.moodle.pojo.response;

import java.util.List;

import com.vedantu.moodle.pojo.EvaluateNowLink;
import lombok.Data;

@Data
public class GetTeacherDashBoardInfoListRes extends AbstractListRes<TeacherDashBoardInfo> {
	private List<EvaluateNowLink> evaluateNowLinks;
}
