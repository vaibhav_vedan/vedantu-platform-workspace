package com.vedantu.moodle.pojo;

import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CMDSQuestion;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@AllArgsConstructor
@Data
@NoArgsConstructor
public class QuestionNodesCorrectenss {
    private String questionId;
    private CMDSQuestion question;
    private int correctNess;
    private Set<BaseTopicTree> nodes;
    private String studentId;
    private Float marksGiven;
}
