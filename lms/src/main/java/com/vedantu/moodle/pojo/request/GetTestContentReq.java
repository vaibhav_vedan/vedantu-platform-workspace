package com.vedantu.moodle.pojo.request;

import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

import java.util.List;
import java.util.Set;
import javax.validation.constraints.Size;

public class GetTestContentReq {

    private String contextId;
    private List<Long> boardIds;
    private Long boardId;
    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String contentQuery;
    private Long userId;
    private ContentState contentState;
    private Integer start;
    private Integer size;
    private String sortParam;
    private ContentInfoType selectedContentInfoType;
    private List<ContentType> contentType;
    private Set<String> contextIds;
    private Long startTime;
    private Long endTime;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<ContentType> getContentType() {
        return contentType;
    }

    public void setContentType(List<ContentType> contentType) {
        this.contentType = contentType;
    }

    public ContentInfoType getSelectedContentInfoType() {
        return selectedContentInfoType;
    }

    public void setSelectedContentInfoType(ContentInfoType selectedContentInfoType) {
        this.selectedContentInfoType = selectedContentInfoType;
    }

    public Integer getStart() {
        return start;
    }

    public void setStart(Integer start) {
        this.start = start;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public String getSortParam() {
        return sortParam;
    }

    public void setSortParam(String sortParam) {
        this.sortParam = sortParam;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public List<Long> getBoardIds() {
        return boardIds;
    }

    public void setBoardIds(List<Long> boardIds) {
        this.boardIds = boardIds;
    }

    public String getContentQuery() {
        return contentQuery;
    }

    public void setContentQuery(String contentQuery) {
        this.contentQuery = contentQuery;
    }

    public ContentState getContentState() {
        return contentState;
    }

    public void setContentState(ContentState contentState) {
        this.contentState = contentState;
    }

    /**
     * @return the boardId
     */
    public Long getBoardId() {
        return boardId;
    }

    /**
     * @param boardId the boardId to set
     */
    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Set<String> getContextIds() {
        return contextIds;
    }

    public void setContextIds(Set<String> contextIds) {
        this.contextIds = contextIds;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }
}
