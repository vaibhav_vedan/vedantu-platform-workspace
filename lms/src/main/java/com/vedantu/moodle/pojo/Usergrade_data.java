package com.vedantu.moodle.pojo;

public class Usergrade_data
{
    private String information;

    private String overridden;

    private String locktime;

    private String timemodified;

    private String userid;

    private String usermodified;

    private String exported;

    private String itemid;

    private String rawscaleid;

    private String feedback;

    private String id;

    private String rawgrademax;

    private String finalgrade;

    private String aggregationweight;

    private String feedbackformat;

    private String excluded;

    private String rawgrademin;

    private String hidden;

    private String aggregationstatus;

    private String timecreated;

    private String informationformat;

    private String locked;

    private String rawgrade;

    public String getInformation ()
    {
        return information;
    }

    public void setInformation (String information)
    {
        this.information = information;
    }

    public String getOverridden ()
    {
        return overridden;
    }

    public void setOverridden (String overridden)
    {
        this.overridden = overridden;
    }

    public String getLocktime ()
    {
        return locktime;
    }

    public void setLocktime (String locktime)
    {
        this.locktime = locktime;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getUserid ()
    {
        return userid;
    }

    public void setUserid (String userid)
    {
        this.userid = userid;
    }

    public String getUsermodified ()
    {
        return usermodified;
    }

    public void setUsermodified (String usermodified)
    {
        this.usermodified = usermodified;
    }

    public String getExported ()
    {
        return exported;
    }

    public void setExported (String exported)
    {
        this.exported = exported;
    }

    public String getItemid ()
    {
        return itemid;
    }

    public void setItemid (String itemid)
    {
        this.itemid = itemid;
    }

    public String getRawscaleid ()
    {
        return rawscaleid;
    }

    public void setRawscaleid (String rawscaleid)
    {
        this.rawscaleid = rawscaleid;
    }

    public String getFeedback ()
    {
        return feedback;
    }

    public void setFeedback (String feedback)
    {
        this.feedback = feedback;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getRawgrademax ()
    {
        return rawgrademax;
    }

    public void setRawgrademax (String rawgrademax)
    {
        this.rawgrademax = rawgrademax;
    }

    public String getFinalgrade ()
    {
        return finalgrade;
    }

    public void setFinalgrade (String finalgrade)
    {
        this.finalgrade = finalgrade;
    }

    public String getAggregationweight ()
    {
        return aggregationweight;
    }

    public void setAggregationweight (String aggregationweight)
    {
        this.aggregationweight = aggregationweight;
    }

    public String getFeedbackformat ()
    {
        return feedbackformat;
    }

    public void setFeedbackformat (String feedbackformat)
    {
        this.feedbackformat = feedbackformat;
    }

    public String getExcluded ()
    {
        return excluded;
    }

    public void setExcluded (String excluded)
    {
        this.excluded = excluded;
    }

    public String getRawgrademin ()
    {
        return rawgrademin;
    }

    public void setRawgrademin (String rawgrademin)
    {
        this.rawgrademin = rawgrademin;
    }

    public String getHidden ()
    {
        return hidden;
    }

    public void setHidden (String hidden)
    {
        this.hidden = hidden;
    }

    public String getAggregationstatus ()
    {
        return aggregationstatus;
    }

    public void setAggregationstatus (String aggregationstatus)
    {
        this.aggregationstatus = aggregationstatus;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getInformationformat ()
    {
        return informationformat;
    }

    public void setInformationformat (String informationformat)
    {
        this.informationformat = informationformat;
    }

    public String getLocked ()
    {
        return locked;
    }

    public void setLocked (String locked)
    {
        this.locked = locked;
    }

    public String getRawgrade ()
    {
        return rawgrade;
    }

    public void setRawgrade (String rawgrade)
    {
        this.rawgrade = rawgrade;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [information = "+information+", overridden = "+overridden+", locktime = "+locktime+", timemodified = "+timemodified+", userid = "+userid+", usermodified = "+usermodified+", exported = "+exported+", itemid = "+itemid+", rawscaleid = "+rawscaleid+", feedback = "+feedback+", id = "+id+", rawgrademax = "+rawgrademax+", finalgrade = "+finalgrade+", aggregationweight = "+aggregationweight+", feedbackformat = "+feedbackformat+", excluded = "+excluded+", rawgrademin = "+rawgrademin+", hidden = "+hidden+", aggregationstatus = "+aggregationstatus+", timecreated = "+timecreated+", informationformat = "+informationformat+", locked = "+locked+", rawgrade = "+rawgrade+"]";
    }
}
		