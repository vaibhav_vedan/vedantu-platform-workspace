package com.vedantu.moodle.pojo;

public class Gradeitem_data
{
    private String aggregationcoef2;

    private String grademax;

    private String locktime;

    private String needsupdate;

    private String multfactor;

    private String aggregationcoef;

    private String calculation;

    private String id;

    private String itemname;

    private String itemmodule;

    private String gradetype;

    private String iteminfo;

    private String courseid;

    private String itemtype;

    private String gradepass;

    private String grademin;

    private String timemodified;

    private String iteminstance;

    private String idnumber;

    private String scaleid;

    private String display;

    private String weightoverride;

    private String plusfactor;

    private String decimals;

    private String categoryid;

    private String outcomeid;

    private String sortorder;

    private String itemnumber;

    private String hidden;

    private String timecreated;

    private String locked;

    public String getAggregationcoef2 ()
    {
        return aggregationcoef2;
    }

    public void setAggregationcoef2 (String aggregationcoef2)
    {
        this.aggregationcoef2 = aggregationcoef2;
    }

    public String getGrademax ()
    {
        return grademax;
    }

    public void setGrademax (String grademax)
    {
        this.grademax = grademax;
    }

    public String getLocktime ()
    {
        return locktime;
    }

    public void setLocktime (String locktime)
    {
        this.locktime = locktime;
    }

    public String getNeedsupdate ()
    {
        return needsupdate;
    }

    public void setNeedsupdate (String needsupdate)
    {
        this.needsupdate = needsupdate;
    }

    public String getMultfactor ()
    {
        return multfactor;
    }

    public void setMultfactor (String multfactor)
    {
        this.multfactor = multfactor;
    }

    public String getAggregationcoef ()
    {
        return aggregationcoef;
    }

    public void setAggregationcoef (String aggregationcoef)
    {
        this.aggregationcoef = aggregationcoef;
    }

    public String getCalculation ()
    {
        return calculation;
    }

    public void setCalculation (String calculation)
    {
        this.calculation = calculation;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getItemname ()
    {
        return itemname;
    }

    public void setItemname (String itemname)
    {
        this.itemname = itemname;
    }

    public String getItemmodule ()
    {
        return itemmodule;
    }

    public void setItemmodule (String itemmodule)
    {
        this.itemmodule = itemmodule;
    }

    public String getGradetype ()
    {
        return gradetype;
    }

    public void setGradetype (String gradetype)
    {
        this.gradetype = gradetype;
    }

    public String getIteminfo ()
    {
        return iteminfo;
    }

    public void setIteminfo (String iteminfo)
    {
        this.iteminfo = iteminfo;
    }

    public String getCourseid ()
    {
        return courseid;
    }

    public void setCourseid (String courseid)
    {
        this.courseid = courseid;
    }

    public String getItemtype ()
    {
        return itemtype;
    }

    public void setItemtype (String itemtype)
    {
        this.itemtype = itemtype;
    }

    public String getGradepass ()
    {
        return gradepass;
    }

    public void setGradepass (String gradepass)
    {
        this.gradepass = gradepass;
    }

    public String getGrademin ()
    {
        return grademin;
    }

    public void setGrademin (String grademin)
    {
        this.grademin = grademin;
    }

    public String getTimemodified ()
    {
        return timemodified;
    }

    public void setTimemodified (String timemodified)
    {
        this.timemodified = timemodified;
    }

    public String getIteminstance ()
    {
        return iteminstance;
    }

    public void setIteminstance (String iteminstance)
    {
        this.iteminstance = iteminstance;
    }

    public String getIdnumber ()
    {
        return idnumber;
    }

    public void setIdnumber (String idnumber)
    {
        this.idnumber = idnumber;
    }

    public String getScaleid ()
    {
        return scaleid;
    }

    public void setScaleid (String scaleid)
    {
        this.scaleid = scaleid;
    }

    public String getDisplay ()
    {
        return display;
    }

    public void setDisplay (String display)
    {
        this.display = display;
    }

    public String getWeightoverride ()
    {
        return weightoverride;
    }

    public void setWeightoverride (String weightoverride)
    {
        this.weightoverride = weightoverride;
    }

    public String getPlusfactor ()
    {
        return plusfactor;
    }

    public void setPlusfactor (String plusfactor)
    {
        this.plusfactor = plusfactor;
    }

    public String getDecimals ()
    {
        return decimals;
    }

    public void setDecimals (String decimals)
    {
        this.decimals = decimals;
    }

    public String getCategoryid ()
    {
        return categoryid;
    }

    public void setCategoryid (String categoryid)
    {
        this.categoryid = categoryid;
    }

    public String getOutcomeid ()
    {
        return outcomeid;
    }

    public void setOutcomeid (String outcomeid)
    {
        this.outcomeid = outcomeid;
    }

    public String getSortorder ()
    {
        return sortorder;
    }

    public void setSortorder (String sortorder)
    {
        this.sortorder = sortorder;
    }

    public String getItemnumber ()
    {
        return itemnumber;
    }

    public void setItemnumber (String itemnumber)
    {
        this.itemnumber = itemnumber;
    }

    public String getHidden ()
    {
        return hidden;
    }

    public void setHidden (String hidden)
    {
        this.hidden = hidden;
    }

    public String getTimecreated ()
    {
        return timecreated;
    }

    public void setTimecreated (String timecreated)
    {
        this.timecreated = timecreated;
    }

    public String getLocked ()
    {
        return locked;
    }

    public void setLocked (String locked)
    {
        this.locked = locked;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [aggregationcoef2 = "+aggregationcoef2+", grademax = "+grademax+", locktime = "+locktime+", needsupdate = "+needsupdate+", multfactor = "+multfactor+", aggregationcoef = "+aggregationcoef+", calculation = "+calculation+", id = "+id+", itemname = "+itemname+", itemmodule = "+itemmodule+", gradetype = "+gradetype+", iteminfo = "+iteminfo+", courseid = "+courseid+", itemtype = "+itemtype+", gradepass = "+gradepass+", grademin = "+grademin+", timemodified = "+timemodified+", iteminstance = "+iteminstance+", idnumber = "+idnumber+", scaleid = "+scaleid+", display = "+display+", weightoverride = "+weightoverride+", plusfactor = "+plusfactor+", decimals = "+decimals+", categoryid = "+categoryid+", outcomeid = "+outcomeid+", sortorder = "+sortorder+", itemnumber = "+itemnumber+", hidden = "+hidden+", timecreated = "+timecreated+", locked = "+locked+"]";
    }
}