package com.vedantu.moodle.pojo;

public class Other
{
    private String submission_editable;

    public String getSubmission_editable ()
    {
        return submission_editable;
    }

    public void setSubmission_editable (String submission_editable)
    {
        this.submission_editable = submission_editable;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [submission_editable = "+submission_editable+"]";
    }
}