package com.vedantu.moodle.pojo;

public class ResourceAttempted {

	private String authtoken;

	private Student_data student_data;

	private Submission_data submission_data;

	private Course_data course_data;

	private Quiz_data quiz_data;

	private Quizattempt_data quizattempt_data;

	private Assign_data assign_data;

	private Module_data module_data;

	private Event_data event_data;

	public Quizattempt_data getQuizattempt_data() {
		return quizattempt_data;
	}

	public void setQuizattempt_data(Quizattempt_data quizattempt_data) {
		this.quizattempt_data = quizattempt_data;
	}

	public Quiz_data getQuiz_data() {
		return quiz_data;
	}

	public void setQuiz_data(Quiz_data quiz_data) {
		this.quiz_data = quiz_data;
	}

	public String getAuthtoken() {
		return authtoken;
	}

	public void setAuthtoken(String authtoken) {
		this.authtoken = authtoken;
	}

	public Student_data getStudent_data() {
		return student_data;
	}

	public void setStudent_data(Student_data student_data) {
		this.student_data = student_data;
	}

	public Submission_data getSubmission_data() {
		return submission_data;
	}

	public void setSubmission_data(Submission_data submission_data) {
		this.submission_data = submission_data;
	}

	public Course_data getCourse_data() {
		return course_data;
	}

	public void setCourse_data(Course_data course_data) {
		this.course_data = course_data;
	}

	public Assign_data getAssign_data() {
		return assign_data;
	}

	public void setAssign_data(Assign_data assign_data) {
		this.assign_data = assign_data;
	}

	public Module_data getModule_data() {
		return module_data;
	}

	public void setModule_data(Module_data module_data) {
		this.module_data = module_data;
	}

	public Event_data getEvent_data() {
		return event_data;
	}

	public void setEvent_data(Event_data event_data) {
		this.event_data = event_data;
	}

	@Override
	public String toString() {
		return "ClassPojo [authtoken = " + authtoken + ", student_data = " + student_data + ", submission_data = "
				+ submission_data + ", course_data = " + course_data + ", assign_data = " + assign_data
				+ ", module_data = " + module_data + ", event_data = " + event_data + "]";
	}

}
