package com.vedantu.moodle.pojo;

import com.vedantu.User.UserBasicInfo;
import java.util.List;


public class GroupPojo {
	@Override
	public String toString() {
		return "GroupPojo [batchId=" + batchId + ", courseInfo=" + courseInfo + ", teachers=" + teachers
				+ ", enrolledStudents=" + enrolledStudents + ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}

	private String batchId;
	private CourseBasicInfo courseInfo;
	private List<UserBasicInfo> teachers;
	private List<UserBasicInfo> enrolledStudents;
	private Long startTime;
	private Long endTime;
	
	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public List<UserBasicInfo> getTeachers() {
		return teachers;
	}

	public void setTeachers(List<UserBasicInfo> teachers) {
		this.teachers = teachers;
	}

	public List<UserBasicInfo> getEnrolledStudents() {
		return enrolledStudents;
	}

	public void setEnrolledStudents(List<UserBasicInfo> enrolledStudents) {
		this.enrolledStudents = enrolledStudents;
	}

	public CourseBasicInfo getCourseInfo() {
		return courseInfo;
	}

	public void setCourseInfo(CourseBasicInfo courseInfo) {
		this.courseInfo = courseInfo;
	}

	public Long getStartTime() {
		return startTime;
	}

	public void setStartTime(Long startTime) {
		this.startTime = startTime;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	
}
