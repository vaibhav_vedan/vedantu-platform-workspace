package com.vedantu.moodle.pojo.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AMUsageStatusReq extends AbstractFrontEndReq {
    private Long studentId;
}
