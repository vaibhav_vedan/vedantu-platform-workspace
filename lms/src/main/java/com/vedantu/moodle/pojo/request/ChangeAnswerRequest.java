/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.moodle.pojo.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class ChangeAnswerRequest extends AbstractFrontEndReq{
    
    private String questionId;
    private List<String> testIdsForReEvaluation;
    private List<String> newAnswer;
    private List<String> challengeIdsForReEvaluation;
    private String sendNotificationTestId;

    /**
     * @return the questionId
     */
    public String getQuestionId() {
        return questionId;
    }

    /**
     * @param questionId the questionId to set
     */
    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }

    /**
     * @return the testIdsForReEvaluation
     */
    public List<String> getTestIdsForReEvaluation() {
        return testIdsForReEvaluation;
    }

    /**
     * @param testIdsForReEvaluation the testIdsForReEvaluation to set
     */
    public void setTestIdsForReEvaluation(List<String> testIdsForReEvaluation) {
        this.testIdsForReEvaluation = testIdsForReEvaluation;
    }

    /**
     * @return the newAnswer
     */
    public List<String> getNewAnswer() {
        return newAnswer;
    }

    /**
     * @param newAnswer the newAnswer to set
     */
    public void setNewAnswer(List<String> newAnswer) {
        this.newAnswer = newAnswer;
    }
    
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        if(StringUtils.isEmpty(questionId)){
            errors.add("Invalid questionId");
        }
        
        if(ArrayUtils.isEmpty(newAnswer)){
            errors.add("Invalid Answers");
        }
        
        if(super.getCallingUserId() == null){
            errors.add("Invalid callingUserId");
        }
        
        return errors;
        
    }

    /**
     * @return the challengeIdsForReEvaluation
     */
    public List<String> getChallengeIdsForReEvaluation() {
        return challengeIdsForReEvaluation;
    }

    /**
     * @param challengeIdsForReEvaluation the challengeIdsForReEvaluation to set
     */
    public void setChallengeIdsForReEvaluation(List<String> challengeIdsForReEvaluation) {
        this.challengeIdsForReEvaluation = challengeIdsForReEvaluation;
    }

    /**
     * @return the sendNotificationTestId
     */
    public String getSendNotificationTestId() {
        return sendNotificationTestId;
    }

    /**
     * @param sendNotificationTestId the sendNotificationTestId to set
     */
    public void setSendNotificationTestId(String sendNotificationTestId) {
        this.sendNotificationTestId = sendNotificationTestId;
    }
    
    
}
