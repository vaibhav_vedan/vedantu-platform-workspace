package com.vedantu.moodle.pojo;

public class CourseMarks {
	    private String[] outcomes;

	    private Items[] items;

	    public String[] getOutcomes ()
	    {
	        return outcomes;
	    }

	    public void setOutcomes (String[] outcomes)
	    {
	        this.outcomes = outcomes;
	    }

	    public Items[] getItems ()
	    {
	        return items;
	    }

	    public void setItems (Items[] items)
	    {
	        this.items = items;
	    }

	    @Override
	    public String toString()
	    {
	        return "ClassPojo [outcomes = "+outcomes+", items = "+items+"]";
	    }

	
	
}
