package com.vedantu.moodle.youscore.response;

public class YouScoreTestDetailsRes {

	public String ExamCode;
	public String ContentTitle;
	public String Subject;
	public String Duration;
	public String TotalMarks;

	public YouScoreTestDetailsRes() {
		super();
	}

	public String getExamCode() {
		return ExamCode;
	}

	public void setExamCode(String examCode) {
		ExamCode = examCode;
	}

	public String getContentTitle() {
		return ContentTitle;
	}

	public void setContentTitle(String contentTitle) {
		ContentTitle = contentTitle;
	}

	public String getSubject() {
		return Subject;
	}

	public void setSubject(String subject) {
		Subject = subject;
	}

	public String getDuration() {
		return Duration;
	}

	public void setDuration(String duration) {
		Duration = duration;
	}

	public String getTotalMarks() {
		return TotalMarks;
	}

	public void setTotalMarks(String totalMarks) {
		TotalMarks = totalMarks;
	}

	@Override
	public String toString() {
		return "YouScoreTestDetailsRes [ExamCode=" + ExamCode + ", ContentTitle=" + ContentTitle + ", Subject="
				+ Subject + ", Duration=" + Duration + ", TotalMarks=" + TotalMarks + "]";
	}
}
