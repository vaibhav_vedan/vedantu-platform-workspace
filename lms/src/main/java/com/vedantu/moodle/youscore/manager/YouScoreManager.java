package com.vedantu.moodle.youscore.manager;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.youscore.pojo.YouScoreOTFContentShareReq;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.pojo.response.BasicRes;
import com.vedantu.moodle.youscore.request.YouScoreContentEvaluatedReq;
import com.vedantu.moodle.youscore.request.YouScoreContentSharedReq;
import com.vedantu.moodle.youscore.response.YouScoreTestDetailsRes;
import com.vedantu.notification.pojos.MoodleContentInfo;
import com.vedantu.notification.requests.SendMoodleNotificationsReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.PlatformTools;
import com.vedantu.util.WebUtils;

@Service
public class YouScoreManager {

	@Autowired
	private LogFactory logfactory;

	@SuppressWarnings("static-access")
	private Logger logger = logfactory.getLogger(YouScoreManager.class);

	@Autowired
	private ContentInfoManager contentInfoManager;

	@Autowired
	private PlatformTools platformTools;

	@Autowired
	private FosUtils fosUtils;

	private String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

	private String YOUSCORE_ENDPOINT = "http://api.scobotic.com/";

	private String YOUSCORE_APP_ID = "vedantuzxz";

	private String YOUSCORE_APP_PASS = "zxcxzc";

	private String YOUSCORE_TESTDETAILS_API = YOUSCORE_ENDPOINT + "api/User/GetDataByExamCode?AppId=" + YOUSCORE_APP_ID
			+ "&AppPass=" + YOUSCORE_APP_PASS;

	public static final String YOUSCORE_DEFAULT_CONTENT_TITLE = "Test powered by youscore";

	public static final Gson gson = new Gson();

	public BasicRes contentShared(YouScoreContentSharedReq youScoreContentSharedReq) {
		BasicRes res = new BasicRes();
		List<String> errors = youScoreContentSharedReq.validate();
		if (!CollectionUtils.isEmpty(errors)) {
			logger.info("contentShared- req:" + youScoreContentSharedReq.toString() + "errors:"
					+ Arrays.toString(errors.toArray()));
		}

		// Check if the content exists
		Set<String> userIds = new HashSet<String>();
		userIds.add(youScoreContentSharedReq.getStudentId());
		userIds.add(youScoreContentSharedReq.getTeacherId());

		List<UserBasicInfo> userResults = fosUtils.getUserBasicInfosSet(userIds, false);
		UserBasicInfo student = null;
		UserBasicInfo teacher = null;

		for (UserBasicInfo entry : userResults) {
			if (entry == null) {
				continue;
			}

			if (Role.STUDENT.equals(entry.getRole())) {
				student = entry;
			} else if (Role.TEACHER.equals(entry.getRole())) {
				teacher = entry;
			}
		}

		if (student == null || teacher == null) {
			logger.info("User not found for req:" + youScoreContentSharedReq.toString());
			res.setSuccess(false);
			return res;
		}

		ContentInfo contentInfo = new ContentInfo(youScoreContentSharedReq, student, teacher);
		setContentLinks(contentInfo);
		updateTestDetails(contentInfo);
		upsertContentInfo(contentInfo);
		sendYouscoreNotifications(contentInfo.getId(), "SHARED");
		return new BasicRes();
	}

	public BasicRes contentEvaluated(YouScoreContentEvaluatedReq youScoreContentEvaluatedReq) {
		BasicRes res = new BasicRes();
		List<ContentInfo> contentInfos = contentInfoManager.fetchContents(youScoreContentEvaluatedReq.getStudentId(),
				null, youScoreContentEvaluatedReq.getTestId());
		if (CollectionUtils.isEmpty(contentInfos)) {
			logger.info("No content found for request :" + youScoreContentEvaluatedReq.toString());
			res.setSuccess(false);
			return res;
		}

		if (contentInfos.size() > 0) {
			// Track no of tests are shared multiple times
			logger.info("Multiple content infos found for req:" + youScoreContentEvaluatedReq.toString()
					+ " contents: " + contentInfos.toString());
		}
		ContentInfo contentInfo = contentInfos.get(0);
		contentInfo.setContentState(ContentState.EVALUATED);
		long currentTime = System.currentTimeMillis();
		contentInfo.setEvaulatedTime(currentTime);
		contentInfo.setAttemptedTime(currentTime);
		contentInfo.setMarksAcheived(youScoreContentEvaluatedReq.getMarksAcheived());
		contentInfoManager.addContents(contentInfo);
		logger.info("contentInfo:" + contentInfo.toString());
		sendYouscoreNotifications(contentInfo.getId(), "ATTEMPTED");
		return new BasicRes();
	}

	public BasicRes contentShareBulk(YouScoreOTFContentShareReq req) {
		logger.info("Request - " + req.toString());
		List<UserBasicInfo> students = req.getStudents();
		List<UserBasicInfo> teachers = req.getTeachers();
		List<String> examCodes = req.getExamCodes();
		if (!CollectionUtils.isEmpty(students) && !CollectionUtils.isEmpty(teachers)
				&& !CollectionUtils.isEmpty(examCodes)) {
			for (UserBasicInfo student : students) {
				for (UserBasicInfo teacher : teachers) {
					for (String examCode : examCodes) {
						ContentInfo contentInfo = new ContentInfo(student, teacher, examCode);
						setContentLinks(contentInfo);
						updateTestDetails(contentInfo);
						logger.info("ContentInfo : " + contentInfo.toString());
						upsertContentInfo(contentInfo);
						logger.info("contentInfo : " + contentInfo.toString());
					}
				}
			}
		}

		return new BasicRes();
	}

	private void upsertContentInfo(ContentInfo contentInfo) {
		// Validate checking
		List<ContentInfo> contentInfos = contentInfoManager.fetchContents(contentInfo.getStudentId(),
				contentInfo.getTeacherId(), contentInfo.getTestId());
		if (!CollectionUtils.isEmpty(contentInfos)) {
			ContentInfo entry = contentInfos.get(0);
			logger.info("Content found for req : " + contentInfo.toString());
			entry.setExpiryTime(contentInfo.getExpiryTime());
			entry.setDuration(contentInfo.getDuration());
			contentInfo = entry;
		}

		contentInfoManager.addContents(contentInfo);
	}

	public void setContentLinks(ContentInfo contentInfo) {
		contentInfo.setContentLink(FOS_ENDPOINT + "youscore/studentTakeTest?examCode=" + contentInfo.getTestId());
		contentInfo.setStudentActionLink(FOS_ENDPOINT + "youscore/studentTakeTest?examCode=" + contentInfo.getTestId());
		contentInfo.setTeacherActionLink(FOS_ENDPOINT + "youscore/teacherportal?examCode=" + contentInfo.getTestId());
	}

	public void sendYouscoreNotifications(String contentInfoId, String eventName) {
		String moodleNotificationUrl = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT")
				+ "/sendMoodleNotifications";
		ContentInfo contentInfo = contentInfoManager.getContentById(contentInfoId);

		MoodleContentInfo moodleContentInfo = gson.fromJson(gson.toJson(contentInfo), MoodleContentInfo.class);
		SendMoodleNotificationsReq req = new SendMoodleNotificationsReq(eventName, moodleContentInfo);
		logger.info("sendMoodleNotifications : " + moodleNotificationUrl);
		ClientResponse response = WebUtils.INSTANCE.doCall(moodleNotificationUrl, HttpMethod.POST,
				new Gson().toJson(req), true);
		logger.info("response : " + response);
	}

	public void updateTestDetails(ContentInfo contentInfo) {
		String examCode = contentInfo.getTestId();
		if (StringUtils.isEmpty(examCode)) {
			logger.info("updateTestDetailsError - No exam code : " + contentInfo.toString());
		}

		YouScoreTestDetailsRes testDetailsRes = getTestDetails(contentInfo.getTestId());
		logger.info("updateTestDetails - " + testDetailsRes);
		contentInfo.updateTestDetails(testDetailsRes);
	}

	public YouScoreTestDetailsRes getTestDetails(String examCode) {
		String location = YOUSCORE_TESTDETAILS_API + "&ExamCode=" + examCode;
		PlatformBasicResponse response = platformTools.postPlatformData(location, null, true, HttpMethod.GET);
		YouScoreTestDetailsRes testDetails = null;
		if (!response.isSuccess()) {
			logger.info("getTestDetailsError : " + examCode + " response:" + response.toString());
			return testDetails;
		}

		try {
			String responsStr = response.getResponse();
			if (!StringUtils.isEmpty(responsStr)) {
				Type listType = new TypeToken<List<YouScoreTestDetailsRes>>() {
				}.getType();
				List<YouScoreTestDetailsRes> results = new Gson().fromJson(responsStr, listType);
				if (!CollectionUtils.isEmpty(results)) {
					if (results.size() > 1) {
						logger.error("getTestDetailsIssue - Multiple results found " + examCode + " response:"
								+ response.toString());
					}
					testDetails = results.get(0);
					if (StringUtils.isEmpty(testDetails.getContentTitle())) {
						logger.error("getTestDetailsError - Title missing " + testDetails.toString());
					}
				}
			}
		} catch (Exception ex) {
			logger.error(
					"getTestDetailsException - " + ex.getMessage() + " examCode:" + examCode + " response:" + response);
		}
		return testDetails;
	}
}
