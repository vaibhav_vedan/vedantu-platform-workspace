package com.vedantu.moodle.youscore.request;

public class YouScoreContentEvaluatedReq {
	private String testId;
	private String studentId;
	private Float marksAcheived;

	public YouScoreContentEvaluatedReq() {
		super();
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public Float getMarksAcheived() {
		return marksAcheived;
	}

	public void setMarksAcheived(Float marksAcheived) {
		this.marksAcheived = marksAcheived;
	}

	@Override
	public String toString() {
		return "YouScoreContentEvaluatedReq [testId=" + testId + ", studentId=" + studentId + "]";
	}
}
