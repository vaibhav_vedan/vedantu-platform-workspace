package com.vedantu.moodle.youscore.controller;

import java.io.UnsupportedEncodingException;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.lms.youscore.pojo.YouScoreOTFContentShareReq;
import com.vedantu.moodle.pojo.response.BasicRes;
import com.vedantu.moodle.youscore.manager.YouScoreManager;
import com.vedantu.moodle.youscore.request.YouScoreContentEvaluatedReq;
import com.vedantu.moodle.youscore.request.YouScoreContentSharedReq;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("/youscore/")
@Service
public class YouScoreController {

	@Autowired
	private LogFactory logfactory;

	@SuppressWarnings("static-access")
	private Logger logger = logfactory.getLogger(YouScoreController.class);

	@Autowired
	private YouScoreManager youScoreManager;

	@RequestMapping(value = "/content/share", method = RequestMethod.POST)
	public BasicRes contentShared(@RequestBody YouScoreContentSharedReq youScoreContentSharedReq)
			throws UnsupportedEncodingException, BadRequestException {
		logger.info("Request-" + youScoreContentSharedReq);
		return youScoreManager.contentShared(youScoreContentSharedReq);
	}

	@RequestMapping(value = "/content/evaluate", method = RequestMethod.POST)
	public BasicRes contentEvaluated(@RequestBody YouScoreContentEvaluatedReq youScoreContentEvaluatedReq)
			throws UnsupportedEncodingException, BadRequestException {
		logger.info("Request-" + youScoreContentEvaluatedReq);
		return youScoreManager.contentEvaluated(youScoreContentEvaluatedReq);
	}

	@RequestMapping(value = "/content/shareBulk", method = RequestMethod.POST)
	public BasicRes contentShareBulk(@RequestBody YouScoreOTFContentShareReq youScoreContentEvaluatedReq)
			throws UnsupportedEncodingException, BadRequestException {
		logger.info("Request-" + youScoreContentEvaluatedReq);
		return youScoreManager.contentShareBulk(youScoreContentEvaluatedReq);
	}
}
