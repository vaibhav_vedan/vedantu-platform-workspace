package com.vedantu.moodle.youscore.request;

import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;
public class YouScoreContentSharedReq {
	private String testId;
	private String studentId;
	private String teacherId;
	private String contentTitle;
	private String subject;
	private Long duration;
	private Long expiryTime;
	private Float totalMarks;

	private ContentType contentType = ContentType.TEST;
	private ContentInfoType contentInfoType = ContentInfoType.OBJECTIVE;
	private EngagementType engagementType = EngagementType.OTO;
	private ContentState contentState = ContentState.SHARED;

	public YouScoreContentSharedReq() {
		super();
	}

	public String getTestId() {
		return testId;
	}

	public void setTestId(String testId) {
		this.testId = testId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getContentTitle() {
		return contentTitle;
	}

	public void setContentTitle(String contentTitle) {
		this.contentTitle = contentTitle;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	public ContentType getContentType() {
		return contentType;
	}

	public void setContentType(ContentType contentType) {
		this.contentType = contentType;
	}

	public ContentInfoType getContentInfoType() {
		return contentInfoType;
	}

	public void setContentInfoType(ContentInfoType contentInfoType) {
		this.contentInfoType = contentInfoType;
	}

	public EngagementType getEngagementType() {
		return engagementType;
	}

	public void setEngagementType(EngagementType engagementType) {
		this.engagementType = engagementType;
	}

	public ContentState getContentState() {
		return contentState;
	}

	public void setContentState(ContentState contentState) {
		this.contentState = contentState;
	}

	public Float getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(Float totalMarks) {
		this.totalMarks = totalMarks;
	}

	public List<String> validate() {
		List<String> errors = new ArrayList<String>();
		if (StringUtils.isEmpty(this.testId)) {
			errors.add("testId");
		}

		if (StringUtils.isEmpty(this.studentId)) {
			errors.add("studentId");
		}

		if (StringUtils.isEmpty(this.teacherId)) {
			errors.add("teacherId");
		}

		if (StringUtils.isEmpty(this.contentTitle)) {
			errors.add("contentTitle");
		}

		if (this.duration == null || this.duration <= 0l) {
			errors.add("duration");
		}

		return errors;
	}

	@Override
	public String toString() {
		return "YouScoreContentSharedReq [testId=" + testId + ", studentId=" + studentId + ", teacherId=" + teacherId
				+ ", contentTitle=" + contentTitle + ", subject=" + subject + ", duration=" + duration + ", expiryTime="
				+ expiryTime + ", totalMarks=" + totalMarks + ", contentType=" + contentType + ", contentInfoType="
				+ contentInfoType + ", engagementType=" + engagementType + ", contentState=" + contentState + "]";
	}
}
