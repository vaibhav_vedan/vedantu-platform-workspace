package com.vedantu.moodle.dao;

import java.time.Instant;
import java.util.*;

import com.vedantu.cmds.entities.*;
import com.vedantu.doubts.entities.mongo.Doubt;
import com.vedantu.doubts.entities.mongo.DoubtChatMessage;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.entity.LOUsageStats;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Field;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@SuppressWarnings("Duplicates")
@Service
public class LOAMAmbassadorLMSDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(LOAMAmbassadorLMSDAO.class);

	public static final long ONE_DAY_IN_MILLIS = 86400000;

	public LOAMAmbassadorLMSDAO() {
		super();
	}

	@Override
	public MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void saveEntity(TestNodeStats testNodeStats) {
		super.saveEntity(testNodeStats);
	}

	private void loam_manageFieldsInclude(Query q, Set<String> keySet, boolean isInclude){
		if(keySet !=null && !keySet.isEmpty())
		{
			if(isInclude)
				keySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
			else
				keySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
		}
	}

    public List<ContentInfo> loam_getContentInfo(Long fromTime,Long thruTime, Set<String> keySet, boolean isInclude, Integer start, Integer size) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(ContentInfo.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		loam_manageFieldsInclude(query, keySet, isInclude);

		query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		return runQuery(query, ContentInfo.class);
    }

	public List<CMDSTest> loam_getCMDSTest(Long fromTime,Long thruTime, Set<String> keySet, boolean isInclude, Integer start, Integer size) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(CMDSTest.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(CMDSTest.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		loam_manageFieldsInclude(query, keySet, isInclude);

		query.with(Sort.by(Sort.Direction.DESC, CMDSTest.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);
		return runQuery(query, CMDSTest.class);
	}

	public List<CMDSQuestion> loam_getCMDSQuestion(Long fromTime,Long thruTime, Set<String> keySet, boolean isInclude, Integer start, Integer size) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(CMDSQuestion.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(CMDSQuestion.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		loam_manageFieldsInclude(query, keySet, isInclude);

		query.with(Sort.by(Sort.Direction.DESC, CMDSQuestion.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		return runQuery(query, CMDSQuestion.class);
	}

	public List<BaseTopicTree> loam_getBaseTopicTree(Long fromTime,Long thruTime, Set<String> keySet, boolean isInclude, Integer start, Integer size) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(BaseTopicTree.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(BaseTopicTree.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		loam_manageFieldsInclude(query, keySet, isInclude);

		query.with(Sort.by(Sort.Direction.DESC, BaseTopicTree.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		return runQuery(query, BaseTopicTree.class);
	}

	public List<QuestionAttempt> loam_getQuestionAttempt(Long fromTime,Long thruTime, Set<String> keySet, boolean isInclude, Integer start, Integer size) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(QuestionAttempt.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(QuestionAttempt.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		loam_manageFieldsInclude(query, keySet, isInclude);

		query.with(Sort.by(Sort.Direction.DESC, QuestionAttempt.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		return runQuery(query, QuestionAttempt.class);
	}

	public List<Doubt> loam_getDoubts(Long fromTime,Long thruTime, Set<String> keySet, boolean isInclude, Integer start, Integer size) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(Doubt.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(Doubt.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		loam_manageFieldsInclude(query, keySet, isInclude);

		query.with(Sort.by(Sort.Direction.DESC, Doubt.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		return runQuery(query, Doubt.class);
	}

	public List<DoubtChatMessage> loam_getDoubtChatMessages(Long fromTime,Long thruTime, Set<String> keySet, boolean isInclude, Integer start, Integer size) {
		Query query = new Query();

		Criteria andCriteria = new Criteria().andOperator(Criteria.where(DoubtChatMessage.Constants.LAST_UPDATED).gte(fromTime), Criteria.where(DoubtChatMessage.Constants.LAST_UPDATED).lte(thruTime));
		query.addCriteria(andCriteria);
		loam_manageFieldsInclude(query, keySet, isInclude);

		query.with(Sort.by(Sort.Direction.DESC, DoubtChatMessage.Constants.LAST_UPDATED));
		setFetchParameters(query, start, size);

		return runQuery(query, DoubtChatMessage.class);
	}
}
