package com.vedantu.moodle.dao;

import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class UpdateContentInfoDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(UpdateContentInfoDAO.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public List<ContentInfo> getAssignmentData(final Long creationTime) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(creationTime));
        q.addCriteria(Criteria.where("metadata.duration").is(0));
        q.with(Sort.by(Direction.DESC, String.valueOf(Arrays.asList(ContentInfo.Constants.SUBJECT, "metadata." + ContentInfo.Constants.TEST_ID, "metadata.testAttempts.0.marksAcheived"))));
        logger.info("Getting in DAO:" + "Find query " + q);
        return runQuery(q, ContentInfo.class);
    }

    public void updateAssignmentData(final ContentInfo p) {
        try {
            if (p != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(ContentInfo.Constants._ID).is(p.getId()));
                Update u = new Update();
                u.set(ContentInfo.Constants.METADATA, p.getMetadata());
                updateFirst(q, u, ContentInfo.class);
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Error updating ContentInfo" + p + "with rank, percentage and percentile. Exception: " + ex.toString());
            throw new RuntimeException("Error updating content info", ex);
        }
    }

    public List<ContentInfo> getTestData(final Long creationTime) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(creationTime));
        q.addCriteria(Criteria.where("metadata.duration").gt(0));
        q.with(Sort.by(Direction.DESC, String.valueOf(Arrays.asList(ContentInfo.Constants.SUBJECT, "metadata." + ContentInfo.Constants.TEST_ID, "metadata.testAttempts.0.marksAcheived"))));
        logger.info("Getting in DAO:" + "Find query " + q);
        return runQuery(q, ContentInfo.class);
    }

    public void updateTestData(final ContentInfo p) {
        try {
            if (p != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(ContentInfo.Constants._ID).is(p.getId()));
                Update u = new Update();
                u.set(ContentInfo.Constants.METADATA, p.getMetadata());
                updateFirst(q, u, ContentInfo.class);
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Error updating ContentInfo" + p + "with rank, percentage and percentile. Exception: " + ex.toString());
            throw new RuntimeException("Error updating content info", ex);
        }
    }
    
    public List<ContentInfo> getAssignmentByStudent(final Long studentId, final Long fromTime, final Long thruTime) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        q.addCriteria(Criteria.where("metadata.duration").exists(false));
        q.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()));
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(fromTime), Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(thruTime));
        q.addCriteria(andCriteria);
        q.with(Sort.by(Direction.DESC, String.valueOf(Arrays.asList(ContentInfo.Constants.SUBJECT, "metadata." + ContentInfo.Constants.TEST_ID, "metadata.testAttempts.0.marksAcheived"))));
        return runQuery(q, ContentInfo.class);
    }

    public List<ContentInfo> getTestByStudent(final Long studentId, final Long fromTime, final Long thruTime) {
        Query q = new Query();
        q.addCriteria(Criteria.where("metadata.duration").gt(0));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        q.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()));
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(fromTime), Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(thruTime));
        q.addCriteria(andCriteria);
        q.with(Sort.by(Direction.DESC, String.valueOf(Arrays.asList(ContentInfo.Constants.SUBJECT, "metadata." + ContentInfo.Constants.TEST_ID, "metadata.testAttempts.0.marksAcheived"))));
        logger.info("Query: " + q);
        return runQuery(q, ContentInfo.class);
    }

}
