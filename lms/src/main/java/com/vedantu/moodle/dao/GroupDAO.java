package com.vedantu.moodle.dao;

import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import com.vedantu.moodle.entity.Group;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class GroupDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public GroupDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
        
	public void save(Group group) {
		try {
			Assert.notNull(group);
			
			saveEntity(group);
		} catch (Exception ex) {
			throw new RuntimeException(
					"ContentInfoUpdateError : Error updating the content info " + group.toString(), ex);
		}
	}

	public Group getById(String id) {
		Group group = null;
		try {
			Assert.hasText(id);
			group = (Group) getEntityById(id, Group.class);
		} catch (Exception ex) {
			throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
		}
		return group;
	}
}
