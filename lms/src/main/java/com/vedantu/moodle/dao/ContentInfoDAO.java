package com.vedantu.moodle.dao;

import com.google.gson.Gson;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Field;
import com.mongodb.client.model.Projections;
import com.mongodb.client.model.Sorts;
import com.vedantu.cmds.dao.CMDSTestAttemptDAO;
import com.vedantu.cmds.managers.AwsSNSManager;
import com.vedantu.cmds.managers.AwsSQSManager;
import com.vedantu.cmds.pojo.AggregatedTestId;
import com.vedantu.cmds.pojo.QuestionDurationAggregation;
import com.vedantu.cmds.pojo.TeacherContentDashboardIncrementInfo;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.cmds.pojo.TestAnalyticsAggregation;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.lms.cmds.enums.*;
import com.vedantu.lms.cmds.pojo.QuestionAttemptState;
import com.vedantu.lms.request.ContentInfoReq;
import com.vedantu.lms.request.UnshareContentReq;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.pojo.request.GetTestContentReq;
import com.vedantu.moodle.response.HomepageTestResponse;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.*;
import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.fos.request.ReqLimits;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.Optional;
import static com.vedantu.moodle.entity.ContentInfo.Constants.*;


@Service
public class ContentInfoDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    @Autowired
    private CMDSTestAttemptDAO cmdsTestAttemptDAO;


    public ContentInfoDAO() {
        super();
    }

    private Logger logger = LogFactory.getLogger(ContentInfoDAO.class);
    private static final Integer MAX_COUNT = ConfigUtils.INSTANCE.getIntValue("teacherId.change.max", 1000);
    private static final Gson gson=new Gson();

    public void save(ContentInfo contentInfo) {
        try {
            TeacherContentDashboardIncrementInfo oldInfo=null;
            try {
                if(StringUtils.isNotEmpty(contentInfo.getId()) && StringUtils.isNotEmpty(contentInfo.getTeacherId())){
                    List<String> includeFields = Arrays.asList(ContentInfo.Constants.TEACHER_ID, ContentInfo.Constants.CONTENT_INFO_TYPE, ContentInfo.Constants.CONTENT_STATE);
                    ContentInfo oldContentInfo = getEntityById(contentInfo.getId(), ContentInfo.class, includeFields);
                    if(Objects.nonNull(oldContentInfo) && StringUtils.isNotEmpty(oldContentInfo.getTeacherId())){
                        oldInfo=mapper.map(oldContentInfo,TeacherContentDashboardIncrementInfo.class);
                        oldInfo.setIncrementCount(-1l);
                    }
                }
            }catch (Exception ex){
                logger.error("Exception occured while deducting value {}",ex);
            }
            logger.info("Saving content info for content info id {}", contentInfo.getId());
            Assert.notNull(contentInfo);
            contentInfo.setTags();
            saveEntity(contentInfo);

//            try{
//                cmdsTestAttemptDAO.saveAllTheAttemptsIfNecessary(contentInfo);
//            }catch (Exception ex){
//                logger.error("Error while saving test attempt for content info id ",ex);
//            }

            try {
                if(Objects.nonNull(oldInfo)){
                    awsSNSManager.triggerSNS(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE, SNSSubject.UPDATE_TEACHER_CONTENT_DASHBOARD_INFO.name(),gson.toJson(oldInfo));
                }
                if(StringUtils.isNotEmpty(contentInfo.getId())){
                    if(StringUtils.isNotEmpty(contentInfo.getTeacherId())){
                        TeacherContentDashboardIncrementInfo newInfo=mapper.map(contentInfo,TeacherContentDashboardIncrementInfo.class);
                        newInfo.setIncrementCount(1l);
                        awsSNSManager.triggerSNS(SNSTopic.TEACHER_CONTENT_DASHBOARD_UPDATE,SNSSubject.UPDATE_TEACHER_CONTENT_DASHBOARD_INFO.name(),gson.toJson(newInfo));
                    }
                }
            }catch (Exception ex){
                logger.error("Exception occured while adding value {}",ex);
            }
            logger.info("Saved content info for content info id {}", contentInfo.getId());
        } catch (Exception ex) {
            throw new RuntimeException(
                    "ContentInfoUpdateError : Error updating the content info " + contentInfo.toString(), ex);
        }
    }

    public ContentInfo getById(String id) {
        ContentInfo contentInfo = null;
        try {
            Assert.hasText(id);
            contentInfo = getEntityById(id, ContentInfo.class);
        } catch (Exception ex) {
            throw new RuntimeException("ContentInfoFetchError : Error fetch the content info with id " + id, ex);
        }
        return contentInfo;
    }

    public List<ContentInfo> getTestContentInfo(GetTestContentReq getTestContentReq) {
        if (getTestContentReq == null) {
            return new ArrayList<>();
        } else {
            Query query = new Query();

            if (getTestContentReq.getUserId() != null) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(getTestContentReq.getUserId().toString()));
            } else {
                return new ArrayList<>();
            }
            if (getTestContentReq.getContentType() != null) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).in(getTestContentReq.getContentType()));
            }
            if (getTestContentReq.getContextId() != null) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(getTestContentReq.getContextId()));
            }else if (ArrayUtils.isNotEmpty(getTestContentReq.getContextIds())) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(getTestContentReq.getContextIds()));
            }

            if (ArrayUtils.isNotEmpty(getTestContentReq.getBoardIds())) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.BOARD_IDS).in(getTestContentReq.getBoardIds()));
            } else if (getTestContentReq.getBoardId() != null) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.BOARD_IDS).is(getTestContentReq.getBoardId()));
            }

            if (!StringUtils.isEmpty(getTestContentReq.getContentQuery())) {
                int max = (getTestContentReq.getContentQuery().length() < ReqLimits.NAME_TYPE_MAX) ? getTestContentReq.getContentQuery().length() : ReqLimits.NAME_TYPE_MAX;
                getTestContentReq.setContentQuery(getTestContentReq.getContentQuery().substring(0, max));
                int length = getTestContentReq.getContentQuery().length();
                int index = length < 30 ? length : 30;
                query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TITLE).regex(getTestContentReq.getContentQuery().substring(0, index), "i"));
            }

            if (getTestContentReq.getContentState() != null) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(getTestContentReq.getContentState()));
            }

            if (getTestContentReq.getSelectedContentInfoType() != null) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(getTestContentReq.getSelectedContentInfoType()));
            }

            if (getTestContentReq.getStartTime() != null && getTestContentReq.getEndTime() != null &&
                    getTestContentReq.getStartTime() < getTestContentReq.getEndTime()) {
                query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(getTestContentReq.getStartTime())
                        .lt(getTestContentReq.getEndTime()));
            }

            if (getTestContentReq.getStart() != null && getTestContentReq.getSize() != null) {
                setFetchParameters(query, getTestContentReq.getStart(), getTestContentReq.getSize());
            }

            query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.CREATION_TIME));

            logger.info("query for testContent: " + query);
            return runQuery(query, ContentInfo.class);
        }
    }

    public List<ContentInfo> getContentInfoForBatches(List<String> batchIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(batchIds));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_TYPE).is(EntityType.OTF));
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfoForTests(String userId, List<String> testIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(testIds));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(userId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfoByTestId(String testId, Long fromTime, Long thruTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where("metadata.testId").is(testId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(fromTime), Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(thruTime));
        query.addCriteria(andCriteria);
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfoByAssignmentId(String testId, Long fromTime, Long thruTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID).is(testId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.ASSIGNMENT));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(fromTime), Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(thruTime));
        query.addCriteria(andCriteria);
        query.with(Sort.by(Direction.DESC, ContentInfo.Constants.CREATION_TIME));
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getTestInfoForBatches(List<String> batchIds, String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(batchIds));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(userId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_TYPE).is(EntityType.OTF));
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getTestInfoForCoursePlan(List<String> coursePlanIds, String userId, int start, int size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(coursePlanIds));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(userId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_TYPE).is(EntityType.COURSE_PLAN));
        setFetchParameters(query, start, size);
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getLatestSharedAssignments(String studentId, int size) throws BadRequestException {
        if (StringUtils.isEmpty(studentId)) {
            throwInvalidOrMissingParameterException();
        }
        if (size <= 0) {
            size = 20;
        }
    	logger.info("ContentInfoDAO getLatestSharedAssignments {} : ",studentId);
    	List<Object> assignments = new ArrayList<>();
        assignments.add(ContentType.ASSIGNMENT.toString());
        assignments.add(ContentType.ASSIGNMENT_PDF.toString());
        assignments.add(ContentType.TEST.toString());

        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants._ID));
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).exists(true));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).in(assignments));
        query.addCriteria(Criteria.where(ContentInfo.Constants.DURATION).exists(false));
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_DURATION).exists(false));

        query.fields().include(ContentInfo.Constants.CONTENT_TITLE);
        query.fields().include(ContentInfo.Constants.CONTEXT_ID);
        query.fields().include(ContentInfo.Constants.BOARD_IDS);
        query.fields().include(ContentInfo.Constants.CONTENT_TYPE);
        query.fields().include(ContentInfo.Constants.CONTENT_SUB_TYPE);
        query.fields().include(ContentInfo.Constants.STUDENT_ACTION_LINK);
        query.fields().include(ContentInfo.Constants.CREATION_TIME);
        query.fields().include(ContentInfo.Constants.TEST_ID);
        query.fields().include(ContentInfo.Constants.METADATA);

        query.limit(size);
        logger.info("Query for latest assignments {} ",query.toString());
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfosById(List<String> Ids) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants._ID).in(Ids));
        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfosByIdAndType(List<String> Ids, Integer start, Integer size, ContentState contentState) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants._ID).in(Ids));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(contentState));
        setFetchParameters(query, start, size);
        return runQuery(query, ContentInfo.class);
    }

    public void unshareContent(UnshareContentReq req) {
        Query query = new Query();
        if (!StringUtils.isEmpty(req.getContentId())) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.ID).is(req.getContentId()));
        }
        if (req.getContextType() != null && !StringUtils.isEmpty(req.getContextId())) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_TYPE).is(req.getContextType()));
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(req.getContextId()));

        }

        Update update = new Update();
        update.set(ContentInfo.Constants.ENTITY_STATE, EntityState.INACTIVE);
        updateMulti(query, update, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfos(List<String> studentIds, String teacherId, EntityType contextType, String contextId, String testId) {
        Query query = new Query();
        if (CollectionUtils.isNotEmpty(studentIds)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).in(studentIds));
        }
        if (com.vedantu.util.StringUtils.isNotEmpty(teacherId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(teacherId));
        }
        if (com.vedantu.util.StringUtils.isNotEmpty(testId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).is(testId));
        }
        if (contextType != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_TYPE).is(contextType));
        }
        if (com.vedantu.util.StringUtils.isNotEmpty(contextId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(contextId));
        }
        return runQuery(query, ContentInfo.class);

    }

    public List<ContentInfo> getContentInfos(String testId, String lastFetchedId, Integer size) {
        return getContentInfos(testId,lastFetchedId,size,null);
    }
    public List<ContentInfo> getContentInfos(String testId, String lastFetchedId, Integer size,List<String> includeFields) {
        Query query = new Query();
        if (com.vedantu.util.StringUtils.isNotEmpty(testId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).is(testId));
        }
        if (!StringUtils.isEmpty(lastFetchedId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants._ID).gt(new ObjectId(lastFetchedId)));
        }
        query.limit(size);
        return runQuery(query, ContentInfo.class,includeFields);

    }


    public List<ContentInfo> getContentInfosUsingStart(String testId, Integer start, Integer size) {
        Query query = new Query();
        if (com.vedantu.util.StringUtils.isNotEmpty(testId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).is(testId));
        }
        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.LAST_UPDATED));
        setFetchParameters(query, start, size);
        return runQuery(query, ContentInfo.class);

    }


    public List<ContentInfo> getContentInfosForUserAndContextId(String studentId, List<String> testIds, String contextId,List<String> includeFields) {
        Query query = new Query();
        if (com.vedantu.util.StringUtils.isNotEmpty(studentId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId));
        }
        if (CollectionUtils.isNotEmpty(testIds)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).in(testIds));
        }
        if (com.vedantu.util.StringUtils.isNotEmpty(contextId)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(contextId));
        }
        logger.info(query);

        return runQuery(query, ContentInfo.class,includeFields);

    }

    public List<ContentInfo> getEvaluatedAttempts(String testId, String studentId) throws BadRequestException {
        Query query = new Query();
        if (StringUtils.isEmpty(testId) || StringUtils.isEmpty(studentId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "studentId/testId");
        }
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).is(testId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        logger.info("getEvaluatedAttempts - query - {}", query);

        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfosByContextId(List<String> contextId, Long afterCreationTime, Long beforeCreationTime) throws BadRequestException {
        Query query = new Query();
        if (ArrayUtils.isEmpty(contextId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "contextId");
        }
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(contextId));
        if (afterCreationTime != null && beforeCreationTime != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(afterCreationTime).lte(beforeCreationTime));
        } else if (afterCreationTime != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(afterCreationTime));
        } else if (beforeCreationTime != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(beforeCreationTime));
        }
        logger.info(query);

        return runQuery(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfosByFilter(ContentInfoReq req) throws BadRequestException {

        int start = req.getStart() != null ? req.getStart() : 0;
        int size = req.getSize() != null ? req.getSize() : MAX_COUNT;

        Query query = new Query();

        if (req.getContextIds() != null)
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(req.getContextIds()));
        else if (req.getContextId() != null)
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(req.getContextId()));

        if (req.getStartTime() != null && req.getEndTime() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(req.getStartTime()).lte(req.getEndTime()));
        } else if (req.getStartTime() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(req.getStartTime()));
        } else if (req.getEndTime() != null) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(req.getEndTime()));
        }

        if (CollectionUtils.isNotEmpty(req.getFieldsInclude())) {
            req.getFieldsInclude().forEach(query.fields()::include);
        } else if (CollectionUtils.isNotEmpty(req.getFieldsExclude())) {
            req.getFieldsExclude().forEach(query.fields()::exclude);
        }

        query.skip(start);
        query.limit(size);
        logger.info(query);

        return runQuery(query, ContentInfo.class);
    }

    public void updateTeacherId(String teacherId, String testId, String teacherFullName, String lastUpdatedBy) {
        int size = MAX_COUNT;
        int start = 0;

        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).is(testId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).ne(teacherId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).in(Arrays.asList(ContentState.SHARED, ContentState.ATTEMPTED)));

        Long count = queryCount(query, ContentInfo.class);
        logger.info("count of tests " + count);
        while (start < count) {

            setFetchParameters(query, start, size);
            Update update = new Update();
            update.set(ContentInfo.Constants.TEACHER_ID, teacherId);
            if (!StringUtils.isEmpty(teacherFullName)) {
                update.set(ContentInfo.Constants.TEACHER_FULL_NAME, teacherFullName);
            }
            update.set(ContentInfo.Constants.LAST_UPDATED_BY, lastUpdatedBy);
            update.set(ContentInfo.Constants.LAST_UPDATED, System.currentTimeMillis());
            updateMulti(query, update, ContentInfo.class);
            start = start + size;
            logger.info("start is : " + start);
            if (start > count) {
                break;
            }
        }
    }

    public List<String> getTestIdsForQuestion(List<String> testIds, Long time, Boolean beforeTime, int skip, int limit) {

        Query query=new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).in(Arrays.asList(ContentInfoType.OBJECTIVE, ContentInfoType.MIXED)));
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).in(testIds));

        if (beforeTime) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).lte(time));
        } else {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CREATION_TIME).gte(time));
        }

        query.skip(skip);
        query.limit(limit);

        query.fields().include(ContentInfo.Constants.METADATA_TESTID);

        return Optional.ofNullable(runQuery(query,ContentInfo.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(ContentInfo::getMetadata)
                        .map(TestContentInfoMetadata::getTestId)
                        .collect(Collectors.toList());

    }

    public List<ContentInfo> getContentInfosForPhaseTests(List<String> batchIds, List<String> testIds, int start, int size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).in(testIds));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        if (ArrayUtils.isNotEmpty(batchIds)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).in(batchIds));
        }
        setFetchParameters(query, start, size);

        return runQuery(query, ContentInfo.class);

    }

    public List<ContentInfo> getLastThreeAssignmentsOfStudent(Long studentId) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        q.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_DURATION).is(0));
        q.with(Sort.by(Direction.DESC, ContentInfo.Constants.CREATION_TIME));
        q.limit(3);
        return runQuery(q, ContentInfo.class);
    }

    public List<ContentInfo> getTestsOfStudentAfter(Long studentId, Long creationTime) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId.toString()));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST));
        q.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_DURATION).gt(0));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
        q.addCriteria(Criteria.where(ContentInfo.Constants.LAST_UPDATED).gte(creationTime));
        logger.info("Query: " + q);
        return runQuery(q, ContentInfo.class);
    }

    public Long getTestInfoByUserId(String userId, Long releaseDate) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(userId))
                .addCriteria(Criteria.where(ContentInfo.Constants.LAST_UPDATED).gte(releaseDate))
                .addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(ContentType.TEST))
                .addCriteria(Criteria.where(ContentInfo.Constants.METADATA).exists(true).ne(null));

        return queryCount(query, ContentInfo.class);
    }

    public List<ContentInfo> getAttemptedContentInfosForTeacher(String teacherId, long fromTime, long toTime,
                                                                int fetchSize, List<String> includeFields) {
        Query q = new Query();
        q.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(teacherId));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.ATTEMPTED));
        if (ConfigUtils.INSTANCE.getStringValue("DEMO_STUDENT_ID") != null) {
            q.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID)
                    .ne(ConfigUtils.INSTANCE.getStringValue("DEMO_STUDENT_ID")));
        }
        q.addCriteria(Criteria.where(ContentInfo.Constants.LAST_UPDATED).gte(fromTime).lt(toTime));
        q.limit(fetchSize);

        return runQuery(q, ContentInfo.class, includeFields);
    }

    @Deprecated
    public long getContentInfosCount(String teacherId, ContentInfoType contentInfoType, ContentState contentState) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(teacherId));

        if (Objects.nonNull(contentState)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(contentState));
        }

        if (Objects.nonNull(contentInfoType)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(contentInfoType));
        }
        return queryCount(query, ContentInfo.class);
    }

    @Deprecated
    public ContentInfo getOneContentInfo(String teacherId, ContentInfoType contentInfoType, ContentState contentState) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.TEACHER_ID).is(teacherId));

        if (Objects.nonNull(contentState)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(contentState));
        }

        if (Objects.nonNull(contentInfoType)) {
            query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_INFO_TYPE).is(contentInfoType));
        }
        return findOne(query, ContentInfo.class);
    }

    public long getTestInfosCountForStudent(Long boardId, String studentId, String contextId, ContentType contentType, ContentState contentState) {
        Query q = new Query();

        q.addCriteria(Criteria.where(ContentInfo.Constants.BOARD_ID).is(boardId));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTEXT_ID).is(contextId));
        q.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_TYPE).is(contentType));
        q.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(contentState));

        return queryCount(q, ContentInfo.class);
    }

    public ContentInfo getLatestContentInfoForTestForStudent(String studentId, String testId, List<String> includeFields) throws BadRequestException {

        if(StringUtils.isEmpty(studentId) || StringUtils.isEmpty(testId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Either studentId or testId is empty, not possible, look into it", Level.ERROR);
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(ContentInfo.Constants.METADATA_TESTID).is(testId));
        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);
        return findOne(query, ContentInfo.class);
    }

    public List<ContentInfo> getContentInfosForMigration(long skip, int limit) {

        Query query=new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants.LAST_UPDATED_BY).ne("CMDSTestAttempt_Migration"));

        query.skip(skip);
        query.limit(limit);

        return runQuery(query,ContentInfo.class);
    }

    public void markContentInfosMigrated(List<String> ids) {
        Query query=new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants._ID).in(ids));

        Update update=new Update();
        update.set(ContentInfo.Constants.LAST_UPDATED_BY,"CMDSTestAttempt_Migration");

        updateMulti(query,update,ContentInfo.class);

    }

    public void setUpMigrationChange(List<String> contentInfoIds, long creationTime) {
        Query query=new Query();
        query.addCriteria(Criteria.where(ContentInfo.Constants._ID).in(contentInfoIds));

        Update update=new Update();
        update.set(ContentInfo.Constants.CREATION_TIME,creationTime);
        update.set("MIGRATION_FIELD","ASSIGNMENT_MIGRATION");

        updateMulti(query,update,ContentInfo.class);
    }

//    public List<ContentInfo> getContentInfosForMigration(long skip, int limit) {
//
//        Query query=new Query();
//        query.addCriteria(Criteria.where("MIGRATION_FIELD").is(null));
//
//        query.skip(skip);
//        query.limit(limit);
//
//        return runQuery(query,ContentInfo.class);
//    }
//
//    public void markContentInfosMigrated(List<String> ids) {
//        Query query=new Query();
//        query.addCriteria(Criteria.where(ContentInfo.Constants._ID).in(ids));
//
//        Update update=new Update();
//        update.set("MIGRATION_FIELD","CMDSTestAttempt_Migration");
//
//        updateMulti(query,update,ContentInfo.class);
//
//    }

    public List<ContentInfo> getUpcomingTestsForStudent(String studentId, Long tillTime, int limit, List<String> includeFields) {
        Query q = new Query();

        q.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        q.addCriteria(Criteria.where(CONTENT_TYPE).is(ContentType.TEST.name()));

        q.addCriteria(Criteria.where(METADATA_DURATION).gt(0));
        q.addCriteria(Criteria.where(METADATA_MIN_START_TIME).lt(tillTime).gt(System.currentTimeMillis()));

        q.with(Sort.by(Sort.Direction.ASC, ContentInfo.Constants.METADATA_MIN_START_TIME));
        q.limit(limit);

        Optional.ofNullable(includeFields).orElseGet(ArrayList::new).forEach(q.fields()::include);
        logger.info("getUpcomingTestsForStudent query-- {}", q);

        return runQuery(q, ContentInfo.class);
    }

    public List<ContentInfo> getPastLiveOrUnattemptedTests(String studentId, int limit, List<String> includeFields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(CONTENT_TYPE).is(ContentType.TEST.name()));
        query.addCriteria(Criteria.where(METADATA_DURATION).gt(0));
        query.addCriteria(Criteria.where(CONTENT_STATE).is(ContentState.SHARED));

        Criteria orConditionCriteria = new Criteria();
        orConditionCriteria.orOperator(Criteria.where(METADATA_MIN_START_TIME).exists(false),
                Criteria.where(METADATA_MIN_START_TIME).lt(System.currentTimeMillis()));
        query.addCriteria(orConditionCriteria);

        query.with(Sort.by(Sort.Direction.DESC, ContentInfo.Constants.CREATION_TIME));
        query.limit(limit);

        Optional.ofNullable(includeFields).orElseGet(ArrayList::new).forEach(query.fields()::include);
        logger.info("getUnattemptedPastTests query-- {}", query);

        return runQuery(query, ContentInfo.class);
    }


    public List<ContentInfo> getLiveTestsForStudent(String studentId, int limit, List<String> includeFields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(METADATA_DURATION).gt(0));
        query.addCriteria(Criteria.where(CONTENT_STATE).is(ContentState.SHARED));
        query.addCriteria(Criteria.where(CONTENT_TYPE).is(ContentType.TEST.name()));

        query.addCriteria(Criteria.where(METADATA_MIN_START_TIME).lt(System.currentTimeMillis()));
        query.with(Sort.by(Sort.Direction.DESC, METADATA_MIN_START_TIME));

        Optional.ofNullable(includeFields).orElseGet(ArrayList::new).forEach(query.fields()::include);
        query.limit(limit);
        logger.info("getLiveTestsForStudent query-- {}", query);
        return runQuery(query, ContentInfo.class);
    }


        @Deprecated
        public List<HomepageTestResponse> getLiveTestsForStudentOld(String studentId, long limit) {

            Criteria matchCriteria = new Criteria();
            matchCriteria.andOperator(
                    Criteria.where(STUDENT_ID).is(studentId),
                    Criteria.where(CONTENT_TYPE).is(ContentType.TEST.toString()),
                    Criteria.where(METADATA_DURATION).gt(0),
                    Criteria.where(METADATA_MIN_START_TIME).lt(System.currentTimeMillis())
            );

            ProjectionOperation projections = Aggregation.project()
                    .and(_ID).as("id").and(METADATA_DURATION).as("duration")
                    .and(CONTENT_LINK).as("joiningLink").and(METADATA_TESTID).as("testId")
                    .and(CONTENT_TITLE).as("title").and(BOARD_IDS).as("boardIds")
                    .and(SUBJECTS).as("subjects")
                    .and(METADATA_MIN_START_TIME).as("minStartTime")
                    .and(METADATA_MAX_START_TIME).as("attemptBy")
                    .and(METADATA_EXPIRY_DATE).as("expiryDate")
                    .and(ArrayOperators.ArrayElemAt.arrayOf(METADATA_TESTATTEMPTS).elementAt(0)).as("firstAttempt");

            Aggregation aggregation = Aggregation.newAggregation(
                    Aggregation.match(matchCriteria),
                    Aggregation.sort(Direction.DESC, METADATA_MIN_START_TIME),
                    projections,
                    Aggregation.limit(limit)
            );

            logger.info("getLiveTestsForStudent query-- {}", aggregation);
            AggregationResults<HomepageTestResponse> result = getMongoOperations().aggregate(aggregation,
                    ContentInfo.class.getSimpleName(), HomepageTestResponse.class);

            return result.getMappedResults();

        }
}
