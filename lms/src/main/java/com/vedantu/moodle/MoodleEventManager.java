package com.vedantu.moodle;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.internal.LinkedTreeMap;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.dao.GroupDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.entity.Group;
import com.vedantu.moodle.pojo.ContentInfoCommunicationType;
import com.vedantu.moodle.pojo.ResourceAttempted;
import com.vedantu.moodle.pojo.ResourceCreated;
import com.vedantu.moodle.pojo.ResourceDeleted;
import com.vedantu.moodle.pojo.ResourceEvaluated;
import com.vedantu.moodle.pojo.ResourceUpdated;
import com.vedantu.moodle.pojo.response.BasicRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.util.HashSet;

@Service
public class MoodleEventManager {

	@Autowired
	private ContentInfoDAO contentInfoDAO;

	@Autowired
	private ContentInfoManager contentInfoManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MoodleEventManager.class);

	@Autowired
	private GroupDAO groupDAO;

	private String moodleBaseUrl;

	private static Gson gson = new Gson();

	@PostConstruct
	public void init() {
		moodleBaseUrl = ConfigUtils.INSTANCE.getStringValue("MOODLE_SERVER_URL");
	}

	public BasicRes resourceCreated(String request) {
		logger.info("request for create " + request);

		BasicRes basicRes = new BasicRes();
		if (request.charAt(request.length() - 1) == '=') {
			// logger.error("moodle data format wrong for creation" + request);
			request = request.substring(0, request.length() - 1);

		}
		ResourceCreated resourceCreated;
		try {
			resourceCreated = new Gson().fromJson(request, ResourceCreated.class);
		} catch (Exception e) {
			logger.info("content not shared with student creation , parsing failure " + request);
			basicRes.setSuccess(false);
			return basicRes;
		}
		Group group = groupDAO.getById(resourceCreated.getCourse_data().getShortname().split("-")[0]);
		ContentInfo contentInfo = createContentInfo(resourceCreated, group);
		try {
			contentInfo.setContentInfoType(GetContentInfoType(request, contentInfo.getContentType()));
		} catch (Exception e) {
			logger.info("could not get content type :" + e.getMessage());
		}
		if (group != null && resourceCreated.getCourse_data().getShortname().split("-").length != 3) {
			for (UserBasicInfo Student : group.getEnrolledStudents()) {
				contentInfo.setStudentFullName(Student.getFirstName() + " " + Student.getLastName());
				contentInfo.setStudentId(String.valueOf(Student.getUserId()));
				contentInfo.setId(null);
				contentInfoDAO.save(contentInfo);
				if (!StringUtils.isEmpty(contentInfo.getStudentId())) {
					logger.info("contentInfo id to send notification:" + contentInfo.getId());
//					contentInfoManager.sendContentInfoNotifications(contentInfo.getId(),
//							ContentInfoCommunicationType.SHARED);
				}
			}
		} else {
			contentInfo.setId(null);
			contentInfoDAO.save(contentInfo);
			if (!StringUtils.isEmpty(contentInfo.getStudentId())) {
//				contentInfoManager.sendContentInfoNotifications(contentInfo.getId(),
//						ContentInfoCommunicationType.SHARED);
			}
		}

		basicRes.setSuccess(true);
		return basicRes;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private ContentInfoType GetContentInfoType(String request, ContentType contentType) {
		JsonElement element = null;
		try {
			JsonParser parser = new JsonParser();
			JsonObject jsonObject = parser.parse(request).getAsJsonObject();
			element = jsonObject.get("question_data");
		} catch (Exception ex) {
			logger.info(" getting question data wrong :" + ex);
			return ContentInfoType.SUBJECTIVE;
		}

		int count = 0;
		int essaycount = 0;
		Map<String, LinkedTreeMap> map = new HashMap<String, LinkedTreeMap>();
		try {
			map = gson.fromJson(element.toString(), map.getClass());

			for (Map.Entry<String, LinkedTreeMap> entry : map.entrySet()) {
				LinkedTreeMap value = entry.getValue();
				++count;
				if ("essay".equals(value.get("qtype"))) {
					++essaycount;
				}
			}

		} catch (Exception e) {
			logger.info("could not get question data : " + request);
		}
		logger.info("count is " + count);
		logger.info("essay count " + essaycount);
		if (essaycount == 0 && count == 0 && contentType.equals(ContentType.NOTES))
			return ContentInfoType.NOTES;
		else if (essaycount == 0 && count == 0 && contentType.equals(ContentType.ASSIGNMENT))
			return ContentInfoType.ASSIGNMENT;
		else if (essaycount == 0 && count == 0)
			return ContentInfoType.SUBJECTIVE;
		else if (essaycount == 0)
			return ContentInfoType.OBJECTIVE;
		else if (essaycount == count && essaycount > 0)
			return ContentInfoType.SUBJECTIVE;
		else
			return ContentInfoType.MIXED;

	}


	public BasicRes resourceUpdated(String request) {
		logger.info("request for update " + request);
		BasicRes basicRes = new BasicRes();
		if (request.charAt(request.length() - 1) == '=') {
			// logger.error("moodle data format wrong for updation" + request);
			request = request.substring(0, request.length() - 1);
		}
		ResourceUpdated resourceUpdated;
		try {
			resourceUpdated = new Gson().fromJson(request, ResourceUpdated.class);
		} catch (Exception e) {
			logger.info("content not shared with student updation , parsing failure " + request);
			basicRes.setSuccess(false);
			return basicRes;
		}
		Group group = groupDAO.getById(resourceUpdated.getCourse_data().getShortname().split("-")[0]);
		ContentInfo contentInfo = new ContentInfo();
		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID).is(resourceUpdated.getModule_data().getId()));
		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);

		if (contentInfos.size() > 0) {
			contentInfo = contentInfos.get(0);
		} else {
			logger.info("No content found to update : " + resourceUpdated.toString());
			contentInfo.setId(null);
		}

		contentInfo.setCourseName(resourceUpdated.getCourse_data().getFullname());
		contentInfo.setTestId(resourceUpdated.getModule_data().getId());
		if (resourceUpdated.getTeacher_data() != null) {
			contentInfo.setTeacherFullName(resourceUpdated.getTeacher_data().getFirstname() + " "
					+ resourceUpdated.getTeacher_data().getLastname());
		}
		if (resourceUpdated.getAssign_data() != null)
			contentInfo.setContentTitle(resourceUpdated.getAssign_data().getName());
		else if (resourceUpdated.getQuiz_data() != null)
			contentInfo.setContentTitle(resourceUpdated.getQuiz_data().getName());
		else if (resourceUpdated.getResource_data() != null)
			contentInfo.setContentTitle(resourceUpdated.getResource_data().getName());

		contentInfo.setCourseId(resourceUpdated.getCourse_data().getId());

		// otf check
		if (group == null) {
			contentInfo.setSubject(resourceUpdated.getCourse_data().getFullname().split("-")[1]);
                        contentInfo.setSubjects(new HashSet<>());
                        contentInfo.getSubjects().add(contentInfo.getSubject());
		} else {
			List<String> subjects = group.getSubjects();
			if (subjects != null && !subjects.isEmpty()) {
				contentInfo.setSubject(subjects.get(0));
                                contentInfo.setSubjects(new HashSet<>(group.getSubjects()));
			} else {
				contentInfo.setSubject(resourceUpdated.getCourse_data().getFullname().split("-")[1]);
                                contentInfo.setSubjects(new HashSet<>());
                                contentInfo.getSubjects().add(contentInfo.getSubject());                                
			}
		}

		if (resourceUpdated.getQuiz_data() != null)
			contentInfo.setDuration(Long.parseLong(resourceUpdated.getQuiz_data().getTimelimit()) * 1000);
		if (resourceUpdated.getAssign_data() != null)
			contentInfo.setExpiryTime(Long.parseLong(resourceUpdated.getAssign_data().getDuedate()) * 1000);
		else if (resourceUpdated.getQuiz_data() != null)
			contentInfo.setExpiryTime(Long.parseLong(resourceUpdated.getQuiz_data().getTimeclose()) * 1000);

		// check below -total marks for quiz
		if (resourceUpdated.getAssign_data() != null)
			contentInfo.setTotalMarks(Float.parseFloat(resourceUpdated.getAssign_data().getGrade()));
		else if (resourceUpdated.getQuiz_data() != null)
			contentInfo.setTotalMarks(Float.parseFloat(resourceUpdated.getQuiz_data().getSumgrades()));
		if (resourceUpdated.getAssign_data() != null) {
			contentInfo.setContentType(ContentType.ASSIGNMENT);
			contentInfo.setContentInfoType(ContentInfoType.ASSIGNMENT);
		} else if (resourceUpdated.getQuiz_data() != null)
			contentInfo.setContentType(ContentType.TEST);
		else if (resourceUpdated.getResource_data() != null) {
			contentInfo.setContentType(ContentType.NOTES);
			contentInfo.setContentInfoType(ContentInfoType.NOTES);
		}
		// otf check????
		if (resourceUpdated.getCourse_data().getFullname().split("-").length == 4 && group == null) {
			contentInfo.setStudentFullName(resourceUpdated.getCourse_data().getFullname().split("-")[0]);
			contentInfo.setStudentId(resourceUpdated.getCourse_data().getShortname().split("-")[0]);
			contentInfo.setTeacherId(resourceUpdated.getCourse_data().getShortname().split("-")[2]);
			contentInfo.setEngagementType(
					EngagementType.valueOf(resourceUpdated.getCourse_data().getFullname().split("-")[3]));
		} else if (resourceUpdated.getCourse_data().getFullname().split("-").length == 2 && group != null) {
			contentInfo.setTeacherId(resourceUpdated.getCourse_data().getShortname().split("-")[0]);
			contentInfo.setEngagementType(group.getEngagementType());
		}

		if (resourceUpdated.getAssign_data() != null)
			contentInfo.setContentLink(
					moodleBaseUrl + "/mod/" + "assign" + "/view.php?id=" + resourceUpdated.getModule_data().getId());
		else if (resourceUpdated.getQuiz_data() != null)
			contentInfo.setContentLink(
					moodleBaseUrl + "/mod/" + "quiz" + "/view.php?id=" + resourceUpdated.getModule_data().getId());
		else if (resourceUpdated.getResource_data() != null)
			contentInfo.setContentLink(
					moodleBaseUrl + "/mod/" + "resource" + "/view.php?id=" + resourceUpdated.getModule_data().getId());

		try {
			contentInfo.setContentInfoType(GetContentInfoType(request, contentInfo.getContentType()));
		} catch (Exception e) {
			logger.info("could not get content type :" + e.getMessage());
		}
		contentInfo.setTeacherActionLink(contentInfo.getContentLink());
		contentInfo.setStudentActionLink(contentInfo.getContentLink());

		if (group != null) {
			contentInfo.setTeacherId(String.valueOf(group.getTeachers().get(0).getUserId()));
			contentInfo.setEngagementType(EngagementType.OTF);
			contentInfo.setContentState(ContentState.SHARED);

			if (resourceUpdated.getCourse_data().getFullname().split("-").length != 3) {
				for (UserBasicInfo Student : group.getEnrolledStudents()) {
					contentInfo.setStudentFullName(Student.getFirstName() + " " + Student.getLastName());
					contentInfo.setStudentId(String.valueOf(Student.getUserId()));
					contentInfo.setId(null);
					if (contentInfos.size() > 0) {
						for (ContentInfo contentInfoInGroup : contentInfos) {
							if (contentInfoInGroup.getStudentId().equals(String.valueOf(Student.getUserId()))) {
								contentInfo.setId(contentInfoInGroup.getId());
								contentInfo.setMarksAcheived(contentInfoInGroup.getMarksAcheived());
								contentInfo.setAttemptedTime(contentInfoInGroup.getAttemptedTime());
								contentInfo.setEvaulatedTime(contentInfoInGroup.getEvaulatedTime());
							}
						}
					}

					contentInfoDAO.save(contentInfo);
					if (!StringUtils.isEmpty(contentInfo.getStudentId())) {
						logger.info("contentInfo id to send notification:" + contentInfo.getId());
//						contentInfoManager.sendContentInfoNotifications(contentInfo.getId(),
//								ContentInfoCommunicationType.SHARED);
					}
				}
			} else {
				contentInfo.setId(null);
				contentInfoDAO.save(contentInfo);

			}
		} else {
			ContentState contentState = contentInfo.getContentState();
			if (contentState == null || ContentState.DRAFT.equals(contentState)) {
				contentInfo.setContentState(ContentState.SHARED);
			}

			if (ContentState.EXPIRED.equals(contentInfo) && contentInfo.getExpiryTime() != null
					&& contentInfo.getExpiryTime() > System.currentTimeMillis()) {
				contentInfo.setContentState(ContentState.SHARED);
			}
			contentInfoDAO.save(contentInfo);
			if (!StringUtils.isEmpty(contentInfo.getStudentId())) {
//				contentInfoManager.sendContentInfoNotifications(contentInfo.getId(),
//						ContentInfoCommunicationType.SHARED);
			}

		}
		basicRes.setSuccess(true);
		return basicRes;

	}

	// http://vedantu.skoolbox.in/mod/assign/view.php?id=38&action=grading
	public BasicRes resourceEvaluated(String request) {
		logger.info("request for evaluate " + request);
		BasicRes basicRes = new BasicRes();
		if (request.charAt(request.length() - 1) == '=') {
			// logger.error("moodle data format wrong for evaluation" +
			// request);
			request = request.substring(0, request.length() - 1);

		}
		ResourceEvaluated resourceEvaluated;
		try {
			resourceEvaluated = new Gson().fromJson(request, ResourceEvaluated.class);
		} catch (Exception e) {
			logger.info("content not evaluated with student evaluation , parsing failure " + request);
			basicRes.setSuccess(false);
			return basicRes;
		}
		logger.info("resource evaluated :v" + resourceEvaluated.toString());
		// Group group =
		// groupDAO.getById(resourceEvaluated.getCourse_data().getShortname().split("-")[0]);

		ContentInfo contentInfo = new ContentInfo();
		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID).is(resourceEvaluated.getModule_data().getId()));
		query.addCriteria(
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(resourceEvaluated.getStudent_data().getUsername()));

		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
		if (contentInfos.size() > 0) {
			contentInfo = contentInfos.get(0);
		} else {
			logger.info("No content found for evaluate event : " + request);
			return new BasicRes();
		}

		if (resourceEvaluated.getAssign_data() != null) {
			contentInfo.setMarksAcheived(
					new Float(Math.round(Float.parseFloat(resourceEvaluated.getAssigngrade_data().getGrade()))));
		} else if (resourceEvaluated.getUsergrade_data() != null
				&& resourceEvaluated.getUsergrade_data().getRawgrade() != null) {
			contentInfo.setMarksAcheived(new Float(Math.round(
					Float.parseFloat(resourceEvaluated.getUsergrade_data().getRawgrade()) * contentInfo.getTotalMarks()
							/ Float.parseFloat(resourceEvaluated.getUsergrade_data().getRawgrademax()))));
		}
		contentInfo.setContentState(ContentState.EVALUATED);

		// http://vedantu.skoolbox.in/mod/assign/review.php?attempt=52

		if (resourceEvaluated.getUsergrade_data() == null) {
			contentInfo.setTeacherActionLink(contentInfo.getContentLink());
			contentInfo.setStudentActionLink(contentInfo.getContentLink());
		}

		// http://vedantu.skoolbox.in/mod/quiz/review.php?attempt=52
		Long attemptedTime = contentInfo.getAttemptedTime();
		Long currentTime = System.currentTimeMillis();
		if (attemptedTime == null || attemptedTime <= 0) {
			contentInfo.setAttemptedTime(currentTime);
		}

		contentInfoManager.sendContentInfoNotifications(contentInfo.getId(), ContentInfoCommunicationType.EVALUATED);
		contentInfo.setEvaulatedTime(currentTime);
		if (resourceEvaluated.getCourse_data().getFullname().split("-").length != 3)
			contentInfoDAO.save(contentInfo);
		basicRes.setSuccess(true);
		return basicRes;
	}

	public BasicRes resourceAttempted(String request) {
		logger.info("request for attempted " + request);
		BasicRes basicRes = new BasicRes();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		if (request.charAt(request.length() - 1) == '=') {
			// logger.error("moodle data format wrong for attempt" + request);
			request = request.substring(0, request.length() - 1);

		}
		ResourceAttempted resourceAttempted;
		try {
			resourceAttempted = new Gson().fromJson(request, ResourceAttempted.class);
		} catch (Exception e) {
			logger.info("content not shared with student attempt , parsing failure " + request);
			basicRes.setSuccess(false);
			return basicRes;

		}
		// logger.info("resource attempted :v" + resourceAttempted.toString());
		// Group group =
		// groupDAO.getById(resourceAttempted.getCourse_data().getShortname().split("-")[0]);
		ContentInfo contentInfo = new ContentInfo();

		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID).is(resourceAttempted.getModule_data().getId()));
		query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.SHARED));
		query.addCriteria(
				Criteria.where(ContentInfo.Constants.STUDENT_ID).is(resourceAttempted.getStudent_data().getUsername()));
		logger.info("student id " + resourceAttempted.getStudent_data().getId());
		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
		if (contentInfos.size() > 0 && contentInfos.get(0).getContentState().equals(ContentState.SHARED)) {
			contentInfo = contentInfos.get(0);
			logger.info("more than one " + contentInfo.toString());
		} else {

			Query objectiveQuery = new Query();
			objectiveQuery.addCriteria(
					Criteria.where(ContentInfo.Constants.TEST_ID).is(resourceAttempted.getModule_data().getId()));
			objectiveQuery.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
			objectiveQuery.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID)
					.is(resourceAttempted.getStudent_data().getUsername()));

			List<ContentInfo> objectiveContentInfos = contentInfoDAO.runQuery(objectiveQuery, ContentInfo.class);
			if (objectiveContentInfos.size() > 0) {
				objectiveContentInfos.get(0).setStudentActionLink(moodleBaseUrl + "/mod/quiz/review.php?attempt="
						+ resourceAttempted.getEvent_data().getObjectid());
				objectiveContentInfos.get(0).setTeacherActionLink(moodleBaseUrl + "/mod/quiz/review.php?attempt="
						+ resourceAttempted.getEvent_data().getObjectid());
				objectiveContentInfos.get(0).setAttemptedTime(System.currentTimeMillis());
				if (resourceAttempted.getCourse_data().getFullname().split("-").length != 3)
					contentInfoDAO.save(objectiveContentInfos.get(0));
				logger.info("attempt after evaluate");
//				contentInfoManager.sendContentInfoNotifications(objectiveContentInfos.get(0).getId(),
//						ContentInfoCommunicationType.ATTEMPTED);
			}
			logger.info("No content found for attempt event : " + resourceAttempted.toString());
			return new BasicRes();
		}

		contentInfo.setContentState(ContentState.ATTEMPTED);
		contentInfo.setTeacherActionLink(contentInfo.getContentLink() + "&action=grading");
		if (resourceAttempted.getQuizattempt_data() != null) {
			contentInfo.setTeacherActionLink(
					moodleBaseUrl + "/mod/quiz/review.php?attempt=" + resourceAttempted.getEvent_data().getObjectid());
			contentInfo.setStudentActionLink(
					moodleBaseUrl + "/mod/quiz/review.php?attempt=" + resourceAttempted.getEvent_data().getObjectid());

		}
		contentInfo.setAttemptedTime(System.currentTimeMillis());

		if (resourceAttempted.getCourse_data().getFullname().split("-").length != 3)
			contentInfoDAO.save(contentInfo);
//		contentInfoManager.sendContentInfoNotifications(contentInfo.getId(), ContentInfoCommunicationType.ATTEMPTED);
		basicRes.setSuccess(true);
		return basicRes;
	}

	public BasicRes resourceDeleted(String request) {
		logger.info("resourceDeleted : " + request);

		ResourceDeleted resourceDeleted = new Gson().fromJson(request, ResourceDeleted.class);
		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID)
				.is(resourceDeleted.getEvent_data().getContextinstanceid()));
		query.addCriteria(
				Criteria.where(ContentInfo.Constants.COURSE_NAME).is(resourceDeleted.getCourse_data().getFullname()));

		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
		for (ContentInfo contentInfo : contentInfos) {
			contentInfoDAO.deleteEntityById(contentInfo.getId(), ContentInfo.class);
		}

		return new BasicRes();
	}

	public BasicRes display(String request) {
		// TODO Auto-generated method stub
		logger.info("event json from moodle side " + request);
		BasicRes basicRes = new BasicRes();
		basicRes.setSuccess(true);
		return basicRes;

	}

	public ContentInfo createContentInfo(ResourceCreated resourceCreated, Group group) {
		ContentInfo contentInfo = new ContentInfo();
		contentInfo.setCourseName(resourceCreated.getCourse_data().getFullname());
		contentInfo.setTestId(resourceCreated.getModule_data().getId());
		if (resourceCreated.getTeacher_data() != null) {
			contentInfo.setTeacherFullName(resourceCreated.getTeacher_data().getFirstname() + " "
					+ resourceCreated.getTeacher_data().getLastname());
		}
		if (resourceCreated.getAssign_data() != null) {
			contentInfo.setContentTitle(resourceCreated.getAssign_data().getName());
		} else if (resourceCreated.getQuiz_data() != null) {
			contentInfo.setContentTitle(resourceCreated.getQuiz_data().getName());
		} else if (resourceCreated.getResource_data() != null) {
			contentInfo.setContentTitle(resourceCreated.getResource_data().getName());
		}
		contentInfo.setCourseId(resourceCreated.getCourse_data().getId());

		// OTF check
		if (group == null) {
			contentInfo.setSubject(resourceCreated.getCourse_data().getFullname().split("-")[1]);
		} else {
			List<String> subjects = group.getSubjects();
			if (subjects != null && !subjects.isEmpty()) {
				contentInfo.setSubject(subjects.get(0));
                                contentInfo.setSubjects(new HashSet<>(subjects));
			} else {
				contentInfo.setSubject(resourceCreated.getCourse_data().getFullname().split("-")[1]);
                                contentInfo.setSubjects(new HashSet<>());
                                contentInfo.getSubjects().add(contentInfo.getSubject());
			}
		}

		if (resourceCreated.getQuiz_data() != null)
			contentInfo.setDuration(Long.parseLong(resourceCreated.getQuiz_data().getTimelimit()) * 1000);
		if (resourceCreated.getAssign_data() != null)
			contentInfo.setExpiryTime(Long.parseLong(resourceCreated.getAssign_data().getDuedate()) * 1000);
		else if (resourceCreated.getQuiz_data() != null)
			contentInfo.setExpiryTime(Long.parseLong(resourceCreated.getQuiz_data().getTimeclose()) * 1000);

		// check below -total marks for quiz
		if (resourceCreated.getAssign_data() != null)
			contentInfo.setTotalMarks(Float.parseFloat(resourceCreated.getAssign_data().getGrade()));
		else if (resourceCreated.getQuiz_data() != null)
			contentInfo.setTotalMarks(Float.parseFloat(resourceCreated.getQuiz_data().getGrade()));
		if (resourceCreated.getAssign_data() != null) {
			contentInfo.setContentType(ContentType.ASSIGNMENT);
			contentInfo.setContentInfoType(ContentInfoType.ASSIGNMENT);
		} else if (resourceCreated.getQuiz_data() != null) {
			contentInfo.setContentType(ContentType.TEST);
		} else if (resourceCreated.getResource_data() != null) {
			contentInfo.setContentType(ContentType.NOTES);
			contentInfo.setContentInfoType(ContentInfoType.NOTES);
		}
		// otf check????
		if (resourceCreated.getCourse_data().getFullname().split("-").length == 4 && group == null) {
			contentInfo.setStudentFullName(resourceCreated.getCourse_data().getFullname().split("-")[0]);
			contentInfo.setStudentId(resourceCreated.getCourse_data().getShortname().split("-")[0]);
			contentInfo.setTeacherId(resourceCreated.getCourse_data().getShortname().split("-")[2]);
			contentInfo.setEngagementType(
					EngagementType.valueOf(resourceCreated.getCourse_data().getFullname().split("-")[3]));
			// contentInfo.setContentState(ContentState.DRAFT);
			contentInfo.setContentState(ContentState.SHARED);
		} else if (resourceCreated.getCourse_data().getFullname().split("-").length == 2) {
			if (group == null) {
				contentInfo.setTeacherId(resourceCreated.getCourse_data().getShortname().split("-")[0]);
				contentInfo.setContentState(ContentState.DRAFT);
				contentInfo.setEngagementType(EngagementType.OTO);
			} else {
				if (group.getTeachers() != null && group.getTeachers().size() > 0) {
					contentInfo.setTeacherId(String.valueOf(group.getTeachers().get(0).getUserId()));
				}
				contentInfo.setContentState(ContentState.SHARED);
				contentInfo.setEngagementType(EngagementType.OTF);
			}
		} else if (resourceCreated.getCourse_data().getFullname().split("-").length == 3) {
			contentInfo.setTeacherId(resourceCreated.getCourse_data().getShortname().split("-")[0]);
			contentInfo.setContentState(ContentState.PUBLIC);
			contentInfo.setEngagementType(EngagementType.OTF);
		}
		// change this below one====

		if (resourceCreated.getAssign_data() != null)
			contentInfo.setContentLink(
					moodleBaseUrl + "/mod/" + "assign" + "/view.php?id=" + resourceCreated.getModule_data().getId());
		else if (resourceCreated.getQuiz_data() != null)
			contentInfo.setContentLink(
					moodleBaseUrl + "/mod/" + "quiz" + "/view.php?id=" + resourceCreated.getModule_data().getId());
		else if (resourceCreated.getResource_data() != null)
			contentInfo.setContentLink(
					moodleBaseUrl + "/mod/" + "resource" + "/view.php?id=" + resourceCreated.getModule_data().getId());

		contentInfo.setTeacherActionLink(contentInfo.getContentLink());
		contentInfo.setStudentActionLink(contentInfo.getContentLink());

		return contentInfo;
	}
}
