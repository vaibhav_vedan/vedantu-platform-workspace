package com.vedantu.moodle;

import java.io.ByteArrayInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.LMSType;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.dao.GroupDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.entity.Group;
import com.vedantu.moodle.pojo.CourseBasicInfo;
import com.vedantu.moodle.pojo.CourseMarks;
import com.vedantu.moodle.pojo.Grades;
import com.vedantu.moodle.pojo.GroupPojo;
import com.vedantu.moodle.pojo.Items;
import com.vedantu.moodle.pojo.MoodleUser;
import com.vedantu.moodle.pojo.request.CreateCategoryReq;
import com.vedantu.moodle.pojo.request.CreateCourseReq;
import com.vedantu.moodle.pojo.request.EnrollToCourseReq;
import com.vedantu.moodle.pojo.request.GetCoursesReq;
import com.vedantu.moodle.pojo.request.GetMarksReq;
import com.vedantu.moodle.pojo.request.SearchCategoryReq;
import com.vedantu.moodle.pojo.request.SearchCourseReq;
import com.vedantu.moodle.pojo.request.SearchUserReq;
import com.vedantu.moodle.pojo.response.BasicRes;
import com.vedantu.moodle.pojo.response.CategoryInfoRes;
import com.vedantu.moodle.pojo.response.CourseInfoRes;
import com.vedantu.moodle.pojo.response.CoursePojo;
import com.vedantu.moodle.pojo.response.UserInfoRes;
import com.vedantu.moodle.utils.MoodleConfigProperties;
import com.vedantu.moodle.utils.MoodleUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@Service
public class MoodleDataManager extends MoodleConfigProperties {

	LogFactory logfactory;

	@SuppressWarnings("static-access")
	private Logger logger = logfactory.getLogger(MoodleDataManager.class);

	@Autowired
	private GroupDAO groupDAO;

	@Autowired
	private ContentInfoDAO contentInfoDAO;

	public BasicRes AddTeacherSubjectHandler(MoodleUser userInfo, String subject) {
		BasicRes res = new BasicRes();
		boolean present = false;
		try {
			SearchCategoryReq searchReq = new SearchCategoryReq(userInfo.getUserId());
			String searchCategoryRes = postMoodleData(getMoodleUrl(METHOD_SEARCH_CATEGORY), searchReq.getPostBody(),
					CONTENT_TYPE_FORM, "POST");
			List<CategoryInfoRes> categoryInfoRes = MoodleUtils.getObjectResponse(searchCategoryRes,
					CategoryInfoRes.class);
			if (categoryInfoRes == null || categoryInfoRes.isEmpty()) {
				// Create teacher category
				CreateCategoryReq categoryReq = new CreateCategoryReq(userInfo.getUserId(),
						String.valueOf(NODE_ID_TEACHER));
				String createCategoryRes = postMoodleData(getMoodleUrl(METHOD_CREATE_CATEGORY),
						categoryReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
				categoryInfoRes = MoodleUtils.getObjectResponse(createCategoryRes, CategoryInfoRes.class);
			}
			String courseName = userInfo.getUserId() + "-" + subject;
			String fullCourseName = userInfo.getFirstName() + "-" + subject;
			String courseId = getCourseId(courseName);

			logger.info(" course info res in create " + courseName);
			if (StringUtils.isEmpty(courseId)) {
				CreateCourseReq courseReq = new CreateCourseReq(fullCourseName, courseName,
						categoryInfoRes.get(0).getId(), FORMAT_TYPE);
				String createCourseRes = postMoodleData(getMoodleUrl(METHOD_CREATE_COURSE), courseReq.getPostBody(),
						CONTENT_TYPE_FORM, "POST");
				List<CourseInfoRes> courseInfoRes = MoodleUtils.getObjectResponse(createCourseRes, CourseInfoRes.class);
				courseId = courseInfoRes.get(0).getId();
				EnrollToCourseReq enrollToCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_TEACHER,
						getMoodleId(userInfo.getUserId()), courseId);
				String enrollToCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
						enrollToCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
				logger.info("additional subject to teacher :" + enrollToCourseRes.toString());
			}
			present = true;
		} catch (Exception ex) {
			if (!present) {
				logger.info("user :" + userInfo + " subject :" + subject + "cause :" + ex.getMessage());
			}
			res.setSuccess(false);
			return res;
		}

		return res;
	}

	public BasicRes createUserHandler(MoodleUser userInfo) {
		BasicRes res = new BasicRes();
		boolean present = false;
		try {
			String createUserRes = postMoodleData(getMoodleUrl(METHOD_CREATE_USER), userInfo.getPostBody(),
					CONTENT_TYPE_FORM, "POST");
			List<UserInfoRes> userInfoRes = MoodleUtils.getObjectResponse(createUserRes, UserInfoRes.class);

			String moodleUserId;
			if (userInfoRes == null || userInfoRes.isEmpty()) {
				res.setSuccess(false);
				moodleUserId = getMoodleId(userInfo.getUserId());
				// return res;
			} else {
				moodleUserId = userInfoRes.get(0).getId();
			}

			logger.info("user role res " + userInfo.getRole());
			if (Role.TEACHER.equals(userInfo.getRole())) {
				SearchCategoryReq searchReq = new SearchCategoryReq(userInfo.getUserId());
				String searchCategoryRes = postMoodleData(getMoodleUrl(METHOD_SEARCH_CATEGORY), searchReq.getPostBody(),
						CONTENT_TYPE_FORM, "POST");
				List<CategoryInfoRes> categoryInfoRes = MoodleUtils.getObjectResponse(searchCategoryRes,
						CategoryInfoRes.class);
				if (categoryInfoRes == null || categoryInfoRes.isEmpty()) {
					CreateCategoryReq categoryReq = new CreateCategoryReq(userInfo.getUserId(),
							String.valueOf(NODE_ID_TEACHER));
					String createCategoryRes = postMoodleData(getMoodleUrl(METHOD_CREATE_CATEGORY),
							categoryReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
					categoryInfoRes = MoodleUtils.getObjectResponse(createCategoryRes, CategoryInfoRes.class);
				}

				// Search for teacher private container
				String courseName = userInfo.getUserId() + "-" + userInfo.getPrimarySubject();
				String fullCourseName = userInfo.getFirstName() + "-" + userInfo.getPrimarySubject();
				String courseId = getCourseId(courseName);

				if (StringUtils.isEmpty(courseId)) {
					CreateCourseReq courseReq = new CreateCourseReq(fullCourseName, courseName,
							categoryInfoRes.get(0).getId(), FORMAT_TYPE);
					String createCourseRes = postMoodleData(getMoodleUrl(METHOD_CREATE_COURSE), courseReq.getPostBody(),
							CONTENT_TYPE_FORM, "POST");
					List<CourseInfoRes> courseInfoRes = MoodleUtils.getObjectResponse(createCourseRes,
							CourseInfoRes.class);
					courseId = courseInfoRes.get(0).getId();
				}
				present = true;
				EnrollToCourseReq enrollToCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_TEACHER, moodleUserId,
						courseId);
				String enrollToCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
						enrollToCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");

				if (userInfo.getSecondarySubject() != null && !StringUtils.isEmpty(userInfo.getSecondarySubject())) {
					courseName = userInfo.getUserId() + "-" + userInfo.getSecondarySubject();
					fullCourseName = userInfo.getFirstName() + "-" + userInfo.getSecondarySubject();
					courseId = getCourseId(courseName);

					if (StringUtils.isEmpty(courseId)) {
						CreateCourseReq courseReq = new CreateCourseReq(fullCourseName, courseName,
								categoryInfoRes.get(0).getId(), FORMAT_TYPE);
						String createCourseRes = postMoodleData(getMoodleUrl(METHOD_CREATE_COURSE),
								courseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
						List<CourseInfoRes> courseInfoRes = MoodleUtils.getObjectResponse(createCourseRes,
								CourseInfoRes.class);
						courseId = courseInfoRes.get(0).getId();
					}
					enrollToCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_TEACHER, moodleUserId, courseId);
					enrollToCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
							enrollToCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
				}
				enrollToCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_STUDENT, moodleUserId,
						String.valueOf(NODE_ID_VEDANTUCONTENT));
				enrollToCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
						enrollToCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");

				enrollToCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_TEACHER, moodleUserId,
						String.valueOf(NODE_ID_COMMON_REPO_TEACHERS));
				enrollToCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
						enrollToCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");

			} else {
				SearchCategoryReq searchReq = new SearchCategoryReq(userInfo.getUserId());
				String searchCategoryRes = postMoodleData(getMoodleUrl(METHOD_SEARCH_CATEGORY), searchReq.getPostBody(),
						CONTENT_TYPE_FORM, "POST");
				List<CategoryInfoRes> categoryInfoRes = MoodleUtils.getObjectResponse(searchCategoryRes,
						CategoryInfoRes.class);
				if (categoryInfoRes == null || categoryInfoRes.isEmpty()) {
					CreateCategoryReq categoryReq = new CreateCategoryReq(userInfo.getUserId(),
							String.valueOf(NODE_ID_STUDENT));
					String createCategoryRes = postMoodleData(getMoodleUrl(METHOD_CREATE_CATEGORY),
							categoryReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
				}
				present = true;
			}
		} catch (Exception ex) {
			if (!present) {
				logger.info("private container not created for user :" + userInfo + " cause :" + ex.getMessage());
			}
			res.setSuccess(false);
			return res;
		}

		return res;
	}

	public BasicRes createStudentTeacherMappingHandler(MoodleUser studentInfo, MoodleUser teacherInfo, String subject,
			String engagementType) {
		BasicRes res = new BasicRes();
		logger.info("teacher : " + teacherInfo.toString());
		logger.info("student : " + studentInfo.toString());
		boolean presence = false;
		// Create student
		try {
			String moodleId = getMoodleId(studentInfo.getId());
			if (StringUtils.isEmpty(moodleId)) {
				String createUserRes = postMoodleData(getMoodleUrl(METHOD_CREATE_USER), studentInfo.getPostBody(),
						CONTENT_TYPE_FORM, "POST");
				List<UserInfoRes> userInfoRes = MoodleUtils.getObjectResponse(createUserRes, UserInfoRes.class);
				moodleId = userInfoRes.get(0).getId();
			}

			// Check if student category already exists
			SearchCategoryReq searchStudentCategoryReq = new SearchCategoryReq(studentInfo.getUserId());
			String searchStudentCategoryRes = postMoodleData(getMoodleUrl(METHOD_SEARCH_CATEGORY),
					searchStudentCategoryReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
			List<CategoryInfoRes> categoryInfoRes = MoodleUtils.getObjectResponse(searchStudentCategoryRes,
					CategoryInfoRes.class);
			if (categoryInfoRes == null || categoryInfoRes.isEmpty()) {
				CreateCategoryReq categoryReq = new CreateCategoryReq(studentInfo.getUserId(),
						String.valueOf(NODE_ID_STUDENT));
				String createCategoryRes = postMoodleData(getMoodleUrl(METHOD_CREATE_CATEGORY),
						categoryReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
				categoryInfoRes = MoodleUtils.getObjectResponse(createCategoryRes, CategoryInfoRes.class);
			}
			String shortName = studentInfo.getUserId() + "-" + subject.replaceAll("-", "_") + "-"
					+ teacherInfo.getUserId() + "-" + engagementType;
			String longName = studentInfo.getFirstName() + "-" + subject.replaceAll("-", "_") + "-"
					+ teacherInfo.getFirstName() + "-" + engagementType;
			logger.info("short name " + shortName);
			logger.info("long name " + longName);
			String courseId = getCourseId(shortName);
			if (StringUtils.isEmpty(courseId)) {
				CreateCourseReq courseReq = new CreateCourseReq(longName, shortName, categoryInfoRes.get(0).getId(),
						FORMAT_TYPE);
				String createCourseRes = postMoodleData(getMoodleUrl(METHOD_CREATE_COURSE), courseReq.getPostBody(),
						CONTENT_TYPE_FORM, "POST");
				List<CourseInfoRes> courseInfoRes = MoodleUtils.getObjectResponse(createCourseRes, CourseInfoRes.class);
				courseId = courseInfoRes.get(0).getId();
			}

			presence = true;
			EnrollToCourseReq enrollToCourse = new EnrollToCourseReq(ROLE_VEDANTU_STUDENT,
					getMoodleId(studentInfo.getId()), courseId);
			postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE), enrollToCourse.getPostBody(), CONTENT_TYPE_FORM,
					"POST");
			enrollToCourse = new EnrollToCourseReq(ROLE_VEDANTU_TEACHER, getMoodleId(teacherInfo.getId()), courseId);
			postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE), enrollToCourse.getPostBody(), CONTENT_TYPE_FORM,
					"POST");
		} catch (Exception ex) {
			if (!presence) {
				logger.info(" container not created for student :" + studentInfo.toString() + "teacher :"
						+ teacherInfo.toString() + " subject :" + subject + " cause :" + ex.getMessage());
			}
			res.setSuccess(false);
			return res;
		}
		return res;
	}

	@Deprecated
	public BasicRes createGroupTeacherHandler(String groupString) {
		logger.info("group String : " + groupString);
		Gson gson = new Gson();
		GroupPojo groupPojo = gson.fromJson(groupString, GroupPojo.class);
		groupPojo.getEnrolledStudents().add(getDemoStudent());
		logger.info("groupPojo" + groupPojo.toString());
		BasicRes res = new BasicRes();
		boolean presence = false;
		try {
			Group group = new Group();
			Query query = new Query();
			Update update = new Update();
			query.addCriteria(Criteria.where(Group.Constants.BATCH_ID).is(groupPojo.getBatchId()));
			logger.info("query is " + query.toString());
			List<Group> groups = groupDAO.runQuery(query, Group.class);
			logger.info("intial groups no " + groups.size());

			if (groups.size() > 0 && groups.get(0).getEnrolledStudents() != null
					&& groups.get(0).getEnrolledStudents().size() > 0) {
				// add existing content for new student
				UserBasicInfo enrolledStudent = groups.get(0).getEnrolledStudents().get(0);
				logger.info("enrolled student exists:" + enrolledStudent.getUserId());
				Query contentsQuery = new Query();
				contentsQuery.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID)
						.is(String.valueOf(enrolledStudent.getUserId())));
				contentsQuery.addCriteria(
						Criteria.where(ContentInfo.Constants.COURSE_NAME).is(groups.get(0).getBatchTitle() + "-OTF"));
				logger.info("exisitng contents Query:" + contentsQuery.toString());
				List<ContentInfo> existingContentInfos = contentInfoDAO.runQuery(contentsQuery, ContentInfo.class);
				logger.info("exisitng contents size:" + existingContentInfos.size());
				for (UserBasicInfo newStudent : groupPojo.getEnrolledStudents()) {
					logger.info("supposedly new student " + newStudent.getUserId());
					boolean match = false;
					for (UserBasicInfo existingStudent : groups.get(0).getEnrolledStudents()) {
						logger.info("existing enrolled id " + existingStudent.getUserId());
						if (existingStudent.getUserId().longValue() == newStudent.getUserId().longValue())
							match = true;
					}
					if (match == true) {
						continue;
					}
					logger.info("match false for :" + newStudent.getFirstName());

					for (ContentInfo existingContentInfo : existingContentInfos) {
						logger.info("existing content to be added to new student :"
								+ existingContentInfo.getContentTitle());
						ContentInfo newContentInfo = new ContentInfo();
						newContentInfo.setId(null);
						newContentInfo.setTestId(existingContentInfo.getTestId());
						newContentInfo.setCourseName(existingContentInfo.getCourseName());
						newContentInfo.setContentTitle(existingContentInfo.getContentTitle());
						newContentInfo.setTeacherFullName(existingContentInfo.getTeacherFullName());
						newContentInfo.setTeacherId(existingContentInfo.getTeacherId());
						newContentInfo.setStudentActionLink(existingContentInfo.getContentLink());
						newContentInfo.setTeacherActionLink(existingContentInfo.getContentLink());
						newContentInfo.setContentLink(existingContentInfo.getContentLink());
						newContentInfo.setEngagementType(EngagementType.OTF);
						newContentInfo.setContentInfoType(existingContentInfo.getContentInfoType());
						newContentInfo.setContentType(existingContentInfo.getContentType());
						newContentInfo.setExpiryTime(existingContentInfo.getExpiryTime());
						newContentInfo.setDuration(existingContentInfo.getDuration());
						newContentInfo.setTotalMarks(existingContentInfo.getTotalMarks());
						newContentInfo.setCourseId(existingContentInfo.getCourseId());
						newContentInfo.setSubject(existingContentInfo.getSubject());
                                                newContentInfo.setBoardIds(existingContentInfo.getBoardIds());
                                                newContentInfo.setSubjects(existingContentInfo.getSubjects());
						newContentInfo.setTestId(existingContentInfo.getTestId());
						newContentInfo.setStudentId(String.valueOf(newStudent.getUserId()));
						newContentInfo.setStudentFullName(newStudent.getFullName());
						if (newContentInfo.getExpiryTime() != null
								&& (newContentInfo.getExpiryTime() > System.currentTimeMillis()
										|| newContentInfo.getExpiryTime().longValue() == 0)) {
							newContentInfo.setContentState(ContentState.SHARED);
						} else if (newContentInfo.getExpiryTime() != null
								&& newContentInfo.getExpiryTime() <= System.currentTimeMillis()) {
							newContentInfo.setContentState(ContentState.EXPIRED);
						}
						contentInfoDAO.save(newContentInfo);

					}
				}
			}
			logger.info("gave past contents to new students");
			update.set(Group.Constants.BATCH_ID, groupPojo.getBatchId().toString());
			CourseBasicInfo groupCourseInfo = groupPojo.getCourseInfo();
			if (groupCourseInfo != null && groupCourseInfo.getTitle() != null) {
				update.set(Group.Constants.BATCH_TITLE, groupCourseInfo.getTitle());
				Set<String> subjects = groupCourseInfo.getSubjects();
				if (subjects != null && !subjects.isEmpty()) {
					group.setSubjects(new ArrayList<>(subjects));
					update.set(Group.Constants.SUBJECTS, group.getSubjects());
				}
			}

			logger.info("partial qeury");
			if (groupPojo.getTeachers() != null && groupPojo.getTeachers().size() > 0) {
				update.set(Group.Constants.TEACHERS, groupPojo.getTeachers());
			}
			if (groupPojo.getEnrolledStudents() != null && groupPojo.getEnrolledStudents().size() > 0) {
				update.set(Group.Constants.ENROLLED_STUDENTS, groupPojo.getEnrolledStudents());
			}
			if (groupPojo.getCourseInfo().getLaunchDate() != null) {
				update.set(Group.Constants.LAUNCH_DATE, groupPojo.getStartTime());
			}
			if (groupPojo.getCourseInfo().getBatchStartTime() != null) {
				update.set(Group.Constants.START_TIME, groupPojo.getStartTime());
			}
			if (groupPojo.getCourseInfo().getBatchEndTime() != null) {
				update.set(Group.Constants.END_TIME, groupPojo.getEndTime());
			}
			logger.info("update is " + update.toString());
			update.set(Group.Constants.ENGAGEMENT_TYPE, EngagementType.OTF);
                        update.set(Group.Constants.LAST_UPDATED, System.currentTimeMillis());
			groupDAO.upsertEntity(query, update, Group.class);

			query = new Query();
			query.addCriteria(Criteria.where(Group.Constants.BATCH_ID).is(groupPojo.getBatchId()));
			logger.info("group id " + groupPojo.getBatchId());
			groups = groupDAO.runQuery(query, Group.class);
			group = groups.get(0);

			String courseName = group.getId() + "-" + "OTF";
			String publicCourseName = group.getId() + "-" + "OTF" + "-" + "PUBLIC";
			logger.info("group title : " + group.getBatchTitle());
			String fullName = group.getBatchTitle().replace("-", "_") + "-" + "OTF";
			String publicFullName = group.getBatchTitle().replace("-", "_") + "-" + "OTF" + "-" + "PUBLIC";
			String courseId = getCourseId(courseName);
			String publicCourseId = getCourseId(publicCourseName);

			if (StringUtils.isEmpty(publicCourseId)) {
				CreateCourseReq publicCourseReq = new CreateCourseReq(publicFullName, publicCourseName,
						String.valueOf(NODE_ID_GROUP), FORMAT_TYPE);
				String publicCreateCourseRes = postMoodleData(getMoodleUrl(METHOD_CREATE_COURSE),
						publicCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
				List<CourseInfoRes> publicCourseInfoRes = MoodleUtils.getObjectResponse(publicCreateCourseRes,
						CourseInfoRes.class);
				publicCourseId = publicCourseInfoRes.get(0).getId();
			}
			if (StringUtils.isEmpty(courseId)) {
				CreateCourseReq courseReq = new CreateCourseReq(fullName, courseName, String.valueOf(NODE_ID_GROUP),
						FORMAT_TYPE);
				String createCourseRes = postMoodleData(getMoodleUrl(METHOD_CREATE_COURSE), courseReq.getPostBody(),
						CONTENT_TYPE_FORM, "POST");
				List<CourseInfoRes> courseInfoRes = MoodleUtils.getObjectResponse(createCourseRes, CourseInfoRes.class);
				courseId = courseInfoRes.get(0).getId();
			}

			presence = true;
			logger.info("enrolling teachers");
			if (groupPojo.getTeachers() != null) {
				for (UserBasicInfo teacher : groupPojo.getTeachers()) {
					teacher.setRole(Role.TEACHER);
					String moodleId = getMoodleId(String.valueOf(teacher.getUserId()));
					if (StringUtils.isEmpty(moodleId)) {
                                            //commented by ajith, please uncomment it if u r using this feature
//						String createUserRes = postMoodleData(getMoodleUrl(METHOD_CREATE_USER), teacher.getPostBody(),
//								CONTENT_TYPE_FORM, "POST");
//						List<UserInfoRes> userInfoRes = MoodleUtils.getObjectResponse(createUserRes, UserInfoRes.class);
//						moodleId = userInfoRes.get(0).getId();
					}

					EnrollToCourseReq enrollToCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_TEACHER,
							getMoodleId(String.valueOf(teacher.getUserId())), courseId);
					String enrollToCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
							enrollToCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");

					EnrollToCourseReq enrollToPublicCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_TEACHER,
							getMoodleId(String.valueOf(teacher.getUserId())), publicCourseId);
					String enrollToPublicCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
							enrollToPublicCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");

					UserBasicInfo demoStudent = getDemoStudent();
					String demoMoodleId = getMoodleId(String.valueOf(demoStudent.getUserId()));

					logger.info("demo student searched");
					try {
						if (StringUtils.isEmpty(demoMoodleId)) {
                                                    //commented by ajith, please uncomment it if u r using this feature
//							String createUserRes = postMoodleData(getMoodleUrl(METHOD_CREATE_USER),
//									demoStudent.getPostBody(), CONTENT_TYPE_FORM, "POST");
//							logger.info("studentcreation demostudent public" + createUserRes);
//							List<UserInfoRes> demouserInfoRes = MoodleUtils.getObjectResponse(createUserRes,
//									UserInfoRes.class);
//							demoMoodleId = demouserInfoRes.get(0).getId();
						}
						logger.info("demo student moodle id:" + demoMoodleId);
						enrollToPublicCourseReq = new EnrollToCourseReq(ROLE_VEDANTU_STUDENT, demoMoodleId,
								publicCourseId);
						enrollToPublicCourseRes = postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE),
								enrollToPublicCourseReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
					} catch (Exception ex) {
						logger.info("demo student issue in public enroll");
					}

				}

			}
			// till here for public
			logger.info("enrolling students");
			if (groupPojo.getEnrolledStudents() != null && groupPojo.getEnrolledStudents().size() > 0) {
				for (UserBasicInfo student : groupPojo.getEnrolledStudents()) {
					String moodleId = getMoodleId(String.valueOf(student.getUserId()));
					if (StringUtils.isEmpty(moodleId)) {
						student.setRole(Role.STUDENT);
                                                //commented by ajith, please uncomment it if u r using this feature
//						String createUserRes = postMoodleData(getMoodleUrl(METHOD_CREATE_USER), student.getPostBody(),
//								CONTENT_TYPE_FORM, "POST");
//						List<UserInfoRes> userInfoRes = MoodleUtils.getObjectResponse(createUserRes, UserInfoRes.class);
//						moodleId = userInfoRes.get(0).getId();
					}
					EnrollToCourseReq enrollToCourse = new EnrollToCourseReq(ROLE_VEDANTU_STUDENT, moodleId, courseId);
					postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE), enrollToCourse.getPostBody(),
							CONTENT_TYPE_FORM, "POST");
				}
			}
		} catch (Exception ex) {
			if (!presence) {
				logger.info("group container creation failed for " + groupString + "cause : " + ex.getMessage());
			}
			res.setSuccess(false);
			return res;
		}
		return res;
	}

	private UserBasicInfo getDemoStudent() {
		UserBasicInfo demoStudent = new UserBasicInfo();
		demoStudent.setEmail("demo.student@vedantu.com");
		demoStudent.setRole(Role.STUDENT);
		demoStudent.setUserId(Long.parseLong(ConfigUtils.INSTANCE.getStringValue("DEMO_STUDENT_ID")));
		demoStudent.setFirstName("Demo");
		demoStudent.setLastName("Student");
		demoStudent.setFullName("demo student");
		return demoStudent;
	}

	public List<CourseInfoRes> getUserCourses(String userId) {
		String moodleId = getMoodleId(userId);
		List<CourseInfoRes> ResultcourseInfoRes = new ArrayList<CourseInfoRes>();
		if (!StringUtils.isEmpty(moodleId)) {
			GetCoursesReq req = new GetCoursesReq(moodleId);
			String coursesRes = postMoodleData(getMoodleUrl(METHOD_USER_GET_COURSES), req.getPostBody(),
					CONTENT_TYPE_FORM, "POST");
			List<CourseInfoRes> courseInfoRes = MoodleUtils.getObjectResponse(coursesRes, CourseInfoRes.class);
			for (CourseInfoRes course : courseInfoRes) {
				if (course.getShortname().split("-").length != 3)
					ResultcourseInfoRes.add(course);
			}
			return ResultcourseInfoRes;
		}
		return null;
	}

	public String getMoodleId(String userId) {
		logger.info("get user for " + userId);
		SearchUserReq userReq = new SearchUserReq(userId);
		String userInfos = postMoodleData(getMoodleUrl(METHOD_GET_USER_INFO), userReq.getPostBody(), CONTENT_TYPE_FORM,
				"POST");
		String moodleId = "";
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(userInfos.toString().getBytes("UTF-8"));
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(input);
			if (doc != null) {
				NodeList keyNodes = doc.getElementsByTagName("KEY");
				for (int i = 0; i < keyNodes.getLength(); i++) {
					if (keyNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) keyNodes.item(i);
						if ("id".equals(eElement.getAttribute("name"))) {
							moodleId = eElement.getElementsByTagName("VALUE").item(0).getTextContent();
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		logger.info("moolde id  " + moodleId);
		return moodleId;
	}

	public String enrollToPublicGroup(MoodleUser studentInfo, String contentInfoId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID).is(contentInfoId));
		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
		ContentInfo contentInfo = contentInfos.get(0);
		logger.info("content info id " + contentInfoId);
		String courseId = contentInfo.getCourseId();
		String moodleId = getMoodleId(studentInfo.getId());

		if (StringUtils.isEmpty(moodleId)) {
			String createUserRes = postMoodleData(getMoodleUrl(METHOD_CREATE_USER), studentInfo.getPostBody(),
					CONTENT_TYPE_FORM, "POST");
			List<UserInfoRes> userInfoRes = MoodleUtils.getObjectResponse(createUserRes, UserInfoRes.class);
			moodleId = userInfoRes.get(0).getId();
		}

		EnrollToCourseReq enrollToCourse = new EnrollToCourseReq(ROLE_VEDANTU_STUDENT, moodleId, courseId);
		postMoodleData(getMoodleUrl(METHOD_ENROLL_TO_COURSE), enrollToCourse.getPostBody(), CONTENT_TYPE_FORM, "POST");
		return contentInfo.getContentLink();

	}

	public String getCourseId(String courseName) {
		logger.info("get user for " + courseName);
		SearchCourseReq searchCourseReq = new SearchCourseReq(courseName);
		String searchCourseRes = postMoodleData(getMoodleUrl(METHOD_SEARCH_COURSE), searchCourseReq.getPostBody(),
				CONTENT_TYPE_FORM, "POST");
		String courseId = "";
		try {
			ByteArrayInputStream input = new ByteArrayInputStream(searchCourseRes.toString().getBytes("UTF-8"));
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document doc = builder.parse(input);
			if (doc != null) {

				NodeList keyNodes = doc.getElementsByTagName("KEY");
				for (int i = 0; i < keyNodes.getLength(); i++) {
					if (keyNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
						Element eElement = (Element) keyNodes.item(i);
						if ("id".equals(eElement.getAttribute("name"))) {
							courseId = eElement.getElementsByTagName("VALUE").item(0).getTextContent();
							break;
						}
					}
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return courseId;
	}

	public List<CoursePojo> getCourseWithId(String courseId) {
		logger.info("get user for " + courseId);
		String searchCourseRes = postMoodleData(getMoodleUrl(METHOD_COURSE_GET_COURSE) + "&options[ids][0]=" + courseId,
				"", CONTENT_TYPE_FORM, "GET");
		List<CoursePojo> courseInfoRes = MoodleUtils.getObjectResponse(searchCourseRes, CoursePojo.class);
		if (courseInfoRes.size() == 0) {
			logger.info("No course exists with " + courseId);
		}
		return courseInfoRes;
	}

	public Float fetchMarks(String contentId, String studentId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.TEST_ID).is(contentId));
		query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
		query.addCriteria(Criteria.where(ContentInfo.Constants.STUDENT_ID).is(studentId));
		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
		Float marks = null;
		if (contentInfos.size() > 0) {
			ContentInfo contentInfo = contentInfos.get(0);
			String courseId = contentInfo.getCourseId();
			String moodleId = getMoodleId(studentId);
			List<String> moodleIds = new ArrayList<String>();
			moodleIds.add(moodleId);
			GetMarksReq getMarksReq = new GetMarksReq(courseId, moodleIds);
			String marksRes = postMoodleData(
					getMoodleUrl(METHOD_GET_COURSE_MARKS_FOR_USER) + "&moodlewsrestformat=json",
					getMarksReq.getPostBody(), CONTENT_TYPE_FORM, "POST");
			logger.info("marks res :" + marksRes);
			CourseMarks courseMarks = new Gson().fromJson(marksRes, CourseMarks.class);
			for (Items item : courseMarks.getItems()) {
				if (contentId.equals(item.getActivityid())) {
					for (Grades grade : item.getGrades()) {
						try {
							marks = Float.parseFloat(grade.getStr_grade());
						} catch (Exception ex) {
							continue;
						}
						marks = marks * contentInfo.getTotalMarks()
								/ Float.parseFloat(grade.getStr_long_grade().split("/")[1]);
					}
				}
			}
		}
		return marks;
	}

	public String fetchIncorrectMarkContents(int start, int end) {
		String s = "";
		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
		query.skip(start);
		query.limit(end - start);
		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
		int correct = 0;
		for (ContentInfo contentInfo : contentInfos) {
			try {
				if (LMSType.YOUSCORE.equals(contentInfo.getLmsType())) {
					continue;
				}

				Float marks = fetchMarks(contentInfo.getTestId(), contentInfo.getStudentId());
				logger.info("fetching marks for " + contentInfo.getTestId() + " " + contentInfo.getStudentId());
				logger.info("marks is " + marks);
				if (contentInfo.getMarksAcheived() == null
						|| Math.round(marks) != Math.round(contentInfo.getMarksAcheived())) {
					s = s + contentInfo.getCourseName() + " " + contentInfo.getStudentFullName() + " current marks: "
							+ contentInfo.getMarksAcheived() + " actual marks: " + marks + " \n";
				} else {
					++correct;
				}
			} catch (Exception e) {
				s = s + contentInfo.getTestId() + " " + contentInfo.getStudentId() + " \n";
				logger.info("error in getting marks " + e.getMessage());
			}
		}

		s = s + " total size " + contentInfos.size() + " actual matching count " + correct;
		return s;
	}

	public void updateIncorrectMarkContents(Long fromTime, Long TillTime) {
		Query query = new Query();
		query.addCriteria(Criteria.where(ContentInfo.Constants.CONTENT_STATE).is(ContentState.EVALUATED));
		query.addCriteria(Criteria.where(ContentInfo.Constants.EVAULATED_TIME).gte(fromTime).lte(TillTime));
		List<ContentInfo> contentInfos = contentInfoDAO.runQuery(query, ContentInfo.class);
		for (ContentInfo contentInfo : contentInfos) {
			try {
				Float marks = fetchMarks(contentInfo.getTestId(), contentInfo.getStudentId());
				logger.info("fetching marks for " + contentInfo.getTestId() + " " + contentInfo.getStudentId());
				logger.info("marks is " + marks);
				if (contentInfo.getMarksAcheived() == null
						|| Math.round(marks) != Math.round(contentInfo.getMarksAcheived())) {
					logger.info("Moodle sending wrong marks for content: " + contentInfo.toString());
					contentInfo.setMarksAcheived(marks);
					contentInfoDAO.save(contentInfo);
				}
			} catch (Exception e) {
				logger.info("error in getting marks for update" + contentInfo.toString() + "issue " + e.getMessage());
			}
		}
	}

}
