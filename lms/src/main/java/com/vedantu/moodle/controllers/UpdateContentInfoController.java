package com.vedantu.moodle.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;

import java.util.List;

import com.vedantu.util.DateTimeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.moodle.UpdateContentInfoManager;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@RestController
@RequestMapping("/updatecontentinfo")
public class UpdateContentInfoController {

    @Autowired
    UpdateContentInfoManager updateContentInfoManager;

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(UpdateContentInfoController.class);

//    @RequestMapping(value = "/calculateTestRanks", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public void calculateTestRanks(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
//            @RequestBody String request) throws Exception {
//        Gson gson = new Gson();
//        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
//        if (messageType.equals("SubscriptionConfirmation")) {
//            String json = null;
//            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
//            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
//            logger.info(resp.getEntity(String.class));
//        } else if (messageType.equals("Notification")) {
//            logger.info("Notification received - SNS");
//            logger.info(subscriptionRequest.toString());
//            updateContentInfoManager.updateTestResultsAfterMinStartTime(System.currentTimeMillis());
//        }
//    }

    @RequestMapping(value = "/calculateTestRankCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void calculateTestRankCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                   @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            updateContentInfoManager.updateTestResultsAfterMinStartTime(System.currentTimeMillis());
        }
    }

    @RequestMapping(value = "/getAssignmentByStudent/{studentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getAssignmentByStudent(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) {
        return updateContentInfoManager.getAssignmentByStudent(studentId, fromTime, thruTime);
    }

    @RequestMapping(value = "/getTestByStudent/{studentId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> getTestByStudent(@PathVariable("studentId") Long studentId, @RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime) {
        return updateContentInfoManager.getTestByStudent(studentId, fromTime, thruTime);
    }
}
