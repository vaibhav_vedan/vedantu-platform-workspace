package com.vedantu.moodle.controllers;

import java.io.UnsupportedEncodingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.MoodleEventManager;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.pojo.response.BasicRes;

@RestController
@Service
public class MoodleEventController {

	@Autowired
	private MoodleEventManager moodleEventManager;

	@Autowired
	private ContentInfoManager contentInfoManager;

	@RequestMapping(value = "/contentShared", method = RequestMethod.POST)
	public BasicRes contentShared(@RequestBody String request) throws UnsupportedEncodingException {
		moodleEventManager.display(request);
		request = request.replace("event_json=", "");
		request = java.net.URLDecoder.decode(request, "UTF-8");
		return moodleEventManager.resourceCreated(request);
	}

	@RequestMapping(value = "/contentUpdated", method = RequestMethod.POST)
	public BasicRes contentUpdated(@RequestBody String request) throws UnsupportedEncodingException {
		moodleEventManager.display(request);
		request = request.replace("event_json=", "");
		request = java.net.URLDecoder.decode(request, "UTF-8");
		return moodleEventManager.resourceUpdated(request);
	}

	@RequestMapping(value = "/contentEvaluated", method = RequestMethod.POST)
	public BasicRes contentEvaluated(@RequestBody String request) throws UnsupportedEncodingException {
		request = request.replace("event_json=", "");
		request = java.net.URLDecoder.decode(request, "UTF-8");
		return moodleEventManager.resourceEvaluated(request);
	}

	@RequestMapping(value = "/contentAttempted", method = RequestMethod.POST)
	public BasicRes contentAttempted(@RequestBody String request) throws UnsupportedEncodingException {
		request = request.replace("event_json=", "");
		request = java.net.URLDecoder.decode(request, "UTF-8");
		return moodleEventManager.resourceAttempted(request);
	}

	@RequestMapping(value = "/addContent", method = RequestMethod.POST, consumes = "application/json")
	public BasicRes contentAdded(@RequestBody ContentInfo request) {
		return contentInfoManager.addContents(request);
	}

	@RequestMapping(value = "/contentDeleted", method = RequestMethod.POST)
	public BasicRes contentDeleted(@RequestBody String request) throws UnsupportedEncodingException {
		request = request.replace("event_json=", "");
		request = java.net.URLDecoder.decode(request, "UTF-8");
		return moodleEventManager.resourceDeleted(request);
	}
}
