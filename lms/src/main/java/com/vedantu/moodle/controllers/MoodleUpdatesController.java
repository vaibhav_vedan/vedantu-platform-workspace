package com.vedantu.moodle.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.moodle.MoodleDataManager;
import com.vedantu.moodle.pojo.NewSubjectUser;
import com.vedantu.moodle.pojo.MoodleUser;
import com.vedantu.moodle.pojo.request.EnrollStudentToPublicReq;
import com.vedantu.moodle.pojo.request.StudentTeacherMappingReq;
import com.vedantu.moodle.pojo.response.BasicRes;

@RestController
@Service
public class MoodleUpdatesController {

	@Autowired
	private MoodleDataManager moodleDataManager;

	@RequestMapping(value = "/createUser", method = RequestMethod.POST, consumes = "application/json")
	public BasicRes createUser(@RequestBody MoodleUser userInfo) {
		return moodleDataManager.createUserHandler(userInfo);
	}

	@RequestMapping(value = "/addNewSubjectUser", method = RequestMethod.POST, consumes = "application/json")
	public BasicRes updateUser(@RequestBody NewSubjectUser newSubjectUserInfo) {
		return moodleDataManager.AddTeacherSubjectHandler(newSubjectUserInfo.getUser(),
				newSubjectUserInfo.getSubject());
	}

	@Deprecated
	@RequestMapping(value = "/createGroup", method = RequestMethod.POST, consumes = "application/json")
	public BasicRes createUser(@RequestBody String groupInfo) {
		return moodleDataManager.createGroupTeacherHandler(groupInfo);
	}

	@RequestMapping(value = "/createStudentTeacherMapping", method = RequestMethod.POST, consumes = "application/json")
	public BasicRes createStudentTeacherMapping(@RequestBody StudentTeacherMappingReq req) {
		return moodleDataManager.createStudentTeacherMappingHandler(req.getStudentInfo(), req.getTeacherInfo(),
				req.getSubject(), req.getEngagementType());
	}

	@RequestMapping(value = "/enrollToPublicGroup", method = RequestMethod.POST, consumes = "application/json")
	public String enrollToPublicGroup(@RequestBody EnrollStudentToPublicReq req) {
		return moodleDataManager.enrollToPublicGroup(req.getStudentInfo(), req.getContentId());
	}
}
