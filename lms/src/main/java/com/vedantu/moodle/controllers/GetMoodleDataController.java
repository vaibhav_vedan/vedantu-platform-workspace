package com.vedantu.moodle.controllers;

import java.net.UnknownHostException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.mail.internet.AddressException;
import javax.validation.Valid;

import com.vedantu.User.Role;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.exception.VException;
import com.vedantu.moodle.pojo.request.GetTestContentReq;
import com.vedantu.moodle.pojo.request.SessionDateReq;
import com.vedantu.moodle.pojo.response.StudentDashboardInfo;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.ApiOperation;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.LMSType;
import com.vedantu.lms.cmds.pojo.TeacherContentCount;
import com.vedantu.lms.cmds.request.GetContentSharedForDashboardReq;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.MoodleDataManager;
import com.vedantu.moodle.dao.ContentInfoDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.moodle.pojo.SubscriptionRequest;
import com.vedantu.moodle.pojo.request.GetContentsReq;
import com.vedantu.moodle.pojo.request.GetTeacherCourses;
import com.vedantu.moodle.pojo.response.AssignmentCardInfo;
import com.vedantu.moodle.pojo.response.GetContentInfoListRes;
import com.vedantu.moodle.pojo.response.GetTeacherCoursesListRes;
import com.vedantu.moodle.pojo.response.GetTeacherDashBoardInfoListRes;
import com.vedantu.moodle.pojo.response.SessionDateRes;
import com.vedantu.onetofew.pojo.StudentContentPojo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@RestController
@Service
public class GetMoodleDataController {

    @Autowired
    private ContentInfoManager contentInfoManager;

    @Autowired
    private MoodleDataManager moodleDataManager;
    
    @Autowired
    private ContentInfoDAO contentInfoDAO;

    @Autowired
    HttpSessionUtils sessionUtils;
    
    @Autowired
    private LogFactory logFactory;
    
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GetMoodleDataController.class);

    @RequestMapping(value = "/getContents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetContentInfoListRes getUserContents(@RequestParam(name = "studentId", required = false) String studentId,
            @RequestParam(name = "teacherId", required = false) String teacherId,
            @RequestParam(name = "contentType", required = false) List<ContentType> contentType,
            @RequestParam(name = "contentState", required = false) List<ContentState> contentState,
            @RequestParam(name = "engagementType", required = false) List<EngagementType> engagementType,
            @RequestParam(name = "studentName", required = false) String studentName,
            @RequestParam(name = "courseId", required = false) String courseId,
            @RequestParam(name = "courseName", required = false) String courseName,
            @RequestParam(name = "searchFieldValue", required = false) String searchFieldValue,
            @RequestParam(name = "sortParam", required = false) String sortParam,
            @RequestParam(name = "lmsType", required = false) LMSType lmsType,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "end", required = false) Integer end) throws BadRequestException {
        GetContentsReq req = new GetContentsReq(studentId, teacherId, courseId, courseName, contentType, contentState,
                engagementType, studentName, searchFieldValue, sortParam, lmsType, start, end);
        return contentInfoManager.getContents(req);
    }

    @RequestMapping(value = "/getContentsForStudents", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<ContentInfo> getUsersContents(@RequestBody  com.vedantu.lms.request.GetContentsReq getContentsReq) {
        return contentInfoManager.getTestContentInfosForStudents(getContentsReq.getStudentIds(), getContentsReq.getFromTime(),getContentsReq.getThruTime());
    }

    @RequestMapping(value = "/getContentsForStudentsByFilter", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<ContentInfo> getContentsForStudentsByFilter(@RequestBody  com.vedantu.lms.request.GetContentsReq getContentsReq) throws BadRequestException {
        return contentInfoManager.getContentsForStudentsByFilter(getContentsReq);
    }

    @RequestMapping(value = "/getTeacherDashboardInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetTeacherDashBoardInfoListRes getTeacherDashBoardInfo(
            @RequestParam(name = "teacherId", required = false) String teacherId) {
        return contentInfoManager.getTeacherDashBoardInfo(teacherId);
    }

    @RequestMapping(value = "/getCourseUrl", method = RequestMethod.GET)
    public String getCourseUrl(@RequestParam(name = "courseName", required = false) String courseName) {
        return contentInfoManager.getCourseUrl(courseName);
    }

    @RequestMapping(value = "/isLastThreeAssignmentsInterventionNeeded/{studentId}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String isLastThreeAssignmentsInterventionNeeded(@PathVariable("studentId") Long studentId, @RequestParam(name = "creationTime", required = false) Long creationTime) {
        return "" +contentInfoManager.isLastThreeAssignmentsInterventionNeeded(studentId, creationTime);
    }

    @RequestMapping(value = "/isInLastTenPercentileInTests/{studentId}", method = RequestMethod.GET, produces = MediaType.TEXT_PLAIN_VALUE)
    public String isInLastTenPercentileInTests(@PathVariable("studentId") Long studentId, @RequestParam(name = "creationTime", required = false) Long creationTime) {
    	return "" +contentInfoManager.isInLastTenPercentileInTests(studentId, creationTime);
    }

    @RequestMapping(value = "/getTeacherCourses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetTeacherCoursesListRes getTeacherCourses(
            @RequestParam(name = "teacherId", required = false) String teacherId,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "end", required = false) Integer end,
            @RequestParam(name = "searchStudentId", required = false) List<String> userIds) {
        GetTeacherCourses req = new GetTeacherCourses(teacherId, start, end, userIds);
        return contentInfoManager.getTeacherCourses(req);
    }

    @RequestMapping(value = "/getAllContents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ContentInfo> getAllCourses(@RequestParam(name = "startTime", required = false) Long startTime,
            @RequestParam(name = "endTime", required = false) Long endTime,
            @RequestParam(name = "queryParam", required = false) String queryParam) {
        return contentInfoManager.getAllContents(startTime, endTime, queryParam);
    }
    
    
    //Get 3 shared latest assignments for the given studentId
    @RequestMapping(value = "/getLatestSharedAssignments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AssignmentCardInfo> getLatestSharedAssignments() throws VException  {
    	logger.info("getLatestSharedAssignments Controller");
    	HttpSessionData httpSessionData = sessionUtils.getCurrentSessionData();
    	if (Objects.isNull(httpSessionData)) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
    	Long studentId = httpSessionData.getUserId();
    	if (studentId == null) {
    		throw new UnauthorizedException(ErrorCode.USERID_IS_EMPTY, "Unable to fetch studentId");
        }
    	logger.info("getLatestSharedAssignments Controller studentId : {}", String.valueOf(studentId));
        return contentInfoManager.getLatestSharedAssignments(String.valueOf(studentId));    
    }
    
    
    //Get start time of shared assignments with session subtype for the given subject(boardId) and batch(contextId)
    @RequestMapping(value = "/getSessionDates", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SessionDateRes> getSessionDates(@Valid @RequestBody SessionDateReq sessionDateReq) throws VException  {
    	logger.info("getSessionDates Controller");
    	if (sessionDateReq == null)
    		throw new BadRequestException(ErrorCode.IO_ERROR, "Unable to fetch sessionDates");
    	sessionDateReq.verify();
        return contentInfoManager.getSessionDates(sessionDateReq);
    }
    
    
    @RequestMapping(value = "/getContentById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ContentInfo getContentById(@RequestParam(name = "contentInfoId", required = false) String contentId) {
        return contentInfoManager.getContentById(contentId);
    }

    @RequestMapping(value = "/getTestContent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public GetContentInfoListRes getContentById(GetTestContentReq getTestContentReq) throws VException {
        return contentInfoManager.getTestContent(getTestContentReq);
    }

    @ApiOperation("get counts of shared and attempted test in a batch for the given boardId and studentId")
    @RequestMapping(value = "/getTestContentCountsForStudent", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudentDashboardInfo getTestContentCountsForStudent(@RequestParam(value = "boardId", required = true) Long boardId,
            @RequestParam(name = "studentId", required = true) String studentId, @RequestParam(name = "contextId", required = true) String contextId,
            @RequestParam(value = "contentType") ContentType contentType) throws VException {
        if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
        sessionUtils.checkIfAllowedList(sessionUtils.getCallingUserId(), Collections.singletonList(Role.STUDENT), false);

        return contentInfoManager.getTestContentCountsForStudent(boardId, studentId, contextId,  contentType);
    }

    @RequestMapping(value = "/expireContents", method = RequestMethod.GET)
    public void expireContents() {
        contentInfoManager.expireContents();
    }

    @RequestMapping(value = "/expireContentsBeforeTime", method = RequestMethod.GET)
    public void expireContentsBeforeTime(Long beforeTime) {
        contentInfoManager.expireContentsBeforeTime(beforeTime);
    }

    @RequestMapping(value = "/privateContents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ContentInfo> getPrivateContents() {
        return contentInfoManager.fetchPrivateContents();
    }

    @RequestMapping(value = "/groupContents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ContentInfo> getGroupContents() {
        return contentInfoManager.fetchGroupContents();
    }

    @RequestMapping(value = "/getPendingEvaluationContents", method = RequestMethod.GET)
    public void getPendingEvaluationContents(@RequestParam(name = "allPending", required = true) boolean allPending) {
        contentInfoManager.getPendingEvaluations(allPending);
    }

    @RequestMapping(value = "getPendingEvaluationCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE)
    @ResponseBody
    public void sendPendingContentsEmail(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
            @RequestBody String request) throws UnknownHostException, AddressException, NoSuchFieldException,
            SecurityException, IllegalArgumentException, IllegalAccessException {
        Gson gson = new Gson();
        gson.toJson(request);
        SubscriptionRequest subscriptionRequest = gson.fromJson(request, SubscriptionRequest.class);

        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
        } else if (messgaetype.equals("Notification")) {
            //contentInfoManager.getPendingEvaluations(false);
        }
    }

    @RequestMapping(value = "/moodleId", method = RequestMethod.GET)
    public String getMoodleId(@RequestParam(name = "userId", required = true) String userId) {
        return moodleDataManager.getMoodleId(userId);
    }

    @Deprecated
    @RequestMapping(value = "/getMarks", method = RequestMethod.GET)
    public Float getMarks(@RequestParam(name = "contentId") String contentId,
            @RequestParam(name = "studentId") String studentId) {
        return moodleDataManager.fetchMarks(contentId, studentId);
    }

    @Deprecated
    @RequestMapping(value = "/inconsistentMarks", method = RequestMethod.GET)
    public String getMarks(@RequestParam(name = "start") int start, @RequestParam(name = "end") int end) {
        return moodleDataManager.fetchIncorrectMarkContents(start, end);
    }

    @Deprecated
    @RequestMapping(value = "/updateInconsistentMarks", method = RequestMethod.GET)
    public void getMarks(@RequestParam(name = "fromTime") Long fromTime, @RequestParam(name = "endTime") Long endTime) {
        moodleDataManager.updateIncorrectMarkContents(fromTime, endTime);
    }

    @RequestMapping(value = "/getContentInfoForDashboard", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String, List<StudentContentPojo>> getContentInfoForDashboardPOSTReq(@RequestBody GetContentSharedForDashboardReq req) throws BadRequestException, NotFoundException {
        return contentInfoManager.getContentInfoForDashboard(req.getStudentIds(), req.getBatchId());
    }

    @RequestMapping(value = "/getTeacherContentForDashboard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<Long, List<TeacherContentCount>> getTeacherContentForDashboard(@RequestParam("startTime") Long startTime,
            @RequestParam("endTime") Long endTime) throws BadRequestException, NotFoundException {

        if (startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "start or endTime not given");
        }
        return contentInfoManager.getTeacherContentForDashboard(startTime, endTime);
    }
}
