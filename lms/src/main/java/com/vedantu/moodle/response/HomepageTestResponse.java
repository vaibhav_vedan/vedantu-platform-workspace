package com.vedantu.moodle.response;

import com.vedantu.cmds.entities.CMDSTestAttempt;
import com.vedantu.cmds.pojo.TestContentInfoMetadata;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.CollectionUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.ArrayOperators;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import static com.vedantu.moodle.entity.ContentInfo.Constants.*;
import static com.vedantu.moodle.entity.ContentInfo.Constants.METADATA_TESTATTEMPTS;
import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class HomepageTestResponse {
    private String id;
    private String title;
    private String testId;
    private Long startTime;
    private Long expiryDate;
    private Long attemptBy;
    private String joiningLink;
    private Long minStartTime;
    private Long maxStartTime;
    private Long duration;
    private CMDSTestAttempt firstAttempt;
    private Set<String> subjects;
    private Set<Long> boardIds;
    private Boolean isPastUnattempted;
    private Long sharedTime;
    private Boolean isPastLive;

    public static List<HomepageTestResponse> generateFromContentInfos(List<ContentInfo> tests) {
        List<HomepageTestResponse> homePageTests = new ArrayList<>();
        if (CollectionUtils.isEmpty(tests)) {
            return homePageTests;
        }
        for (ContentInfo contentInfo : tests) {
            TestContentInfoMetadata metadata = contentInfo.getMetadata();

            HomepageTestResponse homepageTest = new HomepageTestResponse();
            homepageTest.id = contentInfo.getId();
            homepageTest.title = contentInfo.getContentTitle();
            homepageTest.subjects = contentInfo.getSubjects();
            homepageTest.boardIds = contentInfo.getBoardIds();
            homepageTest.joiningLink = contentInfo.getContentLink();
            homepageTest.duration = metadata.getDuration();
            homepageTest.testId = metadata.getTestId();
            homepageTest.startTime = metadata.getMinStartTime();

            if (Objects.nonNull(metadata.getMaxStartTime())) {
                homepageTest.attemptBy = metadata.getMaxStartTime();
            } else if (Objects.nonNull(metadata.getMinStartTime())) {
                homepageTest.attemptBy = metadata.getMinStartTime() + metadata.getDuration();
            } else {
                homepageTest.attemptBy = metadata.getExpiryDate();
            }

            homepageTest.expiryDate = metadata.getExpiryDate();
            homepageTest.sharedTime = contentInfo.getCreationTime();
            homePageTests.add(homepageTest);
        }

        return homePageTests;

    }

    public static List<HomepageTestResponse> generateCompetitiveTestResponse(List<ContentInfo> tests, Map<String, CMDSTestAttempt> attemptMap) {

        List<HomepageTestResponse> homePageTests = new ArrayList<>();
        if (CollectionUtils.isEmpty(tests)) {
            return homePageTests;
        }

        for (ContentInfo contentInfo : tests) {
            TestContentInfoMetadata metadata = contentInfo.getMetadata();

            HomepageTestResponse homepageTest = new HomepageTestResponse();
            homepageTest.id = contentInfo.getId();
            homepageTest.duration = metadata.getDuration();
            homepageTest.joiningLink = contentInfo.getContentLink();
            homepageTest.testId = metadata.getTestId();
            homepageTest.title = contentInfo.getContentTitle();
            homepageTest.subjects = contentInfo.getSubjects();
            homepageTest.boardIds = contentInfo.getBoardIds();
            homepageTest.startTime = metadata.getMinStartTime();
            homepageTest.minStartTime = metadata.getMinStartTime();
            homepageTest.expiryDate = metadata.getExpiryDate();
            homepageTest.sharedTime = contentInfo.getCreationTime();

            if (attemptMap.containsKey(contentInfo.getId())) {
                homepageTest.firstAttempt = attemptMap.get(contentInfo.getId());
            }

            if (Objects.nonNull(metadata.getMaxStartTime())) {
                homepageTest.attemptBy = metadata.getMaxStartTime();
            } else if (Objects.nonNull(metadata.getMinStartTime())) {
                homepageTest.attemptBy = metadata.getMinStartTime() + metadata.getDuration();
            } else {
                homepageTest.attemptBy = metadata.getExpiryDate();
            }

            homePageTests.add(homepageTest);
        }

        return homePageTests;
    }

}
