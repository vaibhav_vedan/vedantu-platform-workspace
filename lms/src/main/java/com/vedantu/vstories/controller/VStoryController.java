package com.vedantu.vstories.controller;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vstories.entity.VStory;
import com.vedantu.vstories.enums.RequestType;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.requests.VStoryRequest;
import com.vedantu.vstories.responses.VStoryBasicResponse;
import com.vedantu.vstories.responses.VStoryResponse;
import com.vedantu.vstories.service.VStoryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import static com.vedantu.User.Role.ADMIN;
/**
 *
 * @author Ausaf
 */
@RestController
@RequestMapping("/VStory")
public class VStoryController {
    @Autowired
    private VStoryService vStoryService;
    @Autowired
    private LogFactory logFactory;
    @Autowired
    private HttpSessionUtils sessionUtils;
    @Autowired
    private FosUtils fosUtils;
    private final Logger logger = LoggerFactory.getLogger(VStoryController.class);
    @RequestMapping(value = "/createVStory", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VStoryBasicResponse>> createVStory(@Valid @RequestBody VStoryRequest vStoryRequest) throws VException {
        logger.info("method : createVStory, vStory request : {}", vStoryRequest);
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        VStoryBasicResponse vStoryBasicResponse = new VStoryBasicResponse();
        try {
            if (vStoryRequest.getStoryType().getValue().equals("VIDEO") &&
                    ((vStoryRequest.getVideoDetails() == null || vStoryRequest.getVideoDetails().getUrl() == null) || vStoryRequest.getVideoDetails().getThumbnail() == null))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Video details");
            else if ((vStoryRequest.getStoryType().getValue().equals("IMAGE") || vStoryRequest.getStoryType().getValue().equals("GIF"))
                    && (vStoryRequest.getImageDetails() == null || vStoryRequest.getImageDetails().getImageUrl() == null || vStoryRequest.getImageDetails().getThumbnail() == null))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid Image/GIF details");
            else {
                CompletableFuture<VStoryBasicResponse> vStoryBasicResponseCompletableFuture = vStoryService.createVStory(vStoryRequest);
                return vStoryBasicResponseCompletableFuture.thenApply(response -> {
                    vStoryBasicResponse.setHttpStatus(response.getHttpStatus());
                    vStoryBasicResponse.setResponse(response.getResponse());
                    logger.info("vStory create Response : {}", vStoryBasicResponse);
                    return new ResponseEntity<VStoryBasicResponse>(vStoryBasicResponse, vStoryBasicResponse.getHttpStatus());
                });
            }
        }
        catch (BadRequestException br) {
            logger.info("Could not create vstory due to : {}", br.toString());
            vStoryBasicResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
            vStoryBasicResponse.setResponse(br.getMessage());
            return CompletableFuture.completedFuture(new ResponseEntity<VStoryBasicResponse>(vStoryBasicResponse, vStoryBasicResponse.getHttpStatus()));
        }
    }
    @RequestMapping(value = "/deleteVStory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VStoryBasicResponse>> deleteVStory(@RequestParam(value = "storyId") String storyId,
                                                                               @RequestParam(value = "grade") String grade) throws VException {
        logger.info("method : deleteVStory, vStoryId : {}", storyId);
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        VStoryBasicResponse vStoryBasicResponse = new VStoryBasicResponse();
        try {
            if (StringUtils.isEmpty(storyId))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid StoryId");
            CompletableFuture<VStoryBasicResponse> vStoryBasicResponseCompletableFuture = vStoryService.deleteVStory(storyId, grade);
            return vStoryBasicResponseCompletableFuture.thenApply(response -> {
                vStoryBasicResponse.setResponse(response.getResponse());
                vStoryBasicResponse.setHttpStatus(response.getHttpStatus());
                logger.info("vStory delete Response : {}", vStoryBasicResponse);
                return new ResponseEntity<VStoryBasicResponse>(vStoryBasicResponse, vStoryBasicResponse.getHttpStatus());
            });
        }
        catch (BadRequestException e) {
            logger.info("Could not delete vstory due to : {}", e.toString());
            vStoryBasicResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
            vStoryBasicResponse.setResponse(e.getMessage());
            return CompletableFuture.completedFuture(new ResponseEntity<VStoryBasicResponse>(vStoryBasicResponse, vStoryBasicResponse.getHttpStatus()));
        }
    }
    @RequestMapping(value = "/editVStory/{storyId}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VStoryBasicResponse> editVStory(@PathVariable("storyId") String storyId, @Valid @RequestBody VStoryRequest vStoryRequest) throws VException {
        logger.info("method : createVStory, vStory request : {}", vStoryRequest);
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        VStoryBasicResponse vStoryBasicResponse = new VStoryBasicResponse();
        try {
            if (StringUtils.isEmpty(storyId))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty StoryId");
            vStoryBasicResponse = vStoryService.editVStory(storyId, vStoryRequest);
            return new ResponseEntity<VStoryBasicResponse>(vStoryBasicResponse, vStoryBasicResponse.getHttpStatus());
        }
        catch (BadRequestException br) {
            logger.info("Could not edit vstory due to : {}", br.toString());
            vStoryBasicResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
            vStoryBasicResponse.setResponse(br.getMessage());
            return new ResponseEntity<VStoryBasicResponse>(vStoryBasicResponse, vStoryBasicResponse.getHttpStatus());
        }
    }
    @RequestMapping(value = "/fetchCurrentVStories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VStoryResponse>> fetchCurrentVStories(@RequestParam(value = "grade") String grade,
                                                                     @RequestParam(value = "requestType") RequestType requestType,
                                                                     @RequestParam(value = "sectionType") SectionType sectionType)
    {
        logger.info("method : fetchCurrentVStories, grade : {}", grade);
        List<VStoryResponse> vStoryResponseList = new ArrayList<>();
        try {
            if (StringUtils.isEmpty(grade) || StringUtils.isEmpty(requestType.getValue()) || StringUtils.isEmpty(sectionType.getValue()) )
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid grade/requestType/sectionType");
            String target = "";
            if (requestType.equals(RequestType.APP) && (grade.equals("11") || grade.equals("12") || grade.equals("13")))
            {
                Long userId = sessionUtils.getCallingUserId();
                UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);
                target = userBasicInfo.getTarget();
            }
            vStoryResponseList = vStoryService.fetchCurrentVStories(grade, requestType, sectionType, target,true);
        }
        catch (BadRequestException br)
        {
            logger.info("Could not fetch vstories due to : {}", br.toString());
            return new ResponseEntity<List<VStoryResponse>>(vStoryResponseList, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e)
        {
            logger.info("Could not fetch vstories due to : {}", e.getMessage());
            return new ResponseEntity<List<VStoryResponse>>(vStoryResponseList, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<List<VStoryResponse>>(vStoryResponseList, HttpStatus.OK);
    }
    @RequestMapping(value = "/fetchArchivedVStories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VStoryResponse>> fetchArchivedVStories(@RequestParam(value = "grade") String grade,
                                                                      @RequestParam(value = "requestType") RequestType requestType,
                                                                      @RequestParam(value = "sectionType") SectionType sectionType,
                                                                      @RequestParam(value = "start") Integer start,
                                                                      @RequestParam(value = "size") Integer size)
    {
        logger.info("method : fetchArchivedVStories, grade : {}", grade);
        List<VStoryResponse> vStoryResponseList = new ArrayList<>();
        try {
            if (StringUtils.isEmpty(grade) || StringUtils.isEmpty(requestType.getValue()) || StringUtils.isEmpty(sectionType.getValue()) )
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid grade/requestType/sectionType");
            String target = "";
            if (requestType.equals(RequestType.APP) && (grade.equals("11") || grade.equals("12") || grade.equals("13")))
            {
                Long userId = sessionUtils.getCallingUserId();
                UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);
                target = userBasicInfo.getTarget();
            }
            vStoryResponseList = vStoryService.fetchArchivedVStories(grade, requestType, sectionType, target,false,start,size);
        }
        catch (BadRequestException br)
        {
            logger.info("Could not fetch vstories due to : {}", br.toString());
            return new ResponseEntity<List<VStoryResponse>>(vStoryResponseList, HttpStatus.BAD_REQUEST);
        }
        catch (Exception e)
        {
            logger.info("Could not fetch vstories due to : {}", e.getMessage());
            return new ResponseEntity<List<VStoryResponse>>(vStoryResponseList, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<List<VStoryResponse>>(vStoryResponseList, HttpStatus.OK);
    }

    @RequestMapping(value = "/getStoryById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public VStory getStoryById(@RequestParam(value = "storyId") String storyId) throws VException {
        logger.info("method : getStoryById, storyId : {}", storyId);
        return vStoryService.getStoryById(storyId);
    }
}