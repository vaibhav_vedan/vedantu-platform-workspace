package com.vedantu.vstories.enums;

public enum Type
{
    MASTERCLASS("MASTERCLASS"),
    VSAT("VSAT"),
    COURSE("COURSE"),
    APP_TEST("APP_TEST"),
    APP_PDF("APP_PDF"),
    APP_VIDEO("APP_VIDEO"),
    ENGAGEMENT("ENGAGEMENT"),
    OTHERS("OTHERS");

    String value;

    Type(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
