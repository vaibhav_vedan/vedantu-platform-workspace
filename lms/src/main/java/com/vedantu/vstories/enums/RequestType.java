package com.vedantu.vstories.enums;

public enum RequestType
{
    APP("APP"),
    CMS("CMS");

    String value;

    RequestType(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
