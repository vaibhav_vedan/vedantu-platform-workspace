package com.vedantu.vstories.pojo;

import com.vedantu.vstories.responses.MuxResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VideoDetails
{
    private String thumbnail;
    private String url;
    private String hlsUrl;
    private MuxResponse muxResponse;
}
