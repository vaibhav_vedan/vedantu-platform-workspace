package com.vedantu.vstories.service;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.util.LogFactory;
import com.vedantu.vstories.client.MuxClient;
import com.vedantu.vstories.requests.MuxRequest;
import com.vedantu.vstories.responses.MuxResponse;
import com.vedantu.vstories.responses.MuxWrapper;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.io.IOException;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author Ausaf
 */

@Service
public class MuxService
{
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(MuxService.class);

    @Autowired
    private MuxClient muxClient;

    @Async("VStoryAsyncTaskExecutor")
    @Retryable(value = {
            Exception.class},
            maxAttempts = 3,
            backoff = @Backoff(delay = 2000L)
    )
    public CompletableFuture<MuxResponse> createMuxAsset(MuxRequest muxRequest) throws Exception {

        Response<MuxWrapper> muxResponseEntity = muxClient.createAsset(muxRequest).execute();
        if (muxResponseEntity.isSuccessful()) {

            MuxWrapper muxWrapper = muxResponseEntity.body();
            MuxResponse muxResponse = muxWrapper.getData();
            logger.info("muxResponse createAsset: {}", muxResponse);

            return CompletableFuture.completedFuture(muxResponse);
        } else
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Could not create Mux Asset");
    }


    @Async("VStoryAsyncTaskExecutor")
    @Retryable(value = {
            Exception.class},
            maxAttempts = 3,
            backoff = @Backoff(delay = 2000L)
    )
    public CompletableFuture<MuxResponse> getMuxAsset(String assetId) throws Exception {

        Response<MuxWrapper> muxResponseEntity = muxClient.getAsset(assetId).execute();

        if (muxResponseEntity.isSuccessful()) {

            MuxWrapper muxWrapper = muxResponseEntity.body();
            MuxResponse muxResponse = muxWrapper.getData();

            logger.info("muxResponse getAsset : {}", muxResponse);

            if (!muxResponse.getStatus().equals("ready"))
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Mux Video Asset is not ready");

            return CompletableFuture.completedFuture(muxResponse);

        } else
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Could not get Mux Asset");
    }

    @Async("VStoryAsyncTaskExecutor")
    @Retryable(value = {
            Exception.class},
            maxAttempts = 3,
            backoff = @Backoff(delay = 2000L)
    )
    public CompletableFuture<Boolean> deleteMuxAsset(String assetId) throws Exception {

        Response<Boolean> muxResponseEntity = muxClient.deleteAsset(assetId).execute();
        return CompletableFuture.completedFuture(muxResponseEntity.isSuccessful());
    }
}
