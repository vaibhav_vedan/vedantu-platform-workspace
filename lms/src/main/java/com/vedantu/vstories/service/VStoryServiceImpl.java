package com.vedantu.vstories.service;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.vstories.dao.VStoryDAO;
import com.vedantu.vstories.entity.VStory;
import com.vedantu.vstories.enums.RequestType;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.enums.StoryType;
import com.vedantu.vstories.requests.MuxRequest;
import com.vedantu.vstories.requests.VStoryRequest;
import com.vedantu.vstories.responses.MuxResponse;
import com.vedantu.vstories.responses.VStoryBasicResponse;
import com.vedantu.vstories.responses.VStoryResponse;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static com.vedantu.util.DateTimeUtils.*;
/**
 *
 * @author Ausaf
 */
@Service
public class VStoryServiceImpl implements VStoryService
{
    @Autowired
    private LogFactory logFactory;
    private final Logger logger = logFactory.getLogger(VStoryServiceImpl.class);
    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    @Autowired
    private VStoryDAO vStoryDAO;
    @Autowired
    private DozerBeanMapper dozerBeanMapper;
    @Autowired
    private RedisDAO redisDAO;
    @Autowired
    private MuxService muxService;
    @Autowired
    private RedisService redisService;
    @Override
    public CompletableFuture<VStoryBasicResponse> createVStory(VStoryRequest vStoryRequest)
    {
        logger.info("method : create VStory, vStoryRequest : {}", vStoryRequest);
        VStoryBasicResponse vStoryBasicResponse = new VStoryBasicResponse();
        try
        {
            if (vStoryRequest == null)
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "vStory request null");
            String gradesAllowedResponse = isStoryAllowedForGrades(vStoryRequest.getGrades(), vStoryRequest.getSectionType(), vStoryRequest.getStartTime(), null);
            if (gradesAllowedResponse.equals("success"))
            {
                VStory vStory = dozerBeanMapper.map(vStoryRequest, VStory.class);
                logger.info("VStory mapped : {}", vStory);
                if (vStory.getStoryType().getValue().equals("VIDEO")) {
                    MuxRequest muxRequest = new MuxRequest();
                    muxRequest.setInput(vStory.getVideoDetails().getUrl());
                    muxRequest.setPlayback_policy("public");
                    CompletableFuture<MuxResponse> muxResponseCompletableFuture = getMuxDetails(muxRequest);
                    return muxResponseCompletableFuture.thenApply(muxResponse ->
                    {
                        VStoryBasicResponse vStoryBasicResponse1 = new VStoryBasicResponse();
                        logger.info("Mux Response : {}", muxResponse);
                        try
                        {
                            if (muxResponse != null) {
                                if (muxResponse.getDuration() != null && Double.parseDouble(muxResponse.getDuration()) > 61.0)
                                {
                                    try
                                    {
                                        //asynchronously delete the just created mux asset.
                                        muxService.deleteMuxAsset(muxResponse.getId());
                                    } catch (Exception e) {
                                        //
                                    }
                                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Duration length exceeds 60sec");
                                }
                                else {
                                    vStory.getVideoDetails().setMuxResponse(muxResponse);
                                    String playbackId = muxResponse.getPlayback_ids().get(0).getId();
                                    String hlsUrl = "https://stream.mux.com/".concat(playbackId).concat(".m3u8");
                                    vStory.getVideoDetails().setHlsUrl(hlsUrl);
                                }
                            } else {
                                vStoryBasicResponse1.setResponse("Mux Error! Tip: Try uploading video of length up to 60 secs.");
                                vStoryBasicResponse1.setHttpStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                                return vStoryBasicResponse1;
                            }
                        }
                        catch (BadRequestException br)
                        {
                            logger.info("Could not save VStory due to {}", br.toString());
                            vStoryBasicResponse1.setResponse(br.getErrorMessage());
                            vStoryBasicResponse1.setHttpStatus(HttpStatus.BAD_REQUEST);
                            return vStoryBasicResponse1;
                        }
                        vStoryBasicResponse1 = vStoryDAO.save(vStory);
                        logger.info("VStory created : {}", vStory);
                        redisService.deleteRedisStories(vStory.getGrades(), vStory.getSectionType());
                        return vStoryBasicResponse1;
                    });
                }
                else
                {
                    vStoryBasicResponse = vStoryDAO.save(vStory);
                    logger.info("VStory created : {}", vStory);
                    redisService.deleteRedisStories(vStory.getGrades(), vStory.getSectionType());
                    return CompletableFuture.completedFuture(vStoryBasicResponse);
                }
            }
            else
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, gradesAllowedResponse);
        }
        catch (BadRequestException br)
        {
            logger.info("Could not save VStory due to {}", br.toString());
            vStoryBasicResponse.setResponse(br.getErrorMessage());
            vStoryBasicResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
            return CompletableFuture.completedFuture(vStoryBasicResponse);
        }
    }
    @Override
    public CompletableFuture<VStoryBasicResponse> deleteVStory(String storyId, String grade)
    {
        VStoryBasicResponse vStoryBasicResponse = new VStoryBasicResponse();
        logger.info("method : deleteVStory, storyId : {}, grade : {}", storyId, grade);
        VStory vStory = vStoryDAO.findById(storyId);
        if (vStory != null)
        {
            vStory.setEnable(false);
            vStoryBasicResponse = vStoryDAO.save(vStory);
            redisService.deleteRedisStories(Collections.singleton(grade), vStory.getSectionType());
            return CompletableFuture.completedFuture(vStoryBasicResponse);
        }
        else
        {
            vStoryBasicResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
            vStoryBasicResponse.setResponse("No VStory present with the given id");
            return CompletableFuture.completedFuture(vStoryBasicResponse);
        }
    }
    @Override
    public VStoryBasicResponse editVStory(String storyId, VStoryRequest vStoryRequest)
    {
        logger.info("method : edit VStory, vStoryRequest : {},  storyId : {}", vStoryRequest, storyId);
        VStoryBasicResponse vStoryBasicResponse = new VStoryBasicResponse();
        VStory vStory = vStoryDAO.findById(storyId);
        if (vStory == null)
        {
            vStoryBasicResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
            vStoryBasicResponse.setResponse("No VStory present with the given id");
            return vStoryBasicResponse;
        }
        else
        {
            String gradesAllowedResponse = isStoryAllowedForGrades(vStoryRequest.getGrades(), vStoryRequest.getSectionType(), vStoryRequest.getStartTime(), storyId);
            if (gradesAllowedResponse.equals("success"))
            {
                Set<String> grades = vStory.getGrades();
                if (grades.size() != 1) {
                    String grade = vStoryRequest.getGrades().iterator().next();
                    grades.remove(grade);
                    vStory.setGrades(grades);
                    Set<String> mainTags = vStory.getMainTags();
                    if (mainTags != null && !mainTags.isEmpty()) {
                        mainTags.removeIf(mainTag -> mainTag.startsWith(grade));
                        vStory.setMainTags(mainTags);
                    }
                    logger.info("Updating existing vstory after removing the grade : {}", vStory);
                    vStoryBasicResponse = vStoryDAO.save(vStory);
                    logger.info("Update response : {}", vStoryBasicResponse);
                    storyId = null;
                }
                if (vStory.getStoryType().equals(StoryType.VIDEO)) {
                    String hlsUrl = vStory.getVideoDetails().getHlsUrl();
                    MuxResponse muxResponse = vStory.getVideoDetails().getMuxResponse();
                    vStory = dozerBeanMapper.map(vStoryRequest, VStory.class);
                    vStory.getVideoDetails().setHlsUrl(hlsUrl);
                    vStory.getVideoDetails().setMuxResponse(muxResponse);
                } else
                    vStory = dozerBeanMapper.map(vStoryRequest, VStory.class);
                if (storyId != null)
                    vStory.setId(storyId);
                logger.info("Edited VStory , Vstory edit request : {} ", vStory);
                vStoryBasicResponse = vStoryDAO.save(vStory);
                vStoryBasicResponse.setHttpStatus(vStoryBasicResponse.getHttpStatus());
                vStoryBasicResponse.setResponse("Successfully edited VStory");
                return vStoryBasicResponse;
            }
            else
            {
                logger.info("Could not save VStory due to {}", gradesAllowedResponse);
                vStoryBasicResponse.setResponse(gradesAllowedResponse);
                vStoryBasicResponse.setHttpStatus(HttpStatus.OK);
                return vStoryBasicResponse;
            }
        }
    }
    @Override
    public List<VStoryResponse> fetchCurrentVStories(String grade, RequestType requestType, SectionType sectionType, String target,boolean enable)
    {
        logger.info("method : fetchCurrentVStories, grade : {}, sectionType : {}, requestType : {}", grade, sectionType.getValue(), requestType.getValue());
        String resString = "";
        List<VStoryResponse> vStoryResponseList = null;
        if (StringUtils.isEmpty(resString)) {
            List<VStory> vStoryList = vStoryDAO.fetchCurrentVStories(grade, requestType, sectionType,enable);
            logger.info("VStories fetched from db : {}", vStoryList);
            if (vStoryList != null)
            {
                vStoryResponseList = new ArrayList<>();
                for (VStory vStory : vStoryList)
                {
                    VStoryResponse vStoryResponse = dozerBeanMapper.map(vStory, VStoryResponse.class);
                    vStoryResponseList.add(vStoryResponse);
                }
            }
        }
        else
        {
            logger.info("Fetching data from redis");
            Type typeToken = new TypeToken<List<VStoryResponse>>(){}.getType();
            vStoryResponseList = new Gson().fromJson(resString, typeToken);
        }
        logger.info("VStory list Response : {}", vStoryResponseList);
        if (requestType.equals(RequestType.APP) && ((grade.equals("11") || grade.equals("12") || grade.equals("13")) && StringUtils.isNotEmpty(target)
                && vStoryResponseList!= null && !vStoryResponseList.isEmpty()))
        {
            target = target.split(" ")[0];
            target = grade + "-" + target.toUpperCase();
            List<VStoryResponse> responseList = new ArrayList<>();
            logger.info("Filtering vstory for grade : {}, target : {}", grade, target);
            for (VStoryResponse vStory : vStoryResponseList)
            {
                if (vStory.getMainTags() != null && !vStory.getMainTags().isEmpty()) {
                    if (vStory.getMainTags().contains(target))
                        responseList.add(vStory);
                }
                else
                {
                    responseList.add(vStory);
                }
            }
            return responseList;
        }
        return vStoryResponseList;
    }
    @Override
    public List<VStoryResponse> fetchArchivedVStories(String grade, RequestType requestType, SectionType sectionType, String target,boolean enable,Integer start,Integer size)
    {
        logger.info("method : fetchCurrentVStories, grade : {}, sectionType : {}, requestType : {}", grade, sectionType.getValue(), requestType.getValue());
        List<VStoryResponse> vStoryResponseList = null;
        List<VStory> vStoryList = vStoryDAO.fetchArchivedVStories(grade, requestType, sectionType,enable,start,size);
        logger.info("VStories fetched from db : {}", vStoryList);
        if (vStoryList != null)
        {
            vStoryResponseList = new ArrayList<>();
            for (VStory vStory : vStoryList)
            {
                VStoryResponse vStoryResponse = dozerBeanMapper.map(vStory, VStoryResponse.class);
                vStoryResponseList.add(vStoryResponse);
            }
        }
        logger.info("VStory list Response : {}", vStoryResponseList);
        if (requestType.equals(RequestType.APP) && ((grade.equals("11") || grade.equals("12") || grade.equals("13")) && StringUtils.isNotEmpty(target)
                && vStoryResponseList!= null && !vStoryResponseList.isEmpty()))
        {
            target = target.split(" ")[0];
            target = grade + "-" + target.toUpperCase();
            List<VStoryResponse> responseList = new ArrayList<>();
            logger.info("Filtering vstory for grade : {}, target : {}", grade, target);
            for (VStoryResponse vStory : vStoryResponseList)
            {
                if (vStory.getMainTags() != null && !vStory.getMainTags().isEmpty()) {
                    if (vStory.getMainTags().contains(target))
                        responseList.add(vStory);
                }
                else
                {
                    responseList.add(vStory);
                }
            }
            return responseList;
        }
        return vStoryResponseList;
    }

    public  VStory getStoryById(String storyId) throws NotFoundException {
        logger.info("method : getStoryById, storyId : {}", storyId);
        VStory vStory = vStoryDAO.findById(storyId);
        if(vStory == null){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,"Vstory Id is not found");
        }
        return  vStory;
    }

    private CompletableFuture<MuxResponse> getMuxDetails(MuxRequest muxRequest)
    {
        logger.info("method : getMuxDetails, muxRequest : {} ", muxRequest);
        CompletableFuture<MuxResponse> muxResponseCompletableFuture;
        try {
            muxResponseCompletableFuture = muxService.createMuxAsset(muxRequest)
                    .exceptionally(throwable -> {
                        logger.info("Error in Mux Asset creation : {}", throwable.getMessage());
                        return null;
                    });
        }
        catch (Exception e)
        {
            logger.info("Error in Mux Asset creation : {}", e.getMessage());
            return CompletableFuture.completedFuture(null);
        }
        return muxResponseCompletableFuture.thenCompose(muxResponse ->
        {
            if (muxResponse == null)
                return null;
            logger.info("Mux asset created : {}", muxResponse);
            try
            {
                return muxService.getMuxAsset(muxResponse.getId())
                        .exceptionally(throwable -> {
                            try
                            {
                                //asynchronously delete the just created mux asset.
                                muxService.deleteMuxAsset(muxResponse.getId());
                            } catch (Exception e) {
                                //
                            }
                            logger.info("Error in Mux Asset retrieval : {}", throwable.getMessage());
                            return null;
                        });
            }
            catch (Exception e)
            {
                logger.info("Error in Mux Asset retrieval : {}", muxResponse.getStatus());
                return null;
            }
        }).thenApply(muxResponse ->
        {
            logger.info("Mux asset retrieved : {}", muxResponse );
            return muxResponse;
        });
    }
    private String isStoryAllowedForGrades(Set<String> grades, SectionType sectionType, Long startTime, String storyId)
    {
        logger.info("method : checkIfStoryAllowedForGrade for grade : {}", grades);
        String response = "success";
        Date date = new Date(startTime);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.clear();
        calendar.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        calendar.set(year, month, day);
        startTime = calendar.getTimeInMillis();
        Long endTime = startTime + MILLIS_PER_DAY;
        StringBuilder totalLimitReached = new StringBuilder();
        StringBuilder dailyLimitReached = new StringBuilder();
        Date dayLimit = new Date(startTime);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        simpleDateFormat.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String startDay = simpleDateFormat.format(dayLimit);
        if (!grades.isEmpty())
        {
            logger.info("Verifying story limit : grades : {}, sectionType : {}, startTime : {}", grades, sectionType, startTime);
            long gradeCount = -1, dateCount;
            for (String grade : grades)
            {
                if (storyId == null)
                    gradeCount = vStoryDAO.getStoryCountForGrade(grade, sectionType, null, null, null);
                dateCount = vStoryDAO.getStoryCountForGrade(grade, sectionType, startTime, endTime, storyId);
                //create story case
                if (storyId == null) {
                    if (dateCount == 50)
                    {
                        dailyLimitReached.append(grade).append(", ");
                    }
                    else if (gradeCount == 105)
                    {
                        totalLimitReached.append(grade).append(", ");
                    }
                }
                else
                {
                    //edit story case will have only one grade
                    if (dateCount == 50) {
                        response = "Story limit exceeded for grade : " + grade + " for " + startDay;
                        return response;
                    }
                }
            }
            if (StringUtils.isNotEmpty(dailyLimitReached.toString()))
            {
                response = "Story limit exceeded for grade(s) : " + dailyLimitReached.substring(0, dailyLimitReached.length() - 2) + " for " + startDay;
            }
            else if (StringUtils.isNotEmpty(totalLimitReached.toString()))
            {
                response = "Story limit exceeded for grade(s) : " + totalLimitReached.substring(0, totalLimitReached.length() - 2);
            }
            return response;
        }
        else
            return "Empty grade request";
    }
}
