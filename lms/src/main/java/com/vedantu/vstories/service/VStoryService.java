package com.vedantu.vstories.service;
import com.vedantu.exception.NotFoundException;
import com.vedantu.vstories.entity.VStory;
import com.vedantu.vstories.enums.RequestType;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.requests.VStoryRequest;
import com.vedantu.vstories.responses.VStoryBasicResponse;
import com.vedantu.vstories.responses.VStoryResponse;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.concurrent.CompletableFuture;
/**
 *
 * @author Ausaf
 */
public interface VStoryService
{
    CompletableFuture<VStoryBasicResponse> createVStory(VStoryRequest vStoryRequest);

    CompletableFuture<VStoryBasicResponse> deleteVStory(String storyId, String grade);

    VStoryBasicResponse editVStory(String storyId, VStoryRequest vStoryRequest);

    List<VStoryResponse> fetchCurrentVStories(String grade, RequestType requestType, SectionType sectionType, String target,boolean enable);

    List<VStoryResponse> fetchArchivedVStories(String grade, RequestType requestType, SectionType sectionType, String target,boolean enable,Integer start,Integer size);

    VStory getStoryById(String storyId) throws NotFoundException;
}