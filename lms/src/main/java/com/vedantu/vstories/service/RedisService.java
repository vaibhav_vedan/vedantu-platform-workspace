package com.vedantu.vstories.service;

import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.vstories.enums.SectionType;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 *
 * @author Ausaf
 */

@Service
public class RedisService
{
    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(RedisService.class);

    public void deleteRedisStories(Set<String> grades, SectionType sectionType)
    {
        for (String grade : grades)
        {
            String vStoriesKey = "VSTORIES_" + sectionType.getValue() + "_" +grade;
            try
            {
                redisDAO.del(vStoriesKey);
            }
            catch (VException e)
            {
                logger.info("Could not delete Vstories  from Redis due to : {}", e.toString());
            }
        }
    }

    public String getRedisStories(String grade, SectionType sectionType)
    {
        String vStoriesKey = "VSTORIES_" + sectionType.getValue() + "_" +grade;
        String res = "";
        try
        {
            res = redisDAO.get(vStoriesKey);
        }
        catch (VException e)
        {
            logger.info("Could not get Vstories from Redis due to : {}", e.toString());
        }
        return res;
    }

    public void setRedisStories(String grade, String vStory, SectionType sectionType)
    {
        String vStoriesKey = "VSTORIES_" + sectionType.getValue() + "_" +grade;
        try
        {
            redisDAO.setex(vStoriesKey, vStory, 600);
        }
        catch (VException e)
        {
            logger.info("Could not set Vstories in Redis due to : {}", e.toString());
        }
    }
}
