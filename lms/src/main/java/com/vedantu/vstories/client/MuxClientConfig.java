package com.vedantu.vstories.client;

import com.vedantu.util.ConfigUtils;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author Ausaf
 */

@Configuration
public class MuxClientConfig
{
    private Retrofit retrofit;

    private final String username = ConfigUtils.INSTANCE.getStringValue("mux.username");
    private final String password = ConfigUtils.INSTANCE.getStringValue("mux.password");

    public MuxClientConfig()
    {
        String muxEndpoint = ConfigUtils.INSTANCE.getStringValue("MUX_ENDPOINT");

        Interceptor interceptor = new BasicInterceptor(username, password);

        OkHttpClient okhttpClient = OkhttpClient.getUnsafeOkHttpClient();

        okhttpClient = okhttpClient.newBuilder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                       .baseUrl(muxEndpoint)
                       .client(okhttpClient)
                       .addConverterFactory(GsonConverterFactory.create())
                       .build();
    }

    @Bean
    public MuxClient getMuxClient()
    {
        return retrofit.create(MuxClient.class);
    }
}
