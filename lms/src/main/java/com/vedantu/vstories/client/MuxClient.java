package com.vedantu.vstories.client;

import com.vedantu.vstories.requests.MuxRequest;
import com.vedantu.vstories.responses.MuxWrapper;
import retrofit2.Call;
import retrofit2.http.*;

/**
 *
 * @author Ausaf
 */

public interface MuxClient
{
    @POST("/video/v1/assets")
    Call<MuxWrapper> createAsset(@Body MuxRequest muxRequest);

    @GET("/video/v1/assets/{id}")
    Call<MuxWrapper> getAsset(@Path("id") String id);

    @DELETE("/video/v1/assets/{id}")
    Call<Boolean> deleteAsset(@Path("id") String id);
}
