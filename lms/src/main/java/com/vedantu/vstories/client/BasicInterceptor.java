package com.vedantu.vstories.client;

import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 *
 * @author Ausaf
 */

public class BasicInterceptor implements Interceptor {

    private final String credential;

    public BasicInterceptor(String username, String password)
    {
        this.credential = Credentials.basic(username, password);
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request original = chain.request();
        Request request = original.newBuilder()
                          .addHeader("Authorization", credential)
                          .build();
        return chain.proceed(request);
    }
}
