package com.vedantu.vstories.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.enums.StoryType;
import com.vedantu.vstories.enums.Type;
import com.vedantu.vstories.pojo.ImageDetails;
import com.vedantu.vstories.pojo.VideoDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

/**
 *
 * @author Ausaf
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(value = "VStory")
@CompoundIndexes({
        @CompoundIndex(name = "grades_1_sectionType_1_startTime_-1_endTime_1", def = "{'grades': 1, 'sectionType': 1, 'startTime' : -1, 'endTime' : 1}", background = true),
})
public class VStory extends AbstractMongoStringIdEntity
{
    @NotNull
    @Size(max = ReqLimits.VSTORY_TITLE_MAX, message = ReqLimitsErMsgs.VSTORY_TITLE_MAX)
    private String title;

    @NotNull
    private Set<String> grades;

    private Set<String> mainTags;

    @NotNull
    private StoryType storyType;

    @NotNull
    private SectionType sectionType;

    private ImageDetails imageDetails;

    private VideoDetails videoDetails;

    @Size(max = ReqLimits.VSTORY_TITLE_MAX, message = ReqLimitsErMsgs.VSTORY_TITLE_MAX)
    private String ctaTitle;

    private String ctaDeepLink;

    @NotNull
    private Long startTime;

    @NotNull
    private Long endTime;

    private boolean enableAutoRun;

    private Type type;
    @Size(max = ReqLimits.VSTORY_TAG_MAX, message = ReqLimitsErMsgs.VSTORY_TAG_MAX)
    private String tag;
    private Long weightage;
    private boolean enable;

    public static class Constants extends AbstractMongoEntity.Constants
    {
        public static final String GRADES = "grades";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String SECTION_TYPE = "sectionType";
        public static final String MUX_ID = "videoDetails.muxResponse.id";
        public static final String ENABLE = "enable";
    }

}
