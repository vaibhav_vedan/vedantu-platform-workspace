package com.vedantu.vstories.dao;

import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vstories.entity.VStory;
import com.vedantu.vstories.enums.RequestType;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.responses.VStoryBasicResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author Ausaf
 */

@Service
public class VStoryDAO extends AbstractMongoDAO
{
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(VStoryDAO.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public VStoryBasicResponse save(VStory vStory)
    {
        logger.info("vStory Request : {}", vStory);
        VStoryBasicResponse vStoryBasicResponse = new VStoryBasicResponse();
        if (vStory != null)
        {
            saveEntity(vStory);
            vStoryBasicResponse.setResponse("Successfully saved");
            vStoryBasicResponse.setHttpStatus(HttpStatus.OK);
        }
        else {
            vStoryBasicResponse.setResponse("Empty request");
            vStoryBasicResponse.setHttpStatus(HttpStatus.BAD_REQUEST);
        }

        return vStoryBasicResponse;
    }

    public long getStoryCountForGrade(String grade, SectionType sectionType, Long lowerLimit, Long upperLimit, String id)
    {
        logger.info("method : getStoryCountForGrade for grade : {}", grade);
        long count = -1;
        if (StringUtils.isNotEmpty(grade))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VStory.Constants.GRADES).is(grade));
            query.addCriteria(Criteria.where(VStory.Constants.SECTION_TYPE).is(sectionType));
            if (lowerLimit != null && upperLimit != null)
                query.addCriteria(Criteria.where(VStory.Constants.START_TIME).gte(lowerLimit).lte(upperLimit));

            if (id != null) {
                query.addCriteria(Criteria.where(VStory.Constants._ID).ne(id));
            }
            query.addCriteria(Criteria.where(VStory.Constants.ENABLE).ne(!true));
            count = queryCount(query, VStory.class);
        }

        return count;
    }

    public VStory findById(String id)
    {
        logger.info("method : findById, id : {}", id);
        if (StringUtils.isNotEmpty(id))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VStory.Constants._ID).is(id));
            return findOne(query, VStory.class);
        }
        return null;
    }

    public List<VStory> fetchCurrentVStories(String grade, RequestType requestType, SectionType sectionType,boolean enable)
    {
        logger.info("method : fetchCurrentVStories, grade : {}", grade);
        if (StringUtils.isNotEmpty(grade) && StringUtils.isNotEmpty(sectionType.getValue()))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VStory.Constants.GRADES).is(grade));
            query.addCriteria(Criteria.where(VStory.Constants.SECTION_TYPE).is(sectionType));
            if (requestType.getValue().equals("APP"))
            {
                Long currentTime = System.currentTimeMillis();
                query.addCriteria(Criteria.where(VStory.Constants.START_TIME).lte(currentTime));
                query.addCriteria(Criteria.where(VStory.Constants.END_TIME).gte(currentTime));
            }
            if(enable){
                query.addCriteria(Criteria.where(VStory.Constants.ENABLE).ne(!enable));
            }else {
                query.addCriteria(Criteria.where(VStory.Constants.ENABLE).is(false));
            }
            Sort sort = Sort.by(Direction.DESC, VStory.Constants.START_TIME);
            query.with(sort);
            logger.info("Query to fetch current stories : " + query);

            return runQuery(query, VStory.class);
        }
        else
            return null;
    }

    public List<VStory> fetchArchivedVStories(String grade, RequestType requestType, SectionType sectionType,boolean enable,Integer start,Integer size)
    {
        logger.info("method : fetchCurrentVStories, grade : {}", grade);
        if (StringUtils.isNotEmpty(grade) && StringUtils.isNotEmpty(sectionType.getValue()))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VStory.Constants.GRADES).is(grade));
            query.addCriteria(Criteria.where(VStory.Constants.SECTION_TYPE).is(sectionType));
            if (requestType.getValue().equals("APP"))
            {
                Long currentTime = System.currentTimeMillis();
                query.addCriteria(Criteria.where(VStory.Constants.START_TIME).lte(currentTime));
                query.addCriteria(Criteria.where(VStory.Constants.END_TIME).gte(currentTime));
            }
            query.addCriteria(Criteria.where(VStory.Constants.ENABLE).ne(!enable));
            Sort sort = Sort.by(Direction.DESC, VStory.Constants.START_TIME);
            query.with(sort);
            logger.info("Query to fetch current stories : " + query);
            setFetchParameters(query,start,size);
            return runQuery(query, VStory.class);
        }
        else
            return null;
    }
}
