package com.vedantu.vstories.requests;

import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.enums.StoryType;
import com.vedantu.vstories.enums.Type;
import com.vedantu.vstories.pojo.ImageDetails;
import com.vedantu.vstories.pojo.VideoDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VStoryRequest
{
    @NotNull
    @Size(max = ReqLimits.VSTORY_TITLE_MAX, message = ReqLimitsErMsgs.VSTORY_TITLE_MAX)
    private String title;

    @NotNull
    private Set<String> grades;

    private Set<String> targets;

    private Set<String> mainTags;

    @NotNull
    private StoryType storyType;

    @NotNull
    private SectionType sectionType;

    private ImageDetails imageDetails;

    private VideoDetails videoDetails;

    @Size(max = ReqLimits.VSTORY_TITLE_MAX, message = ReqLimitsErMsgs.VSTORY_TITLE_MAX)
    private String ctaTitle;

    private String ctaDeepLink;

    @NotNull
    private Long startTime;

    @NotNull
    private Long endTime;

    private Type type;
    @Size(max = ReqLimits.VSTORY_TAG_MAX, message = ReqLimitsErMsgs.VSTORY_TAG_MAX)
    private String tag;
    private Long weightage;
    private boolean enable;

    private boolean enableAutoRun;
}
