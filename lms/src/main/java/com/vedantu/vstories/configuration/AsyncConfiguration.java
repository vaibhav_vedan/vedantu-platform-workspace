package com.vedantu.vstories.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.Executor;

/**
 *
 * @author Ausaf
 */

@Configuration
@EnableAsync
@EnableRetry
public class AsyncConfiguration
{
    @Bean(name = "VStoryAsyncTaskExecutor")
    public Executor asyncExecutor()
    {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.setCorePoolSize(15);
        threadPoolTaskExecutor.setMaxPoolSize(20);
        threadPoolTaskExecutor.setQueueCapacity(100);
        threadPoolTaskExecutor.setThreadNamePrefix("VStory Async Task -");
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }
}
