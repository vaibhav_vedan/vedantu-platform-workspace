package com.vedantu.vstories.responses;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VStoryBasicResponse
{
    private HttpStatus httpStatus;
    private String response;
}
