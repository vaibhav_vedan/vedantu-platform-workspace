package com.vedantu.vstories.responses;

import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.enums.StoryType;
import com.vedantu.vstories.enums.Type;
import com.vedantu.vstories.pojo.ImageDetails;
import com.vedantu.vstories.pojo.VideoDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Size;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VStoryResponse implements HomeFeedCardData
{
    private String id;

    private String title;

    private Set<String> grades;

    private Set<String> targets;

    private Set<String> mainTags;

    private StoryType storyType;

    private SectionType sectionType;

    private ImageDetails imageDetails;

    private VideoDetails videoDetails;

    private String ctaTitle;

    private String ctaDeepLink;

    private Long startTime;

    private Long endTime;

    private boolean enableAutoRun;

    private Type type;
    private String tag;
    private Long weightage;

}
