package com.vedantu.vstories.responses;

import com.vedantu.vstories.pojo.MuxPlaybackId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MuxResponse
{
    private String id;
    private List<MuxPlaybackId> playback_ids;
    private String duration;
    private String status;
}
