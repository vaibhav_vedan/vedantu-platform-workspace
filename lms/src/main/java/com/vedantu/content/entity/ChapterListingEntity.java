package com.vedantu.content.entity;

import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.content.pojo.Format;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;


@Document(collection = "ChapterListingEntity")
public class ChapterListingEntity extends AbstractMongoStringIdEntity {

	private String subject;
	@Indexed(unique = true, background = true)
    private String chapter;
	private List<Format> formats;

	public ChapterListingEntity() {
	}
	
//	@PersistenceConstructor
//	public ChapterListingEntity(String subject, String chapter, List<Format> formats) {
//		super();
//		this.subject = subject;
//		this.chapter = chapter;
//		this.formats = formats;
//	}

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String SUBJECT = "subject";
        public static final String CHAPTER = "chapter";
        public static final String FORMAT_TYPE = "formats.formatType";
        public static final String ID_URL = "formats.uRLs";
    }


	public String getSubject() {
		return subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getChapter() {
		return chapter;
	}


	public void setChapter(String chapter) {
		this.chapter = chapter;
	}


	public List<Format> getFormats() {
		return formats;
	}


	public void setFormats(List<Format> formats) {
		this.formats = formats;
	}
}
