package com.vedantu.content.res;

import java.util.List;

import com.vedantu.content.pojo.CMDSTestContentInfo;
import com.vedantu.content.pojo.CMDSVideoContentInfo;
import com.vedantu.content.pojo.FormatType;
import com.vedantu.subscription.pojo.BundleInfo;

public class FormatRes {

	private String formatType;
	
	private String exerciseName = null;
	
	private String pdfURL;
	
	private List<CMDSVideoContentInfo> videoContentInfos;
	
	private List<CMDSTestContentInfo> testContentInfos;
	
	private List<BundleInfo> courses;

	/**
	 * @return the formatType
	 */
	public String getFormatType() {
		return formatType;
	}

	/**
	 * @param formatType the formatType to set
	 */
	public void setFormatType(String formatType) {
		this.formatType = formatType;
	}

	/**
	 * @return the exerciseName
	 */
	public String getExerciseName() {
		return exerciseName;
	}

	/**
	 * @param exerciseName the exerciseName to set
	 */
	public void setExerciseName(String exerciseName) {
		this.exerciseName = exerciseName;
	}

	/**
	 * @return the pdfURL
	 */
	public String getPdfURL() {
		return pdfURL;
	}

	/**
	 * @param pdfURL the pdfURL to set
	 */
	public void setPdfURL(String pdfURL) {
		this.pdfURL = pdfURL;
	}

	/**
	 * @return the videoContentInfos
	 */
	public List<CMDSVideoContentInfo> getVideoContentInfos() {
		return videoContentInfos;
	}

	/**
	 * @param videoContentInfos the videoContentInfos to set
	 */
	public void setVideoContentInfos(List<CMDSVideoContentInfo> videoContentInfos) {
		this.videoContentInfos = videoContentInfos;
	}

	/**
	 * @return the testContentInfos
	 */
	public List<CMDSTestContentInfo> getTestContentInfos() {
		return testContentInfos;
	}

	/**
	 * @param testContentInfos the testContentInfos to set
	 */
	public void setTestContentInfos(List<CMDSTestContentInfo> testContentInfos) {
		this.testContentInfos = testContentInfos;
	}

	/**
	 * @return the courses
	 */
	public List<BundleInfo> getCourses() {
		return courses;
	}

	/**
	 * @param courses the courses to set
	 */
	public void setCourses(List<BundleInfo> courses) {
		this.courses = courses;
	}
}
