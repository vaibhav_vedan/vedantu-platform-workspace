package com.vedantu.content.res;

import java.util.List;

import lombok.Data;

@Data
public class ListingRes {
	
	private String subjectChapterImageUri;
	private String subjectHomeFeedImageUri;
	private List<String> chapters;
}
