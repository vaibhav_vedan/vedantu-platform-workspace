package com.vedantu.content.controllers;

import static com.vedantu.User.Role.ADMIN;
import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.io.IOException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.content.entity.ChapterListingEntity;
import com.vedantu.content.pojo.FormatType;
import com.vedantu.content.res.ChapterListingEntityRes;
import com.vedantu.content.res.ListingRes;
import com.vedantu.content.service.ContentFeedService;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("/content")
public class ContentFeedController {

    private Logger LOGGER = LogFactory.getLogger(ContentFeedController.class);
    private HttpSessionUtils sessionUtils;
    private ContentFeedService contentFeedService;

    @Autowired
    public ContentFeedController(HttpSessionUtils sessionUtils, ContentFeedService contentFeedService) {
        this.sessionUtils = sessionUtils;
        this.contentFeedService = contentFeedService;
    }
    
    @RequestMapping(value = "/saveContent", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ChapterListingEntity> saveContent(@RequestParam(value = "csvUrl", required = true) String csvUrl) throws VException, IOException {
    	LOGGER.info("Saving Content from URL : " + csvUrl);
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        return contentFeedService.saveChapterListingFromCSVUrl(csvUrl);
    }
    
    @RequestMapping(value = "/getPossibleTopics", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getPossibleTopics(@RequestParam(value = "searchString", required = true) String searchString) {
    	return contentFeedService.getPossibleTopics(searchString);
    }
    
    @RequestMapping(value = "/getChapterListingByTopic", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ChapterListingEntityRes getChapterListingByTopic(@RequestParam(value = "chapter", required = true) String chapter) {
    	return contentFeedService.getChapterListingByTopic(chapter);
    }
    
    @RequestMapping(value = "/getChaptersBySubject", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ListingRes getChaptersBySubject(@RequestParam(value = "subject") String subject) throws VException {
    	return contentFeedService.getChaptersBySubject(subject);
    }

}
