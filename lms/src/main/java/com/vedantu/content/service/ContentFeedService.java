package com.vedantu.content.service;


import java.io.IOException;
import java.util.List;

import com.vedantu.content.entity.ChapterListingEntity;
import com.vedantu.content.res.ChapterListingEntityRes;
import com.vedantu.content.res.ListingRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;

public interface ContentFeedService {

	public List<ChapterListingEntity> saveChapterListingFromCSVUrl(String csvUrl) throws IOException;
	public List<String> getPossibleTopics(String searchString);
	public ChapterListingEntityRes getChapterListingByTopic(String chapter);
	public ListingRes getChaptersBySubject(String subject) throws InternalServerErrorException, BadRequestException;
}
