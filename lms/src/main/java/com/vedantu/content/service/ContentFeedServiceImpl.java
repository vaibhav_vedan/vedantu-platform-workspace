package com.vedantu.content.service;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.cmds.dao.CMDSTestDAO;
import com.vedantu.cmds.dao.CMDSVideoDAO;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.content.dao.ChapterListingDAO;
import com.vedantu.content.entity.ChapterListingEntity;
import com.vedantu.content.pojo.CMDSTestContentInfo;
import com.vedantu.content.pojo.CMDSVideoContentInfo;
import com.vedantu.content.pojo.Format;
import com.vedantu.content.pojo.FormatType;
import com.vedantu.content.res.ChapterListingEntityRes;
import com.vedantu.content.res.FormatRes;
import com.vedantu.content.res.ListingRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Service
public class ContentFeedServiceImpl implements ContentFeedService {

	private Logger LOGGER = LogFactory.getLogger(ContentFeedServiceImpl.class);
	private static final String CSV_PATH = "/home/vedantu/Downloads/Journey_Path_Content_Sheet.csv";
	private final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
	//private static final String CSV_URL = "https://vedantudoubtsample1.s3-ap-southeast-1.amazonaws.com/Journey+Path+Content+Sheet+-+Final_CSV.csv";
	private ChapterListingDAO chapterListingDAO;
	private CMDSVideoDAO cMDSVideoDAO;
	private CMDSTestDAO cMDSTestDAO;
	private RedisDAO redisDAO;
	
	@Autowired
	public ContentFeedServiceImpl(ChapterListingDAO chapterListingDAO, CMDSVideoDAO cMDSVideoDAO, CMDSTestDAO cMDSTestDAO, RedisDAO redisDAO) {
		this.chapterListingDAO = chapterListingDAO;
		this.cMDSVideoDAO = cMDSVideoDAO;
		this.cMDSTestDAO = cMDSTestDAO;
		this.redisDAO = redisDAO;
	}
	
	@Override
	public List<ChapterListingEntity> saveChapterListingFromCSVUrl(String csvUrl) throws IOException {
		File csvFile = new File(CSV_PATH);
		URL csvURL = new URL(csvUrl);
		int connectionTimeOut = 10000;
		int readTimeOut = 10000;
		FileUtils.copyURLToFile(csvURL, csvFile, connectionTimeOut, readTimeOut);
		//String csvData = readCSVContent();
		return getObjectFromCSV();
	}
	
	@Override
	public List<String> getPossibleTopics(String searchString) {
		return chapterListingDAO.getPossibleTopics(searchString);
	}
	
	private List<ChapterListingEntity> getObjectFromCSV() throws IOException {
		List<ChapterListingEntity> chapterListings = new ArrayList<>();
		try { 
	        // Create an object of file reader 
	        // class with CSV file as a parameter. 
	        FileReader filereader = new FileReader(CSV_PATH); 
	  
	        // create csvReader object and skip first Line 
	        CSVReader csvReader = new CSVReaderBuilder(filereader) 
	                                  .withSkipLines(1) 
	                                  .build(); 
	        List<String[]> allData = csvReader.readAll(); 
	  
	        // print Data 
//	        for (String[] row : allData) { 
//	            for (String cell : row) { 
//	                LOGGER.info(cell + "\t"); 
//	            } 
//	        } 
	        for(int i=0; i < allData.size(); i++) {
		        ChapterListingEntity chapterListing = new ChapterListingEntity();
	        	String[] row = allData.get(i);
	        	String subject = row[0];
	        	LOGGER.info("subject : "+ subject); 
	        	String chapter = row[1];
	        	LOGGER.info("chapter : "+ chapter); 
	        	
	        	String solVids = row[2];      // Multiple

	        	String exerciseName = "";
	        	List<Format> solVidFormats = new ArrayList<Format>();

	        	if(StringUtils.isNotEmpty(solVids)) {
					for (String solVid : solVids.split("#")) {
						List<String> exerciseVids = new ArrayList<String>();
						exerciseName = solVid.split("@")[0].trim();
						LOGGER.info("exerciseName : " + exerciseName);
						exerciseVids.addAll(Arrays.asList(solVid.split("@")[1].trim().split("/")));

						for (String exerciseVid : exerciseVids) {
							exerciseVid = exerciseVid.trim();
							LOGGER.info("exerciseVid : " + exerciseVid);
						}

						Format solVidFormat = new Format();
						solVidFormat.setFormatType(FormatType.NCERT_SOLUTION);
						solVidFormat.setExerciseName(exerciseName);
						LOGGER.info("solVidFormat : " + solVidFormat);
						List<CMDSVideoContentInfo> videoContentInfos = new ArrayList<CMDSVideoContentInfo>();
						for (String id : exerciseVids) {
							CMDSVideo video = cMDSVideoDAO.getEntityById(id, CMDSVideo.class);
							CMDSVideoContentInfo videoContentInfo = new CMDSVideoContentInfo();
							videoContentInfo.setId(video.getId());
							videoContentInfo.setThumbnailUrl(video.getThumbnail());
							videoContentInfo.setVideoUrl(video.getVideoUrl());
							videoContentInfo.setTitle(video.getTitle());
							LOGGER.info("sol videoContentInfo : " + videoContentInfo);
							videoContentInfos.add(videoContentInfo);
						}
						solVidFormat.setVideoContentInfos(videoContentInfos);
						solVidFormats.add(solVidFormat);
					}
				}
	        	
	        	String nCERT_PDF = row[3];                 // Single
	        	String impQues = row[4];                   // Single
	        	String[] classVids = row[5].split("/");    // Multiple
	        	String[] microCourses = row[6].split("/"); // Multiple
	        	String[] tests = row[7].split("/");        // Multiple
	        	
	        	Format nCERT_PDFFormat = new Format();
	        	nCERT_PDFFormat.setFormatType(FormatType.NCERT_PDF);
	        	nCERT_PDFFormat.setPdfURL(nCERT_PDF);
	        	LOGGER.info("nCERT_PDFFormat : "+ nCERT_PDFFormat); 
	        	
	        	Format impQuesFormat = new Format();
	        	impQuesFormat.setFormatType(FormatType.IMPORTANT_QUESTION);
	        	impQuesFormat.setPdfURL(impQues);
	        	LOGGER.info("impQuesFormat : "+ impQuesFormat); 

	        	Format classVidsFormat = new Format();
	        	classVidsFormat.setFormatType(FormatType.CLASSES);
	        	List<CMDSVideoContentInfo> videoContentInfos = new ArrayList<CMDSVideoContentInfo>();
	        	for(String videoId : classVids) {
	        		CMDSVideo video = cMDSVideoDAO.getEntityById(videoId, CMDSVideo.class);
	        		CMDSVideoContentInfo videoContentInfo = new CMDSVideoContentInfo();
	        		videoContentInfo.setId(video.getId());
	        		videoContentInfo.setThumbnailUrl(video.getThumbnail());
	        		videoContentInfo.setVideoUrl(video.getVideoUrl());
	        		videoContentInfo.setTitle(video.getTitle());
		        	LOGGER.info("class videoContentInfo : "+ videoContentInfo); 
	        		videoContentInfos.add(videoContentInfo);
	        	}
	        	classVidsFormat.setVideoContentInfos(videoContentInfos);
	        	
	        	Format microCoursesFormat = new Format();
	        	microCoursesFormat.setFormatType(FormatType.MICRO_COURSE);
	        	List<BundleInfo> courses = new ArrayList<BundleInfo>();
	        	if(StringUtils.isNotEmpty(row[6])) {
					for (String courseId : microCourses) {
						String url = SUBSCRIPTION_ENDPOINT + "/bundle/getFullBundle/" + courseId;
						ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
						VExceptionFactory.INSTANCE.parseAndThrowException(response);
						String jsonString = response.getEntity(String.class);
						LOGGER.info("jsonString : " + jsonString);
						BundleInfo bundleInfo = new Gson().fromJson(jsonString, BundleInfo.class);
						LOGGER.info("bundleInfo : " + bundleInfo);
						courses.add(bundleInfo);
					}
				}
	        	microCoursesFormat.setCourses(courses);

	        	Format testsFormat = new Format();
	        	testsFormat.setFormatType(FormatType.TESTS);
				List<CMDSTestContentInfo> testContentInfos = new ArrayList<CMDSTestContentInfo>();
				for(String testID: tests) {
					CMDSTest test = cMDSTestDAO.getById(testID);
					CMDSTestContentInfo testContentInfo = new CMDSTestContentInfo();
					testContentInfo.setId(test.getId());
					testContentInfo.setMetadata(test.getMetadata());
					testContentInfo.setName(test.getName());
		        	LOGGER.info("testContentInfo : "+ testContentInfo); 
					testContentInfos.add(testContentInfo);
				}
				testsFormat.setTestContentInfos(testContentInfos);
	        	
	        	List<Format> formats = new ArrayList<Format>();
	        	formats.addAll(solVidFormats);
		        LOGGER.info("solVidFormats : " + solVidFormats);
	        	formats.add(nCERT_PDFFormat);
		        LOGGER.info("nCERT_PDFFormat : " + nCERT_PDFFormat);
	        	formats.add(impQuesFormat);
		        LOGGER.info("impQuesFormat : " + impQuesFormat);
	        	formats.add(classVidsFormat);
		        LOGGER.info("classVidsFormat : " + classVidsFormat);
	        	formats.add(microCoursesFormat);
		        LOGGER.info("microCoursesFormat : " + microCoursesFormat);
	        	formats.add(testsFormat);
		        LOGGER.info("testsFormat : " + testsFormat);
	        	
	        	
	        	chapterListing.setSubject(subject);
	        	chapterListing.setChapter(chapter);
	        	chapterListing.setFormats(formats);
	        	chapterListings.add(chapterListing);
	        }
	        LOGGER.info("ChapterListings : " + chapterListings);
	        
	    } 
	    catch (Exception e) { 
	        LOGGER.error(e.getLocalizedMessage()); 
	    } 
    	chapterListingDAO.save(chapterListings);

		return chapterListings;
	}

	@Override
	public ChapterListingEntityRes getChapterListingByTopic(String chapter) {
		ChapterListingEntityRes res = new ChapterListingEntityRes();
		List<FormatRes> formatRess = new ArrayList<>();
		ChapterListingEntity chapterListingEntity = new ChapterListingEntity();
		chapterListingEntity = chapterListingDAO.getChapterListingByTopic(chapter);
		List<Format> formats = new ArrayList<>();
		if(null != chapterListingEntity && null != chapterListingEntity.getFormats() && !ArrayUtils.isEmpty(chapterListingEntity.getFormats())) {
			formats = chapterListingEntity.getFormats();
			Collections.sort(formats, new Format.FormatComparator());
			chapterListingEntity.setFormats(formats);
			
			res.setChapter(chapterListingEntity.getChapter());
			res.setSubject(chapterListingEntity.getSubject());
			
			for(Format f: chapterListingEntity.getFormats()) {
				FormatRes formatRes = new FormatRes();
				formatRes.setCourses(f.getCourses());
				formatRes.setFormatType(f.getFormatType().getName());
				formatRes.setExerciseName(f.getExerciseName());
				formatRes.setPdfURL(f.getPdfURL());
				formatRes.setTestContentInfos(f.getTestContentInfos());
				formatRes.setVideoContentInfos(f.getVideoContentInfos());
				formatRess.add(formatRes);
			}
			res.setFormats(formatRess);
		}
		
		return res;
	}

	public ListingRes getChaptersBySubject(String subject) throws InternalServerErrorException, BadRequestException {
		ListingRes res = new ListingRes();
		res.setChapters(chapterListingDAO.getChaptersBySubject(subject));

		String subjectIconUrl = "";
		switch (subject) {
			case "Physics":
				subjectIconUrl = "/static/images/react-app/coursecard/physics.svg";
				break;
			case "Chemistry":
				subjectIconUrl = "/static/images/react-app/coursecard/chemistry.svg";
				break;
			case "Maths":
				subjectIconUrl = "/static/images/react-app/coursecard/maths.svg";
				break;
			case "Biology":
				subjectIconUrl = "/static/images/react-app/coursecard/biology.svg";
				break;
			case "Science":
				subjectIconUrl = "/static/images/react-app/coursecard/science.svg";
				break;
			default:
				subjectIconUrl = "";
		}

		res.setSubjectChapterImageUri(subjectIconUrl);
		res.setSubjectHomeFeedImageUri(subjectIconUrl);
		return res;
	}
	
}
