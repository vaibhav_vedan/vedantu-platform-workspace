package com.vedantu.content.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.content.entity.ChapterListingEntity;
import com.vedantu.content.pojo.CMDSTestContentInfo;
import com.vedantu.content.pojo.CMDSVideoContentInfo;
import com.vedantu.content.pojo.Format;
import com.vedantu.content.pojo.FormatType;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class ChapterListingDAO extends AbstractMongoDAO{

	private Logger LOGGER = LogFactory.getLogger(ChapterListingDAO.class);
	
	@Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void save(List<ChapterListingEntity> chapterListingEntities) {
    	for(ChapterListingEntity chapterListingEntity: chapterListingEntities) {
            Query query = new Query();
    		query.addCriteria(Criteria.where(ChapterListingEntity.Constants.CHAPTER).is(chapterListingEntity.getChapter()));
    		LOGGER.info("Query : " + query);
    		ChapterListingEntity entity = findOne(query, ChapterListingEntity.class);
    		LOGGER.info("Acquired Entity : " + entity);
    		
    		if(entity == null) {
        		LOGGER.info("Saving Entity : " + chapterListingEntity);
        		saveEntity(chapterListingEntity);
    		}
    		else {
        		LOGGER.info("Updating Entity : " + entity);
//                entity.setEntityDefaultProperties(null);
                Update update = getUpdateForChapterListingUpsert(entity);
                updateFirst(query, update, ChapterListingEntity.class);
    		}
    	}
    }
    
    public ChapterListingEntity getChapterListingByTopic(String chapter) {
    	Query query = new Query();
		query.addCriteria(Criteria.where(ChapterListingEntity.Constants.CHAPTER).is(chapter));
		
		return findOne(query, ChapterListingEntity.class);
    }
    
    private Update getUpdateForChapterListingUpsert(ChapterListingEntity chapterListingEntity) {
    	
        Update update = new Update();
        update.set("subject", chapterListingEntity.getSubject());
        update.set("chapter", chapterListingEntity.getChapter());
        update.set("formats", chapterListingEntity.getFormats());
        update.set("creationTime", chapterListingEntity.getCreationTime());
        update.set("createdBy", chapterListingEntity.getCreatedBy());
        update.set("lastUpdated", chapterListingEntity.getLastUpdated());
        update.set("lastUpdatedBy", chapterListingEntity.getLastUpdatedBy());
        update.set("entityState", chapterListingEntity.getEntityState());        

        return update;
    }

//	public List<String> getListingByFormatTypeAndChapter(FormatType formatType, String chapter) {
//		Query query = new Query();
//		query.addCriteria(Criteria.where(ChapterListingEntity.Constants.CHAPTER).is(chapter));
//		LOGGER.info("Query : " + query);
//		
//		List<String> iDs = new ArrayList<>();
//		List<ChapterListingEntity> chapterListings = runQuery(query, ChapterListingEntity.class);
//		LOGGER.info("Acquired Entity : " + chapterListings);
//		
//		for(ChapterListingEntity chapterListing : chapterListings) {
//			for(Format format : chapterListing.getFormats()) {
//				if(format.getFormatType().equals(formatType)) {
//					if(StringUtils.isNotEmpty(format.getPdfURL())) {
//						iDs.add(format.getPdfURL());
//					}
//					else if(format.getVideoContentInfos() != null) {
//						for(CMDSVideoContentInfo videoContentInfo : format.getVideoContentInfos()) {
//							iDs.add(videoContentInfo.getId());
//						}
//					}
//					else if(format.getTestContentInfos() != null) {
//						for(CMDSTestContentInfo testContentInfo : format.getTestContentInfos()) {
//							iDs.add(testContentInfo.getId());
//						}
//					}
//					else if(format.getCourses() != null) {
//						for(BundleInfo bundleInfo : format.getCourses()) {
//							iDs.add(bundleInfo.getId());
//						}
//					}
//				}
//			}
//			
//		}
//		return iDs;
//	}
	
	public List<String> getPossibleTopics(String searchString) {
		Query query = new Query();
		query.addCriteria(Criteria.where(ChapterListingEntity.Constants.CHAPTER).regex(Pattern.quote(searchString), "i"));
		LOGGER.info("Query : " + query);
		
		List<String> possibleTopics = new ArrayList<String>();
		List<ChapterListingEntity> chapterListings = runQuery(query, ChapterListingEntity.class);
		LOGGER.info("Acquired Entities : " + chapterListings);
		
		for(ChapterListingEntity chapterListing : chapterListings) {
			possibleTopics.add(chapterListing.getChapter());
		}
		LOGGER.info("Possible Topics : " + possibleTopics);
		return possibleTopics;
	}

	public List<String> getChaptersBySubject(String subject) {
		Query query = new Query();
		query.addCriteria(Criteria.where(ChapterListingEntity.Constants.SUBJECT).is(subject));
		
		List<String> chapters = new ArrayList<String>();
		List<ChapterListingEntity> chapterListingEntities = runQuery(query, ChapterListingEntity.class);
		
		for(ChapterListingEntity entity : chapterListingEntities) {
			chapters.add(entity.getChapter());
		}
		
		return chapters;
	}
}
