package com.vedantu.content.pojo;

import java.util.Comparator;
import java.util.List;

import com.vedantu.subscription.pojo.BundleInfo;

public class Format {

	private FormatType formatType;
	private String exerciseName = null;
	private String pdfURL;
	private List<CMDSVideoContentInfo> videoContentInfos;
	private List<CMDSTestContentInfo> testContentInfos;
	private List<BundleInfo> courses;
	
	public FormatType getFormatType() {
		return formatType;
	}
	
	public void setFormatType(FormatType formatType) {
		this.formatType = formatType;
	}
	
	public String getExerciseName() {
		return exerciseName;
	}
	
	public void setExerciseName(String exerciseName) {
		this.exerciseName = exerciseName;
	}

	public List<CMDSVideoContentInfo> getVideoContentInfos() {
		return videoContentInfos;
	}

	public void setVideoContentInfos(List<CMDSVideoContentInfo> videoContentInfos) {
		this.videoContentInfos = videoContentInfos;
	}

	public List<CMDSTestContentInfo> getTestContentInfos() {
		return testContentInfos;
	}

	public void setTestContentInfos(List<CMDSTestContentInfo> testContentInfos) {
		this.testContentInfos = testContentInfos;
	}

	public String getPdfURL() {
		return pdfURL;
	}

	public void setPdfURL(String pdfURL) {
		this.pdfURL = pdfURL;
	}

	public List<BundleInfo> getCourses() {
		return courses;
	}

	public void setCourses(List<BundleInfo> courses) {
		this.courses = courses;
	}

	public static class FormatComparator implements Comparator<Format> { 
		  
       	@Override
		public int compare(Format o1, Format o2) {
			
			Integer i1 = o1.getFormatType().getRank();
			Integer i2 = o2.getFormatType().getRank();
			return i1.compareTo(i2);
		} 
    } 
  
}
