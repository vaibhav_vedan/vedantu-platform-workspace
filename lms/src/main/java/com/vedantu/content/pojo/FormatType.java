package com.vedantu.content.pojo;

public enum FormatType {

	CLASSES("CLASSES", 1),
	NCERT_PDF("NCERT_PDF", 2),
	NCERT_SOLUTION("NCERT_SOLUTION", 3),
	IMPORTANT_QUESTION("IMPORTANT_QUESTION", 4),
	MICRO_COURSE("MICRO_COURSE", 5), 
	TESTS("TESTS", 6);
	
	private final String name;
	private final Integer rank;
	
	private FormatType(String name, Integer rank) {
		this.name = name;
		this.rank = rank;
	}
	
	public String getName() {
		return name;
	}
	
	public Integer getRank() {
		return rank;
	}
}
