package com.vedantu.content.pojo;

import java.util.List;

public class ChapterListing {

	private String subject;
	private String chapter;
	private List<Format> formats;

	public ChapterListing(String subject, String chapter, List<Format> formats) {
		super();
		this.subject = subject;
		this.chapter = chapter;
		this.formats = formats;
	}
	
	public String getSubject() {
		return subject;
	}
	
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getChapter() {
		return chapter;
	}
	
	public void setChapter(String chapter) {
		this.chapter = chapter;
	}
	
	public List<Format> getFormats() {
		return formats;
	}
	
	public void setFormats(List<Format> formats) {
		this.formats = formats;
	}

}
