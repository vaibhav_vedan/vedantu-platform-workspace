package com.vedantu.content.pojo;

import java.util.List;

import com.vedantu.cmds.pojo.TestMetadata;

public class CMDSTestContentInfo {

	private String id;
	private String name;
	private List<TestMetadata> metadata;
	
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<TestMetadata> getMetadata() {
		return metadata;
	}

	public void setMetadata(List<TestMetadata> metadata) {
		this.metadata = metadata;
	}
}
