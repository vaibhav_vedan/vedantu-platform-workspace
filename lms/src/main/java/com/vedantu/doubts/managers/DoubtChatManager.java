/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.cmds.dao.sql.SqlSessionFactory;
import com.vedantu.User.DexInfo;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.cmds.managers.AmazonS3Manager;
import com.vedantu.cmds.managers.challenges.CommunicationManager;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.doubts.dao.DoubtChatMessageDAO;
import com.vedantu.doubts.dao.DoubtsDAO;
import com.vedantu.doubts.entities.mongo.DoubtChatMessage;
import com.vedantu.doubts.entities.mongo.Doubt;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.enums.StudentDoubtStatus;
import com.vedantu.doubts.pojo.*;
import com.vedantu.doubts.request.*;
import com.vedantu.doubts.response.UserResponse;
import com.vedantu.doubts.response.UserResponseChat;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.enums.DoubtPaidState;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.util.*;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.*;
import javax.mail.internet.InternetAddress;
import org.apache.logging.log4j.Logger;
import org.apache.xpath.operations.Bool;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class DoubtChatManager {


    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private DoubtsDAO doubtsDAO;

    @Autowired
    private DoubtChatMessageDAO doubtChatMessageDAO;

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private RedisDAO redisDAO;


    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DoubtChatManager.class);
    private static String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private static int T1_CHAT_WAIT_TIME = ConfigUtils.INSTANCE.getIntValue("doubts.chat.expiry");
    private static String PAID_USERS = ConfigUtils.INSTANCE.getStringValue("doubt.paid.users");
    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");



    public GetDoubtChatMessages getDoubtChatMessages(Long doubtId, int start, int size, String userId, Role role) throws InternalServerErrorException, NotFoundException, ForbiddenException{
        Doubt doubt = (Doubt)doubtsDAO.getEntityById(doubtId, Doubt.class);
        GetDoubtChatMessages getDoubtChatMessages = new GetDoubtChatMessages();
        if(doubt == null){
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, "");
        }
        if(!Role.ADMIN.equals(role)){

            if(!(doubt.getStudentId().equals(userId) || (doubt.getAcceptedBy() != null && doubt.getAcceptedBy().equals(userId)))){
                throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_MESSAGES, "");
            }

        }

        List<DoubtChatMessage> doubtChatMessages = doubtChatMessageDAO.getChatMessages(doubtId, start, size);

        getDoubtChatMessages.setClosedTime(doubt.getClosedTime());
        getDoubtChatMessages.setDoubtState(doubt.getDoubtState());

        if(ArrayUtils.isEmpty(doubtChatMessages)){
            return getDoubtChatMessages;
        }

        if(Role.TEACHER.equals(role)){
            DoubtChatMessage doubtChatMessage = doubtChatMessageDAO.getLastMessageByTeacher(doubtId);
            if(doubtChatMessage != null){
                getDoubtChatMessages.setLastTeacherTime(doubtChatMessage.getCreationTime());
            }else{
                if(ArrayUtils.isNotEmpty(doubt.getDoubtSolverPojoList())){

                    for(DoubtSolverPojo doubtSolverPojo : doubt.getDoubtSolverPojoList()){
                        if(doubtSolverPojo.getSolverId().equals(doubt.getAcceptedBy())){
                            getDoubtChatMessages.setLastTeacherTime(doubtSolverPojo.getAcceptedTime());
                            break;
                        }
                    }
                    //getDoubtChatMessages.setLastTeacherTime();
                }
            }
        }

        List<DoubtChatMessagePojo> doubtChatMessagePojos = new ArrayList<>();

        for(DoubtChatMessage doubtChatMessage : doubtChatMessages){
            updateAwsUrl(doubtChatMessage);
            DoubtChatMessagePojo doubtChatMessagePojo = new DoubtChatMessagePojo();
            doubtChatMessagePojo.setCreationTime(doubtChatMessage.getCreationTime());
            doubtChatMessagePojo.setDoubtId(doubtChatMessage.getDoubtId());
            doubtChatMessagePojo.setFromId(doubtChatMessage.getFromId());
            doubtChatMessagePojo.setFromRole(doubtChatMessage.getFromRole());
            doubtChatMessagePojo.setPicUrl(doubtChatMessage.getPicUrl());
            doubtChatMessagePojo.setRead(doubtChatMessage.isRead());
            doubtChatMessagePojo.setReadTime(doubtChatMessage.getReadTime());
            doubtChatMessagePojo.setText(doubtChatMessage.getText());
            doubtChatMessagePojo.setMessageId(doubtChatMessage.getId());
            doubtChatMessagePojos.add(doubtChatMessagePojo);
        }

        getDoubtChatMessages.setDoubtChatMessagePojos(doubtChatMessagePojos);
        return getDoubtChatMessages;

    }



    public List<DoubtChatPojo> getDoubtChatPojos(String userId, int start, int size, Role role, StudentDoubtStatus studentDoubtStatus){

        List<Doubt> doubtStatses = doubtsDAO.getDoubtStatsForUser(userId, start, size, studentDoubtStatus, role);

        List<DoubtChatPojo> doubtChatPojos = new ArrayList<>();

        if(ArrayUtils.isEmpty(doubtStatses)){
            return new ArrayList<>();
        }
        // Need to find a better way
        // To do
        for(Doubt doubtStats : doubtStatses){
            Long unreadMessages = doubtChatMessageDAO.getCountOfUnread(doubtStats.getId(), doubtStats.getStudentId(), role);
            if(unreadMessages==null){
                unreadMessages = 0l;
            }
            updateAwsUrl(doubtStats);
            DoubtChatMessage doubtChatMessage = doubtChatMessageDAO.getLastChatMessage(doubtStats.getId());
            DoubtChatPojo doubtChatPojo = new DoubtChatPojo();
            doubtChatPojo.setTargetGrades(doubtStats.getTargetGrades());
            doubtChatPojo.settGrades(doubtStats.gettGrades());
            doubtChatPojo.settTargets(doubtStats.gettTargets());
            doubtChatPojo.settTopics(doubtStats.gettTopics());
            doubtChatPojo.settSubjects(doubtStats.gettSubjects());
            doubtChatPojo.setDoubtId(doubtStats.getId());
            doubtChatPojo.setDoubtState(doubtStats.getDoubtState());
            doubtChatPojo.setLastActivity(doubtStats.getLastActivityTime());
            doubtChatPojo.setStudentId(doubtStats.getStudentId());
            doubtChatPojo.setDoubtSpamType(doubtStats.getDoubtSpamType());

            doubtChatPojo.setDoubtId(doubtStats.getId());
            doubtChatPojo.setDoubtState(doubtStats.getDoubtState());
            doubtChatPojo.setLastActivity(doubtStats.getLastActivityTime());
            doubtChatPojo.setStudentId(doubtStats.getStudentId());
            doubtChatPojo.setDoubtSpamType(doubtStats.getDoubtSpamType());
            if(doubtStats.getUserDoubtFeedback() != null){
                doubtChatPojo.setUserFeedback(true);
            }else{
                doubtChatPojo.setUserFeedback(false);
            }
            for(DoubtStateChangePojo doubtStateChangePojo: doubtStats.getDoubtStateChanges()){
                if(DoubtState.DOUBT_T1_ACCEPTED.equals(doubtStateChangePojo.getNewState())){
                    doubtChatPojo.setOpened(true);
                }
            }
            updateAwsUrl(doubtChatMessage);
            DoubtChatMessagePojo doubtChatMessagePojo = new DoubtChatMessagePojo(doubtChatMessage);
            if(doubtChatMessagePojo.getFromId().equals(doubtStats.getStudentId())){
                doubtChatMessagePojo.setFromRole(Role.STUDENT);
            }else{
                doubtChatMessagePojo.setFromRole(Role.TEACHER);
            }
            doubtChatMessagePojo.setMessageId(doubtChatMessage.getId());
            doubtChatPojo.setPicUrl(doubtStats.getPicUrl());
            doubtChatPojo.setDoubtChatMessagePojo(doubtChatMessagePojo);
            doubtChatPojo.setNumOfUnread(unreadMessages.intValue());

            doubtChatPojos.add(doubtChatPojo);

        }

        return doubtChatPojos;
    }


    public void createFirstDoubtMessage(Doubt doubt){

        DoubtChatMessage doubtChatMessage = new DoubtChatMessage();
        doubtChatMessage.setDoubtId(doubt.getId());
        doubtChatMessage.setFromId(doubt.getStudentId());
        doubtChatMessage.setPicUrl(doubt.getPicUrl());
        doubtChatMessage.setPicFileName(doubt.getPicFileName());
        doubtChatMessage.setText(doubt.getText());
        doubtChatMessage.setRead(true);
        doubtChatMessage.setFromRole(Role.STUDENT);
        doubtChatMessage.setReadTime(System.currentTimeMillis());
        doubtChatMessageDAO.save(doubtChatMessage, null);

    }

    /* public void doubtAccepted(String userId, Long doubtId, TeacherPoolType teacherPoolType, Role role){
         Doubt doubt = doubtStatsDAO.getDoubtStatsByDoubtId(doubtId);
         Long currentTime = System.currentTimeMillis();
         doubt.getMembersList().add(userId);
         DoubtSolverPojo doubtSolverPojo = new DoubtSolverPojo();
         if(TeacherPoolType.T1_POOL.equals(teacherPoolType)){
             doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_POSTED, DoubtState.DOUBT_T1_ACCEPTED));
         }else{
             doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T2_POOL, DoubtState.DOUBT_T2_ACCEPTED));
         }

         doubtSolverPojo.setRole(role);
         doubtSolverPojo.setAcceptedTime(currentTime);
         doubtSolverPojo.setSolverId(userId);
         doubtSolverPojo.setTeacherPoolType(teacherPoolType);
         if(ArrayUtils.isEmpty(doubt.getDoubtSolverPojoList())){
             doubt.setDoubtSolverPojoList(new ArrayList<>());
         }
         doubt.getDoubtSolverPojoList().add(doubtSolverPojo);
         doubt.setLastActivityTime(currentTime);
         doubtStatsDAO.save(doubt, Long.parseLong(userId));
     }



     public void doubtUnableToSolve(Long doubtId, String userId){
         Doubt doubt = doubtStatsDAO.getDoubtStatsByDoubtId(doubtId);
         doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(doubtId, DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T1_UNABLE_TO_SOLVE));
         doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(doubtId, DoubtState.DOUBT_T1_UNABLE_TO_SOLVE, DoubtState.DOUBT_T2_POOL));
         doubt.getMembersList().remove(userId);
         doubtStatsDAO.save(doubt, null);
     }

     public void doubtSolved(Long doubtId, TeacherPoolType teacherPoolType){
         Doubt doubt = doubtStatsDAO.getDoubtStatsByDoubtId(doubtId);
         if(TeacherPoolType.T1_POOL.equals(teacherPoolType)){
             doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(doubtId, DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T1_SOLVED));
         }else if(TeacherPoolType.T2_POOL.equals(teacherPoolType)){
             doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(doubtId, DoubtState.DOUBT_T2_ACCEPTED, DoubtState.DOUBT_T2_SOLVED));
         }
         doubtStatsDAO.save(doubt, null);
     }

     public void markDoubtAsSpam(String doubtId, TeacherPoolType teacherPoolType){
         Doubt doubtStats = doubtStatsDAO.get;
         if(TeacherPoolType.T1_POOL.equals(teacherPoolType)){
             doubtStats.getDoubtStateChanges().add(new DoubtStateChangePojo(System.currentTimeMillis(), DoubtState.DOUBT_POSTED, DoubtState.DOUBT_MARKED_SPAM));
         }else if(TeacherPoolType.T2_POOL.equals(teacherPoolType)){
             doubtStats.getDoubtStateChanges().add(new DoubtStateChangePojo(System.currentTimeMillis(), DoubtState.DOUBT_T2_POOL, DoubtState.DOUBT_MARKED_SPAM));
         }
         doubtStats.setClosed(true);
         doubtStatsDAO.save(doubtStats, null);
     }

     public void doubtUserFeedback(Long doubtId){
         Doubt doubtStats = doubtStatsDAO.getDoubtStatsByDoubtId(doubtId);
         // doubtStats.setUserFeedback(true);
         doubtStatsDAO.save(doubtStats, null);
     }
     */
    public void markAsRead(List<String> messageIds, Long doubtId, String userId, Role role) throws ForbiddenException{

        Doubt doubtStats = doubtsDAO.getDoubtStatsByDoubtId(doubtId);

        if(Role.STUDENT.equals(role) && !userId.equals(doubtStats.getStudentId())){
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_MARK_MESSAGES_AS_READ, "");
        }

        if(Role.TEACHER.equals(role) || Role.STUDENT.equals(role)){

            if(!doubtStats.getMembersList().contains(userId)){
                throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_MARK_MESSAGES_AS_READ, "");
            }

            List<DoubtChatMessage> doubtMessages = doubtChatMessageDAO.getDoubtChatMessages(messageIds);
            List<DoubtChatMessage> newMessages = new ArrayList<>();
            for(DoubtChatMessage doubtChatMessage : doubtMessages){
                if(!doubtChatMessage.getFromId().equals(userId)){
                    DoubtChatMessage doubtChatMessage1 = doubtChatMessage;
                    doubtChatMessage1.setRead(true);
                    doubtChatMessage1.setReadTime(System.currentTimeMillis());
                    newMessages.add(doubtChatMessage1);
                }
            }
            logger.info(newMessages);
            for(DoubtChatMessage doubtChatMessage : newMessages){
                doubtChatMessageDAO.save(doubtChatMessage, null);
            }

        }

    }

    public UnreadMessagesCount getUnreadCount(String userId) throws VException {
        UnreadMessagesCount unreadMessagesCount = new UnreadMessagesCount();

        UserBasicInfo ubi = fosUtils.getUserBasicInfo(userId, false);

        String grade = ubi.getGrade();

        if (grade == null || grade.trim().length() == 0 || !"12345678910111213".contains(grade))
            return null;

        AmplifyEvents ampEvents = new AmplifyEvents();
        try {
            String low = redisDAO.get("AMPLIFY-EVENTS-LOW");
            String high = redisDAO.get("AMPLIFY-EVENTS-HIGH");
            String moderate = redisDAO.get("AMPLIFY-EVENTS-MODERATE");
            String master = redisDAO.get("AMPLIFY-EVENTS-MASTER");
            ampEvents.setHigh(Integer.parseInt(high));
            ampEvents.setLow(Integer.parseInt(low));
            ampEvents.setModerate(Integer.parseInt(moderate));
            ampEvents.setMaster(Integer.parseInt(master));
        } catch (Exception e) {
            ampEvents.setHigh(1);
            ampEvents.setLow(1);
            ampEvents.setModerate(1);
            ampEvents.setMaster(1);
        }
        unreadMessagesCount.setAmplifyEvents(ampEvents);

        AmplifyEvents firebaseEvents = new AmplifyEvents();
        try {
            String low = redisDAO.get("FIREBASE-EVENTS-LOW");
            String high = redisDAO.get("FIREBASE-EVENTS-HIGH");
            String moderate = redisDAO.get("FIREBASE-EVENTS-MODERATE");
            String master = redisDAO.get("FIREBASE-EVENTS-MASTER");
            firebaseEvents.setHigh(Integer.parseInt(high));
            firebaseEvents.setLow(Integer.parseInt(low));
            firebaseEvents.setModerate(Integer.parseInt(moderate));
            firebaseEvents.setMaster(Integer.parseInt(master));
        } catch (Exception e) {
            firebaseEvents.setHigh(1);
            firebaseEvents.setLow(1);
            firebaseEvents.setModerate(1);
            firebaseEvents.setMaster(1);
        }
        unreadMessagesCount.setFirebaseEvents(firebaseEvents);


//        // Todo remove this hardcoding when course or tracks thing gets finalized
//        List<String> allowedUserIds = Arrays.asList("4102376732869805",
//                "4102376732893401",
//                "4102376732850507",
//                "4102376732843345",
//                "4102376732798099",
//                "4102376732823155");
//
//        for(String str: allowedUserIds) {
//            if(str.trim().contains(userId)) {
//                unreadMessagesCount.setMaxDoubtCount(Integer.parseInt("50"));
//                unreadMessagesCount.setUnreadMessageCountText("You have unlimited doubts");
//                break;
//            }
//        }


        String startTimeKey = getDexStartTimeKey();
        String endTimeKey = getDexEndTimeKey();
        try {
            String v1 = redisDAO.get(startTimeKey);
            String v2 = redisDAO.get(endTimeKey);
            unreadMessagesCount.setDexStartTime(Integer.parseInt(v1));
            unreadMessagesCount.setDexEndTime(Integer.parseInt(v2));
        } catch (Exception e) {
            unreadMessagesCount.setDexStartTime(0);
            unreadMessagesCount.setDexEndTime(10);
            logger.error("Error in redis get for DEX START END TIME " + e.getMessage());
        }

        int currentHour = DateTimeUtils.getISTHourCurrentTime();
        logger.info("CURRENT-HOUR");
        logger.info(currentHour);
        if(currentHour >= unreadMessagesCount.getDexStartTime() || currentHour < unreadMessagesCount.getDexEndTime()) {
            // ignore
            unreadMessagesCount.setPopupText("It seems like you’re a bit late. Our doubt experts are available between 8am - 10pm everyday. Please come back tomorrow to ask your doubt :)");
            unreadMessagesCount.setShowPopup(true);
            unreadMessagesCount.setAllowDoubts(false);
        }


        String key;
        if(StringUtils.isNotEmpty(ubi.getGrade())) {
            key = getMaxAllowedKeyForGrade(ubi.getGrade());
        } else {
            key = getUserDetailsKey();
        }

        try {
            String value = redisDAO.get(key);
            logger.info("MAX_USER_DOUBT_ALLOWED value: " + value);
            if(StringUtils.isEmpty(unreadMessagesCount.getUnreadMessageCountText())) {
                unreadMessagesCount.setMaxDoubtCount(Integer.parseInt(value));
            }
        } catch (Exception e) {
            unreadMessagesCount.setMaxDoubtCount(8);
//            logger.error("Error in redis get for MAX USER DOUBT ALLOWED " + e.getMessage());
        }


        Long dayStartTime = DateTimeUtils.getISTDayStartTime(System.currentTimeMillis());

        boolean paidUser = false;

        String paidUsersString = redisDAO.get("PAID_USERS");
        List<String> paidUsersList = new ArrayList<>();
        if(StringUtils.isNotEmpty(paidUsersString)) {
            paidUsersList = Arrays.asList(paidUsersString.split(","));
        }
        if(ArrayUtils.isNotEmpty(paidUsersList) && paidUsersList.contains(userId)) {
            logger.info("MARKED-PAID-DOUBT");
            paidUser = true;
            try {
                String value = redisDAO.get("MOBILE_APP_PAID_USERS_MAX_DOUBTS");
                logger.info("MOBILE_APP_PAID_USERS_MAX_DOUBTS value: " + value);
                unreadMessagesCount.setMaxDoubtCount(Integer.parseInt(value));
                unreadMessagesCount.setUnreadMessageCountText("You have unlimited doubts");
            } catch (Exception e) {
                logger.error("Error in redis get for MOBILE_APP_PAID_USERS_MAX_DOUBTS " + e.getMessage());
            }
        } else {
            String url = DINERO_ENDPOINT + "/payment/isDoubtsPaidUser?";

            url = url + "userId=" + userId;

            logger.info("PAID-USER-DINERO-URL: " + url);

            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String respString = resp.getEntity(String.class);

            Type filterListType = new TypeToken<Map<String, Boolean>>() {
            }.getType();

            Map<String, Boolean> userInDoubt = new Gson().fromJson(respString, filterListType);
            logger.info("PAID-USER-DINERO-VERIFIED");
            logger.info(userInDoubt);

            if(userInDoubt.get("isPaid") != null && userInDoubt.get("isPaid") == true) {
                paidUser = true;
                try {
                    String value = redisDAO.get("MOBILE_APP_PAID_USERS_MAX_DOUBTS");
                    logger.info("MOBILE_APP_PAID_USERS_MAX_DOUBTS value: " + value);
                    unreadMessagesCount.setMaxDoubtCount(Integer.parseInt(value));
                    unreadMessagesCount.setUnreadMessageCountText("You have unlimited doubts");
                } catch (Exception e) {
                    logger.error("Error in redis get for MOBILE_APP_PAID_USERS_MAX_DOUBTS " + e.getMessage());
                }
            }

        }

        List<Doubt> doubts = new ArrayList<>();
        int start = 0;
        int size = 100;
        while(true){
            List<Doubt> tempDoubts = doubtsDAO.getDoubtStatsForUser(userId, start, size);

            if(ArrayUtils.isEmpty(tempDoubts)){
                break;
            }

            doubts.addAll(tempDoubts);

            if(tempDoubts.size() < size){
                break;
            }
            start = start + size;
        }

        List<Long> doubtIds = new ArrayList<>();
        int todayDoubts = 0;

        int gotOne = 0;

        for(Doubt doubt : doubts){
            if(DoubtState.DOUBT_T1_SOLVED.equals(doubt.getDoubtState()) || DoubtState.DOUBT_T2_SOLVED.equals(doubt.getDoubtState())) {
                if(doubt.getUserDoubtFeedback() == null && unreadMessagesCount.getFeedbackRequiredFor() == null && gotOne == 0) {
                    unreadMessagesCount.setFeedbackRequiredFor(doubt.getId());
                } else {
                    gotOne = 1;
                }
            }
            doubtIds.add(doubt.getId());
            if(doubt.getCreationTime() > dayStartTime){
                todayDoubts = todayDoubts + 1 ;
            }
        }

        Long unreadMessages = doubtChatMessageDAO.getCountOfUnread(doubtIds, userId, Role.STUDENT);

        unreadMessagesCount.setUnreadCount(unreadMessages.intValue());
        unreadMessagesCount.setDoubtsCount(todayDoubts);

        if(StringUtils.isEmpty(unreadMessagesCount.getUnreadMessageCountText())) {
            unreadMessagesCount.setUnreadMessageCountText("You have " + (unreadMessagesCount.getMaxDoubtCount() - unreadMessagesCount.getDoubtsCount()) + " doubts");
        }

        if(StringUtils.isEmpty(unreadMessagesCount.getPopupText()) && unreadMessagesCount.getMaxDoubtCount().equals(unreadMessagesCount.getDoubtsCount())) {
            unreadMessagesCount.setPopupText("You have reached your daily limit. Please come back tomorrow to ask more doubts.");
            unreadMessagesCount.setShowPopup(true);
            unreadMessagesCount.setAllowDoubts(false);
        }


        // Global check on grade
        if(!paidUser) {
            Long gradeDayDoubtCount = doubtsDAO.getDoubtsCountForGrade(ubi.getGrade(), dayStartTime);
            String globalKey = getGlobalAllowedKeyForGrade(ubi.getGrade());
            try {
                String value = redisDAO.get(globalKey);
                logger.info("GLOBAL_USER_DOUBT_ALLOWED value: " + value);
                if (gradeDayDoubtCount >= Long.valueOf(value)) {
                    if (StringUtils.isEmpty(unreadMessagesCount.getPopupText())) {
                        unreadMessagesCount.setPopupText("Doubt volume surge alert!. Our experts are currently occupied solving doubts please come back tomorrow to ask your doubt");
                        unreadMessagesCount.setShowPopup(true);
                        unreadMessagesCount.setAllowDoubts(false);
                    }
                }
            } catch (Exception e) {
                logger.error("Error in redis get for GLOBAL USER DOUBT ALLOWED " + e.getMessage());
            }
        }


        try {
            String homePageBannerImage = redisDAO.get("HOME_PAGE_BANNER_IMAGE_URL");
            logger.info("HOME_PAGE_BANNER_IMAGE_URL value: " + homePageBannerImage);
            unreadMessagesCount.setHomePageBannerImage(homePageBannerImage);
            if(StringUtils.isNotEmpty(homePageBannerImage)) {
                unreadMessagesCount.setHomePageShow(true);
            }
            String homePageBannerUrl = redisDAO.get("HOME_PAGE_BANNER_URL");
            logger.info("HOME_PAGE_BANNER_URL value: " + homePageBannerUrl);
            unreadMessagesCount.setHomePageBannerUrl(homePageBannerUrl);

            String studyPageBannerImage = redisDAO.get("STUDY_PAGE_BANNER_IMAGE_URL");
            logger.info("STUDY_PAGE_BANNER_IMAGE_URL value: " + studyPageBannerImage);
            unreadMessagesCount.setStudyPageBannerImage(studyPageBannerImage);
            if(StringUtils.isNotEmpty(studyPageBannerImage)) {
                unreadMessagesCount.setStudyPageShow(true);
            }
            String studyPageBannerUrl = redisDAO.get("STUDY_PAGE_BANNER_URL");
            logger.info("STUDY_PAGE_BANNER_URL value: " + studyPageBannerUrl);
            unreadMessagesCount.setStudyPageBannerUrl(studyPageBannerUrl);
        } catch (Exception e) {
            logger.error("Error in redis get for BANNER details " + e.getMessage());
        }


        if(unreadMessagesCount.getDoubtsCount() < 0) {
            unreadMessagesCount.setDoubtsCount(0);
        }

        return unreadMessagesCount;
    }

    public PlatformBasicResponse doubtSupportMessage(DoubtHelpReq req, Long userId) throws UnsupportedEncodingException, VException{
        Map<String, Object> bodyScopes = new HashMap<>();

        User user = fosUtils.getUserInfo(userId, true);

        if(user == null){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "");
        }

        bodyScopes.put("emailId", req.getEmailId());
        bodyScopes.put("message", req.getMessage());
        bodyScopes.put("phoneNumber", user.getContactNumber());
        bodyScopes.put("fullName", user.getFullName());
        if(Role.TEACHER.equals(user.getRole())){
            if(user.getTeacherInfo().getDexInfo()!=null){
                DexInfo dexInfo = user.getTeacherInfo().getDexInfo();
                bodyScopes.put("grade", String.join( ",",dexInfo.gettGrades()));
                bodyScopes.put("board", String.join( ",",dexInfo.gettTargets()));
                bodyScopes.put("target", String.join( ",",dexInfo.gettTargets()));
            }
        }else{
            bodyScopes.put("grade", user.getStudentInfo().getGrade());
            bodyScopes.put("board", user.getStudentInfo().getBoard());
            bodyScopes.put("target", String.join( ",",user.getStudentInfo().getExamTargets()));
        }



        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress("vcare@vedantu.com", "VCare"));
        EmailRequest emailRequest = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.DOUBT_HELP_SUPPORT, user.getRole());
        communicationManager.sendEmail(emailRequest);
        return new PlatformBasicResponse(true, null, null);


    }


    public void updateAwsUrl(DoubtChatMessage doubtChatMessage){
        if(StringUtils.isNotEmpty(doubtChatMessage.getPicFileName()) && !doubtChatMessage.getPicFileName().equals("false")){
            doubtChatMessage.setPicUrl(amazonS3Manager.getDoubtsImageUrl(doubtChatMessage.getPicFileName()));
        }
    }

    public void updateAwsUrl(Doubt doubt){
        if(StringUtils.isNotEmpty(doubt.getPicFileName()) && !doubt.getPicFileName().equals("false")){
            doubt.setPicUrl(amazonS3Manager.getDoubtsImageUrl(doubt.getPicFileName()));
        }
    }

    public String getMaxAllowedKeyForGrade(String grade) {
        if(grade.equals("6")) {
            return "MAX_USER_DOUBT_ALLOWED_6";
        }
        if(grade.equals("7")) {
            return "MAX_USER_DOUBT_ALLOWED_7";
        }
        if(grade.equals("8")) {
            return "MAX_USER_DOUBT_ALLOWED_8";
        }
        if(grade.equals("9")) {
            return "MAX_USER_DOUBT_ALLOWED_9";
        }
        if(grade.equals("10")) {
            return "MAX_USER_DOUBT_ALLOWED_10";
        }
        if(grade.equals("11")) {
            return "MAX_USER_DOUBT_ALLOWED_11";
        }
        if(grade.equals("12")) {
            return "MAX_USER_DOUBT_ALLOWED_12";
        }
        if(grade.equals("13")) {
            return "MAX_USER_DOUBT_ALLOWED_13";
        }
        else {
            return null;
        }
    }

    public String getGlobalAllowedKeyForGrade(String grade) {
        if(StringUtils.isNotEmpty(grade)) {
            if (grade.equals("1")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_1";
            }
            if (grade.equals("2")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_2";
            }
            if (grade.equals("3")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_3";
            }
            if (grade.equals("4")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_4";
            }
            if (grade.equals("5")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_5";
            }
            if (grade.equals("6")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_6";
            }
            if (grade.equals("7")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_7";
            }
            if (grade.equals("8")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_8";
            }
            if (grade.equals("9")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_9";
            }
            if (grade.equals("10")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_10";
            }
            if (grade.equals("11")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_11";
            }
            if (grade.equals("12")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_12";
            }
            if (grade.equals("13")) {
                return "GLOBAL_USER_DOUBT_ALLOWED_13";
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    public String getUserDetailsKey(){
        return "MAX_USER_DOUBT_ALLOWED";
    }

    public String getDexStartTimeKey() {
        return "DEX_DOUBT_ALLOWED_START_TIME";
    }

    public String getDexEndTimeKey() {
        return "DEX_DOUBT_ALLOWED_END_TIME";
    }

	public String appendPaidViewers(AddPaidViewerReq req) throws InternalServerErrorException, BadRequestException {
		// Set Paid users backup if redis key gets reset
    	if (StringUtils.isEmpty(redisDAO.get("PAID_USERS")))
			redisDAO.set("PAID_USERS", PAID_USERS);
		String[] initialPaidUsers = redisDAO.get("PAID_USERS").split(",");
		logger.info("Previous Paid Users : " + initialPaidUsers);

		LinkedHashSet<String> paidUsers = new LinkedHashSet<String>();
		
		for(String paidUser : initialPaidUsers) {
			paidUsers.add(paidUser);
 		}
		logger.info("Previous Paid Users (LinkedHashSet) : " + paidUsers);
		logger.info("New Paid Users : " + req.getPaidViewerIds());
 		
		paidUsers.addAll(req.getPaidViewerIds());
		logger.info("Added Paid Users (LinkedHashSet) : " + paidUsers);

		String usersToAdd = String.join(",", paidUsers);
		redisDAO.set("PAID_USERS", usersToAdd);
 		
 		return redisDAO.get("PAID_USERS");
	}

}
