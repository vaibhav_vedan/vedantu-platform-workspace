/*
  * To change this license header, choose License Headers in Project Properties.
  * To change this template file, choose Tools | Templates
  * and open the template in the editor.
 */
package com.vedantu.doubts.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.User.enums.ViewerAccess;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.cmds.entities.*;
import com.vedantu.cmds.dao.TopicTreeDAO;
import com.vedantu.cmds.enums.AsyncTaskName;
import com.vedantu.cmds.managers.AmazonS3Manager;
import com.vedantu.cmds.managers.CMDSVideoManager;
import com.vedantu.cmds.managers.StudyManager;
import com.vedantu.cmds.pojo.PlaylistVideoPojo;
import com.vedantu.cmds.request.GetCMDSVideoPlaylistReq;
import com.vedantu.cmds.response.UpcommingVideosRes;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.doubts.dao.DoubtChatMessageDAO;
import com.vedantu.doubts.dao.DoubtsDAO;
import com.vedantu.doubts.entities.mongo.Doubt;
import com.vedantu.doubts.entities.mongo.DoubtChatMessage;
import com.vedantu.doubts.enums.DoubtFilter;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.enums.TeacherAvailabilityFilter;
import com.vedantu.doubts.enums.UserDoubtFeedback;
import com.vedantu.doubts.pojo.*;
import com.vedantu.doubts.request.*;
import com.vedantu.doubts.response.UserResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.DoubtPaidState;
import com.vedantu.lms.cmds.pojo.TestUserInfo;
import com.vedantu.notification.pojos.MessageUserRes;
import com.vedantu.subscription.pojo.CourseMobileRes;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;
import com.vedantu.util.enums.DoubtFeedType;
import com.vedantu.util.pojo.DoubtFeedPojo;

import java.lang.reflect.Type;

import java.util.*;
import java.time.*;
import java.sql.Timestamp;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
// TO DO doubts unique constraint for race conditions
@Service
public class DoubtsManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    /*@Autowired
     private DoubtDAO doubtDAO;
     */
    @Autowired
    private DoubtsDAO doubtsDAO;

    @Autowired
    private DoubtChatManager doubtChatManager;

    @Autowired
    private DoubtChatMessageDAO doubtChatMessageDAO;

    @Autowired
    private AmazonS3Manager amazonS3Manager;

    @Autowired
    private TopicTreeDAO topicTreeDAO;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private StudyManager studyManager;

    @Autowired
    CMDSVideoManager cMDSVideoManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private RedisDAO redisDAO;

    Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DoubtsManager.class);

     private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    private static int T1_POOL_WAIT_TIME = ConfigUtils.INSTANCE.getIntValue("doubts.pool.expiry");
    private static int T1_CHAT_WAIT_TIME = ConfigUtils.INSTANCE.getIntValue("doubts.chat.expiry");
    private static String NODE_END_PONT = ConfigUtils.INSTANCE.getStringValue("DOUBT_NODE_SERVER");
    private static String USER_END_PONT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    private static String PAID_USERS = ConfigUtils.INSTANCE.getStringValue("doubt.paid.users");
    private static String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    private static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static int DAIL_DOUBT_LIMIT = 8;
    private static int DOUBT_FEED_SIZE = 12;
    private static int DOUBT_FEED_CLASSROOM_SIZE = 3;
    private static Set<String> ALLOWED_TARGETS = new HashSet<>(Arrays.asList("JEE_Main", "NEET"));

    public Doubt postDoubt(CreateDoubtsReq req, Long callingUserId) throws InternalServerErrorException, ForbiddenException, Exception {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

//        Long dayStartTime = DateTimeUtils.getISTDayStartTime(System.currentTimeMillis());
//        GetDoubtDashboardReq getDoubtDashboardReq = new GetDoubtDashboardReq();
//        getDoubtDashboardReq.setStudentId(callingUserId.toString());
//        getDoubtDashboardReq.setStartTime(dayStartTime);
//        logger.info("getdoubt req: " + getDoubtDashboardReq);
//        List<Doubt> doubts = doubtsDAO.getDoubts(getDoubtDashboardReq, true);
//        if (doubts.size() >= DAIL_DOUBT_LIMIT) {
//            throw new ForbiddenException(ErrorCode.DAILY_DOUBT_LIMIT_EXCEEDED, "");
//        }
        Doubt doubt = null;

        /*
        try{


            transaction.commit();
            platformBasicResponse.setSuccess(true);
        }catch(Exception ex){
            session.getTransaction().rollback();
            platformBasicResponse.setSuccess(false);
            logger.error("Exception while posting doubt: " + ex.getMessage());             
        }finally{
            session.close();            
        }
        
        
        if(!platformBasicResponse.isSuccess()){
            throw new InternalServerErrorException(ErrorCode.ERROR_POSTING_DOUBT, "Error posting doubt");
        }
         */
        User userInfo = fosUtils.getUserInfo(callingUserId, true);

        Set<String> targetGrades = new HashSet<>();

//        [
//        "CBSE",
//                "cbse",
//                "icse",
//                "Other",
//                "ICSE",
//                "State Board",
//                "state-board",
//                "STATE BOARD",
//                "IGCSE",
//                "IB",
//                "ib",
//                "igcse",
//                "jee-main",
//                "ISC",
//                "jee-foundation",
//                "",
//                "Under Graduate",
//                "OTHER",
//                "karnataka-state-board",
//                "Post Graduate",
//                "STATE-BOARD",
//                "ntse",
//                "JEE-MAIN",
//                "cbse.pdf",
//                "maharashtra-state-board",
//                "MAHARASHTRA-STATE-BOARD"
//        ]
        String board = userInfo.getStudentInfo().getBoard();
        if (StringUtils.isNotEmpty(board)) {
            if (board.contains("CBSE") || board.contains("cbse") || board.contains("others") || board.contains("Others") || board.contains("Maharashtra") || board.contains("MAHARASHTRA") ) { //TEMP: add Others in CBSE
                targetGrades.add("CBSE-" + userInfo.getStudentInfo().getGrade());
            } else if (board.contains("ICSE") || board.contains("icse")) {
                targetGrades.add("ICSE-" + userInfo.getStudentInfo().getGrade());
            } else if (board.contains("IB") || board.contains("ib")) {
                targetGrades.add("IB-" + userInfo.getStudentInfo().getGrade());
            } else if (board.contains("igcse") || board.contains("IGCSE")) {
                targetGrades.add("IGCSE-" + userInfo.getStudentInfo().getGrade());
            } else if (board.contains("jee") || board.contains("JEE")) {
                targetGrades.add("JEE_Main-" + userInfo.getStudentInfo().getGrade());
            } else if (board.contains("state") || board.contains("State") || board.contains("STATE")) {
                targetGrades.add("State_Board-" + userInfo.getStudentInfo().getGrade());
            }
        }else {
        	targetGrades.add("CBSE-" + userInfo.getStudentInfo().getGrade());
        }

//        req.setTargetGrades(new HashSet<>(Arrays.asList(userInfo.getStudentInfo().getBoard().toUpperCase() + "-" + userInfo.getStudentInfo().getGrade())));
//        if(!ArrayUtils.isEmpty(userInfo.getStudentInfo().getExamTargets())){
//            for(String target: userInfo.getStudentInfo().getExamTargets()){
//
//                if(target.contains("Engineering Foundation") || target.contains("IIT-JEE") || target.contains("JEE Main")) {
//                    targetGrades.add("JEE_Main" + "-" + userInfo.getStudentInfo().getGrade());
//                    targetGrades.add("JEE_Advanced" + "-" + userInfo.getStudentInfo().getGrade());
//                } else if (target.contains("KCET")) {
//                    targetGrades.add("KCET" + "-" + userInfo.getStudentInfo().getGrade());
//                } else if (target.contains("KVPY")) {
//                    targetGrades.add("KVPY" + "-" + userInfo.getStudentInfo().getGrade());
//                } else if (target.contains("Medical Foundation")) {
//                    targetGrades.add("NEET_UG" + "-" + userInfo.getStudentInfo().getGrade());
//                } else if (target.contains("NEET")) {
//                    targetGrades.add("NEET_UG" + "-" + userInfo.getStudentInfo().getGrade());
//                }
//                else if (target.contains("NTSE")) {
//                    if(userInfo.getStudentInfo().getGrade().equals("9") || userInfo.getStudentInfo().getGrade().equals("10")) {
//                        targetGrades.add("NTSE" + "-" + userInfo.getStudentInfo().getGrade());
//                    }
//                }
////                if(ALLOWED_TARGETS.contains(target.toUpperCase())){
////                    req.getTargetGrades().add(target.toUpperCase() + "-" + userInfo.getStudentInfo().getGrade());
////                }
//            }
//        }
        doubt = new Doubt(req, callingUserId);

        DoubtStateChangePojo doubtStateChangePojo = new DoubtStateChangePojo();
        doubtStateChangePojo.setChangeTime(System.currentTimeMillis());
        doubtStateChangePojo.setNewState(DoubtState.DOUBT_POSTED);
        if (userInfo.getRole().equals(Role.TEACHER)) {
            doubtStateChangePojo.setDexId(userInfo.getId().toString());
        }
        doubt.setDoubtStateChanges(Arrays.asList(doubtStateChangePojo));
        doubt.setLastActivityTime(System.currentTimeMillis());
        doubt.setMembersList(new HashSet<>(Arrays.asList(doubt.getStudentId())));

        if (ArrayUtils.isNotEmpty(targetGrades) && ArrayUtils.isNotEmpty(req.getTopics())) {
            req.setTargetGrades(targetGrades);

            AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
            atte.settTopics(req.getTopics());
            atte.setTargetGrades(req.getTargetGrades());

            AbstractTargetTopicEntity abstractTargetTopicEntity = fosUtils.setAndGenerateTags(atte);

            doubt.setTags(abstractTargetTopicEntity);
        }

        String paidUsersString = redisDAO.get("PAID_USERS");
        List<String> paidUsersList = new ArrayList<>();
        if (StringUtils.isNotEmpty(paidUsersString)) {
            paidUsersList = Arrays.asList(paidUsersString.split(","));
        }

        if (ArrayUtils.isNotEmpty(paidUsersList) && paidUsersList.contains(callingUserId.toString())) {
            logger.info("MARKED-PAID-DOUBT");
            doubt.setDoubtPaidState(DoubtPaidState.PAID);
        } else {

            String url = DINERO_ENDPOINT + "/payment/isDoubtsPaidUser?";

            url = url + "userId=" + userInfo.getId().toString();

            logger.info("PAID-USER-DINERO-URL: " + url);

            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String respString = resp.getEntity(String.class);

            Type filterListType = new TypeToken<Map<String, Boolean>>() {
            }.getType();

            Map<String, Boolean> userInDoubt = new Gson().fromJson(respString, filterListType);
            logger.info("PAID-USER-DINERO-VERIFIED");
            logger.info(userInDoubt);

            if (userInDoubt.get("isPaid") != null && userInDoubt.get("isPaid") == true) {
                doubt.setDoubtPaidState(DoubtPaidState.PAID);
            } else {
                logger.info("MARKED-UNPAID-DOUBT");
                doubt.setDoubtPaidState(DoubtPaidState.UNPAID);
            }

        }

        // Override DoubtPaidState to FOS, in case it was raised by a FOS user
        if (userInfo.getEmail() != null && userInfo.getEmail().endsWith("@vedantu.com")) {
            doubt.setDoubtPaidState(DoubtPaidState.FOS);
        }

        doubtsDAO.save(doubt, callingUserId);
        doubtChatManager.createFirstDoubtMessage(doubt);

        // redisDAO.setex(redisDAO.getDoubtPoolKey(env, doubt.getId().toString()), "1", T1_POOL_WAIT_TIME);
        try {
            pushToT1Pool(doubt);
        } catch (Exception ex) {
            logger.error("Error in http request to node server for doubt posting: " + ex.getMessage());
        }
        updateAWSUrl(doubt);
        return doubt;

    }

    public void pushToT1Pool(Doubt doubt) throws VException {
        DoubtPojo doubtPojo = new DoubtPojo(doubt);
        String url = NODE_END_PONT + "doubtPosted";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(doubtPojo), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
    }

    public void doubtAccepted(Doubt doubt) throws VException {
        DoubtPojo doubtPojo = new DoubtPojo(doubt);
        String url = NODE_END_PONT + "doubtAccepted";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(doubtPojo), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
    }

    public void doubtMarkedAsSpam(Doubt doubt) throws VException {
        DoubtPojo doubtPojo = new DoubtPojo(doubt);
        String url = NODE_END_PONT + "markDoubtAsSpam";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(doubtPojo), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
    }

    public void doubtChatTimedOut(Doubt doubt) throws VException {
        DoubtPojo doubtPojo = new DoubtPojo(doubt);
        String url = NODE_END_PONT + "doubtChatTimeout";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(doubtPojo), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
    }

    public List<DoubtPojo> getDoubtsForSupply(String userId, GetDoubtsReq req) throws ForbiddenException {

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);

        if (Role.TEACHER.equals(userBasicInfo.getRole()) && userBasicInfo.getTeacherPoolType() == null) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }

        TeacherPoolType teacherPoolType = userBasicInfo.getTeacherPoolType();

        GetDoubtDashboardReq getDoubtDashboardReq = new GetDoubtDashboardReq();
        if (TeacherPoolType.T1_POOL.equals(teacherPoolType)) {
            getDoubtDashboardReq.setDoubtState(DoubtState.DOUBT_POSTED);
        } else if (TeacherPoolType.T2_POOL.equals(teacherPoolType)) {
            getDoubtDashboardReq.setDoubtState(DoubtState.DOUBT_T2_POOL);
        }

        if (req.getDoubtPaidState() != null) {
            getDoubtDashboardReq.setDoubtPaidState(req.getDoubtPaidState());
        }

        if (userBasicInfo.getViewerAccesses() != null) {
            getDoubtDashboardReq.setViewerAccesses(userBasicInfo.getViewerAccesses());
            logger.info("Viewer Access : " + userBasicInfo.getViewerAccesses());
        } else {
            logger.info("No Viewer Access fetched");
        }

        getDoubtDashboardReq.setStart(req.getStart());
        getDoubtDashboardReq.setSize(req.getSize());
        getDoubtDashboardReq.setMainTags(req.getMainTags());
        getDoubtDashboardReq.setSubjects(req.getSubjects());
        getDoubtDashboardReq.setDoubtPaidState(req.getDoubtPaidState());
        getDoubtDashboardReq.setStartTime(req.getStartTime());
        getDoubtDashboardReq.setEndTime(req.getEndTime());
        getDoubtDashboardReq.setSort(req.getSort());
        getDoubtDashboardReq.setDoubtFilter(req.getDoubtFilter());

        logger.info("getDoubtsForSupply Request -> " + getDoubtDashboardReq);

        List<Doubt> doubts = doubtsDAO.getDoubts(getDoubtDashboardReq, false);
        if (ArrayUtils.isEmpty(doubts)) {
            return new ArrayList<>();
        }
        List<DoubtPojo> doubtPojos = new ArrayList<>();

        for (Doubt doubt : doubts) {
            updateAWSUrl(doubt);
            DoubtPojo dp = new DoubtPojo(doubt);
            doubtPojos.add(dp);
        }

        return doubtPojos;

    }

    public List<DoubtBasicInfo> getDoubtsForDashboard(GetDoubtDashboardReq req) throws NotFoundException {

        if (DoubtState.DOUBT_MARKED_SPAM.equals(req.getUserDoubtFeedback())) {
            req.setDoubtState(DoubtState.DOUBT_MARKED_SPAM);
        }

        if (StringUtils.isNotEmpty(req.getStudentMailId())) {

            Set<String> emailFromReqSet = new HashSet<>();
            String studentMailID = req.getStudentMailId();
            emailFromReqSet.add(studentMailID);

            Map<String, UserInfo> userInfoFromMail = fosUtils.getUserInfosFromEmails(emailFromReqSet, true);
            logger.info("Extracted User Info from Request : " + userInfoFromMail);
            UserInfo userInfo = userInfoFromMail.get(studentMailID);

            if (userInfoFromMail == null || userInfo == null) {
                throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "User Not Found for Mail " + req.getStudentMailId());
            }

            logger.info("Extracted User Info : " + userInfo + "\n");
            req.setStudentId(userInfo.getUserId().toString());
            logger.info("Updated Request : " + req + "\n");
        }

        List<Doubt> doubts = doubtsDAO.getDoubts(req, true);
        List<DoubtBasicInfo> doubtBasicInfos = new ArrayList<>();

        Set<String> userIds = new HashSet<>();

        for (Doubt doubt : doubts) {
            userIds.add(doubt.getStudentId());
            userIds.add(doubt.getAcceptedBy());
        }

        Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, false);

        for (Doubt doubt : doubts) {
            updateAWSUrl(doubt);
            DoubtBasicInfo doubtBasicInfo = mapper.map(doubt, DoubtBasicInfo.class);
            doubtBasicInfo.setAcceptedByUser(userMap.get(doubt.getAcceptedBy()));
            doubtBasicInfo.setAskedByUser(userMap.get(doubt.getStudentId()));
            doubtBasicInfos.add(doubtBasicInfo);
        }

        return doubtBasicInfos;
    }

    public PlatformBasicResponse acceptDoubt(Long doubtId, String userId) throws ForbiddenException, NotFoundException, ConflictException {

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);

        if (!(TeacherPoolType.T1_POOL.equals(userBasicInfo.getTeacherPoolType()) || TeacherPoolType.T2_POOL.equals(userBasicInfo.getTeacherPoolType()))) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }
        TeacherPoolType teacherPoolType = userBasicInfo.getTeacherPoolType();
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        List<Doubt> doubtAssigned = doubtsDAO.getCurrentDoubtForTeacher(userId);

        if (doubtAssigned != null && doubtAssigned.size() >= 3) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_ACCEPT_MULTIPLE_DOUBTS, "");
        }

//         Doubt doubtAssigned = doubtsDAO.getCurrentDoubtForTeacher(userId);
//         if (doubtAssigned != null) {
//             throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_ACCEPT_MULTIPLE_DOUBTS, "");
//         }
        Doubt doubt = null;
        boolean alreadyAccepted = false;
        boolean notAllowedToAccept = false;
        /*try{
	        transaction.begin();
	        doubt = doubtDAO.getDoubtById(doubtId, session, LockMode.PESSIMISTIC_WRITE);
	        if(doubt != null){
	            
	            if(doubt.getAcceptedBy() == null){
	                doubt.setAcceptedBy(userId);
	                if(TeacherPoolType.T1_POOL.equals(teacherPoolType) && DoubtState.DOUBT_POSTED.equals(doubt.getDoubtState())){
	                    doubt.setDoubtState(DoubtState.DOUBT_T1_ACCEPTED);
	                }else if(TeacherPoolType.T2_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T2_POOL.equals(doubt.getDoubtState())){
	                    doubt.setDoubtState(DoubtState.DOUBT_T2_ACCEPTED);
	                }else{
	                    notAllowedToAccept = true;
	                }
	                doubtDAO.update(doubt, session, userId);
	            }else{
	                alreadyAccepted = true;
	            }
	            
	        }
	        
	        transaction.commit();
	        
	    }catch(Exception ex){
	        session.getTransaction().rollback();            
	        logger.error("Exception while accepting doubt: " + ex.getMessage());                         
	    }finally{
	        session.close();
	    }*/

        doubt = doubtsDAO.getEntityById(doubtId, Doubt.class);
        if (doubt != null) {

            if (doubt.getAcceptedBy() == null) {
                if (TeacherPoolType.T1_POOL.equals(teacherPoolType) && DoubtState.DOUBT_POSTED.equals(doubt.getDoubtState())) {

                    doubt.setAcceptedBy(userId);
                    doubt.setDoubtState(DoubtState.DOUBT_T1_ACCEPTED);
                    Long currentTime = System.currentTimeMillis();
                    doubt.getMembersList().add(userId);
                    DoubtSolverPojo doubtSolverPojo = new DoubtSolverPojo();
                    doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_POSTED, DoubtState.DOUBT_T1_ACCEPTED, userId));
                    doubtSolverPojo.setRole(userBasicInfo.getRole());
                    doubtSolverPojo.setAcceptedTime(currentTime);
                    doubtSolverPojo.setSolverId(userId);
                    doubtSolverPojo.setTeacherPoolType(teacherPoolType);
                    if (ArrayUtils.isEmpty(doubt.getDoubtSolverPojoList())) {
                        doubt.setDoubtSolverPojoList(new ArrayList<>());
                    }
                    doubt.getDoubtSolverPojoList().add(doubtSolverPojo);
                    doubt.setLastActivityTime(currentTime);
                    doubtsDAO.save(doubt, null);

                } else if (TeacherPoolType.T2_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T2_POOL.equals(doubt.getDoubtState())) {
                    doubt.setAcceptedBy(userId);
                    doubt.setDoubtState(DoubtState.DOUBT_T2_ACCEPTED);
                    Long currentTime = System.currentTimeMillis();
                    doubt.getMembersList().add(userId);
                    DoubtSolverPojo doubtSolverPojo = new DoubtSolverPojo();
                    doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T2_POOL, DoubtState.DOUBT_T2_ACCEPTED, userId));
                    doubtSolverPojo.setRole(userBasicInfo.getRole());
                    doubtSolverPojo.setAcceptedTime(currentTime);
                    doubtSolverPojo.setSolverId(userId);
                    doubtSolverPojo.setTeacherPoolType(teacherPoolType);
                    if (ArrayUtils.isEmpty(doubt.getDoubtSolverPojoList())) {
                        doubt.setDoubtSolverPojoList(new ArrayList<>());
                    }
                    doubt.getDoubtSolverPojoList().add(doubtSolverPojo);
                    doubt.setLastActivityTime(currentTime);
                    doubtsDAO.save(doubt, null);

                } else {
                    notAllowedToAccept = true;
                }

            } else {
                alreadyAccepted = true;
            }

        }

        if (doubt == null) {
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, userId);
        }

        if (alreadyAccepted || notAllowedToAccept) {
            throw new ConflictException(ErrorCode.DOUBT_ACCEPTED_BY_SOMEOTHER_TEACHER, "");
        }

        try {
            doubtAccepted(doubt);
        } catch (Exception ex) {
            logger.error("Error in http request to node server for doubt accepted: " + ex.getMessage());
        }

        platformBasicResponse.setSuccess(true);
        return platformBasicResponse;

    }

    public PlatformBasicResponse closeDoubt(Long doubtId, String userId) throws ForbiddenException, NotFoundException, ConflictException {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);

        if (!(TeacherPoolType.T1_POOL.equals(userBasicInfo.getTeacherPoolType()) || TeacherPoolType.T2_POOL.equals(userBasicInfo.getTeacherPoolType()))) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }
        TeacherPoolType teacherPoolType = userBasicInfo.getTeacherPoolType();

        Doubt doubt = null;
        boolean alreadyClosed = false;
        /*try{
            transaction.begin();
            doubt = doubtDAO.getDoubtById(doubtId, session, LockMode.PESSIMISTIC_WRITE);
            if(doubt != null){
                
                if(doubt.getAcceptedBy().equals(userId)){
                    
                    if(TeacherPoolType.T1_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T1_ACCEPTED.equals(doubt.getDoubtState())){
                        doubt.setDoubtState(DoubtState.DOUBT_T1_SOLVED);
                        doubt.setClosedBy(userId);
                        doubt.setClosedTime(System.currentTimeMillis());
                        doubtDAO.update(doubt, session, userId);

                    }else if(TeacherPoolType.T2_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T2_ACCEPTED.equals(doubt.getDoubtState())){
                        doubt.setDoubtState(DoubtState.DOUBT_T2_SOLVED);
                        doubt.setClosedBy(userId);
                        doubt.setClosedTime(System.currentTimeMillis());
                        doubtDAO.update(doubt, session, userId);
                    }else{
                        alreadyClosed = true;
                    }
                }else{
                    alreadyClosed = true;
                }
                
            }
            
            transaction.commit();
            
        }catch(Exception ex){
            session.getTransaction().rollback();            
            logger.error("Exception while closing doubt: " + ex.getMessage());                         
        }finally{
            session.close();
        } 
         */

        doubt = doubtsDAO.getEntityById(doubtId, Doubt.class);
        if (doubt != null) {

            DoubtChatMessage doubtChatMessage = doubtChatMessageDAO.getLastMessageByTeacher(doubtId);

            if (doubtChatMessage == null) {
                throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_CLOSE_WITHOUT_SOLVING, "");
            }

            if (userId.equals(doubt.getAcceptedBy())) {

                if (TeacherPoolType.T1_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T1_ACCEPTED.equals(doubt.getDoubtState())) {
                    doubt.setDoubtState(DoubtState.DOUBT_T1_SOLVED);
                    doubt.setClosedBy(userId);
                    Long currentTime = System.currentTimeMillis();
                    doubt.setClosedTime(currentTime);
                    doubt.setLastActivityTime(currentTime);
                    doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T1_SOLVED, userId));
                    Long duration = null;
                    for (DoubtStateChangePojo doubtStateChangePojo : doubt.getDoubtStateChanges()) {
                        if (DoubtState.DOUBT_T1_ACCEPTED.equals(doubtStateChangePojo.getNewState())) {
                            duration = doubt.getClosedTime() - doubtStateChangePojo.getChangeTime();
                        }
                    }
                    doubt.setDuration(duration);
                    doubtsDAO.save(doubt, Long.parseLong(userId));

                } else if (TeacherPoolType.T2_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T2_ACCEPTED.equals(doubt.getDoubtState())) {
                    doubt.setDoubtState(DoubtState.DOUBT_T2_SOLVED);
                    doubt.setClosedBy(userId);
                    Long currentTime = System.currentTimeMillis();
                    doubt.setClosedTime(currentTime);
                    doubt.setLastActivityTime(currentTime);
                    doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T2_ACCEPTED, DoubtState.DOUBT_T2_SOLVED, userId));
                    Long duration = null;
                    for (DoubtStateChangePojo doubtStateChangePojo : doubt.getDoubtStateChanges()) {
                        if (DoubtState.DOUBT_T2_ACCEPTED.equals(doubtStateChangePojo.getNewState())) {
                            duration = doubt.getClosedTime() - doubtStateChangePojo.getChangeTime();
                        }
                    }
                    doubt.setDuration(duration);

                    doubtsDAO.save(doubt, Long.parseLong(userId));
                } else {
                    alreadyClosed = true;
                }
            } else {
                alreadyClosed = true;
            }

        }

        if (doubt == null) {
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, "");
        }

        if (alreadyClosed) {
            throw new ConflictException(ErrorCode.ACTION_NOT_ALLOWED_FOR_DOUBT, "");
        }
        platformBasicResponse.setSuccess(true);
        return platformBasicResponse;
    }

    public PlatformBasicResponse unableToSolve(Long doubtId, String unableToSolveReason, String userId) throws ForbiddenException, ConflictException, NotFoundException {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);

        if (!(TeacherPoolType.T1_POOL.equals(userBasicInfo.getTeacherPoolType()) || TeacherPoolType.T2_POOL.equals(userBasicInfo.getTeacherPoolType()))) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }
        TeacherPoolType teacherPoolType = userBasicInfo.getTeacherPoolType();

        Doubt doubt = null;
        boolean alreadyClosed = false;
        doubt = doubtsDAO.getEntityById(doubtId, Doubt.class);

        if (doubt == null) {
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, "");
        }
        if (userId.equals(doubt.getAcceptedBy())) {

            if (TeacherPoolType.T1_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T1_ACCEPTED.equals(doubt.getDoubtState())) {
                String solverId = doubt.getAcceptedBy();
                doubt.setAcceptedBy(null);
                doubt.setDoubtState(DoubtState.DOUBT_POSTED);
                Long currentTime = System.currentTimeMillis();
                doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T1_UNABLE_TO_SOLVE, userId));
                doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T1_UNABLE_TO_SOLVE, DoubtState.DOUBT_POSTED, userId));
                doubt.getMembersList().remove(solverId);
                doubt.setUnableToSolveReason(unableToSolveReason);
                doubtsDAO.save(doubt, Long.parseLong(userId));
//                try {
//                    doubtChatTimedOut(doubt);
//                } catch (Exception ex) {
//                    logger.error("Error in http request to node server for doubt chat timeout: " + ex.getMessage());
//                }

            } else if (TeacherPoolType.T2_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T2_ACCEPTED.equals(doubt.getDoubtState())) {

//                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED_FOR_DOUBT, "");
                String solverId = doubt.getAcceptedBy();
                doubt.setAcceptedBy(null);
                doubt.setDoubtState(DoubtState.DOUBT_T2_POOL);
                Long currentTime = System.currentTimeMillis();
                doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T2_ACCEPTED, DoubtState.DOUBT_T2_UNABLE_TO_SOLVE, userId));
                doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T2_UNABLE_TO_SOLVE, DoubtState.DOUBT_T2_POOL, userId));
                doubt.setUnableToSolveT2(true);
                doubt.getMembersList().remove(solverId);
                doubt.setUnableToSolveReason(unableToSolveReason);
                doubtsDAO.save(doubt, Long.parseLong(userId));
//                try {
//                    doubtChatTimedOut(doubt);
//                } catch (Exception ex) {
//                    logger.error("Error in http request to node server for doubt chat timeout: " + ex.getMessage());
//                }

            } else {
                alreadyClosed = true;
            }
        } else {
            alreadyClosed = true;
        }

        if (alreadyClosed) {
            throw new ConflictException(ErrorCode.ACTION_NOT_ALLOWED_FOR_DOUBT, "");
        }

        platformBasicResponse.setSuccess(true);
        return platformBasicResponse;

    }

    public PlatformBasicResponse markDoubtAsSpam(MarkDoubtSpamReq req, String userId) throws ForbiddenException, NotFoundException, ConflictException {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);

        if (!(TeacherPoolType.T1_POOL.equals(userBasicInfo.getTeacherPoolType()) || TeacherPoolType.T2_POOL.equals(userBasicInfo.getTeacherPoolType()))) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }
        TeacherPoolType teacherPoolType = userBasicInfo.getTeacherPoolType();

        Doubt doubt = null;
        boolean alreadyAcceptedOrClosed = false;

        /*try{
            transaction.begin();
            doubt  = doubtDAO.getDoubtById(req.getDoubtId(), session, LockMode.PESSIMISTIC_WRITE);
            
            if(doubt!=null){
                
                if(StringUtils.isEmpty(doubt.getAcceptedBy())){
                    if((TeacherPoolType.T1_POOL.equals(teacherPoolType) && DoubtState.DOUBT_POSTED.equals(doubt.getDoubtState())) || (TeacherPoolType.T2_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T2_POOL.equals(doubt.getDoubtState()))){
                        doubt.setDoubtState(DoubtState.DOUBT_MARKED_SPAM);
                        doubt.setDoubtSpamType(req.getDoubtSpamType());
                        doubt.setSpamReason(req.getReason());
                        doubt.setSpammedBy(userId);
                        doubt.setClosedBy(userId);
                        doubtDAO.update(doubt, session, userId);
                    }else{
                        alreadyAcceptedOrClosed = true;
                    }
                }
                
            }
            transaction.commit();
            
        }catch(Exception ex){
            session.getTransaction().rollback();            
            logger.error("Exception while marking doubt as spam: " + ex.getMessage());             
        }finally{
            session.close();
        } */
        doubt = doubtsDAO.getEntityById(req.getDoubtId(), Doubt.class);

        if (doubt != null) {

            if (StringUtils.isEmpty(doubt.getAcceptedBy())) {
                if ((TeacherPoolType.T1_POOL.equals(teacherPoolType) && DoubtState.DOUBT_POSTED.equals(doubt.getDoubtState())) || (TeacherPoolType.T2_POOL.equals(teacherPoolType) && DoubtState.DOUBT_T2_POOL.equals(doubt.getDoubtState()))) {
                    doubt.setDoubtState(DoubtState.DOUBT_MARKED_SPAM);
                    doubt.setDoubtSpamType(req.getDoubtSpamType());
                    doubt.setSpamReason(req.getReason());
                    doubt.setSpammedBy(userId);
                    doubt.setClosedBy(userId);
                    if (TeacherPoolType.T1_POOL.equals(teacherPoolType)) {
                        doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(System.currentTimeMillis(), DoubtState.DOUBT_POSTED, DoubtState.DOUBT_MARKED_SPAM, userId));
                    } else if (TeacherPoolType.T2_POOL.equals(teacherPoolType)) {
                        doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(System.currentTimeMillis(), DoubtState.DOUBT_T2_POOL, DoubtState.DOUBT_MARKED_SPAM, userId));
                    }

                    doubtsDAO.save(doubt, Long.parseLong(userId));
                } else {
                    alreadyAcceptedOrClosed = true;
                }
            }

        }

        if (doubt == null) {
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, "");
        }

        if (alreadyAcceptedOrClosed) {
            throw new ConflictException(ErrorCode.ACTION_NOT_ALLOWED_FOR_DOUBT, "");
        }

        try {
            doubtMarkedAsSpam(doubt);
        } catch (Exception ex) {
            logger.error("Error in http request to node server for doubt marked as spam: " + ex.getMessage());
        }

        platformBasicResponse.setSuccess(true);
        return platformBasicResponse;

    }

    public PlatformBasicResponse reviewDoubt(ReviewDoubtReq req, String userId) throws ForbiddenException, NotFoundException, ConflictException {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        Doubt doubt = null;
        boolean alreadyAcceptedOrClosed = false;

        /*try{
            transaction.begin();
            doubt  = doubtDAO.getDoubtById(req.getDoubtId(), session, LockMode.PESSIMISTIC_WRITE);
            
            if(doubt!=null){
                
                if(!doubt.getReviewed()){
                    doubt.setReviewed(true);
                    doubt.setReviewRemarks(req.getRemark());
                    doubt.setReviewedBy(userId);
                    doubtDAO.update(doubt, session);
                }else{
                    alreadyAcceptedOrClosed = true;
                }
                
            }
            transaction.commit();
            
        }catch(Exception ex){
            session.getTransaction().rollback();            
            logger.error("Exception while marking doubt as spam: " + ex.getMessage());             
        }finally{
            session.close();
        } */
        doubt = doubtsDAO.getEntityById(req.getDoubtId(), Doubt.class);
        if (doubt != null) {

            if (!doubt.getReviewed()) {
                doubt.setReviewed(true);
                doubt.setReviewRemarks(req.getRemark());
                doubt.setReviewedBy(userId);
                doubt.setQcFeedbackStates(req.getQcFeedbackStates());
                doubt.setQcOption(req.getQcOption());
                doubtsDAO.save(doubt, Long.parseLong(userId));
            } else {
                alreadyAcceptedOrClosed = true;
            }

        }
        if (doubt == null) {
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, "");
        }

        if (alreadyAcceptedOrClosed) {
            throw new ConflictException(ErrorCode.ACTION_NOT_ALLOWED_FOR_DOUBT, "");
        }
        platformBasicResponse.setSuccess(true);
        return platformBasicResponse;

    }

    public PlatformBasicResponse submitUserFeedback(UserDoubtFeedbackReq req, String userId) throws ConflictException, NotFoundException {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        Doubt doubt = null;
        boolean notAllowed = false;
        /*try{
           transaction.begin();
           doubt  = doubtDAO.getDoubtById(req.getDoubtId(), session, LockMode.PESSIMISTIC_WRITE);
           if(doubt.getAskedBy().equals(userId) && (DoubtState.DOUBT_T1_SOLVED.equals(doubt.getDoubtState()) || DoubtState.DOUBT_T2_SOLVED.equals(doubt.getDoubtState())) && doubt.getUserDoubtFeedback() == null){
               if(req.isSolved()){
                   doubt.setUserDoubtFeedback(UserDoubtFeedback.SOLVED);
               }else{
                   doubt.setUserDoubtFeedback(UserDoubtFeedback.NOT_SOLVED);
               }
               
               doubt.setUserRemark(req.getRemark());
               doubtDAO.update(doubt, session);
           }else{
               notAllowed = true;
           }
           
           transaction.commit();
           
       }catch(Exception ex){
           session.getTransaction().rollback();            
           logger.error("Exception while submitting user feedback: " + ex.getMessage());              
       }finally{
           session.close();
       }  
         */

        doubt = doubtsDAO.getEntityById(req.getDoubtId(), Doubt.class);

        if (doubt == null) {
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, "");
        }

        if (doubt.getStudentId().equals(userId) && (DoubtState.DOUBT_T1_SOLVED.equals(doubt.getDoubtState()) || DoubtState.DOUBT_T2_SOLVED.equals(doubt.getDoubtState())) && doubt.getUserDoubtFeedback() == null) {
            if (req.isSolved()) {
                doubt.setUserDoubtFeedback(UserDoubtFeedback.SOLVED);
            } else {
                doubt.setUserDoubtFeedback(UserDoubtFeedback.NOT_SOLVED);
//                String solverId = doubt.getAcceptedBy();
//                doubt.setAcceptedBy(null);
//                doubt.setDoubtState(DoubtState.DOUBT_POSTED);
//                Long currentTime = System.currentTimeMillis();
//                doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_FEEDBACK_NO));
//                doubt.getDoubtStateChanges().add(new DoubtStateChangePojo(currentTime, DoubtState.DOUBT_FEEDBACK_NO, DoubtState.DOUBT_POSTED));
//                doubt.getMembersList().remove(solverId);
//                doubt.setUnableToSolveReason(req.getRemark());
//
//                String closedBy = doubt.getClosedBy();
//
//                doubt.setClosedBy(null);
//                doubt.setClosedTime(null);
//                doubt.setLastActivityTime(currentTime);
//                doubt.setDuration(null);
//
//                if(StringUtils.isNotEmpty(closedBy)) {
//
//                    logger.info("NO-FEEDBACK-CLOSED");
//
//                    Map<String, Object> payload = new HashMap<>();
//                    payload.put("dexId", closedBy);
//                    payload.put("doubtId", doubt.getId().toString());
//                    AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.SEND_DOUBT_REOPEN_NOTIFICATION, payload);
//                    asyncTaskFactory.executeTask(asyncTaskParams);
//                }

                // Send notification to respective users
            }

            doubt.setUserRemark(req.getRemark());
            doubtsDAO.save(doubt, Long.parseLong(userId));
        } else {
            notAllowed = true;
        }

        if (notAllowed) {
            throw new ConflictException(ErrorCode.ACTION_NOT_ALLOWED_FOR_DOUBT, "");
        }
        platformBasicResponse.setSuccess(true);
        return platformBasicResponse;

    }

    public void sendDoubtReopenNotifcationToDex(String dexId, String doubtId) throws VException {
        Map<String, Object> request = new HashMap<>();
        Map<String, Object> payload = new HashMap<>();
        request.put("toUserId", Long.parseLong(dexId));
        request.put("targetRole", "TEACHER");
        request.put("key", "qazwsxedcrfvtgbyhnujmikolp");
        request.put("notificationtype", "SERVER_NOTIFICATION");

        payload.put("title", "Doubt has been re-opened by the user");
        payload.put("deep_link", "https://www.vedantu.com/dex/doubts/" + doubtId);
        payload.put("body", "");

        request.put("payload", payload);

        String url = NODE_END_PONT + "sendNotification";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(request), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        logger.info("DEX-DOUBT-REOPEN-NOTIFICATION: " + respString);
    }

    public Doubt getDoubtById(Long doubtId) throws NotFoundException {
        Doubt doubt = null;
        doubt = doubtsDAO.getEntityById(doubtId, Doubt.class);

        return doubt;
    }

    public List<Doubt> getOpenDoubts(String userId) {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);
        List<Doubt> doubts = doubtsDAO.getOpenDoubts(userId, userBasicInfo.getRole());
        if (ArrayUtils.isEmpty(doubts)) {
            return new ArrayList<>();
        }

        for (Doubt doubt : doubts) {
            updateAWSUrl(doubt);
        }
        return doubts;

    }

    public SolverDoubtStats getDoubtSolverStatsByEmail(String email, DoubtFilter filterDoubt) throws InternalServerErrorException, BadRequestException {
        UserBasicInfo info = fosUtils.getUserBasicInfoFromEmail(email, false);
        return getDoubtSolverStats(info.getUserId().toString(), filterDoubt);
    }

    public SolverDoubtStats getDoubtSolverStats(String userId, DoubtFilter filterDoubt) throws InternalServerErrorException, BadRequestException {

        String solverStatsRedis = redisDAO.get("DoubtSolverStats:" + userId);
        if (StringUtils.isNotEmpty(solverStatsRedis)) {
            SolverDoubtStats solverResp;
            solverResp = gson.fromJson(solverStatsRedis, SolverDoubtStats.class);
            return solverResp;
        }

        List<Doubt> doubts = new ArrayList<>();

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        LocalDateTime adjustedLocalMinDateTime = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(calendar.getTime().getTime()),
                ZoneId.systemDefault());

        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);

        LocalDateTime adjustedLocalMaxDateTime = LocalDateTime.ofInstant(
                Instant.ofEpochMilli(calendar.getTime().getTime()),
                ZoneId.systemDefault());

        Long startTime = 0l, endTime = 0l;

        if (filterDoubt == null) {
            startTime = adjustedLocalMinDateTime
                    .minusDays(adjustedLocalMinDateTime.getDayOfMonth() - 1)
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
            endTime = adjustedLocalMaxDateTime
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
        } else if (filterDoubt.equals(DoubtFilter.LAST_MONTH)) {
            // minus 1 month, now get day 1 of that month by minusDays by current date -1
            startTime = adjustedLocalMinDateTime.minusMonths(1)
                    .minusDays(adjustedLocalMinDateTime.minusMonths(1).getDayOfMonth() - 1)
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
            // minusDays by current date to reach last date of last month, time maxed to 23:59:59
            endTime = adjustedLocalMaxDateTime
                    .minusDays(adjustedLocalMaxDateTime.getDayOfMonth())
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
        } else if (filterDoubt.equals(DoubtFilter.LAST_WEEK)) {
            // minus 1 week, now get Monday for that week by minusDays by currentDay of last week -1
            startTime = adjustedLocalMinDateTime.minusWeeks(1)
                    .minusDays(adjustedLocalMinDateTime.minusWeeks(1).getDayOfWeek().getValue() - 1)
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
            // minusDays by current day of week to reach Sunday of last week, time maxed to 23:59:59
            endTime = adjustedLocalMaxDateTime
                    .minusDays(adjustedLocalMaxDateTime.getDayOfWeek().getValue())
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
        } else if (filterDoubt.equals(DoubtFilter.LAST_DAY)) {
            // minus 1 day, at 00:00:00
            startTime = adjustedLocalMinDateTime.minusDays(1)
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
            // minus 1 day, at 23:59:59
            endTime = adjustedLocalMaxDateTime.minusDays(1)
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
        } else if (filterDoubt.equals(DoubtFilter.TODAY)) {
            startTime = adjustedLocalMinDateTime
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
            endTime = adjustedLocalMaxDateTime
                    .atZone(ZoneId.of("Asia/Kolkata"))
                    .toInstant().toEpochMilli();
        }

        logger.info("getDoubtSolverStats -> startTime : " + startTime + ", endTime : " + endTime);

        doubts = doubtsDAO.getDoubtsBetweenTimeForTeacher(userId, startTime, endTime);
        List<Doubt> spamDoubts = doubtsDAO.getDoubtsBetweenTimeForTeacherSpam(userId, startTime, endTime);
        doubts.addAll(spamDoubts);

        logger.info("getDoubtSolverStats -> Doubts fetched for filter " + filterDoubt + " " + doubts.size());

        SolverDoubtStats result = new SolverDoubtStats();

        if (ArrayUtils.isNotEmpty(doubts)) {
            // result.setCountOfAllDoubts(doubts.size());
            int count = 0, fRTCount = 0; // Counts all the closed Doubts
            int feedbackYesCount = 0;
            int feedbackNoCount = 0;
            int feedbackPendingCount = 0;
            int spamCount = 0;
            Long duration = 0l;
            Long firstResponseDuration = 0l;
            for (Doubt doubt : doubts) {
                if (doubt.getClosedTime() != null && doubt.getDuration() != null) {
                    logger.info("Closed Doubt id : " + doubt.getId());
                    duration = duration + doubt.getDuration();
                    count++;
                    if (UserDoubtFeedback.SOLVED.equals(doubt.getUserDoubtFeedback())) {
                        feedbackYesCount++;
                    } else if (UserDoubtFeedback.NOT_SOLVED.equals(doubt.getUserDoubtFeedback())) {
                        feedbackNoCount++;
                    } else {
                        feedbackPendingCount++;
                    }

                    if (ArrayUtils.isNotEmpty(doubt.getDoubtSolverPojoList()) && doubt.getDoubtSolverPojoList().get(0).getSolverId().equals(userId)) {
                        Long doubtResponseTimeForUser = doubtChatMessageDAO.getDoubtResponseTimeForUser(userId, doubt.getId());
                        if (doubtResponseTimeForUser != null) {
                            fRTCount++;
                            firstResponseDuration = firstResponseDuration + doubtResponseTimeForUser - doubt.getCreationTime();
                        }
                    }
                } else if (doubt.getDoubtState() == DoubtState.DOUBT_MARKED_SPAM) {
                    spamCount++;
                }
            }

            logger.info("Count : " + count);
            logger.info("FRTCount : " + fRTCount);
            if (fRTCount > 0) {
                logger.info("Average FRT : " + firstResponseDuration / fRTCount / 1000 / 60);
            }

            if (count > 0) {
                result.setAverageDuration(duration / count);
                result.setCountOfAllDoubts(count);
                result.setFeedbackNoCount(feedbackNoCount);
                result.setFeedbackYesCount(feedbackYesCount);
                result.setFeedbackPendingCount(feedbackPendingCount);
                if (fRTCount > 0) {
                    result.setAverageFRT(firstResponseDuration / fRTCount / 1000 / 60);// AvgFRT in minutes
                } else {
                    result.setAverageFRT(0l);
                }
                result.setSpamCount(spamCount);
            } else {
                logger.info("getDoubtSolverStats -> No Doubts Closed");
                result.setAverageDuration(0l);
                result.setCountOfAllDoubts(0);
                result.setFeedbackNoCount(0);
                result.setFeedbackYesCount(0);
                result.setFeedbackPendingCount(0);
                result.setAverageFRT(0l);
                result.setSpamCount(0);
            }
        } else {
            logger.info("getDoubtSolverStats -> No Doubts Found for user " + userId + " within this month");
            result.setAverageDuration(0l);
            result.setCountOfAllDoubts(0);
            result.setFeedbackNoCount(0);
            result.setFeedbackYesCount(0);
            result.setFeedbackPendingCount(0);
            result.setAverageFRT(0l);
            result.setSpamCount(0);
        }

        redisDAO.setex("DoubtSolverStats:" + userId, gson.toJson(result, SolverDoubtStats.class), 30 * DateTimeUtils.SECONDS_PER_MINUTE);
        return result;
    }

    public Set<Long> dexIdsInChat(TeacherPoolType teacherPoolType) {
        List<DoubtState> doubtStates = new ArrayList<>();
        if (TeacherPoolType.T1_POOL.equals(teacherPoolType)) {
            doubtStates.add(DoubtState.DOUBT_T1_ACCEPTED);
        } else if (TeacherPoolType.T2_POOL.equals(teacherPoolType)) {
            doubtStates.add(DoubtState.DOUBT_T2_ACCEPTED);
        } else {
            doubtStates = Arrays.asList(DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T2_ACCEPTED);
        }

        logger.info("Doubt states: " + doubtStates);

        List<Doubt> doubts = doubtsDAO.getDoubtsForTeachers(doubtStates);
        if (ArrayUtils.isEmpty(doubts)) {
            return new HashSet<>();
        }
        Set<Long> dexIds = new HashSet<>();

        for (Doubt doubt : doubts) {
            dexIds.add(Long.parseLong(doubt.getAcceptedBy()));
        }
        logger.info("Return for dexIds in chat: " + dexIds);
        return dexIds;
    }

    public Set<Long> getOnlineUserIds(TeacherPoolType teacherPoolType) throws VException {
        String url = NODE_END_PONT + "getOnlineTeachers?room=" + teacherPoolType.toString();
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        GetNodeServerTeacherIdsPojo getNodeServerTeacherIdsPojo = gson.fromJson(respString, GetNodeServerTeacherIdsPojo.class);
        if (getNodeServerTeacherIdsPojo.getResult() == null || ArrayUtils.isEmpty(getNodeServerTeacherIdsPojo.getResult().getTeacherIds())) {
            return new HashSet<>();
        }

        Set<Long> result = new HashSet<>();

        for (String teacherId : getNodeServerTeacherIdsPojo.getResult().getTeacherIds()) {
            result.add(Long.parseLong(teacherId));
        }

        return result;
    }

    public List<User> getDexInfos(TeacherPoolType teacherPoolType, Set<String> mainTags, Integer start, Integer size, Set<String> userIds) throws VException {
        String url = USER_END_PONT + "/getDEXTeachers?mainTags=" + String.join(",", mainTags);

        if (teacherPoolType != null) {
            url = url + "&teacherPoolType=" + teacherPoolType;
        }

        /*if(start != null){
           url = url + "&start="+start;
       }else{
           url = url + "&start=0";
       }
       
       if(size != null){
           url = url + "&size="+size;
       } */
        if (ArrayUtils.isNotEmpty(userIds)) {
            url = url + "&userIds=" + String.join(",", userIds);
        }

        logger.info("Url for fetching live dashboard: " + url);

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<User>>() {
        }.getType();
        List<User> result = gson.fromJson(respString, listType);
        logger.info("Users from request: " + result);
        return result;
    }

    public DoubtSolverDashboardStats getDexLivePojos(Set<Long> chatIds, Set<Long> onlineIds, List<User> userBasicInfos, TeacherAvailabilityFilter teacherAvailabilityFilter) {
        int t1Count = 0;
        int t2Count = 0;
        List<DexLivePojo> dexLivePojos = new ArrayList<>();
        for (User userBasicInfo : userBasicInfos) {
            logger.info("Userbasic info : " + userBasicInfo);
            DexLivePojo dexLivePojo = new DexLivePojo();
            dexLivePojo.setEmail(userBasicInfo.getEmail());
            dexLivePojo.setContactNumber(userBasicInfo.getContactNumber());

            if (userBasicInfo.getTeacherInfo() != null && userBasicInfo.getTeacherInfo().getDexInfo() != null) {

                if (TeacherPoolType.T1_POOL.equals(userBasicInfo.getTeacherInfo().getDexInfo().getTeacherPoolType())) {
                    t1Count++;
                } else if (TeacherPoolType.T2_POOL.equals(userBasicInfo.getTeacherInfo().getDexInfo().getTeacherPoolType())) {
                    t2Count++;
                }

                dexLivePojo.setTeacherPoolType(userBasicInfo.getTeacherInfo().getDexInfo().getTeacherPoolType());
                dexLivePojo.setTopics(userBasicInfo.getTeacherInfo().getDexInfo().gettTopics());
                dexLivePojo.setBoards(userBasicInfo.getTeacherInfo().getDexInfo().gettTargets());
                dexLivePojo.setGrades(userBasicInfo.getTeacherInfo().getDexInfo().gettGrades());
                dexLivePojo.setTargets(userBasicInfo.getTeacherInfo().getDexInfo().gettTargets());

                if (TeacherAvailabilityFilter.CHAT.equals(teacherAvailabilityFilter)) {
                    if (chatIds.contains(userBasicInfo.getId())) {
                        dexLivePojo.setInChat(true);
                        dexLivePojo.setAvailable(false);
                        dexLivePojo.setFullName(userBasicInfo.getFullName());
                        dexLivePojos.add(dexLivePojo);

                    }
                } else if (TeacherAvailabilityFilter.ONLINE.equals(teacherAvailabilityFilter)) {

                    if ((!chatIds.contains(userBasicInfo.getId())) && onlineIds.contains(userBasicInfo.getId())) {
                        dexLivePojo.setAvailable(true);
                        dexLivePojo.setInChat(false);
                        dexLivePojo.setFullName(userBasicInfo.getFullName());
                        dexLivePojos.add(dexLivePojo);
                    }
                } else {
                    if (onlineIds.contains(userBasicInfo.getId())) {
                        if (chatIds.contains(userBasicInfo.getId())) {
                            dexLivePojo.setAvailable(false);
                            dexLivePojo.setInChat(true);
                        } else {
                            dexLivePojo.setAvailable(true);
                            dexLivePojo.setInChat(false);
                        }
                        dexLivePojo.setFullName(userBasicInfo.getFullName());
                        dexLivePojos.add(dexLivePojo);
                    }
                }

            }
        }
        DoubtSolverDashboardStats doubtSolverDashboardStats = new DoubtSolverDashboardStats();
        doubtSolverDashboardStats.setT1Count(t1Count);
        doubtSolverDashboardStats.setT2Count(t2Count);
        doubtSolverDashboardStats.setDexInfos(dexLivePojos);
        return doubtSolverDashboardStats;
    }

    public DoubtSolverDashboardStats getDoubtSolverDashboardStats(GetTeacherDashboardReq req) {
        DoubtSolverDashboardStats doubtSolverDashboardStats = new DoubtSolverDashboardStats();

        if (req.getMainTags() == null) {
            req.setMainTags(new HashSet<>());
        }

        Set<Long> onlineIds = new HashSet<>();
        Set<Long> chatIds = new HashSet<>();
        if (req.getTeacherPoolType() != null) {
            try {
                chatIds = dexIdsInChat(req.getTeacherPoolType());
            } catch (Exception ex) {
                logger.error("Error in fetching chat ids : " + ex.getMessage());
            }
        }

        if (req.getTeacherPoolType() != null) {
            try {
                onlineIds = getOnlineUserIds(req.getTeacherPoolType());
            } catch (Exception ex) {
                logger.error("Errr in fetching onlineIds : " + ex.getMessage());
            }
        }

        if (req.getTeacherPoolType() != null) {

            if (TeacherPoolType.T1_POOL.equals(req.getTeacherPoolType())) {
                doubtSolverDashboardStats.setT2Count(0);
            } else {
                doubtSolverDashboardStats.setT1Count(0);
            }

            if (TeacherAvailabilityFilter.CHAT.equals(req.getFilter())) {

                List<User> userBasicInfos = new ArrayList<>();
                Set<String> userIdsForChat = new HashSet<>();

                for (Long userId : chatIds) {
                    userIdsForChat.add(userId.toString());
                }

                try {
                    userBasicInfos = getDexInfos(null, req.getMainTags(), req.getStart(), req.getSize(), userIdsForChat);
                } catch (Exception ex) {
                    logger.error("Eror in fetching user basic infos for dex: " + ex.getMessage());
                }

                if (ArrayUtils.isEmpty(userBasicInfos)) {
                    return doubtSolverDashboardStats;
                }

                doubtSolverDashboardStats = getDexLivePojos(chatIds, onlineIds, userBasicInfos, req.getFilter());

            } else if (TeacherAvailabilityFilter.ONLINE.equals(req.getFilter())) {
                Set<String> userIdsForOnline = new HashSet<>();
                chatIds.retainAll(onlineIds);
                for (Long userId : onlineIds) {
                    userIdsForOnline.add(userId.toString());
                }

                List<User> userBasicInfos = new ArrayList<>();
                try {
                    userBasicInfos = getDexInfos(null, req.getMainTags(), req.getStart(), req.getSize(), userIdsForOnline);
                } catch (Exception ex) {
                    logger.error("Eror in fetching user basic infos for dex: " + ex.getMessage());
                }

                if (ArrayUtils.isEmpty(userBasicInfos)) {
                    return doubtSolverDashboardStats;
                }

                doubtSolverDashboardStats = getDexLivePojos(chatIds, onlineIds, userBasicInfos, req.getFilter());

            } else {
                Set<String> userIdsForOnline = new HashSet<>();

                for (Long userId : onlineIds) {
                    userIdsForOnline.add(userId.toString());
                }

                List<User> userBasicInfos = new ArrayList<>();
                try {
                    userBasicInfos = getDexInfos(null, req.getMainTags(), req.getStart(), req.getSize(), userIdsForOnline);
                } catch (Exception ex) {
                    logger.error("Eror in fetching user basic infos for dex: " + ex.getMessage());
                }

                if (ArrayUtils.isEmpty(userBasicInfos)) {
                    return doubtSolverDashboardStats;
                }

                doubtSolverDashboardStats = getDexLivePojos(chatIds, onlineIds, userBasicInfos, req.getFilter());

            }

        } else {

            onlineIds = new HashSet<>();
            Set<Long> t1Online = new HashSet<>();
            Set<Long> t2Online = new HashSet<>();

            try {
                t1Online = getOnlineUserIds(TeacherPoolType.T1_POOL);
                onlineIds.addAll(t1Online);
            } catch (Exception ex) {
                logger.error("Errr in fetching onlineIds : " + ex.getMessage());
            }

            try {
                t2Online = getOnlineUserIds(TeacherPoolType.T2_POOL);
                onlineIds.addAll(t2Online);
            } catch (Exception ex) {
                logger.error("Errr in fetching onlineIds : " + ex.getMessage());
            }
            chatIds = new HashSet<>();
            Set<Long> t1Chat = new HashSet<>();
            Set<Long> t2Chat = new HashSet<>();
            t1Chat = dexIdsInChat(TeacherPoolType.T1_POOL);
            chatIds.addAll(t1Chat);

            t2Chat = dexIdsInChat(TeacherPoolType.T2_POOL);
            chatIds.addAll(t2Chat);
            logger.info("Chat Ids: " + t1Chat + " T2Chat Ids: " + t2Chat);
            if (TeacherAvailabilityFilter.CHAT.equals(req.getFilter())) {

                doubtSolverDashboardStats.setT1Count(t1Chat.size());
                doubtSolverDashboardStats.setT2Count(t2Chat.size());
                if (ArrayUtils.isEmpty(chatIds)) {
                    return doubtSolverDashboardStats;
                }

                Set<String> userIdsForChat = new HashSet<>();

                for (Long userId : chatIds) {
                    userIdsForChat.add(userId.toString());
                }

                List<User> userBasicInfos = new ArrayList<>();
                try {
                    userBasicInfos = getDexInfos(null, req.getMainTags(), req.getStart(), req.getSize(), userIdsForChat);
                } catch (Exception ex) {
                    logger.error("Eror in fetching user basic infos for dex: " + ex.getMessage());
                }

                if (ArrayUtils.isEmpty(userBasicInfos)) {
                    return doubtSolverDashboardStats;
                }

                doubtSolverDashboardStats = getDexLivePojos(chatIds, onlineIds, userBasicInfos, req.getFilter());

            } else if (TeacherAvailabilityFilter.ONLINE.equals(req.getFilter())) {
                t1Chat.retainAll(t1Online);
                t2Chat.retainAll(t2Online);
                doubtSolverDashboardStats.setT1Count(t1Online.size() - t1Chat.size());
                doubtSolverDashboardStats.setT2Count(t2Online.size() - t2Chat.size());
                if (ArrayUtils.isEmpty(onlineIds)) {
                    return doubtSolverDashboardStats;
                }

                Set<String> userIdsForChat = new HashSet<>();

                for (Long userId : onlineIds) {
                    userIdsForChat.add(userId.toString());
                }

                List<User> userBasicInfos = new ArrayList<>();
                try {
                    userBasicInfos = getDexInfos(null, req.getMainTags(), req.getStart(), req.getSize(), userIdsForChat);
                } catch (Exception ex) {
                    logger.error("Eror in fetching user basic infos for dex: " + ex.getMessage());
                }

                if (ArrayUtils.isEmpty(userBasicInfos)) {
                    return doubtSolverDashboardStats;
                }

                doubtSolverDashboardStats = getDexLivePojos(chatIds, onlineIds, userBasicInfos, req.getFilter());

            } else {
                List<User> userBasicInfos = new ArrayList<>();
                doubtSolverDashboardStats.setT1Count(t1Online.size());
                doubtSolverDashboardStats.setT2Count(t2Online.size());

                if (ArrayUtils.isEmpty(onlineIds)) {
                    return doubtSolverDashboardStats;
                }
                Set<String> userIdsForChat = new HashSet<>();

                for (Long userId : onlineIds) {
                    userIdsForChat.add(userId.toString());
                }

                try {
                    userBasicInfos = getDexInfos(null, req.getMainTags(), req.getStart(), req.getSize(), userIdsForChat);
                } catch (Exception ex) {
                    logger.error("Eror in fetching user basic infos for dex: " + ex.getMessage());
                }

                if (ArrayUtils.isEmpty(userBasicInfos)) {
                    return doubtSolverDashboardStats;
                }
                doubtSolverDashboardStats = getDexLivePojos(chatIds, onlineIds, userBasicInfos, req.getFilter());

            }

        }

        return doubtSolverDashboardStats;

    }

    public void updateAWSUrl(Doubt doubt) {
        if (StringUtils.isNotEmpty(doubt.getPicFileName()) && !doubt.getPicFileName().equals("false")) {
            doubt.setPicUrl(amazonS3Manager.getDoubtsImageUrl(doubt.getPicFileName()));
        }
    }

    public PlatformBasicResponse tagDoubt(DoubtTaggingReq req, String userId) throws ForbiddenException, NotFoundException, ConflictException, Exception {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);

        if (!(TeacherPoolType.T1_POOL.equals(userBasicInfo.getTeacherPoolType()) || TeacherPoolType.T2_POOL.equals(userBasicInfo.getTeacherPoolType()))) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }

        Doubt doubt = doubtsDAO.getEntityById(req.getDoubtId(), Doubt.class);

        if (doubt == null) {
            throw new NotFoundException(ErrorCode.DOUBT_NOT_FOUND, null);
        }

        if (StringUtils.isNotEmpty(doubt.getAcceptedBy()) && !doubt.getAcceptedBy().equals(userId)) {
            throw new ConflictException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, null);
        }

        AbstractTargetTopicEntity atte = new AbstractTargetTopicEntity();
        atte.settTopics(req.getTopics());
        atte.setTargetGrades(doubt.getTargetGrades());

        AbstractTargetTopicEntity abstractTargetTopicEntity = fosUtils.setAndGenerateTags(atte);

        doubt.setTags(abstractTargetTopicEntity);
        doubtsDAO.save(doubt, Long.parseLong(userId));

        return new PlatformBasicResponse(true, null, null);

    }

    public List<BaseTopicTree> getBaseTreeNodesByLevelWithoutChildren(Long callingUserId) {

    	User user = fosUtils.getUserInfo(callingUserId, false);
    	List<BaseTopicTree> subjects = new ArrayList<>();
    	List<CurriculumTopicTree> totalCurriculumTopicTrees = new ArrayList<>();
    	if (user.getStudentInfo() != null) {
    		Set<String> subNames = new HashSet<>();

    		String grade = "";
    		if(StringUtils.isNotEmpty(user.getStudentInfo().getGrade())) {
    			grade = user.getStudentInfo().getGrade();
    			boolean isBelow9thGrade = false;
    			if (StringUtils.isNotEmpty(grade))
    			{
    				isBelow9thGrade = grade.equalsIgnoreCase("1") || grade.equalsIgnoreCase("2") || grade.equalsIgnoreCase("3") || grade.equalsIgnoreCase("4")
    						|| grade.equalsIgnoreCase("5") || grade.equalsIgnoreCase("6") || grade.equalsIgnoreCase("7") || grade.equalsIgnoreCase("8");
    			}
    			if(isBelow9thGrade) {
    				subNames.add("Science");
    				subNames.add("Maths");
    				subNames.add("Social Studies");
    				subNames.add("English");

    			}else {
    				if(grade.equalsIgnoreCase("9") || grade.equalsIgnoreCase("10")){

    					subNames.add("Physics");
    					subNames.add("Chemistry");
    					subNames.add("Maths");
    					subNames.add("Biology");
    					subNames.add("Social Studies");
    					subNames.add("English");

    				} else if(grade.equalsIgnoreCase("11") || grade.equalsIgnoreCase("12") || grade.equalsIgnoreCase("13")) {

    					if(StringUtils.isNotEmpty(user.getStudentInfo().getStream()) && user.getStudentInfo().getStream().equalsIgnoreCase("Commerce")) {
    						subNames.add("Accountancy");
    						subNames.add("Business Study");
    						subNames.add("Economic Studies");
    						subNames.add("Maths");
    					}
    					if(StringUtils.isNotEmpty(user.getStudentInfo().getTarget())){
    						if(user.getStudentInfo().getTarget().equalsIgnoreCase("JEE")) {
    							subNames.add("Physics");
    							subNames.add("Chemistry");
    							subNames.add("Maths");
    							
    						}else if(user.getStudentInfo().getTarget().equalsIgnoreCase("NEET")) {
    							subNames.add("Physics");
    							subNames.add("Chemistry");
    							subNames.add("Biology");
    						}else if(user.getStudentInfo().getTarget().equalsIgnoreCase("School Prep")) {
    	    					subNames.add("Physics");
    	    					subNames.add("Chemistry");
    	    					subNames.add("Maths");
    	    					subNames.add("Biology");
    						}
    					}
    				}
    			}
    		}


            if (subNames.isEmpty()) {
                subNames.add("Physics");
                subNames.add("Chemistry");
                subNames.add("Biology");
                subNames.add("Maths");
                subNames.add("English");
            }
            
            subjects = topicTreeDAO.getNodesForTopicsWithoutChildren(subNames);
            
            
            
//            if(StringUtils.isNotEmpty(user.getStudentInfo().getBoard())){
//                CurriculumTopicTree curriculumTopicTree = topicTreeDAO.getCurriculumTopicTreeNodeByField(CurriculumTopicTree.Constants.NAME, user.getStudentInfo().getBoard().toUpperCase());
//                if(curriculumTopicTree != null){
//                    if(StringUtils.isNotEmpty(user.getStudentInfo().getGrade())){
//                        CurriculumTopicTree gradeTreeNode = topicTreeDAO.getCurriculumTopicTreeNodesByParentIdAndName(curriculumTopicTree.getId(), user.getStudentInfo().getGrade());
//                        totalCurriculumTopicTrees.add(gradeTreeNode);
//                    }
//                }
//            }
            
//            if (ArrayUtils.isNotEmpty(user.getStudentInfo().getExamTargets())) {
//
//                for (String target : user.getStudentInfo().getExamTargets()) {
//                    if (target.contains("JEE")) {
//                        subNames.add("Physics");
//                        subNames.add("Maths");
//                        subNames.add("Chemistry");
//                    }
//                    if (target.contains("NEET")) {
//                        subNames.add("Biology");
//                        subNames.add("Physics");
//                        subNames.add("Chemistry");
//                    }
//                }

//                List<CurriculumTopicTree> curriculumTopicTrees = topicTreeDAO.getCurriculumTopicTreesByNames(new HashSet<>(user.getStudentInfo().getExamTargets()));
//                logger.info("targettrees: "+curriculumTopicTrees);
//                if(ArrayUtils.isNotEmpty(curriculumTopicTrees)){
//                    Set<String> curriculumIds = new HashSet<>();
//                    for(CurriculumTopicTree curriculumTopicTree : curriculumTopicTrees){
//                        curriculumIds.add(curriculumTopicTree.getId());
//                    }
//                    logger.info("ids : "+curriculumIds);
//                    if(StringUtils.isNotEmpty(user.getStudentInfo().getGrade())){
//                        List<CurriculumTopicTree> curriculumTopicTrees1 = topicTreeDAO.getCurriculumTopicTreeNodesByParentIdsAndName(curriculumIds, user.getStudentInfo().getGrade());
//                        logger.info("child trees: "+curriculumTopicTrees1);
//                        totalCurriculumTopicTrees.addAll(curriculumTopicTrees1);
//                    }
//                }
//            }

//           if(ArrayUtils.isNotEmpty(totalCurriculumTopicTrees)){
//               for(CurriculumTopicTree curriculumTopicTree : totalCurriculumTopicTrees){
//                   subNames.addAll(curriculumTopicTree.getChildren());
//               }
//               subjects = topicTreeDAO.getNodesForTopicsWithoutChildren(subNames);
//           }
        }
        return subjects;
    }

    public DoubtChatPojo getDoubtChatPojoById(Long doubtId, Role role) {

        Doubt doubtStats = doubtsDAO.getEntityById(doubtId, Doubt.class);
        Long unreadMessages = doubtChatMessageDAO.getCountOfUnread(doubtStats.getId(), doubtStats.getStudentId(), role);
        if (unreadMessages == null) {
            unreadMessages = 0l;
        }
        updateAWSUrl(doubtStats);
        DoubtChatMessage doubtChatMessage = doubtChatMessageDAO.getLastChatMessage(doubtStats.getId());
        DoubtChatPojo doubtChatPojo = new DoubtChatPojo();

        doubtChatPojo.setDoubtId(doubtStats.getId());
        doubtChatPojo.setDoubtState(doubtStats.getDoubtState());
        doubtChatPojo.setMainTags(doubtStats.getMainTags());
        doubtChatPojo.setTargetGrades(doubtStats.getTargetGrades());
        doubtChatPojo.settGrades(doubtStats.gettGrades());
        doubtChatPojo.settSubjects(doubtStats.gettSubjects());
        doubtChatPojo.settTargets(doubtStats.gettTargets());
        doubtChatPojo.settTopics(doubtStats.gettTopics());
//        doubtChatPojo.setBoard(doubtStats.getBoard());
//        doubtChatPojo.setBoardId(doubtStats.getBoardId());
//        doubtChatPojo.setDoubtId(doubtStats.getId());
//        doubtChatPojo.setDoubtState(doubtStats.getDoubtState());
//        doubtChatPojo.setGrade(doubtStats.getGrade());
//        doubtChatPojo.setTarget(doubtStats.getTarget());
        doubtChatPojo.setLastActivity(doubtStats.getLastActivityTime());
        doubtChatPojo.setStudentId(doubtStats.getStudentId());
        doubtChatPojo.setDoubtSpamType(doubtStats.getDoubtSpamType());
        if (doubtStats.getUserDoubtFeedback() != null) {
            doubtChatPojo.setUserFeedback(true);
        } else {
            doubtChatPojo.setUserFeedback(false);
        }
        for (DoubtStateChangePojo doubtStateChangePojo : doubtStats.getDoubtStateChanges()) {
            if (DoubtState.DOUBT_T1_ACCEPTED.equals(doubtStateChangePojo.getNewState())) {
                doubtChatPojo.setOpened(true);
            }
        }
        updateAwsUrl(doubtChatMessage);
        DoubtChatMessagePojo doubtChatMessagePojo = new DoubtChatMessagePojo(doubtChatMessage);
        if (doubtChatMessagePojo.getFromId().equals(doubtStats.getStudentId())) {
            doubtChatMessagePojo.setFromRole(Role.STUDENT);
        } else {
            doubtChatMessagePojo.setFromRole(Role.TEACHER);
        }
        doubtChatMessagePojo.setMessageId(doubtChatMessage.getId());
        doubtChatPojo.setPicUrl(doubtStats.getPicUrl());
        doubtChatPojo.setDoubtChatMessagePojo(doubtChatMessagePojo);
        doubtChatPojo.setNumOfUnread(unreadMessages.intValue());

        return doubtChatPojo;

    }

    public void updateAwsUrl(DoubtChatMessage doubtChatMessage) {
        if (StringUtils.isNotEmpty(doubtChatMessage.getPicFileName()) && !doubtChatMessage.getPicFileName().equals("false")) {
            doubtChatMessage.setPicUrl(amazonS3Manager.getDoubtsImageUrl(doubtChatMessage.getPicFileName()));
        }
    }

    public DoubtFeedPojo toDoubtFeedPojo(PlaylistVideoPojo playlistVideoPojo) {
        DoubtFeedPojo doubtFeedPojo = new DoubtFeedPojo();
        doubtFeedPojo.setCardSubText(DoubtFeedType.CLASS);
        doubtFeedPojo.setCardTitle(playlistVideoPojo.getTitle());
        doubtFeedPojo.setThumbnailUrl(playlistVideoPojo.getThumbnailUrl());
        doubtFeedPojo.setEntityId(playlistVideoPojo.getEntityId());
        doubtFeedPojo.setTime(playlistVideoPojo.getStartTime());
        doubtFeedPojo.setTitle("Your Last Doubt");
        return doubtFeedPojo;
    }

    public DoubtFeedPojo toDoubtFeedPojo(Doubt doubt) {
        updateAWSUrl(doubt);
        DoubtFeedPojo doubtFeedPojo = new DoubtFeedPojo();
        doubtFeedPojo.setCardSubText(DoubtFeedType.DOUBT);
        if (ArrayUtils.isEmpty(doubt.gettSubjects())) {
            doubt.settSubjects(new HashSet<>());
        }
        doubtFeedPojo.setEntityId(doubt.getId().toString());
        doubtFeedPojo.setCardTitle(String.join("", doubt.gettSubjects()) + " doubt #" + doubt.getId());
        doubtFeedPojo.setThumbnailUrl(doubt.getPicUrl());
        doubtFeedPojo.setTime(doubt.getClosedTime());
        for (DoubtStateChangePojo doubtStateChangePojo : doubt.getDoubtStateChanges()) {
            if (DoubtState.DOUBT_T1_ACCEPTED.equals(doubtStateChangePojo.getNewState())) {
                doubtFeedPojo.setOpened(true);
            }
        }
        doubtFeedPojo.setDoubtState(doubt.getDoubtState().name());
        return doubtFeedPojo;
    }

    public DoubtFeedPojo toDoubtFeedPojo(CourseMobileRes courseMobileRes) {
        DoubtFeedPojo doubtFeedPojo = new DoubtFeedPojo();
        doubtFeedPojo.setCardSubText(DoubtFeedType.COURSE);
        doubtFeedPojo.setCardTitle(courseMobileRes.getTitle());
        doubtFeedPojo.setTime(courseMobileRes.getStartsFrom());
        doubtFeedPojo.setEntityId(courseMobileRes.getId());
        doubtFeedPojo.setTagInfo("Upcoming Free/Paid");
        if (StringUtils.isNotEmpty(courseMobileRes.getWebLink())) {
            doubtFeedPojo.setWebLink(courseMobileRes.getWebLink());
        }

        return doubtFeedPojo;
    }

    public DoubtFeedPojo toDoubtFeedPojo(StudyEntry studyEntry) {
        DoubtFeedPojo doubtFeedPojo = new DoubtFeedPojo();
        doubtFeedPojo.setCardSubText(DoubtFeedType.STUDY_MATERIAL);
        doubtFeedPojo.setCardTitle(studyEntry.getTitle());
        doubtFeedPojo.setEntityId(studyEntry.getId());
        doubtFeedPojo.setLevel("f1");
        return doubtFeedPojo;
    }

    public DoubtFeedPojo toDoubtFeedPojo(StudyEntryItem studyEntryItem) {
        DoubtFeedPojo doubtFeedPojo = new DoubtFeedPojo();
        doubtFeedPojo.setCardSubText(DoubtFeedType.STUDY_MATERIAL);
        doubtFeedPojo.setCardTitle(studyEntryItem.getChapterTitle());
        doubtFeedPojo.setEntityId(studyEntryItem.getId());
        doubtFeedPojo.setSeoUrl(studyEntryItem.getSeoUrl());
        return doubtFeedPojo;
    }

    public List<CourseMobileRes> getCoursesForFeed(String grade, int size) throws VException {
        String url = SUBSCRIPTION_ENDPOINT + "/course/getallcoursesforfeed?grade=" + grade + "&size=" + size;

        /*if(start != null){
            url = url + "&start="+start;
        }else{
            url = url + "&start=0";
        }
        
        if(size != null){
            url = url + "&size="+size;
        } */
        logger.info("Url for fetching courses for feed: " + url);

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<CourseMobileRes>>() {
        }.getType();
        List<CourseMobileRes> result = gson.fromJson(respString, listType);
        logger.info("Result from request: " + result);
        return result;
    }

    public List<DoubtFeedPojo> getDoubtsFeed(Long userId, String board, String grade) {

        logger.info("GET-DOUBTS-FEED");

        List<DoubtFeedPojo> doubtFeedPojos = new ArrayList<>();
        Long currentTime = System.currentTimeMillis();
        Doubt doubt = doubtsDAO.getLastSolvedDoubtByUser(userId.toString());
        GetCMDSVideoPlaylistReq req = new GetCMDSVideoPlaylistReq();
        req.setMainTags(new HashSet<>(Arrays.asList(grade)));
        req.setPublished(Boolean.TRUE);
        //CMDSVideoPlaylist cMDSVideoPlaylist = null;
        UpcommingVideosRes upcommingVideosRes = null;
        try {
            upcommingVideosRes = cMDSVideoManager.getUpcomingCMDSVideoPlaylist(req);
        } catch (Exception ex) {
            logger.info("Upcoming playlist not found for board grade");
        }
        boolean doubtAdded = false;
        if (upcommingVideosRes != null && ArrayUtils.isNotEmpty(upcommingVideosRes.getVideos())) {

            for (PlaylistVideoPojo playlistVideoPojo : upcommingVideosRes.getVideos()) {
                if (playlistVideoPojo.getStartTime() >= currentTime) {
                    if (doubt != null) {
                        doubtFeedPojos.add(toDoubtFeedPojo(doubt));
                        doubtAdded = true;
                    }
                    DoubtFeedPojo dfp = toDoubtFeedPojo(playlistVideoPojo);
                    dfp.setTitle(upcommingVideosRes.getTitle());
                    dfp.setTagInfo("LIVE NOW");
                    doubtFeedPojos.add(dfp);
                } else {
                    DoubtFeedPojo dfp = toDoubtFeedPojo(playlistVideoPojo);
                    dfp.setTitle(upcommingVideosRes.getTitle());
                    doubtFeedPojos.add(dfp);
                }
                if (doubtFeedPojos.size() == DOUBT_FEED_SIZE) {
                    return doubtFeedPojos;
                }
            }
        }

        if (doubt != null && doubtAdded == false) {
            doubtFeedPojos.add(toDoubtFeedPojo(doubt));
        }

        if (doubtFeedPojos.size() == DOUBT_FEED_SIZE) {
            return doubtFeedPojos;
        }

        if (doubtFeedPojos.size() < DOUBT_FEED_SIZE) {
            List<CourseMobileRes> courseMobileReses = new ArrayList<>();
            try {
                courseMobileReses = getCoursesForFeed(grade, DOUBT_FEED_SIZE - doubtFeedPojos.size());
                logger.info("GET-Courses-For-Feed");
                logger.info(courseMobileReses.size());
            } catch (Exception ex) {
                logger.error("Error in fetching course mobile res for feed " + ex.getMessage());
            }

            if (ArrayUtils.isNotEmpty(courseMobileReses)) {
                for (CourseMobileRes courseMobileRes : courseMobileReses) {
                    logger.info("Print-Course-Detail");
                    logger.info(courseMobileRes);
                    doubtFeedPojos.add(toDoubtFeedPojo(courseMobileRes));
                    if (doubtFeedPojos.size() == DOUBT_FEED_SIZE) {
                        return doubtFeedPojos;
                    }
                }
            }

        }

        List<StudyEntry> studyItems = studyManager.getStudyWithSubjects(new HashSet<>(Arrays.asList(grade)));
        int i = 0;
        if (ArrayUtils.isNotEmpty(studyItems)) {
            for (StudyEntry studyEntry : studyItems) {

                doubtFeedPojos.add(toDoubtFeedPojo(studyEntry));
                if (doubtFeedPojos.size() == DOUBT_FEED_SIZE) {
                    return doubtFeedPojos;
                }
                i++;
                if (i == 2) {
                    break;
                }
            }
        }

        StudyEntryItem studyEntryItem = studyManager.getMostViewedStudyEntryItem();
        if (studyEntryItem != null) {
            doubtFeedPojos.add(toDoubtFeedPojo(studyEntryItem));
        }

        return doubtFeedPojos;

    }

    public BaseTopicTree getBaseTreeForSubject(String subject) {

        BaseTopicTree baseTopicTree = topicTreeDAO.getBaseTopicTreeNodeByField(BaseTopicTree.Constants.NAME, subject);

        return baseTopicTree;
    }

    public String appendPaidViewers(AddPaidViewerReq req) throws InternalServerErrorException, BadRequestException {


		// Set Paid users backup if redis key gets reset
    	if (StringUtils.isEmpty(redisDAO.get("PAID_USERS")))
			redisDAO.set("PAID_USERS", PAID_USERS);
		String[] initialPaidUsers = redisDAO.get("PAID_USERS").split(",");
		logger.info("Previous Paid Users : " + initialPaidUsers);

		LinkedHashSet<String> paidUsers = new LinkedHashSet<String>();
		
		for(String paidUser : initialPaidUsers) {
			paidUsers.add(paidUser);
 		}
		logger.info("Previous Paid Users (LinkedHashSet) : " + paidUsers);
		logger.info("New Paid Users : " + req.getPaidViewerIds());
 		
		paidUsers.addAll(req.getPaidViewerIds());
		logger.info("Added Paid Users (LinkedHashSet) : " + paidUsers);

		String usersToAdd = String.join(",", paidUsers);
		redisDAO.set("PAID_USERS", usersToAdd);
 		
 		return redisDAO.get("PAID_USERS");
 	}

 	public UserBasicInfo fetchUserByContact(String phoneNumber) throws VException
    {
        String url = USER_ENDPOINT + "/getIdByContactNumber/" + phoneNumber;
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String respString = response.getEntity(String.class);
        logger.info("Student Id fetched from user : {}", respString);
        Type type = new TypeToken<UserBasicInfo>(){}.getType();
        return new Gson().fromJson(respString, type);
    }
 }

