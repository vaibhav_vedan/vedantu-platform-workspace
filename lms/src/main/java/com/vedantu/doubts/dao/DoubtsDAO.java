/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.dao;

import com.vedantu.User.Role;
import com.vedantu.User.enums.ViewerAccess;
import com.vedantu.doubts.entities.mongo.Doubt;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.enums.StudentDoubtStatus;
import com.vedantu.doubts.enums.UserDoubtFeedback;
import com.vedantu.doubts.request.GetDoubtDashboardReq;
import com.vedantu.lms.cmds.enums.DoubtPaidState;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.vedantu.util.enums.SortOrder;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class DoubtsDAO extends AbstractMongoDAO{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DoubtsDAO.class);

    @Autowired
    private CounterService counterService;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public DoubtsDAO(){
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(Doubt p, Long callingUserId) {
        // try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }

            if (p.getId() == null || p.getId() == 0L) {
                p.setId(counterService.getNextSequentialSequence(Doubt.class.getSimpleName()));
            }

            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }


    public List<Doubt> getDoubtStatsForUser(String userId, int start, int size){

        Query query = new Query();
//        query.addCriteria(Criteria.where(Doubt.Constants.MEMBERS_LIST).is(userId));
        query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(userId));
        query.with(Sort.by(Sort.Direction.DESC, Doubt.Constants.LAST_ACTIVITY_TIME));
        setFetchParameters(query, start, size);

        return runQuery(query, Doubt.class);
    }


    public List<Doubt> getDoubtStatsForUser(String userId, int start, int size, StudentDoubtStatus studentDoubtStatus, Role role){
        Query query = new Query();

        if(StudentDoubtStatus.ANSWERED.equals(studentDoubtStatus)){
            query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(Arrays.asList(DoubtState.DOUBT_T1_SOLVED, DoubtState.DOUBT_T2_SOLVED)));
        }else if(StudentDoubtStatus.CURRENT.equals(studentDoubtStatus)){
            query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(Arrays.asList(DoubtState.DOUBT_POSTED, DoubtState.DOUBT_T2_POOL, DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T2_ACCEPTED)));
        }
        if(Role.STUDENT.equals(role)) {
            query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(userId));
        } else {
            query.addCriteria(Criteria.where(Doubt.Constants.MEMBERS_LIST).is(userId));
        }
        query.with(Sort.by(Sort.Direction.DESC, Doubt.Constants.LAST_ACTIVITY_TIME));
        setFetchParameters(query, start, size);

        return runQuery(query, Doubt.class);
    }

    public Doubt getDoubtStatsByDoubtId(Long doubtId){
        Query query = new Query();
        query.addCriteria(Criteria.where(Doubt.Constants.ID).is(doubtId));
        return findOne(query, Doubt.class);
    }

    public void updateLastActivityTime(Long doubtId){
        Long currentTime = System.currentTimeMillis();
        Query query = new Query();
        query.addCriteria(Criteria.where(Doubt.Constants.ID).is(doubtId));
        Update update = new Update();
        update.set(Doubt.Constants.LAST_ACTIVITY_TIME, currentTime);
        updateFirst(query, update, Doubt.class);
    }

    public Long getDoubtsCountForGrade(String grade, Long dayStartTime) {
        Query query = new Query();
        if(StringUtils.isNotEmpty(grade)){
            query.addCriteria(Criteria.where(Doubt.Constants.MAIN_TAGS).in(grade));
        }
        if(dayStartTime != null) {
            query.addCriteria(Criteria.where(Doubt.Constants.CREATION_TIME).gt(dayStartTime));
        }
        return queryCount(query, Doubt.class);
    }

    public List<Doubt> getDoubtsbyStudentMailID(String studentMailID) {

        Query query = new Query();

        query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_EMAIL_ID).is(studentMailID));

        return runQuery(query, Doubt.class);

    }


    public List<Doubt> getDoubts(GetDoubtDashboardReq req, boolean isDashboard){

        List<Doubt> doubts = new ArrayList<>();

        Query query = new Query();

        if(req.getDoubtState() != null){
            query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).is(req.getDoubtState()));
        }

        if(ArrayUtils.isNotEmpty(req.getMainTags())){
            if(isDashboard) {
                query.addCriteria(Criteria.where(Doubt.Constants.MAIN_TAGS).all(req.getMainTags()));
            } else {
                query.addCriteria(Criteria.where(Doubt.Constants.MAIN_TAGS).in(req.getMainTags()));
            }
        }

        if(ArrayUtils.isNotEmpty(req.getSubjects())) {
            if(isDashboard) {
                query.addCriteria(Criteria.where(Doubt.Constants.TSUBJECTS).all(req.getSubjects()));
            } else {
                query.addCriteria(Criteria.where(Doubt.Constants.TSUBJECTS).in(req.getSubjects()));
            }
        }

        if(req.getTeacherId() != null){
            query.addCriteria(Criteria.where(Doubt.Constants.ACCEPTED_BY).is(req.getTeacherId().toString()));
        }

        if(StringUtils.isNotEmpty(req.getStudentId())){
            query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(req.getStudentId()));
        }

        if(req.getUserDoubtFeedback() != null){
            if(req.getUserDoubtFeedback().equals(UserDoubtFeedback.NA)){
                query.addCriteria(Criteria.where(Doubt.Constants.USER_DOUBT_FEEDBACK).is(null));
            }
            else {
                query.addCriteria(Criteria.where(Doubt.Constants.USER_DOUBT_FEEDBACK).is(req.getUserDoubtFeedback()));
            }
        }

        if(req.getStartTime() != null){
            if(req.getEndTime() != null){
                query.addCriteria(Criteria.where(Doubt.Constants.CREATION_TIME).gte(req.getStartTime()).lte(req.getEndTime()));
            }else{
                query.addCriteria(Criteria.where(Doubt.Constants.CREATION_TIME).gte(req.getStartTime()));
            }
        }else if(req.getEndTime() != null){

            query.addCriteria(Criteria.where(Doubt.Constants.CREATION_TIME).lte(req.getEndTime()));

        }
    	
    	if (isDashboard) {
    		if (req.getDoubtPaidState()!=null) {
    			if(DoubtPaidState.FOS.equals(req.getDoubtPaidState())) {
        			query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_PAID_STATE).is(DoubtPaidState.FOS));
        		}
    			else if (DoubtPaidState.PAID.equals(req.getDoubtPaidState())) {
        			query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_PAID_STATE).is(DoubtPaidState.PAID));
        		}
        		else if (DoubtPaidState.UNPAID.equals(req.getDoubtPaidState())) {
        			query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_PAID_STATE).is(DoubtPaidState.UNPAID));
        		}
    		}
    	}
    	else {
    		Set<DoubtPaidState> viewer = new HashSet<>();
    		logger.info("req.getViewerAccesses() : " + req.getViewerAccesses());
    		viewer.add(DoubtPaidState.UNPAID);
    		logger.info("Default Viewer : " + viewer);
        	for (ViewerAccess viewerAccess : req.getViewerAccesses()) {
        		if(ViewerAccess.FOS.equals(viewerAccess))
        			viewer.add(DoubtPaidState.FOS);
        		else if (ViewerAccess.PAID.equals(viewerAccess))
        			viewer.add(DoubtPaidState.PAID);
        	}
    		logger.info("After viewerAccess Viewer : " + viewer);

    		logger.info("req.getDoubtPaidState() : " + req.getDoubtPaidState());
        	if (req.getDoubtPaidState() != null && DoubtPaidState.FOS.equals(req.getDoubtPaidState())) {
        		if (viewer.contains(DoubtPaidState.UNPAID)) viewer.remove(DoubtPaidState.UNPAID);
        		if (viewer.contains(DoubtPaidState.PAID)) viewer.remove(DoubtPaidState.PAID);
        	}
        	else if (req.getDoubtPaidState() != null && DoubtPaidState.PAID.equals(req.getDoubtPaidState())) {
        		if (viewer.contains(DoubtPaidState.UNPAID)) viewer.remove(DoubtPaidState.UNPAID);
        		if (viewer.contains(DoubtPaidState.FOS)) viewer.remove(DoubtPaidState.FOS);
        	}
        	else if (req.getDoubtPaidState() != null && DoubtPaidState.UNPAID.equals(req.getDoubtPaidState())) {
        		if (viewer.contains(DoubtPaidState.FOS)) viewer.remove(DoubtPaidState.FOS);
        		if (viewer.contains(DoubtPaidState.PAID)) viewer.remove(DoubtPaidState.PAID);
        	}
    		logger.info("After DoubtPaidState Viewer : " + viewer);
        	
        	query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_PAID_STATE).in(viewer));
    	}

        if(req.getSort() != null) {
            if(SortOrder.ASC.equals(req.getSort())) {
                query.with(Sort.by(Sort.Direction.ASC, Doubt.Constants.CREATION_TIME));
            } else if(SortOrder.DESC.equals(req.getSort())){
                query.with(Sort.by(Sort.Direction.DESC, Doubt.Constants.CREATION_TIME));
            }
        } else {
            query.with(Sort.by(Sort.Direction.DESC, Doubt.Constants.CREATION_TIME));
        }
        
        

        setFetchParameters(query, req.getStart(), req.getSize());
        logger.info("getDoubts -> Query : " + query);

        doubts = runQuery(query, Doubt.class);
        logger.info("Doubts " + doubts);
        return doubts;
    }

    public List<Doubt> getCurrentDoubtForTeacher(String teacherId){
        Query query = new Query();
        query.addCriteria(Criteria.where(Doubt.Constants.ACCEPTED_BY).is(teacherId));
        query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(Arrays.asList(DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T2_ACCEPTED)));
        return runQuery(query, Doubt.class);
//        return findOne(query, Doubt.class);
    }

    public List<Doubt> getOpenDoubts(String userId, Role role){
        Query query = new Query();
        if(Role.TEACHER.equals(role)){
            query.addCriteria(Criteria.where(Doubt.Constants.ACCEPTED_BY).is(userId));
        }else if(Role.STUDENT.equals(role)){
            query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(userId));
        }
        query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(Arrays.asList(DoubtState.DOUBT_POSTED, DoubtState.DOUBT_T2_POOL,DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T2_ACCEPTED)));

        return runQuery(query, Doubt.class);

    }
    

    public List<Doubt> getDoubtsBetweenTimeForTeacherSpam(String userId, Long startTime, Long endTime){
        Query query = new Query();
        query.addCriteria(Criteria.where(Doubt.Constants.SPAMMED_BY).is(userId));

        query.addCriteria(Criteria.where(Doubt.Constants.CREATION_TIME).gte(startTime).lte(endTime));
        logger.info("getDoubtsBetweenTimeForTeacherSpam -> Query : " + query);
        return runQuery(query, Doubt.class);
    }

    public List<Doubt> getDoubtsBetweenTimeForTeacher(String userId, Long startTime, Long endTime){
        Query query = new Query();
        query.addCriteria(Criteria.where(Doubt.Constants.ACCEPTED_BY).is(userId));
        query.addCriteria(Criteria.where(Doubt.Constants.CLOSED_TIME).gte(startTime).lte(endTime));
        logger.info("getDoubtsBetweenTimeForTeacher -> Query : " + query);
        return runQuery(query, Doubt.class);
    }

    public List<Doubt> getDoubtsForTeachers(List<DoubtState> doubtStates){

        List<Doubt> doubts = new ArrayList<>();

        Query query = new Query();
        query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(doubtStates));
        doubts = runQuery(query, Doubt.class);

        if(ArrayUtils.isEmpty(doubts)){
            return new ArrayList<>();
        }
        return doubts;

    }

    public Doubt getLastSolvedDoubtByUser(String userId){

        Query query = new Query();
//        query.addCriteria(Criteria.where(Doubt.Constants.DOUBT_STATE).in(Arrays.asList(DoubtState.DOUBT_T1_SOLVED, DoubtState.DOUBT_T2_SOLVED)));
        query.addCriteria(Criteria.where(Doubt.Constants.STUDENT_ID).is(userId));
        query.with(Sort.by(Sort.Direction.DESC, Doubt.Constants.LAST_ACTIVITY_TIME));
        return findOne(query, Doubt.class);

    }

}
