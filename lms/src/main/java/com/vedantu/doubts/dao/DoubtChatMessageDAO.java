/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.dao;

import com.vedantu.User.Role;
import com.vedantu.doubts.entities.mongo.DoubtChatMessage;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class DoubtChatMessageDAO extends AbstractMongoDAO{


    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DoubtChatMessageDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public DoubtChatMessageDAO(){
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public void save(DoubtChatMessage p, Long callingUserId) {
        // try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }

    public List<DoubtChatMessage> getChatMessages(Long doubtId, int start, int size){

        Query query = new Query();
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.DOUBT_ID).is(doubtId));
        query.with(Sort.by(Sort.Direction.DESC, DoubtChatMessage.Constants.CREATION_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, DoubtChatMessage.class);
    }


    public DoubtChatMessage getLastMessageByTeacher(Long doubtId){
        Query query = new Query();
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.DOUBT_ID).is(doubtId));
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.FROM_ROLE).is(Role.TEACHER));
        query.with(Sort.by(Sort.Direction.DESC, DoubtChatMessage.Constants.CREATION_TIME));
        return findOne(query, DoubtChatMessage.class);
    }

    public Long getCountOfUnread(Long doubtId, String userId, Role role){
        Query query = new Query();
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.READ).is(false));
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.DOUBT_ID).is(doubtId));
        if(Role.TEACHER.equals(role)){
            query.addCriteria(Criteria.where(DoubtChatMessage.Constants.FROM_ID).is(userId));
        }else{
            query.addCriteria(Criteria.where(DoubtChatMessage.Constants.FROM_ID).ne(userId));
        }
        return queryCount(query, DoubtChatMessage.class);
    }


    public Long getCountOfUnread(List<Long> doubtIds, String userId, Role role){
        Query query = new Query();
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.READ).is(false));
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.DOUBT_ID).in(doubtIds));
        if(Role.TEACHER.equals(role)){
            query.addCriteria(Criteria.where(DoubtChatMessage.Constants.FROM_ROLE).is(Role.STUDENT));
        }else{
            query.addCriteria(Criteria.where(DoubtChatMessage.Constants.FROM_ROLE).is(Role.TEACHER));
        }
        return queryCount(query, DoubtChatMessage.class);
    }

    public DoubtChatMessage getLastChatMessage(Long doubtId){
        DoubtChatMessage doubtChatMessage = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.DOUBT_ID).is(doubtId));
        query.with(Sort.by(Sort.Direction.DESC, DoubtChatMessage.Constants.CREATION_TIME));
        setFetchParameters(query, 0, 1);
        List<DoubtChatMessage> messages = runQuery(query, DoubtChatMessage.class);
        if(ArrayUtils.isEmpty(messages)){
            return doubtChatMessage;
        }

        return messages.get(0);
    }

    public List<DoubtChatMessage> getDoubtChatMessages(List<String> messageIds){

        Query query = new Query();
        query.addCriteria(Criteria.where(DoubtChatMessage.Constants.ID).in(messageIds));

        return runQuery(query, DoubtChatMessage.class);

    }
    
    public Long getDoubtResponseTimeForUser(String userId, Long doubtId) {
    	
    	Query query = new Query();
    	query.addCriteria(Criteria.where(DoubtChatMessage.Constants.FROM_ID).is(userId));
    	query.addCriteria(Criteria.where(DoubtChatMessage.Constants.DOUBT_ID).is(doubtId));
        query.with(Sort.by(Sort.Direction.ASC, DoubtChatMessage.Constants.CREATION_TIME));   
    	
    	logger.info("getDoubtResponseTimeForUser Query -> " + query);
    	DoubtChatMessage doubtChatMessages = findOne(query, DoubtChatMessage.class);
    	logger.info("getDoubtResponseTimeForUser -> doubtChatMessages : " + doubtChatMessages);
    	if (doubtChatMessages == null) {
    		return null;
    	}
    	return doubtChatMessages.getCreationTime();
    }

}
