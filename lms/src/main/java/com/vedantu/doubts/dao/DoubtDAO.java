/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.dao;

import com.vedantu.User.Role;
import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.cmds.dao.sql.SqlSessionFactory;
import com.vedantu.doubts.entities.sql.LastDoubt;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.request.GetDoubtDashboardReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractSqlDAO;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class DoubtDAO extends AbstractSqlDAO{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(DoubtDAO.class);

    
    @Autowired
    SqlSessionFactory sqlSessionFactory;


    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }
    
    public void create(LastDoubt p, Session session) {
        create(p, session, null);
    }    
    
    public void create(LastDoubt p, Session session, String callingUserId){
        try{
            if(p != null){
                saveEntity(p, session, callingUserId);
            }
        } catch(Exception ex) {
            logger.error("Create EnrollmentTransaction: " + ex.getMessage());
        }
    }  
    
    public void save(LastDoubt p, Session session, String callingUserId){
        saveEntity(p, session, callingUserId);
    }  
    
    public LastDoubt getById(Long id, Boolean enabled, Session session) {
        LastDoubt doubt = null;
        try{
            if(id != null){
                doubt = (LastDoubt) getEntityById(id, enabled, LastDoubt.class, session);
            }
        } catch(Exception ex) {
            logger.error("getById Doubt: " + ex.getMessage());
        }
        return doubt;
    }  
    
    public List<LastDoubt> getDoubts(GetDoubtDashboardReq req){
        
        List<LastDoubt> doubts = new ArrayList<>();
        
        Session session = sqlSessionFactory.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(LastDoubt.class);
            if(req.getDoubtState() != null){
                criteria.add(Restrictions.eq(LastDoubt.Constants.DOUBT_STATE, req.getDoubtState()));
            }
            if(StringUtils.isNotEmpty(req.getBoard())){
                criteria.add(Restrictions.eq(LastDoubt.Constants.BOARD, req.getBoard()));
            }
            if(StringUtils.isNotEmpty(req.getGrade())){
                criteria.add(Restrictions.eq(LastDoubt.Constants.GRADE, req.getGrade()));
            }
            if(StringUtils.isNotEmpty(req.getTarget())){
                criteria.add(Restrictions.eq(LastDoubt.Constants.TARGET, req.getTarget()));
            }
            if(req.getBoardId() != null){
                criteria.add(Restrictions.eq(LastDoubt.Constants.BOARD_ID, req.getBoardId()));
            }
            if(req.getTeacherId() != null){
                criteria.add(Restrictions.eq(LastDoubt.Constants.ACCEPTED_BY, req.getTeacherId().toString()));
            }
            
            if(StringUtils.isNotEmpty(req.getStudentId())){
                criteria.add(Restrictions.eq(LastDoubt.Constants.ASKED_BY, req.getStudentId()));
            }
            
            if(req.getStartTime() != null){
                criteria.add(Restrictions.ge(LastDoubt.Constants.CREATION_TIME, req.getStartTime()));
            }
            
            if(req.getEndTime() != null){
                criteria.add(Restrictions.le(LastDoubt.Constants.CREATION_TIME, req.getEndTime()));
            }
            
            setFetchParameters(criteria, req.getStart(), req.getSize());
            
            criteria.addOrder(Order.asc(LastDoubt.Constants.CREATION_TIME));
            
            doubts = runQuery(session, criteria, LastDoubt.class);
        }catch(Exception ex){
            logger.error("getDoubts : "+ex.getMessage());            
        }finally{
            session.close();
        }
        
        return doubts;
    }
    
    public LastDoubt getCurrentDoubtForTeacher(String teacherId){        
        Session session = sqlSessionFactory.getSessionFactory().openSession();
        LastDoubt doubt = null;
        try{
            Criteria criteria = session.createCriteria(LastDoubt.class);
            criteria.add(Restrictions.eq(LastDoubt.Constants.ACCEPTED_BY, teacherId));
            criteria.add(Restrictions.in(LastDoubt.Constants.DOUBT_STATE, Arrays.asList(DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T2_ACCEPTED)));
            List<LastDoubt> doubts = runQuery(session, criteria, LastDoubt.class);
            if(ArrayUtils.isNotEmpty(doubts)){
                doubt = doubts.get(0);
            }
        } catch(Exception ex) {
            logger.error("getById EnrollmentTransaction: " + ex.getMessage());
        }finally{
            session.close();
        }        
        return doubt;
    }
    
    public LastDoubt getDoubtById(Long id, Session session, LockMode lockMode) {
        LastDoubt doubt = null;
        Criteria criteria = session.createCriteria(LastDoubt.class);
        criteria.add(Restrictions.eq(LastDoubt.Constants.ID, id));
        List<LastDoubt> doubts = runQuery(session, criteria, LastDoubt.class, lockMode);
        
        if(ArrayUtils.isNotEmpty(doubts)){
            doubt = doubts.get(0);
        }
        
        return doubt;
    }      
    
    public void update(LastDoubt doubt, Session session){
        update(doubt, session, null);
    }
    
    public void update(LastDoubt doubt, Session session, String callingUserId){
        try{
            if(doubt != null){
                updateEntity(doubt, session, callingUserId);
            }
        }catch(Exception ex){
            logger.error("update Doubt: " + ex.getMessage());            
        }
    }
    
    public List<LastDoubt> getOpenDoubts(String userId, Role role){
        List<LastDoubt> doubts = new ArrayList<>();
        
        Session session = sqlSessionFactory.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(LastDoubt.class);
            if(Role.TEACHER.equals(role)){
                criteria.add(Restrictions.eq(LastDoubt.Constants.ACCEPTED_BY, userId));
            }
            else if(Role.STUDENT.equals(role)){
                criteria.add(Restrictions.eq(LastDoubt.Constants.ASKED_BY, userId));
            }
            
            criteria.add(Restrictions.in(LastDoubt.Constants.DOUBT_STATE, Arrays.asList(DoubtState.DOUBT_POSTED, DoubtState.DOUBT_T2_POOL,DoubtState.DOUBT_T1_ACCEPTED, DoubtState.DOUBT_T2_ACCEPTED)));
            
            doubts = runQuery(session, criteria, LastDoubt.class);
            
        }catch(Exception ex){
            logger.error("getDoubts : "+ex.getMessage());            
        }finally{
            session.close();
        }   
        
        return doubts;
    }
    
    public List<LastDoubt> getDoubtsBetweenTime(String userId, Long startTime, Long endTime){
        
        List<LastDoubt> doubts = new ArrayList<>();
        Session session = sqlSessionFactory.getSessionFactory().openSession();
        try{
            Criteria criteria = session.createCriteria(LastDoubt.class);
            criteria.add(Restrictions.eq(LastDoubt.Constants.ACCEPTED_BY, userId));
            criteria.add(Restrictions.between(LastDoubt.Constants.CREATION_TIME, startTime, endTime));
            doubts = runQuery(session, criteria, LastDoubt.class);
        }catch(Exception ex){
            logger.error("get doubts between time: "+ex.getMessage());
        }finally{
            session.close();
        }
        
        return doubts;
        
    }
    
    public List<LastDoubt> getDoubtsForTeachers(List<DoubtState> doubtStates){
        
        List<LastDoubt> doubts = new ArrayList<>();
        
        Session session = sqlSessionFactory.getSessionFactory().openSession();
        try{
            
            Criteria criteria = session.createCriteria(LastDoubt.class);
            
            criteria.add(Restrictions.in(LastDoubt.Constants.DOUBT_STATE, doubtStates));
            
            doubts = runQuery(session, criteria, LastDoubt.class);
        }catch(Exception ex){
            logger.error("getDoubts : "+ex.getMessage());            
        }finally{
            session.close();
        }
        
        return doubts;
    }    
    
}
