/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import com.vedantu.User.enums.TeacherPoolType;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class DexLivePojo {
    
    private TeacherPoolType teacherPoolType;
    private String email;
    private String fullName;
    private boolean available;
    private boolean inChat;
    private Set<String> grades;
    private Set<String> boards;
    private Set<String> targets;
    private Set<String> topics;
    private String contactNumber;

    /**
     * @return the teacherPoolType
     */
    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    /**
     * @param teacherPoolType the teacherPoolType to set
     */
    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the fullName
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName the fullName to set
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return the available
     */
    public boolean isAvailable() {
        return available;
    }

    /**
     * @param available the available to set
     */
    public void setAvailable(boolean available) {
        this.available = available;
    }

    /**
     * @return the inChat
     */
    public boolean isInChat() {
        return inChat;
    }

    /**
     * @param inChat the inChat to set
     */
    public void setInChat(boolean inChat) {
        this.inChat = inChat;
    }

    /**
     * @return the grades
     */
    public Set<String> getGrades() {
        return grades;
    }

    /**
     * @param grades the grades to set
     */
    public void setGrades(Set<String> grades) {
        this.grades = grades;
    }

    /**
     * @return the boards
     */
    public Set<String> getBoards() {
        return boards;
    }

    /**
     * @param boards the boards to set
     */
    public void setBoards(Set<String> boards) {
        this.boards = boards;
    }

    /**
     * @return the targets
     */
    public Set<String> getTargets() {
        return targets;
    }

    /**
     * @param targets the targets to set
     */
    public void setTargets(Set<String> targets) {
        this.targets = targets;
    }

    /**
     * @return the topics
     */
    public Set<String> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }

    /**
     * @return the contactNumber
     */
    public String getContactNumber() {
        return contactNumber;
    }

    /**
     * @param contactNumber the contactNumber to set
     */
    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    @Override
    public String toString() {
        return "DexLivePojo{" + "teacherPoolType=" + teacherPoolType + ", email=" + email + ", fullName=" + fullName + ", available=" + available + ", inChat=" + inChat + ", grades=" + grades + ", boards=" + boards + ", targets=" + targets + ", topics=" + topics + ", contactNumber=" + contactNumber + '}';
    }

}
