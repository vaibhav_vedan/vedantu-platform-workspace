/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import com.vedantu.util.PlatformBasicResponse;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class GetNodeServerTeacherIdsPojo extends PlatformBasicResponse {
    
    private NodeServerTeacherResult result;

    /**
     * @return the result
     */
    public NodeServerTeacherResult getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(NodeServerTeacherResult result) {
        this.result = result;
    }
    
}
