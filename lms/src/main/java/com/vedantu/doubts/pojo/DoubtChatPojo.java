/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import com.vedantu.doubts.enums.DoubtSpamType;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;

/**
 *
 * @author parashar
 */
public class DoubtChatPojo extends AbstractTargetTopicEntity{
    
    private Long doubtId;
    private String studentId;
    private int numOfUnread = 0;
    private DoubtChatMessagePojo doubtChatMessagePojo;
    private Long lastActivity;
    private Long startedOn;
    private DoubtState doubtState;
    private String picUrl;
    private boolean opened = false;
    private DoubtSpamType doubtSpamType;
    private boolean userFeedback = false;

    public DoubtChatPojo(Long doubtId, String studentId, DoubtChatMessagePojo doubtChatMessagePojo, Long lastActivity, Long startedOn, DoubtState doubtState, int numOfUnread) {
        this.doubtId = doubtId;
        this.studentId = studentId;
        this.doubtChatMessagePojo = doubtChatMessagePojo;
        this.lastActivity = lastActivity;
        this.startedOn = startedOn;
        this.doubtState = doubtState;
        this.numOfUnread = numOfUnread;        
    }

    public DoubtChatPojo() {
    }

    /**
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    /**
     * @return the numOfUnread
     */
    public int getNumOfUnread() {
        return numOfUnread;
    }

    /**
     * @param numOfUnread the numOfUnread to set
     */
    public void setNumOfUnread(int numOfUnread) {
        this.numOfUnread = numOfUnread;
    }

    /**
     * @return the doubtChatMessagePojo
     */
    public DoubtChatMessagePojo getDoubtChatMessagePojo() {
        return doubtChatMessagePojo;
    }

    /**
     * @param doubtChatMessagePojo the doubtChatMessagePojo to set
     */
    public void setDoubtChatMessagePojo(DoubtChatMessagePojo doubtChatMessagePojo) {
        this.doubtChatMessagePojo = doubtChatMessagePojo;
    }

    /**
     * @return the lastActivity
     */
    public Long getLastActivity() {
        return lastActivity;
    }

    /**
     * @param lastActivity the lastActivity to set
     */
    public void setLastActivity(Long lastActivity) {
        this.lastActivity = lastActivity;
    }

    /**
     * @return the startedOn
     */
    public Long getStartedOn() {
        return startedOn;
    }

    /**
     * @param startedOn the startedOn to set
     */
    public void setStartedOn(Long startedOn) {
        this.startedOn = startedOn;
    }

    /**
     * @return the doubtState
     */
    public DoubtState getDoubtState() {
        return doubtState;
    }

    /**
     * @param doubtState the doubtState to set
     */
    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }


    /**
     * @return the opened
     */
    public boolean isOpened() {
        return opened;
    }

    /**
     * @param opened the opened to set
     */
    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the doubtSpamType
     */
    public DoubtSpamType getDoubtSpamType() {
        return doubtSpamType;
    }

    /**
     * @param doubtSpamType the doubtSpamType to set
     */
    public void setDoubtSpamType(DoubtSpamType doubtSpamType) {
        this.doubtSpamType = doubtSpamType;
    }

    /**
     * @return the userFeedback
     */
    public boolean isUserFeedback() {
        return userFeedback;
    }

    /**
     * @param userFeedback the userFeedback to set
     */
    public void setUserFeedback(boolean userFeedback) {
        this.userFeedback = userFeedback;
    }

    @Override
    public String toString() {
        return "DoubtChatPojo{" + "doubtId=" + doubtId + ", studentId=" + studentId + ", numOfUnread=" + numOfUnread + ", doubtChatMessagePojo=" + doubtChatMessagePojo + ", lastActivity=" + lastActivity + ", startedOn=" + startedOn + ", doubtState=" + doubtState + ", picUrl=" + picUrl + ", opened=" + opened + ", doubtSpamType=" + doubtSpamType + ", userFeedback=" + userFeedback + '}';
    }

}
