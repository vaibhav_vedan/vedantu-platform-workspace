/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import java.util.List;

/**
 *
 * @author parashar
 */
public class UnreadMessagesCount {

    private Integer unreadCount;
    private Integer doubtsCount;
    private Integer maxDoubtCount;
    private Long feedbackRequiredFor;
    private Integer dexStartTime;
    private Integer dexEndTime;
    private List<String> activeTabs;
    private String unreadMessageCountText;

    private String homePageBannerImage;
    private String homePageBannerUrl;
    private String studyPageBannerImage;
    private String studyPageBannerUrl;
    private boolean homePageShow = false;
    private boolean studyPageShow = false;

    private boolean showPopup = false;
    private String popupText;
    private boolean allowDoubts = true;

    private AmplifyEvents amplifyEvents;
    private AmplifyEvents firebaseEvents;

    /**
     * @return the unreadCount
     */
    public Integer getUnreadCount() {
        return unreadCount;
    }

    /**
     * @param unreadCount the unreadCount to set
     */
    public void setUnreadCount(Integer unreadCount) {
        this.unreadCount = unreadCount;
    }

    /**
     * @return the doubtsCount
     */
    public Integer getDoubtsCount() {
        return doubtsCount;
    }

    /**
     * @param doubtsCount the doubtsCount to set
     */
    public void setDoubtsCount(Integer doubtsCount) {
        this.doubtsCount = doubtsCount;
    }

    public Integer getMaxDoubtCount() {
        return maxDoubtCount;
    }

    public void setMaxDoubtCount(Integer maxDoubtCount) {
        this.maxDoubtCount = maxDoubtCount;
    }

    public Long getFeedbackRequiredFor() {
        return feedbackRequiredFor;
    }

    public void setFeedbackRequiredFor(Long feedbackRequiredFor) {
        this.feedbackRequiredFor = feedbackRequiredFor;
    }

    public Integer getDexStartTime() {
        return dexStartTime;
    }

    public void setDexStartTime(Integer dexStartTime) {
        this.dexStartTime = dexStartTime;
    }

    public Integer getDexEndTime() {
        return dexEndTime;
    }

    public void setDexEndTime(Integer dexEndTime) {
        this.dexEndTime = dexEndTime;
    }

    public List<String> getActiveTabs() {
        return activeTabs;
    }

    public void setActiveTabs(List<String> activeTabs) {
        this.activeTabs = activeTabs;
    }

    public String getUnreadMessageCountText() {
        return unreadMessageCountText;
    }

    public void setUnreadMessageCountText(String unreadMessageCountText) {
        this.unreadMessageCountText = unreadMessageCountText;
    }

    public boolean isShowPopup() {
        return showPopup;
    }

    public void setShowPopup(boolean showPopup) {
        this.showPopup = showPopup;
    }

    public String getPopupText() {
        return popupText;
    }

    public void setPopupText(String popupText) {
        this.popupText = popupText;
    }

    public boolean isAllowDoubts() {
        return allowDoubts;
    }

    public void setAllowDoubts(boolean allowDoubts) {
        this.allowDoubts = allowDoubts;
    }

    public String getHomePageBannerImage() {
        return homePageBannerImage;
    }

    public void setHomePageBannerImage(String homePageBannerImage) {
        this.homePageBannerImage = homePageBannerImage;
    }

    public String getHomePageBannerUrl() {
        return homePageBannerUrl;
    }

    public void setHomePageBannerUrl(String homePageBannerUrl) {
        this.homePageBannerUrl = homePageBannerUrl;
    }

    public String getStudyPageBannerImage() {
        return studyPageBannerImage;
    }

    public void setStudyPageBannerImage(String studyPageBannerImage) {
        this.studyPageBannerImage = studyPageBannerImage;
    }

    public String getStudyPageBannerUrl() {
        return studyPageBannerUrl;
    }

    public void setStudyPageBannerUrl(String studyPageBannerUrl) {
        this.studyPageBannerUrl = studyPageBannerUrl;
    }

    public boolean isHomePageShow() {
        return homePageShow;
    }

    public void setHomePageShow(boolean homePageShow) {
        this.homePageShow = homePageShow;
    }

    public boolean isStudyPageShow() {
        return studyPageShow;
    }

    public void setStudyPageShow(boolean studyPageShow) {
        this.studyPageShow = studyPageShow;
    }

    public AmplifyEvents getAmplifyEvents() {
        return amplifyEvents;
    }

    public void setAmplifyEvents(AmplifyEvents amplifyEvents) {
        this.amplifyEvents = amplifyEvents;
    }

    public AmplifyEvents getFirebaseEvents() {
        return firebaseEvents;
    }

    public void setFirebaseEvents(AmplifyEvents firebaseEvents) {
        this.firebaseEvents = firebaseEvents;
    }

    @Override
    public String toString() {
        return "UnreadMessagesCount{" + "unreadCount=" + unreadCount + ", doubtsCount=" + doubtsCount + ", maxDoubtCount=" + maxDoubtCount + '}';
    }

}
