/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import com.vedantu.User.Role;
import com.vedantu.User.enums.TeacherPoolType;

/**
 *
 * @author parashar
 */
public class DoubtSolverPojo {
    
    private String solverId;
    private Role role;
    private Long acceptedTime;
    private TeacherPoolType teacherPoolType;

    /**
     * @return the solverId
     */
    public String getSolverId() {
        return solverId;
    }

    /**
     * @param solverId the solverId to set
     */
    public void setSolverId(String solverId) {
        this.solverId = solverId;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the acceptedTime
     */
    public Long getAcceptedTime() {
        return acceptedTime;
    }

    /**
     * @param acceptedTime the acceptedTime to set
     */
    public void setAcceptedTime(Long acceptedTime) {
        this.acceptedTime = acceptedTime;
    }

    /**
     * @return the teacherPoolType
     */
    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    /**
     * @param teacherPoolType the teacherPoolType to set
     */
    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }
    
}
