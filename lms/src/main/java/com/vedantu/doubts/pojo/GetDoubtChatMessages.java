/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import com.vedantu.doubts.enums.DoubtState;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class GetDoubtChatMessages {

    private List<DoubtChatMessagePojo> doubtChatMessagePojos = new ArrayList<>();
    private Long lastTeacherTime;
    private Long closedTime;
    private DoubtState doubtState;

    /**
     * @return the lastTeacherTime
     */
    public Long getLastTeacherTime() {
        return lastTeacherTime;
    }

    /**
     * @param lastTeacherTime the lastTeacherTime to set
     */
    public void setLastTeacherTime(Long lastTeacherTime) {
        this.lastTeacherTime = lastTeacherTime;
    }

    /**
     * @return the doubtChatMessagePojos
     */
    public List<DoubtChatMessagePojo> getDoubtChatMessagePojos() {
        return doubtChatMessagePojos;
    }

    /**
     * @param doubtChatMessagePojos the doubtChatMessagePojos to set
     */
    public void setDoubtChatMessagePojos(List<DoubtChatMessagePojo> doubtChatMessagePojos) {
        this.doubtChatMessagePojos = doubtChatMessagePojos;
    }

    /**
     * @return the closedTime
     */
    public Long getClosedTime() {
        return closedTime;
    }

    /**
     * @param closedTime the closedTime to set
     */
    public void setClosedTime(Long closedTime) {
        this.closedTime = closedTime;
    }

    public DoubtState getDoubtState() {
        return doubtState;
    }

    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    @Override
    public String toString() {
        return "GetDoubtChatMessages{" + "doubtChatMessagePojos=" + doubtChatMessagePojos + ", lastTeacherTime=" + lastTeacherTime + ", closedTime=" + closedTime + ", doubtState=" + doubtState + '}';
    }

}
