/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import com.vedantu.doubts.enums.DoubtState;

/**
 *
 * @author parashar
 */
public class DoubtStateChangePojo {
    
    private Long changeTime;
    private DoubtState previousState;
    private DoubtState newState;
    private String dexId;

    public DoubtStateChangePojo() {
    }

    public DoubtStateChangePojo(Long changeTime, DoubtState previousState, DoubtState newState, String dexId) {
        this.changeTime = changeTime;
        this.previousState = previousState;
        this.newState = newState;
        this.dexId = dexId;
    }
    

    /**
     * @return the changeTime
     */
    public Long getChangeTime() {
        return changeTime;
    }

    /**
     * @param changeTime the changeTime to set
     */
    public void setChangeTime(Long changeTime) {
        this.changeTime = changeTime;
    }

    /**
     * @return the previousState
     */
    public DoubtState getPreviousState() {
        return previousState;
    }

    /**
     * @param previousState the previousState to set
     */
    public void setPreviousState(DoubtState previousState) {
        this.previousState = previousState;
    }

    /**
     * @return the newState
     */
    public DoubtState getNewState() {
        return newState;
    }

    /**
     * @param newState the newState to set
     */
    public void setNewState(DoubtState newState) {
        this.newState = newState;
    }

	public String getDexId() {
		return dexId;
	}

	public void setDexId(String dexId) {
		this.dexId = dexId;
	}
    
    
}
