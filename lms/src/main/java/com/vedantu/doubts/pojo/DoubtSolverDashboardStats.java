/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import java.util.List;

/**
 *
 * @author parashar
 */
public class DoubtSolverDashboardStats {
    
    private int t1Count;
    private int t2Count;
    private List<DexLivePojo> dexInfos;

    /**
     * @return the t1Count
     */
    public int getT1Count() {
        return t1Count;
    }

    /**
     * @param t1Count the t1Count to set
     */
    public void setT1Count(int t1Count) {
        this.t1Count = t1Count;
    }

    /**
     * @return the t2Count
     */
    public int getT2Count() {
        return t2Count;
    }

    /**
     * @param t2Count the t2Count to set
     */
    public void setT2Count(int t2Count) {
        this.t2Count = t2Count;
    }

    /**
     * @return the t1Dexes
     */
    public List<DexLivePojo> getDexInfos() {
        return dexInfos;
    }

    /**
     * @param dexInfos the t1Dexes to set
     */
    public void setDexInfos(List<DexLivePojo> dexInfos) {
        this.dexInfos = dexInfos;
    }

    @Override
    public String toString() {
        return "DoubtSolverDashboardStats{" + "t1Count=" + t1Count + ", t2Count=" + t2Count + ", dexInfos=" + dexInfos + '}';
    }

}
