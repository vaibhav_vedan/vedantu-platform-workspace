/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.doubts.entities.mongo.Doubt;

/**
 *
 * @author parashar
 */
public class DoubtBasicInfo extends Doubt {

    private UserBasicInfo acceptedByUser;
    private UserBasicInfo askedByUser;

    /**
     * @return the acceptedByUser
     */
    public UserBasicInfo getAcceptedByUser() {
        return acceptedByUser;
    }

    /**
     * @param acceptedByUser the acceptedByUser to set
     */
    public void setAcceptedByUser(UserBasicInfo acceptedByUser) {
        this.acceptedByUser = acceptedByUser;
    }

    /**
     * @return the askedByUser
     */
    public UserBasicInfo getAskedByUser() {
        return askedByUser;
    }

    /**
     * @param askedByUser the askedByUser to set
     */
    public void setAskedByUser(UserBasicInfo askedByUser) {
        this.askedByUser = askedByUser;
    }

    @Override
    public String toString() {
        return "DoubtBasicInfo{" + "acceptedByUser=" + acceptedByUser + ", askedByUser=" + askedByUser + '}';
    }

}
