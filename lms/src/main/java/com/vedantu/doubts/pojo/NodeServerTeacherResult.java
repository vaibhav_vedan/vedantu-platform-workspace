/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class NodeServerTeacherResult {
    
    private List<String> teacherIds; 

    /**
     * @return the teacherIds
     */
    public List<String> getTeacherIds() {
        return teacherIds;
    }

    /**
     * @param teacherIds the teacherIds to set
     */
    public void setTeacherIds(List<String> teacherIds) {
        this.teacherIds = teacherIds;
    }
    
}
