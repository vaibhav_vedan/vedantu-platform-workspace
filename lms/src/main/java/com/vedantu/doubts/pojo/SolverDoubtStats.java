/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.pojo;

/**
 *
 * @author parashar
 */
public class SolverDoubtStats {

    private Integer countOfAllDoubts;
    private Long averageDuration;

    private Integer feedbackYesCount;
    private Integer feedbackNoCount;
    private Integer feedbackPendingCount;
    private Long averageFRT;
    private Integer spamCount;

    /**
     * @return the countOfAllDoubts
     */
    public Integer getCountOfAllDoubts() {
        return countOfAllDoubts;
    }

    /**
     * @param countOfAllDoubts the countOfAllDoubts to set
     */
    public void setCountOfAllDoubts(Integer countOfAllDoubts) {
        this.countOfAllDoubts = countOfAllDoubts;
    }

    /**
     * @return the averageDuration
     */
    public Long getAverageDuration() {
        return averageDuration;
    }

    /**
     * @param averageDuration the averageDuration to set
     */
    public void setAverageDuration(Long averageDuration) {
        this.averageDuration = averageDuration;
    }

    public Integer getFeedbackYesCount() {
        return feedbackYesCount;
    }

    public void setFeedbackYesCount(Integer feedbackYesCount) {
        this.feedbackYesCount = feedbackYesCount;
    }

    public Integer getFeedbackNoCount() {
        return feedbackNoCount;
    }

    public void setFeedbackNoCount(Integer feedbackNoCount) {
        this.feedbackNoCount = feedbackNoCount;
    }

    public Integer getFeedbackPendingCount() {
        return feedbackPendingCount;
    }

    public void setFeedbackPendingCount(Integer feedbackPendingCount) {
        this.feedbackPendingCount = feedbackPendingCount;
    }

    public Long getAverageFRT() {
        return averageFRT;
    }

    public void setAverageFRT(Long averageFRT) {
        this.averageFRT = averageFRT;
    }

	public Integer getSpamCount() {
		return spamCount;
	}

	public void setSpamCount(Integer spamCount) {
		this.spamCount = spamCount;
	}

    @Override
    public String toString() {
        return "SolverDoubtStats{" + "countOfAllDoubts=" + countOfAllDoubts + ", averageDuration=" + averageDuration + '}';
    }

}
