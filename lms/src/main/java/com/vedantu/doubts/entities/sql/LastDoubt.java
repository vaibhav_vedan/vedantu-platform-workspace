/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.entities.sql;

import com.vedantu.doubts.enums.DoubtSpamType;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.enums.UserDoubtFeedback;
import com.vedantu.doubts.request.CreateDoubtsReq;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 *
 * @author parashar
 */
@Entity
@Table(name = "doubtss")
public class LastDoubt extends AbstractSqlLongIdEntity {

    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String text;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String picUrl;
    @Enumerated(EnumType.STRING)
    private DoubtState doubtState = DoubtState.DOUBT_POSTED;
    private Long boardId;
    private String grade;
    private String target;
    private String board;
    private String askedBy;
    private String acceptedBy;
    private String closedBy;
    private UserDoubtFeedback userDoubtFeedback;
    private String userRemark;
    private String reviewedBy;
    private Boolean reviewed = false;
    private String reviewRemarks;
    @Enumerated(EnumType.STRING)
    private DoubtSpamType doubtSpamType;
    private String spamReason;
    private String spammedBy;
    private Long closedTime;
    private String picFileName;

    public LastDoubt() {
    }

    public LastDoubt(String text, String picUrl, String askedBy, String acceptedBy, String closedBy, String userRemark, String reviewedBy, String reviewRemarks, DoubtSpamType doubtSpamType, String spamReason, Long boardId, String grade, String target, String board) {
        this.text = text;
        this.picUrl = picUrl;
        this.askedBy = askedBy;
        this.acceptedBy = acceptedBy;
        this.closedBy = closedBy;
        this.userRemark = userRemark;
        this.reviewedBy = reviewedBy;
        this.reviewRemarks = reviewRemarks;
        this.doubtSpamType = doubtSpamType;
        this.spamReason = spamReason;
        this.target = target;
        this.grade = grade;
        this.boardId = boardId;
        this.board = board;
    }

    public LastDoubt(CreateDoubtsReq createDoubtsReq, Long callingUserId) {
        this.text = createDoubtsReq.getText();
        this.picUrl = createDoubtsReq.getPicUrl();
        if (createDoubtsReq.getCallingUserId() != null) {
            this.askedBy = createDoubtsReq.getCallingUserId().toString();
        } else {
            this.askedBy = callingUserId.toString();
        }
        this.picFileName = createDoubtsReq.getPicFileName();
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the doubtState
     */
    public DoubtState getDoubtState() {
        return doubtState;
    }

    /**
     * @param doubtState the doubtState to set
     */
    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    /**
     * @return the askedBy
     */
    public String getAskedBy() {
        return askedBy;
    }

    /**
     * @param askedBy the askedBy to set
     */
    public void setAskedBy(String askedBy) {
        this.askedBy = askedBy;
    }

    /**
     * @return the acceptedBy
     */
    public String getAcceptedBy() {
        return acceptedBy;
    }

    /**
     * @param acceptedBy the acceptedBy to set
     */
    public void setAcceptedBy(String acceptedBy) {
        this.acceptedBy = acceptedBy;
    }

    /**
     * @return the closedBy
     */
    public String getClosedBy() {
        return closedBy;
    }

    /**
     * @param closedBy the closedBy to set
     */
    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    /**
     * @return the userRemark
     */
    public String getUserRemark() {
        return userRemark;
    }

    /**
     * @param userRemark the userRemark to set
     */
    public void setUserRemark(String userRemark) {
        this.userRemark = userRemark;
    }

    /**
     * @return the reviewedBy
     */
    public String getReviewedBy() {
        return reviewedBy;
    }

    /**
     * @param reviewedBy the reviewedBy to set
     */
    public void setReviewedBy(String reviewedBy) {
        this.reviewedBy = reviewedBy;
    }

    /**
     * @return the reviewed
     */
    public Boolean getReviewed() {
        return reviewed;
    }

    /**
     * @param reviewed the reviewed to set
     */
    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    /**
     * @return the reviewRemarks
     */
    public String getReviewRemarks() {
        return reviewRemarks;
    }

    /**
     * @param reviewRemarks the reviewRemarks to set
     */
    public void setReviewRemarks(String reviewRemarks) {
        this.reviewRemarks = reviewRemarks;
    }

    /**
     * @return the doubtSpamType
     */
    public DoubtSpamType getDoubtSpamType() {
        return doubtSpamType;
    }

    /**
     * @param doubtSpamType the doubtSpamType to set
     */
    public void setDoubtSpamType(DoubtSpamType doubtSpamType) {
        this.doubtSpamType = doubtSpamType;
    }

    /**
     * @return the spamReason
     */
    public String getSpamReason() {
        return spamReason;
    }

    /**
     * @param spamReason the spamReason to set
     */
    public void setSpamReason(String spamReason) {
        this.spamReason = spamReason;
    }

    /**
     * @return the boardId
     */
    public Long getBoardId() {
        return boardId;
    }

    /**
     * @param boardId the boardId to set
     */
    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    /**
     * @return the grade
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @param grade the grade to set
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return the board
     */
    public String getBoard() {
        return board;
    }

    /**
     * @param board the board to set
     */
    public void setBoard(String board) {
        this.board = board;
    }

    /**
     * @return the spammedBy
     */
    public String getSpammedBy() {
        return spammedBy;
    }

    /**
     * @param spammedBy the spammedBy to set
     */
    public void setSpammedBy(String spammedBy) {
        this.spammedBy = spammedBy;
    }

    /**
     * @return the userDoubtFeedback
     */
    public UserDoubtFeedback getUserDoubtFeedback() {
        return userDoubtFeedback;
    }

    /**
     * @param userDoubtFeedback the userDoubtFeedback to set
     */
    public void setUserDoubtFeedback(UserDoubtFeedback userDoubtFeedback) {
        this.userDoubtFeedback = userDoubtFeedback;
    }

    /**
     * @return the closedTime
     */
    public Long getClosedTime() {
        return closedTime;
    }

    /**
     * @param closedTime the closedTime to set
     */
    public void setClosedTime(Long closedTime) {
        this.closedTime = closedTime;
    }

    public static class Constants extends com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity.Constants {

        public static final String ASKED_BY = "askedBy";
        public static final String ACCEPTED_BY = "acceptedBy";
        public static final String BOARD_ID = "boardId";
        public static final String TARGET = "target";
        public static final String BOARD = "board";
        public static final String GRADE = "grade";
        public static final String DOUBT_STATE = "doubtState";

    }

    /**
     * @return the picFileName
     */
    public String getPicFileName() {
        return picFileName;
    }

    /**
     * @param picFileName the picFileName to set
     */
    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }

}
