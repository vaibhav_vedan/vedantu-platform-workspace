/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.entities.mongo;

import com.vedantu.doubts.enums.DoubtSpamType;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.enums.QCFeedbackState;
import com.vedantu.doubts.enums.QCOption;
import com.vedantu.doubts.enums.UserDoubtFeedback;
import com.vedantu.doubts.pojo.DoubtSolverPojo;
import com.vedantu.doubts.pojo.DoubtStateChangePojo;
import com.vedantu.doubts.request.CreateDoubtsReq;
import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.lms.cmds.enums.DoubtPaidState;
import com.vedantu.platform.enums.SocialContextType;
import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicLongIdEntity;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import static com.vedantu.platform.enums.SocialContextType.DOUBT;

/**
 *
 * @author parashar
 */
@Document(collection = "Doubt")
public class Doubt extends AbstractTargetTopicLongIdEntity implements HomeFeedCardData {
    
    @Indexed(background = true)
    private String studentId;
    private Set<String> membersList = new HashSet<>();
    private List<DoubtStateChangePojo> doubtStateChanges = new ArrayList<>();
    private List<DoubtSolverPojo> doubtSolverPojoList = new ArrayList<>();
    private Long lastActivityTime;
    private String picUrl;
    private String text;
    @Indexed(background = true)    
    private DoubtState doubtState = DoubtState.DOUBT_POSTED;
    private String acceptedBy;
    private String closedBy;
    private DoubtSpamType doubtSpamType;
    private String spamReason;
    private String spammedBy;
    private Long closedTime;
    private UserDoubtFeedback userDoubtFeedback;
    private String userRemark;
    private String reviewedBy;
    private Boolean reviewed = false;
    private String reviewRemarks;  
    private QCOption qcOption;
    private List<QCFeedbackState> qcFeedbackStates;
    private Long duration;
    private String picFileName;
    private String unableToSolveReason;
	private boolean unableToSolveT2 = false;
    @Indexed(background = true)
    private DoubtPaidState doubtPaidState;
    private SocialContextType socialContextType = DOUBT;
    private boolean opened = false;
 
    
    public Doubt() {
    }    

    public Doubt(CreateDoubtsReq createDoubtsReq, Long callingUserId) {
        this.text = createDoubtsReq.getText();
        this.picUrl = createDoubtsReq.getPicUrl();
        if (createDoubtsReq.getCallingUserId() != null) {
            this.studentId = createDoubtsReq.getCallingUserId().toString();
        } else {
            this.studentId = callingUserId.toString();
        }
        this.picFileName =  createDoubtsReq.getPicFileName();
    }

    /**
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }
    
    /**
     * @return the membersList
     */
    public Set<String> getMembersList() {
        return membersList;
    }

    /**
     * @param membersList the membersList to set
     */
    public void setMembersList(Set<String> membersList) {
        this.membersList = membersList;
    }

    /**
     * @return the doubtStateChanges
     */
    public List<DoubtStateChangePojo> getDoubtStateChanges() {
        return doubtStateChanges;
    }

    /**
     * @param doubtStateChanges the doubtStateChanges to set
     */
    public void setDoubtStateChanges(List<DoubtStateChangePojo> doubtStateChanges) {
        this.doubtStateChanges = doubtStateChanges;
    }

    /**
     * @return the lastActivityTime
     */
    public Long getLastActivityTime() {
        return lastActivityTime;
    }

    /**
     * @param lastActivityTime the lastActivityTime to set
     */
    public void setLastActivityTime(Long lastActivityTime) {
        this.lastActivityTime = lastActivityTime;
    }

    /**
     * @return the doubtSolverPojoList
     */
    public List<DoubtSolverPojo> getDoubtSolverPojoList() {
        return doubtSolverPojoList;
    }

    /**
     * @param doubtSolverPojoList the doubtSolverPojoList to set
     */
    public void setDoubtSolverPojoList(List<DoubtSolverPojo> doubtSolverPojoList) {
        this.doubtSolverPojoList = doubtSolverPojoList;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public static class Constants extends AbstractTargetTopicLongIdEntity.Constants {
            public static final String MEMBERS_LIST = "membersList";
            public static final String LAST_ACTIVITY_TIME = "lastActivityTime";

            public static final String STUDENT_ID = "studentId";
            public static final String STUDENT_EMAIL_ID = "email";
            public static final String ACCEPTED_BY = "acceptedBy";
            public static final String DOUBT_STATE = "doubtState";
            public static final String CLOSED_TIME = "closedTime";
            public static final String USER_DOUBT_FEEDBACK = "userDoubtFeedback";
            public static final String DOUBT_PAID_STATE = "doubtPaidState";
            public static final String QC_OPTION = "qcOption";
            public static final String QC_FEEDBACK_STATE = "qcFeedbackStates";
            public static final String SPAMMED_BY = "spammedBy";
            public static final String DOUBT_STATE_CHANGES = "doubtStateChanges";
            public static final String DOUBT_SOLVER_POJO_LIST = "doubtSolverPojoList";
            public static final String PICURL = "picUrl";
            public static final String TEXT = "text";
    }    

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the doubtState
     */
    public DoubtState getDoubtState() {
        return doubtState;
    }

    /**
     * @param doubtState the doubtState to set
     */
    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    /**
     * @return the acceptedBy
     */
    public String getAcceptedBy() {
        return acceptedBy;
    }

    /**
     * @param acceptedBy the acceptedBy to set
     */
    public void setAcceptedBy(String acceptedBy) {
        this.acceptedBy = acceptedBy;
    }

    /**
     * @return the closedBy
     */
    public String getClosedBy() {
        return closedBy;
    }

    /**
     * @param closedBy the closedBy to set
     */
    public void setClosedBy(String closedBy) {
        this.closedBy = closedBy;
    }

    /**
     * @return the doubtSpamType
     */
    public DoubtSpamType getDoubtSpamType() {
        return doubtSpamType;
    }

    /**
     * @param doubtSpamType the doubtSpamType to set
     */
    public void setDoubtSpamType(DoubtSpamType doubtSpamType) {
        this.doubtSpamType = doubtSpamType;
    }

    /**
     * @return the spamReason
     */
    public String getSpamReason() {
        return spamReason;
    }

    /**
     * @param spamReason the spamReason to set
     */
    public void setSpamReason(String spamReason) {
        this.spamReason = spamReason;
    }

    /**
     * @return the spammedBy
     */
    public String getSpammedBy() {
        return spammedBy;
    }

    /**
     * @param spammedBy the spammedBy to set
     */
    public void setSpammedBy(String spammedBy) {
        this.spammedBy = spammedBy;
    }

    /**
     * @return the closedTime
     */
    public Long getClosedTime() {
        return closedTime;
    }

    /**
     * @param closedTime the closedTime to set
     */
    public void setClosedTime(Long closedTime) {
        this.closedTime = closedTime;
    }

    /**
     * @return the userDoubtFeedback
     */
    public UserDoubtFeedback getUserDoubtFeedback() {
        return userDoubtFeedback;
    }

    /**
     * @param userDoubtFeedback the userDoubtFeedback to set
     */
    public void setUserDoubtFeedback(UserDoubtFeedback userDoubtFeedback) {
        this.userDoubtFeedback = userDoubtFeedback;
    }

    /**
     * @return the userRemark
     */
    public String getUserRemark() {
        return userRemark;
    }

    /**
     * @param userRemark the userRemark to set
     */
    public void setUserRemark(String userRemark) {
        this.userRemark = userRemark;
    }

    /**
     * @return the reviewedBy
     */
    public String getReviewedBy() {
        return reviewedBy;
    }

    /**
     * @param reviewedBy the reviewedBy to set
     */
    public void setReviewedBy(String reviewedBy) {
        this.reviewedBy = reviewedBy;
    }

    /**
     * @return the reviewed
     */
    public Boolean getReviewed() {
        return reviewed;
    }

    /**
     * @param reviewed the reviewed to set
     */
    public void setReviewed(Boolean reviewed) {
        this.reviewed = reviewed;
    }

    /**
     * @return the reviewRemarks
     */
    public String getReviewRemarks() {
        return reviewRemarks;
    }

    /**
     * @param reviewRemarks the reviewRemarks to set
     */
    public void setReviewRemarks(String reviewRemarks) {
        this.reviewRemarks = reviewRemarks;
    }

    /**
     * @return the duration
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * @return the picFileName
     */
    public String getPicFileName() {
        return picFileName;
    }

    /**
     * @param picFileName the picFileName to set
     */
    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }

    /**
     * @return the unableToSolveReason
     */
    public String getUnableToSolveReason() {
        return unableToSolveReason;
    }

    /**
     * @param unableToSolveReason the unableToSolveReason to set
     */
    public void setUnableToSolveReason(String unableToSolveReason) {
        this.unableToSolveReason = unableToSolveReason;
    }

    public boolean isUnableToSolveT2() {
        return unableToSolveT2;
    }

    public void setUnableToSolveT2(boolean unableToSolveT2) {
        this.unableToSolveT2 = unableToSolveT2;
    }

    public DoubtPaidState getDoubtPaidState() {
        return doubtPaidState;
    }

    public void setDoubtPaidState(DoubtPaidState doubtPaidState) {
        this.doubtPaidState = doubtPaidState;
    }

	public QCOption getQcOption() {
		return qcOption;
	}

	public void setQcOption(QCOption qcOption) {
		this.qcOption = qcOption;
	}

	public List<QCFeedbackState> getQcFeedbackStates() {
		return qcFeedbackStates;
	}

	public void setQcFeedbackStates(List<QCFeedbackState> qcFeedbackStates) {
		this.qcFeedbackStates = qcFeedbackStates;
	}

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }
	
}
