/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.entities.mongo;

import com.vedantu.User.Role;
import com.vedantu.doubts.request.SendDoubtMessageRequest;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author parashar
 */
@Document(collection = "DoubtChatMessage")
public class DoubtChatMessage extends AbstractMongoStringIdEntity {
    
    @Indexed(background = true)
    private Long doubtId;
    @Indexed(background = true)    
    private String fromId;
    private String text;
    private String picUrl;
    private boolean read = false;
    private Long readTime;
    private String picFileName;
    private Role fromRole;
    

    public DoubtChatMessage() {
    }

    public DoubtChatMessage(SendDoubtMessageRequest req, String userId){
        this.doubtId = req.getDoubtId();
        this.fromId = userId;
        this.picUrl = req.getPicUrl();
        this.text = req.getText();
    }
    
    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    /**
     * @return the fromId
     */
    public String getFromId() {
        return fromId;
    }

    /**
     * @param fromId the fromId to set
     */
    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the read
     */
    public boolean isRead() {
        return read;
    }

    /**
     * @param read the read to set
     */
    public void setRead(boolean read) {
        this.read = read;
    }

    /**
     * @return the readTime
     */
    public Long getReadTime() {
        return readTime;
    }

    /**
     * @param readTime the readTime to set
     */
    public void setReadTime(Long readTime) {
        this.readTime = readTime;
    }
    

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
            public static final String DOUBT_ID = "doubtId";
            public static final String READ = "read";
            public static final String FROM_ID = "fromId";
            public static final String FROM_ROLE = "fromRole";
            public static final String TEXT = "text";
            public static final String PIC_URL = "picUrl";
            public static final String READ_TIME = "readTime";
    }        

    @Override
    public String toString() {
        return "DoubtChatMessage{" + "doubtId=" + doubtId + ", fromId=" + fromId + ", text=" + text + ", picUrl=" + picUrl + ", read=" + read + ", readTime=" + readTime + '}';
    }

    /**
     * @return the picFileName
     */
    public String getPicFileName() {
        return picFileName;
    }

    /**
     * @param picFileName the picFileName to set
     */
    public void setPicFileName(String picFileName) {
        this.picFileName = picFileName;
    }

    /**
     * @return the fromRole
     */
    public Role getFromRole() {
        return fromRole;
    }

    /**
     * @param fromRole the fromRole to set
     */
    public void setFromRole(Role fromRole) {
        this.fromRole = fromRole;
    }
    
}
