/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.doubts.enums.DoubtSpamType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author parashar
 */
public class MarkDoubtSpamReq extends AbstractFrontEndReq {
    private Long doubtId;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String reason;
    private DoubtSpamType doubtSpamType;
    
    @Override
    protected List<String> collectVerificationErrors(){
        List<String> errors = new ArrayList<>();
        if(doubtSpamType == null){
            errors.add("doubtSpamType");
        }
        if(doubtId == null){
            errors.add("doubtId");
        }        
        return errors;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the doubtSpamType
     */
    public DoubtSpamType getDoubtSpamType() {
        return doubtSpamType;
    }

    /**
     * @param doubtSpamType the doubtSpamType to set
     */
    public void setDoubtSpamType(DoubtSpamType doubtSpamType) {
        this.doubtSpamType = doubtSpamType;
    }

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }
    
}
