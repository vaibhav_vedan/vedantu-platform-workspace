/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.User.enums.ViewerAccess;
import com.vedantu.doubts.enums.DoubtFilter;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.enums.UserDoubtFeedback;
import com.vedantu.lms.cmds.enums.DoubtPaidState;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class GetDoubtDashboardReq extends AbstractFrontEndListReq{

    private DoubtState doubtState;
    private Long startTime;
    private Long endTime;
    private Long teacherId;
    private String grade;
    private String target;
    private String board;
    private Long boardId;
    private String studentId;
    private String studentMailId;
    private UserDoubtFeedback userDoubtFeedback;
    private DoubtPaidState doubtPaidState;
    private SortOrder sort;
    private Set<String> subjects;
    private List<ViewerAccess> viewerAccesses = Arrays.asList(new ViewerAccess[]{ViewerAccess.UNPAID});
    private DoubtFilter doubtFilter = null;

    /**
     * @return the doubtState
     */
    public DoubtState getDoubtState() {
        return doubtState;
    }

    /**
     * @param doubtState the doubtState to set
     */
    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the teacherId
     */
    public Long getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the grade
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @param grade the grade to set
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * @return the board
     */
    public String getBoard() {
        return board;
    }

    /**
     * @param board the board to set
     */
    public void setBoard(String board) {
        this.board = board;
    }

    /**
     * @return the boardId
     */
    public Long getBoardId() {
        return boardId;
    }

    /**
     * @param boardId the boardId to set
     */
    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    /**
     * @return the studentId
     */
    public String getStudentId() {
        return studentId;
    }

    /**
     * @param studentId the studentId to set
     */
    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    @Override
    public String toString() {
        return "GetDoubtDashboardReq{" + "doubtState=" + doubtState + 
        		", startTime=" + startTime + ", endTime=" + endTime + 
        		", teacherId=" + teacherId + ", grade=" + grade + 
        		", target=" + target + ", board=" + board + 
        		", boardId=" + boardId + ", studentId=" + studentId + 
        		", mailId=" + studentMailId + ", userDoubtFeedback=" + userDoubtFeedback + 
        		", doubtPaidState=" + doubtPaidState + ", doubtFilter=" + doubtFilter + 
        		", subjects=" + subjects + ", viewerAccesses=" + viewerAccesses + '}';
    }

    /**
     * @return the userDoubtFeedback
     */
    public UserDoubtFeedback getUserDoubtFeedback() {
        return userDoubtFeedback;
    }

    /**
     * @param userDoubtFeedback the userDoubtFeedback to set
     */
    public void setUserDoubtFeedback(UserDoubtFeedback userDoubtFeedback) {
        this.userDoubtFeedback = userDoubtFeedback;
    }

    public DoubtPaidState getDoubtPaidState() {
        return doubtPaidState;
    }

    public void setDoubtPaidState(DoubtPaidState doubtPaidState) {
        this.doubtPaidState = doubtPaidState;
    }

    public SortOrder getSort() {
        return sort;
    }

    public void setSort(SortOrder sort) {
        this.sort = sort;
    }

    public Set<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    public String getStudentMailId() {
        return studentMailId;
    }

    public void setStudentMailId(String studentMailId) {
        this.studentMailId = studentMailId;
    }
	
	public DoubtFilter getDoubtFilter() {
		return doubtFilter;
	}

	public void setDoubtFilter(DoubtFilter doubtFilter) {
		this.doubtFilter = doubtFilter;
	}

	public List<ViewerAccess> getViewerAccesses() {
		return viewerAccesses;
	}

	public void setViewerAccesses(List<ViewerAccess> viewerAccesses) {
		this.viewerAccesses = viewerAccesses;
	}
	
}
