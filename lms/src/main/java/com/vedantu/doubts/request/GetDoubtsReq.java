package com.vedantu.doubts.request;

import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.doubts.enums.DoubtFilter;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.lms.cmds.enums.DoubtPaidState;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SortOrder;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class GetDoubtsReq extends AbstractFrontEndListReq {

    TeacherPoolType teacherPoolType;
    DoubtPaidState doubtPaidState;

    private SortOrder sort;
    private DoubtState doubtState;
    private Long startTime;
    private Long endTime;
    private Set<String> subjects;
    private DoubtFilter doubtFilter = null;

    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    public DoubtPaidState getDoubtPaidState() {
        return doubtPaidState;
    }

    public void setDoubtPaidState(DoubtPaidState doubtPaidState) {
        this.doubtPaidState = doubtPaidState;
    }

    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }

    public SortOrder getSort() {
        return sort;
    }

    public void setSort(SortOrder sort) {
        this.sort = sort;
    }

    public DoubtState getDoubtState() {
        return doubtState;
    }

    public void setDoubtState(DoubtState doubtState) {
        this.doubtState = doubtState;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Set<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();

        if(teacherPoolType == null){
            errors.add("teacherPoolType");
        }

        return new ArrayList<>();
    }

	public DoubtFilter getDoubtFilter() {
		return doubtFilter;
	}

}
