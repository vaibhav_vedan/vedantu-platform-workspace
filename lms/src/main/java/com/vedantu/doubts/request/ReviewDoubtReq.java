/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.doubts.enums.QCFeedbackState;
import com.vedantu.doubts.enums.QCOption;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Size;

/**
 *
 * @author parashar
 */
public class ReviewDoubtReq extends AbstractFrontEndReq {
    private Long doubtId;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)    
    private String remark;

    private QCOption qcOption;
    private List<QCFeedbackState> qcFeedbackStates;
    

	/**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }
    
    @Override
    protected List<String> collectVerificationErrors(){
        List<String> errors = new ArrayList<>();
        if(StringUtils.isEmpty(remark)){
            errors.add("remark");
        }
        if(doubtId == null){
            errors.add("doubtId");
        }
        if(qcOption == null) {
        	errors.add("qcOption");
        }
        if(qcFeedbackStates == null) {
        	errors.add("qcFeedbackStates");
        }
        return errors;
    }    

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    public QCOption getQcOption() {
        return qcOption;
    }

    public void setQcOption(QCOption qcOption) {
        this.qcOption = qcOption;
    }

    public List<QCFeedbackState> getQcFeedbackStates() {
        return qcFeedbackStates;
    }

    public void setQcFeedbackStates(List<QCFeedbackState> qcFeedbackStates) {
        this.qcFeedbackStates = qcFeedbackStates;
    }

    @Override
    public String toString() {
        return "ReviewDoubtReq{" +
                "doubtId=" + doubtId +
                ", remark='" + remark + '\'' +
                ", qcOption=" + qcOption +
                ", qcFeedbackStates=" + qcFeedbackStates +
                '}';
    }
}
