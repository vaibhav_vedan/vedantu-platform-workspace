/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class DoubtTaggingReq extends AbstractFrontEndReq{
    
    private Long doubtId;
    private Set<String> targetGrades;
    private Set<String> topics;

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    /**
     * @return the targetGrades
     */
    public Set<String> getTargetGrades() {
        return targetGrades;
    }

    /**
     * @param targetGrades the targetGrades to set
     */
    public void setTargetGrades(Set<String> targetGrades) {
        this.targetGrades = targetGrades;
    }

    /**
     * @return the topics
     */
    public Set<String> getTopics() {
        return topics;
    }

    /**
     * @param topics the topics to set
     */
    public void setTopics(Set<String> topics) {
        this.topics = topics;
    }
    
}
