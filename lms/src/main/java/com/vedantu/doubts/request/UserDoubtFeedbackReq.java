/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author parashar
 */
public class UserDoubtFeedbackReq extends AbstractFrontEndReq {
    
    private Long doubtId;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)    
    private String remark;
    private boolean solved = true;

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the isSolved
     */
    public boolean isSolved() {
        return solved;
    }

    /**
     * @param solved the isSolved to set
     */
    public void setSolved(boolean solved) {
        this.solved = solved;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        
        List<String> errors = super.collectVerificationErrors();
        
        if(doubtId == null){
            errors.add("doubtId");
        }
        
        if((!solved) && StringUtils.isEmpty(remark)){
            errors.add("remark");
        }
        
        return errors;
    }    
    
}
