/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.doubts.enums.TeacherAvailabilityFilter;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class GetTeacherDashboardReq extends AbstractFrontEndListReq{
    
    private TeacherAvailabilityFilter filter;
    private TeacherPoolType teacherPoolType;
    

    /**
     * @return the t2Filter
     */
    public TeacherAvailabilityFilter getFilter() {
        return filter;
    }

    /**
     * @param filter the t2Filter to set
     */
    public void setFilter(TeacherAvailabilityFilter filter) {
        this.filter = filter;
    }

    /**
     * @return the teacherPoolType
     */
    public TeacherPoolType getTeacherPoolType() {
        return teacherPoolType;
    }

    /**
     * @param teacherPoolType the teacherPoolType to set
     */
    public void setTeacherPoolType(TeacherPoolType teacherPoolType) {
        this.teacherPoolType = teacherPoolType;
    }
    
}
