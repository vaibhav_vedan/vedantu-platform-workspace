/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author parashar
 */
public class SendDoubtMessageRequest extends AbstractFrontEndReq {
    
    private Long doubtId;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String text;
    private String picUrl;

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the picUrl
     */
    public String getPicUrl() {
        return picUrl;
    }

    /**
     * @param picUrl the picUrl to set
     */
    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        if(StringUtils.isEmpty(text) && StringUtils.isEmpty(picUrl)){
            errors.add("Empty Error");
        }

        
        if(doubtId == null){
            errors.add("doubtId");
        }
        
        return errors;
    }      
    
}
