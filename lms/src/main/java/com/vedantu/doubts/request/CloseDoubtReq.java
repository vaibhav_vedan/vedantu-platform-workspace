/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.ArrayList;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author parashar
 */
public class CloseDoubtReq extends AbstractFrontEndReq{
    
    private Long doubtId;
    private boolean unableToSolve = false;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_COMMENT_TYPE)
    private String unableToSolveReason;

    /**
     * @return the doubtId
     */
    public Long getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(Long doubtId) {
        this.doubtId = doubtId;
    }

    /**
     * @return the unableToSolve
     */
    public boolean isUnableToSolve() {
        return unableToSolve;
    }

    /**
     * @param unableToSolve the unableToSolve to set
     */
    public void setUnableToSolve(boolean unableToSolve) {
        this.unableToSolve = unableToSolve;
    }

    /**
     * @return the unableToSolveReason
     */
    public String getUnableToSolveReason() {
        return unableToSolveReason;
    }

    /**
     * @param unableToSolveReason the unableToSolveReason to set
     */
    public void setUnableToSolveReason(String unableToSolveReason) {
        this.unableToSolveReason = unableToSolveReason;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        
        if(doubtId == null){
            errors.add("doubtId");
        }
        
        if(unableToSolve && StringUtils.isEmpty(unableToSolveReason)){
            errors.add("unableToSolveReason");
        }
        
        return new ArrayList<>();
    }    
    
}
