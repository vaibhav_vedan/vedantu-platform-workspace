package com.vedantu.doubts.request;

import com.vedantu.util.ArrayUtils;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class AddPaidViewerReq extends AbstractFrontEndReq {

	private List<String> paidViewerIds;
	
	@Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        
        if(paidViewerIds == null || ArrayUtils.isEmpty(paidViewerIds)){
            errors.add("paidViewerIds");
        }
        
        return errors;
    }

	public List<String> getPaidViewerIds() {
		return paidViewerIds;
	}

	public void setPaidViewerIds(List<String> paidViewerIds) {
		this.paidViewerIds = paidViewerIds;
	}
}
