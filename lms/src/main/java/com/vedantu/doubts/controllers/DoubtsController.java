/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.controllers;

import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.User.enums.TeacherPoolType;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.doubts.entities.mongo.Doubt;
import com.vedantu.doubts.enums.DoubtFilter;
import com.vedantu.doubts.enums.DoubtState;
import com.vedantu.doubts.managers.DoubtsManager;
import com.vedantu.doubts.pojo.*;
import com.vedantu.doubts.request.*;
import com.vedantu.exception.*;
import com.vedantu.homefeed.entity.WebinarResponse;
import com.vedantu.util.*;
import com.vedantu.util.enums.DoubtFeedType;
import com.vedantu.util.pojo.DoubtFeedPojo;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author parashar
 */

@CrossOrigin
@RestController
@RequestMapping("doubts/")
public class DoubtsController {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    public DoubtsManager doubtsManager;

    @Autowired
    private DozerBeanMapper mapper;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DoubtsController.class);

    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Doubt createDoubt(@RequestBody CreateDoubtsReq createDoubtsReq) throws VException, ForbiddenException, Exception, Exception {
        createDoubtsReq.verify();
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        if (!Role.STUDENT.equals(role)) {
            throw new BadRequestException(ErrorCode.NON_STUDENT_NOT_ALLOWED_FOR_DOUBT, "");
        }

        logger.info("Request : " + createDoubtsReq.toString());
        //doubtsManager;

        return doubtsManager.postDoubt(createDoubtsReq, userId);
    }

    @RequestMapping(value = "/getDoubts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DoubtPojo> getDoubts(GetDoubtsReq req) throws VException, BadRequestException {
        logger.info("Request : " + req);
        //doubtsManager;

        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        if (!(Role.TEACHER.equals(role))) {
            throw new ForbiddenException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }

//        List<DoubtPojo> doubtPojos = new ArrayList<>();
//        DoubtPojo doubtPojo = new DoubtPojo();
//        doubtPojo.setDoubtId(1231231l);
//        if (TeacherPoolType.T1_POOL.equals(teacherPoolType)) {
//            doubtPojo.setDoubtState(DoubtState.DOUBT_POSTED);
//        } else {
//            doubtPojo.setDoubtState(DoubtState.DOUBT_T2_POOL);
//        }
//        doubtPojo.setPicUrl("http://images4.fanpop.com/image/photos/14700000/Beautifull-cat-cats-14749874-1600-1200.jpg");
//        doubtPojo.setText("What should be the name of this cat?");
//        doubtPojos.add(doubtPojo);

        // return doubtPojos;
        return doubtsManager.getDoubtsForSupply(userId.toString(), req);
    }


    @RequestMapping(value = "/getDoubtsForDashboard", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DoubtBasicInfo> createDoubt(@ModelAttribute GetDoubtDashboardReq req) throws VException, BadRequestException {
        logger.info("Request : " + req.toString());
        //doubtsManager;       
        // return doubts;
        return doubtsManager.getDoubtsForDashboard(req);
    }

    @RequestMapping(value = "/acceptDoubt", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse acceptDoubt(@RequestParam(name = "doubtId", required = true) Long doubtId) throws VException, BadRequestException {

        logger.info("Request : " + doubtId);
        //doubtsManager;
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        if (!(Role.TEACHER.equals(role))) {
            throw new BadRequestException(ErrorCode.ONLY_TEACHER_ALLOWED_TO_ACCEPT_DOUBT, "");
        }

        return doubtsManager.acceptDoubt(doubtId, userId.toString());

        // return new PlatformBasicResponse(true, null, null);
    }

    @RequestMapping(value = "/closeDoubt", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse closeDoubt(CloseDoubtReq req) throws VException, BadRequestException {
        req.verify();
        logger.info("Request : " + req.getDoubtId());
        //doubtsManager;
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        /*if(!(Role.TEACHER.equals(role) || Role.DEX.equals(role))){
            throw new BadRequestException(ErrorCode.ONLY_TEACHER_ALLOWED_TO_ACCEPT_DOUBT, "");
        }*/
        if (req.isUnableToSolve()) {
            return doubtsManager.unableToSolve(req.getDoubtId(), req.getUnableToSolveReason(), userId.toString());
        }

        return doubtsManager.closeDoubt(req.getDoubtId(), userId.toString());
        // return new PlatformBasicResponse(true, null, null);
    }

    @RequestMapping(value = "/markDoubtAsSpam", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markDoubtSpam(@RequestBody MarkDoubtSpamReq req) throws VException, BadRequestException {
        req.verify();
        logger.info("Request : " + req.toString());
        //doubtsManager;
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        /*if(!(Role.TEACHER.equals(role) || Role.DEX.equals(role))){
            throw new BadRequestException(ErrorCode.ONLY_TEACHER_ALLOWED_TO_ACCEPT_DOUBT, "");
        }*/
        return doubtsManager.markDoubtAsSpam(req, userId.toString());
        // return new PlatformBasicResponse(true, null, null);
    }

    @RequestMapping(value = "/reviewDoubt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse reviewDoubt(@RequestBody ReviewDoubtReq req) throws VException, BadRequestException {
        req.verify();
        logger.info("Request : " + req.toString());
        //doubtsManager;
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        /*if(!(Role.ADMIN.equals(role))){
            throw new BadRequestException(ErrorCode.ONLY_ADMIN_ALLOWED_TO_REVIEW, "");
        }*/
        return doubtsManager.reviewDoubt(req, userId.toString());

        // return new PlatformBasicResponse(true, null, null);
    }

    @RequestMapping(value = "/userFeedback", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse userFeedback(@RequestBody UserDoubtFeedbackReq req) throws VException, BadRequestException {
        req.verify();
        logger.info("Request : " + req.toString());
        //doubtsManager;
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        /*if(!(Role.STUDENT.equals(role))){
            throw new BadRequestException(ErrorCode.NON_STUDENT_NOT_ALLOWED_FOR_DOUBT, "");
        }*/
        return doubtsManager.submitUserFeedback(req, userId.toString());
        // return new PlatformBasicResponse(true, null, null);
    }

    @RequestMapping(value = "/getDoubtById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Doubt getDoubtById(@RequestParam(name = "doubtId") Long doubtId) throws NotFoundException {
        return doubtsManager.getDoubtById(doubtId);
    }

    @RequestMapping(value = "/getOpenChats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Doubt> getOpenChats(@RequestParam(name = "userId", required = true) String userId) {
        return doubtsManager.getOpenDoubts(userId);
    }

    @RequestMapping(value = "/getDoubtStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SolverDoubtStats getDoubtStatsForSolver(@RequestParam(name="filterDoubt", required = false) DoubtFilter filterDoubt) throws ConflictException, InternalServerErrorException, BadRequestException {
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        if (!(Role.TEACHER.equals(role))) {
            throw new ConflictException(ErrorCode.NOT_ALLOWED_TO_VIEW_OR_ACCEPT_DOUBT, "");
        }

        return doubtsManager.getDoubtSolverStats(userId.toString(), filterDoubt);

    }

    @RequestMapping(value = "/getDoubtStatsByEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SolverDoubtStats getDoubtStatsForSolverByEmail(@RequestParam(name="filterDoubt", required = false) DoubtFilter filterDoubt, @RequestParam(name="email", required = false) String email) throws ConflictException, InternalServerErrorException, BadRequestException {
        return doubtsManager.getDoubtSolverStatsByEmail(email, filterDoubt);
    }

    @RequestMapping(value = "/getLiveDashboardStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DoubtSolverDashboardStats getDoubtsTeacherStats(GetTeacherDashboardReq req) {
        DoubtSolverDashboardStats doubtSolverDashboardStats = new DoubtSolverDashboardStats();
        doubtSolverDashboardStats.setT1Count(1);
        doubtSolverDashboardStats.setT2Count(0);
        DexLivePojo dexLivePojo = new DexLivePojo();
        dexLivePojo.setAvailable(true);
        dexLivePojo.setBoards(new HashSet<>(Arrays.asList("CBSE")));
        dexLivePojo.setEmail("test1@vedantu.com");
        dexLivePojo.setInChat(false);
        dexLivePojo.setFullName("Test test");
        dexLivePojo.setGrades(new HashSet<>(Arrays.asList("10")));
        dexLivePojo.setTeacherPoolType(TeacherPoolType.T1_POOL);
        dexLivePojo.setTopics(new HashSet<>(Arrays.asList("Comet", "Physics")));
        doubtSolverDashboardStats.setDexInfos(Arrays.asList(dexLivePojo));
        DoubtSolverDashboardStats newDashboardStats = doubtsManager.getDoubtSolverDashboardStats(req);
        logger.info("dashboardStats as in controllers : " + newDashboardStats);
        return newDashboardStats;
    }

    @RequestMapping(value = "/tagDoubt", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse tagDoubts(@RequestBody DoubtTaggingReq req) throws NotFoundException, ConflictException, Exception {
        Long userId = sessionUtils.getCallingUserId();
        return doubtsManager.tagDoubt(req, userId.toString());
    }


    @RequestMapping(value = "/getDoubtChatPojoById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public DoubtChatPojo getDoubtChatPojoById(@RequestParam(name = "doubtId") Long doubtId) {
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();

        return doubtsManager.getDoubtChatPojoById(doubtId, role);
    }

    @RequestMapping(value = "/studentconfig", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String studentconfig() {
        //{"":8,"":"10:00","":"23:59"}
        JSONObject j = new JSONObject();
        j.put("doubtLimitPerDay", 8);
        j.put("availabilityStart", "10:00");
        j.put("availabilityEnd", "23:59");
        return j.toString();
    }

    @RequestMapping(value = "/dexconfig", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String dexconfig() {
        JSONObject j = new JSONObject();
        j.put("chatT1Timeout", "23:59");
        return j.toString();
    }

    @RequestMapping(value = "/getBaseTreeNodesWithoutChildren", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BaseTopicTree> getBaseTreeNodesBySubject() throws VException, inti.ws.spring.exception.client.BadRequestException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT), Boolean.TRUE);
        return doubtsManager.getBaseTreeNodesByLevelWithoutChildren(sessionUtils.getCallingUserId());
    }

    @RequestMapping(value = "/getDoubtsFeed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DoubtFeedPojo> getDoubtFeeds(@RequestParam(name="board") String board, @RequestParam(name = "grade")String grade) throws ForbiddenException {
        /*JSONObject j = new JSONObject();
        j.put("chatT1Timeout", "23:59");*/
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        return new ArrayList<>();
//        return Arrays.asList(new DoubtFeedPojo("https://www.google.co.in/imgres?imgurl=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1518791841217-8f162f1e1131%3Fixlib%3Drb-1.2.1%26ixid%3DeyJhcHBfaWQiOjEyMDd9%26w%3D1000%26q%3D80&imgrefurl=https%3A%2F%2Funsplash.com%2Fsearch%2Fphotos%2Fcats&docid=CKh80I0v0MIolM&tbnid=bnL5sgrZ6Gt0cM%3A&vet=10ahUKEwiF9Orb19vfAhXFpI8KHdGtBkMQMwg9KAAwAA..i&w=1000&h=667&bih=821&biw=1440&q=cat%20pictures&ved=0ahUKEwiF9Orb19vfAhXFpI8KHdGtBkMQMwg9KAAwAA&iact=mrc&uact=8", DoubtFeedType.CLASS, "CAT Video", System.currentTimeMillis() - (long)10 * DateTimeUtils.MILLIS_PER_MINUTE),new DoubtFeedPojo(null, DoubtFeedType.DOUBT, "Physics Doubt #123", System.currentTimeMillis() - (long)DateTimeUtils.MILLIS_PER_HOUR));
//        return doubtsManager.getDoubtsFeed(sessionData.getUserId(), board, grade);
    }

    @RequestMapping(value = "/getHomeFeed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DoubtFeedPojo> getHomeFeed(@RequestParam(name="board") String board, @RequestParam(name = "grade")String grade,
                                           @RequestParam(name = "appVersion") String appVersion) throws ForbiddenException {
        /*JSONObject j = new JSONObject();
        j.put("chatT1Timeout", "23:59");*/
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        return new ArrayList<>();
//        return Arrays.asList(new DoubtFeedPojo("https://www.google.co.in/imgres?imgurl=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1518791841217-8f162f1e1131%3Fixlib%3Drb-1.2.1%26ixid%3DeyJhcHBfaWQiOjEyMDd9%26w%3D1000%26q%3D80&imgrefurl=https%3A%2F%2Funsplash.com%2Fsearch%2Fphotos%2Fcats&docid=CKh80I0v0MIolM&tbnid=bnL5sgrZ6Gt0cM%3A&vet=10ahUKEwiF9Orb19vfAhXFpI8KHdGtBkMQMwg9KAAwAA..i&w=1000&h=667&bih=821&biw=1440&q=cat%20pictures&ved=0ahUKEwiF9Orb19vfAhXFpI8KHdGtBkMQMwg9KAAwAA&iact=mrc&uact=8", DoubtFeedType.CLASS, "CAT Video", System.currentTimeMillis() - (long)10 * DateTimeUtils.MILLIS_PER_MINUTE),new DoubtFeedPojo(null, DoubtFeedType.DOUBT, "Physics Doubt #123", System.currentTimeMillis() - (long)DateTimeUtils.MILLIS_PER_HOUR));
//        return doubtsManager.getDoubtsFeed(sessionData.getUserId(), board, grade);
    }

    @RequestMapping(value = "/getTesDoubtsFeed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DoubtFeedPojo> getTestDoubtFeeds(@RequestParam(name="board") String board, @RequestParam(name = "grade")String grade) {
        /*JSONObject j = new JSONObject();
        j.put("chatT1Timeout", "23:59");*/
        Long userId = sessionUtils.getCallingUserId();
        return doubtsManager.getDoubtsFeed(userId, board, grade);
        //return Arrays.asList(new DoubtFeedPojo("https://www.google.co.in/imgres?imgurl=https%3A%2F%2Fimages.unsplash.com%2Fphoto-1518791841217-8f162f1e1131%3Fixlib%3Drb-1.2.1%26ixid%3DeyJhcHBfaWQiOjEyMDd9%26w%3D1000%26q%3D80&imgrefurl=https%3A%2F%2Funsplash.com%2Fsearch%2Fphotos%2Fcats&docid=CKh80I0v0MIolM&tbnid=bnL5sgrZ6Gt0cM%3A&vet=10ahUKEwiF9Orb19vfAhXFpI8KHdGtBkMQMwg9KAAwAA..i&w=1000&h=667&bih=821&biw=1440&q=cat%20pictures&ved=0ahUKEwiF9Orb19vfAhXFpI8KHdGtBkMQMwg9KAAwAA&iact=mrc&uact=8", DoubtFeedType.CLASS, "CAT Video", System.currentTimeMillis() - (long)10 * DateTimeUtils.MILLIS_PER_MINUTE),new DoubtFeedPojo(null, DoubtFeedType.DOUBT, "Physics Doubt #123", System.currentTimeMillis() - (long)DateTimeUtils.MILLIS_PER_HOUR));
    }

    @RequestMapping(value = "/getBaseTreeNodeForSubject", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BaseTopicTree getBaseTreeNodesBySubject(@RequestParam(name="subject", required = true) String subject) throws VException, inti.ws.spring.exception.client.BadRequestException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.TEACHER), Boolean.TRUE);
        return doubtsManager.getBaseTreeForSubject(subject);
    }
    
    @RequestMapping(value = "/appendPaidViewers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String appendPaidViewers(@RequestBody AddPaidViewerReq req) throws VException {
        Role role = sessionUtils.getCallingUserRole();
        
        if (!(Role.ADMIN.equals(role))) {
            throw new ConflictException(ErrorCode.FORBIDDEN_ERROR, "");
        }
        
    	return doubtsManager.appendPaidViewers(req);
    }

    @RequestMapping(value = "/fetchUserIdByContactNumber/{phoneNumber}", method = RequestMethod.GET)
    @ResponseBody
    public UserBasicInfo fetchUserIdByContactNumber(@PathVariable("phoneNumber") String phoneNumber) throws VException {
        logger.info("method = fetchUserIdByContactNumber, phoneNumber = {}", phoneNumber);
        return doubtsManager.fetchUserByContact(phoneNumber);
    }

}
