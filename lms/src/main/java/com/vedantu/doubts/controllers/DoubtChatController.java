/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.doubts.controllers;

import com.vedantu.User.Role;
import com.vedantu.doubts.enums.StudentDoubtStatus;
import com.vedantu.doubts.managers.DoubtChatManager;
import com.vedantu.doubts.pojo.*;
import com.vedantu.doubts.request.AddPaidViewerReq;
import com.vedantu.doubts.request.DoubtHelpReq;
import com.vedantu.exception.*;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import java.io.UnsupportedEncodingException;
import java.util.*;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author parashar
 */
@CrossOrigin
@RestController
@RequestMapping("doubtchat/")
public class DoubtChatController {
    
    @Autowired
    public LogFactory logFactory;

    @Autowired
    HttpSessionUtils sessionUtils;  
    
    @Autowired
    public DoubtChatManager doubtChatManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DoubtChatController.class);  

    @RequestMapping(value = "/getDoubtChatMessages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetDoubtChatMessages getDoubtChatMessages(@RequestParam(name="doubtId", required=true) Long doubtId,
                                                            @RequestParam(name="start", required=true) int start,
                                                            @RequestParam(name="size", required = true) int size) throws VException {
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        
        if(!(Role.STUDENT.equals(role) || Role.TEACHER.equals(role) || Role.ADMIN.equals(role))){
            throw new BadRequestException(ErrorCode.ONLY_PARTICIPANT_ALLOWED_FOR_CHAT, "");
        }
        
        logger.info("Request : " + doubtId);
        //doubtsManager;
        
        List<DoubtChatMessagePojo> doubtChatMessagePojos = new ArrayList<>();
        doubtChatMessagePojos.add(new DoubtChatMessagePojo("83498199", Role.STUDENT, true, "What is the name of this cat", "http://images4.fanpop.com/image/photos/14700000/Beautifull-cat-cats-14749874-1600-1200.jpg", System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_HOUR, System.currentTimeMillis() - (DateTimeUtils.MILLIS_PER_HOUR - 5*DateTimeUtils.MILLIS_PER_MINUTE)));
        doubtChatMessagePojos.add(new DoubtChatMessagePojo("83498199", Role.STUDENT, true, "I don't think it can be fluffy", "http://images4.fanpop.com/image/photos/14700000/Beautifull-cat-cats-14749874-1600-1200.jpg", System.currentTimeMillis() - (DateTimeUtils.MILLIS_PER_HOUR - 20*DateTimeUtils.MILLIS_PER_MINUTE), System.currentTimeMillis() - (DateTimeUtils.MILLIS_PER_HOUR - 25*DateTimeUtils.MILLIS_PER_MINUTE)));
       
        return doubtChatManager.getDoubtChatMessages(doubtId, start, size, userId.toString(), role);
    }
    
    @RequestMapping(value = "/getDoubtChats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DoubtChatPojo> getDoubtChats(@RequestParam(name="start", required=true) int start,
                                             @RequestParam(name="size", required = true) int size,
                                             @RequestParam(name = "studentDoubtStatus", required = false) StudentDoubtStatus studentDoubtStats) throws VException {
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        
        if(!(Role.STUDENT.equals(role) || Role.TEACHER.equals(role))){
            throw new BadRequestException(ErrorCode.ONLY_PARTICIPANT_ALLOWED_FOR_CHAT, "");
        }
               
        return doubtChatManager.getDoubtChatPojos(userId.toString(), start, size, role, studentDoubtStats);
    }
    
    @RequestMapping(value = "/marksMessagesAsRead", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void markMessagesAsRead(@RequestParam(name="messageIds", required=true) List<String> messageIds, @RequestParam(name="doubtId", required=true) Long doubtId) throws VException {
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        
        if(!(Role.STUDENT.equals(role) || Role.TEACHER.equals(role))){
            throw new BadRequestException(ErrorCode.ONLY_PARTICIPANT_ALLOWED_FOR_CHAT, "");
        }
         
               
        //return doubtChatManager.getDoubtChatPojos(userId.toString(), start, size, role);
        doubtChatManager.markAsRead(messageIds, doubtId, userId.toString(), role);
    }

    @RequestMapping(value = "/getUnreadMessages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public UnreadMessagesCount getUnreadCount() throws BadRequestException, VException{
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        
        if(!Role.STUDENT.equals(role)){
            throw new BadRequestException(ErrorCode.NON_STUDENT_NOT_ALLOWED_FOR_DOUBT, "");
        }
        
        return doubtChatManager.getUnreadCount(userId.toString());
        
    }
    
    @RequestMapping(value="/supportMessage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse postSupportMessage(@RequestBody DoubtHelpReq req) throws UnsupportedEncodingException, VException{
        Long userId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();        
        if(!(Role.STUDENT.equals(role) || Role.TEACHER.equals(role))){
            throw new BadRequestException(ErrorCode.NOT_ALLOWED_TO_SEND_MESSAGE, "");
        }        
        return doubtChatManager.doubtSupportMessage(req, userId);
    }
    
    @RequestMapping(value = "/appendPaidViewers", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String appendPaidViewers(@RequestBody AddPaidViewerReq req) throws VException {
    	Role role = sessionUtils.getCallingUserRole();
        
        if (!(Role.ADMIN.equals(role))) {
            throw new ConflictException(ErrorCode.FORBIDDEN_ERROR, "");
        }
        
        return doubtChatManager.appendPaidViewers(req);
    }
}

