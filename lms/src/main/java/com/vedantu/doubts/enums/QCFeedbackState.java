package com.vedantu.doubts.enums;

public enum QCFeedbackState {

	CORRECT_SOLUTION,
	CREDITED_TO_T1, 
	CREDITED_TO_T2, 
	INCORRECT_SOLUTION, 
	ABUSIVE_TEACHER, 
	CHAT_QUALITY,
	GENERAL_ENQUIRY,
	SOLUTION_BY_EMAIL,
	INCORRECT_QUESTION,
	OTHERS,
	ABUSIVE_STUDENT,
	GENUINE_DOUBT, 
	GENUINE_SPAM;

}
