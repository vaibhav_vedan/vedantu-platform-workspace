package com.vedantu.doubts.enums;

public enum DoubtFilter {
	LAST_MONTH,
	LAST_WEEK,
	LAST_DAY,
	TODAY
}
