package com.vedantu.vgroups.dao;

import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vgroups.entity.VMessage;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author Ausaf
 */


@Service
public class VMessageDAO extends AbstractMongoDAO
{
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(VMessageDAO.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public VMessage findById(String id, List<String> includeFields)
    {
        logger.info("Id : {}", id);
        if (StringUtils.isNotEmpty(id))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VMessage.Constants._ID).is(id));

            if (includeFields != null && !includeFields.isEmpty())
                setProjection(query, includeFields);

            return findOne(query, VMessage.class);
        }
        return null;
    }

    public void deleteById(String id)
    {
        logger.info("method : deleteById, id : {}", id);
        if (StringUtils.isNotEmpty(id))
            deleteEntityById(id, VMessage.class);
    }

    public void save(VMessage vMessage) {
        logger.info("VMessage : {} ", vMessage);
        if (vMessage != null) {
            saveEntity(vMessage);
        }
    }

    public void setProjection(Query query, List<String> includeFields)
    {
        if (!includeFields.isEmpty())
        {
            for (String field : includeFields)
            {
                query.fields().include(field);
            }
        }
    }


}
