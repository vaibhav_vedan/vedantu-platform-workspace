package com.vedantu.vgroups.dao;

import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.vgroups.entity.VGroup;
import com.vedantu.vgroups.response.VGroupFeedResponse;
import com.vedantu.vgroups.response.VGroupResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Ausaf
 */


@Service
public class VGroupDAO extends AbstractMongoDAO
{
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(VGroup.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public VGroup findById(String id, List<String> includeFields)
    {
        logger.info("Id : {}", id);
        if (StringUtils.isNotEmpty(id))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VGroup.Constants._ID).is(id));

            if (includeFields != null && !includeFields.isEmpty())
                setProjection(query, includeFields);

            return findOne(query, VGroup.class);
        }
        return null;
    }

    public List<VGroup> findByGroupNames(List<String> names)
    {
        logger.info("Group names : {}", names);
        if (names != null && !names.isEmpty())
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VGroup.Constants.GROUP_NAME).in(names));
            query.fields().include(VGroup.Constants.ID);
            query.fields().include(VGroup.Constants.GROUP_NAME);
            query.fields().include(VGroup.Constants.ADMIN_IDS);
            query.fields().include(VGroup.Constants.STUDENT_IDS);
            query.fields().include(VGroup.Constants.BLOCKED_IDS);

            return runQuery(query, VGroup.class);
        }
        return null;
    }

    public VGroup findByGroupName(String groupName)
    {
        logger.info("group name : {}", groupName);
        if (StringUtils.isNotEmpty(groupName))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VGroup.Constants.GROUP_NAME).is(groupName));
            return findOne(query, VGroup.class);
        }
        return null;
    }

    public List<VGroup> findByGrade(String grade)
    {
        logger.info("grade : {}", grade);
        if (StringUtils.isNotEmpty(grade))
        {
            Query query = new Query();
            query.addCriteria(Criteria.where(VGroup.Constants.GRADES).all(grade));
            query.fields().include(VGroup.Constants.ID);
            query.with(Sort.by(Sort.Direction.DESC, VGroup.Constants.TOTAL_MEMBERS));
            query.limit(5);
            return runQuery(query, VGroup.class);
        }
        return null;
    }

    public void deleteById(String id)
    {
        logger.info("method : deleteById, id : {}", id);
        if (StringUtils.isNotEmpty(id))
            deleteEntityById(id, VGroup.class);
    }

    public void save(VGroup vGroup) {
        logger.info("VGroup : {} ", vGroup);
        if (vGroup != null) {
            saveEntity(vGroup);
        }
    }

    public List<VGroup> fetchVGroups(String groupId, Integer skip, Integer limit)
    {
        Query query = new Query();


        if (skip != null)
            query.skip(skip);

        if (limit != null)
            query.limit(limit);

        if (StringUtils.isNotEmpty(groupId))
            query.addCriteria(Criteria.where(VGroup.Constants.ID).is(groupId));

        query.with(Sort.by(Sort.Direction.DESC, VGroup.Constants.CREATION_TIME));

        logger.info("Query for fetching VGroup : {}", query);

        List<VGroup> vGroups = runQuery(query, VGroup.class);

        logger.info("VGroup fetched : {}", vGroups);

        return vGroups;
    }

    public VGroupFeedResponse fetchCurrentVGroups(String userId, String grade)
    {
        VGroupFeedResponse vGroupFeedResponse = new VGroupFeedResponse();

        if (StringUtils.isEmpty(userId))
            return null;

        Query joined = new Query();

        if (StringUtils.isNotEmpty(grade))
            joined.addCriteria(Criteria.where(VGroup.Constants.STUDENT_IDS).all(userId));
        else
        {
            Criteria VteacherOrVadmin = new Criteria();
            VteacherOrVadmin.orOperator(Criteria.where(VGroup.Constants.TEACHER_IDS).all(userId), Criteria.where(VGroup.Constants.ADMIN_IDS).all(userId));
            joined.addCriteria(VteacherOrVadmin);
        }

        List<String> include = new ArrayList<>();

        include.add(VGroup.Constants.ID);
        include.add(VGroup.Constants.GROUP_NAME);
        include.add(VGroup.Constants.GROUP_THUMBNAIL);
        include.add(VGroup.Constants.TOTAL_MEMBERS);
        include.add(VGroup.Constants.BLOCKED_IDS);
        include.add(VGroup.Constants.CHANNEL_ID);

        setProjection(joined, include);

        logger.info("Query : {}", joined);

        List<VGroup> vGroupsJoined = runQuery(joined, VGroup.class);

        List<VGroupResponse> joinedGroups = VGroupResponseConverter(vGroupsJoined, userId);

//        if (StringUtils.isNotEmpty(grade))
//        {
//            Query recommended = new Query();
//            recommended.addCriteria(Criteria.where(VGroup.Constants.GRADES).all(grade));
//            recommended.addCriteria(Criteria.where(VGroup.Constants.STUDENT_IDS).nin(userId));
//            recommended.addCriteria(Criteria.where(VGroup.Constants.REMOVED_IDS).nin(userId));
//            setProjection(recommended, include);
//
//            logger.info("Query : {}", recommended);
//
//            List<VGroup> vGroupsRecommended = runQuery(recommended, VGroup.class);
//
//            List<VGroupResponse> recommendedGroups = VGroupResponseConverter(vGroupsRecommended, userId);
//            vGroupFeedResponse.setRecommended(recommendedGroups);
//        }

        vGroupFeedResponse.setJoined(joinedGroups);

        return vGroupFeedResponse;
    }


    private List<VGroupResponse> VGroupResponseConverter(List<VGroup> vGroups, String userId)
    {
        List<VGroupResponse> vGroupResponseList  = new ArrayList<>();
        if (vGroups != null && !vGroups.isEmpty() && StringUtils.isNotEmpty(userId))
        {
            for (VGroup vGroup : vGroups)
            {
                VGroupResponse vGroupResponse = new VGroupResponse();
                vGroupResponse.setChannelId(vGroup.getChannelData().getChannel().getId());
                vGroupResponse.setGroupId(vGroup.getId());
                vGroupResponse.setBanned(vGroup.getBlockedStudents().contains(userId));
                vGroupResponse.setGroupName(vGroup.getGroupName());
                vGroupResponse.setGroupThumbnail(vGroup.getGroupThumbnail());
                vGroupResponse.setMemberCount(vGroup.getChannelData().getChannel().getMember_count());
                vGroupResponseList.add(vGroupResponse);
            }
        }
        logger.info("VGroupResponse list after conversion : {}", vGroupResponseList);
        return vGroupResponseList;
    }

    private void setProjection(Query query, List<String> includeFields)
    {
        if (!includeFields.isEmpty())
        {
            for (String field : includeFields)
            {
                query.fields().include(field);
            }
        }
    }
}
