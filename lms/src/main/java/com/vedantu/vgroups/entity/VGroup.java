package com.vedantu.vgroups.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vgroups.response.ChannelResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Set;


/**
 *
 * @author Ausaf
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(value = "VGroup")
@CompoundIndexes( {
        @CompoundIndex(name = "creationTime_-1", def = "{'creationTime':-1}", background = true)
})
public class VGroup extends AbstractMongoStringIdEntity
{
    @NotNull
    @Indexed(unique = true, background = true)
    private String groupName;

    @NotNull
    private String groupThumbnail;

    @NotNull
    private Set<String> classes;

    private Set<String> mainTags;

    @NotNull
    private Set<String> adminIds;

    @NotNull
    private Set<String> teacherIds;

    private Set<String> studentIds;

    private Set<String> pendingStudents;

    private Set<String> removedStudents;

    @NotNull
    private ChannelResponse channelData;

    private Set<String> blockedStudents;

    private String createdByUser;


    public static class Constants extends AbstractMongoEntity.Constants
    {
        public static final String GROUP_NAME = "groupName";
        public static final String GRADES = "classes";
        public static final String GROUP_THUMBNAIL = "groupThumbnail";
        public static final String MAIN_TAGS = "mainTags";
        public static final String TOTAL_MEMBERS = "channelData.channel.member_count";
        public static final String CHANNEL_ID = "channelData.channel.id";
        public static final String STUDENT_IDS = "studentIds";
        public static final String TEACHER_IDS = "teacherIds";
        public static final String ADMIN_IDS = "adminIds";
        public static final String BLOCKED_IDS = "blockedStudents";
        public static final String REMOVED_IDS = "removedStudents";
        public static final String PENDING_STUDENTS = "pendingStudents";
    }
}
