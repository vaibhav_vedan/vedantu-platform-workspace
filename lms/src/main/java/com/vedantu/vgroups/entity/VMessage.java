package com.vedantu.vgroups.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.vgroups.enums.AttachmentType;
import com.vedantu.vgroups.pojo.Attachment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;


/**
 *
 * @author Ausaf
 */


@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(value = "VMessage")
public class VMessage extends AbstractMongoStringIdEntity
{
    private String text;

    private String type;

    @NotNull
    private String userId;

    @NotNull
    private String groupId;

    private String parentId;

    private List<Attachment> attachments;

    private AttachmentType attachmentType;

    private boolean pinnedMessage;
}
