package com.vedantu.vgroups.enums;

public enum StreamRole
{
    user("user"), admin("admin"), guest("guest");

    String value;

    StreamRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
