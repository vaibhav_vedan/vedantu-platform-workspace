package com.vedantu.vgroups.enums;

public enum AttachmentType
{
    file("file"), image("image"), video("video");

    String val;

    AttachmentType(String val) {
        this.val = val;
    }

    public String getVal() {
        return val;
    }
}
