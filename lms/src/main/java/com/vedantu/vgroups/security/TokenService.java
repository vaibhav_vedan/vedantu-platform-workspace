package com.vedantu.vgroups.security;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.vgroups.pojo.Token;
import com.vedantu.vgroups.service.VGroupService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Header;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 *
 * @author Ausaf
 */


@Component
public class TokenService
{
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(TokenService.class);

    @Autowired
    private VGroupService vGroupService;

    private final String secretKey = ConfigUtils.INSTANCE.getStringValue("STREAM_SECRET_KEY");

    public Token getClientToken(String userId)
    {
        Claims claims = Jwts.claims();
        claims.put("user_id" , userId);

        Header header = Jwts.header();
        header.setType("JWT");

        String token = Jwts.builder()
                    .setHeader((Map<String, Object>)header)
                    .setClaims(claims)
                    .signWith(SignatureAlgorithm.HS256, secretKey.getBytes(StandardCharsets.UTF_8))
                    .compact();

        return new Token(token);
    }
}
