package com.vedantu.vgroups.pojo;

import com.vedantu.vgroups.enums.StreamRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SetStreamRole {
    private StreamRole role;
}
