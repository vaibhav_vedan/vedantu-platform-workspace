package com.vedantu.vgroups.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Channel
{
    private String id;
    private String name;
    private String type;
    private String cid;
    private String created_at;
    private String updated_at;
    private CreatedBy created_by;
    private int member_count;
}
