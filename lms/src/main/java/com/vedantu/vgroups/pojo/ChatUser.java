package com.vedantu.vgroups.pojo;

import com.vedantu.vgroups.enums.StreamRole;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ChatUser
{
    private String id;
    private StreamRole role;
    private SetStreamRole set;
    private String image;
    private String name;
}
