package com.vedantu.vgroups.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelData
{
    private String name;
    private CreatedBy created_by;
    private List<ChannelMember> members;
    private String channelThumbnail;
    private Set<String> bannedStudents;
    private String groupId;
}
