package com.vedantu.vgroups.client;

import com.vedantu.util.ConfigUtils;
import com.vedantu.vstories.client.OkhttpClient;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 *
 * @author Ausaf
 */


@Configuration
public class StreamClientConfig
{
    private Retrofit retrofit;

    private final String api_key = ConfigUtils.INSTANCE.getStringValue("STREAM_API_KEY");
    private final String token = ConfigUtils.INSTANCE.getStringValue("STREAM_SERVER_SIDE_TOKEN");

    public StreamClientConfig()
    {
        String streamEndpoint = ConfigUtils.INSTANCE.getStringValue("STREAM_ENDPOINT");

        Interceptor interceptor = new StreamInterceptor(token, api_key);

        OkHttpClient okhttpClient = OkhttpClient.getUnsafeOkHttpClient();

        okhttpClient = okhttpClient.newBuilder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(streamEndpoint)
                .client(okhttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    @Bean
    public StreamClient getStreamClient()
    {
        return retrofit.create(StreamClient.class);
    }
}

