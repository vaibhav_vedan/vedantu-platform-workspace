package com.vedantu.vgroups.client;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 *
 * @author Ausaf
 */

public class StreamInterceptor implements Interceptor {

    private final String token;
    private final String apiKey;

    public StreamInterceptor(String token, String apiKey)
    {
        this.token = token;
        this.apiKey = apiKey;
    }

    @Override
    public Response intercept(Chain chain) throws IOException
    {
        Request original = chain.request();
        Request request = original.newBuilder()
                .addHeader("api_key", apiKey)
                .addHeader("Stream-Auth-Type", "jwt")
                .addHeader("Authorization", token)
                .build();
        return chain.proceed(request);
    }
}
