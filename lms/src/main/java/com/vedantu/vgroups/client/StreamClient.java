package com.vedantu.vgroups.client;

import com.vedantu.vgroups.pojo.ChatUser;
import com.vedantu.vgroups.requests.*;
import com.vedantu.vgroups.response.ChannelResponse;
import com.vedantu.vgroups.response.FileResponse;
import com.vedantu.vgroups.response.MessageResponse;
import com.vedantu.vgroups.response.MessageResponseWrapper;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.*;

/**
 *
 * @author Ausaf
 */


public interface StreamClient
{
    @POST("/channels/{type}/{id}/query")
    Call<ChannelResponse> createChannel(@Path("type") String type,
                                        @Path("id") String id,
                                        @Body ChannelRequest channelRequest);

    @DELETE("/channels/{type}/{id}")
    Call<ChannelResponse> deleteChannel(@Path("type") String type,
                                        @Path("id") String id);

    @POST("/channels/{type}/{id}")
    Call<ChannelResponse>  updateChannel(@Path("type") String type,
                                         @Path("id") String id,
                                         @Body UpdateChannelRequest updateChannelRequest);

    @PATCH("/users")
    Call<UpdateUser> updateUserRole(@Body PatchRoleRequest patchRoleRequest);

    @POST("/users")
    Call<UpdateUser> createAdminUsers(@Body UpdateUser updateUser);

    @POST("/channels/{type}/{id}/message")
    Call<MessageResponseWrapper> sendMessage(@Path("type") String type,
                                             @Path("id") String id,
                                             @Body MessageRequestWrapper messageRequestWrapper);

    @DELETE("/messages/{id}")
    Call<MessageResponse> deleteMessage(@Path("id") String id);

    @Multipart
    @POST("/channels/{type}/{id}/file")
    Call<FileResponse> uploadAttachment(@Path("type") String type,
                                        @Path("id") String id,
                                        @Part MultipartBody.Part file,
                                        @Part("user") ChatUser user);

}
