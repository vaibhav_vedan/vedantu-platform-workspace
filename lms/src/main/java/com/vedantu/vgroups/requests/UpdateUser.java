package com.vedantu.vgroups.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.vgroups.pojo.ChatUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUser extends AbstractFrontEndReq
{
    private Map<String, ChatUser> users;
}
