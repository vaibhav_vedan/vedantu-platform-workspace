package com.vedantu.vgroups.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VGroupRequest extends AbstractFrontEndReq
{
    @NotNull
    private String groupName;

    @NotNull
    private String groupThumbnail;

    @NotNull
    private Set<String> classes;

    private Set<String> mainTags;

    @NotNull
    private Set<String> adminIds;

    @NotNull
    private Set<String> teacherIds;

}
