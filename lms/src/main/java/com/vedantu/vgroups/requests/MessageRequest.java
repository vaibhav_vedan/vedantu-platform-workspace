package com.vedantu.vgroups.requests;

import com.vedantu.User.Role;
import com.vedantu.vgroups.enums.AttachmentType;
import com.vedantu.vgroups.pojo.Attachment;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageRequest
{
    private String id;

    private String groupId;

    private String channelId;

    private String parent_id;

    private String text;

    private String user_id;

    private boolean pinnedMessage;

    private List<Attachment> attachments;

    private AttachmentType attachmentType;

    private Role callingUserRole;

}
