package com.vedantu.vgroups.requests;

import com.vedantu.vgroups.enums.AttachmentType;
import com.vedantu.vgroups.pojo.ChatUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FileRequest
{
    private String channelId;
    private MultipartFile multipartFile;
    private File file;
    private ChatUser user;
    private AttachmentType attachmentType;
}
