package com.vedantu.vgroups.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BroadcastRequest
{
    @NotNull
    private List<String> groupNames;

    @NotNull
    private List<String> groupDeepLinks;

    @NotNull
    private String text;

    @NotNull
    private String user_id;

    private boolean pinnedMessage;

    private boolean pushNotification;

}
