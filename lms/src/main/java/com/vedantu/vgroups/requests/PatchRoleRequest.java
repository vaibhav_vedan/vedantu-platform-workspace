package com.vedantu.vgroups.requests;

import com.vedantu.vgroups.pojo.ChatUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatchRoleRequest
{
    private List<ChatUser> users;
}
