package com.vedantu.vgroups.requests;

import com.vedantu.User.Role;
import com.vedantu.vgroups.enums.AttachmentType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttachmentRequest
{
    @NotNull
    private List<String> fileUrls;

    @NotNull
    private String userId;

    @NotNull
    private AttachmentType attachmentType;

    @NotNull
    private String groupId;

    private String text;

    private String parent_id;

    @NotNull
    private Role callingUserRole;
}
