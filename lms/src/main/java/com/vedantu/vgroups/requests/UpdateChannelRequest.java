package com.vedantu.vgroups.requests;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.vgroups.pojo.ChannelData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateChannelRequest extends AbstractFrontEndReq
{
    private String groupId;
    private String channelId;
    private List<String> add_members;
    private List<String> remove_members;
    private ChannelData data;
    private boolean updateChannelData;
}
