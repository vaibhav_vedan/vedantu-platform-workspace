package com.vedantu.vgroups.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MoengagePushRequest
{
    private String userId;

    private String groupName;

    private String body;

    private String deepLink;
}
