package com.vedantu.vgroups.requests;

import com.vedantu.vgroups.pojo.ChannelData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelRequest
{
    private String channelId;
    private ChannelData data;
}
