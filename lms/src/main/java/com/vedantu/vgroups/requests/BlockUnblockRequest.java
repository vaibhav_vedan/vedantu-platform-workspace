package com.vedantu.vgroups.requests;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class BlockUnblockRequest
{
    private Set<String> studentIds;
    private String groupName;
    private boolean blockRequest;
}
