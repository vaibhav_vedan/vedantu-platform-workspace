package com.vedantu.vgroups.service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.request.UserContactList;
import com.vedantu.User.response.UserBasicResponse;
import com.vedantu.cmds.managers.AmazonS3Manager;
import com.vedantu.cmds.managers.AwsSQSManager;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.*;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.vgroups.dao.VGroupDAO;
import com.vedantu.vgroups.dao.VMessageDAO;
import com.vedantu.vgroups.entity.VGroup;
import com.vedantu.vgroups.entity.VMessage;
import com.vedantu.vgroups.enums.AttachmentType;
import com.vedantu.vgroups.pojo.Attachment;
import com.vedantu.vgroups.pojo.ChannelData;
import com.vedantu.vgroups.pojo.ChatUser;
import com.vedantu.vgroups.requests.*;
import com.vedantu.vgroups.response.VGroupBasicResponse;
import com.vedantu.vgroups.response.VGroupFeedResponse;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author Ausaf
 */


@Service
public class VGroupChatServiceImpl implements VGroupChatService
{

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(VGroupChatServiceImpl.class);

    private final String NOTIFICATION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
    private final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private final String NODE_END_POINT = ConfigUtils.INSTANCE.getStringValue("DOUBT_NODE_SERVER");
    private final String VGROUP_BUCKET_NAME = ConfigUtils.INSTANCE.getStringValue("VGROUP_S3_BUCKET");
    private static final String TIME_ZONE_IN = "Asia/Kolkata";
    private static final int ONE_DAY = DateTimeUtils.SECONDS_PER_DAY;



    private static final Gson gson = new Gson();


    @Autowired
    private StreamService streamService;

    @Autowired
    private VGroupDAO vGroupDAO;

    @Autowired
    private VMessageDAO vMessageDAO;

    @Autowired
    private AmazonS3Manager s3Manager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private RedisDAO redisDAO;

    @Override
    public CompletableFuture<VGroupBasicResponse> updateStreamUserRole(ChatUser users) {
        return streamService.updateUserRole(users);

    }

    @Override
    public CompletableFuture<VGroupBasicResponse> createAdminUsers(UpdateUser userRequest) {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        if (userRequest.getUsers() == null || userRequest.getUsers().isEmpty()) {
            vGroupBasicResponse.setResponse("Empty user list");
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }
        return streamService.createAdminUsers(userRequest).thenApply(res -> {
            if (res != null)
                vGroupBasicResponse.setResponse("success");
            else
                vGroupBasicResponse.setResponse("Admins couldn't be created.");
            return vGroupBasicResponse;
        });
    }

    @Override
    public VGroupBasicResponse blockOrUnblockStudents(Set<String> studentList, String groupName, boolean blockRequest) {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        logger.info("Student list : {}, groupname : {}", studentList, groupName);
        VGroup vGroup = vGroupDAO.findByGroupName(groupName);

        if (vGroup == null)
        {
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            vGroupBasicResponse.setResponse("No VGroup present with name : " + groupName);
            return vGroupBasicResponse;
        }

        Set<String> notAMember = new HashSet<>();
        Set<String> notBlocked = new HashSet<>();

        for (String id : studentList) {
            if (!vGroup.getStudentIds().contains(id))
                notAMember.add(id);

            if (!blockRequest)
            {
                if (!vGroup.getBlockedStudents().contains(id))
                    notBlocked.add(id);
            }
        }

        if (!notAMember.isEmpty()) {
            vGroupBasicResponse.setResponse("Not a member of this group :" + notAMember);
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            return vGroupBasicResponse;
        }

        if (blockRequest)
        {
            vGroup.getBlockedStudents().addAll(studentList);
            vGroupDAO.save(vGroup);

            vGroupBasicResponse.setResponse("Blocked users : " + studentList);
        }
        else
        {
            if (!notBlocked.isEmpty())
            {
                vGroupBasicResponse.setResponse("These users are not blocked : " + notBlocked +" Can't unblock them. Please remove from list.");
                vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
                return vGroupBasicResponse;
            }

            vGroup.getBlockedStudents().removeAll(studentList);
            vGroupDAO.save(vGroup);

            vGroupBasicResponse.setResponse("Unblocked users : " + studentList);
        }

        //asynchronously update the channel's block list
        UpdateChannelRequest updateChannelRequest = new UpdateChannelRequest();
        ChannelData data = new ChannelData();
        data.setChannelThumbnail(vGroup.getGroupThumbnail());
        data.setName(vGroup.getGroupName());
        data.setBannedStudents(vGroup.getBlockedStudents());
        data.setGroupId(vGroup.getId());
        updateChannelRequest.setChannelId(vGroup.getChannelData().getChannel().getId());
        updateChannelRequest.setData(data);
        updateChannelRequest.setUpdateChannelData(true);
        awsSQSManager.sendToSQS(SQSQueue.VGROUP_QUEUE, SQSMessageType.UPDATE_VGROUP_CHANNEL, gson.toJson(updateChannelRequest));


        vGroupBasicResponse.setStatus(HttpStatus.OK);
        return vGroupBasicResponse;
    }

    @Override
    public CompletableFuture<VGroupBasicResponse> sendMessage(MessageRequest messageRequest)
    {
        logger.info("message request : {}", messageRequest);
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();

        //broadcast message
        if (messageRequest.getCallingUserRole().equals(Role.ADMIN))
        {

            List<String> include = new ArrayList<>();
            include.add(VGroup.Constants.CHANNEL_ID);
            include.add(VGroup.Constants.STUDENT_IDS);
            include.add(VGroup.Constants.TEACHER_IDS);
            include.add(VGroup.Constants.ADMIN_IDS);
            VGroup vGroup = vGroupDAO.findById(messageRequest.getGroupId(), include);
            logger.info("VGroup fetched : {}", vGroup);

            if (vGroup == null)
            {
                vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
                vGroupBasicResponse.setResponse("No Group present with the given id");
                return CompletableFuture.completedFuture(vGroupBasicResponse);
            }

            String userId = messageRequest.getUser_id();

            if (!vGroup.getAdminIds().contains(userId) && !vGroup.getTeacherIds().contains(userId) && !vGroup.getStudentIds().contains(userId))
            {
                vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
                vGroupBasicResponse.setResponse("User not a member of this VGroup!");
                return CompletableFuture.completedFuture(vGroupBasicResponse);
            }

            String channelId = vGroup.getChannelData().getChannel().getId();
            String messageId = UUID.randomUUID().toString();
            messageRequest.setChannelId(channelId);
            messageRequest.setId(messageId);
            MessageRequestWrapper messageRequestWrapper = new MessageRequestWrapper(messageRequest);
            return streamService.sendMessage(messageRequestWrapper)
                    .thenApply(messageResponse ->
                    {
                        if (messageResponse == null) {
                            vGroupBasicResponse.setResponse("Failed to send message!");
                            vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                            return vGroupBasicResponse;
                        }

                        if (messageRequest.getAttachmentType() != null) {
                            VMessage vMessage = new VMessage();
                            vMessage.setUserId(messageRequest.getUser_id());
                            vMessage.setGroupId(messageRequest.getGroupId());
                            vMessage.setAttachments(messageRequest.getAttachments());
                            vMessage.setParentId(messageRequest.getParent_id());
                            vMessage.setText(messageRequest.getText());
                            vMessage.setId(messageRequest.getId());
                            vMessage.setAttachmentType(messageRequest.getAttachmentType());
                            vMessage.setPinnedMessage(messageRequest.isPinnedMessage());
                            logger.info("VMessage : {}", vMessage);
                            vMessageDAO.save(vMessage);
                        }

                        vGroupBasicResponse.setStatus(HttpStatus.OK);
                        vGroupBasicResponse.setResponse("Message successfully sent!");
                        return vGroupBasicResponse;
                    })
                    .exceptionally(throwable ->
                    {
                        logger.info("Failed to save message due to : {}", throwable.getMessage());
                        vGroupBasicResponse.setResponse("Failed to save message!");
                        vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                        return vGroupBasicResponse;
                    });
        }
        //chat message
        else
        {
            if (messageRequest.getAttachmentType() != null) {
                VMessage vMessage = new VMessage();
                vMessage.setUserId(messageRequest.getUser_id());
                vMessage.setGroupId(messageRequest.getGroupId());
                vMessage.setAttachments(messageRequest.getAttachments());
                vMessage.setParentId(messageRequest.getParent_id());
                vMessage.setText(messageRequest.getText());
                vMessage.setId(messageRequest.getId());
                vMessage.setAttachmentType(messageRequest.getAttachmentType());
                vMessage.setPinnedMessage(messageRequest.isPinnedMessage());
                logger.info("VMessage : {}", vMessage);
                vMessageDAO.save(vMessage);
            }

            vGroupBasicResponse.setStatus(HttpStatus.OK);
            vGroupBasicResponse.setResponse("Message successfully sent!");
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }
    }

    @Override
    public CompletableFuture<VGroupBasicResponse> deleteMessage(String messageId) {

        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();

        if (StringUtils.isEmpty(messageId)) {
            vGroupBasicResponse.setResponse("Empty messageId");
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }

        return streamService.deleteMessage(messageId)
                .thenApply(messageResponse ->
                {
                    if (messageResponse == null)
                    {
                        vGroupBasicResponse.setResponse("Failed to delete message!");
                        vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                        return vGroupBasicResponse;
                    }

                    vMessageDAO.deleteById(messageId);
                    vGroupBasicResponse.setStatus(HttpStatus.OK);
                    vGroupBasicResponse.setResponse("Successfully deleted message!");
                    return vGroupBasicResponse;
                })
                .exceptionally(throwable ->
                {
                    logger.info("Failed to delete message due to : {}", throwable.getMessage());
                    vGroupBasicResponse.setResponse("Failed to delete message!");
                    vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                    return vGroupBasicResponse;
                });
    }

    @Override
    public CompletableFuture<VGroupBasicResponse> sendAttachment(AttachmentRequest attachmentRequest) {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();

        try {

            MessageRequest message = new MessageRequest();
            message.setUser_id(attachmentRequest.getUserId());
            message.setAttachmentType(attachmentRequest.getAttachmentType());
            message.setGroupId(attachmentRequest.getGroupId());
            message.setText(attachmentRequest.getText());
            message.setParent_id(attachmentRequest.getParent_id());
            message.setCallingUserRole(attachmentRequest.getCallingUserRole());

            AttachmentType attachmentType = attachmentRequest.getAttachmentType();

            if (attachmentType == null)
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Attachment type not provided : [ PDF, IMAGE, VIDEO ]");

            List<String> fileUrls = attachmentRequest.getFileUrls();

            if (fileUrls != null && !fileUrls.isEmpty())
            {
                Attachment attachment = new Attachment();
                attachment.setFileUrls(fileUrls);
                List<Attachment> attachments = new ArrayList<>();
                attachments.add(attachment);

                message.setAttachments(attachments);

                return sendMessage(message);
            }
        }
        catch (BadRequestException e)
        {
            logger.info("Couldn't send attachment due to : " + e.getMessage());
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            vGroupBasicResponse.setResponse(e.getMessage());
        }
        catch (Exception e)
        {
            logger.info("Couldn't send attachment due to : " + e.getMessage());
            vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            vGroupBasicResponse.setResponse(e.getMessage());
        }

        return CompletableFuture.completedFuture(vGroupBasicResponse);
    }

    @Override
    public VGroupFeedResponse fetchCurrentVGroups(String userId, String grade)
    {
        VGroupFeedResponse vGroupFeedResponse = vGroupDAO.fetchCurrentVGroups(userId, grade);
        logger.info("VGroup Feed Response : {}", vGroupFeedResponse);
        return vGroupFeedResponse;
    }

    @Override
    public UserContactList filterContacts(UserContactList userContactList) {

        try
        {
            String userActive = "ISUSERACTIVE";

            String activeStatus = redisDAO.get(userActive);

            if (!activeStatus.equals("true"))
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "User Service not active");
            else
            {
                ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT
                        + "/getUsersByContactVGroup", HttpMethod.POST, new Gson().toJson(userContactList), true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String respJson = resp.getEntity(String.class);
                UserBasicResponse userResponse = gson.fromJson(respJson, UserBasicResponse.class);
                logger.info("response : {} ", userResponse);

                if (!userResponse.isSuccess())
                    return userContactList;
                else
                {
                    List<String> include = new ArrayList<>();
                    include.add(VGroup.Constants.ID);
                    include.add(VGroup.Constants.STUDENT_IDS);
                    VGroup vGroup = vGroupDAO.findById(userContactList.getGroupId(), include);

                    if (vGroup == null)
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid group id");
                    else
                    {
                        for (User user : userResponse.getUsers())
                        {
                            if (vGroup.getStudentIds().contains(user.getId().toString()))
                            {
                                userContactList.getContactNumbers().remove(user.getContactNumber());
                            }
                        }
                        return userContactList;
                    }
                }
            }

        }
        catch (Exception e)
        {
            logger.info("Could not filter contacts due to : {}", e.getMessage());
            return null;
        }
    }

    @Override
    @Async("VStoryAsyncTaskExecutor")
    public void triggerInvitationSMS(String phoneCode, String phoneNumber, String groupName, String deepLink)
    {
        try {
            TextSMSRequest textSMSRequest = new TextSMSRequest();

            String textMessage = "Yo! I am part of this awesome " + groupName + " where you can connect with expert teachers.Do check out!- " + deepLink;

            textSMSRequest.setTo(phoneNumber);
            textSMSRequest.setPhoneCode(phoneCode);
            textSMSRequest.setBody(textMessage);

            ClientResponse respsms = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT
                    + "/SMS/sendSMS", HttpMethod.POST, new Gson().toJson(textSMSRequest), true);
            VExceptionFactory.INSTANCE.parseAndThrowException(respsms);
            String respsmsJson = respsms.getEntity(String.class);
            logger.info("respone of sendsms " + respsmsJson);

            VGroup vGroup = vGroupDAO.findByGroupName(groupName);
            if (vGroup != null) {
                vGroup.getPendingStudents().add(phoneNumber);
                vGroupDAO.save(vGroup);
            }
        }
        catch(VException e)
        {
            logger.info("Couldn't send sms due to : {}", e.getErrorMessage());
        }
    }

    @Override
    public VGroupBasicResponse registerUserInVGroups(String userId, String grade) {

        List<VGroup> vGroups = vGroupDAO.findByGrade(grade);
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();

        if (vGroups != null && !vGroups.isEmpty())
        {
            for (VGroup vGroup : vGroups)
            {
                UpdateChannelRequest updateChannelRequest = new UpdateChannelRequest();
                updateChannelRequest.setGroupId(vGroup.getId());
                updateChannelRequest.setCallingUserRole(Role.STUDENT);
                List<String> addStudent = new ArrayList<>();
                addStudent.add(userId);
                updateChannelRequest.setAdd_members(addStudent);
                String message = gson.toJson(updateChannelRequest);
                logger.info("initiating reg in group : {} for user : {}", vGroup.getId(), userId);
                awsSQSManager.sendToSQS(SQSQueue.VGROUP_QUEUE, SQSMessageType.JOIN_VGROUP, message);
            }
            vGroupBasicResponse.setResponse("Successfully initiated registration.");
        }
        else
        {
            vGroupBasicResponse.setResponse("Couldn't find any VGroup for this grade.");
        }
        vGroupBasicResponse.setStatus(HttpStatus.OK);

        return vGroupBasicResponse;
    }

    @Override
    public VGroupBasicResponse broadcastMessage(BroadcastRequest broadcastRequest)
    {
        logger.info("method : broadcast message  req: {}", broadcastRequest);
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        try {
            if (broadcastRequest.getGroupNames() != null && !broadcastRequest.getGroupNames().isEmpty()) {
                List<VGroup> vGroups = vGroupDAO.findByGroupNames(broadcastRequest.getGroupNames());
                if (vGroups != null && !vGroups.isEmpty()) {
                    List<String> notAnAdmin = new ArrayList<>();
                    Map<String, List<String>> unblockedStudents = new HashMap<>();
                    List<MessageRequest> messageRequests = new ArrayList<>();

                    for (VGroup vGroup : vGroups) {
                        if (!vGroup.getAdminIds().contains(broadcastRequest.getUser_id()))
                            notAnAdmin.add(vGroup.getGroupName());

                        MessageRequest messageRequest = new MessageRequest();
                        messageRequest.setUser_id(broadcastRequest.getUser_id());
                        messageRequest.setText(broadcastRequest.getText());
                        messageRequest.setGroupId(vGroup.getId());
                        messageRequest.setPinnedMessage(broadcastRequest.isPinnedMessage());
                        messageRequest.setCallingUserRole(Role.ADMIN);
                        messageRequests.add(messageRequest);

                        Set<String> studentIds = vGroup.getStudentIds();
                        studentIds.removeAll(vGroup.getBlockedStudents());
                        List<String> unblockedStudentIds = new ArrayList<>(studentIds);
                        unblockedStudents.put(vGroup.getGroupName(), unblockedStudentIds);
                    }

                    if (!notAnAdmin.isEmpty()) {
                        vGroupBasicResponse.setResponse("Admin not a part of these groups : " + notAnAdmin);
                        vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
                        return vGroupBasicResponse;
                    }

                    for (MessageRequest m : messageRequests) {
                        String messageRequest = gson.toJson(m);
                        awsSQSManager.sendToSQS(SQSQueue.VGROUP_QUEUE, SQSMessageType.BROADCAST_VGROUP_MESSAGE, messageRequest);
                    }

                        String pushResponse = "";

                        boolean push = false;

                    if (broadcastRequest.isPushNotification())
                    {
                        List<MoengagePushRequest> pushRequests = new ArrayList<>();

                        Date date = new Date();
                        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone(TIME_ZONE_IN));
                        calendar.setTime(date);
                        int day = calendar.get(Calendar.DATE);
                        String pushDailyLimit = "VGROUP_PUSH_DAILY_LIMIT_" + day;

                        String res = redisDAO.get(pushDailyLimit);

                        if (res == null) {
                            redisDAO.setex(pushDailyLimit, "1", ONE_DAY);
                            push = true;
                        } else {
                            int pushedCount = Integer.parseInt(res);
                            if (pushedCount == 0)
                                pushResponse = "Push Notification limit reached for today.";
                            else {
                                redisDAO.decr(pushDailyLimit);
                                push = true;
                            }
                        }


                        if (push) {
                            Map<String, String> deepLinkMap = generateDeepLinkMap(broadcastRequest);
                            if (!unblockedStudents.isEmpty()) {
                                for (Map.Entry<String, List<String>> entry : unblockedStudents.entrySet()) {
                                    if (entry.getValue().isEmpty())
                                        continue;

                                    for (String id : entry.getValue()) {
                                        MoengagePushRequest moengagePushRequest = new MoengagePushRequest();
                                        moengagePushRequest.setGroupName(entry.getKey());
                                        moengagePushRequest.setUserId(id);
                                        moengagePushRequest.setDeepLink(deepLinkMap.get(entry.getKey()));
                                        moengagePushRequest.setBody(broadcastRequest.getText());
                                        pushRequests.add(moengagePushRequest);
                                    }
                                }
                            }

                            logger.info("moengage push req : {}", pushRequests);

                            if (!pushRequests.isEmpty()) {
                                for (MoengagePushRequest moengagePushRequest : pushRequests) {
                                    String pushRequest = gson.toJson(moengagePushRequest);
                                    awsSQSManager.sendToSQS(SQSQueue.VGROUP_QUEUE, SQSMessageType.VGROUP_PUSH_NOTIFICATION, pushRequest);
                                }
                            }
                        }
                    }

                    vGroupBasicResponse.setStatus(HttpStatus.OK);
                    vGroupBasicResponse.setResponse("Message Broadcast Successful!");
                    if (!push)
                        vGroupBasicResponse.setResponse("Message Broadcast Successful! " + pushResponse);
                    return vGroupBasicResponse;
                }
            }
            vGroupBasicResponse.setResponse("Invalid group names");
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            return vGroupBasicResponse;
        }
        catch (Exception e)
        {
            logger.info("Couldn't broadcast message due to : {}", e.getMessage());
            vGroupBasicResponse.setResponse("Internal Server Error");
            vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            return vGroupBasicResponse;
        }
    }


    @Override
    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<String> pushVGroupNotification(MoengagePushRequest moengagePushRequest)
    {
        String respString;
        try
        {
            Map<String, Object> request = new HashMap<>();
            Map<String, Object> payload = new HashMap<>();
            request.put("toUserId", moengagePushRequest.getUserId());
            request.put("targetRole", "STUDENT");
            request.put("key", "qazwsxedcrfvtgbyhnujmikolp");
            request.put("notificationtype", "SERVER_NOTIFICATION");

            payload.put("title", moengagePushRequest.getGroupName());
            payload.put("deep_link", moengagePushRequest.getDeepLink());
            payload.put("body", moengagePushRequest.getBody());

            request.put("payload", payload);

            String url = NODE_END_POINT + "sendNotification";
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(request), true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            respString = resp.getEntity(String.class);
            logger.info("VGroup push notification response : " + respString);
        }
        catch (VException e)
        {
            logger.info("VGroup push notification failed due to : {}", e.getErrorMessage());
            respString = "VGroup push notification failed due to : " + e.getErrorMessage();
        }
        return CompletableFuture.completedFuture(respString);
    }

    @Override
    public VGroupBasicResponse uploadFile(MultipartFile multipartFile, String ext) throws BadRequestException {

        if (multipartFile == null)
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid request");

        String name = multipartFile.getOriginalFilename();

        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();


        if (StringUtils.isNotEmpty(name))
        {
            Date date = new Date();
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            String currentDay = simpleDateFormat.format(date);
            File file = handleFileUpload(multipartFile, ext);
            String key = currentDay + "/" + file.getName();
            String url = s3Manager.uploadFile(file, key, VGROUP_BUCKET_NAME);

            try {
                file.delete();
            }
            catch (Exception e)
            {
                //
            }

            vGroupBasicResponse.setResponse(url);
            vGroupBasicResponse.setStatus(HttpStatus.OK);
            return vGroupBasicResponse;
        }

        vGroupBasicResponse.setResponse("Could not upload file due to invalid input file");
        vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
        return vGroupBasicResponse;
    }

    private File handleFileUpload(MultipartFile file, String ext)
    {
        if (!file.isEmpty()) {
            BufferedOutputStream stream = null;
            try {
                File uploadedFile = File.createTempFile(UUID.randomUUID().toString(), ext);
                byte[] bytes = file.getBytes();
                stream = new BufferedOutputStream(new FileOutputStream(uploadedFile));
                stream.write(bytes);
                return uploadedFile;
            }
            catch (Exception e)
            {
                //
            }
            finally {
                IOUtils.closeQuietly(stream);
            }
        }
        return null;
    }

    private Map<String, String> generateDeepLinkMap(BroadcastRequest broadcastRequest)
    {
        Map<String, String>  map = new HashMap<>();
        List<String> groupNames = broadcastRequest.getGroupNames();
        List<String> deepLinks = broadcastRequest.getGroupDeepLinks();
        for (int i = 0; i < groupNames.size(); i++)
            map.put(groupNames.get(i), deepLinks.get(i));
        return map;
    }

}
