package com.vedantu.vgroups.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.request.UserContactList;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.listing.pojo.EsTeacherData;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.vgroups.dao.VGroupDAO;
import com.vedantu.vgroups.entity.VGroup;
import com.vedantu.vgroups.enums.StreamRole;
import com.vedantu.vgroups.pojo.ChannelData;
import com.vedantu.vgroups.pojo.ChannelMember;
import com.vedantu.vgroups.pojo.ChatUser;
import com.vedantu.vgroups.pojo.CreatedBy;
import com.vedantu.vgroups.requests.*;
import com.vedantu.vgroups.response.VGroupBasicResponse;
import okhttp3.Credentials;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author Ausaf
 */


@Service
public class VGroupServiceImpl implements VGroupService
{
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(VGroupServiceImpl.class);

    private final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private StreamService streamService;

    @Autowired
    private VGroupChatService vGroupChatService;

    @Autowired
    private VGroupDAO vGroupDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Override
    public CompletableFuture<VGroupBasicResponse> createVGroup(VGroupRequest vGroupRequest, String callingUserName) {

        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();

        try
        {
            logger.info("method : createVGroup, vgroup request : {}", vGroupRequest);
            VGroup vGroup = mapper.map(vGroupRequest, VGroup.class);
            vGroup.setCreatedByUser(callingUserName);

            ChannelRequest channelRequest = new ChannelRequest();
            String uuid = UUID.randomUUID().toString();
            channelRequest.setChannelId(uuid);
            ChannelData data = new ChannelData();
            data.setName(vGroupRequest.getGroupName());
            CreatedBy createdBy = new CreatedBy();
            createdBy.setId(vGroupRequest.getCallingUserId().toString());
            data.setCreated_by(createdBy);
            List<ChannelMember> memberList = new ArrayList<>();

            Map<String, ChatUser> userMap = new HashMap<>();

            vGroupRequest.getAdminIds().addAll(vGroupRequest.getTeacherIds());

            String userIdList = String.join(",", vGroupRequest.getAdminIds());
            String url = PLATFORM_ENDPOINT + "/user/getTeacherInfos?userIdsList=" + userIdList;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String respJson = resp.getEntity(String.class);
            Type type = new TypeToken<List<EsTeacherData>>() {}.getType();
            List<EsTeacherData> userResponse = new Gson().fromJson(respJson, type);
            logger.info("response : {} ", userResponse);


            for (String id : vGroupRequest.getAdminIds()) {
                ChannelMember member = new ChannelMember();
                ChatUser chatUser = new ChatUser();
                chatUser.setId(id);
                chatUser.setRole(StreamRole.admin);
                chatUser.setImage("https://vmkt.s3-ap-southeast-1.amazonaws.com/user.png");
                chatUser.setName("Admin");
                userMap.put(id, chatUser);
                member.setUser(chatUser);
                memberList.add(member);
            }

            if (userResponse != null && !userResponse.isEmpty()) {
                for (EsTeacherData esTeacherData : userResponse) {
                    ChatUser chatUser = userMap.get(esTeacherData.getTeacherId().toString());
                    if (StringUtils.isNotEmpty(esTeacherData.getProfilePicUrl()))
                        chatUser.setImage(esTeacherData.getProfilePicUrl());
                }
            }

            data.setMembers(memberList);
            data.setChannelThumbnail(vGroupRequest.getGroupThumbnail());
            data.setBannedStudents(new HashSet<>());
            channelRequest.setData(data);

            UpdateUser userRequest = new UpdateUser(userMap);

             return vGroupChatService.createAdminUsers(userRequest)
                     .thenCompose(res -> {
                         if (!res.getResponse().equals("success"))
                         {
                             vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                             vGroupBasicResponse.setResponse("VGroup could not be created due to :" + res.getResponse());
                             return CompletableFuture.completedFuture(vGroupBasicResponse);
                         }

                         return streamService.createChannel(channelRequest)
                            .thenApply(response -> {
                                 logger.info("Channel Response : {}", response);
                                 if (response != null)
                                 {
                                     vGroup.setChannelData(response);
                                     Set<String> studentsList = new HashSet<>();
                                     Set<String> blockedStudents = new HashSet<>();
                                     Set<String> pendingContacts = new HashSet<>();
                                     Set<String> removedStudents = new HashSet<>();
                                     vGroup.setStudentIds(studentsList);
                                     vGroup.setBlockedStudents(blockedStudents);
                                     vGroup.setPendingStudents(pendingContacts);
                                     vGroup.setRemovedStudents(removedStudents);
                                     vGroupDAO.save(vGroup);
                                     vGroupBasicResponse.setStatus(HttpStatus.OK);
                                     vGroupBasicResponse.setResponse("VGroup successfully created!");

                                     //asynchronously send a welcome message to the group
                                     MessageRequest messageRequest = new MessageRequest();
                                     String channelId = vGroup.getChannelData().getChannel().getId();
                                     String messageId = UUID.randomUUID().toString();
                                     messageRequest.setChannelId(channelId);
                                     messageRequest.setId(messageId);
                                     messageRequest.setUser_id(vGroupRequest.getAdminIds().iterator().next());
                                     messageRequest.setText("Welcome to " + vGroupRequest.getGroupName() + ".");
                                     messageRequest.setCallingUserRole(Role.ADMIN);
                                     MessageRequestWrapper messageRequestWrapper = new MessageRequestWrapper(messageRequest);
                                     streamService.sendMessage(messageRequestWrapper);
                                 }
                                 else
                                 {
                                     vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                                     vGroupBasicResponse.setResponse("VGroup could not be created");
                                 }
                                 return vGroupBasicResponse;
                             })
                             .exceptionally(throwable -> {
                                 logger.info("VGroup could not be created due to {}", throwable.getMessage());
                                 vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                                 vGroupBasicResponse.setResponse("VGroup could not be created.");
                                 return vGroupBasicResponse;
                             });
            });
        }
        catch (Exception e)
        {
            logger.info("VGroup cannot be created due to : {}", e.getMessage());
            vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            vGroupBasicResponse.setResponse("VGroup could not be created");
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }
    }

    @Override
    public CompletableFuture<VGroupBasicResponse> updateGroup(VGroupRequest vGroupRequest) {
        VGroup vGroup = vGroupDAO.findByGroupName(vGroupRequest.getGroupName());
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        if (vGroup == null)
        {
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            vGroupBasicResponse.setResponse("No group present with the given group name");
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }

        UpdateChannelRequest channelRequest = new UpdateChannelRequest();

        if (!vGroup.getGroupThumbnail().equals(vGroupRequest.getGroupThumbnail()))
            channelRequest.setUpdateChannelData(true);

        vGroup.setGroupThumbnail(vGroupRequest.getGroupThumbnail());
        vGroup.setClasses(vGroupRequest.getClasses());
        vGroup.setMainTags(vGroupRequest.getMainTags());

        Set<String> removeAdmins = new HashSet<>();

        Set<String> removeTeachers = new HashSet<>();

        for (String id : vGroup.getAdminIds())
        {
            if (vGroupRequest.getAdminIds().contains(id))
                vGroupRequest.getAdminIds().remove(id);
            else
                removeAdmins.add(id);
        }

        Set<String> addAdmins = new HashSet<>(vGroupRequest.getAdminIds());


        for (String id : vGroup.getTeacherIds())
        {
            if (vGroupRequest.getTeacherIds().contains(id))
                vGroupRequest.getTeacherIds().remove(id);
            else
                removeTeachers.add(id);
        }

        Set<String> addTeachers = new HashSet<>(vGroupRequest.getTeacherIds());

        String channelId = vGroup.getChannelData().getChannel().getId();


        channelRequest.setChannelId(channelId);

        ArrayList<String> addMembers = new ArrayList<>(addAdmins);
        addMembers.addAll(addTeachers);

        UpdateUser updateUser = new UpdateUser();
        Map<String, ChatUser> map = new HashMap<>();

        if (!addMembers.isEmpty()) {
            for (String id : addMembers) {
                ChatUser user = new ChatUser();
                user.setId(id);
                user.setRole(StreamRole.admin);
                map.put(id, user);
            }
            channelRequest.setAdd_members(addMembers);
        }

        updateUser.setUsers(map);

        ArrayList<String> removeMembers = new ArrayList<>(removeAdmins);
        removeMembers.addAll(removeTeachers);

        if (!removeMembers.isEmpty())
            channelRequest.setRemove_members(removeMembers);

        if (addMembers.isEmpty() && removeMembers.isEmpty() && !channelRequest.isUpdateChannelData())
        {
            logger.info("Updated VGroup : {}", vGroup);
            vGroupDAO.save(vGroup);
            vGroupBasicResponse.setStatus(HttpStatus.OK);
            vGroupBasicResponse.setResponse("Successfully updated VGroup.");
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }

        ChannelData data = new ChannelData();
        data.setChannelThumbnail(vGroupRequest.getGroupThumbnail());
        data.setName(vGroup.getGroupName());
        data.setBannedStudents(vGroup.getBlockedStudents());
        data.setGroupId(vGroup.getId());
        channelRequest.setData(data);

        logger.info("Channel Request : {}", channelRequest);


        return vGroupChatService.createAdminUsers(updateUser)
                .thenCompose(response -> {
                    if (!response.getResponse().equals("success") && !response.getResponse().equals("Empty user list"))
                    {
                        vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                        vGroupBasicResponse.setResponse("Couldn't update VGroup.");
                        return CompletableFuture.completedFuture(vGroupBasicResponse);
                    }

                    return streamService.updateChannel(channelRequest)
                            .thenApply(res -> {
                                if (res != null)
                                {
                                    vGroup.getAdminIds().addAll(addAdmins);
                                    vGroup.getAdminIds().removeAll(removeAdmins);
                                    vGroup.getTeacherIds().addAll(addTeachers);
                                    vGroup.getTeacherIds().removeAll(removeTeachers);
                                    vGroup.setChannelData(res);
                                    logger.info("Updated VGroup : {}", vGroup);
                                    vGroupDAO.save(vGroup);
                                    vGroupBasicResponse.setStatus(HttpStatus.OK);
                                    vGroupBasicResponse.setResponse("Successfully updated VGroup.");
                                    return vGroupBasicResponse;
                                }

                                vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                                vGroupBasicResponse.setResponse("Couldn't update VGroup.");
                                return vGroupBasicResponse;
                            });

                });
    }

    @Override
    public CompletableFuture<VGroupBasicResponse> deleteVGroup(String groupId)
    {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        VGroup vGroup = vGroupDAO.findById(groupId, null);
        if (vGroup == null)
        {
            vGroupBasicResponse.setResponse("No VGroup present with the given id");
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }

        String channelId = vGroup.getChannelData().getChannel().getId();
        return streamService.deleteChannel(channelId)
                .thenApply(response -> {
                    if (response == null)
                    {
                        vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                        vGroupBasicResponse.setResponse("VGroup channel could not be deleted");
                    }
                    else
                    {
                        vGroupDAO.deleteById(groupId);
                        vGroupBasicResponse.setStatus(HttpStatus.OK);
                        vGroupBasicResponse.setResponse("VGroup successfully deleted.");
                    }
                    return vGroupBasicResponse;
                });
    }


    @Override
    public CompletableFuture<VGroupBasicResponse> joinOrLeaveVGroup(UpdateChannelRequest updateChannelRequest) {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        VGroup vGroup = vGroupDAO.findById(updateChannelRequest.getGroupId(), null);

        if (vGroup == null)
        {
            vGroupBasicResponse.setResponse("No VGroup present with the given id");
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            return CompletableFuture.completedFuture(vGroupBasicResponse);
        }

        updateChannelRequest.setChannelId(vGroup.getChannelData().getChannel().getId());


        Set<String> blockedUser = new HashSet<>();
        Set<String> removedStudents = new HashSet<>();
        Set<String> notPresent = new HashSet<>();
        UpdateUser updateUser = new UpdateUser();

        if (updateChannelRequest.getAdd_members() != null && !updateChannelRequest.getAdd_members().isEmpty()) {
            Map<String, ChatUser> map = new HashMap<>();

            for (String id : updateChannelRequest.getAdd_members()) {

                if (vGroup.getStudentIds().contains(id) && vGroup.getBlockedStudents().contains(id))
                    blockedUser.add(id);

                if (vGroup.getRemovedStudents().contains(id))
                    removedStudents.add(id);

                ChatUser user = new ChatUser();
                user.setId(id);
                user.setRole(StreamRole.admin);
                map.put(id, user);
            }
            updateUser.setUsers(map);

            if (!blockedUser.isEmpty())
            {
                vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
                vGroupBasicResponse.setResponse("User(s) already present in the group but blocked! " + blockedUser);
                return CompletableFuture.completedFuture(vGroupBasicResponse);
            }
        }



        if (updateChannelRequest.getRemove_members() != null && !updateChannelRequest.getRemove_members().isEmpty())
        {
            for (String id : updateChannelRequest.getRemove_members())
            {
                if (!vGroup.getStudentIds().contains(id))
                {
                    notPresent.add(id);
                }
                if (vGroup.getBlockedStudents().contains(id))
                {
                    blockedUser.add(id);
                }
            }

            if (!notPresent.isEmpty())
            {
                vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
                vGroupBasicResponse.setResponse("User(s) not present in the group. " + notPresent);
                return CompletableFuture.completedFuture(vGroupBasicResponse);
            }

            if (!blockedUser.isEmpty())
            {
                vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
                vGroupBasicResponse.setResponse("User(s) blocked! Need to be unblocked first to leave the group. " + blockedUser);
                return CompletableFuture.completedFuture(vGroupBasicResponse);
            }
        }



        ChannelData data = new ChannelData();
        data.setChannelThumbnail(vGroup.getGroupThumbnail());
        data.setName(vGroup.getGroupName());
        data.setBannedStudents(vGroup.getBlockedStudents());
        data.setGroupId(vGroup.getId());
        updateChannelRequest.setData(data);
        updateChannelRequest.setUpdateChannelData(true);

        logger.info("Channel Request : {}", updateChannelRequest);


        return vGroupChatService.createAdminUsers(updateUser)
                .thenCompose(res -> {
                    if (!res.getResponse().equals("success") && !res.getResponse().equals("Empty user list"))
                    {
                        vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                        vGroupBasicResponse.setResponse("Couldn't add member to the group.");
                        return CompletableFuture.completedFuture(vGroupBasicResponse);
                    }

                    return streamService.updateChannel(updateChannelRequest)
                            .thenApply(response -> {
                                if (response != null)
                                {
                                    if (updateChannelRequest.getAdd_members() != null && !updateChannelRequest.getAdd_members().isEmpty()) {
                                        vGroup.getStudentIds().addAll(updateChannelRequest.getAdd_members());

                                    if (updateChannelRequest.getCallingUserRole().equals(Role.ADMIN))
                                        vGroup.getRemovedStudents().removeAll(removedStudents);

                                        //asynchronously update pending status
                                        updatePendingStatus(vGroup, updateChannelRequest.getAdd_members().get(0));
                                    }

                                    if (updateChannelRequest.getRemove_members() != null && !updateChannelRequest.getRemove_members().isEmpty()) {
                                        vGroup.getStudentIds().removeAll(updateChannelRequest.getRemove_members());

                                        if (updateChannelRequest.getCallingUserRole().equals(Role.ADMIN))
                                            vGroup.getRemovedStudents().addAll(updateChannelRequest.getRemove_members());
                                    }

                                    vGroup.setChannelData(response);
                                    vGroupDAO.save(vGroup);
                                    vGroupBasicResponse.setResponse("Successfully updated VGroup.");
                                    vGroupBasicResponse.setStatus(HttpStatus.OK);
                                }
                                else
                                {
                                    vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
                                    vGroupBasicResponse.setResponse("Couldn't add/remove member to the group.");
                                }
                                return vGroupBasicResponse;
                            });
                });
    }

    @Override
    public List<VGroup> fetchVGroups(String groupId, Integer skip, Integer limit) {
        return vGroupDAO.fetchVGroups(groupId, skip, limit);
    }

    @Async("VStoryAsyncTaskExecutor")
    private void updatePendingStatus(VGroup vGroup, String userId)
    {
        try
        {
            String userActive = "ISUSERACTIVE";

            String activeStatus = redisDAO.get(userActive);

            if (!activeStatus.equals("true"))
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "User Service not active");
            else
            {
                String url = USER_ENDPOINT + "/getUserById?userId=" + userId;
                ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET,null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String respJson = resp.getEntity(String.class);
                User userResponse = new Gson().fromJson(respJson, User.class);
                logger.info("response : {} ", userResponse);

                Set<String> pendingStudents = vGroup.getPendingStudents();
                if (pendingStudents != null && !pendingStudents.isEmpty())
                {
                    pendingStudents.remove(userResponse.getContactNumber());
                    vGroupDAO.save(vGroup);
                    logger.info("Successfully updated user's pending status for userId : {}, contact : {}", userId, userResponse.getContactNumber());
                }
            }
        }
        catch (Exception e)
        {
            logger.info("Could not get User info due to : {}", e.getMessage());
        }
    }


    //not needed
    //Moengage user is getting created from SDK itself
    @Async("VStoryAsyncTaskExecutor")
    public void createMoengageUser(String studentId)
    {
        try {
            ConfigUtils configUtils = ConfigUtils.INSTANCE;
            String moengageBasicUrl = configUtils.getStringValue("MOENGAGE_ENDPOINT");
            String appId = configUtils.getStringValue("MOENGAGE_APP_ID");
            String password = configUtils.getStringValue("MOENGAGE_SECRET_KEY");
            moengageBasicUrl += "customer/" + appId;
            String basicCreds = Credentials.basic(appId, password);


            JsonObject jsonObject = new JsonObject();
            jsonObject.addProperty("type", "customer");
            jsonObject.addProperty("customer_id", studentId);

            ClientResponse resp = WebUtils.INSTANCE.doCall(moengageBasicUrl, HttpMethod.POST, new Gson().toJson(jsonObject), basicCreds);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info("User created In MoEngage" + jsonString);
        }
        catch (Exception e)
        {
            logger.info("Could not create moengage user due to : {}", e.getMessage());
        }
    }

    @Override
    public UserContactList getInvitedStudents(UserContactList userContactList)
    {
        List<String> include = new ArrayList<>();
        include.add(VGroup.Constants.PENDING_STUDENTS);
        include.add(VGroup.Constants.ID);
        VGroup vGroup = vGroupDAO.findById(userContactList.getGroupId(), include);

        Set<String> pendingStudents = vGroup.getPendingStudents();

        userContactList.getContactNumbers()
                       .removeIf(contactNumber -> !pendingStudents.contains(contactNumber));

        logger.info("Invited Students : {}", userContactList);

        return userContactList;
    }
}
