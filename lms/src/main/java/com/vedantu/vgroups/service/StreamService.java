package com.vedantu.vgroups.service;

import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.vgroups.client.StreamClient;
import com.vedantu.vgroups.pojo.ChatUser;
import com.vedantu.vgroups.requests.*;
import com.vedantu.vgroups.response.*;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import retrofit2.Response;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author Ausaf
 */


@Component
public class StreamService
{
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(StreamService.class);

    @Autowired
    private StreamClient streamClient;


    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<ChannelResponse> createChannel(ChannelRequest channelRequest)
    {
        try {
            logger.info("Channel Request : {}", channelRequest);
            Response<ChannelResponse> response = streamClient.createChannel("team", channelRequest.getChannelId(), channelRequest).execute();
            if (response.isSuccessful()) {
                return CompletableFuture.completedFuture(response.body());
            } else {
                logger.info("Could not create channel due to : {}", response.message());
            }
        }
        catch (Exception e)
        {
            logger.info("Could not create channel due to : {}", e.getMessage());
        }
        return null;
    }

    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<UpdateUser> createAdminUsers(UpdateUser updateUser)
    {
        try {
            logger.info("Create Admin request : {}", updateUser);
            Response<UpdateUser> userResponse = streamClient.createAdminUsers(updateUser).execute();
            if (userResponse.isSuccessful()) {
                UpdateUser user = userResponse.body();
                logger.info("Created Admins successfully : {}", user);
                return CompletableFuture.completedFuture(user);
            }
            else
            {
                logger.info("Admins couldn't be created for {} due to :{}", updateUser, userResponse.message());
            }
        }
        catch (Exception e)
        {
            logger.info("Admins couldn't be created for {} due to :{}", updateUser, e.getMessage());
        }
        return null;
    }

    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<ChannelResponse> deleteChannel(String channelId)
    {
        try
        {
            logger.info("Delete channel, id : {}", channelId);
            Response<ChannelResponse> response = streamClient.deleteChannel("team", channelId).execute();
            if (response.isSuccessful())
            {
                return CompletableFuture.completedFuture(response.body());
            }
            else
            {
                logger.info("Channel could not be deleted due to {}", response.message());
            }
        }
        catch (Exception e)
        {
            logger.info("Channel could not be deleted due to {}", e.getMessage());
        }
        return null;
    }

    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<VGroupBasicResponse> updateUserRole(ChatUser user)
    {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        try {
            List<ChatUser> users = new ArrayList<>();
            users.add(user);
            PatchRoleRequest patchRoleRequest = new PatchRoleRequest(users);
            logger.info("Update role request : {}", user);
            Response<UpdateUser> response = streamClient.updateUserRole(patchRoleRequest).execute();
            if (response.isSuccessful()) {
                logger.info("Chat role successfully updated for user : {}", user);
                vGroupBasicResponse.setResponse("Chat role successfully updated for user " + user.getId());
                vGroupBasicResponse.setStatus(HttpStatus.OK);
            }
            else {
                logger.info("Chat role couldn't be updated due to : {}", response.message());
                vGroupBasicResponse.setResponse("Chat role couldn't be updated.");
                vGroupBasicResponse.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        catch (Exception e)
        {
            logger.info("Chat role couldn't be updated due to : {}", e.getMessage());
        }
        return CompletableFuture.completedFuture(vGroupBasicResponse);
    }

    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<ChannelResponse> updateChannel(UpdateChannelRequest updateChannelRequest)
    {
        logger.info("updateChannel request : {}", updateChannelRequest);

        if ((updateChannelRequest.getAdd_members() == null || updateChannelRequest.getAdd_members().isEmpty() )
                && (updateChannelRequest.getRemove_members() == null || updateChannelRequest.getRemove_members().isEmpty())
                && !updateChannelRequest.isUpdateChannelData())
        {
            return null;
        }
        try
        {
            Response<ChannelResponse> response = streamClient.updateChannel("team",updateChannelRequest.getChannelId(), updateChannelRequest).execute();
            if (response.isSuccessful())
            {
                ChannelResponse channelResponse = response.body();
                logger.info("Updated channel successfully : {}", channelResponse);
                return CompletableFuture.completedFuture(channelResponse);
            }
            else
            {
                logger.info("Couldn't update the channel due to {}", response.message());
                return null;
            }
        }
        catch (Exception e)
        {
            logger.info("Couldn't update the channel due to {}", e.getMessage());
            return null;
        }
    }

    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<MessageResponse> sendMessage(MessageRequestWrapper messageRequestWrapper)
    {
        logger.info("message request : {}", messageRequestWrapper);

        if (messageRequestWrapper == null || messageRequestWrapper.getMessage() == null)
            return null;
        try
        {
            Response<MessageResponseWrapper> response = streamClient.sendMessage("team", messageRequestWrapper.getMessage().getChannelId(), messageRequestWrapper).execute();
            if (response.isSuccessful())
            {
                MessageResponseWrapper messageResponseWrapper = response.body();
                MessageResponse messageResponse = messageResponseWrapper.getMessage();
                logger.info("message response : {}", messageResponse);
                return CompletableFuture.completedFuture(messageResponse);
            }
            else
            {
                logger.info("Couldn't send message due to :{}", response.message());
                return null;
            }
        }
        catch (Exception e) {
            logger.info("Couldn't send message due to :{}", e.getMessage());
            return null;
        }
    }

    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<FileResponse> uploadAttachment(FileRequest fileRequest)
    {
        logger.info("upload media request : {}", fileRequest);

        if (fileRequest.getFile() == null || fileRequest.getUser() == null || fileRequest.getAttachmentType() == null || StringUtils.isEmpty(fileRequest.getChannelId()))
        {
            logger.info("Empty file request");
            return null;
        }

        try
        {
            File file = fileRequest.getFile();
            RequestBody requestBody = RequestBody.create(MultipartBody.FORM, file);
            MultipartBody.Part body = MultipartBody.Part.createFormData("file", file.getName(), requestBody);

            Response<FileResponse> response = streamClient.uploadAttachment("team", fileRequest.getChannelId(), body, fileRequest.getUser()).execute();

            if (response.isSuccessful())
            {
                FileResponse fileResponse = response.body();
                logger.info("file response : {}", fileResponse);
                return CompletableFuture.completedFuture(fileResponse);
            }
            else
            {
                logger.info("Could not upload media due to : {} ", response.message());
            }
        }
        catch (Exception e)
        {
              logger.info("Could not upload media due to : {}", e.getMessage());
        }
        return null;
    }


    @Async("VStoryAsyncTaskExecutor")
    public CompletableFuture<MessageResponse> deleteMessage(String messageId)
    {
        if (messageId == null)
        {
            logger.info("Empty message id");
            return null;
        }

        try {
            Response<MessageResponse> response = streamClient.deleteMessage(messageId).execute();

            if (response.isSuccessful())
            {
                MessageResponse messageResponse = response.body();
                logger.info("Message Response : {}", messageResponse);
                return CompletableFuture.completedFuture(messageResponse);
            }
            else
            {
                logger.info("Could not delete message due to : {}", response.message());
            }
        }
        catch (Exception e)
        {
            logger.info("Could not delete message due to : {}", e.getMessage());
        }

        return null;
    }
}
