package com.vedantu.vgroups.service;

import com.vedantu.User.request.UserContactList;
import com.vedantu.vgroups.entity.VGroup;
import com.vedantu.vgroups.requests.UpdateChannelRequest;
import com.vedantu.vgroups.requests.VGroupRequest;
import com.vedantu.vgroups.response.VGroupBasicResponse;

import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author Ausaf
 */


public interface VGroupService
{
    CompletableFuture<VGroupBasicResponse> createVGroup(VGroupRequest vGroupRequest, String callingUserName);

    CompletableFuture<VGroupBasicResponse> updateGroup(VGroupRequest vGroupRequest);

    CompletableFuture<VGroupBasicResponse> deleteVGroup(String groupId);

    CompletableFuture<VGroupBasicResponse> joinOrLeaveVGroup(UpdateChannelRequest updateChannelRequest);

    List<VGroup> fetchVGroups(String groupId, Integer skip, Integer limit);

    void createMoengageUser(String studentId);

    UserContactList getInvitedStudents(UserContactList userContactList);
}
