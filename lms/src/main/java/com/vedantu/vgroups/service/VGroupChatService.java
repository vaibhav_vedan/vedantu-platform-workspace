package com.vedantu.vgroups.service;


import com.vedantu.User.request.UserContactList;
import com.vedantu.exception.BadRequestException;
import com.vedantu.vgroups.pojo.ChatUser;
import com.vedantu.vgroups.requests.*;
import com.vedantu.vgroups.response.VGroupBasicResponse;
import com.vedantu.vgroups.response.VGroupFeedResponse;
import org.springframework.web.multipart.MultipartFile;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author Ausaf
 */


public interface VGroupChatService
{
    CompletableFuture<VGroupBasicResponse> updateStreamUserRole(ChatUser user);

    CompletableFuture<VGroupBasicResponse> createAdminUsers(UpdateUser updateUser);

    VGroupBasicResponse blockOrUnblockStudents(Set<String> studentList, String groupName, boolean block);

    CompletableFuture<VGroupBasicResponse> sendMessage(MessageRequest messageRequest);

    CompletableFuture<VGroupBasicResponse> deleteMessage(String messageId);

    CompletableFuture<VGroupBasicResponse> sendAttachment(AttachmentRequest attachmentRequest);

    VGroupFeedResponse fetchCurrentVGroups(String userId, String grade);

    UserContactList filterContacts(UserContactList userContactList);

    void triggerInvitationSMS(String phoneCode, String phoneNumber, String groupName, String textMessage);

    VGroupBasicResponse registerUserInVGroups(String userId, String grade);

    VGroupBasicResponse broadcastMessage(BroadcastRequest broadcastRequest);

    CompletableFuture<String> pushVGroupNotification(MoengagePushRequest moengagePushRequest);

    VGroupBasicResponse uploadFile(MultipartFile multipartFile, String ext) throws BadRequestException;
}
