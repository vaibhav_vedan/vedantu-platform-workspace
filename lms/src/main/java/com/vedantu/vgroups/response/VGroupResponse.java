package com.vedantu.vgroups.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VGroupResponse
{
    private String groupId;

    private String groupName;

    private String groupThumbnail;

    private int memberCount;

    private boolean banned;

    private String channelId;
}
