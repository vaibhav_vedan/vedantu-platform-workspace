package com.vedantu.vgroups.response;

import com.vedantu.vgroups.pojo.Channel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChannelResponse
{
    private Channel channel;
    //private List<ChannelMember> members;
}
