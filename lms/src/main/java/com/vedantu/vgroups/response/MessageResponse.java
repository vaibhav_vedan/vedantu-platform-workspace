package com.vedantu.vgroups.response;

import com.vedantu.vgroups.pojo.Attachment;
import com.vedantu.vgroups.pojo.ChatUser;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MessageResponse
{
    private String id;
    private String text;
    private String type;
    private ChatUser user;
    private String parent_id;
    private List<Attachment> attachments;
    private boolean pinnedMessage;
}
