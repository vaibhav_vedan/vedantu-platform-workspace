package com.vedantu.vgroups.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class VGroupFeedResponse
{
    private List<VGroupResponse> joined;
    private List<VGroupResponse> recommended;
}
