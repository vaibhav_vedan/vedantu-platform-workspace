package com.vedantu.vgroups.controllers;


import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.UserContactList;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vgroups.entity.VGroup;
import com.vedantu.vgroups.requests.UpdateChannelRequest;
import com.vedantu.vgroups.requests.VGroupRequest;
import com.vedantu.vgroups.response.VGroupBasicResponse;
import com.vedantu.vgroups.service.VGroupService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.CompletableFuture;

/**
 *
 * @author Ausaf
 */


@RestController
@RequestMapping("/VGroup")
public class VGroupController
{
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(VGroupController.class);

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private VGroupService vGroupService;

    @RequestMapping(value = "/createVGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> createVGroup(@Valid @RequestBody VGroupRequest vGroupRequest) throws BadRequestException {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(vGroupRequest.getCallingUserId(), false);
        logger.info("method : createVGroup, VGroup request : {}, User : {}", vGroupRequest, userBasicInfo);

        if (!userBasicInfo.getRole().equals(Role.ADMIN))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Only Admins allowed to create VGroup");

        CompletableFuture<VGroupBasicResponse> vGroupBasicResponseCompletableFuture = vGroupService.createVGroup(vGroupRequest, userBasicInfo.getFullName());

        return vGroupBasicResponseCompletableFuture.thenApply(res -> {
            logger.info("VGroup creation response : {}", res);
            return ResponseEntity.ok(res);
        });
    }

    @RequestMapping(value = "/editVGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> updateVGroup(@Valid @RequestBody VGroupRequest vGroupRequest) throws BadRequestException {
        logger.info("method : editVGroup, VGroup request : {}", vGroupRequest);

        Role role = httpSessionUtils.getCallingUserRole();

        if (!role.equals(Role.ADMIN))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Only Admins allowed to edit VGroup");

        return vGroupService.updateGroup(vGroupRequest)
                .thenApply(ResponseEntity::ok);
    }

    @RequestMapping(value = "/deleteVGroup", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> deleteVGroup(@RequestParam("groupId") String groupId,
                                                                               @RequestParam("callingUserId") String userId) {
        VGroupBasicResponse vGroupBasicResponse = new VGroupBasicResponse();
        logger.info("method : deleteVGroup : groupId : {}", groupId);

        try
        {
            if (StringUtils.isEmpty(groupId) || StringUtils.isEmpty(userId))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty groupId/userId");

            Role role = httpSessionUtils.getCallingUserRole();

            if (!role.equals(Role.ADMIN))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Only Admins allowed to delete VGroup");


            return vGroupService.deleteVGroup(groupId)
                    .thenApply(res -> {
                        logger.info("VGroup delete response : {}", res);
                        return ResponseEntity.ok(res);
                    });
        }
        catch (BadRequestException b)
        {
            vGroupBasicResponse.setStatus(HttpStatus.BAD_REQUEST);
            vGroupBasicResponse.setResponse("BAD REQUEST");
            return CompletableFuture.completedFuture(new ResponseEntity<>(vGroupBasicResponse, vGroupBasicResponse.getStatus()));
        }
    }

    @RequestMapping(value = "/joinOrLeaveVGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> joinOrLeaveVGroup(@RequestBody UpdateChannelRequest updateChannelRequest) throws BadRequestException {

        if (updateChannelRequest == null || StringUtils.isEmpty(updateChannelRequest.getGroupId()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty request");

        Role role = httpSessionUtils.getCallingUserRole();

        logger.info("method : joinOrLeaveVGroup, updateChannelRequest : {}, role : {}", updateChannelRequest, role);

        if (role.equals(Role.STUDENT) && StringUtils.isEmpty(updateChannelRequest.getAppVersionCode()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty appVersionCode");

        if (role.equals(Role.STUDENT) && updateChannelRequest.getAppVersionCode().compareTo("1.7.3") < 0)
            return CompletableFuture.completedFuture(ResponseEntity.ok(new VGroupBasicResponse("Can't join with old version", HttpStatus.OK)));

        return vGroupService.joinOrLeaveVGroup(updateChannelRequest)
                .thenApply(res -> {
                    logger.info(res.getResponse());
                    return ResponseEntity.ok(res);
                });
    }


    @RequestMapping(value = "/fetchAllVGroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VGroup>> fetchAllVGroups(@RequestParam(value = "groupId", required = false) String groupId,
                                                        @RequestParam(value = "skip", required = false) Integer skip,
                                                        @RequestParam(value = "limit", required = false) Integer limit)
    {
        logger.info("method : fetchAllVGroups, groupId : {}, skip : {}, limit : {}", groupId, skip, limit);
        return ResponseEntity.ok(vGroupService.fetchVGroups(groupId, skip, limit));
    }

    @RequestMapping(value = "/getInvitedStudents", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserContactList> getInvitedStudents(@RequestBody UserContactList userContactList) throws BadRequestException {
        logger.info("method : filter pending contacts, req : {}", userContactList);

        if (userContactList.getContactNumbers() == null || userContactList.getContactNumbers().isEmpty()
                   || StringUtils.isEmpty(userContactList.getGroupId()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty request");

        return ResponseEntity.ok(vGroupService.getInvitedStudents(userContactList));
    }

}
