package com.vedantu.vgroups.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.request.UserContactList;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import com.vedantu.vgroups.pojo.ChatUser;
import com.vedantu.vgroups.pojo.Token;
import com.vedantu.vgroups.requests.AttachmentRequest;
import com.vedantu.vgroups.requests.BlockUnblockRequest;
import com.vedantu.vgroups.requests.BroadcastRequest;
import com.vedantu.vgroups.requests.MessageRequest;
import com.vedantu.vgroups.response.VGroupBasicResponse;
import com.vedantu.vgroups.response.VGroupFeedResponse;
import com.vedantu.vgroups.security.TokenService;
import com.vedantu.vgroups.service.VGroupChatService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.concurrent.CompletableFuture;


/**
 *
 * @author Ausaf
 */

@RestController
@RequestMapping("/VChat")
public class VChatController
{
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(VChatController.class);

    @Autowired
    private TokenService tokenService;

    @Autowired
    private VGroupChatService vGroupChatService;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @RequestMapping(value = "/getClientToken", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Token> getClientToken(@RequestParam("userId") String userId) throws BadRequestException {
        if (StringUtils.isEmpty(userId))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty userId");

        logger.info("method : getClientToken, userId : {}", userId);
        Token token = tokenService.getClientToken(userId);
        logger.info("Client side token : {}", token);
        return ResponseEntity.ok(token);
    }

    @RequestMapping(value = "/updateChatUserRole", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> updateRole(@RequestBody ChatUser user) throws BadRequestException {
        if (user == null || user.getId() == null || user.getSet() == null)
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty request");

        logger.info("method : updateChatRole, user : {}", user);
        return vGroupChatService.updateStreamUserRole(user)
        .thenApply(ResponseEntity::ok);
    }

    @RequestMapping(value = "/blockOrUnblockStudents", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VGroupBasicResponse> blockStudent(@RequestBody BlockUnblockRequest blockUnblockRequest)
    {
        VGroupBasicResponse vGroupBasicResponse = vGroupChatService.blockOrUnblockStudents(blockUnblockRequest.getStudentIds(), blockUnblockRequest.getGroupName() ,blockUnblockRequest.isBlockRequest());
        logger.info("Block/Unblock response : {}", vGroupBasicResponse);
        return ResponseEntity.ok(vGroupBasicResponse);
    }

    @RequestMapping(value = "/sendMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> sendMessage(@RequestBody MessageRequest message) throws BadRequestException {

        logger.info("Send Message Request : {}", message);
        if (StringUtils.isEmpty(message.getUser_id()) || StringUtils.isEmpty(message.getGroupId()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty calling userId/groupId");

        message.setCallingUserRole(httpSessionUtils.getCallingUserRole());

        return vGroupChatService.sendMessage(message)
            .thenApply(response ->
            {
                logger.info("send message response : {}", response);
                return ResponseEntity.ok(response);
            });
    }

    @RequestMapping(value = "/sendAttachment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> sendAttachment(@Valid @RequestBody AttachmentRequest attachmentRequest) throws BadRequestException {
        if (StringUtils.isEmpty(attachmentRequest.getUserId()) || StringUtils.isEmpty(attachmentRequest.getGroupId()) || attachmentRequest.getFileUrls().isEmpty())
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty calling userId/groupId/fileUrl");


        logger.info("Attachment request :  {}", attachmentRequest);

        attachmentRequest.setCallingUserRole(httpSessionUtils.getCallingUserRole());

        return vGroupChatService.sendAttachment(attachmentRequest)
                .thenApply(response ->
                {
                    logger.info("send attachment response : {}", response);
                    return ResponseEntity.ok(response);
                });
    }

    @RequestMapping(value = "/deleteMessage/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public CompletableFuture<ResponseEntity<VGroupBasicResponse>> sendMessage(@PathVariable("id") String id) throws BadRequestException {

        logger.info("Delete messageId : {}", id);
        if (StringUtils.isEmpty(id))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty message id!");

        return vGroupChatService.deleteMessage(id)
                .thenApply(response ->
                {
                    logger.info("Delete message response : {}", response);
                    return ResponseEntity.ok(response);
                });
    }

    @RequestMapping(value = "/fetchCurrentVGroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VGroupFeedResponse> getVGroupFeed(@RequestParam("userId") String userId,
                                                            @RequestParam(value = "grade", required = false) String grade) throws BadRequestException {

        logger.info("method : getVGroupFeed, userId : {}, grade : {}", userId, grade);

        if (StringUtils.isEmpty(userId))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty userId");

        Role role = httpSessionUtils.getCallingUserRole();

        if (role.equals(Role.STUDENT) && StringUtils.isEmpty(grade))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty grade");

        VGroupFeedResponse vGroupFeedResponse = vGroupChatService.fetchCurrentVGroups(userId, grade);
        return ResponseEntity.ok(vGroupFeedResponse);
    }

    @RequestMapping(value = "/filterContacts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserContactList> filterUserContacts(@Valid @RequestBody UserContactList userContactList) throws BadRequestException {

        if (userContactList.getContactNumbers().isEmpty() || StringUtils.isEmpty(userContactList.getGroupId()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Empty contact list/groupId");

        return ResponseEntity.ok(vGroupChatService.filterContacts(userContactList));
    }

    @RequestMapping(value = "/triggerInvitationSms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VGroupBasicResponse> triggerInvitationSms(@RequestParam("phoneCode") String phoneCode,
                                                                    @RequestParam("phoneNumber") String phoneNumber,
                                                                    @RequestParam("groupName") String groupName,
                                                                    @RequestParam("deepLink") String deepLink)
    {
        logger.info("triggerSms request : {}, {}, {}, {}", phoneCode, phoneNumber, groupName, deepLink);
        vGroupChatService.triggerInvitationSMS(phoneCode, phoneNumber, groupName, deepLink);
        return ResponseEntity.ok(new VGroupBasicResponse("Sms triggered successfully", HttpStatus.OK));
    }

    @RequestMapping(value = "/registerUserInVGroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VGroupBasicResponse> registerInVGroups(@RequestParam("grade") String grade,
                                                                 @RequestParam("userId") String userId) throws BadRequestException {
        logger.info("method : registerUserInVGroups, userId : {}, grade : {}", userId, grade);
        if (StringUtils.isEmpty(grade) || StringUtils.isEmpty(userId))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid request");

        VGroupBasicResponse vGroupBasicResponse = vGroupChatService.registerUserInVGroups(userId, grade);
        return ResponseEntity.ok(vGroupBasicResponse);
    }

    @RequestMapping(value = "/broadcastMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VGroupBasicResponse> broadcastMessage(@Valid @RequestBody BroadcastRequest broadcastRequest) throws BadRequestException {
        logger.info("method : broadcastMessage, broadcast req : {}", broadcastRequest);
        if (broadcastRequest.getGroupNames().isEmpty() || StringUtils.isEmpty(broadcastRequest.getText()) || StringUtils.isEmpty(broadcastRequest.getUser_id()) || broadcastRequest.getGroupDeepLinks().isEmpty())
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid request");

        VGroupBasicResponse vGroupBasicResponse = vGroupChatService.broadcastMessage(broadcastRequest);
        return ResponseEntity.ok(vGroupBasicResponse);
    }

    @RequestMapping(value = "/uploadFileS3", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<VGroupBasicResponse> uploadFileS3(@RequestParam("file") MultipartFile multipartFile,
                                               @RequestParam("extension") String extension) throws BadRequestException {
        logger.info("method : uploadFileS3");
        VGroupBasicResponse vGroupBasicResponse = vGroupChatService.uploadFile(multipartFile, extension);
        logger.info("response : {}", vGroupBasicResponse);
        return ResponseEntity.ok(vGroupBasicResponse);
    }
}
