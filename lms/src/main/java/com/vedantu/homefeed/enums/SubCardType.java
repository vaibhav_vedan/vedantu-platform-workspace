package com.vedantu.homefeed.enums;

public enum SubCardType {
    V_QUIZ("V_QUIZ"),
    V_GROUP("V_GROUP");


    private String value;

    public String getValue() {
        return value;
    }

    SubCardType(String cardType) {
        this.value = cardType;
    }

    public static SubCardType getCardTypeForString(String card) {
        for (SubCardType cardType : SubCardType.values()) {
            if (cardType.value.equals(card)) {
                return cardType;
            }
        }
        throw new IllegalArgumentException();
    }
}
