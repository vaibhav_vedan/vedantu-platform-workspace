package com.vedantu.homefeed.enums;

public enum PublishType {
    PUBLISHED,
    NON_PUBLISHED,
    ALL
}