package com.vedantu.homefeed.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.homefeed.entity.LiveQuizSchedule;
import com.vedantu.homefeed.enums.CardType;
import com.vedantu.homefeed.enums.PublishType;
import com.vedantu.homefeed.request.HFCardForMultipleGrades;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.homefeed.response.HomeFeedCardOrderList;
import com.vedantu.homefeed.response.LiveQuizScheduleRes;
import com.vedantu.homefeed.service.HomeFeedService;
import com.vedantu.util.*;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

import static com.vedantu.User.Role.ADMIN;
import static com.vedantu.User.Role.STUDENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping("/homefeed")
public class HomeFeedController {
	
	@Autowired
	private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;
	@SuppressWarnings("static-access")
	private Logger LOGGER = logFactory.getLogger(HomeFeedController.class);

    
    private HttpSessionUtils sessionUtils;
    private FosUtils fosUtils;
    private HomeFeedService homeFeedService;

    @Autowired
    public HomeFeedController(HttpSessionUtils sessionUtils,
                              FosUtils fosUtils, HomeFeedService homeFeedService) {
        this.sessionUtils = sessionUtils;
        this.fosUtils = fosUtils;
        this.homeFeedService = homeFeedService;
    }

    @RequestMapping(value = "/getHomeFeedForUser", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<HomeFeedCard> getHomeFeedForUser(@RequestParam(name = "appVersionCode", required = false) String appVersionCode, @RequestParam(name= "requestSource", required = false) RequestSource requestSource, @RequestParam(name = "restrict", required = false) boolean restrict) throws VException {

        if (StringUtils.isEmpty(appVersionCode))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Null/Empty appVersionCode");

        Long userId = sessionUtils.getCallingUserId();
        LOGGER.info("method=getHomeFeedForUser, userId={} inside HomeFeedController", userId);
        sessionUtils.checkIfAllowed(userId, STUDENT, true);
        User ubi = fosUtils.getUserInfo(userId, false);
        UserBasicInfo user = fosUtils.getUserBasicInfo(userId, false);
        user.setCreationTime(ubi.getCreationTime());
        if(ubi.getStudentInfo() != null && StringUtils.isNotEmpty(ubi.getStudentInfo().getSchool())) {
            user.setSchool(ubi.getStudentInfo().getSchool());
        }
        LOGGER.info("Fetched user Info for user={}", userId);
        return homeFeedService.getHomeFeedResponseForUser(user, appVersionCode, requestSource, restrict);
    }

    @RequestMapping(value = "/getHomeFeedForWeb/{grade}", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<HomeFeedCard> getHomeFeedForWeb(@PathVariable("grade") String grade) throws VException {
        LOGGER.info("method=getHomeFeedForWeb,  inside HomeFeedController for grade : {}", grade);
        UserBasicInfo userBasicInfo = new UserBasicInfo();
        if (grade == null)
            grade = "10";
        //Using a dummy userId
        Long dummyUserId = 12345678L;
        userBasicInfo.setUserId(dummyUserId);
        userBasicInfo.setGrade(grade);
        Boolean restrict = true;
        if(Objects.nonNull(sessionUtils.getCurrentSessionData())){
            restrict = false;
        }
        return homeFeedService.getHomeFeedForWeb(userBasicInfo, grade, restrict);
    }

    @RequestMapping(value = "/getHomeFeedForWeb", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<HomeFeedCard> getHomeFeedForWeb(@RequestParam(value = "grades", required = true) Set<String> grades) throws VException {
        LOGGER.info("method=getHomeFeedForWeb,  inside HomeFeedController for grades [] : {}", grades);
        if (CollectionUtils.isEmpty(grades))
            grades = new HashSet<>(Arrays.asList("10"));

        Boolean restrict = true;
        if(Objects.nonNull(sessionUtils.getCurrentSessionData())){
            restrict = false;
        }
        return homeFeedService.getHomeFeedForWeb(grades, restrict);
    }

    @RequestMapping(value = "/configureHomeFeed", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HomeFeedCardOrderList configureHomeFeed(@RequestBody HomeFeedCardOrderList homeFeedCardOrderList) throws VException {
        LOGGER.info("method=configureHomeFeed for grade={}, inside HomeFeedController", homeFeedCardOrderList.getGrade());
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        return homeFeedService.configureHomeFeed(homeFeedCardOrderList);
    }

    @RequestMapping(value = "/fetchCurrentOrdering/{grade}", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HomeFeedCardOrderList fetchCurrentOrdering(@PathVariable(value = "grade") String grade,
                                                      @RequestParam(value = "publishType") PublishType publishType) throws VException {
        LOGGER.info("method=fetchCurrentOrdering for grade={}, publishType={} inside HomeFeedController", grade, publishType);
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), STUDENT, true);
        return homeFeedService.fetchCurrentOrdering(grade, publishType);
    }

    @RequestMapping(value = "addHomeFeedCardToGrades", method = POST)
    public PlatformBasicResponse addHomeFeedCardToGrades(@RequestBody HFCardForMultipleGrades hfCardForMultipleGrades) throws VException {
        LOGGER.info("method=addHomeFeedCardToGrades, cardType={} for grades={}",
                hfCardForMultipleGrades.getHomeFeedConfigurationCard().getCardType(), hfCardForMultipleGrades.getGrades());
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        return homeFeedService.addHomeFeedCardToGrades(hfCardForMultipleGrades);
    }

    @RequestMapping(value = "/checkIfAllowedToPublish/{grade}/{cardType}", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse  checkIfAllowedToPublish(@PathVariable("grade") String grade, @PathVariable("cardType") CardType cardType)
    {
        LOGGER.info("method=checkIfAllowedToPublish for grade={}, cardType={} inside HomeFeedController", grade, cardType);
        boolean allowed = false;
        if (cardType != null && StringUtils.isNotEmpty(grade))
            allowed = homeFeedService.allowedToPublish(grade, cardType);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setSuccess(allowed);
        return platformBasicResponse;
    }

    @RequestMapping(value = "/updateLiveQuizSchedule", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateLiveQuizSchedule(@RequestBody List<LiveQuizSchedule> liveQuizScheduleList) throws VException
    {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        homeFeedService.updateLiveQuizScheduleService(liveQuizScheduleList);
    }

    @RequestMapping(value = "/getTodaysLiveQuizSchedule", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public LiveQuizScheduleRes getTodaysLiveQuizSchedule() throws VException
    {
        httpSessionUtils.checkIfAllowed(null, STUDENT, Boolean.TRUE);
        return homeFeedService.getTodaysLiveQuizScheduleService();
    }

}
