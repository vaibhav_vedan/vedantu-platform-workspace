package com.vedantu.homefeed.dao;

import com.vedantu.doubts.dao.CounterService;
import com.vedantu.homefeed.entity.HomeFeedCardOrderEntity;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.entity.LiveQuizSchedule;
import com.vedantu.homefeed.enums.CardType;
import com.vedantu.moodle.dao.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class HomeFeedDao extends AbstractMongoDAO {

    private Logger LOGGER = LogFactory.getLogger(HomeFeedDao.class);
    private MongoClientFactory mongoClientFactory;
    private CounterService counterService;

    @Autowired
    public HomeFeedDao(MongoClientFactory mongoClientFactory,
                       CounterService counterService) {
        this.mongoClientFactory = mongoClientFactory;
        this.counterService = counterService;
    }

    public void save(HomeFeedCardOrderEntity cardOrderEntity) {
        LOGGER.info("Saving new HomeFeedCardOrderEntity for grade={}", cardOrderEntity.getGrade());
        cardOrderEntity.getHomeFeedConfigurationCards()
                .forEach(e -> e.setId(counterService.getNextSequentialSequence(HomeFeedConfigurationCard.class.getName())));
        saveEntity(cardOrderEntity);
    }

    public HomeFeedCardOrderEntity getOrderConfigurationByGrade(String grade) {
        LOGGER.info("Getting Home Feed Card ordering configuration for grade={}", grade);
        Query query = new Query();
        query.addCriteria(Criteria.where(HomeFeedCardOrderEntity.Constants.GRADE).is(grade));
        return findOne(query, HomeFeedCardOrderEntity.class);
    }

    public List<HomeFeedCardOrderEntity> getOrderConfigurationByGrades(Set<String> grades) {
        LOGGER.info("Getting Home Feed Card ordering configuration for grades={}", grades);
        Query query = new Query();
        query.addCriteria(Criteria.where(HomeFeedCardOrderEntity.Constants.GRADE).in(grades));
        LOGGER.info("Query : {}", query);
        return runQuery(query, HomeFeedCardOrderEntity.class);
    }

    public int getPublishedCardCount(String grade, CardType cardType)
    {
        LOGGER.info("Getting published card count for grade : {}, card : {}", grade, cardType);
        Query query = new Query();
        query.addCriteria(Criteria.where(HomeFeedCardOrderEntity.Constants.GRADE).is(grade));
        query.fields().include(HomeFeedCardOrderEntity.Constants.HOME_FEED_CONFIGURATION_CARDS);
        LOGGER.info("Query : {}", query);
        int count = 0;
        HomeFeedCardOrderEntity homeFeedCardOrderEntity = findOne(query, HomeFeedCardOrderEntity.class);
        LOGGER.info("HomeFeedCardOrderEntity fetched : {}", homeFeedCardOrderEntity);
        if (homeFeedCardOrderEntity != null) {
            for (HomeFeedConfigurationCard homeFeedConfigurationCard : homeFeedCardOrderEntity.getHomeFeedConfigurationCards()) {
                if (homeFeedConfigurationCard.getCardType().equals(cardType) && homeFeedConfigurationCard.isPublished())
                    count++;
            }
        }
        LOGGER.info("Publish Count : " + count);
        return count;
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void insertAllLiveQuizScheduleDao(List<LiveQuizSchedule> liveQuizScheduleList){
        super.insertAllEntities(liveQuizScheduleList,"LiveQuizSchedule");
    }
    public void removeAllLiveQuizScheduleDao(){
        Query query = new Query();

        super.deleteEntities(query,LiveQuizSchedule.class);
    }

    public LiveQuizSchedule getTodaysLiveQuizScheduleDao(Long time){
        Query query = new Query();
        query.addCriteria(Criteria.where(LiveQuizSchedule.Constants.TIME_TO_ACTIVATE).gte(time))
                .with(Sort.by( Sort.Direction.ASC,LiveQuizSchedule.Constants.TIME_TO_ACTIVATE));
        query.fields().include( LiveQuizSchedule.Constants.DAY );
        query.fields().include( LiveQuizSchedule.Constants.TIME_TO_ACTIVATE );
        query.fields().include( LiveQuizSchedule.Constants.MONEY );
        query.fields().include( LiveQuizSchedule.Constants.THEME );
        query.fields().include( LiveQuizSchedule.Constants.TIME_FORMAT );
        return findOne(query,LiveQuizSchedule.class);
    }


}
