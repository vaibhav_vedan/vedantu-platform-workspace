package com.vedantu.homefeed.service;

import com.google.common.base.Throwables;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.homefeed.converters.HomeFeedCardOrderToEntityConverter;
import com.vedantu.homefeed.converters.HomeFeedEntityToResponseConverter;
import com.vedantu.homefeed.dao.HomeFeedDao;
import com.vedantu.homefeed.entity.FindWebinarsReq;
import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.homefeed.entity.HomeFeedCardOrderEntity;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.entity.LiveQuizSchedule;
import com.vedantu.homefeed.entity.WebinarResponse;
import com.vedantu.homefeed.entity.WebinarStatus;
import com.vedantu.homefeed.enums.CardType;
import com.vedantu.homefeed.enums.PublishType;
import com.vedantu.homefeed.handlers.HomeFeedCardHandler;
import com.vedantu.homefeed.handlers.HomeFeedCardHandlerFactory;
import com.vedantu.homefeed.request.HFCardForMultipleGrades;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.homefeed.response.HomeFeedCardOrderList;
import com.vedantu.homefeed.response.LiveQuizScheduleRes;
import com.vedantu.homefeed.utils.HomeFeedRedisService;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.RequestSource;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.vedantu.homefeed.enums.CardType.*;
import static com.vedantu.homefeed.enums.PublishType.PUBLISHED;
import static com.vedantu.util.StringUtils.isNotEmpty;

@Service
public class HomeFeedServiceImpl implements HomeFeedService {
	
	@Autowired
    private RedisDAO redisDAO;
	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
    private Logger LOGGER = LogFactory.getLogger(HomeFeedServiceImpl.class);

   
    private HomeFeedCardOrderToEntityConverter toEntityConverter;
    private HomeFeedDao homeFeedDao;
    private HomeFeedEntityToResponseConverter toResponseConverter;
    private HomeFeedCardHandlerFactory handlerFactory;
    private HomeFeedRedisService homeFeedRedisService;
    //private static String CHURNED_USERS = ConfigUtils.INSTANCE.getStringValue("doubt.churned.users");  
    private static final String APP_CONFIG_CARDS_ORDER_FOR_GRADE = "APP_CONFIG_CARDS_ORDER_FOR_GRADE_";
    private static final String HOMEFEED_MICROCOURSES_FOR_GRADE = "HOMEFEED_MICROCOURSES_";
    private static final String HOMEFEED_MCR_COLLECTION_FOR_GRADE = "HOMEFEED_MICROCOURSE_COLLECTION_BY_GRADE_";
    private static final String NCERT_SOLUTIONS_GRADE = "NCERT_SOLUTIONS_GRADE_";
    private static final String GROWTH_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT");

    @Autowired
    public HomeFeedServiceImpl(HomeFeedCardOrderToEntityConverter toEntityConverter, HomeFeedDao homeFeedDao,
                               HomeFeedEntityToResponseConverter toResponseConverter,
                               HomeFeedCardHandlerFactory handlerFactory,
                               HomeFeedRedisService homeFeedRedisService) {
        this.toEntityConverter = toEntityConverter;
        this.homeFeedDao = homeFeedDao;
        this.toResponseConverter = toResponseConverter;
        this.handlerFactory = handlerFactory;
        this.homeFeedRedisService = homeFeedRedisService;
    }

    @Override
    public List<HomeFeedCard> getHomeFeedResponseForUser(UserBasicInfo user, String appVersionCode, RequestSource requestSource, boolean restrict) throws VException {
        LOGGER.info("method=getHomeFeedResponseForUser, userId={}", user.getUserId());
        String grade = user.getGrade();
        List<HomeFeedConfigurationCard> configCards = fetchCurrentOrdering(grade, PUBLISHED).getHomeFeedConfigurationCards();
        List<CardType> cardTypes = configCards.stream()
                .map(configCard -> CardType.getCardTypeForString(configCard.getCardType().getValue()))
                .collect(Collectors.toList());
        LOGGER.info("CardTypes requested by user={}, cardTypes={}", user.getUserId(), cardTypes);
        List<HomeFeedCard> homeFeedCardList = new ArrayList<>();
     // The list of churned users is saved on 22/01/2020 in properties file if redis key gets reset by any chance
    //	if (StringUtils.isEmpty(redisDAO.get("CHURNED_USERS")))
	//		redisDAO.set("CHURNED_USERS", CHURNED_USERS);
		String churnedUsers = redisDAO.get("CHURNED_USERS");
		

        if(requestSource != null && !RequestSource.IOS.equals(requestSource)) {
        	if(StringUtils.isNotEmpty(churnedUsers)){
        		if(!churnedUsers.contains(String.valueOf(user.getUserId()))){
           		 handlerFactory.getHandler(DOUBT).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
        	}
        	}  else {
        		handlerFactory.getHandler(DOUBT).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
        	}
        }

        String VERSION2 = "1.3.6";

        if (appVersionCode.compareTo(VERSION2) >= 0)
        {
        		handlerFactory.getHandler(TRIAL_BANNER).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);

                handlerFactory.getHandler(VSTORIES).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
        }

//        if(StringUtils.isNotEmpty(grade) && (grade.equals("10"))) {
//            handlerFactory.getHandler(SUBJECT_ENTRY).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
//        }

        if(appVersionCode.compareTo(VERSION2) < 0) {
            handlerFactory.getHandler(RECENT_ACTIVITY).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
        } else {
            handlerFactory.getHandler(UPCOMING_WEBINARS).handle(configCards.get(1), homeFeedCardList, user, appVersionCode, restrict);
            HomeFeedConfigurationCard masterTalkConfig = new HomeFeedConfigurationCard();
            masterTalkConfig.setCardType(SINGLE_BANNER);
            masterTalkConfig.setSectionTitle("MasterTalks");
            masterTalkConfig.setPublished(true);
            masterTalkConfig.setContextUrls(new String[]{"https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/Banner.jpg"});
            masterTalkConfig.setRedirectDeepLinkList(new String[]{ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT")+"mastertalks/mental-health-wellness-tips-by-deepika-padukone"});
//            masterTalkConfig.setRedirectDeepLinkList(new String[]{"https://fos-qa3.vedantu.com/mastertalks/mental-health-wellness-tips-by-deepika-padukone"});
            handlerFactory.getHandler(SINGLE_BANNER).handle(masterTalkConfig, homeFeedCardList, user, appVersionCode, restrict);
            handlerFactory.getHandler(NEW_RELEASE).handle(configCards.get(1), homeFeedCardList, user, appVersionCode, restrict);
        }

        int count = 0;
        for (HomeFeedConfigurationCard configCard : configCards) {
            if(count < 20) {
                CardType cardType = configCard.getCardType();
                if(appVersionCode.compareTo(VERSION2) >= 0) {
                    if(CardType.MICROCOURSES.equals(cardType) || CardType.NCERT_SOLUTIONS.equals(cardType) || CardType.MICROCOURSE_COLLECTION.equals(cardType) || CardType.BANNER_LIST.equals(cardType)) {
                        HomeFeedCardHandler cardHandler = handlerFactory.getHandler(configCard.getCardType());
                        cardHandler.handle(configCard, homeFeedCardList, user, appVersionCode, restrict);
                        count++;
                    }
                } else {
                    if(!CardType.MICROCOURSES.equals(cardType) && !CardType.NCERT_SOLUTIONS.equals(cardType) && !CardType.MICROCOURSE_COLLECTION.equals(cardType) && !CardType.BANNER_LIST.equals(cardType)) {
                    	HomeFeedCardHandler cardHandler = handlerFactory.getHandler(configCard.getCardType());
                        cardHandler.handle(configCard, homeFeedCardList, user, appVersionCode, restrict);
                        count++;
                    }
                }
            } else {
                break;
            }
        }

        if (appVersionCode.compareTo(VERSION2) < 0)
            handlerFactory.getHandler(SCHOOL_INFO).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
        return homeFeedCardList;
    }

    @Override
    public HomeFeedCardOrderList configureHomeFeed(HomeFeedCardOrderList homeFeedCardOrderList) {
    	StringBuilder key = new StringBuilder(APP_CONFIG_CARDS_ORDER_FOR_GRADE);
        String grade = homeFeedCardOrderList.getGrade();
        StringBuilder keyForMCR = new StringBuilder(HOMEFEED_MICROCOURSES_FOR_GRADE);
        StringBuilder keyForMCRCollection = new StringBuilder(HOMEFEED_MCR_COLLECTION_FOR_GRADE);
        StringBuilder keyForNCERTSolutions = new StringBuilder(NCERT_SOLUTIONS_GRADE);
        key.append(grade);
        keyForMCR.append(grade);
        keyForMCRCollection.append(grade);
        keyForNCERTSolutions.append(grade);
        LOGGER.info("method=configureHomeFeed, for grade={} inside HomeFeedServiceImpl. homeFeedCardOrderList={}", grade, homeFeedCardOrderList);
        HomeFeedCardOrderEntity cardOrderEntityInDb = homeFeedDao.getOrderConfigurationByGrade(grade);
        HomeFeedCardOrderEntity homeFeedCardOrderEntity = toEntityConverter.convert(homeFeedCardOrderList);
        setIdIfEntityPresentInDb(cardOrderEntityInDb, homeFeedCardOrderEntity);
        homeFeedDao.save(homeFeedCardOrderEntity);
        List<CardType> cards = new ArrayList<CardType>();
        cards.addAll(Optional.of(homeFeedCardOrderEntity.getHomeFeedConfigurationCards()).orElseGet(ArrayList::new)
                .stream()
                .map(HomeFeedConfigurationCard::getCardType)
                .collect(Collectors.toList()));
        try {
        	LOGGER.debug("Redis key for deletion : "+key.toString());
        	redisDAO.del(key.toString());
            LOGGER.info("Successfully deleted key={} for grade={}", key.toString(), grade);
        } catch (InternalServerErrorException|BadRequestException e) {
           LOGGER.error("Could not delete Redis key={}, grade={} Exception={}",  key.toString(), grade, e.getMessage());
        }
        
        if(ArrayUtils.isNotEmpty(cards) && cards.contains(CardType.MICROCOURSES)){
       	 try {
            	LOGGER.debug("Redis key for deletion : "+keyForMCR.toString());
            	redisDAO.del(keyForMCR.toString());
                LOGGER.info("Successfully deleted key={} for grade={}", keyForMCR.toString(), grade);
       		} catch (InternalServerErrorException|BadRequestException e) {
       	           LOGGER.error("Could not delete Redis key={}, grade={} Exception={}",  keyForMCR.toString(), grade, e.getMessage());
               }
       }
        if(ArrayUtils.isNotEmpty(cards) && cards.contains(CardType.MICROCOURSE_COLLECTION)){
          	 try {
               	LOGGER.debug("Redis key for deletion : "+keyForMCRCollection.toString());
               	redisDAO.del(keyForMCRCollection.toString());
                LOGGER.info("Successfully deleted key={} for grade={}", keyForMCRCollection.toString(), grade);
          		} catch (InternalServerErrorException|BadRequestException e) {
          	           LOGGER.error("Could not delete Redis key={}, grade={} Exception={}",  keyForMCRCollection.toString(), grade, e.getMessage());
                  }
          }
        if(ArrayUtils.isNotEmpty(cards) && cards.contains(CardType.NCERT_SOLUTIONS)){
          	 try {
               	LOGGER.debug("Redis key for deletion : "+keyForNCERTSolutions.toString());
               	redisDAO.del(keyForNCERTSolutions.toString());
                   LOGGER.info("Successfully deleted key={} for grade={}", keyForNCERTSolutions.toString(), grade);
          		} catch (InternalServerErrorException|BadRequestException e) {
          	           LOGGER.error("Could not delete Redis key={}, grade={} Exception={}",  keyForNCERTSolutions.toString(), grade, e.getMessage());
                  }
          }
        
        LOGGER.debug("Returning new home feed card ordering configuration for grade={}", grade);
        return toResponseConverter.convert(homeFeedDao.getOrderConfigurationByGrade(grade));
    }

    private void setIdIfEntityPresentInDb(HomeFeedCardOrderEntity cardOrderEntityInDb, HomeFeedCardOrderEntity homeFeedCardOrderEntity) {
        if (cardOrderEntityInDb != null && isNotEmpty(cardOrderEntityInDb.getId())) {
            homeFeedCardOrderEntity.setId(cardOrderEntityInDb.getId());
        }
    }

    @Override
    public HomeFeedCardOrderList fetchCurrentOrdering(String grade, PublishType publishType) {
        LOGGER.info("method=fetchCurrentOrdering, for grade={}, publishType={}, getting data from database.", grade, publishType);
        HomeFeedCardOrderEntity cardOrderEntity = homeFeedDao.getOrderConfigurationByGrade(grade);
        if (cardOrderEntity == null) {
            LOGGER.info("No data found for grade={}", grade);
            return getEmptyHomeFeedCardOrderList(grade);
        }
        cardOrderEntity.setHomeFeedConfigurationCards(getFilteredCardsOnPublishType(cardOrderEntity, publishType));
        LOGGER.info("Successfully fetched card order list from database, returning card order response.");
        return toResponseConverter.convert(cardOrderEntity);
    }

    @Override
    public List<HomeFeedCardOrderList> fetchCurrentOrdering(Set<String> grades, PublishType publishType) {
        LOGGER.info("method=fetchCurrentOrdering, for grade={}, publishType={}, getting data from database.", grades, publishType);
        List<HomeFeedCardOrderEntity> cardOrderEntities = homeFeedDao.getOrderConfigurationByGrades(grades);
        if (CollectionUtils.isEmpty(cardOrderEntities)) {
            LOGGER.info("No data found for grades = {}", grades);
            return getEmptyHomeFeedCardOrderList(grades);
        }
        cardOrderEntities.forEach(cardOrderEntity -> cardOrderEntity.setHomeFeedConfigurationCards(getFilteredCardsOnPublishType(cardOrderEntity, publishType)));
        LOGGER.info("Successfully fetched card order list from database for grades, returning card order response.");
        return cardOrderEntities.stream().map(cardOrderEntity -> toResponseConverter.convert(cardOrderEntity)).collect(Collectors.toList());
    }

    @Override
    public PlatformBasicResponse addHomeFeedCardToGrades(HFCardForMultipleGrades hfCardForMultipleGrades) {
        LOGGER.info("method=addHomeFeedCardToGrades, class=HomeFeedServiceImpl");
        for (String grade : hfCardForMultipleGrades.getGrades()) {
            HomeFeedCardOrderEntity configurationOrder = homeFeedDao.getOrderConfigurationByGrade(grade);
            if (configurationOrder != null) {
                List<HomeFeedConfigurationCard> homeFeedConfigurationCards = configurationOrder.getHomeFeedConfigurationCards();
                homeFeedConfigurationCards.add(hfCardForMultipleGrades.getHomeFeedConfigurationCard());
                configurationOrder.setHomeFeedConfigurationCards(homeFeedConfigurationCards);
                LOGGER.info("Adding HomeFeed configurationCard={}, for grade={}", hfCardForMultipleGrades.getHomeFeedConfigurationCard(), grade);
                homeFeedDao.save(configurationOrder);
                StringBuilder key = new StringBuilder(APP_CONFIG_CARDS_ORDER_FOR_GRADE);
                key.append(grade);
                StringBuilder keyForMCR = new StringBuilder(HOMEFEED_MICROCOURSES_FOR_GRADE);
                StringBuilder keyForMCRCollection = new StringBuilder(HOMEFEED_MCR_COLLECTION_FOR_GRADE);
                key.append(grade);
                keyForMCR.append(grade);
                keyForMCRCollection.append(grade);
                List<CardType> cards = new ArrayList<CardType>();
                cards.addAll(Optional.of(configurationOrder.getHomeFeedConfigurationCards()).orElseGet(ArrayList::new)
                        .stream()
                        .map(HomeFeedConfigurationCard::getCardType)
                        .collect(Collectors.toList()));
                try {
                	LOGGER.info("Redis key for deletion : "+key.toString());
                	redisDAO.del(key.toString());
                    LOGGER.info("Successfully deleted key={} for grade={}", key.toString(), grade);
                } catch (InternalServerErrorException|BadRequestException e) {
                   LOGGER.error("Could not delete Redis key={}, grade={} Exception={}",  key.toString(), grade, e.getMessage());
                }
                if(ArrayUtils.isNotEmpty(cards) && cards.contains(CardType.MICROCOURSES)){
                  	 try {
                       	LOGGER.info("Redis key for deletion : "+keyForMCR.toString());
                       	redisDAO.del(keyForMCR.toString());
                           LOGGER.info("Successfully deleted key={} for grade={}", keyForMCR.toString(), grade);
                  		} catch (InternalServerErrorException|BadRequestException e) {
                  	           LOGGER.error("Could not delete Redis key={}, grade={} Exception={}",  keyForMCR.toString(), grade, e.getMessage());
                          }
                  }
                   if(ArrayUtils.isNotEmpty(cards) && cards.contains(CardType.MICROCOURSE_COLLECTION)){
                     	 try {
                          	LOGGER.info("Redis key for deletion : "+keyForMCRCollection.toString());
                          	redisDAO.del(keyForMCRCollection.toString());
                           LOGGER.info("Successfully deleted key={} for grade={}", keyForMCRCollection.toString(), grade);
                     		} catch (InternalServerErrorException|BadRequestException e) {
                     	           LOGGER.error("Could not delete Redis key={}, grade={} Exception={}",  keyForMCRCollection.toString(), grade, e.getMessage());
                             }
                     }
            } else {
                LOGGER.warn("No HomeFeed Configuration present for grade={}", grade);
            }
        }
        return new PlatformBasicResponse();
    }

    @Override
    public List<HomeFeedCard> getHomeFeedForWeb(UserBasicInfo user, String grade, boolean restrict) throws VException {
        LOGGER.info("method=getHomeFeedResponseForWeb, grade={}", grade);
        List<HomeFeedConfigurationCard> configCards = fetchCurrentOrdering(grade, PUBLISHED).getHomeFeedConfigurationCards();
        List<CardType> cardTypes = configCards.stream()
                .map(configCard -> CardType.getCardTypeForString(configCard.getCardType().getValue()))
                .collect(Collectors.toList());
        LOGGER.info("CardTypes requested for grade : {}, cardTypes={}", grade, cardTypes);
        List<HomeFeedCard> homeFeedCardList = new ArrayList<>();

        HomeFeedCardHandler cardHandler = handlerFactory.getHandler(UPCOMING_WEBINARS);
        cardHandler.handle(configCards.get(0), homeFeedCardList, user, null, restrict);

        for (HomeFeedConfigurationCard configCard : configCards)
        {
            CardType cardType = configCard.getCardType();
            if (!cardType.equals(MICROCOURSES) && !cardType.equals(MICROCOURSE_COLLECTION))
                continue;
            cardHandler = handlerFactory.getHandler(configCard.getCardType());
            cardHandler.handle(configCard, homeFeedCardList, user, "WEB", restrict);
        }
        return homeFeedCardList;
    }

    @Override
    public List<HomeFeedCard> getHomeFeedForWeb(Set<String> grades, boolean restrict) throws VException {
        LOGGER.info("method=getHomeFeedResponseForWeb, grade={}", grades);

        List<HomeFeedCardOrderList> homeFeedCardOrderLists = fetchCurrentOrdering(grades, PUBLISHED);
        List<CardType> cardTypes = new ArrayList<>();
        homeFeedCardOrderLists.stream().
                map(homeFeedCardOrderList -> homeFeedCardOrderList.getHomeFeedConfigurationCards()).
                forEach(configCards -> {
                    cardTypes.addAll(configCards.stream().
                            filter(Objects::nonNull)
                            .map(configCard -> CardType.getCardTypeForString(configCard.getCardType().getValue()))
                            .collect(Collectors.toList()));
                });
        LOGGER.info("CardTypes requested for grades : {}, cardTypes={}", grades, cardTypes);

        List<HomeFeedCard> homeFeedCardListResponse = new ArrayList<>();

        Map<CardType,Object> cardGroupMap = new HashMap<>();

        getHomeFeedCardsUpcomingWebinars(grades, restrict, homeFeedCardOrderLists, cardGroupMap);

        homeFeedCardOrderLists.
                stream().
                filter(cardOrderList -> Objects.nonNull(cardOrderList)
                        && CollectionUtils.isNotEmpty(cardOrderList.getHomeFeedConfigurationCards())).
                forEach(cardOrderList -> {
                    List<HomeFeedConfigurationCard> configCards = cardOrderList.getHomeFeedConfigurationCards();

                    if(CollectionUtils.isNotEmpty(configCards)) {
                        UserBasicInfo user = getDummyUserInfo(cardOrderList.getGrade());
                        List<HomeFeedCard> homeFeedCardList = new ArrayList<>();

                        try {

                            for (HomeFeedConfigurationCard configCard : configCards) {
                                CardType cardType = configCard.getCardType();
                                if (!cardType.equals(MICROCOURSES) && !cardType.equals(MICROCOURSE_COLLECTION))
                                    continue;
                                HomeFeedCardHandler cardHandler = handlerFactory.getHandler(configCard.getCardType());
                                cardHandler.handle(configCard, homeFeedCardList, user, "WEB", restrict);
                            }

                            homeFeedCardList.forEach(homeFeedCard -> {
                                if (cardGroupMap.containsKey(homeFeedCard.getCardType())) {
                                    Map<String, Object> cardMap = (Map<String, Object>) cardGroupMap.get(homeFeedCard.getCardType());
                                    List<HomeFeedCardData> homeFeedCardDataList = (List<HomeFeedCardData>) cardMap.get("homeFeedCardDataList");
                                    homeFeedCardDataList.addAll(homeFeedCard.getHomeFeedCardDataList());
                                    cardMap.put("homeFeedCardDataList", homeFeedCardDataList);
                                } else {
                                    Map<String, Object> cardMap = new HashMap<>();
                                    cardMap.put("homeFeedCardDataList", homeFeedCard.getHomeFeedCardDataList());
                                    cardMap.put("serverTime", homeFeedCard.getServerTime());
                                    cardMap.put("sectionTitle", homeFeedCard.getSectionTitle());
                                    cardGroupMap.put(homeFeedCard.getCardType(), cardMap);
                                }
                            });

                        } catch (VException e) {
                            throw Throwables.propagate(e);
                        }
                    }
                });

        List<HomeFeedCard> homeFeedCardList = new ArrayList<>();

        for (Map.Entry<CardType,Object> entry : cardGroupMap.entrySet()){

            CardType cardType = entry.getKey();

            Map<String,Object> cardMap = (Map<String, Object>) entry.getValue();
            Long serverTime = (Long) cardMap.get("serverTime");
            String sectionTitle = (String) cardMap.get("sectionTitle");

            List< HomeFeedCardData> homeFeedCardDataList = (List<HomeFeedCardData>) cardMap.get("homeFeedCardDataList");
            HomeFeedCard homeFeedCard = HomeFeedCard.builder()
                    .cardType(cardType)
                    .serverTime(serverTime)
                    .homeFeedCardDataList(homeFeedCardDataList)
                    .sectionTitle(sectionTitle)
                    .build();
            homeFeedCardList.add(homeFeedCard);
        }

        homeFeedCardListResponse.addAll(homeFeedCardList);

        return homeFeedCardListResponse;
    }

    private List<HomeFeedCard> getHomeFeedCardsUpcomingWebinars(Set<String> grades, boolean restrict, List<HomeFeedCardOrderList> homeFeedCardOrderLists, Map<CardType, Object> cardGroupMap) {
        List<HomeFeedCard> homeFeedCardListUpcomingWebinars = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(homeFeedCardOrderLists) && CollectionUtils.isNotEmpty(homeFeedCardOrderLists.get(0).getHomeFeedConfigurationCards())) {
            List<HomeFeedConfigurationCard> configurationCards = homeFeedCardOrderLists.get(0).getHomeFeedConfigurationCards();
            HomeFeedCard homeFeedCard = HomeFeedCard.builder()
                    .cardType(UPCOMING_WEBINARS)
                    .sectionTitle(configurationCards.get(0).getSectionTitle())
                    .serverTime(System.currentTimeMillis())
                    .build();

            try {
                FindWebinarsReq findWebinarsReq = new FindWebinarsReq();
                findWebinarsReq.setRestrict(restrict);
                findWebinarsReq.setHomefeedWeb(Boolean.TRUE);
                findWebinarsReq.setGrades(grades);
                findWebinarsReq.setWebinarStatus(WebinarStatus.ACTIVE);
                findWebinarsReq.setAppVersionCode(null);
                List<WebinarResponse> webinarResponses = getWebinarResponseFromGrowth(findWebinarsReq);
                webinarResponses = deduplicateSimLives(webinarResponses);
                LOGGER.info("Found {} webinars from platform", webinarResponses.size());
                homeFeedCard.setHomeFeedCardDataList(webinarResponses);
                LOGGER.info("Successfully set {} HomeFeedCardData  for WebinarHomeFeedCard", homeFeedCard.getHomeFeedCardDataList().size());
                homeFeedCardListUpcomingWebinars.add(homeFeedCard);

                Map<String, Object> cardMap = new HashMap<>();
                cardMap.put("homeFeedCardDataList", webinarResponses);
                cardMap.put("serverTime", homeFeedCard.getServerTime());
                cardMap.put("sectionTitle", homeFeedCard.getSectionTitle());
                cardGroupMap.put(UPCOMING_WEBINARS, cardMap);
            }
            catch (Exception e)
            {
                LOGGER.error("Could not get Webinar from Platform for grades: {} ", grades, e);
            }

        }

        return homeFeedCardListUpcomingWebinars;
    }

    //TODO: move this to utils and use in growth and lms
    private List<WebinarResponse> deduplicateSimLives(List<WebinarResponse> webinars) {

        if(CollectionUtils.isEmpty(webinars)){
            return new ArrayList<>();
        }
        Set<String> fetchedSimLiveParentIds = new HashSet<>();
        List<WebinarResponse> filteredWebinars = new ArrayList<>();
        webinars.forEach(webinar -> {
            if(StringUtils.isEmpty(webinar.getParentWebinarId()) || !fetchedSimLiveParentIds.contains(webinar.getParentWebinarId())){
                filteredWebinars.add(webinar);
                if(StringUtils.isNotEmpty(webinar.getParentWebinarId())) {
                    fetchedSimLiveParentIds.add(webinar.getParentWebinarId());
                }
            }
        });
        return filteredWebinars;
    }

    private List<WebinarResponse> getWebinarResponseFromGrowth(FindWebinarsReq findWebinarsReq) throws VException {

        LOGGER.info("method=getWebinarResponseFromPlatform, class=WebinarCardHandler, webinarReq={}", findWebinarsReq);
        String url = GROWTH_ENDPOINT + "/cms/webinar/findUpcomingWebinars";
        LOGGER.info("Calling Platform with the URL : {}", url);
        String query = new Gson().toJson(findWebinarsReq);
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String resString = response.getEntity(String.class);
        Type type = new TypeToken<List<WebinarResponse>>(){}.getType();
        return new Gson().fromJson(resString, type);

    }

    private UserBasicInfo getDummyUserInfo(String grade) {
        UserBasicInfo userBasicInfo = new UserBasicInfo();
        //Using a dummy userId
        Long dummyUserId = 12345678L;
        userBasicInfo.setUserId(dummyUserId);
        userBasicInfo.setGrade(grade);
        return userBasicInfo;
    }

    @Override
    public boolean allowedToPublish(String grade, CardType cardType)
    {
        LOGGER.info("method : allowedToPublish for grade : {}, cardType : {}", grade, cardType);
        if (cardType != null && !StringUtils.isEmpty(grade))
        {
            int cardPublishCount = homeFeedDao.getPublishedCardCount(grade, cardType);
            boolean allowed = false;
            if (cardType.equals(BANNER_LIST) && (cardPublishCount < 3))
                allowed = true;
            else if ((cardType.equals(MICROCOURSE_COLLECTION) || cardType.equals(MICROCOURSES) || cardType.equals(NCERT_SOLUTIONS)) && (cardPublishCount < 1))
                allowed = true;

            return allowed;
        }
        else
        {
            LOGGER.warn("Invalid request : grade {},  card : {}", grade, cardType);
            return false;
        }
    }

    private List<HomeFeedConfigurationCard> getFilteredCardsOnPublishType(HomeFeedCardOrderEntity cardOrderEntityFromDb, PublishType publishType) {
        List<HomeFeedConfigurationCard> cards = new ArrayList<>();
        switch (publishType) {
            case PUBLISHED:
                cardOrderEntityFromDb.getHomeFeedConfigurationCards()
                        .forEach(card -> addToListIfPublished(cards, card));
                break;
            case NON_PUBLISHED:
                cardOrderEntityFromDb.getHomeFeedConfigurationCards()
                        .forEach(card -> addToListIfNotPublished(cards, card));
                break;
            case ALL:
                return cardOrderEntityFromDb.getHomeFeedConfigurationCards();
            default:
                throw new IllegalStateException("Unexpected value: " + publishType);
        }
        return cards;
    }

    private void addToListIfNotPublished(List<HomeFeedConfigurationCard> cards, HomeFeedConfigurationCard card) {
        if (!card.isPublished()) {
            cards.add(card);
        }
    }

    private void addToListIfPublished(List<HomeFeedConfigurationCard> cards, HomeFeedConfigurationCard card) {
        if (card.isPublished()) {
            cards.add(card);
        }
    }

    private HomeFeedCardOrderList getEmptyHomeFeedCardOrderList(String grade) {
        return HomeFeedCardOrderList.builder()
                .grade(grade)
                .homeFeedConfigurationCards(Collections.emptyList())
                .build();
    }

    private List<HomeFeedCardOrderList> getEmptyHomeFeedCardOrderList(Set<String> grades) {
        return grades.stream().map(grade -> getEmptyHomeFeedCardOrderList(grade)).collect(Collectors.toList());
    }

    public void updateLiveQuizScheduleService(List<LiveQuizSchedule> liveQuizScheduleList){
        homeFeedDao.removeAllLiveQuizScheduleDao();
        homeFeedDao.insertAllLiveQuizScheduleDao(liveQuizScheduleList);

    }

    public LiveQuizScheduleRes getTodaysLiveQuizScheduleService() throws VException{
        LiveQuizSchedule liveQuizSchedule = homeFeedDao.getTodaysLiveQuizScheduleDao(System.currentTimeMillis() - (12 * 60 * 1000));
        if(liveQuizSchedule == null){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,"No data found for the request");
        }
        return new LiveQuizScheduleRes(liveQuizSchedule,System.currentTimeMillis());
    }
}
