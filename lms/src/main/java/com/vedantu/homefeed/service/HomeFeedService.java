package com.vedantu.homefeed.service;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.homefeed.entity.LiveQuizSchedule;
import com.vedantu.homefeed.enums.CardType;
import com.vedantu.homefeed.enums.PublishType;
import com.vedantu.homefeed.request.HFCardForMultipleGrades;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.homefeed.response.HomeFeedCardOrderList;
import com.vedantu.homefeed.response.LiveQuizScheduleRes;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.enums.RequestSource;

import java.util.List;
import java.util.Set;

public interface HomeFeedService {

    List<HomeFeedCard> getHomeFeedResponseForUser(UserBasicInfo user, String appVersionCode, RequestSource requestSource, boolean restrict) throws VException;

    HomeFeedCardOrderList configureHomeFeed(HomeFeedCardOrderList homeFeedCardOrderList) throws BadRequestException;

    HomeFeedCardOrderList fetchCurrentOrdering(String userId, PublishType publishType);

    List<HomeFeedCardOrderList> fetchCurrentOrdering(Set<String> grade, PublishType publishType);

    PlatformBasicResponse addHomeFeedCardToGrades(HFCardForMultipleGrades hfCardForMultipleGrades);

    List<HomeFeedCard> getHomeFeedForWeb(UserBasicInfo user, String grade, boolean restrict) throws VException;

    List<HomeFeedCard> getHomeFeedForWeb(Set<String> grades, boolean restrict) throws VException;

    boolean allowedToPublish(String grade, CardType cardType);

    void updateLiveQuizScheduleService(List<LiveQuizSchedule> liveQuizScheduleList);

    LiveQuizScheduleRes getTodaysLiveQuizScheduleService() throws VException;
}
