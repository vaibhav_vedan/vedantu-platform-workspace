package com.vedantu.homefeed.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicLongIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "LiveQuizSchedule", uniqueConstraints = {
        @UniqueConstraint(columnNames = {"day"})})
public class LiveQuizSchedule extends AbstractMongoStringIdEntity {
    @Indexed(background = true)
    @NotNull
    private Long timeToActivate;
    @NotNull
    private String day;
    @NotNull
    private Long money;
    @NotNull
    private String timeFormat;
    private String theme;

    public static class Constants extends AbstractTargetTopicLongIdEntity.Constants {
        public static final String TIME_TO_ACTIVATE = "timeToActivate";
        public static final String DAY = "day";
        public static final String MONEY = "money";
        public static final String THEME = "theme";
        public static final String TIME_FORMAT = "timeFormat";

    }
}

