package com.vedantu.homefeed.entity;

public enum WebinarType {
    LIVECLASS, QUIZ, WEBINAR
}
