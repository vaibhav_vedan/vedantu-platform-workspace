package com.vedantu.homefeed.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.List;
import java.util.Map;
import java.util.Set;

@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class WebinarResponse implements HomeFeedCardData {
    private String id;
    private String title;
    private Long startTime;
    private Long endTime;
    private Map<String, Object> teacherInfo;
    private Map<String, Object> webinarInfo;
    private List<String> grades;
    private Map<String, Object> courseInfo;
    private String webinarCode;
    private long totalParticipants;
    private Long creationTime;
    private String sessionId;
    private String subject;
    @Builder.Default
    private Boolean isReminderSet = false;
    private Set<String> subjects;
    private String masterClassUrl;
    private Long popUpDeplay;
    private String parentWebinarId;
}
