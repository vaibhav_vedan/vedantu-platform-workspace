/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.homefeed.entity;

/**
 *
 * @author jeet
 */
public enum WebinarToolType {
    GTW, YOUTUBE, VEDANTU_WAVE, VEDANTU_WAVE_BIG_WHITEBOARD, VEDANTU_WAVE_OTO
}
