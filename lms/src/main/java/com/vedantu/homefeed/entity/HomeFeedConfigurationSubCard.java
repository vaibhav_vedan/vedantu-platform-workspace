package com.vedantu.homefeed.entity;

import com.vedantu.homefeed.enums.SubCardType;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedConfigurationSubCard implements HomeFeedCardData{
    private String title;
    private SubCardType subCardType;
    private boolean isPublished;
}
