package com.vedantu.homefeed.entity;

import java.util.List;

import com.vedantu.subscription.pojo.bundle.AioPackage;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedBundleResponse implements HomeFeedCardData
{
    private String id;
    private String title;
    private List<String> teacherIds;
    private String teacherName;
    private String bundleImageUrl;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<AioPackage> packages;
    private boolean bought;
    private List<String> subjects;
}
