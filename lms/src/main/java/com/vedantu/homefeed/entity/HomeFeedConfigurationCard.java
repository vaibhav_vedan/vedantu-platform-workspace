package com.vedantu.homefeed.entity;

import com.vedantu.homefeed.enums.CardType;
import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedConfigurationCard {
    private Long id;
    private String title;
    private String description;
    private String contextId;
    private String[] contextUrls;
    private CardType cardType;
    private String subTitle;
    private String clickToAction;
    private String[] redirectDeepLinkList;
    private String sectionTitle;
    private String thumbnail;
    private boolean isPublished;
    private String[] thumbnailArray;
    private String[] thumbnailArrayWeb;
    private List<HomeFeedConfigurationSubCard> subCards;

}
