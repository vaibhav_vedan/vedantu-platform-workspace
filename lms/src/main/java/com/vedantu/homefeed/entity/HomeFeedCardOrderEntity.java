package com.vedantu.homefeed.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicLongIdEntity;
import lombok.*;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "HomeFeedCardOrderEntity")
public class HomeFeedCardOrderEntity extends AbstractMongoStringIdEntity {
    @Indexed(unique = true, background = true)
    private String grade;
    private List<HomeFeedConfigurationCard> homeFeedConfigurationCards;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<HomeFeedConfigurationCard> getHomeFeedConfigurationCards() {
        return homeFeedConfigurationCards;
    }

    public void setHomeFeedConfigurationCards(List<HomeFeedConfigurationCard> homeFeedConfigurationCards) {
        this.homeFeedConfigurationCards = homeFeedConfigurationCards;
    }

    public static class Constants extends AbstractTargetTopicLongIdEntity.Constants {
        public static final String GRADE = "grade";
        public static final String HOME_FEED_CONFIGURATION_CARDS = "homeFeedConfigurationCards";
    }
}
