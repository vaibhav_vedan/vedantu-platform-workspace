package com.vedantu.homefeed.entity;

import com.vedantu.platform.enums.SocialContextType;

import java.io.Serializable;

public class UserActivity implements Serializable {
    public static final long serialVersionUID = 101L;
    private SocialContextType socialContextType;
    private String contextId;
    private long lastUpdated;

    public UserActivity(SocialContextType socialContextType, String contextId, long lastUpdated) {
        this.socialContextType = socialContextType;
        this.contextId = contextId;
        this.lastUpdated = lastUpdated;
    }

    public UserActivity(SocialContextType socialContextType, String contextId) {
        this.socialContextType = socialContextType;
        this.contextId = contextId;
    }

    public SocialContextType getSocialContextType() {
        return socialContextType;
    }

    public void setSocialContextType(SocialContextType socialContextType) {
        this.socialContextType = socialContextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}

