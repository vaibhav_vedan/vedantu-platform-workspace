package com.vedantu.homefeed.request;

import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HFCardForMultipleGrades {
    private List<String> grades;
    private HomeFeedConfigurationCard homeFeedConfigurationCard;
}
