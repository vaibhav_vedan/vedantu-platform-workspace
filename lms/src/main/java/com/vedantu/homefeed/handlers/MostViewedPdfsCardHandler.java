package com.vedantu.homefeed.handlers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.managers.StudyManager;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component(value = "MOST_VIEWED_PDFS")
public class MostViewedPdfsCardHandler implements HomeFeedCardHandler {

    private final Logger LOGGER = LogFactory.getLogger(MostViewedPdfsCardHandler.class);
    private static final int PDF_LIMIT = 20;
    private StudyManager studyManager;

    @Autowired
    public MostViewedPdfsCardHandler(StudyManager studyManager) {
        this.studyManager = studyManager;
    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        String grade = user.getGrade();
        LOGGER.info("Getting top {} MostViewedPdfsCard for homefeed, grade={}", PDF_LIMIT, grade);
        try {
            HomeFeedCard homeFeedMostViewedPdfsCard = getHomeFeedCard(configurationCard);
            homeFeedMostViewedPdfsCard.setHomeFeedCardDataList(studyManager.getTopMostViewedPdfs(user.getGrade(), PDF_LIMIT));
            homeFeedCardList.add(homeFeedMostViewedPdfsCard);
        } catch (Exception e) {
            LOGGER.error("Error in setting MostViewedVideo Card for grade={}, userId={}", grade, user.getUserId());
        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .title(configurationCard.getTitle())
                .cardType(configurationCard.getCardType())
                .sectionTitle(configurationCard.getSectionTitle())
                .clickToAction(configurationCard.getClickToAction())
                .build();
    }
}
