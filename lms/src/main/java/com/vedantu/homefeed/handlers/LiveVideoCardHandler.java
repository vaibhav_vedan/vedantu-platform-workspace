package com.vedantu.homefeed.handlers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.managers.CMDSVideoManager;
import com.vedantu.cmds.request.GetCMDSVideoPlaylistReq;
import com.vedantu.exception.NotFoundException;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@Component(value = "LIVE_NOW")
public class LiveVideoCardHandler implements HomeFeedCardHandler {

    private final Logger LOGGER = LogFactory.getLogger(LiveVideoCardHandler.class);
    private CMDSVideoManager videoManager;

    @Autowired
    public LiveVideoCardHandler(CMDSVideoManager videoManager) {
        this.videoManager = videoManager;
    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        LOGGER.info("Setting up Live Video Card for Homefeed for grade={}", user.getGrade());
        HomeFeedCard liveVideoHomeFeedCard = getHomeFeedCard(configurationCard);
        try {
            List<CMDSVideo> cmdsVideos = videoManager.getLiveVideoForHomeFeed(getGetCMDSVideoPlaylistReq(user));
            liveVideoHomeFeedCard.setHomeFeedCardDataList(cmdsVideos);
            homeFeedCardList.add(liveVideoHomeFeedCard);
        } catch (NotFoundException e) {
//            LOGGER.error("No Live Video currently being broadcasted.");
        } catch (Exception e) {
//            LOGGER.error("Error in adding Live Video card for userId={}, grade={}.", user.getUserId(), user.getGrade());
        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }


    private GetCMDSVideoPlaylistReq getGetCMDSVideoPlaylistReq(UserBasicInfo user) {
        GetCMDSVideoPlaylistReq req = new GetCMDSVideoPlaylistReq();
        req.setMainTags(new HashSet<>(Arrays.asList(user.getGrade())));
        req.setPublished(Boolean.TRUE);
        return req;
    }
}
