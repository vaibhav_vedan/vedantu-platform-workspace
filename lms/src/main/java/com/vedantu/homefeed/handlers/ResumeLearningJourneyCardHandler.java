package com.vedantu.homefeed.handlers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("RESUME_LEARNING_JOURNEY")
public class ResumeLearningJourneyCardHandler implements HomeFeedCardHandler {
    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {

    }
}
