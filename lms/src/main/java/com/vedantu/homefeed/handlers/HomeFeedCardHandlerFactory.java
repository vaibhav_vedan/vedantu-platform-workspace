package com.vedantu.homefeed.handlers;

import com.vedantu.homefeed.enums.CardType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static com.vedantu.homefeed.enums.CardType.*;

@Component
public class HomeFeedCardHandlerFactory {

    @Autowired
    private Map<String, HomeFeedCardHandler> handlerMap;

    public HomeFeedCardHandler getHandler(CardType cardType) {
        return handlerMap.get(cardType.getValue());
    }
}
