package com.vedantu.homefeed.handlers;

import com.google.gson.Gson;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.managers.CMDSTestManager;
import com.vedantu.cmds.response.CMDSTestListRes;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.vedantu.homefeed.utils.RedisKeyConstants.HOMEFEED_TEST_SERIES_CARD_DATA;
import static com.vedantu.util.DateTimeUtils.SECONDS_PER_DAY;

@Component(value = "TEST_SERIES")
public class TestSeriesCardHandler implements HomeFeedCardHandler {

    private static final int TWO_DAYS = 2 * SECONDS_PER_DAY;
    private Logger LOGGER = LogFactory.getLogger(TestSeriesCardHandler.class);
    private CMDSTestManager testManager;
    private RedisDAO redisDAO;

    @Autowired
    public TestSeriesCardHandler(CMDSTestManager testManager, RedisDAO redisDAO) {
        this.testManager = testManager;
        this.redisDAO = redisDAO;
    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        String grade = user.getGrade();
        Gson gson = new Gson();
        LOGGER.info("Setting up TestSeriesCard for userId={}, grade={}", user.getUserId(), grade);
        HomeFeedCard testSeriesCard = getHomeFeedCard(configurationCard);
        String contextId = configurationCard.getContextId();
        String testSeriesRedisKey = HOMEFEED_TEST_SERIES_CARD_DATA + grade + "_" + contextId;
        try {
            String testSeriesCardString = redisDAO.get(testSeriesRedisKey);
            if (StringUtils.isNotEmpty(testSeriesCardString)) {
                CMDSTestListRes testSeriesCardData = gson.fromJson(testSeriesCardString, CMDSTestListRes.class);
                testSeriesCard.setHomeFeedCardData(testSeriesCardData);
                homeFeedCardList.add(testSeriesCard);
                LOGGER.info("Successfuly set HomeFeed TestSeriesCardData using redis for grade={}", grade);
                return;
            }
            CMDSTestListRes testListRes = testManager.getTestSeriesForHomeFeed(user.getUserId(), contextId);
            if (testListRes != null) {
                testSeriesCard.setHomeFeedCardData(testListRes);
                homeFeedCardList.add(testSeriesCard);
                LOGGER.info("HOME-FEED-TEST-SERIES-CARD: " + testSeriesCard.toString());
                String testSeriesCardRedisString = gson.toJson(testSeriesCard.getHomeFeedCardData());
                LOGGER.info("Setting redis key={} and value={}", testSeriesRedisKey, testSeriesCardRedisString);
                redisDAO.setex(testSeriesRedisKey, testSeriesCardRedisString, TWO_DAYS);
            }
        } catch (BadRequestException | InternalServerErrorException e) {
            LOGGER.error("Error in getting or setting redis Key={}", testSeriesRedisKey);
        } catch (Exception e) {
            LOGGER.error("TestSeriesCard for Homefeed not found for testId={}, userId={}, grade={}", contextId, user.getUserId(), grade);
        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .title(configurationCard.getTitle())
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }
}
