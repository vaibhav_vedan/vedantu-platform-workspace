package com.vedantu.homefeed.handlers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.StudyEntry;
import com.vedantu.cmds.managers.StudyManager;
import com.vedantu.exception.VException;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.entity.NCERTResponse;
import com.vedantu.homefeed.enums.CardType;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component(value = "NCERT_SOLUTIONS")
public class NCERTSolutionsHandler implements HomeFeedCardHandler
{
    private Logger logger = LogFactory.getLogger(NCERTSolutionsHandler.class);

    @Autowired
    private StudyManager studyManager;

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws VException
    {
        logger.info("Setting NCERTSolutions HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
        HomeFeedCard ncertSolutionsCard = getHomeFeedCard(configurationCard);
        try
        {
           List<String> subjectTitles = Arrays.asList(configurationCard.getContextUrls());
           List<StudyEntry> studyEntries = studyManager.getStudyEntriesForNCERT(subjectTitles, user.getGrade());
           logger.info("Found {} studyEntries from db", studyEntries.size());
           StudyEntry NCERTEntry = studyEntries.get(0);
           List<NCERTResponse> ncertResponses = new ArrayList<>();

           for (int i = 0; i < configurationCard.getContextUrls().length; i++)
           {
                NCERTResponse ncertResponse = new NCERTResponse();
                ncertResponse.setId(NCERTEntry.getId());
                ncertResponse.setSectionTitle(configurationCard.getContextUrls()[i]);
                ncertResponse.setThumbnail(configurationCard.getThumbnailArray()[i]);
                ncertResponses.add(ncertResponse);
           }
           ncertSolutionsCard.setHomeFeedCardDataList(ncertResponses);
           logger.info("Successfully set {} HomeFeedCardData  for NCERT HomeFeedCard", ncertSolutionsCard.getHomeFeedCardDataList().size());
           homeFeedCardList.add(ncertSolutionsCard);
        }
        catch (Exception e)
        {
            logger.error("Could not get StudyEntry from Db for userId : {}, grade : {} due to exception : {}", user.getUserId(), user.getGrade(), e);
        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(CardType.NCERT_SOLUTIONS)
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }
}
