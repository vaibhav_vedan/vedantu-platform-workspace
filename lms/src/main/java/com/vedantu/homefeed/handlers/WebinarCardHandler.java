package com.vedantu.homefeed.handlers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.homefeed.entity.FindWebinarsReq;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.entity.WebinarResponse;
import com.vedantu.homefeed.entity.WebinarStatus;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.lang.reflect.Type;
import java.util.List;

import static com.vedantu.homefeed.enums.CardType.UPCOMING_WEBINARS;

@Component(value = "UPCOMING_WEBINARS")
public class WebinarCardHandler implements HomeFeedCardHandler {

    private Logger logger = LogFactory.getLogger(WebinarCardHandler.class);
    private static final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private static final String GROWTH_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT");

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws VException
    {
        logger.info("Setting Webinar HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
        HomeFeedCard webinarHomeFeedCard = getHomeFeedCard(configurationCard);
        try {
            FindWebinarsReq findWebinarsReq = new FindWebinarsReq();
            findWebinarsReq.setRestrict(restrict);
            findWebinarsReq.setGrade(user.getGrade());
            findWebinarsReq.setWebinarStatus(WebinarStatus.ACTIVE);
            findWebinarsReq.setCallingUserId(user.getUserId());
            findWebinarsReq.setAppVersionCode(appVersionCode);
            if(StringUtils.isNotEmpty(user.getEmail()))
                findWebinarsReq.setStudentEmail(user.getEmail());
            List<WebinarResponse> webinarResponses = getWebinarResponseFromPlatform(findWebinarsReq);
            logger.info("Found {} webinars from platform", webinarResponses.size());
            webinarHomeFeedCard.setHomeFeedCardDataList(webinarResponses);
            logger.info("Successfully set {} HomeFeedCardData  for WebinarHomeFeedCard", webinarHomeFeedCard.getHomeFeedCardDataList().size());
            homeFeedCardList.add(webinarHomeFeedCard);
        }
        catch (Exception e)
        {
            logger.error("Could not get Webinar from Platform for userId : {}, grade : {} due to exception : {}", user.getUserId(), user.getGrade(), e);
        }
    }

    private List<WebinarResponse> getWebinarResponseFromPlatform(FindWebinarsReq findWebinarsReq) throws VException
    {
        logger.info("method=getWebinarResponseFromPlatform, class=WebinarCardHandler, webinarReq={}", findWebinarsReq);
        String url = GROWTH_ENDPOINT + "/cms/webinar/findUpcomingWebinars";
        logger.info("Calling Platform with the URL : {}", url);
        String query = new Gson().toJson(findWebinarsReq);
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String resString = response.getEntity(String.class);
        Type type = new TypeToken<List<WebinarResponse>>(){}.getType();
        return new Gson().fromJson(resString, type);
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(UPCOMING_WEBINARS)
                .sectionTitle(configurationCard.getSectionTitle())
                .serverTime(System.currentTimeMillis())
                .build();
    }
}
