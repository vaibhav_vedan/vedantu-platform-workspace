package com.vedantu.homefeed.handlers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.managers.CMDSQuestionManager;
import com.vedantu.cmds.pojo.SolutionInfo;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.vedantu.homefeed.utils.RedisKeyConstants.HOMEFEED_QUIZ_CARD_DATA;
import static com.vedantu.util.DateTimeUtils.SECONDS_PER_DAY;

@Component("QUIZ")
public class QuizCardHandler implements HomeFeedCardHandler {

    private static final int TWO_DAYS = 2 * SECONDS_PER_DAY;
    private Logger LOGGER = LogFactory.getLogger(QuizCardHandler.class);
    private CMDSQuestionManager questionManager;
    private RedisDAO redisDAO;

    @Autowired
    public QuizCardHandler(CMDSQuestionManager questionManager, RedisDAO redisDAO) {
        this.questionManager = questionManager;
        this.redisDAO = redisDAO;
    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        String grade = user.getGrade();
        ObjectMapper objectMapper = new ObjectMapper();
        LOGGER.info("Setting up QuizCard for userId={}, grade={}", user.getUserId(), grade);
        HomeFeedCard quizCard = getHomeFeedCard(configurationCard);
        String contextId = configurationCard.getContextId();
//        String quizRedisKey = HOMEFEED_QUIZ_CARD_DATA + grade + "_" + contextId;
        try {
//            String quizCardString = redisDAO.get(quizRedisKey);
//            if (StringUtils.isNotEmpty(quizCardString)) {
//                CMDSQuestion quizCardData = objectMapper.readValue(quizCardString, CMDSQuestion.class);
//                quizCard.setHomeFeedCardData(quizCardData);
//                homeFeedCardList.add(quizCard);
//                LOGGER.info("Successfuly set HomeFeed QuizCardData using redis for grade={}", grade);
//                return;
//            }
            CMDSQuestion question = questionManager.getQuestion(contextId);

            // Index issue on app hence handling this
            if(question != null && question.getSolutionInfo() != null) {
                SolutionInfo sInfo = question.getSolutionInfo();
                List<String> answerList = sInfo.getAnswer();
                if(ArrayUtils.isNotEmpty(answerList)) {
                    Integer answer = Integer.parseInt(sInfo.getAnswer().get(0));
                    if(answer != 0) {
                        answer = answer - 1;
                    }
                    answerList.set(0,answer.toString());
                    sInfo.setAnswer(answerList);
                }
            }
            quizCard.setHomeFeedCardData(question);
            homeFeedCardList.add(quizCard);
            String quizCardRedisString = objectMapper.writeValueAsString(quizCard.getHomeFeedCardData());
//            LOGGER.info("Setting redis key={} and value={}", quizRedisKey, quizCardRedisString);
//            redisDAO.setex(quizRedisKey, quizCardRedisString, TWO_DAYS);
        } catch (BadRequestException | InternalServerErrorException e) {
//            LOGGER.error("Error in getting or setting redis Key={}", quizRedisKey);
        } catch (VException e) {
            LOGGER.error("Could not get QuizCard for questionId={} userId={}, grade={}.", contextId, user.getUserId(), grade);
        } catch (Exception e) {
            LOGGER.error("Error building Quiz card in homefeed for userId={}, grade={}, exception={}", user.getUserId(), grade, e.getStackTrace());
        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }
}
