package com.vedantu.homefeed.handlers;

import com.google.gson.Gson;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.managers.CMDSTestManager;
import com.vedantu.cmds.pojo.CMDSTestInfo;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

import static com.vedantu.homefeed.utils.RedisKeyConstants.HOMEFEED_TEST_CARD_DATA;
import static com.vedantu.lms.cmds.enums.ContentState.ATTEMPTED;
import static com.vedantu.lms.cmds.enums.ContentState.EVALUATED;
import static com.vedantu.util.DateTimeUtils.SECONDS_PER_DAY;
import static java.util.Collections.singletonList;

@Component(value = "TEST")
public class TestCardHandler implements HomeFeedCardHandler {

    private static final int TWO_DAYS = 2 * SECONDS_PER_DAY;
    private Logger LOGGER = LogFactory.getLogger(TestCardHandler.class);
    private CMDSTestManager testManager;
    private RedisDAO redisDAO;
    private ContentInfoManager contentInfoManager;

    @Autowired
    public TestCardHandler(CMDSTestManager testManager, RedisDAO redisDAO, ContentInfoManager contentInfoManager) {
        this.testManager = testManager;
        this.redisDAO = redisDAO;
        this.contentInfoManager = contentInfoManager;
    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        String grade = user.getGrade();
        Gson gson = new Gson();
        LOGGER.info("Setting up HomeFeedTestCard for userId={}, grade={}", user.getUserId(), grade);
        HomeFeedCard testCard = getHomeFeedCard(configurationCard);
        String contextId = configurationCard.getContextId();
        String testRedisKey = HOMEFEED_TEST_CARD_DATA + grade + "_" + contextId;
        try {
            String quizCardString = redisDAO.get(testRedisKey);
            if (StringUtils.isNotEmpty(quizCardString)) {
                CMDSTestInfo testCardData = gson.fromJson(quizCardString, CMDSTestInfo.class);
                testCard.setHomeFeedCardData(testCardData);
                homeFeedCardList.add(testCard);
                LOGGER.info("Successfuly set HomeFeed TestCardData using redis for grade={}", grade);
                return;
            }
            CMDSTest cmdsTest = testManager.getCMDSTestById(configurationCard.getContextId());
            CMDSTestInfo testInfo = getTestInfo(user.getUserId(), cmdsTest);
            testCard.setHomeFeedCardData(testInfo);
            homeFeedCardList.add(testCard);
            String testCardRedisString = gson.toJson(testCard.getHomeFeedCardData());
            LOGGER.info("Setting redis key={} and value={}", testRedisKey, testCardRedisString);
            redisDAO.setex(testRedisKey, testCardRedisString, TWO_DAYS);
        } catch (BadRequestException | InternalServerErrorException e) {
            LOGGER.error("Error in getting or setting redis Key={}", testRedisKey);
        } catch (Exception e) {
            LOGGER.error("Error building upcomingVideosPlaylistCard in homefeed for testId={}, userId={}, grade={}", contextId, user.getUserId(), grade);
        }
    }

    private CMDSTestInfo getTestInfo(Long userId, CMDSTest cmdsTest) throws VException {
        CMDSTestInfo testInfo = testManager.getTestInfo(cmdsTest);
        LOGGER.info("Found testInfo. Getting contentInfo for the testId={}", cmdsTest.getId());
        List<ContentInfo> contentInfos = contentInfoManager.getContentInfoForTests(userId.toString(), singletonList(cmdsTest.getId()));
        Optional<ContentInfo> contentInfoOptional = contentInfos.stream().findFirst();
        contentInfoOptional.ifPresent(contentInfo -> setAttempted(testInfo, contentInfo));
        return testInfo;
    }

    private void setAttempted(CMDSTestInfo testInfo, ContentInfo contentInfo) {
        ContentState contentState = contentInfo.getContentState();
        boolean isAttempted = ATTEMPTED.equals(contentState) || EVALUATED.equals(contentState);
        testInfo.setAttempted(isAttempted);
        LOGGER.info("Succesfully set isAttempted value={} in CMDSTestInfo for testInfoId={}", isAttempted, testInfo.getId());
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .title(configurationCard.getTitle())
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }
}
