package com.vedantu.homefeed.handlers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.enums.CardType;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.vstories.enums.RequestType;
import com.vedantu.vstories.enums.SectionType;
import com.vedantu.vstories.responses.VStoryResponse;
import com.vedantu.vstories.service.VStoryService;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("VSTORIES")
public class VStoriesCardHandler implements HomeFeedCardHandler
{
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    Logger logger = logFactory.getLogger(VStoriesCardHandler.class);

    @Autowired
    private VStoryService vStoryService;

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException
    {
        logger.info("Setting VStories card for homefeed for grade : {}, userId : {}", user.getGrade(), user.getUserId());
        HomeFeedCard vStoryHomeFeedCard = getHomeFeedCard(configurationCard);
        List<VStoryResponse> vStoryResponseList = getVStoriesForHomeFeed(user.getGrade(), user.getTarget());
        logger.info("VStories fetched : {}", vStoryResponseList);
        if (vStoryResponseList != null && vStoryResponseList.size() > 0)
        {
            vStoryHomeFeedCard.setHomeFeedCardDataList(vStoryResponseList);
            homeFeedCardList.add(vStoryHomeFeedCard);
            logger.info("Successfully set {} VStories  for VStoryHomeFeedCard", vStoryHomeFeedCard.getHomeFeedCardDataList().size());
        }
    }

    private List<VStoryResponse> getVStoriesForHomeFeed(String grade, String target)
    {
        logger.info("Getting VStories for grade :{}, target : {}", grade, target);
        if (StringUtils.isNotEmpty(grade))
        {
            List<VStoryResponse> vStoryResponseList = vStoryService.fetchCurrentVStories(grade, RequestType.APP, SectionType.HOMEPAGE, target,true);
            logger.info("VStories fetched from service : {}", vStoryResponseList);
            return vStoryResponseList;
        }
        return null;
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .sectionTitle(configurationCard.getSectionTitle())
                .cardType(CardType.VSTORIES)
                .build();
    }
}
