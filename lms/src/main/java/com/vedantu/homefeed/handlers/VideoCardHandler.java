package com.vedantu.homefeed.handlers;

import com.google.gson.Gson;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.cmds.entities.CMDSVideo;
import com.vedantu.cmds.managers.CMDSVideoManager;
import com.vedantu.cmds.security.redis.RedisDAO;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.vedantu.homefeed.utils.RedisKeyConstants.HOMEFEED_VIDEO_CARD_DATA;
import static com.vedantu.util.DateTimeUtils.SECONDS_PER_DAY;
import static com.vedantu.util.DateTimeUtils.SECONDS_PER_MINUTE;

@Component(value = "VIDEO_CARD")
public class VideoCardHandler implements HomeFeedCardHandler {

    private static final int THIRTY_MINUTES = 30 * SECONDS_PER_MINUTE;
    private Logger LOGGER = LogFactory.getLogger(VideoCardHandler.class);
    private CMDSVideoManager videoManager;
    private RedisDAO redisDAO;

    @Autowired
    public VideoCardHandler(CMDSVideoManager videoManager, RedisDAO redisDAO) {
        this.videoManager = videoManager;
        this.redisDAO = redisDAO;
    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        String grade = user.getGrade();
        Gson gson = new Gson();
        LOGGER.info("Setting up VideoCard data for userId={}, grade={}", user.getUserId(), grade);
        HomeFeedCard homeFeedVideoCard = getHomeFeedCard(configurationCard);
        String contextId = configurationCard.getContextId();
        String videoRedisKey = HOMEFEED_VIDEO_CARD_DATA + grade + "_" + contextId;
        try {
            String videoCardString = redisDAO.get(videoRedisKey);
            if (StringUtils.isNotEmpty(videoCardString)) {
                CMDSVideo videoCardData = gson.fromJson(videoCardString, CMDSVideo.class);
                homeFeedVideoCard.setHomeFeedCardData(videoCardData);
                homeFeedCardList.add(homeFeedVideoCard);
                LOGGER.info("Successfuly set HomeFeed VideoCardData using redis for grade={}", grade);
                return;
            }
            homeFeedVideoCard.setHomeFeedCardData(videoManager.getAdminCMDSVideosById(contextId));
            homeFeedCardList.add(homeFeedVideoCard);
            String videoCardRedisString = gson.toJson(homeFeedVideoCard.getHomeFeedCardData());
            LOGGER.info("Setting redis key={} and value={}", videoRedisKey, videoCardRedisString);
            redisDAO.setex(videoRedisKey, videoCardRedisString, THIRTY_MINUTES);
        } catch (NotFoundException e) {
            LOGGER.error("Could not get VideoCard for videoId={}, userId={}, grade={}.", contextId, user.getUserId(), grade);
        } catch (BadRequestException | InternalServerErrorException e) {
            LOGGER.error("Error in getting or setting redis Key={}", videoRedisKey);
        } catch (Exception e) {
            LOGGER.error("Error building Video card in homefeed for userId={}, grade={}", user.getUserId(), grade);
        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .title(configurationCard.getTitle())
                .sectionTitle(configurationCard.getSectionTitle())
                .description(configurationCard.getDescription())
                .build();
    }
}
