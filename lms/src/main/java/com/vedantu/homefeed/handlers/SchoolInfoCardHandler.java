package com.vedantu.homefeed.handlers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.util.List;

import static com.vedantu.homefeed.enums.CardType.SCHOOL_INFO;

@Component(value = "SCHOOL_INFO")
public class SchoolInfoCardHandler implements HomeFeedCardHandler {
    private Logger LOGGER = LogFactory.getLogger(TestCardHandler.class);

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        LOGGER.info("Setting up SchoolInfo for userId={}, grade={}", user.getUserId(), user.getSchool());
        HomeFeedCard schoolInfoCard = getHomeFeedCard();
        try {
            if(StringUtils.isEmpty(user.getSchool())) {
                homeFeedCardList.add(schoolInfoCard);
            }
        } catch (Exception e) {
            LOGGER.error("SchoolInfo for Homefeed not found for email={}", user.getEmail());
        }
    }

    private HomeFeedCard getHomeFeedCard() {
        return HomeFeedCard.builder()
                .cardType(SCHOOL_INFO)
                .build();
    }
}

