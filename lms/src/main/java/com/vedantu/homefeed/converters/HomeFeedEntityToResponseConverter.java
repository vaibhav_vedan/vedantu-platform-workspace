package com.vedantu.homefeed.converters;

import com.vedantu.homefeed.entity.HomeFeedCardOrderEntity;
import com.vedantu.homefeed.response.HomeFeedCardOrderList;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HomeFeedEntityToResponseConverter implements Converter<HomeFeedCardOrderEntity, HomeFeedCardOrderList> {
    @Override
    public HomeFeedCardOrderList convert(HomeFeedCardOrderEntity entity) {
        return HomeFeedCardOrderList.builder()
                .id(entity.getId())
                .grade(entity.getGrade())
                .homeFeedConfigurationCards(entity.getHomeFeedConfigurationCards())
                .createdBy(entity.getCreatedBy())
                .creationTime(entity.getCreationTime())
                .lastUpdated(entity.getLastUpdated())
                .lastUpdatedBy(entity.getLastUpdatedBy())
                .build();
    }
}
