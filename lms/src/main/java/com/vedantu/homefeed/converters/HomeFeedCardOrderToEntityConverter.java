package com.vedantu.homefeed.converters;

import com.vedantu.homefeed.entity.HomeFeedCardOrderEntity;
import com.vedantu.homefeed.response.HomeFeedCardOrderList;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class HomeFeedCardOrderToEntityConverter implements Converter<HomeFeedCardOrderList, HomeFeedCardOrderEntity> {

    @Override
    public HomeFeedCardOrderEntity convert(HomeFeedCardOrderList homeFeedCardOrderList) {
        return HomeFeedCardOrderEntity.builder()
                .grade(homeFeedCardOrderList.getGrade())
                .homeFeedConfigurationCards(homeFeedCardOrderList.getHomeFeedConfigurationCards())
                .build();
    }
}
