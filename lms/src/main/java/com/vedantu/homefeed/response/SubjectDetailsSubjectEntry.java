package com.vedantu.homefeed.response;

import com.vedantu.homefeed.entity.HomeFeedCardData;
import lombok.*;

@Data
@Builder
@ToString
@NoArgsConstructor
public class SubjectDetailsSubjectEntry implements HomeFeedCardData {
    private String subjectName;
    private Integer noOfChapters;
    private String imageUri;

    public SubjectDetailsSubjectEntry(String subjectName, Integer noOfChapters, String imageUri) {
        this.subjectName = subjectName;
        this.noOfChapters = noOfChapters;
        this.imageUri = imageUri;
    }
}
