package com.vedantu.homefeed.response;

import java.util.List;

import com.vedantu.homefeed.entity.HomeFeedCardData;
import com.vedantu.homefeed.enums.CardType;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class HomeFeedCard {
    private String title;
    private String sectionTitle;
    private String description;
    private String clickToAction;
    private CardType cardType;
    private HomeFeedCardData homeFeedCardData;
    private Long serverTime;
    private List<? extends HomeFeedCardData> homeFeedCardDataList;
}
