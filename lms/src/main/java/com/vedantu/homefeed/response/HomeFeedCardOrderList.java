package com.vedantu.homefeed.response;

import com.vedantu.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.*;

import java.util.List;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedCardOrderList extends AbstractFrontEndReq {
    private String id;
    private String grade;
    List<HomeFeedConfigurationCard> homeFeedConfigurationCards;
    private Long creationTime;
    private String createdBy;
    private Long lastUpdated;
    private String lastUpdatedBy;

    @Override
    protected List<String> collectVerificationErrors() {
        List var1 = super.collectVerificationErrors();
        if (StringUtils.isEmpty(grade)) {
            var1.add("grade");
        }
        return var1;
    }
}
