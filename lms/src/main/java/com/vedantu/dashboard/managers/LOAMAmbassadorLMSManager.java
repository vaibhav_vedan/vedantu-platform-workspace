package com.vedantu.dashboard.managers;

import com.vedantu.cmds.entities.*;
import com.vedantu.doubts.entities.mongo.Doubt;
import com.vedantu.doubts.entities.mongo.DoubtChatMessage;
import com.vedantu.moodle.dao.LOAMAmbassadorLMSDAO;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicLongIdEntity;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*Arun Dhwaj: 29th May, 2019: code review done as per scale point of view */
/*
 * Overall data structure adopted in this class is very poor from Concurrent Point of View.
 *
 * */

@SuppressWarnings("Duplicates")
@Service
public class LOAMAmbassadorLMSManager
{

    @Autowired
    private LOAMAmbassadorLMSDAO lOAMAmbassadorLMSDAO;

    @Autowired
    public LogFactory logFactory;


    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(LOAMAmbassadorLMSManager.class);

    public List<ContentInfo> loam_getContentInfoData(Long fromTime, Long thruTime, Integer start, Integer size) {
        List<ContentInfo> contentInfos = new ArrayList<>();

        //Projection on fields to fetch
        Set<String> requiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addRequiredFieldsForContentInfo(requiredFields);

        contentInfos = lOAMAmbassadorLMSDAO.loam_getContentInfo(fromTime, thruTime,requiredFields,true, start, size);

        return contentInfos;

    }

    private void loam_addRequiredFieldsForContentInfo(Set<String> requiredFields) {
        requiredFields.add(ContentInfo.Constants.BOARD_ID);
        requiredFields.add(ContentInfo.Constants.BOARD_IDS);
        requiredFields.add(ContentInfo.Constants.EVAULATED_TIME);
        requiredFields.add(ContentInfo.Constants.CONTENT_INFO_TYPE);
        requiredFields.add(ContentInfo.Constants.CONTENT_STATE);
        requiredFields.add(ContentInfo.Constants.METADATA);
        requiredFields.add(ContentInfo.Constants.CONTENT_TYPE);
        requiredFields.add(ContentInfo.Constants.ATTEMPTED_TIME);
        requiredFields.add(ContentInfo.Constants.TEST_ID);
        requiredFields.add(ContentInfo.Constants.STUDENT_ID);
    }

    public List<CMDSTest> loam_getCMDSTestData(Long fromTime, Long thruTime, Integer start, Integer size) {
        List<CMDSTest> cmdsTests = new ArrayList<>();

        //Projection on fields to fetch
        Set<String> requiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addAbstractCMDSEntityFields(requiredFields);
        loam_addRequiredFieldsForCMDSTest(requiredFields);
        cmdsTests = lOAMAmbassadorLMSDAO.loam_getCMDSTest(fromTime, thruTime,requiredFields,true, start, size);

        return cmdsTests;
    }

    private void loam_addRequiredFieldsForCMDSTest(Set<String> requiredFields) {
        requiredFields.add(CMDSTest.Constants.TEST_STATE);
        requiredFields.add(CMDSTest.Constants.DURATION);
        requiredFields.add(CMDSTest.Constants.CONTENT_INFO_TYPE);
        requiredFields.add(CMDSTest.Constants.METADATA);
        requiredFields.add(CMDSTest.Constants.MAX_START_TIME);
        requiredFields.add(CMDSTest.Constants.MIN_START_TIME);
        requiredFields.add(CMDSTest.Constants.QUS_COUNT);
    }

    public List<CMDSQuestion> loam_getCMDSQuestionData(Long fromTime, Long thruTime, Integer start, Integer size) {
        List<CMDSQuestion> cmdsQuestions = new ArrayList<>();

        //Projection on fields to fetch
        Set<String> requiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addAbstractCMDSEntityFields(requiredFields);
        loam_addRequiredFieldsForCMDSQuestion(requiredFields);
        cmdsQuestions = lOAMAmbassadorLMSDAO.loam_getCMDSQuestion(fromTime, thruTime,requiredFields,true, start, size);

        return cmdsQuestions;
    }

    public List<BaseTopicTree> loam_getBaseTopicTreeData(Long fromTime, Long thruTime, Integer start, Integer size) {
        List<BaseTopicTree> baseTopicTrees = new ArrayList<>();

         baseTopicTrees = lOAMAmbassadorLMSDAO.loam_getBaseTopicTree(fromTime, thruTime,null,true, start, size);

        return baseTopicTrees;
    }

    private void loam_addRequiredFieldsForCMDSQuestion(Set<String> requiredFields) {
        requiredFields.add(CMDSQuestion.Constants._ID);
        requiredFields.add(CMDSQuestion.Constants.MAIN_TAGS);
        requiredFields.add(CMDSQuestion.Constants.TARGETS);
        requiredFields.add(CMDSQuestion.Constants.GRADES);
        requiredFields.add(CMDSQuestion.Constants.SOLUTION_INFO);
        requiredFields.add(CMDSQuestion.Constants.DIFFICULTY);
        requiredFields.add(CMDSQuestion.Constants.MARKS);
        requiredFields.add(CMDSQuestion.Constants.QUESTION_BODY);
        requiredFields.add(CMDSQuestion.Constants.TYPE);
    }

    public List<QuestionAttempt> loam_getQuestionAttemptData(Long fromTime, Long thruTime, Integer start, Integer size) {
        List<QuestionAttempt> questionAttempts = new ArrayList<>();

        //Projection on fields to fetch
        Set<String> requiredFields = new HashSet<>();
        questionAttempts = lOAMAmbassadorLMSDAO.loam_getQuestionAttempt(fromTime, thruTime,requiredFields,true, start, size);

        return questionAttempts;
    }

    public List<Doubt> loam_getDoubtData(Long fromTime, Long thruTime, Integer start, Integer size) {

        List<Doubt> doubts = new ArrayList<>();

        //Projection on fields to fetch
        Set<String> requiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addAbstractTargetTopicLongIdEntityFields(requiredFields);
        loam_addRequiredFieldsForDoubt(requiredFields);
        doubts = lOAMAmbassadorLMSDAO.loam_getDoubts(fromTime, thruTime,requiredFields,true, start, size);

        return doubts;
    }

    private void loam_addRequiredFieldsForDoubt(Set<String> requiredFields) {
        requiredFields.add(Doubt.Constants.DOUBT_STATE);
        requiredFields.add(Doubt.Constants.STUDENT_ID);
        requiredFields.add(Doubt.Constants.LAST_ACTIVITY_TIME);
        requiredFields.add(Doubt.Constants.DOUBT_SOLVER_POJO_LIST);
        requiredFields.add(Doubt.Constants.DOUBT_STATE_CHANGES);
        requiredFields.add(Doubt.Constants.PICURL);
        requiredFields.add(Doubt.Constants.TEXT);
    }

    private void loam_addAbstractTargetTopicLongIdEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractTargetTopicLongIdEntity.Constants.MAIN_TAGS);
    }

    public List<DoubtChatMessage> loam_getDoubtChatMessageData(Long fromTime, Long thruTime, Integer start, Integer size) {

        List<DoubtChatMessage> doubtChatMessages = new ArrayList<>();

        //Projection on fields to fetch
        Set<String> requiredFields = new HashSet<>();
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addRequiredFieldsForDoubtChatMessage(requiredFields);
        doubtChatMessages = lOAMAmbassadorLMSDAO.loam_getDoubtChatMessages(fromTime, thruTime,requiredFields,true, start, size);

        return doubtChatMessages;
    }

    private void loam_addRequiredFieldsForDoubtChatMessage(Set<String> requiredFields) {
        requiredFields.add(DoubtChatMessage.Constants.FROM_ROLE);
        requiredFields.add(DoubtChatMessage.Constants.DOUBT_ID);
        requiredFields.add(DoubtChatMessage.Constants.FROM_ID);
        requiredFields.add(DoubtChatMessage.Constants.READ);
        requiredFields.add(DoubtChatMessage.Constants.TEXT);
        requiredFields.add(DoubtChatMessage.Constants.PIC_URL);
        requiredFields.add(DoubtChatMessage.Constants.READ_TIME);
    }

    private void loam_addAbstractMongoEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractMongoEntity.Constants._ID);
        requiredFields.add(AbstractMongoEntity.Constants.CREATION_TIME);
        requiredFields.add(AbstractMongoEntity.Constants.CREATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED);
        requiredFields.add(AbstractMongoEntity.Constants.ID);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.ENTITY_STATE);
    }

    private void loam_addAbstractCMDSEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractCMDSEntity.Constants.NAME);
        requiredFields.add(AbstractCMDSEntity.Constants.RECORD_STATE);
        requiredFields.add(AbstractCMDSEntity.Constants.BOARD_IDS);
        requiredFields.add(AbstractCMDSEntity.Constants.GRADES);
        requiredFields.add(AbstractCMDSEntity.Constants.SUBJECT);
        requiredFields.add(AbstractCMDSEntity.Constants.TAGS);
        requiredFields.add(AbstractCMDSEntity.Constants.TARGET_IDS);
        requiredFields.add(AbstractCMDSEntity.Constants.TARGETS);
        requiredFields.add(AbstractCMDSEntity.Constants.GRADES_SLUGS);
        requiredFields.add(AbstractCMDSEntity.Constants.SUBJECT_SLUG);
        requiredFields.add(AbstractCMDSEntity.Constants.TAGS_SLUGS);
        requiredFields.add(AbstractCMDSEntity.Constants.TOPICS);
        requiredFields.add(AbstractCMDSEntity.Constants.TOPICS_SLUGS);
        requiredFields.add(AbstractCMDSEntity.Constants.TARGETS_SLUGS);

    }
}
