package com.vedantu.dashboard.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.enums.SubRole;
import com.vedantu.cmds.entities.BaseTopicTree;
import com.vedantu.cmds.entities.CMDSQuestion;
import com.vedantu.cmds.entities.CMDSTest;
import com.vedantu.cmds.entities.QuestionAttempt;
import com.vedantu.dashboard.managers.LOAMAmbassadorLMSManager;
import com.vedantu.doubts.entities.mongo.Doubt;
import com.vedantu.doubts.entities.mongo.DoubtChatMessage;
import com.vedantu.exception.*;
import com.vedantu.moodle.entity.ContentInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/loam")
public class LOAMAmbassadorLMSController
{

    @Autowired
    HttpSessionUtils sessionUtils;

	@Autowired
	private LOAMAmbassadorLMSManager lOAMAmbassadorLMSManager;

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(LOAMAmbassadorLMSController.class);

    /**
     * checkAuthorization
     *
     * @throws VException
     */
    private void checkAuthorization() throws VException
    {

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);

        Set<SubRole> subRoles = sessionUtils.getCurrentSessionData().getSubRoles();

        if (subRoles == null || !(subRoles.contains(SubRole.ACADMENTOR)))
        {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR,
                    "User other than Acedemic Mentor are forbidden to use this API");
        }
    }


    @RequestMapping(value = "/loam_getContentInfoData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ContentInfo> loam_getContentInfoData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                                     @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
//        return lOAMAmbassadorLMSManager.loam_getContentInfoData(fromTime, thruTime, start, size);

    }

    @RequestMapping(value = "/loam_getCMDSTestData", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSTest> loam_getCMDSTestData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                               @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return new ArrayList<>();
//        return lOAMAmbassadorLMSManager.loam_getCMDSTestData(fromTime, thruTime, start, size);

    }

    @RequestMapping(value = "/loam_getCMDSQuestionData", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CMDSQuestion> loam_getCMDSQuestionData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                                       @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
//        return lOAMAmbassadorLMSManager.loam_getCMDSQuestionData(fromTime, thruTime, start, size);

    }

    @RequestMapping(value = "/loam_getQuestionAttemptData", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<QuestionAttempt> loam_getQuestionAttemptData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                                             @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
//        return lOAMAmbassadorLMSManager.loam_getQuestionAttemptData(fromTime, thruTime, start, size);

    }

    @RequestMapping(value = "/loam_getDoubtData", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Doubt> loam_getDoubtData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                         @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
//        return lOAMAmbassadorLMSManager.loam_getDoubtData(fromTime, thruTime, start, size);

    }
    @RequestMapping(value = "/loam_getDoubtChatMessageData", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<DoubtChatMessage> loam_getDoubtChatMessageData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                                               @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return new ArrayList<>();
//        return lOAMAmbassadorLMSManager.loam_getDoubtChatMessageData(fromTime, thruTime, start, size);

    }

    @RequestMapping(value = "/loam_getBaseTopicTreeData", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BaseTopicTree> loam_getBaseTopicTreeData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                                         @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return new ArrayList<>();
//        return lOAMAmbassadorLMSManager.loam_getBaseTopicTreeData(fromTime, thruTime, start, size);

    }
}
