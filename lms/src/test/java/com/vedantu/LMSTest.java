package com.vedantu;


import com.vedantu.cmds.managers.CMDSQuestionSetManager;
import com.vedantu.cmds.request.UploadQuestionSetFileReq;
import com.vedantu.doubts.controllers.DoubtsController;
import com.vedantu.doubts.managers.DoubtsManager;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.AccessLevel;
import com.vedantu.moodle.ContentInfoManager;
import com.vedantu.moodle.response.HomepageTestResponse;
import com.vedantu.scheduling.pojo.session.Session;
import inti.ws.spring.exception.client.BadRequestException;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import java.io.File;
import java.util.List;


@WebAppConfiguration
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({"classpath*:dispatcher-servlet.xml"})
public class LMSTest {

    @Autowired
    DoubtsManager doubtsManager;


    @Autowired
    CMDSQuestionSetManager cmdsQuestionSetManager;


    @Autowired
    ContentInfoManager contentInfoManager;

    @Test
    public void testDummyMethod(){

        doubtsManager.getBaseTreeNodesByLevelWithoutChildren(4102376726463287l);
    }


    @Test
    public void testingParser() throws Exception {

        File file=new File("/Users/vipl1920clll081/Downloads/KVPY WITH 3 questions.docx");
        UploadQuestionSetFileReq  uploadQuestionSetFileReq = new UploadQuestionSetFileReq(file,"KVPY WITH 3 questions", "4102376726463287", AccessLevel.PUBLIC);
        cmdsQuestionSetManager.uploadQuestionFile(uploadQuestionSetFileReq);
    }

    @Test
    public void testHomePageTests(){
        List<HomepageTestResponse> homepageTestResponses = contentInfoManager.getHomepageUpcomingTests("4102376736691791");
        System.out.println(homepageTestResponses);
    }
}
