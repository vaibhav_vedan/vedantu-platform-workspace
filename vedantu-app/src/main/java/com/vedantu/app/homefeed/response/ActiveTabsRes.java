/**
 * 
 */
package com.vedantu.app.homefeed.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class ActiveTabsRes {
	 private List<String> activeTabs;
	 private  List<String> moreMenu;
}
