package com.vedantu.app.homefeed.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HFCardForMultipleGrades {
    private List<String> grades;
    private HomeFeedConfigurationCard homeFeedConfigurationCard;
}
