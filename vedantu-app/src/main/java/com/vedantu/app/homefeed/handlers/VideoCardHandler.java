package com.vedantu.app.homefeed.handlers;

import static com.vedantu.util.DateTimeUtils.SECONDS_PER_MINUTE;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;

//@Component(value = "VIDEO_CARD")
public class VideoCardHandler implements HomeFeedCardHandler {

    private static final int THIRTY_MINUTES = 30 * SECONDS_PER_MINUTE;
    
	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(VideoCardHandler.class);

    
    @Autowired
    private RedisDAO redisDAO;

//    @Autowired
//    public VideoCardHandler(CMDSVideoManager videoManager, RedisDAO redisDAO) {
//        this.videoManager = videoManager;
//        this.redisDAO = redisDAO;
//    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
//        String grade = user.getGrade();
//        Gson gson = new Gson();
//        LOGGER.info("Setting up VideoCard data for userId={}, grade={}", user.getUserId(), grade);
//        HomeFeedCard homeFeedVideoCard = getHomeFeedCard(configurationCard);
//        String contextId = configurationCard.getContextId();
//        String videoRedisKey = HOMEFEED_VIDEO_CARD_DATA + grade + "_" + contextId;
//        try {
//            String videoCardString = redisDAO.get(videoRedisKey);
//            if (StringUtils.isNotEmpty(videoCardString)) {
//                CMDSVideo videoCardData = gson.fromJson(videoCardString, CMDSVideo.class);
//                homeFeedVideoCard.setHomeFeedCardData(videoCardData);
//                homeFeedCardList.add(homeFeedVideoCard);
//                LOGGER.info("Successfuly set HomeFeed VideoCardData using redis for grade={}", grade);
//                return;
//            }
//            homeFeedVideoCard.setHomeFeedCardData(videoManager.getAdminCMDSVideosById(contextId));
//            homeFeedCardList.add(homeFeedVideoCard);
//            String videoCardRedisString = gson.toJson(homeFeedVideoCard.getHomeFeedCardData());
//            LOGGER.info("Setting redis key={} and value={}", videoRedisKey, videoCardRedisString);
//            redisDAO.setex(videoRedisKey, videoCardRedisString, THIRTY_MINUTES);
//        } catch (NotFoundException e) {
//            LOGGER.error("Could not get VideoCard for videoId={}, userId={}, grade={}.", contextId, user.getUserId(), grade);
//        } catch (BadRequestException | InternalServerErrorException e) {
//            LOGGER.error("Error in getting or setting redis Key={}", videoRedisKey);
//        } catch (Exception e) {
//            LOGGER.error("Error building Video card in homefeed for userId={}, grade={}", user.getUserId(), grade);
//        }
    }

//    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
//        return HomeFeedCard.builder()
//                .cardType(configurationCard.getCardType())
//                .title(configurationCard.getTitle())
//                .sectionTitle(configurationCard.getSectionTitle())
//                .description(configurationCard.getDescription())
//                .build();
//    }
}
