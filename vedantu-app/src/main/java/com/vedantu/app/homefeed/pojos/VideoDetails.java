package com.vedantu.app.homefeed.pojos;

import com.vedantu.app.homefeed.response.MuxResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VideoDetails
{
    private String thumbnail;
    private String url;
    private String hlsUrl;
    private MuxResponse muxResponse;
}
