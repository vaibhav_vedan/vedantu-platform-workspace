package com.vedantu.app.homefeed.handlers;

import static com.vedantu.util.StringUtils.isNotEmpty;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.Banner;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;

@Component(value = "SINGLE_BANNER")
public class SingleBannerCardHandler implements HomeFeedCardHandler {

    private Logger LOGGER = LogFactory.getLogger(SingleBannerCardHandler.class);

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        LOGGER.info("Setting up SingleBannerCard data for userId={}, grade={}", user.getUserId(), user.getGrade());
        HomeFeedCard homeFeedBannerCard = new HomeFeedCard();
        homeFeedBannerCard = getHomeFeedSingleBannerCard(configurationCard);
        homeFeedBannerCard.setHomeFeedCardData(getHomeFeedBannerCardData(configurationCard, appVersionCode));
        if (isNotEmpty(configurationCard.getContextUrls()[0]) && isNotEmpty(configurationCard.getRedirectDeepLinkList()[0])) {
            homeFeedCardList.add(homeFeedBannerCard);
        } else {
            LOGGER.error("Image Url or redirect deeplink for banner is null.");
        }
    }

    private Banner getHomeFeedBannerCardData(HomeFeedConfigurationCard configurationCard, String appVersionCode) {

//        int total = 0;
//        if(StringUtils.isNotEmpty(appVersionCode)) {
//            String a[] = appVersionCode.split("\\.");
//            if(a.length == 3) {
//                total = Integer.parseInt(a[0]) * 100 + Integer.parseInt(a[1]) * 10 + Integer.parseInt(a[2]);
//            }
//        }

        String inputUrl = configurationCard.getRedirectDeepLinkList()[0];

//        if(total >= 131) {
//            if(StringUtils.isNotEmpty(inputUrl) && inputUrl.contains("aio")) {
//                inputUrl = inputUrl.replaceFirst("aio", "app/courses");
//            }
//        }

        return Banner.builder()
                .contextUrl(configurationCard.getContextUrls()[0])
                .redirectionDeeplink(inputUrl)
                .thumbnail(configurationCard.getThumbnail())
                .build();
    }

    private HomeFeedCard getHomeFeedSingleBannerCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .sectionTitle(configurationCard.getSectionTitle())
                .description(configurationCard.getDescription())
                .build();
    }
}
