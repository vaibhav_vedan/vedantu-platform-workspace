package com.vedantu.app.homefeed.handlers;

import static com.vedantu.app.homefeed.enums.CardType.MICROCOURSE_COLLECTION;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.entity.MicroCourseCollectionResponse;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.request.GetBundlesRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Component(value = "MICROCOURSE_COLLECTION")
public class MicroCourseCollectionHandler implements HomeFeedCardHandler
{
	
	 @Autowired
	 private RedisDAO redisDAO;

    private Logger logger = LogFactory.getLogger(MicroCourseCardHandler.class);
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException
    {
        logger.info("Setting MicroCourseCollection HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
        HomeFeedCard microCourseCollectionHomeFeedCard = new HomeFeedCard();
        microCourseCollectionHomeFeedCard = getHomeFeedCard(configurationCard);
        try
        {
            List<String> displayTags = Arrays.asList(configurationCard.getContextUrls());
            GetBundlesRequest bundlesRequest = new GetBundlesRequest();
            bundlesRequest.setDisplayTags(displayTags);
            bundlesRequest.setGrade(user.getGrade());
            logger.info("GetBundleRequest for MicrocourseCollection : {}", bundlesRequest);
            List<MicroCourseCollectionResponse> microCourseCollectionResponses = getBundlesFromSubscription(bundlesRequest);
            if (microCourseCollectionResponses != null) {
                logger.info("MicroCourseCollection Response from Subscription : {}", microCourseCollectionResponses);
                Map<String, String> tagsThumbnail;
                if (appVersionCode.equals("WEB"))
                    tagsThumbnail = getTagThumbnail(configurationCard.getContextUrls(), configurationCard.getThumbnailArrayWeb());
                else
                    tagsThumbnail = getTagThumbnail(configurationCard.getContextUrls(), configurationCard.getThumbnailArray());
                setThumbnails(microCourseCollectionResponses, tagsThumbnail);
                microCourseCollectionHomeFeedCard.setHomeFeedCardDataList(microCourseCollectionResponses);
                logger.info("Successfully set {} bundles to microcourseCollection home feed", microCourseCollectionHomeFeedCard.getHomeFeedCardDataList().size());
            }
            if(null != microCourseCollectionHomeFeedCard && ArrayUtils.isNotEmpty(microCourseCollectionHomeFeedCard.getHomeFeedCardDataList())) {
            homeFeedCardList.add(microCourseCollectionHomeFeedCard);
            }
        }
        catch (Exception e)
        {
            logger.error("Could not get Bundles from Subscription for userId : {}, grade : {} due to exception : {}", user.getUserId(), user.getGrade(), e);
        }

    }

    private List<MicroCourseCollectionResponse> getBundlesFromSubscription(GetBundlesRequest getBundlesRequest) throws VException
    {
        String isSubscriptionActive = "ISSUBSCRIPTIONACTIVE";
        String subscriptionStatus = "";

        try
        {
            subscriptionStatus = redisDAO.get(isSubscriptionActive);
        }
        catch (Exception e)
        {
            logger.error("ERROR Occured while fetching data from Cache = " + e.getMessage());
        }

        logger.info("method=getBundlesFromSubscription, class=MicroCourseCollectionCardHandler, displayTags={}", getBundlesRequest.getDisplayTags());
        String key = "HOMEFEED_MICROCOURSE_COLLECTION_BY_GRADE_" + getBundlesRequest.getGrade();
        String resString = "";
        try {
        	resString = redisDAO.get(key);
        } catch (Exception e) {
            logger.error("ERROR Occured while fetching data from Cache = " + e.getMessage());
        }
        String url = SUBSCRIPTION_ENDPOINT + "/bundle/getMicroCourseCollectionForHomeFeed";
        String query = new Gson().toJson(getBundlesRequest);
        try {
        	if (subscriptionStatus.equals("true")) {
        		if (StringUtils.isEmpty(resString)) {
        			logger.info("Not available in Redis, Calling Subscription with the URL : {}", url);
        			ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true, true);
        			VExceptionFactory.INSTANCE.parseAndThrowException(response);
        			resString = response.getEntity(String.class);
        			try {
        				redisDAO.setex(key, resString, 3600);
        			} catch (Exception e) {
        				logger.error("ERROR Occured while saving data to Cache = " + e.getMessage());
        			}
        		}
        	}
        	else
        	{
        		logger.error("Subscription is not active, status : {}", subscriptionStatus);
        	}
        } catch (Exception e) {
        	logger.error("ERROR Occured while retrieving data from subscription = " + e.getMessage());
        }
        Type type = new TypeToken<List<MicroCourseCollectionResponse>>() {
        }.getType();
        List<MicroCourseCollectionResponse> microCourseCollectionResponses = new Gson().fromJson(resString, type);
        logger.info("MicroCourseCollection : {}", microCourseCollectionResponses);
        return microCourseCollectionResponses;
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(MICROCOURSE_COLLECTION)
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }

    private void setThumbnails(List<MicroCourseCollectionResponse> microCourseCollectionResponses, Map<String, String> tagsThumbnailMap)
    {
        for (MicroCourseCollectionResponse microCourseCollectionResponse : microCourseCollectionResponses)
        {
            microCourseCollectionResponse.setThumbnail(tagsThumbnailMap.get(microCourseCollectionResponse.getDisplayTag()));
        }
    }

    private Map<String, String> getTagThumbnail(String[] displayTags, String[] thumbnailArray)
    {
        Map<String, String> tagsMap = new HashMap<>();
        for (int i = 0; i < displayTags.length ; i++)
        {
            tagsMap.put(displayTags[i], thumbnailArray[i]);
        }

        return tagsMap;
    }
}