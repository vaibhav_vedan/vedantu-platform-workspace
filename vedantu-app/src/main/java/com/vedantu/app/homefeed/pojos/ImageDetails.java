package com.vedantu.app.homefeed.pojos;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ImageDetails
{
    private String imageUrl;
    private String thumbnail;
}
