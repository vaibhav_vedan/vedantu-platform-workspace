/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.app.homefeed.entity;

import java.util.List;

import com.vedantu.app.homefeed.pojos.ChapterPdf;
import com.vedantu.app.homefeed.pojos.StudySubjectOrBook;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicEntity;

/**
 *
 * @author ajith
 */
//@Document(collection = "StudyEntry")
public class StudyEntry extends AbstractTargetTopicEntity {

    private String title; //ncert solutions, previous yrs papers, syllabus,forumale titles
    private List<StudySubjectOrBook> subjectOrBooks;
    private String topHeadingName = "Select Subject - Book Name";//hardcoding this
    private List<ChapterPdf> chapters;//will only be populated in case of syllabus, because in syllabus there are no subject/books nodes
    private String bottomText;
    private Long views = 0l;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<StudySubjectOrBook> getSubjectOrBooks() {
        return subjectOrBooks;
    }

    public void setSubjectOrBooks(List<StudySubjectOrBook> subjectOrBooks) {
        this.subjectOrBooks = subjectOrBooks;
    }

    public String getTopHeadingName() {
        return topHeadingName;
    }

    public void setTopHeadingName(String topHeadingName) {
        this.topHeadingName = topHeadingName;
    }

    public List<ChapterPdf> getChapters() {
        return chapters;
    }

    public void setChapters(List<ChapterPdf> chapters) {
        this.chapters = chapters;
    }

    public String getBottomText() {
        return bottomText;
    }

    public void setBottomText(String bottomText) {
        this.bottomText = bottomText;
    }

    public Long getViews() {
        return views;
    }

    public void setViews(Long views) {
        this.views = views;
    }

    public static class Constants extends AbstractTargetTopicEntity.Constants {

        public static final String TITLE = "title";
        public static final String TARGETGRADES ="targetGrades";
        public static final String TOP_HEADING_NAME = "topHeadingName";
        public static final String SUBJECT_OR_BOOKS_TITLE = "subjectOrBooks.title";
        public static final String SUBJECT_OR_BOOKS_CHAPTER_COUNT = "subjectOrBooks.chapterCount";
        public static final String SUBJECT_OR_BOOKS_PDF_COUNT = "subjectOrBooks.pdfCount";
        public static final String BOTTOM_TEXT = "bottomText";
        public static final String VIEWS = "views";
        public static final String CREATIONTIME = "creationTime";
    }

}
