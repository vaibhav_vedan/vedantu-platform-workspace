/**
 * 
 */
package com.vedantu.app.homefeed.entity;

import java.util.List;

import com.vedantu.User.Role;
import com.vedantu.app.enums.TimeFrame;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class OTFSessionsReq {
	
    private Integer start;
    private Integer size;
    private List<String> batchIds;
    private String callingUserId;
    private Role callingUserRole;
    private String grade;
    private String board;
    private String stream;
    private String target;
    private TimeFrame timeFrame;
    

}
