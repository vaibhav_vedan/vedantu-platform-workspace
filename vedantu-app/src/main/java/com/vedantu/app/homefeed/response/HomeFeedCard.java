package com.vedantu.app.homefeed.response;

import java.util.List;

import com.vedantu.app.homefeed.entity.HomeFeedCardData;
import com.vedantu.app.homefeed.enums.CardType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedCard {
    private String title;
    private String sectionTitle;
    private String description;
    private String clickToAction;
    private CardType cardType;
    private HomeFeedCardData homeFeedCardData;
    private Long serverTime;
    private List<? extends HomeFeedCardData> homeFeedCardDataList;
}
