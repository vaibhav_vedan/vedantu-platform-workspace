package com.vedantu.app.homefeed.response;

import java.util.Set;

import com.vedantu.app.homefeed.entity.HomeFeedCardData;
import com.vedantu.app.homefeed.enums.SectionType;
import com.vedantu.app.homefeed.enums.StoryType;
import com.vedantu.app.homefeed.pojos.ImageDetails;
import com.vedantu.app.homefeed.pojos.VideoDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VStoryResponse implements HomeFeedCardData
{
    private String id;

    private String title;

    private Set<String> grades;

    private Set<String> targets;

    private Set<String> mainTags;

    private StoryType storyType;

    private SectionType sectionType;

    private ImageDetails imageDetails;

    private VideoDetails videoDetails;

    private String ctaTitle;

    private String ctaDeepLink;

    private Long startTime;

    private Long endTime;

    private boolean enableAutoRun;
}
