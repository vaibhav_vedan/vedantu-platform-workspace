package com.vedantu.app.homefeed.handlers;

import java.util.List;

import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;

//@Component(value = "MOST_VIEWED_VIDEOS")
public class MostViewedVideosCardHandler implements HomeFeedCardHandler {

//    private final Logger LOGGER = LogFactory.getLogger(MostViewedVideosCardHandler.class);
//    private CMDSVideoManager videoManager;
//    private static final int VIDEO_LIMIT = 20;
//
//    @Autowired
//    public MostViewedVideosCardHandler(CMDSVideoManager videoManager) {
//        this.videoManager = videoManager;
//    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
//        LOGGER.info("Getting top {} most viewed videos on homefeed for grade={}", VIDEO_LIMIT, user.getGrade());
//        HomeFeedCard homeFeedMostViewedVideosCard = getHomeFeedCard(configurationCard);
//        try {
//            homeFeedMostViewedVideosCard.setHomeFeedCardDataList(videoManager.getMostViewedVideosForGrade(user.getGrade(), VIDEO_LIMIT));
//            homeFeedCardList.add(homeFeedMostViewedVideosCard);
//        } catch (Exception e) {
//            LOGGER.error("Error building MostViewedVideo card in homefeed for userId={}, grade={}", user.getUserId(), user.getGrade());
//        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .title(configurationCard.getTitle())
                .cardType(configurationCard.getCardType())
                .sectionTitle(configurationCard.getSectionTitle())
                .clickToAction(configurationCard.getClickToAction())
                .build();
    }
}
