package com.vedantu.app.homefeed.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.vedantu.app.homefeed.entity.HomeFeedCardOrderEntity;
import com.vedantu.app.homefeed.response.HomeFeedCardOrderList;

@Component
public class HomeFeedCardOrderToEntityConverter implements Converter<HomeFeedCardOrderList, HomeFeedCardOrderEntity> {

    public HomeFeedCardOrderEntity convert(HomeFeedCardOrderList homeFeedCardOrderList) {
        return HomeFeedCardOrderEntity.builder()
                .grade(homeFeedCardOrderList.getGrade())
                .homeFeedConfigurationCards(homeFeedCardOrderList.getHomeFeedConfigurationCards())
                .build();
    }
}
