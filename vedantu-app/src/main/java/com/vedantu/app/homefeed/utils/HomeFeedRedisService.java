package com.vedantu.app.homefeed.utils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vedantu.app.homefeed.redis.RedisDAO;

@Component
public class HomeFeedRedisService {

	@Autowired
    private RedisDAO redisDAO;

    @Autowired
    public HomeFeedRedisService(RedisDAO redisDAO) {
        this.redisDAO = redisDAO;
    }

    public void deleteAllRedisKeys(String grade) {
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_DOUBT_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_RECENT_ACTIVITY_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_RESUME_VIDEO_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_RESUME_RESUME_LEARNING_JOURNEY_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_UPCOMING_VIDEOS_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_LIVE_NOW_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_MOST_VIEWED_VIDEOS_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_MOST_VIEWED_PDFS_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_SINGLE_BANNER_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_BANNER_LIST_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_VIDEO_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_PLAYLIST_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_QUIZ_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_TEST_CARD_DATA + grade);
//        redisDAO.deleteAllMatchingKeys(HOMEFEED_TEST_SERIES_CARD_DATA + grade);
    }
}
