package com.vedantu.app.homefeed.entity;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class Banner implements HomeFeedCardData{
    private String contextUrl;
    private String redirectionDeeplink;
    private String thumbnail;
}
