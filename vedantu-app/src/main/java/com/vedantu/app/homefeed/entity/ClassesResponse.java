/**
 * 
 */
package com.vedantu.app.homefeed.entity;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.app.homefeed.enums.ClassTypes;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Builder
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class ClassesResponse implements HomeFeedCardData{

	private String id;
    private String title;
    private Long startTime;
    private Long endTime;
    private String teacherName;
    private Map<String, Object> courseInfo;
    private String webinarCode;
    private long totalParticipants;
    private Long creationTime;
    private String sessionId;
    private String subject;
    private Set<String> subjects;
    private ClassTypes typeOfClass;
    private String courseName;
    private String ongoingClassNumber;
    private String classRedirectUrl;
    private TeacherBasicInfo teacherBasicInfo;
    private String bundleId;
    private List<String> batchIds;
    @Builder.Default
    private Boolean isReminderSet = false;
	@Builder.Default
    private Boolean isEnrolled = false;
    private EnrollmentState enrollmentState;
    private EntityStatus enrollmentStatus;
}
