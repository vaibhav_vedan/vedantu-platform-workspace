package com.vedantu.app.homefeed.response;

import com.vedantu.app.homefeed.entity.LiveQuizSchedule;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class LiveQuizScheduleRes {

    private Long timeToActivate;
    private String day;
    private Long money;
    private String theme;
    private String timeFormat;
    private Long serverTime;

    public LiveQuizScheduleRes(LiveQuizSchedule liveQuizSchedule, long serverTime){
        this.day = liveQuizSchedule.getDay();
        this.money = liveQuizSchedule.getMoney();
        this.theme = liveQuizSchedule.getTheme();
        this.timeToActivate = liveQuizSchedule.getTimeToActivate();
        this.timeFormat = liveQuizSchedule.getTimeFormat();
        this.serverTime = serverTime;
    }
}
