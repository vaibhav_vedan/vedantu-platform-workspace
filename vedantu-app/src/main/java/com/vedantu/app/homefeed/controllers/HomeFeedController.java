package com.vedantu.app.homefeed.controllers;

import static com.vedantu.User.Role.ADMIN;
import static com.vedantu.User.Role.STUDENT;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.app.homefeed.response.HomeFeedCardOrderList;
import com.vedantu.app.homefeed.service.HomeFeedService;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("/homefeed")
public class HomeFeedController {
	
	@Autowired
	private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;
	@SuppressWarnings("static-access")
	private Logger LOGGER = logFactory.getLogger(HomeFeedController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;
    
    @Autowired
    private FosUtils fosUtils;
    
    @Autowired
    private HomeFeedService homeFeedService;

    @Autowired
    public HomeFeedController(HttpSessionUtils sessionUtils,
                              FosUtils fosUtils, HomeFeedService homeFeedService) {
        this.sessionUtils = sessionUtils;
        this.fosUtils = fosUtils;
        this.homeFeedService = homeFeedService;
    }

    @RequestMapping(value = "/getHomeFeedForUser", method = GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<HomeFeedCard> getHomeFeedForUser(@RequestParam(name = "appVersionCode", required = false) String appVersionCode, @RequestParam(name= "requestSource", required = false) RequestSource requestSource, @RequestParam(name = "restrict", required = false) boolean restrict) throws VException {

        if (StringUtils.isEmpty(appVersionCode))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Null/Empty appVersionCode");

        Long userId = sessionUtils.getCallingUserId();
        LOGGER.info("method=getHomeFeedForUser, userId={} inside HomeFeedController", userId);
        sessionUtils.checkIfAllowed(userId, STUDENT, true);
        User ubi = fosUtils.getUserInfo(userId, false);
        UserBasicInfo user = fosUtils.getUserBasicInfo(userId, false);
        user.setCreationTime(ubi.getCreationTime());
        if(ubi.getStudentInfo() != null) { 
        	if(StringUtils.isNotEmpty(ubi.getStudentInfo().getSchool())) {
        		user.setSchool(ubi.getStudentInfo().getSchool());
        	}
        	if(StringUtils.isNotEmpty(ubi.getStudentInfo().getBoard()))
        		user.setBoard(ubi.getStudentInfo().getBoard());
        	if(StringUtils.isNotEmpty(ubi.getStudentInfo().getTarget()))
        		user.setTarget(ubi.getStudentInfo().getTarget());
        	//TODO: Add medium when added in user
        	if(StringUtils.isNotEmpty(ubi.getStudentInfo().getStream()))
        		user.setStream(ubi.getStudentInfo().getStream());
        }
        
        LOGGER.info("Fetched user Info for user={}", userId);
        return homeFeedService.getHomeFeedResponseForUser(user, appVersionCode, requestSource, restrict);
    }


    @RequestMapping(value = "/configureHomeFeed", method = POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public HomeFeedCardOrderList configureHomeFeed(@RequestBody HomeFeedCardOrderList homeFeedCardOrderList) throws VException {
        LOGGER.info("method=configureHomeFeed for grade={}, inside HomeFeedController", homeFeedCardOrderList.getGrade());
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        return homeFeedService.configureHomeFeed(homeFeedCardOrderList);
    }


}
