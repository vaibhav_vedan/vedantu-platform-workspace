package com.vedantu.app.homefeed.handlers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;

//@Component(value = "UPCOMING_VIDEOS")
public class UpcomingVideosCardHandler implements HomeFeedCardHandler {

    private Logger LOGGER = LogFactory.getLogger(UpcomingVideosCardHandler.class);
   // private CMDSVideoManager videoManager;

//    @Autowired
//    public UpcomingVideosCardHandler(CMDSVideoManager videoManager) {
//        this.videoManager = videoManager;
//    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
//        LOGGER.info("Setting up UpcomingVideoPlaylistCard for userId={}, grade={}", user.getUserId(), user.getGrade());
//        HomeFeedCard upcomingVideosPlaylistCard = getHomeFeedCard(configurationCard);
//        try {
//            upcomingVideosPlaylistCard.setHomeFeedCardData(videoManager.getUpcomingCMDSVideoPlaylist(getGetCMDSVideoPlaylistReq(user)));
//            homeFeedCardList.add(upcomingVideosPlaylistCard);
//        } catch (NotFoundException e) {
//            LOGGER.error("Could not get upcomingVideoPlaylist for playlistId={}, userId={}, grade={}."
//                    , configurationCard.getContextId(), user.getUserId(), user.getGrade());
//        } catch (Exception e) {
//            LOGGER.error("Error building upcomingVideosPlaylistCard in homefeed for testId={}, userId={}, grade={}"
//                    , configurationCard.getContextId(), user.getUserId(), user.getGrade());
//        }
    }

//    private GetCMDSVideoPlaylistReq getGetCMDSVideoPlaylistReq(UserBasicInfo user) {
//        GetCMDSVideoPlaylistReq req = new GetCMDSVideoPlaylistReq();
//        req.setMainTags(new HashSet<>(Arrays.asList(user.getGrade())));
//        req.setPublished(Boolean.TRUE);
//        return req;
//    }
//
//    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
//        return HomeFeedCard.builder()
//                .cardType(configurationCard.getCardType())
//                .sectionTitle(configurationCard.getSectionTitle())
//                .description(configurationCard.getDescription())
//                .build();
//    }
}
