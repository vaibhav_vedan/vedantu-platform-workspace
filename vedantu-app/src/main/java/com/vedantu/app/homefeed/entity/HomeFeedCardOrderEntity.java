package com.vedantu.app.homefeed.entity;

import java.util.List;

import org.springframework.data.mongodb.core.index.Indexed;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractTargetTopicLongIdEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
//@Document(collection = "HomeFeedCardOrderEntity")
public class HomeFeedCardOrderEntity extends AbstractMongoStringIdEntity {
    @Indexed(unique = true, background = true)
    private String grade;
    private List<HomeFeedConfigurationCard> homeFeedConfigurationCards;

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<HomeFeedConfigurationCard> getHomeFeedConfigurationCards() {
        return homeFeedConfigurationCards;
    }

    public void setHomeFeedConfigurationCards(List<HomeFeedConfigurationCard> homeFeedConfigurationCards) {
        this.homeFeedConfigurationCards = homeFeedConfigurationCards;
    }

    public static class Constants extends AbstractTargetTopicLongIdEntity.Constants {
        public static final String GRADE = "grade";
        public static final String HOME_FEED_CONFIGURATION_CARDS = "homeFeedConfigurationCards";
    }
}
