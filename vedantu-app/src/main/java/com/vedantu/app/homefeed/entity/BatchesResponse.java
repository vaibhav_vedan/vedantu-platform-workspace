/**
 * 
 */
package com.vedantu.app.homefeed.entity;

import java.util.List;

import com.vedantu.subscription.enums.CourseCategory;
import com.vedantu.subscription.response.BundleOfBatchResponse;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class BatchesResponse implements HomeFeedCardData {

	private String sectionTitle;
	
	private CourseCategory courseCategory;
	
	private List<BundleOfBatchResponse> bundleOfBatchesResponses;
}
