package com.vedantu.app.homefeed.entity;

import com.vedantu.app.homefeed.enums.SubCardType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedConfigurationSubCard implements HomeFeedCardData{
    private String title;
    private SubCardType subCardType;
    private boolean isPublished;
}
