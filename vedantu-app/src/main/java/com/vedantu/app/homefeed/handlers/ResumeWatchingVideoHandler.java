package com.vedantu.app.homefeed.handlers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

//@Component(value = "RESUME_VIDEO")
public class ResumeWatchingVideoHandler implements HomeFeedCardHandler {

    private Logger LOGGER = LogFactory.getLogger(ResumeWatchingVideoHandler.class);
    private static final String PLATFORM_END_PONT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private final int LIMIT = 1;
  //  private CMDSVideoManager videoManager;

//    @Autowired
//    public ResumeWatchingVideoHandler(CMDSVideoManager videoManager) {
//        this.videoManager = videoManager;
//    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
//        LOGGER.info("Setting up ResumeWatchingVideoCard for userId={}, grade={}", user.getUserId(), user.getGrade());
//        HomeFeedCard resumeWatchingVideoCard = getHomeFeedCard(configurationCard);
//        try {
//            HomeFeedCardData latestWatchedVideo = getLatestWatchedVideo(user.getUserId());
//            resumeWatchingVideoCard.setHomeFeedCardData(latestWatchedVideo);
//            LOGGER.info("Successfully fetched Last Watched Video for userId={}", user.getUserId());
//            homeFeedCardList.add(resumeWatchingVideoCard);
//        } catch (VException e) {
////            LOGGER.error("No resumable video found for userId={}", user.getUserId());
//        } catch (Exception e) {
////            LOGGER.error("Error building ResumeWatchingVideo card in homefeed for userId={}, grade={}", user.getUserId(), user.getGrade());
//        }
    }

//    private HomeFeedCardData getLatestWatchedVideo(Long userId) throws VException {
//        LOGGER.info("method=getLatestWatchedVideo, class=ResumeWatchingVideoHandler, userId={}", userId);
//        Optional<String> videoContextIdOptional = getRecentlyWatchedVideosFromPlatform(userId).stream()
//                .findFirst();
//        if (videoContextIdOptional.isPresent()) {
//            String contextId = videoContextIdOptional.get();
//            LOGGER.info("Found recentlywatchedVideo, contextId={} for userId={}", contextId, userId);
//            return videoManager.getCMDSVideosById(contextId, userId);
//        }
//        throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No resumable video found for userId=" + userId);
//    }
//
//    public List<String> getRecentlyWatchedVideosFromPlatform(Long userId) throws VException {
//        LOGGER.info("method=getRecentlyWatchedVideosFromPlatform, class=ResumeWatchingVideoCardHandler, userId={}.", userId);
//        String url = PLATFORM_END_PONT + "/social/getRecentlyWatchedVideos?";
//        url = url + "userId=" + userId + "&limit=" + LIMIT;
//        LOGGER.info("Calling platform using url={}", url);
//        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
//        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//        String respString = resp.getEntity(String.class);
//        Type _type = new TypeToken<ArrayList<String>>() {
//        }.getType();
//        return new Gson().fromJson(respString, _type);
//    }
//
//    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
//        return HomeFeedCard.builder()
//                .cardType(configurationCard.getCardType())
//                .sectionTitle(configurationCard.getSectionTitle())
//                .build();
//    }
}
