package com.vedantu.app.homefeed.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MicroCourseCollectionResponse implements HomeFeedCardData
{
    private String displayTag;
    private Long size;
    private String thumbnail;
}
