package com.vedantu.app.homefeed.converters;

import com.vedantu.app.homefeed.entity.ClassesResponse;
import com.vedantu.app.homefeed.enums.ClassTypes;
import com.vedantu.scheduling.response.session.OTFSessionPojoApp;

public class SessionToClassConverter {

	public static ClassesResponse convert(OTFSessionPojoApp session) {
		return ClassesResponse.builder()
				.id(null != session.getSessionId()?session.getSessionId():"")
				.title(null != session.getTitle()?session.getTitle():"")
				.startTime(null != session.getStartTime()?session.getStartTime():null)
				.endTime(null != session.getEndTime()?session.getEndTime():null)
				.teacherName(null != session.getTeacherBasicInfo()?session.getTeacherBasicInfo().getFullName():"")
				.bundleId(null != session.getBundleId()?session.getBundleId():"")
				.batchIds(null != session.getBatchIds()?session.getBatchIds():null)
				//.creationTime(session.getCreationTime())
				.subject(null != session.getSubject()?session.getSubject():null)
				.typeOfClass(ClassTypes.PAIDSESSION)
				.teacherBasicInfo(null != session.getTeacherBasicInfo()?session.getTeacherBasicInfo():null)
				.classRedirectUrl(null != session.getSessionUrl()?session.getSessionUrl():null)
				.courseName(null != session.getCourseTitle()?session.getCourseTitle():null)
				.isEnrolled(null != session.getIsEnrolled()?session.getIsEnrolled():false)
				.enrollmentState(null != session.getEnrollmentState()?session.getEnrollmentState():null)
				.enrollmentStatus(null != session.getEnrollmentStatus()?session.getEnrollmentStatus():null)
				//.ongoingClassNumber(session.)
				//TODO: Introduce Reminder in Explore tab for sessions as well as webinars
				//.isReminderSet(isReminderSet)
				.build();

	}
}
