package com.vedantu.app.homefeed.response;

import java.util.List;

import com.vedantu.app.homefeed.pojos.MuxPlaybackId;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MuxResponse
{
    private String id;
    private List<MuxPlaybackId> playback_ids;
    private String duration;
    private String status;
}
