package com.vedantu.app.homefeed.handlers;

import static com.vedantu.app.homefeed.enums.CardType.MICROCOURSES;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedBundleResponse;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.request.GetBundlesRequest;
import com.vedantu.subscription.pojo.EnrolledBundlesRes;
import com.vedantu.subscription.pojo.GetEnrolledBundlesReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Component(value = "MICROCOURSES")
public class MicroCourseCardHandler implements HomeFeedCardHandler
{
    private Logger logger = LogFactory.getLogger(MicroCourseCardHandler.class);
    private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");

    @Autowired
    private RedisDAO redisDAO;
    
    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws VException
    {
        logger.info("Setting MicroCourses HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
        HomeFeedCard microCoursesHomeFeedCard = new HomeFeedCard();
        microCoursesHomeFeedCard = getHomeFeedCard(configurationCard);
        try
        {
            GetBundlesRequest getBundlesRequest = new GetBundlesRequest();
            getBundlesRequest.setUserId(user.getUserId().toString());
            getBundlesRequest.setBundleIds(Arrays.asList(configurationCard.getContextUrls()));
            getBundlesRequest.setGrade(user.getGrade());
            logger.info("Bundle ids : {}", getBundlesRequest.getBundleIds());
            List<HomeFeedBundleResponse> bundleResponse = getBundlesFromSubscription(getBundlesRequest, appVersionCode);
            logger.info("Found {} bundles from subscription", bundleResponse);
            if (bundleResponse != null) {
                microCoursesHomeFeedCard.setHomeFeedCardDataList(bundleResponse);
                logger.info("Successfully set {} bundles to microcourse home feed", microCoursesHomeFeedCard.getHomeFeedCardDataList().size());
            }
            if(null != microCoursesHomeFeedCard && ArrayUtils.isNotEmpty(microCoursesHomeFeedCard.getHomeFeedCardDataList())) {
            homeFeedCardList.add(microCoursesHomeFeedCard);
            }
        }
        catch (Exception e)
        {
            logger.error("Could not get Bundles from Subscription for userId : {}, grade : {} due to exception : {}", user.getUserId(), user.getGrade(), e);
        }

    }

    private List<HomeFeedBundleResponse> getBundlesFromSubscription(GetBundlesRequest getBundlesRequest, String appVersionCode) throws VException
    {
        String isSubscriptionActive = "ISSUBSCRIPTIONACTIVE";
        String subscriptionStatus = "";

        try
        {
            subscriptionStatus = redisDAO.get(isSubscriptionActive);
        }
        catch (Exception e)
        {
            logger.error("ERROR Occured while fetching data from Cache = " + e.getMessage());
        }

        logger.info("method=getBundlesFromSubscription, class=MicroCourseCardHandler, bundleIds={}", getBundlesRequest.getBundleIds());
        String url = SUBSCRIPTION_ENDPOINT + "/bundle/getBundlesForMobileHomeFeed";
        String resString = "";
        String query = new Gson().toJson(getBundlesRequest);
        String key = "HOMEFEED_MICROCOURSES_" + getBundlesRequest.getGrade();

        try {
            resString = redisDAO.get(key);
        } catch (Exception e) {
            logger.error("ERROR Occured while fetching data from Cache = " + e.getMessage());
        }
        try {
        	if (subscriptionStatus.equals("true")) {
        		if (StringUtils.isEmpty(resString)) {
        			logger.info("Not available in Redis, Calling Subscription with the URL : {}", url);
        			ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true, true);
        			VExceptionFactory.INSTANCE.parseAndThrowException(response);
        			resString = response.getEntity(String.class);
        			try {
        				redisDAO.setex(key, resString, 3600);
        			} catch (Exception e) {
        				logger.error("Error Occured while saving data to Cache = " + e.getMessage());
        			}
        		}
        	}else{
        		logger.error("Subscription is not active, status : {}", subscriptionStatus);
        	}
        }catch (Exception e){
        	logger.error("ERROR Occured while retrieving data from subscription = " + e.getMessage());
        }
        Type type = new TypeToken<List<HomeFeedBundleResponse>>() {
        }.getType();
        List<HomeFeedBundleResponse> homeFeedBundleResponses = new Gson().fromJson(resString, type);

        try {
        	if (subscriptionStatus.equals("true")  && !appVersionCode.equals("WEB"))
        	{
        		String enrolmentUrl = SUBSCRIPTION_ENDPOINT + "/bundle/getEnrolledBundlesHomeFeed";
        		logger.info("Calling Subscription with the URL : {}", enrolmentUrl);
        		GetEnrolledBundlesReq getEnrolledBundlesReq = new GetEnrolledBundlesReq();
        		getEnrolledBundlesReq.setUserId(getBundlesRequest.getUserId());
        		getEnrolledBundlesReq.setBundleIds(getBundlesRequest.getBundleIds());
        		String enrollmentQuery = new Gson().toJson(getEnrolledBundlesReq);
        		ClientResponse enrollmentResponse = WebUtils.INSTANCE.doCall(enrolmentUrl, HttpMethod.POST, enrollmentQuery, true, true);
        		VExceptionFactory.INSTANCE.parseAndThrowException(enrollmentResponse);
        		String enrolmentResString = enrollmentResponse.getEntity(String.class);
        		Type enrolledType = new TypeToken<EnrolledBundlesRes>() {
        		}.getType();
        		EnrolledBundlesRes enrolledBundlesRes = new Gson().fromJson(enrolmentResString, enrolledType);

        		logger.info("Enrolled Bundles fetched from subscription : {}", enrolledBundlesRes);
        		Set<String> enrolledBundles = enrolledBundlesRes.getEnrolledBundles();

        		for (HomeFeedBundleResponse homeFeedBundleResponse : homeFeedBundleResponses) {    
        			if (enrolledBundles != null) {
        				if (enrolledBundles.contains(homeFeedBundleResponse.getId())) {
        					homeFeedBundleResponse.setBought(true);
        				}
        			}
        			logger.info("homeFeedBundleResponse.getSubjects() ->"+homeFeedBundleResponse.getSubjects());
        			if (ArrayUtils.isNotEmpty(homeFeedBundleResponse.getSubjects())) {
        				logger.info("Subjects is not empty");
        				homeFeedBundleResponse.setSubject(Collections.min(homeFeedBundleResponse.getSubjects()));
        				logger.info("homeFeedBundleResponse.getSubject()-> "+homeFeedBundleResponse.getSubject());
        			}
        		}

        	}
        }
        catch (Exception e) {
            logger.error("Could not get Enrolled Bundles from Subscription for userId : {} due to {}", getBundlesRequest.getUserId(), e);
        }
        logger.info("HomeFeedBundleResponse : {}", homeFeedBundleResponses);
        return homeFeedBundleResponses;
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(MICROCOURSES)
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }
}
