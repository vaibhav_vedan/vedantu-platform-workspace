/**
 * 
 */
package com.vedantu.app.homefeed.handlers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.BatchesResponse;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.enums.CardType;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.request.UserPremiumSubscriptionEnrollmentReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

/**
 * @author subarna
 *
 */
@Component(value = "BATCHES")
public class BatchesCardHandler implements HomeFeedCardHandler{

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(BatchesCardHandler.class);

	@Autowired
	private RedisDAO redisDAO;

	private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
	private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
	private static final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
	private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
	private static final String IS_USER_ACTIVE = "ISUSERACTIVE";
	private static final String IS_SUBSCRIPTION_ACTIVE ="ISSUBSCRIPTIONACTIVE";
	private static final String IS_SCHEDULING_ACTIVE = "ISSCHEDULINGACTIVE";

	@Override
	public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList,
			UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException {
		logger.info("Setting BatchResponse HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
		HomeFeedCard batchesCard = new HomeFeedCard();
		batchesCard = getHomeFeedCard(configurationCard);
		List<BatchesResponse> batchesResponse = getBatchesResponseForHomefeed(user);
		if(null != batchesResponse && !batchesResponse.isEmpty() && batchesResponse.size()>0) {
			batchesCard.setHomeFeedCardDataList(batchesResponse);
			homeFeedCardList.add(batchesCard);
			logger.info("[BatchesCardHandler] Successfully set {} HomeFeedCardData  for WebinarHomeFeedCard", batchesCard.getHomeFeedCardDataList().size());
		}
	}

	private List<BatchesResponse> getBatchesResponseForHomefeed(UserBasicInfo user) {

		List<BatchesResponse> bundleOfBatchesResponse = new ArrayList<BatchesResponse>();
		String subscriptionStatus = "";
		String responseString = "";
		try {
			subscriptionStatus = redisDAO.get(IS_SUBSCRIPTION_ACTIVE);
			logger.debug("subscriptionStatus -> "+subscriptionStatus);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("Subscription Module Health Status retrieval from Redis Failed");
		}

		if(null != subscriptionStatus && !subscriptionStatus.isEmpty() && subscriptionStatus.equalsIgnoreCase("true")) {

			UserPremiumSubscriptionEnrollmentReq req = new UserPremiumSubscriptionEnrollmentReq();
			String url = SUBSCRIPTION_ENDPOINT + "batch/getBatchesForHomefeed";
			if(null != user) {
				if(StringUtils.isNotEmpty(user.getUserId().toString())) {
					req.setUserId(user.getUserId().toString());
				}
				if(StringUtils.isNotEmpty(user.getGrade())) {
					req.setGrade(user.getGrade());
				}
				if(StringUtils.isNotEmpty(user.getBoard())) {
					req.setBoard(user.getBoard());
				}
				if(StringUtils.isNotEmpty(user.getStream())) {
					req.setStream(user.getStream());
				}
				if(StringUtils.isNotEmpty(user.getTarget())) {
					req.setTarget(user.getTarget());
				}
			}
			logger.debug("Calling subscription by url ->"+url.toString());
			String query = new Gson().toJson(req);
			try {
				ClientResponse response = WebUtils.INSTANCE.doCall(url.toString(), HttpMethod.POST, query, true);

				VExceptionFactory.INSTANCE.parseAndThrowException(response);

				if(null != response && response.getStatus() == 200) {
					responseString = response.getEntity(String.class);
					logger.debug("PremiumBundles Fetched -> "+user.getUserId().toString()+" " +responseString.toString());
					Type type = new TypeToken<List<BatchesResponse>>() {
					}.getType();
					bundleOfBatchesResponse = new Gson().fromJson(responseString, type);
					logger.debug("Subscription batchesResponse ->"+bundleOfBatchesResponse);
				}
			} catch (VException e) {
				logger.error("Error occured while fetching data from getBatchesForHomefeed ->"+e);
			}

		}else {

			logger.error("Batch Enrollment Status could not be fetched for user -> "+user.getUserId().toString());
			return bundleOfBatchesResponse;

		}
		return bundleOfBatchesResponse;

	}

	private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
		return HomeFeedCard.builder()
				.cardType(CardType.BATCHES)
				.serverTime(System.currentTimeMillis())
				.build();
	}

}
