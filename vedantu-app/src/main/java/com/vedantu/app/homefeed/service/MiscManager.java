/**
 * 
 */
package com.vedantu.app.homefeed.service;

import com.vedantu.User.User;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.ActiveTabsRes;
import com.vedantu.exception.VException;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.RequestSource;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author subarna
 *
 */
@Service
public class MiscManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MiscManager.class);

	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	private FosUtils fosUtils;

	private static final String MOBILE_APP_ACTIVE_TABS_GRADE = "MOBILE_APP_ACTIVE_TABS_GRADE_";

	private static final String MOBILE_APP_MORE_MENU_GRADE = "MOBILE_APP_MORE_MENU_GRADE_";
	
	private static final String MOBILE_APP_IOS_ACTIVE_TABS_GRADE = "MOBILE_APP_IOS_ACTIVE_TABS_GRADE_";
	
	private static final String MOBILE_APP_IOS_MORE_MENU_GRADE = "MOBILE_APP_IOS_MORE_MENU_GRADE_";

	private static final String LOWER = "LOWER_";

	private static final String UPPER = "UPPER_";

	private static final String FOR_APP_VER = "FOR_VER_";

	private static final String GTE_170 = "GTE_170";
	
	private static final String LT_170 = "LT_170";

	private static final String LTE_130 = "LTE_130";

	private static final String GT_130_LT_170 = "GT_130_LT_170";
	
	private static final String GTE_148 = "GTE_148";

	private static final String GTE_170_LT_173 = "GTE_170_LT_173";

	private static final String GTE_173 = "GTE_173";
	private static final String GTE_175 = "GTE_175";

	public ActiveTabsRes getActiveTabs(String userId, String appVersionCode, RequestSource requestSource) throws VException {

		ActiveTabsRes res = new ActiveTabsRes();

		logger.info("APP-VERSION-CODE: " + appVersionCode);

		int total = 0;
		if(StringUtils.isNotEmpty(appVersionCode)) {
			String[] a = appVersionCode.split("\\.");
			if(a.length == 3) {
				total = Integer.parseInt(a[0]) * 100 + Integer.parseInt(a[1]) * 10 + Integer.parseInt(a[2]);
			}
		}

		User ubi = fosUtils.getUserInfo(Long.valueOf(userId), false);
		String grade = "";
		if(StringUtils.isNotEmpty(ubi.getStudentInfo().getGrade())) {
			grade = ubi.getStudentInfo().getGrade();
		}
		
		boolean isBelowSixthGrade = false;

		if (StringUtils.isNotEmpty(grade))
		{
			isBelowSixthGrade = grade.equalsIgnoreCase("1") || grade.equalsIgnoreCase("2") || grade.equalsIgnoreCase("3") || grade.equalsIgnoreCase("4")
					|| grade.equalsIgnoreCase("5");
		}

		if(RequestSource.IOS.equals(requestSource) && (total < 141)) {
			String nValue = "STUDY";
			List<String> ntabs = Arrays.asList(nValue.split(","));
			res.setActiveTabs(ntabs);
			return res;
		}

		String nValue = "";
		String moreMenu = "";
		StringBuilder activeTabsKey = new StringBuilder("");
		StringBuilder moreMenuKey = new StringBuilder("");

		List<String> ntabs = new ArrayList<String>();
		List<String> moreMenuList = new ArrayList<String>();

		if(StringUtils.isNotEmpty(grade) && !isBelowSixthGrade) {

			if(total <= 130) {

				activeTabsKey.append(MOBILE_APP_ACTIVE_TABS_GRADE).append(UPPER).append(FOR_APP_VER).append(LTE_130);

				nValue = redisDAO.get(activeTabsKey.toString());

				logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);

				if(StringUtils.isEmpty(nValue)){
					nValue = "CLASSROOM,COURSES,STUDY,TEST";
				}
				ntabs = Arrays.asList(nValue.split(","));
				res.setActiveTabs(ntabs);
				return res;
			}
			else if(total > 130 && total <170) {

				if(RequestSource.IOS.equals(requestSource) && total >= 148) {
					//Send more menu data if UI uses well and good 
					activeTabsKey.append(MOBILE_APP_IOS_ACTIVE_TABS_GRADE).append(UPPER).append(FOR_APP_VER).append(GTE_148);
					moreMenuKey.append(MOBILE_APP_IOS_MORE_MENU_GRADE).append(UPPER).append(FOR_APP_VER).append(GTE_148);

					nValue = redisDAO.get(activeTabsKey.toString());
					moreMenu = redisDAO.get(moreMenuKey.toString());

					logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);
					logger.info("moreMenuKey" +moreMenuKey.toString()+ " "+ " value: " + moreMenu);

					if(StringUtils.isEmpty(nValue)){
						nValue = "DOUBTS,STUDY,PDF,TEST,MORE";
					}
					if(StringUtils.isEmpty(moreMenu)){
						moreMenu = "CLASSROOM,MY_CONTENT";
					}

					ntabs = Arrays.asList(nValue.split(","));
					moreMenuList = Arrays.asList(moreMenu.split(","));
					res.setActiveTabs(ntabs);
					res.setMoreMenu(moreMenuList);
					return res;

				}else {
					activeTabsKey.append(MOBILE_APP_ACTIVE_TABS_GRADE).append(UPPER).append(FOR_APP_VER).append(GT_130_LT_170);

					nValue = redisDAO.get(activeTabsKey.toString());
					logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);

					if(StringUtils.isEmpty(nValue)){
						nValue = "DOUBTS,CLASSROOM,COURSES,STUDY,TEST";
					}
					ntabs = Arrays.asList(nValue.split(","));
					res.setActiveTabs(ntabs);
					return res;
				}
				
			}else if (total >= 170 && total < 173) {
				// Default values for moreMenu are hardcoded in app before 1.7.4

				activeTabsKey.append(MOBILE_APP_ACTIVE_TABS_GRADE).append(UPPER).append(FOR_APP_VER).append(GTE_170_LT_173);

				nValue = redisDAO.get(activeTabsKey.toString());
				logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);
				
				if(StringUtils.isEmpty(nValue)){
					nValue = "DOUBTS,STUDY,MORE";
				}
				ntabs = Arrays.asList(nValue.split(","));
				moreMenuList = Arrays.asList(moreMenu.split(","));
				res.setActiveTabs(ntabs);
				res.setMoreMenu(moreMenuList);
				return res;
			} else if(total >= 173 && total < 175) {
				//Send more menu data if UI uses well and good 
				activeTabsKey.append(MOBILE_APP_ACTIVE_TABS_GRADE).append(UPPER).append(FOR_APP_VER).append(GTE_173);
				moreMenuKey.append(MOBILE_APP_MORE_MENU_GRADE).append(UPPER).append(FOR_APP_VER).append(GTE_173);

				nValue = redisDAO.get(activeTabsKey.toString());
				moreMenu = redisDAO.get(moreMenuKey.toString());

				logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);
				logger.info("moreMenuKey" +moreMenuKey.toString()+ " "+ " value: " + moreMenu);

				if(StringUtils.isEmpty(nValue)){
					nValue = "DOUBTS,STUDY,MORE";
				}
				if(StringUtils.isEmpty(moreMenu)){
					moreMenu = "CLASSROOM,TEST,PDF,MY_CONTENT";
				}

				ntabs = Arrays.asList(nValue.split(","));
				moreMenuList = Arrays.asList(moreMenu.split(","));
				res.setActiveTabs(ntabs);
				res.setMoreMenu(moreMenuList);
				return res;
			}else if(total >= 175){
				//Send more menu data if UI uses well and good
				activeTabsKey.append(MOBILE_APP_ACTIVE_TABS_GRADE).append(UPPER).append(FOR_APP_VER).append(GTE_175);
				moreMenuKey.append(MOBILE_APP_MORE_MENU_GRADE).append(UPPER).append(FOR_APP_VER).append(GTE_175);

				nValue = redisDAO.get(activeTabsKey.toString());
				moreMenu = redisDAO.get(moreMenuKey.toString());

				logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);
				logger.info("moreMenuKey" +moreMenuKey.toString()+ " "+ " value: " + moreMenu);

				if(StringUtils.isEmpty(nValue)){
					nValue = "DOUBTS,STUDY,PLAY,MORE";
				}
				if(StringUtils.isEmpty(moreMenu)){
					moreMenu = "CLASSROOM,TEST,PDF,MY_CONTENT";
				}

				ntabs = Arrays.asList(nValue.split(","));
				moreMenuList = Arrays.asList(moreMenu.split(","));
				res.setActiveTabs(ntabs);
				res.setMoreMenu(moreMenuList);
				return res;
			}

		}else {
			if(total >= 170){
				//For 1.7.0 app version onwards the the bottom tabs are getting renamed-> now Study tab is actually the courses tab of previous versions
				activeTabsKey.append(MOBILE_APP_ACTIVE_TABS_GRADE).append(LOWER).append(FOR_APP_VER).append(GTE_170);
				moreMenuKey.append(MOBILE_APP_MORE_MENU_GRADE).append(LOWER).append(FOR_APP_VER).append(GTE_170);
				
				nValue = redisDAO.get(activeTabsKey.toString());
				moreMenu = redisDAO.get(moreMenuKey.toString());

				logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);
				logger.info("moreMenuKey" +moreMenuKey.toString()+ " "+ " value: " + moreMenu);

				if(StringUtils.isEmpty(nValue)){
					nValue = "DOUBTS,STUDY,MORE";
				}
				if(StringUtils.isEmpty(moreMenu)) {
					moreMenu = "CLASSROOM,MY_CONTENT";
				}
				ntabs = Arrays.asList(nValue.split(","));
				moreMenuList = Arrays.asList(moreMenu.split(","));
				res.setActiveTabs(ntabs);
				res.setMoreMenu(moreMenuList);
				return res;
			}else{
				if(RequestSource.IOS.equals(requestSource) && total >= 148) {
					//Send more menu data if UI uses well and good 
					activeTabsKey.append(MOBILE_APP_IOS_ACTIVE_TABS_GRADE).append(LOWER).append(FOR_APP_VER).append(GTE_148);
					moreMenuKey.append(MOBILE_APP_IOS_MORE_MENU_GRADE).append(LOWER).append(FOR_APP_VER).append(GTE_148);

					nValue = redisDAO.get(activeTabsKey.toString());
					moreMenu = redisDAO.get(moreMenuKey.toString());

					logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);
					logger.info("moreMenuKey" +moreMenuKey.toString()+ " "+ " value: " + moreMenu);

					if(StringUtils.isEmpty(nValue)){
						nValue = "DOUBTS,STUDY,MORE";
					}
					if(StringUtils.isEmpty(moreMenu)){
						moreMenu = "CLASSROOM,MY_CONTENT";
					}

					ntabs = Arrays.asList(nValue.split(","));
					moreMenuList = Arrays.asList(moreMenu.split(","));
					res.setActiveTabs(ntabs);
					res.setMoreMenu(moreMenuList);
					return res;

				}else {
					//Hence UI needs courses in response for backward compatibility of versions
					activeTabsKey.append(MOBILE_APP_ACTIVE_TABS_GRADE).append(LOWER).append(FOR_APP_VER).append(LT_170);

					nValue = redisDAO.get(activeTabsKey.toString());
					logger.info("activeTabsKey" +activeTabsKey.toString()+ " "+ " value: " + nValue);

					if(StringUtils.isEmpty(nValue)){
						nValue = "COURSES";
					}

					ntabs = Arrays.asList(nValue.split(","));
					res.setActiveTabs(ntabs);
					return res;
				}
			}
		}
		return res;

	}
}
