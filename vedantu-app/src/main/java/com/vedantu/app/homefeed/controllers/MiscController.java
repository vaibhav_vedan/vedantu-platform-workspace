/**
 * 
 */
package com.vedantu.app.homefeed.controllers;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.app.homefeed.response.ActiveTabsRes;
import com.vedantu.app.homefeed.service.MiscManager;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.RequestSource;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

/**
 * @author subarna
 *
 */

@RestController
@RequestMapping("/misc")
public class MiscController {
	
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(MiscController.class);

    @Autowired
    private MiscManager miscManager;
    
    @Autowired
    HttpSessionUtils sessionUtils;
	
	@RequestMapping(value = "/getActiveTabsOrder", method = RequestMethod.GET)
    @ResponseBody
    public ActiveTabsRes getActiveTabsOrder(@RequestParam(name = "appVersionCode", required = false) String appVersionCode, @RequestParam(name= "requestSource", required = false) RequestSource requestSource, HttpServletRequest httpRequest, HttpServletResponse httpResponse) throws NotFoundException, ForbiddenException, VException, URISyntaxException, IOException {
        HttpSessionData sessionData=sessionUtils.getCurrentSessionData(true);
        sessionUtils._setCookieAndHeaders(httpRequest, httpResponse, sessionData);
        ActiveTabsRes activeTabs = miscManager.getActiveTabs(sessionData.getUserId().toString(), appVersionCode, requestSource);
        return activeTabs;
    }

}
