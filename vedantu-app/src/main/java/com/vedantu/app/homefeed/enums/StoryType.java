package com.vedantu.app.homefeed.enums;

public enum StoryType {
    IMAGE("IMAGE"),
    GIF("GIF"),
    VIDEO("VIDEO");

    String value;

    StoryType(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
