package com.vedantu.app.homefeed.handlers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

//@Component(value = "RECENT_ACTIVITY")
public class RecentActivityCardHandler implements HomeFeedCardHandler {

    private Logger LOGGER = LogFactory.getLogger(RecentActivityCardHandler.class);
    private static final String PLATFORM_END_PONT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private final int LIMIT = 20;
//    private CMDSVideoManager videoManager;
//    private CMDSTestManager testManager;
//    private StudyManager studyManager;
//    private DoubtsManager doubtsManager;
//
//    @Autowired
//    public RecentActivityCardHandler(CMDSVideoManager videoManager, CMDSTestManager testManager,
//                                     StudyManager studyManager, DoubtsManager doubtsManager) {
//        this.videoManager = videoManager;
//        this.testManager = testManager;
//        this.studyManager = studyManager;
//        this.doubtsManager = doubtsManager;
//    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
//        LOGGER.info("Setting up RecentActivityCard data for userId={}, grade={}", user.getUserId(), user.getGrade());
//        HomeFeedCard recentActivityHomeFeedCard = getHomeFeedCard();
//        try {
//            List<UserActivity> userActivities = getRecentActivitiesFromPlatform(user.getUserId());
//            LOGGER.info("Found {} UserActivities from View Collection in Platform.", userActivities.size());
//            List<HomeFeedCardData> recentActivityCardDataList = new ArrayList<>();
//            userActivities.forEach(userActivity -> addCard(user, recentActivityCardDataList, userActivity));
//            recentActivityHomeFeedCard.setHomeFeedCardDataList(recentActivityCardDataList);
//            LOGGER.info("Successfully set {} recentActivityCardData to RecentActivity Card.", recentActivityCardDataList.size());
//            homeFeedCardList.add(recentActivityHomeFeedCard);
//        } catch (Exception e) {
//            LOGGER.error("Could not get Recent Activities from Vedantu Platform for userId={}, grade={}", user.getUserId(), user.getGrade());
//        }
    }

//    private void addCard(UserBasicInfo user, List<HomeFeedCardData> recentActivityCardDataList, UserActivity userActivity) {
//        LOGGER.info("method=addCard, class=RecentActivityCardHandler, userId={}", user.getUserId());
//        HomeFeedCardData cardData;
//        try {
//            cardData = getRecentActivities(userActivity, user.getUserId());
//            recentActivityCardDataList.add(cardData);
//        } catch (NoSuchElementException e) {
////            LOGGER.error("No Data found for socialContextType={} and contextId={}", userActivity.getSocialContextType(), userActivity.getContextId());
//        }
//    }
//
//    private HomeFeedCardData getRecentActivities(UserActivity userActivity, Long userId) {
//        LOGGER.info("Returning recent activity for userId={}, socialContextType={}", userId, userActivity.getSocialContextType());
//        String contextId = userActivity.getContextId();
//        SocialContextType socialContextType = userActivity.getSocialContextType();
//        try {
//            switch (socialContextType) {
////                case CMDSVIDEO:
////                    CMDSVideoBasicInfo video = videoManager.getCMDSVideosById(contextId, userId);
////                    video.setLastUpdated(userActivity.getLastUpdated());
////                    return video;
//                case CMDSTEST:
//                    CMDSTestInfo testInfo = testManager.getTestInfo(testManager.getCMDSTestById(contextId));
//                    testInfo.setLastUpdated(userActivity.getLastUpdated());
//                    return testInfo;
//                case STUDY_ENTRY_ITEM:
//                    StudyEntryItem studyEntryItem = studyManager.getStudyEntryItemById(contextId);
//                    studyEntryItem.setLastUpdated(userActivity.getLastUpdated());
//                    return studyEntryItem;
//                case DOUBT:
//                    Doubt doubt = doubtsManager.getDoubtById(Long.parseLong(contextId));
//                    doubt.setLastUpdated(userActivity.getLastUpdated());
//                    for(DoubtStateChangePojo doubtStateChangePojo: doubt.getDoubtStateChanges()){
//                        if(DoubtState.DOUBT_T1_ACCEPTED.equals(doubtStateChangePojo.getNewState())){
//                            doubt.setOpened(true);
//                        }
//                    }
//                    return doubt;
//                default:
//                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Invalid SocialContextType=" + socialContextType);
//            }
//        } catch (VException e) {
////            LOGGER.error("Could not find data for SocialContextType={}, contextId={}, userId={}", socialContextType, contextId, userId);
//            throw new NoSuchElementException();
//        }
//    }
//
//
//    public List<UserActivity> getRecentActivitiesFromPlatform(Long userId) throws VException {
//        LOGGER.info("method=getRecentActivitiesFromPlatform, class=RecentActivityCardHandler, userId={}.", userId);
//        String url = PLATFORM_END_PONT + "/social/getRecentActivities?";
//        url = url + "userId=" + userId + "&limit=" + LIMIT;
//        LOGGER.info("Calling platform using url={}", url);
//        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
//        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//        String respString = resp.getEntity(String.class);
//        Type _type = new TypeToken<ArrayList<UserActivity>>() {
//        }.getType();
//        return new Gson().fromJson(respString, _type);
//    }
//
//    private HomeFeedCard getHomeFeedCard() {
//        return HomeFeedCard.builder()
//                .cardType(RECENT_ACTIVITY)
//                .sectionTitle("Your 7 days activity")
//                .build();
//    }
}
