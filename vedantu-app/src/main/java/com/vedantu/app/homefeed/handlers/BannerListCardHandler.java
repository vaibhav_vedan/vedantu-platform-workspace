package com.vedantu.app.homefeed.handlers;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.Banner;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;

@Component(value = "BANNER_LIST")
public class BannerListCardHandler implements HomeFeedCardHandler {

    private Logger LOGGER = LogFactory.getLogger(BannerListCardHandler.class);

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        String grade = user.getGrade();
        LOGGER.info("Setting up BannerListCard data for userId={}, grade={}", user.getUserId(), grade);
        try {
            HomeFeedCard homeFeedBannerListCard = new HomeFeedCard();
            homeFeedBannerListCard = getHomeFeedBannerListCard(configurationCard);
            if (isBannerListConfigurationValid(configurationCard)) {
                buildBannerListCard(configurationCard, homeFeedBannerListCard, appVersionCode);
                homeFeedCardList.add(homeFeedBannerListCard);
            } else {
                LOGGER.error("BannerUrls={} not equal to redirectLinks={} for userId={}, grade={}."
                        , configurationCard.getContextUrls().length, configurationCard.getRedirectDeepLinkList().length
                        , user.getUserId(), grade);
            }
        }
        catch (Exception e)
        {
            LOGGER.info("Could not add Banner list due to : {}", e.getMessage());
        }
    }

    private boolean isBannerListConfigurationValid(HomeFeedConfigurationCard configurationCard) {
        LOGGER.info("HomeFeedConfigurationCard-isBannerListConfigurationValid");
        int bannerSize = configurationCard.getContextUrls().length;
        int redirectDeepLinkSize = configurationCard.getRedirectDeepLinkList().length;
        return bannerSize == redirectDeepLinkSize;
    }

    private void buildBannerListCard(HomeFeedConfigurationCard configurationCard, HomeFeedCard homeFeedBannerCard, String appVersionCode) {
        int numberOfBanners = configurationCard.getContextUrls().length;
        List<Banner> bannerList = new ArrayList<>();
        for (int i = 0; i < numberOfBanners; i++) {
            bannerList.add(getBanner(configurationCard, i, appVersionCode));
        }
        homeFeedBannerCard.setHomeFeedCardDataList(bannerList);
    }

    private Banner getBanner(HomeFeedConfigurationCard configurationCard, int i, String appVersionCode) {

        LOGGER.info("HomeFeedConfigurationCard-appVersionCode: " + appVersionCode);

//        int total = 0;
//        if(StringUtils.isNotEmpty(appVersionCode)) {
//            String a[] = appVersionCode.split("\\.");
//            if(a.length == 3) {
//                total = Integer.parseInt(a[0]) * 100 + Integer.parseInt(a[1]) * 10 + Integer.parseInt(a[2]);
//            }
//        }

        String inputUrl = configurationCard.getRedirectDeepLinkList()[i];
//        if(total >= 131) {
//            if(StringUtils.isNotEmpty(inputUrl) && inputUrl.contains("aio")) {
//                inputUrl = inputUrl.replaceFirst("aio", "app/courses");
//            }
//        }

        Banner banner = Banner.builder()
                .contextUrl(configurationCard.getContextUrls()[i])
                .redirectionDeeplink(inputUrl)
                .thumbnail(configurationCard.getThumbnail())
                .build();

        if (appVersionCode.equals("WEB"))
            banner.setContextUrl(configurationCard.getThumbnailArrayWeb()[i]);

        return banner;
    }

    private HomeFeedCard getHomeFeedBannerListCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .sectionTitle(configurationCard.getSectionTitle())
                .build();
    }
}