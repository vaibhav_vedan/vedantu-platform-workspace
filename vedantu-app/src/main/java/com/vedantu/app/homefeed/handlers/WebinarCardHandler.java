package com.vedantu.app.homefeed.handlers;

import static com.vedantu.app.homefeed.enums.CardType.UPCOMING_WEBINARS;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.FindWebinarsReq;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.entity.WebinarResponse;
import com.vedantu.app.homefeed.entity.WebinarStatus;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Component(value = "UPCOMING_WEBINARS")
public class WebinarCardHandler implements HomeFeedCardHandler {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
    private Logger logger = LogFactory.getLogger(WebinarCardHandler.class);
    private static final String GROWTH_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT");
    private static final String IS_GROWTH_ACTIVE = "ISGROWTHACTIVE";
    
    @Autowired
	private RedisDAO redisDAO;

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws VException
    {
        logger.info("Setting Webinar HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
        HomeFeedCard webinarHomeFeedCard = new HomeFeedCard();
        webinarHomeFeedCard = getHomeFeedCard(configurationCard);
        try {
            FindWebinarsReq findWebinarsReq = new FindWebinarsReq();
            findWebinarsReq.setRestrict(restrict);
            findWebinarsReq.setGrade(user.getGrade());
            findWebinarsReq.setWebinarStatus(WebinarStatus.ACTIVE);
            findWebinarsReq.setCallingUserId(user.getUserId());
            findWebinarsReq.setAppVersionCode(appVersionCode);
            if(StringUtils.isNotEmpty(user.getEmail()))
            findWebinarsReq.setStudentEmail(user.getEmail());
            List<WebinarResponse> webinarResponses = getWebinarResponseFromPlatform(findWebinarsReq);
            logger.info("Found {} webinars from platform", webinarResponses.size());
            if(ArrayUtils.isNotEmpty(webinarResponses)) {
            	webinarHomeFeedCard.setHomeFeedCardDataList(webinarResponses);
            }
            logger.info("Successfully set {} HomeFeedCardData  for WebinarHomeFeedCard", webinarHomeFeedCard.getHomeFeedCardDataList().size());
            if(null != webinarHomeFeedCard && ArrayUtils.isNotEmpty(webinarHomeFeedCard.getHomeFeedCardDataList())) {
            	homeFeedCardList.add(webinarHomeFeedCard);
            }
        }
        catch (Exception e)
        {
            logger.error("Could not get Webinar from Platform for userId : {}, grade : {} due to exception : {}", user.getUserId(), user.getGrade(), e);
        }
    }


    private List<WebinarResponse> getWebinarResponseFromPlatform(FindWebinarsReq findWebinarsReq) throws VException
    {
    	List<WebinarResponse> webinarResponse = new ArrayList<WebinarResponse>();
    	String growthStatus = "";
    	String resString = "";
    	try {
    		growthStatus = redisDAO.get(IS_GROWTH_ACTIVE);
    		logger.info("platformStatus -> "+growthStatus);
    	} catch (InternalServerErrorException | BadRequestException e) {
    		logger.error("Platform Module Health Status retrieval from Redis Failed");
    	}

    	if(null != growthStatus && !growthStatus.isEmpty() && growthStatus.equalsIgnoreCase("true")) {
    		logger.info("method=getWebinarResponseFromPlatform, class=WebinarCardHandler, webinarReq={}", findWebinarsReq);
    		String url = GROWTH_ENDPOINT + "cms/webinar/findUpcomingWebinars";
    		logger.info("Calling Platform with the URL : {}", url);
    		String query = new Gson().toJson(findWebinarsReq);
    		ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true, true);
    		VExceptionFactory.INSTANCE.parseAndThrowException(response);
    		resString = response.getEntity(String.class);
    		Type type = new TypeToken<List<WebinarResponse>>(){}.getType();
    		webinarResponse = new Gson().fromJson(resString, type);
    	}
		return webinarResponse;
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(UPCOMING_WEBINARS)
                .sectionTitle(configurationCard.getSectionTitle())
                .serverTime(System.currentTimeMillis())
                .build();
    }
}
