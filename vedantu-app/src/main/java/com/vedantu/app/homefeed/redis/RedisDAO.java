/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.app.homefeed.redis;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.redis.AbstractRedisDAO;


@Service
public class RedisDAO extends AbstractRedisDAO{

    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RedisDAO.class);    
    
    private static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    
    public RedisDAO() {
    }

}
