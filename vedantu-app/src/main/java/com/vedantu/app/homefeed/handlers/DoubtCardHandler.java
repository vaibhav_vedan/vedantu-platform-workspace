package com.vedantu.app.homefeed.handlers;

import static com.vedantu.app.homefeed.enums.CardType.DOUBT;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Component(value = "DOUBT")
public class DoubtCardHandler implements HomeFeedCardHandler {

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger LOGGER = logFactory.getLogger(DoubtCardHandler.class);
   
	@Autowired
    private RedisDAO redisDAO;
	
    private static final String DOUBTS = "DOUBTS";
	
    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    
    private static String PAID_USERS = ConfigUtils.INSTANCE.getStringValue("doubt.paid.users");
    
    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    
    private static final String IS_DINERO_ACTIVE = "ISDINEROACTIVE";

    @Autowired
    public DoubtCardHandler(RedisDAO redisDAO) {
        this.redisDAO = redisDAO;
    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        LOGGER.info("Setting up AskDoubtCard for userId={}, grade={}", user.getUserId(), user.getGrade());
        HomeFeedCard homeFeedAskDoubtCard = new HomeFeedCard();
        homeFeedAskDoubtCard =	getHomeFeedBannerListCard(configurationCard);
        int grade = Integer.parseInt(user.getGrade());
        if (isDoubtAllowed(grade)) {
            String doubtKey = "MOBILE_APP_ACTIVE_TABS_GRADE_" + grade;
            try {
                LOGGER.info("Getting value from redis for key={}, userId={}", doubtKey, user.getUserId());
                String value = redisDAO.get(doubtKey);
                if (StringUtils.isNotEmpty(value) && value.contains(DOUBTS)) {


                    // Urgent requirement - hence jugaad code
                    // Will revisit it again

//                    Calendar calendar = Calendar.getInstance();
//                    calendar.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
//                    calendar.set(Calendar.YEAR, 2019);
//                    calendar.set(Calendar.MONTH, Calendar.APRIL);
//                    calendar.set(Calendar.DAY_OF_MONTH, 19);
//                    calendar.set(Calendar.HOUR_OF_DAY, 0);
//                    calendar.set(Calendar.MINUTE, 0);
//                    calendar.set(Calendar.SECOND, 0);
//                    calendar.set(Calendar.MILLISECOND, 0);
//                    Long dateTime = calendar.getTime().getTime();

                    Calendar calendar2 = Calendar.getInstance();
                    calendar2.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
                    calendar2.set(Calendar.YEAR, 2019);
                    calendar2.set(Calendar.MONTH, Calendar.MAY);
                    calendar2.set(Calendar.DAY_OF_MONTH, 10);
                    calendar2.set(Calendar.HOUR_OF_DAY, 0);
                    calendar2.set(Calendar.MINUTE, 0);
                    calendar2.set(Calendar.SECOND, 0);
                    calendar2.set(Calendar.MILLISECOND, 0);
                    Long dateTime2 = calendar2.getTime().getTime();

                    String userId = user.getUserId().toString();

                    if(user.getCreationTime() > dateTime2) {
                        boolean paidUser = paidOrFree(userId);
                        LOGGER.info("Final DoubtCardHandler paidUser -> "+paidUser);
                        if(paidUser == true) {
                            homeFeedCardList.add(homeFeedAskDoubtCard);
                        }
                    } else {
                    	 LOGGER.info("Running else paidUser -> ");
                        homeFeedCardList.add(homeFeedAskDoubtCard);
                    }

                }
            } catch ( VException e) {
                LOGGER.error("Could not get values from redis for key={}, userId={}", doubtKey, user.getUserId());
            }
        }
    }

    private boolean isDoubtAllowed(int grade) {
        int[] gradesAllowed = {4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
        for (int gradeAllowed : gradesAllowed) {
            if (grade == gradeAllowed) {
                return true;
            }
        }
        LOGGER.info("Asking Doubt not allowed for grade={}", grade);
        return false;
    }

    private boolean paidOrFree(String userId) throws VException {
        boolean paidUser = false;

        String[] paidUsersList = PAID_USERS.split(",");
        if(Arrays.asList(paidUsersList).contains(userId)) {
            paidUser = true;
        } else {
        	String dineroStatus = "";
        	try {
        		dineroStatus = redisDAO.get(IS_DINERO_ACTIVE);
        		LOGGER.info("dineroStatus -> "+dineroStatus);
        	} catch (InternalServerErrorException | BadRequestException e) {
        		LOGGER.error("DINERO Module Health Status retrieval from Redis Failed");
        	}

        	if(null != dineroStatus && !dineroStatus.isEmpty() && dineroStatus.equalsIgnoreCase("true")) {
        		try {
        			String url = DINERO_ENDPOINT + "/payment/isDoubtsPaidUser?";
        			url = url + "userId=" + userId;
        			ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true, true);
        			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        			String respString = resp.getEntity(String.class);
        			LOGGER.info("DoubtCardHandler respString = "+respString);
        			Type filterListType = new TypeToken<Map<String, Boolean>>() {
        			}.getType();

        			Map<String, Boolean> userInDoubt = new Gson().fromJson(respString, filterListType);

        			if (userInDoubt.get("isPaid") != null && userInDoubt.get("isPaid") == true) {
        				paidUser = true;
        			}
        		} catch (VException vex) {
        			paidUser = false;
        		}
        	}
        }
        String paidUsersString = redisDAO.get("PAID_USERS");
        List<String> paidUsersListRedis = new ArrayList<>();
        if(StringUtils.isNotEmpty(paidUsersString)) {
            paidUsersListRedis = Arrays.asList(paidUsersString.split(","));
            if(ArrayUtils.isNotEmpty(paidUsersListRedis) && paidUsersListRedis.contains(userId)) {
                paidUser = true;
            }
        }
        LOGGER.info("DoubtCardHandler paidUser -> "+paidUser);
        return paidUser;
    }

    private HomeFeedCard getHomeFeedBannerListCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(DOUBT)
                .build();
    }
}
