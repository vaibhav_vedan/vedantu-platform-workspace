package com.vedantu.app.homefeed.handlers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;

import java.util.List;

public interface HomeFeedCardHandler {
    void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException;
}
