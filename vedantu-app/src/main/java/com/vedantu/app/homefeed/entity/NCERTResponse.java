package com.vedantu.app.homefeed.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class NCERTResponse implements HomeFeedCardData
{
    private String id;
    private String sectionTitle;
    private String thumbnail;
}
