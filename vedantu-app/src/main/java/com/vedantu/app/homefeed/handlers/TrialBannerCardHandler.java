/**
 * 
 */
package com.vedantu.app.homefeed.handlers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.entity.TrialBannerResponse;
import com.vedantu.app.homefeed.enums.CardType;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.subscription.response.TrialExpirationResp;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

/**
 * @author subarna
 *
 */
@Component(value = "TRIAL_BANNER")
public class TrialBannerCardHandler implements HomeFeedCardHandler {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(TrialBannerCardHandler.class);
	
	@Autowired
	private RedisDAO redisDAO;

	private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
	private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
	private static final String IS_USER_ACTIVE = "ISUSERACTIVE";
	private static final String IS_SUBSCRIPTION_ACTIVE ="ISSUBSCRIPTIONACTIVE";
	private static final String DEFAULT = "TRIAL_BANNER";
	private static final String FILTER_BY_TOUCHPOINT = "_FOR_";
	private static final String NOT_STARTED = "DEFAULT";
	private static final String STARTED_BUT_PENDING = "PENDING";
	private static final String COMPLETED = "COMPLETED";
	private static final String EXPIRED = "EXPIRED";



	@Override
	public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList,
			UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException {

		logger.debug("Setting TrialBanner HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
		HomeFeedCard trialBannerCard = new HomeFeedCard();
		trialBannerCard =getHomeFeedCard(configurationCard);
		List<TrialBannerResponse> trialBannerResponse = getTrialBannerForHomefeed(user);
		if(null != trialBannerResponse && !trialBannerResponse.isEmpty() && trialBannerResponse.size()>0) {
			trialBannerCard.setHomeFeedCardDataList(trialBannerResponse);
			homeFeedCardList.add(trialBannerCard);
		}

	}


	private List<TrialBannerResponse> getTrialBannerForHomefeed(UserBasicInfo user) throws VException {
		List<TrialBannerResponse> listOfTrialBannerRepsonses = new ArrayList<TrialBannerResponse>();

		TrialBannerResponse  trialBannerResponse = new TrialBannerResponse();
		TrialExpirationResp trialExpirationResp = new TrialExpirationResp();
		StringBuilder redisKeyBuilder = new StringBuilder(DEFAULT+FILTER_BY_TOUCHPOINT);
		String status = "";
		String redisResult = "";
		Long validTill = 0L;
		String subscriptionStatus = "";
		String responseString = "";
		try {
			subscriptionStatus = redisDAO.get(IS_SUBSCRIPTION_ACTIVE);
			logger.debug("subscriptionStatus -> "+subscriptionStatus);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("Subscription Module Health Status retrieval from Redis Failed");
		}
		
		if(null != subscriptionStatus && !subscriptionStatus.isEmpty() && subscriptionStatus.equalsIgnoreCase("true")) {

			String url = SUBSCRIPTION_ENDPOINT + "bundle/trial/enrollment/" + user.getUserId().toString() ;
			ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true, true);
			VExceptionFactory.INSTANCE.parseAndThrowException(response);
			responseString = response.getEntity(String.class);
			logger.debug("Trial Enrollment Status fetched for user = "+user.getUserId().toString()+" " +responseString.toString());
			Type type = new TypeToken<TrialExpirationResp>() {
			}.getType();
			trialExpirationResp = new Gson().fromJson(responseString, type);
			logger.debug("Trial Enrollment data trialExpirationResp ->"+trialExpirationResp);

		}else {

			logger.error("Trial Enrollment Status could not be fetched for user -> "+user.getUserId().toString());
			return listOfTrialBannerRepsonses;

		}
		

		if(null != trialExpirationResp && null != trialExpirationResp.getIsPaidUser() && null != trialExpirationResp.getIsTrialUser()) {
			//User who has Non Trial Bundle Enrollment is a Paid User and should not be shown any banners
			if(trialExpirationResp.getIsPaidUser() && !trialExpirationResp.getIsTrialUser()) {
				return listOfTrialBannerRepsonses;
				
			//Either user has no enrollments at all OR User is already Enrolled to TRIAL 	
			}else {

			// Status of TRIAL Enrollment is ACTIVE
				if(null != trialExpirationResp.getStatus()
						&& trialExpirationResp.getStatus().equals(EntityStatus.ACTIVE)) {

					logger.debug("User has an active subscription for trial -> "+trialExpirationResp);
					String userStatus = "";
					String respString = "";

					try {
						userStatus = redisDAO.get(IS_USER_ACTIVE);
						logger.debug("subscriptionStatus -> "+userStatus);
					} catch (InternalServerErrorException | BadRequestException e) {
						logger.error("User Module Health Status retrieval from Redis Failed");
					}
					if(null!=userStatus && !userStatus.isEmpty() && userStatus.equalsIgnoreCase("true")) {

						String url = USER_ENDPOINT + "/onboarding/getCompletionStatus/" + user.getUserId().toString() ;
						ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true, true);
						VExceptionFactory.INSTANCE.parseAndThrowException(response);
						respString = response.getEntity(String.class);
						logger.debug("Trial Completion Status fetched for user = " +respString.toString());

					}else {

						logger.error("Trial Completion Status could not be fetched for user");
						return listOfTrialBannerRepsonses;

					}
					if(null != respString && StringUtils.isNotEmpty(respString)) {

						logger.debug("respString for student onboarding is not null = "+respString);
						//Onboarding is pending and user is migrated to onboarding page on login everytime
						if(respString.toUpperCase().contains("PENDING") || respString.toUpperCase().contains("STARTED") || respString.toUpperCase().contains("ONGOING")) {
							logger.info("Onboarding is pending -> ");
							redisKeyBuilder.append(STARTED_BUT_PENDING);
							redisResult = redisDAO.get(redisKeyBuilder.toString());
							validTill = trialExpirationResp.getValidTill();
							status = STARTED_BUT_PENDING;
							
							logger.debug("Trial Banner fetched for Redis for {} : {}",redisKeyBuilder.toString(), redisResult);
						}

						//Onboarding Completed, user is migrated to course enrollment page
						else if(respString.toUpperCase().contains("COMPLETED") || respString.toUpperCase().contains("SKIPPED")) {

							logger.debug("Onboarding is COMPLETED -> ");
							redisKeyBuilder.append(COMPLETED);
							status = COMPLETED;
							redisResult = redisDAO.get(redisKeyBuilder.toString());
							validTill = trialExpirationResp.getValidTill();
							logger.debug("Trial Banner fetched for Redis for {} : {}",redisKeyBuilder.toString(), redisResult);
							
						//Onboarding Not Started
						}else {

							logger.debug("Onboarding is NOT FOUND-> ");
							redisKeyBuilder.append(NOT_STARTED);
							redisResult = redisDAO.get(redisKeyBuilder.toString());
							status = NOT_STARTED;
							logger.debug("Trial Banner fetched for Redis for {} : {}",redisKeyBuilder.toString(), redisResult);
						}

					}else {
						logger.error("Could not fetch data from StudentOnboarding for user = "+user.getUserId().toString());

					}
				}
				//Subscription to the TRIAL is INACTIVE
				else if(null != trialExpirationResp && null != trialExpirationResp.getStatus()
						&& trialExpirationResp.getStatus().equals(EntityStatus.INACTIVE)){

					logger.debug("User has an inactive subscription for trial -> "+trialExpirationResp);
					redisKeyBuilder.append(EXPIRED);
					redisResult = redisDAO.get(redisKeyBuilder.toString());
					validTill = trialExpirationResp.getValidTill();
					status = EXPIRED;
					logger.debug("Trial Banner fetched for Redis for {} : {}",redisKeyBuilder.toString(), redisResult);
				}
				//Subscription is either will be ENDED or has no enrollment for TRIAL
				else {

					logger.debug("User has no active or inactive subscription for trial -> "+trialExpirationResp);
					redisKeyBuilder.append(NOT_STARTED);
					redisResult = redisDAO.get(redisKeyBuilder.toString());
					status = NOT_STARTED;
					logger.debug("Trial Banner fetched for Redis for {} : {}",redisKeyBuilder.toString(), redisResult);
				}
			}
		}
		if(StringUtils.isNotEmpty(redisResult)) {
			trialBannerResponse.setImageUrl(redisResult);
			trialBannerResponse.setValidTill(validTill);
		}
		trialBannerResponse.setStatus(status);
		logger.debug("trialBannerResponse = "+trialBannerResponse);
		if(null != trialBannerResponse && null != trialBannerResponse.getImageUrl() && null != trialBannerResponse.getStatus()
				&& !trialBannerResponse.getStatus().isEmpty() && !trialBannerResponse.getImageUrl().isEmpty()) {
			listOfTrialBannerRepsonses.add(trialBannerResponse);
		}
		logger.debug("listOfTrialBannerRepsonses -> "+listOfTrialBannerRepsonses);
		return listOfTrialBannerRepsonses;
	}


	private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
		return HomeFeedCard.builder()
				.cardType(CardType.TRIAL_BANNER)
				.build();
	}
}
