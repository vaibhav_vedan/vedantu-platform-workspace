/**
 * 
 */
package com.vedantu.app.homefeed.handlers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.enums.TimeFrame;
import com.vedantu.app.homefeed.converters.SessionToClassConverter;
import com.vedantu.app.homefeed.converters.WebinarToClassConverter;
import com.vedantu.app.homefeed.entity.ClassesResponse;
import com.vedantu.app.homefeed.entity.FindWebinarsReq;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.entity.OTFSessionsReq;
import com.vedantu.app.homefeed.entity.WebinarResponse;
import com.vedantu.app.homefeed.entity.WebinarStatus;
import com.vedantu.app.homefeed.enums.CardType;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.scheduling.response.session.OTFSessionPojoApp;
import com.vedantu.subscription.request.UserPremiumSubscriptionEnrollmentReq;
import com.vedantu.subscription.response.BatchBundlePojo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

/**
 * @author subarna
 *
 */
@Component(value = "LIVE_NOW_CLASSES")
public class LiveNowClassesCardHandler implements HomeFeedCardHandler{

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(LiveNowClassesCardHandler.class);

	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	private FosUtils fosUtils;

	private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
	private static final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
	private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
	private static final String GROWTH_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("GROWTH_ENDPOINT");
	private static final String IS_SUBSCRIPTION_ACTIVE ="ISSUBSCRIPTIONACTIVE";
	private static final String IS_SCHEDULING_ACTIVE = "ISSCHEDULINGACTIVE";
	private static final String IS_PLATFORM_ACTIVE = "ISPLATFORMACTIVE";
	private static final String IS_GROWTH_ACTIVE = "ISGROWTHACTIVE";
	private static final String APP_LIVE_WEBINARS_TO_SESSIONS_RATIO = "APP_LIVE_WEBINARS_TO_SESSIONS_RATIO";

	@Override
	public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList,
			UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException {
		logger.info("[LiveNowClassesCardHandler] Setting Webinar HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
		HomeFeedCard classesHomeFeedCard = new HomeFeedCard();
		classesHomeFeedCard =	getHomeFeedCard(configurationCard);
		List<ClassesResponse> classesResponse = new ArrayList<ClassesResponse>();
		classesResponse = getClassesForUserGradeTarget(user,appVersionCode,restrict);
		if(null != classesResponse && ArrayUtils.isNotEmpty(classesResponse)) {
			classesHomeFeedCard.setHomeFeedCardDataList(classesResponse);
			homeFeedCardList.add(classesHomeFeedCard);
			logger.info("[LiveNowClassesCardHandler] Successfully set {} HomeFeedCardData  for WebinarHomeFeedCard", classesHomeFeedCard.getHomeFeedCardDataList().size());
		}
		logger.debug("[LiveNowClassesCardHandler] Successfully set {} HomeFeedCardData  for WebinarHomeFeedCard", classesHomeFeedCard);
	}

	private List<ClassesResponse> getClassesForUserGradeTarget(UserBasicInfo user, String appVersionCode, boolean restrict) {
		String numberOfWebinarsToFetch = ""; 
		Integer totalCount = 10;
		Integer totalWebinarstoFetch = 6;
		try {
			numberOfWebinarsToFetch = redisDAO.get(APP_LIVE_WEBINARS_TO_SESSIONS_RATIO);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("Error occured while fetching data getClassesForUserGradeTarget from redis -> "+e);
		}
		if(StringUtils.isEmpty(numberOfWebinarsToFetch)) {
			numberOfWebinarsToFetch = "6:4";
		}
		if(null != numberOfWebinarsToFetch && numberOfWebinarsToFetch.contains(":")) {
			String[] classes = numberOfWebinarsToFetch.split(":");
			if(null != classes && classes.length == 2) {
				totalCount = Integer.parseInt(classes[0])+Integer.parseInt(classes[1]);
				totalWebinarstoFetch = Integer.parseInt(classes[0]);
			}
		}
		logger.debug("LiveNowClassesCardHandler totalWebinarstoFetch -> "+totalWebinarstoFetch);
		List<WebinarResponse> webinarResponses = new ArrayList<WebinarResponse>();
		List<OTFSessionPojoApp> sessionResponses = new ArrayList<OTFSessionPojoApp>();
		List<ClassesResponse> classesResponses = new ArrayList<ClassesResponse>();

		FindWebinarsReq findWebinarsReq = new FindWebinarsReq();
		findWebinarsReq.setRestrict(restrict);
		findWebinarsReq.setGrade(user.getGrade());
		findWebinarsReq.setWebinarStatus(WebinarStatus.ACTIVE);
		findWebinarsReq.setCallingUserId(user.getUserId());
		findWebinarsReq.setAppVersionCode(appVersionCode);
		if(StringUtils.isNotEmpty(user.getEmail()))
			findWebinarsReq.setStudentEmail(user.getEmail());
		findWebinarsReq.setWebinarToSessionLimit(totalWebinarstoFetch);
		webinarResponses = getWebinarResponseFromPlatform(findWebinarsReq);
		logger.debug("[LiveNowClassesCardHandler] Found {} webinars from platform", webinarResponses.size());

		if(ArrayUtils.isNotEmpty(webinarResponses)) {
			for(WebinarResponse webinar:webinarResponses) {
				classesResponses.add(WebinarToClassConverter.convert(webinar));
			}
		}
		Integer remainingClassesToFetch = Math.abs(totalCount - webinarResponses.size());
		logger.debug("LiveNowClassesCardHandler remainingClassesToFetch for sessions -> "+remainingClassesToFetch);
		OTFSessionsReq otfSessionsReq = new OTFSessionsReq();
		if(StringUtils.isNotEmpty(user.getGrade()))
			otfSessionsReq.setGrade(user.getGrade());
		if(StringUtils.isNotEmpty(user.getBoard()))
			otfSessionsReq.setBoard(user.getBoard());
		if(StringUtils.isNotEmpty(user.getStream()))
			otfSessionsReq.setStream(user.getStream());
		if(StringUtils.isNotEmpty(user.getTarget()))
			otfSessionsReq.setTarget(user.getTarget());
		otfSessionsReq.setCallingUserId(user.getUserId().toString());
		otfSessionsReq.setCallingUserRole(user.getRole());
		otfSessionsReq.setStart(0);
		otfSessionsReq.setSize(remainingClassesToFetch);
		otfSessionsReq.setTimeFrame(TimeFrame.LIVE);

		sessionResponses = getSessionResponseFromScheduling(otfSessionsReq);
		logger.debug("[LiveNowClassesCardHandler] Found {} sessionResponses from scheduling", sessionResponses.size());
		if(ArrayUtils.isNotEmpty(sessionResponses)) {
			for(OTFSessionPojoApp session: sessionResponses) {
				ClassesResponse classResponse = SessionToClassConverter.convert(session);
				classesResponses.add(classResponse);
			}
		}

		classesResponses.sort(Comparator.comparing(ClassesResponse::getStartTime)
				.reversed()
				.thenComparing(Comparator.comparing(ClassesResponse::getTypeOfClass)
						.thenComparing(Comparator.comparing(ClassesResponse::getSubject)
								.thenComparing(Comparator.comparing(ClassesResponse::getTeacherName))))
				);
		logger.debug("LiveNowClassesCardHandler Found classes for homefeed -> "+classesResponses);
		return classesResponses;
	}

	private List<OTFSessionPojoApp> getSessionResponseFromScheduling(OTFSessionsReq otfSessionsReq) {


		String subscriptionStatus = "";
		//List<OTFSessionResponse> sessions = new ArrayList<OTFSessionResponse>();
		List<BatchBundlePojo> batchBundlePojos = new ArrayList<BatchBundlePojo>();
		List<String> enrollments = new ArrayList<String>();
		List<OTFSessionPojoApp> oTFSessionPojoApp = new ArrayList<OTFSessionPojoApp>();
		try {
			subscriptionStatus = redisDAO.get(IS_SUBSCRIPTION_ACTIVE);
			logger.debug("[LiveNowClassesCardHandler] subscriptionStatus -> "+subscriptionStatus);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("[LiveNowClassesCardHandler] Subscription Module Health Status retrieval from Redis Failed");
		}

		if(StringUtils.isNotEmpty(subscriptionStatus) && subscriptionStatus.equalsIgnoreCase("true")) {

			UserPremiumSubscriptionEnrollmentReq enrolReq = new UserPremiumSubscriptionEnrollmentReq();
			enrolReq.setUserId(otfSessionsReq.getCallingUserId());
			if(StringUtils.isNotEmpty(otfSessionsReq.getGrade()))
				enrolReq.setGrade(otfSessionsReq.getGrade());
			if(StringUtils.isNotEmpty(otfSessionsReq.getBoard()))
				enrolReq.setBoard(otfSessionsReq.getBoard());
			if(StringUtils.isNotEmpty(otfSessionsReq.getStream()))
				enrolReq.setStream(otfSessionsReq.getStream());
			if(StringUtils.isNotEmpty(otfSessionsReq.getTarget()))
				enrolReq.setTarget(otfSessionsReq.getTarget());
			batchBundlePojos = getEnrollmentsDataForUser(enrolReq);
			batchBundlePojos.stream().forEach(b->{
				enrollments.addAll(b.getBatchIds());
			});
			otfSessionsReq.setBatchIds(enrollments);
			String schedulingStatus = "";
			try {
				schedulingStatus = redisDAO.get(IS_SCHEDULING_ACTIVE);
				logger.debug("[LiveNowClassesCardHandler] schedulingStatus -> "+schedulingStatus);
			} catch (InternalServerErrorException | BadRequestException e) {
				logger.error("[LiveNowClassesCardHandler] Scheduling Module Health Status retrieval from Redis Failed");
			}

			if(StringUtils.isNotEmpty(schedulingStatus) && schedulingStatus.equalsIgnoreCase("true")) {

				String url = SCHEDULING_ENDPOINT + "onetofew/session/getUserLiveSessionsForApp";
				String query = new Gson().toJson(otfSessionsReq);
				try {
					ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true, true);

					VExceptionFactory.INSTANCE.parseAndThrowException(resp);

					String jsonString = resp.getEntity(String.class);
					logger.debug(" LiveNowClassesCardHandler Response for getUserLiveSessionsForApp: " + jsonString);
					Type otfSessionListType = new TypeToken<List<OTFSessionPojoApp>>() {
					}.getType();
					oTFSessionPojoApp = new Gson().fromJson(jsonString, otfSessionListType);
					logger.debug("LiveNowClassesCardHandler Response count for sessions:" + oTFSessionPojoApp);

				} catch (VException e) {
					logger.error("Error occured furing fetching data from getSessionResponseFromScheduling ->"+e);
				}

				if(ArrayUtils.isNotEmpty(batchBundlePojos) && ArrayUtils.isNotEmpty(oTFSessionPojoApp)) {
					for(BatchBundlePojo b: batchBundlePojos) {
						for(OTFSessionPojoApp s: oTFSessionPojoApp) {
							if(ArrayUtils.isNotEmpty(b.getBatchIds())) {
								for(String batchId: b.getBatchIds()) {
									if(ArrayUtils.isNotEmpty(s.getBatchIds())) {
										if(s.getBatchIds().contains(batchId)) {
											logger.debug("Going to set bundle batch n bundle title UpcomingClassesCardHandler");
											s.setBundleId(b.getBundleId());
											s.setBatchIds(b.getBatchIds());
											s.setCourseTitle(b.getPremiumBundleTitle());
											if(b.getBatchEnrollmentStateMap().containsKey(batchId)) {
												s.setIsEnrolled(true);
												s.setEnrollmentState(b.getBatchEnrollmentStateMap().get(batchId));
												s.setEnrollmentStatus(b.getBatchEnrollmentStatusMap().get(batchId));
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		logger.debug("LiveNowClassesCardHandler Final oTFSessionPojoApp -> "+oTFSessionPojoApp);
		return oTFSessionPojoApp;

	}

	private List<WebinarResponse> getWebinarResponseFromPlatform(FindWebinarsReq findWebinarsReq)	{		
		List<WebinarResponse> webinarResponse = new ArrayList<WebinarResponse>();
		String growthStatus = "";
		String resString = "";
		try {
			growthStatus = redisDAO.get(IS_GROWTH_ACTIVE);
			logger.debug("platformStatus -> "+growthStatus);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("Platform Module Health Status retrieval from Redis Failed");
		}
		//TODO: Add Growth Subsystem REDIS KEY ON PROD

		if(null != growthStatus && !growthStatus.isEmpty() && growthStatus.equalsIgnoreCase("true")) {
			logger.debug("method=getWebinarResponseFromPlatform, class=LiveNowClassesCardHandler, webinarReq={}", findWebinarsReq);

			String url = GROWTH_ENDPOINT + "cms/webinar/findLiveWebinarsForApp";
			logger.debug("[LiveNowClassesCardHandler] Calling Platform with the URL : {}", url);
			logger.debug("Calling Platform with the URL : {}", url);
			String query = new Gson().toJson(findWebinarsReq);
			try {
				ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true, true);

				VExceptionFactory.INSTANCE.parseAndThrowException(response);

				resString = response.getEntity(String.class);
				logger.debug("LiveNowClassesCardHandler Response for findLiveWebinarsForApp: " + resString);
				Type type = new TypeToken<List<WebinarResponse>>(){}.getType();
				webinarResponse = new Gson().fromJson(resString, type);
			} catch (VException e) {
				logger.error("Error occured during fetching data from getWebinarResponseFromPlatform -> "+e);
			}
		}
		return webinarResponse;
	}



	private List<BatchBundlePojo> getEnrollmentsDataForUser(UserPremiumSubscriptionEnrollmentReq req) {
		List<BatchBundlePojo> batchBundlePojos = new ArrayList<BatchBundlePojo>();
		String subscriptionStatus = "";
		try {
			subscriptionStatus = redisDAO.get(IS_SUBSCRIPTION_ACTIVE);
			logger.debug("[LiveNowClassesCardHandler] subscriptionStatus -> "+subscriptionStatus);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("[LiveNowClassesCardHandler] Subscription Module Health Status retrieval from Redis Failed");
		}

		if(StringUtils.isNotEmpty(subscriptionStatus) && subscriptionStatus.equalsIgnoreCase("true")) {
			String getEnrollmentsUrl = SUBSCRIPTION_ENDPOINT + "enroll/getAppEnrollmentsOfUser";
			String query = new Gson().toJson(req);
			logger.debug("[LiveNowClassesCardHandler] Calling Subscription with the URL : {}", getEnrollmentsUrl);
			try {
				ClientResponse resp = WebUtils.INSTANCE.doCall(getEnrollmentsUrl, HttpMethod.POST, query, true, true);

				VExceptionFactory.INSTANCE.parseAndThrowException(resp);

				String jsonString = resp.getEntity(String.class);
				logger.debug("LiveNowClassesCardHandler Response for getEnrollmentsDataForUser: " + req.getUserId() + "  ->" + jsonString);
				Type listType = new TypeToken<ArrayList<BatchBundlePojo>>() {
				}.getType();
				batchBundlePojos = new Gson().fromJson(jsonString, listType);
			} catch (VException e) {
				logger.error("Error occured during fetching data from getAppEnrollmentsOfUser api -> "+e);
			}
		}
		return batchBundlePojos;
	}


	private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
		return HomeFeedCard.builder()
				.cardType(CardType.LIVE_NOW_CLASSES)
				.serverTime(System.currentTimeMillis())
				.build();
	}

}
