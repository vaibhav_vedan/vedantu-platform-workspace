package com.vedantu.app.homefeed.handlers;

import static com.vedantu.app.homefeed.enums.CardType.SUBJECT_ENTRY;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedCardData;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;

//@Component(value = "SUBJECT_ENTRY")
public class SubjectEntryCardHandler implements HomeFeedCardHandler {
    private Logger LOGGER = LogFactory.getLogger(TestCardHandler.class);

    @Autowired
   // private ChapterListingDAO chapterListingDAO;

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
//        LOGGER.info("Setting up SchoolInfo for userId={}, grade={}", user.getUserId(), user.getSchool());
//        HomeFeedCard subjectEntryCard = getHomeFeedCard();
//
//        List<HomeFeedCardData> subjectEntryCardDataList = new ArrayList<>();
//        addCards(user, subjectEntryCardDataList);
//        subjectEntryCard.setHomeFeedCardDataList(subjectEntryCardDataList);
//
//        try {
//            homeFeedCardList.add(subjectEntryCard);
//        } catch (Exception e) {
//            LOGGER.error("SchoolInfo for Homefeed not found for email={}", user.getEmail());
//        }
    }

    private void addCards(UserBasicInfo user, List<HomeFeedCardData> recentActivityCardDataList) {
//        LOGGER.info("method=addCard, class=RecentActivityCardHandler, userId={}", user.getUserId());
//
//        if(StringUtils.isNotEmpty(user.getGrade())) {
//            List<String> mathsChapters = chapterListingDAO.getChaptersBySubject("Maths");
//            List<String> scienceChapters = chapterListingDAO.getChaptersBySubject("Science");
////            List<String> physicsChapters = chapterListingDAO.getChaptersBySubject("Physics");
////            List<String> chemistryChapters = chapterListingDAO.getChaptersBySubject("Chemistry");
////            List<String> biologyChapters = chapterListingDAO.getChaptersBySubject("Biology");
//            recentActivityCardDataList.add(new SubjectDetailsSubjectEntry("Maths", mathsChapters.size(), "/static/images/react-app/coursecard/maths.svg"));
//            recentActivityCardDataList.add(new SubjectDetailsSubjectEntry("Science", scienceChapters.size(), "/static/images/react-app/coursecard/science.svg"));
////            recentActivityCardDataList.add(new SubjectDetailsSubjectEntry("Physics", physicsChapters.size(), "/static/images/react-app/coursecard/physics.svg"));
////            recentActivityCardDataList.add(new SubjectDetailsSubjectEntry("Chemistry", chemistryChapters.size(), "/static/images/react-app/coursecard/chemistry.svg"));
////            recentActivityCardDataList.add(new SubjectDetailsSubjectEntry("Biology", biologyChapters.size(), "/static/images/react-app/coursecard/biology.svg"));
//        }
    }

    private HomeFeedCard getHomeFeedCard() {
        return HomeFeedCard.builder()
                .cardType(SUBJECT_ENTRY)
                .sectionTitle("Learn by subject")
                .build();
    }
}