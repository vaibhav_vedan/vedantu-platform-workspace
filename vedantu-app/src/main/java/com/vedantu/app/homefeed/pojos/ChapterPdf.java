/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.app.homefeed.pojos;

/**
 *
 * @author ajith
 */
public class ChapterPdf {

    private String chapterTitle;
    private String pdfLink;
    private String seoUrl;
    private String id;
    private boolean bookmark;// will only be used in response -- hackish way

    public String getChapterTitle() {
        return chapterTitle;
    }

    public void setChapterTitle(String chapterTitle) {
        this.chapterTitle = chapterTitle;
    }

    public String getPdfLink() {
        return pdfLink;
    }

    public void setPdfLink(String pdfLink) {
        this.pdfLink = pdfLink;
    }

    public String getSeoUrl() {
        return seoUrl;
    }

    public void setSeoUrl(String seoUrl) {
        this.seoUrl = seoUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isBookmark() {
        return bookmark;
    }

    public void setBookmark(boolean bookmark) {
        this.bookmark = bookmark;
    }

    @Override
    public String toString() {
        return "ChapterPdf{" + "chapterTitle=" + chapterTitle + ", pdfLink=" + pdfLink + ", seoUrl=" + seoUrl + ", id=" + id + ", bookmark=" + bookmark + '}';
    }

}
