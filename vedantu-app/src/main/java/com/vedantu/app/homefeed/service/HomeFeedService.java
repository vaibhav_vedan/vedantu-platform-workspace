package com.vedantu.app.homefeed.service;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.enums.PublishType;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.app.homefeed.response.HomeFeedCardOrderList;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.util.enums.RequestSource;

public interface HomeFeedService {

    List<HomeFeedCard> getHomeFeedResponseForUser(UserBasicInfo user, String appVersionCode, RequestSource requestSource, boolean restrict) throws VException;

    HomeFeedCardOrderList configureHomeFeed(HomeFeedCardOrderList homeFeedCardOrderList) throws BadRequestException;

    HomeFeedCardOrderList fetchCurrentOrdering(String userId, PublishType publishType) throws VException;
//
//    PlatformBasicResponse addHomeFeedCardToGrades(HFCardForMultipleGrades hfCardForMultipleGrades);
//
//    List<HomeFeedCard> getHomeFeedForWeb(UserBasicInfo user, String grade, boolean restrict) throws VException;
//
//    boolean allowedToPublish(String grade, CardType cardType);
//
//    void updateLiveQuizScheduleService(List<LiveQuizSchedule> liveQuizScheduleList);
//
//    LiveQuizScheduleRes getTodaysLiveQuizScheduleService() throws VException;
}
