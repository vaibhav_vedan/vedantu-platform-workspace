package com.vedantu.app.homefeed.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.vedantu.app.homefeed.entity.HomeFeedCardOrderEntity;
import com.vedantu.app.homefeed.response.HomeFeedCardOrderList;

@Component
public class HomeFeedEntityToResponseConverter implements Converter<HomeFeedCardOrderEntity, HomeFeedCardOrderList> {
    public HomeFeedCardOrderList convert(HomeFeedCardOrderEntity entity) {
        return HomeFeedCardOrderList.builder()
                .id(entity.getId())
                .grade(entity.getGrade())
                .homeFeedConfigurationCards(entity.getHomeFeedConfigurationCards())
                .createdBy(entity.getCreatedBy())
                .creationTime(entity.getCreationTime())
                .lastUpdated(entity.getLastUpdated())
                .lastUpdatedBy(entity.getLastUpdatedBy())
                .build();
    }
}
