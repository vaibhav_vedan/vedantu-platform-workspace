/**
 * 
 */
package com.vedantu.app.homefeed.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class TrialBannerResponse implements HomeFeedCardData{

    private String id;
    private String copyText;
    private String ctaText;
    private String status;
    private String imageUrl;
    private String redirectLink;
    private Long validTill;
}
