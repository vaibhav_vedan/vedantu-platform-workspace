package com.vedantu.app.homefeed.enums;

public enum SectionType
{
    HOMEPAGE("HOMEPAGE"),
    COURSES("COURSES");

    String value;

    SectionType(String value)
    {
        this.value = value;
    }

    public String getValue()
    {
        return this.value;
    }
}
