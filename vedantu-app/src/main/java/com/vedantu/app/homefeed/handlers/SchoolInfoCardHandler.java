package com.vedantu.app.homefeed.handlers;

import static com.vedantu.app.homefeed.enums.CardType.SCHOOL_INFO;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;

@Component(value = "SCHOOL_INFO")
public class SchoolInfoCardHandler implements HomeFeedCardHandler {
    private Logger LOGGER = LogFactory.getLogger(TestCardHandler.class);

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
        LOGGER.info("Setting up SchoolInfo for userId={}, grade={}", user.getUserId(), user.getSchool());
        HomeFeedCard schoolInfoCard = new HomeFeedCard();
        schoolInfoCard = getHomeFeedCard();
        try {
            if(StringUtils.isEmpty(user.getSchool())) {
                homeFeedCardList.add(schoolInfoCard);
            }
        } catch (Exception e) {
            LOGGER.error("SchoolInfo for Homefeed not found for email={}", user.getEmail());
        }
    }

    private HomeFeedCard getHomeFeedCard() {
        return HomeFeedCard.builder()
                .cardType(SCHOOL_INFO)
                .build();
    }
}

