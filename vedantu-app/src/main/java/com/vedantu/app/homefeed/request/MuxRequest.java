package com.vedantu.app.homefeed.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MuxRequest
{
    private String input;
    private String playback_policy;
}
