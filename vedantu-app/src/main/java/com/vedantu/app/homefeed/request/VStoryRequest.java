package com.vedantu.app.homefeed.request;

import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.vedantu.app.homefeed.enums.SectionType;
import com.vedantu.app.homefeed.enums.StoryType;
import com.vedantu.app.homefeed.pojos.ImageDetails;
import com.vedantu.app.homefeed.pojos.VideoDetails;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VStoryRequest
{
    @NotNull
    @Size(max = ReqLimits.VSTORY_TITLE_MAX, message = ReqLimitsErMsgs.VSTORY_TITLE_MAX)
    private String title;

    @NotNull
    private Set<String> grades;

    private Set<String> targets;

    private Set<String> mainTags;

    @NotNull
    private StoryType storyType;

    @NotNull
    private SectionType sectionType;

    private ImageDetails imageDetails;

    private VideoDetails videoDetails;

    @Size(max = ReqLimits.VSTORY_TITLE_MAX, message = ReqLimitsErMsgs.VSTORY_TITLE_MAX)
    private String ctaTitle;

    private String ctaDeepLink;

    @NotNull
    private Long startTime;

    @NotNull
    private Long endTime;

    private boolean enableAutoRun;
}
