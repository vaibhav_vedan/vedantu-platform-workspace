package com.vedantu.app.homefeed.handlers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.enums.CardType;
import com.vedantu.app.homefeed.enums.RequestType;
import com.vedantu.app.homefeed.enums.SectionType;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.app.homefeed.response.VStoryResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Component("VSTORIES")
public class VStoriesCardHandler implements HomeFeedCardHandler
{
	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(VStoriesCardHandler.class);

	@Autowired
	private RedisDAO redisDAO;

	private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
	private static final String IS_LMS_ACTIVE = "ISLMSACTIVE";

	@Override
	public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException
	{
		logger.info("Setting VStories card for homefeed for grade : {}, userId : {}", user.getGrade(), user.getUserId());
		HomeFeedCard vStoryHomeFeedCard = new HomeFeedCard();
		vStoryHomeFeedCard = getHomeFeedCard(configurationCard);
		List<VStoryResponse> vStoryResponseList = getVStoriesForHomeFeed(user.getGrade(), user.getTarget());
		logger.debug("VStories fetched : {}", vStoryResponseList);
		if (vStoryResponseList != null && vStoryResponseList.size() > 0)
		{
			vStoryHomeFeedCard.setHomeFeedCardDataList(vStoryResponseList);
			homeFeedCardList.add(vStoryHomeFeedCard);
			logger.info("Successfully set {} VStories  for VStoryHomeFeedCard", vStoryHomeFeedCard.getHomeFeedCardDataList().size());
		}
	}

	private List<VStoryResponse> getVStoriesForHomeFeed(String grade, String target) throws VException{
		logger.info("Getting VStories for grade :{}, target : {}", grade, target);
		List<VStoryResponse> vStoryResponse = new ArrayList<VStoryResponse>();
		try {
			if (StringUtils.isNotEmpty(grade)){
				String responseString = "";
				String lmsStatus = "";

				String vStoriesKey = "VSTORIES_" +SectionType.HOMEPAGE.getValue()+ "_" +grade;
				String res = "";
				try
				{
					res = redisDAO.get(vStoriesKey);
				}
				catch (VException e)
				{
					logger.error("Could not get Vstories from Redis due to : {}", e.toString());
				}


				Type type = new TypeToken<List<VStoryResponse>>() {
				}.getType();

				if(StringUtils.isEmpty(res)) {
					try {
						lmsStatus = redisDAO.get(IS_LMS_ACTIVE);
						logger.debug("lmsStatus -> "+lmsStatus);
					} catch (InternalServerErrorException | BadRequestException e) {
						logger.error("LMS Module Health Status retrieval from Redis Failed");
					}

					if(StringUtils.isNotEmpty(lmsStatus) && lmsStatus.equalsIgnoreCase(String.valueOf(Boolean.TRUE))) {
						String url = LMS_ENDPOINT + "VStory/fetchCurrentVStories?grade=" +grade+ "&requestType=" +RequestType.APP+ "&sectionType=" +SectionType.HOMEPAGE;
						ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true, true);
						VExceptionFactory.INSTANCE.parseAndThrowException(response);
						responseString = response.getEntity(String.class);
						logger.debug("fetchCurrentVStories called in lms for grade "+grade+" -> "+responseString.toString());
						vStoryResponse = new Gson().fromJson(responseString, type);
						logger.debug("VStories fetched from lms for grade "+grade+" -> "+vStoryResponse);

						try
						{
							redisDAO.setex(vStoriesKey, new Gson().toJson(vStoryResponse), 600);
						}
						catch (VException e)
						{
							logger.error("Could not set Vstories in Redis due to : {}", e.toString());
						}

					}else {
						logger.error("VStories fetched from lms could not be fetched for grade -> " +grade);
					}
				}else {
					vStoryResponse = new Gson().fromJson(res, type);
					logger.debug("in else vStoryResponse ->"+vStoryResponse);
				}
			}
		}catch(Exception e) {
			logger.error("Error occured while fetching VStories -> "+e);
		}

		return vStoryResponse;
	}

	private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
		return HomeFeedCard.builder()
				.sectionTitle(configurationCard.getSectionTitle())
				.cardType(CardType.VSTORIES)
				.build();
	}
}
