package com.vedantu.app.homefeed.handlers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationSubCard;
import com.vedantu.app.homefeed.enums.CardType;
import com.vedantu.app.homefeed.enums.SubCardType;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.util.LogFactory;


@Component("NEW_RELEASE")
public class NewReleaseCardHandler implements HomeFeedCardHandler {
    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(NewReleaseCardHandler.class);

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws NotFoundException, VException {
        HomeFeedCard liveQuizHomeFeedCard = new HomeFeedCard();
        liveQuizHomeFeedCard = getHomeFeedCard(configurationCard,appVersionCode);
        homeFeedCardList.add(liveQuizHomeFeedCard);
    }


    //    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
//        return HomeFeedCard.builder()
//                .cardType(configurationCard.getCardType())
//                .homeFeedCardDataList(configurationCard.getSubCards())
//                .sectionTitle(configurationCard.getSectionTitle())
//                .build();
//    }
    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard, String appVersionCode) {
    	String versionToShowVgroups = "1.7.3";
    	HomeFeedConfigurationSubCard vGroupCard;
    	HomeFeedConfigurationSubCard vQuizCard = new HomeFeedConfigurationSubCard(SubCardType.V_QUIZ.toString(),SubCardType.V_QUIZ,true);
    	if(appVersionCode.compareTo(versionToShowVgroups) >= 0){
    		vGroupCard = new HomeFeedConfigurationSubCard(SubCardType.V_GROUP.toString(),SubCardType.V_GROUP,false);
    	}else {
    		vGroupCard = new HomeFeedConfigurationSubCard(SubCardType.V_GROUP.toString(), SubCardType.V_GROUP, false);
    	}
    	List<HomeFeedConfigurationSubCard> homeFeedConfigurationSubCards = new ArrayList<>(Arrays.asList(vQuizCard,vGroupCard));
    	return HomeFeedCard.builder()
    			.cardType(CardType.NEW_RELEASE)
    			.homeFeedCardDataList(homeFeedConfigurationSubCards)
    			.sectionTitle("New Release")
    			.build();
    }
}
