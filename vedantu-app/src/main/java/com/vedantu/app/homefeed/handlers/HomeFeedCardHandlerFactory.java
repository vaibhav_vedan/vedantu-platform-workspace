package com.vedantu.app.homefeed.handlers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.vedantu.app.homefeed.enums.CardType;

import static com.vedantu.app.homefeed.enums.CardType.*;

import java.util.HashMap;
import java.util.Map;

@Component
public class HomeFeedCardHandlerFactory {

    @Autowired
    private Map<String, HomeFeedCardHandler> handlerMap;

    public HomeFeedCardHandler getHandler(CardType cardType) {
        return handlerMap.get(cardType.getValue());
    }
}
