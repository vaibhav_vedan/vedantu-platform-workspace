package com.vedantu.app.homefeed.entity;

public enum WebinarType {
    LIVECLASS, QUIZ, WEBINAR
}
