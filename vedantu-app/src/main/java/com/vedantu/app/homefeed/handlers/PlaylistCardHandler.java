package com.vedantu.app.homefeed.handlers;

import static com.vedantu.util.DateTimeUtils.SECONDS_PER_DAY;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.util.LogFactory;

//@Component("PLAYLIST_CARD")
public class PlaylistCardHandler implements HomeFeedCardHandler {

    private static final int TWO_DAYS = 2 * SECONDS_PER_DAY;
    private Logger LOGGER = LogFactory.getLogger(PlaylistCardHandler.class);
//    private CMDSVideoManager videoManager;
//    private RedisDAO redisDAO;
//
//    @Autowired
//    public PlaylistCardHandler(CMDSVideoManager videoManager, RedisDAO redisDAO) {
//        this.videoManager = videoManager;
//        this.redisDAO = redisDAO;
//    }

    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {
//        String grade = user.getGrade();
//        Gson gson = new Gson();
//        LOGGER.info("Setting up HomeFeedPlaylistCard data for userId={}, grade={}", user.getUserId(), grade);
//        HomeFeedCard homeFeedPlaylistCard = getHomeFeedCard(configurationCard);
//        String contextId = configurationCard.getContextId();
//        String playListRedisKey = HOMEFEED_PLAYLIST_CARD_DATA + grade + "_" + contextId;
//        try {
//            String playlistCardString = redisDAO.get(playListRedisKey);
//            if (StringUtils.isNotEmpty(playlistCardString)) {
//                CMDSVideoPlaylist playlistCardData = gson.fromJson(playlistCardString, CMDSVideoPlaylist.class);
//                homeFeedPlaylistCard.setHomeFeedCardData(playlistCardData);
//                homeFeedCardList.add(homeFeedPlaylistCard);
//                LOGGER.info("Successfuly set HomeFeed PlaylistCardData using redis for grade={}", grade);
//                return;
//            }
//            homeFeedPlaylistCard.setHomeFeedCardData(videoManager.getCMDSVideoPlaylistById(contextId,user.getUserId()));
//            homeFeedCardList.add(homeFeedPlaylistCard);
//            String playlistCardRedisString = gson.toJson(homeFeedPlaylistCard.getHomeFeedCardData());
//            LOGGER.info("Setting redis key={} and value={}", playListRedisKey, playlistCardRedisString);
//            redisDAO.setex(playListRedisKey, playlistCardRedisString, TWO_DAYS);
//        } catch (NotFoundException e) {
//            LOGGER.error("Error in getting PlaylistCard for playlistId={}, userId={}, grade={}.", contextId, user.getUserId(), grade);
//        } catch (BadRequestException | InternalServerErrorException e) {
//            LOGGER.error("Error in getting or setting redis Key={}", playListRedisKey);
//        } catch (Exception e) {
//            LOGGER.error("Error building Playlist card in homefeed for userId={}, grade={}", user.getUserId(), grade);
//        }
    }

    private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
        return HomeFeedCard.builder()
                .cardType(configurationCard.getCardType())
                .title(configurationCard.getTitle())
                .sectionTitle(configurationCard.getSectionTitle())
                .description(configurationCard.getDescription())
                .build();
    }
}
