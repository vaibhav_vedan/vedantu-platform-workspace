package com.vedantu.app.homefeed.handlers;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.entity.NCERTResponse;
import com.vedantu.app.homefeed.entity.StudyEntry;
import com.vedantu.app.homefeed.enums.CardType;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

@Component(value = "NCERT_SOLUTIONS")
public class NCERTSolutionsHandler implements HomeFeedCardHandler
{
	@Autowired
	private LogFactory logFactory;

	@Autowired
	private RedisDAO redisDAO;

	@SuppressWarnings("static-access")
	Logger logger = logFactory.getLogger(NCERTSolutionsHandler.class);

	private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
	private static final String IS_LMS_ACTIVE = "ISLMSACTIVE";

	@Override
	public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) throws VException
	{
		logger.info("Setting NCERTSolutions HomeFeedCard Data for UserId = {}, Grade = {}", user.getUserId(), user.getGrade());
		HomeFeedCard ncertSolutionsCard = new HomeFeedCard();
		ncertSolutionsCard = getHomeFeedCard(configurationCard);
		try
		{
			List<String> subjectTitles = Arrays.asList(configurationCard.getContextUrls());
			List<StudyEntry> studyEntries = new ArrayList<StudyEntry>();
			StudyEntry NCERTEntry = new StudyEntry();

			String responseString = "";
			String lmsStatus = "";
			String key = "NCERT_SOLUTIONS_GRADE_" + user.getGrade();
			String res = "";
			String resString = "";
			List<NCERTResponse> ncertResponses = new ArrayList<NCERTResponse>();

			try
			{
				res = redisDAO.get(key);
				logger.debug("NCERTSolutions res -> "+res);
			}
			catch (VException e)
			{
				logger.error("Could not get NCERTSolutions from Redis due to : {}", e.toString());
			}

			Type type = new TypeToken<List<StudyEntry>>() {
			}.getType();

			if(StringUtils.isEmpty(res)) {

				try {
					lmsStatus = redisDAO.get(IS_LMS_ACTIVE);
					logger.debug("lmsStatus -> "+lmsStatus);
				} catch (InternalServerErrorException | BadRequestException e) {
					logger.error("LMS Module Health Status retrieval from Redis Failed");
				}

				if(StringUtils.isNotEmpty(lmsStatus) && lmsStatus.equalsIgnoreCase(String.valueOf(Boolean.TRUE))) {
					String url = LMS_ENDPOINT + "study/getStudyEntriesForNCERT?grade=" +user.getGrade();
					logger.debug("method=getStudyEntriesForNCERT, class=NCERTSolutionsHandler, subjectTitles={}", subjectTitles);
					String query = new Gson().toJson(subjectTitles);
					ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, query, true, true);
					VExceptionFactory.INSTANCE.parseAndThrowException(response);
					responseString = response.getEntity(String.class);
					logger.debug("studyEntries called in lms for grade "+user.getGrade()+" -> "+responseString.toString());
					studyEntries = new Gson().fromJson(responseString, type);
					logger.debug("studyEntries fetched from lms for grade "+user.getGrade()+" -> "+studyEntries);

				}else {
					logger.error("studyEntries fetched from lms could not be fetched for grade -> " +user.getGrade());
				}

				logger.info("Found {} studyEntries from lms", studyEntries.size());
				if(ArrayUtils.isNotEmpty(studyEntries)) {
					NCERTEntry = studyEntries.get(0);
				}

				for (int i = 0; i < configurationCard.getContextUrls().length; i++)
				{
					NCERTResponse ncertResponse = new NCERTResponse();
					ncertResponse.setId(NCERTEntry.getId());
					ncertResponse.setSectionTitle(configurationCard.getContextUrls()[i]);
					ncertResponse.setThumbnail(configurationCard.getThumbnailArray()[i]);
					ncertResponses.add(ncertResponse);
				}

				logger.debug("ncertResponses -> "+ncertResponses);
				try
				{
					if(ArrayUtils.isNotEmpty(ncertResponses)) {
						resString = new Gson().toJson(ncertResponses);
						redisDAO.setex(key, resString, 3600);
					}
				}
				catch (VException e)
				{
					logger.error("Could not set NCERTSolutions in Redis due to : {}", e.toString());
				}
			}else {

				logger.debug("NCERT SOULTIONS found in Redis for grade: "+user.getGrade()+" -> "+res);
				Type typeToken = new TypeToken<List<NCERTResponse>>(){}.getType();
				ncertResponses = new Gson().fromJson(res, typeToken);
				logger.debug("in else ncertResponses ->"+ncertResponses);

			}

			if(ArrayUtils.isNotEmpty(ncertResponses)) {
				ncertSolutionsCard.setHomeFeedCardDataList(ncertResponses);
			}
			logger.info("Successfully set {} HomeFeedCardData  for NCERT HomeFeedCard", ncertSolutionsCard.getHomeFeedCardDataList().size());
			if(null != ncertSolutionsCard && ArrayUtils.isNotEmpty(ncertSolutionsCard.getHomeFeedCardDataList())) {
				homeFeedCardList.add(ncertSolutionsCard);
			}
		}
		catch (Exception e)
		{
			logger.error("Could not get StudyEntry from Db for userId : {}, grade : {} due to exception : {}", user.getUserId(), user.getGrade(), e);
		}
	}

	private HomeFeedCard getHomeFeedCard(HomeFeedConfigurationCard configurationCard) {
		return HomeFeedCard.builder()
				.cardType(CardType.NCERT_SOLUTIONS)
				.sectionTitle(configurationCard.getSectionTitle())
				.build();
	}
}
