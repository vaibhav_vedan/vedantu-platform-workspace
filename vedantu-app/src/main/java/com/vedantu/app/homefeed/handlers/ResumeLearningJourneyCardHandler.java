package com.vedantu.app.homefeed.handlers;

import java.util.List;

import org.springframework.stereotype.Component;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.response.HomeFeedCard;

@Component("RESUME_LEARNING_JOURNEY")
public class ResumeLearningJourneyCardHandler implements HomeFeedCardHandler {
    @Override
    public void handle(HomeFeedConfigurationCard configurationCard, List<HomeFeedCard> homeFeedCardList, UserBasicInfo user, String appVersionCode, boolean restrict) {

    }
}
