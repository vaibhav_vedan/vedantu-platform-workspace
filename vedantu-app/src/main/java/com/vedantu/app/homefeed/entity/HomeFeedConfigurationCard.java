package com.vedantu.app.homefeed.entity;

import java.util.List;

import com.vedantu.app.homefeed.enums.CardType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class HomeFeedConfigurationCard {
    private Long id;
    private String title;
    private String description;
    private String contextId;
    private String[] contextUrls;
    private CardType cardType;
    private String subTitle;
    private String clickToAction;
    private String[] redirectDeepLinkList;
    private String sectionTitle;
    private String thumbnail;
    private boolean isPublished;
    private String[] thumbnailArray;
    private String[] thumbnailArrayWeb;
    private List<HomeFeedConfigurationSubCard> subCards;

}
