package com.vedantu.app.homefeed.service;

import static com.vedantu.app.homefeed.enums.CardType.BATCHES;
import static com.vedantu.app.homefeed.enums.CardType.DOUBT;
import static com.vedantu.app.homefeed.enums.CardType.LIVE_NOW_CLASSES;
import static com.vedantu.app.homefeed.enums.CardType.SINGLE_BANNER;
import static com.vedantu.app.homefeed.enums.CardType.TRIAL_BANNER;
import static com.vedantu.app.homefeed.enums.CardType.UPCOMING_CLASSES;
import static com.vedantu.app.homefeed.enums.CardType.UPCOMING_WEBINARS;
import static com.vedantu.app.homefeed.enums.CardType.VSTORIES;
import static com.vedantu.app.homefeed.enums.PublishType.PUBLISHED;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.app.homefeed.converters.HomeFeedCardOrderToEntityConverter;
import com.vedantu.app.homefeed.converters.HomeFeedEntityToResponseConverter;
import com.vedantu.app.homefeed.entity.HomeFeedConfigurationCard;
import com.vedantu.app.homefeed.enums.CardType;
import com.vedantu.app.homefeed.enums.PublishType;
import com.vedantu.app.homefeed.handlers.HomeFeedCardHandler;
import com.vedantu.app.homefeed.handlers.HomeFeedCardHandlerFactory;
import com.vedantu.app.homefeed.redis.RedisDAO;
import com.vedantu.app.homefeed.response.HomeFeedCard;
import com.vedantu.app.homefeed.response.HomeFeedCardOrderList;
import com.vedantu.app.homefeed.utils.HomeFeedRedisService;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.response.PremiumSubscriptionResp;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.RequestSource;

@Service
public class HomeFeedServiceImpl implements HomeFeedService {

	@Autowired
	private RedisDAO redisDAO;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = LogFactory.getLogger(HomeFeedServiceImpl.class);


	private HomeFeedCardOrderToEntityConverter toEntityConverter;
	//private HomeFeedDao homeFeedDao;
	private HomeFeedEntityToResponseConverter toResponseConverter;
	private HomeFeedCardHandlerFactory handlerFactory;


	// private HomeFeedRedisService homeFeedRedisService;


	@Autowired
	public HomeFeedServiceImpl(HomeFeedCardOrderToEntityConverter toEntityConverter,// HomeFeedDao homeFeedDao,
			HomeFeedEntityToResponseConverter toResponseConverter,
			HomeFeedCardHandlerFactory handlerFactory,
			HomeFeedRedisService homeFeedRedisService) {
		this.toEntityConverter = toEntityConverter;
		//this.homeFeedDao = homeFeedDao;
		this.toResponseConverter = toResponseConverter;
		this.handlerFactory = handlerFactory;
		//this.homeFeedRedisService = homeFeedRedisService;
	}

	private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
	private static final String SUBSCRIPTION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
	private static final String IS_LMS_ACTIVE = "ISLMSACTIVE";
	private static final String IS_SUBSCRIPTION_ACTIVE ="ISSUBSCRIPTIONACTIVE";
	private static final String APP_CONFIG_CARDS_ORDER_FOR_GRADE = "APP_CONFIG_CARDS_ORDER_FOR_GRADE_";
	private static final String NEW_MODULE_APP_VERSION = "1.7.4";
	private static final String SHOW_DOUBT = "SHOW_DOUBT";
	private static final String MASTERTALK_BANNER = "MASTERTALK_BANNER";
	private static final String MASTERTALK_BANNER_REDIRECTION = "MASTERTALK_BANNER_REDIRECTION";
	private static final String PLAY_BANNER = "PLAY_BANNER";
	private static final String SHOW_TRIAL_BANNER = "SHOW_TRIAL_BANNER";
	private static final String SHOW_VSTORIES = "SHOW_VSTORIES";
	private static final String SHOW_UPCOMING_WEBINARS = "SHOW_UPCOMING_WEBINARS";
	private static final String SHOW_NEW_RELEASE = "SHOW_NEW_RELEASE";
	private static final String SHOW_MICROCOURSES = "SHOW_MICROCOURSES";
	private static final String SHOW_NCERT_SOLUTIONS = "SHOW_NCERT_SOLUTIONS";
	private static final String SHOW_MCR_COLLECTION = "SHOW_MCR_COLLECTION";
	private static final String SHOW_BANNER_LIST = "SHOW_BANNER_LIST";
	private static final String SHOW_SINGLE_BANNER = "SHOW_SINGLE_BANNER";
	private static final String SHOW_BATCHES = "SHOW_BATCHES";
	private static final String SHOW_LIVE_NOW_CLASSES = "SHOW_LIVE_NOW_CLASSES";
	private static final String SHOW_UPCOMING_CLASSES = "SHOW_UPCOMING_CLASSES";
	private static final String SHOW_SINGLE_BANNER_ON_TOP = "SHOW_SINGLE_BANNER_ON_TOP";
	private static final String SECTION_TITLE_FOR_TOP_BANNER = "SECTION_TITLE_FOR_TOP_BANNER";
	private static final String IMAGE_URL_FOR_TOP_BANNER = "IMAGE_URL_FOR_TOP_BANNER";
	private static final String REDIRECTION_URL_FOR_TOP_BANNER = "REDIRECTION_URL_FOR_TOP_BANNER";
	private static final String TOP_BANNER_GRADES = "TOP_BANNER_GRADES";
	private static final String TOP_BANNER_VERSION_AND_ABOVE = "TOP_BANNER_VERSION_AND_ABOVE";


	@Override
	public List<HomeFeedCard> getHomeFeedResponseForUser(UserBasicInfo user, String appVersionCode, RequestSource requestSource, boolean restrict) throws VException {
		logger.info("method=getHomeFeedResponseForUser, userId={}", user.getUserId());

		String grade = user.getGrade();
		List<HomeFeedConfigurationCard> configCards = new ArrayList<HomeFeedConfigurationCard>();
		configCards = fetchCurrentOrdering(grade, PUBLISHED).getHomeFeedConfigurationCards();
		List<CardType> cardTypes = new ArrayList<CardType>();
		cardTypes =	configCards.stream()
				.map(configCard -> CardType.getCardTypeForString(configCard.getCardType().getValue()))
				.collect(Collectors.toList());
		logger.info("CardTypes requested by user={}, cardTypes={}", user.getUserId(), cardTypes);
		List<HomeFeedCard> homeFeedCardList = new ArrayList<>();

		Boolean isPremiumSubscriber = false;
		if((getRedisValueForKey(SHOW_LIVE_NOW_CLASSES).equalsIgnoreCase("true") || getRedisValueForKey(SHOW_UPCOMING_CLASSES).equalsIgnoreCase("true")
				|| getRedisValueForKey(SHOW_BATCHES).equalsIgnoreCase("true")) && appVersionCode.compareTo(NEW_MODULE_APP_VERSION) >= 0) {
			isPremiumSubscriber	= isPremiumSubscriber(user.getUserId(), user.getGrade(), user.getBoard(), user.getStream(), user.getTarget());
		}
		logger.info("isPremiumSubscriber for user -> "+user.getUserId()+ " : " +isPremiumSubscriber);
		if(requestSource != null && !RequestSource.IOS.equals(requestSource)) {
			if(getRedisValueForKey(SHOW_DOUBT).equalsIgnoreCase("true")) {
				handlerFactory.getHandler(DOUBT).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
			}
		}

		String VERSION2 = "1.7.0";

		boolean isBelowSixthGrade = false;

		if (StringUtils.isNotEmpty(grade))
		{
			isBelowSixthGrade = grade.equalsIgnoreCase("1") || grade.equalsIgnoreCase("2") || grade.equalsIgnoreCase("3") || grade.equalsIgnoreCase("4")
					|| grade.equalsIgnoreCase("5");
		}


		if (appVersionCode.compareTo(VERSION2) >= 0 || RequestSource.IOS.equals(requestSource))
		{
						if(StringUtils.isNotEmpty(grade) && !isBelowSixthGrade && appVersionCode.compareTo(NEW_MODULE_APP_VERSION) < 0) {
							if (getRedisValueForKey(SHOW_TRIAL_BANNER).equalsIgnoreCase("true") && !RequestSource.IOS.equals(requestSource)) {
								handlerFactory.getHandler(TRIAL_BANNER).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
							}
						}

			if (getRedisValueForKey(SHOW_VSTORIES).equalsIgnoreCase("true")) {
				handlerFactory.getHandler(VSTORIES).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
			}

			if(getRedisValueForKey(SHOW_SINGLE_BANNER_ON_TOP).equalsIgnoreCase("true") && appVersionCode.compareToIgnoreCase(getRedisValueForKey(TOP_BANNER_VERSION_AND_ABOVE))>=0) {
				HomeFeedConfigurationCard configurableSingleBanner = getConfigurableSingleBanner();
				String[] targetGrades = getRedisValueForKey(TOP_BANNER_GRADES).split(",");
				if(null != targetGrades && targetGrades.length>0)
				for(String s:targetGrades) {
					if(s.equalsIgnoreCase(grade)) {
						handlerFactory.getHandler(SINGLE_BANNER).handle(configurableSingleBanner, homeFeedCardList, user, appVersionCode, restrict);
						break;
					}
				}
			}


			if(StringUtils.isNotEmpty(grade) && !isBelowSixthGrade && appVersionCode.compareTo(NEW_MODULE_APP_VERSION) >= 0 && isPremiumSubscriber) {
				if(getRedisValueForKey(SHOW_LIVE_NOW_CLASSES).equalsIgnoreCase("true")) {
					logger.debug("Going to call live now & upcoming classes card handler------->");
					handlerFactory.getHandler(LIVE_NOW_CLASSES).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
				}
				if(getRedisValueForKey(SHOW_UPCOMING_CLASSES).equalsIgnoreCase("true")) {
					handlerFactory.getHandler(UPCOMING_CLASSES).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
				}
			} else {
				if(!isBelowSixthGrade && getRedisValueForKey(SHOW_UPCOMING_WEBINARS).equalsIgnoreCase("true")) 
					handlerFactory.getHandler(UPCOMING_WEBINARS).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
			}

////Commenting masterTalk banner as next masterTalk is not in pipeline
//			String defaultRedirectionLink = "mastertalks/learn-the-art-of-winning-with-pv-sindhu?doubtApp=true";
//			HomeFeedConfigurationCard masterTalkConfig = getSingleBannerRedisConfigCard("MasterTalks",MASTERTALK_BANNER,"https://vmkt.s3-ap-southeast-1.amazonaws.com/Banner/PV+sindhu.jpg",MASTERTALK_BANNER_REDIRECTION,defaultRedirectionLink,true);
//			String redirectionUrl = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT")+ masterTalkConfig.getRedirectDeepLinkList()[0];
//			masterTalkConfig.setRedirectDeepLinkList(new String[]{redirectionUrl});
//			if(getRedisValueForKey(SHOW_SINGLE_BANNER).equalsIgnoreCase("true") && !isBelowSixthGrade) {
//				handlerFactory.getHandler(SINGLE_BANNER).handle(masterTalkConfig, homeFeedCardList, user, appVersionCode, restrict);
//			}


			if(StringUtils.isNotEmpty(grade) && !isBelowSixthGrade && appVersionCode.compareTo(NEW_MODULE_APP_VERSION) >= 0 && getRedisValueForKey(SHOW_BATCHES).equalsIgnoreCase("true") && isPremiumSubscriber) {	
				logger.debug("Going to call batch card handler------->");
				handlerFactory.getHandler(BATCHES).handle(configCards.get(0), homeFeedCardList, user, appVersionCode, restrict);
			}




			int count = 0;
			for (HomeFeedConfigurationCard configCard : configCards) {
				if(count < 20) {
					CardType cardType = configCard.getCardType();
						if((CardType.MICROCOURSES.equals(cardType) && getRedisValueForKey(SHOW_MICROCOURSES).equalsIgnoreCase("true"))
								|| (CardType.NCERT_SOLUTIONS.equals(cardType) && getRedisValueForKey(SHOW_NCERT_SOLUTIONS).equalsIgnoreCase("true"))
								|| (CardType.MICROCOURSE_COLLECTION.equals(cardType) && getRedisValueForKey(SHOW_MCR_COLLECTION).equalsIgnoreCase("true")) 
								|| (CardType.BANNER_LIST.equals(cardType) && getRedisValueForKey(SHOW_BANNER_LIST).equalsIgnoreCase("true"))) {

							HomeFeedCardHandler cardHandler = handlerFactory.getHandler(configCard.getCardType());
							cardHandler.handle(configCard, homeFeedCardList, user, appVersionCode, restrict);
							count++;
					} 

				} else {
					break;
				}
			}
			//Explicit Handling for grades 1 to 5--> Banner list then single banner then webinars
			if(isBelowSixthGrade) {
				//Commenting masterTalk banner as next masterTalk is not in pipeline
//				if(getRedisValueForKey(SHOW_SINGLE_BANNER).equalsIgnoreCase("true")) {
//					handlerFactory.getHandler(SINGLE_BANNER).handle(masterTalkConfig, homeFeedCardList, user, appVersionCode, restrict);
//				}

				if(getRedisValueForKey(SHOW_UPCOMING_WEBINARS).equalsIgnoreCase("true")) {
					handlerFactory.getHandler(UPCOMING_WEBINARS).handle(configCards.get(1), homeFeedCardList, user, appVersionCode, restrict);
				}
			}
		}
		return homeFeedCardList;
	}

	private HomeFeedConfigurationCard getConfigurableSingleBanner() {
		HomeFeedConfigurationCard singleBannerCard = new HomeFeedConfigurationCard();
		String imageURL = "";
		String redirectionLink = "";
		String sectionTitle = "";
		try {
		imageURL = redisDAO.get(IMAGE_URL_FOR_TOP_BANNER);
		redirectionLink = redisDAO.get(REDIRECTION_URL_FOR_TOP_BANNER);
		sectionTitle = redisDAO.get(SECTION_TITLE_FOR_TOP_BANNER);
		}catch(Exception e) {
			logger.error("Error occured while fetching value from redis for single banner ");
		}
		if(StringUtils.isNotEmpty(imageURL) && StringUtils.isNotEmpty(redirectionLink) && StringUtils.isNotEmpty(sectionTitle)) {
			singleBannerCard.setCardType(SINGLE_BANNER);
			singleBannerCard.setSectionTitle(sectionTitle);
			singleBannerCard.setPublished(true);
			singleBannerCard.setContextUrls(new String[]{imageURL});
			singleBannerCard.setRedirectDeepLinkList(new String[]{redirectionLink});
		}
		
		return singleBannerCard;
	}

	private boolean isPremiumSubscriber(Long userId, String grade, String board, String streaam, String target) {
		// TODO add logic to fetch whether user has any active inactive premium subscription
		String subscriptionStatus = "";
		String responseString = "";
		ClientResponse response = null;
		Type type = new TypeToken<PremiumSubscriptionResp>() {
		}.getType();
		PremiumSubscriptionResp resp = new PremiumSubscriptionResp();
		try {
			subscriptionStatus = redisDAO.get(IS_SUBSCRIPTION_ACTIVE);
			logger.debug("subscriptionStatus -> "+subscriptionStatus);
		} catch (InternalServerErrorException | BadRequestException e) {
			logger.error("Subscription Module Health Status retrieval from Redis Failed");
		}

		if(null != subscriptionStatus && !subscriptionStatus.isEmpty() && subscriptionStatus.equalsIgnoreCase("true")) {

			StringBuilder url = new StringBuilder(SUBSCRIPTION_ENDPOINT + "bundle/isPremiumSubscriber?userId=" + userId.toString());
			if(StringUtils.isNotEmpty(grade)) {
				url.append("&grade=");
				url.append(WebUtils.getUrlEncodedValue(grade));
			}
			if(StringUtils.isNotEmpty(board)) {
				url.append("&board=");
				url.append(WebUtils.getUrlEncodedValue(board));
			}
			if(StringUtils.isNotEmpty(streaam)) {
				url.append("&stream=");
				url.append(WebUtils.getUrlEncodedValue(streaam));
			}
			if(StringUtils.isNotEmpty(target)) {
				url.append("&target=");
				url.append(WebUtils.getUrlEncodedValue(target));
			}
			try {
				response = WebUtils.INSTANCE.doCall(url.toString(), HttpMethod.GET, null, true, true);

				VExceptionFactory.INSTANCE.parseAndThrowException(response);

				responseString = response.getEntity(String.class);
				resp = new Gson().fromJson(responseString, type);

				logger.debug("Preimum Subscription Status fetched for user = "+userId.toString()+" " +responseString.toString());

			} catch (VException e) {
				logger.error("Error occured while fetching data from subscription for api isPremiumSubscriber ->" +e);
			}
		}
		if(null != response && response.getStatus() == 200) {
			if(null != resp && Boolean.TRUE.equals(Boolean.valueOf(resp.getIsPremiumSubscriber())))
				return true;
			else
				return false;
		}
		return false;
	}

	public HomeFeedCardOrderList configureHomeFeed(HomeFeedCardOrderList homeFeedCardOrderList)
			throws BadRequestException {

		return null;
	}

	@Override
	public HomeFeedCardOrderList fetchCurrentOrdering(String grade, PublishType publishType) throws VException {
		String responseString = "";
		String lmsStatus = "";
		String configCardsCurrentOrdering = "";
		StringBuilder key = new StringBuilder(APP_CONFIG_CARDS_ORDER_FOR_GRADE);
		HomeFeedCardOrderList homeFeedCard = new HomeFeedCardOrderList();
		key.append(grade);
		Type type = new TypeToken<HomeFeedCardOrderList>() {
		}.getType();

		try {
			configCardsCurrentOrdering = redisDAO.get(key.toString());
		} catch (Exception e) {
			logger.error("ERROR Occured while fetching data from Cache = " + e.getMessage());
		}


		if(StringUtils.isEmpty(configCardsCurrentOrdering)) {

			try {
				lmsStatus = redisDAO.get(IS_LMS_ACTIVE);
				logger.debug("lmsStatus -> "+lmsStatus);
			} catch (InternalServerErrorException | BadRequestException e) {
				logger.error("LMS Module Health Status retrieval from Redis Failed");
			}

			if(StringUtils.isNotEmpty(lmsStatus) && lmsStatus.equalsIgnoreCase(String.valueOf(Boolean.TRUE))) {
				String url = LMS_ENDPOINT + "homefeed/fetchCurrentOrdering/" +grade+ "?publishType="+publishType;
				ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true, true);
				VExceptionFactory.INSTANCE.parseAndThrowException(response);
				responseString = response.getEntity(String.class);
				logger.debug("fetchCurrentOrdering called in lms for grade "+grade+" -> "+responseString.toString());
				homeFeedCard = new Gson().fromJson(responseString, type);
				logger.debug("fetchCurrentOrdering called in lms for grade "+grade+" -> "+homeFeedCard);
				try {
					redisDAO.setex(key.toString(), responseString, 9000);
				} catch (Exception e) {
					logger.error("Error Occured while saving data to REDIS for key "+ key.toString()+ " -> " + e.getMessage());
				}

			}else {
				logger.error("Current Ordering of homefeed cards could not be fetched for grade -> " +grade);
			}
		}else {
			homeFeedCard = new Gson().fromJson(configCardsCurrentOrdering, type);
		}
		return homeFeedCard;

	}

	private HomeFeedConfigurationCard getSingleBannerRedisConfigCard(String sectionTitle, String bannerRedisKey, String defaultBannerLink,String redirectionRedisKey,String defaultRedirectionLink,boolean isPublished) throws VException{
		HomeFeedConfigurationCard singleBannerCard = new HomeFeedConfigurationCard();
		String redisBannerLink = redisDAO.get(bannerRedisKey);
		String redirectionLink = redisDAO.get(redirectionRedisKey);
		singleBannerCard.setCardType(SINGLE_BANNER);
		singleBannerCard.setSectionTitle(sectionTitle);
		singleBannerCard.setPublished(isPublished);
		if(StringUtils.isNotEmpty(redisBannerLink)){
			singleBannerCard.setContextUrls(new String[]{redisBannerLink});
		}else {
			singleBannerCard.setContextUrls(new String[]{defaultBannerLink});
		}
		if(StringUtils.isNotEmpty(redirectionLink)){
			singleBannerCard.setRedirectDeepLinkList(new String[]{redirectionLink});
		}else {
			singleBannerCard.setRedirectDeepLinkList(new String[]{defaultRedirectionLink});
		}

		return singleBannerCard;
	}

	private String getRedisValueForKey(String key) {
		String value = "false";
		try {
			value = redisDAO.get(key);
		} catch (InternalServerErrorException | BadRequestException e) {
			value = "false";
		}
		if(StringUtils.isEmpty(value)) {
			value = "false";
		}
		return value;
	}


}
