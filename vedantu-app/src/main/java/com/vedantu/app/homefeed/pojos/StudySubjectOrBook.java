/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.app.homefeed.pojos;

import java.util.List;

/**
 *
 * @author ajith
 */
public class StudySubjectOrBook {

    private String title;
    private Integer chapterCount;
    private Integer pdfCount;
    private List<ChapterPdf> chapters;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getChapterCount() {
        return chapterCount;
    }

    public void setChapterCount(Integer chapterCount) {
        this.chapterCount = chapterCount;
    }

    public Integer getPdfCount() {
        return pdfCount;
    }

    public void setPdfCount(Integer pdfCount) {
        this.pdfCount = pdfCount;
    }

    public List<ChapterPdf> getChapters() {
        return chapters;
    }

    public void setChapters(List<ChapterPdf> chapters) {
        this.chapters = chapters;
    }

    @Override
    public String toString() {
        return "StudySubjectOrBook{" + "title=" + title + ", chapterCount=" + chapterCount + ", pdfCount=" + pdfCount + ", chapters=" + chapters + '}';
    }

}
