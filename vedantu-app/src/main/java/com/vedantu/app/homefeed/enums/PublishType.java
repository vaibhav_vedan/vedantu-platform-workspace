package com.vedantu.app.homefeed.enums;

public enum PublishType {
    PUBLISHED,
    NON_PUBLISHED,
    ALL
}