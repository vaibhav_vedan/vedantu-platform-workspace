package com.vedantu.app.homefeed.enums;

public enum CardType {

    DOUBT("DOUBT"),
    SUBJECT_ENTRY("SUBJECT_ENTRY"),
    SCHOOL_INFO("SCHOOL_INFO"),
    RECENT_ACTIVITY("RECENT_ACTIVITY"),
    RESUME_VIDEO("RESUME_VIDEO"),
    RESUME_LEARNING_JOURNEY("RESUME_LEARNING_JOURNEY"),
    UPCOMING_VIDEOS("UPCOMING_VIDEOS"),
    LIVE_NOW("LIVE_NOW"),
    MOST_VIEWED_VIDEOS("MOST_VIEWED_VIDEOS"),
    MOST_VIEWED_PDFS("MOST_VIEWED_PDFS"),
    SINGLE_BANNER("SINGLE_BANNER"),
    BANNER_LIST("BANNER_LIST"),
    VIDEO_CARD("VIDEO_CARD"),
    PLAYLIST_CARD("PLAYLIST_CARD"),
    QUIZ("QUIZ"),
    TEST("TEST"),
    TEST_SERIES("TEST_SERIES"),
    UPCOMING_WEBINARS("UPCOMING_WEBINARS"),
    MICROCOURSES("MICROCOURSES"),
    NCERT_SOLUTIONS("NCERT_SOLUTIONS"),
    MICROCOURSE_COLLECTION("MICROCOURSE_COLLECTION"),
    VSTORIES("VSTORIES"),
    NEW_RELEASE("NEW_RELEASE"),
    TRIAL_BANNER("TRIAL_BANNER"),
    UPCOMING_CLASSES("UPCOMING_CLASSES"),
    LIVE_NOW_CLASSES("LIVE_NOW_CLASSES"),
    BATCHES("BATCHES");


    private String value;

    public String getValue() {
        return value;
    }

    CardType(String cardType) {
        this.value = cardType;
    }

    public static CardType getCardTypeForString(String card) {
        for (CardType cardType : CardType.values()) {
            if (cardType.value.equals(card)) {
                return cardType;
            }
        }
        throw new IllegalArgumentException();
    }
}
