package com.vedantu.app.homefeed.converters;

import com.vedantu.app.homefeed.entity.ClassesResponse;
import com.vedantu.app.homefeed.entity.WebinarResponse;
import com.vedantu.app.homefeed.enums.ClassTypes;

public class WebinarToClassConverter {

	public static ClassesResponse convert(WebinarResponse webinar) {
		return ClassesResponse.builder()
				.id(null != webinar.getId()?webinar.getId():"")
				.title(null != webinar.getTitle()?webinar.getTitle():"")
				.startTime(null != webinar.getStartTime()?webinar.getStartTime():null)
				.endTime(null != webinar.getEndTime()? webinar.getEndTime():null)
				.teacherName(null !=webinar.getTeacherInfo() && null != webinar.getTeacherInfo().get("name")?webinar.getTeacherInfo().get("name").toString():"")
				.courseInfo(null != webinar.getCourseInfo()?webinar.getCourseInfo():null)
				.webinarCode(null != webinar.getWebinarCode()?webinar.getWebinarCode():null)
				.totalParticipants(null != webinar.getTotalParticipants()?webinar.getTotalParticipants():null)
				.creationTime(null != webinar.getCreationTime()?webinar.getCreationTime():null)
				.sessionId(null != webinar.getSessionId()?webinar.getSessionId():"")
				.subject(null != webinar.getSubject()?webinar.getSubject():"")
				.subjects(null != webinar.getSubjects()?webinar.getSubjects():null)
				.typeOfClass(ClassTypes.MASTERCLASS)
				.courseName("Master class")
				.classRedirectUrl(null != webinar.getMasterClassUrl()?webinar.getMasterClassUrl():"")
				.isReminderSet(webinar.getIsReminderSet())
				.classRedirectUrl(null != webinar.getMasterClassUrl()?webinar.getMasterClassUrl():"")
				.build();

	}
}
