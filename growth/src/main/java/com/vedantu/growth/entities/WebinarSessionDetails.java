package com.vedantu.growth.entities;

import com.vedantu.growth.pojo.cms.WebinarScoreReport;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "WebinarSessionDetails")
public class WebinarSessionDetails extends AbstractMongoStringIdEntity {

    private Integer quizCount;
    private Integer uniqueStudentAttendants;
    private Integer totalDoubtsAsked;
    private Integer totalDoubtsResolved;

    //calculated from WebinarGttDetails
    private List<WebinarScoreReport> scoreReport;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String UNIQUE_STUDENT_ATTENDANCE = "uniqueStudentAttendants";
        public static final String QUIZ_COUNT = "quizCount";
        public static final String TOTAL_DOUBTS_ASKED = "totalDoubtsAsked";
        public static final String TOTAL_DOUBTS_RESOLVED = "totalDoubtsResolved";
        public static final String WEBINAR_SCORE_REPORT = "scoreReport";

    }
}
