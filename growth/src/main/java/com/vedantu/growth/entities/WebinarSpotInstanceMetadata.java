package com.vedantu.growth.entities;


import com.vedantu.growth.enums.WebinarSpotInstanceStatus;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.helper.StatusChangeHistoryManageHelper;
import com.vedantu.util.pojo.StatusChangeHistory;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@ToString
@Document(collection = "WebinarSpotInstanceMetadata")
@CompoundIndexes({
        @CompoundIndex(name = "lastUpdated_-1", def = "{'lastUpdated': -1}", background = true),
        @CompoundIndex(name = "webinarId_1_lastUpdated_-1", def = "{'webinarId':1, 'lastUpdated': -1}", background = true)
})
public class WebinarSpotInstanceMetadata extends AbstractMongoStringIdEntity {

    private String webinarId;
    private String sessionId;
    private Long webinarStartTime;
    private Long webinarEndTime;
    private String teacherEmail;
    private String teacherUserId;
    private String instanceId;
    private WebinarSpotInstanceStatus instanceStatus;
    private List<StatusChangeHistory> statusChangeHistory = new ArrayList<>();
    private Integer attemptCount = 0;
    private Map<Long,String> terminateMessage = new HashMap<>();
    private Map<Long,String> failureMessage = new HashMap<>();

    public void markAttempt() {
        this.attemptCount++;
    }

    public void setInstanceStatus(WebinarSpotInstanceStatus instanceStatus) {

        String currentStatus = this.instanceStatus !=null ? this.instanceStatus.name() : null;
        String newStatus = instanceStatus !=null ? instanceStatus.name() : null;

        StatusChangeHistoryManageHelper.updateStateChangeHistory(statusChangeHistory, currentStatus, newStatus, null);
        this.instanceStatus = instanceStatus;

    }

    public void setTetminateMessage(String message){

        if(StringUtils.isNotEmpty(message))
            terminateMessage.put(System.currentTimeMillis(),message);
    }

    public void setFailureMessage(String message){

        if(StringUtils.isNotEmpty(message))
            failureMessage.put(System.currentTimeMillis(),message);
    }

    public static class Constants {

        public static final String WEBINAR_ID = "webinarId";
        public static final String INSTANCE_STATUS = "instanceStatus";
        public static final String WEBINAR_START_TIME = "webinarStartTime";
        public static final String LAST_UPDATED_TIME = "lastUpdated";
    }
}
