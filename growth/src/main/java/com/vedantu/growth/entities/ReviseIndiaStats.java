package com.vedantu.growth.entities;


import com.vedantu.growth.pojo.TestAnalytics;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


@Getter
@Setter
@ToString
@Document(collection = "ReviseIndiaStats")
public class ReviseIndiaStats extends AbstractMongoStringIdEntity {
    @Indexed(background = true)
    private Long userId;

    private Integer classes;
    private Integer tests;
    private Integer doubts;
    private List<String> remindWebinars;
    private String testPerformaneCode;
    private String testAttemptId;
    private String s3Link;
    private TestAnalytics testAnalytics;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String TESTS = "tests";
        public static final String CLASSES = "classes";
        public static final String DOUBTS = "doubts";
        public static final String REMIND_WEBINARS = "remindWebinars";
        public static final String TEST_PERFORMANCE_CODE = "testPerformaneCode";
        public static final String TEST_ATTEMPT_ID = "testAttemptId";
        public static final String TEST_ANALYTICS = "testAnalytics";
    }
}

