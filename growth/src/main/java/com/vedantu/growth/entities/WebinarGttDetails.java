package com.vedantu.growth.entities;

import com.vedantu.scheduling.pojo.QuizNumbers;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "WebinarGttDetails")
@CompoundIndexes({
        @CompoundIndex(name = "userId_1_sessionId_1", def = "{'userId':1,'sessionId':1}", background = true),
        @CompoundIndex(name = "sessionId_1_quizAttempted_-1", def = "{'sessionId' : 1, 'quizNumbers.numberAttempted' : -1}", background = true),
        @CompoundIndex(name = "sessionId_1_lbRank_1_quizNumbers.numberAttempted_1", def = "{'sessionId' : 1, 'lbRank' : 1, 'quizNumbers.numberAttempted' : 1}", background = true)
})
public class WebinarGttDetails extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String userId;
    @Indexed(background = true)
    private String sessionId;
    private QuizNumbers quizNumbers;
    private Integer lbRank;
    private Float lbPercentile;
    private Integer lbPoints;

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String SESSION_ID = "sessionId";
        public static final String USER_ID = "userId";
        public static final String LB_RANK = "lbRank";
        public static final String LB_POINTS = "lbPoints";
        public static final String QUIZ_ATTEMPTED = "quizNumbers.numberAttempted";

    }

    @Override
    public String toString() {
        return "WebinarGttDetails{" +
                "id='" + getId() + '\'' +
                "userId='" + userId + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", quizNumbers=" + quizNumbers +
                ", lbRank=" + lbRank +
                ", lbPercentile=" + lbPercentile +
                ", lbPoints=" + lbPoints +
                ", lastUpdated=" + getLastUpdated() +
                ", lastUpdatedBy=" + getLastUpdatedBy() +
                ", creationTime=" + getCreationTime() +
                ", createdBy=" + getCreatedBy() +
                '}';
    }
}
