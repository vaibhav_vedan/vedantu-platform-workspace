package com.vedantu.growth.entities;

import com.vedantu.growth.pojo.mastertalk.MasterTalkAboutTalk;
import com.vedantu.growth.pojo.mastertalk.MasterTalkAskQuestions;
import com.vedantu.growth.pojo.mastertalk.MasterTalkCelebrity;
import com.vedantu.growth.pojo.mastertalk.MasterTalkCommunication;
import com.vedantu.growth.pojo.mastertalk.MasterTalkCoupon;
import com.vedantu.growth.pojo.mastertalk.MasterTalkKeyValueObject;
import com.vedantu.growth.pojo.mastertalk.MasterTalkOurSpeaker;
import com.vedantu.growth.pojo.mastertalk.MasterTalkSeoContent;
import com.vedantu.growth.pojo.mastertalk.MasterTalkSuperFanContent;
import com.vedantu.growth.pojo.mastertalk.MasterTalkSuperFanQuestion;
import com.vedantu.growth.pojo.mastertalk.MasterTalkTile;
import com.vedantu.growth.pojo.mastertalk.MasterTalkTracking;
import com.vedantu.growth.pojo.mastertalk.MastertalkSessionLinks;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Getter
@Setter
@ToString
@Document(collection = "MasterTalkDetails")
@CompoundIndexes({
        @CompoundIndex(name = "creationTime_-1", def = "{'creationTime': -1}", background = true),
        @CompoundIndex(name = "talkEndTime-1", def = "{'talkEndTime': -1}", background = true)
})
public class MasterTalkDetails extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String pageURL;

    private String masterTalkType;
    private MasterTalkSeoContent seoContent;
    private MasterTalkCommunication communication;
    private MasterTalkTracking tracking;
    private Long stateChangeTime;
    private MasterTalkTile talkTile;
    private Integer counterLimit;
    private MasterTalkCelebrity celebrity;
    private MasterTalkAboutTalk aboutTalk;
    private String ourSpeakerTitle;
    private MasterTalkCoupon coupon;
    private String eventFlag;
    private Long launchDate;
    private Long releaseDate;
    private Long talkStartTime;
    private Long talkEndTime;
    private Long preToLiveTime;
    private Long liveToPostTime;
    private List<MasterTalkOurSpeaker> ourSpeaker;
    private List<MasterTalkAskQuestions> askQuestions;
    private List<MasterTalkKeyValueObject> faq;
    private String pastMasterTalkHeading;
    private String pastMasterTalkUtm;
    private String masterClassUtm;
    private MastertalkSessionLinks sessionLinks;
    private String askQuestionsImage;
    private String postTalkSubhead;
    private String liveDateMonth;
    private String liveTime;
    private MasterTalkSuperFanContent superFanContent;
    private List<MasterTalkSuperFanQuestion> superFanQuestions;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String PAGE_URL = "pageURL";
        public static final String TALK_TILE = "talkTile";
        public static final String PROD_SESSION_URL_POST = "prodSessionUrlPost";
        public static final String TALK_END_TIME = "talkEndTime";
        public static final String CELEBRITY = "celebrity";
        public static final String TALK_START_TIME = "talkStartTime";
        public static final String SESSION_LINKS = "sessionLinks";
        public static final String LAUNCH_DATE = "launchDate";
        public static final String RELEASE_DATE = "releaseDate";
        public static final String PRE_TO_LIVE_TIME = "preToLiveTime";
        public static final String TO_LIVE_POST_TIME = "liveToPostTime";
        public static final String LIVE_TIME = "liveTime";
        public static final String LIVE_DATE_MONTH = "liveDateMonth";
        public static final String COUNTER_LIMIT = "counterLimit";
    }

}
