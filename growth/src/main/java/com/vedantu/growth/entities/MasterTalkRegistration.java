package com.vedantu.growth.entities;

import com.vedantu.growth.pojo.FeedbackPojo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Document(collection = "MasterTalkRegistration")
@CompoundIndexes({
        @CompoundIndex(name = "userId_1_masterTalkId_1", def = "{'userId': 1, 'masterTalkId': 1}", background = true),
        @CompoundIndex(name = "lastUpdated_-1", def = "{'lastUpdated': -1}", background = true)
})
public class MasterTalkRegistration extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long userId;

    @Indexed(background = true)
    private String masterTalkId;

    private Boolean isRegistered = Boolean.FALSE;
    private List<String> questions = new ArrayList<>();
    private Boolean isTestAttempted = Boolean.FALSE;
    private FeedbackPojo feedback;
    private String utmCampaign;
    private String utmMedium;
    private String utmSource;
    private String utmTerm;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String MASTER_TALK_TYPE = "masterTalkType";
        public static final String MASTER_TALK_ID = "masterTalkId";
    }

    public void addQuestion(String question){
        questions.add(question);
    }

}
