/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.entities;

import com.vedantu.growth.enums.cms.WebinarStatus;
import com.vedantu.growth.enums.cms.WebinarToolType;
import com.vedantu.growth.enums.cms.WebinarType;
import com.vedantu.growth.pojo.cms.LeaderBoard;
import com.vedantu.growth.pojo.cms.Testimonial;
import com.vedantu.growth.requests.cms.CreateWebinarReq;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.json.JSONArray;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jeet
 */
@Document(collection = "Webinar")
@CompoundIndexes({
        @CompoundIndex(name = "startTime_-1_grades_1", def = "{'startTime': -1, 'grades': 1}", background = true)
})
//Directly created on DB : grades:1,webinarStatus:1,type:1,toolType:1,parentWebinarId:1,startTime:1,endTime:1
public class Webinar extends AbstractMongoStringIdEntity {

    @Indexed(background = true, unique = true)
    private String webinarCode;
    @Indexed(background = true)
    private String title;
    @Indexed(background = true)
    private List<String> grades;
    @Indexed(background = true)
    private List<String> targets;
    @Indexed(background = true)
    private List<String> tags;
    @Indexed(background = true)
    private List<String> boards;
    private String replayUrl;
    @Indexed(background = true)
    private Long startTime;
    private Long duration;
    @Indexed(background = true)
    private Long endTime;
    private JSONArray rescheduleData;
    private WebinarToolType toolType;
    private Map<String, Object> webinarInfo;
    @Indexed(background = true)
    private String teacherEmail;
    private Map<String, Object> teacherInfo;
    private Map<String, Object> courseInfo;
    private Map<String, Object> registerSectionInfo;
    private String gtwSeat;
    private String gtwWebinarId;
    private String emailImageUrl;
    private WebinarStatus webinarStatus;
    private Boolean showInPastWebinars = Boolean.FALSE;
    private List<LeaderBoard> leaderBoard;
    private WebinarType type;
    private Testimonial testimonial;
    private Set<String> subjects;

    @Indexed(background = true)
    private Boolean gtwAttendenceSynced = false;

    private String sessionId;
    private Set<Long> taIds;
    private OTFSessionInfo sessionInfo;
    private String parentWebinarId;
    private Boolean isSimLive;
    private String parentWebinarSessionId;
    private List<String> contextTags;
    private Boolean replayCreated = Boolean.FALSE;

    public Webinar() {
    }

    public Webinar(CreateWebinarReq req) {
        this.webinarCode = req.getWebinarCode();
        this.grades = req.getGrades();
        this.targets = req.getTargets();
        this.tags = req.getTags();
        this.boards = req.getBoards();
        this.replayUrl = req.getReplayUrl();
        this.startTime = req.getStartTime();
        this.toolType = req.getToolType();
        this.webinarInfo = req.getWebinarInfo();
        this.teacherEmail = req.getTeacherEmail();
        this.teacherInfo = req.getTeacherInfo();
        this.courseInfo = req.getCourseInfo();
        this.registerSectionInfo = req.getRegisterSectionInfo();
        this.gtwSeat = req.getGtwSeat();
        this.gtwWebinarId = req.getGtwWebinarId();
        this.title = req.getTitle();
        this.duration = req.getDuration();
        this.endTime = req.getEndTime();
        this.emailImageUrl = req.getEmailImageUrl();
        this.endTime = this.startTime + duration * DateTimeUtils.MILLIS_PER_MINUTE;
        this.webinarStatus = req.getWebinarStatus();
        this.showInPastWebinars = req.getShowInPastWebinars();
        this.type = req.getType();
        this.testimonial = req.getTestimonial();
        this.leaderBoard = req.getLeaderBoard();
        this.subjects = req.getSubjects();
        this.contextTags = req.getContextTags();
    }

    public void mapDataFromReq(CreateWebinarReq req) {
        this.webinarCode = req.getWebinarCode();
        this.grades = req.getGrades();
        this.targets = req.getTargets();
        this.tags = req.getTags();
        this.boards = req.getBoards();
        this.replayUrl = req.getReplayUrl();
        this.startTime = req.getStartTime();
        this.toolType = req.getToolType();
        this.webinarInfo = req.getWebinarInfo();
        this.teacherEmail = req.getTeacherEmail();
        this.teacherInfo = req.getTeacherInfo();
        this.courseInfo = req.getCourseInfo();
        this.registerSectionInfo = req.getRegisterSectionInfo();
        this.gtwSeat = req.getGtwSeat();
        this.gtwWebinarId = req.getGtwWebinarId();
        this.title = req.getTitle();
        this.duration = req.getDuration();
        this.endTime = req.getEndTime();
        this.emailImageUrl = req.getEmailImageUrl();
        this.endTime = startTime + duration * DateTimeUtils.MILLIS_PER_MINUTE;
        this.webinarStatus = req.getWebinarStatus();
        this.showInPastWebinars = req.getShowInPastWebinars();
        this.leaderBoard = req.getLeaderBoard();
        this.type = req.getType();
        this.testimonial = req.getTestimonial();
        this.subjects = req.getSubjects();
        this.contextTags = req.getContextTags();
    }

    public String getWebinarCode() {
        return webinarCode;
    }

    public void setWebinarCode(String webinarCode) {
        this.webinarCode = webinarCode;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getBoards() {
        return boards;
    }

    public void setBoards(List<String> boards) {
        this.boards = boards;
    }

    public String getReplayUrl() {
        return replayUrl;
    }

    public void setReplayUrl(String replayUrl) {
        this.replayUrl = replayUrl;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public JSONArray getRescheduleData() {
        return rescheduleData;
    }

    public void setRescheduleData(JSONArray rescheduleData) {
        this.rescheduleData = rescheduleData;
    }

    public WebinarToolType getToolType() {
        return toolType;
    }

    public void setToolType(WebinarToolType toolType) {
        this.toolType = toolType;
    }

    public Map<String, Object> getWebinarInfo() {
        return webinarInfo;
    }

    public void setWebinarInfo(Map<String, Object> webinarInfo) {
        this.webinarInfo = webinarInfo;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public Map<String, Object> getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(Map<String, Object> teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public Map<String, Object> getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(Map<String, Object> courseInfo) {
        this.courseInfo = courseInfo;
    }

    public Map<String, Object> getRegisterSectionInfo() {
        return registerSectionInfo;
    }

    public void setRegisterSectionInfo(Map<String, Object> registerSectionInfo) {
        this.registerSectionInfo = registerSectionInfo;
    }

    public String getGtwSeat() {
        return gtwSeat;
    }

    public void setGtwSeat(String gtwSeat) {
        this.gtwSeat = gtwSeat;
    }

    public String getGtwWebinarId() {
        return gtwWebinarId;
    }

    public void setGtwWebinarId(String gtwWebinarId) {
        this.gtwWebinarId = gtwWebinarId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Boolean getGtwAttendenceSynced() {
        return gtwAttendenceSynced;
    }

    public void setGtwAttendenceSynced(Boolean gtwAttendenceSynced) {
        this.gtwAttendenceSynced = gtwAttendenceSynced;
    }

    public String getEmailImageUrl() {
        return emailImageUrl;
    }

    public void setEmailImageUrl(String emailImageUrl) {
        this.emailImageUrl = emailImageUrl;
    }

    public WebinarStatus getWebinarStatus() {
        return webinarStatus;
    }

    public void setWebinarStatus(WebinarStatus webinarStatus) {
        this.webinarStatus = webinarStatus;
    }

    public Boolean getShowInPastWebinars() {
        return showInPastWebinars;
    }

    public void setShowInPastWebinars(Boolean showInPastWebinars) {
        this.showInPastWebinars = showInPastWebinars;
    }

    public List<LeaderBoard> getLeaderBoard() {
        return leaderBoard;
    }

    public void setLeaderBoard(List<LeaderBoard> leaderBoard) {
        this.leaderBoard = leaderBoard;
    }

    public WebinarType getType() {
        return type;
    }

    public void setType(WebinarType type) {
        this.type = type;
    }

    public String getParentWebinarId() {
        return parentWebinarId;
    }

    public void setParentWebinarId(String parentWebinarId) {
        this.parentWebinarId = parentWebinarId;
    }

    public Boolean getIsSimLive() {
        return isSimLive;
    }

    public void setIsSimLive(Boolean isSimLive) {
        this.isSimLive = isSimLive;
    }

    public String getParentWebinarSessionId() {
        return parentWebinarSessionId;
    }

    public void setParentWebinarSessionId(String parentWebinarSessionId) {
        this.parentWebinarSessionId = parentWebinarSessionId;
    }

    public List<String> getContextTags() {
        return contextTags;
    }

    public void setContextTags(List<String> contextTags) {
        this.contextTags = contextTags;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String WEBINARCODE = "webinarCode";
        public static final String GRADES = "grades";
        public static final String TARGETS = "targets";
        public static final String TAGS = "tags";
        public static final String BOARDS = "boards";
        public static final String REPLAYURL = "replayUrl";
        public static final String TEACHEREMAIL = "teacherEmail";
        public static final String TITLE = "title";
        public static final String GTW_ATTENDENCE_SYNCED = "gtwAttendenceSynced";
        public static final String ENDTIME = "endTime";
        public static final String STARTTIME = "startTime";
        public static final String WEBINAR_STATUS = "webinarStatus";
        public static final String SHOW_IN_PAST_WEBINARS = "showInPastWebinars";
        public static final String TYPE = "type";
        public static final String TESTIMONIAL = "testimonialPOJO";
        public static final String TOOL_TYPE  = "toolType";
        public static final String TEACHER_INFO = "teacherInfo";
        public static final String WEBINAR_INFO = "webinarInfo";
        public static final String THUMBNAIL = "courseInfo.grades";
        public static final String SESSION_ID = "sessionId";
        public static final String PARENT_WEBINAR_ID = "parentWebinarId";
        public static final String IS_SIME_LIVE = "isSimLive";
        public static final String REPLAY_CREATED = "replayCreated";
        public static final String SUBJECTS = "subjects";
    }

    /**
     * @return the subjects
     */
    public Set<String> getSubjects() {
        return subjects;
    }

    /**
     * @param subjects the subjects to set
     */
    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public OTFSessionInfo getSessionInfo() {
        return sessionInfo;
    }

    public void setSessionInfo(OTFSessionInfo sessionInfo) {
        this.sessionInfo = sessionInfo;
    }

    public Set<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(Set<Long> taIds) {
        this.taIds = taIds;
    }

	public Testimonial getTestimonial() {
		return testimonial;
	}

	public void setTestimonial(Testimonial testimonial) {
		this.testimonial = testimonial;
	}

    public Boolean getReplayCreated() {
        return replayCreated;
    }

    public void setReplayCreated(Boolean replayCreated) {
        this.replayCreated = replayCreated;
    }
}
