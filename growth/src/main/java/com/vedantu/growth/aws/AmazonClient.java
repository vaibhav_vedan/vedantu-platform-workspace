package com.vedantu.growth.aws;

import com.amazonaws.HttpMethod;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.vedantu.aws.pojo.UploadFileSourceType;
import com.vedantu.aws.response.UploadFileUrlRes;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.net.URL;
import java.util.Date;
import java.util.UUID;

@Service
public class AmazonClient {

    public static final String AWS_CMDS_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.cmds.bucket");
    public static final String VSAT_RESULT_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.vsat.bucket");
    public static final String VSAT_RESULT_FOLDER = "vsat-result";
    public static final String AWS_CMDS_ANSWERS_FOLDER = "answers";
    public static final int CMDS_ANSWER_UPLOAD_URL_EXPIRATION_MARGIN_IN_HOURS = 12;
    public static final int CMDS_ANSWER_DOWNLOAD_URL_EXPIRATION_MARGIN_IN_HOURS = 24;
    public static final String PUBLIC_CONTENT_IDENTIFIER = "/public/";

    private static AmazonS3 s3Client;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonClient.class);

    @PostConstruct
    public void init() {
        try {
            s3Client = new AmazonS3Client();
            s3Client.setRegion(Region.getRegion(Regions.AP_SOUTHEAST_1));
        } catch (Exception ex) {
            logger.info("Error occured initializing s3Client : " + ex.getMessage());
        }
    }

    public UploadFileUrlRes getPresignedUploadUrl(UploadFileSourceType sourceType, String fileName, String contentType)
            throws VException {
        String keyName = generateRandomKeyFromFileName(fileName);
        GeneratePresignedUrlRequest generatePresignedUrlRequest = new GeneratePresignedUrlRequest(
                getS3Location(UploadFileSourceType.CMDS_ANSWER_UPLOAD), keyName);
        generatePresignedUrlRequest.setMethod(HttpMethod.PUT);
        Date uploadExpiryDate = getExpiryTime(CMDS_ANSWER_UPLOAD_URL_EXPIRATION_MARGIN_IN_HOURS);
        generatePresignedUrlRequest.setExpiration(uploadExpiryDate);

        // TODO : Predict content type
        generatePresignedUrlRequest.setContentType(contentType);
        URL url = s3Client.generatePresignedUrl(generatePresignedUrlRequest);

        // Generate Download url
        Date downloadExpiryDate = getExpiryTime(CMDS_ANSWER_DOWNLOAD_URL_EXPIRATION_MARGIN_IN_HOURS);
        String downloadUrl = getPresignedDownloadUrl(keyName, getS3Location(UploadFileSourceType.CMDS_ANSWER_UPLOAD),
                downloadExpiryDate);
        UploadFileUrlRes uploadFileUrlRes = new UploadFileUrlRes(fileName, keyName, downloadUrl, url.toString(),
                uploadExpiryDate.getTime(), downloadExpiryDate.getTime(), contentType);
        return uploadFileUrlRes;
    }

    
    public String getPresignedDownloadUrl(String keyName, String bucketName) {
        //key == fully qualified key name along with folder path
        Date expiration = new Date();
        long milliSeconds = expiration.getTime();
        milliSeconds += 1000 * 60 * 60; // Add 1 hour.
        expiration.setTime(milliSeconds);
        return getPresignedDownloadUrl(keyName, bucketName, expiration);
    }

    public String getPresignedDownloadUrl(String keyName, String bucketName, Date expiryTime) {
        if (keyName.contains(PUBLIC_CONTENT_IDENTIFIER)) {
            return s3Client.getUrl(bucketName, keyName).toString();
        }
        GeneratePresignedUrlRequest grt = new GeneratePresignedUrlRequest(bucketName, keyName);
        grt.setExpiration(expiryTime);
        return s3Client.generatePresignedUrl(grt).toString();
    }

    public String getS3Location(UploadFileSourceType sourceType) throws VException {
        switch (sourceType) {
            case CMDS_ANSWER_UPLOAD:
                return AWS_CMDS_BUCKET + "/" + ConfigUtils.INSTANCE.getEnvironmentSlug() + "/" + AWS_CMDS_ANSWERS_FOLDER;
            case VSAT_RESULT_SALES:
                return VSAT_RESULT_BUCKET + "/" + ConfigUtils.INSTANCE.getEnvironmentSlug() + "/" + VSAT_RESULT_FOLDER;
            default:
                throw new VException(ErrorCode.NOT_FOUND_ERROR, "Source type not found");
        }
    }

    private static Date getExpiryTime(int expiryDurationInHours) {
        Date expiration = new Date();
        long milliSeconds = expiration.getTime();
        milliSeconds += expiryDurationInHours * 1000 * 60 * 60;
        expiration.setTime(milliSeconds);
        return expiration;
    }

    private static String generateRandomKeyFromFileName(String fileName) {
        String[] split = StringUtils.split(fileName, ".");
        return UUID.randomUUID().toString() + ((split.length != 0) ? ("." + split[split.length - 1]) : "");
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}
