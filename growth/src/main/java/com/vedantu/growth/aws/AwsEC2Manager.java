/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.aws;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.IamInstanceProfileSpecification;
import com.amazonaws.services.ec2.model.InstanceNetworkInterfaceSpecification;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.ShutdownBehavior;
import com.amazonaws.services.ec2.model.Tag;
import com.vedantu.growth.dao.cms.WebinarSpotInstanceMetadataDao;
import com.vedantu.growth.entities.WebinarSpotInstanceMetadata;
import com.vedantu.growth.enums.WebinarSpotInstanceStatus;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

@Service
public class AwsEC2Manager {

    private AmazonEC2 ec2Client;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private WebinarSpotInstanceMetadataDao webinarSpotInstanceMetadataDao;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsEC2Manager.class);

    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment");
    private static final String WEBINAR_LAUNCH_SECURITY_GROUP = ConfigUtils.INSTANCE.getStringValue("webinar.launch.security_group");
    private static final String SESSION_RECORDER_IAM = ConfigUtils.INSTANCE.getStringValue("vedantu.oto.recorder.iam");
    private static final String WEBINAR_LAUNCHER_AMI_ID = ConfigUtils.INSTANCE.getStringValue("vedantu.webinar.launcher.ami_id");
    private static final String WEBINAR_LAUNCH_SUBNET = ConfigUtils.INSTANCE.getStringValue("vedantu.webinar.launch.subnet");
    private static final String WEBINAR_LAUNCHER_KEY = ConfigUtils.INSTANCE.getStringValue("webinar.launcher.key");

    @PostConstruct
    public void init() {
        if (!ENV.equalsIgnoreCase("LOCAL")) {
            ec2Client = AmazonEC2ClientBuilder.defaultClient();
        }
    }

    public boolean createWebinarLauncherEc2(WebinarSpotInstanceMetadata instanceMetadata) {

        instanceMetadata.markAttempt();
        instanceMetadata.setInstanceStatus(WebinarSpotInstanceStatus.INSTANCE_LAUNCHING);
        webinarSpotInstanceMetadataDao.upsert(instanceMetadata);

        try {

            String webinarId = instanceMetadata.getWebinarId();
            String sessionId = instanceMetadata.getSessionId();
            Long timeoutInMillis = instanceMetadata.getWebinarEndTime() - System.currentTimeMillis();
            String teacherEmail = instanceMetadata.getTeacherEmail();
            String presenterId = instanceMetadata.getTeacherUserId();

            Long timeout = timeoutInMillis / 1000;

            Long waitTimeout = (instanceMetadata.getWebinarStartTime() - System.currentTimeMillis()) / 1000;

            //server up time considering 30 seconds for overall up time
            timeout -= 30;

            if(waitTimeout < 60)
                waitTimeout = 60l;

            waitTimeout -= 30;

            if (ENV.equalsIgnoreCase("LOCAL")) {
                instanceMetadata.setFailureMessage("LOCAL");
                instanceMetadata.setInstanceStatus(WebinarSpotInstanceStatus.INSTANCE_LAUNCH_FAILED);
                return true;
            }
            if (!ENV.equalsIgnoreCase("PROD")) {
                if (ENV.equalsIgnoreCase("wave2")) {
                    if (timeout > DateTimeUtils.SECONDS_PER_HOUR)
                        timeout = 3600l;
                }else{
                    timeout = 300l;
                }
            }

            String instanceEnv = ENV.equalsIgnoreCase("PROD") ? ENV : "qa";

            logger.info("got request to start a webinar launcher webinarId:" + webinarId + " timeout:" + timeout);
            String userData = String.format("#!/bin/bash\n"
                            + "cd /home/ubuntu/vedantu-spot-instances/oto-session-recorder \n"
                            + "ssh-agent bash -c 'ssh-add /home/ubuntu/.ssh/id_rsa; git checkout feature/webinar-spot-instance'\n"
                            + "ssh-agent bash -c 'ssh-add /home/ubuntu/.ssh/id_rsa; git stash'\n"
                            + "ssh-agent bash -c 'ssh-add /home/ubuntu/.ssh/id_rsa; git pull'\n"
                            + "sudo -u ubuntu echo \"env='%s'\" > /home/ubuntu/vedantu-spot-instances/oto-session-recorder/env.py\n"
                            + "sudo -u ubuntu python /home/ubuntu/vedantu-spot-instances/oto-session-recorder/webinar-launcher.py -w %s -s %s -t %s -u %s -r %s -p %s >> /home/ubuntu/vedantu-spot-instances/oto-session-recorder/test.log 2>&1",
                    instanceEnv.toLowerCase(),
                    webinarId,
                    sessionId,
                    timeout,
                    teacherEmail,
                    waitTimeout,
                    presenterId);

            logger.info("userData : {}", userData);

            InstanceNetworkInterfaceSpecification interfaceSpecification = new InstanceNetworkInterfaceSpecification()
                    .withSubnetId(WEBINAR_LAUNCH_SUBNET)
                    .withAssociatePublicIpAddress(true)
                    .withGroups(WEBINAR_LAUNCH_SECURITY_GROUP)
                    .withDeviceIndex(0);
            IamInstanceProfileSpecification iamInstanceProfileSpecification = new IamInstanceProfileSpecification()
                    .withName(SESSION_RECORDER_IAM);

            InstanceType instanceType = InstanceType.C4Large;
            if(instanceMetadata.getAttemptCount() > 5){
                instanceType = InstanceType.M4Large;
            }

            RunInstancesRequest run_request = new RunInstancesRequest()
                    .withImageId(WEBINAR_LAUNCHER_AMI_ID)
                    .withInstanceType(instanceType)
                    .withMaxCount(1)
                    .withMinCount(1)
                    .withInstanceInitiatedShutdownBehavior(ShutdownBehavior.Terminate)
                    .withUserData(org.apache.commons.codec.binary.Base64.encodeBase64String(userData.getBytes()))
                    .withNetworkInterfaces(interfaceSpecification)
                    .withIamInstanceProfile(iamInstanceProfileSpecification)
                    .withKeyName(WEBINAR_LAUNCHER_KEY);
            RunInstancesResult run_response = ec2Client.runInstances(run_request);
            String instanceId = run_response.getReservation().getInstances().get(0).getInstanceId();
            logger.info("Start a webinar launcher webinarId:" + webinarId + " timeout:" + timeout + " instanceId:" + instanceId);

            Collection<Tag> tags = new ArrayList<>();
            Tag instanceName  = new Tag();
            instanceName.setKey("Name");
            instanceName.setValue("Sim-Live-" + sessionId);
            tags.add(instanceName);

            CreateTagsRequest tagRequest = new CreateTagsRequest();
            tagRequest.setResources(Arrays.asList(instanceId));
            tagRequest.setTags(tags);

            ec2Client.createTags(tagRequest);

            instanceMetadata.setInstanceId(instanceId);
            instanceMetadata.setInstanceStatus(WebinarSpotInstanceStatus.INSTANCE_LAUNCHED);

            return true;

        }catch (Exception e){
            logger.warn("Error launching webinarInstance webinarId: {}", instanceMetadata.getWebinarId(), e);
            instanceMetadata.setFailureMessage(e.getMessage());
            instanceMetadata.setInstanceStatus(WebinarSpotInstanceStatus.INSTANCE_LAUNCH_FAILED);

        }finally {
            webinarSpotInstanceMetadataDao.upsert(instanceMetadata);
        }

        return false;

    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (ec2Client != null) {
                ec2Client.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in shutting down ec2client ", e);
        }
    }        
}
