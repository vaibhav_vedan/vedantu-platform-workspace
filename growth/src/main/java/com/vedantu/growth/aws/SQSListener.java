package com.vedantu.growth.aws;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.exception.VException;
import com.vedantu.growth.entities.WebinarGttDetails;
import com.vedantu.growth.entities.WebinarSessionDetails;
import com.vedantu.growth.entities.WebinarSpotInstanceMetadata;
import com.vedantu.growth.managers.MasterTalkManager;
import com.vedantu.growth.managers.cms.WebinarManager;
import com.vedantu.growth.managers.cms.WebinarSessionDataManager;
import com.vedantu.growth.pojo.PostWebinarRegisterReqPojo;
import com.vedantu.growth.pojo.cms.WebinarSessionReplayAvailabilityPojo;
import com.vedantu.growth.requests.MasterTalkFeedbackReq;
import com.vedantu.growth.seo.PageBuilderManager;
import com.vedantu.growth.seo.SeoSearchEngineManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StatsdClient;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;
import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 *
 * @author ajith
 */
@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @Autowired
    StatsdClient statsdClient;

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private MasterTalkManager masterTalkManager;

    @Autowired
    private SeoSearchEngineManager seoSearchEngineManager;

    @Autowired
    private WebinarSessionDataManager webinarSessionDataManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private static final Gson gson = new Gson();

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;

        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            String queueName = ((Queue)textMessage.getJMSDestination()).getQueueName();
            logger.info("queueName "+queueName);
            Long startTime = System.currentTimeMillis();
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessage(queueName, sqsMessageType, textMessage.getText());
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                if(null != sqsMessageType) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }

        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }
    }

    public void handleMessage(String queueName, SQSMessageType sqsMessageType, String text) throws Exception {

        if(SQSQueue.WEBINAR_REGISTER_QUEUE.getQueueName(env).equals(queueName)){
            PostWebinarRegisterReqPojo postWebinarRegisterReqPojo = gson.fromJson(text, PostWebinarRegisterReqPojo.class);
            if(Objects.nonNull(postWebinarRegisterReqPojo)) {
                webinarManager.postWebinarRegistrationConfirmed(postWebinarRegisterReqPojo.getUserWebinarRegistration(), postWebinarRegisterReqPojo.getWebinar());
            }
        }else if(SQSQueue.WEBINAR_GTT_DETAILS_UPSERT.getQueueName(env).equals(queueName)){
            WebinarGttDetails webinarGttDetails = gson.fromJson(text, WebinarGttDetails.class);
            webinarSessionDataManager.upsertWebinarGttDetails(webinarGttDetails);
        }else if(SQSQueue.WEBINAR_SESSION_DETAILS_UPSERT.getQueueName(env).equals(queueName)){
            if(sqsMessageType.equals(SQSMessageType.WEBINAR_SESSION_REPLAY_AVAILABLE)) {
                WebinarSessionReplayAvailabilityPojo webinarSessionReplayAvailabilityPojo = gson.fromJson(text, WebinarSessionReplayAvailabilityPojo.class);
                webinarManager.markSessionReplayAvailability(webinarSessionReplayAvailabilityPojo);
            }else {
                WebinarSessionDetails webinarSessionDetails = gson.fromJson(text, WebinarSessionDetails.class);
                webinarSessionDataManager.upsertWebinarSessionDetails(webinarSessionDetails);
            }
        }else if(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE.getQueueName(env).equals(queueName)){
            Type _type = new TypeToken<HashMap<String, String>>() {
            }.getType();
            Map<String, String> payload = new Gson().fromJson(text, _type);
            String key = payload.get("key");
            pageBuilderManager.regenerateWebpAndPng(key);
        }else if(SQSQueue.START_UPCOMING_WEBINAR.getQueueName(env).equals(queueName)) {

            if(sqsMessageType.equals(SQSMessageType.LAUNCH_WEBINAR_INSTANCE)) {
                WebinarSpotInstanceMetadata instanceMetadata = gson.fromJson(text, WebinarSpotInstanceMetadata.class);
                webinarManager.launchWebinarInstance(instanceMetadata);
            }else {
                webinarManager.startUpcomingWebinars();
            }
        }else if(SQSQueue.MASTERTALK_FEEDBACK.getQueueName(env).equals(queueName)) {
            MasterTalkFeedbackReq masterTalkFeedbackReq = gson.fromJson(text, MasterTalkFeedbackReq.class);
            masterTalkManager.feedback(masterTalkFeedbackReq);
        }else if(SQSQueue.SEO_QUEUE.getQueueName(env).equals(queueName)){
            handleSeoMessages(sqsMessageType, text);
        }
        logger.info("Message handled");
    }

    private void handleSeoMessages(SQSMessageType sqsMessageType, String text) throws VException {

        switch (sqsMessageType){
            case SEO_CATEGORY_PAGE_UPDATED:{
                Map<String,Object> objectMap  = gson.fromJson(text, Map.class);
                seoSearchEngineManager.upsertESDocument(objectMap);
            }
        }
    }

}

