package com.vedantu.growth.aws;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.Statement.Effect;
import com.amazonaws.auth.policy.actions.SQSActions;
import com.amazonaws.auth.policy.conditions.ConditionFactory;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * Created by somil on 31/08/17.
 * http://developer.lightbend.com/docs/alpakka/latest/sqs.html
 */
@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    @Autowired
    private AwsSNSManager awsSNSManager;

    private String env;

    private static Integer defaultMaxRecieve = 6;

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {

                //START_UPCOMING_WEBINAR queue creation
                createQueue(SQSQueue.START_UPCOMING_WEBINAR, SQSQueue.START_UPCOMING_WEBINAR_DL, defaultMaxRecieve);

                //MASTERTALK_FEEDBACK queue creation
                createQueue(SQSQueue.MASTERTALK_FEEDBACK, SQSQueue.MASTERTALK_FEEDBACK_DL, defaultMaxRecieve);

                //creating queue for SEO_QUEUE.
                createQueue(SQSQueue.SEO_QUEUE, SQSQueue.SEO_QUEUE_DL, defaultMaxRecieve);

                //creating queue for WEBINAR_GTT_DETAILS_UPSERT.
                createQueue(SQSQueue.WEBINAR_GTT_DETAILS_UPSERT, SQSQueue.WEBINAR_GTT_DETAILS_UPSERT_DL, defaultMaxRecieve);

                //creating queue for WEBINAR_SESSION_DETAILS_UPSERT.
                createQueue(SQSQueue.WEBINAR_SESSION_DETAILS_UPSERT, SQSQueue.WEBINAR_SESSION_DETAILS_UPSERT_DL, defaultMaxRecieve);

                //creating queue for WEBINAR_REGISTER_QUEUE.
                createQueue(SQSQueue.WEBINAR_REGISTER_QUEUE, SQSQueue.WEBINAR_REGISTER_QUEUE_DL, defaultMaxRecieve);

            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager {}", e.getMessage());
        }

    }

    private void createQueue(SQSQueue queue, SQSQueue dlQueue, Integer maxRecieve){

        try {

            String queueName= getQueueName(queue);
            String dlQueueName= getQueueName(dlQueue);
            logger.info("creating dead letter queue {}", dlQueueName);
            CreateQueueRequest dlQueueRequest = new CreateQueueRequest(dlQueueName);
            dlQueueRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), dlQueue.getVisibilityTimeout());
            sqsClient.createQueue(dlQueueRequest);
            logger.info("created dead letter queue {}", dlQueueName);

            logger.info("creating queue: {}", queueName);
            CreateQueueRequest queueRequest = new CreateQueueRequest(queueName);
            queueRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), queue.getVisibilityTimeout());
            if(queue.getFifo()){
                queueRequest.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
            }
            sqsClient.createQueue(queueRequest);
            logger.info("created queue: {}", queueName);

            assignDeadLetterQueue(queueName, dlQueueName, maxRecieve);
        }catch (Exception e){
            logger.error("Error creating queue : ", e);
        }

    }

    private String getQueueName(SQSQueue sqsQueue){
        return sqsQueue.getQueueName(env);
    }

    private void assignDeadLetterQueue(String src_queue_name, String dl_queue_name, Integer maxReceive) {
        if (maxReceive == null) {
            maxReceive = 4;
        }
        // Get dead-letter queue ARN
        String dl_queue_url = sqsClient.getQueueUrl(dl_queue_name)
                .getQueueUrl();

        GetQueueAttributesResult queue_attrs = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dl_queue_url)
                        .withAttributeNames("QueueArn"));

        String dl_queue_arn = queue_attrs.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String src_queue_url = sqsClient.getQueueUrl(src_queue_name)
                .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(src_queue_url)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\"" + maxReceive + "\", \"deadLetterTargetArn\":\""
                        + dl_queue_arn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: {} to: {}" , dl_queue_name, src_queue_name);

    }

    public void setPermissions(String m_snsName, String sqsArn, String queueUrl) {

        Statement statement = new Statement(Effect.Allow)
                .withActions(SQSActions.SendMessage)
                .withPrincipals(new Principal("*"))
                .withConditions(ConditionFactory.newSourceArnCondition(m_snsName))
                .withResources(new Resource(sqsArn));
        Policy policy = new Policy("SubscriptionPermission")
                .withStatements(statement);

        HashMap<String, String> attributes = new HashMap();
        attributes.put("Policy", policy.toJson());
        SetQueueAttributesRequest request = new SetQueueAttributesRequest(queueUrl, attributes);
        sqsClient.setQueueAttributes(request);

    }

    public void createSubscription(SNSTopic topicName, SQSQueue queueName) {
        try{
            String queueUrl = getQueueURL(queueName);
            String sqsArn = getQueueArn(queueName);
            awsSNSManager.createSubscription(topicName, "sqs", sqsArn);
            setPermissions(awsSNSManager.getTopicArn(topicName), sqsArn, queueUrl);
        }catch (Exception e){

        }
    }

    @PostConstruct
    public void createSNSSubscription() {
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
//            createSubscription(SNSTopic.CMS_WEBINAR_EVENTS, SQSQueue.CLEVERTAP_QUEUE);
        }

    }

}
