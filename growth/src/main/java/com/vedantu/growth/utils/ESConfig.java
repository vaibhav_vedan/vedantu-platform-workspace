package com.vedantu.growth.utils;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.Objects;

@Service
@Data
@NoArgsConstructor
public class ESConfig {

    @Autowired
    private LogFactory logFactory;

    private static String host;
    private static Integer port;

    private RestHighLevelClient client = null;
    private Logger logger = logFactory.getLogger(ESConfig.class);
    private static final String SECURE_PROTOCOL = "https://";

    @PostConstruct
    public void init() {
        logger.info("Initializing elasticsearch configuration...");
        host = ConfigUtils.INSTANCE.getStringValue("elasticsearch.host");
        if(host.startsWith(SECURE_PROTOCOL)){
            host = host.substring(SECURE_PROTOCOL.length());
        }
        logger.info("host - {}\nport - {}",host,port);
        HttpHost httpHost;
        if(Objects.isNull(port)){
            httpHost = new HttpHost(host,-1,"https");
        }else{
            httpHost = new HttpHost(host, port, HttpHost.DEFAULT_SCHEME_NAME);
        }
        client=new RestHighLevelClient(RestClient.builder(httpHost)
                        .setRequestConfigCallback(requestConfigBuilder -> requestConfigBuilder
                                .setConnectTimeout(-1) //Timeout until connection is established
                                .setSocketTimeout(-1) //Timeout when waiting for data
                                .setConnectionRequestTimeout(-1) //Timeout when requesting a connection from the connection manager
                        ));
        logger.info("RestHighLevelClient Client connected successfully...");
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing es connection ", e);
        }
    }
}
