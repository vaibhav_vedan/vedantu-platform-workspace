package com.vedantu.growth.utils;

public class Constants {

    public static final String TITLE = "title";
    public static final String CHANNEL = "channel";
    public static final String RSS = "rss";
    public static final String VERSION = "version";
    public static final String LINK = "link";
    public static final String DESCRIPTION = "description";
    public static final String LANGUAGE = "language";
    public static final String EN_US = "en-us";
    public static final String IMAGE = "image";
    public static final String URL = "url";
    public static final String ITEM = "item";
    public static final String PUB_DATE = "pubDate";
    public static final String AUTHOR = "author";
    public static final String CATEGORY = "category";
    public static final String UTC = "UTC";
    public static final String UPCOMING_WEBINARS_CACHE_KEY =  "UPCOMING_WEBINARS_CACHE_KEY";
    public static final String FIND_WEBINARS_CACHE_KEY =  "FIND_WEBINARS_CACHE_KEY";
    public static final String FIND_WEBINARS_MOBILE_CACHE_KEY =  "FIND_WEBINARS_MOBILE_CACHE_KEY_";
    public static final String CAN_FETCH_WEBINAR_FROM_CACHE =  "CAN_FETCH_WEBINAR_FROM_CACHE";
    public static final String IS_OTM_WEBINAR_INFO_BLOCKED = "IS_OTM_WEBINAR_INFO_BLOCKED";
    public static final Integer WEBINAR_INFO_BLOCK_MINUTES = 2;
    public static final String HIDE_WEBINARS = "HIDE_WEBINARS";
    public static final String POP_UP_DELAY = "POP_UP_DELAY";
    public static final String MASTER_CLASS_URL_PREFIX = "/masterclass/";
    public static final String MASTER_CLASS_UTM_PARAMS = "?utm_source=seo&utm_medium=seo_hp&utm_campaign=seo_hp-masterclass&utm_content=homepage_tiles&utm_term=seo_hp_list_masterclass";
}
