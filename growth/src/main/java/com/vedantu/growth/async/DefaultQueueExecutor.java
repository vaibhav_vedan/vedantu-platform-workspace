/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.async;

import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.growth.aws.AwsSNSManager;
import com.vedantu.growth.managers.cms.WebinarManager;
import com.vedantu.growth.seo.PageBuilderManager;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 *
 * @author somil
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    AwsSNSManager awsSNSManager;

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case TRIGGER_SNS:
                SNSTopic topic = (SNSTopic) payload.get("topic");
                String subject = (String) payload.get("subject");
                String message = (String) payload.get("message");
                awsSNSManager.triggerSNS(topic, subject, message);
                break;
            case CMS_SYNC_WEBINAR_ATTENDEE_INFO:
                webinarManager.syncGtwAttendence();
                break;
            case CMS_SEND_WEBINAR_REMINDERS:
                webinarManager.sendWebinarsReminders();
                break;
            case CMS_SEND_WEBINAR_REGISTRATION_QUESTIONS:
                webinarManager.sendWebinarRegistrationQuestions();
                break;
            case GENERATE_CATEGORY_PAGES_FROM_INDEXABLE_QUESTIONS: {
                SeoDomain domain = SeoDomain.valueOf(String.valueOf(payload.get("domain")));
                pageBuilderManager.generateCategoryPagesFromQuestions(domain);
                break;
            }
            case GENERATE_MICROSITE_HTML_PAGES: {
                SeoDomain domain = SeoDomain.valueOf(String.valueOf(payload.get("domain")));
                pageBuilderManager.buildHtmlPages(domain);
                break;
            }
            case WEBINAR_NOTIFICATION_TASK:
                webinarManager.prepareAlertForWebinarRegisteredUsers();
                break;
            case SEND_EMAIL_AFTER_WEBINAR_COMPLETED:
                webinarManager.prepareEmailForWebinarCompleted();
                break;
            default:
                logger.error("Logic not defined for task:" + taskName);
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
