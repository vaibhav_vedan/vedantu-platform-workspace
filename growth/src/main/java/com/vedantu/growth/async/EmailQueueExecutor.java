/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.async;

import com.vedantu.User.User;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.growth.notifications.CommunicationManager;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 *
 * @author somil
 */
@Service
public class EmailQueueExecutor implements IAsyncQueueExecutor {

    

    @Autowired
    public LogFactory logFactory;

    @Autowired
    private CommunicationManager emailManager;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(EmailQueueExecutor.class);

    //TODO: Add executor here
    @Async("emailExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        logger.info("ENTRY " + params);
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case EMAIL_GENERAL_TASK:
                //EmailRequest request = new Gson().fromJson(jsonString, EmailRequest.class);
                EmailRequest request = (EmailRequest) payload.get("emailRequest");
                emailManager.sendEmail(request);
                break;
            case REGISTRATION_EMAIL_REVISEINDIA:
                User u1 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForReviseIndia(u1);
                break;
            case REGISTRATION_EMAIL_REVISEJEE:
                User u2 = (User) payload.get("user");
                emailManager.sendRegistrationEmailForReviseJee(u2);
                break;
            case TEST_RESULT_EMAIL_REVISEJEE:
                emailManager.sendResultEmailForReviseJee(payload);
                break;
            case TEST_RESULT_EMAIL_REVISE_TARGET_JEE_NEET:
                emailManager.sendResultEmailForReviseTargetJeeNeet(payload);
                break;
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
