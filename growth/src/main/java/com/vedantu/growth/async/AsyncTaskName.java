/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.async;

import com.vedantu.async.IAsyncTaskName;

/**
 *
 * @author somil
 */
public enum AsyncTaskName implements IAsyncTaskName {
    EMAIL_GENERAL_TASK(AsyncQueueName.EMAIL_QUEUE),
    TRIGGER_SNS(AsyncQueueName.DEFAULT_QUEUE),
    CMS_SYNC_WEBINAR_ATTENDEE_INFO(AsyncQueueName.DEFAULT_QUEUE),
    CMS_SEND_WEBINAR_REMINDERS(AsyncQueueName.DEFAULT_QUEUE),
    CMS_SEND_WEBINAR_REGISTRATION_QUESTIONS(AsyncQueueName.DEFAULT_QUEUE),
    GENERATE_MICROSITE_HTML_PAGES(AsyncQueueName.DEFAULT_QUEUE),
    WEBINAR_NOTIFICATION_TASK(AsyncQueueName.DEFAULT_QUEUE),
    SEND_EMAIL_AFTER_WEBINAR_COMPLETED(AsyncQueueName.DEFAULT_QUEUE),
    REGISTRATION_EMAIL_REVISEINDIA(AsyncQueueName.EMAIL_QUEUE),
    REGISTRATION_EMAIL_REVISEJEE(AsyncQueueName.EMAIL_QUEUE),
    TEST_RESULT_EMAIL_REVISEJEE(AsyncQueueName.EMAIL_QUEUE),
    TEST_RESULT_EMAIL_REVISE_TARGET_JEE_NEET(AsyncQueueName.EMAIL_QUEUE),
    GENERATE_CATEGORY_PAGES_FROM_INDEXABLE_QUESTIONS(AsyncQueueName.DEFAULT_QUEUE),
    ;

    private final AsyncQueueName queue;

    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }

    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
