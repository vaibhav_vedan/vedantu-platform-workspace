package com.vedantu.growth.dao.cms;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.entities.WebinarGttDetails;
import com.vedantu.growth.entities.WebinarSessionDetails;
import com.vedantu.growth.pojo.cms.WebinarScoreReport;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class WebinarSessionDataDao  extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(WebinarSessionDataDao.class);

    public WebinarSessionDataDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public <E extends AbstractMongoEntity> void upsert(E p){
        if (Objects.nonNull(p)) {
            saveEntity(p);
        }
    }

    public WebinarGttDetails getWebinarGttBySessionAndUserId(String sessionId, String userId) {

        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.SESSION_ID).is(sessionId));
        return findOne(query, WebinarGttDetails.class);
    }


    public List<WebinarSessionDetails> getWebinarSessionsByIds(List<String> sessionIds)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarSessionDetails.Constants._ID).in(sessionIds));
        query.fields().include(WebinarSessionDetails.Constants.ID);
        query.fields().include(WebinarSessionDetails.Constants.QUIZ_COUNT);
        query.fields().include(WebinarSessionDetails.Constants.UNIQUE_STUDENT_ATTENDANCE);
        query.fields().include(WebinarSessionDetails.Constants.TOTAL_DOUBTS_RESOLVED);
        query.fields().include(WebinarSessionDetails.Constants.WEBINAR_SCORE_REPORT);
        return runQuery(query, WebinarSessionDetails.class);
    }

    public List<WebinarGttDetails> gttAttendeeDetailsMobile(String userId, List<String> sessionIds)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.SESSION_ID).in(sessionIds));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.LB_RANK).gt(0));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.LB_POINTS).gt(0));
        query.fields().include(WebinarGttDetails.Constants.SESSION_ID);
        query.fields().include(WebinarGttDetails.Constants.LB_RANK);
        query.fields().include(WebinarGttDetails.Constants.LB_POINTS);
        return runQuery(query, WebinarGttDetails.class);
    }

    public List<WebinarGttDetails> gttAttendeeLeaderBoardMobile(List<String> sessionIds)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.SESSION_ID).in(sessionIds));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.QUIZ_ATTEMPTED).gt(0));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.LB_RANK).gt(0));
        query.fields().include(WebinarGttDetails.Constants.SESSION_ID);
        query.fields().include(WebinarGttDetails.Constants.LB_RANK);
        query.fields().include(WebinarGttDetails.Constants.USER_ID);
        query.with(Sort.by(Sort.Direction.ASC, WebinarGttDetails.Constants.SESSION_ID, WebinarGttDetails.Constants.LB_RANK));
        query.limit(5);
        return runQuery(query, WebinarGttDetails.class);
    }

    public List<WebinarScoreReport> getWebinarScoreReport(String sessionId)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.SESSION_ID).is(sessionId));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.QUIZ_ATTEMPTED).gt(0));
        query.addCriteria(Criteria.where(WebinarGttDetails.Constants.LB_POINTS).gt(0));
        query.fields().include(WebinarGttDetails.Constants.LB_POINTS);
        query.fields().include(WebinarGttDetails.Constants.QUIZ_ATTEMPTED);
        query.fields().include(WebinarGttDetails.Constants.CREATION_TIME);
        query.with(Sort.by(Sort.Direction.DESC, WebinarGttDetails.Constants.QUIZ_ATTEMPTED));
        query.limit(500);

        List<WebinarGttDetails> attendeeDetails = runQuery(query, WebinarGttDetails.class);

        List<WebinarScoreReport> webinarScoreReports = new ArrayList<>();
        webinarScoreReports.add(new WebinarScoreReport());
        webinarScoreReports.add(new WebinarScoreReport());
        webinarScoreReports.add(new WebinarScoreReport());
        webinarScoreReports.get(0).setMin(0);
        webinarScoreReports.get(0).setMax(50);
        webinarScoreReports.get(1).setMin(50);
        webinarScoreReports.get(1).setMax(70);
        webinarScoreReports.get(2).setMin(70);
        webinarScoreReports.get(2).setMax(100);

        if (attendeeDetails != null && !attendeeDetails.isEmpty()) {

            int score;

            for (WebinarGttDetails gttAttendeeDetails : attendeeDetails) {
                int attempted = gttAttendeeDetails.getQuizNumbers().getNumberAttempted();
                if (attempted != 0)
                    score = gttAttendeeDetails.getLbPoints() / attempted;
                else
                    score = 0;

                if (score <= 50)
                    webinarScoreReports.get(0).setCount(webinarScoreReports.get(0).getCount() + 1);
                else if (score <= 70)
                    webinarScoreReports.get(1).setCount(webinarScoreReports.get(1).getCount() + 1);
                else
                    webinarScoreReports.get(2).setCount(webinarScoreReports.get(2).getCount() + 1);
            }

            //persisting the calculated report to avoid querying same data again.
            //waiting for 5 mins after the first entry has been created.

            Long currentTime = System.currentTimeMillis();

            if (currentTime >= attendeeDetails.get(0).getCreationTime() + 300000) {
                Query addScoreReport = new Query();
                addScoreReport.addCriteria(Criteria.where(WebinarSessionDetails.Constants._ID).is(sessionId));
                Update update = new Update();
                update.set(WebinarSessionDetails.Constants.WEBINAR_SCORE_REPORT, webinarScoreReports);
                updateFirst(addScoreReport, update, WebinarSessionDetails.class);
            }
        }

        return webinarScoreReports;
    }
}
