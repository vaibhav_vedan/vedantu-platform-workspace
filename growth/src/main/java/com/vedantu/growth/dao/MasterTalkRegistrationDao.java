package com.vedantu.growth.dao;

import com.google.gson.Gson;
import com.vedantu.growth.entities.MasterTalkRegistration;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class MasterTalkRegistrationDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = LogFactory.getLogger(MasterTalkRegistrationDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private RedisDAO redisDAO;

    private static Gson gson = new Gson();

    private String getMasterTalkRegistrationRedisKey(String masterTalkId, String userId) {

        return "MASTER_TALK_REGISTRATION_" + masterTalkId + "_" + userId;
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public MasterTalkRegistration getMasterTalkRegisteredRecord(String masterTalkId, Long userId) {

        try {
            String key = getMasterTalkRegistrationRedisKey(masterTalkId, userId.toString());
            String value = redisDAO.get(key);

            if(StringUtils.isNotEmpty(value)){
                return gson.fromJson(value, MasterTalkRegistration.class);
            }
        }catch (Exception e){
            logger.error("Error fetching from redis : {}", e.getMessage(), e);
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(MasterTalkRegistration.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(MasterTalkRegistration.Constants.MASTER_TALK_ID).is(masterTalkId));
        MasterTalkRegistration masterTalksRegistration = findOne(query, MasterTalkRegistration.class);

        setInRedis(masterTalksRegistration);

        return masterTalksRegistration;
    }

    public List<MasterTalkRegistration> getMasterTalkRegisteredRecord(List<String> masterTalkIds, Long userId) {

        Query query = new Query();
        query.addCriteria(Criteria.where(MasterTalkRegistration.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(MasterTalkRegistration.Constants.MASTER_TALK_ID).in(masterTalkIds));
        List<MasterTalkRegistration> masterTalksRegistrations = runQuery(query, MasterTalkRegistration.class);

        return masterTalksRegistrations;
    }

    public void save(MasterTalkRegistration masterTalkRegistration, String callingUserId) {

        logger.info("ENTRY: " + masterTalkRegistration);
        if (masterTalkRegistration != null) {
            saveEntity(masterTalkRegistration, callingUserId);

            setInRedis(masterTalkRegistration);
        }
        logger.info("EXIT");
    }

    private void setInRedis(MasterTalkRegistration masterTalkRegistration) {

        if(Objects.isNull(masterTalkRegistration))
            return;

        try {
            String key = getMasterTalkRegistrationRedisKey(masterTalkRegistration.getMasterTalkId(), masterTalkRegistration.getUserId().toString());
            redisDAO.setex(key, gson.toJson(masterTalkRegistration), DateTimeUtils.HOURS_PER_2DAYS);
        }catch (Exception e){
            logger.error("Error saving in redis : {}", e.getMessage(), e);
        }
    }
}
