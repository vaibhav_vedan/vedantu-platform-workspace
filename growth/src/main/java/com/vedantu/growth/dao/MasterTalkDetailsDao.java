package com.vedantu.growth.dao;

import com.vedantu.growth.entities.MasterTalkDetails;
import com.vedantu.growth.pojo.MastertalkFilterReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MasterTalkDetailsDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = LogFactory.getLogger(MasterTalkDetailsDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(MasterTalkDetails masterTalkDetails, String callingUserId) {
        logger.info("ENTRY: {}", masterTalkDetails);
        if (masterTalkDetails != null) {
            saveEntity(masterTalkDetails, callingUserId);
        }
        logger.info("EXIT");
    }

    public MasterTalkDetails getMasterTalkDetailsByTypeUrl(String url) {

        Query query = new Query();
        query.addCriteria(Criteria.where(MasterTalkDetails.Constants.PAGE_URL).is(url));
        MasterTalkDetails masterTalkDetails = findOne(query, MasterTalkDetails.class);
        return masterTalkDetails;
    }

    public List<MasterTalkDetails> getMasterTalkDetails(Integer start, Integer size) {

        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, MasterTalkDetails.Constants.CREATION_TIME));
        query.fields().include(MasterTalkDetails.Constants.PAGE_URL);
        query.fields().include(MasterTalkDetails.Constants._ID);
        query.fields().include(MasterTalkDetails.Constants.CELEBRITY);
        query.fields().include(MasterTalkDetails.Constants.TALK_START_TIME);
        query.fields().include(MasterTalkDetails.Constants.ENTITY_STATE);
        query.skip(start);
        query.limit(size);

        return runQuery(query, MasterTalkDetails.class);
    }

    public MasterTalkDetails getMasterTalkDetailsById(String masterTalkId) {

        MasterTalkDetails masterTalkDetails = getEntityById(masterTalkId,MasterTalkDetails.class);
        return masterTalkDetails;
    }

    public Long getTotalmasterTalksCount() {

        Query query = new Query();
        return queryCount(query, MasterTalkDetails.class);

    }

    public List<MasterTalkDetails> getPastMasterTalksMetadata(Integer start, Integer size) {

        Query query = new Query();
        query.addCriteria(Criteria.where(MasterTalkDetails.Constants.TALK_END_TIME).lt(System.currentTimeMillis()));
        query.with(Sort.by(Sort.Direction.DESC, MasterTalkDetails.Constants.TALK_END_TIME));

        query.fields().include(MasterTalkDetails.Constants.TALK_TILE);
        query.fields().include(MasterTalkDetails.Constants.PROD_SESSION_URL_POST);
        query.fields().include(MasterTalkDetails.Constants.SESSION_LINKS);
        query.skip(start);
        query.limit(size);

        return runQuery(query, MasterTalkDetails.class);

    }

    public List<MasterTalkDetails> getUpcomingMasterTalks(MastertalkFilterReq mastertalkFilterReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MasterTalkDetails.Constants.TALK_END_TIME).gt(System.currentTimeMillis()));
        query.with(Sort.by(Sort.Direction.ASC, MasterTalkDetails.Constants.TALK_START_TIME));

        setMasterTalkHomePageRequiredFields(query);
        query.skip(mastertalkFilterReq.getStart());
        query.limit(mastertalkFilterReq.getSize());

        return runQuery(query, MasterTalkDetails.class);
    }

    public List<MasterTalkDetails> getPastMasterTalks(MastertalkFilterReq mastertalkFilterReq) {
        Query query = new Query();
        query.addCriteria(Criteria.where(MasterTalkDetails.Constants.TALK_END_TIME).lt(System.currentTimeMillis()));
        query.with(Sort.by(Sort.Direction.DESC, MasterTalkDetails.Constants.TALK_END_TIME));

        setMasterTalkHomePageRequiredFields(query);
        query.skip(mastertalkFilterReq.getStart());
        query.limit(mastertalkFilterReq.getSize());

        return runQuery(query, MasterTalkDetails.class);
    }

    private void setMasterTalkHomePageRequiredFields(Query query) {
        query.fields().include(MasterTalkDetails.Constants.TALK_TILE);
        query.fields().include(MasterTalkDetails.Constants.CELEBRITY);
        query.fields().include(MasterTalkDetails.Constants.PAGE_URL);
        query.fields().include(MasterTalkDetails.Constants.TALK_START_TIME);
        query.fields().include(MasterTalkDetails.Constants.TALK_END_TIME);
        query.fields().include(MasterTalkDetails.Constants.LAUNCH_DATE);
        query.fields().include(MasterTalkDetails.Constants.RELEASE_DATE);
        query.fields().include(MasterTalkDetails.Constants.PRE_TO_LIVE_TIME);
        query.fields().include(MasterTalkDetails.Constants.TO_LIVE_POST_TIME);
        query.fields().include(MasterTalkDetails.Constants.LIVE_TIME);
        query.fields().include(MasterTalkDetails.Constants.LIVE_DATE_MONTH);
        query.fields().include(MasterTalkDetails.Constants.COUNTER_LIMIT);
        query.fields().include(MasterTalkDetails.Constants.SESSION_LINKS);
    }

}

