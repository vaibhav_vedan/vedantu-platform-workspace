package com.vedantu.growth.dao;

import com.vedantu.User.enums.EventName;
import com.vedantu.growth.entities.ReviseStats;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;


@Service
public class ReviseDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReviseDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public <E extends AbstractMongoStringIdEntity> void save(E p, String callingUserId) {
        logger.info("ENTRY: " + p);
        if (p != null) {
            saveEntity(p, callingUserId);
        }
        logger.info("EXIT");
    }

    public ReviseStats getJeeRevise(EventName event, Long userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(ReviseStats.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(ReviseStats.Constants.EVENT).is(event));
        List<ReviseStats> reviseStats = runQuery(query, ReviseStats.class);
        if(reviseStats.size() > 0){
            return reviseStats.get(0);
        }
        return null;
    }

    public void updateReviseStatsAfterTest(Update update, String callingUserId, EventName event) {
        if (update == null || StringUtils.isEmpty(callingUserId)) {
            return;
        }
        if (!StringUtils.isEmpty(callingUserId)) {
            update.set(ReviseStats.Constants.LAST_UPDATED_BY, callingUserId);
        }
        update.set(ReviseStats.Constants.LAST_UPDATED, System.currentTimeMillis());
        Query query = new Query();
        query.addCriteria(Criteria.where(ReviseStats.Constants.USER_ID).is(Long.parseLong(callingUserId)));
        query.addCriteria(Criteria.where(ReviseStats.Constants.EVENT).is(event));
        upsertEntity(query, update, ReviseStats.class);
    }


}
