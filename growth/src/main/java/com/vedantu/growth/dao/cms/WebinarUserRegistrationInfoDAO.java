/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.dao.cms;

import com.google.gson.Gson;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.entities.Webinar;
import com.vedantu.growth.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 *
 * @author jeet
 */
@Service
public class WebinarUserRegistrationInfoDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = LogFactory.getLogger(WebinarDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private RedisDAO redisDAO;

    private Gson gson = new Gson();

    public WebinarUserRegistrationInfoDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(WebinarUserRegistrationInfo p) throws InternalServerErrorException {
        if (p != null && p.getUserId() != null) {
            saveEntity(p);
            setWebinarRegistrationInfoInRedis(p);
        }
    }

    public WebinarUserRegistrationInfo getById(String id) {
        WebinarUserRegistrationInfo p = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                p = getEntityById(id, WebinarUserRegistrationInfo.class);
            }
        } catch (Exception ex) {
            logger.error("error in getById id:"+id+" ex:"+ex.getMessage());
            p = null;
        }
        return p;
    }

    public void save(WebinarUserRegistrationInfo p) throws InternalServerErrorException {
        if (p != null) {
            if(null != p.getUserId()) {
                setWebinarRegistrationInfoInRedis(p);
            }
            saveEntity(p);
        }
    }

    private void setWebinarRegistrationInfoInRedis(WebinarUserRegistrationInfo p) throws InternalServerErrorException {
        String key = "WEBINARUSERREGITRATIONINFO_" + p.getUserId() + "_" + p.getWebinarId();
        redisDAO.setex(key, gson.toJson(p), DateTimeUtils.HOURS_PER_2DAYS);
    }

    public WebinarUserRegistrationInfo getWebinarUserInfo(String userId, String webinarId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).is(webinarId));

        WebinarUserRegistrationInfo webinarUserRegistrationInfo = findOne(query, WebinarUserRegistrationInfo.class);
        return webinarUserRegistrationInfo;
    }

    public List<WebinarUserRegistrationInfo> getWebinarUserInfo(String userId, List<String> webinarIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).in(webinarIds));

        return runQuery(query, WebinarUserRegistrationInfo.class);
    }

    public Set<String> getWebinarUserInfoByWebinars(String userId, Set<String> parentWebinarList) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).in(parentWebinarList));
        List<WebinarUserRegistrationInfo> webinars = runQuery(query, WebinarUserRegistrationInfo.class);
        Set<String> watchedWebinars = new HashSet<>();
        for(WebinarUserRegistrationInfo w : webinars) {
            watchedWebinars.add(w.getWebinarId());
        }
        return watchedWebinars;
    }

    public List<WebinarUserRegistrationInfo> getUpcomingWebinarRegistrationForUser(String userId, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.END_TIME).gte(System.currentTimeMillis()));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.with(Sort.by(Sort.Direction.ASC,
                WebinarUserRegistrationInfo.Constants.USER_ID, WebinarUserRegistrationInfo.Constants.END_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, WebinarUserRegistrationInfo.class);
    }

    public WebinarUserRegistrationInfo getWebinarUserInfoTrainingId(String userId, String trainingId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.TRAININGID).is(trainingId));

        WebinarUserRegistrationInfo webinarUserRegistrationInfo = findOne(query, WebinarUserRegistrationInfo.class);
        return webinarUserRegistrationInfo;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, WebinarUserRegistrationInfo.class);
        } catch (Exception ex) {
            logger.error("error in deleteById id:"+id+" ex:"+ex.getMessage());
        }

        return result;
    }

    public List<WebinarUserRegistrationInfo> getWebinarUserRegistrationInfos(String webinarId, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).is(webinarId));
        query.with(Sort.by(Sort.Direction.ASC, WebinarUserRegistrationInfo.Constants.CREATION_TIME));
        setFetchParameters(query, start, size);
        logger.info("Query : " + query);
        return runQuery(query, WebinarUserRegistrationInfo.class);
    }

    public List<WebinarUserRegistrationInfo> getWebinarUserRegistrationInfosWithQuestion(String webinarId, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).is(webinarId));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.QUESTIONS).exists(true).ne(Collections.EMPTY_LIST));
        query.with(Sort.by(Sort.Direction.ASC, WebinarUserRegistrationInfo.Constants.CREATION_TIME));
        setFetchParameters(query, start, size);
        return runQuery(query, WebinarUserRegistrationInfo.class);
    }
    public Long getWebinarUserRegistrationInfosCount(String webinarId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).is(webinarId));
        return queryCount(query, WebinarUserRegistrationInfo.class);
    }

    public List<WebinarUserRegistrationInfo> getWebinarRegistrationsForMobileUser(String userId, Integer start, Integer size) {
        Long last3Time = System.currentTimeMillis() - 12 * DateTimeUtils.MILLIS_PER_WEEK;
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.CREATION_TIME).gte(last3Time));
        query.fields().include(WebinarUserRegistrationInfo.Constants.WEBINARID);
        query.fields().include(WebinarUserRegistrationInfo.Constants.CREATION_TIME);
        setFetchParameters(query, start, size);
        return runQuery(query, WebinarUserRegistrationInfo.class);
    }

    public Long getWebinarUserRegInfoByUserId(String userId, Long releaseDate) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.LAST_UPDATED).gte(releaseDate));
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINAR_ATTENDED).is(true));

        return queryCount(query, WebinarUserRegistrationInfo.class);
    }

    public List<String> getAttendedIds(String userId, Set<String> parentWebIds)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.USER_ID).is(userId))
                .addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINAR_ATTENDED).is(true))
                .addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).in(parentWebIds));

        query.fields().include(WebinarUserRegistrationInfo.Constants.WEBINARID);

        logger.info("Query : {}", query);

        List<WebinarUserRegistrationInfo> attendedWeb = runQuery(query, WebinarUserRegistrationInfo.class);

        List<String> attendedIds = new ArrayList<>();

        for (WebinarUserRegistrationInfo w : attendedWeb)
            attendedIds.add(w.getWebinarId());

        return  attendedIds;
    }

    public void updateRegistrationPostWebinarUpdate(Webinar webinar) {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.WEBINARID).is(webinar.getId()));
        Update update=new Update();
        update.set(WebinarUserRegistrationInfo.Constants.START_TIME, webinar.getStartTime());
        update.set(WebinarUserRegistrationInfo.Constants.END_TIME, webinar.getEndTime());
        update.set(WebinarUserRegistrationInfo.Constants.ENTITY_STATE, webinar.getWebinarStatus());
        update.set(WebinarUserRegistrationInfo.Constants.LAST_UPDATED,System.currentTimeMillis());
        updateMulti(query,update,WebinarUserRegistrationInfo.class);
    }
}

