/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.dao.cms;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.entities.Webinar;
import com.vedantu.growth.enums.cms.TimeFrame;
import com.vedantu.growth.enums.cms.WebinarStatus;
import com.vedantu.growth.enums.cms.WebinarToolType;
import com.vedantu.growth.enums.cms.WebinarType;
import com.vedantu.growth.pojo.cms.WebinarCallbackData;
import com.vedantu.growth.requests.cms.FindWebinarsReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;

/**
 *
 * @author jeet
 */
@Service
public class WebinarDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    @Autowired
    RedisDAO redisDAO;

    Logger logger = LogFactory.getLogger(WebinarDAO.class);

    private final Gson gson = new Gson();

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public WebinarDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void upsert(Webinar p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            super.saveEntity(p, callingUserIdString);
        }
    }

    public void upsert(WebinarCallbackData p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            super.saveEntity(p, callingUserIdString);
        }
    }

    public Webinar getByWebinarCode(String code) {
        logger.info("getByWebinarCode ", code);
        Webinar webinarListing = null;
        Query query = new Query();

        if (code != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.WEBINARCODE).is(
                    code));
            logger.info("query " + query);
            webinarListing = findOne(query, Webinar.class);
            logger.info("getByWebinarCode" + webinarListing);
        }
        return webinarListing;
    }

    public List<Webinar> getByWebinarsCodes(Set<String> codes) {
        logger.info("getByWebinarsCodes ", codes);
        List<Webinar> webinarListing = new ArrayList<>();
        Query query = new Query();

        if (CollectionUtils.isNotEmpty(codes)) {
            query.addCriteria(Criteria.where(Webinar.Constants.WEBINARCODE).in(codes));
            logger.info("query " + query);
            webinarListing = runQuery(query, Webinar.class);
            logger.info("getByWebinarsCodes" + gson.toJson(webinarListing));
        }
        return webinarListing;
    }

    public WebinarCallbackData getWebinarCallbackWithUser(Long userId) {
        Query query = new Query();
        WebinarCallbackData webinarCallbackData = null;
        if(userId != null) {
            query.addCriteria(Criteria.where(WebinarCallbackData.Constants.USERID).is(userId));
            query.with(Sort.by(Sort.Direction.DESC, Webinar.Constants.CREATION_TIME));
        }
        webinarCallbackData = findOne(query, WebinarCallbackData.class);
        return webinarCallbackData;
    }

    public Webinar getById(String id) throws InternalServerErrorException {
        logger.info("getById ", id);
        Webinar webinarListing = null;

        String key = "WEBINAR_" + id;
        try {
            String stats = redisDAO.get(key);
            if (stats != null) {
                return gson.fromJson(stats, Webinar.class);
            }
        } catch (InternalServerErrorException | BadRequestException e) {
            logger.warn(e.getErrorMessage(), e);
        }

        if (id != null) {
            webinarListing = super.getEntityById(id,
                    Webinar.class);
            logger.info("getById" + webinarListing);
        }

        if(null != webinarListing) {
            redisDAO.setex(key, gson.toJson(webinarListing), DateTimeUtils.HOURS_PER_2DAYS);
        }
        return webinarListing;
    }

    private Query findWebinarQuery(FindWebinarsReq findReq) {
        Query query = new Query();
        if (findReq.getStart() == null) {
            findReq.setStart(0);
        }
        if (findReq.getSize() == null) {
            findReq.setSize(20);
        }

        if (findReq.getType() != null) {
            if (WebinarType.WEBINAR.equals(findReq.getType())) {
                logger.info("IN WEBINARTYPEWEBINAR");
                Criteria criteria = new Criteria();
        		query.addCriteria(Criteria.where(Webinar.Constants.TYPE).exists(false));
                query.addCriteria(criteria);
            } else {
                logger.info("IN WEBINARTYPEQUIZ");
                query.addCriteria(Criteria.where(Webinar.Constants.TYPE).is(
                        findReq.getType()));
            }
        }

        if (Objects.nonNull(findReq.getTimeFrame()) && TimeFrame.PAST.equals(findReq.getTimeFrame())) {
            query.addCriteria(Criteria.where(Webinar.Constants.REPLAY_CREATED).is(true));
        }

        if (StringUtils.isNotEmpty(findReq.getTarget())) {
            query.addCriteria(Criteria.where(Webinar.Constants.TARGETS).is(
                    findReq.getTarget()));
        }
        if (StringUtils.isNotEmpty(findReq.getBoard())) {
            query.addCriteria(Criteria.where(Webinar.Constants.BOARDS).is(
                    findReq.getBoard()));
        }
        if (StringUtils.isNotEmpty(findReq.getTag())) {
            query.addCriteria(Criteria.where(Webinar.Constants.TAGS).is(
                    findReq.getTag()));
        }
        if (StringUtils.isNotEmpty(findReq.getWebinarCode())) {
            int length = findReq.getWebinarCode().length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(Webinar.Constants.WEBINARCODE).regex(
                    Pattern.quote(findReq.getWebinarCode().substring(0, index)), "i"));
        }
        if (StringUtils.isNotEmpty(findReq.getTitle())) {
            int length = findReq.getTitle().length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(Webinar.Constants.TITLE).regex(
                    Pattern.quote(findReq.getTitle().substring(0, index)), "i"));
        }
        if (StringUtils.isNotEmpty(findReq.getTeacherEmail())) {
            int length = findReq.getTeacherEmail().length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(Webinar.Constants.TEACHEREMAIL).regex(
                    Pattern.quote(findReq.getTeacherEmail().substring(0, index)), "i"));
        }
        if (findReq.getShowInPastWebinars() != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.SHOW_IN_PAST_WEBINARS).is(findReq.getShowInPastWebinars()));
        }
        if (findReq.getAfterStartTime() != null && findReq.getBeforeStartTime() != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.STARTTIME).gte(
                    findReq.getAfterStartTime()
            ).lte(findReq.getBeforeStartTime()));
        } else if (findReq.getAfterStartTime() == null && findReq.getBeforeStartTime() != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.STARTTIME).lte(
                    findReq.getBeforeStartTime()
            ));
        } else if (findReq.getAfterStartTime() != null && findReq.getBeforeStartTime() == null) {
            query.addCriteria(Criteria.where(Webinar.Constants.STARTTIME).gte(
                    findReq.getAfterStartTime()
            ));
        }

        if (findReq.getAfterEndTime() != null && findReq.getBeforeEndTime() != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.ENDTIME).gte(
                    findReq.getAfterEndTime()
            ).lte(findReq.getBeforeEndTime()));
        } else if (findReq.getAfterEndTime() == null && findReq.getBeforeEndTime() != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.ENDTIME).lte(
                    findReq.getBeforeEndTime()
            ));
        } else if (findReq.getAfterEndTime() != null && findReq.getBeforeEndTime() == null) {
            query.addCriteria(Criteria.where(Webinar.Constants.ENDTIME).gte(
                    findReq.getAfterEndTime()
            ));
        }
        if (StringUtils.isNotEmpty(findReq.getGrade())) {
            query.addCriteria(Criteria.where(Webinar.Constants.GRADES).is(
                    findReq.getGrade()));
        }else if (CollectionUtils.isNotEmpty(findReq.getGrades())) {
            query.addCriteria(Criteria.where(Webinar.Constants.GRADES).in(
                    findReq.getGrades()));
        }
        if (findReq.getWebinarStatus() != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.WEBINAR_STATUS).is(
                    findReq.getWebinarStatus()
            ));
        }
        if (findReq.getToolType() != null) {
            query.addCriteria(Criteria.where(Webinar.Constants.TOOL_TYPE).in(
                    findReq.getToolType()
            ));
        }
        if (findReq.getShowSimLiveWebinars() != null && findReq.getShowSimLiveWebinars() == false) {
            query.addCriteria(Criteria.where(Webinar.Constants.IS_SIME_LIVE).ne(true));
        }else if(findReq.getSimLive() != null && findReq.getSimLive()){
            query.addCriteria(Criteria.where(Webinar.Constants.IS_SIME_LIVE).is(true));
        }

        query.limit(findReq.getSize());
        query.skip(findReq.getStart());
        if (findReq.getSortOrder() != null) {
            query.with(Sort.by(Sort.Direction.valueOf(findReq.getSortOrder().name()), Webinar.Constants.STARTTIME));
        } else {
            query.with(Sort.by(Sort.Direction.ASC, Webinar.Constants.STARTTIME));
        }

        return query;
    }

    public List<Webinar> findWebinarsClassRoomMobile(FindWebinarsReq findReq)
    {
        logger.info("findWebinars =" + findReq);
        List<Webinar> webinars = null;
        findReq.setToolType(null);
        Query query = findWebinarQuery(findReq);

        if (TimeFrame.UPCOMING.equals(findReq.getTimeFrame())) {

            Long currentTime = System.currentTimeMillis();

            //live webinars will be visible only for 15 mins.
            Criteria liveAndUpcoming = Criteria.where(Webinar.Constants.STARTTIME).gte(currentTime - 900000);
            query.addCriteria(liveAndUpcoming);
        }

        query.addCriteria(Criteria.where(Webinar.Constants.TOOL_TYPE).in(
                Arrays.asList(WebinarToolType.VEDANTU_WAVE, WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD)
        ));
        if (findReq.isRestricted() == null || findReq.isRestricted())
            query.addCriteria(Criteria.where(Webinar.Constants.PARENT_WEBINAR_ID).exists(false));

        if (TimeFrame.PAST.equals(findReq.getTimeFrame()))
            query.with(Sort.by(Sort.Direction.DESC, Webinar.Constants.STARTTIME));
        else
            query.with(Sort.by(Sort.Direction.ASC, Webinar.Constants.STARTTIME));

        logger.info("findWebinarsClassRoom query:" + query);
        webinars = runQuery(query, Webinar.class);
        logger.info("findWebinars" + webinars);
        return webinars;
    }

    public List<Webinar> findWebinars(FindWebinarsReq findReq) {
        logger.info("findWebinars =" + findReq);
        List<Webinar> webinars = null;
        Query query = findWebinarQuery(findReq);
        logger.info("findWebinars query:" + query);
        webinars = runQuery(query, Webinar.class);
        logger.info("findWebinars" + webinars);
        return webinars;
    }

    public Webinar findWebinar(FindWebinarsReq findReq) {
        logger.info("findWebinars =" + findReq);
        Query query = findWebinarQuery(findReq);
        logger.info("findWebinars query:" + query);
        return findOne(query, Webinar.class);
    }

    public List<Webinar> findWebinarsByGradeAndBoard(String board, String grade, Integer start, Integer size) {
        Long currentTime = System.currentTimeMillis();
        Query query = new Query();
        if (StringUtils.isNotEmpty(board)) {
            query.addCriteria(Criteria.where(Webinar.Constants.BOARDS).is(board.toUpperCase()));
        }
//        query.addCriteria(Criteria.where(Webinar.Constants.TYPE).is(WebinarType.WEBINAR));
        query.addCriteria(Criteria.where(Webinar.Constants.ENDTIME).gt(currentTime));
        query.addCriteria(Criteria.where(Webinar.Constants.GRADES).is(grade));
        query.addCriteria(Criteria.where(Webinar.Constants.WEBINAR_STATUS).is(WebinarStatus.ACTIVE));
        setFetchParameters(query, start, size);
        return runQuery(query, Webinar.class);
    }

    public List<Webinar> findWebinarsByGradeAndBoardPast(String board, String grade, Integer start, Integer size) {
        Long currentTime = System.currentTimeMillis();
        Query query = new Query();
        if (StringUtils.isNotEmpty(board)) {
            query.addCriteria(Criteria.where(Webinar.Constants.BOARDS).is(board.toUpperCase()));
        }
        query.addCriteria(Criteria.where(Webinar.Constants.ENDTIME).lt(currentTime));
        query.addCriteria(Criteria.where(Webinar.Constants.GRADES).is(grade));
        query.addCriteria(Criteria.where(Webinar.Constants.SHOW_IN_PAST_WEBINARS).is(Boolean.TRUE));
        query.addCriteria(Criteria.where(Webinar.Constants.WEBINAR_STATUS).is(WebinarStatus.ACTIVE));
        setFetchParameters(query, start, size);
        return runQuery(query, Webinar.class);
    }

    public List<Webinar> getLiveOrUpcomingWebinarsForIds(Set<String> webinarIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Webinar.Constants.ID).in(webinarIds));
        query.addCriteria(Criteria.where(Webinar.Constants.ENDTIME).gt(System.currentTimeMillis()));
        return runQuery(query, Webinar.class);
    }

    public List<Webinar> getUpcomingWebinarsForIds(Set<String> webinarIds, int start, int size) {
        if (webinarIds.size() > 1000) {
            logger.error("Querying for more than 1000 docs for getUpcomingWebinarsForIds");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Webinar.Constants.ID).in(webinarIds));
        query.addCriteria(Criteria.where(Webinar.Constants.ENDTIME).gt(System.currentTimeMillis()));
        query.addCriteria(Criteria.where(Webinar.Constants.WEBINAR_STATUS).is(WebinarStatus.ACTIVE));
        query.fields().include(Webinar.Constants.SESSION_ID);
        query.fields().include(Webinar.Constants._ID);
        query.fields().include(Webinar.Constants.SUBJECTS);
        query.with(Sort.by(Sort.Direction.ASC,Webinar.Constants.STARTTIME));
        setFetchParameters(query, start, size);
        return Optional.ofNullable(runQuery(query, Webinar.class))
                .orElseGet(ArrayList::new);
    }

    public List<Webinar> getWebinarsForIds(Set<String> webinarIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Webinar.Constants.ID).in(webinarIds));
        query.with(Sort.by(Sort.Direction.DESC,Webinar.Constants.STARTTIME));
        return runQuery(query, Webinar.class);
    }

    public List<Webinar> getWebinarsByStartTime(long fromTime, long toTime) {
        logger.info("getWebinarsByEndtime toTime=" + toTime + ", fromTime=" + fromTime );
        List<Webinar> webinars;
        Query query = new Query();
        Criteria criteria = new Criteria();
        if (fromTime > 0) {
            criteria = Criteria.where(Webinar.Constants.STARTTIME).gte(fromTime);
            if (toTime > 0) {
                criteria.lte(toTime);
            }
        }
        query.addCriteria(criteria);
        logger.info("findWebinars query:" + query);
        webinars = runQuery(query, Webinar.class);
        logger.info("findWebinars" + webinars);
        return webinars;
    }
    public List<Webinar> getWebinarsByEndTime(long fromTime, long toTime) {
        logger.info("getWebinarsByEndtime toTime=" + toTime + ", fromTime=" + fromTime );
        List<Webinar> webinars;
        Query query = new Query();
        Criteria criteria = new Criteria();
        if (fromTime > 0) {
            criteria = Criteria.where(Webinar.Constants.ENDTIME).gte(fromTime);
            if (toTime > 0) {
                criteria.lte(toTime);
            }
        }
        query.addCriteria(criteria);
        logger.info("findWebinars query:" + query);
        webinars = runQuery(query, Webinar.class);
        logger.info("findWebinars" + webinars);
        return webinars;
    }

    public List<Webinar> findWebinarsForHomeFeed(FindWebinarsReq findWebinarsReq)
    {
        Query query = findWebinarQuery(findWebinarsReq);
        Long currentTime = System.currentTimeMillis();
        //live webinars will be visible only for 15 mins.
        Criteria liveAndUpcoming = Criteria.where(Webinar.Constants.STARTTIME).gte(currentTime - 900000);
        query.addCriteria(liveAndUpcoming);

        query.addCriteria(Criteria.where(Webinar.Constants.TOOL_TYPE).in(Arrays.asList(WebinarToolType.VEDANTU_WAVE, WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD)));
        if (findWebinarsReq.isRestricted() == null || findWebinarsReq.isRestricted())
            query.addCriteria(Criteria.where(Webinar.Constants.PARENT_WEBINAR_ID).exists(false));
        logger.info("findWebinar query : {}", query);
        getProjectionQuery(query);
        query.with(Sort.by(Sort.Direction.ASC,Webinar.Constants.STARTTIME));
        logger.info("Query for fetching webinars : {}", query);
        List<Webinar> webinars = runQuery(query, Webinar.class);
        logger.info("webinar list : {}", webinars);
        return webinars;
    }

    public void getProjectionQuery(Query query)
    {
        query.fields().include(Webinar.Constants._ID)
                .include(Webinar.Constants.WEBINARCODE)
                .include(Webinar.Constants.PARENT_WEBINAR_ID)
                .include(Webinar.Constants.TITLE)
                .include(Webinar.Constants.STARTTIME)
                .include(Webinar.Constants.ENDTIME)
                .include(Webinar.Constants.THUMBNAIL)
                .include(Webinar.Constants.SESSION_ID)
                .include(Webinar.Constants.CREATION_TIME)
                .include(Webinar.Constants.GRADES)
                .include(Webinar.Constants.TEACHER_INFO)
                .include(Webinar.Constants.WEBINAR_INFO)
                .include(Webinar.Constants.SUBJECTS)
                .include(Webinar.Constants.IS_SIME_LIVE)
                .include(Webinar.Constants.PARENT_WEBINAR_ID);

        logger.info("Projection Query : {}", query);
    }

    public List<Webinar> getSimLiveParentIds(Set<String> parentWebIds)
    {
        Query query = new Query();
        query.addCriteria(Criteria.where(Webinar.Constants.PARENT_WEBINAR_ID).in(parentWebIds));

        query.with(Sort.by(Sort.Direction.DESC, Webinar.Constants.LAST_UPDATED));
        query.limit(50);

        query.fields().include(Webinar.Constants.PARENT_WEBINAR_ID)
                .include(Webinar.Constants._ID);

        logger.info("Query : {}", query);
        return runQuery(query, Webinar.class);
    }

    public void insertAll(List<Webinar> entities, String collectionName, String callingUserId) {


        insertAllEntities(entities,collectionName,callingUserId);

    }
    
    public List<Webinar> findLiveWebinarsForApp(FindWebinarsReq findWebinarsReq){
    	
    	Query query = findWebinarQuery(findWebinarsReq);
        Long currentTime = System.currentTimeMillis();
        
        //live webinars will be visible only for 15 mins but an upcoming webinar will go live 10 mins before it's start Time.
        query.addCriteria(Criteria.where(Webinar.Constants.STARTTIME).gte(currentTime - 900000).lte(currentTime + 600000));
        query.addCriteria(Criteria.where(Webinar.Constants.TOOL_TYPE).in(Arrays.asList(WebinarToolType.VEDANTU_WAVE, WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD)));
        
        if (findWebinarsReq.isRestricted() == null || findWebinarsReq.isRestricted())
            query.addCriteria(Criteria.where(Webinar.Constants.PARENT_WEBINAR_ID).exists(false));
        
        if(null != findWebinarsReq.getWebinarToSessionLimit()) {
        	query.limit(findWebinarsReq.getWebinarToSessionLimit());
        }
        
        logger.debug("findWebinar query : {}", query);
        
        getProjectionQuery(query);
        
        query.with(Sort.by(Sort.Direction.DESC,Webinar.Constants.STARTTIME));
        
        logger.debug("Query for fetching findLiveWebinarsForApp : {}", query);
        
        List<Webinar> webinars = runQuery(query, Webinar.class);
        logger.debug("webinar list : {}", webinars);
        
        return webinars;
    }
    
    public List<Webinar> findUpcomingWebinarsForApp(FindWebinarsReq findWebinarsReq){
    	
    	Query query = findWebinarQuery(findWebinarsReq);
        Long currentTime = System.currentTimeMillis();

        query.addCriteria(Criteria.where(Webinar.Constants.STARTTIME).gt(currentTime + 600000).lt(DateTimeUtils.getTimestampForTomorrowMidnight()));

        query.addCriteria(Criteria.where(Webinar.Constants.TOOL_TYPE).in(Arrays.asList(WebinarToolType.VEDANTU_WAVE, WebinarToolType.VEDANTU_WAVE_BIG_WHITEBOARD)));
        
        if (findWebinarsReq.isRestricted() == null || findWebinarsReq.isRestricted())
            query.addCriteria(Criteria.where(Webinar.Constants.PARENT_WEBINAR_ID).exists(false));
        
        if(null != findWebinarsReq.getWebinarToSessionLimit()) {
        	query.limit(findWebinarsReq.getWebinarToSessionLimit());
        }
        
        logger.debug("findWebinar query : {}", query);
        
        getProjectionQuery(query);
        
        query.with(Sort.by(Sort.Direction.ASC,Webinar.Constants.STARTTIME));
        
        logger.debug("Query for fetching findUpcomingWebinarsForApp : {}", query);
        
        List<Webinar> webinars = runQuery(query, Webinar.class);
        logger.debug("webinar list : {}", webinars);
        
        return webinars;
    }

    public void markReplayAvailability(String sessionId, Boolean isReplayAvailable){
        Query query = new Query();
        query.addCriteria(Criteria.where(Webinar.Constants.SESSION_ID).is(sessionId));
        Update update = new Update();
        update.set(Webinar.Constants.REPLAY_CREATED, isReplayAvailable);
        updateFirst(query, update, Webinar.class);
    }
}
