/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.notifications;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.growth.aws.AwsSQSManager;
import com.vedantu.growth.managers.UserManager;
import com.vedantu.growth.seo.PageBuilderManager;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;

/**
 *
 * @author somil
 */
@Service
public class SMSManager {

    
    
    @Autowired
    private UserManager userManager;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SMSManager.class);

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");


    public String sendSMS(TextSMSRequest request) throws VException {
        if(StringUtils.isEmpty(request.getTo())) {
            logger.warn("To field is empty: "+ request);
            return null;
        }

        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(request));
        return null;
    }
    
    public void sendUserSMSToDownloadSEOContent(User user, String contentId) {
        logger.info("Sending sms to download seo content" + user);
        try {
            if (user != null) {
                String downloadUrl = null;
                try {
                    downloadUrl = pageBuilderManager.getModifiedPDFDownloadLink(contentId);
                } catch (Exception e) {
                    logger.error("error in modifying content pdf:" + contentId + " email Exception:" + e.getMessage() + "\n" + ExceptionUtils.getStackTrace(e));
                }
                if (StringUtils.isEmpty(downloadUrl)) {
                    downloadUrl = pageBuilderManager.getPublicDownloadUrl(contentId);
                }


                String masterClassUrl = FOS_ENDPOINT +"/masterclass/?utm_source=crm&utm_medium=crm_sms_pdf&utm_campaign=crm_sms_pdf-masterclass&utm_content=" + contentId + "&utm_term=" + user.getContactNumber();


                String shortDownloadUrl = WebUtils.INSTANCE.shortenUrl(downloadUrl);
                String shortMasterclassUrl = WebUtils.INSTANCE.shortenUrl(masterClassUrl);

                HashMap<String, Object> scopesParams = new HashMap<>();
                scopesParams.put("pdfLink", shortDownloadUrl);
                scopesParams.put("masterclassLink", shortMasterclassUrl);
                scopesParams.put("firstName", user.getFirstName());

                CommunicationType communicationType = CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD_SMS;

                String phoneNumber = user.getContactNumber();
                String phoneCode = user.getPhoneCode();


                TextSMSRequest textSMSRequest = new TextSMSRequest(phoneNumber, phoneCode, scopesParams, communicationType , Role.STUDENT);
                sendSMS(textSMSRequest);

            }
        } catch (VException e) {
            logger.error("error in sending content download sms", e);
        }
    }

}


