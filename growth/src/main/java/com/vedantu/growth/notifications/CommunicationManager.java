/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.notifications;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.User;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.growth.aws.AwsSQSManager;
import com.vedantu.growth.entities.Webinar;
import com.vedantu.growth.managers.UserManager;
import com.vedantu.growth.seo.PageBuilderManager;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.EmailPriorityType;
import com.vedantu.notification.enums.EmailService;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/**
 *
 * @author somil
 */
@Service
public class CommunicationManager {

    public static final String PARTNER_LINK = "partnerLink";
    public static final String PARTNER_NAME = "partnerName";
    public static final String PARTNER_CONTACT_NUMBER = "partnerContactNumber";
    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    public static final String TIME_ZONE_GMT = "GMT";
    public static String TEACHER_CARE_EMAIL_ID = new String();
    public static String ACADEMICS_EMAIL_ID = new String();
    public static String DNA_EMAIL_ID = new String();
    public static String PRODUCT_EMAIL_ID = new String();
    public static String TRAIL_UPDATE_EMAIL_ID = new String();

    private static final SimpleDateFormat sdfTime = new SimpleDateFormat("hh:mma");
    private static final SimpleDateFormat sdfDateDay = new SimpleDateFormat("E, dd MMM yyyy");

    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

    public static final int TASK_BUFFER_IN_MILLIES = 5 * DateTimeUtils.MILLIS_PER_MINUTE;
    public String[] hyderabadVariations = {"Hyd", "Hyderabad", "Hderabad", "Hyd", "Hyderbad", "Hyderebad", "hydreabad",
        "hydrabad", "hyedrabad", "hyderabad`", "hyderabah"};

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CommunicationManager.class);

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private UserManager userManager;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @PostConstruct
    public void init() {
        TEACHER_CARE_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.teacherCare");
        ACADEMICS_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.academics");
        DNA_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.dna");
        PRODUCT_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.product");
        TRAIL_UPDATE_EMAIL_ID = ConfigUtils.INSTANCE.getStringValue("email.trail.update");
    }


    public void sendUserEmailToDownloadSEOContent(User user, String contentId) {
        logger.info("Sending email to download seo content" + user);
        try {
            if (user != null) {
                StudentInfo studentInfo = user.getStudentInfo();
                HashMap<String, Object> bodyScopes = new HashMap<>();
                bodyScopes.put("webinarExists", false);
                bodyScopes.put("webinarRegisterLink", FOS_ENDPOINT + "/webinar/?otpRequired=true&utm_source=pdf_download_email");
                String downloadUrl = null;
                try {
                    downloadUrl = pageBuilderManager.getModifiedPDFDownloadLink(contentId);
                } catch (Exception e) {
                    logger.error("error in modifying content pdf:" + contentId + " email Exception:" + e.getMessage() + "\n" + ExceptionUtils.getStackTrace(e));
                }
                if (StringUtils.isEmpty(downloadUrl)) {
                    downloadUrl = pageBuilderManager.getPublicDownloadUrl(contentId);
                }
                bodyScopes.put("contentId", contentId);
                bodyScopes.put("contactNumber", user.getContactNumber());
                bodyScopes.put("pdfDownloadLink", downloadUrl);
                CommunicationType communicationType = CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD_2;

                ArrayList<InternetAddress> toList = new ArrayList<>();
                toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
                EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, user.getRole());
                request.setIncludeHeaderFooter(false);
                request.setClickTrackersEnabled(true);
                request.setPriorityType(EmailPriorityType.HIGH);
                //request.setEmailService(EmailService.MANDRILL);
                sendEmailViaRest(request);
            }
        } catch (VException e) {
            logger.error("error in sending content download email", e);
        } catch (UnsupportedEncodingException e) {
            logger.error("error in sending content download email", e);
        }
    }

    public String sendEmail(EmailRequest request) throws VException {
        logger.info("Inside sendEmail" + new Gson().toJson(request));

        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, new Gson().toJson(request));
        return null;
    }

    public String sendEmailViaRest(EmailRequest request) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }

    public void sendWebinarQuestionsEmail(Webinar webinar, String data, List<InternetAddress> ccList) throws AddressException, VException {
        if (StringUtils.isEmpty(webinar.getTeacherEmail())) {
            return;
        }
        EmailRequest email = new EmailRequest();
        SimpleDateFormat sdfWebinarReg = new SimpleDateFormat("hh:mma, dd MMM yyyy");
        sdfWebinarReg.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String timestr = sdfWebinarReg.format(new Date(webinar.getStartTime()));
        email.setBody("Please find the attachment. It contains Questions, Which were asked by students on registration");
        String subject = "Questions for webinar on " + webinar.getTitle() + " at " + timestr;

        subject = (ConfigUtils.INSTANCE.getBooleanValue("email.SUBJECT.env.append")
                ? ConfigUtils.INSTANCE.getStringValue("environment") + ": "
                : "") + subject;

        email.setSubject(subject);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(webinar.getTeacherEmail()));
        email.setTo(toList);
        email.setType(CommunicationType.SYSTEM_INFO);
        EmailAttachment attachment = new EmailAttachment();
        attachment.setApplication("application/csv");
        attachment.setAttachmentData(data);

        attachment.setFileName(webinar.getTitle() + ":" + timestr + ".csv");
        email.setAttachment(attachment);
        email.setCc(ccList);
        sendEmailViaRest(email);
    }

    public void sendMailToNotifyPageDeleted(String url) throws UnsupportedEncodingException, VException {
        logger.info("Sending mail to notify PageDeleted");
        CommunicationType emailType = CommunicationType.LMS_SEO_PAGE_DELETED;
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("url", url);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        String emailSeo = ConfigUtils.INSTANCE.getStringValue("email.seo");

        toList.add(new InternetAddress(emailSeo, "Vedantu SEO"));

        EmailRequest request = new EmailRequest(toList, null, bodyScopes, emailType, Role.ADMIN);
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    private String getISLCategoryString(UserDetailsInfo userDetailsInfo) {
        String category = null;
        if (null != userDetailsInfo.getCategory()) {
            switch (userDetailsInfo.getCategory()) {
                case "ISL_2017_CAT_3_5":
                    category = "In Category 1 : Grades 3-5";
                    break;
                case "ISL_2017_CAT_6_8":
                    category = "In Category 2 : Grades 6-8";
                    break;
                case "ISL_2017_CAT_9_10":
                    category = "In Category 3 : Grades 9-10";
                    break;
                default:
                    break;
            }
        }
        return category;
    }

    public void sendRegistrationEmailForReviseIndia(User user) throws UnsupportedEncodingException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if(StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if(StringUtils.isNotEmpty(user.getTempContactNumber())){
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_REVISEINDIA;

//        // NEED TO MAKE CHANGES HERE
//        if (newInfo != null) {
//            bodyScopes.put("studentTarget", newInfo.getExam());
//            if (null != newInfo.getGrade()) {
//                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
//            }
//        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmailViaRest(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_REVISEINDIA) {
            CommunicationType communicationType = CommunicationType.REGISTRATION_SMS_REVISEINDIA;
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
            smsManager.sendSMS(textSMSRequest);
        }
    }

    public void sendRegistrationEmailForReviseJee(User user) throws UnsupportedEncodingException, VException {
        logger.info("sending welcome email to : " + user.getEmail());
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", user._getFullName());
        bodyScopes.put("studentEmail", user.getEmail());
        if (user.getStudentInfo() != null) {
            bodyScopes.put("parentName", user.getStudentInfo().getParentFirstName());
        }
        if(StringUtils.isNotEmpty(user.getContactNumber())) {
            bodyScopes.put("contactNumber", user.getContactNumber());
        } else if(StringUtils.isNotEmpty(user.getTempContactNumber())){
            bodyScopes.put("contactNumber", user.getTempContactNumber());
        } else {
            bodyScopes.put("contactNumber", user.getContactNumber());
        }

        String loginUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
        bodyScopes.put("loginUrl", loginUrl);

        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("url.base"));

        CommunicationType emailType = CommunicationType.REGISTRATION_EMAIL_REVISEJEE;

//        // NEED TO MAKE CHANGES HERE
//        if (newInfo != null) {
//            bodyScopes.put("studentTarget", newInfo.getExam());
//            if (null != newInfo.getGrade()) {
//                bodyScopes.put("studentGrade", newInfo.getGrade().toString());
//            }
//        }

        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("name", user.getFirstName());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user._getFullName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, user.getRole());

        request.setIncludeHeaderFooter(Boolean.FALSE);
        request.setEmailService(EmailService.MANDRILL);
        sendEmail(request);
        if (emailType == CommunicationType.REGISTRATION_EMAIL_REVISEJEE) {
            CommunicationType communicationType = CommunicationType.REGISTRATION_SMS_REVISEJEE;
            TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(),
                    user.getPhoneCode(), bodyScopes, communicationType, user.getRole());
            awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(textSMSRequest));
        }
    }

    public void sendResultEmailForReviseJee(Map<String,Object> payload) throws UnsupportedEncodingException, VException {
        logger.info("sending jee result email to : " + payload.get("userId"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(Long.parseLong(payload.get("userId").toString()), "REVISE_JEE_2020_MARCH");

        CommunicationType emailType = CommunicationType.TEST_RESULT_EMAIL_REVISEJEE;

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", newInfo.getStudentName());
        bodyScopes.put("predictedRankRange", payload.get("predictedRankRange"));
        bodyScopes.put("physicsScore", payload.get("physicsScore"));
        bodyScopes.put("physicsC", payload.get("physicsC"));
        bodyScopes.put("physicsW", payload.get("physicsW"));
        bodyScopes.put("physicsU", payload.get("physicsU"));
        bodyScopes.put("chemistryScore", payload.get("chemistryScore"));
        bodyScopes.put("chemistryC", payload.get("chemistryC"));
        bodyScopes.put("chemistryW", payload.get("chemistryW"));
        bodyScopes.put("chemistryU", payload.get("chemistryU"));
        bodyScopes.put("mathsScore", payload.get("mathsScore"));
        bodyScopes.put("mathsC", payload.get("mathsC"));
        bodyScopes.put("mathsW", payload.get("mathsW"));
        bodyScopes.put("mathsU", payload.get("mathsU"));

        HashMap<String, Object> subjectScopes = new HashMap<>();

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(newInfo.getStudentEmail(), newInfo.getStudentName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        sendEmail(request);
    }

    public void sendResultEmailForReviseTargetJeeNeet(Map<String,Object> payload) throws UnsupportedEncodingException, VException {
        logger.info("sending jee result email to : " + payload.get("userId"));

        UserDetailsInfo newInfo = userManager.getUserDetailsForISLInfo(Long.parseLong(payload.get("userId").toString()), "REVISE_JEE_2020_MARCH");

        CommunicationType emailType = CommunicationType.TEST_RESULT_EMAIL_REVISE_TARGET_JEE_NEET;

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", newInfo.getStudentName());
        bodyScopes.put("predictedRankRange", payload.get("predictedRankRange"));
        bodyScopes.put("physicsScore", payload.get("physicsScore"));
        bodyScopes.put("physicsC", payload.get("physicsC"));
        bodyScopes.put("physicsW", payload.get("physicsW"));
        bodyScopes.put("physicsU", payload.get("physicsU"));
        bodyScopes.put("chemistryScore", payload.get("chemistryScore"));
        bodyScopes.put("chemistryC", payload.get("chemistryC"));
        bodyScopes.put("chemistryW", payload.get("chemistryW"));
        bodyScopes.put("chemistryU", payload.get("chemistryU"));
        bodyScopes.put("mathsScore", payload.get("mathsScore"));
        bodyScopes.put("mathsC", payload.get("mathsC"));
        bodyScopes.put("mathsW", payload.get("mathsW"));
        bodyScopes.put("mathsU", payload.get("mathsU"));

        HashMap<String, Object> subjectScopes = new HashMap<>();

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(newInfo.getStudentEmail(), newInfo.getStudentName()));
        EmailRequest request = new EmailRequest(toList, subjectScopes, bodyScopes, emailType, Role.STUDENT);

        //sendEmail(request);
    }


}
