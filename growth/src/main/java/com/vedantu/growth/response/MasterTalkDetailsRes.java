package com.vedantu.growth.response;

import com.vedantu.growth.pojo.mastertalk.MasterTalkAboutTalk;
import com.vedantu.growth.pojo.mastertalk.MasterTalkAskQuestions;
import com.vedantu.growth.pojo.mastertalk.MasterTalkCelebrity;
import com.vedantu.growth.pojo.mastertalk.MasterTalkCommunication;
import com.vedantu.growth.pojo.mastertalk.MasterTalkCoupon;
import com.vedantu.growth.pojo.mastertalk.MasterTalkKeyValueObject;
import com.vedantu.growth.pojo.mastertalk.MasterTalkOurSpeaker;
import com.vedantu.growth.pojo.mastertalk.MasterTalkSeoContent;
import com.vedantu.growth.pojo.mastertalk.MasterTalkSuperFanContent;
import com.vedantu.growth.pojo.mastertalk.MasterTalkSuperFanQuestion;
import com.vedantu.growth.pojo.mastertalk.MasterTalkTile;
import com.vedantu.growth.pojo.mastertalk.MasterTalkTracking;
import com.vedantu.growth.pojo.mastertalk.MastertalkSessionLinks;
import com.vedantu.growth.pojo.mastertalk.PastMasterTalksMetaData;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import lombok.Data;

import java.util.List;

@Data
public class MasterTalkDetailsRes{

    private String id;
    private String pageURL;
    private MasterTalkSeoContent seoContent;
    private MasterTalkCommunication communication;
    private MasterTalkTracking tracking;
    private Long stateChangeTime;
    private MasterTalkTile talkTile;
    private Integer counterLimit;
    private MasterTalkCelebrity celebrity;
    private MasterTalkAboutTalk aboutTalk;
    private String ourSpeakerTitle;
    private MasterTalkCoupon coupon;
    private String eventFlag;
    private Long launchDate;
    private Long releaseDate;
    private Long talkStartTime;
    private Long talkEndTime;
    private Long preToLiveTime;
    private Long liveToPostTime;
    private List<MasterTalkOurSpeaker> ourSpeaker;
    private List<MasterTalkAskQuestions> askQuestions;
    private List<MasterTalkKeyValueObject> faq;
    private String pastMasterTalkHeading;
    private String pastMasterTalkUtm;
    private String masterClassUtm;
    private MastertalkSessionLinks sessionLinks;
    private String askQuestionsImage;
    private String postTalkSubhead;
    private List<PastMasterTalksMetaData> pastMasterTalks;
    private String liveDateMonth;
    private String liveTime;
    private Long creationTime;
    private String createdBy;
    private Long lastUpdated;
    private String lastUpdatedBy;
    private EntityState entityState;
    private MasterTalkSuperFanContent superFanContent;
    private List<MasterTalkSuperFanQuestion> superFanQuestions;

}
