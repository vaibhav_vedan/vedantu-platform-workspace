package com.vedantu.growth.response.cms;

import com.vedantu.util.fos.response.AbstractRes;

public class WebinarCallbackRes extends AbstractRes {

    private Long callbackTime;
    private String inboundNumber;

    public Long getCallbackTime() {
        return callbackTime;
    }

    public void setCallbackTime(Long callbackTime) {
        this.callbackTime = callbackTime;
    }

    public String getInboundNumber() {
        return inboundNumber;
    }

    public void setInboundNumber(String inboundNumber) {
        this.inboundNumber = inboundNumber;
    }
}
