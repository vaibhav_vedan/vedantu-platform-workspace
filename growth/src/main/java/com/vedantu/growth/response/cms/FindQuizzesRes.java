/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.response.cms;

import com.vedantu.growth.entities.Webinar;
import com.vedantu.util.fos.response.AbstractRes;

import java.util.List;

/**
 *
 * @author ajith
 */
public class FindQuizzesRes extends AbstractRes {

    private List<Webinar> pastWebinars;
    private List<Webinar> liveOrUpcomingWebinars;

    public List<Webinar> getLiveOrUpcomingWebinars() {
        return liveOrUpcomingWebinars;
    }

    public void setLiveOrUpcomingWebinars(List<Webinar> liveOrUpcomingWebinars) {
        this.liveOrUpcomingWebinars = liveOrUpcomingWebinars;
    }

    public List<Webinar> getPastWebinars() {
        return pastWebinars;
    }

    public void setPastWebinars(List<Webinar> pastWebinars) {
        this.pastWebinars = pastWebinars;
    }


}
