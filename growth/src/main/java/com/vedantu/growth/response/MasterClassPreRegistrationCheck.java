package com.vedantu.growth.response;

import lombok.Data;

@Data
public class MasterClassPreRegistrationCheck {

    private Boolean isRegistered;
    private Boolean isTestAttempted;
    private Boolean isFeedbackGiven;

}
