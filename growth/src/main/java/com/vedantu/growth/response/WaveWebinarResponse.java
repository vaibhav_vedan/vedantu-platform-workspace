package com.vedantu.growth.response;

import lombok.Data;
import lombok.ToString;

@ToString
@Data
public class WaveWebinarResponse {

    private WaveWebinarResponsePojo result;
    private String errorCode;
    private String errorMessage;

}