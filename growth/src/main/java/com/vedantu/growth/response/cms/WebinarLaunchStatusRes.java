package com.vedantu.growth.response.cms;

import com.vedantu.growth.entities.WebinarSpotInstanceMetadata;
import lombok.Data;

@Data
public class WebinarLaunchStatusRes {

    private WebinarSpotInstanceMetadata instanceMetadata;
    private String latestScreenshotUrl;
}
