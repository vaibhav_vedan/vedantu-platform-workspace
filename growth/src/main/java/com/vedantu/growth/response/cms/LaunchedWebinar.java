package com.vedantu.growth.response.cms;

import com.vedantu.growth.enums.WebinarSpotInstanceStatus;
import lombok.Data;

@Data
public class LaunchedWebinar {

    private String id;
    private String webinarCode;
    private String title;
    private Long startTime;
    private Long endTime;
    private WebinarSpotInstanceStatus instanceStatus;

}
