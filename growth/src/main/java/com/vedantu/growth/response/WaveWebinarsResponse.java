package com.vedantu.growth.response;

import lombok.ToString;

import java.util.List;

@ToString
public class WaveWebinarsResponse {

    private List<WaveWebinarResponsePojo> result;
    private String errorCode;
    private String errorMessage;

    public List<WaveWebinarResponsePojo> getResult() {
        return result;
    }

    public void setResult(List<WaveWebinarResponsePojo> result) {
        this.result = result;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}