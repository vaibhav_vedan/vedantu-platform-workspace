package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkCelebrity {
    private String name;
    private String nickName;
    private String image;
    private String imageMobile;
}
