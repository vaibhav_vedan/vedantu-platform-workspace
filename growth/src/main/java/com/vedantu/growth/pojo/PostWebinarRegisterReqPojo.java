package com.vedantu.growth.pojo;

import com.vedantu.growth.entities.Webinar;
import com.vedantu.growth.pojo.cms.WebinarUserRegistrationInfo;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PostWebinarRegisterReqPojo {
    private WebinarUserRegistrationInfo userWebinarRegistration;
    private Webinar webinar;
}
