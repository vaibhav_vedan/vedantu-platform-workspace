package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkAskQuestions {
    private String question;
    private String subText;
    private String time;
}
