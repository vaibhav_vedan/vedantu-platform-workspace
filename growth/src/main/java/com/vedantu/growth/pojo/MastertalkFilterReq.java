package com.vedantu.growth.pojo;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class MastertalkFilterReq{
    private Integer start = 0;
    private Integer size = 10;
    private Long talkStartTime;
    private Long talkEndTime;
}
