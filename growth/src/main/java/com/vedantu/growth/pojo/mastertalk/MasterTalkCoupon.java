package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

import java.util.List;

@Data
public class MasterTalkCoupon {
    private String code;
    private String link;
    private Long expiryDate;
    private String preText;
    private String postText;
    private String scratchedText;
    private String preSubtext;
    private String postSubtext;
    private String scratchedSubtext;
    private List<String> tc;
    private String utmParams;
    private String couponText;
}
