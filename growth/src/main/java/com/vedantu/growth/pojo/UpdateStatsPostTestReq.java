package com.vedantu.growth.pojo;

import com.vedantu.User.enums.EventName;
import com.vedantu.util.fos.request.AbstractReq;
import lombok.Data;

import java.util.List;

@Data
public class UpdateStatsPostTestReq extends AbstractReq {

    private String type;
    private String secretKey;
    private EventName eventName;
    private String userId;
    private String testId;
    private String testAttemptId;
    private String performanceCode;
    private List<String> areasToWorkOn;
    private TestAnalytics analytics;
    private String s3Link;
    private String sessionId;

}
