/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.pojo.cms;

import java.util.List;

/**
 *
 * @author jeet
 */
public class GTWSessionAttendeeInfo {

    private String firstName;
    private String lastName;
    private List<String> sessionIds;
    private Integer attendanceTimeInSeconds;
    private String registrantKey;
    private String email;
    private List<WebinarUserAttendenceInterval> attendance;

    public GTWSessionAttendeeInfo() {
        super();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<String> getSessionIds() {
        return sessionIds;
    }

    public void setSessionIds(List<String> sessionIds) {
        this.sessionIds = sessionIds;
    }

    public Integer getAttendanceTimeInSeconds() {
        return attendanceTimeInSeconds;
    }

    public void setAttendanceTimeInSeconds(Integer attendanceTimeInSeconds) {
        this.attendanceTimeInSeconds = attendanceTimeInSeconds;
    }

    public String getRegistrantKey() {
        return registrantKey;
    }

    public void setRegistrantKey(String registrantKey) {
        this.registrantKey = registrantKey;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<WebinarUserAttendenceInterval> getAttendance() {
        return attendance;
    }

    public void setAttendance(List<WebinarUserAttendenceInterval> attendance) {
        this.attendance = attendance;
    }

    @Override
    public String toString() {
        return "GTWSessionAttendeeInfo [firstName=" + firstName + ", lastName=" + lastName + ", sessionIds="
                + sessionIds + ", attendanceTimeInSeconds=" + attendanceTimeInSeconds + ", registrantKey="
                + registrantKey + ", email=" + email + ", attendance=" + attendance + ", toString()=" + super.toString()
                + "]";
    }
}
