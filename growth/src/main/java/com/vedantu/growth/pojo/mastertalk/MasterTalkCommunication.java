package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkCommunication {
    private String shareText;
    private String registrationSmsContent;
}
