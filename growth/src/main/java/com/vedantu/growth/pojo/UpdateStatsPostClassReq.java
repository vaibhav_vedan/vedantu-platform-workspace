package com.vedantu.growth.pojo;

import com.vedantu.util.fos.request.AbstractReq;

public class UpdateStatsPostClassReq extends AbstractReq {

    private Integer doubts;
    private String userId;

    public Integer getDoubts() {
        return doubts;
    }

    public void setDoubts(Integer doubts) {
        this.doubts = doubts;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
