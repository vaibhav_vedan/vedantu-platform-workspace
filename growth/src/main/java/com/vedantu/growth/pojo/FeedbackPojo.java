package com.vedantu.growth.pojo;

import lombok.Data;

@Data
public class FeedbackPojo {

    private Integer rating;
    private String feedback;
}
