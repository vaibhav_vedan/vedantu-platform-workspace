package com.vedantu.growth.pojo.cms;

import lombok.Data;

@Data
public class WebinarSessionReplayAvailabilityPojo {
    private String id;
    private Boolean replayCreated = Boolean.TRUE;
}
