package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkTile {
    private String imgLink;
    private String heading;
    private String subheading;
}
