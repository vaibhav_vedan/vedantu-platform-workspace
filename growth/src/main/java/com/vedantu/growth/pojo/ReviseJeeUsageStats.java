package com.vedantu.growth.pojo;

import lombok.Data;

@Data
public class ReviseJeeUsageStats {
    private Integer tests = 0;
    private Integer sessionsAttended = 0;
}
