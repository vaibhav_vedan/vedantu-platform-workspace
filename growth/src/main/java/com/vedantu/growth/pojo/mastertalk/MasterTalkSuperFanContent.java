package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkSuperFanContent {
    private String subHeading;
    private String imageLink;
    private String successText;
}
