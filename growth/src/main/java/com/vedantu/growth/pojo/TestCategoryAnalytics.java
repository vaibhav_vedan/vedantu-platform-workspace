package com.vedantu.growth.pojo;

import lombok.Data;

@Data
public class TestCategoryAnalytics {
    private String name;
    private AnalyticsInfo info;
}
