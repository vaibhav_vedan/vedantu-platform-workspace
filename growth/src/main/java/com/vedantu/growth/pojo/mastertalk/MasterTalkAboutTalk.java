package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkAboutTalk {
    private String title;
    private String sutitle;
    private String description;
}
