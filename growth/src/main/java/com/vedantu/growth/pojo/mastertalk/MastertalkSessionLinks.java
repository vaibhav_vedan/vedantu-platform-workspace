package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MastertalkSessionLinks {
    private String thumbnail;
    private String stagingSessionUrlLive;
    private String sessionImage;
    private String prodSessionUrlLive;
    private String stagingSessionUrlPost;
    private String prodSessionUrlPost;
}
