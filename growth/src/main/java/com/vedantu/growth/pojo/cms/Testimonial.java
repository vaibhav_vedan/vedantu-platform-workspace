package com.vedantu.growth.pojo.cms;

import java.util.List;

public class Testimonial {

	private String sectionTitle;
	private List<TestimonialPOJO> testimonials;
	
	
	public String getSectionTitle() {
		return sectionTitle;
	}
	
	public void setSectionTitle(String sectionTitle) {
		this.sectionTitle = sectionTitle;
	}
	
	public List<TestimonialPOJO> getTestimonials() {
		return testimonials;
	}
	
	public void setTestimonials(List<TestimonialPOJO> testimonials) {
		this.testimonials = testimonials;
	}
	
	
}
