package com.vedantu.growth.pojo.cms;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.growth.enums.WebinarNotifcation;
import com.vedantu.growth.enums.cms.WebinarUserRegistrationStatus;
import com.vedantu.growth.enums.cms.WebinarUserTag;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.RequestSource;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Document(collection = "WebinarUserRegistrationInfo")
@CompoundIndexes({
        @CompoundIndex(name = "userId_1_webinarId_1", def = "{'userId' : 1, 'webinarId': 1}", background = true),
        @CompoundIndex(name = "lastUpdated_1__id_1", def = "{'lastUpdated': 1, '_id': 1}", background = true),
        @CompoundIndex(name = "userId_1_endTime_1", def = "{'userId' : 1, 'endTime': 1}", background = true)
})
public class WebinarUserRegistrationInfo extends AbstractMongoStringIdEntity {

    private String firstName;
    private String lastName;

    @Indexed(background = true)
    private String userId;
    private String utm_source;
    private String utm_campaign;
    private String utm_medium;
    private String utm_term;
    private String utm_content;
    private String grade;
    private RequestSource source;

    @Indexed(background = true)
    private String trainingId;
    private String timeZone = "Asia/Kolkata";
    private String registerJoinUrl;
    private String registerJoinUrlShort;
    private String registerRegistrantKey;
    private String registerStatus;
    private String registerEmail;
    private List<String> responses;
    private Boolean otpVerificationRequired = false;
    private WebinarUserRegistrationStatus status;

    @Indexed(background = true)
    private String webinarId;

    private String clevarTapId;

    private Integer timeInSession;
    private boolean webinarAttended = false;
    private boolean replayWatched = false;
    private List<WebinarUserAttendenceInterval> attendanceIntervals;

    private List<WebinarUserTag> tags = new ArrayList<>();

    private List<WebinarRegistrationQuestion> questions = new ArrayList<>();

    private Set<WebinarNotifcation> notifcations;
    private Long startTime;
    private Long endTime;

    public WebinarUserRegistrationInfo() {
        super();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUtm_source() {
        return utm_source;
    }

    public void setUtm_source(String utm_source) {
        this.utm_source = utm_source;
    }

    public String getUtm_campaign() {
        return utm_campaign;
    }

    public void setUtm_campaign(String utm_campaign) {
        this.utm_campaign = utm_campaign;
    }

    public String getUtm_medium() {
        return utm_medium;
    }

    public void setUtm_medium(String utm_medium) {
        this.utm_medium = utm_medium;
    }

    public String getTrainingId() {
        return trainingId;
    }

    public void setTrainingId(String trainingId) {
        this.trainingId = trainingId;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getRegisterJoinUrl() {
        return registerJoinUrl;
    }

    public void setRegisterJoinUrl(String registerJoinUrl) {
        this.registerJoinUrl = registerJoinUrl;
    }

    public String getRegisterRegistrantKey() {
        return registerRegistrantKey;
    }

    public void setRegisterRegistrantKey(String registerRegistrantKey) {
        this.registerRegistrantKey = registerRegistrantKey;
    }

    public String getRegisterStatus() {
        return registerStatus;
    }

    public void setRegisterStatus(String registerStatus) {
        this.registerStatus = registerStatus;
    }

    public List<String> getResponses() {
        return responses;
    }

    public void setResponses(List<String> responses) {
        this.responses = responses;
    }

    public void updateUserBasicInfo(UserBasicInfo userBasicInfo) {
        if (userBasicInfo != null) {
            this.firstName = userBasicInfo.getFirstName();
            this.lastName = userBasicInfo.getLastName();
            this.userId = String.valueOf(userBasicInfo.getUserId());
            if(StringUtils.isEmpty(this.grade)){
                this.grade= userBasicInfo.getGrade();
            }
        }
    }

    public Boolean getOtpVerificationRequired() {
        return otpVerificationRequired;
    }

    public void setOtpVerificationRequired(Boolean otpVerificationRequired) {
        this.otpVerificationRequired = otpVerificationRequired;
    }

    public WebinarUserRegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(WebinarUserRegistrationStatus status) {
        this.status = status;
    }

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public List<WebinarUserAttendenceInterval> getAttendanceIntervals() {
        return attendanceIntervals;
    }

    public void setAttendanceIntervals(List<WebinarUserAttendenceInterval> attendanceIntervals) {
        this.attendanceIntervals = attendanceIntervals;
    }

    public void addTag(WebinarUserTag webinarUserTag) {
        if (webinarUserTag != null) {
            if (this.tags == null) {
                this.tags = new ArrayList<>();
            }
            this.tags.add(webinarUserTag);
        }
    }

    public boolean containsTag(WebinarUserTag webinarUserTag) {
        return !CollectionUtils.isEmpty(this.tags) && this.tags.contains(webinarUserTag);
    }

    public String getRegisterJoinUrlShort() {
        return registerJoinUrlShort;
    }

    public void setRegisterJoinUrlShort(String registerJoinUrlShort) {
        this.registerJoinUrlShort = registerJoinUrlShort;
    }

    public void updateRegisterResponse(JSONObject jsonObject) throws JSONException {
        /*
		    {
		        "registrantKey": "7637606173704895757",
		        "joinUrl": "https://global.gotowebinar.com/join/150586194716221441/785351865",
		        "status": "APPROVED"
		    }
         */
        if (jsonObject != null) {
            if (jsonObject.has("registrantKey")) {
                this.registerRegistrantKey = jsonObject.getString("registrantKey");
            }
            if (jsonObject.has("joinUrl")) {
                this.registerJoinUrl = jsonObject.getString("joinUrl") + "?clientType=html5";
            }
            if (jsonObject.has("status")) {
                this.registerStatus = jsonObject.getString("status");
            }
        }
    }

    public void updateAttendance(List<GTWSessionAttendeeInfo> attendanceInfos) {
        int timeInsession = 0;
        List<WebinarUserAttendenceInterval> attendanceIntervals = new ArrayList<>();
        if (!CollectionUtils.isEmpty(attendanceInfos)) {
            for (GTWSessionAttendeeInfo entry : attendanceInfos) {
                if (entry.getAttendanceTimeInSeconds() != null) {
                    timeInsession += entry.getAttendanceTimeInSeconds();
                }
                if (!CollectionUtils.isEmpty(entry.getAttendance())) {
                    attendanceIntervals.addAll(entry.getAttendance());
                }
            }
            this.webinarAttended = true;
            this.timeInSession = timeInsession;
            this.attendanceIntervals = attendanceIntervals;
        }
    }

    public Integer getTimeInSession() {
        return timeInSession;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public List<WebinarUserTag> getTags() {
        return tags;
    }

    public void setTags(List<WebinarUserTag> tags) {
        this.tags = tags;
    }

    public void setTimeInSession(Integer timeInSession) {
        this.timeInSession = timeInSession;
    }

    public String getClevarTapId() {
        return clevarTapId;
    }

    public void setClevarTapId(String clevarTapId) {
        this.clevarTapId = clevarTapId;
    }

    public void updateRegisterResponse(WebinarUserRegistrationInfo webinarUserRegistrationInfo) throws JSONException {
        this.registerRegistrantKey = webinarUserRegistrationInfo.getRegisterRegistrantKey();
        this.registerJoinUrl = webinarUserRegistrationInfo.getRegisterJoinUrl();
        this.registerStatus = webinarUserRegistrationInfo.getRegisterStatus();
    }

    public String getRegisterEmail() {
        return registerEmail;
    }

    public void setRegisterEmail(String registerEmail) {
        this.registerEmail = registerEmail;
    }

    public List<WebinarRegistrationQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(List<WebinarRegistrationQuestion> questions) {
        this.questions = questions;
    }

    public void addQuestion(WebinarRegistrationQuestion question) {
        if (this.questions == null) {
            this.questions = new ArrayList<>();
        }
        this.questions.add(question);
    }

    public String getUtm_term() {
        return utm_term;
    }

    public void setUtm_term(String utm_term) {
        this.utm_term = utm_term;
    }

    public String getUtm_content() {
        return utm_content;
    }

    public void setUtm_content(String utm_content) {
        this.utm_content = utm_content;
    }

    public boolean isWebinarAttended() {
        return webinarAttended;
    }

    public void setWebinarAttended(boolean webinarAttended) {
        this.webinarAttended = webinarAttended;
    }

    public boolean isReplayWatched() {
        return replayWatched;
    }

    public void setReplayWatched(boolean replayWatched) {
        this.replayWatched = replayWatched;
    }

    public RequestSource getSource() {
		return source;
	}

	public void setSource(RequestSource source) {
		this.source = source;
	}

    public Set<WebinarNotifcation> getNotifcations() {
        this.notifcations = this.notifcations == null ? new LinkedHashSet<>() : this.notifcations;
        return Collections.unmodifiableSet(notifcations);
    }

    public void addNotifcations(WebinarNotifcation notifcation) {
        this.notifcations = this.notifcations == null ? new LinkedHashSet<>() : this.notifcations;
        this.notifcations.add(notifcation);
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String WEBINARID = "webinarId";
        public static final String TRAININGID = "trainingId";
        public static final String PHONE = "phone";
        public static final String EMAIL = "emailId";
        public static final String QUESTIONS = "questions";
        public static final String STATUS = "status";
        public static final String OTP_VERIFICATION_REQUIRED = "otpVerificationRequired";
        public static final String USER_ID = "userId";
        public static final String NOTIFICATION = "notifcations";
        public static final String WEBINAR_ATTENDED = "webinarAttended";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
    }
}
	