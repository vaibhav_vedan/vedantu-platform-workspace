/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.pojo.cms;

import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.util.CollectionUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.TimeZone;

/**
 *
 * @author jeet
 */
@Document(collection = "WebinarInfo")
public class WebinarInfo extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String webinarId;
    private String webinarSubId;
    private String accountKey;
    private String organizerKey;
    private String accountEmailId;
    private String registrationUrl;

    private String name;
    private String subject;
    private String description;

    private long startTime;
    private long endTime;
    private String timeZone;

    public WebinarInfo() {
        super();
    }

    public WebinarInfo(WebinarInfoRes entry, String accountEmailId) throws ParseException {
        super();
        this.webinarId = entry.getWebinarKey();
        this.webinarSubId = entry.getWebinarId();
        this.accountKey = entry.getAccountKey();
        this.organizerKey = entry.getOrganizerKey();
        this.accountEmailId = accountEmailId;
        this.registrationUrl = entry.getRegistrationUrl();

        this.name = entry.getName();
        this.subject = entry.getSubject();
        this.description = entry.getDescription();

        // Update timings
        this.timeZone = entry.getTimeZone();
        if (!CollectionUtils.isEmpty(entry.getTimes())) {
            GTWInterval gttInterval = entry.getTimes().get(0);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
            this.startTime = sdf.parse(gttInterval.getStartTime()).getTime();
            this.endTime = sdf.parse(gttInterval.getEndTime()).getTime();
        } else {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
            sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
            this.startTime = sdf.parse(entry.getStartTime()).getTime();
            this.endTime = sdf.parse(entry.getEndTime()).getTime();
        }
        
    }

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }

    public String getAccountEmailId() {
        return accountEmailId;
    }

    public void setAccountEmailId(String accountEmailId) {
        this.accountEmailId = accountEmailId;
    }

    public String getWebinarSubId() {
        return webinarSubId;
    }

    public void setWebinarSubId(String webinarSubId) {
        this.webinarSubId = webinarSubId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getOrganizerKey() {
        return organizerKey;
    }

    public void setOrganizerKey(String organizerKey) {
        this.organizerKey = organizerKey;
    }

    public String getAccountKey() {
        return accountKey;
    }

    public void setAccountKey(String accountKey) {
        this.accountKey = accountKey;
    }

    public String getRegistrationUrl() {
        return registrationUrl;
    }

    public void setRegistrationUrl(String registrationUrl) {
        this.registrationUrl = registrationUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String WEBINAR_ID = "webinarId";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
    }
}
