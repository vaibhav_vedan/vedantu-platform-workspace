package com.vedantu.growth.pojo.cms;

import lombok.ToString;

import java.util.List;

@ToString
public class WebinarLeaderBoardData {
    private String _id;
    private String studentName;
    private double rank;
    private String sessionId;
    private String userId;
    private long points;
    private long sessionRank;
    private List<String> quizIds;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public double getRank() {
        return rank;
    }

    public void setRank(double rank) {
        this.rank = rank;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public long getSessionRank() {
        return sessionRank;
    }

    public void setSessionRank(long sessionRank) {
        this.sessionRank = sessionRank;
    }

    public List<String> getQuizIds() {
        return quizIds;
    }

    public void setQuizIds(List<String> quizIds) {
        this.quizIds = quizIds;
    }

}
