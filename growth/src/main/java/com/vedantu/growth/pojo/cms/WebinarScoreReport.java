package com.vedantu.growth.pojo.cms;

import lombok.ToString;

@ToString
public class WebinarScoreReport {
    private int min;
    private int max;
    private long count;

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }
}
