package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkSuperFanQuestion {
    private String question;
    private String option1;
    private String option2;
    private String option3;
}
