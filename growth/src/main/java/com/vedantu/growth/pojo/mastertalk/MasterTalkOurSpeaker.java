package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkOurSpeaker {

    private String title;
    private String subTitle;
    private String imageUrl;

}
