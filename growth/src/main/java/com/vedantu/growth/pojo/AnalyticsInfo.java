package com.vedantu.growth.pojo;

import lombok.Data;

@Data
public class AnalyticsInfo {
    private Integer marks = 0;
    private Integer attempted = 0;
    private Integer unattempted = 0;
    private Integer cutOff;
    private Integer correct = 0;
    private Integer incorrect = 0;
    private Integer maxMarks = 0;
    private Long duration = 0L;
    private Long averageDuration = 0L;
    private Integer averageMarks = 0;
    private Integer rank;
}
