package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkKeyValueObject {
    private String question;
    private String answer;
    private String note;
}
