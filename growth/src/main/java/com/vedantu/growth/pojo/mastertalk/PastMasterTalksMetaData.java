package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class PastMasterTalksMetaData {

    private String title;
    private String subText;
    private String tileImg;
    private String talkLink;
}
