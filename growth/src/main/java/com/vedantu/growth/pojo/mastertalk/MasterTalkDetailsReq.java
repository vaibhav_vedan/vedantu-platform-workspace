package com.vedantu.growth.pojo.mastertalk;

import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

@Data
public class MasterTalkDetailsReq extends AbstractFrontEndReq {

    private String id;
    private String pageURL;
    private MasterTalkSeoContent seoContent;
    private MasterTalkCommunication communication;
    private MasterTalkTracking tracking;
    private Long stateChangeTime;
    private MasterTalkTile talkTile;
    private Integer counterLimit;
    private MasterTalkCelebrity celebrity;
    private MasterTalkAboutTalk aboutTalk;
    private String ourSpeakerTitle;
    private MasterTalkCoupon coupon;
    private String eventFlag;
    private Long launchDate;
    private Long releaseDate;
    private Long talkStartTime;
    private Long talkEndTime;
    private Long preToLiveTime;
    private Long liveToPostTime;
    private List<MasterTalkOurSpeaker> ourSpeaker;
    private List<MasterTalkAskQuestions> askQuestions;
    private List<MasterTalkKeyValueObject> faq;
    private String pastMasterTalkHeading;
    private String pastMasterTalkUtm;
    private String masterClassUtm;
    private MastertalkSessionLinks sessionLinks;
    private String askQuestionsImage;
    private String postTalkSubhead;
    private String liveDateMonth;
    private String liveTime;
    private EntityState entityState;
    private MasterTalkSuperFanContent superFanContent;
    private List<MasterTalkSuperFanQuestion> superFanQuestions;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(pageURL)) {
            errors.add("pageURL");
        }
        if (launchDate == null) {
            errors.add("launchDate");
        }
        if (talkStartTime == null) {
            errors.add("talkStartTime");
        }
        if (talkEndTime == null) {
            errors.add("talkEndTime");
        }

        if (talkEndTime == null || StringUtils.isEmpty(communication.getRegistrationSmsContent())) {
            errors.add("communication.registrationSmsContent");
        }

        if (talkEndTime == null || StringUtils.isEmpty(communication.getShareText())) {
            errors.add("communication.shareText");
        }
        return errors;
    }

}
