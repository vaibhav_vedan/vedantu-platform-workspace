package com.vedantu.growth.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class CategoryPageTitleUrlMapping {

    private String url;
    private String title;

}