package com.vedantu.growth.pojo.mastertalk;

import lombok.Data;

@Data
public class MasterTalkSeoContent {
    private String title;
    private String description;
    private String keywords;
    private String canonicalTags;
    private String ogImage;
}
