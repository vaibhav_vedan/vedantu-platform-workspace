/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.controllers;

import com.vedantu.exception.VException;
import com.vedantu.growth.managers.UserManager;
import com.vedantu.growth.requests.SendSeoPdfByEmailReq;
import com.vedantu.growth.requests.SendSeoPdfBySMSReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 *
 * @author somil
 */
@RestController
@RequestMapping("/user")
// @CrossOrigin
public class UserController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserController.class);

    @Autowired
    private UserManager userManager;

    @RequestMapping(value = "/sendSeoPdfByEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendSeoPdfByEmail(@RequestBody SendSeoPdfByEmailReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        return userManager.sendSeoPdfByEmail(req, request, response);
    }

    @RequestMapping(value = "/sendSeoPdfBySMS", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendSeoPdfBySMS(@RequestBody SendSeoPdfBySMSReq req) throws VException, IOException {
        return userManager.sendSeoPdfBySMS(req);
    }

}
