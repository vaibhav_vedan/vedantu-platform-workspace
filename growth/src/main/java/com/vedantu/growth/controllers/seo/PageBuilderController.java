package com.vedantu.growth.controllers.seo;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.growth.async.AsyncTaskName;
import com.vedantu.growth.seo.CategoryPageVoteManager;
import com.vedantu.growth.seo.PageBuilderManager;
import com.vedantu.growth.seo.SeoRssFeedManager;
import com.vedantu.growth.seo.entity.AppendPdfRuleSet;
import com.vedantu.growth.seo.entity.Category;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.enums.MasterSidebarUploadOptions;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.pojo.CategoryPageVoteRequest;
import com.vedantu.growth.seo.response.CategoryPageResponse;
import com.vedantu.growth.seo.response.CategoryPageVoteStatus;
import com.vedantu.platform.seo.dto.CMDSQuestionDto;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@RestController
@Api(value = "SeoManager")
public class PageBuilderController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PageBuilderController.class);

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private SeoRssFeedManager seoRssFeedManager;

    @Autowired
    private CategoryPageVoteManager categoryPageVoteManager;

    @RequestMapping(value = "/sitemaps/{name}", method = RequestMethod.GET, produces = MediaType.APPLICATION_XML_VALUE)
    @ApiOperation(value = "Get all sitemaps", notes = "Sitemaps XML")
    public String getSitemaps(@PathVariable(value = "name") String name,
                              @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws MalformedURLException, ParseException {
        logger.info("SEO-SITEMAPS: " + name);
        String response = pageBuilderManager.getSitemap(name, domain);
        logger.info("SITEMAP: " + response);
        return response;
    }

    @RequestMapping(value = "/seo/category", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get all categories", notes = "List<Category (category entries that are added/updated.)")
    public List<Category> getAllCategories(@RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws Exception {
        return pageBuilderManager.getAllCategories(domain);
    }

    @RequestMapping(value = "/seo/category/{categoryId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get category by Id", notes = "Category (category entries that are added/updated.)")
    public Category getCategory(@PathVariable(value = "categoryId") String categoryId) throws Exception {
        return pageBuilderManager.getCategoryById(categoryId);
    }

    // TODO add field inside object
    @RequestMapping(value = "/seo/category", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Add a new category", notes = "Category (category that is to be added.)")
    public Category upsertCategory(@RequestBody Category category) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        if (StringUtils.isEmpty(category.getName())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "name not found");
        }
        return pageBuilderManager.createCategory(category);
    }

    @RequestMapping(value = "/seo/category/{categoryId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Delete a category", notes = "CategoryPage (category that is to be deleted.)")
    public PlatformBasicResponse deleteCategory(@PathVariable(value = "categoryId") String categoryId)
            throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), false);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            Boolean deleteCategoryStatus = pageBuilderManager.deleteCategory(categoryId);
            platformBasicResponse.setSuccess(deleteCategoryStatus);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    @RequestMapping(value = "/seo/category-page/question/{questionId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get categoryPage by questionId", notes = "questionId (categoryPage entries that are added/updated.)")
    public CategoryPage getCategoryPageByQuestionId(@PathVariable(value = "questionId") String questionId,
                                                    @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) {
        return pageBuilderManager.getCategoryPageByQuestionId(questionId, domain, false);
    }

    @RequestMapping(value = "/seo/category-page", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get categoryPages by categoryId", notes = "CategoryPages (categoryPages entries that are added/updated.)")
    @ResponseBody
    public CategoryPageResponse getCategoryPages(
            @RequestParam(value = "categoryId", required = false) String categoryId,
            @RequestParam(value = "categoryPageUrl", required = false) String categoryPageUrl,
            @RequestParam(value = "categoryPageType", required = false) String categoryPageType,
            @RequestParam(value = "categoryTarget", required = false) String categoryTarget,
            @RequestParam(value = "categoryGrade", required = false) String categoryGrade,
            @RequestParam(value = "disabled", required = false) Boolean disabled,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws Exception {

        CategoryPageResponse entity = new CategoryPageResponse();
        List<CategoryPage> categoryPages = pageBuilderManager.getCategoryPages(domain, categoryId, categoryPageUrl, categoryPageType, categoryTarget, categoryGrade, disabled, start, size);

        pageBuilderManager.checkFor301Redirection(categoryPageUrl, entity, categoryPages, domain);

        entity.setCategoryPages(categoryPages);
        return entity;
    }

    @RequestMapping(value = "/seo/category-page/{categoryPageId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get categoryPage by Id", notes = "CategoryPage (categoryPage entries that are added/updated.)")
    public CategoryPage getCategoryPageById(@PathVariable(value = "categoryPageId") String categoryPageId)
            throws Exception {

        CategoryPage categoryPage = pageBuilderManager.getCategoryPageById(categoryPageId);
        pageBuilderManager.setCategoryPageRelatedLinks(categoryPage);

        return categoryPage;
    }

    @RequestMapping(value = "/seo/category-page-url/{categoryPageId}", method = RequestMethod.GET, produces = "text/plain")
    @ApiOperation(value = "Get categoryPage URL by Id", notes = "CategoryPage.url (categoryPage URL entry that are added/updated.)")
    public String getCategoryPageUrlById(@PathVariable(value = "categoryPageId") String categoryPageId)
            throws Exception {
    	try {
    		return pageBuilderManager.getCategoryPageById(categoryPageId).getUrl();
    	} catch(NullPointerException e) {
    		logger.error("CategoryPage not found");
    		throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, e.getMessage());
    	}
    }

    @RequestMapping(value = "/seo/category-page/get-id-from-url", method = RequestMethod.GET, produces = "text/plain")
    @ApiOperation(value = "Get categoryPage URL by Id", notes = "CategoryPage.url (categoryPage URL entry that are added/updated.)")
    public String getCategoryPageIdByUrl(@RequestParam(value = "categoryPageUrl") String categoryPageUrl,
                                         @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain)
            throws Exception {
        try {
            return pageBuilderManager.getCategoryPageByUrl(categoryPageUrl, domain).getId();
        } catch(NullPointerException e) {
            logger.error("CategoryPage not found");
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, e.getMessage());
        }
    }

    @RequestMapping(value = "/seo/category-page", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Add a new categoryPage", notes = "CategoryPage (category entries that are added/updated.)")
    public CategoryPage upsertCategoryPage(@RequestBody CategoryPage categoryPage) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return pageBuilderManager.createOrUpdateCategoryPage(categoryPage);
    }

    @RequestMapping(value = "/seo/category-page/{categoryPageId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Delete a category", notes = "CategoryPage (category that is to be deleted.)")
    public PlatformBasicResponse deleteCategoryPage(@PathVariable(value = "categoryPageId") String categoryPageId)
            throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN), false);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            Boolean deleteCategoryPageStatus = pageBuilderManager.deleteCategoryPage(categoryPageId);
            platformBasicResponse.setSuccess(deleteCategoryPageStatus);
            if(deleteCategoryPageStatus){
                seoRssFeedManager.deleteRssItem(categoryPageId);
            }
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    @RequestMapping(value = "/seo/category-page/content-file-status", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Get contentFile pdf to image conversion status")
    public PlatformBasicResponse getContentFileProcessingStatus(
            @RequestParam(value = "categoryPageId", required = true) String categoryPageId) throws Exception {

        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        Boolean contentFileProcessingStatus = pageBuilderManager.getContentFileProcessingStatus(categoryPageId);
        platformBasicResponse.setResponse(contentFileProcessingStatus.toString());

        return platformBasicResponse;
    }

    @RequestMapping(value = "/seo/category-page/content", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Get AWS bucket content")
    public List<Map<String, String>> getCategoryPageContent(@RequestParam(value = "prefix", required = true) String prefix,
                                               @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws Exception {

        String bucket = domain.getAwsBucket();
        return pageBuilderManager.getCategoryPageContentUrlMap(bucket, prefix);
    }

    @RequestMapping(value = "/seo/category-page/content", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Upload To AWS", notes = "Boolean uploadToAWS()")
    public PlatformBasicResponse uploadToAWS(
            @RequestParam(value = "categoryPageId", required = true) String categoryPageId,
            @RequestParam(value = "key", required = true) String key,
            @RequestParam(value = "contentFile") MultipartFile contentFile,
            @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            String bucket = domain.getAwsBucket();
            Boolean isUplaodSuccess = pageBuilderManager.uploadContentFile(categoryPageId, bucket, key, contentFile);

            platformBasicResponse.setSuccess(isUplaodSuccess);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    @RequestMapping(value = "/seo/aws/pre-signed-url", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Generate a PreSigned url for AWS operation", notes = "Boolean uploadToAWS()")
    public PlatformBasicResponse getPreSignedUrl(
            @RequestParam(value = "key", required = true) String key,
            @RequestParam(value = "contentType") String contentType,
            @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws Exception {
        return pageBuilderManager._getPresignedUrl(key, contentType, domain);
    }

    @RequestMapping(value = "/seo/aws/content-page/updateUploadStatus", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "updateUploadStatus", notes = "Boolean updateUploadStatus()")
    public PlatformBasicResponse updateContentFileProcessingStatus(
            @RequestParam(value = "contentPageId", required = true) String contentPageId,
            @RequestParam(value = "status", required = true) Boolean contentFileProcessingStatus) throws Exception {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            Boolean statusResponse = pageBuilderManager.updateContentFileProcessingStatus(contentPageId,
                    contentFileProcessingStatus);

            platformBasicResponse.setResponse(statusResponse.toString());
            platformBasicResponse.setSuccess(true);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    @RequestMapping(value = "/seo/addeditappendpdfruleset", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AppendPdfRuleSet addAppendPdfRuleSet(@RequestBody AppendPdfRuleSet req,
                                                @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return pageBuilderManager.addAppendPdfRuleSet(req);
    }

    @RequestMapping(value = "/seo/deleteappendpdfruleset/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse deleteAppendPdfRuleSet(@PathVariable(value = "id") String id,
                                                        @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return pageBuilderManager.deleteappendpdfruleset(id);
    }

    @RequestMapping(value = "/seo/getappendpdfrulesets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AppendPdfRuleSet> getAppendPdfRuleSets(@RequestParam(value = "query", required = false) String query,
                                                       @RequestParam(value = "categoryId", required = false) String categoryId,
                                                       @RequestParam(value = "target", required = false) String target,
                                                       @RequestParam(value = "grade", required = false) String grade,
                                                       @RequestParam(value = "subject", required = false) String subject,
                                                       @RequestParam(value = "pageRelativeUrl", required = false) String pageRelativeUrl,
                                                       @RequestParam(value = "start", required = false) Integer start,
                                                       @RequestParam(value = "size", required = false) Integer size,
                                                       @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return pageBuilderManager.getAppendPdfRuleSets(query, categoryId, target, grade, subject, pageRelativeUrl,
                start, size);
    }

    @RequestMapping(value = "/seo/getAppendPdfUploadUrl", method = RequestMethod.GET)
    public Map<String, Object> getAppendPdfUploadUrl(@RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return pageBuilderManager.getAppendPdfUploadUrl();
    }

    //almost a duplicate api to /seo/aws/pre-signed-url
    @RequestMapping(value = "/seo-manager/storage/pre-signed-key", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse getAwsPreSignedUrl(@RequestParam(value = "categoryPageId") String categoryPageId,
                                                    @RequestParam(value = "contentType") String contentType,
                                                    @RequestParam(value = "downloadableFile") boolean downloadableFile,
                                                    @RequestParam(value = "contentStorageType", required = false) String contentStorageType) throws VException {
        String env = ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase();
        String key = null;
        if (downloadableFile) {
            contentStorageType = "DOWNLOADBLE_FILE";
        }
        if (StringUtils.isEmpty(contentStorageType)) {
            contentStorageType = "DEFAULT";
        }

        CategoryPage categoryPage = pageBuilderManager.getCategoryPageById(categoryPageId);
        String awsImageSavePath = categoryPage.getUrl();
        SeoDomain domain = categoryPage.getDomain() == null ? SeoDomain.VEDANTU : categoryPage.getDomain();
        if (StringUtils.isEmpty(awsImageSavePath) || "/".equals(awsImageSavePath.trim())) {
            throw new VException(ErrorCode.SERVICE_ERROR, "Category Page url is empty");
        }
        if (awsImageSavePath.startsWith("/")) {
            awsImageSavePath = awsImageSavePath.replaceFirst("/", "");
        }

        switch (contentStorageType) {
            case "CUSTOM_IMAGE":
                key = env + "/content-images/" + awsImageSavePath + "/" + UUID.randomUUID();
                break;
            case "BLOG_CUSTOM_IMAGE":
                key = env + "/public/seo/blog-images/" + awsImageSavePath + "/" + UUID.randomUUID();
                break;
            case "DOWNLOADBLE_FILE":
                key = env + "/content-files-downloadable/" + awsImageSavePath + ".pdf";
                break;
            default:
                key = env + "/content-files/" + awsImageSavePath + ".pdf";
                break;
        }

        return pageBuilderManager._getPresignedUrl(key, contentType, domain);
    }

    //almost a duplicate api to /seo/category-page/content
    @RequestMapping(value = "/seo-manager/storage/content", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Map<String, String>> seoManagerStorageContent(@RequestParam(value = "categoryPageId") String categoryPageId) throws VException {
        String env = ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase();
        CategoryPage categoryPage = pageBuilderManager.getCategoryPageById(categoryPageId);
        String bucket = categoryPage.getDomain().getAwsBucket();
        String awsImageSavePath = categoryPage.getUrl();
        if (StringUtils.isEmpty(awsImageSavePath) || "/".equals(awsImageSavePath.trim())) {
            return new ArrayList<>();
        }
        if (awsImageSavePath.startsWith("/")) {
            awsImageSavePath = awsImageSavePath.replaceFirst("/", "");
        }

        awsImageSavePath = awsImageSavePath.endsWith("/") ? awsImageSavePath : awsImageSavePath + "/";
        String prefix = env + "/content-images/" + awsImageSavePath;

        try {
            pageBuilderManager.invalidateCacheDomainVedantu(categoryPage);
        }catch (Exception e){
            logger.error("Error clearing cloudfrontCache : ", e);
        }

        return pageBuilderManager.getCategoryPageContentUrlMap(bucket, prefix);
    }

    @RequestMapping(value = "/generate-related-links", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse generateRelatedLinksFromQuestions(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                                   @RequestBody String request,
                                                                   @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws VException {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if ("SubscriptionConfirmation".equals(messageType)) {
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, null, false);
            logger.info(resp.getEntity(String.class));
        } else if ("Notification".equals(messageType)) {
            Map<String, Object> payload = new HashMap<>();
            payload.putIfAbsent("domain", domain.toString());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.GENERATE_CATEGORY_PAGES_FROM_INDEXABLE_QUESTIONS, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/generate-related-links-limited", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse generateRelatedLinksFromQuestionsLimited(@RequestParam(name = "limit") int limit,
                                                                          @RequestParam(name = "page") int page,
                                                                          @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        int pages = pageBuilderManager.limitAndGenerateCategoryPagesFromQuestions(domain, limit, page);
        return new PlatformBasicResponse(true, "Page generated count: " + pages, "");
    }

    @RequestMapping(value = "/generate-category-page", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse generateCategoryPage(@RequestBody CMDSQuestionDto question,
                                                      @RequestParam(value = "domain", defaultValue = "VEDANTU") SeoDomain domain,
                                                      HttpServletRequest servletRequest) throws VException {
        httpSessionUtils.isAllowedApp(servletRequest);
        pageBuilderManager.createCategoryPage(null, question, domain, Boolean.FALSE);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/generate/html/microsite", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse generateMicrositeStaticPage(@RequestParam(value = "domain") SeoDomain domain) {
        if (!SeoDomain.VEDANTU.equals(domain)) {
            Map<String, Object> payload = new HashMap<>();
            payload.putIfAbsent("domain", domain.toString());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.GENERATE_MICROSITE_HTML_PAGES, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/generate/csv", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse generateCsv(@RequestParam(value = "domain", required = true) SeoDomain domain,
                              @RequestParam(value = "categoryId", required = false) String categoryId) throws IOException, IllegalAccessException, NoSuchFieldException, VException {
        return new PlatformBasicResponse(true, pageBuilderManager.generateXlsx(domain, categoryId), "");
    }

    @RequestMapping(value = "/getSubListUrlObjectFromCSV", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getSubListUrlObjectFromCSV(@RequestParam(name = "file", required = true) MultipartFile multipart,
                                             HttpServletResponse response) throws IOException, IllegalAccessException, NoSuchFieldException, VException {
        return new PlatformBasicResponse(true, pageBuilderManager.getSubListUrlObjectFromCSV(multipart), "");
    }


    @RequestMapping(value = "/regenerateWebpAndPng", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse regenerateWebpAndPng(@RequestParam(name = "url", required = true) String url, @RequestParam(name = "domain", required = true) SeoDomain domain) throws VException, IOException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return new PlatformBasicResponse(true, pageBuilderManager.regenerateWebpAndPngAsync(domain, url), "");
    }

    @RequestMapping(value = "/checkWebpStatus", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse checkWebpStatus(@RequestParam(name = "url", required = true) String url, @RequestParam(name = "domain", required = true) SeoDomain domain) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return new PlatformBasicResponse(true, pageBuilderManager.checkWebpStatus(domain, url), "");
    }

    //NOTE: Will be removed once verified
    @RequestMapping(value = "/clearCloudfrontCache", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse checkWebpStatus(@RequestBody String key) throws VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        pageBuilderManager.clearCloudFrontCache(key);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/generateInternalLinks", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CategoryPage generateInternalLinks(@RequestParam(value = "categoryPageId", required = true) String categoryPageId){

        return pageBuilderManager.generateInternalLinks(categoryPageId);
    }

    @RequestMapping(value = "/getMasterListUrlObjectFromCSV", method = RequestMethod.POST, headers = "content-type=multipart/form-data", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getMasterListUrlObjectFromCSV(@RequestParam(name = "file", required = true) MultipartFile multipart,
                                                               @RequestParam(name = "domain", required = true) SeoDomain domain,
                                                               @RequestParam(name = "title", required = false) String title,
                                                               @RequestParam(name = "url", required = false) String url,
                                                               @RequestParam(name = "options", required = true) MasterSidebarUploadOptions masterSidebarUploadOptions,
                                                               HttpServletResponse response) throws IOException, IllegalAccessException, NoSuchFieldException, VException {

        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        Set<String> urlSet = pageBuilderManager.getMasterListUrlObjectFromCSV(multipart, domain, title, url, masterSidebarUploadOptions);

        return new PlatformBasicResponse(true, urlSet, "");
    }

    @RequestMapping(value = "/getAllPdfMissingType3Pages", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Set<String> getAllPdfMissingType3Pages(@RequestParam(value = "start") Integer start,
                                                  @RequestParam(value = "page") Integer page){

        return pageBuilderManager.getAllPdfMissingType3Pages(start, page);
    }

    @RequestMapping(value = "/seo/vote-page", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse votePage(@RequestBody CategoryPageVoteRequest categoryPageVoteRequest) throws VException {

        CategoryPageVoteStatus voteStatus  = categoryPageVoteManager.votePage(categoryPageVoteRequest);
        return new PlatformBasicResponse(true, voteStatus, "");
    }

    @RequestMapping(value = "/seo/getTotalVotes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getTotalVotes(@RequestParam(value = "categoryPageId") String categoryPageId) throws VException {

        CategoryPageVoteStatus voteStatus = categoryPageVoteManager.getTotalVotes(categoryPageId);
        return new PlatformBasicResponse(true, voteStatus, "");
    }

    @RequestMapping(value = "/seo/migrateBulkUploadContents", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CategoryPage> migrateBulkUploadContents(@RequestBody Set<String> categoryIds, @RequestParam(value = "canUpdate", defaultValue = "false") boolean canUpdate) {

        return pageBuilderManager.migrateBulkUploadContents(categoryIds, canUpdate);
    }

}
