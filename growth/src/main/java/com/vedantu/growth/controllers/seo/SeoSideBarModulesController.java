package com.vedantu.growth.controllers.seo;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.VException;
import com.vedantu.growth.seo.SeoSideBarManager;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.entity.SeoSidebarModules;
import com.vedantu.growth.seo.pojo.SeoSidebarModulesDataRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/seoSideBar")
public class SeoSideBarModulesController {

    @Autowired
    SeoSideBarManager seoSideBarManager;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SeoSideBarModulesController.class);

    @RequestMapping(value = "/seo/upsertSideBarModules", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse upsertSideBarModules(@RequestBody SeoSidebarModules moduleGroup) throws BadRequestException, ConflictException {

        seoSideBarManager.upsertSideBarModules(moduleGroup);

        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getSideBarModules", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getSideBarModules(@RequestParam(value = "pageType") CategoryPageType pageType) {

        SeoSidebarModules moduleGroup = seoSideBarManager.getSideBarModules(pageType);

        return new PlatformBasicResponse(true, moduleGroup, "");
    }

    @RequestMapping(value = "/open/getSideBarModulesData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getSideBarModulesData(@RequestParam(value = "pageType") CategoryPageType pageType,
                                                       @RequestParam(value = "categoryPageId") String categoryPageId) throws VException, IOException {

        SeoSidebarModulesDataRes moduleGroup = seoSideBarManager.getSideBarModulesData(pageType, categoryPageId);

        return new PlatformBasicResponse(true, moduleGroup, "");
    }
}
