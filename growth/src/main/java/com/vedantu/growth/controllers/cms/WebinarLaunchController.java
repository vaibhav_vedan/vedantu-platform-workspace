package com.vedantu.growth.controllers.cms;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.VException;
import com.vedantu.growth.enums.WebinarSpotInstanceStatus;
import com.vedantu.growth.managers.cms.WebinarManager;
import com.vedantu.growth.response.cms.LaunchedWebinar;
import com.vedantu.growth.response.cms.WebinarLaunchStatusRes;
import com.vedantu.growth.response.cms.WebinarMonitorActionButtonResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("cms/webinar/launch")
public class WebinarLaunchController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CmsWebinarController.class);

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @RequestMapping(value = "/startUpcomingWebinarsCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void startUpcomingWebinarsCron(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                          @RequestBody String request) throws Exception {
        logger.info("startUpcomingWebinarsCron sns triggered with : "+request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("startUpcomingWebinarsCron received - SNS");
            logger.info(subscriptionRequest.toString());

            webinarManager.startUpcomingWebinarsAsync();
        }
    }

    @RequestMapping(value = "/launchUpcomingWebinars", method = RequestMethod.GET)
    @ResponseBody
    public void testWebinarTrigger(HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        webinarManager.startUpcomingWebinars();

    }

    @RequestMapping(value = "/launchWebinarsById/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void launchWebinarsById(@PathVariable(value = "webinarId") String webinarId,
                                   HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        webinarManager.launchWebinarsById(webinarId);

    }

    @RequestMapping(value = "/launchById/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void launchById(@PathVariable(value = "webinarId") String webinarId,
                                   HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);
        webinarManager.launchById(webinarId);

    }

    @RequestMapping(value = "/instanceShutdown/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void instanceShutdown(@PathVariable(value = "webinarId") String webinarId,
                                 HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        webinarManager.updateWebinarStatus(webinarId, WebinarSpotInstanceStatus.INSTANCE_SHUTDOWN, null);

    }

    @RequestMapping(value = "/instanceTerminated/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void instanceTerminated(@PathVariable(value = "webinarId") String webinarId,
                                   @RequestParam(value = "message", required = false) String message,
                                   HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        webinarManager.updateWebinarStatus(webinarId, WebinarSpotInstanceStatus.WEBINAR_TERMINATED, message);

    }

    @RequestMapping(value = "/webinarLoaded/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void webinarLoaded(@PathVariable(value = "webinarId") String webinarId,
                              HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        webinarManager.updateWebinarStatus(webinarId, WebinarSpotInstanceStatus.WEBINAR_LAUNCHED, null);

    }

    @RequestMapping(value = "/teacherLoggedIn/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public void teacherLoggedIn(@PathVariable(value = "webinarId") String webinarId,
                                @RequestParam(value = "isSuccess", defaultValue = "TRUE") String isSuccess,
                              HttpServletRequest request) throws VException {

        httpSessionUtils.isAllowedApp(request);

        if("TRUE".equalsIgnoreCase(isSuccess))
            webinarManager.updateWebinarStatus(webinarId, WebinarSpotInstanceStatus.TEACHER_LOGIN_SUCCESS, null);
        else
            webinarManager.updateWebinarStatus(webinarId, WebinarSpotInstanceStatus.TEACHER_LOGIN_FAILED, null);
    }

    @RequestMapping(value = "/getTodaysWebinars", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getTodaysWebinar(HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        List<LaunchedWebinar> launchedWebinars = webinarManager.getTodaysWebinars();

        return new PlatformBasicResponse(true, launchedWebinars, "");

    }

    @RequestMapping(value = "/getWebinarStatus/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getWebinarStatus(@PathVariable(value = "webinarId") String webinarId,
                                                  HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        WebinarLaunchStatusRes webinarLaunchStatusRes = webinarManager.getWebinarStatus(webinarId);

        return new PlatformBasicResponse(true, webinarLaunchStatusRes, "");

    }

    @RequestMapping(value = "/getActionButton/{webinarId}", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getActionButton(@PathVariable(value = "webinarId") String webinarId,
                                                  HttpServletRequest request) throws VException {

        if(httpSessionUtils.getCurrentSessionData() != null)
            httpSessionUtils.checkIfAllowed(null, null, true);
        else
            httpSessionUtils.isAllowedApp(request);

        WebinarMonitorActionButtonResponse webinarMonitorActionButtonResponse =  webinarManager.getActionButton(webinarId);

        return new PlatformBasicResponse(true, webinarMonitorActionButtonResponse, "");

    }

}
