package com.vedantu.growth.controllers.seo;

import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.growth.seo.StudyBoardManager;
import com.vedantu.growth.seo.request.BookmarkRequest;
import com.vedantu.growth.seo.request.BookmarkResponse;
import com.vedantu.growth.seo.response.StudyBoardResponse;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("seo/studyboard")
public class StudyBoardController {

    @Autowired
    private StudyBoardManager studyBoardManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Get Study Board for user", notes = "")
    public PlatformBasicResponse getStudyBoard(@PathVariable(value = "userId", required = true) String userId)
            throws VException {
        validateUser(userId);
        StudyBoardResponse studyBoard = studyBoardManager.getStudyBoard(userId);
        return new PlatformBasicResponse(true, studyBoard, "");
    }

    @RequestMapping(value = "/{userId}/notes", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Add or Update Notes", notes = "")
    public PlatformBasicResponse updateNotes(@PathVariable(value = "userId", required = true) String userId,
                                             @RequestBody List<String> notes) throws VException {
        validateUser(userId);
        studyBoardManager.updateNotes(userId, notes);
        return new PlatformBasicResponse(true, null, "");
    }

    @RequestMapping(value = "/{userId}/bookmarks", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Bookmark a category page for User", notes = "")
    public PlatformBasicResponse addBookmark(@PathVariable(value = "userId", required = true) String userId,
                                             @RequestBody BookmarkRequest request) throws VException {
        validateUser(userId);
        BookmarkResponse response = studyBoardManager.addBookmark(userId, request);
        return new PlatformBasicResponse(true, response, "");
    }

    @RequestMapping(value = "/{userId}/bookmarks", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete a bookmark", notes = "")
    public @ResponseBody
    PlatformBasicResponse deleteBookmark(@PathVariable(value = "userId", required = true) String userId,
                                         @RequestBody BookmarkRequest request) throws VException {
        validateUser(userId);
        boolean deleteBookmark = studyBoardManager.deleteBookmark(userId, request);
        return new PlatformBasicResponse(deleteBookmark, null, "");
    }

    @RequestMapping(value = "/{userId}/downloads/{categoryPageId}", method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Delete download history", notes = "")
    public PlatformBasicResponse deleteDownloadHistory(@PathVariable(value = "userId", required = true) String userId,
                                                       @PathVariable(value = "categoryPageId", required = true)
                                                               String categoryPageId) throws VException {
        validateUser(userId);
        boolean deleteDownloadHistory = studyBoardManager.deleteDownloadHistory(userId, categoryPageId);
        return new PlatformBasicResponse(deleteDownloadHistory, null, "");
    }

    private void validateUser(String userId) throws ConflictException {
        if (!sessionUtils.getCallingUserId().toString().equalsIgnoreCase(userId)) {
            throw new ConflictException(ErrorCode.CONFLICTS_FOUND, "Calling userId and userId in request doesn't match");
        }
    }

}
