package com.vedantu.growth.controllers.seo;

import com.vedantu.growth.seo.SeoRssFeedManager;
import com.vedantu.growth.seo.enums.SeoRssFeedGroup;
import com.vedantu.util.PlatformBasicResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;

@RestController
@RequestMapping("/seo/feedburner")
public class SeoRssFeedController {

    @Autowired
    private SeoRssFeedManager seoRssFeedManager;

    @RequestMapping(value = "/open/rss/{group}", method = RequestMethod.GET)
    @ResponseBody
    public String getRssFeed(@PathVariable(value = "group", required = true) SeoRssFeedGroup seoRssFeedGroup) throws ParserConfigurationException, TransformerException, IOException, SAXException {
        return seoRssFeedManager.getRssFeed(seoRssFeedGroup);
    }

    @RequestMapping(value = "/open/rss", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse upsertRssFeed(@RequestParam(value = "categoryPageId", required = true) String categoryPageId) {

        seoRssFeedManager.upsertRssItem(categoryPageId);

        return new PlatformBasicResponse(true, "", "");
    }
}
