package com.vedantu.growth.controllers.seo;

import com.vedantu.growth.seo.SeoNewsPageManager;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.response.BlogCategoryResponse;
import com.vedantu.growth.seo.response.BlogHomePageResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/seo/news")
public class SeoNewsPageController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoNewsPageController.class);

    @Autowired
    private SeoNewsPageManager seoNewsPageManager;

    @RequestMapping(value = "/open/home", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getNewsHomepage(@RequestParam(value = "domain") SeoDomain domain,
                                                 @RequestParam(value = "size") Integer size) {
        BlogHomePageResponse homePageResponse = seoNewsPageManager.getNewsHomepage(domain, size);
        return new PlatformBasicResponse(true, homePageResponse, "");
    }

    @RequestMapping(value = "/open/category/home", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getNewsCategoryHomepage(@RequestParam(value = "domain") SeoDomain domain,
                                                 @RequestParam(value = "start") Integer start,
                                                 @RequestParam(value = "size") Integer size,
                                                 @RequestParam(value = "target") String target) {
        BlogCategoryResponse metadataPojos = seoNewsPageManager.getNewsCategoryHomepage(domain, start, size, target);
        return new PlatformBasicResponse(true, metadataPojos, "");
    }

    @RequestMapping(value = "/open/latest/home", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getLatestNewsHomepage(@RequestParam(value = "domain") SeoDomain domain,
                                                       @RequestParam(value = "start") Integer start,
                                                       @RequestParam(value = "size") Integer size) {
        BlogCategoryResponse metadataPojos = seoNewsPageManager.getNewsCategoryHomepage(domain, start, size, null);
        return new PlatformBasicResponse(true, metadataPojos, "");
    }
}
