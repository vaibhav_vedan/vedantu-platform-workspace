/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.controllers.seo;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.growth.seo.SeoTutorListPageManager;
import com.vedantu.growth.seo.entity.SeoTutorListPage;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/seotutorlist")
public class SeoTutorListPageController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoTutorListPageController.class);

    @Autowired
    private SeoTutorListPageManager seoTutorListPageManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;


    @RequestMapping(value = "/addEditSeoTutorListPage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SeoTutorListPage addEditSeoTutorListPage(@RequestBody SeoTutorListPage seoTutorListPage) throws Exception {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        if (StringUtils.isEmpty(seoTutorListPage.getPath()) || StringUtils.isEmpty(seoTutorListPage.getName())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "name/path not found");
        }
        return seoTutorListPageManager.addEditPage(seoTutorListPage);
    }

    @RequestMapping(value = "/getByPath", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeoTutorListPage getByPath(@RequestParam(value = "path") String path) throws Exception {
        return seoTutorListPageManager.getByPath(path);
    }

    @RequestMapping(value = "/getList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SeoTutorListPage> getList(@RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size) throws Exception {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return seoTutorListPageManager.getList(start, size);
    }

    @RequestMapping(value = "/getById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public SeoTutorListPage getById(@RequestParam(value = "id") String id) throws Exception {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return seoTutorListPageManager.getById(id);
    }

}
