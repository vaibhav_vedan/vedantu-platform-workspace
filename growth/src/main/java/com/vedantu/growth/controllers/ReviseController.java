package com.vedantu.growth.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.enums.EventName;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.growth.entities.ReviseStats;
import com.vedantu.growth.managers.ReviseManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.ws.rs.core.MediaType;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/revise")
public class ReviseController {
    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(ReviseController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private ReviseManager reviseManager;

    @RequestMapping(value = "/reviseStats", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ReviseStats reviseStats(@RequestParam("eventName")  EventName event) throws Exception {

        if(sessionUtils.getCurrentSessionData() == null){
            throw new ForbiddenException(ErrorCode.NOT_LOGGED_IN, "Not Logged in");
        }

        return reviseManager.reviseStats(event, sessionUtils.getCallingUserId());
    }

    @RequestMapping(value = "/reviseS3Links", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public Map<String, String> reviseS3Links(@RequestParam("eventName")  EventName event) throws Exception {
        return reviseManager.reviseS3Links(event);
    }

    @RequestMapping(value = "/updateAfterReviseTargetJeeTest", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE ,produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public void updateAfterReviseJeeTest(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                         @RequestBody String req) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(req, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            if (StringUtils.isNotEmpty(subscriptionRequest.getSubject())) {

                SNSSubject subject = SNSSubject.valueOf(subscriptionRequest.getSubject());
                String message = subscriptionRequest.getMessage();
                reviseManager.invokeUpdateEventsAfterReviseJeeTest(subject, message);
            }

            // for topic
            // reviseManager.updateReviseStatsAfterTest(message);
        }
    }

    @RequestMapping(value = "/reloadReviseJeeTestIds", method = RequestMethod.POST)
    @ResponseBody
    public void reloadReviseJeeTestIds(@RequestBody List<String> testIds) throws BadRequestException, InternalServerErrorException {
        reviseManager.reloadReviseJeeTestIds(testIds);
    }

    @RequestMapping(value = "/reloadReviseJeeSessionIds", method = RequestMethod.POST)
    @ResponseBody
    public void reloadReviseJeeSessionIds(@RequestBody List<String> sessionIds) throws BadRequestException, InternalServerErrorException {
        reviseManager.reloadReviseJeeSessionIds(sessionIds);
    }

    @RequestMapping(value = "/getReviseJeeTestIds", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getReviseJeeTestIds() throws BadRequestException {
        return reviseManager.getReviseJeeTestIds();
    }

    @RequestMapping(value = "/getReviseJeeSessionIds", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getReviseJeeSessionIds() throws BadRequestException {
        return reviseManager.getReviseJeeSessionIds();
    }

    @RequestMapping(value = "/testSentry", method = RequestMethod.GET)
    @ResponseBody
    public void testSentry() {
        logger.error("Checking sentry error");
    }
}
