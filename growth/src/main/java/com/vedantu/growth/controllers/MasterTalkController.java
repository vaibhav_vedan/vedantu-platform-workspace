package com.vedantu.growth.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.growth.entities.MasterTalkDetails;
import com.vedantu.growth.managers.MasterTalkManager;
import com.vedantu.growth.pojo.mastertalk.MasterTalkDetailsReq;
import com.vedantu.growth.requests.MasterTalkFeedbackReq;
import com.vedantu.growth.requests.MasterTalkQuestionReq;
import com.vedantu.growth.requests.MasterTalkRegistrationReq;
import com.vedantu.growth.response.MasterClassPreRegistrationCheck;
import com.vedantu.growth.response.MasterTalkDetailsRes;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/master-talk")
public class MasterTalkController {

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private MasterTalkManager masterTalkManager;

    @RequestMapping(value = "/createOrUpdate", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse create(@RequestBody MasterTalkDetailsReq masterTalkDetails) throws VException {

        masterTalkDetails.verify();

        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);

        masterTalkManager.create(masterTalkDetails, sessionUtils.getCallingUserId());

        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getMasterTalkDetailsById", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getMasterTalkDetailsById(@RequestParam(value = "masterTalkId") String masterTalkId) throws VException {

        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);

        MasterTalkDetails masterTalkDetailsRes = masterTalkManager.getMasterTalkDetailsById(masterTalkId);

        return new PlatformBasicResponse(true, masterTalkDetailsRes, "");
    }

    @RequestMapping(value = "/open/getMasterTalkDetailsByUrl", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getMasterTalkDetailsByUrl(@RequestParam(value = "url") String url) throws VException {

        MasterTalkDetailsRes masterTalkDetailsRes = masterTalkManager.getMasterTalkDetailsByUrl(url);

        return new PlatformBasicResponse(true, masterTalkDetailsRes, "");
    }

    @RequestMapping(value = "/getMasterTalkDetails", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getMasterTalkDetails(@RequestParam(value = "start") Integer start,
                                        @RequestParam(value = "size") Integer size) throws VException {

        if(size > 20){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Size cannot be more than 20");
        }

        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);

        List<MasterTalkDetails> masterTalkDetailsList = masterTalkManager.getMasterTalkDetails(start, size);

        return new PlatformBasicResponse(true, masterTalkDetailsList, "");
    }

    @RequestMapping(value = "/preRegistrationCheck", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse preRegistrationCheck(@RequestParam(value = "masterTalkId") String masterTalkId) throws ForbiddenException {

        sessionUtils.checkIfUserLoggedIn();

        MasterClassPreRegistrationCheck masterClassPreRegistrationCheck = masterTalkManager.preRegistrationCheck(masterTalkId, sessionUtils.getCallingUserId());

        return new PlatformBasicResponse(true, masterClassPreRegistrationCheck, "");
    }

    @RequestMapping(value = "/preRegistrationCheckForMasterTalks", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse preRegistrationCheckForMasterTalks(@RequestParam(value = "masterTalkIds") List<String> masterTalkIds) throws ForbiddenException {

        sessionUtils.checkIfUserLoggedIn();

        Map<String, MasterClassPreRegistrationCheck> masterClassPreRegistrationCheck = masterTalkManager.preRegistrationCheckForMasterTalks(masterTalkIds, sessionUtils.getCallingUserId());

        return new PlatformBasicResponse(true, masterClassPreRegistrationCheck, "");
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse register(@RequestParam(value = "masterTalkId") String masterTalkId, @RequestBody MasterTalkRegistrationReq masterTalkRegistrationReq) throws BadRequestException, ForbiddenException, NotFoundException {

        sessionUtils.checkIfUserLoggedIn();

        boolean isRegistered = masterTalkManager.register(masterTalkId,masterTalkRegistrationReq, sessionUtils.getCallingUserId());

        return new PlatformBasicResponse(true, isRegistered, "");
    }

    @RequestMapping(value = "/addQuestion", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse addQuestion(@RequestBody MasterTalkQuestionReq masterTalkQuestionReq) throws BadRequestException, ForbiddenException {

        sessionUtils.checkIfUserLoggedIn();

        masterTalkManager.addQuestion(masterTalkQuestionReq, sessionUtils.getCallingUserId());

        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/markTestAttempted", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse markTestAttempted(@RequestBody MasterTalkQuestionReq masterTalkQuestionReq) throws ForbiddenException {

        sessionUtils.checkIfUserLoggedIn();

        masterTalkManager.markTestAttempted(masterTalkQuestionReq, sessionUtils.getCallingUserId());

        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/feedback", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse feedback(@RequestBody MasterTalkFeedbackReq masterTalkFeedbackReq) throws ForbiddenException {

        sessionUtils.checkIfUserLoggedIn();

        masterTalkManager.feedbackAsync(masterTalkFeedbackReq, sessionUtils.getCallingUserId());

        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/open/getUpcomingMasterTalks", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getUpcomingMasterTalks(@RequestParam(value = "start") Integer start,
                                                        @RequestParam(value = "size") Integer size) {

        List<MasterTalkDetails> upcomingMasterTalks = masterTalkManager.getUpcomingMasterTalks(start,size);
        return new PlatformBasicResponse(true, upcomingMasterTalks, "");
    }

    @RequestMapping(value = "/open/getUpcomingAndPastMasterTalks", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getUpcomingAndPastMasterTalks(@RequestParam(value = "start") Integer start,
                                                        @RequestParam(value = "size") Integer size) {

        Map<String, Object> masterTalks = masterTalkManager.getUpcomingAndPastMasterTalks(start,size);
        return new PlatformBasicResponse(true, masterTalks, "");
    }

}
