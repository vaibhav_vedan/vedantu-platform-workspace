package com.vedantu.growth.controllers.cms;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.VException;
import com.vedantu.growth.async.AsyncTaskName;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.entities.Webinar;
import com.vedantu.growth.enums.cms.WebinarType;
import com.vedantu.growth.managers.cms.WebinarManager;
import com.vedantu.growth.pojo.cms.WebinarCallbackData;
import com.vedantu.growth.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.growth.requests.cms.AddQuestionToRegistrationReq;
import com.vedantu.growth.requests.cms.CreateWebinarReq;
import com.vedantu.growth.requests.cms.EditWebinarReq;
import com.vedantu.growth.requests.cms.FindWebinarsReq;
import com.vedantu.growth.requests.cms.GetWebinarRegistrationInfoReq;
import com.vedantu.growth.requests.cms.WebinarCallbackReq;
import com.vedantu.growth.requests.cms.WebinarRegistrationReq;
import com.vedantu.growth.response.WebinarResponse;
import com.vedantu.growth.response.cms.FindQuizzesRes;
import com.vedantu.growth.response.cms.WebinarRegisterUserRes;
import com.vedantu.onetofew.pojo.WebinarSessionInfo;
import com.vedantu.subscription.pojo.CourseMobileRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("cms/webinar")
public class CmsWebinarController {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private WebinarManager webinarManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CmsWebinarController.class);

    @Autowired
    private RedisDAO redisDAO;

    private static Gson gson = new Gson();

    @RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WebinarRegisterUserRes webinarRegister(@RequestBody WebinarRegistrationReq req) throws VException {
        logger.info("webinar register request :" + req.toString());
        // Handle user data
        return webinarManager.webinarRegister(req);
    }

    @RequestMapping(value = "/updateWebinarAttended", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateWebinarAttended(@RequestBody GetWebinarRegistrationInfoReq req) throws VException {
        logger.info("webinar update attendence request :" + req.toString());
        if (StringUtils.isEmpty(req.getUserId()) && httpSessionUtils.getCurrentSessionData() != null) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setUserId(data.getUserId().toString());
        }
        return webinarManager.updateWebinarAttended(req);
    }

    @RequestMapping(value = "/updateOtmWebinarAttended", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateOtmWebinarAttended(@RequestBody GetWebinarRegistrationInfoReq req) throws VException {
        logger.info("otm webinar update attendence request :" + req.toString());
        if (StringUtils.isEmpty(req.getUserId()) && httpSessionUtils.getCurrentSessionData() != null) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setUserId(data.getUserId().toString());
        }
        return webinarManager.updateOtmWebinarAttended(req);
    }

    @RequestMapping(value = "/addQuestionToRegistration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WebinarRegisterUserRes addQuestionToRegistration(@RequestBody AddQuestionToRegistrationReq req) throws VException {
        logger.info("webinar addQuestionToRegistration request :" + req.toString());
        if (StringUtils.isEmpty(req.getEmail())) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setEmail(data.getEmail());
        }
        return webinarManager.addQuestionToRegistration(req);
    }

    @RequestMapping(value = "/webinarRequestCallback", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public WebinarCallbackData webinarRequestCallback(@RequestBody WebinarCallbackReq req) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.STUDENT, true);
        return webinarManager.webinarRequestCallback(req, httpSessionUtils.getCallingUserId());
    }

    @RequestMapping(value = "/getWebinarCallbackInfo", method = RequestMethod.GET)
    @ResponseBody
    public WebinarCallbackData getWebinarCallbackInfo() throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.STUDENT, true);
        return webinarManager.getWebinarCallbackInfo(httpSessionUtils.getCallingUserId());
    }

    @Deprecated
    @RequestMapping(value = "/getWebinarUserRegistrationInfo", method = RequestMethod.POST)
    @ResponseBody
    public WebinarUserRegistrationInfo getWebinarUserRegistrationInfoPost(@RequestBody GetWebinarRegistrationInfoReq req) throws VException {
        logger.info("webinar register request :" + req.toString());
        if (StringUtils.isEmpty(req.getEmail())) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setEmail(data.getEmail());
        }
        logger.info("after the email check : " + req.toString());
        return webinarManager.getWebinarUserRegistrationInfo(req);
    }

    @Deprecated
    @RequestMapping(value = "/getWebinarUserRegistrationInfos", method = RequestMethod.POST)
    @ResponseBody
    public List<WebinarRegisterUserRes> getWebinarUserRegistrationInfosPost(@RequestParam(value = "email", required = false) String email, @RequestBody List<String> webinarIds) throws VException {
        if (StringUtils.isEmpty(email)) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            email = data.getEmail();
        }

        return webinarManager.getWebinarUserRegistrationInfos(email, webinarIds);
    }

    @RequestMapping(value = "/getWebinarUserRegistrationInfo", method = RequestMethod.GET)
    @ResponseBody
    public WebinarUserRegistrationInfo getWebinarUserRegistrationInfo(GetWebinarRegistrationInfoReq req) throws VException {
        logger.info("webinar register request :" + req.toString());
        if (StringUtils.isEmpty(req.getEmail())) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            req.setEmail(data.getEmail());
        }
        logger.info("after the email check : " + req.toString());

        return webinarManager.getWebinarUserRegistrationInfo(req);
    }


    @RequestMapping(value = "/getWebinarUserRegistrationInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<WebinarRegisterUserRes> getWebinarUserRegistrationInfos(@RequestParam(value = "email", required = false) String email, @RequestParam(value = "webinarIds", required = true) List<String> webinarIds) throws VException {
        if (StringUtils.isEmpty(email)) {
            HttpSessionData data = httpSessionUtils.getCurrentSessionData();
            email = data.getEmail();
        }

        return webinarManager.getWebinarUserRegistrationInfos(email, webinarIds);
    }


    @RequestMapping(value = "/getWebinarsForUser", method = RequestMethod.GET)
    @ResponseBody
    public List<CourseMobileRes> getWebinarForUser(@RequestParam(value = "userId", required = true) String userId,
                                                   @RequestParam(value = "start", required = false) Integer start,
                                                   @RequestParam(value = "size", required = false) Integer size) throws VException {

        return webinarManager.getWebinarsForUser(userId, start, size);
    }

    @GetMapping(value = "/upcomingWebinarSessionIdForUser" , consumes = MediaType.APPLICATION_JSON_VALUE ,produces = MediaType.APPLICATION_JSON_VALUE )
    @ResponseBody
    public List<WebinarSessionInfo> upcomingWebinarSessionIdForUser(@RequestParam(value = "start", required = false) Integer start,
                                                                    @RequestParam(value = "size", required = false) Integer size) throws VException {

        httpSessionUtils.checkIfUserLoggedIn();
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.STUDENT, Role.TEACHER), Boolean.FALSE);
        Long callingUserId = httpSessionUtils.getCallingUserId();
        return webinarManager.upcomingWebinarSessionIdForUser(callingUserId, start, size);
    }


    @RequestMapping(value = "/registerForGTWWebinar", method = RequestMethod.GET)
    @ResponseBody
    public WebinarUserRegistrationInfo registerForGTWWebinar(@RequestParam(value = "id", required = true) String id) throws VException {
        logger.info("webinar register request :" + id);
        return webinarManager.registerForGTWWebinar(id);
    }

    @RequestMapping(value = "/createWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Webinar createWebinar(@RequestBody CreateWebinarReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return webinarManager.createWebinar(req);
    }

    @RequestMapping(value = "/bulkCreateWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, String> bulkCreateWebinar(@RequestBody List<CreateWebinarReq> createWebinarReqs) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return webinarManager.createWebinarBulk(createWebinarReqs);
    }

    @RequestMapping(value = "/editWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Webinar editWebinar(@RequestBody EditWebinarReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return webinarManager.editWebinar(req);
    }

    @RequestMapping(value = "/changeWebinarStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Webinar changeWebinarStatus(@RequestBody EditWebinarReq req) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return webinarManager.changeWebinarStatus(req);
    }

    @Deprecated
    @RequestMapping(value = "/getWebinarByCode", method = RequestMethod.POST)
    @ResponseBody
    public WebinarResponse getWebinarByCodePost(@RequestBody FindWebinarsReq req) throws VException {
        return webinarManager.getWebinarByCode(req);
    }

    @Deprecated
    @RequestMapping(value = "/getWebinarByCodeMobile", method = RequestMethod.POST)
    @ResponseBody
    public WebinarResponse getWebinarByCodeMobilePost(@RequestBody FindWebinarsReq req) throws VException {
//        httpSessionUtils.checkIfAllowed(null, Role.STUDENT, true);
        return webinarManager.getWebinarByCodeMobile(req);
    }

    @RequestMapping(value = "/getWebinarByCode", method = RequestMethod.GET)
    @ResponseBody
    public WebinarResponse getWebinarByCode(FindWebinarsReq req) throws VException {

        return webinarManager.getWebinarByCode(req);
    }

    @RequestMapping(value = "/getWebinarByCodeMobile", method = RequestMethod.GET)
    @ResponseBody
    public WebinarResponse getWebinarByCodeMobile(FindWebinarsReq req) throws VException {
//        httpSessionUtils.checkIfAllowed(null, Role.STUDENT, true);
        return webinarManager.getWebinarByCodeMobile(req);
    }

    @RequestMapping(value = "/getWebinarById", method = RequestMethod.GET)
    @ResponseBody
    public Webinar getWebinarById(@RequestParam(value = "webinarId", required = true) String webinarId) throws VException {
        return webinarManager.getWebinarById(webinarId);
    }

    @RequestMapping(value = "/isWebinarCodeAvailable", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse isWebinarCodeAvailable(@RequestBody FindWebinarsReq req) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return webinarManager.isWebinarCodeAvailable(req);
    }

    @Deprecated
    @RequestMapping(value = "/findWebinars", method = RequestMethod.POST)
    @ResponseBody
    public List<Webinar> findWebinarsPost(@RequestBody FindWebinarsReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);

        HttpSessionData data = httpSessionUtils.getCurrentSessionData();
        if(data != null) {
            if(data.getRole() != null && Role.ADMIN.equals(data.getRole())) {
                req.setShowSimLiveWebinars(true);
            }
        }
        List<Webinar> webinars = webinarManager.findWebinars(req);
        List<Webinar> finalWebinars = new ArrayList<>();
        if(data != null && Role.ADMIN.equals(data.getRole())) {
            return webinars;
        } else {
            List<String> hideWebinars = webinarManager.getHideWebinarIds();
            for (Webinar webinar : webinars) {
                if(!hideWebinars.contains(webinar.getId())) {
                    finalWebinars.add(webinar);
                }
            }
            return finalWebinars;
        }

    }

    @RequestMapping(value = "/findWebinars", method = RequestMethod.GET)
    @ResponseBody
    public List<Webinar> findWebinars(FindWebinarsReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);

        HttpSessionData data = httpSessionUtils.getCurrentSessionData();
        if(data != null) {
            if(data.getRole() != null && Role.ADMIN.equals(data.getRole())) {
                req.setShowSimLiveWebinars(true);
            }
        }
        List<Webinar> webinars = webinarManager.findWebinars(req);
        List<Webinar> finalWebinars = new ArrayList<>();
        if(data != null && Role.ADMIN.equals(data.getRole())) {
            return webinars;
        } else {
            List<String> hideWebinars  = webinarManager.getHideWebinarIds();
            for (Webinar webinar : webinars) {
                if(!hideWebinars.contains(webinar.getId())) {
                    finalWebinars.add(webinar);
                }
            }
            return finalWebinars;
        }
    }

    @Deprecated
    @RequestMapping(value = "/findWebinarsMobile", method = RequestMethod.POST)
    @ResponseBody
    public List<WebinarResponse> findWebinarsMobilePost(@RequestBody FindWebinarsReq req) throws VException {
        return webinarManager.findWebinarsMobile(req);
    }

    @RequestMapping(value = "/findWebinarsMobile", method = RequestMethod.GET)
    @ResponseBody
    public List<WebinarResponse> findWebinarsMobile(FindWebinarsReq req) throws VException {
//            httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return webinarManager.findWebinarsMobile(req);
    }

    @Deprecated
    @RequestMapping(value = "/getUpcomingWebinar", method = RequestMethod.POST)
    @ResponseBody
    public Webinar getUpcomingWebinarPost(@RequestBody FindWebinarsReq req) throws VException {
        return webinarManager.getUpcomingWebinar(req);
    }

    @RequestMapping(value = "/getUpcomingWebinar", method = RequestMethod.GET)
    @ResponseBody
    public Webinar getUpcomingWebinar(FindWebinarsReq req) throws VException {
        return webinarManager.getUpcomingWebinar(req);
    }

    @RequestMapping(value = "/syncGtwAttendence", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void syncGtwAttendence(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                  @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            webinarManager.syncGtwAttendenceAsync();
        }
    }

    @RequestMapping(value = "/sendWebinarRegistrationQuestions", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendWebinarRegistrationQuestions(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                                 @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            webinarManager.sendWebinarRegistrationQuestionsAsync();
        }
    }

    @RequestMapping(value = "/sendReminders", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void webinarReminder(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            webinarManager.sendWebinarRemindersAsync();
        }
    }

    @RequestMapping(value = "/findQuizzes", method = RequestMethod.GET)
    @ResponseBody
    public FindQuizzesRes findQuizzes() throws VException {

        return webinarManager.findQuizzes(WebinarType.QUIZ);
    }

    @RequestMapping(value = "/findLiveSessions", method = RequestMethod.GET)
    @ResponseBody
    public FindQuizzesRes findLiveSessions() throws VException {

        return webinarManager.findQuizzes(WebinarType.LIVECLASS);
    }

    @RequestMapping(value = "/getWebinarsForGradeBoard", method = RequestMethod.GET)
    @ResponseBody
    public List<CourseMobileRes> getWebinarForGradeBoard(@RequestParam(name = "board", required = false) String board,
                                                         @RequestParam(name = "grade", required = true) String grade,
                                                         @RequestParam(value = "start", required = false) Integer start,
                                                         @RequestParam(value = "size", required = false) Integer size) {
        return webinarManager.getWebinars(board, grade, start, size);
    }

    @RequestMapping(value = "/getPastWebinarsForGradeBoard", method = RequestMethod.GET)
    @ResponseBody
    public List<CourseMobileRes> getPastWebinarForGradeBoard(@RequestParam(name = "board", required = false) String board,
                                                             @RequestParam(name = "grade", required = true) String grade,
                                                             @RequestParam(value = "start", required = false) Integer start,
                                                             @RequestParam(value = "size", required = false) Integer size) {
        return webinarManager.getWebinarsForPast(board, grade, start, size);
    }

    @RequestMapping(value = "/send-webinar-attend-alert", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendWebinarAttendAlert(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, null, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());

            Map<String, Object> payload = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.WEBINAR_NOTIFICATION_TASK, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/send-email-after-webinar-completed", method = RequestMethod.POST,
            consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse sendEmailAfterWebinarCompleted(
            @RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException {
        logger.info(request);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, null, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());

            Map<String, Object> payload = new HashMap<>();
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_EMAIL_AFTER_WEBINAR_COMPLETED, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/findUpcomingWebinars", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<WebinarResponse> findWebinarsForHomeFeed(@RequestBody FindWebinarsReq findWebinarsReq) throws VException {

        return webinarManager.findWebinarsForHomeFeed(findWebinarsReq);
    }
    
    @RequestMapping(value = "/findLiveWebinarsForApp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<WebinarResponse> findLiveWebinarsForApp(@RequestBody FindWebinarsReq findWebinarsReq) throws VException {

        return webinarManager.findLiveWebinarsForApp(findWebinarsReq);
    }
    
    @RequestMapping(value = "/findUpcomingWebinarsForApp", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<WebinarResponse> findUpcomingWebinarsForApp(@RequestBody FindWebinarsReq findWebinarsReq) throws VException {

        return webinarManager.findUpcomingWebinarsForApp(findWebinarsReq);
    }

    @RequestMapping(value = "/getHideWebinarIds", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getHideWebinarIds() throws VException{
        List<String> webinarList = webinarManager.getHideWebinarIds();
        return new PlatformBasicResponse(true,webinarList,"");
    }

    @RequestMapping(value = "/setHideWebinarIds", method = RequestMethod.POST,consumes=MediaType.APPLICATION_JSON_VALUE,produces=MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setHideWebinarIds(@RequestBody List<String> itemsList) throws VException{
       httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
       webinarManager.setHideWebinarIds(itemsList);
       return new PlatformBasicResponse();
    }

}
