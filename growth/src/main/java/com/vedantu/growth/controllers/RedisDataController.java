package com.vedantu.growth.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.growth.enums.RedisPrefixType;
import com.vedantu.growth.managers.RedisDataManager;
import com.vedantu.growth.requests.CachedContentRequest;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/redis-data")
public class RedisDataController {

    @Autowired
    private RedisDataManager redisDataManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @RequestMapping(value = "/setPrefix", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse setPrefix(@RequestParam(value = "type") RedisPrefixType type,
                                                      @RequestParam(value = "prefix") String prefix) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        redisDataManager.setPrefix(type, prefix);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/setRedisData", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse setRedisData(@RequestBody CachedContentRequest cachedContentRequest) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        redisDataManager.setRedisData(cachedContentRequest);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getRedisData", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getRedisData(@RequestParam(value = "key") String key) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        String data = redisDataManager.getRedisData(key);
        return new PlatformBasicResponse(true, data, "");
    }
}
