
package com.vedantu.growth.controllers.seo;

import com.vedantu.exception.VException;
import com.vedantu.growth.seo.SeoSearchEngineManager;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.enums.ESSearchMode;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.request.SeoSearchReq;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/seoSearchEngine")
public class SeoSearchEngineController {

    @Autowired
    private SeoSearchEngineManager seoSearchEngineManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @RequestMapping(value = "/load-to-es", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse loadToEs(@RequestParam(value = "start", required = true) int start,
                                          @RequestParam(value = "size", required = true) int size,
                                          @RequestParam(value = "type", required = true) CategoryPageType type,
                                          @RequestParam(value = "domain", required = true) SeoDomain domain,
                                          HttpServletRequest httpServletRequest) throws VException {

        httpSessionUtils.isAllowedApp(httpServletRequest);
        return new PlatformBasicResponse(true, seoSearchEngineManager.loadToEs(start,size, domain, type), "");
    }

    @RequestMapping(value = "/search", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "getQnARelatedDocuments", notes = "Search for related category page QNA")
    public PlatformBasicResponse getQnARelatedDocuments(@RequestBody SeoSearchReq seoSearchReq) throws Exception {

        Map<String, Object> seoSearchResponses = seoSearchEngineManager.getQnARelatedDocuments(seoSearchReq);
        return new PlatformBasicResponse(true, seoSearchResponses,"");
    }

    @RequestMapping(value = "/search-test", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "SearchCategoryPage", notes = "Search for category page")
    public PlatformBasicResponse searchDocumentsTest(@RequestBody Map<String, Object> searchPayload) throws Exception {

        Map<String, Object> seoSearchResponses = seoSearchEngineManager.searchDocumentsTest(searchPayload);
        return new PlatformBasicResponse(true, seoSearchResponses,"");
    }

    @RequestMapping(value = "/setElasticUsageProperty", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "SearchCategoryPage", notes = "setElasticUsageProperty")
    public PlatformBasicResponse setElasticUsageProperty(@RequestParam(value = "canUseES", required = true) String canUseES,
                                                         @RequestParam(value = "esSearchMode", required = true) ESSearchMode esSearchMode,
                                                         HttpServletRequest httpServletRequest) throws Exception {

        httpSessionUtils.isAllowedApp(httpServletRequest);
        seoSearchEngineManager.setElasticUsageProperty(esSearchMode, canUseES);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/setSearchFilter", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse setSearchFilter(@RequestParam(value = "type") ESSearchMode esSearchMode,
                                                 @RequestBody List<String> searchFields,
                                                 HttpServletRequest httpServletRequest) throws Exception {

        httpSessionUtils.isAllowedApp(httpServletRequest);
        List<String> response = seoSearchEngineManager.setSearchFilter(esSearchMode, searchFields);
        return new PlatformBasicResponse(true, response,"");
    }

}
