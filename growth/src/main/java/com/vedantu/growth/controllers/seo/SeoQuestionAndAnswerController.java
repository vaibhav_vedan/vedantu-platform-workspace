package com.vedantu.growth.controllers.seo;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.growth.seo.SeoQuestionAndAnswerManager;
import com.vedantu.growth.seo.request.SeoQuestionAndAnswerFilterReq;
import com.vedantu.growth.seo.response.SeoQuestionAndAnswerFilterOptionsRes;
import com.vedantu.growth.seo.response.SeoQuestionAndAnswerFilterRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/question-answer")
public class SeoQuestionAndAnswerController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoQuestionAndAnswerController.class);

    @Autowired
    private SeoQuestionAndAnswerManager seoQuestionAndAnswerManager;

    @RequestMapping(value = "/open/get-filter-options", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse getFilterOptions() throws BadRequestException, InternalServerErrorException {

        SeoQuestionAndAnswerFilterOptionsRes filterOptionsRes = seoQuestionAndAnswerManager.getFilterOptions();

        return new PlatformBasicResponse(true, filterOptionsRes, "");
    }

    @RequestMapping(value = "/open/filtered-list", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse getFilteredList(@RequestBody SeoQuestionAndAnswerFilterReq filterReq) throws VException {

        SeoQuestionAndAnswerFilterRes questions = seoQuestionAndAnswerManager.getFilteredList(filterReq);

        return new PlatformBasicResponse(true, questions, "");
    }

}
