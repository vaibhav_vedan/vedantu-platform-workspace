package com.vedantu.growth.controllers.seo;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.growth.seo.PageBuilderManager;
import com.vedantu.growth.seo.entity.BlogCategory;
import com.vedantu.growth.seo.enums.BlogPageType;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.pojo.BlogArticlePojo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.Arrays;

@RestController
@RequestMapping("/seo/blog")
public class SeoBlogController {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoBlogController.class);

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @RequestMapping(value = "/open/home", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getBlogHomepage(@RequestParam(value = "domain", required = true) SeoDomain domain, @RequestParam(value = "size", required = true) Integer size) throws IOException, IllegalAccessException, NoSuchFieldException {
        return new PlatformBasicResponse(true, pageBuilderManager.getBlogHomepage(domain, size), "");
    }

    @RequestMapping(value = "/open/category/home", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getBlogCategoryHomepage(@RequestParam(value = "categoryName", required = true) String categoryName,
                                                         @RequestParam(value = "domain", required = true) SeoDomain domain,
                                                         @RequestParam(value = "totalCountRequired", required = false) Boolean totalCountRequired,
                                                         @RequestParam(value = "start", required = true) Integer start,
                                                         @RequestParam(value = "size", required = true) Integer size) throws VException {

        totalCountRequired = totalCountRequired !=null ? totalCountRequired : false;

        return new PlatformBasicResponse(true, pageBuilderManager.getBlogCategoryHomepage(domain, categoryName.trim(), start,  size, totalCountRequired), "");
    }

    @RequestMapping(value = "/open/latest/home", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getBlogLatestHomepage(@RequestParam(value = "domain", required = true) SeoDomain domain,
                                                       @RequestParam(value = "totalCountRequired", required = false) Boolean totalCountRequired,
                                                       @RequestParam(value = "start", required = true) Integer start,
                                                       @RequestParam(value = "size", required = true) Integer size) throws IOException{

        totalCountRequired = totalCountRequired !=null ? totalCountRequired : false;
        return new PlatformBasicResponse(true, pageBuilderManager.getBlogLatestHomepage(domain, start,  size, totalCountRequired), "");
    }

    @RequestMapping(value = "/categories", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse addBlogCategories(@RequestBody BlogCategory blogCategory, @RequestParam(value = "domain", required = true) SeoDomain domain) throws IOException, VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return new PlatformBasicResponse(true, pageBuilderManager.addBlogCategories(domain, blogCategory), "");
    }

    @RequestMapping(value = "/categories", method = RequestMethod.PUT)
    @ResponseBody
    public PlatformBasicResponse updateBlogCategories(@RequestBody BlogCategory blogCategory,  @RequestParam(value = "domain", required = true) SeoDomain domain) throws IOException, VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return new PlatformBasicResponse(true, pageBuilderManager.updateBlogCategories(domain, blogCategory), "");
    }

    @RequestMapping(value = "/open/categories", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getBlogCategories(@RequestParam(value = "domain", required = true) SeoDomain domain){
        return new PlatformBasicResponse(true, pageBuilderManager.getBlogCategories(domain), "");
    }

    @RequestMapping(value = "/article", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse upsertBlogArticle(@RequestBody BlogArticlePojo blogArticlePojo) throws  VException {
        httpSessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.SEO_EDITOR, Role.SEO_MANAGER), false);
        return new PlatformBasicResponse(true, pageBuilderManager.upsertBlogArticle(blogArticlePojo), "");
    }

    @RequestMapping(value = "/article", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getBlogArticle(@RequestParam(value = "domain", required = true) SeoDomain domain,
                                                @RequestParam(value = "blogPageType", required = true) BlogPageType blogPageType,
                                                @RequestParam(value = "blogCategoryId", required = false) String blogCategoryId) throws VException {

        BlogArticlePojo blogArticlePojo = new BlogArticlePojo();
        blogArticlePojo.setDomain(domain);
        blogArticlePojo.setBlogPageType(blogPageType);
        blogArticlePojo.setBlogCategoryId(blogCategoryId);

        return new PlatformBasicResponse(true, pageBuilderManager.getBlogArticle(blogArticlePojo), "");
    }

}
