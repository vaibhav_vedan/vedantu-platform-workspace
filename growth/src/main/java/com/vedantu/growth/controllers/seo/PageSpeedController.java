package com.vedantu.growth.controllers.seo;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.requests.PageSpeedDataRequest;
import com.vedantu.growth.seo.pageSpeed.PageSpeedManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/* about sns message: https://docs.aws.amazon.com/sns/latest/dg/sns-message-and-json-formats.html */
@RestController
@RequestMapping("pageSpeed")
public class PageSpeedController {

    @Autowired
    private PageSpeedManager pageSpeedManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PageSpeedController.class);

    @RequestMapping(value = "/triggerCategoryToSNS", method = RequestMethod.GET)
    public PlatformBasicResponse triggerCategoryToSNS(@RequestParam(value = "domain", required = true) SeoDomain domain){
        logger.info("In PageSpeedController calling PageSpeedManager triggerCategoryIdToSNS method for domain: "+domain);
        return pageSpeedManager.triggerCategoryToSNS(domain);
    }



    @RequestMapping(value = "/triggerPageUrlsToSQS", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void triggerPageUrlsToSQS(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
                                             @RequestBody String request) throws Exception {
        logger.info("In PageSpeedController, got message of messageTYpe : "+messageType);
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            logger.info("sns payload message : "+subscriptionRequest.getMessage());
            pageSpeedManager.triggerPageUrlsToSQS(subscriptionRequest.getMessage());
        }
    }

    @RequestMapping(value = "/createEditPageSpeedData", method = RequestMethod.POST)
    public boolean createEditPageSpeedData(@RequestBody PageSpeedDataRequest pageSpeedData) throws VException {
        logger.info("In PageSpeedController calling createEditPageSpeedData with pageSpeedData: "+pageSpeedData.toString());
        return pageSpeedManager.createEditPageSpeedData(pageSpeedData);
    }

    @RequestMapping(value = "/seoPageUrlsSendToSQSWithLimit", method = RequestMethod.GET)
    public PlatformBasicResponse seoPageUrlsSendToSQSWithLimit(@RequestParam(value = "seoDomain", required = true) SeoDomain seoDomain,
                                                               @RequestParam(value = "start", required = true) Integer start,
                                                               @RequestParam(value = "end", required = true) Integer end,
                                                               @RequestParam(value = "limit", required = true) Integer limit) throws BadRequestException {
        return pageSpeedManager.seoPageUrlsSendToSQSWithLimit(seoDomain,start,end,limit);
    }

}
