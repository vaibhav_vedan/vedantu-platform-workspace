package com.vedantu.growth.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.enums.EventName;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.dao.ReviseDAO;
import com.vedantu.growth.entities.ReviseStats;
import com.vedantu.growth.pojo.AnalyticsInfo;
import com.vedantu.growth.pojo.TestAnalytics;
import com.vedantu.growth.pojo.TestCategoryAnalytics;
import com.vedantu.growth.pojo.UpdateStatsPostTestReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ReviseManager {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ReviseManager.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    ReviseDAO reviseDao;

    @Autowired
    RedisDAO redisDAO;

    private final Gson gson = new Gson();

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    private static final String REVISE_TARGET_JEE_NEET_TEST_IDS = "REVISE_TARGET_JEE_NEET_TEST_IDS";
    private static final String REVISE_TARGET_JEE_NEET_SESSION_IDS = "REVISE_TARGET_JEE_NEET_SESSION_IDS";


    public ReviseStats reviseStats(EventName event, Long userId) {
        String key = getReviseResultRedisKey(event, userId.toString());
        try {
            String stats = redisDAO.get(key);
            if (stats != null) {
                return gson.fromJson(stats, ReviseStats.class);
            }
        } catch (InternalServerErrorException | BadRequestException e) {
            logger.error(e.getErrorMessage(), e);
        }

        ReviseStats stats =  reviseDao.getJeeRevise(event, userId);

        if(stats == null){
            return null;
        }
        try{
            redisDAO.setex(key, gson.toJson(stats, ReviseStats.class),DateTimeUtils.SECONDS_PER_WEEK);
        }
        catch(InternalServerErrorException e) {
            logger.error(e.getErrorMessage(), e);
        }

        return stats;
    }

    private String getReviseResultRedisKey(EventName event, String userId) {
        return "REVISE_RESULT_" + userId + "_" + event;
    }

    public PlatformBasicResponse updateReviseStatsAfterTest(UpdateStatsPostTestReq req) {
            logger.info("updateReviseJeeAfterTest : " + req);

            Update update = new Update();

            if(StringUtils.isNotEmpty(req.getType()) && "POST_SESSION".equals(req.getType())){

                try {
                    String key = "REVISE_JEE_POST_SESSION_" + req.getUserId() + "_" + req.getSessionId();
                    String val = redisDAO.get(key);

                    if(StringUtils.isNotEmpty(val))
                        return new PlatformBasicResponse();

                    redisDAO.setex(key,Boolean.TRUE.toString(), DateTimeUtils.SECONDS_PER_WEEK);
                }catch (Exception e){
                    logger.error("Error fetching the session count from rediss:",e);

                }
                update.inc(ReviseStats.Constants.SESSIONS_ATTENDED, 1);
            }else {

                List<String> attemptedTestsList = new ArrayList<>();

                ReviseStats reviseStats = reviseStats(req.getEventName(), Long.parseLong(req.getUserId()));
                if(reviseStats != null && CollectionUtils.isNotEmpty(reviseStats.getTestAttempted()))
                    attemptedTestsList = reviseStats.getTestAttempted();

                String testId = req.getTestId();

                update.set(ReviseStats.Constants.TEST_ATTEMPTED, attemptedTestsList);

                if(null != req.getEventName()){
                    update.set(ReviseStats.Constants.EVENT, req.getEventName());
                }

                if (StringUtils.isNotEmpty(req.getTestAttemptId())) {
                    update.set(ReviseStats.Constants.TEST_ATTEMPT_ID, req.getTestAttemptId());
                }
                if (StringUtils.isNotEmpty(req.getPerformanceCode())) {
                    update.set(ReviseStats.Constants.TEST_PERFORMANCE_CODE, req.getPerformanceCode());
                }
                if (null != (req.getAnalytics())) {

                    TestAnalytics testAnalytics = req.getAnalytics();

                    testId = testAnalytics.getTestId();

                    AnalyticsInfo analyticsInfo = testAnalytics.getInfo();
                    Integer totalMarks = 0;

                    List<TestCategoryAnalytics> testCategoryAnalytics = testAnalytics.getCategories();
                    for (TestCategoryAnalytics categoryAnalytics : testCategoryAnalytics) {
                        AnalyticsInfo categoryAnalyticsInfo = categoryAnalytics.getInfo();
                        Integer categoryMarks ;

                        if("Physics".equalsIgnoreCase(categoryAnalytics.getName())){
                            categoryMarks = categoryAnalyticsInfo.getCorrect() * 2;
                        }else if("Chemistry".equalsIgnoreCase(categoryAnalytics.getName())){
                            categoryMarks = categoryAnalyticsInfo.getCorrect() * 2;
                        }else if("Mathematics".equalsIgnoreCase(categoryAnalytics.getName())){
                            categoryMarks = categoryAnalyticsInfo.getCorrect() * 2;
                        }else {
                            categoryMarks = categoryAnalyticsInfo.getCorrect() * 3;
                        }

                        categoryAnalyticsInfo.setMarks(categoryMarks);
                        totalMarks += categoryMarks;
                    }

                    analyticsInfo.setMarks(totalMarks);

                    update.set(ReviseStats.Constants.TEST_ANALYTICS, req.getAnalytics());

                }

                if (StringUtils.isNotEmpty(testId) && !attemptedTestsList.contains(testId)) {
                    update.inc(ReviseStats.Constants.TESTS, 1);
                    attemptedTestsList.add(req.getTestId());
                }

                if (StringUtils.isNotEmpty(req.getS3Link())) {
                    update.set("s3Link", req.getS3Link());
                }
            }
            reviseDao.updateReviseStatsAfterTest(update, req.getUserId(), req.getEventName());
            deleteReviseResultRedisData(req.getEventName(), req.getUserId());
            if(null != req.getAnalytics()){
                try {
                    setReviseTestAttemptStatus(Long.parseLong(req.getUserId()), req.getAnalytics().getTestId());
                } catch (Exception e) {
                    logger.info("Error in updating user details after revise jee" + e.getMessage());
                }
            }
        return new PlatformBasicResponse();
    }

    public void setReviseTestAttemptStatus(Long userId, String testId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(userEndpoint + "/revisetargetjeeneet/userdetails/attempt?" +
                        String.format("userId=%d&testId=%s", userId, testId)
                , HttpMethod.POST,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public void deleteReviseResultRedisData(EventName event, String userId){
        String key = getReviseResultRedisKey(event, userId);
        try{
            redisDAO.del(key);
        } catch (Exception e) {
            logger.warn("Could not delete redis key : {}", key);
        }
    }

    public Map<String, String> reviseS3Links(EventName event) {

        switch (event){
            case REVISE_JEE_2020_MARCH:
                return getReviseJeeMarchS3Urls();
            case TARGET_JEE_NEET:
                return getReviseTargetJeeNeetS3Urls();
        }

        return new HashMap<>();
    }

    private Map<String, String> getReviseJeeMarchS3Urls() {

        Map<String, String> map = new HashMap<>();

        if ("prod".equalsIgnoreCase(env)) {
            map.put("classes", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes.json");
            map.put("pdfs", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/pdfs.json");
            map.put("tests", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/tests.json");
            map.put("rankedStudents", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/rankList.json");
        } else {
            map.put("classes", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes.json");
            map.put("pdfs", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/pdfs.json");
            map.put("tests", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/tests.json");
            map.put("rankedStudents", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/rankList.json");
        }

        return map;
    }

    private Map<String, String> getReviseTargetJeeNeetS3Urls() {

        Map<String, String> map = new HashMap<>();

        if ("prod".equalsIgnoreCase(env)) {
            map.put("classes_jee", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_jee.json");
            map.put("classes_neet", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_neet.json");
            map.put("classes_jee_11", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_jee_11.json");
            map.put("classes_neet_11", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_neet_11.json");
            map.put("classes_jee_12", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_jee_12.json");
            map.put("classes_neet_12", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_neet_12.json");
            map.put("classes_jee_13", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_jee_13.json");
            map.put("classes_neet_13", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/classes_neet_13.json");
            map.put("pdfs_jee", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/pdf_jee.json");
            map.put("pdfs_neet", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/pdf_neet.json");
            map.put("tests_jee", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/tests_jee.json");
            map.put("tests_neet", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/tests_neet.json");
            map.put("rankedStudents", "https://vmkt.s3-ap-southeast-1.amazonaws.com/revise-jee/rankList.json");
        } else {
            map.put("classes_jee_11", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes_jee_11.json");
            map.put("classes_neet_11", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes_neet_11.json");
            map.put("classes_jee_12", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes_jee_12.json");
            map.put("classes_neet_12", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes_neet_12.json");
            map.put("classes_jee_13", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes_jee_13.json");
            map.put("classes_neet_13", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/classes_neet_13.json");
            map.put("pdfs_jee", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/pdf_jee.json");
            map.put("pdfs_neet", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/pdf_neet.json");
            map.put("tests_jee", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/tests_jee.json");
            map.put("tests_neet", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/tests_neet.json");
            map.put("rankedStudents", "https://vmkt-qa.s3-ap-southeast-1.amazonaws.com/dev7/json-files/rankList.json");
        }

        return map;
    }

    public void reloadReviseJeeTestIds(List<String> testIds) throws BadRequestException, InternalServerErrorException {

        redisDAO.del(REVISE_TARGET_JEE_NEET_TEST_IDS);

        for (String id : testIds){
            redisDAO.addToList(REVISE_TARGET_JEE_NEET_TEST_IDS, id);
        }

    }

    public List<String> getReviseJeeTestIds() {

        List<String> reviseJeeTestIds = new ArrayList<>();

        try {
            reviseJeeTestIds = redisDAO.getList(REVISE_TARGET_JEE_NEET_TEST_IDS, 0 , 200);
        }catch (Exception e){
            logger.error("Error fetching REVISE_JEE_TEST_IDS", e);
        }

        return reviseJeeTestIds;

    }

    public void reloadReviseJeeSessionIds(List<String> sessionIds) throws BadRequestException, InternalServerErrorException {

        redisDAO.del(REVISE_TARGET_JEE_NEET_SESSION_IDS);

        for (String id : sessionIds){
            redisDAO.addToList(REVISE_TARGET_JEE_NEET_SESSION_IDS, id);
        }

    }

    public List<String> getReviseJeeSessionIds() {

        List<String> reviseJeeSessionIds = new ArrayList<>();

        try {
            reviseJeeSessionIds = redisDAO.getList(REVISE_TARGET_JEE_NEET_SESSION_IDS, 0 , 200);
        }catch (Exception e){
            logger.error("Error fetching REVISE_JEE_SESSION_IDS", e);
        }

        return reviseJeeSessionIds;

    }

    public void invokeUpdateEventsAfterReviseJeeTest(SNSSubject subject, String payload) {

        Type type = new TypeToken<UpdateStatsPostTestReq>() {
        }.getType();
        UpdateStatsPostTestReq req = gson.fromJson(payload, type);

        switch (subject) {

            case UPDATE_JEE_STATS_POST_SESSION:
                List<String> reviseJeeSessionIds = null;
                try {
                    // todo define redis keys in static context
                    reviseJeeSessionIds = redisDAO.getList("REVISE_TARGET_JEE_NEET_SESSION_IDS", 0, 200);
                } catch (BadRequestException e) {
                    logger.warn("Error while fetching cached REVISE_TARGET_JEE_NEET_SESSION_IDS");
                }

                if (CollectionUtils.isNotEmpty(reviseJeeSessionIds) && reviseJeeSessionIds.contains(req.getSessionId())) {
                    updateReviseStatsAfterTest(req);
                }
                break;

            case UPDATE_JEE_STATS_POST_TEST:
                List<String> reviseJeeTestIds = new ArrayList<>();

                try {
                    reviseJeeTestIds = redisDAO.getList("REVISE_TARGET_JEE_NEET_TEST_IDS", 0, 200);
                } catch (BadRequestException e) {
                    logger.warn("Error while fetching reviseJeeTestIds");
                }

                if (CollectionUtils.isNotEmpty(reviseJeeTestIds) && reviseJeeTestIds.contains(req.getTestId())) {
                    updateReviseStatsAfterTest(req);
                }
                break;

            default:
                logger.warn("invalid subject for sns subscription req {}", subject.name());
        }

    }
}
