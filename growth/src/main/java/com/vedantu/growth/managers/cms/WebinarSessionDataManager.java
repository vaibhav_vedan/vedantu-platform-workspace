package com.vedantu.growth.managers.cms;

import com.vedantu.growth.dao.cms.WebinarSessionDataDao;
import com.vedantu.growth.entities.WebinarGttDetails;
import com.vedantu.growth.entities.WebinarSessionDetails;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class WebinarSessionDataManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(WebinarSessionDataManager.class);

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private WebinarSessionDataDao webinarSessionDataDao;

    public void upsertWebinarSessionDetails(WebinarSessionDetails webinarSessionDetails){

        logger.info("upsertWebinarSessionDetails ENTRY : {}", webinarSessionDetails);

        WebinarSessionDetails existingWebinarSessionDetails =  mapper.map(webinarSessionDetails, WebinarSessionDetails.class);
        existingWebinarSessionDetails.setCreatedBy(webinarSessionDetails.getCreatedBy());
        existingWebinarSessionDetails.setCreationTime(webinarSessionDetails.getCreationTime());
        webinarSessionDataDao.upsert(existingWebinarSessionDetails);

        logger.info("upsertWebinarSessionDetails EXIT");

    }

    public void upsertWebinarGttDetails(WebinarGttDetails webinarGttDetails){

        logger.info("upsertWebinarGttDetails ENTRY: {}", webinarGttDetails);

        WebinarGttDetails mappedWebinarGttDetails =  webinarSessionDataDao.getWebinarGttBySessionAndUserId(webinarGttDetails.getSessionId(), webinarGttDetails.getUserId());

        if(Objects.isNull(mappedWebinarGttDetails)){
            mappedWebinarGttDetails =  mapper.map(webinarGttDetails, WebinarGttDetails.class);
        }else {
            mappedWebinarGttDetails.setLbPercentile(webinarGttDetails.getLbPercentile());
            mappedWebinarGttDetails.setLbRank(webinarGttDetails.getLbRank());
            mappedWebinarGttDetails.setLbPoints(webinarGttDetails.getLbPoints());
            mappedWebinarGttDetails.setQuizNumbers(webinarGttDetails.getQuizNumbers());
        }

        webinarSessionDataDao.upsert(mappedWebinarGttDetails);
        logger.info("upsertWebinarGttDetails EXIT");

    }

}
