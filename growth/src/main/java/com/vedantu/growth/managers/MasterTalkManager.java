package com.vedantu.growth.managers;

import com.google.gson.Gson;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.growth.aws.AwsSQSManager;
import com.vedantu.growth.dao.MasterTalkDetailsDao;
import com.vedantu.growth.dao.MasterTalkRegistrationDao;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.entities.MasterTalkDetails;
import com.vedantu.growth.entities.MasterTalkRegistration;
import com.vedantu.growth.notifications.SMSManager;
import com.vedantu.growth.pojo.FeedbackPojo;
import com.vedantu.growth.pojo.MastertalkFilterReq;
import com.vedantu.growth.pojo.mastertalk.MasterTalkDetailsReq;
import com.vedantu.growth.requests.MasterTalkFeedbackReq;
import com.vedantu.growth.requests.MasterTalkQuestionReq;
import com.vedantu.growth.requests.MasterTalkRegistrationReq;
import com.vedantu.growth.response.MasterClassPreRegistrationCheck;
import com.vedantu.growth.response.MasterTalkDetailsRes;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class MasterTalkManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MasterTalkManager.class);

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private MasterTalkRegistrationDao masterTalkRegistrationDao;

    @Autowired
    private MasterTalkDetailsDao masterTalkDetailsDao;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private SMSManager smsManager;

    private static Gson gson = new Gson();

    private static final String MASTER_TALK_EP = "MASTER_TALK_EP_";

    public MasterClassPreRegistrationCheck preRegistrationCheck(String masterTalkId, Long userId) {

        logger.info("MasterTalk preRegistrationCheck check : {}", userId);

        MasterTalkRegistration masterTalkRegistration = masterTalkRegistrationDao.getMasterTalkRegisteredRecord(masterTalkId, userId);

        Boolean isRegistered = Boolean.FALSE;
        Boolean isTestAttempted = Boolean.FALSE;
        Boolean isFeedbackGiven = Boolean.FALSE;

        if(Objects.nonNull(masterTalkRegistration)) {
            isRegistered = masterTalkRegistration.getIsRegistered();
            isTestAttempted = masterTalkRegistration.getIsTestAttempted();
            isFeedbackGiven = Objects.nonNull(masterTalkRegistration.getFeedback());
        }

        MasterClassPreRegistrationCheck preRegistrationCheck = new MasterClassPreRegistrationCheck();
        preRegistrationCheck.setIsRegistered(isRegistered);
        preRegistrationCheck.setIsTestAttempted(isTestAttempted);
        preRegistrationCheck.setIsFeedbackGiven(isFeedbackGiven);

        logger.info("MasterTalk preRegistrationCheck check : {} :: {}", userId, preRegistrationCheck);

        return preRegistrationCheck;

    }

    public Map<String, MasterClassPreRegistrationCheck> preRegistrationCheckForMasterTalks(List<String> masterTalkIds, Long callingUserId) {
        logger.info("MasterTalk preRegistrationCheck check : {}", callingUserId);

        List<MasterTalkRegistration> masterTalkRegistrations = masterTalkRegistrationDao.getMasterTalkRegisteredRecord(masterTalkIds, callingUserId);

        Map<String, MasterClassPreRegistrationCheck> masterClassPreRegistrationCheckMap = new HashMap<>();
        for(MasterTalkRegistration masterTalkRegistration: masterTalkRegistrations){
            Boolean isRegistered = masterTalkRegistration.getIsRegistered();
            Boolean isTestAttempted = masterTalkRegistration.getIsTestAttempted();
            Boolean isFeedbackGiven = Objects.nonNull(masterTalkRegistration.getFeedback());

            MasterClassPreRegistrationCheck preRegistrationCheck = new MasterClassPreRegistrationCheck();
            preRegistrationCheck.setIsRegistered(isRegistered);
            preRegistrationCheck.setIsTestAttempted(isTestAttempted);
            preRegistrationCheck.setIsFeedbackGiven(isFeedbackGiven);
            masterClassPreRegistrationCheckMap.put(masterTalkRegistration.getMasterTalkId(),preRegistrationCheck);
            masterTalkIds.remove(masterTalkRegistration.getMasterTalkId());
        }

        //Set default values for remaining ids
        for(String masterTalkId : masterTalkIds){
            MasterClassPreRegistrationCheck preRegistrationCheck = new MasterClassPreRegistrationCheck();
            preRegistrationCheck.setIsRegistered(Boolean.FALSE);
            preRegistrationCheck.setIsTestAttempted(Boolean.FALSE);
            preRegistrationCheck.setIsFeedbackGiven(Boolean.FALSE);
            masterClassPreRegistrationCheckMap.put(masterTalkId,preRegistrationCheck);
        }

        return masterClassPreRegistrationCheckMap;
    }

    public boolean register(String masterTalkId, MasterTalkRegistrationReq masterTalkRegistrationReq, Long callingUserId) throws BadRequestException, NotFoundException {

        logger.info("MasterTalk register request : {}::{}",masterTalkId, callingUserId);

        MasterTalkDetails masterTalkDetails = getMasterTalkDetailsById(masterTalkId);

        if(Objects.isNull(masterTalkDetails)){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Masterclass not found with the given id");
        }

        MasterTalkRegistration masterTalkRegistration = masterTalkRegistrationDao.getMasterTalkRegisteredRecord(masterTalkId, callingUserId);

        if(Objects.nonNull(masterTalkRegistration)) {
            if(masterTalkRegistration.getIsRegistered()) {
                throw new BadRequestException(ErrorCode.ALREADY_REGISTERED, "Already registered");
            }
        }else {

            masterTalkRegistration = new MasterTalkRegistration();
            masterTalkRegistration.setMasterTalkId(masterTalkId);
            masterTalkRegistration.setUserId(callingUserId);
        }

        masterTalkRegistration.setUtmCampaign(masterTalkRegistrationReq.getUtmCampaign());
        masterTalkRegistration.setUtmMedium(masterTalkRegistrationReq.getUtmMedium());
        masterTalkRegistration.setUtmSource(masterTalkRegistrationReq.getUtmSource());
        masterTalkRegistration.setUtmTerm(masterTalkRegistrationReq.getUtmTerm());

        masterTalkRegistration.setIsRegistered(Boolean.TRUE);

        masterTalkRegistrationDao.save(masterTalkRegistration, callingUserId.toString());

        logger.info("MasterTalk registered: {} :: id: {}", callingUserId, masterTalkRegistration.getId());

        String smsContent = masterTalkDetails.getCommunication().getRegistrationSmsContent();

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(callingUserId, true);
        String parsedContent = smsContent.replace("{{name}}",userBasicInfo.getFirstName());

        TextSMSRequest textSMSRequest = new TextSMSRequest(userBasicInfo.getContactNumber(), userBasicInfo.getPhoneCode(), null, CommunicationType.MASTER_TALK, userBasicInfo.getRole());
        textSMSRequest.setBody(parsedContent);
        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_SMS, new Gson().toJson(textSMSRequest));
        return true;

    }

    public void addQuestion(MasterTalkQuestionReq masterTalkQuestionReq, Long callingUserId) throws BadRequestException {

        logger.info("MasterTalk addQuestion {}", masterTalkQuestionReq);

        MasterTalkRegistration masterTalkRegistration = masterTalkRegistrationDao.getMasterTalkRegisteredRecord(masterTalkQuestionReq.getMasterTalkId(), callingUserId);

        if(Objects.nonNull(masterTalkRegistration)) {
            if (masterTalkRegistration.getQuestions().size() > 2) {
                throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED, "Max allowed questions are 3");
            }
        }
        else {
                masterTalkRegistration = new MasterTalkRegistration();
                masterTalkRegistration.setMasterTalkId(masterTalkQuestionReq.getMasterTalkId());
                masterTalkRegistration.setUserId(callingUserId);
            }

        masterTalkRegistration.addQuestion(masterTalkQuestionReq.getQuestion());

        masterTalkRegistrationDao.save(masterTalkRegistration, callingUserId.toString());

        logger.info("MasterTalk added Question");

    }

    public void markTestAttempted(MasterTalkQuestionReq masterTalkQuestionReq, Long callingUserId) {

        logger.info("MasterTalk markTestAttempted {}", masterTalkQuestionReq);

        MasterTalkRegistration masterTalkRegistration = masterTalkRegistrationDao.getMasterTalkRegisteredRecord(masterTalkQuestionReq.getMasterTalkId(), callingUserId);

        if(Objects.isNull(masterTalkRegistration)) {
            masterTalkRegistration = new MasterTalkRegistration();
            masterTalkRegistration.setMasterTalkId(masterTalkQuestionReq.getMasterTalkId());
            masterTalkRegistration.setUserId(callingUserId);
        }else if(masterTalkRegistration.getIsTestAttempted() == masterTalkQuestionReq.getIsTestAttempted()) {
            return;
        }

        masterTalkRegistration.setIsTestAttempted(masterTalkQuestionReq.getIsTestAttempted());

        masterTalkRegistrationDao.save(masterTalkRegistration, callingUserId.toString());

    }

    public void feedback(MasterTalkFeedbackReq masterTalkFeedbackReq){

        logger.info("masterTalkFeedbackReq {}", masterTalkFeedbackReq);

        MasterTalkRegistration masterTalkRegistration = masterTalkRegistrationDao.getMasterTalkRegisteredRecord(masterTalkFeedbackReq.getMasterTalkId(), masterTalkFeedbackReq.getUserId());

        if(Objects.isNull(masterTalkRegistration)){
            masterTalkRegistration = new MasterTalkRegistration();
            masterTalkRegistration.setMasterTalkId(masterTalkFeedbackReq.getMasterTalkId());
            masterTalkRegistration.setUserId(masterTalkFeedbackReq.getUserId());
        }

        FeedbackPojo feedbackPojo = new FeedbackPojo();
        feedbackPojo.setRating(masterTalkFeedbackReq.getRating());
        feedbackPojo.setFeedback(masterTalkFeedbackReq.getFeedback());

        masterTalkRegistration.setFeedback(feedbackPojo);

        masterTalkRegistrationDao.save(masterTalkRegistration, masterTalkFeedbackReq.getUserId().toString());

        logger.info("MasterTalk added feedback");

    }

    public void feedbackAsync(MasterTalkFeedbackReq masterTalkFeedbackReq, Long callingUserId) {

        masterTalkFeedbackReq.setUserId(callingUserId);

        String request = gson.toJson(masterTalkFeedbackReq);
        awsSQSManager.sendToSQS(SQSQueue.MASTERTALK_FEEDBACK, SQSMessageType.MASTERTALK_FEEDBACK, request, SQSMessageType.MASTERTALK_FEEDBACK.name());
    }

    public void create(MasterTalkDetailsReq masterTalkDetailsReq, Long userId) throws BadRequestException, ConflictException, NotFoundException {

        MasterTalkDetails existingMasterTalkDetails = getMasterTalkDetailsByTypeUrl(masterTalkDetailsReq.getPageURL());

        if(StringUtils.isEmpty(masterTalkDetailsReq.getId()) && Objects.nonNull(existingMasterTalkDetails)){
            throw new ConflictException(ErrorCode.CONFLICTS_FOUND, " Page url already used by some other master talk");
        }

        MasterTalkDetails masterTalkDetails = gson.fromJson(gson.toJson(masterTalkDetailsReq), MasterTalkDetails.class);

        String masterTalkType = null;
        if(StringUtils.isNotEmpty(masterTalkDetailsReq.getId())){

            if(Objects.nonNull(existingMasterTalkDetails)&& !masterTalkDetailsReq.getId().equalsIgnoreCase(existingMasterTalkDetails.getId())){
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Document id is not matching with the record");
            }

            if(Objects.isNull(existingMasterTalkDetails)){

                existingMasterTalkDetails = masterTalkDetailsDao.getMasterTalkDetailsById(masterTalkDetailsReq.getId());
                if(Objects.isNull(existingMasterTalkDetails)){
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No record found with the given document id");
                }

                //Deleting the redis as the pageUrl is updated
                clearRedisDataForMasterTalkUrl(existingMasterTalkDetails.getPageURL());

            }

            masterTalkType = existingMasterTalkDetails.getMasterTalkType();
            masterTalkDetails.setCreationTime(existingMasterTalkDetails.getCreationTime());
            masterTalkDetails.setCreatedBy(existingMasterTalkDetails.getCreatedBy());

        }else {
            Long totalMasterTalks = masterTalkDetailsDao.getTotalmasterTalksCount();
            Long oldMasterTalkCount = ConfigUtils.INSTANCE.getLongValue("old.master.talk.count");
            masterTalkType = MASTER_TALK_EP +  (totalMasterTalks + oldMasterTalkCount + 1);
        }

        masterTalkDetails.setMasterTalkType(masterTalkType);

        masterTalkDetailsDao.save(masterTalkDetails, userId.toString());
        setInRedis(masterTalkDetails);

    }

    private MasterTalkDetails getMasterTalkDetailsByTypeUrl(String pageURL) {

        try {
            String masterTalkUrlIdRedisKey = getMasterTalkURLAndIdMappingRedisKey(pageURL);
            String masterTalkId = redisDAO.get(masterTalkUrlIdRedisKey);

            if(StringUtils.isNotEmpty(masterTalkId)) {
                return getMasterTalkDetailsById(masterTalkId);
            }
        } catch (Exception e) {
            logger.error("Error fetching from redis : {}", e.getMessage(), e);
        }

        MasterTalkDetails masterTalkDetails = masterTalkDetailsDao.getMasterTalkDetailsByTypeUrl(pageURL);
        setInRedis(masterTalkDetails);
        return masterTalkDetails;
    }

    public void clearRedisDataForMasterTalkUrl(String url){
        try {
            String masterTalkUrlIdRedisKey = getMasterTalkURLAndIdMappingRedisKey(url);
            redisDAO.del(masterTalkUrlIdRedisKey);
        } catch (Exception e) {
            logger.error("Error deleting from redis : {}", e.getMessage(), e);
        }

    }

    private void setInRedis(MasterTalkDetails masterTalkDetails) {

        if(masterTalkDetails == null)
            return;
        try {
            String masterTalkRedisKey = getMasterTalkDetailsRedisKey(masterTalkDetails.getId());
            redisDAO.setex(masterTalkRedisKey, gson.toJson(masterTalkDetails), DateTimeUtils.HOURS_PER_2DAYS);

            String masterTalkUrlIdRedisKey = getMasterTalkURLAndIdMappingRedisKey(masterTalkDetails.getPageURL());
            redisDAO.setex(masterTalkUrlIdRedisKey, masterTalkDetails.getId(), DateTimeUtils.HOURS_PER_2DAYS);
        } catch (Exception e) {
            logger.error("Error saving in redis : {}", e.getMessage(), e);
        }
    }

    public static String getMasterTalkDetailsRedisKey(String masterTalkId) {

        return "MASTER_TALK_DETAILS_" + masterTalkId;
    }

    public static String getMasterTalkURLAndIdMappingRedisKey(String url) {

        url = url.trim().replaceAll("/", "_");

        return "MASTER_TALK_URL_ID_MAPPING_" + url;
    }

    public MasterTalkDetails getMasterTalkDetailsById(String masterTalkId) throws NotFoundException {

        try {
            String redisKey = getMasterTalkDetailsRedisKey(masterTalkId);
            String value = redisDAO.get(redisKey);
            if(StringUtils.isNotEmpty(value)){
                return gson.fromJson(value,MasterTalkDetails.class);
            }
        } catch (Exception e) {
            logger.error("Error fetching from redis : {}", e.getMessage(), e);
        }

        MasterTalkDetails masterTalkDetails = masterTalkDetailsDao.getMasterTalkDetailsById(masterTalkId);

        if(Objects.isNull(masterTalkDetails)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Mastertalk not found with the given id : " + masterTalkId);
        }
        setInRedis(masterTalkDetails);
        return masterTalkDetails;

    }

    public List<MasterTalkDetails> getMasterTalkDetails(Integer start, Integer size) {

        return masterTalkDetailsDao.getMasterTalkDetails(start, size);
    }

    public MasterTalkDetailsRes getMasterTalkDetailsByUrl(String url) throws NotFoundException {

        MasterTalkDetails masterTalkDetails = null;
        try {
            String masterTalkUrlIdRedisKey = getMasterTalkURLAndIdMappingRedisKey(url);
            String masterTalkId = redisDAO.get(masterTalkUrlIdRedisKey);

            if(StringUtils.isNotEmpty(masterTalkId)) {
                masterTalkDetails =  getMasterTalkDetailsById(masterTalkId);
            }
        } catch (Exception e) {
            logger.error("Error fetching from redis : {}", e.getMessage(), e);
        }

        if(Objects.isNull(masterTalkDetails)) {
            masterTalkDetails = masterTalkDetailsDao.getMasterTalkDetailsByTypeUrl(url);

            if (Objects.isNull(masterTalkDetails) || !EntityState.ACTIVE.equals(masterTalkDetails.getEntityState())) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "No Active Mastertalk details found with the given URL :" + url);
            }
            setInRedis(masterTalkDetails);
        }

        MasterTalkDetailsRes masterTalkDetailsRes = gson.fromJson(gson.toJson(masterTalkDetails), MasterTalkDetailsRes.class);
        //masterTalkDetailsRes.setPastMasterTalks(getPastMasterTalksMetadata(0, 3));

        return masterTalkDetailsRes;
    }

    public List<MasterTalkDetails> getUpcomingMasterTalks(Integer start, Integer size) {
        MastertalkFilterReq mastertalkFilterReq = MastertalkFilterReq.builder()
                .start(start)
                .size(size)
                .build();
        List<MasterTalkDetails> masterTalkDetailsList =  masterTalkDetailsDao.getUpcomingMasterTalks(mastertalkFilterReq);
        return masterTalkDetailsList;
    }

    public Map<String, Object> getUpcomingAndPastMasterTalks(Integer start, Integer size) {

        Map<String, Object> payload = new HashMap<>();
        List<MasterTalkDetails> masterTalkDetailsList =  getUpcomingMasterTalks(start,size);

        // Checking if the upcoming records are less than the requested size
        if(masterTalkDetailsList.size() < size){
            MastertalkFilterReq mastertalkFilterReq = MastertalkFilterReq.builder()
                    .start(start)
                    .size(size-masterTalkDetailsList.size())
                    .build();
            List<MasterTalkDetails> pastMasterTalkDetailsList =  masterTalkDetailsDao.getPastMasterTalks(mastertalkFilterReq);
            masterTalkDetailsList.addAll(pastMasterTalkDetailsList);
        }
        Long totalMasterTalks = masterTalkDetailsDao.getTotalmasterTalksCount();
        payload.put("payload", masterTalkDetailsList);
        payload.put("total", totalMasterTalks);
        return payload;
    }

}
