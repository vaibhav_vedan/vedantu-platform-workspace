package com.vedantu.growth.managers;

import com.google.gson.Gson;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.seo.enums.ESSearchMode;
import com.vedantu.growth.utils.ESConfig;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ESManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private ESConfig eSConfig;

    private RestHighLevelClient client;
    private Logger logger;

    @Autowired
    private RedisDAO redisDAO;

    private static final Gson gson = new Gson();

    public static final String  CAN_USE_ES_REDIS_KEY = "CAN_USE_ES_";
    public static final String  ES_DISABLED_PERMANENT = "ES_DISABLED_PERMANENT";
    public static final String  ES_DISABLED_TEMPORARY = "ES_DISABLED_TEMPORARY";
    public static final Integer ES_DISABLED_TEMPORARY_TIMEOUT_MINUTES = 5;
    public static final Integer ES_CALLS_MAX_RESPONSE_TIME_IN_MINUTES = 2;

    @PostConstruct
    public void init() {
        client = eSConfig.getClient();
        logger = logFactory.getLogger(ESManager.class);
    }

    public IndexResponse doIndexRequest(IndexRequest indexRequest) throws IOException {
        return doIndexRequest(indexRequest,RequestOptions.DEFAULT);
    }

    public IndexResponse doIndexRequest(IndexRequest indexRequest, RequestOptions requestOptions) throws IOException {
        IndexResponse indexResponse = client.index(indexRequest, requestOptions);
        return indexResponse;
    }

    public SearchResponse doSearchRequest(SearchRequest searchRequest) throws IOException {
        Long startTime = System.currentTimeMillis();
        SearchResponse searchResponse =  doSearchRequest(searchRequest,RequestOptions.DEFAULT);
        Long timeTook = System.currentTimeMillis() - startTime;

        if(timeTook > DateTimeUtils.MILLIS_PER_SECOND*ES_CALLS_MAX_RESPONSE_TIME_IN_MINUTES){
            markTemporaryDisableESCalls();
        }
        return  searchResponse;
    }

    public SearchResponse doSearchRequest(SearchRequest searchRequest, RequestOptions requestOptions) throws IOException {
        return client.search(searchRequest, requestOptions);
    }

    public DeleteResponse doDeleteRequest(DeleteRequest deleteRequest) {
        return doDeleteRequest(deleteRequest,RequestOptions.DEFAULT);
    }

    public BulkResponse doBulkIndexRequest(BulkRequest bulkRequest) throws IOException {
        return client.bulk(bulkRequest,RequestOptions.DEFAULT);
    }

    public DeleteResponse doDeleteRequest(DeleteRequest deleteRequest, RequestOptions requestOptions) {
        DeleteResponse deleteResponse = null;
        try {
            deleteResponse = client.delete(deleteRequest, requestOptions);
        } catch (IOException e) {
            logger.error("Error in deleting for deleteRequest {} with error {}",deleteRequest,e);
        }
        return deleteResponse;
    }


    private void markTemporaryDisableESCalls(){
        try {
            Long disableTill = System.currentTimeMillis() + DateTimeUtils.MILLIS_PER_MINUTE * ES_DISABLED_TEMPORARY_TIMEOUT_MINUTES;

            for(ESSearchMode esSearchMode : ESSearchMode.values()) {
                try {
                    String esUsageKey = CAN_USE_ES_REDIS_KEY + esSearchMode;
                    String esUsagePropertyString = redisDAO.get(esUsageKey);

                    Map<String, String> esUsageProperty = new HashMap<>();
                    if (StringUtils.isNotEmpty(esUsagePropertyString)) {
                        esUsageProperty = gson.fromJson(esUsagePropertyString, Map.class);
                    }
                    esUsageProperty.put(ES_DISABLED_TEMPORARY, String.valueOf(disableTill));
                    redisDAO.set(esUsageKey, gson.toJson(esUsageProperty));
                }catch (Exception e){
                    logger.error("Error setting redis", e);
                }
            }
            redisDAO.incr(ES_DISABLED_TEMPORARY+"_counter");
        }catch (Exception e){
            logger.error("Error setting redis", e);
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing es connection ", e);
        }
    }

}
