/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.managers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.*;
import com.vedantu.User.response.UserDetailsInfo;
import com.vedantu.exception.*;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.notifications.CommunicationManager;
import com.vedantu.growth.notifications.SMSManager;
import com.vedantu.growth.requests.SendSeoPdfByEmailReq;
import com.vedantu.growth.requests.SendSeoPdfBySMSReq;
import com.vedantu.growth.seo.StudyBoardManager;
import com.vedantu.util.*;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URISyntaxException;

/*
 *
 *
 * @author somil
 */
@Service
public class UserManager {

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private final Gson gson = new Gson();
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(UserManager.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private CommunicationManager emailManager;

    @Autowired
    private StudyBoardManager studyBoardManager;

    @Autowired
    private SMSManager smsManager;

    public UserManager() {

    }

    public UserDetailsInfo getUserDetailsForISLInfo(Long userId, String event) throws VException {
        UserDetailsInfo info = getUserDetailsInfo(userId, event);
        if (info != null) {
            info.setServerTime(System.currentTimeMillis());
            return info;
        }
        ClientResponse incResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserDetailsForISLInfo?userId=" + userId + "&event=" + event,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(incResp);

        String jsonString = incResp.getEntity(String.class);
        UserDetailsInfo response = new Gson().fromJson(jsonString, UserDetailsInfo.class);
        response.setServerTime(System.currentTimeMillis());
        return response;
    }

    public UserDetailsInfo getUserDetailsInfo(Long userId, String event) {

        String key = "PROD_USER_DETAILS_" + userId + "_EVENT_" + event;
        String keybasicInfo = "PROD_USER_BASIC_" + userId;
        try {
            String detailstring = redisDAO.get(key);
            if (StringUtils.isNotEmpty(detailstring)) {
                UserDetailsInfo info = gson.fromJson(detailstring, UserDetailsInfo.class);
                if (info != null) {
                    UserBasicInfo basicInfo = null;
                    String userString = redisDAO.get(keybasicInfo);
                    if (StringUtils.isNotEmpty(userString)) {
                        basicInfo = gson.fromJson(userString, UserBasicInfo.class);
                    } else {
                        basicInfo = fosUtils.getUserBasicInfo(userId, true);
                    }

                    info.setStudent(basicInfo);
                    return info;
                }
            }
        } catch (Exception e) {
            logger.info("Exception");
        }

        return null;
    }

    public PlatformBasicResponse sendSeoPdfByEmail(SendSeoPdfByEmailReq req, HttpServletRequest request, HttpServletResponse response) throws VException, IOException, URISyntaxException {
        req.verify();
        User toUser = getUserById(sessionUtils.getCallingUserId());
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        if (toUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id[" + req.getUserId() + "]");
        }

        if (StringUtils.isEmpty(toUser.getEmail()) && StringUtils.isNotEmpty(req.getEmail())) {
            toUser.setEmail(req.getEmail());
            String url = USER_ENDPOINT + "/addUserEmailSeoPdf?";
            Long userId = sessionUtils.getCallingUserId();
            String email = req.getEmail();
            url = url + "email=" + email + "&token=4hK6958ubW7AHaWa";
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String userJsonStr = resp.getEntity(String.class);
            User user = gson.fromJson(userJsonStr, User.class);
            sessionUtils.setCookieAndHeaders(request, response, user);
        }

        emailManager.sendUserEmailToDownloadSEOContent(toUser, req.getContentId());
        studyBoardManager.addDownloadHistoryToStudyBoard(sessionUtils.getCallingUserId().toString(), req.getContentId());

        return platformBasicResponse;
    }

    public User getUserById(Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No user id found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + userId, HttpMethod.GET,
                null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        User response = new Gson().fromJson(jsonString, User.class);
        return response;
    }

    public PlatformBasicResponse sendSeoPdfBySMS(SendSeoPdfBySMSReq req) throws VException {
        req.verify();
        User toUser = getUserById(sessionUtils.getCallingUserId());
        if (toUser == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "no user found with id[" + req.getUserId() + "]");
        }
        PlatformBasicResponse response = new PlatformBasicResponse();
        smsManager.sendUserSMSToDownloadSEOContent(toUser, req.getContentId());
        return response;
    }

    public UserInfo getUserInfo(Long userId) throws VException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No user id found");
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserInfo?userId=" + userId, HttpMethod.GET,
                null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson gson = gsonBuilder.create();
        UserInfo response = gson.fromJson(jsonString, UserInfo.class);
        return response;
    }


}
