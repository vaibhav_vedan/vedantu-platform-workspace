/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.managers.cms;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.growth.aws.AwsSNSManager;
import com.vedantu.growth.dao.cms.WebinarInfoDAO;
import com.vedantu.growth.dao.cms.WebinarUserRegistrationInfoDAO;
import com.vedantu.growth.enums.cms.WebinarUserTag;
import com.vedantu.growth.pojo.cms.GTWSessionAttendeeInfo;
import com.vedantu.growth.pojo.cms.GTWSessionInfo;
import com.vedantu.growth.pojo.cms.WebinarInfo;
import com.vedantu.growth.pojo.cms.WebinarInfoRes;
import com.vedantu.growth.pojo.cms.WebinarUserRegistrationInfo;
import com.vedantu.growth.requests.RegisterWebinarUserReq;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author jeet
 */
@Service
public class WebinarGTWManager {
    @Autowired
    private WebinarUserRegistrationInfoDAO webinarUserRegistrationInfoDAO;
    //
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(WebinarGTWManager.class);

    private static Gson gson = new Gson();

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private WebinarInfoDAO webinarInfoDAO;

    @Autowired
    private FosUtils fosUtils;

    private static final ArrayList<String> SUPPORTED_GTW_ACCOUNT_IDS =  new ArrayList<>(
            Arrays.asList("gtw@vedantu.com","pranav.mallar@vedantu.com","academics@vedantu.com")
    );

    private static final Type WEBINAR_INFO_RES_TYPE = new TypeToken<List<WebinarInfoRes>>() {
    }.getType();

    private static Map<String, String> GTW_ACCESS_TOKENS = Collections.unmodifiableMap(new HashMap<String, String>() {
        private static final long serialVersionUID = -6665886557479988046L;

        {
            put("gtw@vedantu.com", "Ty5nXCAGnW4VOWo25k64GZrIVbmt");
            put("pranav.mallar@vedantu.com", "60BZSARTMWdnAOe7AeaYv7vp8vh0");
            put("academics@vedantu.com", "chNGb6vkjIrGEprRuu47b2NfJk1M");
        }
    });

    private static Map<String, String> GTW_ORGANIZER_IDS = Collections.unmodifiableMap(new HashMap<String, String>() {
        private static final long serialVersionUID = -6665886557479988046L;

        {
            put("gtw@vedantu.com", "9010144132967191052");
            put("pranav.mallar@vedantu.com", "9017557036820883206");
            put("academics@vedantu.com", "7470416618302296069");
        }
    });

    private static Map<String, String> GTW_ACCOUNT_KEYS = Collections.unmodifiableMap(new HashMap<String, String>() {
        private static final long serialVersionUID = -6665886557479988046L;

        {
            put("gtw@vedantu.com", "1591413714311022853");
            put("pranav.mallar@vedantu.com", "6848942971880425228");
            put("academics@vedantu.com", "94810155808804869");
        }
    });

    private static final Type WEBINAR_SESSIONS_TYPE = new TypeToken<List<GTWSessionInfo>>() {
    }.getType();

    private static final Type WEBINAR_SESSION_ATTENDANCE_TYPE = new TypeToken<List<GTWSessionAttendeeInfo>>() {
    }.getType();

    public boolean isGTWseatSupported(String email){
        return SUPPORTED_GTW_ACCOUNT_IDS.contains(email);
    }

    public WebinarUserRegistrationInfo registerWebinar(WebinarUserRegistrationInfo webinarUserRegistrationInfo,
                                                       String trainingId) throws BadRequestException, InternalServerErrorException {
        logger.info("Request:" + webinarUserRegistrationInfo.toString());

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(webinarUserRegistrationInfo.getUserId(),true);
        RegisterWebinarUserReq registerWebinarUserReq = new RegisterWebinarUserReq(webinarUserRegistrationInfo);
        registerWebinarUserReq.setEmail(userBasicInfo.getEmail());
        registerWebinarUserReq.setPhone(userBasicInfo.getContactNumber());
        if(StringUtils.isEmpty(registerWebinarUserReq.getEmail())){
            throw new BadRequestException(ErrorCode.EMAIL_NOT_FOUND, "Email not found for registering to gtw webinar");
        }
        logger.info("\n\n\n\n\nRequest:" + registerWebinarUserReq.toString());
        ClientResponse respOTFBatch = WebUtils.INSTANCE.doCall(getRegisterWebinarUrl(trainingId), HttpMethod.POST,
                gson.toJson(registerWebinarUserReq));
        String jsonRespOTFBatch = respOTFBatch.getEntity(String.class);
        logger.info(jsonRespOTFBatch);
        logger.info("Status code of response " + respOTFBatch.getStatus());
        if (respOTFBatch.getStatus() == 200 || respOTFBatch.getStatus() == 201) {
            JSONObject jsonObject = new JSONObject(jsonRespOTFBatch);
            updateGtwResponse(webinarUserRegistrationInfo, registerWebinarUserReq, trainingId, jsonObject);
        } else {
            boolean consumed = false;
            if(jsonRespOTFBatch != null){
                JSONObject jsonObject = new JSONObject(jsonRespOTFBatch);
                if (jsonObject.has("registrantKey")) {
                    logger.warn("consumed error in registering to webinar " + jsonRespOTFBatch + " registration:"+webinarUserRegistrationInfo);
                    updateGtwResponse(webinarUserRegistrationInfo, registerWebinarUserReq, trainingId, jsonObject);
                    consumed = true;
                } else if(jsonObject.has("description")){
                    String description = jsonObject.getString("description");
                    if(description != null && description.endsWith("webinar that is in the past")){
                        logger.warn("consumed error in registering to webinar " + jsonRespOTFBatch + " registration:"+webinarUserRegistrationInfo);
                        consumed = true;
                    }
                }

            }
            if(!consumed){
                logger.error("error in registering to webinar " + jsonRespOTFBatch + " registration:"+webinarUserRegistrationInfo);
            }
        }
        return webinarUserRegistrationInfo;
    }

    public WebinarUserRegistrationInfo updateGtwResponse(WebinarUserRegistrationInfo webinarUserRegistrationInfo,RegisterWebinarUserReq registerWebinarUserReq, String trainingId,JSONObject jsonObject) throws InternalServerErrorException {
        webinarUserRegistrationInfo.updateRegisterResponse(jsonObject);
        webinarUserRegistrationInfo.setRegisterEmail(registerWebinarUserReq.getEmail());

        WebinarUserRegistrationInfo existingEntry = webinarUserRegistrationInfoDAO
                .getWebinarUserInfoTrainingId(webinarUserRegistrationInfo.getUserId(), trainingId);
        if (existingEntry == null) {
            webinarUserRegistrationInfo.addTag(WebinarUserTag.GTW_REGISTRATION_DONE);
            webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
            logger.info(" saved entity:" + webinarUserRegistrationInfo.toString());
        } else {
            existingEntry.addTag(WebinarUserTag.GTW_REGISTRATION_DONE);
            existingEntry.updateRegisterResponse(webinarUserRegistrationInfo);
            existingEntry.setRegisterEmail(registerWebinarUserReq.getEmail());
            webinarUserRegistrationInfoDAO.create(existingEntry);
            webinarUserRegistrationInfo = existingEntry;
            logger.info(" saved entity:" + webinarUserRegistrationInfo.toString());
        }
        return webinarUserRegistrationInfo;
    }

    public WebinarInfo getWebinarInfo(String webinarId, String emailId) throws ParseException {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarInfo.Constants.WEBINAR_ID).is(webinarId));
        List<WebinarInfo> results = webinarInfoDAO.runQuery(query, WebinarInfo.class);
        if (!CollectionUtils.isEmpty(results)) {
            return results.get(0);
        } else {
            // Sync webinarId
            return syncWebinarInfos(webinarId,emailId);
        }
    }

    public WebinarInfo syncWebinarInfos(String webinarId, String emailId) throws ParseException {
        String serverUrl = getWebinarInfoByIdUrl(webinarId, emailId);
        ClientResponse syncWebinarInfoByIdRes = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
                GTW_ACCESS_TOKENS.get(emailId));
        String jsonResp = syncWebinarInfoByIdRes.getEntity(String.class);
        if (syncWebinarInfoByIdRes.getStatus() >= 400) {
            return null;
        }
        logger.info("row json " + jsonResp);
        WebinarInfoRes webinarInfoRes = gson.fromJson(jsonResp, WebinarInfoRes.class);
        logger.info("WebinarInfoRes " + webinarInfoRes);
        WebinarInfo webinarInfo = new WebinarInfo(webinarInfoRes, emailId);
        logger.info("webinarInfo " + webinarInfo);
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarInfo.Constants.WEBINAR_ID).is(webinarId));
        Update update = new Update();
        update.set("webinarSubId", webinarInfo.getWebinarSubId());
        update.set("accountKey", webinarInfo.getAccountKey());
        update.set("organizerKey", webinarInfo.getOrganizerKey());
        update.set("accountEmailId", webinarInfo.getAccountEmailId());
        update.set("registrationUrl", webinarInfo.getRegistrationUrl());
        update.set("name", webinarInfo.getName());
        update.set("subject", webinarInfo.getSubject());
        update.set("description", webinarInfo.getDescription());
        update.set("startTime", webinarInfo.getStartTime());
        update.set("endTime", webinarInfo.getEndTime());
        update.set("timeZone", webinarInfo.getTimeZone());
        update.set(WebinarInfo.Constants.LAST_UPDATED, System.currentTimeMillis());
        logger.info("query " + query);
        logger.info("update " + update);
        webinarInfoDAO.upsertEntity(query, update, WebinarInfo.class);
        return webinarInfo;
    }

    public List<WebinarUserRegistrationInfo> processWebinarAttendance(List<WebinarInfo> webinarInfos)
            throws ParseException {
        List<WebinarUserRegistrationInfo> results = new ArrayList<>();
        if (!CollectionUtils.isEmpty(webinarInfos)) {
            for (WebinarInfo webinarInfo : webinarInfos) {
                try {
                    Query query = new Query();
                    query.addCriteria(Criteria.where(WebinarUserRegistrationInfo.Constants.TRAININGID)
                            .is(webinarInfo.getWebinarId()));
                    List<WebinarUserRegistrationInfo> registeredUsers = webinarUserRegistrationInfoDAO.runQuery(query,
                            WebinarUserRegistrationInfo.class);
                    Map<String, List<GTWSessionAttendeeInfo>> attendanceMap = fetchGTWAttendenceInfoMap(
                            webinarInfo.getWebinarId(), webinarInfo.getAccountEmailId());
                    if (!CollectionUtils.isEmpty(registeredUsers)) {

                        List<String> userIds = registeredUsers.parallelStream().map(v -> v.getUserId()).collect(Collectors.toList());
                        Map<String, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMap(userIds,true);

                        // Fetch the attendance info from gtw
                        for (WebinarUserRegistrationInfo entry : registeredUsers) {
                            if (attendanceMap.containsKey(entry.getRegisterEmail())) {
                                entry.updateAttendance(attendanceMap.get(entry.getRegisterEmail()));
                            } else {
                                entry.setTimeInSession(0);
                                entry.setAttendanceIntervals(new ArrayList<>());
                            }

                            UserBasicInfo userBasicInfo = userBasicInfoMap.get(entry.getUserId());
                            entry.addTag(WebinarUserTag.GTW_ATTENDENCE_FETCHED);
                            webinarUserRegistrationInfoDAO.create(entry);
                            awsSNSManager.triggerSNS(SNSTopic.CMS_WEBINAR_EVENTS, "WEBINAR_ATTENDED", gson.toJson(entry));
                            attendanceMap.remove(userBasicInfo.getEmail());
                        }
                        results.addAll(registeredUsers);
                    }

                    for (String userEmailId : attendanceMap.keySet()) {
                        List<GTWSessionAttendeeInfo> sessionAttendeeInfos = attendanceMap.get(userEmailId);
                        if (!CollectionUtils.isEmpty(sessionAttendeeInfos)) {
                            WebinarUserRegistrationInfo webinarUserRegistrationInfo = new WebinarUserRegistrationInfo();
                            webinarUserRegistrationInfo.setFirstName(sessionAttendeeInfos.get(0).getFirstName());
                            webinarUserRegistrationInfo.setLastName(sessionAttendeeInfos.get(0).getLastName());
                            webinarUserRegistrationInfo.setTrainingId(webinarInfo.getWebinarId());
                            webinarUserRegistrationInfo.updateAttendance(sessionAttendeeInfos);
                            webinarUserRegistrationInfo.addTag(WebinarUserTag.GTW_REGISTRATION_DONE);
                            webinarUserRegistrationInfo.addTag(WebinarUserTag.GTW_ATTENDENCE_FETCHED);
                            webinarUserRegistrationInfo.addTag(WebinarUserTag.OTHER_REGISTRATION_SOURCE);
                            webinarUserRegistrationInfoDAO.create(webinarUserRegistrationInfo);
                            results.add(webinarUserRegistrationInfo);
                        }
                    }

                } catch (Exception ex) {
                    logger.error("ProcessWebinarAttendanceError : " + ex.getMessage() + " ex:" + ex.toString()
                            + " webinarInfo:" + webinarInfo.toString());
                }
            }
        }
        return results;
    }

    public Map<String, List<GTWSessionAttendeeInfo>> fetchGTWAttendenceInfoMap(String webinarId, String emailId) {
        List<GTWSessionInfo> sessionInfos = fetchGTWSessions(webinarId, emailId);

        Map<String, List<GTWSessionAttendeeInfo>> attendenceMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(sessionInfos)) {
            for (GTWSessionInfo sessionInfo : sessionInfos) {
                List<GTWSessionAttendeeInfo> attendanceInfos = fetchGTWSessionAttendence(sessionInfo.getSessionKey(),
                        webinarId, emailId);
                if (!CollectionUtils.isEmpty(attendanceInfos)) {
                    for (GTWSessionAttendeeInfo sessionAttendeeInfo : attendanceInfos) {
                        List<GTWSessionAttendeeInfo> entry = new ArrayList<>();
                        if (attendenceMap.containsKey(sessionAttendeeInfo.getEmail())) {
                            entry = attendenceMap.get(sessionAttendeeInfo.getEmail());
                        }
                        entry.add(sessionAttendeeInfo);
                        attendenceMap.put(sessionAttendeeInfo.getEmail(), entry);
                    }
                }
            }
        }

        return attendenceMap;
    }

    public List<GTWSessionInfo> fetchGTWSessions(String webinarId, String emailId) {
        String serverUrl = getWebinarSessionsUrl(webinarId, emailId);
        ClientResponse webinarInfosResponse = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
                GTW_ACCESS_TOKENS.get(emailId));
        String jsonString = webinarInfosResponse.getEntity(String.class);
        logger.info("status:" + webinarInfosResponse.toString() + "response:" + jsonString);

        List<GTWSessionInfo> results = new ArrayList<>();
        if (webinarInfosResponse.getStatus() != 200) {
            return results;
        }

        return gson.fromJson(jsonString, WEBINAR_SESSIONS_TYPE);
    }

    public List<GTWSessionAttendeeInfo> fetchGTWSessionAttendence(String sessionId, String webinarId, String emailId) {
        String serverUrl = getWebinarSessionAttendanceUrl(sessionId, webinarId, emailId);
        ClientResponse webinarInfosResponse = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,
                GTW_ACCESS_TOKENS.get(emailId));
        String jsonString = webinarInfosResponse.getEntity(String.class);
        logger.info("status:" + webinarInfosResponse.toString() + "response:" + jsonString);

        List<GTWSessionAttendeeInfo> results = new ArrayList<>();
        if (webinarInfosResponse.getStatus() != 200) {
            return results;
        }

        return gson.fromJson(jsonString, WEBINAR_SESSION_ATTENDANCE_TYPE);
    }

    public List<WebinarInfo> getWebinarInfos(long beforeEndTime, long afterEndTime) throws ParseException {
        Query query = new Query();
        query.addCriteria(Criteria.where(WebinarInfo.Constants.END_TIME).lt(beforeEndTime).gte(afterEndTime));
        return webinarInfoDAO.runQuery(query, WebinarInfo.class);
    }



    private static String getRegisterWebinarUrl(String traningId) {
        // https://globalattspa.gotowebinar.com/api/webinars/150586194716221441/registrants?requireLicense=true&client=spa
        return "https://globalattspa.gotowebinar.com/api/webinars/" + traningId
                + "/registrants?requireLicense=true&client=spa";
    }

    private static String getWebinarInfoByIdUrl(String webinarId, String emailId) {
        // curl -X GET --header "Accept: application/json" --header "Authorization:
        // 6EDKuyO2v4yPrI35lkaKIjIMjYcW"
        // "https://api.getgo.com/G2W/rest/organizers/7470416618302296069/webinars/5935334575730303234"

        return "https://api.getgo.com/G2W/rest/organizers/" + GTW_ORGANIZER_IDS.get(emailId) + "/webinars/" + webinarId;
    }

    private static String getWebinarSessionAttendanceUrl(String sessionId, String webinarId, String emailId) {
        // curl -X GET --header "Accept: application/json" --header "Authorization:
        // 6EDKuyO2v4yPrI35lkaKIjIMjYcW"
        // "https://api.getgo.com/G2W/rest/organizers/7470416618302296069/webinars/7194267692821624065/sessions/10770772/attendees"

        return "https://api.getgo.com/G2W/rest/organizers/" + GTW_ORGANIZER_IDS.get(emailId) + "/webinars/" + webinarId
                + "/sessions/" + sessionId + "/attendees";
    }

    private static String getWebinarSessionsUrl(String webinarId, String emailId) {
        // curl -X GET --header "Accept: application/json" --header "Authorization:
        // 6EDKuyO2v4yPrI35lkaKIjIMjYcW"
        // "https://api.getgo.com/G2W/rest/organizers/7470416618302296069/webinars/7194267692821624065/sessions"

        return "https://api.getgo.com/G2W/rest/organizers/" + GTW_ORGANIZER_IDS.get(emailId) + "/webinars/" + webinarId
                + "/sessions";
    }

}
