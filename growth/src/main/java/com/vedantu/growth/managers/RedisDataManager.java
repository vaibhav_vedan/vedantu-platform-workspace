package com.vedantu.growth.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.enums.RedisPrefixType;
import com.vedantu.growth.requests.CachedContentRequest;
import com.vedantu.util.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RedisDataManager {

    @Autowired
    private RedisDAO redisDAO;

    public void setPrefix(RedisPrefixType type, String prefix) throws InternalServerErrorException {

        redisDAO.set(getPrefixKey(type), prefix);

    }
    public String getPrefix(RedisPrefixType type) throws InternalServerErrorException, BadRequestException {

        String prefix = redisDAO.get(getPrefixKey(type));
        prefix = StringUtils.isEmpty(prefix)? "" : prefix.trim();
        return prefix;

    }

    private String getPrefixKey(RedisPrefixType type){
        return "PREFIX_" + type;
    }

    public void setRedisData(CachedContentRequest cachedContentRequest) throws InternalServerErrorException {
        redisDAO.setex(cachedContentRequest.getKey(), cachedContentRequest.getData(), cachedContentRequest.getExpiry());
    }
    public String getRedisData(String key) throws InternalServerErrorException, BadRequestException {
        return redisDAO.get(key);
    }
}
