package com.vedantu.growth.requests;

import lombok.Data;

@Data
public class MasterTalkRegistrationReq {

    private String utmCampaign;
    private String utmMedium;
    private String utmSource;
    private String utmTerm;


}
