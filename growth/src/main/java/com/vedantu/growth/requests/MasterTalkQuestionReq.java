package com.vedantu.growth.requests;

import lombok.Data;

@Data
public class MasterTalkQuestionReq {

    private String masterTalkId;
    private Boolean isTestAttempted ;
    private String question;

}
