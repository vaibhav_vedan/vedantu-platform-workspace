package com.vedantu.growth.requests;

import com.vedantu.User.FeatureSource;
import com.vedantu.User.LocationInfo;
import com.vedantu.User.StudentInfo;
import com.vedantu.User.UTMParams;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class ReviseRegReq extends AbstractFrontEndReq {

    private Long userId;
    private String studentName;
    @Valid
    private StudentInfo studentInfo;
    @Valid
    private LocationInfo locationInfo;
    @Valid
    private UTMParams utm;
    private String event;
    private String targetExam;
    private FeatureSource featureSource;
    private FeatureSource signUpFeature;
    private String jeeApplicationNumber;

    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String contactNumber;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String phoneCode;
    @Size(max = ReqLimits.COMMENT_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String registerUrl;
    @Size(max = ReqLimits.NAME_TYPE_MAX, message = ReqLimitsErMsgs.MAX_NAME_TYPE)
    private String socialSource;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (studentInfo == null) {
            errors.add("studentInfo");
        }
        if (StringUtils.isEmpty(studentName)) {
            errors.add("studentName");
        }
        if (StringUtils.isEmpty(event)) {
            errors.add("event");
        }
        if (StringUtils.isEmpty(phoneCode)) {
            errors.add("phoneCode");
        }
        if (StringUtils.isEmpty(contactNumber)) {
            errors.add("contactNumber");
        }
        return errors;
    }
}
