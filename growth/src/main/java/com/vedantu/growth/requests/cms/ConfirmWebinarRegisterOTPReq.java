/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.requests.cms;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.List;

/**
 *
 * @author jeet
 */
public class ConfirmWebinarRegisterOTPReq extends AbstractFrontEndListReq{
    private String webinarId;
    private String otp;
    private String phone;

    public ConfirmWebinarRegisterOTPReq() {
    }

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(webinarId)) {
            errors.add("webinarId");
        }
        
        if (StringUtils.isEmpty(otp)) {
            errors.add("otp");
        }
        
        if (StringUtils.isEmpty(phone)) {
            errors.add("phone");
        }
       
        return errors;
    }
}
