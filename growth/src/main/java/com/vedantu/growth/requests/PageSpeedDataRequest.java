package com.vedantu.growth.requests;

import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.pageSpeed.PerformanceOpportunity;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.ArrayList;
import java.util.List;

public class PageSpeedDataRequest extends AbstractFrontEndReq {
    private String categoryId;
    private String categoryName;
    private String categoryPageId;
    private SeoDomain domain;
    private String url;
    private String performance;
    private String accessibility;
    private String bestPractices;
    private String seo;
    private String pwa;
    private List<PerformanceOpportunity> opportunityList;

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryPageId() {
        return categoryPageId;
    }

    public void setCategoryPageId(String categoryPageId) {
        this.categoryPageId = categoryPageId;
    }

    public SeoDomain getDomain() {
        return domain;
    }

    public void setDomain(SeoDomain domain) {
        this.domain = domain;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public String getAccessibility() {
        return accessibility;
    }

    public void setAccessibility(String accessibility) {
        this.accessibility = accessibility;
    }

    public String getBestPractices() {
        return bestPractices;
    }

    public void setBestPractices(String bestPractices) {
        this.bestPractices = bestPractices;
    }

    public String getSeo() {
        return seo;
    }

    public void setSeo(String seo) {
        this.seo = seo;
    }

    public String getPwa() {
        return pwa;
    }

    public void setPwa(String pwa) {
        this.pwa = pwa;
    }

    public List<PerformanceOpportunity> getOpportunityList() {
        return opportunityList;
    }

    public void setOpportunityList(List<PerformanceOpportunity> opportunityList) {
        this.opportunityList = opportunityList;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();

        if(StringUtils.isEmpty(this.getUrl())){
            errors.add("url should not be empty");
        }

        if(StringUtils.isEmpty(this.getCategoryPageId())){
            errors.add("categoryPageId should not be empty");
        }

        return errors;
    }
}
