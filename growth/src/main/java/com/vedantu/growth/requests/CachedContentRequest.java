package com.vedantu.growth.requests;

import lombok.Data;

@Data
public class CachedContentRequest {
    private String key;
    private Integer expiry;
    private String data;
}
