package com.vedantu.growth.requests;

import com.vedantu.growth.pojo.cms.WebinarUserRegistrationInfo;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;


public class RegisterWebinarUserReq {

	private String firstName;
	private String lastName;
	private String email;
	private String phone;
	private String timeZone = "Asia/Kolkata";
	private List<String> responses = new ArrayList<>();

	public RegisterWebinarUserReq() {
		super();
	}

	public RegisterWebinarUserReq(WebinarUserRegistrationInfo webinarUserRegistrationInfo) {
		super();
		this.firstName = webinarUserRegistrationInfo.getFirstName();
		this.lastName = !StringUtils.isEmpty(webinarUserRegistrationInfo.getLastName())
				? webinarUserRegistrationInfo.getLastName()
				: "Attendee";
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getTimeZone() {
		return timeZone;
	}

	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	public List<String> getResponses() {
		return responses;
	}

	public void setResponses(List<String> responses) {
		this.responses = responses;
	}

	@Override
	public String toString() {
		return "RegisterWebinarUserReq [firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", timeZone=" + timeZone + ", responses=" + responses + ", toString()=" + super.toString() + "]";
	}
}
