package com.vedantu.growth.requests;

import lombok.Data;

@Data
public class MasterTalkFeedbackReq {

    private String masterTalkId;
    private Integer rating ;
    private String feedback;
    private Long userId;

}
