/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.requests.cms;

import com.vedantu.growth.enums.cms.WebinarStatus;
import com.vedantu.growth.enums.cms.WebinarToolType;
import com.vedantu.growth.enums.cms.WebinarType;
import com.vedantu.growth.pojo.cms.LeaderBoard;
import com.vedantu.growth.pojo.cms.Testimonial;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.scheduling.CommonCalendarUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author jeet
 */
public class CreateWebinarReq extends AbstractFrontEndReq {

    private String webinarCode;
    private List<String> grades;
    private List<String> targets;
    private List<String> tags;
    private List<String> boards;
    private String replayUrl;
    private Long startTime;
    private Long duration;
    private Long endTime;
    private WebinarToolType toolType;
    private Map<String, Object> webinarInfo;
    private String teacherEmail;
    private Map<String, Object> teacherInfo;
    private Map<String, Object> courseInfo;
    private Map<String, Object> registerSectionInfo;
    private String gtwSeat;
    private String gtwWebinarId;
    private String title;
    private String emailImageUrl;
    private Boolean showInPastWebinars = Boolean.FALSE;
    private WebinarStatus webinarStatus = WebinarStatus.INACTIVE;
    private List<LeaderBoard> leaderBoard;
    private WebinarType type;
    private Testimonial testimonial;
    private Set<String> subjects;
    private Long presenter;
    private Set<Long> taIds;
    private Boolean isSimLive;
    private String parentWebinarId;
    private List<String> contextTags;

    public CreateWebinarReq() {
    }

    public String getWebinarCode() {
        return webinarCode;
    }

    public void setWebinarCode(String webinarCode) {
        this.webinarCode = webinarCode;
    }

    public List<String> getGrades() {
        return grades;
    }

    public void setGrades(List<String> grades) {
        this.grades = grades;
    }

    public List<String> getTargets() {
        return targets;
    }

    public void setTargets(List<String> targets) {
        this.targets = targets;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getBoards() {
        return boards;
    }

    public void setBoards(List<String> boards) {
        this.boards = boards;
    }

    public String getReplayUrl() {
        return replayUrl;
    }

    public void setReplayUrl(String replayUrl) {
        this.replayUrl = replayUrl;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public WebinarToolType getToolType() {
        return toolType;
    }

    public void setToolType(WebinarToolType toolType) {
        this.toolType = toolType;
    }

    public Map<String, Object> getWebinarInfo() {
        return webinarInfo;
    }

    public void setWebinarInfo(Map<String, Object> webinarInfo) {
        this.webinarInfo = webinarInfo;
    }

    public String getTeacherEmail() {
        return teacherEmail;
    }

    public void setTeacherEmail(String teacherEmail) {
        this.teacherEmail = teacherEmail;
    }

    public Map<String, Object> getTeacherInfo() {
        return teacherInfo;
    }

    public void setTeacherInfo(Map<String, Object> teacherInfo) {
        this.teacherInfo = teacherInfo;
    }

    public Map<String, Object> getCourseInfo() {
        return courseInfo;
    }

    public void setCourseInfo(Map<String, Object> courseInfo) {
        this.courseInfo = courseInfo;
    }

    public Map<String, Object> getRegisterSectionInfo() {
        return registerSectionInfo;
    }

    public void setRegisterSectionInfo(Map<String, Object> registerSectionInfo) {
        this.registerSectionInfo = registerSectionInfo;
    }

    public String getGtwSeat() {
        return gtwSeat;
    }

    public void setGtwSeat(String gtwSeat) {
        this.gtwSeat = gtwSeat;
    }

    public String getGtwWebinarId() {
        return gtwWebinarId;
    }

    public void setGtwWebinarId(String gtwWebinarId) {
        this.gtwWebinarId = gtwWebinarId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getEmailImageUrl() {
        return emailImageUrl;
    }

    public void setEmailImageUrl(String emailImageUrl) {
        this.emailImageUrl = emailImageUrl;
    }

    public WebinarStatus getWebinarStatus() {
        return webinarStatus;
    }

    public void setWebinarStatus(WebinarStatus webinarStatus) {
        this.webinarStatus = webinarStatus;
    }

    public Boolean getShowInPastWebinars() {
        return showInPastWebinars;
    }

    public void setShowInPastWebinars(Boolean showInPastWebinars) {
        this.showInPastWebinars = showInPastWebinars;
    }

    public List<LeaderBoard> getLeaderBoard() {
        return leaderBoard;
    }

    public void setLeaderBoard(List<LeaderBoard> leaderBoard) {
        this.leaderBoard = leaderBoard;
    }

    public WebinarType getType() {
        return type;
    }

    public void setType(WebinarType type) {
        this.type = type;
    }

    public Boolean getIsSimLive() {
        return isSimLive;
    }

    public void setIsSimLive(Boolean isSimLive) {
        this.isSimLive = isSimLive;
    }

    public String getParentWebinarId() {
        return parentWebinarId;
    }

    public void setParentWebinarId(String parentWebinarId) {
        this.parentWebinarId = parentWebinarId;
    }

    public List<String> getContextTags() {
        return contextTags;
    }

    public void setContextTags(List<String> contextTags) {
        this.contextTags = contextTags;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(webinarCode)) {
            errors.add("webinarCode");
        }

        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }

        if (grades == null || grades.isEmpty()) {
            errors.add("grades");
        }

        if (startTime == null) {
            errors.add("startTime");
        }

        if (duration == null) {
            errors.add("duration");
        }

        if (endTime - startTime > 5 * CommonCalendarUtils.MILLIS_PER_HOUR) {
            errors.add("duration of a session can not be more than 5 hours");
        }

        if (WebinarType.QUIZ.equals(type) || WebinarType.LIVECLASS.equals(type)) {
            // Quiz validations
        } else {
            if (targets == null || targets.isEmpty()) {
                errors.add("targets");
            }

            if (boards == null || boards.isEmpty()) {
                errors.add("boards");
            }

            if (toolType == null) {
                errors.add("toolType");
            }

            if (StringUtils.isEmpty(teacherEmail)) {
                errors.add("teacherEmail");
            }

            if (null != toolType) {
                switch (toolType) {
                    case GTW:
                        if (StringUtils.isEmpty(gtwWebinarId)) {
                            errors.add("gtwWebinarId");
                        }
                        if (StringUtils.isEmpty(gtwSeat)) {
                            errors.add("gtwSeat");
                        }
                        break;
                    case YOUTUBE:
                        if (StringUtils.isEmpty(gtwWebinarId)) {
                            errors.add("gtwWebinarId");
                        }
                        break;
                    case VEDANTU_WAVE:
                    case VEDANTU_WAVE_BIG_WHITEBOARD:
                    case VEDANTU_WAVE_OTO:
                        if (presenter == null) {
                            errors.add("presenter");
                        }
                        if (startTime == null) {
                            errors.add("startTime");
                        }
                        if (endTime == null) {
                            errors.add("endTime");
                        }
                        break;
                    default:
                        break;
                }
            }

            if (StringUtils.isEmpty(emailImageUrl)) {
                errors.add("emailImageUrl");
            }

            if (isSimLive != null && isSimLive && StringUtils.isEmpty(parentWebinarId)) {
                errors.add("parentWebinarId");
            }
        }

        return errors;
    }

    /**
     * @return the subjects
     */
    public Set<String> getSubjects() {
        return subjects;
    }

    /**
     * @param subjects the subjects to set
     */
    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    public Long getPresenter() {
        return presenter;
    }

    public void setPresenter(Long presenter) {
        this.presenter = presenter;
    }

    public Set<Long> getTaIds() {
        return taIds;
    }

    public void setTaIds(Set<Long> taIds) {
        this.taIds = taIds;
    }

	public Testimonial getTestimonial() {
		return testimonial;
	}

	public void setTestimonial(Testimonial testimonial) {
		this.testimonial = testimonial;
	}

}
