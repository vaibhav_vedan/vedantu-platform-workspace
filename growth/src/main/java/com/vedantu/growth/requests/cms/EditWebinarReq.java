/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.requests.cms;

import java.util.List;

/**
 *
 * @author jeet
 */
public class EditWebinarReq extends CreateWebinarReq {

    private String webinarId;

    public String getWebinarId() {
        return webinarId;
    }

    public void setWebinarId(String webinarId) {
        this.webinarId = webinarId;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (null == webinarId) {
            errors.add("webinarId");
        }
        return errors;
    }
}
