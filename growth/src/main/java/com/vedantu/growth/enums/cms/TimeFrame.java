package com.vedantu.growth.enums.cms;

public enum TimeFrame {
    PAST, UPCOMING
}