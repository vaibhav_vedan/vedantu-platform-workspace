/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.enums.cms;

/**
 *
 * @author jeet
 */
public enum WebinarUserTag {
    GTW_REGISTRATION_DONE, GTW_ATTENDENCE_FETCHED, LS_REGISTER_ACTIVITY_PUSHED, LS_ATTENDENCE_ACTIVITY_PUSHED, OTHER_REGISTRATION_SOURCE
}

