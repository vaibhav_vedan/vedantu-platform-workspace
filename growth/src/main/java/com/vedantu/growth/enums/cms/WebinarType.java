package com.vedantu.growth.enums.cms;

public enum WebinarType {
    LIVECLASS, QUIZ, WEBINAR
}
