/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.seo;

import com.vedantu.exception.ForbiddenException;
import com.vedantu.growth.seo.dao.SeoTutorPageDAO;
import com.vedantu.growth.seo.entity.SeoTutorListPage;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *
 * @author ajith
 */
@Service
public class SeoTutorListPageManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoTutorListPageManager.class);

    @Autowired
    private SeoTutorPageDAO seoTutorPageDAO;

    public SeoTutorListPage addEditPage(SeoTutorListPage reqPojo) throws ForbiddenException {
        return seoTutorPageDAO.save(reqPojo);
    }

    public List<SeoTutorListPage> getList(Integer start, Integer size) {
        return seoTutorPageDAO.getList(start, size);
    }

    public SeoTutorListPage getById(String id) {
        return seoTutorPageDAO.getById(id);
    }

    public SeoTutorListPage getByPath(String path) {
        return seoTutorPageDAO.getByPath(path);
    }

}
