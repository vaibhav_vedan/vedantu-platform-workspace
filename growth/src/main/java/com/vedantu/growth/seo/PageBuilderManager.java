package com.vedantu.growth.seo;

import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.forms.PdfPageFormCopier;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.redfin.sitemapgenerator.ChangeFreq;
import com.redfin.sitemapgenerator.WebSitemapGenerator;
import com.redfin.sitemapgenerator.WebSitemapUrl;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.AbstractInfo;
import com.vedantu.User.AbstractInfoAdapter;
import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.cmds.pojo.SeoQuestion;
import com.vedantu.cmds.pojo.SolutionFormat;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.growth.aws.AwsCloudFrontManager;
import com.vedantu.growth.aws.AwsS3SEOManager;
import com.vedantu.growth.aws.AwsSQSManager;
import com.vedantu.growth.aws.AwslambdaManager;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.lms.AddEditQuestionReq;
import com.vedantu.growth.notifications.CommunicationManager;
import com.vedantu.growth.pojo.CategoryPageTitleUrlMapping;
import com.vedantu.growth.seo.dao.BlogArticleDao;
import com.vedantu.growth.seo.dao.BlogCategoryDao;
import com.vedantu.growth.seo.dao.CategoryDAO;
import com.vedantu.growth.seo.dao.CategoryPageDAO;
import com.vedantu.growth.seo.dao.CategoryPageHistoryDAO;
import com.vedantu.growth.seo.entity.AppendPdfRuleSet;
import com.vedantu.growth.seo.entity.BlogArticle;
import com.vedantu.growth.seo.entity.BlogCategory;
import com.vedantu.growth.seo.entity.Category;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.CategoryPageContentModule;
import com.vedantu.growth.seo.entity.CategoryPageFooter;
import com.vedantu.growth.seo.entity.CategoryPageHeader;
import com.vedantu.growth.seo.entity.CategoryPageHistory;
import com.vedantu.growth.seo.entity.CategoryPageListModule;
import com.vedantu.growth.seo.entity.CategoryPageRelatedQuestion;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.enums.BlogPageType;
import com.vedantu.growth.seo.enums.MasterSidebarUploadOptions;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.pojo.AppendPdfPageLocationRule;
import com.vedantu.growth.seo.pojo.BlogArticlePojo;
import com.vedantu.growth.seo.response.BlogCategoryResponse;
import com.vedantu.growth.seo.response.BlogHomePageResponse;
import com.vedantu.growth.seo.response.BlogMetadataPojo;
import com.vedantu.growth.seo.response.CategoryPageResponse;
import com.vedantu.growth.seo.response.CategoryPageVoteStatus;
import com.vedantu.lms.cmds.enums.QuestionType;
import com.vedantu.platform.seo.dto.CMDSQuestionDto;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class PageBuilderManager {

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private CategoryPageDAO categoryPageDAO;

    @Autowired
    private CategoryPageHistoryDAO categoryPageHistoryDAO;

    @Autowired
    private AwsS3SEOManager awsS3Manager;

    @Autowired
    private AwsCloudFrontManager cloudFrontManager;

    @Autowired
    private AwslambdaManager awslambdaManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private BlogCategoryDao blogCategoryDao;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private BlogArticleDao blogArticleDao;

    @Autowired
    private SeoRssFeedManager seoRssFeedManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private CategoryPageVoteManager categoryPageVoteManager;

    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");
    private static final String SEO_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");
    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase();
    private static final Gson GSON = new Gson();
    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

    public static final String CATEGORY_NAME = "categoryName";
    public static final String CATEGORY_PAGE_ID = "categoryPageId";
    public static final String CATEGORY_PAGE = "categoryPage";
    public static final String BLOG_CATEGORY_REDIS_KEY = "BLOG_CATEGORY";

    private static final String BLOG_HOME_PAGE_RESPONSE_REDIS_KEY_PREFIX = "BLOG_HOME_PAGE_RESPONSE_";

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PageBuilderManager.class);

    /*
     * Category
     */
    public Category getCategoryById(String id) {
        return categoryDAO.getCategoryById(id);
    }

    public Category getCategoryByName(String name, SeoDomain domain) {
        return categoryDAO.getCategoryByName(name, domain);
    }

    public List<Category> getAllCategories(SeoDomain domain) {
        return categoryDAO.getAllCategories(domain);
    }

    public Category createCategory(Category category) {

        return categoryDAO.updateCategory(category);

    }

    public Boolean deleteCategory(String categoryId) {
        return categoryDAO.deleteCategory(categoryId);
    }

    public String getSitemap(String name, SeoDomain domain) throws MalformedURLException, ParseException {
        String[] nameParts = name.split("-");
        nameParts = Arrays.copyOf(nameParts, nameParts.length - 1);
        String str = Arrays.toString(nameParts);
        Category category = categoryDAO.getCategoryByName(str.substring(1, str.length() - 1).replace(",", ""), domain);
        Integer start = 0;
        Integer size = 1000;
        List<CategoryPage> list = new ArrayList<CategoryPage>();
        while (true) {
            List<CategoryPage> dummyList = categoryPageDAO.findCategoryPages(domain, category.getId(), null, null, null, null, false, start, size);
            list.addAll(dummyList);
            start = start + size;
            if (size != dummyList.size()) {
                break;
            }
        }
        logger.info("GETSITEMAP size : " + list.size());

        Date today = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(today);
        int month = cal.get(Calendar.MONTH) + 1;
        int year = cal.get(Calendar.YEAR);
        String d = year + "-" + month + "-01";

        DateFormat formatter = new SimpleDateFormat("yyyy-M-dd");
        Date date = formatter.parse(d);
        logger.info(date);

        String BASE_URL = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

        WebSitemapGenerator sitemap = new WebSitemapGenerator(BASE_URL);

        // main category url
        WebSitemapUrl murl = new WebSitemapUrl.Options(BASE_URL + "/" + str.substring(1, str.length() - 1).replace(", ", "-") + "/")
                .lastMod(date).priority(0.9).changeFreq(ChangeFreq.MONTHLY).build();
        sitemap.addUrl(murl);

        for (CategoryPage c : list) {
            Double priority = c.getPriority();
            if (priority == null) {
                priority = 0.8;
            }
            String frequency = c.getFrequency();
            ChangeFreq f = null;
            if (StringUtils.isEmpty(frequency)) {
                f = ChangeFreq.MONTHLY;
            } else if (frequency.equals("weekly")) {
                f = ChangeFreq.WEEKLY;
            } else if (frequency.equals("hourly")) {
                f = ChangeFreq.HOURLY;
            } else if (frequency.equals("yearly")) {
                f = ChangeFreq.YEARLY;
            } else if (frequency.equals("daily")) {
                f = ChangeFreq.DAILY;
            } else {
                f = ChangeFreq.MONTHLY;
            }
            // category page url
            WebSitemapUrl url = new WebSitemapUrl.Options(BASE_URL + c.getUrl())
                    .lastMod(date).priority(priority).changeFreq(f).build();
            sitemap.addUrl(url);
        }
        return String.join("", sitemap.writeAsStrings());
    }

    /*
     * Category Page
     */
    public CategoryPage getCategoryPageById(String categoryPageId) throws VException {

        CategoryPage categoryPage = categoryPageDAO.getCategoryPageById(categoryPageId);
        if (categoryPage == null) {
            throw new VException(ErrorCode.SERVICE_ERROR, "Category Page Not Found");
        }
        String questionId = categoryPage.getQuestionId();

        if (questionId != null) {

            SeoQuestion response = getSeoQuestion(questionId);
            if (response != null) {
                logger.info("GETTING Question");
                logger.info(response.toString());
                categoryPage.setQuestionBody(response.getQuestionBody());

                if(StringUtils.isEmpty(categoryPage.getTopic()) && StringUtils.isNotEmpty(response.getTopic())){
                    categoryPage.setTopic(response.getTopic());
                }

                categoryPage.setSQuestion(response.getQuestionBody().getNewText());
                categoryPage.setSolutions(response.getSolutions());
                categoryPage.setDifficulty(response.getDifficulty());
                List<SolutionFormat> list = response.getSolutions();
                List<String> sList = new ArrayList<String>();
                for (SolutionFormat s : list) {
                    if (s != null) {
                        sList.add(s.getNewText());
                    }
                }
                categoryPage.setSSolutions(sList);
            }

            categoryPage.setIsVerified(true);
        }

        setCategoryName(categoryPage);

        sortContentModules(categoryPage);

        updateAwsContentFileUrl(categoryPage);
        return categoryPage;

    }

    public void setCategoryName(CategoryPage categoryPage) {

        if(CategoryPageType.BlogPage.equals(categoryPage.getPageType())){
            String blogCategoryId = categoryPage.getBlogCategoryId();

            if(StringUtils.isEmpty(blogCategoryId))
                return;

            BlogCategory blogCategory = blogCategoryDao.getBlogCategoryById(blogCategoryId);

            if(blogCategory != null)
                categoryPage.setBlogCategoryName(blogCategory.getName());
        }else if(CategoryPageType.NewsPage.equals(categoryPage.getPageType())){
            categoryPage.setBlogCategoryName(categoryPage.getTarget());
        }
    }

    public CategoryPage getCategoryPageByQuestionId(String questionId, SeoDomain domain, boolean getActivePages) {
        List<CategoryPage> categoryPages = categoryPageDAO.getCategoryPageByQuestionIds(Collections.singleton(questionId), domain, getActivePages);
        if (ArrayUtils.isNotEmpty(categoryPages)) {
            return categoryPages.get(0);
        }
        return new CategoryPage();
    }

    public void setCategoryPageRelatedLinks(CategoryPage categoryPage) throws VException {
        try {
            Set<String> relatedPageIds = categoryPage.getRelatedPageIds();
            if (ArrayUtils.isEmpty(relatedPageIds)) {
                return;
            }
            List<CategoryPage> categoryPageByIds = categoryPageDAO.getCategoryPageByIds(relatedPageIds);
            List<CategoryPage> categoryPageListOrdered = orderCategoryPageLinks(categoryPageByIds, relatedPageIds);
            LinkedHashSet<CategoryPageRelatedQuestion> links = categoryPageListOrdered.stream()
                    .filter(e -> Objects.nonNull(e) && Objects.nonNull(e.getUrl()) && Objects.nonNull(e.getName()))
                    .filter(e -> Objects.nonNull(e.getDisabled()) && !e.getDisabled())
                    .filter(e -> Objects.nonNull(e.getActive()) && e.getActive())
                    .map(e -> new CategoryPageRelatedQuestion(e.getName(), e.getUrl()))
                    .collect(Collectors.toCollection(LinkedHashSet::new));
            categoryPage.setRelatedPageLinks(links);
        } catch (Exception e) {
            throw new VException(ErrorCode.SERVICE_ERROR, "Unable to set relatedpage links");
        }
    }

    private List<CategoryPage> orderCategoryPageLinks(List<CategoryPage> categoryPageByIds, Set<String> relatedQuestionsIds) {
        ArrayList<String> ids = new ArrayList<>(relatedQuestionsIds);
        categoryPageByIds.sort(Comparator.comparing(page -> {
            int i = ids.indexOf(page.getId());
            if (i == -1) {
                return 1;
            } else {
                return i;
            }
        }));
        return categoryPageByIds;
    }

    public CategoryPage createOrUpdateCategoryPage(CategoryPage categoryPage) throws VException {

        SeoDomain domain = categoryPage.getDomain();

        if (categoryPage.getPageType().equals(CategoryPageType.QnA) && SeoDomain.VEDANTU.equals(domain)) { // QnA is only for vedantu

            AddEditQuestionReq request = new AddEditQuestionReq();
            request.setQuestionId(categoryPage.getQuestionId());
            request.setSolutions(categoryPage.getSolutions());
            request.setQuestionBody(categoryPage.getQuestionBody());
            request.setDifficulty(categoryPage.getDifficulty());
            request.setQuestionType(QuestionType.PARA);

            // Add question
            ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/question/addEditQuestionSeo",
                    HttpMethod.POST, new Gson().toJson(request), true);

            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            categoryPage.setSolutions(null);
            categoryPage.setQuestionBody(null);
            categoryPage.setDifficulty(null);
            categoryPage.setQuestionId(jsonString);
            if (StringUtils.isNotEmpty(categoryPage.getUrl()) && StringUtils.isNotEmpty(jsonString) && !categoryPage.getUrl().contains(jsonString)) {
                categoryPage.setUrl(categoryPage.getUrl() + "-" + jsonString);
            }

            // Save categoryPage without Question
            categoryPageDAO.updateCategoryPage(categoryPage);

            //Clear topic list cache
            String redsKeyTopics = SeoQuestionAndAnswerManager.getQnAFilterOptionsFromRedissKey();
            redisDAO.del(redsKeyTopics);

            // Get Question with formatted html

            getSeoQnAWithFormattedHTML(categoryPage);

            categoryPage.setIsVerified(null);

        } else {
            // Check if there's already a page present with this url -- Reason: To remove PageType3 pdf save path duplicates
            if (StringUtils.isEmpty(categoryPage.getId())) {
                List<CategoryPage> categoryPages = categoryPageDAO.findCategoryPages(domain, null, categoryPage.getUrl(), null, null, null, null, null, null);
                if (ArrayUtils.isNotEmpty(categoryPages)) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Already a page present with this url");
                }
            } else {
                // Previous Page Urls required for 301 redirection
                CategoryPage page = categoryPageDAO.getCategoryPageById(categoryPage.getId());
                Set<String> previousUrls = page.getPreviousUrls();
                previousUrls = previousUrls == null ? new HashSet<>() : previousUrls;
                previousUrls.add(page.getUrl());
                categoryPage.setPreviousUrls(previousUrls);
            }

            if(CategoryPageType.BlogPage.equals(categoryPage.getPageType()) || CategoryPageType.NewsPage.equals(categoryPage.getPageType())){

                if(categoryPage.getBlogPublishedDate() == null)
                    categoryPage.setBlogPublishedDate(System.currentTimeMillis());

                //CategoryName will be updated while returning the record
                categoryPage.setBlogCategoryName(null);
            }

            categoryPageDAO.updateCategoryPage(categoryPage);
            if (!SeoDomain.VEDANTU.equals(categoryPage.getDomain())) {
                uploadStaticHtmlToS3(categoryPage);
                invalidateCache(categoryPage);
            }

            if(CategoryPageType.NewsPage.equals(categoryPage.getPageType())){
                String newsHomePageResponseRedisKey = SeoNewsPageManager.NEWS_HOME_PAGE_RESPONSE_REDIS_KEY_PREFIX + domain;
                redisDAO.del(newsHomePageResponseRedisKey);
            }
        }

        updateAwsContentFileUrl(categoryPage);

        if(CategoryPageType.BlogPage.equals(categoryPage.getPageType())
                || CategoryPageType.NewsPage.equals(categoryPage.getPageType()))
            seoRssFeedManager.upsertRssItem(categoryPage.getId());

        pushCategoryPageToES(categoryPage);
        updateCategoryPageEditHistoryAsync(categoryPage, sessionUtils.getCallingUserId());
        return categoryPage;
    }

    private void updateCategoryPageEditHistoryAsync(CategoryPage categoryPage, Long userId) {
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(() -> {
            if (StringUtils.isNotEmpty(categoryPage.getId())) {
                logger.info("Creating CategoryPage edit history asynchronously for pageId: {}", categoryPage.getId());
                CategoryPageHistory categoryPageHistory = new CategoryPageHistory();
                categoryPageHistory.setCategoryPageInfo(categoryPage);
                categoryPageHistory.setCategoryPageId(categoryPage.getId());

                String callingUserId = (userId == null) ? null : userId.toString();
                categoryPageHistoryDAO.saveCategoryPageHistory(categoryPageHistory, callingUserId);
                int categoryPageEditLimit = ConfigUtils.INSTANCE.getIntValue("category.page.edit.limit", 5);
                categoryPageHistoryDAO.getCategoryPageHistory(categoryPage.getId(), Sort.Direction.DESC, categoryPageEditLimit)
                        .forEach(categoryPageHistoryInfo -> categoryPageHistoryDAO
                                .deleteEntityById(categoryPageHistoryInfo.getId(), CategoryPageHistory.class)
                        );
            }
        });
    }

    private void getSeoQnAWithFormattedHTML(CategoryPage categoryPage) throws VException {

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        SeoQuestion response = getSeoQuestion(categoryPage.getQuestionId());
        if (response != null) {
            categoryPage.setQuestionBody(response.getQuestionBody());
            categoryPage.setSQuestion(response.getQuestionBody().getNewText());
            categoryPage.setSolutions(response.getSolutions());
            List<SolutionFormat> list = response.getSolutions();
            List<String> sList = new ArrayList<String>();
            for (SolutionFormat s : list) {
                if (s != null) {
                    sList.add(s.getNewText());
                }
            }
            categoryPage.setSSolutions(sList);
            categoryPage.setDifficulty(response.getDifficulty());
        }
    }

    private void pushCategoryPageToES(CategoryPage categoryPage) {

        try {
            Map<String, Object> objectMap = new HashMap<>();
            objectMap.put(CATEGORY_PAGE_ID, categoryPage.getId());

            awsSQSManager.sendToSQS(SQSQueue.SEO_QUEUE,
                    SQSMessageType.SEO_CATEGORY_PAGE_UPDATED, new Gson().toJson(objectMap), SQSMessageType.SEO_CATEGORY_PAGE_UPDATED.name());
        }catch (Exception e){
            logger.error("Error pushing to queue: SEO_QUEUE", e);
        }

    }

    public Boolean deleteCategoryPage(String categoryPageId) throws UnsupportedEncodingException, VException {
        Map<String, Object> pageDeletedResponse = categoryPageDAO.deleteCategoryPage(categoryPageId);
        if (pageDeletedResponse.containsKey("deleted")) {
            boolean pageDeleted = (boolean) pageDeletedResponse.get("deleted");
            if (pageDeleted) {
                String url = (String) pageDeletedResponse.get("url");
                communicationManager.sendMailToNotifyPageDeleted(url);
                CategoryPage categoryPage = ((CategoryPage) pageDeletedResponse.get("page"));
                SeoDomain domain = categoryPage.getDomain();
                List<CategoryPageContentModule> images = categoryPage.getContentModules();
                List<String> pdf = categoryPage.getDownloadableLinks();

                if (domain != null && !SeoDomain.VEDANTU.equals(domain)) {
                    String prefix = getAwsKeyPrefixForDomain(domain);
                    awsS3Manager.deleteFile(domain.getAwsBucket(), prefix + url);
                    // Removed delete option for pdf and images because of cloned page will have same download link as the parent page
                    /*if (images != null && images.size() > 0) {
                        for (CategoryPageContentModule image : images) {
                            String imageUrl = image.getImageUrl();
                            try {
                                URL uri = new URL(imageUrl);
                                awsS3Manager.deleteFile(domain.getAwsBucket(), uri.getPath().replaceFirst("/", ""));
                            } catch (MalformedURLException ignored) {
                            }
                        }
                    }
                    if (pdf != null && pdf.size() > 0) {
                        for (String link : pdf) {
                            try {
                                URL uri = new URL(link);
                                awsS3Manager.deleteFile(domain.getAwsBucket(), uri.getPath().replaceFirst("/", ""));
                            } catch (MalformedURLException ignored) {
                            }
                        }
                    }*/

                    // Invalidate cache in cloudfront
                    invalidateCache(categoryPage);
                }
            }

            return pageDeleted;
        } else {
            return false;
        }
    }

    private String getAwsKeyPrefixForDomain(SeoDomain domain) {
        switch (domain) {
            case ACADSTACK:
            case VEDANTU:
                return ENV;
            default:
                throw new RuntimeException("cant determine microsite prefix ");
        }
    }

    public List<CategoryPage> getCategoryPages(SeoDomain domain, String categoryId, String categoryPageUrl, String categoryPageType,
                                               String categoryTarget, String categoryGrade) throws VException {
        return getCategoryPages(domain, categoryId, categoryPageUrl, categoryPageType, categoryTarget,
                categoryGrade, null, null, null);
    }

    public List<CategoryPage> getCategoryPages(SeoDomain domain, String categoryId, String categoryPageUrl, String categoryPageType,
                                               String categoryTarget, String categoryGrade, Boolean disabled, Integer start, Integer size) throws VException {

        List<CategoryPage> list = categoryPageDAO.findCategoryPages(domain, categoryId, categoryPageUrl, categoryPageType, categoryTarget,
                categoryGrade, disabled, start, size);

        List<String> questionIds = list.stream().filter(page -> Objects.nonNull(page.getQuestionId()))
                .map(page -> page.getQuestionId())
                .collect(Collectors.toList());

        List<SeoQuestion> seoQuestionList = getSeoQuestions(questionIds);
        Map<String, List<SeoQuestion>> mapData = seoQuestionList.stream().collect(Collectors.groupingBy(SeoQuestion::getId));

        for (CategoryPage page : list) {
            String questionId = page.getQuestionId();

            if (questionId != null && mapData.containsKey(questionId)) {

                SeoQuestion response = mapData.get(questionId).get(0);

                if (response != null) {

                    if (null != response.getQuestionBody()) {
                        page.setQuestionBody(response.getQuestionBody());
                        if (null != response.getQuestionBody().getNewText()) {
                            page.setSQuestion(response.getQuestionBody().getNewText());
                        }
                    }
                    if (null != response.getSolutions()) {
                        page.setSolutions(response.getSolutions());
                        List<SolutionFormat> clist = response.getSolutions();
                        List<String> sList = new ArrayList<String>();
                        for (SolutionFormat s : clist) {
                            if (s != null) {
                                sList.add(s.getNewText());
                            }
                        }
                        page.setSSolutions(sList);
                    }

                    if(StringUtils.isEmpty(page.getTopic()) && StringUtils.isNotEmpty(response.getTopic())){
                        page.setTopic(response.getTopic());
                    }

                }

                page.setIsVerified(true);
                CategoryPageVoteStatus voteStatus =categoryPageVoteManager.getTotalVotes(page.getId());
                page.setTotalVotes(voteStatus.getTotalVotes());

                if(StringUtils.isNotEmpty(page.getSubject())){
                    String subject = StringUtils.capitalize(page.getSubject().toLowerCase());
                    page.setSubject(subject);
                }

                if(StringUtils.isNotEmpty(page.getTarget()) && page.getTarget().contains("_"))
                    page.setTarget(page.getTarget().replaceAll("_","-"));

            }

            updateAwsContentFileUrl(page);
            setCategoryName(page);

            List<CategoryPageContentModule> contentModules = page.getContentModules();

            if(CollectionUtils.isNotEmpty(contentModules)){
                for (CategoryPageContentModule contentModule : contentModules){
                    if(StringUtils.isEmpty(contentModule.getImageUrlWebp())){
                        contentModule.setImageUrlWebp(contentModule.getImageUrl());
                    }
                }
            }

            sortContentModules(page);

            if(page.getPageType().equals(CategoryPageType.BlogPage)){
                setRelatedContents(page);

                if(StringUtils.isEmpty(page.getFeatureImageTag())){
                    page.setFeatureImageTag(page.getTitle());
                }

            }
        }
        return list;
    }

    private void sortContentModules(CategoryPage page) {

        try {

            List<CategoryPageContentModule> contentModules = page.getContentModules();

            Collections.sort(contentModules, new Comparator<CategoryPageContentModule>() {
                public int compare(CategoryPageContentModule o1, CategoryPageContentModule o2) {

                    String url1 = o1.getImageUrl();
                    String url2 = o2.getImageUrl();

                    String fileName1 = url1.substring(url1.lastIndexOf("/") + 1,url1.lastIndexOf("."));
                    String fileName2 = url2.substring(url2.lastIndexOf("/") + 1,url2.lastIndexOf("."));

                    return Long.compare(Long.parseLong(fileName1), Long.parseLong(fileName2));
                }
            });

        }catch (Exception e){
            //Ignore on failure
        }

    }

    /*public SeoQuestion getSeoQuestion(String questionId) throws VException {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(AbstractInfo.class, new AbstractInfoAdapter());
        Gson _gson = gsonBuilder.create();

        String jsonQnA = redisDAO.get("ques:" + questionId);

        SeoQuestion response = new SeoQuestion();
        if (jsonQnA != null) {
            logger.info("WENT FROM REDIS");
            response = _gson.fromJson(jsonQnA, SeoQuestion.class);
        } else {
            logger.info("WENT FROM REST");
            ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/question/getQuestionSeo?qid=" + questionId,
                    HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            redisDAO.setex("ques:" + questionId, jsonString, DateTimeUtils.SECONDS_PER_WEEK);
            response = _gson.fromJson(jsonString, SeoQuestion.class);
        }
        return response;
    }*/

    public SeoQuestion getSeoQuestion(String questionId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/question/getQuestionSeo?qid=" + questionId,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        return GSON.fromJson(jsonString, SeoQuestion.class);
    }

    private void setRelatedContents(CategoryPage page) {

        Type _type = new TypeToken<ArrayList<BlogMetadataPojo>>() {
        }.getType();

        List<BlogMetadataPojo> blogRelatedArticles =  getBlogArticleByCategoryMetadata(page.getDomain(), page.getBlogCategoryId(), _type, 0 , 4);

        page.setBlogRelatedArticles(blogRelatedArticles);

    }

    private void updateAwsContentFileUrl(CategoryPage page) {

        if (page == null || page.getPageType() == null) {
            return;
        }
        switch (page.getPageType()) {
            case PageType3:
                updatePageType3Url(page);
                break;
            case QnA:
                updateQnAPageUrl(page);
                break;
            case TopicPage:
                updateTopicPageUrl(page);
                break;
            case BlogPage:
                updateBlogPageUrl(page);
                break;
        }
    }

    private void updateTopicPageUrl(CategoryPage page) {
        //https://seo-manager.s3-ap-southeast-1.amazonaws.com/prod/public/seo/topicPages/3a03fda2-beb6-4259-bb21-aee6fd424ba87299084515884567646.png
        String htmlContent = page.getHtmlContent();
        SeoDomain domain = page.getDomain() == null ? SeoDomain.VEDANTU : page.getDomain();
        if (StringUtils.isNotEmpty(htmlContent)) {
            Document document = Jsoup.parse(htmlContent);
            Elements images = document.select("img");
            for (Element image : images) {
                String src = image.attr("src");
                if (StringUtils.isNotEmpty(src)) {
                    String url = removeUrlParams(src);
                    String newUrl = getReplacementUrl(url, domain.getDomainUrl(),
                            "https://" + domain.getAwsBucket() + ".s3-ap-southeast-1.amazonaws.com/prod/public", "public/seo/topicPages");
                    htmlContent = htmlContent.replaceAll(Pattern.quote(src), newUrl);
                }
            }
            page.setHtmlContent(htmlContent);
        }
    }

    private void updateQnAPageUrl(CategoryPage page) {
        //https://cmds-vedantu.s3-ap-southeast-1.amazonaws.com/prod/public/question-sets/fcfba889-2dd7-44e9-a72b-faf9421e9df2
        SeoDomain domain = page.getDomain();

        // Return if the QnA page is not vedantu domain
        if (!SeoDomain.VEDANTU.equals(domain)) {
            return;
        }
        String question = page.getSQuestion();
        if (StringUtils.isNotEmpty(question)) {
            Document document = Jsoup.parse(question);
            Elements images = document.select("img");
            for (Element image : images) {
                String src = image.attr("src");
                if (StringUtils.isNotEmpty(src)) {
                    String url = removeUrlParams(src);
                    String solutionUrl = getReplacementUrl(url, domain.getDomainUrl(), "https://cmds-vedantu.s3-ap-southeast-1.amazonaws.com/prod", "public/questions");
                    String newUrl = getReplacementUrl(solutionUrl, domain.getDomainUrl(), "https://cmds-vedantu.s3-ap-southeast-1.amazonaws.com/prod", "public/question-sets");
                    question = question.replaceAll(Pattern.quote(src), newUrl);
                }
            }
            page.setSQuestion(question);
        }

        List<String> sSolutions = page.getSSolutions();
        List<String> newSSolutions = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(sSolutions)) {
            for (String solution : sSolutions) {
                Document document = Jsoup.parse(solution);
                if (document != null) {
                    Elements images = document.select("img");
                    for (Element image : images) {
                        String src = image.attr("src");
                        if (StringUtils.isNotEmpty(src)) {
                            String url = removeUrlParams(src);
                            String solutionUrl = getReplacementUrl(url, domain.getDomainUrl(), "https://cmds-vedantu.s3-ap-southeast-1.amazonaws.com/prod", "public/solutions");
                            String newUrl = getReplacementUrl(solutionUrl, domain.getDomainUrl(), "https://cmds-vedantu.s3-ap-southeast-1.amazonaws.com/prod", "public/question-sets");
                            solution = solution.replaceAll(Pattern.quote(src), newUrl);
                        }
                    }
                }
                newSSolutions.add(solution);
            }
            page.setSSolutions(newSSolutions);
        }
    }

    private void updatePageType3Url(CategoryPage page) {
        List<CategoryPageContentModule> contentModules = page.getContentModules();
        SeoDomain domain = page.getDomain() == null ? SeoDomain.VEDANTU : page.getDomain();
        if (ArrayUtils.isNotEmpty(contentModules)) {
            for (CategoryPageContentModule contentModule : contentModules) {
                String imageUrl = contentModule.getImageUrl();
                String newUrl = getReplacementUrl(imageUrl, domain.getDomainUrl(),
                        "https://" + domain.getAwsBucket() + ".s3.amazonaws.com/prod", "content-images");
                contentModule.setImageUrl(newUrl);

                if(StringUtils.isNotEmpty(contentModule.getImageUrlWebp())){
                    String webpImageUrl = contentModule.getImageUrlWebp();
                    String webpNewUrl = getReplacementUrl(webpImageUrl, domain.getDomainUrl(),
                            "https://" + domain.getAwsBucket() + ".s3.amazonaws.com/prod", "content-images");
                    contentModule.setImageUrlWebp(webpNewUrl);
                }
            }
        }

        List<String> downloadableLinks = page.getDownloadableLinks();
        List<String> newDownloadableLinks = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(downloadableLinks)) {
            for (String link : downloadableLinks) {
                String newUrl = getReplacementUrl(link, domain.getDomainUrl(),
                        "https://" + domain.getAwsBucket() + ".s3.amazonaws.com/prod", "content-files-downloadable");
                if (StringUtils.isNotEmpty(newUrl)) {
                    newDownloadableLinks.add(newUrl);
                }
            }
        }
        page.setDownloadableLinks(newDownloadableLinks);
    }

    private void updateBlogPageUrl(CategoryPage page) {

        if("PROD".equalsIgnoreCase(ENV)) {

            String featureImageUrl = page.getFeatureImageUrl();

            if (StringUtils.isNotEmpty(featureImageUrl) && featureImageUrl.contains("https://www.vedantu.com/public/")) {

                featureImageUrl = featureImageUrl.replace("https://www.vedantu.com/public/", "https://www.vedantu.com/");

                page.setFeatureImageUrl(featureImageUrl);
            }
        }

    }

    private String getReplacementUrl(String url, String replacement, String awsUrlCheckStr, String urlPathCheckStr) {
        if (url != null && url.startsWith(awsUrlCheckStr) && url.contains(urlPathCheckStr)) {
            return url.replaceAll(Pattern.quote(awsUrlCheckStr), replacement);
        }
        return url;
    }

    private String removeUrlParams(String imageUrl) {
        if (StringUtils.isNotEmpty(imageUrl)) {
            try {
                URI uri = new URI(imageUrl);
                return new URI(uri.getScheme(),
                        uri.getAuthority(),
                        uri.getPath(),
                        null, // Ignore the query part of the input url
                        null // Ignore the fragment part of the input url
                ).toString();

            } catch (Exception ignored) {
                return imageUrl;
            }
        }
        return imageUrl;
    }

    public void checkFor301Redirection(String categoryPageUrl, CategoryPageResponse entity, List<CategoryPage> categoryPages, SeoDomain domain) throws VException {
        if (StringUtils.isNotEmpty(categoryPageUrl)) {
            if (ArrayUtils.isEmpty(categoryPages)) {
                String id = "";
                try {
                    id = categoryPageUrl.substring(categoryPageUrl.lastIndexOf("-") + 1);
                } catch (Exception ignored) {
                }
                // id length is 24
                if (id.length() == 24) {
                    CategoryPage categoryPage = getCategoryPageByQuestionId(id, domain, true);
                    if (!categoryPageUrl.equalsIgnoreCase(categoryPage.getUrl())) {
                        entity.setHttpStatus(String.valueOf(HttpStatus.MOVED_PERMANENTLY.value()));
                        entity.setToBeRedirected(categoryPage.getUrl());
                    }
                } else {
                    List<CategoryPage> pageList = categoryPageDAO.findCategoryPages(domain, null, null, null, null, null, null, null, null, categoryPageUrl);
                    if (ArrayUtils.isNotEmpty(pageList)) {
                        CategoryPage categoryPage = pageList.get(0);
                        if (!categoryPageUrl.equalsIgnoreCase(categoryPage.getUrl())) {
                            entity.setHttpStatus(String.valueOf(HttpStatus.MOVED_PERMANENTLY.value()));
                            entity.setToBeRedirected(categoryPage.getUrl());
                        }
                    }
                }
            } else {
                CategoryPage categoryPage = categoryPages.get(0);
                setCategoryPageRelatedLinks(categoryPage);
            }
        }
    }

    public PlatformBasicResponse _getPresignedUrl(String key, String contentType, SeoDomain domain) {
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            String bucket = domain.getAwsBucket();
            String preSignedUrl = getPresignedUrl(bucket, key, contentType);

            platformBasicResponse.setResponse(preSignedUrl);
            platformBasicResponse.setSuccess(true);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    public String getPresignedUrl(String bucket, String key, String contentType) {
        return awsS3Manager.getPresignedUploadUrl(bucket, key, contentType);
    }

    public List<Map<String, String>> getCategoryPageContentUrlMap(String bucket, String prefix) {

        List<Map<String, String>> mapList = new ArrayList<>();

        Pattern pattern = Pattern.compile(".*" + Pattern.quote(prefix) + "(\\d+(\\.png|\\.webp))");
        List<String> categorypageContents = getCategoryPageContent(bucket, prefix).stream()
                .filter(e -> pattern.matcher(e).matches())
                .collect(Collectors.toList());

        logger.info("=======categorypageContents {}", categorypageContents);

        for (int i=0; i<categorypageContents.size(); i++){
            Map<String, String> categoryPageUrlMap = new HashMap<>();
            categoryPageUrlMap.put("png",categorypageContents.get(i));
            if(i+1 < categorypageContents.size() && categorypageContents.get(i + 1).endsWith(".webp"))
                i++;

            //Setting png as the default image if the webp is not found
            categoryPageUrlMap.put("webp",categorypageContents.get(i));
            mapList.add(categoryPageUrlMap);
        }
        return mapList;
    }

    public List<String> getCategoryPageContent(String bucket, String prefix) {

        return awsS3Manager.getBucketContents(bucket, prefix);
    }

    public Boolean getContentFileProcessingStatus(String categoryPageId) {
        return categoryPageDAO.getCategoryPageById(categoryPageId).getContentFileProcessingStatus();
    }

    public Boolean uploadContentFile(String categoryPageId, String bucket, String key, MultipartFile content)
            throws Exception {
        File contentFile = File.createTempFile(categoryPageId, ".pdf");
        FileOutputStream outputStream = new FileOutputStream(contentFile);

        byte[] readBytes = content.getBytes();

        outputStream.write(readBytes);
        outputStream.close();

        return awsS3Manager.uploadFile(bucket, key, contentFile);
    }

    public Boolean updateContentFileProcessingStatus(String contentPageId, Boolean contentFileProcessingStatus) throws BadRequestException, InternalServerErrorException {
        CategoryPage categoryPage = categoryPageDAO.getCategoryPageById(contentPageId);
        categoryPage.setContentFileProcessingStatus(contentFileProcessingStatus);

        boolean isUpdated = false;
        String key = getWebpRedissKey(categoryPage.getDomain(), categoryPage.getUrl());
        if(contentFileProcessingStatus) {
            try {
                syncContentImageUrls(categoryPage);
                isUpdated = true;

                String categoryPageUrl = categoryPage.getUrl();

                if (categoryPageUrl.startsWith("/")) {
                    categoryPageUrl = categoryPageUrl.replaceFirst("/", "");
                }

                categoryPageUrl = categoryPageUrl.endsWith("/") ? categoryPageUrl : categoryPageUrl + "/";
                String prefix = "/content-images/" + categoryPageUrl + "*";

                clearCloudFrontCache(prefix);

            } catch (Exception e) {
                logger.error("Error synchronising content image urls :", e);
                isUpdated = false;
            }

            redisDAO.del(key);
        }else {
            redisDAO.setex(key,"TRUE",600);
        }

        if(!isUpdated)
            categoryPageDAO.updateCategoryPage(categoryPage);

        return true;
    }

    public AppendPdfRuleSet addAppendPdfRuleSet(AppendPdfRuleSet ruleSet) throws VException {
        if (ArrayUtils.isEmpty(ruleSet.getRules())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "rule sets absent");
        }
        if (StringUtils.isEmpty(ruleSet.getCategoryId()) && StringUtils.isEmpty(ruleSet.getPageRelativeUrl())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "categoryId or page relative url is must");
        }
        if (StringUtils.isNotEmpty(ruleSet.getCategoryId())) {
            Category category = categoryDAO.getCategoryById(ruleSet.getCategoryId());
            if (category == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "category not found");
            }
        }
        categoryPageDAO.saveAppendPdfRuleSet(ruleSet);
        return ruleSet;
    }

    public PlatformBasicResponse deleteappendpdfruleset(String id) throws VException {
        categoryPageDAO.deleteAppendPdfRuleSet(id);
        return new PlatformBasicResponse();
    }

    // TODO impl for micro site

    public List<AppendPdfRuleSet> getAppendPdfRuleSets(String query, String categoryId,
                                                       String target, String grade, String subject, String pageRelativeUrl, Integer start,
                                                       Integer size) throws VException {
        List<AppendPdfRuleSet> ruleSets = categoryPageDAO.getAppendPdfRuleSets(query, categoryId, target,
                grade, subject, pageRelativeUrl, start, size);
        if (ArrayUtils.isNotEmpty(ruleSets)) {
            for (AppendPdfRuleSet ruleSet : ruleSets) {
                if (ArrayUtils.isEmpty(ruleSet.getRules())) {
                    for (AppendPdfPageLocationRule locRule : ruleSet.getRules()) {
                        CMDSImageDetails imageDetails = locRule.getPdf();
                        String publicUrl = awsS3Manager.getPresignedDownloadUrl(imageDetails);
                        imageDetails.setPublicUrl(publicUrl);
                    }
                }
            }
        }
        return ruleSets;
    }

    public Map<String, Object> getAppendPdfUploadUrl() {
        return awsS3Manager.getPresignedUploadUrlForAppendPdf();

    }

    public AppendPdfRuleSet getRuleSetForCategoryPage(CategoryPage page) {
        AppendPdfRuleSet response = null;
        if (page == null) {
            return response;
        }
        AppendPdfRuleSet url = null;
        AppendPdfRuleSet c_t_g_s = null;
        AppendPdfRuleSet c_t_g = null;
        AppendPdfRuleSet c_t = null;
        AppendPdfRuleSet c = null;
        List<AppendPdfRuleSet> ruleSets = categoryPageDAO.getRuleSetsForCategoryPage(page);
        if (ArrayUtils.isNotEmpty(ruleSets)) {
            for (AppendPdfRuleSet ruleSet : ruleSets) {
                boolean matchURL = StringUtils.isNotEmpty(page.getUrl())
                        && StringUtils.isNotEmpty(ruleSet.getPageRelativeUrl())
                        && page.getUrl().equalsIgnoreCase(ruleSet.getPageRelativeUrl());
                boolean matchGrade = StringUtils.isNotEmpty(page.getGrade())
                        && StringUtils.isNotEmpty(ruleSet.getGrade())
                        && page.getGrade().equalsIgnoreCase(ruleSet.getGrade());
                boolean matchTarget = StringUtils.isNotEmpty(page.getTarget())
                        && StringUtils.isNotEmpty(ruleSet.getTarget())
                        && page.getTarget().equalsIgnoreCase(ruleSet.getTarget());
                boolean matchSubject = StringUtils.isNotEmpty(page.getSubject())
                        && StringUtils.isNotEmpty(ruleSet.getSubject())
                        && page.getSubject().equalsIgnoreCase(ruleSet.getSubject());
                boolean matchCategoryId = StringUtils.isNotEmpty(page.getCategoryId())
                        && StringUtils.isNotEmpty(ruleSet.getCategoryId())
                        && page.getCategoryId().equalsIgnoreCase(ruleSet.getCategoryId());

                if (matchURL) {
                    url = ruleSet;
                } else if (matchCategoryId && matchGrade && matchTarget && matchSubject) {
                    c_t_g_s = ruleSet;
                } else if (matchCategoryId && matchGrade && matchTarget) {
                    c_t_g = ruleSet;
                } else if (matchCategoryId && matchTarget) {
                    c_t = ruleSet;
                } else if (matchCategoryId) {
                    c = ruleSet;
                }
            }
        }
        if (url != null) {
            return url;
        }
        if (c_t_g_s != null) {
            return c_t_g_s;
        }
        if (c_t_g != null) {
            return c_t_g;
        }
        if (c_t != null) {
            return c_t;
        }
        if (c != null) {
            return c;
        }
        return response;
    }

    public String getModifiedPDFDownloadLink(String pageId) throws IOException, NotFoundException {
        CategoryPage page = categoryPageDAO.getCategoryPageById(pageId);
        if (page == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "category page not found");
        }
        logger.info("getting ruleset for categoryPageID:" + pageId);
        AppendPdfRuleSet ruleSet = getRuleSetForCategoryPage(page);
        //usually it is always pageId but in some cases where a copy is made
        //from existing category page, then the pageid and the downloadFileRefId will differ
        String downloadFileRefId = extractDownloadFileRefIdFromUrl(page.getDownloadableLinks());
        String previousKey = ENV + "/content-files-downloadable/" + downloadFileRefId + ".pdf";
        if (ruleSet == null) {
            logger.info("no ruleset for categoryPageID:" + pageId);
            return awsS3Manager.getPublicBucketDownloadUrl(SEO_BUCKET, previousKey);
        }

        logger.info("got ruleset:" + ruleSet);
        String newFileName = pageId + "_" + ruleSet.getId() + "_" + ruleSet.getLastUpdated() + ".pdf";
        String newKey = ENV + "/content-files-downloadable-modified/" + newFileName;
        newFileName += System.currentTimeMillis();
        if (awsS3Manager.checkKeyExists(SEO_BUCKET, newKey)) {
            logger.info("modified file already exists");
            return awsS3Manager.getPublicBucketDownloadUrl(SEO_BUCKET, newKey);
        }
        InputStream stream = awsS3Manager.getObjectInputStream(SEO_BUCKET, previousKey);
        PdfReader reader = new PdfReader(stream);
        reader.setUnethicalReading(true);
        PdfDocument stamper = new PdfDocument(reader, new PdfWriter(newFileName));
        List<PdfDocument> covers = new ArrayList<>();
        List<InputStream> secondStreams = new ArrayList<>();
        try {
            List<AppendPdfPageLocationRule> rules = ruleSet.getRules();
            if (ArrayUtils.isNotEmpty(rules)) {
                for (AppendPdfPageLocationRule rule : rules) {
                    if (AppendPdfPageLocationRule.PageLocationtype.BOTTOM.equals(rule.getPageLocationtype())) {
                        rule.setValue(stamper.getNumberOfPages());
                    } else if (AppendPdfPageLocationRule.PageLocationtype.TOP.equals(rule.getPageLocationtype())) {
                        rule.setValue(0);
                    }
                }
                Collections.sort(rules, (AppendPdfPageLocationRule o1, AppendPdfPageLocationRule o2) -> {
                    return o2.getValue() - (o1.getValue());
                });
            }
            if (ArrayUtils.isNotEmpty(rules)) {
                for (AppendPdfPageLocationRule rule : rules) {
                    logger.info("applying rule" + rule);
                    int location = rule.getValue();
                    CMDSImageDetails detail = rule.getPdf();
                    String bucket = detail.getUploadTarget().getBucketName();
                    String key = ENV + "/" + detail.getUploadTarget().getFolderPath() + "/" + detail.getFileName();
                    InputStream secondStream = awsS3Manager.getObjectInputStream(bucket, key);
                    PdfReader coverReader = new PdfReader(secondStream);
                    coverReader.setUnethicalReading(true);
                    PdfDocument cover = new PdfDocument(coverReader);
                    cover.copyPagesTo(1, cover.getNumberOfPages(), stamper, (location + 1), new PdfPageFormCopier());
                    covers.add(cover);
                    secondStreams.add(secondStream);
                }
            }
            if (stamper != null) {
                stamper.close();
            }
            if (reader != null) {
                reader.close();
            }
            for (PdfDocument c : covers) {
                if (c != null) {
                    c.close();
                }
            }
            logger.info("all rules applied. uploading file to s3");
            File file = new File(newFileName);
            awsS3Manager.uploadPdf(SEO_BUCKET, newKey, file);
            logger.info("file upload to s3 successfully");
        } finally {
            logger.info("Closing all streams in finally block");
            IOUtils.closeQuietly(stream);
            for (InputStream s : secondStreams) {
                IOUtils.closeQuietly(s);
            }
            File dfile = new File(newFileName);
            Files.deleteIfExists(dfile.toPath());
            logger.info("all streams are closed");
        }
        if (awsS3Manager.checkKeyExists(SEO_BUCKET, newKey)) {
            return awsS3Manager.getPublicBucketDownloadUrl(SEO_BUCKET, newKey);
        }
        return null;
    }

    public String getPublicDownloadUrl(String pageId) throws NotFoundException {
        CategoryPage page = categoryPageDAO.getCategoryPageById(pageId);
        if (page == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "category page not found");
        }
        //usually it is always pageId but in some cases where a copy is made
        //from existing category page, then the pageid and the downloadFileRefId will differ
        String downloadFileRefId = extractDownloadFileRefIdFromUrl(page.getDownloadableLinks());
        String previousKey = ENV + "/content-files-downloadable/" + downloadFileRefId + ".pdf";
        return awsS3Manager.getPublicBucketDownloadUrl(SEO_BUCKET, previousKey);
    }

    @Deprecated
    private String extractDownloadFileRefId(List<String> downloadableLinks) {
        String refId = null;
        try {
            if (ArrayUtils.isNotEmpty(downloadableLinks)) {
                String downloadLink = downloadableLinks.get(0);
                downloadLink = downloadLink.substring(downloadLink.lastIndexOf("/") + 1);
                downloadLink = downloadLink.substring(0, downloadLink.lastIndexOf("."));
                refId = downloadLink;
            }
        } catch (Exception e) {
            logger.error("Error in extracting download ref id" + e.getMessage());
        }
        return refId;
    }

    private String extractDownloadFileRefIdFromUrl(List<String> downloadableLinks) {
        String refId = null;

        try {
            if (ArrayUtils.isNotEmpty(downloadableLinks)) {
                String downloadLink = downloadableLinks.get(0);

                String commonPath = null;
                if (downloadLink.contains("content-files-downloadable/")) {
                    commonPath = "content-files-downloadable/";
                } else if (downloadLink.contains("content-files/")) {
                    commonPath = "content-files/";
                } else
                    throw new VException(ErrorCode.SERVICE_ERROR, "Unknown common url path for url : " + downloadLink);
                downloadLink = downloadLink.substring(downloadLink.indexOf(commonPath) + commonPath.length());
                downloadLink = downloadLink.substring(0, downloadLink.lastIndexOf("."));
                refId = downloadLink;
            }
        } catch (Exception e) {
            logger.error("Error in extracting download ref id" + e.getMessage());
        }
        return refId;
    }


//    public static void main(String[] args) throws FileNotFoundException, IOException {
//        try {
//            File initialFile = new File("/Users/ajith/Downloads/58cfbdf6e4b07c707eb49743.pdf");
//            InputStream stream = new FileInputStream(initialFile);
//            PdfReader reader = new PdfReader(stream);
//            reader.unethicalreading=true;
//            System.err.println(">> first read");
//            FileOutputStream outStream = new FileOutputStream("/Users/ajith/Downloads/finalfile.pdf");
//            System.err.println(">> before stamper");
//            PdfStamper stamper = new PdfStamper(reader, outStream);
//            System.err.println(">> ageter stamper");
//            List<PdfReader> covers = new ArrayList<>();
//            int location = 0;
//            File topfile = new File("/Users/ajith/Downloads/top.pdf");
//            InputStream secondStream = new FileInputStream(topfile);
//            System.err.println(">> before 2nd");
//            PdfReader cover = new PdfReader(secondStream);
//            System.err.println(">> cover read");
//            int number = cover.getNumberOfPages();
//            for (int i = 1; i <= number; i++) {
//                System.out.println(">>page inet "+i);
//                stamper.insertPage(location + i, cover.getPageSizeWithRotation(i));
//                PdfContentByte page1 = stamper.getOverContent(location + i);
//                PdfImportedPage paget = stamper.getImportedPage(cover, i);
//                page1.addTemplate(paget, 0, 0);
//            }
//            System.out.println("loop count done");
//            covers.add(cover);
//            System.out.println("local file created closing all files");
//            stamper.close();
//            reader.close();
//            for (PdfReader c : covers) {
//                c.close();
//            }
//        } catch (Exception e) {
//            System.err.println(">>" + e.getMessage());
//        }
//    }

    /**
     * @throws VException On Service Error
     */
    public void generateCategoryPagesFromQuestions(SeoDomain domain) throws VException {
        final int MAX_FETCH_SIZE = 10;
        int count = 0, page = 0;
        List<CMDSQuestionDto> indexableQuestions = getCMDSQuestion(page, MAX_FETCH_SIZE);
        Category category = getQnACategory();
        while (ArrayUtils.isNotEmpty(indexableQuestions) && page < 1000 && page >= 0) {
            logger.info("CREATE CATEGORY PAGE - ITERATION: " + page);
            page++;
            for (CMDSQuestionDto question : indexableQuestions) {
                count++;
                createCategoryPage(category, question, domain, true);
            }
            indexableQuestions = getCMDSQuestion(page, MAX_FETCH_SIZE);
        }

        if (page == 1000 && ArrayUtils.isNotEmpty(indexableQuestions)) {
            throw new VException(ErrorCode.SERVICE_ERROR, "More questions may be available for indexing");
        }

        logger.info("CATEGORY PAGES CREATED / UPDATED : " + count);
    }

    /**
     * @throws VException On Service Error
     */
    public int limitAndGenerateCategoryPagesFromQuestions(SeoDomain domain, int limit, int page) throws VException {
        final int MAX_FETCH_SIZE = limit < 10 ? limit : 10;
        int count = 0;
        List<CMDSQuestionDto> indexableQuestions = getCMDSQuestion(page, MAX_FETCH_SIZE);
        Category category = getQnACategory();
        outer:
        while (ArrayUtils.isNotEmpty(indexableQuestions) && page < 1000 && page >= 0 && count <= limit) {
            logger.info("CREATE CATEGORY PAGE - ITERATION: " + page);
            page++;
            for (CMDSQuestionDto question : indexableQuestions) {
                count++;
                createCategoryPage(category, question, domain, true);
                if (count >= limit) break outer;
            }
            indexableQuestions = getCMDSQuestion(page, MAX_FETCH_SIZE);
        }

        logger.info("CATEGORY PAGES CREATED / UPDATED : " + count);
        return count;
    }

    private List<CMDSQuestionDto> getCMDSQuestion(int page, int fetchSize) throws VException {
        ClientResponse resp;
        resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/cmds/question/seo/indexable?page=" + page + "&max=" + fetchSize,
                HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respEntity = resp.getEntity(String.class);

        Gson gson = new Gson();
        List entities = gson.fromJson(respEntity, List.class);
        List<CMDSQuestionDto> dtos = new ArrayList<>();
        entities.forEach(entity -> dtos.add(gson.fromJson(gson.toJson(entity), CMDSQuestionDto.class)));
        return dtos;
    }

    /**
     * @param category             Category for creating the Category Page
     * @param question             Question to be converted to category page
     * @param populateRelatedLinks To generate related links
     * @throws VException On Service Error
     */
    public void createCategoryPage(Category category, CMDSQuestionDto question, SeoDomain domain, boolean populateRelatedLinks)
            throws VException {
        if (category == null) {
            category = getQnACategory();
        }

        CategoryPage categoryPage = setQuestionDataInCategoryPage(question, category, domain, populateRelatedLinks);

        categoryPageDAO.updateCategoryPage(categoryPage);

        //Clear topic list cache
        String redsKeyTopics = SeoQuestionAndAnswerManager.getQnAFilterOptionsFromRedissKey();
        redisDAO.del(redsKeyTopics);

        // Get Question with formatted html
        getSeoQnAWithFormattedHTML(categoryPage);

        categoryPage.setIsVerified(null);

        updateAwsContentFileUrl(categoryPage);

        pushCategoryPageToES(categoryPage);

    }

    /**
     * @return Default Category for generated CategoryPages
     */
    private Category getQnACategory() {
        return categoryDAO.getCategoryByName("Question Answer", SeoDomain.VEDANTU); // TODO Change this
    }

    /**
     * @param questionId To check for category page present with this Question Id
     * @return CategoryPage
     */
    private CategoryPage getOrSetCategoryPage(String questionId, SeoDomain domain) {
        CategoryPage categoryPage = getCategoryPageByQuestionId(questionId, domain, false);
        if (categoryPage == null || StringUtils.isEmpty(categoryPage.getId())) {
            return new CategoryPage();
        }
        return categoryPage;
    }

    /**
     * @param question             Question to be converted to Category Page
     * @param category             Category of the given question
     * @param populateRelatedLinks To generate related links
     * @return CategoryPage with updated data
     * @throws VException On Service Error
     */
    private CategoryPage setQuestionDataInCategoryPage(CMDSQuestionDto question, Category category, SeoDomain domain,
                                                       boolean populateRelatedLinks) throws VException {
        String questionId = question.getId();
        CategoryPage categoryPage = getOrSetCategoryPage(questionId, domain);

        // Set these data only for first time page creation
        if (StringUtils.isEmpty(categoryPage.getUrl())) {

            String target = ArrayUtils.isNotEmpty(question.getTargets()) ?
                    question.getTargets().toArray(new String[]{})[0] : null;
            String grade = ArrayUtils.isNotEmpty(question.getGrades()) ?
                    question.getGrades().toArray(new String[]{})[0] : null;
            String targetGrade = ArrayUtils.isNotEmpty(question.getTargetGrade()) ?
                    question.getTargetGrade().toArray(new String[]{})[0] : null;
            String topic = ArrayUtils.isNotEmpty(question.getTopics()) ?
                    question.getTopics().toArray(new String[]{})[0] : null;

            if ((StringUtils.isEmpty(target) || StringUtils.isEmpty(grade)) && StringUtils.isNotEmpty(targetGrade)) {
                try {
                    String[] split = targetGrade.split("-");
                    target = StringUtils.isEmpty(target) ? split[0] : target;
                    grade = StringUtils.isEmpty(grade) ? split[1] : grade;
                } catch (Exception ignored) {

                }
            }

            categoryPage.setQuestionId(questionId);
            categoryPage.setCategoryId(category.getId());
            categoryPage.setPageType(CategoryPageType.QnA);
            categoryPage.setTopic(topic);
            categoryPage.setDifficulty(question.getDifficulty());

            categoryPage.setActive(Boolean.TRUE);
            categoryPage.setDisabled(Boolean.FALSE.equals(question.getMakePageLive()));

            String subject = StringUtils.isNotEmpty(question.getSubject()) ? question.getSubject().toLowerCase().trim() : null;
            categoryPage.setSubject(subject);
            categoryPage.setTarget(StringUtils.isNotEmpty(target) ? target.toLowerCase().trim().replaceAll("_","-") : null);
            categoryPage.setGrade(grade);
            String questionText = removeNonAlphaChars(question.getQuestionBody().getNewText());

            String description = questionText;
            if(description.length()>200) {
                String temp1 = description.substring(0, 200);
                String temp2 = description.substring(temp1.length());
                description = temp1;

                int charIndex = 0;
                if(temp2.contains(" ")){
                    charIndex = temp2.indexOf(" ");
                }
                if(temp2.contains(".") && (charIndex == 0 || charIndex > temp2.indexOf("."))){
                    charIndex = temp2.indexOf(".");
                }
                if(temp2.contains(",") && (charIndex == 0 || charIndex > temp2.indexOf(","))){
                    charIndex = temp2.indexOf(",");
                }

                if(charIndex > 0)
                    description += temp2.substring(0,charIndex);
            }

            categoryPage.setDescription(description.trim());

            if(questionText.length()>50) {
                String temp1 = questionText.substring(0, 50);
                String temp2 = questionText.substring(temp1.length());
                questionText = temp1;

                int charIndex = 0;
                if(temp2.contains(" ")){
                    charIndex = temp2.indexOf(" ");
                }
                if(temp2.contains(".") && (charIndex == 0 || charIndex > temp2.indexOf("."))){
                    charIndex = temp2.indexOf(".");
                }
                if(temp2.contains(",") && (charIndex == 0 || charIndex > temp2.indexOf(","))){
                    charIndex = temp2.indexOf(",");
                }

                if(charIndex > 0)
                    questionText += temp2.substring(0,charIndex);
            }

            categoryPage.setName(questionText.trim());

            String metaFields = "-class-" + grade + "-";
            if(StringUtils.isNotEmpty(subject)){
                String shortSubject = subject;
                if("mathematics".equalsIgnoreCase(subject))
                    shortSubject = "maths";

                metaFields += shortSubject + "-";
            }
            metaFields += target;

            String title = questionText + metaFields;

            categoryPage.setTitle(title.replaceAll("-"," ").trim());
            categoryPage.setTopic(topic);

            String urlPart = removeUnwantedPhrases(questionText.toLowerCase()).replaceAll(" ", "-");
            urlPart += metaFields;
            String customisedUrl = "/question-answer/" + urlPart.toLowerCase() + "-" + question.getId();

            customisedUrl = customisedUrl.replaceAll("_","-");

            categoryPage.setUrl(customisedUrl);

            if(StringUtils.isNotEmpty(question.getVideo()))
                categoryPage.setVideoUrl(question.getVideo());

        }

        if (populateRelatedLinks) {
            categoryPage.setRelatedPageIds(getRelatedPageIds(categoryPage)); // TODO API
        }

        return categoryPage;
    }

    /**
     * @param categoryPage CategoryPage to be updated
     * @return Link IDs of related questions
     */
    private LinkedHashSet<String> getRelatedPageIds(CategoryPage categoryPage) {
        LinkedHashSet<String> relatedPageSet;

        String questionId = categoryPage.getId();

        int maxSize = 10;

        String[] searchTerms = getSearchTerms(categoryPage, CategoryPage.Constants.TARGET, CategoryPage.Constants.GRADE, CategoryPage.Constants.SUBJECT);
        String[] mustContain = getSearchTerms(categoryPage, CategoryPage.Constants.GRADE);

        List<CategoryPage> questions = categoryPageDAO.getCategoryPagesFromSearchTerms(searchTerms, null, mustContain, questionId, maxSize);
        relatedPageSet = questions.stream().map(CategoryPage::getId).filter(Objects::nonNull)
                .filter(e -> !e.equalsIgnoreCase(questionId)).collect(Collectors.toCollection(LinkedHashSet::new));

        return relatedPageSet;
    }

    /**
     * @param categoryPage Question to be converted to Category Page
     * @return Array of search terms to use in text search query
     */
    private String[] getSearchTerms(CategoryPage categoryPage, @NotNull String... fields) {
        Set<String> set = new LinkedHashSet<>();

        for (String field : fields) {
            switch (field) {
                case CategoryPage.Constants.TARGET:
                    if (StringUtils.isNotEmpty(categoryPage.getTarget())) {
                        set.add(categoryPage.getTarget().toLowerCase());
                    }
                    break;
                case CategoryPage.Constants.GRADE:
                    if (StringUtils.isNotEmpty(categoryPage.getGrade())) {
                        set.add(categoryPage.getGrade().toLowerCase());
                    }
                    break;
                case CategoryPage.Constants.SUBJECT:
                    if (StringUtils.isNotEmpty(categoryPage.getSubject())) {
                        set.add(categoryPage.getSubject().toLowerCase());
                    }
                    break;
            }
        }
        return set.toArray(new String[]{});
    }

    /**
     * @param urlPart Removes the unwanted string in the url
     * @return part of url of the question
     */
    private static String removeUnwantedPhrases(String urlPart) {

        String verbs = "(\\bis\\b|\\bwas\\b|\\bbe\\b|\\bbeen\\b|\\bhave\\s+been\\b|\\bhad\\s+been\\b|\\bhad\\b|\\bhave\\b|\\bwere\\b|\\bhas\\b|\\bhas\\s+been\\b|\\bdo\\b|\\bcan\\b|\\bcould\\b|\\bshall\\b|\\bshould\\b|\\bwill\\b|\\bwould\\b|\\bof\\b|)";
        String pronouns = "(\\byou\\b|\\bhe\\b|\\bshe\\b|\\bit\\b|\\bthey\\b|\\bi\\b|\\bmy\\b|\\bmine\\b|\\bwe\\b|\\bthat\\b|)";
        String prepositions = "(\\bthe\\b|\\band\\b|\\bthat\\b|\\ban\\b|\\bmuch\\b|\\bmany\\b|\\boften\\b|\\bto\\b|\\bfar\\b|\\bab|\\bbe\\b|)";

        String verbprepro = verbs + "\\s*" + prepositions + "\\s*" + pronouns;
        String verbpropre = verbs + "\\s*" + pronouns + "\\s*" + prepositions;
        String preverbpro = prepositions + "\\s*" + verbs + "\\s*" + prepositions;
        String preproverb = prepositions + "\\s*" + prepositions + "\\s*" + verbs;
        String propreverb = pronouns + "\\s*" + prepositions + "\\s*" + verbs;
        String proverbpre = pronouns + "\\s*" + verbs + "\\s*" + prepositions;
        List<String> phrases = Arrays.asList(
                "^who\\s+" + verbprepro,
                "^who\\s+" + verbpropre,
                "^who\\s+" + preverbpro,
                "^who\\s+" + preproverb,
                "^who\\s+" + propreverb,
                "^who\\s+" + proverbpre,

                "^where\\s+" + verbprepro,
                "^where\\s+" + verbpropre,
                "^where\\s+" + preverbpro,
                "^where\\s+" + preproverb,
                "^where\\s+" + propreverb,
                "^where\\s+" + proverbpre,

                "^when\\s+" + verbprepro,
                "^when\\s+" + verbpropre,
                "^when\\s+" + preverbpro,
                "^when\\s+" + preproverb,
                "^when\\s+" + propreverb,
                "^when\\s+" + proverbpre,

                "^why\\s+" + verbprepro,
                "^why\\s+" + verbpropre,
                "^why\\s+" + preverbpro,
                "^why\\s+" + preproverb,
                "^why\\s+" + propreverb,
                "^why\\s+" + proverbpre,

                "^what\\s+" + verbprepro,
                "^what\\s+" + verbpropre,
                "^what\\s+" + preverbpro,
                "^what\\s+" + preproverb,
                "^what\\s+" + propreverb,
                "^what\\s+" + proverbpre,

                "^how\\s+" + verbprepro,
                "^how\\s+" + verbpropre,
                "^how\\s+" + preverbpro,
                "^how\\s+" + preproverb,
                "^how\\s+" + propreverb,
                "^how\\s+" + proverbpre);

        urlPart = urlPart.toLowerCase().trim();
        if (urlPart.length() <= 25) {
            return urlPart;
        }

        String tmp;
        String prev = urlPart;
        for (String phrase : phrases) {
            tmp = urlPart.replaceFirst(phrase, "");
            if (tmp.length() < prev.length()) {
                prev = tmp;
            }
        }


        if (prev.length() <= 25) return urlPart;

        int count = 0;
        String[] parts = prev.split("\\s+");
        List<String> urlParts = new ArrayList<>();
        for (String part : parts) {
            count += (part.length() + 1);
            if (count < 50) {
                urlParts.add(part);
            }
        }

        prev = String.join(" ", urlParts);

        return prev.trim();
    }


    /**
     * @param newText Question string of the the CMDSQuestionDto
     * @return Removes non alphabetic characters
     * @throws VException On Service Error
     */
    private String removeNonAlphaChars(String newText) throws VException {
        if (StringUtils.isNotEmpty(newText)) {

            newText = newText.replaceAll("\\\\text", "");

            Document parse = Jsoup.parse(newText);
            String text = parse.body().text();

            String[] split = text.trim()
                    .replaceAll("[^A-Za-z0-9\\s+]", "")
                    .split("\\s+");
            List<String> urlParts = new ArrayList<>();
            Collections.addAll(urlParts, split);
            return String.join(" ", urlParts);
        }
        throw new VException(ErrorCode.INVALID_QUESTION_FORMAT, "URL FIELD IS EMPTY");
    }

    public CategoryPage getCategoryPageByUrl(String categoryPageUrl, SeoDomain domain) throws VException {
        List<CategoryPage> categoryPages = categoryPageDAO.getCategoryPageByUrl(domain, categoryPageUrl, null, null, null, null);
        if (ArrayUtils.isEmpty(categoryPages)) {
            throw new VException(ErrorCode.SERVICE_ERROR, "Category Page not found for Url : " + categoryPageUrl);
        } else {
            return categoryPages.get(0);
        }
    }

    public void buildHtmlPages(SeoDomain domain) throws VException {
        if (SeoDomain.VEDANTU.equals(domain)) {
            return;
        }

        int page = 0;
        int limit = 100;
        List<CategoryPage> pages = categoryPageDAO.getDomainPages(domain, page, limit);
        while (pages.size() > 0) {
            for (CategoryPage categoryPage : pages) {
                uploadStaticHtmlToS3(categoryPage);

            }
            pages = categoryPageDAO.getDomainPages(domain, ++page, limit);
        }
    }

    private void uploadStaticHtmlToS3(CategoryPage categoryPage) throws VException {
        if (Boolean.FALSE.equals(categoryPage.getDisabled())) {
            File htmlFile = getMicrositeHtmlFile(categoryPage);
            String url = categoryPage.getUrl();
            url = url.trim().startsWith("/") ? url : "/" + url.trim();
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(htmlFile.length());
            metadata.setContentType("text/html");
            metadata.setContentEncoding("UTF-8");
            url = url.startsWith("/") ? url : "/" + url;
            awsS3Manager.uploadFile(categoryPage.getDomain().getAwsBucket(),
                    ENV + url, htmlFile, metadata);
        }
    }

    private File getMicrositeHtmlFile(CategoryPage categoryPage) throws VException {
        File html = null;
        try {
            // TODO Front End API

            String url = FOS_ENDPOINT + "/acadStack/createPage";
            logger.info("FOS URL = " + url);
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, GSON.toJson(categoryPage));
            VExceptionFactory.INSTANCE.parseAndThrowException(response);
            String entity = response.getEntity(String.class);
            html = File.createTempFile("ACADSTACK", "");
            Files.write(html.toPath(), entity.getBytes(), StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            logger.error(e.getMessage(), e);
            throw new VException(ErrorCode.SERVICE_ERROR, "Error creating file from html");
        }
        return html;
    }

    private void invalidateCache(CategoryPage categoryPage) {
        SeoDomain domain = categoryPage.getDomain();
        if ("prod".equals(ENV) && domain != null &&
                !SeoDomain.VEDANTU.equals(domain)) {
            String url = categoryPage.getUrl();
            List<String> paths = new ArrayList<>();
            if (url != null && !"".equals(url.trim())) {
                url = getAwsKeyPrefixForDomain(domain) + url;
                paths.add(removeInvisiblePrefix(url, domain));
            }

            List<CategoryPageContentModule> contentModules = categoryPage.getContentModules();
            if (contentModules != null && contentModules.size() > 0) {
                for (CategoryPageContentModule contentModule : contentModules) {
                    String imageUrl = contentModule.getImageUrl();
                    if (imageUrl != null && !"".equals(imageUrl.trim())) {
                        try {
                            URL link = new URL(imageUrl);
                            String path = link.getPath();
                            path = path.substring(0, path.lastIndexOf("/") + 1) + "*";
                            paths.add(removeInvisiblePrefix(path, domain));
                            break;
                        } catch (MalformedURLException ignored) {
                        }
                    }
                }
            }

            List<String> links = categoryPage.getDownloadableLinks();
            if (links != null && links.size() > 0) {
                for (String link : links) {
                    if (link != null && !"".equals(link.trim())) {
                        try {
                            URL pdfUrl = new URL(link);
                            String path = pdfUrl.getPath();
                            paths.add(removeInvisiblePrefix(path, domain));
                        } catch (MalformedURLException ignored) {
                        }

                    }
                }
            }

            if (paths.size() > 0) {
                try {
                    cloudFrontManager.invalidateCache(AwsCloudFrontManager.ACADTACK_DISTRIBUTION_ID, paths);
                } catch (Exception e) {
                    logger.warn("CLOUFRONT INVALIDATION " + e.getMessage(), e);
                }
            }
        }
    }

    public void invalidateCacheDomainVedantu(CategoryPage categoryPage) {
        SeoDomain domain = categoryPage.getDomain();
        if ("prod".equals(ENV) && domain != null &&
                SeoDomain.VEDANTU.equals(domain)) {

            List<String> paths = new ArrayList<>();

            List<CategoryPageContentModule> contentModules = categoryPage.getContentModules();
            if (contentModules != null && contentModules.size() > 0) {
                for (CategoryPageContentModule contentModule : contentModules) {
                    String imageUrl = contentModule.getImageUrl();
                    if (imageUrl != null && !"".equals(imageUrl.trim())) {
                        try {
                            URL link = new URL(imageUrl);
                            String path = link.getPath();
                            path = path.substring(0, path.lastIndexOf("/") + 1) + "*";
                            paths.add(removeInvisiblePrefix(path, domain));
                            break;
                        } catch (MalformedURLException ignored) {
                        }
                    }
                }
            }

            if (CollectionUtils.isNotEmpty(paths)) {
                try {
                    cloudFrontManager.invalidateCache(AwsCloudFrontManager.VEDANTU_DISTRIBUTION_ID, paths);
                } catch (Exception e) {
                    logger.warn("CLOUFRONT INVALIDATION " + e.getMessage(), e);
                }
            }
        }
    }

    private String removeInvisiblePrefix(String url, SeoDomain domain) {
        if (StringUtils.isEmpty(url)) {
            return url;
        }
        if (domain == SeoDomain.ACADSTACK) {
            url = url.replaceAll("\\Qhttps://seo-acadstack.s3.ap-southeast-1.amazonaws.com/prod\\E", "");
        }else if (domain == SeoDomain.VEDANTU) {
            url = url.replaceAll("\\Qhttps://www.vedantu.com\\E", "");
        }
        return url;
    }

    public String generateCsv(SeoDomain domain, String categoryId) throws IOException, IllegalAccessException, NoSuchFieldException {

        int size = 100, start = 0;
        List<CategoryPage> categoryPages;
        Path page = Files.createTempFile("page", ".csv");
        Map<String, String> headerMap = getCategoryPageCsvHeaderMap();
        List<String> header = new ArrayList<>(headerMap.values());
        Files.write(page, String.join(",", header).concat(System.lineSeparator()).getBytes(),
                StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        List<Category> allCategories = categoryDAO.getAllCategories(SeoDomain.VEDANTU);
        Map<String, String> category = allCategories.stream().collect(Collectors.toMap(Category::getId, Category::getName));
        do {
            categoryPages = categoryPageDAO.findCategoryPages(domain,categoryId, null, null, null, null, null, start, size);
            start += size;
            Files.write(page, getCategoryPageCsvBytes(categoryPages, category, headerMap), StandardOpenOption.CREATE, StandardOpenOption.APPEND);
        } while (categoryPages.size() == 100);

        String key = ENV + "/temp/csv/" + System.currentTimeMillis() + ".csv";
        awsS3Manager.uploadFile(domain.getAwsBucket(), key, page.toFile());
        logger.info(page.toAbsolutePath());
        page.toFile().delete();
        return awsS3Manager.getPublicBucketDownloadUrl(domain.getAwsBucket(), key);
    }

    public String generateXlsx(SeoDomain domain, String categoryId) throws IOException, IllegalAccessException, NoSuchFieldException, VException {

        int size = 100, start = 0;
        List<CategoryPage> categoryPages;
        Map<String, String> headerMap = getCategoryPageCsvHeaderMap();

        List<String> titleValues = new ArrayList<>(headerMap.values());

        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet("Category_Pages");

        int rowCount = 0;
        Row headerRow = sheet.createRow(rowCount++);
        for(int i = 0; i < titleValues.size(); i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(titleValues.get(i));
        }

        List<Category> allCategories = categoryDAO.getAllCategories(domain);
        Map<String, String> category = allCategories.stream().collect(Collectors.toMap(Category::getId, Category::getName));

        String regExLatex = "\\\\\\w*";
        Pattern latexPattern = Pattern.compile(regExLatex, Pattern.CASE_INSENSITIVE);

        do {
            categoryPages = categoryPageDAO.findCategoryPages(domain,categoryId, null, null, null, null, null, start, size);
            start += size;
            for (CategoryPage categoryPage : categoryPages) {

                Row cellRow = sheet.createRow(rowCount++);
                int cellCount = 0;

                CategoryPageHeader header = categoryPage.getHeader();
                String headerDescriptionText = header != null ? getDescriptionParsedText(header.getDescription()) : "";

                CategoryPageFooter footer = categoryPage.getFooter();
                String footerDescriptionText = footer != null ? getDescriptionParsedText(footer.getDescription()) : "";

                String htmlContent = categoryPage.getHtmlContent();
                String htmlContentText = StringUtils.isNotEmpty(htmlContent) ? getDescriptionParsedText(htmlContent) : "";

                SeoQuestion seoQuestion;
                String questionBodyText = "";
                String solutions = "";
                if(StringUtils.isNotEmpty(categoryPage.getQuestionId())){
                    seoQuestion = getSeoQuestion(categoryPage.getQuestionId());

                    if(seoQuestion!= null) {
                        String questionBody = seoQuestion.getQuestionBody() != null ? seoQuestion.getQuestionBody().getNewText() : "";
                        questionBodyText = StringUtils.isNotEmpty(questionBody) ? getDescriptionParsedText(questionBody) : "";

                        List<SolutionFormat> solutionFormats = seoQuestion.getSolutions();
                        for (SolutionFormat solutionFormat : solutionFormats) {
                            solutions += "\n" + solutionFormat.getNewText();
                        }

                        solutions = getDescriptionParsedText(solutions);
                    }
                }

                if(StringUtils.isNotEmpty(questionBodyText)){
                    questionBodyText = filterRegex(latexPattern, questionBodyText);
                }

                if(StringUtils.isNotEmpty(solutions)){
                    solutions = filterRegex(latexPattern, solutions);
                }

                for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                    String key = entry.getKey();

                    String content = "";

                    if ("category".equals(key)) {
                        String cat = category.get(categoryPage.getCategoryId());
                        content = cat;
                    } else if ("id".equals(key)) {
                        content = categoryPage.getId();
                    } else if ("header_title".equals(key) ) {
                        content = header != null ? StringUtils.defaultIfEmpty(header.getTitle()) : "";
                    } else if ("header_sub_title".equals(key)) {
                        content = header != null ? StringUtils.defaultIfEmpty(header.getSubTitle()) : "";
                    } else if ("header_description".equals(key)) {
                        content = headerDescriptionText;
                    } else if ("header_description_word_count".equals(key)) {
                        String count = StringUtils.isEmpty(headerDescriptionText) ? "0" : String.valueOf(headerDescriptionText.split("\\s+").length);
                        content = count;
                    } else if ("question".equals(key)) {
                        content = questionBodyText;
                    } else if ("question_word_count".equals(key)) {
                        String count = StringUtils.isEmpty(questionBodyText) ? "0" : String.valueOf(questionBodyText.split("\\s+").length);
                        content = count;
                    } else if ("footer_title".equals(key)) {
                        content = footer != null ? StringUtils.defaultIfEmpty(footer.getTitle()) : "";
                    } else if ("footer_description".equals(key)) {
                        content = footerDescriptionText;
                    } else if ("footer_description_word_count".equals(key)) {
                        String count = StringUtils.isEmpty(footerDescriptionText) ? "0" : String.valueOf(footerDescriptionText.split("\\s+").length);
                        content = count;
                    } else if ("solutions".equals(key)) {
                        content = solutions;
                    } else if ("solutions_word_count".equals(key)) {
                        String count = StringUtils.isEmpty(solutions) ? "0" : String.valueOf(solutions.split("\\s+").length);
                        content = count;
                    } else if ("html_content".equals(key)) {
                        content = htmlContentText;
                    } else if ("html_content_word_count".equals(key)) {
                        String count = StringUtils.isEmpty(htmlContentText) ? "0" : String.valueOf(htmlContentText.split("\\s+").length);
                        content = count;
                    }else if("creationTime".equals(key)){
                        DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
                        String datestr = DATE_FORMATTER.format(new Date(categoryPage.getCreationTime()));
                        content = datestr;
                    } else {
                        Field field = CategoryPage.class.getDeclaredField(key);
                        field.setAccessible(true);
                        Object o = field.get(categoryPage);

                        if (o instanceof Collection) {
                            if (((Collection) o).isEmpty()) {
                                o = "";
                            }
                        }

                        if (o != null) {
                            content =  GSON.toJson(o).replaceAll("\"", "");
                        }
                    }

                    Cell cell = cellRow.createCell(cellCount++);
                    cell.setCellValue(content);
                }
            }
        } while (categoryPages.size() == 100);

        File outputfile = File.createTempFile("categoryPage", ".xlsx");
        FileOutputStream fileOut = new FileOutputStream(outputfile.getPath());
        workbook.write(fileOut);
        fileOut.close();

        String key = ENV + "/temp/xls/" + System.currentTimeMillis() + ".xlsx";
        awsS3Manager.uploadFile(domain.getAwsBucket(), key, outputfile);
        logger.info(outputfile.getPath());

        outputfile.delete();
        return awsS3Manager.getPublicBucketDownloadUrl(domain.getAwsBucket(), key);

    }

    private String filterRegex(Pattern latexPattern, String questionBodyText){
        StringBuffer buffer = new StringBuffer();
        Matcher matcher = latexPattern.matcher(questionBodyText);
        while (matcher.find()) {
            matcher.appendReplacement(buffer,"");
        }
        matcher.appendTail(buffer);
        return buffer.toString();
    }

    private String getDescriptionParsedText(String description){

        return Jsoup.parse(StringUtils.defaultIfEmpty(description)).text();
    }


    private byte[] getCategoryPageCsvBytes(List<CategoryPage> categoryPages, Map<String, String> category, Map<String, String> headerMap) throws IllegalAccessException, NoSuchFieldException {
        List<Map<String, String>> list = new ArrayList<>();
        for (CategoryPage categoryPage : categoryPages) {
            Map<String, String> map = new LinkedHashMap<>();

            CategoryPageHeader header = categoryPage.getHeader();
            String headerDescriptionText = header != null ? getDescriptionParsedText(header.getDescription()) : "";

            CategoryPageFooter footer = categoryPage.getFooter();
            String footerDescriptionText = footer != null ? getDescriptionParsedText(footer.getDescription()) : "";

            for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                String key = entry.getKey();
                if ("category".equals(key)) {
                    String cat = category.get(categoryPage.getCategoryId());
                    map.put(key, cat);
                } else if ("id".equals(key)) {
                    map.put(key, categoryPage.getId());
                } else if ("header_title".equals(key) ) {
                    map.put(key, header != null ? StringUtils.defaultIfEmpty(header.getTitle()) : "");
                } else if ("header_sub_title".equals(key)) {
                    map.put(key, header != null ? StringUtils.defaultIfEmpty(header.getSubTitle()) : "");
                } else if ("header_description".equals(key)) {
                    map.put(key, headerDescriptionText);
                } else if ("header_description_word_count".equals(key)) {
                    String count = StringUtils.isEmpty(headerDescriptionText) ? "0" : String.valueOf(headerDescriptionText.split("\\s+").length);
                    map.put(key, count);
                } else if ("footer_title".equals(key)) {
                    map.put(key, footer != null ? StringUtils.defaultIfEmpty(footer.getTitle()) : "");
                } else if ("footer_description".equals(key)) {
                    map.put(key, footerDescriptionText);
                } else if ("footer_description_word_count".equals(key)) {
                    String count = StringUtils.isEmpty(footerDescriptionText) ? "0" : String.valueOf(footerDescriptionText.split("\\s+").length);
                    map.put(key, count);
                }else if("creationTime".equals(key)){
                    DATE_FORMATTER.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
                    String datestr = DATE_FORMATTER.format(new Date(categoryPage.getCreationTime()));
                    map.put(key,datestr);
                } else {
                    Field field = CategoryPage.class.getDeclaredField(key);
                    field.setAccessible(true);
                    Object o = field.get(categoryPage);

                    if (o instanceof Collection) {
                        if (((Collection) o).isEmpty()) {
                            o = "";
                        }
                    }

                    if (o != null) {
                        map.put(field.getName(), GSON.toJson(o).replaceAll("\"", ""));
                    } else {
                        map.put(field.getName(), "");
                    }
                }
            }
            list.add(map);
        }

        StringBuilder builder = new StringBuilder();
        for (Map<String, String> map : list) {
            builder.append(map.values()
                    .stream()
                    .map(e -> "\"" + e + "\"")
                    .collect(Collectors.joining(",")))
                    .append(System.lineSeparator());
        }

        return builder.toString().getBytes();
    }

    private Map<String, String> getCategoryPageCsvHeaderMap() {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("id", "id");
        map.put("category", "Category");
        map.put("disabled", "Is Page Disabled");
        map.put("frequency", "changefreq");
        map.put("priority", "Priority");
        map.put("name", "Name");
        map.put("title", "Meta Title");
        map.put("canonicalUrl", "Meta Canonical");
        map.put("url", "Page Url");
        map.put("description", "Meta Description");
        map.put("metaKeywords", "Meta Keywords");
        map.put("alternateLinks", "Meta Alternate");
        map.put("schemaKeywords", "Schema Keywords");
        map.put("customTags", "Tags");
        map.put("target", "Target");
        map.put("grade", "Grade");
        map.put("subject", "Subject");
        map.put("pageType", "Page Type");
//        map.put("listModules", "List Modules");
//        map.put("sidebarModules", "Sidebar Modules");
        map.put("header_title", "Header Title");
        map.put("header_sub_title", "Header Sub Title");
        map.put("header_description", "Header Description");
        map.put("header_description_word_count", "Header Description Word Count");
        map.put("question", "question");
        map.put("question_word_count", "Question Word Count");
        map.put("footer_title", "Footer Title");
        map.put("footer_description", "Footer Description");
        map.put("footer_description_word_count", "Footer Description Word Count");
        map.put("solutions", "Solutions");
        map.put("solutions_word_count", "Solutions Word Count");
        map.put("html_content", "Html Content");
        map.put("html_content_word_count", "Html Content Word Count");
        map.put("creationTime","creation_date");
//        map.put("footer", "Footer");
//        map.put("metaTags", "Meta Tags");

        return map;
    }

    public  List<Map<String, String>> getSubListUrlObjectFromCSV(MultipartFile multipart) throws IOException, VException {

        File file = CommonUtils.multipartToFile(multipart);

        if(file == null)
            throw new VException(ErrorCode.SERVICE_ERROR, "File is missing");

        if(!file.getName().toLowerCase().endsWith(".csv"))
            throw new VException(ErrorCode.SERVICE_ERROR, "File format not supported");

        List<Map<String, String>> records = new ArrayList<>();
        try{
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();

            while(StringUtils.isNotEmpty(line)){

                logger.info("row: {}", line);

                String[] values = line.trim().split(",");

                if(line.trim().length() < 2 || values.length < 2)
                    break;

                String title = values[0].trim();
                String url = values[1].trim();

                Map<String, String> subListObject = new HashMap<>();
                subListObject.put("title", title);
                subListObject.put("url", url);
                records.add(subListObject);

                line = reader.readLine();
            }
        }catch (FileNotFoundException e) {
            logger.error("Error while processing csv file", e);
        }

        return records;
    }
    public Set<String> getMasterListUrlObjectFromCSV(MultipartFile multipart, SeoDomain domain, String masterTitle, String masterUrl, MasterSidebarUploadOptions masterSidebarUploadOptions) throws IOException, VException {

        File file = CommonUtils.multipartToFile(multipart);

        if(file == null)
            throw new VException(ErrorCode.SERVICE_ERROR, "File is missing");

        if(!file.getName().toLowerCase().endsWith(".csv"))
            throw new VException(ErrorCode.SERVICE_ERROR, "File format not supported");

        ArrayList<Map<String, String>> masterSidebarModules = new ArrayList<>();

        Set<String> pageUrlMainSet = new HashSet<>();
        Set<String> pageUrlTempSet = new HashSet<>();

        List<CategoryPage> categoryPageList = new ArrayList<>();
        try{
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();

            boolean header = true;

            while(StringUtils.isNotEmpty(line)){

                String[] values = line.trim().split(",");

                if(StringUtils.isEmpty(line.trim().replaceAll(",", "")) || values.length < 1)
                    break;

                if(header) {
                    header = false;
                    line = reader.readLine();
                    continue;
                }

                String pageUrl = values[0];

                String linkTtitle = "";
                String linkUrl = "";
                if(values.length > 1) {
                    linkTtitle = values[1];
                    linkUrl = values[2];
                }

                if(StringUtils.isNotEmpty(pageUrl)) {
                    pageUrl = pageUrl.trim();
                    pageUrlTempSet.add(pageUrl);
                    pageUrlMainSet.add(pageUrl);
                }

                if(StringUtils.isNotEmpty(linkTtitle) && StringUtils.isNotEmpty(linkUrl)) {
                    Map<String, String> stringMap = new HashMap<>();
                    stringMap.put("title", linkTtitle);
                    stringMap.put("href", linkUrl);
                    masterSidebarModules.add(stringMap);
                }

                line = reader.readLine();

                if(pageUrlTempSet.size() == 100){
                    categoryPageList.addAll(categoryPageDAO.getCategoryPageByUrls(pageUrlTempSet, domain));
                    pageUrlTempSet.clear();
                }
            }

            if(pageUrlTempSet.size() > 0){
                categoryPageList.addAll(categoryPageDAO.getCategoryPageByUrls(pageUrlTempSet, domain));
            }

            for(CategoryPage categoryPage : categoryPageList){

                //Do not add if option is IGNORE and sidebar module is not empty
                if(!(MasterSidebarUploadOptions.IGNORE.equals(masterSidebarUploadOptions)
                        && (CollectionUtils.isNotEmpty(categoryPage.getSidebarModules())
                        && CollectionUtils.isNotEmpty(categoryPage.getSidebarModules().get(0).getLinks())))){

                    CategoryPageListModule module = null;

                    if(CollectionUtils.isNotEmpty(masterSidebarModules)){
                        module = new CategoryPageListModule();
                        module.setTitle(masterTitle);
                        module.setHref(masterUrl);
                        module.setLinks(masterSidebarModules);
                    }

                    categoryPage.setMasterSidebarModules(module);

                    if(MasterSidebarUploadOptions.OVERRIDE.equals(masterSidebarUploadOptions))
                        categoryPage.setSidebarModules(null);

                    categoryPageDAO.updateCategoryPage(categoryPage);

                }

            }

        }catch (FileNotFoundException e) {
            logger.error("Error while processing csv file", e);
        }

        return pageUrlMainSet;

    }

    public BlogHomePageResponse getBlogHomepage(SeoDomain domain, int limit) {

        String blogHomePageResponseRedisKey = BLOG_HOME_PAGE_RESPONSE_REDIS_KEY_PREFIX + domain;
        try{
            String responseFromRedis = redisDAO.get(blogHomePageResponseRedisKey);
            if(StringUtils.isNotEmpty(responseFromRedis)) {
                return GSON.fromJson(responseFromRedis, BlogHomePageResponse.class);
            }
        }catch (Exception e){
            logger.error("Error fetching from redis", e);
        }

        Category blogCategory = categoryDAO.getCategoryByName("Blog", domain);
        List<CategoryPage> latestCategoryPages = categoryPageDAO.getLatestBlogsMetadata(domain, blogCategory.getId(), 0, limit);

        for (CategoryPage page : latestCategoryPages){
            updateBlogPageUrl(page);
        }

        Type _type = new TypeToken<ArrayList<BlogMetadataPojo>>() {
        }.getType();

        List<BlogMetadataPojo> latestBlogsMetadataPojo = GSON.fromJson(GSON.toJson(latestCategoryPages), _type);

        setBlogDefaultvalues(latestBlogsMetadataPojo);

        List<BlogCategory> blogCategories = blogCategoryDao.getBlogCategoriesByDomain(domain);

        List<BlogCategoryResponse> categoryPagesGroup = new ArrayList<>();
        for(BlogCategory category : blogCategories){

            List<BlogMetadataPojo> blogs = getBlogArticleByCategoryMetadata(category.getDomain(), category.getId(), _type, 0, limit);

            if(CollectionUtils.isEmpty(blogs))
                continue;

            setBlogDefaultvalues(blogs);

            Long recordCount = categoryPageDAO.getBlogCategoryPagesCount(domain, category.getId());

            BlogCategoryResponse blogCategoryResponse = new BlogCategoryResponse.Builder()
                    .id(category.getId())
                    .name(category.getName())
                    .totalBlogs(recordCount)
                    .blogs(blogs)
                    .build();

            categoryPagesGroup.add(blogCategoryResponse);

        }

        BlogArticlePojo req = new BlogArticlePojo();
        req.setDomain(domain);
        req.setBlogPageType(BlogPageType.BLOG_HOME_PAGE);

        BlogArticle blogArticle = blogArticleDao.getBlogArticle(req);
        if(blogArticle==null)
            blogArticle = new BlogArticle();

        BlogArticlePojo articlePojo = GSON.fromJson(GSON.toJson(blogArticle), BlogArticlePojo.class);

        BlogHomePageResponse blogHomePageResponse = new BlogHomePageResponse.Builder()
                .latestBlogs(latestBlogsMetadataPojo)
                .categories(categoryPagesGroup)
                .article(articlePojo)
                .build();

        try{
            redisDAO.setex(blogHomePageResponseRedisKey, GSON.toJson(blogHomePageResponse), DateTimeUtils.SECONDS_PER_DAY);
        }catch (Exception e){
            logger.error("Error fetching from redis", e);
        }

        return blogHomePageResponse;
    }

    private void setBlogDefaultvalues(List<BlogMetadataPojo> latestBlogsMetadataPojo) {

        for (BlogMetadataPojo blogMetadataPojo : latestBlogsMetadataPojo){

            if(StringUtils.isEmpty(blogMetadataPojo.getFeatureImageTag())){
                blogMetadataPojo.setFeatureImageTag(blogMetadataPojo.getTitle());
            }

        }
    }

    private List<BlogMetadataPojo> getBlogArticleByCategoryMetadata(SeoDomain domain, String categoryId, Type _type, int start, int limit) {
        List<CategoryPage> categoryPages = categoryPageDAO.getBlogCategoryHomeMetadata(domain, categoryId, start, limit);

        for (CategoryPage page : categoryPages){
            updateBlogPageUrl(page);
        }

        return GSON.fromJson(GSON.toJson(categoryPages), _type);
    }

    public BlogCategoryResponse getBlogCategoryHomepage(SeoDomain domain, String categoryName, Integer start, Integer limit, Boolean totalCountRequired) throws VException {

        BlogCategory blogCategory = blogCategoryDao.getBlogCategoryByName(domain, categoryName);

        if(blogCategory == null)
            throw new VException(ErrorCode.NOT_FOUND_ERROR, "Category not found with the given id");

        List<CategoryPage> categoryPages = categoryPageDAO.getBlogCategoryHomeMetadata(domain, blogCategory.getId(), start, limit);

        Long recordCount = null;

        if(totalCountRequired)
            recordCount = categoryPageDAO.getBlogCategoryPagesCount(domain, blogCategory.getId());

        for (CategoryPage page : categoryPages){
            updateBlogPageUrl(page);
        }

        Type _type = new TypeToken<ArrayList<BlogMetadataPojo>>() {
        }.getType();

        List<BlogMetadataPojo> metadataPojos = GSON.fromJson(GSON.toJson(categoryPages), _type);

        setBlogDefaultvalues(metadataPojos);

        BlogArticlePojo req = new BlogArticlePojo();
        req.setDomain(domain);
        req.setBlogPageType(BlogPageType.BLOG_CATEGORY_PAGE);
        req.setBlogCategoryId(blogCategory.getId());

        BlogArticle blogArticle = blogArticleDao.getBlogArticle(req);
        if(blogArticle==null)
            blogArticle = new BlogArticle();

        BlogArticlePojo articlePojo = GSON.fromJson(GSON.toJson(blogArticle), BlogArticlePojo.class);

        BlogCategoryResponse blogCategoryResponse = new BlogCategoryResponse.Builder()
                .id(blogCategory.getId())
                .name(blogCategory.getName())
                .totalBlogs(recordCount)
                .blogs(metadataPojos)
                .article(articlePojo)
                .build();

        return blogCategoryResponse;

    }

    public List<BlogCategory> getBlogCategories(SeoDomain domain) {

        try {
            String data = redisDAO.get(BLOG_CATEGORY_REDIS_KEY);
            if(StringUtils.isNotEmpty(data)) {
                Type _type = new TypeToken<ArrayList<BlogCategory>>() {
                }.getType();
                return GSON.fromJson(data, _type);
            }
        }catch (Exception e){
            logger.error("Error fetching from redis", e);
        }

        List<BlogCategory> blogCategories = blogCategoryDao.getBlogCategoriesByDomain(domain);

        try {
            redisDAO.setex(BLOG_CATEGORY_REDIS_KEY, GSON.toJson(blogCategories), DateTimeUtils.SECONDS_PER_DAY);
        }catch (Exception e){
            logger.error("Error setting in redis", e);
        }
        return blogCategories;

    }

    public String addBlogCategories(SeoDomain domain, BlogCategory blogCategory) throws VException {

        blogCategory.setDomain(domain);

        if(StringUtils.isEmpty(blogCategory.getName())){
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Category name cannot be empty");
        }

        cleanBlogCategoryName(blogCategory);

        BlogCategory category = blogCategoryDao.getBlogCategoryByName(domain, blogCategory.getName());
        if(category != null)
            throw new VException(ErrorCode.ALREADY_EXISTS, "Category name already exists");

        return blogCategoryDao.upsertBlogCategories(blogCategory);

    }

    private void cleanBlogCategoryName(BlogCategory blogCategory) {
        String name = blogCategory.getName().trim();

        while(name.contains("  ")){
            name = name.replaceAll("  "," ");
        }

        blogCategory.setName(name.replace(" ","-"));
    }

    public String updateBlogCategories(SeoDomain domain, BlogCategory blogCategory) throws VException {

        blogCategory.setDomain(domain);

        if(StringUtils.isEmpty(blogCategory.getId()) && StringUtils.isEmpty(blogCategory.getName())){
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Category Name/Id cannot be empty");
        }

        cleanBlogCategoryName(blogCategory);

        BlogCategory category = blogCategoryDao.getBlogCategoryByName(domain, blogCategory.getName());
        if(category != null)
            throw new VException(ErrorCode.ALREADY_EXISTS, "Category name already exists");

        category = blogCategoryDao.getBlogCategoryById(blogCategory.getId());
        if(category == null)
            throw new VException(ErrorCode.NOT_FOUND_ERROR, "Category doesnt exists");

        category.setName(blogCategory.getName());

        return blogCategoryDao.upsertBlogCategories(blogCategory);

    }

    public BlogCategoryResponse  getBlogLatestHomepage(SeoDomain domain, Integer start, Integer size, Boolean totalCountRequired) {

        Category blogCategory = categoryDAO.getCategoryByName("Blog", domain);
        List<CategoryPage> latestCategoryPages = categoryPageDAO.getLatestBlogsMetadata(domain, blogCategory.getId(), start, size);

        for (CategoryPage page : latestCategoryPages){
            updateBlogPageUrl(page);
        }

        Long recordCount = null;

        if(totalCountRequired)
            recordCount = categoryPageDAO.getBlogCategoryPagesCount(domain, null);

        Type _type = new TypeToken<ArrayList<BlogMetadataPojo>>() {
        }.getType();

        List<BlogMetadataPojo> BlogMetadataPojo = GSON.fromJson(GSON.toJson(latestCategoryPages), _type);

        setBlogDefaultvalues(BlogMetadataPojo);

        BlogCategoryResponse blogCategoryResponse = new BlogCategoryResponse.Builder()
                .name("Latest Blogs")
                .totalBlogs(recordCount)
                .blogs(BlogMetadataPojo)
                .build();

        return blogCategoryResponse;
    }

    public String upsertBlogArticle(BlogArticlePojo req) throws VException {

        logger.info("upsertBlogArticle Req : {}", req.toString());

        req.verify();

        BlogArticle blogArticle = blogArticleDao.getBlogArticle(req);

        if(blogArticle == null) {
            blogArticle = new BlogArticle();
            blogArticle.setDomain(req.getDomain());
            blogArticle.setBlogPageType(req.getBlogPageType());

            if(BlogPageType.BLOG_CATEGORY_PAGE.equals(req.getBlogPageType()))
                blogArticle.setBlogCategoryId(req.getBlogCategoryId());
        }

        blogArticle.setTitle(req.getTitle());
        blogArticle.setDescription(req.getDescription());

        return blogArticleDao.upsert(blogArticle);

    }

    public BlogArticlePojo getBlogArticle(BlogArticlePojo req) throws VException {

        if(req.getDomain() == null || req.getBlogPageType() == null ){
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Invalid request");
        }else if(BlogPageType.BLOG_CATEGORY_PAGE.equals(req.getBlogPageType()) && StringUtils.isEmpty(req.getBlogCategoryId())){
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "blog category id is must for the page type BLOG_CATEGORY_PAGE");
        }

        BlogArticle blogArticle = blogArticleDao.getBlogArticle(req);

        if(blogArticle == null)
            return new BlogArticlePojo();

        return GSON.fromJson(GSON.toJson(blogArticle), BlogArticlePojo.class);

    }

    public String regenerateWebpAndPngAsync(SeoDomain domain, String url) throws VException {

        if(StringUtils.isEmpty(url))
            return  "URL cannot be empty";

        String redisKey = getWebpRedissKey(domain,url);

        String isProcessing = redisDAO.get(redisKey);

        if(StringUtils.isNotEmpty(isProcessing)){
            return "Conversion request is already made and the image is processing... please refresh the tools page after some time";
        }

        boolean isPageExist = categoryPageDAO.checkIfCategoryPageExistsByUrl(domain, url);

        if(!isPageExist)
            return "No record found with the given url : " + url + " and domain : " + domain;

        String key = ENV + "/content-files" + url + ".pdf";

        logger.info("key : {}", key);

        String SEO_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");

        if(!awsS3Manager.checkKeyExists(SEO_BUCKET, key)){
            return "PDF not found with the given key, please re-upload it";
        }

        Map<String, String> payload =  new HashMap<>();
        payload.put("key", key);

        awsSQSManager.sendToSQS(SQSQueue.SEO_PAGE_REGENERATE_CONTENT_IMAGE,
                SQSMessageType.SEO_PAGE_REGENERATE_CONTENT_IMAGE, new Gson().toJson(payload), SQSMessageType.SEO_PAGE_REGENERATE_CONTENT_IMAGE.name());

        redisDAO.setex(redisKey, "TRUE", 600);

        return "Request accepted, Please refresh the tools page after some time to see if the images are generated.";

    }

    public String checkWebpStatus(SeoDomain domain, String url) throws VException {

        String key = getWebpRedissKey(domain,url);

        String isProcessing = redisDAO.get(key);

        if(StringUtils.isNotEmpty(isProcessing))
            return "Processing..";

        return "Completed.";

    }

    public void regenerateWebpAndPng(String key) throws VException, IOException {

        logger.info("key : {}", key);

        String SEO_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");

        InputStream objectData = awsS3Manager.getObjectInputStream(SEO_BUCKET, key);

        File file = File.createTempFile(key, "pdf");

        FileUtils.copyInputStreamToFile(objectData, file);

        PutObjectRequest putObjectRequest = new PutObjectRequest(SEO_BUCKET, key, file);
        putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead);

        awsS3Manager.uploadPdf(SEO_BUCKET, key, file);

    }

    private List<Map<String, String>> syncContentImageUrls(CategoryPage categoryPage) {

        String categoryPageUrl =  categoryPage.getUrl();
        SeoDomain domain = categoryPage.getDomain();

        if (categoryPageUrl.startsWith("/")) {
            categoryPageUrl = categoryPageUrl.replaceFirst("/", "");
        }

        categoryPageUrl = categoryPageUrl.endsWith("/") ? categoryPageUrl : categoryPageUrl + "/";
        String prefix = ENV + "/content-images/" + categoryPageUrl;

        String bucket = "prod".equalsIgnoreCase(ENV) ? domain.getAwsBucket() : SEO_BUCKET;

        List<Map<String, String>> urlList =getCategoryPageContentUrlMap(bucket, prefix);


        List<CategoryPageContentModule> contentModules = categoryPage.getContentModules();

        String tag = "";
        if(CollectionUtils.isNotEmpty(contentModules)){
            CategoryPageContentModule contentModule = contentModules.get(0);
            String imageTags = contentModule.getImageTags();
            if(StringUtils.isNotEmpty(imageTags)) {
                tag = imageTags.substring(0, imageTags.lastIndexOf(" ") + 1);
            }
        }

        List<CategoryPageContentModule> contentModulesNewList = new ArrayList<>();

        int index = 1;
        for(Map<String, String> map : urlList){

            CategoryPageContentModule contentModule = new CategoryPageContentModule();
            String png = map.get("png");
            String webp = map.get("webp");

            if(StringUtils.isNotEmpty(tag))
                contentModule.setImageTags(tag + "part-" + index++);

            contentModule.setImageUrl(png);
            contentModule.setImageUrlWebp(webp);

            contentModulesNewList.add(contentModule);
        }

        categoryPage.setContentModules(contentModulesNewList);

        categoryPageDAO.updateCategoryPage(categoryPage);
        return urlList;
    }

    private String getWebpRedissKey(SeoDomain domain, String url) {
        String prefix = getContentImageProcessingPrefixKey(domain);
        return prefix + "_" + url;
    }

    private String getContentImageProcessingPrefixKey(SeoDomain domain){
        return "SEO_WEBP_CONTENT_IMAGE_PROCESSING_" + domain.name();
    }


    public void clearCloudFrontCache(String key){

        awslambdaManager.invokeCloudfrontClearCacheLambda(key);

    }
    public CategoryPage generateInternalLinks(String categoryPageId) {

        CategoryPage categoryPage = categoryPageDAO.getEntityById(categoryPageId, CategoryPage.class);

        SeoDomain domain = categoryPage.getDomain();

        String htmlContent = categoryPage.getHtmlContent() != null ? categoryPage.getHtmlContent() : "";
        String headerDescrription = categoryPage.getHeader() != null && categoryPage.getHeader().getDescription() != null ? categoryPage.getHeader().getDescription() : "";
        String footerDescrription = categoryPage.getFooter() != null && categoryPage.getFooter().getDescription() != null ? categoryPage.getFooter().getDescription() : "";

        headerDescrription = " ".concat(headerDescrription.concat(" "));
        footerDescrription = " ".concat(footerDescrription.concat(" "));
        htmlContent = " ".concat(htmlContent.concat(" "));

        String headerDescriptionTemp = "".concat(headerDescrription);
        String footerDescriptionTemp = "".concat(footerDescrription);
        String htmlContentTemp = "".concat(htmlContent);

        int start = 0;
        int size =500;

        String allInOne = headerDescrription.concat(footerDescrription).concat(htmlContentTemp);

        logger.info("Fetch all existing links");
        List<String> allInternalLinksExisting = getAllInternalLinks(allInOne);

        headerDescriptionTemp = maskUsedLinks(allInternalLinksExisting,headerDescriptionTemp);
        footerDescriptionTemp = maskUsedLinks(allInternalLinksExisting,footerDescriptionTemp);
        htmlContentTemp = maskUsedLinks(allInternalLinksExisting,htmlContentTemp);

        List<String> allInternalLinksText = new ArrayList<>();

        allInternalLinksExisting.forEach(s -> allInternalLinksText.add(Jsoup.parse(StringUtils.defaultIfEmpty(s.toLowerCase())).text()));

        List<CategoryPageTitleUrlMapping> titleUrlMapList;
        List<String> allInternalLinks= new ArrayList<>();

        logger.info("Analysing record for internal linking");

        do{

            Set<String> fields = new HashSet<>();
            fields.add(CategoryPage.Constants.TITLE);
            fields.add(CategoryPage.Constants.URL);
            List<CategoryPage>  categoryPages;
            categoryPages = categoryPageDAO.findCategoryPagesForInternalLinking(domain, categoryPage.getPageType().toString(), start,size, fields, CategoryPage.Constants.TITLE, Sort.Direction.DESC, categoryPage.getTitle());

            logger.info("Batch of records fetch : {}", start);

            Type _type = new TypeToken<ArrayList<CategoryPageTitleUrlMapping>>() {
            }.getType();
            titleUrlMapList = GSON.fromJson(GSON.toJson(categoryPages), _type);

            headerDescriptionTemp = maskUsedLinks(allInternalLinks,headerDescriptionTemp);
            logger.info("Header content masked");

            logger.info("Generating internal link for header");
            headerDescrription = generateInternalLinks(titleUrlMapList, allInternalLinks, allInternalLinksText, headerDescrription, headerDescriptionTemp);

            footerDescriptionTemp = maskUsedLinks(allInternalLinks,footerDescriptionTemp);
            logger.info("Footer content masked");

            logger.info("Generating internal link for footer");
            footerDescrription = generateInternalLinks(titleUrlMapList, allInternalLinks, allInternalLinksText, footerDescrription, footerDescriptionTemp);

            htmlContentTemp = maskUsedLinks(allInternalLinks,htmlContentTemp);
            logger.info("HTML content masked");

            logger.info("Generating internal link for html contents");
            htmlContent = generateInternalLinks(titleUrlMapList, allInternalLinks, allInternalLinksText, htmlContent, htmlContentTemp);

            start += size;
        }while (titleUrlMapList.size()==size);

        logger.info("Internal ling generated");

        categoryPage.getHeader().setDescription(headerDescrription.trim());
        categoryPage.getFooter().setDescription(footerDescrription.trim());
        categoryPage.setHtmlContent(htmlContent.trim());

        return categoryPage;

    }

    private String maskUsedLinks(List<String> allInternalLinks, String tempDescription) {

        for (String link : allInternalLinks){
            String stars = new String(new char[link.length()]).replace('\0', '*');

            if(tempDescription.toLowerCase().contains(link.toLowerCase()))
                tempDescription = tempDescription.replaceAll("(?i)" + link, stars);

            String text = Jsoup.parse(StringUtils.defaultIfEmpty(link)).text();
            stars = new String(new char[text.length()]).replace('\0', '*');
            if(tempDescription.toLowerCase().contains(text.toLowerCase()))
                tempDescription = tempDescription.replaceAll("(?i)" + text, stars);

        }

        return tempDescription;

    }

    private String generateInternalLinks(List<CategoryPageTitleUrlMapping> titleUrlMapList,List<String> allUsedInternalLinks, List<String> allInternalLinksText, String description, String tempDescription) {

        for (CategoryPageTitleUrlMapping titleUrlMap : titleUrlMapList){

            String titleMatcher = titleUrlMap.getTitle();

            if(tempDescription.toLowerCase().contains(titleMatcher.toLowerCase())){

                String text = Jsoup.parse(StringUtils.defaultIfEmpty(titleUrlMap.getTitle())).text();

                if(allInternalLinksText.contains(text.toLowerCase()))
                    continue;

                int index = tempDescription.toLowerCase().indexOf(titleMatcher.toLowerCase());

                String subString = description.substring(index);
                String documentTitle = subString.substring(0, titleMatcher.trim().length());
                description = description.substring(0, index);

                String linkTag = generateInternalLinkTag(documentTitle, titleUrlMap.getUrl());
                description = description.concat(subString.replaceFirst("(?i)" + titleMatcher.trim(), linkTag));

                String stars = new String(new char[linkTag.length()]).replace('\0', '*');
                tempDescription = tempDescription.replaceFirst("(?i)" + titleMatcher.trim(),stars);
                tempDescription = tempDescription.replaceAll("(?i)" + titleMatcher.trim(),stars.substring(0,titleMatcher.trim().length()));

                allInternalLinksText.add(Jsoup.parse(StringUtils.defaultIfEmpty(linkTag.toLowerCase())).text());
                allUsedInternalLinks.add(linkTag.toLowerCase());
            }
        }

        return description;
    }

    private List<String> getAllInternalLinks(String content){

        List<String> internalLinks = new ArrayList<>();

        String temp = "".concat(content.toLowerCase());
        while (temp.contains("<a href")) {
            String link = temp.substring(temp.indexOf("<a href"), temp.indexOf("</a>") + 4);
            internalLinks.add(link.toLowerCase());
            temp = temp.toLowerCase().substring(temp.toLowerCase().indexOf(link) + link.length());
        }

        return internalLinks;

    }

    private String generateInternalLinkTag(String title, String url) {

        if("prod".equalsIgnoreCase(ENV)){
            url = "https://www.vedantu.com" + url;
        }else {
            url = "https://fos-" + ENV + ".vedantu.com" + url;
        }

        return  "<a href='" + url + "'>" + title + "</a>";
    }

    public Set<String> getAllPdfMissingType3Pages(Integer startMain, Integer page) {

        int size = 100, start = 0;

        if(startMain != null)
            start = startMain;

        List<CategoryPage> categoryPages;
        Set<String> urls = new HashSet<>();

        do {

            if(page != null && start >= (page*size))
                break;

            categoryPages = categoryPageDAO.findCategoryPages(SeoDomain.VEDANTU,null, null, "PageType3", null, null, false, start, size);
            start += size;

            for(CategoryPage categoryPage : categoryPages){

                String url = categoryPage.getUrl();

                String key = ENV + "/content-files" + url + ".pdf";

                String SEO_BUCKET = ConfigUtils.INSTANCE.getStringValue("aws.seo.bucket");

                if(!awsS3Manager.checkKeyExists(SEO_BUCKET, key)){
                    urls.add(url);
                }

            }

        } while (categoryPages.size() == size);

        return urls;
    }

    public List<CategoryPage> migrateBulkUploadContents(Set<String> categoryIds, boolean canUpdate) {

        List<CategoryPage> categoryPages = categoryPageDAO.getCategoryPageByIds(categoryIds);

        for(CategoryPage page : categoryPages){

            String title = page.getTitle();

            if(title.contains("-")) {
                page.setTitle(title.replaceAll("-"," "));
                if (canUpdate)
                    categoryPageDAO.updateCategoryPage(page);
            }

        }

        return categoryPages;
    }

    public List<SeoQuestion> getSeoQuestions(List<String> questionIds) throws VException {

        if(CollectionUtils.isEmpty(questionIds))
            return new ArrayList<>();

        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "cmds/question/getQuestionsSeo",
                HttpMethod.POST, new Gson().toJson(questionIds), true);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type _type = new TypeToken<List<SeoQuestion>>() {
        }.getType();
        List<SeoQuestion> seoQuestionList = GSON.fromJson(jsonString, _type);

        return seoQuestionList;
    }

    public List<BlogMetadataPojo> getNewsMetadata(SeoDomain domain, String target, Integer start, Integer size) {
        List<CategoryPage> categoryPages = categoryPageDAO.getNewsMetadata(domain, CategoryPageType.NewsPage,target, start, size);
        for (CategoryPage page : categoryPages){
            updateBlogPageUrl(page);
        }
        return getBlogPageMetadataFromCategoryPage(categoryPages);
    }

    public Long getNewsCountByTarget(SeoDomain domain, String target) {
        return categoryPageDAO.getNewsMetadataByTargetCount(domain, CategoryPageType.NewsPage,target);
    }

    private List<BlogMetadataPojo> getBlogPageMetadataFromCategoryPage(List<CategoryPage> categoryPages) {
        Type _type = new TypeToken<ArrayList<BlogMetadataPojo>>() {
        }.getType();

        return GSON.fromJson(GSON.toJson(categoryPages), _type);
    }

}
