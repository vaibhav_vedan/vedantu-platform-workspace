package com.vedantu.growth.seo;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.cmds.pojo.SeoQuestion;
import com.vedantu.cmds.pojo.SolutionFormat;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.enums.RedisPrefixType;
import com.vedantu.growth.managers.ESManager;
import com.vedantu.growth.managers.RedisDataManager;
import com.vedantu.growth.seo.dao.CategoryDAO;
import com.vedantu.growth.seo.dao.CategoryPageDAO;
import com.vedantu.growth.seo.entity.Category;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.enums.ESSearchMode;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.request.SeoSearchReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SeoSearchEngineManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoSearchEngineManager.class);

    private static final Gson gson = new Gson();

    private static final String ELASTICSEARCH_HOST = ConfigUtils.INSTANCE.getStringValue("elasticsearch.host");
    private static final String SEO_ES_INDEX_NAME_PREFIX= "seo-pages_";
    private static final String SEO_ES_DOC_TYPE= "seo";

    private static final String env = ConfigUtils.INSTANCE.getStringValue("environment");

    public static final String CATEGORY_NAME = "categoryName";

    public static final String CATEGORY_PAGE_ID = "categoryPageId";
    public static final String CATEGORY_PAGE = "categoryPage";

    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private CategoryPageDAO categoryPageDAO;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private ESManager esManager;

    @Autowired
    private RedisDataManager redisDataManager;

    private static String  ES_SEARCH_RESULTS_REDIS_KEY = "ES_SEARCH_RESULTS_";

    public String getSearchResultsRedisKey(ESSearchMode esSearchMode, String categoryPageId) throws BadRequestException, InternalServerErrorException {
        RedisPrefixType prefixType = esSearchMode.equals(ESSearchMode.SIDEBAR_MODULE)?RedisPrefixType.SEO_SIDEBAR:RedisPrefixType.QNA_RELATED_Q;
        String customPrefix = redisDataManager.getPrefix(prefixType);
        return customPrefix + ES_SEARCH_RESULTS_REDIS_KEY + esSearchMode + "_" + categoryPageId;
    }

    public String loadToEs(int startIndex, int maxSize, SeoDomain domain, CategoryPageType type) throws VException {

        int size =100;
        int start = startIndex;
        List<CategoryPage> categoryPages;
        int i = 0;
        String index = getIndex();
        int indexValue = 1;
        List<Category> categories = new ArrayList<>();
        categories.addAll(categoryDAO.getAllCategories(SeoDomain.VEDANTU));
        //categories.addAll(categoryDAO.getAllCategories(SeoDomain.ACADSTACK));
        Map<String, Category> categoriesMap = categories.parallelStream()
                .collect(Collectors.toMap(Category::getId, category -> category));

        BulkRequest bulkRequest = new BulkRequest();
        do {

            categoryPages = categoryPageDAO.findCategoryPages(domain, null, null, type.name(), null, null, false, start, size);
            List<SeoQuestion> seoQuestionList = new ArrayList<>();
            List<String> questionIds = categoryPages.parallelStream()
                    .filter(v -> StringUtils.isNotEmpty(v.getQuestionId()))
                    .map(v ->  v.getQuestionId() )
                    .collect(Collectors.toList());
            List<SeoQuestion> seoQuestionListResponse = pageBuilderManager.getSeoQuestions(questionIds);
            if(CollectionUtils.isNotEmpty(seoQuestionListResponse)){
                seoQuestionList.addAll(seoQuestionListResponse);
            }
            Map<String, List<SeoQuestion>> mapData = seoQuestionList.parallelStream().collect(Collectors.groupingBy(SeoQuestion::getId));
            for (CategoryPage categoryPage : categoryPages){
                if(StringUtils.isNotEmpty(categoryPage.getQuestionId()) && mapData.containsKey(categoryPage.getQuestionId())){
                    List<SeoQuestion> seoQuestions = mapData.get(categoryPage.getQuestionId());
                    if(CollectionUtils.isNotEmpty(seoQuestions)){
                        SeoQuestion response = seoQuestions.get(0);
                        categoryPage.setTopic(response.getTopic());
                        categoryPage.setSQuestion(response.getQuestionBody().getNewText());
                        categoryPage.setSolutions(response.getSolutions());
                        List<SolutionFormat> list = response.getSolutions();
                        List<String> sList = new ArrayList<String>();
                        for (SolutionFormat s : list) {
                            if (s != null) {
                                sList.add(s.getNewText());
                            }
                        }
                        categoryPage.setSSolutions(sList);
                        categoryPage.setDifficulty(response.getDifficulty());
                    }else
                        continue;
                }
                JsonObject jsonObject = gson.fromJson(new Gson().toJson(categoryPage), JsonObject.class);
                if(categoriesMap.containsKey(categoryPage.getCategoryId()))
                    jsonObject.addProperty(CATEGORY_NAME,categoriesMap.get(categoryPage.getCategoryId()).getName());
                IndexRequest indexRequest = new IndexRequest(index, SEO_ES_DOC_TYPE, categoryPage.getCategoryId());
                indexRequest.source(gson.toJson(jsonObject), XContentType.JSON);
                bulkRequest.add(indexRequest);
                i++;
                if(i>=50){
                    BulkResponse bulkResponse = pushToEsBulkUpload(bulkRequest);
                    logger.info("=====bulkResponse Status: {},Took : {}", bulkResponse.status(),bulkResponse.getTook());
                    bulkRequest = new BulkRequest();
                    logger.info("=====index : {}", indexValue++);
                    i=0;
                }
            }
            start += size;
        } while (categoryPages.size()==size && start < (maxSize-size));
        if(CollectionUtils.isNotEmpty(bulkRequest.requests()))
            pushToEsBulkUpload(bulkRequest);
        return "Success";
    }

    private BulkResponse pushToEsBulkUpload(BulkRequest bulkRequest){

        logger.info("pushing to ES");
        try {
            return esManager.doBulkIndexRequest(bulkRequest);
        }catch (Exception e){
            logger.error("Error creating bulk index", e);
        }
        return null;
    }

    public Map<String, Object> getQnARelatedDocuments(SeoSearchReq seoSearchReq) throws BadRequestException, InternalServerErrorException, IOException {

        if(seoSearchReq.getSize() > SeoSearchReq.Constants.MAX_SIZE){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Size cannot be more than 20");
        }

        Map<String, Object> stringObjectMap= new HashMap<>();

        String cachedSearchResultKey = getSearchResultsRedisKey(ESSearchMode.RELATED_QNA_DOCS, seoSearchReq.getCategoryPageId());
        try{
            String cachedSearchResult = redisDAO.get(cachedSearchResultKey);
            if(StringUtils.isNotEmpty(cachedSearchResult)){
                Type _type = new TypeToken<List<CategoryPage>>(){}.getType();
                List<CategoryPage> seoSearchResponses = gson.fromJson(cachedSearchResult, _type);
                stringObjectMap.put("payload",seoSearchResponses);
                return stringObjectMap;
            }
        }catch (Exception e){
            logger.error("Error fetching from redis", e);
        }

        if(!canFetchFromES(ESSearchMode.RELATED_QNA_DOCS)){
            return new HashMap<>();
        }
        boolean canCache = true;
        SearchRequest searchRequest = constructQnARelatedDocsESQuery(seoSearchReq.getSearchString(), seoSearchReq.getStart(), seoSearchReq.getSize(), getIndex());
        List<CategoryPage> seoSearchResponses = new ArrayList<>();
        try {
            SearchResponse searchResponse = esManager.doSearchRequest(searchRequest);
            seoSearchResponses = processQnARelatedDocs(searchResponse,seoSearchReq.getCategoryPageId());
        }catch (Exception e){
            canCache = false;
            logger.error("Error fetching from ES", e);
        }

        stringObjectMap.put("payload",seoSearchResponses);

        if(canCache) {
            redisDAO.setex(cachedSearchResultKey, gson.toJson(seoSearchResponses), DateTimeUtils.SECONDS_PER_DAY);
        }

        return stringObjectMap;

    }

    private List<CategoryPage> processQnARelatedDocs(SearchResponse searchResponse, String categoryPageId) {
        List<CategoryPage> seoSearchResponses = new ArrayList<>();
        SearchHits searchHits =  searchResponse.getHits();
        for (SearchHit searchHit : searchHits) {
            String sourceAsString = searchHit.getSourceAsString();
            CategoryPage categoryPage = gson.fromJson(sourceAsString, CategoryPage.class);
            if (categoryPage.getId().equals(categoryPageId))
                continue;
            seoSearchResponses.add(categoryPage);
        }
        return seoSearchResponses;
    }

    private SearchRequest constructQnARelatedDocsESQuery(String searchString, Integer start, Integer size, String index) {
        BoolQueryBuilder boolQueryBuilderMust=new BoolQueryBuilder();
        Map<String,Float> fieldsMap = new HashMap<>();
        fieldsMap.put("topic",20f);
        fieldsMap.put("subject",10f);
        fieldsMap.put("target",5f);
        fieldsMap.put("grade",5f);
        QueryBuilder multiMatchQuery= QueryBuilders.multiMatchQuery(searchString).fields(fieldsMap);
        boolQueryBuilderMust.must(multiMatchQuery);
        QueryBuilder mustMatchDisabled= QueryBuilders.termQuery(CategoryPage.Constants.DISABLED,"false");
        boolQueryBuilderMust.must(mustMatchDisabled);
        QueryBuilder mustMatchType= QueryBuilders.termQuery(CategoryPage.Constants.PAGE_TYPE,CategoryPageType.QnA.toString().toLowerCase());
        boolQueryBuilderMust.must(mustMatchType);
        SearchSourceBuilder searchSourceBuilder=new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilderMust);
        searchSourceBuilder.from(start);
        searchSourceBuilder.size(size);
        String[] includeFields = {"id","url","sQuestion","questionBody"};
        String[] excludeFields = {};
        searchSourceBuilder.fetchSource(includeFields, excludeFields);

        SearchRequest intermediateSearchRequest=new SearchRequest();
        intermediateSearchRequest.source(searchSourceBuilder);
        intermediateSearchRequest.indices(index);
        logger.info("The intermediate search request is: {}", intermediateSearchRequest);
        return intermediateSearchRequest;
    }

    public boolean canFetchFromES(ESSearchMode esSearchMode) {

        try {
            String esUsageKey = ESManager.CAN_USE_ES_REDIS_KEY + esSearchMode;
            String esUsagePropertyString = redisDAO.get(esUsageKey);

            if(StringUtils.isNotEmpty(esUsagePropertyString)){
                Map<String, String> esUsageProperty = gson.fromJson(esUsagePropertyString, Map.class);
                String esDisabledPermanent = esUsageProperty.get(ESManager.ES_DISABLED_PERMANENT);
                String esDisabledTemporary = esUsageProperty.get(ESManager.ES_DISABLED_TEMPORARY);
                if(StringUtils.isNotEmpty(esDisabledPermanent) && Boolean.FALSE.toString().equalsIgnoreCase(esDisabledPermanent)){
                    return false;
                }

                if(StringUtils.isNotEmpty(esDisabledTemporary)){
                    if(Long.parseLong(esDisabledTemporary) > System.currentTimeMillis())
                        return false;
                    else {
                        esUsageProperty.remove(ESManager.ES_DISABLED_TEMPORARY);
                        redisDAO.set(esUsageKey, gson.toJson(esUsageProperty));
                    }
                }
            }
            return true;

        }catch (Exception e){
            logger.error("Error setting in redis", e);
        }
        return true;
    }

    private String getEsQueryFieldsRedisKey(ESSearchMode esSearchMode) {

        switch (esSearchMode){
            case RELATED_QNA_DOCS: return "SEARCH_FIELDS_RELATED_QNA_DOCS";
            case SIDEBAR_MODULE : return "SEARCH_FIELDS_SIDEBAR_MODULE";
            default: return "SEO_ES_SEARCH_FIELDS";
        }
    }

    public List<String> setSearchFilter(ESSearchMode esSearchMode, List<String> searchFields) throws BadRequestException, InternalServerErrorException {

       String key =  getEsQueryFieldsRedisKey(esSearchMode);

       redisDAO.del(key);

        searchFields.forEach(value -> {
            try {
                redisDAO.addToList(key, value);
            } catch (BadRequestException e) {
                logger.error("Error setting redis value",e);
            }
        });

        return redisDAO.getList(key,0, 20);

    }

    public void upsertESDocument(Map<String,Object> objectMap) throws VException {

        String categoryPageId = (String) objectMap.get(CATEGORY_PAGE_ID);
        CategoryPage categoryPage = pageBuilderManager.getCategoryPageById(categoryPageId);

        String index = getIndex();

        if(categoryPage.getDisabled() || !categoryPage.getActive()){
            DeleteRequest deleteRequest = new DeleteRequest(index,SEO_ES_DOC_TYPE,categoryPageId);
            esManager.doDeleteRequest(deleteRequest);
        }else {
            Category category = categoryDAO.getCategoryById(categoryPage.getCategoryId());
            String categoryName = category.getName();

            JsonObject jsonObject = gson.fromJson(new Gson().toJson(categoryPage), JsonObject.class);
            jsonObject.addProperty(CATEGORY_NAME, categoryName);

            IndexRequest indexRequest = new IndexRequest(index, SEO_ES_DOC_TYPE, categoryPageId);
            indexRequest.source(gson.toJson(jsonObject), XContentType.JSON);

            try {
                esManager.doIndexRequest(indexRequest);
            } catch (IOException e) {
                logger.error("Error in index creation",e);
            }
        }

    }

    public String getIndex(){

        return SEO_ES_INDEX_NAME_PREFIX + env.toLowerCase();
    }

    public void setElasticUsageProperty(ESSearchMode esSearchMode, String canUseES){
        try {

            Map<String, String> esUsageProperty = new HashMap<>();
            esUsageProperty.put(ESManager.ES_DISABLED_PERMANENT, canUseES);
            redisDAO.set(ESManager.CAN_USE_ES_REDIS_KEY + esSearchMode, gson.toJson(esUsageProperty));
        }catch (Exception e){
            logger.error("Error setting in redis", e);
        }
    }

    public Map<String, Object> searchDocumentsTest(Map<String, Object> searchPayload) {

        String index = getIndex();

        String url =ELASTICSEARCH_HOST + "/" + index + "/" + SEO_ES_DOC_TYPE + "/_search";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url,
                HttpMethod.POST, gson.toJson(searchPayload), false);
        String jsonString = resp.getEntity(String.class);
        return gson.fromJson(jsonString, Map.class);
    }

}
