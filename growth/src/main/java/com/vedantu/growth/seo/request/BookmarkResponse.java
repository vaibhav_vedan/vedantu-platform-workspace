package com.vedantu.growth.seo.request;

import lombok.Data;

@Data
public class BookmarkResponse {
    String addedBookmark;
    String removedBookmark;
}
