package com.vedantu.growth.seo.pageSpeed;

public class PerformanceOpportunity {

    private  String title;
    private String description;
    private String score;
    private String estimatedSavingsInSeconds;

    public PerformanceOpportunity(){
        super();
    }

    public PerformanceOpportunity(String title, String description, String score, String estimatedSavingsInSeconds){
        this.title = title;
        this.description = description;
        this.score = score;
        this.estimatedSavingsInSeconds = estimatedSavingsInSeconds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public String getEstimatedSavingsInSeconds() {
        return estimatedSavingsInSeconds;
    }

    public void setEstimatedSavingsInSeconds(String estimatedSavingsInSeconds) {
        this.estimatedSavingsInSeconds = estimatedSavingsInSeconds;
    }
}
