package com.vedantu.growth.seo.entity;

import lombok.*;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "CategoryPage")
@CompoundIndexes({
        @CompoundIndex(name = "user-category-1", def = "{'userId': 1,'categoryPageId': 1}", background = true),
        @CompoundIndex(name = "category-vote-1", def = "{'categoryPageId': 1, 'isUpvote': 1}", background = true)
})
public class CategoryPageVote extends AbstractSeoEntity {

    private Long userId;
    private String categoryPageId;
    private Boolean isUpvote = true;
    private Boolean isBookmarked = false;

    public CategoryPageVote(Long userId, String categoryPageId, Boolean isUpvote) {
        this.userId = userId;
        this.categoryPageId = categoryPageId;
        this.isUpvote = isUpvote;
    }

    public static class Constants {
        public static final String USER_ID = "userId";
        public static final String CATEGORY_PAGE_ID = "categoryPageId";
        public static final String IS_UPVOTE = "isUpvote";
        public static final String IS_BOOKMARKED = "isBookmarked";
    }

}
