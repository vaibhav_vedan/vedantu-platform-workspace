package com.vedantu.growth.seo.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SeoQuestionAndAnswerFilterRes {

    private Long totalSize;
    private List<SeoQuestionAndAnswerFilterMetadataRes> questionAndAnswers = new ArrayList<>();

    public void addQuestionAndAnswer(SeoQuestionAndAnswerFilterMetadataRes metadataRes){

        questionAndAnswers.add(metadataRes);
    }

}
