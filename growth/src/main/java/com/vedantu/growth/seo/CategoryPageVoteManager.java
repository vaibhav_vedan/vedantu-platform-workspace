package com.vedantu.growth.seo;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.seo.dao.CategoryPageVoteDao;
import com.vedantu.growth.seo.entity.CategoryPageVote;
import com.vedantu.growth.seo.pojo.CategoryPageVoteRequest;
import com.vedantu.growth.seo.response.CategoryPageVoteStatus;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryPageVoteManager {

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private CategoryPageVoteDao categoryPageVoteDao;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CategoryPageVoteManager.class);

    public CategoryPageVoteStatus votePage(CategoryPageVoteRequest categoryPageVoteRequest) throws ForbiddenException, BadRequestException, InternalServerErrorException {

        if (sessionUtils.getCurrentSessionData() == null)
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not authorised to perform this operation");

        Long userId = sessionUtils.getCallingUserId();
        String categoryPageId = categoryPageVoteRequest.getCategoryPageId();
        Boolean isUpVote = categoryPageVoteRequest.getIsUpvote();

        if (StringUtils.isEmpty(categoryPageId))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Category page id cannot be empty");

        if (isUpVote == null)
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "isUpVote boolean value required");

        CategoryPageVote categoryPageVote = categoryPageVoteDao.getByUserIdAndCategoryId(userId, categoryPageId);

        boolean isChanged = false;

        if (categoryPageVote != null) {
            if (categoryPageVote.getIsUpvote() != isUpVote) {
                categoryPageVote.setIsUpvote(isUpVote);
                categoryPageVoteDao.saveOrUpdateVote(categoryPageVote);
                isChanged = true;
            }
        } else {
            categoryPageVote = new CategoryPageVote(userId, categoryPageId, isUpVote);
            categoryPageVoteDao.saveOrUpdateVote(categoryPageVote);
            isChanged = true;
        }

        logger.info("vote updated for categoryPage : {}, isUpVote: {}", categoryPageId, isUpVote);

        Long count;
        if (isChanged)
            count = syncVoteTotalInRedis(categoryPageId);
        else {
            String countString = redisDAO.get(getCategoryPageVoteRedisKey(categoryPageId));

            if (StringUtils.isNotEmpty(countString))
                count = Long.parseLong(countString);
            else
                count = syncVoteTotalInRedis(categoryPageId);
        }

        CategoryPageVoteStatus categoryPageVoteStatus = new CategoryPageVoteStatus();
        categoryPageVoteStatus.setIsUserVoted(isUpVote);
        categoryPageVoteStatus.setTotalVotes(count);

        return categoryPageVoteStatus;
    }

    public CategoryPageVoteStatus getTotalVotes(String categoryPageId)
            throws BadRequestException, InternalServerErrorException {
        CategoryPageVoteStatus categoryPageVoteStatus = new CategoryPageVoteStatus();
        if (sessionUtils.getCurrentSessionData() != null) {
            Long userId = sessionUtils.getCallingUserId();
            CategoryPageVote categoryPageVote = categoryPageVoteDao.getByUserIdAndCategoryId(userId, categoryPageId);
            if (categoryPageVote != null) {
                categoryPageVoteStatus.setIsUserVoted(categoryPageVote.getIsUpvote());
                categoryPageVoteStatus.setIsBookmarked(categoryPageVote.getIsBookmarked());
            }
        }

        String countString = redisDAO.get(getCategoryPageVoteRedisKey(categoryPageId));
        if (StringUtils.isEmpty(countString)) {
            Long count = syncVoteTotalInRedis(categoryPageId);
            categoryPageVoteStatus.setTotalVotes(count);
            return categoryPageVoteStatus;
        }
        Long totalVoteCount = Long.valueOf(countString);
        categoryPageVoteStatus.setTotalVotes(totalVoteCount);
        return categoryPageVoteStatus;
    }

    private Long syncVoteTotalInRedis(String categoryPageId) throws InternalServerErrorException {
        Long count = categoryPageVoteDao.getTotalUpvotes(categoryPageId);
        redisDAO.setex(getCategoryPageVoteRedisKey(categoryPageId), String.valueOf(count), DateTimeUtils.HOURS_PER_2DAYS);
        return count;
    }

    private String getCategoryPageVoteRedisKey(String categoryPageId) {
        String CATEGORY_PAGE_TOTAL_VOTE_KEY = "_CATEGORY_PAGE_TOTAL_VOTE";
        return categoryPageId + CATEGORY_PAGE_TOTAL_VOTE_KEY;
    }

}
