package com.vedantu.growth.seo.pageSpeed;

import com.vedantu.growth.seo.enums.SeoDomain;

public class CategoryPayloadToSNS {
    private String categoryId;
    private  String categoryName;
    private SeoDomain domain;

    public CategoryPayloadToSNS(){
        super();
    }

    public CategoryPayloadToSNS(String categoryId, String categoryName, SeoDomain domain){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.domain = domain;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public SeoDomain getDomain() {
        return domain;
    }

    public void setDomain(SeoDomain domain) {
        this.domain = domain;
    }

    public String getCategoryName(){
        return categoryName;
    }

    public void setCategoryName(String categoryName){
        this.categoryName = categoryName;
    }
}
