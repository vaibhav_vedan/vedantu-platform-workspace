package com.vedantu.growth.seo.enums;

public enum SeoRssFeedGroup {
    BLOG, NEWS;
}
