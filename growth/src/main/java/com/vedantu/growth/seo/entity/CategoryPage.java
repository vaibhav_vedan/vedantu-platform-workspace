package com.vedantu.growth.seo.entity;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.cmds.pojo.RichTextFormat;
import com.vedantu.cmds.pojo.SolutionFormat;
import com.vedantu.growth.seo.response.BlogMetadataPojo;
import com.vedantu.onetofew.pojo.FAQPojo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDefinition;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.TextIndexDefinition;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.TextScore;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@ToString
@Document(collection = "CategoryPage")
@CompoundIndexes({
        @CompoundIndex(name = "lastUpdated_-1", def = "{'lastUpdated': -1}", background = true),
        @CompoundIndex(name = "pagetype-title", def = "{'domain': 1,'disabled': 1,'pageType': 1,'title': -1}", background = true),
        @CompoundIndex(name = "domain-url-1", def = "{'domain': 1, 'url': 1}", background = true),
        @CompoundIndex(name = "domain-previousUrls-1", def = "{'domain': 1, 'previousUrls': 1}", background = true),
        @CompoundIndex(name = "domain-disabled-pageType-1", def = "{'domain': 1,'disabled': 1,'pageType': 1}", background = true),
        @CompoundIndex(name = "blogCategoryId-domain-disabled", def = "{'blogCategoryId':1,'domain': 1,'disabled': 1}", background = true),
        @CompoundIndex(name = "categoryId_domain_disabled_active_copyPage_lastUpdated", def = "{'categoryId':1,'domain': 1,'disabled': 1,'active':1,'copyPage':1,'lastUpdated':-1}", background = true),
        @CompoundIndex(name = "topic-domain-disabled", def = "{'topic':1,'domain': 1,'disabled': 1}", background = true),
        @CompoundIndex(name = "target-domain-disabled", def = "{'target':1,'domain': 1,'disabled': 1}", background = true),
        @CompoundIndex(name = "subject-domain-disabled-pagetype-1lastUpdated_-1", def = "{'subject':1,'domain': 1,'disabled': 1,'pageType': 1,'lastUpdated': -1}", background = true)
})

public class CategoryPage extends AbstractSeoEntity {

    @Indexed(background = true)
    private String categoryId;

    private String blogCategoryId;

    private String blogCategoryName;

    private Double priority;

    private String frequency;

    private CategoryPageType pageType;

    private String name;

    private String authorName;

    private Long blogPublishedDate;

    private String featureImageUrl;
    private String featureImageTag;

    private Boolean disabled;

    private Boolean copyPage = false;

    @Indexed(background = true)
    private String url;

    @Indexed(background = true)
    private String title;

    private String description;

    private String videoUrl;

    private List<String> customTags;

    private String target;

    private String grade;

    private String subject;

    private String topic;

    private List<CategoryPageMetaTag> metaTags;

    private CategoryPageHeader header;

    @Indexed(background = true)
    private String questionId;

    private RichTextFormat questionBody;

    private String sQuestion;

    private List<String> sSolutions;

    private List<SolutionFormat> solutions;

    private CategoryPageFooter footer;

    private CategoryPageListModule masterSidebarModules;

    private List<CategoryPageListModule> sidebarModules;

    private List<CategoryPageListModule> listModules;

    private List<CategoryPageContentModule> contentModules;

    private List<CategoryPageListModule> customLinkModules;

    private String htmlContent;

    private List<String> downloadableLinks;

    private boolean showDownloadOption = false;

    private Boolean contentFileProcessingStatus = false;

    private String dataObject;

    private String downloadButtonText;

    private List<String> keywords;

    private String canonicalUrl;
    private String metaKeywords;
    private List<AlternateLink> alternateLinks;

    private LinkedHashSet<String> relatedPageIds;

    private LinkedHashSet<CategoryPageRelatedQuestion> relatedPageLinks;

    private Set<String> previousUrls;

    private boolean noIndex = false;

    @TextScore
    public Float score;

    private String schemaKeywords;

    private List<FAQPojo> faqs;

    private List<CategoryPageListModule> breadCrumbModules;

    private List<BlogMetadataPojo> blogRelatedArticles;

    private Boolean isVerified;

    private Long totalVotes;

    private Difficulty difficulty;

    public CategoryPage() {
    }

    public static IndexDefinition getTextIndexDefinition() {
        return new TextIndexDefinition.TextIndexDefinitionBuilder()
                .onField(Constants.TARGET, 1F)
                .onField(Constants.GRADE, 1F)
                .onField(Constants.SUBJECT, 3F)
                .named("CategoryPage_TextIndex")
                .build();
    }

    public String getSubject() {

        return subject;
    }

    public static class Constants extends AbstractSeoEntity.Constants {
        public static final String NAME = "name";
        public static final String TITLE = "title";
        public static final String DOMAIN = "domain";
        public static final String URL = "url";
        public static final String FEATURE_IMAGE_TAG = "featureImageTag";
        public static final String DISABLED = "disabled";
        public static final String CATEGORY_ID = "categoryId";
        public static final String PAGE_TYPE = "pageType";
        public static final String TARGET = "target";
        public static final String DESCRIPTION = "description";
        public static final String FEATURE_IMAGE_URL = "featureImageUrl";
        public static final String GRADE = "grade";
        public static final String COPY_PAGE = "copyPage";
        public static final String QUESTION_ID = "questionId";
        public static final String PREVIOUS_URLS = "previousUrls";
        public static final String SUBJECT = "subject";
        public static final String BLOG_CATEGORY_ID = "blogCategoryId";
        public static final String BLOG_PUBLISHED_DATE = "blogPublishedDate";
        public static final String TOPIC = "topic";
        public static final String HEADER_TITLE = "header.title";
    }
}