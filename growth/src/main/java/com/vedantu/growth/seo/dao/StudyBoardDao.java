package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.CategoryPageVote;
import com.vedantu.growth.seo.entity.StudyBoard;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;


@Service
public class StudyBoardDao extends AbstractMongoDAO {
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(StudyBoardDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public StudyBoard getStudyBoardByUserId(String userId) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId));
        return findOne(query, StudyBoard.class);
    }

    public void insertBookmark(String userId, String categoryPageId) {
        StudyBoard studyBoard = new StudyBoard();
        studyBoard.setUserId(userId);
        studyBoard.setBookmarks(Collections.singletonList(categoryPageId));
        saveEntity(studyBoard, userId);
    }

    public void insertDownloadHistory(String userId, String categoryPageId) {
        StudyBoard studyBoard = new StudyBoard();
        studyBoard.setUserId(userId);
        studyBoard.setDownloads(Collections.singletonList(categoryPageId));
        saveEntity(studyBoard, userId);
    }

    public void addBookmark(String userId, String categoryPageId) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId))
                .addCriteria(Criteria.where(StudyBoard.Constants.BOOKMARKS).ne(categoryPageId));
        Update update = new Update().push(StudyBoard.Constants.BOOKMARKS).atPosition(0).value(categoryPageId);
        updateFirst(query, update, StudyBoard.class);
    }

    public int deleteBookmark(String userId, String categoryPageId) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId));
        Update update = new Update().pull(StudyBoard.Constants.BOOKMARKS, categoryPageId);
        return updateFirst(query, update, StudyBoard.class);
    }

    public void pushDownloadHistory(String userId, String categoryPageId) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId))
                .addCriteria(Criteria.where(StudyBoard.Constants.DOWNLOADS).ne(categoryPageId));
        Update update = new Update().push(StudyBoard.Constants.DOWNLOADS).atPosition(0).value(categoryPageId);
        updateFirst(query, update, StudyBoard.class);
    }

    public int deleteDownloadHistory(String userId, String categoryPageId) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId));
        Update update = new Update().pull(StudyBoard.Constants.DOWNLOADS, categoryPageId);
        return updateFirst(query, update, StudyBoard.class);
    }

    public void updateNotes(String userId, List<String> notes) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId));
        Update update = new Update().set(StudyBoard.Constants.NOTES, notes);
        upsertEntity(query, update, StudyBoard.class);
    }

    public void removeOlderBookmarkedPage(String userId) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId));
        Update update = new Update().pop(StudyBoard.Constants.BOOKMARKS, Update.Position.LAST);
        updateFirst(query, update, StudyBoard.class);
    }

    public void removeOlderDownloadHistory(String userId) {
        Query query = new Query().addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId));
        Update update = new Update().pop(StudyBoard.Constants.DOWNLOADS, Update.Position.LAST);
        updateFirst(query, update, StudyBoard.class);
    }
}
