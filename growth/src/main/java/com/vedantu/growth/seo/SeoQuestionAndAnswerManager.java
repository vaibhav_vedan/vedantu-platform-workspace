package com.vedantu.growth.seo;

import com.google.gson.Gson;
import com.vedantu.cmds.pojo.SeoQuestion;
import com.vedantu.cmds.pojo.SolutionFormat;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.seo.dao.CategoryPageDAO;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.request.SeoQuestionAndAnswerFilterReq;
import com.vedantu.growth.seo.response.SeoQuestionAndAnswerFilterMetadataRes;
import com.vedantu.growth.seo.response.SeoQuestionAndAnswerFilterOptionsRes;
import com.vedantu.growth.seo.response.SeoQuestionAndAnswerFilterRes;
import com.vedantu.growth.seo.response.SeoQuestionAndAnswerFilterSubjectTopicsPojo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class SeoQuestionAndAnswerManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoQuestionAndAnswerManager.class);

    @Autowired
    private CategoryPageDAO categoryPageDAO;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @Autowired
    private RedisDAO redisDAO;

    private static final String SEO_FILTER_REDIS_KEY_BASE_KEY = "_SEO_QNA_FILTER_REDIS_KEY";

    private Gson _gson = new Gson();

    public SeoQuestionAndAnswerFilterOptionsRes getFilterOptions() throws BadRequestException, InternalServerErrorException {

        String redisKey = getQnAFilterOptionsFromRedissKey();

        String filterString = redisDAO.get(redisKey);
        if(StringUtils.isNotEmpty(filterString)) {

            logger.info("Fetched filter from rediss : {}", filterString);

            return _gson.fromJson(filterString,SeoQuestionAndAnswerFilterOptionsRes.class);
        }

        int size = 500;

        List<CategoryPage> categoryPageList = new ArrayList<>();

        List<CategoryPage> categoryPageSubList;

        List<String> fieldsToInclude = new ArrayList<>();
        fieldsToInclude.add(CategoryPage.Constants.TOPIC);
        fieldsToInclude.add(CategoryPage.Constants.TARGET);
        fieldsToInclude.add(CategoryPage.Constants.SUBJECT);
        String lastFetchedId = null;
        do {
            categoryPageSubList = categoryPageDAO.getQnAFilterOptions(SeoDomain.VEDANTU, CategoryPageType.QnA, lastFetchedId, size, new HashSet<>());
            if(CollectionUtils.isEmpty(categoryPageSubList)){
                break;
            }
            categoryPageList.addAll(categoryPageSubList);

            lastFetchedId = categoryPageList.get(categoryPageList.size()-1).getId();

        }while (categoryPageSubList.size() == size);

        Set<String> targets = new HashSet<>();
        Map<String, Set<String>> subjectsMap = new HashMap<>();

        for(CategoryPage categoryPage : categoryPageList){

            if(StringUtils.isNotEmpty(categoryPage.getTarget())) {
                String target = categoryPage.getTarget().replaceAll("_","-");
                targets.add(target);
            }

            if(StringUtils.isNotEmpty(categoryPage.getSubject()) ) {
                String subject = StringUtils.capitalize(categoryPage.getSubject());
                Set<String> topics = CollectionUtils.putIfAbsentAndGet(subjectsMap, subject, HashSet::new);
                if(StringUtils.isNotEmpty(categoryPage.getTopic())) {
                    topics.add(categoryPage.getTopic());
                }
            }
        }

        SeoQuestionAndAnswerFilterOptionsRes filterOptionsRes = new SeoQuestionAndAnswerFilterOptionsRes();
        List<String> targetList = new ArrayList<>(targets);
        Collections.sort(targetList);
        filterOptionsRes.setTargets(targetList);

        List<String> subjectsList = new ArrayList<>(subjectsMap.keySet());
        Collections.sort(subjectsList);

        for(String subject : subjectsList){
            List<String> topicsList = new ArrayList<>(subjectsMap.get(subject));
            Collections.sort(topicsList);

            SeoQuestionAndAnswerFilterSubjectTopicsPojo subjectTopicsPojo = new SeoQuestionAndAnswerFilterSubjectTopicsPojo();
            subjectTopicsPojo.setSubjectName(subject);
            subjectTopicsPojo.setTopics(topicsList);
            filterOptionsRes.addSubject(subjectTopicsPojo);
        }

        redisDAO.setex(redisKey, _gson.toJson(filterOptionsRes), DateTimeUtils.SECONDS_PER_WEEK);

        logger.info("Fetched topics : {}", filterOptionsRes);

        return filterOptionsRes;

    }

    public static String getQnAFilterOptionsFromRedissKey(){

        return  "ALL".concat(SEO_FILTER_REDIS_KEY_BASE_KEY);

    }

    public SeoQuestionAndAnswerFilterRes getFilteredList(SeoQuestionAndAnswerFilterReq filterReq) throws VException {

        if(filterReq.getSize() > 20)
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Maximum permitted size is 20");

        Set<String> includeFields = new HashSet<>();
        includeFields.add(CategoryPage.Constants.GRADE);
        includeFields.add(CategoryPage.Constants.TARGET);
        includeFields.add(CategoryPage.Constants.TOPIC);
        includeFields.add(CategoryPage.Constants.SUBJECT);
        includeFields.add(CategoryPage.Constants.QUESTION_ID);
        includeFields.add(CategoryPage.Constants.URL);

        if(CollectionUtils.isNotEmpty(filterReq.getTargets())){
            boolean containsSearchStr = filterReq.getTargets().parallelStream().anyMatch("jee-main"::equalsIgnoreCase);
            if(containsSearchStr){
                filterReq.getTargets().add("jee-foundation");
                filterReq.getTargets().add("jee_main");
            }
        }

        List<CategoryPage> categoryPages = categoryPageDAO.questionAndAnswerFilteredSet(filterReq, includeFields);

        Long totalDocs = categoryPageDAO.questionAndAnswerFilteredGetTotalCount(filterReq);

        SeoQuestionAndAnswerFilterRes filterRes = new SeoQuestionAndAnswerFilterRes();
        filterRes.setTotalSize(totalDocs);

        List<String> questionIds = categoryPages.parallelStream().map(v -> v.getQuestionId()).collect(Collectors.toList());

        List<SeoQuestion> seoQuestionList = pageBuilderManager.getSeoQuestions(questionIds);

        Map<String, List<SeoQuestion>> mapData = seoQuestionList.parallelStream().collect(Collectors.groupingBy(SeoQuestion::getId));

        for(CategoryPage categoryPage : categoryPages){
            String questionId = categoryPage.getQuestionId();

            if (questionId != null && CollectionUtils.isNotEmpty(mapData.get(questionId))) {

                SeoQuestion response = mapData.get(questionId).get(0);
                if (response != null) {
                    logger.info("GETTING Question");
                    logger.info(response.toString());
                    categoryPage.setQuestionBody(response.getQuestionBody());

                    categoryPage.setSQuestion(response.getQuestionBody().getNewText());
                    categoryPage.setSolutions(response.getSolutions());
                    List<SolutionFormat> list = response.getSolutions();
                    List<String> sList = new ArrayList<String>();
                    for (SolutionFormat s : list) {
                        if (s != null) {
                            sList.add(s.getNewText());
                        }
                    }
                    categoryPage.setSSolutions(sList);
                }
            }

            SeoQuestionAndAnswerFilterMetadataRes res = new SeoQuestionAndAnswerFilterMetadataRes();
            res.setGrade(categoryPage.getGrade());
            res.setSubject(StringUtils.captialize(categoryPage.getSubject()));
            String target = categoryPage.getTarget();
            if(StringUtils.isNotEmpty(target) && target.contains("_"))
                target = target.replaceAll("_","-");
            res.setTarget(target);
            res.setTopic(StringUtils.captialize(categoryPage.getTopic()));
            res.setQuestion(categoryPage.getSQuestion());
            res.setUrl(categoryPage.getUrl());
            filterRes.addQuestionAndAnswer(res);
        }

        return filterRes;

    }


}
