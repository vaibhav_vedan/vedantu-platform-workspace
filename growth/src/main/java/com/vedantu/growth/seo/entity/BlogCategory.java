package com.vedantu.growth.seo.entity;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "BlogCategory")
public class BlogCategory extends AbstractSeoEntity{

    private String name;
}
