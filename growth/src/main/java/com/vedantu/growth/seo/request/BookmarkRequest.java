package com.vedantu.growth.seo.request;

import com.vedantu.growth.seo.enums.SeoDomain;
import lombok.Data;

@Data
public class BookmarkRequest {
    String categoryPageId;
    boolean forceRemoveOlderBookmark = false;
    String url;
    SeoDomain seoDomain;
}

