package com.vedantu.growth.seo.response;

import com.vedantu.growth.seo.entity.CategoryPage;
import lombok.Data;

import java.util.List;

@Data
public class CategoryPageResponse {
    private List<CategoryPage> categoryPages;
    private String httpStatus;
    private String toBeRedirected;

    public CategoryPageResponse() {

    }

    public CategoryPageResponse(String httpStatus, String toBeRedirected) {
        this.httpStatus = httpStatus;
        this.toBeRedirected = toBeRedirected;
    }

    public List<CategoryPage> getCategoryPages() {
        return categoryPages;
    }

    public void setCategoryPages(List<CategoryPage> categoryPages) {
        this.categoryPages = categoryPages;
    }

    public String getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(String httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getToBeRedirected() {
        return toBeRedirected;
    }

    public void setToBeRedirected(String toBeRedirected) {
        this.toBeRedirected = toBeRedirected;
    }
}
