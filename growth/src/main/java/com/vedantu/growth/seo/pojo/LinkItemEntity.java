/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.seo.pojo;

import org.apache.commons.validator.routines.UrlValidator;

import java.util.Arrays;
import java.util.List;

/**
 *
 * @author ajith
 */
public class LinkItemEntity {

    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static List<String> getProperties() {
        List<String> properties = Arrays.asList("name", "url");
        return properties;
    }

    private boolean validateUrl(String url) {
        String[] schemes = {"http", "https"};
        UrlValidator urlValidator = new UrlValidator(schemes);
        if (urlValidator.isValid(url)) {
            return true;//url is valid
        } else {
            return false;//url is invalid
        }
    }

    @Override
    public String toString() {
        return "LinkItemEntity{" + "name=" + name + ", url=" + url + '}';
    }

}
