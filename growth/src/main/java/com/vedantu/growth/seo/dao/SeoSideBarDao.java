package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.entity.SeoSidebarModules;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class SeoSideBarDao extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SeoSideBarDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public String upsert(SeoSidebarModules seoSidebarModules){

        try {
            saveEntity(seoSidebarModules);
            return seoSidebarModules.getId();
        } catch (Exception ex) {
            throw new RuntimeException("seoSidebarModulesUpdateError : Error updating the seoSdebarModules " + seoSidebarModules.toString(), ex);
        }

    }

    public SeoSidebarModules getSideBarModules(CategoryPageType pageType) {

        Query query = new Query();
        query.addCriteria(Criteria.where(SeoSidebarModules.Constants.PAGE_TYPE).is(pageType));

        logger.info("Query: {}", query);

        return findOne(query, SeoSidebarModules.class);
    }
}
