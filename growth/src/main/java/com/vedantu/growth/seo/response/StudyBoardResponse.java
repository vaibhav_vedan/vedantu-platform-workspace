package com.vedantu.growth.seo.response;

import com.vedantu.growth.seo.entity.CategoryPageInfo;
import lombok.Data;

import java.util.List;

@Data
public class StudyBoardResponse {

    private List<String> notes;
    private List<CategoryPageInfo> bookmarks;
    private List<CategoryPageInfo> downloads;
    private List<CategoryPageInfo> suggestions;
    private Integer bookmarksLimit;
    private Integer downloadsHistoryLimit;

}
