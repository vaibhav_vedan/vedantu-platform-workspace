package com.vedantu.growth.seo.pojo;

import lombok.Data;

@Data
public class CategoryPageVoteRequest {

    private String categoryPageId;
    private Boolean isUpvote;

}
