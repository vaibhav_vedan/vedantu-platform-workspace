package com.vedantu.growth.seo.entity;

public enum CategoryPageType {
	PageType1, PageType2, PageType3, PageType4, QnA, TopicPage, BlogPage, NewsPage
}