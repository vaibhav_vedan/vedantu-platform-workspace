/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.seo.pojo;

import com.vedantu.util.StringUtils;

/**
 *
 * @author ajith
 */
public enum SeoTutorListPageState {
    ENABLED, DISABLED;

    public static SeoTutorListPageState valueOfKey(String keyStr) {
        SeoTutorListPageState state = null;
        if (!StringUtils.isEmpty(keyStr)) {
            state = valueOf(keyStr.toUpperCase());
        }
        return state;
    }
}
