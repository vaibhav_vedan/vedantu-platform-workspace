package com.vedantu.growth.seo.enums;

public enum ESSearchMode {
    RELATED_QNA_DOCS, SIDEBAR_MODULE, STUDY_BOARD_RELATED_DOCS;
}
