package com.vedantu.growth.seo.response;

import com.vedantu.growth.seo.pojo.BlogArticlePojo;
import lombok.Data;

import java.util.List;

@Data
public class BlogHomePageResponse {

    private List<BlogMetadataPojo> latestBlogs;
    private List<BlogCategoryResponse> categories;
    private BlogArticlePojo article;

    public static class Builder{

        private BlogHomePageResponse blogHomePageResponse;

        public Builder(){

            blogHomePageResponse = new BlogHomePageResponse();
        }

        public Builder latestBlogs(List<BlogMetadataPojo> latestBlogs){
            blogHomePageResponse.latestBlogs = latestBlogs;
            return this;
        }

        public Builder categories(List<BlogCategoryResponse> categories){
            blogHomePageResponse.categories = categories;
            return this;
        }

        public Builder article(BlogArticlePojo article){
            blogHomePageResponse.article = article;
            return this;
        }

        public BlogHomePageResponse build(){
            return blogHomePageResponse;
        }

    }

}
