package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.SeoRssFeed;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.enums.SeoRssFeedGroup;
import com.vedantu.growth.seo.enums.SeoRssFeedObjectType;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SeoRssFeedDao extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SeoRssFeedDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public void upsert(SeoRssFeed seoRssFeed){
        try {

            saveEntity(seoRssFeed);

        } catch (Exception ex) {
            throw new RuntimeException("seoRssFeedUpsertError : Error updating the seoRssFeed " + seoRssFeed.toString(), ex);
        }
    }

    public SeoRssFeed getRssBaseFeed(SeoDomain domain, SeoRssFeedGroup group) {

        Query query = new Query();
        query.addCriteria(Criteria.where(SeoRssFeed.Constants.FEED_GROUP).is(group));
        query.addCriteria(Criteria.where(SeoRssFeed.Constants.FEED_TYPE).is(SeoRssFeedObjectType.BASE));
        query.addCriteria(Criteria.where(SeoRssFeed.Constants.DOMAIN).is(domain));
        logger.info("query: " + query.toString());

        List<SeoRssFeed> seoRssFeeds = runQuery(query, SeoRssFeed.class);

        if(CollectionUtils.isEmpty(seoRssFeeds))
            return null;

        return seoRssFeeds.get(0);
    }

    public List<SeoRssFeed> getRssTitleFeeds(SeoDomain domain, SeoRssFeedGroup group) {

        Query query = new Query();
        query.addCriteria(Criteria.where(SeoRssFeed.Constants.FEED_GROUP).is(group));
        query.addCriteria(Criteria.where(SeoRssFeed.Constants.FEED_TYPE).is(SeoRssFeedObjectType.TITLE));
        query.addCriteria(Criteria.where(SeoRssFeed.Constants.DOMAIN).is(domain));
        logger.info("query: " + query.toString());

        List<SeoRssFeed> seoRssFeeds = runQuery(query, SeoRssFeed.class);

        return seoRssFeeds;
    }

    public SeoRssFeed getFeedObjectByCategoryPageId(String categoryPageId){

        Query query = new Query();
        query.addCriteria(Criteria.where(SeoRssFeed.Constants.CATEGORY_PAGE_ID).is(categoryPageId));
        logger.info("query: " + query.toString());

        List<SeoRssFeed> seoRssFeeds = runQuery(query, SeoRssFeed.class);

        if(CollectionUtils.isEmpty(seoRssFeeds))
            return null;

        return seoRssFeeds.get(0);
    }
}
