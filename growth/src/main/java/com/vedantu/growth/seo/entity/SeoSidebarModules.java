package com.vedantu.growth.seo.entity;

import com.vedantu.growth.seo.pojo.SidebarModulesMetadata;
import lombok.Data;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document(collection = "SeoSidebarModules")
public class SeoSidebarModules extends AbstractSeoEntity {

    @Indexed(background = true)
    private CategoryPageType pageType;

    List<SidebarModulesMetadata> sidebarModulesMetadata;

    public static class Constants extends AbstractSeoEntity.Constants {
        public static final String PAGE_TYPE = "pageType";
    }

}
