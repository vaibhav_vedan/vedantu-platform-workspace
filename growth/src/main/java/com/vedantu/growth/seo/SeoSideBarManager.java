package com.vedantu.growth.seo;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.managers.ESManager;
import com.vedantu.growth.managers.RedisDataManager;
import com.vedantu.growth.seo.dao.SeoSideBarDao;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.entity.SeoSidebarModules;
import com.vedantu.growth.seo.enums.ESSearchMode;
import com.vedantu.growth.seo.enums.ListModuleTypes;
import com.vedantu.growth.seo.pojo.SeoSidebarModulesDataRes;
import com.vedantu.growth.seo.pojo.SideBarLinksMetadata;
import com.vedantu.growth.seo.pojo.SidebarModulesMetadata;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class SeoSideBarManager {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SeoSideBarManager.class);

    @Autowired
    private SeoSideBarDao seoSideBarDao;

    @Autowired
    private SeoSearchEngineManager seoSearchEngineManager;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private ESManager esManager;

    @Autowired
    private RedisDataManager redisDataManager;

    private Gson gson = new Gson();

    private static final String  ES_SEARCH_SIDEBAR_RESULTS_REDIS_KEY = "ES_SEARCH_SIDEBAR_RESULTS_";
    private static final String  SEO_DEFAULT_TARGET = "cbse";
    private static Integer sidebarDocumentsMaxSize = 10;

    public String upsertSideBarModules(SeoSidebarModules seoSidebarModules) throws BadRequestException, ConflictException {

        if (seoSidebarModules.getPageType() == null)
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Page type is required");
        if (StringUtils.isEmpty(seoSidebarModules.getId())) {
            SeoSidebarModules pageType = seoSideBarDao.getSideBarModules(seoSidebarModules.getPageType());

            if (pageType != null) {
                throw new ConflictException(ErrorCode.CONFLICTS_FOUND, "Cannot create new sidebar module for the page type, please update the existing one");
            }
        }

        return seoSideBarDao.upsert(seoSidebarModules);

    }

    public SeoSidebarModules getSideBarModules(CategoryPageType pageType) {

        return seoSideBarDao.getSideBarModules(pageType);
    }

    public SeoSidebarModulesDataRes getSideBarModulesData(CategoryPageType pageType, String categoryPageId) throws VException, IOException {

        String cachedSearchResultKey = seoSearchEngineManager.getSearchResultsRedisKey(ESSearchMode.SIDEBAR_MODULE, categoryPageId);

        try{
            String cachedSearchResult = redisDAO.get(cachedSearchResultKey);
            if(StringUtils.isNotEmpty(cachedSearchResult)){

                SeoSidebarModulesDataRes seoSidebarModulesDataRes = gson.fromJson(cachedSearchResult, SeoSidebarModulesDataRes.class);
                return seoSidebarModulesDataRes;
            }
        }catch (Exception e){
            logger.error("Error fetching from redis", e);
        }

        if(!seoSearchEngineManager.canFetchFromES(ESSearchMode.SIDEBAR_MODULE)){
            return new SeoSidebarModulesDataRes();
        }

        boolean canCache = true;
        CategoryPage currentCategoryPage = pageBuilderManager.getCategoryPageById(categoryPageId);

        if (currentCategoryPage == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Category page not found with the given id " + categoryPageId);
        }

        String url = currentCategoryPage.getUrl();
        String subject = StringUtils.isNotEmpty(currentCategoryPage.getSubject())?currentCategoryPage.getSubject():StringUtils.EMPTY;
        String grade = StringUtils.isNotEmpty(currentCategoryPage.getGrade())?currentCategoryPage.getGrade():StringUtils.EMPTY;
        String target = StringUtils.isNotEmpty(currentCategoryPage.getTarget())?currentCategoryPage.getTarget():SEO_DEFAULT_TARGET;

        String headerTitle = "";

        if (CategoryPageType.QnA.equals(currentCategoryPage.getPageType())) {
            headerTitle = currentCategoryPage.getTopic();
        } else {
            if (currentCategoryPage.getHeader() != null) {
                headerTitle = currentCategoryPage.getHeader().getTitle();
            }
        }

        String title = currentCategoryPage.getTitle();

        String searchString = subject + " " + grade + " " + target + " " + url + " " + headerTitle + " " + title;

        searchString = searchString.replaceAll("-", " ");
        searchString = searchString.replaceAll("  ", " ");

        SeoSidebarModules seoSidebarModules = getSideBarModules(pageType);

        List<SidebarModulesMetadata> sidebarModulesMetadata = seoSidebarModules != null ? seoSidebarModules.getSidebarModulesMetadata() : new ArrayList<>();

        for (SidebarModulesMetadata seoSidebarModulesMetadata : sidebarModulesMetadata) {
            if (ListModuleTypes.SEO.equals(seoSidebarModulesMetadata.getType())
                    && CollectionUtils.isNotEmpty(seoSidebarModulesMetadata.getCategoryIds())) {
                String index = seoSearchEngineManager.getIndex();
                SearchRequest intermediateSearchRequest = constructSidebarESQuery(searchString, seoSidebarModulesMetadata,index);
                try {
                    SearchResponse resultData = esManager.doSearchRequest(intermediateSearchRequest);
                    processSidebarESResponse(seoSidebarModulesMetadata, resultData, categoryPageId);
                }catch (Exception e){
                    canCache = false;
                    logger.error("Error fetching from ES", e);
                }
            }
        }

        SeoSidebarModulesDataRes seoSidebarModulesDataRes = new SeoSidebarModulesDataRes();
        seoSidebarModulesDataRes.setPageType(pageType);
        seoSidebarModulesDataRes.setSidebarModulesMetadata(sidebarModulesMetadata);

        if(canCache) {
            setInRedis(cachedSearchResultKey, gson.toJson(seoSidebarModulesDataRes));
        }

        return seoSidebarModulesDataRes;
    }

    private void processSidebarESResponse(SidebarModulesMetadata seoSidebarModulesMetadata, SearchResponse resultData, String categoryPageId) {
        SearchHits searchHits = resultData.getHits();
        List<SideBarLinksMetadata> pagesMetadata = new ArrayList<>();
        for (SearchHit searchHit : searchHits) {
            String sourceAsString = searchHit.getSourceAsString();
            logger.info("Source : {}", sourceAsString);
            CategoryPage categoryPage = gson.fromJson(sourceAsString, CategoryPage.class);
            if (categoryPage.getId().equals(categoryPageId))
                continue;
            SideBarLinksMetadata metadata = new SideBarLinksMetadata();
            metadata.setUrl(categoryPage.getUrl());
            String pageTitle = categoryPage.getTitle();
            if (!CategoryPageType.QnA.equals(categoryPage.getPageType())
                    && Objects.nonNull(categoryPage.getHeader())
                    && StringUtils.isNotEmpty(categoryPage.getHeader().getTitle())) {
                pageTitle = categoryPage.getHeader().getTitle();
            }
            metadata.setTitle(pageTitle);
            metadata.setId(categoryPage.getId());
            pagesMetadata.add(metadata);
        }
        if (pagesMetadata.size() > sidebarDocumentsMaxSize)
            seoSidebarModulesMetadata.setPagesMetadata(pagesMetadata.subList(0, sidebarDocumentsMaxSize));
        else
            seoSidebarModulesMetadata.setPagesMetadata(pagesMetadata);
    }

    private SearchRequest constructSidebarESQuery(String searchString, SidebarModulesMetadata seoSidebarModulesMetadata, String index) {
        BoolQueryBuilder boolQueryBuilderMust=new BoolQueryBuilder();
        Map<String,Float> fieldsMap = new HashMap<>();
        fieldsMap.put("url",40f);
        fieldsMap.put("header.title",30f);
        fieldsMap.put("topic",30f);
        fieldsMap.put("target",20f);
        fieldsMap.put("subject",15f);
        fieldsMap.put("grade",15f);
        QueryBuilder multiMatchQuery= QueryBuilders.multiMatchQuery(searchString)
                .fields(fieldsMap);
        boolQueryBuilderMust.must(multiMatchQuery);
        QueryBuilder mustMatch= QueryBuilders.termQuery(CategoryPage.Constants.DISABLED,"false");
        boolQueryBuilderMust.must(mustMatch);

        BoolQueryBuilder boolQueryBuilderShould=new BoolQueryBuilder();
        for (String categoryId : seoSidebarModulesMetadata.getCategoryIds()) {
            QueryBuilder multiMatchQueryCategoryId= QueryBuilders.termQuery(CategoryPage.Constants.CATEGORY_ID,categoryId);
            boolQueryBuilderShould.should(multiMatchQueryCategoryId);
        }
        boolQueryBuilderMust.must(boolQueryBuilderShould);

        SearchSourceBuilder searchSourceBuilder=new SearchSourceBuilder();
        searchSourceBuilder.query(boolQueryBuilderMust);
        searchSourceBuilder.from(0);
        searchSourceBuilder.size(11);
        String[] includeFields = {"id","url","title","header.title","pageType"};
        String[] excludeFields = {};
        searchSourceBuilder.fetchSource(includeFields, excludeFields);

        SearchRequest intermediateSearchRequest=new SearchRequest();
        intermediateSearchRequest.source(searchSourceBuilder);
        intermediateSearchRequest.indices(index);
        logger.info("The intermediate search request is: {}", intermediateSearchRequest);
        return intermediateSearchRequest;
    }

    private void setInRedis(String cachedSearchResultKey, String data) {
        try {
            redisDAO.setex(cachedSearchResultKey, data, DateTimeUtils.SECONDS_PER_DAY);
        }catch (Exception e){
            logger.error("Error setting in redis", e);
        }
    }

}
