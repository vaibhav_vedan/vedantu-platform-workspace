package com.vedantu.growth.seo;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.growth.managers.ESManager;
import com.vedantu.growth.seo.dao.CategoryPageDAO;
import com.vedantu.growth.seo.dao.CategoryPageVoteDao;
import com.vedantu.growth.seo.dao.StudyBoardDao;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.CategoryPageInfo;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.entity.StudyBoard;
import com.vedantu.growth.seo.enums.ESSearchMode;
import com.vedantu.growth.seo.request.BookmarkRequest;
import com.vedantu.growth.seo.request.BookmarkResponse;
import com.vedantu.growth.seo.response.StudyBoardResponse;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class StudyBoardManager {

    private static final int start = 0;
    private static final int limit = 10;
    private final Gson gson = new Gson();
    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(StudyBoardManager.class);
    @Autowired
    private SeoSearchEngineManager seoSearchEngineManager;
    @Autowired
    private StudyBoardDao studyBoardDao;
    @Autowired
    private CategoryPageDAO categoryPageDAO;
    @Autowired
    private CategoryPageVoteDao categoryPageVoteDao;
    @Autowired
    private ESManager esManager;
    @Autowired
    private PageBuilderManager pageBuilderManager;
    @Autowired
    private HttpSessionUtils sessionUtils;

    public StudyBoardResponse getStudyBoard(String userId) throws VException {

        sessionUtils.checkIfUserLoggedIn();

        StudyBoard studyBoard = studyBoardDao.getStudyBoardByUserId(userId);
        if (Objects.isNull(studyBoard)) {
            return new StudyBoardResponse();
        } else {
            StudyBoardResponse response = new StudyBoardResponse();
            response.setNotes(studyBoard.getNotes());
            if (CollectionUtils.isNotEmpty(studyBoard.getBookmarks())) {
                List<CategoryPage> categoryPages = categoryPageDAO.getCategoryPageByIds(studyBoard.getBookmarks())
                        .stream()
                        .sorted(sortByLastModifiedOrder(studyBoard.getBookmarks()))
                        .collect(Collectors.toList());
                response.setBookmarks(processCategoryPages(categoryPages));
                response.setSuggestions(getSuggestionsForStudyBoard(userId, studyBoard.getBookmarks(), categoryPages));
            }

            if (CollectionUtils.isNotEmpty(studyBoard.getDownloads())) {
                List<CategoryPage> categoryPages = categoryPageDAO.getCategoryPageByIds(studyBoard.getDownloads())
                        .stream()
                        .sorted(sortByLastModifiedOrder(studyBoard.getDownloads()))
                        .collect(Collectors.toList());
                response.setDownloads(processCategoryPages(categoryPages));
            }

            response.setBookmarksLimit(StudyBoard.Constants.BOOKMARKS_LIMIT);
            response.setDownloadsHistoryLimit(StudyBoard.Constants.DOWNLOAD_HISTORY_LIMIT);
            return response;
        }
    }

    private Comparator<CategoryPage> sortByLastModifiedOrder(List<String> bookmarks) {
        return Comparator.comparing(categoryPage -> bookmarks.indexOf(categoryPage.getId()));
    }

    private List<CategoryPageInfo> processCategoryPages(List<CategoryPage> categoryPages) {
        //logger.info("CategoryPages retrieved for study board: " + categoryPages);
        if (CollectionUtils.isNotEmpty(categoryPages)) {
            return categoryPages.stream()
                    .map(this::parseCategoryPage)
                    .collect(Collectors.toList());
        }

        return new ArrayList<>();
    }

    public BookmarkResponse addBookmark(String userId, BookmarkRequest request) throws VException {
        StudyBoard studyBoard = studyBoardDao.getStudyBoardByUserId(userId);
        //Check whether the category page is valid or not
        CategoryPage categoryPage = null;
        BookmarkResponse response = new BookmarkResponse();
        String categoryPageId = request.getCategoryPageId();
        if (StringUtils.isEmpty(categoryPageId)) {
            categoryPage = getCategoryPageByUrl(request);
            categoryPageId = categoryPage.getId();
        } else {
            categoryPage = categoryPageDAO.getEntityById(categoryPageId, CategoryPage.class);
        }

        if (Objects.isNull(categoryPage) || !categoryPage.getActive() || categoryPage.getDisabled()) {
            logger.error("Category page {} is not found or disabled, userId: {}", categoryPageId, userId);
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Category page not found or its disabled currently");
        }
        //Checking for Bookmark limit to add new one
        if (Objects.nonNull(studyBoard) && studyBoard.getBookmarks().size() >= StudyBoard.Constants.BOOKMARKS_LIMIT
                && !studyBoard.getBookmarks().contains(categoryPageId)) {
            logger.debug("Bookmark limit got exceeded for user: {} - forceRemoveOlderBookmark flag: {}",
                    userId, request.isForceRemoveOlderBookmark());
            if (request.isForceRemoveOlderBookmark()) {
                //force remove older bookmark if user has chosen not to show pop up message
                studyBoardDao.removeOlderBookmarkedPage(userId);
                categoryPageVoteDao.updateBookmarkFlag(
                        userId,
                        studyBoard.getBookmarks().get(StudyBoard.Constants.BOOKMARKS_LIMIT - 1),
                        false
                );
                response.setRemovedBookmark(studyBoard.getBookmarks().get(StudyBoard.Constants.BOOKMARKS_LIMIT - 1));
            } else {
                //throwing limit reached exception if limit exceeded and user has not chosen to show pop up message
                throw new VException(ErrorCode.LIST_SIZE_EXCEEDED, StudyBoard.Constants.BOOKMARKS_LIMIT_EXCEEDED);
            }
        }
        //if limit has not reached then add the bookmark to study board page
        if (Objects.isNull(studyBoard)) {
            studyBoardDao.insertBookmark(userId, categoryPageId);
        } else {
            studyBoardDao.addBookmark(userId, categoryPageId);
        }

        categoryPageVoteDao.updateBookmarkFlag(userId, categoryPageId, true);
        logger.info("Bookmark added successfully for user: {}, with category page: {}", userId, categoryPageId);
        response.setAddedBookmark(categoryPageId);
        return response;
    }

    public boolean deleteBookmark(String userId, BookmarkRequest request) throws VException {
        String categoryPageId = request.getCategoryPageId();
        if (StringUtils.isEmpty(categoryPageId)) {
            CategoryPage categoryPage = getCategoryPageByUrl(request);
            categoryPageId = categoryPage.getId();
        }

        studyBoardDao.deleteBookmark(userId, categoryPageId);
        categoryPageVoteDao.updateBookmarkFlag(userId, categoryPageId, false);
        return true;
    }

    private CategoryPage getCategoryPageByUrl(BookmarkRequest request) throws VException {
        String url = request.getUrl();
        if (StringUtils.isEmpty(url)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                    "Category page Id or URL is not provided in the request");
        }
        url = url.replaceAll("https://www.vedantu.com", "");
        return pageBuilderManager.getCategoryPageByUrl(url, request.getSeoDomain());
    }

    public void addDownloadHistoryToStudyBoard(String userId, String categoryPageId) {
        StudyBoard studyBoard = studyBoardDao.getStudyBoardByUserId(userId);
        if (studyBoard != null && studyBoard.getDownloads().size() >= StudyBoard.Constants.DOWNLOAD_HISTORY_LIMIT
                && !studyBoard.getDownloads().contains(categoryPageId)) {
            logger.info("Download history limit exceeded for user: {}, removing older bookmark forcefully", userId);
            //force remove older download history
            studyBoardDao.removeOlderDownloadHistory(userId);
        }

        if (Objects.isNull(studyBoard)) {
            studyBoardDao.insertDownloadHistory(userId, categoryPageId);
        } else {
            studyBoardDao.pushDownloadHistory(userId, categoryPageId);
        }
    }

    public boolean deleteDownloadHistory(String userId, String categoryPageId) throws VException {
        studyBoardDao.deleteDownloadHistory(userId, categoryPageId);
        return true;
    }

    public void updateNotes(String userId, List<String> notes) {
        studyBoardDao.updateNotes(userId, notes);
    }

    public List<CategoryPageInfo> getSuggestionsForStudyBoard(String userId, List<String> bookmarks,
                                                              List<CategoryPage> categoryPages) {
        String index = seoSearchEngineManager.getIndex();
        if (!seoSearchEngineManager.canFetchFromES(ESSearchMode.STUDY_BOARD_RELATED_DOCS)) {
            return new ArrayList<>();
        }

        SearchRequest studyBoardESRequest = constructStudyBoardESQuery(index, categoryPages, bookmarks);
        try {
            SearchResponse resultFromES = esManager.doSearchRequest(studyBoardESRequest);
            return processStudyBoardESResponse(resultFromES, bookmarks);
        } catch (Exception e) {
            logger.error("Error while fetching data from ES for study board with user {}", userId, e);
        }

        return new ArrayList<>();
    }

    private List<CategoryPageInfo> processStudyBoardESResponse(SearchResponse response, List<String> bookmarks) {
        logger.info("Response retrieved form ES for Study Board Suggestions: {}", response.getHits().totalHits);
        return Arrays.stream(response.getHits().getHits())
                .map(searchHit -> gson.fromJson(searchHit.getSourceAsString(), CategoryPage.class))
                .filter(categoryPage -> !bookmarks.contains(categoryPage.getId()))
                .map(this::parseCategoryPage)
                .limit(ConfigUtils.INSTANCE.getIntValue("study.board.suggestions.limit", 10))
                .collect(Collectors.toList());
    }

    private CategoryPageInfo parseCategoryPage(CategoryPage categoryPage) {
        CategoryPageInfo categoryPageInfo = new CategoryPageInfo();
        categoryPageInfo.setCategoryPageId(categoryPage.getId());
        String pageTitle = categoryPage.getTitle();
        if (!CategoryPageType.QnA.equals(categoryPage.getPageType())
                && Objects.nonNull(categoryPage.getHeader())
                && StringUtils.isNotEmpty(categoryPage.getHeader().getTitle())) {
            pageTitle = categoryPage.getHeader().getTitle();
        }
        categoryPageInfo.setTitle(pageTitle);
        categoryPageInfo.setUrl(categoryPage.getUrl());
        categoryPageInfo.setActive(categoryPage.getActive() && !categoryPage.getDisabled());
        return categoryPageInfo;
    }

    private SearchRequest constructStudyBoardESQuery(String index, List<CategoryPage> categoryPages,
                                                     List<String> bookmarks) {

        QueryBuilder notDisabledPages = QueryBuilders.termQuery(CategoryPage.Constants.DISABLED, "false");
        BoolQueryBuilder boolQueryBuilderSubject = new BoolQueryBuilder();
        BoolQueryBuilder boolQueryBuilderTarget = new BoolQueryBuilder();
        BoolQueryBuilder boolQueryBuilderGrade = new BoolQueryBuilder();
        QueryBuilder mustNotThisCategoryPageIds = QueryBuilders.termsQuery(CategoryPage.Constants.ID, bookmarks);

        categoryPages.stream()
                .limit(5)
                .forEach(categoryPage -> {
                    if (StringUtils.isNotEmpty(categoryPage.getSubject())) {
                        QueryBuilder mustMatchField = QueryBuilders
                                .termQuery(CategoryPage.Constants.SUBJECT, categoryPage.getSubject());
                        boolQueryBuilderSubject.should(mustMatchField);
                    }

                    if (StringUtils.isNotEmpty(categoryPage.getGrade())) {
                        QueryBuilder mustMatchField = QueryBuilders
                                .termQuery(CategoryPage.Constants.GRADE, categoryPage.getGrade());
                        boolQueryBuilderGrade.should(mustMatchField);
                    }

                    if (StringUtils.isNotEmpty(categoryPage.getTarget())) {
                        QueryBuilder mustMatchField = QueryBuilders
                                .termQuery(CategoryPage.Constants.TARGET, categoryPage.getTarget());
                        boolQueryBuilderTarget.should(mustMatchField);
                    }
                });

        BoolQueryBuilder boolQueryBuilderForStudyBoard = new BoolQueryBuilder()
                .must(notDisabledPages)
                .must(boolQueryBuilderSubject)
                .must(boolQueryBuilderGrade)
                .must(boolQueryBuilderTarget)
                .mustNot(mustNotThisCategoryPageIds);

        String[] includeFields = {
                CategoryPage.Constants.ID,
                CategoryPage.Constants.URL,
                CategoryPage.Constants.TITLE,
                CategoryPage.Constants.HEADER_TITLE,
                CategoryPage.Constants.PAGE_TYPE,
                CategoryPage.Constants.Active,
                CategoryPage.Constants.DISABLED
        };
        String[] excludeFields = {};

        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder()
                .fetchSource(includeFields, excludeFields)
                .query(boolQueryBuilderForStudyBoard)
                .from(start)
                .size(limit);

        logger.info("The search request for study board suggestions: {}, index {}", searchSourceBuilder, index);
        return new SearchRequest()
                .source(searchSourceBuilder)
                .indices(index);
    }
}
