package com.vedantu.growth.seo.pojo;

import com.vedantu.growth.seo.entity.CategoryPageType;
import lombok.Data;

import java.util.List;

@Data
public class SeoSidebarModulesDataRes {

    private CategoryPageType pageType;
    List<SidebarModulesMetadata> sidebarModulesMetadata;

}
