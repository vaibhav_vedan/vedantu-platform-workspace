package com.vedantu.growth.seo.request;

import com.vedantu.growth.seo.enums.ESSearchMode;
import com.vedantu.growth.seo.pojo.ESSort;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Data
public class SeoEsSearchReq {

    private Integer from;
    private Integer size;
    private ESQuery query;
    private Set<String> _source;
    private ESFilter filter;
    private List<Map<String, ESSort>> sort;

    @Data
    public class ESQuery{
        private Bool bool;
    }

    @Data
    public class ESMatch {
        private String query;
        private String type;
        private String fuzziness;
        private List<String> fields;
    }

    @Data
    public class Bool{
        private List<Object> must;
        private List<Object> should;
    }

    @Data
    public class ESFilter{
        private Map<String, Object> term;
    }

    public static class Params{
        public static List<String> SEARCH_FIELDS = new ArrayList<>();
        public static Set<String> FETCH_FIELDS = new HashSet<>();

        static {
            {
                SEARCH_FIELDS.add("categoryName^20.0");
                SEARCH_FIELDS.add("grade^10.0");
                SEARCH_FIELDS.add("title^10.0");
                SEARCH_FIELDS.add("subject^10.0");
                SEARCH_FIELDS.add("target^10.0");
                SEARCH_FIELDS.add("name^1.0");
                SEARCH_FIELDS.add("description^1.0");
                SEARCH_FIELDS.add("url^1.0");
                SEARCH_FIELDS.add("htmlContent^1.0");
                SEARCH_FIELDS.add("footer.description^1.0");
                SEARCH_FIELDS.add("header.description^1.0");
            }
            {
                FETCH_FIELDS.add("pageType");
                FETCH_FIELDS.add("categoryName");
                FETCH_FIELDS.add("grade");
                FETCH_FIELDS.add("title");
                FETCH_FIELDS.add("subject");
                FETCH_FIELDS.add("target");
                FETCH_FIELDS.add("name");
                FETCH_FIELDS.add("url");
                FETCH_FIELDS.add("description");
                FETCH_FIELDS.add("difficulty");
            }
        }

        public static List<String> getSearchFields(ESSearchMode searchMode){

            switch (searchMode){
                case RELATED_QNA_DOCS:{
                    List<String> SEARCH_FIELDS_RELATED_Q = new ArrayList<>();
                    SEARCH_FIELDS_RELATED_Q.add("topic^20.0");
                    SEARCH_FIELDS_RELATED_Q.add("subject^10.0");
                    SEARCH_FIELDS_RELATED_Q.add("target^5.0");
                    SEARCH_FIELDS_RELATED_Q.add("grade^5.0");
                    return SEARCH_FIELDS_RELATED_Q;
                }
                case SIDEBAR_MODULE : {
                    List<String> SEARCH_FIELDS_SIDEBAR_MODULE = new ArrayList<>();
                    SEARCH_FIELDS_SIDEBAR_MODULE.add("url^40.0");
                    SEARCH_FIELDS_SIDEBAR_MODULE.add("header.title^30.0");
                    SEARCH_FIELDS_SIDEBAR_MODULE.add("topic^30.0");
                    SEARCH_FIELDS_SIDEBAR_MODULE.add("target^20.0");
                    SEARCH_FIELDS_SIDEBAR_MODULE.add("subject^15.0");
                    SEARCH_FIELDS_SIDEBAR_MODULE.add("grade^15.0");
                    return SEARCH_FIELDS_SIDEBAR_MODULE;
                }
                default: return SEARCH_FIELDS;
            }

        }

    }
}

