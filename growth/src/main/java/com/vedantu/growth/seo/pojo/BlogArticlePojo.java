package com.vedantu.growth.seo.pojo;

import com.vedantu.growth.seo.enums.BlogPageType;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractReq;
import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

@Data
@ToString
public class BlogArticlePojo extends AbstractReq {

    private BlogPageType blogPageType;
    private String blogCategoryId;
    private SeoDomain domain;
    private String title;
    private String description;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = new ArrayList<>();
        if (domain == null) {
            errors.add("domain");
        }
        if (StringUtils.isEmpty(title)) {
            errors.add("title");
        }
        if (StringUtils.isEmpty(description)) {
            errors.add("description");
        }
        if (blogPageType == null) {
            errors.add("blogPageType");
        }else if (BlogPageType.BLOG_CATEGORY_PAGE.equals(blogPageType) && StringUtils.isEmpty(blogCategoryId)) {
            errors.add("blogCategoryId");
        }
        return errors;
    }

}
