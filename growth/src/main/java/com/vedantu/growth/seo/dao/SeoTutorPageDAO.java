/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.SeoTutorListPage;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 *
 * @author ajith
 */
@Service
public class SeoTutorPageDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SeoTutorPageDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public SeoTutorListPage save(SeoTutorListPage seoTutorListPage) {
        try {
            Assert.notNull(seoTutorListPage);
            saveEntity(seoTutorListPage);
        } catch (Exception ex) {
            throw new RuntimeException("Error updating the seoTutorListPage ", ex);
        }

        return seoTutorListPage;
    }

    public SeoTutorListPage getById(String id) {
        return getEntityById(id, SeoTutorListPage.class);
    }

    public SeoTutorListPage getByPath(String path) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SeoTutorListPage.Constants.PATH).is(path));
        List<SeoTutorListPage> pages = runQuery(query, SeoTutorListPage.class);
        if (ArrayUtils.isNotEmpty(pages)) {
            if (pages.size() > 1) {
                logger.error("multiple SeoTutorListPage with same path " + path);
            }
            return pages.get(0);
        } else {
            return null;
        }
    }

    public List<SeoTutorListPage> getList(Integer start, Integer size) {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, CategoryPage.Constants.LAST_UPDATED));
        setFetchParameters(query, start, size);
        return runQuery(query, SeoTutorListPage.class);
    }

}
