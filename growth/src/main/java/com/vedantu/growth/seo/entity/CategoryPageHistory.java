package com.vedantu.growth.seo.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@Document(collection = "CategoryPageHistory")
public class CategoryPageHistory extends AbstractSeoEntity {

    @Indexed(background = true, direction = IndexDirection.DESCENDING)
    private String categoryPageId;
    private CategoryPage categoryPageInfo;

    public static class Constants extends AbstractSeoEntity.Constants {
        public static final String CATEGORY_PAGE_ID = "categoryPageId";
    }

}
