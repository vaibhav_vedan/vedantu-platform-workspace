package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.BlogCategory;
import com.vedantu.growth.seo.entity.Category;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class BlogCategoryDao extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(BlogCategoryDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public List<BlogCategory> getBlogCategoriesByDomain(SeoDomain domain){

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where(Category.Constants.DOMAIN).is(domain));
            query.addCriteria(Criteria.where(Category.Constants.Active).is(Boolean.TRUE));
            query.fields().include(Category.Constants.Name);
            query.fields().include(Category.Constants._ID);
            query.fields().include(Category.Constants.DOMAIN);
            return runQuery(query, BlogCategory.class);

        } catch (Exception ex) {
            throw new RuntimeException("BlogCategoryGetError : Error fetching all blog Category by domain", ex);
        }

    }

    public String upsertBlogCategories(BlogCategory blogCategory) {

        try {

            saveEntity(blogCategory);
            return blogCategory.getId();

        } catch (Exception ex) {
            throw new RuntimeException("BlogCategoryUpdateError : Error updating the Blog category " + blogCategory.toString(), ex);
        }
    }

    public BlogCategory getBlogCategoryById(String categoryId) {

        return getEntityById(categoryId, BlogCategory.class);
    }

    public BlogCategory getBlogCategoryByName(SeoDomain domain,String categoryName) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where(Category.Constants.DOMAIN).is(domain));
            query.addCriteria(Criteria.where(Category.Constants.Name).regex(Pattern.quote(categoryName), "i"));
            query.addCriteria(Criteria.where(Category.Constants.Active).is(Boolean.TRUE));

            List<BlogCategory> blogCategories = runQuery(query, BlogCategory.class);

            if(CollectionUtils.isEmpty(blogCategories))
                return null;

            return blogCategories.get(0);

        } catch (Exception ex) {
            throw new RuntimeException("BlogCategoryGetError : Error fetching blog Category by name", ex);
        }
    }
}
