package com.vedantu.growth.seo.response;

import lombok.Data;

@Data
public class CategoryPageVoteStatus {
    private Boolean isUserVoted = false;
    private Long totalVotes;
    private Boolean isBookmarked = false;
}
