package com.vedantu.growth.seo.response;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SeoQuestionAndAnswerFilterOptionsRes {

    private List<String> targets;
    private List<SeoQuestionAndAnswerFilterSubjectTopicsPojo> subjects = new ArrayList<>();

    public void addSubject(SeoQuestionAndAnswerFilterSubjectTopicsPojo subject){

        subjects.add(subject);

    }
}
