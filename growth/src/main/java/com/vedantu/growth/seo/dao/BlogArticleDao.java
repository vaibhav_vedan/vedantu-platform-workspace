package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.BlogArticle;
import com.vedantu.growth.seo.enums.BlogPageType;
import com.vedantu.growth.seo.pojo.BlogArticlePojo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogArticleDao extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(BlogArticleDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {

        return mongoClientFactory.getMongoOperations();
    }

    public BlogArticle getBlogArticle(BlogArticlePojo blogArticlePojo) {

        try {

            Query query = new Query();
            query.addCriteria(Criteria.where(BlogArticle.Constants.DOMAIN).is(blogArticlePojo.getDomain()));
            query.addCriteria(Criteria.where(BlogArticle.Constants.BLOG_PAGE_TYPE).is(blogArticlePojo.getBlogPageType()));

            if(BlogPageType.BLOG_CATEGORY_PAGE.equals(blogArticlePojo.getBlogPageType()))
                query.addCriteria(Criteria.where(BlogArticle.Constants.BLOG_CATEGORY_ID).is(blogArticlePojo.getBlogCategoryId()));

            logger.info("query: {}", query);

            List<BlogArticle> blogArticles = runQuery(query, BlogArticle.class);

            if(CollectionUtils.isEmpty(blogArticles))
                return null;

            return blogArticles.get(0);

        } catch (Exception ex) {
            throw new RuntimeException("BlogArticleGetError : Error fetching blog article by page type", ex);
        }
    }

    public String upsert(BlogArticle blogArticle){

        try {

            saveEntity(blogArticle);
            return blogArticle.getId();

        } catch (Exception ex) {
            throw new RuntimeException("BlogArticleUpdateError : Error updating the Blog article " + blogArticle.toString(), ex);
        }

    }
}
