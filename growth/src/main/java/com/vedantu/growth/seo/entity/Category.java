package com.vedantu.growth.seo.entity;

import com.google.gson.Gson;

public class Category extends AbstractSeoEntity {
    private String name;

    public Category() {
    }

    public Category(String name) {
        this.setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return (new Gson()).toJson(this);
    }

    public static class Constants extends AbstractSeoEntity.Constants {
        public static final String Name = "name";
    }
}