package com.vedantu.growth.seo.response;

import lombok.Data;

import java.util.List;

@Data
public class SeoQuestionAndAnswerFilterSubjectTopicsPojo {

    private String subjectName;
    private List<String> topics;
}
