package com.vedantu.growth.seo.pojo;

import com.vedantu.growth.seo.entity.CategoryPageHeader;
import com.vedantu.growth.seo.entity.CategoryPageType;
import lombok.Data;

@Data
public class ESSearchResultsPojo {

    private String id;
    private String title;
    private String url;
    private String sQuestion;
    private CategoryPageType pageType;
    private CategoryPageHeader header;

}
