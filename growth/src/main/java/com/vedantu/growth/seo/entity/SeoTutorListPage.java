/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.growth.seo.entity;

import com.vedantu.growth.seo.pojo.LinkItemEntity;
import com.vedantu.growth.seo.pojo.SeoTutorListPageState;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
public class SeoTutorListPage extends AbstractMongoStringIdEntity {

    private String name;
    private String path;
    private String title;
    private String keywords;

    private String h1;
    private String h2;
    private String desc;
    private String metaDesc;
    private String seoH2;
    private String seoText;
    private String trkSource;
    private String grade;
    private String target;
    private String subject;
    private String boardId;
    private SeoTutorListPageState state;
    private List<String> priceRange;
    private List<LinkItemEntity> links = new ArrayList<>();
    private List<LinkItemEntity> bottomLinks = new ArrayList<>();
    private String canonical;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getH1() {
        return h1;
    }

    public void setH1(String h1) {
        this.h1 = h1;
    }

    public String getH2() {
        return h2;
    }

    public void setH2(String h2) {
        this.h2 = h2;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getMetaDesc() {
        return metaDesc;
    }

    public void setMetaDesc(String metaDesc) {
        this.metaDesc = metaDesc;
    }

    public String getSeoH2() {
        return seoH2;
    }

    public void setSeoH2(String seoH2) {
        this.seoH2 = seoH2;
    }

    public String getSeoText() {
        return seoText;
    }

    public void setSeoText(String seoText) {
        this.seoText = seoText;
    }

    public String getTrkSource() {
        return trkSource;
    }

    public void setTrkSource(String trkSource) {
        this.trkSource = trkSource;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBoardId() {
        return boardId;
    }

    public void setBoardId(String boardId) {
        this.boardId = boardId;
    }

    public SeoTutorListPageState getState() {
        return state;
    }

    public void setState(SeoTutorListPageState state) {
        this.state = state;
    }

    public List<String> getPriceRange() {
        return priceRange;
    }

    public void setPriceRange(List<String> priceRange) {
        this.priceRange = priceRange;
    }

    public List<LinkItemEntity> getLinks() {
        return links;
    }

    public void setLinks(List<LinkItemEntity> links) {
        this.links = links;
    }

    public List<LinkItemEntity> getBottomLinks() {
        return bottomLinks;
    }

    public void setBottomLinks(List<LinkItemEntity> bottomLinks) {
        this.bottomLinks = bottomLinks;
    }

    public String getCanonical() {
        return canonical;
    }

    public void setCanonical(String canonical) {
        this.canonical = canonical;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String NAME = "name";
        public static final String ID = "id";
        public static final String PATH = "path";
        public static final String TITLE = "title";
        public static final String KEYWORDS = "keywords";
        public static final String H1 = "h1";
        public static final String H2 = "h2";
        public static final String DESC = "desc";
        public static final String META_DESC = "metaDesc";
        public static final String SEOH2 = "seoH2";
        public static final String SEOTEXT = "seoText";
        public static final String TRKSOURCE = "trkSource";
        public static final String GRADE = "grade";
        public static final String TARGET = "target";
        public static final String SUBJECT = "subject";
        public static final String BOARDID = "boardId";
        public static final String STATE = "state";
        public static final String PRICERANGE = "priceRange";
        public static final String LINKS = "links";
        public static final String BOTTOM_LINKS = "bottomLinks";
        public static final String CANONICAL = "canonical";
    }

}
