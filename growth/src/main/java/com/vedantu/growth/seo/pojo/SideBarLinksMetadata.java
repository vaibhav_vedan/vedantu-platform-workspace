package com.vedantu.growth.seo.pojo;

import lombok.Data;

@Data
public class SideBarLinksMetadata {

    private String title;
    private String url;
    private String id;
}
