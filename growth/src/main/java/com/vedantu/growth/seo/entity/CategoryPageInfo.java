package com.vedantu.growth.seo.entity;

import lombok.Data;

@Data
public class CategoryPageInfo {
    String categoryPageId;
    String title;
    String url;
    boolean active;
}
