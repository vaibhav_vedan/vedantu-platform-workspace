package com.vedantu.growth.seo.pojo;

import com.vedantu.growth.seo.enums.ListModuleTypes;
import lombok.Data;

import java.util.List;

@Data
public class SidebarModulesMetadata {

    private String title;
    private String href;
    private String imageTag;
    private ListModuleTypes type;
    private Integer order;
    private Integer internalOrder;
    private List<String> folders;
    private List<String> categoryIds;
    private List<SideBarLinksMetadata> pagesMetadata;
}
