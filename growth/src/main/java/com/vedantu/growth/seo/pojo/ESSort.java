package com.vedantu.growth.seo.pojo;

import lombok.Data;

@Data
public class ESSort {
    private String order = "ASC";
    private String mode = "avg";
}
