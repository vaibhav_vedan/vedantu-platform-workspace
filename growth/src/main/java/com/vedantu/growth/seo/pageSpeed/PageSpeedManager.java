package com.vedantu.growth.seo.pageSpeed;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.growth.aws.AwsSNSManager;
import com.vedantu.growth.aws.AwsSQSManager;
import com.vedantu.growth.requests.PageSpeedDataRequest;
import com.vedantu.growth.seo.PageBuilderManager;
import com.vedantu.growth.seo.dao.CategoryDAO;
import com.vedantu.growth.seo.dao.CategoryPageDAO;
import com.vedantu.growth.seo.dao.PageSpeedDAO;
import com.vedantu.growth.seo.entity.Category;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSTopic;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PageSpeedManager {
    @Autowired
    private CategoryDAO categoryDAO;

    @Autowired
    private CategoryPageDAO categoryPageDAO;

    @Autowired
    private PageSpeedDAO pageSpeedDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PageBuilderManager.class);

    public PlatformBasicResponse triggerCategoryToSNS(SeoDomain domain) {
        List<Category> categories = categoryDAO.getAllCategories(domain);
        if(ArrayUtils.isNotEmpty(categories)){
            for(Category category: categories){
                CategoryPayloadToSNS categoryPayloadToSNS = new CategoryPayloadToSNS(category.getId(), category.getName(),  domain);
                logger.info("triggering sns in triggerCategoryIdToSNS with payload: "+categoryPayloadToSNS.toString());
                awsSNSManager.triggerSNS(SNSTopic.PAGE_SPEED_LIGHT_HOUSE,SNSTopic.PAGE_SPEED_LIGHT_HOUSE.name(),new Gson().toJson(categoryPayloadToSNS));
            }
        }
        return new PlatformBasicResponse();
    }

    public void triggerPageUrlsToSQS(String request) throws BadRequestException {
        logger.info("in PageSpeedManager, sending payload to SQS using triggerPageUrlsToSQS");
        if(StringUtils.isNotEmpty(request)){
            int start = 0, size =100;
            List<CategoryPage> categoryPages = new ArrayList<>();
            CategoryPagePayloadToSQS categoryPagePayloadToSQS;
            Gson gson = new Gson();
            CategoryPayloadToSNS payload = gson.fromJson(request, CategoryPayloadToSNS.class);
            do{
                categoryPages = categoryPageDAO.findCategoryPages(payload.getDomain(),payload.getCategoryId(), null, null, null, null, Boolean.FALSE, start, size);
                for(CategoryPage categoryPage: categoryPages){
                    categoryPagePayloadToSQS = new CategoryPagePayloadToSQS(payload.getCategoryId(), payload.getCategoryName(), payload.getDomain(), "https://www.vedantu.com"+categoryPage.getUrl(),categoryPage.getId());
                    awsSQSManager.sendToSQS(SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS, SQSMessageType.SEO_PAGE_SPEED_LIGHT_HOUSE, new Gson().toJson(categoryPagePayloadToSQS), SQSMessageType.SEO_PAGE_SPEED_LIGHT_HOUSE.name());
                    logger.info("send to sqs with payload: "+categoryPagePayloadToSQS.toString());
                }
                start = start + size; // equivalent to skip
            }while(categoryPages.size() == 100);
        }
    }

    public boolean createEditPageSpeedData(PageSpeedDataRequest pageSpeedData) throws VException {
        pageSpeedData.verify();
        logger.info("pageSpeedData verified. Required fields are present");

        logger.info("In createEditPageSpeedData of PageSpeedManager");
        logger.info("performance: "+pageSpeedData.getPerformance());
        logger.info("accessibility: "+pageSpeedData.getAccessibility());
        logger.info("best-practices: "+pageSpeedData.getBestPractices());
        logger.info("seo: "+pageSpeedData.getSeo());
        logger.info("pwa: "+pageSpeedData.getPwa());


        SeoPageSpeedData seoPageSpeedDataForUpdating = pageSpeedDAO.getByUrl(pageSpeedData.getUrl());
        if(seoPageSpeedDataForUpdating == null){
            logger.info("inserting into pageSpeedData");
            SeoPageSpeedData seoPageSpeedDataForCreating = new SeoPageSpeedData(pageSpeedData);
            pageSpeedDAO.create(seoPageSpeedDataForCreating);
            logger.info("inserting done");
        }else {
            //updating data
            logger.info("updating pageSpeedData");
            seoPageSpeedDataForUpdating.setPerformance(pageSpeedData.getPerformance());
            seoPageSpeedDataForUpdating.setAccessibility(pageSpeedData.getAccessibility());
            seoPageSpeedDataForUpdating.setBestPractices(pageSpeedData.getBestPractices());
            seoPageSpeedDataForUpdating.setSeo(pageSpeedData.getSeo());
            seoPageSpeedDataForUpdating.setPwa(pageSpeedData.getPwa());
            seoPageSpeedDataForUpdating.setOpportunityList(pageSpeedData.getOpportunityList());
            pageSpeedDAO.create(seoPageSpeedDataForUpdating);
            logger.info("updating done");
        }
        return true;
    }

    public PlatformBasicResponse seoPageUrlsSendToSQSWithLimit(SeoDomain seoDomain, Integer start, Integer end, Integer limit) throws BadRequestException {
        logger.info("start : "+ start + " end : " + end+ " limit : " + limit);
        if(start == null || end == null || limit == null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"start, end and limit are mandatory fields, which are not found");
        }
        List<CategoryPage> categoryPages;
        while (start <= end){
            logger.info("category page fetching start from : "+start);
            categoryPages = categoryPageDAO.findCategoryPages(seoDomain,null, null, null, null, null, Boolean.FALSE, start, limit);
            for(CategoryPage categoryPage:categoryPages){
                logger.info("categoryPage : "+categoryPage);
                CategoryPagePayloadToSQS categoryPagePayloadToSQS = new CategoryPagePayloadToSQS(categoryPage.getCategoryId(), categoryPage.getName(), categoryPage.getDomain(), "https://www.vedantu.com"+categoryPage.getUrl(),categoryPage.getId());
                awsSQSManager.sendToSQS(SQSQueue.PAGE_SPEED_LIGHT_HOUSE_SQS, SQSMessageType.SEO_PAGE_SPEED_LIGHT_HOUSE, new Gson().toJson(categoryPagePayloadToSQS), SQSMessageType.SEO_PAGE_SPEED_LIGHT_HOUSE.name());
            }
            start = start +limit;
        }
        return new PlatformBasicResponse();
    }
}
