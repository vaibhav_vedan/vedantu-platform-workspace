package com.vedantu.growth.seo.pageSpeed;

import com.vedantu.growth.seo.enums.SeoDomain;

public class CategoryPagePayloadToSQS {
    private String categoryId;
    private String categoryName;
    private SeoDomain domain;
    private String url;
    private String categoryPageId;

    public CategoryPagePayloadToSQS(){
        super();
    }

    public CategoryPagePayloadToSQS(String categoryId, String categoryName, SeoDomain domain, String url, String categoryPageId){
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.domain = domain;
        this.url = url;
        this.categoryPageId = categoryPageId;
    }


    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public SeoDomain getDomain() {
        return domain;
    }

    public void setDomain(SeoDomain domain) {
        this.domain = domain;
    }

    public String  getPageUrlsList() {
        return url;
    }

    public void setPageUrlsList(String url) {
        this.url = url;
    }

    public String getCategoryPageId() {
        return categoryPageId;
    }

    public void setCategoryPageId(String categoryPageId) {
        this.categoryPageId = categoryPageId;
    }
}
