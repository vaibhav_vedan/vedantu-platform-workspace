package com.vedantu.growth.seo.dao;

import com.vedantu.exception.VException;
import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.AppendPdfRuleSet;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.request.SeoQuestionAndAnswerFilterReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.*;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Service
public class CategoryPageDAO extends AbstractMongoDAO {

    private final Logger logger = LogFactory.getLogger(CategoryPageDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    public CategoryPageDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    @PostConstruct
    private void createTextIndex() {
//        getMongoOperations().indexOps(CategoryPage.class).ensureIndex(CategoryPage.getTextIndexDefinition());
    }

    public CategoryPage getCategoryPageById(String categoryPageId) {
        try {
            Assert.hasText(categoryPageId);
            return getEntityById(categoryPageId, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching CategoryPage with id " + categoryPageId,
                    ex);
        }
    }

    public List<CategoryPage> getCategoryPageByQuestionIds(Set<String> questionIds, SeoDomain domain, boolean getActivePages) {

        try {
            Assert.isTrue(ArrayUtils.isNotEmpty(questionIds));
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
            query.addCriteria(Criteria.where(CategoryPage.Constants.QUESTION_ID).in(questionIds));
            if (getActivePages) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(Boolean.TRUE));
                query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
            }
            return runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching CategoryPage with id " + questionIds,
                    ex);
        }
    }

    public List<CategoryPage> getCategoryPageByIds(List<String> categoryPageIds) {

        try {
            Assert.isTrue(ArrayUtils.isNotEmpty(categoryPageIds));
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.ID).in(categoryPageIds));
            return runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching CategoryPage with ids " + categoryPageIds,
                    ex);
        }
    }

    public List<CategoryPage> getCategoryPageByIds(Set<String> categoryPageIds) {

        try {
            Assert.isTrue(ArrayUtils.isNotEmpty(categoryPageIds));
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.ID).in(categoryPageIds));
            query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(Boolean.TRUE));
            query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
            return runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching CategoryPage with id " + categoryPageIds,
                    ex);
        }
    }

    public List<CategoryPage> getCategoryPageByUrls(Set<String> categoryPageUrls, SeoDomain seoDomain) {

        try {
            Assert.isTrue(ArrayUtils.isNotEmpty(categoryPageUrls));
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(seoDomain));
            query.addCriteria(Criteria.where(CategoryPage.Constants.URL).in(categoryPageUrls));
            return runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching CategoryPage with urls " + categoryPageUrls,
                    ex);
        }
    }

    public List<CategoryPage> findCategoryPages(SeoDomain domain, String categoryId, String categoryPageUrl, String categoryPageType,
                                                String categoryTarget, String categoryGrade, Boolean disabled,
                                                Integer start, Integer size) {
        return findCategoryPages(domain, categoryId, categoryPageUrl, categoryPageType, categoryTarget, categoryGrade, disabled, start, size, null);
    }

    public List<CategoryPage> findCategoryPages(SeoDomain domain, String categoryId, String categoryPageUrl, String categoryPageType,
                                                String categoryTarget, String categoryGrade, Boolean disabled,
                                                Integer start, Integer size, String previousPageUrl) {
        List<CategoryPage> categoryPages;

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
            query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(Boolean.TRUE));

            if (!StringUtils.isEmpty(categoryId)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.CATEGORY_ID).is(categoryId));
            }

            if (!StringUtils.isEmpty(categoryPageUrl)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.URL).is(categoryPageUrl));
            }

            if (!StringUtils.isEmpty(categoryPageType)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.PAGE_TYPE).is(categoryPageType));
            }

            if (!StringUtils.isEmpty(categoryTarget)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.TARGET).is(categoryTarget));
            }

            if (!StringUtils.isEmpty(categoryGrade)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.GRADE).is(categoryGrade));
            }

            if (null != disabled) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(disabled));
            }

            if (!StringUtils.isEmpty(previousPageUrl)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.PREVIOUS_URLS).all(previousPageUrl));
            }

            query.addCriteria(Criteria.where(CategoryPage.Constants.COPY_PAGE).is(false));

            query.with(Sort.by(Direction.DESC, CategoryPage.Constants.LAST_UPDATED));
            setFetchParameters(query, start, size);
            categoryPages = runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching all CategoryPages", ex);
        }

        return categoryPages;
    }


    public CategoryPage updateCategoryPage(CategoryPage categoryPage) {
        try {
            Assert.notNull(categoryPage);
            saveEntity(categoryPage);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "CategoryPageUpdateError : Error updating the CategoryPage " + categoryPage.toString(), ex);
        }

        return categoryPage;
    }

    public Map<String, Object> deleteCategoryPage(String id) {
        try {
            Assert.notNull(id);
            CategoryPage categoryPage = getEntityById(id, CategoryPage.class);

            HashMap<String, Object> response = new HashMap<>();
            response.put("deleted", true);
            response.put("url", categoryPage.getUrl());
            response.put("page", categoryPage);

            categoryPage.setActive(false);
            updateCategoryPage(categoryPage);

            return response;
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageDeleteError : Error deleting the CategoryPage with id " + id, ex);
        }
    }

    public AppendPdfRuleSet saveAppendPdfRuleSet(AppendPdfRuleSet appendPdfRuleSet) {
        saveEntity(appendPdfRuleSet);
        return appendPdfRuleSet;
    }

    public void deleteAppendPdfRuleSet(String id) {
        Query query = new Query();
        query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants._ID).is(id));
        Update update = new Update();
        update.set(AppendPdfRuleSet.Constants.ENTITY_STATE, EntityState.DELETED);
        updateFirst(query, update, AppendPdfRuleSet.class);
    }

    public List<AppendPdfRuleSet> getAppendPdfRuleSets(String queryString, String categoryId,
                                                       String target, String grade, String subject, String pageRelativeUrl, Integer start,
                                                       Integer size) throws VException {
        Query query = new Query();

        query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        if (!StringUtils.isEmpty(queryString)) {
            int length = queryString.length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants.NAME).regex(Pattern.quote(queryString.substring(0, index)), "i"));
        }
        if (!StringUtils.isEmpty(categoryId)) {
            query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants.CATEGORY_ID).is(categoryId));
        }
        if (!StringUtils.isEmpty(target)) {
            query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants.TARGET).is(target));
        }
        if (!StringUtils.isEmpty(grade)) {
            query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants.GRADE).is(grade));
        }
        if (!StringUtils.isEmpty(subject)) {
            query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants.SUBJECT).is(subject));
        }
        if (!StringUtils.isEmpty(pageRelativeUrl)) {
            query.addCriteria(Criteria.where(AppendPdfRuleSet.Constants.PAGE_RELATIVE_URL).is(pageRelativeUrl));
        }
        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.LAST_UPDATED));
        setFetchParameters(query, start, size);
        return runQuery(query, AppendPdfRuleSet.class);
    }

    public List<AppendPdfRuleSet> getRuleSetsForCategoryPage(CategoryPage page) {
        Query query = new Query();
        Criteria criteria = new Criteria();
        criteria.orOperator(
                Criteria.where(AppendPdfRuleSet.Constants.ENTITY_STATE).is(EntityState.ACTIVE.name()).and(AppendPdfRuleSet.Constants.CATEGORY_ID).is(page.getCategoryId()).and(AppendPdfRuleSet.Constants.TARGET).is(page.getTarget()).and(AppendPdfRuleSet.Constants.GRADE).is(page.getGrade()).and(AppendPdfRuleSet.Constants.SUBJECT).is(page.getSubject()),
                Criteria.where(AppendPdfRuleSet.Constants.ENTITY_STATE).is(EntityState.ACTIVE.name()).and(AppendPdfRuleSet.Constants.CATEGORY_ID).is(page.getCategoryId()).and(AppendPdfRuleSet.Constants.TARGET).is(page.getTarget()).and(AppendPdfRuleSet.Constants.GRADE).is(page.getGrade()),
                Criteria.where(AppendPdfRuleSet.Constants.ENTITY_STATE).is(EntityState.ACTIVE.name()).and(AppendPdfRuleSet.Constants.CATEGORY_ID).is(page.getCategoryId()).and(AppendPdfRuleSet.Constants.TARGET).is(page.getTarget()),
                Criteria.where(AppendPdfRuleSet.Constants.ENTITY_STATE).is(EntityState.ACTIVE.name()).and(AppendPdfRuleSet.Constants.CATEGORY_ID).is(page.getCategoryId()),
                Criteria.where(AppendPdfRuleSet.Constants.ENTITY_STATE).is(EntityState.ACTIVE.name()).and(AppendPdfRuleSet.Constants.PAGE_RELATIVE_URL).is(page.getUrl())
        );
        query = new Query(criteria);
        return runQuery(query, AppendPdfRuleSet.class);
    }


    public List<CategoryPage> getCategoryPagesFromSearchTerms(String[] searchTerms, String[] notMatchingSearchPhrases, String[] mustContainPhrases,
                                                              String pageId, int limit) {
        TextCriteria textCriteria = TextCriteria.forDefaultLanguage().matchingAny(searchTerms);
        if (notMatchingSearchPhrases != null) {
            for (String phrase : notMatchingSearchPhrases) {
                textCriteria.notMatchingPhrase(phrase);
            }
        }
        if (mustContainPhrases != null) {
            for (String phrase : mustContainPhrases) {
                textCriteria.matchingPhrase(phrase);
            }
        }

        Query query = TextQuery.queryText(textCriteria)
                .sortByScore()
                .includeScore()
                .with(PageRequest.of(0, limit));
        query.fields().include(CategoryPage.Constants._ID)
                .include(CategoryPage.Constants.TARGET)
                .include(CategoryPage.Constants.GRADE)
                .include(CategoryPage.Constants.SUBJECT);

//        query.addCriteria(Criteria.where(CategoryPage.Constants.INDEXABLE).is("YES"));
        query.addCriteria(Criteria.where(CategoryPage.Constants._ID).ne(pageId));
        query.addCriteria(Criteria.where(CategoryPage.Constants.PAGE_TYPE).is(CategoryPageType.QnA));
        query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(true));
        query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(false));
        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.LAST_UPDATED));
        return runQuery(query, CategoryPage.class);
    }

    public List<CategoryPage> getCategoryPageByUrl(SeoDomain domain, String url, Boolean disabled, Boolean active, Integer start, Integer size) {
        List<CategoryPage> categoryPages;

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));

            if (active != null) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(active));
            }

            if (!StringUtils.isEmpty(url)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.URL).is(url));
            }

            if (null != disabled) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(disabled));
            }

            query.with(Sort.by(Direction.DESC, CategoryPage.Constants.LAST_UPDATED));
            setFetchParameters(query, start, size);
            categoryPages = runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching all CategoryPages", ex);
        }

        return categoryPages;
    }

    public boolean checkIfCategoryPageExistsByUrl(SeoDomain domain, String url) {

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
            query.addCriteria(Criteria.where(CategoryPage.Constants.URL).is(url));

            return queryCount(query, CategoryPage.class) > 0;
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching all CategoryPages", ex);
        }

    }

    public List<CategoryPage> getDomainPages(SeoDomain domain, int page, int limit) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain))
                .with(PageRequest.of(page, limit))
                .with(Sort.by(Direction.DESC, CategoryPage.Constants.LAST_UPDATED));
        return runQuery(query, CategoryPage.class);
    }

    public List<CategoryPage> getBlogCategoryHomeMetadata(SeoDomain domain, String categoryId, Integer start, Integer limit) {

        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
        query.addCriteria(Criteria.where(CategoryPage.Constants.BLOG_CATEGORY_ID).is(categoryId));
        query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.BLOG_PUBLISHED_DATE));

        query.fields().include(CategoryPage.Constants._ID)
                .include(CategoryPage.Constants.FEATURE_IMAGE_URL)
                .include(CategoryPage.Constants.TITLE)
                .include(CategoryPage.Constants.URL)
                .include(CategoryPage.Constants.FEATURE_IMAGE_TAG)
                .include(CategoryPage.Constants.DESCRIPTION);

        setFetchParameters(query, start, limit);

        return runQuery(query, CategoryPage.class);
    }

    public List<CategoryPage> getNewsHomeMetadata(SeoDomain domain, String target, Integer start, Integer limit) {

        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
        query.addCriteria(Criteria.where(CategoryPage.Constants.PAGE_TYPE).is(CategoryPageType.NewsPage));
        query.addCriteria(Criteria.where(CategoryPage.Constants.TARGET).is(target));
        query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
        query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(Boolean.TRUE));
        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.BLOG_PUBLISHED_DATE));

        query.fields().include(CategoryPage.Constants._ID)
                .include(CategoryPage.Constants.FEATURE_IMAGE_URL)
                .include(CategoryPage.Constants.TITLE)
                .include(CategoryPage.Constants.URL)
                .include(CategoryPage.Constants.FEATURE_IMAGE_TAG)
                .include(CategoryPage.Constants.DESCRIPTION);

        setFetchParameters(query, start, limit);

        return runQuery(query, CategoryPage.class);
    }

    public Long getBlogCategoryPagesCount(SeoDomain domain, String categoryId) {

        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));

        if (StringUtils.isNotEmpty(categoryId))
            query.addCriteria(Criteria.where(CategoryPage.Constants.BLOG_CATEGORY_ID).is(categoryId));
        else
            query.addCriteria(Criteria.where(CategoryPage.Constants.BLOG_CATEGORY_ID).exists(Boolean.TRUE));

        query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.BLOG_PUBLISHED_DATE));

        query.fields().include(CategoryPage.Constants._ID)
                .include(CategoryPage.Constants.FEATURE_IMAGE_URL)
                .include(CategoryPage.Constants.TITLE)
                .include(CategoryPage.Constants.URL)
                .include(CategoryPage.Constants.DESCRIPTION);

        return queryCount(query, CategoryPage.class);
    }

    public List<CategoryPage> getLatestBlogsMetadata(SeoDomain domain, String blogCategoryId, Integer start, Integer limit) {

        Query query = new Query();

        if (domain != null)
            query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));

        query.addCriteria(Criteria.where(CategoryPage.Constants.CATEGORY_ID).is(blogCategoryId));
        query.addCriteria(Criteria.where(CategoryPage.Constants.BLOG_CATEGORY_ID).exists(Boolean.TRUE));
        query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.BLOG_PUBLISHED_DATE));
        query.fields().include(CategoryPage.Constants._ID)
                .include(CategoryPage.Constants.FEATURE_IMAGE_URL)
                .include(CategoryPage.Constants.TITLE)
                .include(CategoryPage.Constants.URL)
                .include(CategoryPage.Constants.FEATURE_IMAGE_TAG)
                .include(CategoryPage.Constants.DESCRIPTION);

        setFetchParameters(query, start, limit);

        logger.info("query: {}", query.toString());

        return runQuery(query, CategoryPage.class);

    }


    public List<CategoryPage> findCategoryPagesForInternalLinking(SeoDomain domain, String categoryPageType, Integer start, Integer size, Set<String> includeFields, String sortBy, Direction order, String ignoreTitle) {
        {
            List<CategoryPage> categoryPages;

            try {
                Query query = new Query();
                query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
                query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));

                if (!StringUtils.isEmpty(categoryPageType)) {
                    query.addCriteria(Criteria.where(CategoryPage.Constants.PAGE_TYPE).is(categoryPageType));
                }

                query.addCriteria(Criteria.where(CategoryPage.Constants.TITLE).ne(ignoreTitle));

                query.with(Sort.by(order, sortBy));

                if (CollectionUtils.isNotEmpty(includeFields)) {
                    for (String field : includeFields)
                        query.fields().include(field);
                }

                setFetchParameters(query, start, size);
                categoryPages = runQuery(query, CategoryPage.class);
            } catch (Exception ex) {
                throw new RuntimeException("CategoryPageGetError : Error fetching all CategoryPages", ex);
            }

            return categoryPages;
        }
    }

    public List<CategoryPage> getQnAFilterOptions(SeoDomain domain, CategoryPageType categoryPageType, String lastFetchedId, Integer size, Set<String> includeFields) {

        List<CategoryPage> categoryPages;

        try {
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
            query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));

            if (categoryPageType != null) {
                query.addCriteria(Criteria.where(CategoryPage.Constants.PAGE_TYPE).is(categoryPageType));
            }
            query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(Boolean.TRUE));
            if(StringUtils.isNotEmpty(lastFetchedId)) {
                query.addCriteria(Criteria.where(CategoryPage.Constants._ID).gt(new ObjectId(lastFetchedId)));
            }

            query.with(Sort.by(Direction.ASC, CategoryPage.Constants._ID));

            if (CollectionUtils.isNotEmpty(includeFields)) {
                for (String field : includeFields)
                    query.fields().include(field);
            }

            setFetchParameters(query, 0, size);

            logger.info("query :: {}", query);

            categoryPages = runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageGetError : Error fetching all CategoryPages", ex);
        }

        return categoryPages;

    }

    public List<CategoryPage> questionAndAnswerFilteredSet(SeoQuestionAndAnswerFilterReq filterReq, Set<String> includeFields) {

        List<CategoryPage> categoryPages;

        try {
            Query query = buildFilterQuery(filterReq, includeFields);

            categoryPages = runQuery(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("questionAndAnswerFilteredSet : Error fetching all CategoryPages", ex);
        }

        return categoryPages;
    }

    private Query buildFilterQuery(SeoQuestionAndAnswerFilterReq filterReq, Set<String> includeFields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(SeoDomain.VEDANTU));
        query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
        query.addCriteria(Criteria.where(CategoryPage.Constants.PAGE_TYPE).is(CategoryPageType.QnA));
        query.addCriteria(Criteria.where(CategoryPage.Constants.Active).is(Boolean.TRUE));

        if (CollectionUtils.isNotEmpty(filterReq.getTargets())) {
            query.addCriteria(Criteria.where(CategoryPage.Constants.TARGET).in(filterReq.getTargets()));
        }


        if (CollectionUtils.isNotEmpty(filterReq.getSubjects())) {
            List<String> subjects = filterReq.getSubjects().stream()
                    .filter(subject->StringUtils.isNotEmpty(subject))
                    .map(subject -> subject.toLowerCase())
                    .distinct().collect(Collectors.toList());
            query.addCriteria(Criteria.where(CategoryPage.Constants.SUBJECT).in(subjects));
        }

        if (CollectionUtils.isNotEmpty(filterReq.getGrades())) {
            query.addCriteria(Criteria.where(CategoryPage.Constants.GRADE).in(filterReq.getGrades()));
        }

        if (CollectionUtils.isNotEmpty(filterReq.getTopics())) {
            query.addCriteria(Criteria.where(CategoryPage.Constants.TOPIC).in(filterReq.getTopics()));
        }

        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.LAST_UPDATED));

        if (CollectionUtils.isNotEmpty(includeFields)) {
            for (String field : includeFields)
                query.fields().include(field);
        }

        setFetchParameters(query, filterReq.getStart(), filterReq.getSize());

        logger.info("query :: {}", query);
        return query;
    }

    public Long questionAndAnswerFilteredGetTotalCount(SeoQuestionAndAnswerFilterReq filterReq) {

        try {
            Query query = buildFilterQuery(filterReq, null);

            return queryCount(query, CategoryPage.class);
        } catch (Exception ex) {
            throw new RuntimeException("questionAndAnswerFilteredSet : Error fetching all CategoryPages", ex);
        }

    }

    public List<CategoryPage> getNewsMetadata(SeoDomain domain, CategoryPageType pageType, String target, Integer start, Integer size) {
        Query query = constructNewsMetadataQuery(domain, pageType, target, start, size);
        return runQuery(query, CategoryPage.class);
    }

    private Query constructNewsMetadataQuery(SeoDomain domain, CategoryPageType pageType, String target, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPage.Constants.DOMAIN).is(domain));
        query.addCriteria(Criteria.where(CategoryPage.Constants.PAGE_TYPE).is(pageType));
        if (StringUtils.isNotEmpty(target)) {
            query.addCriteria(Criteria.where(CategoryPage.Constants.TARGET).is(target));
        }
        query.addCriteria(Criteria.where(CategoryPage.Constants.DISABLED).is(Boolean.FALSE));
        query.with(Sort.by(Direction.DESC, CategoryPage.Constants.BLOG_PUBLISHED_DATE));
        query.fields().include(CategoryPage.Constants._ID)
                .include(CategoryPage.Constants.FEATURE_IMAGE_URL)
                .include(CategoryPage.Constants.TITLE)
                .include(CategoryPage.Constants.URL)
                .include(CategoryPage.Constants.BLOG_PUBLISHED_DATE)
                .include(CategoryPage.Constants.FEATURE_IMAGE_TAG)
                .include(CategoryPage.Constants.DESCRIPTION);

        setFetchParameters(query, start, size);

        logger.info("query: {}", query.toString());
        return query;
    }

    public Long getNewsMetadataByTargetCount(SeoDomain domain, CategoryPageType pageType, String target) {
        Query query = constructNewsMetadataQuery(domain, pageType, target, null, null);
        return queryCount(query, CategoryPage.class);
    }

}
