package com.vedantu.growth.seo.enums;

public enum MasterSidebarUploadOptions {
    ADD, IGNORE, OVERRIDE
}
