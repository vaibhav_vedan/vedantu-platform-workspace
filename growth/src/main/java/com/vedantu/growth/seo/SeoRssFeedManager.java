package com.vedantu.growth.seo;

import com.vedantu.growth.seo.dao.CategoryPageDAO;
import com.vedantu.growth.seo.dao.SeoRssFeedDao;
import com.vedantu.growth.seo.entity.CategoryPage;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.entity.SeoRssFeed;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.enums.SeoRssFeedGroup;
import com.vedantu.growth.seo.enums.SeoRssFeedObjectType;
import com.vedantu.growth.utils.Constants;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

@Service
public class SeoRssFeedManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoRssFeedManager.class);

    @Autowired
    private SeoRssFeedDao seoRssFeedDao;

    @Autowired
    private CategoryPageDAO categoryPageDAO;

    @Autowired
    private PageBuilderManager pageBuilderManager;

    private static final String BLOG_HOME_PAGE_URL = ConfigUtils.INSTANCE.getStringValue("blog.home.page.url");
    private static final String NEWS_HOME_PAGE_URL = ConfigUtils.INSTANCE.getStringValue("news.home.page.url");
    private static final String RSS_FEED_DESCRIPTION_TEXT = ConfigUtils.INSTANCE.getStringValue("rss.feed.description.text");
    private static final String RSS_VEDANTU_LOGO_URL = ConfigUtils.INSTANCE.getStringValue("rss.vedantu.logo.url");
    private static final String RSS_TITLE_BLOG = ConfigUtils.INSTANCE.getStringValue("rss.title.blog");
    private static final String RSS_TITLE_NEWS = ConfigUtils.INSTANCE.getStringValue("rss.title.news");
    private static final String RSS_IMAGE_TITLE = ConfigUtils.INSTANCE.getStringValue("rss.image.title");
    private static final String RSS_VERSION = ConfigUtils.INSTANCE.getStringValue("rss.version");

    public String getRssFeed(SeoRssFeedGroup group) throws TransformerException, ParserConfigurationException, IOException, SAXException {

        SeoRssFeed rssBaseFeed = seoRssFeedDao.getRssBaseFeed(SeoDomain.VEDANTU, group);

        if(rssBaseFeed == null)
            rssBaseFeed = generateBaseRssFeed(group);

        String baseData = rssBaseFeed.getData();

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
        Document baseDocument = documentBuilder.parse(new InputSource(new StringReader(baseData)));

        Node rss = baseDocument.getFirstChild();
        Node channel = rss.getFirstChild();

        List<SeoRssFeed> seoRssFeeds = seoRssFeedDao.getRssTitleFeeds(SeoDomain.VEDANTU, group);

        for(SeoRssFeed rssFeed : seoRssFeeds){

            String data = rssFeed.getData();

            DocumentBuilderFactory documentFactoryTitle = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilderTitle = documentFactoryTitle.newDocumentBuilder();
            Document document = documentBuilderTitle.parse(new InputSource(new StringReader(data)));
            Node title = document.getFirstChild();

            Node baseTitle = baseDocument.importNode(title, true);
            channel.appendChild(baseTitle);

        }

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(baseDocument);

        StringWriter writer = new StringWriter();

        transformer.transform(domSource, new StreamResult(writer));

        return writer.getBuffer().toString();
    }

    public SeoRssFeed generateBaseRssFeed(SeoRssFeedGroup group) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();

        Element rss = document.createElement(Constants.RSS);
        Attr attr = document.createAttribute(Constants.VERSION);
        attr.setValue(RSS_VERSION);
        rss.setAttributeNode(attr);
        document.appendChild(rss);

        Element channel = document.createElement(Constants.CHANNEL);
        rss.appendChild(channel);

        Element title = document.createElement(Constants.TITLE);
        String titleString = getTitle(group);
        title.appendChild(document.createTextNode(titleString));
        channel.appendChild(title);

        Element link = document.createElement(Constants.LINK);
        String linkUrl = getLinkUrl(group);
        link.appendChild(document.createTextNode(linkUrl));
        channel.appendChild(link);

        Element description = document.createElement(Constants.DESCRIPTION);
        String descriptionText = getDescriptionText(group);
        description.appendChild(document.createCDATASection(descriptionText));
        channel.appendChild(description);

        Element language = document.createElement(Constants.LANGUAGE);
        language.appendChild(document.createTextNode(Constants.EN_US));
        channel.appendChild(language);

        Element image = document.createElement(Constants.IMAGE);

        Element url = document.createElement(Constants.URL);
        String imageUrl = getImageUrl(group);
        url.appendChild(document.createTextNode(imageUrl));
        image.appendChild(url);

        Element imageTitle = document.createElement(Constants.TITLE);
        imageTitle.appendChild(document.createTextNode(RSS_IMAGE_TITLE));
        image.appendChild(imageTitle);

        Element landingPage = document.createElement(Constants.LINK);
        landingPage.appendChild(document.createTextNode(linkUrl));
        image.appendChild(landingPage);

        channel.appendChild(image);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);

        StringWriter writer = new StringWriter();

        transformer.transform(domSource, new StreamResult(writer));

        String data = writer.getBuffer().toString();
        SeoRssFeed rssFeed = seoRssFeedDao.getRssBaseFeed(SeoDomain.VEDANTU, group);

        if(rssFeed == null)
            rssFeed = new SeoRssFeed();

        rssFeed.setFeedGroup(group);
        rssFeed.setFeedType(SeoRssFeedObjectType.BASE);
        rssFeed.setData(data);

        seoRssFeedDao.upsert(rssFeed);

        return rssFeed;

    }

    private String getTitle(SeoRssFeedGroup group) {
        switch (group){
            case BLOG: return RSS_TITLE_BLOG;
            case NEWS: return RSS_TITLE_NEWS;
            default: return StringUtils.EMPTY;
        }
    }

    private String getImageUrl(SeoRssFeedGroup group) {
        return RSS_VEDANTU_LOGO_URL;
    }

    private String getDescriptionText(SeoRssFeedGroup group) {
        return RSS_FEED_DESCRIPTION_TEXT;
    }

    private String getLinkUrl(SeoRssFeedGroup group) {
        switch (group){
            case BLOG: return BLOG_HOME_PAGE_URL;
            case NEWS: return NEWS_HOME_PAGE_URL;
            default: return StringUtils.EMPTY;
        }
    }

    public String generateRssItem( String titleStr, String linkStr, String pubDateStr, String authorStr, String categoryStr, String descriptionStr) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

        DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

        Document document = documentBuilder.newDocument();;

        Element item = document.createElement(Constants.ITEM);

        Element itemTitle = document.createElement(Constants.TITLE);
        itemTitle.appendChild(document.createTextNode(StringUtils.defaultIfEmpty(titleStr)));
        item.appendChild(itemTitle);

        Element itemLink = document.createElement(Constants.LINK);
        itemLink.appendChild(document.createTextNode(StringUtils.defaultIfEmpty(linkStr)));
        item.appendChild(itemLink);

        Element pubDate = document.createElement(Constants.PUB_DATE);
        pubDate.appendChild(document.createTextNode(StringUtils.defaultIfEmpty(pubDateStr)));
        item.appendChild(pubDate);

        Element author = document.createElement(Constants.AUTHOR);
        author.appendChild(document.createTextNode(StringUtils.defaultIfEmpty(authorStr)));
        item.appendChild(author);

        Element category = document.createElement(Constants.CATEGORY);
        category.appendChild(document.createTextNode(StringUtils.defaultIfEmpty(categoryStr)));
        item.appendChild(category);

        Element titleDescription = document.createElement(Constants.DESCRIPTION);
        titleDescription.appendChild(document.createCDATASection(StringUtils.defaultIfEmpty(descriptionStr)));
        item.appendChild(titleDescription);

        document.appendChild(item);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource domSource = new DOMSource(document);

        StringWriter writer = new StringWriter();

        transformer.transform(domSource, new StreamResult(writer));

        return writer.getBuffer().toString();
    }

    public void upsertRssItem(String categoryPageId) {

        try {

            CategoryPage categoryPage = categoryPageDAO.getCategoryPageById(categoryPageId);

            if(categoryPage.getDisabled())
                return;

            pageBuilderManager.setCategoryName(categoryPage);

            SeoRssFeed rssFeed = seoRssFeedDao.getFeedObjectByCategoryPageId(categoryPageId);

            if(rssFeed == null){
                rssFeed = new SeoRssFeed();
            }

            String titleStr = categoryPage.getTitle();
            String linkStr = categoryPage.getUrl();

            SimpleDateFormat sdf = new SimpleDateFormat();
            sdf.setTimeZone(TimeZone.getTimeZone(Constants.UTC));
            String pubDateStr = sdf.format(new Date(categoryPage.getBlogPublishedDate()));

            String authorStr = categoryPage.getAuthorName();
            String categoryStr = categoryPage.getBlogCategoryName();
            String descriptionStr = categoryPage.getDescription();
            String item = generateRssItem(titleStr, linkStr, pubDateStr, authorStr, categoryStr, descriptionStr);

            SeoRssFeedGroup seoRssFeedGroup = getRssFeedGroup(categoryPage.getPageType());
            rssFeed.setFeedGroup(seoRssFeedGroup);
            rssFeed.setDomain(categoryPage.getDomain());
            rssFeed.setCategoryPageId(categoryPageId);
            rssFeed.setFeedType(SeoRssFeedObjectType.TITLE);
            rssFeed.setData(item);

            seoRssFeedDao.upsert(rssFeed);
        }catch (Exception e){
            logger.error("Error upserting the rss item for category page : {}", categoryPageId, e);
        }

    }

    private SeoRssFeedGroup getRssFeedGroup(CategoryPageType pageType) {
        switch (pageType){
            case BlogPage: return SeoRssFeedGroup.BLOG;
            case NewsPage: return SeoRssFeedGroup.NEWS;
            default: return null;
        }
    }

    public void deleteRssItem(String categoryPageId) {

    }
}
