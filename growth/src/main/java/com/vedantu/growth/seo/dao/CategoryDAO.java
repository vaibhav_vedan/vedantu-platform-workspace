package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.Category;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.regex.Pattern;

@Service
public class CategoryDAO extends AbstractMongoDAO {
        @Autowired
        private LogFactory logFactory;
        
	private final  Logger logger = logFactory.getLogger(CategoryDAO.class);

    
        @Autowired
        private MongoClientFactory mongoClientFactory;


        public CategoryDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }        

	public Category getCategoryById(String id) {
		Category category;

		try {
			Assert.hasText(id);
			category = getEntityById(id, Category.class);
		} catch (Exception ex) {
			throw new RuntimeException("CategoryGetError : Error fetch the category with id " + id, ex);
		}

		return category;
	}
	
	public Category getCategoryByName(String name, SeoDomain domain) {
		logger.info("getCategoryByName: " + name);
		List<Category> category;
		try {
			Query query = new Query();
			query.addCriteria(Criteria.where(Category.Constants.DOMAIN).is(domain));
			query.addCriteria(Criteria.where(Category.Constants.Name).regex(Pattern.quote(name), "i"));
			query.addCriteria(Criteria.where(Category.Constants.Active).ne(Boolean.FALSE));
			logger.info("query: " + query.toString());
			category = runQuery(query, Category.class);
		} catch (Exception ex) {
			throw new RuntimeException("CategoryGetError : Error fetch the category with name " + name, ex);
		}
		return category.get(0);
	}

	public List<Category> getAllCategories(SeoDomain domain) {
		List<Category> categories;

		try {
			Query query = new Query();
			query.addCriteria(Criteria.where(Category.Constants.DOMAIN).is(domain));
			query.addCriteria(Criteria.where(Category.Constants.Active).ne(Boolean.FALSE));

			categories = runQuery(query, Category.class);
		} catch (Exception ex) {
			throw new RuntimeException("CategoryGetError : Error fetching all Category", ex);
		}

		return categories;
	}

	public Category updateCategory(Category category) {
		try {
			Assert.notNull(category);
			saveEntity(category);
		} catch (Exception ex) {
			throw new RuntimeException("CategoryUpdateError : Error updating the category " + category.toString(), ex);
		}
		
		return category;
	}

	public Boolean deleteCategory(String id) {
		try {
			Assert.notNull(id);
			Category category = (Category) getEntityById(id, Category.class);

			category.setActive(false);
			updateCategory(category);

			return true;
		} catch (Exception ex) {
			throw new RuntimeException("CategoryDeleteError : Error deleting the category with id " + id, ex);
		}
	}
}