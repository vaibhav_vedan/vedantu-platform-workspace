package com.vedantu.growth.seo.entity;

import com.vedantu.growth.seo.enums.BlogPageType;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "BlogArticle")
public class BlogArticle extends AbstractSeoEntity{

    private BlogPageType blogPageType;
    private String blogCategoryId;
    private String title;
    private String description;

    public static class Constants extends AbstractSeoEntity.Constants {
        public static final String BLOG_PAGE_TYPE = "blogPageType";
        public static final String BLOG_CATEGORY_ID = "blogCategoryId";
        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
    }

}
