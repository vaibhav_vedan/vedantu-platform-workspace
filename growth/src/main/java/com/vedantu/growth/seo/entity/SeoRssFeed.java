package com.vedantu.growth.seo.entity;

import com.vedantu.growth.seo.enums.SeoRssFeedObjectType;
import com.vedantu.growth.seo.enums.SeoRssFeedGroup;
import lombok.Data;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@ToString
@Document(collection = "SeoRssFeed")
@CompoundIndexes({
        @CompoundIndex(name = "feedType_domain", def = "{'feedType': 1, 'domain': 1}", background = true)})
public class SeoRssFeed  extends AbstractSeoEntity {

    private SeoRssFeedObjectType feedType;
    private SeoRssFeedGroup feedGroup;

    @Indexed(background = true)
    private String categoryPageId;
    private String data;

    public static class Constants extends AbstractSeoEntity.Constants {

        public static String CATEGORY_PAGE_ID = "categoryPageId";
        public static String FEED_TYPE = "feedType";
        public static String FEED_GROUP = "feedGroup";

    }

}
