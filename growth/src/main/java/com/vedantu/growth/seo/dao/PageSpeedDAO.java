package com.vedantu.growth.seo.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.pageSpeed.SeoPageSpeedData;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

@Service
public class PageSpeedDAO extends AbstractMongoDAO {
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(PageSpeedDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public PageSpeedDAO(){
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }


    public SeoPageSpeedData getByUrl(String url) throws VException {
        if(StringUtils.isEmpty(url)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "url not found");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(SeoPageSpeedData.Constants.URL).is(url));
        SeoPageSpeedData seoPageSpeedData = findOne(query,SeoPageSpeedData.class);
        if(seoPageSpeedData != null){
            return seoPageSpeedData;
        }
        return null;
    }

    public void create(SeoPageSpeedData p){
        try {
            if(p != null){
                saveEntity(p);
            }
        }
        catch (Exception ex){
            logger.error("Create : Error Creating PageSpeed : "+ex.getMessage());
        }

    }


}
