package com.vedantu.growth.seo.entity;

import com.vedantu.util.ConfigUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@ToString
@Document(collection = "StudyBoard")
public class StudyBoard extends AbstractSeoEntity {

    @Indexed(background = true)
    private String userId;

    private List<String> notes = new ArrayList<>();
    private List<String> bookmarks = new ArrayList<>();
    private List<String> downloads = new ArrayList<>();

    public static class Constants extends AbstractSeoEntity.Constants {

        public static final int BOOKMARKS_LIMIT = ConfigUtils.INSTANCE.getIntValue("study.board.bookmarks.limit");
        public static final int DOWNLOAD_HISTORY_LIMIT = ConfigUtils.INSTANCE.getIntValue("study.board.downloads.history.limit", 20);
        public static final String DOWNLOADS = "downloads";
        public static final String BOOKMARKS = "bookmarks";
        public static final String NOTES = "notes";
        public static final String BOOKMARKS_LIMIT_EXCEEDED = "You have reached the limit of " + BOOKMARKS_LIMIT + " bookmarks. " +
                "Bookmarking this page shall remove your oldest bookmark.";
    }

}
