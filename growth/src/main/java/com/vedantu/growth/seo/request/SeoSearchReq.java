package com.vedantu.growth.seo.request;

import com.vedantu.cmds.enums.Difficulty;
import com.vedantu.growth.seo.entity.CategoryPageType;
import com.vedantu.growth.seo.enums.ESSearchMode;
import lombok.Data;

import java.util.Set;


@Data
public class SeoSearchReq {
    private String searchString;
    private Integer start = 0;
    private Integer size = 11;
    private ESSearchMode searchMode = ESSearchMode.RELATED_QNA_DOCS;
    private Set<String> sources;
    private CategoryPageType type;
    private String categoryPageId;
    private String url;
    private Difficulty difficulty;

    public static class Constants{

        public static final Integer MAX_SIZE = 20;

    }
}

