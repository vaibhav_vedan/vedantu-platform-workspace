package com.vedantu.growth.seo.entity;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryPageMetaTag {
	private List<Map<String, String>> attributes;

	public CategoryPageMetaTag() {
	}

	public CategoryPageMetaTag(List<Map<String, String>> attributes) {
		this.setAttributes(attributes);
	}

	public void addAttribute(String attributeName, String attributeValue) {
		Map<String, String> attribute = new HashMap<>();
		attribute.put("name", attributeName);
		attribute.put("value", attributeValue);

		this.attributes.add(attribute);
	}

	public List<Map<String, String>> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<Map<String, String>> attributes) {
		this.attributes = attributes;
	}

	@Override
	public String toString() {
		return (new Gson()).toJson(this);
	}
}