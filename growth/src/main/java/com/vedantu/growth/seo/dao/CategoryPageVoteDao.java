package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.CategoryPageVote;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class CategoryPageVoteDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(CategoryPageVoteDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public CategoryPageVote getByUserIdAndCategoryId(Long userId, String categoryPageId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(CategoryPageVote.Constants.CATEGORY_PAGE_ID).is(categoryPageId));
        List<CategoryPageVote> categoryPageVotes = runQuery(query, CategoryPageVote.class);
        if (categoryPageVotes.size() > 0) {
            return categoryPageVotes.get(0);
        }
        return null;

    }

    public Long getTotalUpvotes(String categoryPageId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CategoryPageVote.Constants.CATEGORY_PAGE_ID).is(categoryPageId));
        query.addCriteria(Criteria.where(CategoryPageVote.Constants.IS_UPVOTE).is(Boolean.TRUE));

        return queryCount(query, CategoryPageVote.class);

    }

    public void saveOrUpdateVote(CategoryPageVote categoryPageVote) {

        try {
            Assert.notNull(categoryPageVote);
            saveEntity(categoryPageVote);
        } catch (Exception ex) {
            throw new RuntimeException(
                    "categoryPageVoteError : Error updating the categoryPageVote " + categoryPageVote.toString(), ex);
        }
    }

    public void updateBookmarkFlag(String userId, String categoryPageId, boolean isBookmarked) {
        Query query = new Query()
                .addCriteria(Criteria.where(CategoryPageVote.Constants.USER_ID).is(new Long(userId)))
                .addCriteria(Criteria.where(CategoryPageVote.Constants.CATEGORY_PAGE_ID).is(categoryPageId));
        Update update = new Update()
                .set(CategoryPageVote.Constants.IS_BOOKMARKED, isBookmarked);

        upsertEntity(query, update, CategoryPageVote.class);
    }
}
