package com.vedantu.growth.seo.dao;

import com.vedantu.growth.dao.MongoClientFactory;
import com.vedantu.growth.seo.entity.CategoryPageHistory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

@Service
public class CategoryPageHistoryDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public CategoryPageHistoryDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void saveCategoryPageHistory(CategoryPageHistory categoryPageHistory, String callingUserId) {
        try {
            Assert.notNull(categoryPageHistory, "CategoryPageHistory object is null");
            saveEntity(categoryPageHistory, callingUserId);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageHistoryUpdateError: " +
                    "Error while saving the CategoryPage history " + categoryPageHistory.toString(), ex);
        }
    }

    public List<CategoryPageHistory> getCategoryPageHistory(String categoryPageId, Sort.Direction sort, long skip) {
        try {
            Assert.notNull(categoryPageId, "categoryPageId is null");
            Query query = new Query();
            query.addCriteria(Criteria.where(CategoryPageHistory.Constants.CATEGORY_PAGE_ID).is(categoryPageId));
            query.with(Sort.by(sort, CategoryPageHistory.Constants._ID));
            query.skip(skip);
            return runQuery(query, CategoryPageHistory.class);
        } catch (Exception ex) {
            throw new RuntimeException("CategoryPageHistoryGetError " +
                    ": Error while fetching CategoryPage history with categoryPageId " + categoryPageId, ex);
        }
    }
}
