package com.vedantu.growth.seo.entity;

import com.google.gson.Gson;

public class CategoryPageContentModule {
	private String imageTags;
	private String imageUrl;
	private String imageUrlWebp;

	public CategoryPageContentModule() {
	}

	public CategoryPageContentModule(String imageTags, String imageUrl) {
		this.imageTags = imageTags;
		this.imageUrl = imageUrl;
	}

	public CategoryPageContentModule(String imageTags, String imageUrl, String imageUrlWebp) {
		this.imageTags = imageTags;
		this.imageUrl = imageUrl;
		this.imageUrlWebp = imageUrlWebp;
	}

	public String getImageTags() {
		return imageTags;
	}

	public void setImageTags(String imageTags) {
		this.imageTags = imageTags;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getImageUrlWebp() {
		return imageUrlWebp;
	}

	public void setImageUrlWebp(String imageUrlWebp) {
		this.imageUrlWebp = imageUrlWebp;
	}

	@Override
	public String toString() {
		return (new Gson()).toJson(this);
	}
}