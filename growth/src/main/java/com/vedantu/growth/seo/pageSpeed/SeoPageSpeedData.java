package com.vedantu.growth.seo.pageSpeed;

import com.vedantu.growth.requests.PageSpeedDataRequest;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "SeoPageSpeedData")
public class SeoPageSpeedData extends AbstractMongoStringIdEntity {
    private String categoryId;
    private String categoryName;
    private String categoryPageId;
    private SeoDomain domain;
    private String url;
    private String performance;
    private String accessibility;
    private String bestPractices;
    private String seo;
    private String pwa;
    private List<PerformanceOpportunity> opportunityList;

    public SeoPageSpeedData(){
        super();
    }

    public SeoPageSpeedData(PageSpeedDataRequest request){
        this.categoryId = request.getCategoryId();
        this.categoryName = request.getCategoryName();
        this.categoryPageId = request.getCategoryPageId();
        this.domain = request.getDomain();
        this.url = request.getUrl();
        this.performance = request.getPerformance();
        this.accessibility = request.getAccessibility();
        this.bestPractices = request.getBestPractices();
        this.seo = request.getSeo();
        this.pwa = request.getPwa();
        this.opportunityList = request.getOpportunityList();
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryPageId() {
        return categoryPageId;
    }

    public void setCategoryPageId(String categoryPageId) {
        this.categoryPageId = categoryPageId;
    }

    public SeoDomain getDomain() {
        return domain;
    }

    public void setDomain(SeoDomain domain) {
        this.domain = domain;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPerformance() {
        return performance;
    }

    public void setPerformance(String performance) {
        this.performance = performance;
    }

    public String getAccessibility() {
        return accessibility;
    }

    public void setAccessibility(String accessibility) {
        this.accessibility = accessibility;
    }

    public String getBestPractices() {
        return bestPractices;
    }

    public void setBestPractices(String bestPractices) {
        this.bestPractices = bestPractices;
    }

    public String getSeo() {
        return seo;
    }

    public void setSeo(String seo) {
        this.seo = seo;
    }

    public String getPwa() {
        return pwa;
    }

    public void setPwa(String pwa) {
        this.pwa = pwa;
    }

    public List<PerformanceOpportunity> getOpportunityList() {
        return opportunityList;
    }

    public void setOpportunityList(List<PerformanceOpportunity> opportunityList) {
        this.opportunityList = opportunityList;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String URL = "url";
    }
}
