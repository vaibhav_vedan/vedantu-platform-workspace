package com.vedantu.growth.seo;

import com.google.gson.Gson;
import com.vedantu.growth.dao.RedisDAO;
import com.vedantu.growth.seo.enums.SeoDomain;
import com.vedantu.growth.seo.response.BlogCategoryResponse;
import com.vedantu.growth.seo.response.BlogHomePageResponse;
import com.vedantu.growth.seo.response.BlogMetadataPojo;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SeoNewsPageManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SeoNewsPageManager.class);

    @Autowired
    private PageBuilderManager pageBuilderManager;

    @Autowired
    private RedisDAO redisDAO;

    private static final Gson gson = new Gson();

    private static final String SEO_TARGETS_FOR_NEWS_HOMEPAGE = ConfigUtils.INSTANCE.getStringValue("seo.targets.for.news.homepage");
    private static final Integer indexStartValue = 0;
    public static final String NEWS_HOME_PAGE_RESPONSE_REDIS_KEY_PREFIX = "NEWS_HOME_PAGE_RESPONSE_";

    public BlogHomePageResponse getNewsHomepage(SeoDomain domain, Integer size) {

        String newsHomePageResponseRedisKey = NEWS_HOME_PAGE_RESPONSE_REDIS_KEY_PREFIX + domain;
        try{
            String responseFromRedis = redisDAO.get(newsHomePageResponseRedisKey);
            if(StringUtils.isNotEmpty(responseFromRedis)) {
                return gson.fromJson(responseFromRedis, BlogHomePageResponse.class);
            }
        }catch (Exception e){
            logger.error("Error fetching from redis", e);
        }

        String[] targets = SEO_TARGETS_FOR_NEWS_HOMEPAGE.split(",");
        BlogHomePageResponse homePageResponse = new BlogHomePageResponse();
        List<BlogCategoryResponse> categoryResponses = new ArrayList<>();
        for(String target : targets) {
            target = target.trim();
            BlogCategoryResponse categoryResponse = getNewsCategoryHomepage(domain, indexStartValue, size, target);
            categoryResponses.add(categoryResponse);
        }
        homePageResponse.setCategories(categoryResponses);
        BlogCategoryResponse categoryResponse = getNewsCategoryHomepage(domain, indexStartValue, size, null);
        homePageResponse.setLatestBlogs(categoryResponse.getBlogs());

        try{
            redisDAO.setex(newsHomePageResponseRedisKey, gson.toJson(homePageResponse), DateTimeUtils.SECONDS_PER_HOUR);
        }catch (Exception e){
            logger.error("Error fetching from redis", e);
        }

        return homePageResponse;
    }

    public BlogCategoryResponse getNewsCategoryHomepage(SeoDomain domain, Integer start, Integer size, String target) {
        List<BlogMetadataPojo> blogMetadataPojos = pageBuilderManager.getNewsMetadata(domain, target, start, size);
        Long recordCount = pageBuilderManager.getNewsCountByTarget(domain, target);
        BlogCategoryResponse blogCategoryResponse = new BlogCategoryResponse.Builder()
                .name(target)
                .totalBlogs(recordCount)
                .blogs(blogMetadataPojos)
                .build();

        return blogCategoryResponse;
    }

}
