#!/bin/bash
source ../build.config
source build.config

echo "$skipCI";
echo "$profile"


if [ "$skipCI" == "false" ] 
then
tag=$tag
branch=default
bucket=$BUILD_BUCKET

if [ "$CI_BRANCH" == "master" ] || [ "$CI_BRANCH" == "develop" ] 
	then 
		branch="$CI_BRANCH"
		
fi

if [ "$tag" == "" ] 
	then 
		tag=$CI_COMMIT_ID
fi

echo $tag
echo $profile

SUBSYSTEM=growth

echo ${BUILD_PROFILE}

#mvn clean install
#mvn compile -P "${BUILD_PROFILE}"
mvn clean package -P "${BUILD_PROFILE}"

filename="${HOME}/${REPO}/${SUBSYSTEM}/target/build/${SUBSYSTEM}.war"
filename_all="${HOME}/build/all/${SUBSYSTEM}.war"
mkdir -p "${HOME}/${REPO}/${SUBSYSTEM}/target/build"

echo ${filename}
echo ${filename_all}

cp "${HOME}/${REPO}/${SUBSYSTEM}/target/${SUBSYSTEM}.war" ${filename}
cp "${HOME}/${REPO}/${SUBSYSTEM}/target/${SUBSYSTEM}.war" ${filename_all}
cp "${HOME}/${REPO}/${SUBSYSTEM}/appspec.yml" "${HOME}/${REPO}/${SUBSYSTEM}/target/build/appspec.yml"
cp -R "${HOME}/${REPO}/${SUBSYSTEM}/deploymentScripts" "${HOME}/${REPO}/${SUBSYSTEM}/target/build/deploymentScripts"
cd "${HOME}/${REPO}/${SUBSYSTEM}/target"
tar -czvf "${BUILD_PROFILE}-${tag}.tar.gz" -C "${HOME}/${REPO}/${SUBSYSTEM}/target/build" .
mkdir -p "${HOME}/${REPO}/${SUBSYSTEM}/target/build/tar"
#mv "${profile}-${tag}.tar.gz" "${HOME}/${REPO}/${SUBSYSTEM}/target/build/tar/${tag}.tar.gz"
if [[ "$profile" == "prod" || "$profile" == "qa" ]];
	then
	aws s3 cp . "s3://${bucket}/${REPO}/${branch}/${SUBSYSTEM}/" --recursive --exclude "*" --include "*.tar.gz"
fi
#aws s3 cp "${BUILD_PROFILE}-${tag}.tar.gz" "s3://${bucket}/${REPO}/${branch}/${SUBSYSTEM}/"
fi
