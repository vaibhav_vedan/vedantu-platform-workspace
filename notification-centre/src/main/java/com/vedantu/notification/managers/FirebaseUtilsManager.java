package com.vedantu.notification.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.MobileRegToken;
import com.vedantu.notification.entity.Notification;
import com.vedantu.notification.pojo.FCMNotificationMessage;
import com.vedantu.notification.pojos.BookAppNotificationInfo;
import com.vedantu.notification.request.RegisterMobileRegTokenReq;
import com.vedantu.notification.requests.PushNotificationReq;
import com.vedantu.notification.serializers.MobileRegTokenDAO;
import com.vedantu.notification.serializers.NotificationDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PreDestroy;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class FirebaseUtilsManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(FirebaseUtilsManager.class);

	@Autowired
	private NotificationDAO notificationDAO;

	@Autowired
	private MobileRegTokenDAO mobileRegTokenDAO;
//	private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
        
        @Autowired
        private DozerBeanMapper mapper;        

	private final Gson gson = new Gson();
	private static String fcmKey = ConfigUtils.INSTANCE.getStringValue("fcm.key");

	private static String fcmEndpoint = ConfigUtils.INSTANCE.getStringValue("fcm.endpoint");

	public PlatformBasicResponse registerMobileRegToken(RegisterMobileRegTokenReq req) throws BadRequestException {
		if (StringUtils.isEmpty(req.getRegToken())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "regToken");
		}
		MobileRegToken newToken = mapper.map(req, MobileRegToken.class);
		List<MobileRegToken> existingTokens = mobileRegTokenDAO.getTokens(req.getDeviceId(), req.getApp(), null);
		if (ArrayUtils.isNotEmpty(existingTokens)) {
			MobileRegToken existingToken = existingTokens.get(0);
			newToken.setId(existingToken.getId());
			if (existingTokens.size() > 1) {
//				logger.error("more than 1 token found for " + req);
			}
		}
		mobileRegTokenDAO.create(newToken);
		return new PlatformBasicResponse();
	}

	public PlatformBasicResponse removeMobileRegToken(RegisterMobileRegTokenReq req) {
		int deletedTokens = mobileRegTokenDAO.deleteTokens(req.getDeviceId(), req.getApp(), req.getRegToken());
		logger.info("deletedTokens " + deletedTokens);
		return new PlatformBasicResponse();
	}

	public PlatformBasicResponse pushNotification(PushNotificationReq req) throws Exception {
		if (StringUtils.isNotEmpty(req.getChannel())) {
			req.setSendTo("/topics/"+req.getChannel());			
		}
		if(StringUtils.isNotEmpty(req.getSendTo())){
			pushFirebaseNotification(req);
		}
		return new PlatformBasicResponse();
	}

	private void pushFirebaseNotification(PushNotificationReq req) throws Exception {
		req.verify();

		if (StringUtils.isEmpty(req.getSendTo())) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "send to is empty");
		}

		Notification notification = mapper.map(req, Notification.class);
		notification.setIsSeen(Boolean.FALSE);
		if(req.getAppInfo() != null){
			notification.setInfo(gson.toJson(req.getAppInfo()));
		}
		List<String> errors = new ArrayList<>();
		BookAppNotificationInfo notiInfo = req.getAppInfo();
		if("WEBINAR".equals(notification.getEntityType())){
			
			if(StringUtils.isEmpty(notiInfo.getTitle())){
				errors.add("title");
			}
			if(StringUtils.isEmpty(notiInfo.getTopic())){
				errors.add("topic");
			}
			if(StringUtils.isEmpty(notiInfo.getTeacherName())){
				errors.add("teacherName");
			}
			if(StringUtils.isEmpty(notiInfo.getDescription())){
				errors.add("description");
			}
			
		}else if("IMAGE".equals(notification.getEntityType())){
			if(StringUtils.isEmpty(notiInfo.getImageLink())){
				errors.add("imageLink");
			}
		}
		if(ArrayUtils.isNotEmpty(errors)){
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,errors.toString());
		}
		notificationDAO.create(notification, req.getCallingUserId());
		try {
			constructAndSendFcm(notification, gson.toJson(notification));
		} catch (Exception e) {
			logger.error("Error in sending FCM message. for notification id : " + notification.getId());
			throw e; 
		}
	}

	// Construct FCM Message
	private void constructAndSendFcm(Notification notifRequest, String notificationData) throws Exception {
		Map<String, String> data = new LinkedHashMap<>();
		data.put("type", notifRequest.getType().toString());
		data.put("info", notificationData);
		data.put("appInfo", notifRequest.getInfo());

//		Map<String, String> notification = new LinkedHashMap<>();
//		notification.put("title", notifRequest.getHeader());
//		notification.put("body", notifRequest.getBody());
		FCMNotificationMessage message = new FCMNotificationMessage();
		message.setTo(notifRequest.getSendTo());
		message.setData(data);
		try {
			sendFcmNotification(message);
		} catch (Exception e) {
			throw e;
		}
	}

	// Send GCM Message
	private void sendFcmNotification(FCMNotificationMessage message) throws Exception {
		try {
			ClientResponse resp = WebUtils.INSTANCE.doCall(fcmEndpoint, HttpMethod.POST, new Gson().toJson(message),
					"key=" + fcmKey);
			logger.info("response:" + resp.getEntity(String.class));
		} catch (Exception e) {
			logger.error("Error Sending fcm Notificatons", e);
			throw e;
		}

	}
        
   
}
