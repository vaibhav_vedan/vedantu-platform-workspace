/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.managers;

import com.vedantu.User.Role;
import com.vedantu.User.TeacherInfo;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.MessageTrigger;
import com.vedantu.notification.entity.MessageUser;
import com.vedantu.notification.enums.TriggerStatus;
import com.vedantu.notification.request.HandleMessageTriggerReq;
import com.vedantu.notification.serializers.MessageTriggerDAO;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class MessageTriggerManager {

    @Autowired
    MessageTriggerDAO messageTriggerDAO;

    

    public void handleMessageTrigger(HandleMessageTriggerReq request) throws VException {
        request.verify();
        User fromUser = request.getFromuser();
        User toUser = request.getToUser();
        MessageUser messageUser = request.getMessageUser();
        Long studentId = fromUser.getId();
        Long teacherId = toUser.getId();

        if (!Role.STUDENT.equals(fromUser.getRole())) {
            studentId = studentId ^ teacherId;
            teacherId = studentId ^ teacherId;
            studentId = studentId ^ teacherId;
        }

        List<MessageTrigger> messageTriggers = messageTriggerDAO
                .getMessageTrigger(studentId, teacherId);
        MessageTrigger messageTrigger = null;
        if (messageTriggers != null && !messageTriggers.isEmpty()) {
            messageTrigger = messageTriggers.get(0);
            if (studentId != fromUser.getId()
                    && TriggerStatus.ACTIVE.equals(messageTrigger.getStatus())) {
                messageTriggerDAO.markMessageTriggerInactive(messageTrigger);
            }
        } else // Create message trigger
        {
            if (!teacherId.equals(fromUser.getId())) {
                Long lastUpdated = messageUser.getLastUpdated();
                User teacher = toUser;
                TeacherInfo teacherInfo = teacher.getTeacherInfo();
                Long responseTime = teacherInfo.getResponseTime();
                if (responseTime == null || responseTime <= 0) {
                    responseTime = ConfigUtils.INSTANCE
                            .getLongValue("teacher.response.default.time.millis");
                }

                messageTrigger = new MessageTrigger(messageUser.getId(), studentId,
                        teacherId, TriggerStatus.ACTIVE, getTriggerTime(
                                lastUpdated, responseTime), false);
                messageTriggerDAO.addMessageTrigger(messageTrigger);
            }
        }
    }

    public Long getTriggerTime(Long lastUpdated, Long responseTime) {
        // Calculate trigger time
        Long startTime = ConfigUtils.INSTANCE
                .getLongValue("customer.care.active.start.time")
                * DateTimeUtils.MILLIS_PER_HOUR;
        Long endTime = ConfigUtils.INSTANCE
                .getLongValue("customer.care.active.end.time")
                * DateTimeUtils.MILLIS_PER_HOUR;
        Long dayInMillis = new Long(DateTimeUtils.MILLIS_PER_DAY);

        // set lastUpdated time to the active hours
        Long lastUpdatedMod = lastUpdated % dayInMillis;
        if (lastUpdatedMod < startTime) {
            lastUpdated = lastUpdated - lastUpdatedMod + startTime;
        } else if (lastUpdatedMod >= endTime) {
            lastUpdated = lastUpdated - lastUpdatedMod + startTime
                    + dayInMillis;
        }

        Long triggerTime = lastUpdated + responseTime;
        Long triggerTimeMod = triggerTime % dayInMillis;
        if (triggerTimeMod < startTime || triggerTimeMod > endTime) {
            triggerTime = triggerTime + dayInMillis - endTime + startTime;
        }

        return triggerTime;
    }
}
