package com.vedantu.notification.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.*;
import com.vedantu.notification.entity.VQuizNotification;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.notification.request.VQuizNotificationReq;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.notification.serializers.RedisDAO;
import com.vedantu.notification.serializers.VQuizNotificationDAO;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * @author MNPK
 */

@Service
public class VQuizNotificationManager {

    @Autowired
    private LogFactory logFactory;

    Logger logger = logFactory.getLogger(VQuizNotificationManager.class);

    @Autowired
    private VQuizNotificationDAO vQuizNotificationDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private EmailManager emailManager;

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;



    public PlatformBasicResponse vQuizRemindMe(VQuizNotificationReq req) throws VException {
        logger.info("VQuizRemindMeReq : " + req);
        logger.info("getting the VQuizNotification by using userId : " + req.userId + " quizId : " + req.getQuizId());
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(req.getUserId(), false);
        if (userBasicInfo == null) {
            throw new BadRequestException(ErrorCode.USER_NOT_FOUND, "Please provide valid userId");
        }
        VQuizNotification vQuizNotification1 = vQuizNotificationDAO.getVQuizNotificationByUserIdAndQuizId(req.getUserId(), req.getQuizId());
        logger.info("VQuizNotification from DB : " + vQuizNotification1);
        if (vQuizNotification1 != null) {
            throw new BadRequestException(ErrorCode.ALREADY_EXISTS, "already scheduled remind Me subscription with userId : " + req.getUserId() + " quizId : " + req.getQuizId());
        }
        Long scheduleTime;
        if (req.getScheduleTime() != null) {
            scheduleTime = req.getScheduleTime();
        } else {
            Long bufferTime = ConfigUtils.INSTANCE.getLongValue("vquiz.remindme.buffertime.millis", 600000l);
            logger.info("bufferTime : " + bufferTime);
            scheduleTime = req.getStartTime() - bufferTime;
        }
        logger.info("scheduleTime : " + scheduleTime);
        VQuizNotification vQuizNotification2 = new VQuizNotification(req.getUserId(), req.getQuizId(), scheduleTime, NotificationStatus.SCHEDULED);
        logger.info("VQuizNotification : " + vQuizNotification2);
        Boolean isEntryCreated = remindEntryCreateRaceCondition(req.getUserId(),req.getQuizId());
        if(isEntryCreated){
            vQuizNotificationDAO.create(vQuizNotification2);
        }
        return new PlatformBasicResponse();
    }

    private Boolean remindEntryCreateRaceCondition(Long userId, String quizId){
        String KEY = "VQUIZ_REMIND_LOCK_"+userId+"_"+quizId;
        return redisDAO.setnx(KEY, "1", 60);
    }

    public void sendVQuizRemindMeNotification() throws VException, IOException, JSONException {
        logger.info("Request for consuming the SCHEDULED status vQuiz notifications");
        int start = 0;
        int size = 500;
        String currLastFetchedId = "";
        List<String> includeFields = Arrays.asList(VQuizNotification.Constants.QUIZ_ID, VQuizNotification.Constants.USER_ID);
        while (true) {
            long currentSystemTime = System.currentTimeMillis();
            logger.info("currentSystemTime : " + currentSystemTime);
            List<VQuizNotification> vQuizNotifications = vQuizNotificationDAO.getVQuizNotifications(currentSystemTime, NotificationStatus.SCHEDULED, currLastFetchedId, includeFields, size);
            if(CollectionUtils.isEmpty(vQuizNotifications)){
                break;
            }
            sendVquizNotificationViaSQS(vQuizNotifications);

            for (VQuizNotification v : vQuizNotifications) {
                if (v.getId().compareTo(currLastFetchedId) > 0) {
                    currLastFetchedId = v.getId();
                }
            }
            if(vQuizNotifications.size() < size){
                break;
            }
            if(start > 500000) {
                logger.error("VQUIZ Notification remainder crosses 5 Lakh");
            }
            start = start + size;
        }
    }

    private void sendVquizNotificationViaSQS(List<VQuizNotification> vQuizNotificationList) {
        SQSMessageType messageType = SQSMessageType.VQUIZ_REMINDER;
        awsSQSManager.sendToSQS(SQSQueue.VQUIZ_REMINDER_QUEUE, messageType, new Gson().toJson(vQuizNotificationList));
    }

    private Boolean checkVQuizNotificationRedisEntryAndSet(VQuizNotification vQuizNotification) throws VException {
        logger.info("Checking Redis entry for Notification already in process or not");
        if (vQuizNotification != null && vQuizNotification.getUserId() != null && StringUtils.isNotEmpty(vQuizNotification.getQuizId())) {
            String key = "VQUIZ_" + vQuizNotification.getUserId() + "_" + vQuizNotification.getQuizId();
            logger.info("getting the redis entry by using key : " + key);
            String fetchedRedisRes = redisDAO.get(key);
            logger.info("fetchedRedisRes : " + fetchedRedisRes);
            if (StringUtils.isNotEmpty(fetchedRedisRes)) {
                VQuizNotification vQuizNotification1 = new Gson().fromJson(fetchedRedisRes, VQuizNotification.class);
                logger.info("checkVQuizNotificationRedisEntryAndSet - vQuizNotification1 : " + vQuizNotification1);
                return Boolean.TRUE;
            }
            redisDAO.setex(key, new Gson().toJson(vQuizNotification), DateTimeUtils.SECONDS_PER_MINUTE);
        }
        return Boolean.FALSE;
    }

    public void sendVquizNotification(List<VQuizNotification> vQuizNotifications) throws VException, IOException, JSONException {
        logger.info("sendVquizNotification - vQuizNotifications : " + vQuizNotifications.size());
        if (ArrayUtils.isNotEmpty(vQuizNotifications)) {
            List<Long> userIds = new ArrayList<>();
            List<String> quizIds = new ArrayList<>();
            Map<String, Map<String, Object>> quizInfoMap = new HashMap<>();
            for (VQuizNotification vQuizNotification : vQuizNotifications) {
                if (vQuizNotification != null && vQuizNotification.getUserId() != null) {
                    userIds.add(vQuizNotification.getUserId());
                }
                if (vQuizNotification != null && StringUtils.isNotEmpty(vQuizNotification.getQuizId()) && !quizIds.contains(vQuizNotification.getQuizId())) {
                    quizIds.add(vQuizNotification.getQuizId());
                    Map<String, Object> quiz = getQuizById(vQuizNotification.getQuizId());
                    if (quiz != null) {
                        quizInfoMap.put(vQuizNotification.getQuizId(), quiz);
                    }
                }
            }
            logger.info("getting users by using userIds : " + userIds);
            Map<Long, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
            logger.info("quizInfoMap count : " + quizInfoMap.size() + " userBasicInfoMap count : " + userBasicInfoMap.size());

            for (VQuizNotification vQuizNotification : vQuizNotifications) {
                if (vQuizNotification != null && vQuizNotification.getUserId() != null && StringUtils.isNotEmpty(vQuizNotification.getQuizId())) {
                    sendSMS(userBasicInfoMap.get(vQuizNotification.getUserId()), quizInfoMap.get(vQuizNotification.getQuizId()));
                }
            }
        }
    }

    private Map<String, Object> getQuizById(String quizId) throws VException {
        logger.info("fetching quiz By Id : " + quizId);
        if (StringUtils.isNotEmpty(quizId)) {
            Map<String, Object> redisResponse = getQuizByIdFromRedis(quizId);
            if (redisResponse != null) {
                return redisResponse;
            }
            //example url :: https://vquiz-dev.vedantu.com/quiz/<quizId>
            String VQUIZ_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("VQUIZ_ENDPOINT");
            String url = VQUIZ_ENDPOINT + "quiz/" + quizId;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            logger.info("response of get quiz By id : " + jsonString);
            Map<String, Object> obj = new Gson().fromJson(jsonString, new TypeToken<Map<String, Object>>() {
            }.getType());
            if (obj == null || obj.get("result") == null) {
                logger.info("quiz result not found");
                return null;
            }
            logger.info("obj : " + obj);
            Map<String, Object> result = (Map<String, Object>) obj.get("result");
            if (result == null) {
                logger.info("result  not found");
                return null;
            }
            logger.info("setting quiz result to redis : " + result);
            String KEY = "VQUIZ_INFO_" + quizId;
            redisDAO.setex(KEY, new Gson().toJson(result), DateTimeUtils.MILLIS_PER_DAY);
            return result;
        }
        return null;
    }

    private Map<String, Object> getQuizByIdFromRedis(String quizId) throws BadRequestException, InternalServerErrorException {
        if (StringUtils.isNotEmpty(quizId)) {
            String KEY = "VQUIZ_INFO_" + quizId;
            String respObj = redisDAO.get(KEY);
            logger.info("KEY : " + KEY + " respObj : " + respObj);
            if (StringUtils.isNotEmpty(respObj)) {
                return new Gson().fromJson(respObj, new TypeToken<Map<String, Object>>() {
                }.getType());
            }
        }
        return null;
    }

    private void sendSMS(UserBasicInfo userBasicInfo, Map<String, Object> quiz) throws InternalServerErrorException, BadRequestException, JSONException, IOException {
        logger.info("userBasicInfo : " + userBasicInfo + " quiz : " + quiz);
        if (userBasicInfo != null && StringUtils.isNotEmpty(userBasicInfo.getContactNumber())) {
            TextSMSRequest textSMSRequest = new TextSMSRequest();
            textSMSRequest.setType(CommunicationType.VQUIZ_REMIND_NOTIFICATION);
            textSMSRequest.setTo(userBasicInfo.getContactNumber());
            if (StringUtils.isNotEmpty(userBasicInfo.getPhoneCode())) {
                textSMSRequest.setPhoneCode(userBasicInfo.getPhoneCode());
            } else {
                textSMSRequest.setPhoneCode("91");
            }
            if (userBasicInfo.getRole() != null) {
                textSMSRequest.setRole(userBasicInfo.getRole());
            } else {
                textSMSRequest.setRole(Role.STUDENT);
            }
            textSMSRequest.setScopeParams(getSMSBodyScopes(userBasicInfo, quiz));
            logger.info("sending sms with textSMSRequest : " + textSMSRequest);
            smsManager.sendSms(textSMSRequest);
            logger.info("Hopefully SMS is sent successfully");
        }
    }

    private HashMap<String, Object> getSMSBodyScopes(UserBasicInfo userBasicInfo, Map<String, Object> quiz) {
        HashMap<String, Object> bodyScopes = new HashMap<>();
        if (userBasicInfo != null && StringUtils.isNotEmpty(userBasicInfo.getFirstName())) {
            bodyScopes.put("studentName", userBasicInfo.getFirstName());
        } else {
            bodyScopes.put("studentName", "student");
        }
        if (quiz != null && quiz.get("prizeAmount") != null) {
            Double prizeAmount = (Double) quiz.get("prizeAmount");
            int amount = (int) (prizeAmount * 1);
            bodyScopes.put("prizeAmount", amount);
        } else {
            bodyScopes.put("prizeAmount", 20000);
        }
        if (quiz != null && quiz.get("startTime") != null) {
            String startTime = (String) quiz.get("startTime");
            logger.info("startTime : " + startTime);
            DateTimeFormatter f = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSz");
            LocalDateTime localDateTime = LocalDateTime.parse(startTime, f);
            long epoch = localDateTime.atZone(ZoneId.systemDefault()).toInstant().getEpochSecond();
            logger.info("epoch : " + epoch);
            SimpleDateFormat sdf = new SimpleDateFormat("hh:mm a");
            sdf.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
            String dateTime = sdf.format(new Date(epoch * 1000));
            logger.info("dateTime : " + dateTime);
            bodyScopes.put("startTime", dateTime);
        } else {
            bodyScopes.put("startTime", "7 PM");
        }
        logger.info("bodyScopes : " + bodyScopes);
        return bodyScopes;
    }

}