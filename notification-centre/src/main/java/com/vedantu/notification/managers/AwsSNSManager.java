/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.managers;

import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class AwsSNSManager extends AbstractAwsSNSManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);    
    
    public AwsSNSManager() {
        super();
        logger.info("initializing AwsSNSManager");
    }    
    
    @Override
    public void createTopics() {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void createSubscriptions() {
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "noticeBoard/updateNoticeBoardDB");
        createCronSubscription(CronTopic.CRON_CHIME_30_Minutes, "manualnotif/sendScheduledEmailOrSMS");
        createCronSubscription(CronTopic.CRON_CHIME_5_Minutes, "email/consumeBulkEmail");
        createCronSubscription(CronTopic.CRON_CHIME_15_Minutes, "/vquiz/sendVQuizRemindMeNotification");
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }



}
