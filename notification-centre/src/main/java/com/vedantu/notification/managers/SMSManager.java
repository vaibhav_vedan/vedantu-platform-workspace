package com.vedantu.notification.managers;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.notification.async.AsyncTaskName;
import com.vedantu.notification.dao.BlockedPhoneDao;
import com.vedantu.notification.entity.BlockedPhone;
import com.vedantu.notification.entity.GupshupDeliveryReport;
import com.vedantu.notification.entity.SmsLinksOpenStatus;
import com.vedantu.notification.entity.UserCommunicationCount;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.request.GupshupDeliveryReportReq;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.SMSPriorityType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.blockedPhone.BlockedPhoneRes;
import com.vedantu.notification.serializers.GupshupDeliveryReportDAO;
import com.vedantu.notification.serializers.RedisDAO;
import com.vedantu.notification.serializers.UserCommunicationCountDAO;
import com.vedantu.notification.thirdparty.GupShupManager;
import com.vedantu.notification.thirdparty.MgageManager;
import com.vedantu.notification.thirdparty.OTPPlusManager;
import com.vedantu.notification.thirdparty.TwilioManager;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.EncryptionUtil;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

/*
 * Manager Service for SMS APIs
 */
@Service
public class SMSManager {

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private GupshupDeliveryReportDAO gupshupDeliveryReportDAO;

    @Autowired
    private TwilioManager twilioManager;

    @Autowired
    private GupShupManager gupShupManager;

    @Autowired
    private MgageManager mgageManager;

    @Autowired
    private OTPPlusManager otpPlusManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SMSManager.class);

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private BlockedPhoneDao blockedPhoneDao;

    @Autowired
    private UserCommunicationCountDAO userCommunicationCountDAO;

    private static Boolean isSMSEnabled;

    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private static final Long DAILY_SMS_LIMIT  = Long.parseLong(ConfigUtils.INSTANCE.properties.getProperty("daily.sms.limit"));
    private final Integer TWO_DAYS_IN_SECONDS = 172800;


    private static final List<CommunicationType> avoidFilterList = new ArrayList<>(Arrays.asList(CommunicationType.PHONE_VERIFICATION, CommunicationType.FORGOT_PASSWORD,
            CommunicationType.GENERATE_PASSWORD, CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD_SMS, CommunicationType.OTF_SESSION_FEEDBACK, CommunicationType.WAVE_LEADERBOARD_RECOGNITION,
            CommunicationType.WAVE_STREAK_RECOGNITION,CommunicationType.FOS_DEMO_SESSION_CREATION, CommunicationType.VQUIZ_REMIND_NOTIFICATION));

    @Autowired
    private DozerBeanMapper mapper;

    private static final int MAX_NUM_RETRIES = 3;

    public SMSManager() {
        super();
        isSMSEnabled = ConfigUtils.INSTANCE.getBooleanValue("sms.enabled");
    }



    /*
	 * Method to send SMS via calling the Message Delivery System's API
	 *
	 * @param TextSMS Object containing message data
     */
    public BaseResponse sendSms(TextSMSRequest sms) throws IOException, JSONException, BadRequestException, InternalServerErrorException {

        logger.info("Entering TextSMS: " + sms.toString());
        BaseResponse jsonResp = new BaseResponse();

        //check if contactNumber have the empty spaces
        if(StringUtils.isNotEmpty(sms.getTo()) && " ".contains(sms.getTo())){
            logger.warn("contact Number : "+ sms.getTo() + " contains the white spaces");
            jsonResp.setErrorCode(ErrorCode.SUCCESS);
            jsonResp.setErrorMessage("sms not allowed to sent contactNumber contains the space");
            return jsonResp;
        }

        // check if country code is 91 or +91 and phone number length is 8 (dummy number)
        if(("+91").equals(sms.getPhoneCode()) || "91".equals(sms.getPhoneCode())){
            if(sms.getTo().length() == 8){
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("dummy number sms not allowed");
                return jsonResp;
            }
        }

        if (CommunicationType.PHONE_VERIFICATION != sms.getType() &&
            CommunicationType.EMAIL_VERIFICATION != sms.getType() &&
            CommunicationType.MANUAL_NOTIFICATION != sms.getType() &&
            CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS != sms.getType() &&
            CommunicationType.GENERATE_PASSWORD != sms.getType() &&
            SMSPriorityType.HIGH != sms.getPriorityType()) {

            long currentTime = System.currentTimeMillis();
            long expectedDelivery = sms.getExpectedDeliveryMillis() > 0 ? sms.getExpectedDeliveryMillis() : sms.getCreatedAt();
            long delay = currentTime - expectedDelivery;

            if (delay > sms.getMaxDelayMillis()) {
                jsonResp.setErrorCode(ErrorCode.DELAYED_REQUEST);
                jsonResp.setErrorMessage("sms not sent because delayed more than allowed");
                logger.info("Expected Delivery " + expectedDelivery);
                logger.info("Expected Delay " + sms.getMaxDelayMillis());
                logger.info("Delay in sending " + delay);
                logger.warn("Not Sending SMS to " + sms.getTo());
                return jsonResp;
            }
        }

        if(null != sms.getType() && !avoidFilterList.contains(sms.getType())) {
            boolean sendSMS = mongoFilterForSMSCount(sms);
            if (!sendSMS) {
                jsonResp.setErrorCode(ErrorCode.RATE_LIMIT_EXCEEDED);
                jsonResp.setErrorMessage("sms not sent because sms count limit exceeded");
                logger.warn("Not Sending SMS to " + sms.getTo());
                return jsonResp;
            }
        }

        // try {
        if (isSMSEnabled) {
            // Temp block assignment share
            if(null != sms.getType() && CommunicationType.ASSIGNMENT_SHARE.equals(sms.getType())){
                logger.info("ASSIGNMENT SHARE SMS BLOCKED TEMPORARILY");
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("");
                return jsonResp;
            }

            if(null != sms.getType() && !avoidFilterList.contains(sms.getType())){
                BlockedPhone phone = blockedPhoneDao.getByContactNumberAndPhoneCode(sms.getTo(), sms.getPhoneCode());
                if(null != phone){
                    jsonResp.setErrorCode(ErrorCode.BAD_REQUEST_ERROR);
                    jsonResp.setErrorMessage("phone number : " + sms.getPhoneCode() + " " + sms.getTo() + " is blocked by the system");
                    return jsonResp;
                }
            }

            if (StringUtils.isEmpty(sms.getBody())) {
                setSMSFromTemplate(sms);
            }
             // add unsubscribe link
            if(null != sms.getType() && !avoidFilterList.contains(sms.getType())){
                addPhoneUnsubscribeLink(sms);
            }

//            if(null != sms.getType() && CommunicationType.OTM_TRIAL_SESSION.equals(sms.getType())){
//                addPhoneUnsubscribeLink(sms);
//            }

            if (StringUtils.isEmpty(sms.getBody())) {
                jsonResp.setErrorCode(ErrorCode.BAD_REQUEST_ERROR);
                jsonResp.setErrorMessage("SMS body empty");
                return jsonResp;
            }
            //for(int i=0; i<MAX_NUM_RETRIES; i++){
            //    try{
            if (StringUtils.isEmpty(sms.getPhoneCode()) || "91".equals(sms.getPhoneCode())) {
                boolean highPriority = false;
                if (CommunicationType.PHONE_VERIFICATION.equals(sms.getType())
                        || CommunicationType.GENERATE_PASSWORD.equals(sms.getType())
                        || SMSPriorityType.HIGH.equals(sms.getPriorityType())) {
                    highPriority = true;
                    //                                        jsonResp = gupShupManager.sendSMS(sms.getTo(), sms.getBody(), highPriority);
                    //                                    } else {
                    //                                        jsonResp = exotelManager.sendSMS(sms.getTo(), sms.getBody(), highPriority);
                    if (CommunicationType.PHONE_VERIFICATION.equals(sms.getType())) {
//                        if(null != sms.getPreferredSMSType() && sms.getPreferredSMSType() == 0){
//                            jsonResp = mgageManager.sendSMS(sms.getTo(), sms.getBody(), true, sms.getType(), sms.getRole());
//                        }
//                        else{
//                            jsonResp = gupShupManager.sendSMS(sms.getTo(), sms.getBody(), highPriority, sms.getType(), sms.getRole());
//                        }

                        if((StringUtils.isNotEmpty(env))) {
                            if (env.equalsIgnoreCase("QA2") || env.equalsIgnoreCase("QA3") || env.equalsIgnoreCase("PROD")) {
                                jsonResp = otpPlusManager.sendSMS(sms.getTo(), sms.getBody(), true, sms.getType(), sms.getRole(), sms.getSqsMessageId());
                                incrementRedisValueForOTPPlus();
                            }else {
                                jsonResp = mgageManager.sendSMS(sms.getTo(), sms.getBody(), true, sms.getType(), sms.getRole(), sms.getSqsMessageId());
                                incrementRedisValueForMgage();
                            }
                        }
                        if (!ErrorCode.SUCCESS.equals(jsonResp.errorCode)) {
                            //jsonResp = gupShupManager.sendSMS(sms.getTo(), sms.getBody(), highPriority, sms.getType(), sms.getRole(), sms.getSqsMessageId());
                            jsonResp = mgageManager.sendSMS(sms.getTo(), sms.getBody(), highPriority, sms.getType(), sms.getRole(), sms.getSqsMessageId());
                            incrementRedisValueForMgage();
                        }

//                        if (!ErrorCode.SUCCESS.equals(jsonResp.errorCode)) {
//                            //jsonResp = gupShupManager.sendSMS(sms.getTo(), sms.getBody(), highPriority, sms.getType(), sms.getRole(), sms.getSqsMessageId());
//                            jsonResp = gupShupManager.sendSMS(sms.getTo(), sms.getBody(), highPriority, sms.getType(), sms.getRole(), sms.getSqsMessageId());
//                            incrementRedisValueForMgage();
//                        }
                    }
                    else {
                        if(env.equalsIgnoreCase("PROD")) {
                            jsonResp = mgageManager.sendSMS(sms.getTo(), sms.getBody(), true, sms.getType(), sms.getRole(), sms.getSqsMessageId());
                            incrementRedisValueForMgage();
                        }
//                        if (!ErrorCode.SUCCESS.equals(jsonResp.errorCode)) {
//                            jsonResp = gupShupManager.sendSMS(sms.getTo(), sms.getBody(), true, sms.getType(), sms.getRole(), sms.getSqsMessageId());
//                        }
                    }
                } else {
                    Long sentSMSCount = getSentSMSMgageCount();
                    if(sentSMSCount > DAILY_SMS_LIMIT){
                        if(sentSMSCount % 10000 == 0) {
                            // sentry error
                            logger.error("DAILY SMS LIMIT EXCEEDED : " + sentSMSCount);
                        }
                        return jsonResp;
                    }
                    jsonResp = mgageManager.sendSMS(sms.getTo(), sms.getBody(), highPriority, sms.getType(), sms.getRole(), sms.getSqsMessageId());
                    incrementRedisValueForMgage();
                    //jsonResp = gupShupManager.sendSMS(sms.getTo(), sms.getBody(), highPriority, sms.getType(), sms.getRole(), sms.getSqsMessageId());
                }
            } else if (CommunicationType.PHONE_VERIFICATION.equals(sms.getType())
                    || CommunicationType.GENERATE_PASSWORD.equals(sms.getType())
                    || CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS.equals(sms.getType())
                    || CommunicationType.WEBINAR_REGISTRATION.equals(sms.getType())
                    || CommunicationType.HOME_DEMO_REQUEST.equals(sms.getType())) {
                String number = "+" + sms.getPhoneCode() + sms.getTo();
                jsonResp = twilioManager.sendSMS(number, sms.getBody(), true, sms.getSqsMessageId());
            } else {
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage(
                        "Non-verification international sms not allowed");

            }

            /*     if(!ErrorCode.SUCCESS.equals(jsonResp.getErrorCode())){
                            throw new Exception("Error in sending sms: "+jsonResp.getErrorMessage());
                        }

                        break;
                    }catch(Exception ex){
                        if(i < MAX_NUM_RETRIES - 1){
                            logger.warn("Error in sending sms: "+ex.getMessage());
                            continue;
                        }

                        if(i == MAX_NUM_RETRIES - 1){
                            logger.error("Error in sending sms: "+ex.getMessage());
                            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in sending sms");
                        }

                    }
                }*/
        } else {
            jsonResp.setErrorCode(ErrorCode.SUCCESS);
            jsonResp.setErrorMessage(
                    "SMS is not enabled for the environment. Enable it from application.properties");
        }
        //} catch (JSONException e) {
        //    jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
        //    jsonResp.setErrorMessage("Internal Server Error. Error Parsing JSON response");
        //    return jsonResp;
        //}

        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    public Boolean mongoFilterForSMSCount(TextSMSRequest request){
        logger.info("Entry: mongoFilterForSMSCount. Request: "+request.toString());
        Integer limit = Integer.parseInt(ConfigUtils.INSTANCE.properties.getProperty("sms.limit.per.communication.type"));
        int count = userCommunicationCountDAO.getCountByAddress(request.getTo(), formatDate(System.currentTimeMillis()), request.getType());
        logger.info("count is: "+count);
        String date = formatDate(System.currentTimeMillis());
        if(count == 0){
            logger.info("Saving address with count 1: "+request.getTo());
            userCommunicationCountDAO.save(new UserCommunicationCount(request.getTo(), date, request.getType()));
            return true;
        }
        if(count <= limit) {
            logger.info("Updating count for the address: "+request.getTo());
            userCommunicationCountDAO.incrementUserDayCount(request.getTo(), date, request.getType());
            return true;
        }
        logger.info("Exit: mongoFilterForSMSCount");
        return false;
    }




    public void addPhoneUnsubscribeLink(TextSMSRequest textSMSRequest) {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("SELF_ENDPOINT");
        String encryptedPhoneCode = "";
        String encryptedPhoneNumber = "";
        try{
            if(null != textSMSRequest.getPhoneCode()) {
                encryptedPhoneCode = EncryptionUtil.encrypt(textSMSRequest.getPhoneCode().getBytes());
            }
            if(null != textSMSRequest.getTo()) {
                encryptedPhoneNumber = EncryptionUtil.encrypt(textSMSRequest.getTo().getBytes());
            }
        }catch (Exception e) {
            throw new RuntimeException(e);
        }
        String unsubscribeLink = notificationEndpoint + "SMS/unsubscribeSMS" + "?contactNumber=" + encryptedPhoneNumber + "&phoneCode=" + encryptedPhoneCode + "&type=" + textSMSRequest.getType();
        String shortUrl = WebUtils.INSTANCE.shortenUrl(unsubscribeLink);
        String body = textSMSRequest.getBody() + " Unsubscribe - " + shortUrl;
        textSMSRequest.setBody(body);
    }

    private void incrementRedisValueForMgage() {
        String key = "SMS_COUNT_MGAGE_" + formatDate(System.currentTimeMillis());
        try {
            redisDAO.incrAndExpireAt(key, TWO_DAYS_IN_SECONDS);
        } catch (Exception e) {
            logger.info("Error in incrementing value to redis" + e.getMessage());
        }
    }

    private void incrementRedisValueForOTPPlus() {
        String key = "SMS_COUNT_OTPPLUS_" + formatDate(System.currentTimeMillis());
        try {
            redisDAO.incrAndExpireAt(key, TWO_DAYS_IN_SECONDS);
        } catch (Exception e) {
            logger.info("Error in incrementing value to redis" + e.getMessage());
        }
    }

    private Long getSentSMSMgageCount() {
        Long count = -1L;
        String key = "SMS_COUNT_MGAGE_" + formatDate(System.currentTimeMillis());
        try{
            String value = redisDAO.get(key);
            if (!StringUtils.isEmpty(value)) {
                count = Long.parseLong(value);
            }
        }catch(VException e){
            logger.warn("Error in getting value from redis : key :" + key);
            return -1L;
        }

        return count;
    }

    public static String formatDate(Long date) {
        if (date == null || date <= 0) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_IN));
        return sdf.format(new Date(date));
    }

    /*
	 * Method to send Bulk SMSs via calling the Message Delivery System's API
	 *
	 * @param BulkTextSMS Object containing message data
     */
    public BaseResponse sendBulkSms(BulkTextSMSRequest bulkSms) {

        logger.info("Entering TextSMS: " + bulkSms.toString());
        BaseResponse jsonResp = new BaseResponse();

        if (CommunicationType.PHONE_VERIFICATION != bulkSms.getType() &&
                CommunicationType.EMAIL_VERIFICATION != bulkSms.getType() &&
                CommunicationType.MANUAL_NOTIFICATION != bulkSms.getType() &&
                CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS != bulkSms.getType() &&
                CommunicationType.GENERATE_PASSWORD != bulkSms.getType() &&
                SMSPriorityType.HIGH != bulkSms.getPriorityType()) {

            long currentTime = System.currentTimeMillis();
            long expectedDelivery = bulkSms.getExpectedDeliveryMillis() > 0 ? bulkSms.getExpectedDeliveryMillis() : bulkSms.getCreatedAt();
            long delay = currentTime - expectedDelivery;

            if (delay > bulkSms.getMaxDelayMillis()) {
                jsonResp.setErrorCode(ErrorCode.DELAYED_REQUEST);
                jsonResp.setErrorMessage("bulkSms not sent because delayed more than allowed");
                logger.info("Expected Delivery " + expectedDelivery);
                logger.info("Expected Delay " + bulkSms.getMaxDelayMillis());
                logger.info("Delay in sending " + delay);
                logger.warn("Not Sending SMS to " + bulkSms.getTo());
                return jsonResp;
            }
        }

        try {
            if (isSMSEnabled) {
                if (StringUtils.isEmpty(bulkSms.getBody())) {
                    setSMSFromTemplate(bulkSms);
                }
                if (StringUtils.isEmpty(bulkSms.getBody())) {
                    jsonResp.setErrorCode(ErrorCode.BAD_REQUEST_ERROR);
                    jsonResp.setErrorMessage("SMS body empty");
                    return jsonResp;
                }
                boolean highPriority = false;
                if (CommunicationType.PHONE_VERIFICATION.equals(bulkSms.getType())
                        || CommunicationType.GENERATE_PASSWORD.equals(bulkSms.getType())
                        || CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS.equals(bulkSms.getType())
                        || SMSPriorityType.HIGH.equals(bulkSms.getPriorityType())) {
                    highPriority = true;
                }
                jsonResp = gupShupManager.sendBulkSMS(bulkSms.getTo(), bulkSms.getBody(), highPriority, bulkSms.getManualNotificationId(), bulkSms.getType(), bulkSms.getRole());
            } else {
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage(
                        "SMS is not enabled for the environment. Enable it from application.properties");
            }
        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
            return jsonResp;
        }

        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    public void setSMSFromTemplate(TextSMSRequest sms) throws IOException {
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        String propertyValue = getPropertyValue(sms.getType(), sms.getRole());
        if (StringUtils.isEmpty(propertyValue)) {
            return;
        }
        Mustache mustache = mf.compile(new StringReader(propertyValue), "sms");
        mustache.execute(writer, sms.getScopeParams());
        writer.flush();
        writer.close();
        sms.setBody(writer.toString());
    }

    public void setSMSFromTemplate(BulkTextSMSRequest sms) throws IOException {
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        String propertyValue = getPropertyValue(sms.getType(), sms.getRole());
        if (StringUtils.isEmpty(propertyValue)) {
            return;
        }
        Mustache mustache = mf.compile(new StringReader(propertyValue), "sms");
        mustache.execute(writer, sms.getScopeParams());
        writer.flush();
        writer.close();
        sms.setBody(writer.toString());
    }

    public String getPropertyValue(CommunicationType type, Role role) {
        String propertyValue = ConfigUtils.INSTANCE.getStringValue("sms." + type.name() + "." + role.name());

        if (StringUtils.isEmpty(propertyValue)) {
            propertyValue = ConfigUtils.INSTANCE.getStringValue("sms." + type.name());
        }
        return propertyValue;
    }

    public PlatformBasicResponse gupshupDeliveryreport(GupshupDeliveryReportReq req) throws VException {
        GupshupDeliveryReport report = gupshupDeliveryReportDAO.getReportByExternalId(req.getExternalId());
        if (report == null) {
            report = mapper.map(req, GupshupDeliveryReport.class);
        } else {
            report.setDeliveredTS(req.getDeliveredTS());
            report.setStatus(req.getStatus());
            report.setCause(req.getCause());
        }
        gupshupDeliveryReportDAO.create(report);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse sendncertapplink(TextSMSRequest req) throws JSONException, VException {
        if (StringUtils.isEmpty(req.getTo())) {
            throw new BadRequestException(ErrorCode.INVALID_PHONE_NUMBER, "INVALID_PHONE_NUMBER");
        }
        Map<String, Object> payload = new HashMap<>();
        String phoneCode = "91";
        if (StringUtils.isNotEmpty(req.getPhoneCode())) {
            phoneCode = req.getPhoneCode();
        }
        TextSMSRequest newreq = new TextSMSRequest(req.getTo(), phoneCode, null, CommunicationType.NCERT_SOLUTIONS_APP_PLAY_STORE_LINK);
        newreq.setRole(Role.STUDENT);
        payload.put("smsRequest", newreq);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_NCERT_SOLUTIONS_PLAY_STORE_LINK, payload);
        asyncTaskFactory.executeTask(params);
        return new PlatformBasicResponse();
    }

    public List<GupshupDeliveryReport> getDeliveryReportForManualNotification(List<String> manualNotificationIds) {
        return gupshupDeliveryReportDAO.getDeliveryReportForManualNotification(manualNotificationIds);
    }

    //smsLinkCliked
    public PlatformBasicResponse smsLinkClicked(String clickId, String linkRandomId) {
        logger.info("Entering  smsLinkClicked");
        GupshupDeliveryReport gupshupDeliveryReport = gupshupDeliveryReportDAO.getById(clickId);
        if (gupshupDeliveryReport != null) {
//            gupshupDeliveryReport.setClicked(true);
            if (StringUtils.isNotEmpty(linkRandomId) && ArrayUtils.isNotEmpty(gupshupDeliveryReport.getSmsLinks())) {
                for (SmsLinksOpenStatus link : gupshupDeliveryReport.getSmsLinks()) {
                    if (linkRandomId.equals(link.getRandomId())) {
                        if (link.isClicked()) {
                            link.setLastUpdated(System.currentTimeMillis());
                        } else {
                            link.setLastUpdated(System.currentTimeMillis());
                            link.setCreationTime(System.currentTimeMillis());
                        }
                        link.setClicked(true);
                    }
                }
            }
            gupshupDeliveryReportDAO.create(gupshupDeliveryReport);
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse addBlockedPhone(TextSMSRequest req) throws BadRequestException {
        BlockedPhone blockedPhone = blockedPhoneDao.getAnyByContactNumberAndPhoneCode(req.getTo(), req.getPhoneCode());
        if(null != blockedPhone){
            blockedPhone.setBlocked(true);
        }
        else{
            blockedPhone = new BlockedPhone();
            blockedPhone.setContactNumber(req.getTo());
            blockedPhone.setPhoneCode(req.getPhoneCode());
            blockedPhone.setBlocked(true);
        }

        if(StringUtils.isNotEmpty(req.getBody())){
            blockedPhone.setType(req.getBody());
        }

        blockedPhone.setIpAddress(req.getIpAddress());
        try {
            blockedPhoneDao.create(blockedPhone);
        }catch(Exception e){
            logger.info("Error" + e);
            throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY, "Phone number and Phone code combination already exists");
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse unblockBlockedPhone(TextSMSRequest req) throws Exception {
        BlockedPhone blockedPhone = blockedPhoneDao.getByContactNumberAndPhoneCode(req.getTo(), req.getPhoneCode());
        if(null != blockedPhone){
            blockedPhone.setBlocked(false);
            blockedPhoneDao.save(blockedPhone);
        }
        return new PlatformBasicResponse();
    }

    public BlockedPhoneRes getBlockedPhones(Integer start, Integer size) {
        if(size > 25)
            size = 25;

       return blockedPhoneDao.getBlockedPhones(start, size);
    }

    public BlockedPhone getBlockedPhone(TextSMSRequest req) {
        return blockedPhoneDao.getByContactNumberAndPhoneCode(req.getTo(), req.getPhoneCode());
    }

    public PlatformBasicResponse unSubscribeSMS(String encryptedContactNumber, String encryptedPhoneCode, String type) throws BadRequestException {
        String decryptedContactNumber;
        String decryptedPhoneCode = "";
        try{
            byte[] numberBytes = DatatypeConverter.parseHexBinary(encryptedContactNumber);
            decryptedContactNumber = EncryptionUtil.decrypt(numberBytes);
            if(!encryptedPhoneCode.equals("")) {
                byte[] codeBytes = DatatypeConverter.parseHexBinary(encryptedPhoneCode);
                decryptedPhoneCode = EncryptionUtil.decrypt(codeBytes);
            }
        }catch (Exception e){
            return new PlatformBasicResponse(false, "", "");
        }
        TextSMSRequest unsubscribeReq = new TextSMSRequest();
        unsubscribeReq.setTo(decryptedContactNumber);
        unsubscribeReq.setPhoneCode(decryptedPhoneCode);
        unsubscribeReq.setBody(type);
        return addBlockedPhone(unsubscribeReq);
    }
}
