package com.vedantu.notification.managers;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.EmailTemplate;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.request.CreateEmailTemplateReq;
import com.vedantu.notification.response.EmailTemplatesWithFilterRes;
import com.vedantu.notification.serializers.EmailTemplateDAO;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EmailTemplateManager {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(EmailTemplateManager.class);

    @Autowired
    private EmailTemplateDAO emailTemplateDAO;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private FosUtils fosUtils;

    public PlatformBasicResponse createEmailTemplate(CreateEmailTemplateReq req) throws VException {
        EmailTemplate emailTemplate = new EmailTemplate(req.getEmailType(), req.getRole(), req.getUnSubscriptionCheck(), req.getUnSubscriptionLink(), req.getDescription());
        emailTemplateDAO.create(emailTemplate, sessionUtils.getCallingUserId());
        return new PlatformBasicResponse();
    }

    public List<EmailTemplate> getEmailTemplates(){
        Query query = new Query();
        return emailTemplateDAO.runQuery(query, EmailTemplate.class);
    }

    public List<EmailTemplatesWithFilterRes> getEmailTemplatesWithFilter(Integer start, Integer limit, EntityState state){
        List<EmailTemplatesWithFilterRes> response = new ArrayList<>();
        List<EmailTemplate> emailTemplates = emailTemplateDAO.getEmailTemplatesWithFilter(start, limit, state);
        Set<String> uniqueUserIdsSet = new HashSet<>();

        emailTemplates.forEach(emailTemplate -> {
            if(emailTemplate != null){
                if(StringUtils.isNotEmpty(emailTemplate.getCreatedBy())){
                    uniqueUserIdsSet.add(emailTemplate.getCreatedBy());
                }
                if(StringUtils.isNotEmpty(emailTemplate.getLastUpdatedBy())){
                    uniqueUserIdsSet.add(emailTemplate.getLastUpdatedBy());
                }
            }
        });

        List<String> uniqueUserIds = uniqueUserIdsSet.stream().collect(Collectors.toList());
        logger.info("uniqueUserIds : " + uniqueUserIds);
        Map<String, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMap(uniqueUserIds, Boolean.TRUE);
        for (EmailTemplate emailTemplate : emailTemplates) {
            String createdByEmail = null;
            if (StringUtils.isNotEmpty(emailTemplate.getCreatedBy())) {
                if (userBasicInfoMap.get(emailTemplate.getCreatedBy()) != null && StringUtils.isNotEmpty(userBasicInfoMap.get(emailTemplate.getCreatedBy()).getEmail())) {
                    createdByEmail = userBasicInfoMap.get(emailTemplate.getCreatedBy()).getEmail();
                }
            }
            String lastUpdatedByEmail = null;
            if (StringUtils.isNotEmpty(emailTemplate.getLastUpdatedBy())) {
                if (userBasicInfoMap.get(emailTemplate.getLastUpdatedBy()) != null && StringUtils.isNotEmpty(userBasicInfoMap.get(emailTemplate.getLastUpdatedBy()).getEmail())) {
                    lastUpdatedByEmail = userBasicInfoMap.get(emailTemplate.getLastUpdatedBy()).getEmail();
                }
            }
            response.add(new EmailTemplatesWithFilterRes(emailTemplate, createdByEmail, lastUpdatedByEmail));
        }
        logger.info("smsTemplatesWithFilterResponse : " + response);
        return response;
    }

    public String getEmailBodyFromEmailTypeAndRole(CommunicationType emailType, Role role) throws IOException, VException {
        String fileName = getEmailBodyFileName(emailType, role);
        if(fileName == null)
            return "Email Body Not Found with given emailType and role";
        return getHTMLContent(null, fileName);
    }


    private String getEmailBodyFileName(CommunicationType type, Role role) {
        logger.info("type {}",type);
        logger.info("role {}",role);
        String propertyValue = ConfigUtils.INSTANCE.getStringValue("email." + "BODY" + "." + type.name());
        if (propertyValue != null) {
            return propertyValue;
        }

        if(role != null){
            propertyValue = ConfigUtils.INSTANCE.getStringValue("email." + "BODY" + "." + type.name() + "." + role.name());
            if (propertyValue != null) {
                return propertyValue;
            }
        }
        propertyValue = ConfigUtils.INSTANCE.getStringValue("email." + "BODY");
        return propertyValue;
    }

    private String getHTMLContent(Map<String, Object> scopes, String fileName) throws VException, IOException {

        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        InputStream is = null;
        final String confDir = "EmailTemplates";

        String propertiesFilePath = confDir + java.io.File.separator + fileName;

        try {
            is = ConfigUtils.class.getClassLoader().getResourceAsStream(propertiesFilePath);
            Mustache mustache = mf.compile(new InputStreamReader(is, "UTF-8"), "emailContent");
            mustache.execute(writer, scopes);
            writer.flush();
        } catch (Exception e) {
            logger.info("error in mustache:" + e.getMessage());
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return writer.toString();
    }
}
