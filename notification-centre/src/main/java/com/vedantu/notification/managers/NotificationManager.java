package com.vedantu.notification.managers;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import com.google.gson.JsonObject;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import com.google.gson.Gson;
import com.pubnub.api.Callback;
import com.pubnub.api.PubnubError;
import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.MobileNotificationData;
import com.vedantu.notification.entity.Notification;
import com.vedantu.notification.entity.UserDevice;
import com.vedantu.notification.enums.NotificationSubType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.pojo.ButtonData;
import com.vedantu.notification.pojo.MobileNotification;
import com.vedantu.notification.pojos.INotificationInfo;
import com.vedantu.notification.pojos.MoodleContentInfo;
import com.vedantu.notification.pojos.MoodleNotificationData;
import com.vedantu.notification.request.MobileNotificationDataRequest;
import com.vedantu.notification.requests.RegisterUserDeviceRequest;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.notification.requests.GetNotificationsReq;
import com.vedantu.notification.requests.MarkSeenByNotificationIdReq;
import com.vedantu.notification.requests.MarkSeenReq;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.MobileNotificationDataResponse;
import com.vedantu.notification.responses.BasicRes;
import com.vedantu.notification.responses.GetNotificationsRes;
import com.vedantu.notification.responses.MarkSeenRes;
import com.vedantu.notification.serializers.MobileNotificationDataDAO;
import com.vedantu.notification.serializers.NotificationDAO;
import com.vedantu.notification.serializers.UserDeviceDAO;
import com.vedantu.notification.thirdparty.AblyChannelsManager;
import com.vedantu.notification.utils.PojoUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/*
 * Manager Service for Mobile APIs
 */
@Service
public class NotificationManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(NotificationManager.class);

    @Autowired
    private UserDeviceDAO userDeviceDAO;

    @Autowired
    private MobileNotificationDataDAO mobileNotificationDataDAO;

    @Autowired
    private ChatMongoManager chatMongoManager;

    @Autowired
    private NotificationDAO notificationDAO;

    @Autowired
    private PojoUtils pojoUtils;

    @Autowired
    private AblyChannelsManager ablyChannelsManager;

    private String gcmKey;

    private Sender sender;

    public static final String TIME_ZONE_IN = "Asia/Kolkata";

    public NotificationManager() {
        super();
        gcmKey = ConfigUtils.INSTANCE.getStringValue("gcm.key");
        sender = new Sender(gcmKey);
    }

    Callback publishCallBack = new Callback() {

        public void successCallback(String channel, Object response) {
            logger.info("Published successfully");
            logger.info(response.toString());
        }

        public void errorCallback(String channel, PubnubError error) {
            logger.info(error.toString());
        }
    };

    public GetNotificationsRes getNotifications(GetNotificationsReq req) throws VException {
        List<Notification> notifications = notificationDAO.getNotifications(req.getUserId(), req.getStart(), req.getSize());
        List<com.vedantu.notification.requests.Notification> pojoList = new ArrayList<>();
        if (notifications != null) {
            for (Notification notification : notifications) {

                String info = notification.getInfo();
                if (StringUtils.isNotEmpty(info)) {
                    JsonObject jsonObject = new Gson().fromJson(info, JsonObject.class);
                    if (jsonObject.get("studentPhoneNo") != null
                            && StringUtils.isNotEmpty(jsonObject.get("studentPhoneNo").toString())) {
                        jsonObject.remove("studentPhoneNo");
                    }
                    notification.setInfo(jsonObject.toString());
                }
                pojoList.add(pojoUtils.convertToNotificationPojo(notification));
            }
        }
        GetNotificationsRes res = new GetNotificationsRes(pojoList);
        return res;
    }

    public BasicRes markSeen(MarkSeenReq req) throws VException {
        Boolean isMarked = false;
        Notification notification = notificationDAO.getNotificationByIdAndUserId(req.getUserId(), req.getNotificationId());

        if (null != notification) {
            notification.setIsSeen(true);
            notification.setSeenAt(System.currentTimeMillis());
            logger.info("marking seen notification: " + notification);
            notificationDAO.create(notification, req.getCallingUserId());
            isMarked = true;
        }
        BasicRes res = new BasicRes();
        res.setSuccess(isMarked);
        return res;
    }

    public MarkSeenRes markSeenByEntity(MarkSeenReq req) throws VException {
        if (req.getEntityId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Entity Id is required");
        }
        MarkSeenRes markSeenRes = new MarkSeenRes();
        Boolean isMarked = false;

        Notification notification = notificationDAO.getNotificationsById(req.getUserId(), req.getEntityId());
        if (null != notification) {
            notification.setIsSeen(true);
            notification.setSeenAt(System.currentTimeMillis());
            logger.info("marking seen notification: " + notification);
            notificationDAO.create(notification, req.getCallingUserId());
            isMarked = true;
            markSeenRes.setNotificationId(notification.getId());
        }
        markSeenRes.setIsMarked(isMarked);
        return markSeenRes;

    }

    public MarkSeenRes markSeenByNotificationType(MarkSeenReq req) throws VException {
        if (req.getNotificationType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Entity Id is required");
        }

        MarkSeenRes markSeenRes = new MarkSeenRes();
        Boolean isMarked = false;

        Notification notification = notificationDAO.getNotificationsByType(req.getUserId(), req.getNotificationType());
        if (null != notification) {
            notification.setIsSeen(true);
            notification.setSeenAt(System.currentTimeMillis());
            logger.info("marking seen notification: " + notification);
            notificationDAO.create(notification, req.getCallingUserId());
            isMarked = true;
            markSeenRes.setNotificationId(notification.getId());
        }
        markSeenRes.setIsMarked(isMarked);
        return markSeenRes;
    }

    public MarkSeenRes setMarkSeen(MarkSeenReq markSeenReq) throws VException {
        MarkSeenRes markSeenRes = new MarkSeenRes();
        if (markSeenReq.getEntityId() == null || markSeenReq.getEntityId() == 0) {
            if (markSeenReq.getNotificationId() != null) {
                BasicRes res = markSeen(markSeenReq);
                markSeenRes.setIsMarked(res.getSuccess());
                markSeenRes.setNotificationId(markSeenReq.getNotificationId());
                return markSeenRes;
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Notification Id is not present");
            }
        }

        markSeenRes = markSeenByEntity(markSeenReq);
        return markSeenRes;
    }

    private class HereNowCallBack extends Callback {

        private JSONObject result;
        private CountDownLatch latch;

        HereNowCallBack(CountDownLatch latch) {
            this.latch = latch;
        }

        public JSONObject getResult() {
            return result;
        }

        @Override
        public void successCallback(String channel, Object message) {
            result = (JSONObject) message;
            if (this.latch != null) {
                this.latch.countDown();
            }
            //logger.info(message);
        }

        @Override
        public void errorCallback(String channel, PubnubError error) {
            logger.error(error.getErrorString());

            if (this.latch != null) {
                this.latch.countDown();
            }
        }
    }

    /*
	 * Method to send Notifications via calling the Mobile Delivery System's API
	 * @param Mobile Notification Object containing notification data
     */
    public BaseResponse sendNotification(Notification notifRequest) {

        logger.info("Entering Notification: " + notifRequest.toString());
        BaseResponse jsonResp = new BaseResponse();
        try {

            NotificationType type = notifRequest.getType();
            Query query = new Query();
            query.addCriteria(Criteria.where("type").is(type));

            List<MobileNotificationData> mobileDataTemplates = mobileNotificationDataDAO.runQuery(query, MobileNotificationData.class);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Notification");
            jsonObject.put("userId", notifRequest.getUserId());
            String notificationData;

            if (mobileDataTemplates != null && !mobileDataTemplates.isEmpty()) {
                MobileNotificationData mobileDataTemplate = mobileDataTemplates.get(0);
                MobileNotification mobileNotification = new MobileNotification(notifRequest, mobileDataTemplate);
                jsonObject.put("pojo", new JSONObject(new Gson().toJson(mobileNotification)));
                notificationData = new Gson().toJson(mobileNotification);
                logger.info("Sending Notification Data: " + mobileNotification.toString());
            } else {
                jsonObject.put("pojo", new JSONObject(new Gson().toJson(notifRequest)));
                notificationData = new Gson().toJson(notifRequest);
                logger.info("Sending Notification Data: " + notifRequest.toString());
            }

            //Check if user is Online
            final CountDownLatch latch = new CountDownLatch(1);
            HereNowCallBack hereNowCallBack = new HereNowCallBack(latch);
            chatMongoManager.getPubnub().hereNow("vedantu-" + notifRequest.getUserId(), hereNowCallBack);

            try {
                latch.await(1, TimeUnit.SECONDS);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            JSONObject response = hereNowCallBack.getResult();

            //Remove this
            if (notifRequest.getType() != null && (notifRequest.getType().equals(NotificationType.ASK_A_DOUBT_BROADCAST) || notifRequest.getType().equals(NotificationType.ASK_A_DOUBT_ACCEPTANCE_TEACHER) || notifRequest.getType().equals(NotificationType.ASK_A_DOUBT_ACCEPTANCE_STUDENT))) {
                response = null;
            }

            if (response != null) {
                JSONArray array = response.getJSONArray("uuids");
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                int flag = 0;
                String uuid;
                for (int i = 0; i < array.length(); i++) {
                    uuid = array.getString(i);
                    if (uuid.split("_")[0].equals(notifRequest.getUserId().toString())) {
                        flag = 1;
                    }
                }
                if (flag == 1) {

                    //Send notification via pubnub chat
                    logger.info("User is Online. Sending notification via pubnub chat");
                    jsonResp.setErrorMessage("Publishing to pubnub channel..");
                    chatMongoManager.getPubnub().publish("vedantu-" + notifRequest.getUserId(), jsonObject, publishCallBack);
                } else {

                    //Send Notification via GCM
                    logger.info("User is Offline. Sending notification via gcm");

                    try {
                        constructAndSendGcm(notifRequest, notificationData);
                        jsonResp.setErrorMessage("Sent gcm notifications successfully..");

                    } catch (Exception e) {
                        jsonResp.setErrorMessage("Error Sending gcm notifications. " + e.getMessage());
                    }

                }
            } else {
                logger.info("Unable to find user presence through pubnub. Sending gcm notifications.");
                try {
                    constructAndSendGcm(notifRequest, notificationData);
                    jsonResp.setErrorMessage("Sent gcm notifications successfully..");

                } catch (Exception e) {
                    jsonResp.setErrorMessage("Error Sending gcm notifications. " + e.getMessage());
                }

            }

        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());

            logger.error("Internal Server Error {}", e.getMessage(), e);
            return jsonResp;
        }

        logger.info("Exiting");
        return jsonResp;
    }

    //Construct GCM Message
    private void constructAndSendGcm(Notification notifRequest, String notificationData) throws Exception {
        Message gcmMsg = new Message.Builder().addData("type", notifRequest.getType().toString())
                .addData("recipientId", notifRequest.getUserId().toString())
                .addData("info", notificationData).build();

        logger.info("GCMMSG: " + gcmMsg.toString());
        logger.info("userId: " + notifRequest.getUserId());
        List<String> regIds = new ArrayList<String>();
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(notifRequest.getUserId()));

        List<UserDevice> lUserDevice = userDeviceDAO.runQuery(query, UserDevice.class);
        if (lUserDevice != null || !lUserDevice.isEmpty()) {
            for (UserDevice device : lUserDevice) {
                logger.info("RegId: " + device.getRegId());

                regIds.add(device.getRegId());
            }
        }
        try {
            sendGcmNotification(gcmMsg, regIds);
        } catch (Exception e) {
            throw e;
        }
    }

    //Send GCM Message
    private void sendGcmNotification(Message gcmMsg, List<String> regIds) throws Exception {
        if (regIds.size() < 1000 && regIds.size() > 0) {

            MulticastResult result;
            try {
                logger.info(gcmMsg.toString() + regIds.toString());
                result = sender.send(gcmMsg, regIds, 2);
                logger.info("Total gcm notifications sent:" + result.getTotal());
                logger.info("Success gcm notifications sent:" + result.getSuccess());
            } catch (Exception e) {
//                logger.error("Error Sending gcm Notificatons", e);
//                throw e;
            }
        }
    }

    /*
	 * Method to save Mobile Notification Data
	 * @param Mobile Notification Data Object containing notification data
     */
    public BaseResponse saveMobileNotificationData(MobileNotificationDataRequest mobileNotifData) {

        logger.info("Entering MobileNotificationData:" + mobileNotifData.toString());
        BaseResponse jsonResp = new BaseResponse();
        try {
            NotificationType type = mobileNotifData.getType();
            Query query = new Query();
            query.addCriteria(Criteria.where("type").is(type));

            List<MobileNotificationData> mobileDataTemplates = mobileNotificationDataDAO.runQuery(query, MobileNotificationData.class);
            MobileNotificationData mobileNotificationData = new MobileNotificationData(mobileNotifData);
            if (mobileDataTemplates == null || mobileDataTemplates.isEmpty()) {

                mobileNotificationDataDAO.create(mobileNotificationData);
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("Added Successfully");
            } else {
                MobileNotificationData prevMobileData = mobileDataTemplates.get(0);
                logger.info("Found entry:" + prevMobileData.toString());
                Update update = createUpdateObject(mobileNotificationData);
                query = new Query();
                query.addCriteria(Criteria.where("_id").is(prevMobileData.getId()));
                mobileNotificationDataDAO.update(query, update);
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("MobileData already present. Other paramters have been updated.");

            }

        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());

        }
        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    /*
	 * Method to get Mobile Notification Data
	 * @param Mobile Notification Data Object containing notification data
     */
    public BaseResponse getMobileNotificationData(MobileNotificationData mobileNotifData) {

        logger.info("Entering MobileNotificationData: " + mobileNotifData.toString());

        BaseResponse jsonResp = new BaseResponse();
        try {
            NotificationType type = mobileNotifData.getType();
            Query query = new Query();
            query.addCriteria(Criteria.where("type").is(type));

            List<MobileNotificationData> mobileDataTemplates = mobileNotificationDataDAO.runQuery(query, MobileNotificationData.class);

            if (mobileDataTemplates == null || mobileDataTemplates.isEmpty()) {
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("No entry found in the db");
            } else {
                MobileNotificationData prevMobileData = mobileDataTemplates.get(0);
                logger.info("Found entry:" + prevMobileData.toString());
                jsonResp = new MobileNotificationDataResponse(prevMobileData);
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("");
            }

        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
        }
        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    ///// HELPER FUNCTIONS
//	private Boolean sendToNodeServer(String nodeServerUrl, String notification) {
//
//		logger.info("Entering nodeServerUrl:"+nodeServerUrl + " notification:"+notification);
//
//		try {
//			Client client = Client.create();
//
//			WebResource webResource = client.resource(nodeServerUrl);
//
//			ClientResponse response = webResource.type("application/json").post(ClientResponse.class,
//					notification);
//
//			if (response.getStatus() == 200) {
//				logger.info("Successfully sent to node server");
//				return true;
//			}
//		} catch (Throwable t) {
//			logger.error("Error occured while sending to Node server", t);
//		}
//		return false;
//	}
    public BaseResponse registerUserDevice(RegisterUserDeviceRequest userDeviceReq) {

        UserDevice userDevice = new UserDevice(userDeviceReq.getRegId(), userDeviceReq.getDeviceId(), userDeviceReq.getUserId());
        BaseResponse jsonResp = new BaseResponse();
        try {
            Query query = new Query();

            query.addCriteria(Criteria.where("userId").is(userDevice.getUserId()).andOperator(Criteria.where("deviceId").is(userDevice.getDeviceId())));

            List<UserDevice> lUserDevice = userDeviceDAO.runQuery(query, UserDevice.class);

            UserDevice prevRegistration = null;
            if (lUserDevice == null || lUserDevice.isEmpty()) {
                userDeviceDAO.create(userDevice);
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("Added Successfully");
                return jsonResp;
            } else {
                prevRegistration = lUserDevice.get(0);
                userDevice.setId(prevRegistration.getId());
//				logger.info(prevRegistration.getRegId());
//				Update update = createUpdateObject(userDevice);
//				query = new Query();
//				query.addCriteria(Criteria.where("_id").is(prevRegistration.getId()));
                userDeviceDAO.create(userDevice);
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("DeviceId already present. Other paramters have been updated.");
                return jsonResp;
            }

        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
            return jsonResp;
        }
    }

    public BaseResponse removeUserDevice(RegisterUserDeviceRequest userDeviceReq) {

        UserDevice userDevice = new UserDevice(userDeviceReq.getRegId(), userDeviceReq.getDeviceId(), userDeviceReq.getUserId());
        BaseResponse jsonResp = new BaseResponse();
        try {
            Query query = new Query();
            query.addCriteria(Criteria.where("regId").is(userDevice.getRegId()));
            query.addCriteria(Criteria.where("deviceId").is(userDevice.getDeviceId()));
            query.addCriteria(Criteria.where("userId").is(userDevice.getUserId()));
            List<UserDevice> lUserDevice = userDeviceDAO.runQuery(query, UserDevice.class);
            UserDevice prevRegistration = null;

            if (lUserDevice == null || lUserDevice.isEmpty()) {
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("No such entry found.");
                return jsonResp;
            } else {
                prevRegistration = lUserDevice.get(0);
                userDeviceDAO.deleteById(prevRegistration.getId());
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage("Removed Successfully");
                return jsonResp;
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
            return jsonResp;
        }
    }

    private Update createUpdateObject(MobileNotificationData prevMobileData) {
        if (prevMobileData == null) {
            return null;
        }
        Update update = new Update();

        String title = prevMobileData.getTitle();
        String description = prevMobileData.getDescription();
        String imageUrl = prevMobileData.getImageUrl();
        String landingActivity = prevMobileData.getLandingActivity();
        Long timestamp = prevMobileData.getTimestamp();
        String extra = prevMobileData.getExtra();
        List<ButtonData> buttons = prevMobileData.getButtons();
        NotificationType type = prevMobileData.getType();

        if (title != null) {
            update.set("title", title);
        }
        if (description != null) {
            update.set("description", description);
        }
        if (imageUrl != null) {
            update.set("imageUrl", imageUrl);
        }
        if (landingActivity != null) {
            update.set("landingActivity", landingActivity);
        }
        if (timestamp != null) {
            update.set("timestamp", timestamp);
        }
        if (extra != null) {
            update.set("extra", extra);
        }
        if (buttons != null && !buttons.isEmpty()) {
            update.set("buttons", buttons);
        }
        if (type != null) {
            update.set("type", type);
        }
        return update;
    }

    public BaseResponse createNotification(CreateNotificationRequest request) {
        Long userId = request.getUserId();
        NotificationType notificationType = request.getNotificationType();
        String notificationObj = request.getNotificationObj();
        Role role = request.getRole();
        String notificationInfo = notificationObj;
        Notification notification = null;
        //INotificationDAO notificationDAO = DAOFactory.INSTANCE.getNotificationDAO();
        //User user = DAOFactory.INSTANCE.getUserDAO().getUserById(userId, mgr);
        NotificationType type = notificationType;
        String header = "";
        String body = "";
        String footer = "";
        String description = "";
        if (notificationObj != null) {
            description = getNotificationDescription(notificationObj, notificationType, role);
        }
        switch (notificationType) {
            case FREEBIES:
                header = ConfigUtils.INSTANCE.getStringValue("notification.FREEBIES.header");
                body = ConfigUtils.INSTANCE.getStringValue("notification.FREEBIES.body");
                footer = ConfigUtils.INSTANCE.getStringValue("notification.FREEBIES.footer");
                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case REFUND:
                header = ConfigUtils.INSTANCE.getStringValue("notification.REFUND.header");
                body = ConfigUtils.INSTANCE.getStringValue("notification.REFUND.body");
                footer = ConfigUtils.INSTANCE.getStringValue("notification.REFUND.footer");
                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case BLOCKED:
                header = "User is Blocked";
                body = "User is Blocked";
                footer = "User is Blocked";
                notification = new Notification(userId, type, header, body, footer, description, null, true);
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case OTF_SESSION:
                header = ConfigUtils.INSTANCE.getStringValue("notification.OTF_SESSION.header");
                body = ConfigUtils.INSTANCE.getStringValue("notification.OTF_SESSION.body");
                footer = ConfigUtils.INSTANCE.getStringValue("notification.OTF_SESSION.footer");
                try {
                    Map<String, Object> scopes = new HashMap<>();
                    //String json = new Gson().toJson(notificationObj, notificationObj.getClass());
                    JSONObject jsonObject = new JSONObject(notificationObj);
                    scopes.put("title", jsonObject.getString("title"));

                    header = compileMustache(header, scopes);
                    footer = compileMustache(footer, scopes);
                } catch (Exception ex) {
                    logger.error("NotificationError - Error occured while parsing session title : " + ex.getMessage(), ex);
                }
                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notification.setEntityType("OTFSession");
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case SUBSCRIPTION_PURCHASED:
            case SUBSCRIPTION_ENDED:
            case SUBSCRIPTION_ENDED_BY_TEACHER:
            case TRIAL_SESSION_SCHEDULE:
            case TRIAL_SESSION_SCHEDULE_BY_SC:
            case TRIAL_SESSION_CANCEL_BY_STUDENT:
            case TRIAL_SESSION_CANCEL_BY_TEACHER:
            case TRIAL_SESSION_CANCEL_BY_SC:
            case SUBSCRIPTION_SESSION_SCHEDULE:
            case SUBSCRIPTION_SESSION_SCHEDULE_BY_SC:
            case SUBSCRIPTION_SESSION_CANCEL_BY_STUDENT:
            case SUBSCRIPTION_SESSION_CANCEL_BY_TEACHER:
            case SUBSCRIPTION_SESSION_CANCEL_BY_SC:
            case SUBSCRIPTION_SESSION_REQUEST_BY_TEACHER:
            case SUBSCRIPTION_SESSION_REQUEST_CANCEL_BY_TEACHER:
            case SUBSCRIPTION_SESSION_REQUEST_ACCEPTED:
            case SUBSCRIPTION_SESSION_SCHEDULE_SESSIONS3CHALLENGE:
            case SUBSCRIPTION_SESSION_REQUEST_REJECTED:
                header = getPropertyValue(type, role, "header");
                body = getPropertyValue(type, role, "body");
                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notification.setEntityType(type.name());
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case SUBSCRIPTION_REQUEST_CREATED:
            case SUBSCRIPTION_REQUEST_CANCELLED:
            case SUBSCRIPTION_REQUEST_REJECTED:
            case SUBSCRIPTION_REQUEST_EXPIRED:
            case SUBSCRIPTION_REQUEST_ACCEPTED:
            case SUBSCRIPTION_REQUEST_EXPIRY_REMINDER:
                header = getPropertyValue(type, role, "header");
                body = getPropertyValue(type, role, "body");
                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notification.setEntityType(type.name());
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case LEAD_REQUEST_CALL_BACK:
                header = getPropertyValue(type, role, "header");
                body = getPropertyValue(type, role, "body");
                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notification.setEntityType(type.name());
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case ASK_A_DOUBT_BROADCAST:
            case ASK_A_DOUBT_ACCEPTANCE_STUDENT:
            case ASK_A_DOUBT_ACCEPTANCE_TEACHER:
                header = getPropertyValue(type, role, "header");
                body = getPropertyValue(type, role, "body");

                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notification.setEntityType(type.name());
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case LMS_NOTIFICATION:
                type = notificationType;
                MoodleNotificationData notificationData = new Gson().fromJson(notificationObj, MoodleNotificationData.class);
                MoodleContentInfo contentInfo = notificationData.getContentInfo();
                contentInfo.setAdditionalFields();
                notificationInfo = new Gson().toJson(contentInfo);
                String notificationSubType = notificationData.getState();
                switch (notificationSubType) {
                    case "LMS_TEST_SHARED":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_SHARED.header");
                        if (contentInfo.getExpiryTime() != null && contentInfo.getExpiryTime() > 0) {
                            body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_SHARED.body");
                        } else {
                            body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_SHARED_WITHOUT_DEADLINE.body");
                        }
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_SHARED.footer");
                        break;
                    case "LMS_NOTES_SHARED":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_NOTES_SHARED.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_NOTES_SHARED.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_NOTES_SHARED.footer");
                        break;
                    case "LMS_ASSIGNMENT_SHARED":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_SHARED.header");
                        if (contentInfo.getExpiryTime() != null && contentInfo.getExpiryTime() > 0) {
                            body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_SHARED.body");
                        } else {
                            body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_SHARED_WITHOUT_DEADLINE.body");
                        }
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_SHARED.footer");
                        break;
                    case "LMS_TEST_REMINDER":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_REMINDER.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_REMINDER.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_REMINDER.footer");
                        break;
                    case "LMS_ASSIGNMENT_REMINDER":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_REMINDER.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_REMINDER.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_REMINDER.footer");
                        break;
                    case "LMS_TEST_REPORT":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_REPORT.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_REPORT.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_REPORT.footer");
                        break;
                    case "LMS_ASSIGNMENT_REPORT":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_REPORT.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_REPORT.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_REPORT.footer");
                        break;
                    case "LMS_TEST_ATTEMPTED":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_ATTEMPTED.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_ATTEMPTED.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_ATTEMPTED.footer");
                        break;
                    case "LMS_ASSIGNMENT_ATTEMPTED":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_ATTEMPTED.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_ATTEMPTED.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_ATTEMPTED.footer");
                        break;
                    case "LMS_TEST_EVAULATION_REMINDER":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_EVAULATION_REMINDER.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_EVAULATION_REMINDER.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_TEST_EVAULATION_REMINDER.footer");
                        break;
                    case "LMS_ASSIGNMENT_EVAULATION_REMINDER":
                        header = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_EVAULATION_REMINDER.header");
                        body = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_EVAULATION_REMINDER.body");
                        footer = ConfigUtils.INSTANCE.getStringValue("notification.LMS_ASSIGNMENT_EVAULATION_REMINDER.footer");
                        break;
                    default:
                        return new BaseResponse();
                }

                notification = new Notification(userId, type, header, body, footer, description, notificationInfo,
                        false);
                notification.setEntityType("ContentInfo");
                description = getLMSNotificationDescription(body, contentInfo);
                logger.info("Found LMS Description:" + description);
                notification.setDescription(description);
                try {
                    notification.setSubType(NotificationSubType.valueOf(notificationSubType));
                } catch (Exception ex) {
                    logger.error("Notification : " + ex.getMessage(), ex);
                }
                notificationDAO.create(notification, request.getCallingUserId());
                break;
            case INSTALMENT_PAYMENT_DUE_7DAYS_LEFT:
            case INSTALMENT_PAYMENT_DUE_1DAY_LEFT:
            case INSTALMENT_PAYMENT_DUE_OTF_BLOCKED://TODO later
            case INSTALMENT_PAYMENT_DUE_1DAY_PASSED:
            case INSTALMENT_PAYMENT_DUE_SUBSCRIPTION_ENDED:
            case INSTALMENT_PAID:
                Map<String, Object> scopes = new HashMap<>();
                try {
                    //scopes can be empty as not required
                    header = compileMustache(getPropertyValue(type, Role.STUDENT, "header"), scopes);
                    body = getNotificationDescription(notificationObj, type, Role.STUDENT);
                    notification = new Notification(userId, type, header, body, footer, body, notificationInfo,
                            false);
                    notification.setEntityType(type.name());
                    notificationDAO.create(notification, request.getCallingUserId());
                } catch (Exception e) {
                    logger.info(e.toString());
                }
                break;
            default:
                if (notificationInfo != null) {
                    notification = new Notification(userId, notificationType, "", "", "", description,
                            notificationInfo, false);
                    notification.setEntityType(notificationType.toString());
                    //notification.setEntityId(id);
                    notification.setIsSeen(false);
//				Notification notificationById = notificationDAO.getNotificationsByid(userId, id);
//				logger.info("================>>>>>>>>>>>>>>> FOUND NOTI IN DB >>>>>>>>> " + notificationById
//						+ " user ID == " + userId + " entity Id = " + id);
//				if (notificationById != null) {
//					notification.setId(notificationById.getId());
//				}
                    notificationDAO.create(notification, request.getCallingUserId());
                } else {
                    logger.error("create Notification", new IllegalArgumentException("No information found"));
                }
                break;
        }

        /*
		String nodeServerUrl = ConfigUtils.INSTANCE.getStringValue("nodeServerUrl.host");
		String nodeServerPort = ConfigUtils.INSTANCE.getStringValue("nodeServerUrl.port");
		if (!StringUtils.isEmpty(nodeServerPort)) {
			nodeServerUrl = nodeServerUrl + ":" + nodeServerPort + "/pushNotification";
		} else {
			nodeServerUrl = nodeServerUrl + "/pushNotification";
		}

		logger.info("Sending request to :" + nodeServerUrl + ". Notification Data: " + notification);

         */
        //sendToNodeServer(nodeServerUrl, notification);
        return sendNotification(notification);
    }

    private String getPropertyValue(NotificationType type, Role role, String property) {
        String notifString = "notification." + type.name() + "." + property + "." + role.name();
        String propertyValue = ConfigUtils.INSTANCE.getStringValue(notifString);
        logger.info("getpropertyValue", propertyValue);
        if (propertyValue == null) {
            return "";
        }
        return propertyValue;
    }

    /*
	private Boolean sendToNodeServer(String nodeServerUrl, Notification notification) {

		boolean isPlatformNotificationEnabled = ConfigUtils.INSTANCE.getBooleanValue("platform.notification.enabled");

		if (isPlatformNotificationEnabled) {
			LOG.info("Sending Notification via Platform" + new Gson().toJson(notification));

			String sendNotificationUrl = ConfigUtils.INSTANCE.getStringValue("platform.fetch.url.api.notification")
					+ configUtils
							.getStringValue("platform.fetch.url.api.notification.notification.sendNotification");
			PlatformNotificationBasicResponse result = NotificationPlatformTools.INSTANCE
					.postPlatformData(sendNotificationUrl, new Gson().toJson(notification), true, HTTPMethod.POST);
			LOG.info("sendToNodeServer result : " + result);
			return true;
		} else {

			try {
				Client client = Client.create();

				WebResource webResource = client.resource(nodeServerUrl);

				ClientResponse response = webResource.type("application/json").post(ClientResponse.class,
						new Gson().toJson(notification));

				if (response.getStatus() == 200) {
					LOG.info("createNotification" + new RuntimeException(response.getStatusInfo().toString()));
					return true;
				}
			} catch (Throwable t) {
				LOG.throwing(getLogTag(), "sendToNodeServer", t);
			}
			return false;
		}
	}
     */
    private String getNotificationDescription(String notificationObj, NotificationType type, Role role) {
        logger.info("getNotificationDescription");

        if (StringUtils.isEmpty(notificationObj)) {
            return null;
        }

        HashMap<String, Object> scopes = new HashMap<String, Object>();

        //String json = new Gson().toJson(notificationObj, notificationObj.getClass());
        String response = "";
        try {
            JSONObject jsonObject = new JSONObject(notificationObj);
            for (int i = 0; i < jsonObject.names().length(); i++) {

                if (jsonObject.names().getString(i) != null && jsonObject.names().getString(i).equals("startDate")) {
                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
                        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
                        Object startDateObject = jsonObject.get(jsonObject.names().getString(i));
                        Long startDate = 0l;
                        if (startDateObject != null) {
                            startDate = Long.parseLong(startDateObject.toString());
                            String dateTime = sdf.format(new Date(startDate));
                            scopes.put(jsonObject.names().getString(i), dateTime);
                        }
                    } catch (Exception e) {
                        logger.error("NotificationManager - getNotificationDescription", e);
                    }
                } else {
                    scopes.put(jsonObject.names().getString(i), jsonObject.get(jsonObject.names().getString(i)));
                }
            }
            response = compileMustache(getPropertyValue(type, role, "body"), scopes);
            logger.info("getNotificationDescription");
        } catch (Exception e) {
            logger.error("NotificationManager - getNotificationDescription", e);
        }

        return response;
    }

    public String compileMustache(String body, Map<String, Object> scopes) throws IOException {
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        // new StringReader()
        Mustache mustache = mf.compile(new StringReader(body), "getNotificationDescription");
        mustache.execute(writer, scopes);
        writer.flush();
        return writer.toString();
    }

    private String getLMSNotificationDescription(String body, INotificationInfo notificationObj) {
        logger.info("getLMSNotificationDescription");
        HashMap<String, Object> scopes = new HashMap<String, Object>();
        Writer writer = new StringWriter();

        String json = new Gson().toJson(notificationObj, notificationObj.getClass());
        try {
            JSONObject jsonObject = new JSONObject(json);
            for (int i = 0; i < jsonObject.names().length(); i++) {
                scopes.put(jsonObject.names().getString(i), jsonObject.get(jsonObject.names().getString(i)));
            }
            MustacheFactory mf = new DefaultMustacheFactory();
            Mustache mustache = mf.compile(new StringReader(body), "getLMSNotificationDescription");
            mustache.execute(writer, scopes);
            writer.flush();
            logger.info("getNotificationDescription");
        } catch (Exception e) {
            logger.error("NotificationManager - getLMSNotificationDescription", e);
        }
        return writer.toString();
    }

    public MarkSeenRes markSeenByNotificationIdAndDeviceId(MarkSeenByNotificationIdReq req) throws VException {

        MarkSeenRes markSeenRes = new MarkSeenRes();
        Boolean isMarked = false;

        Notification notification = notificationDAO.getById(req.getNotificationId());
        if (null != notification) {
            Notification childNotification = new Notification(notification.getUserId(), notification.getType(),
                    notification.getHeader(), notification.getBody(), notification.getFooter(),
                    notification.getDescription(), notification.getInfo(), false);
            childNotification.setEntityId(notification.getEntityId());
            childNotification.setEntityType(notification.getEntityType());
            childNotification.setSendTo(notification.getSendTo());
            childNotification.setIsSeen(true);
            childNotification.setSeenAt(System.currentTimeMillis());
            childNotification.setRegToken(req.getRegToken());
            childNotification.setDeviceId(req.getDeviceId());
            logger.info("marking seen notification: " + notification);
            notificationDAO.create(notification, req.getCallingUserId());
            isMarked = true;
            markSeenRes.setNotificationId(notification.getId());
        }
        markSeenRes.setIsMarked(isMarked);
        return markSeenRes;
    }

}
