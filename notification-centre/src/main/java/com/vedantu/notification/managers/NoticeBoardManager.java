package com.vedantu.notification.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.notification.entity.NoticeBoard;
import com.vedantu.notification.request.ChangeManualNotificationRequest;
import com.vedantu.notification.request.NotificationFilterReq;
import com.vedantu.notification.responses.NoticeBoardRes;
import com.vedantu.notification.serializers.NoticeBoardDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.request.CommunicationDataReq;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class NoticeBoardManager {

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(NoticeBoardManager.class);

    @Autowired
    private NoticeBoardDAO noticeBoardDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public List<NoticeBoardRes> populateNoticeBoardData(CommunicationDataReq req){
        List<NoticeBoardRes> noticeBoards=new ArrayList<>();
        List<String> allUserIds=req.getAllUsers();
        String body=req.getBody();
        Long expiryTime=req.getExpiryTime();
        Long scheduleTime=req.getScheduledTime();
        String manualNotificationId=req.getManualNotificationId();
        allUserIds.forEach(userId->{
            NoticeBoard noticeBoard=new NoticeBoard(userId,body,scheduleTime,expiryTime,manualNotificationId);
            noticeBoard.setCreatedBy(Long.toString(req.getCallingUserId()));
            noticeBoardDAO.create(noticeBoard);
            noticeBoards.add(mapper.map(noticeBoard,NoticeBoardRes.class));
        });
        return noticeBoards;
    }

    public List<NoticeBoardRes> getAllNotices(NotificationFilterReq filterReq){
        List<NoticeBoard> noticeBoardContents=noticeBoardDAO.getAllNotices(filterReq);
        List<NoticeBoardRes> noticeBoardRes=new ArrayList<>();
        noticeBoardContents.forEach(noticeBoardContent-> noticeBoardRes.add(mapper.map(noticeBoardContent,NoticeBoardRes.class)));
        return noticeBoardRes;
    }

    public void updateNoticeBoardDB(){
        logger.info("Inside update notice board db");
        noticeBoardDAO.updateNoticeBoardDB();
    }

    public NoticeBoardRes getNoticeById(String id) throws BadRequestException {
        NoticeBoard notice=noticeBoardDAO.getNoticeById(id);
        return mapper.map(notice,NoticeBoardRes.class);
    }

    public List<NoticeBoard> discardNotices(ChangeManualNotificationRequest discardList) {
        return noticeBoardDAO.discardNotices(discardList);
    }

    public List<NoticeBoard> rescheduleNotices(ChangeManualNotificationRequest rescheduleList) throws BadRequestException {
        return noticeBoardDAO.rescheduleNotices(rescheduleList);
    }

    public List<NoticeBoardRes> getAllNoticeBoardData(NotificationFilterReq filterReq) {
        List<NoticeBoardRes> noticeBoardRes=new ArrayList<>();
        noticeBoardDAO.getAllNoticeBoardData(filterReq);
        return noticeBoardRes;
    }

    public List<NoticeBoardRes> getNoticeUserId(String userId) {
        logger.info("\nInside manager of getting notice by user id ");
        List<NoticeBoard> notices=noticeBoardDAO.getNoticeUserId(userId);
        List<NoticeBoardRes> userNotices=new ArrayList<>();
        if(!ArrayUtils.isEmpty(notices)){
            logger.info("\nNumber of notices retrieved are "+notices.size());
            notices.forEach(notice->userNotices.add(mapper.map(notice,NoticeBoardRes.class)));
            return userNotices;
        }
        return null;
    }
}
