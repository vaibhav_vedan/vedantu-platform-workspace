/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.managers;

import com.amazonaws.services.appstream.model.MessageAction;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.notification.dao.TeacherDayStatsDAO;
import com.vedantu.notification.entity.Channel;
import com.vedantu.notification.entity.ChannelUserInfo;
import com.vedantu.notification.entity.ChatMessage;
import com.vedantu.notification.entity.MessageUser;
import com.vedantu.notification.pojo.MessageUserInfo_v2;
import com.vedantu.notification.pojos.MessageUserLeader;
import com.vedantu.notification.pojos.MessageUserRes;
import com.vedantu.notification.request.GetMessageUsersReq;
import com.vedantu.notification.request.TeacherWishesReq;
import com.vedantu.notification.requests.MessageUserReplyReq;
import com.vedantu.notification.response.GetMessageUsersRes;
import com.vedantu.notification.response.MessageUserServletRes;
import com.vedantu.notification.response.TeacherWishesRes;
import com.vedantu.notification.responses.GetMessageLeadersRes;
import com.vedantu.notification.serializers.ChannelDAO;
import com.vedantu.notification.serializers.ChannelUserInfoDAO;
import com.vedantu.notification.serializers.ChatMessageDAO;
import com.vedantu.notification.serializers.MessageUserDAO;
import com.vedantu.notification.thirdparty.AblyChannelsManager;
import com.vedantu.util.*;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import io.ably.lib.types.Message;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.dozer.DozerBeanMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.match;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.skip;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.limit;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.group;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.sort;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;

/**
 *
 * @author jeet
 */
@Service
public class MessageUserManager {

    @Autowired
    MessageUserDAO messageUserDAO;


    @Autowired
    ChatMessageDAO chatMessageDAO;

    @Autowired
    ChannelDAO channelDAO;

    @Autowired
    TeacherDayStatsDAO teacherDayStatsDAO;

    @Autowired
    private FosUtils fosUtils;

    private static final String TEACHERS_DAY_WISHES_2020 = "TEACHERS_DAY_WISHES_2020";
    private static final Long AFTER_TIME_MILLIS = 1599263465332L;

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(MessageUserManager.class);
    @Autowired
    private DozerBeanMapper mapper;
    private static final Long TIME_BETWEEN_SENDING_MESSAGE_EMAILS = ConfigUtils.INSTANCE.getLongValue("message.timebetween.emails.mins") * DateTimeUtils.MILLIS_PER_MINUTE;

    public MessageUserRes addMessageUser(MessageUser message) throws VException, JSONException {
        MessageUser lastMessageSent = messageUserDAO.getLastMessageSMSSent(message.getUserId(), message.getToUserId());
        logger.info(lastMessageSent);
        if (lastMessageSent == null || (TIME_BETWEEN_SENDING_MESSAGE_EMAILS != null
                && (System.currentTimeMillis() - lastMessageSent.getCreationTime()) > TIME_BETWEEN_SENDING_MESSAGE_EMAILS)) {
            message.setSmsSent(true);
        }
        ChatMessage chatMessage = new ChatMessage();

        messageUserDAO.upsert(message);
        MessageUserRes r = mapper.map(message, MessageUserRes.class);
        r.setSendSMS(message.isSmsSent());

        chatMessage.setSenderId(message.getUserId());
        chatMessage.setTargetUserId(message.getToUserId());

          Long[] parties = {message.getUserId(), message.getToUserId()};
          Channel channel = channelDAO.findByParticipants(parties);

        if(channel != null){
            JSONObject msgPojo = new JSONObject();
            msgPojo.put("type", "MESSAGE");
            msgPojo.put("data", message.getMessage());
            chatMessage.setData(msgPojo.toString());
            chatMessage.setMessageId(r.getId().toString());
            chatMessage.setChannel(channel.getId());
            chatMessage.setSenderTime(message.getCreationTime());
            chatMessage.setServerTime(System.currentTimeMillis());
            logger.info("checking something: " + channel.getId());
            chatMessageDAO.save(chatMessage);
        }
        return r;
    }


    public MessageUser getMessageById(Long messageId) throws VException {
        MessageUser resp = messageUserDAO.getById(messageId);
        if (resp == null) {
            throw new NotFoundException(ErrorCode.MESSAGE_NOT_FOUND, "No message found with id:" + messageId);
        }
        return resp;
    }

    public PlatformBasicResponse messageUserReply(MessageUserReplyReq req) throws VException {
        MessageUser msg = getMessageById(req.getMessageId());
        msg.getReplies().add(req.getMessage());
        messageUserDAO.upsert(msg);
        return new PlatformBasicResponse();
    }

    public List<MessageUser> getMessageUsers(GetMessageUsersReq req) throws VException {
        List<MessageUser> resp
                = messageUserDAO.getMessages(req.getAfterTimeMillis(),
                        req.getUserId(), req.getToUserId(), req.getContextType(), req.getStart(), req.getSize());
        return resp;
    }

    public GetMessageLeadersRes getMessageLeaders(GetMessageUsersReq req) throws VException {
        req.verify();
        if (req.getSize() == null) {
            req.setSize(20);
        }
        if (req.getStart() == null) {
            req.setStart(0);
        }
        logger.info("Aggregation getMessageLeaders request" + req);
        Criteria criteria = Criteria.where("contextType").is(req.getContextType());
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation leaders = newAggregation(match(criteria),
                group("toUserId").count().as("wishesCount").first("toUserId").as("teacherId"), sort(Sort.Direction.DESC, "wishesCount"),
                skip(req.getStart()),
                limit(req.getSize())).withOptions(aggregationOptions);
        AggregationResults<MessageUserLeader> groupResults = messageUserDAO.getMongoOperations().aggregate(leaders,
                MessageUserDAO.COLLECTION_NAME, MessageUserLeader.class);

        logger.info("Aggregation query result" + groupResults);
        GetMessageLeadersRes resp = new GetMessageLeadersRes();
        resp.setList(groupResults.getMappedResults());
        return resp;
    }

    public GetMessageUsersRes getMessageUsers_v2(GetMessageUsersReq request) throws VException, IOException {
        List<MessageUser> messageUsers = getMessageUsers(request);

        ObjectMapper objectMapper = new ObjectMapper().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        String jsonString = objectMapper.writeValueAsString(messageUsers);

        Type listType = new TypeToken<ArrayList<MessageUserRes>>(){}.getType();
        List<MessageUserRes> response = new Gson().fromJson(jsonString, listType);
        return _putUserInfoForMessages(response);
    }

    private GetMessageUsersRes _putUserInfoForMessages(List<MessageUserRes> messageUsers) throws VException {
        List<MessageUserInfo_v2> messageUserInfos = new ArrayList<>();
        if (messageUsers != null && !messageUsers.isEmpty()) {
            List<String> userIdList = new ArrayList<>();
            for (MessageUserRes messageUser : messageUsers) {
                if (messageUser != null) {
                    // messageUserInfos.add(new MessageUserInfo(messageUser));
                    String userId = messageUser.getUserId().toString();
                    String toUserId = messageUser.getToUserId().toString();
                    if (!userIdList.contains(userId)) {
                        userIdList.add(userId);
                    }
                    if (!userIdList.contains(toUserId)) {
                        userIdList.add(toUserId);
                    }
                }
            }
            List<UserBasicInfo> usersList = fosUtils.getUserBasicInfosSet(new HashSet(userIdList), false);
            Map<Long, UserBasicInfo> usersMap = new HashMap<>();
            for (UserBasicInfo messageUser : usersList) {
                if (messageUser != null) {
                    usersMap.put(messageUser.getUserId(), messageUser);
                }
            }
            for (MessageUserRes message : messageUsers) {
                UserBasicInfo fromUser = null;
                UserBasicInfo toUser = null;
                if (usersMap.containsKey(message.getUserId())) {
                    fromUser = usersMap.get(message.getUserId());
                }
                if (usersMap.containsKey(message.getToUserId())) {
                    toUser = usersMap.get(message.getToUserId());
                }
                messageUserInfos.add(new MessageUserInfo_v2(message, fromUser, toUser));
            }
        }

        GetMessageUsersRes res = new GetMessageUsersRes();
        res.setList(messageUserInfos);
        return res;
    }

    public MessageUserServletRes getMessageById_v2(Long messageId) throws VException {
        if (messageId == null || messageId <= 0) {
            throw new NotFoundException(ErrorCode.INVALID_MESSAGE_ID, "Invalid message id");
        }
        MessageUser messageUser = getMessageById(messageId);
        if (messageUser == null) {
            throw new NotFoundException(ErrorCode.MESSAGE_NOT_FOUND, "no message found with id[" + messageId + "]");
        }
        Set<Long> userIds = new HashSet<>();
        userIds.add(messageUser.getUserId());
        userIds.add(messageUser.getToUserId());
        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, false);
        UserBasicInfo fromUser = userMap.get(messageUser.getUserId());
        UserBasicInfo toUser = userMap.get(messageUser.getToUserId());
        MessageUserServletRes messageUserRes = new MessageUserServletRes(fromUser, toUser, messageUser.getMessage());
        return messageUserRes;
    }

    public GetMessageLeadersRes getMessageLeaders_v2(GetMessageUsersReq request) throws VException {
        GetMessageLeadersRes res = getMessageLeaders(request);
        return _putLeaderInfoForMessages(res);
    }

    private GetMessageLeadersRes _putLeaderInfoForMessages(GetMessageLeadersRes messageLeaders) throws VException {
        if (messageLeaders != null && !(messageLeaders.getCount() == 0)) {
            List<Long> userIdList = new ArrayList<Long>();
            for (MessageUserLeader messageUser : messageLeaders.getList()) {
                if (messageUser != null) {
                    // messageUserInfos.add(new MessageUserInfo(messageUser));
                    Long userId = messageUser.getTeacherId();
                    if (!userIdList.contains(userId)) {
                        userIdList.add(userId);
                    }
                }
            }
            Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIdList, false);
            for (MessageUserLeader messageUser : messageLeaders.getList()) {
                if (messageUser != null) {
                    // messageUserInfos.add(new MessageUserInfo(messageUser));
                    Long userId = messageUser.getTeacherId();
                    if (userMap.containsKey(userId)) {
                        messageUser.setTeacher(userMap.get(userId));
                    }
                }
            }
        }
        return messageLeaders;
    }

    public List<TeacherWishesRes> getPostedWishesForTeachers(Long teacherId, int start) {
        List<TeacherWishesRes> res = new ArrayList<>();
        logger.info("teacher id {}, start {}", teacherId, start);
        if (Objects.isNull(teacherId)) {
            return res;
        }

        List<MessageUser> fetchedTeacherMessages = messageUserDAO.getMessages(AFTER_TIME_MILLIS, null, teacherId,
                TEACHERS_DAY_WISHES_2020, start, 20);
        logger.info("fetchedTeacherMessages {}", fetchedTeacherMessages);

        Set<String> userIds = Optional.ofNullable(fetchedTeacherMessages).orElseGet(ArrayList::new).stream()
                .map(MessageUser::getUserId)
                .filter(Objects::nonNull)
                .map(String::valueOf)
                .collect(Collectors.toCollection(HashSet::new));
        logger.info("user ids {}", userIds);
        List<UserBasicInfo> usersList = fosUtils.getUserBasicInfosSet(userIds, false);
        Map<Long, String> userNameMap = Optional.ofNullable(usersList).orElseGet(ArrayList::new).stream()
                .filter(u -> Objects.nonNull(u) && Objects.nonNull(u.getFullName()))
                .collect(Collectors.toMap(UserBasicInfo::getUserId, UserBasicInfo::getFullName));
        res = Optional.ofNullable(fetchedTeacherMessages).orElseGet(ArrayList::new).stream()
                .filter(m -> userNameMap.containsKey(m.getUserId()))
                .map(m -> new TeacherWishesRes(userNameMap.get(m.getUserId()), m.getMessage()))
                .collect(Collectors.toList());

        return res;
    }

    public TeacherWishesRes getPostedWishesForTeacherStudent(Long teacherId, Long studentId) {
        TeacherWishesRes res = new TeacherWishesRes();
        if (Objects.isNull(teacherId) || Objects.isNull(studentId)) {
            return res;
        }

        List<MessageUser> fetchedTeacherMessages = messageUserDAO.getMessages(AFTER_TIME_MILLIS, studentId, teacherId,
                TEACHERS_DAY_WISHES_2020, 0, 1);
        if (CollectionUtils.isNotEmpty(fetchedTeacherMessages)) {
            MessageUser found = fetchedTeacherMessages.get(0);
            res.setMessage(found.getMessage());
            UserBasicInfo basicInfo = fosUtils.getUserBasicInfo(found.getUserId(), false);
            res.setPostedStudentName(basicInfo.getFullName());
        }

        return res;
    }

    public PlatformBasicResponse postWishForTeacher(TeacherWishesReq req) {
        PlatformBasicResponse res = new PlatformBasicResponse();
        Query q = new Query();
        q.addCriteria(Criteria.where(MessageUser.Constants.USER_ID).is(req.getStudentId()));
        q.addCriteria(Criteria.where(MessageUser.Constants.TO_USER_ID).is(req.getTeacherId()));
        q.addCriteria(Criteria.where(MessageUser.Constants.CONTEXT_TYPE).is(TEACHERS_DAY_WISHES_2020));
        MessageUser found = messageUserDAO.findOne(q, MessageUser.class);
        if (Objects.nonNull(found) && found.getId() != null) {
            Update update = new Update();
            update.set(MessageUser.Constants.MESSAGE, req.getMessage());
            messageUserDAO.updateFirst(q, update, MessageUser.class);
            return res;
        }

        MessageUser postMessage = new MessageUser();
        postMessage.setContextType(TEACHERS_DAY_WISHES_2020);
        postMessage.setMessage(req.getMessage());
        postMessage.setUserId(req.getStudentId());
        postMessage.setToUserId(req.getTeacherId());
        messageUserDAO.upsert(postMessage);

        teacherDayStatsDAO.updateCountForTeacherComments(req.getTeacherId());
        return res;
    }
}
