/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.managers;

import com.vedantu.aws.AbstractAwsSQSManager;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);   
    
    private String env;

    public AwsSQSManager(){
        super();
        logger.info("initializing AwsSQSManager");
    }

}
