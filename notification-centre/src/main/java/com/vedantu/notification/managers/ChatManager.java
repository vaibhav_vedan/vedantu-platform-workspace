package com.vedantu.notification.managers;


import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.AblyActiveChannel;
import com.vedantu.notification.entity.ChatMessage;
import com.vedantu.notification.enums.ChatMessageType;
import com.vedantu.notification.request.OfflineMessageRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.GetChatMessageHistoryResponse;
import com.vedantu.notification.serializers.AblyActiveChannelDAO;
import com.vedantu.notification.serializers.ChatMessageDAO;
import com.vedantu.notification.thirdparty.AblyChannelsManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import io.ably.lib.types.AblyException;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONException;
/*
 * Manager Service for Email APIs
 */
@Service
public class ChatManager {
	
	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ChatManager.class);
	
	@Autowired
	public ChatMongoManager chatMongoManager;
        
        @Autowired
	private AblyActiveChannelDAO ablyActiveChannelDAO;
        
	@Autowired
	private ChatMessageDAO chatMessageDAO;
	
        @Autowired
        private AblyChannelsManager ablyChannelsManager;
        
	public BaseResponse fetchUsersLastChattedWith(Long userId, Integer start, Integer limit){
		
		logger.info("Entering userId: "+userId.toString());
		BaseResponse jsonResp = new BaseResponse();
		
		try {			
			jsonResp = chatMongoManager.getPreviouslyChattedUsers(userId, limit, start);			
		} catch (Exception e) {	
			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			jsonResp.setErrorMessage(e.getMessage());
			return jsonResp;			
		}
		
		//logger.info("Exiting Response: "+jsonResp.toString());
		return jsonResp;
	}

	public BaseResponse initiateChatWithUser(Long fromUserId, Long toUserId) {
		logger.info("Entering fromUserId: "+ fromUserId.toString() + " toUserId:" + toUserId.toString());
		BaseResponse jsonResp = new BaseResponse();
		
		try {			
			jsonResp = chatMongoManager.initiateChatWithUser(fromUserId, toUserId);			
		} catch (Exception e) {	
			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			jsonResp.setErrorMessage(e.getMessage());
			return jsonResp;			
		}
		
		//logger.info("Exiting Response: "+jsonResp.toString());
		return jsonResp;
	}

	public BaseResponse updateLastChattedTime(Long userId, String channelName) {
		logger.info("Entering userId: "+ userId.toString() + " channelName:" + channelName);
		BaseResponse jsonResp = new BaseResponse();
		
		try {			
                        setAblyChannelActive(channelName);
			jsonResp = chatMongoManager.updateLastChattedTime(userId, channelName);			
		} catch (Exception e) {	
			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			jsonResp.setErrorMessage(e.getMessage());
			return jsonResp;			
		}
		
		//logger.info("Exiting Response: "+jsonResp.toString());
		return jsonResp;
	}

	public BaseResponse updateUnreadMessageCount(Long userId, String channelName, int count) {
		
		logger.info("Entering userId: "+ userId.toString() + " channelName:" + channelName
				+ " count:" + count);
		BaseResponse jsonResp = new BaseResponse();
		
		try {			
			jsonResp = chatMongoManager.updateUnreadMessageCount(userId, channelName, count);			
		} catch (Exception e) {	
			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			jsonResp.setErrorMessage("Internal Server Error. Error Parsing JSON response");
			return jsonResp;			
		}
		
		//logger.info("Exiting Response: "+jsonResp.toString());
		return jsonResp;
	}

	public BaseResponse replyToOfflineMessage(OfflineMessageRequest request, ChatMessageType chatMessageType) {
		
		logger.info("Entering request: "+ request.toString());
		BaseResponse jsonResp = new BaseResponse();
		
		try {			
			jsonResp = chatMongoManager.replyToOfflineMessage(request, chatMessageType);			
		} catch (Exception e) {	
			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			jsonResp.setErrorMessage("Internal Server Error. Error Parsing JSON response");
			return jsonResp;			
		}
		
		//logger.info("Exiting Response: "+jsonResp.toString());
		return jsonResp;
	}

        public PlatformBasicResponse setAblyChannelActive(String channelName){
            logger.info("Entering setAblyChannelActive: "+ channelName);
            PlatformBasicResponse resp=new PlatformBasicResponse();
            AblyActiveChannel channel=new AblyActiveChannel(channelName);
            ablyActiveChannelDAO.upsert(channel);
            resp.setSuccess(true);
            return resp;
        }
        
        public PlatformBasicResponse getChatChannelName(Long fromUserId,Long toUserId){
            logger.info("Entering getChatChannelName: "+ fromUserId +"  : "+toUserId);
            return chatMongoManager.getChatChannelName(fromUserId, toUserId);
        }
        
        public GetChatMessageHistoryResponse getChatMessageHistory(String channelName, Integer size, Long before) throws VException {
            logger.info("Entering getChatMessageHistory: "+ channelName);
            GetChatMessageHistoryResponse resp=new GetChatMessageHistoryResponse();
            try{
            List<ChatMessage> chatMessages=new ArrayList();
            if(before==null){
                before=System.currentTimeMillis();
            }
            chatMessages.addAll(chatMessageDAO.getChatMessges(channelName, size, before));
//            Long start=null;
//            if(!chatMessages.isEmpty()){
//                start= chatMessages.get(0).getServerTime()+1;
//            }
//            chatMessages.addAll(0, ablyChannelsManager.getChatMessages(channelName, start, before, size));
//            if(chatMessages.size()>size){
//                chatMessages=chatMessages.subList(0, size);
//            }
            resp.setMessages(chatMessages);
            logger.info("Result for  getChatMessageHistory: "+ chatMessages);
            }catch(Exception e){
                logger.error("There is a error while fetching chat history ",e);
                throw new VException(ErrorCode.SERVICE_ERROR ,"There is some error fetching " );
            }
            return resp;
        }
//	public BaseResponse setStatusType(Long userId, Role role, StatusType statusType) {
//		
//		logger.info("Entering userId: "+ userId.toString() + " statusType:" + statusType);
//		BaseResponse jsonResp = new BaseResponse();
//		
//		try {			
//			jsonResp = chatMongoManager.setStatusType(userId, role, statusType);			
//		} catch (Exception e) {	
//			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
//			jsonResp.setErrorMessage("Internal Server Error. Error Parsing JSON response");
//			return jsonResp;			
//		}
//		
//		//logger.info("Exiting Response: "+jsonResp.toString());
//		return jsonResp;
//	}
//
//	public BaseResponse getAllStatusTypes(List<Long> userIds) {
//		logger.info("Entering userIds: "+ userIds.toString());
//		BaseResponse jsonResp = new BaseResponse();
//		
//		try {			
//			jsonResp = chatMongoManager.getAllStatusTypes(userIds);			
//		} catch (Exception e) {	
//			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
//			jsonResp.setErrorMessage("Internal Server Error. Error Parsing JSON response");
//			return jsonResp;			
//		}
//		
//		//logger.info("Exiting Response: "+jsonResp.toString());
//		return jsonResp;
//	}
//
//	public BaseResponse getMyStatusType(Long userId) {
//		logger.info("Entering userId: "+ userId.toString());
//		BaseResponse jsonResp = new BaseResponse();
//		
//		try {			
//			jsonResp = chatMongoManager.getMyStatusType(userId);			
//		} catch (Exception e) {	
//			jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
//			jsonResp.setErrorMessage("Internal Server Error. Error Parsing JSON response");
//			return jsonResp;			
//		}
//		
//		//logger.info("Exiting Response: "+jsonResp.toString());
//		return jsonResp;
//	}
}
		