package com.vedantu.notification.managers;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.SmsTemplate;
import com.vedantu.notification.request.CreateSmsTemplateReq;
import com.vedantu.notification.response.SmsTemplatesWithFilterRes;
import com.vedantu.notification.serializers.SmsTemplateDAO;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class SmsTemplateManager {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(SmsTemplateManager.class);

    @Autowired
    private SmsTemplateDAO smsTemplateDAO;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private FosUtils fosUtils;


    public PlatformBasicResponse createSmsTemplate(CreateSmsTemplateReq req) throws VException {
        SmsTemplate smsTemplate = new SmsTemplate(req.getSmsType(), req.getRole(), req.getBody(), req.getUnSubscriptionCheck(), req.getUnSubscriptionLink(), req.getDescription());
        smsTemplateDAO.create(smsTemplate, sessionUtils.getCallingUserId());
        return new PlatformBasicResponse();
    }


    public PlatformBasicResponse updateSmsTemplate(SmsTemplate smsTemplateReq) throws VException {
        if (smsTemplateReq.getId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "id is mandatory");
        }
        if (StringUtils.isEmpty(smsTemplateReq.getBody())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "body is mandatory to update");
        }
        SmsTemplate template = smsTemplateDAO.getEntityById(smsTemplateReq.getId(), SmsTemplate.class);
        if (template == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid id");
        }
        logger.info("updated smsTemplate: " + smsTemplateReq.toString());
        template.setBody(smsTemplateReq.getBody());
        template.setEntityState(smsTemplateReq.getEntityState());
        template.setUnSubscriptionCheck(smsTemplateReq.getUnSubscriptionCheck());
        template.setUnSubscriptionLink(smsTemplateReq.getUnSubscriptionLink());
        template.setDescription(smsTemplateReq.getDescription());
        smsTemplateDAO.create(template, sessionUtils.getCallingUserId());
        return new PlatformBasicResponse();
    }

    public List<SmsTemplate> getSmsTemplates() {
        Query query = new Query();
        return smsTemplateDAO.runQuery(query, SmsTemplate.class);
    }

    public List<SmsTemplatesWithFilterRes> getSmsTemplatesWithFilter(Integer start, Integer limit, EntityState state) {
        List<SmsTemplatesWithFilterRes> response = new ArrayList<>();
        List<SmsTemplate> smsTemplates =  smsTemplateDAO.getSmsTemplatesWithFilter(start, limit, state);
        Set<String> uniqueUserIdsSet = new HashSet<>();

        smsTemplates.forEach(smsTemplate -> {
            if(smsTemplate != null){
                if(StringUtils.isNotEmpty(smsTemplate.getCreatedBy())){
                    uniqueUserIdsSet.add(smsTemplate.getCreatedBy());
                }
                if(StringUtils.isNotEmpty(smsTemplate.getLastUpdatedBy())){
                    uniqueUserIdsSet.add(smsTemplate.getLastUpdatedBy());
                }
            }
        });

        List<String> uniqueUserIds = uniqueUserIdsSet.stream().collect(Collectors.toList());
        logger.info("uniqueUserIds : " + uniqueUserIds);
        Map<String, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMap(uniqueUserIds, Boolean.TRUE);
        for (SmsTemplate smsTemplate : smsTemplates) {
            String createdByEmail = null;
            if (StringUtils.isNotEmpty(smsTemplate.getCreatedBy())) {
                if (userBasicInfoMap.get(smsTemplate.getCreatedBy()) != null && StringUtils.isNotEmpty(userBasicInfoMap.get(smsTemplate.getCreatedBy()).getEmail())) {
                    createdByEmail = userBasicInfoMap.get(smsTemplate.getCreatedBy()).getEmail();
                }
            }
            String lastUpdatedByEmail = null;
            if (StringUtils.isNotEmpty(smsTemplate.getLastUpdatedBy())) {
                if (userBasicInfoMap.get(smsTemplate.getLastUpdatedBy()) != null && StringUtils.isNotEmpty(userBasicInfoMap.get(smsTemplate.getLastUpdatedBy()).getEmail())) {
                    lastUpdatedByEmail = userBasicInfoMap.get(smsTemplate.getLastUpdatedBy()).getEmail();
                }
            }
            response.add(new SmsTemplatesWithFilterRes(smsTemplate, createdByEmail, lastUpdatedByEmail));
        }
        logger.info("smsTemplatesWithFilterResponse : " + response);
        return response;

    }
}
