package com.vedantu.notification.managers;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.Email;
import com.vedantu.notification.entity.EmailLinksOpenStatus;
import com.vedantu.notification.entity.EmailNotification;
import com.vedantu.notification.entity.UnsubscribedEmail;
import com.vedantu.notification.entity.UserCommunicationCount;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.EmailNotificationSubType;
import com.vedantu.notification.enums.EmailNotificationType;
import com.vedantu.notification.enums.EmailPriorityType;
import com.vedantu.notification.enums.EmailProperty;
import com.vedantu.notification.enums.EmailService;
import com.vedantu.notification.pojo.QuestionChangedPojo;
import com.vedantu.notification.pojos.UnsubscribedEmailInfo;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.GetUnsubscribedListReq;
import com.vedantu.notification.requests.UnsubscribeEmailReq;
import com.vedantu.notification.response.BaseEmailResponse;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.responses.GetUnsubscribedListRes;
import com.vedantu.notification.serializers.EmailDAO;
import com.vedantu.notification.serializers.EmailNotificationDAO;
import com.vedantu.notification.serializers.RedisDAO;
import com.vedantu.notification.serializers.UnsubscribedEmailDAO;
import com.vedantu.notification.serializers.UserCommunicationCountDAO;
import com.vedantu.notification.thirdparty.AmazonSESSDKManager;
import com.vedantu.notification.thirdparty.MandrillManager;
import com.vedantu.notification.thirdparty.emailotpmanager.AmazonSESUSEast;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.EncryptionUtil;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

//import com.vedantu.notification.thirdparty.AmazonSESSMTPManager;
//import com.vedantu.notification.thirdparty.MandrillManager;

/*
 * Manager Service for Email APIs
 */
@Service
public class EmailManager {

    @Autowired
    private MandrillManager mandrillManager;
    //
    // @Autowired
    // private AmazonSESSMTPManager sesSmtpManager;
    @Autowired
    private AmazonSESSDKManager sesSdkManager;

    @Autowired
    private AmazonSESUSEast usEastSDKManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private EmailNotificationDAO emailNotificationDAO;

    @Autowired
    private EmailDAO emailDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private UnsubscribedEmailDAO unsubscribedEmailDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EmailManager.class);

    private static Boolean isEmailEnabled;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private UserCommunicationCountDAO userCommunicationCountDAO;

    public static String env;

    private static final int MAX_NUM_RETRIES = 3;

    private List<CommunicationType> bucketableNotificationType = new ArrayList<>();
    // todo discuss this
    private static final List<CommunicationType> avoidFilterList = new ArrayList<>(Arrays.asList(CommunicationType.EMAIL_VERIFICATION, CommunicationType.FORGOT_PASSWORD,
            CommunicationType.GENERATE_PASSWORD, CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD, CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD_2,CommunicationType.OTF_SESSION_FEEDBACK, CommunicationType.WAVE_LEADERBOARD_RECOGNITION,
            CommunicationType.WAVE_STREAK_RECOGNITION,CommunicationType.ALERT_ACADOPS_FOR_SECTION_BULK_UPDATE_COMPLETION,CommunicationType.ALERT_ACADOPS_FOR_SECTION_FILLING,CommunicationType.ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE, CommunicationType.MANUAL_PASSWORD_RESET, CommunicationType.FOS_DEMO_SESSION_CREATION,
            CommunicationType.ORDERS_EXPORT_EMAIL,CommunicationType.AUTO_ENROLLMENT_UPDATE));

    public EmailManager() {
        super();
        //bucketableNotificationType.add(CommunicationType.OTM_OTO_SESSION_CANCELLED);
        //bucketableNotificationType.add(CommunicationType.OTM_OTO_SESSION_RESCHEDULE);
        bucketableNotificationType.add(CommunicationType.LMS_REEVALUATE_TEST);
        isEmailEnabled = ConfigUtils.INSTANCE.getBooleanValue("email.enabled");
        env = ConfigUtils.INSTANCE.getStringValue("environment");
    }

    /*
	 * Method to send Email via calling the Email Delivery System's API
	 * 
	 * @param Email Object containing email data
     */
    public BaseResponse sendEmail(EmailRequest email) throws VException, AddressException, MessagingException, IOException, JSONException, InterruptedException {
        logger.info("Entering Email: " + email.toString());
        BaseResponse jsonResp = new BaseResponse();
        if (ArrayUtils.isEmpty(email.getTo())
                && ArrayUtils.isEmpty(email.getCc())
                && ArrayUtils.isEmpty(email.getBcc())) {
            logger.info("No email id recepeints found for email of " + email.getType() + " Email payload: " + email);
            return jsonResp;
        }



        //try {
        if (bucketableNotificationType.contains(email.getType())) {
            if (null != email.getTo() && null != email.getTo().get(0) && null != email.getTo().get(0).getAddress()) {
                try {
                    updateRedisHashSet(email);
                    return jsonResp;
                } catch (Exception ex) {
                    logger.info("Error in updating hash set for : " + email + " Exception " + ex);
                }
                //return jsonResp;
            }
        }

        if (CommunicationType.PHONE_VERIFICATION != email.getType() &&
                CommunicationType.EMAIL_VERIFICATION != email.getType() &&
                CommunicationType.MANUAL_NOTIFICATION != email.getType() &&
                CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS != email.getType() &&
                CommunicationType.GENERATE_PASSWORD != email.getType()) {

            long currentTime = System.currentTimeMillis();
            long expectedDelivery = email.getExpectedDeliveryMillis() > 0 ? email.getExpectedDeliveryMillis() : email.getCreatedAt();
            long delay = currentTime - expectedDelivery;

            if (delay > email.getMaxDelayMillis()) {
                jsonResp.setErrorCode(ErrorCode.DELAYED_REQUEST);
                jsonResp.setErrorMessage("email not sent because delayed more than allowed");
                logger.info("Expected Delivery " + expectedDelivery);
                logger.info("Expected Delay " + email.getMaxDelayMillis());
                logger.info("Delay in sending " + delay);
                logger.warn("Not Sending SMS to " + email.getTo());
                return jsonResp;
            }
        }

        if(null != email.getType() && !avoidFilterList.contains(email.getType())) {
            List<InternetAddress> emailAddressList = mongoFilterForEmailCount(email);
            email.setTo(emailAddressList);

            if (CollectionUtils.isEmpty(emailAddressList)) {
                logger.info("No email id recepeints found for email of " + email.getType() + " Email payload: " + email);
                return jsonResp;
            }
        }

        if(null != email.getType() && !avoidFilterList.contains(email.getType()) && env.equalsIgnoreCase("PROD")){
            addEmailUnsubscribeLink(email);
            email.setIncludeUnsubscribeLink(true);
        }

//        // setting only for otm trial session
//        if(null != email.getType() && CommunicationType.OTM_TRIAL_SESSION.equals(email.getType())){
//            addEmailUnsubscribeLink(email);
//            email.setIncludeUnsubscribeLink(true);
//        }

        if (isEmailEnabled) {
            if (email.getBody() == null || StringUtils.isEmpty(email.getBody()) || CommunicationType.MANUAL_NOTIFICATION.equals(email.getType())) {
                logger.info("setting email body");
                setEmailFromTemplate(email);
            }
            //   for(int i=0; i<MAX_NUM_RETRIES; i++){
            //       try{
            if (EmailService.MANDRILL.equals(email.getEmailService())) {
                logger.info("\nSending email from mandrillManager");
                jsonResp = mandrillManager.sendEmail(email);
            } else {
                if(null != email.getPriorityType() && EmailPriorityType.HIGH.equals(email.getPriorityType())){
                    logger.info("\nSending email from amazon sesSdk otp manager");
                    jsonResp = sesSdkManager.sendEmail(email);
                }
                else if(null != email.getType() && CommunicationType.ASSIGNMENT_SHARE.equals(email.getType())){
                    BaseEmailResponse successResp = new BaseEmailResponse();
                    successResp.setErrorCode(ErrorCode.SUCCESS);
                    successResp.setErrorMessage("");
                    successResp.setStatus(ErrorCode.SUCCESS.toString());
                    successResp.setRejectReason("");
                    return successResp;
                }
                else {
                    logger.info("\nSending email from amazon sesSdk manager");
                    jsonResp = sesSdkManager.sendEmail(email);
                }
            }

            if(null != jsonResp && null != email.getPriorityType()
                    && EmailPriorityType.HIGH.equals(email.getPriorityType())
                    && !ErrorCode.SUCCESS.equals(jsonResp.getErrorCode())){
                logger.info("\nretry email from aws other region");
                jsonResp = usEastSDKManager.sendEmail(email);
            }

            if(null != jsonResp && null != email.getPriorityType()
                    && EmailPriorityType.HIGH.equals(email.getPriorityType())
                    && !ErrorCode.SUCCESS.equals(jsonResp.getErrorCode())){
                logger.info("\nretry email from mandrillManager");
                jsonResp = mandrillManager.sendEmail(email);
            }
            /* if(!ErrorCode.SUCCESS.equals(jsonResp.getErrorCode())){
                            throw new Exception("Error in sending email: "+jsonResp.getErrorMessage());
                        }
                        
                        break;
                    
                    }catch(Exception ex){
                        if(i < MAX_NUM_RETRIES-1){
                            logger.warn("Exception : "+ex.getMessage());
                            continue;    
                        }
                        if(i == MAX_NUM_RETRIES - 1){
                            logger.error("Error in sending email: "+ex.getMessage());
                            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "Error in sending email");
                        }
                    }
                }*/
        } else {
            jsonResp.setErrorCode(ErrorCode.SUCCESS);
            jsonResp.setErrorMessage(
                    "Email is not enabled for the environment. Enable it from application.properties");
        }

        //} catch (Exception e) {
        //    jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
        //    jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
        //}
        logger.info("Exiting Response: " + jsonResp);
        return jsonResp;
    }

    public String getBucketSortKey() {
        return env + "_" + "BUCKET";
    }

    public void updateRedisHashSet(EmailRequest emailRequest) throws BadRequestException {
        String emailId = emailRequest.getTo().get(0).getAddress();
        String hashedBucketKey = redisDAO.getBucketedNotificationRedisKey(env, emailId);
        redisDAO.addToSet(getBucketSortKey(), emailId);
        redisDAO.addToList(hashedBucketKey, new Gson().toJson(emailRequest));
    }

    private void addEmailUnsubscribeLink(EmailRequest emailRequest) {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("SELF_ENDPOINT");
        if(null == emailRequest.getTo() || null == emailRequest.getTo().get(0) || null == emailRequest.getTo().get(0).getAddress())
            return;
        String encryptedEmail;
        try{
            encryptedEmail = EncryptionUtil.encrypt(emailRequest.getTo().get(0).getAddress().getBytes());
        }catch (Exception e){
            throw new RuntimeException(e);
        }
        String unsubscribeLink = notificationEndpoint + "email/unsubscribeEmail" + "?email=" + encryptedEmail + "&type=" + emailRequest.getType();
        Map<String, Object> bodyScopes = emailRequest.getBodyScopes();
        if(null != bodyScopes) {
            bodyScopes.put("unsubscribeLink", unsubscribeLink);
        }
        emailRequest.setBodyScopes(bodyScopes);
    }

    private List<InternetAddress> mongoFilterForEmailCount(EmailRequest request){
        logger.info("Entry: mongoFilterForEmailCount with request: "+request.toString());
        List<InternetAddress> internetAddressList = request.getTo();
        logger.info("InternetAddressList: "+internetAddressList.toString());
        Integer limit = Integer.parseInt(ConfigUtils.INSTANCE.properties.getProperty("email.limit.per.communication.type"));

        if(null != request.getType() && CommunicationType.MANUAL_PASSWORD_RESET.equals(request.getType())){
            limit = 1;
        }
        List<InternetAddress> allowedInternetAddressList = new ArrayList<>(); // will be response

        List<String> emailAddressList = new ArrayList<>();
        List<String> updateEmailList = new ArrayList<>();   // for incrementing count at a time
        List<String> newAddressList = new ArrayList<>(); // for inserting new address at a time
        List<UserCommunicationCount> newUserList = new ArrayList<>();

        for(InternetAddress internetAddress: internetAddressList){
            emailAddressList.add(internetAddress.getAddress());
        }
        logger.info("Size of emailAddressList from internetAddressList: "+emailAddressList.size());

        String date = formatDate(System.currentTimeMillis());

        Map<String, Integer> map = new HashMap<>();
        int max_fetch_size = 500;
        for(int i=0;i<emailAddressList.size();i=i+max_fetch_size){
            if(emailAddressList.size() > (i+max_fetch_size)){
                logger.info("Fetching count for "+i+" to "+(i+max_fetch_size)+" email addresses");
                map.putAll(userCommunicationCountDAO.getCountByAddressList(emailAddressList.subList(i, i+max_fetch_size), date, request.getType()));
            }else{
                logger.info("Fetching count for "+i+" to "+emailAddressList.size()+" email addresses");
                map.putAll(userCommunicationCountDAO.getCountByAddressList(emailAddressList.subList(i, emailAddressList.size()), date, request.getType()));
            }
        }

        for(String email: map.keySet()){
            if(map.get(email) == 0){
                newAddressList.add(email);
                newUserList.add(new UserCommunicationCount(email, date, request.getType()));
            }else if(map.get(email) <= limit){
                updateEmailList.add(email);
            }
        }

        logger.info("newAddressList size: "+newAddressList.size());
        logger.info("updateEmailList size: "+updateEmailList.size());

        for(InternetAddress internetAddress: internetAddressList){
            if(newAddressList.contains(internetAddress.getAddress()) || updateEmailList.contains(internetAddress.getAddress())){
                if(StringUtils.isNotEmpty(internetAddress.getAddress())) {
                    allowedInternetAddressList.add(internetAddress);
                }
            }
        }

        logger.info("allowedInternetAddressList: "+allowedInternetAddressList.toString());

        logger.info("Start: incrementing count for updateEmailList:");
        userCommunicationCountDAO.incrementUserDayCount(updateEmailList, date, request.getType());
        logger.info("End: incrementing count for updateEmailList:");
        logger.info("Start: inserting new addresses to db");
        userCommunicationCountDAO.save(newUserList);
        logger.info("END: inserting new addresses to db");
        logger.info("EXIT: mongoFilterForEmailCount");
        return allowedInternetAddressList;
    }

    /*
	 * Method to send Bulk Email via calling the Email Delivery System's API
	 * multiple times
	 * 
	 * @param Email Object containing email data
     */
    public BaseResponse sendBulkEmail(EmailRequest email) {

        logger.info("Entering Email: " + email.toString());
        BaseResponse jsonResp = new BaseResponse();

        if (CommunicationType.PHONE_VERIFICATION != email.getType() &&
                CommunicationType.EMAIL_VERIFICATION != email.getType() &&
                CommunicationType.MANUAL_NOTIFICATION != email.getType() &&
                CommunicationType.SIGNUP_VIA_ISL_REGISTRATION_TOOLS != email.getType() &&
                CommunicationType.GENERATE_PASSWORD != email.getType()) {

            long currentTime = System.currentTimeMillis();
            long expectedDelivery = email.getExpectedDeliveryMillis() > 0 ? email.getExpectedDeliveryMillis() : email.getCreatedAt();
            long delay = currentTime - expectedDelivery;

            if (delay > email.getMaxDelayMillis()) {
                jsonResp.setErrorCode(ErrorCode.DELAYED_REQUEST);
                jsonResp.setErrorMessage("email not sent because delayed more than allowed");
                logger.info("Expected Delivery " + expectedDelivery);
                logger.info("Expected Delay " + email.getMaxDelayMillis());
                logger.info("Delay in sending " + delay);
                logger.warn("Not Sending SMS to " + email.getTo());
                return jsonResp;
            }
        }

        if(null != email.getType() && !avoidFilterList.contains(email.getType())) {
            List<InternetAddress> emailAddressList = mongoFilterForEmailCount(email);
            email.setTo(emailAddressList);

            if (CollectionUtils.isEmpty(emailAddressList)) {
                logger.info("No email id recepeints found for email of " + email.getType() + " Email payload: " + email);
                return jsonResp;
            }
        }

        try {
            if (isEmailEnabled) {
                if (email.getBody() == null || StringUtils.isEmpty(email.getBody()) || CommunicationType.MANUAL_NOTIFICATION.equals(email.getType())) {
                    setEmailFromTemplate(email);
                }
                jsonResp = sesSdkManager.sendBulkEmail(email);
            } else {
                jsonResp.setErrorCode(ErrorCode.SUCCESS);
                jsonResp.setErrorMessage(
                        "Email is not enabled for the environment. Enable it from application.properties");
            }
        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
            return jsonResp;
        }

        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    public static String formatDate(Long date) {
        if (date == null || date <= 0) {
            return null;
        }
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        sdf.setTimeZone(TimeZone.getTimeZone(DateTimeUtils.TIME_ZONE_GMT));
        return sdf.format(new Date(date));
    }

    /*
	 * Method to get sending quota
     */
    public BaseResponse getSendQuota() {

        logger.info("Entering");
        BaseResponse jsonResp = new BaseResponse();

        try {
            jsonResp = sesSdkManager.getSendQuota();
        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
            return jsonResp;
        }

        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    /*
	 * Method to get sending statistics
     */
    public BaseResponse getSendStatistics(Integer lastHours) {

        logger.info("Entering");
        BaseResponse jsonResp = new BaseResponse();

        try {
            jsonResp = sesSdkManager.getSendStatistics(lastHours);
        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
            return jsonResp;
        }

        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    /*
	 * Method to get sending statistics
     */
    public BaseResponse emailOpened(String id) {

        logger.info(" emailOpened emailMa   ");
        BaseResponse jsonResp = new BaseResponse();

        try {
            jsonResp = sesSdkManager.emailOpened(id);
        } catch (Exception e) {
            jsonResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            jsonResp.setErrorMessage("Internal Server Error. " + e.getMessage());
            return jsonResp;
        }

        logger.info("Exiting Response: " + jsonResp.toString());
        return jsonResp;
    }

    public PlatformBasicResponse emailLinkClicked(String clickId, String linkRandomId) {

        logger.info("Entering");
        Email email = emailDAO.getById(clickId);
        if (email != null) {
            email.setClicked(true);
            if (StringUtils.isNotEmpty(linkRandomId) && ArrayUtils.isNotEmpty(email.getEmailLinks())) {
                for (EmailLinksOpenStatus link : email.getEmailLinks()) {
                    if (linkRandomId.equals(link.getRandomId())) {
                        link.setClicked(true);
                    }
                }
            }
            emailDAO.create(email);
        }
        return new PlatformBasicResponse();
    }

    public void emailStatusNotification(String snsMessage) {

        logger.info("Entering " + snsMessage);
        JSONObject snsJSON;
        try {
            // change this whole thing
            snsJSON = new JSONObject(snsMessage);
            List<EmailNotification> emailNotifications = new ArrayList<>();
            String emailAddress = "";
            if (snsJSON.has("notificationType")) {
                if ("Bounce".equals(snsJSON.getString("notificationType"))) {
                    logger.info("Bounce Notification received");
                    JSONObject bounceObject = snsJSON.getJSONObject("bounce");
                    String feedbackId = bounceObject.getString("feedbackId");
                    JSONArray bouncedRecipients = bounceObject.getJSONArray("bouncedRecipients");
                    String bounceType = bounceObject.getString("bounceType");
                    EmailNotificationSubType subType = EmailNotificationSubType.BOUNCE_OTHERS;
                    if ("Permanent".equals(bounceType)) {
                        subType = EmailNotificationSubType.BOUNCE_PERMANENT;
                    } else if ("Transient".equals(bounceType)) {
                        subType = EmailNotificationSubType.BOUNCE_TRANSIENT;
                    }
                    for (int i = 0; i < bouncedRecipients.length(); i++) {
                        JSONObject bouncedRecipient = bouncedRecipients.getJSONObject(i);
                        emailAddress = bouncedRecipient.getString("emailAddress");
                        if (StringUtils.isEmpty(emailAddress)) {
                            continue;
                        }
                        // This makes sure that email address is parsed into
                        // personal name and address
                        InternetAddress internetAddress = new InternetAddress(emailAddress);
                        logger.info("Creating new Entry in EmailNotification");
                        emailNotifications.add(new EmailNotification(internetAddress.getAddress().toLowerCase(),
                                EmailNotificationType.BOUNCE, subType, snsMessage, feedbackId));

                    }
                } else if (snsJSON.getString("notificationType").equals("Complaint")) {
                    logger.info("Complaint Notification received");

                    JSONObject complaintObject = snsJSON.getJSONObject("complaint");
                    String feedbackId = complaintObject.getString("feedbackId");
                    JSONArray complainedRecipients = complaintObject.getJSONArray("complainedRecipients");
                    EmailNotificationSubType subType = EmailNotificationSubType.COMPLAINT_MULTIPLE;
                    if (complainedRecipients.length() == 1) {
                        subType = EmailNotificationSubType.COMPLAINT_SINGLE;
                    }

                    for (int i = 0; i < complainedRecipients.length(); i++) {
                        JSONObject complainedRecipient = complainedRecipients.getJSONObject(i);
                        emailAddress = complainedRecipient.getString("emailAddress");
                        if (StringUtils.isEmpty(emailAddress)) {
                            continue;
                        }
                        InternetAddress internetAddress = new InternetAddress(emailAddress);
                        logger.info("Creating new Entry in EmailNotification");
                        emailNotifications.add(new EmailNotification(internetAddress.getAddress().toLowerCase(),
                                EmailNotificationType.COMPLAINT, subType, snsMessage, feedbackId));
                    }
                }
                if (!emailNotifications.isEmpty()) {
                    logger.info("adding to db emailNotifications " + emailNotifications);
                    emailNotificationDAO.createAll(emailNotifications);
                }
            }
        } catch (JSONException | AddressException e) {
            logger.warn(e);
        }
    }

    private void setEmailFromTemplate(EmailRequest email) throws VException, IOException {
        boolean includeHeaderFooter = true;
        if (Boolean.FALSE.equals(email.getIncludeHeaderFooter())) {
            includeHeaderFooter = false;
        }
        if (CommunicationType.MANUAL_NOTIFICATION.equals(email.getType())) {
            email.setBody(getEmailBody(email.getBodyScopes(), email.getType(), email.getRole(), includeHeaderFooter, email.getBody(), email.getIncludeUnsubscribeLink()));
            email.setSubject(email.getSubject());
        } else {
            email.setBody(getEmailBody(email.getBodyScopes(), email.getType(), email.getRole(), includeHeaderFooter, email.getIncludeUnsubscribeLink()));
            email.setSubject(getEmailSubject(email.getSubjectScopes(), email.getType(), email.getRole()));
        }

        if (!CommunicationType.FORGOT_PASSWORD.equals(email.getType())
                && !CommunicationType.SIGNUP.equals(email.getType())
                && !CommunicationType.GENERATE_PASSWORD.equals(email.getType())
                && !CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD.equals(email.getType())
                && !CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD_2.equals(email.getType())
                && !CommunicationType.ASSIGNMENT_SHARE.equals(email.getType())
                && !CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_1_HOUR.equals(email.getType())
                && !CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_4_HOUR.equals(email.getType())
                && !CommunicationType.UPDATE_CURRICULUM_TRIGGER_AFTER_8_HOUR.equals(email.getType())
                && !CommunicationType.MANUAL_NOTIFICATION.equals(email.getType())) {
            String bccPropertyValue = getpropertyValue(EmailProperty.BCC, email.getType(), email.getRole());
            if (StringUtils.isNotEmpty(bccPropertyValue)) {
                if (email.getBcc() != null) {
                    email.getBcc().addAll(Arrays.asList(getInternetAddress(bccPropertyValue)));
                } else {
                    email.setBcc(new ArrayList<>(Arrays.asList(getInternetAddress(bccPropertyValue))));
                }
            }
        }

        //email.setCc(getInternetAddress(getpropertyValue(EmailProperty.CC, email.getType(), email.getRole())));
        //email.setFrom((InternetAddress.parse(getpropertyValue(EmailProperty.FROM, email.getType(), email.getRole())))[0]);
        //TODO: Push to RJ
        /*
         // send this DATA to RJMetrics
         HashMap<String, Object> emailValues = new HashMap<String, Object>();

         if (subjectScopes != null) {
         emailValues.putAll(subjectScopes);
         }

         if (bodyScopes != null) {
         emailValues.putAll(bodyScopes);
         }

         try {
         InternetAddress toAdd = (InternetAddress) email.getTo()[0];
         emailValues.put("keys", new String[] { "id" });
         emailValues.put("id", StringUtils.randomAlphaNumericStringWithLowerCase(6));
         emailValues.put("to", toAdd.getAddress());
         emailValues.put("toName", toAdd.getPersonal());
         emailValues.put("from", email.getFrom().getAddress());
         emailValues.put("fromName", email.getFrom().getPersonal());
         emailValues.put("emailSendTime", new Date(System.currentTimeMillis()));
         emailValues.put("emailSubject", email.getSubject());
         RJMetricsExportManager.INSTANCE.sendEmailData(emailValues);
         } catch (Throwable t) {
         LOG.throwing(getLogTag(), "setEmail", t);
         }
         */
    }

    private String getEmailBody(Map<String, Object> scopes, CommunicationType emailType, Role role,
            boolean includeHeaderFooter, boolean includeUnsubscribeLink)
            throws VException, IOException {
        return getEmailBody(scopes, emailType, role, includeHeaderFooter, null, includeUnsubscribeLink);
    }

    private String getEmailBody(Map<String, Object> scopes, CommunicationType emailType, Role role,
            boolean includeHeaderFooter, String manualNotificationBody, Boolean includeUnsubscribeLink)
            throws VException, IOException {

        String fileName = null;
        if (!CommunicationType.MANUAL_NOTIFICATION.equals(emailType)) {
            fileName = getpropertyValue(EmailProperty.BODY, emailType, role);
            if (fileName == null) {
                throw new com.vedantu.exception.NotFoundException(ErrorCode.HTML_FILE_NOT_FOUND,
                        "email template not found");
            }
        }
        logger.info("Email body filename " + fileName);
        if (scopes != null || CommunicationType.MANUAL_NOTIFICATION.equals(emailType)) {
            scopes.put("displayMobileNumberFooter",
                    ConfigUtils.INSTANCE.getStringValue("email.display.mobileNumber.footer"));
            scopes.put("displayEmailFooter", ConfigUtils.INSTANCE.getStringValue("email.display.emailId.footer"));
            scopes.put("displayEmailId", ConfigUtils.INSTANCE.getStringValue("email.display.emailId"));
            scopes.put("displayMobileNumber", ConfigUtils.INSTANCE.getStringValue("email.display.mobileNumber"));
            scopes.put("displayFeedbackEmailId", ConfigUtils.INSTANCE.getStringValue("email.display.feedback.emaiId"));
            scopes.put("displayContactusEmailId", ConfigUtils.INSTANCE.getStringValue("email.display.contactus.emaiId"));
            scopes.put("displayMailFromEmailId", ConfigUtils.INSTANCE.getStringValue("email.display.mailfrom.emaiId"));

        }
        String body = CommunicationType.MANUAL_NOTIFICATION.equals(emailType) ? manualNotificationBody : getHTMLContent(scopes, fileName);
        scopes.put("includeUnsubscribeLink",includeUnsubscribeLink); 
        if (!includeHeaderFooter) {
            if(includeUnsubscribeLink){
                HashMap<String, Object> bodyScopes = new HashMap<>();
                bodyScopes.put("body", body);
                bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
                if (scopes != null) {
                    bodyScopes.putAll(scopes);
                }
                body = getHTMLContent(bodyScopes, "UNSUBSCRIBE_LINK.html");                
            }
            return body;
        }            
        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("body", body);
        bodyScopes.put("baseUrl", ConfigUtils.INSTANCE.getStringValue("web.baseUrl"));
        if (scopes != null) {
            bodyScopes.putAll(scopes);
        }
        String emailBody;
        if (CommunicationType.MANUAL_NOTIFICATION == emailType) {
            emailBody = getHTMLContent(bodyScopes, "EMAIL_MANUAL.html");
        } else {
            emailBody = getHTMLContent(bodyScopes, "EMAIL.html");
        }
        return emailBody;
    }

    private String getHTMLContent(Map<String, Object> scopes, String fileName) throws VException, IOException {

        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        InputStream is = null;
        final String confDir = "EmailTemplates";

        String propertiesFilePath = confDir + java.io.File.separator + fileName;

        try {
            is = ConfigUtils.class.getClassLoader().getResourceAsStream(propertiesFilePath);
            Mustache mustache = mf.compile(new InputStreamReader(is, "UTF-8"), "emailContent");
            mustache.execute(writer, scopes);
            writer.flush();
        } catch (Exception e) {
            logger.info("error in mustache:" + e.getMessage());
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
        } finally {
            if (is != null) {
                is.close();
            }
        }
        return writer.toString();
    }

    private String getEmailSubject(Map<String, Object> scopes, CommunicationType emailType, Role role)
            throws IOException {

        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        Mustache mustache = mf.compile(new StringReader(getpropertyValue(EmailProperty.SUBJECT, emailType, role)),
                "emailSubject");
        mustache.execute(writer, scopes);
        writer.flush();

        return (ConfigUtils.INSTANCE.getBooleanValue("email.SUBJECT.env.append")
                ? ConfigUtils.INSTANCE.getStringValue("environment") + ": " : "") + writer.toString();
    }

    private String getpropertyValue(EmailProperty property, CommunicationType type, Role role) {
        logger.info("property {}",property);
        logger.info("type {}",type);
        logger.info("role {}",role);
        String propertyValue = ConfigUtils.INSTANCE.getStringValue("email." + property.name() + "." + type.name());
        if (propertyValue != null) {
            return propertyValue;
        }

        propertyValue = ConfigUtils.INSTANCE.getStringValue("email." + property.name() + "." + type.name() + "." + role.name());
        if (propertyValue != null) {
            return propertyValue;
        }
        propertyValue = ConfigUtils.INSTANCE.getStringValue("email." + property.name());
        return propertyValue;
    }

    private InternetAddress[] getInternetAddress(String address) {
        try {
            return InternetAddress.parse(address);
        } catch (Exception ex) {
            logger.info("Error parsing address", ex);
            return new InternetAddress[0];
        }
    }

    public PlatformBasicResponse markUnsubscribeStatus(UnsubscribeEmailReq req) {
        UnsubscribedEmail unsubscribedEmail = unsubscribedEmailDAO.getByEmail(req.getEmail());
        if (unsubscribedEmail != null) {
            unsubscribedEmail.setEntityState(req.getState());
        } else {
            unsubscribedEmail = new UnsubscribedEmail(req.getEmail());
            unsubscribedEmail.setEntityState(req.getState());
        }
        if(StringUtils.isNotEmpty(req.getType())){
            unsubscribedEmail.setType(req.getType());
            unsubscribedEmail.setEntityState(EntityState.ACTIVE);
        }
        unsubscribedEmail.setIpAddress(req.getIpAddress());
        unsubscribedEmailDAO.create(unsubscribedEmail);
        return new PlatformBasicResponse();
    }

    public GetUnsubscribedListRes getUnsubscribedList(GetUnsubscribedListReq req) throws VException {
        GetUnsubscribedListRes res = new GetUnsubscribedListRes();

        List<UnsubscribedEmail> unsubscribedEmails = unsubscribedEmailDAO.getUnsubscribedList(req);
        List<UnsubscribedEmailInfo> unsubscribedEmailInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(unsubscribedEmails)) {
            for (UnsubscribedEmail unsubscribedEmail : unsubscribedEmails) {
                UnsubscribedEmailInfo unsubscribedEmailInfo = mapper.map(unsubscribedEmail, UnsubscribedEmailInfo.class);
                unsubscribedEmailInfos.add(unsubscribedEmailInfo);
            }
        }
        res.setList(unsubscribedEmailInfos);

        Long count = unsubscribedEmailDAO.getUnsubscribedListCount(req);
        if (count != null) {
            res.setCount(count.intValue());
        }
        return res;
    }

    /*   public static void main(String[] args) throws VException, IOException {
        List<InstalmentInfo> infos = new ArrayList<>();
        infos.add(new InstalmentInfo(null, Long.MIN_VALUE, PaymentStatus.PAID, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.SIZE, Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE, null, Long.MIN_VALUE, null));
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        String s = "this is {{dateTime}} aksjs {{#infos}}value is {{paymentStatus}} other ({{#paymentStatus}}{{displayName}}{{/paymentStatus}}){{/infos}}";
        Map<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("dateTime", "ajith");
        bodyScopes.put("instalments", infos);
        try {
            Mustache mustache = mf.compile(new StringReader(s), "cpu.template.email");
            mustache.execute(writer, bodyScopes);
            writer.flush();
        } catch (Exception e) {
            System.err.println("error in mustache:" + e.getMessage());
        } finally {
        }
        System.err.println("sdddd>>>>> " + writer.toString());

        EmailManager em = new EmailManager();
        String content = em.getHTMLContent(bodyScopes, "SUBSCRIPTION_REQUEST_ACCEPTED_INSTALMENTS_STUDENT.html");
        System.err.println("final content " + content);
    }
     */
    public void consumeBulkEmail() throws BadRequestException {
        String key = getBucketSortKey();
        Set<String> emailIds = redisDAO.getSetMembers(key);
        for (String emailId : emailIds) {
            try {
                processClubbableEmailForId(emailId);
            } catch (Exception ex) {
                logger.error("Error in consuming emails for emailId: " + emailId + ", exception: " + ex.getMessage());
            }
        }
    }

    public void processClubbableEmailForId(String emailId) throws BadRequestException, UnsupportedEncodingException {
        String key = redisDAO.getBucketedNotificationRedisKey(env, emailId);
        long listLength = redisDAO.getLengthOfList(key);
        List<String> emailStringRequests = redisDAO.getList(key, 0, listLength - 1);

        List<EmailRequest> emailRequests = new ArrayList<>();
        for (String stringRequest : emailStringRequests) {
            emailRequests.add(new Gson().fromJson(stringRequest, EmailRequest.class));
        }

        Map<CommunicationType, List<EmailRequest>> emailMap = new HashMap<>();

        for (EmailRequest emailRequest : emailRequests) {
            /*if(CommunicationType.OTM_OTO_SESSION_CANCELLED.equals(emailRequest.getType()) || CommunicationType.OTM_OTO_SESSION_RESCHEDULE.equals(emailRequest.getType())){
                if(!emailMap.containsKey(CommunicationType.MULTI_SESSION_CANCELLED)){
                    emailMap.put(CommunicationType.MULTI_SESSION_CANCELLED, new ArrayList<>());
                }
                emailMap.get(CommunicationType.MULTI_SESSION_CANCELLED).add(emailRequest);
            }else*/
            if (CommunicationType.LMS_REEVALUATE_TEST.equals(emailRequest.getType())) {
                if (!emailMap.containsKey(CommunicationType.CLUBBED_LMS_REEVALUATE)) {
                    emailMap.put(CommunicationType.CLUBBED_LMS_REEVALUATE, new ArrayList<>());
                }
                emailMap.get(CommunicationType.CLUBBED_LMS_REEVALUATE).add(emailRequest);
            }
        }

        for (Map.Entry<CommunicationType, List<EmailRequest>> entry : emailMap.entrySet()) {
            /*if(entry.getKey().equals(CommunicationType.MULTI_SESSION_CANCELLED) && ArrayUtils.isNotEmpty(entry.getValue())){
                EmailRequest emailRequest = new EmailRequest();
                emailRequest.setType(CommunicationType.MULTI_SESSION_CANCELLED);
                emailRequest.setEmailService(entry.getValue().get(0).getEmailService());
                emailRequest.setBodyScopes(entry.getValue().get(0).getBodyScopes());
                emailRequest.setSubjectScopes(entry.getValue().get(0).getSubjectScopes());
                List<InternetAddress> toList = new ArrayList<>();
                toList.add(new InternetAddress("ankit.parashar+009@vedantu.com", "Ankit P"));
                emailRequest.setTo(toList);
                emailRequest.setRole(Role.STUDENT);
                awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, new Gson().toJson(emailRequest));
            }else*/
            if (entry.getKey().equals(CommunicationType.CLUBBED_LMS_REEVALUATE) && ArrayUtils.isNotEmpty(entry.getValue())) {
                processClubbedReevaluateTestMail(entry.getValue());
            }
        }

        redisDAO.trimList(key, listLength, listLength + 100);

        if ((redisDAO.getLengthOfList(key) != null && redisDAO.getLengthOfList(key) == 0) || redisDAO.getLengthOfList(key) == null) {
            redisDAO.popFromSet(getBucketSortKey(), emailId);
        }

    }

    public void processClubbedReevaluateTestMail(List<EmailRequest> emailRequests) {
        Map<String, EmailRequest> resultMap = new HashMap<>();
        Map<String, List<QuestionChangedPojo>> questionsChangedMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(emailRequests)) {
            for (EmailRequest emailRequest : emailRequests) {
                String testId = (String) emailRequest.getBodyScopes().get("testId");
                if (!resultMap.containsKey(testId)) {
                    resultMap.put(testId, emailRequest);
                }
                Map<String, Object> bodyScopes = emailRequest.getBodyScopes();
                if (!questionsChangedMap.containsKey(testId)) {
                    questionsChangedMap.put(testId, new ArrayList<>());
                }
                QuestionChangedPojo questionChangedPojo = new QuestionChangedPojo();
                questionChangedPojo.setCorrectionType((String) bodyScopes.get("correctionType"));
                questionChangedPojo.setQuestionNumber((String) bodyScopes.get("questionNumber"));
                questionChangedPojo.setReevaluationType((String) bodyScopes.get("reevaluationType"));
                questionsChangedMap.get(testId).add(questionChangedPojo);
            }
        }

        for (Map.Entry<String, EmailRequest> entry : resultMap.entrySet()) {
            if (questionsChangedMap.containsKey(entry.getKey())) {
                //entry.getValue().getBodyScopes().put("questionsChanged", new ArrayList<>());
                //entry.getValue().getBodyScopes().put("questionsChanged", questionsChangedMap.get(entry.getKey()));
                EmailRequest emailRequest = entry.getValue();
                emailRequest.getBodyScopes().put("questionsChanged", questionsChangedMap.get(entry.getKey()));
                emailRequest.setType(CommunicationType.CLUBBED_LMS_REEVALUATE);
                awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, new Gson().toJson(emailRequest));
            }

        }

    }

    public List<Email> getDeliveryReportForManualNotification(List<String> manualNotificationIds) {
        return emailDAO.getDeliveryReportForManualNotification(manualNotificationIds);
    }

    public PlatformBasicResponse unsubscribeEmail(String encryptedEmail, String type) {
        // decrypting email
        String decryptedEmail;
        try {
            byte[] bytes = DatatypeConverter.parseHexBinary(encryptedEmail); // hex to binary
            decryptedEmail = EncryptionUtil.decrypt(bytes);
        }catch (Exception e){
            return new PlatformBasicResponse(false, "", "");
        }
        UnsubscribeEmailReq unsubscribeEmailReq = new UnsubscribeEmailReq();
        unsubscribeEmailReq.setEmail(decryptedEmail);
        unsubscribeEmailReq.setType(type);
        return markUnsubscribeStatus(unsubscribeEmailReq);
    }

    public PlatformBasicResponse removeBounceEntries(UnsubscribeEmailReq req) throws BadRequestException {
        String email = req.getEmail();
        if(StringUtils.isEmpty(email)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Email cannot be mail");
        }
        emailNotificationDAO.removeBounceEntries(email);
        return new PlatformBasicResponse();
    }
}
