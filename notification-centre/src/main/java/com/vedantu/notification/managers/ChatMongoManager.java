package com.vedantu.notification.managers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.Channel;
import com.vedantu.notification.entity.ChannelUserInfo;
import com.vedantu.notification.enums.ChannelType;
import com.vedantu.notification.enums.ChatMessageType;
import com.vedantu.notification.pojo.ChattedUser;
import com.vedantu.notification.request.OfflineMessageRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.GetLastChattedUsersResponse;
import com.vedantu.notification.response.InitiateChatResponse;
import com.vedantu.notification.response.UpdateLastChattedTimeResponse;
import com.vedantu.notification.serializers.ChannelDAO;
import com.vedantu.notification.serializers.ChannelUserInfoDAO;
import com.vedantu.notification.serializers.UserStatusDAO;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import java.util.HashSet;
import java.util.Set;
import javax.annotation.PreDestroy;

@Service
public class ChatMongoManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ChatMongoManager.class);

    @Autowired
    public ChannelDAO channelDAO;

    @Autowired
    public ChannelUserInfoDAO channelUserInfoDAO;

    @Autowired
    public UserStatusDAO userStatusDAO;

    @Autowired
    private FosUtils fosUtils;

    //private static String userFetchUrl;
    //private static String pastSessionsFetchUrl;
    private static String pubNubPublishKey;

    private static String pubNubSubscribeKey;

    private String baseUrl;

    private Client client;

    private Pubnub pubnub = null;

    public ChatMongoManager() throws IllegalAccessException {
        super();
        //userFetchUrl = ConfigUtils.INSTANCE.getStringValue("user.fetch.url");
        //pastSessionsFetchUrl = ConfigUtils.INSTANCE.getStringValue("pastSessions.fetch.url");
        pubNubPublishKey = ConfigUtils.INSTANCE.getStringValue("pubnub.publishkey");
        pubNubSubscribeKey = ConfigUtils.INSTANCE.getStringValue("pubnub.subscribekey");
        baseUrl = ConfigUtils.INSTANCE.getStringValue("elasticsearch.baseurl");

        client = Client.create();

        boolean authenticationEnabled = ConfigUtils.INSTANCE.getBooleanValue("elasticsearch.authentication.enabled");
        if (authenticationEnabled) {
            String user = ConfigUtils.INSTANCE.getStringValue("elasticsearch.authentication.user");
            String password = ConfigUtils.INSTANCE.getStringValue("elasticsearch.authentication.password");
            if (!StringUtils.isEmpty(user) && !StringUtils.isEmpty(password)) {
                HTTPBasicAuthFilter authFilter = new HTTPBasicAuthFilter(user, password);
                client.addFilter(authFilter);
            } else {
                logger.error("User Not authorized to call Elastic Search");
                throw new IllegalAccessException("User Not authorized to call Elastic Search");
            }
        }

        pubnub = new Pubnub(ConfigUtils.INSTANCE.getStringValue("ably.apikey"), ConfigUtils.INSTANCE.getStringValue("ably.apikey"));
        pubnub.setCacheBusting(false);
        pubnub.setHeartbeat(60);
        pubnub.setUUID("Server");
        pubnub.setOrigin(ConfigUtils.INSTANCE.getStringValue("ably.origin"));
        pubnub.setDomain(ConfigUtils.INSTANCE.getStringValue("ably.domain"));
        // ChatMongoManager.pubnub.subscribe("vedantu-teachers",
        // subscribeCallBack );
        // ChatMongoManager.pubnub.presence("vedantu-teachers",
        // presenceCallBack);

    }

    public Pubnub getPubnub() {
        return pubnub;
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (pubnub != null) {
                pubnub.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing pubnub connection ", e);
        }
    }

    // Callback presenceCallBack = new Callback() {
    //
    // public void successCallback(String channel, Object message) {
    // logger.info(channel + " : "
    // + message.getClass() + " : " + message.toString());
    // JSONObject presenceObject = (JSONObject) message;
    // try {
    // String uuid = presenceObject.getString("uuid");
    //
    // if(!uuid.equals("Server")){
    //
    // Query query = new Query();
    // query.addCriteria(Criteria.where("userId").is(Long.parseLong(uuid)));
    //
    // UserStatus currentUserStatus = null;
    // List<UserStatus> currentUserStatuses = userStatusDAO.runQuery(query,
    // UserStatus.class);
    // if(!currentUserStatuses.isEmpty() && currentUserStatuses != null){
    //
    // currentUserStatus = currentUserStatuses.get(0);
    //
    // //currentUserStatus.setStatusType(statusType);
    // //userStatusDAO.create(currentUserStatus);
    // }
    //
    // String elasticSearchUrl = baseUrl + "vedantu" + "/" + "teachers" + "/"
    // + uuid + "/_update";
    // String json;
    //
    //
    // if(presenceObject.getString("action").equals("join")){
    // json = "{\"onlineStatus\" : 0}";
    // if(currentUserStatus != null &&
    // currentUserStatus.getStatusType().equals(StatusType.READY_TO_TEACH)){
    // Long lastWentOffline = currentUserStatus.getLastUpdated();
    // logger.info("User Last went offline at:"+lastWentOffline);
    // if(System.currentTimeMillis() - lastWentOffline < 70000){
    // json = "{\"onlineStatus\" : 1}";
    // logger.info("Updating online status of "+ uuid + " to 1");
    // }
    // else{
    // currentUserStatus.setStatusType(StatusType.NONE);
    // logger.info("Updating online status of "+ uuid + " to 0");
    // userStatusDAO.create(currentUserStatus);
    // }
    // } else {
    // logger.info("Updating online status of "+ uuid + " to 0");
    // }
    // }
    // else{
    // json = "{\"onlineStatus\" : -1}";
    // if(currentUserStatus != null){
    // userStatusDAO.create(currentUserStatus);
    // }
    // logger.info("Updating online status of "+ uuid + " to -1");
    // }
    //
    // String tempQuery = "{\"doc\" : %s }";
    // String updateData = String.format(tempQuery, json);
    // updateElasticSearchTeacherStatus(elasticSearchUrl, updateData);
    // }
    //
    // } catch (JSONException e) {
    // logger.error("Error parsing JSON from presence message", e);
    // }
    //
    // }
    //
    // public void errorCallback(String channel, PubnubError error) {
    // logger.info("ERROR on channel " + channel
    // + " : " + error.toString());
    // }
    //
    // };
    //
    // Callback subscribeCallBack = new Callback() {
    // public void successCallback(String channel, Object response) {
    // logger.info(response.toString());
    // }
    // public void errorCallback(String channel, PubnubError error) {
    // logger.info(error.toString());
    // }
    // };
    //
    public BaseResponse getPreviouslyChattedUsers(Long userId, Integer limit, Integer start) throws JSONException {

        logger.info("Entering userId:" + userId);

        List<ChannelUserInfo> channelUserInfos;
        Query query = new Query();
        Map<String, Integer> channelInfoIndexMap = new HashMap<String, Integer>();
        Map<String, Integer> userInfoIndexMap = new HashMap<String, Integer>();
        Integer index;

        List<ChattedUser> chattedUsers = new ArrayList<>();
        List<String> channelNames = new ArrayList<>();
        Set<Long> userIds = new HashSet<>();

        ChattedUser chattedUser;
        String currentChannelName;
        Channel currentChannel;

        query.addCriteria(Criteria.where("userId").is(userId));
        query.with(Sort.by(Sort.Direction.DESC, "lastUpdated"));
        query.skip(start);
        query.limit(limit);

        channelUserInfos = channelUserInfoDAO.runQuery(query, ChannelUserInfo.class);
        for (ChannelUserInfo currentChannelUserInfo : channelUserInfos) {
            currentChannelName = currentChannelUserInfo.getChannelName();
            chattedUser = new ChattedUser();
            chattedUser.setChannelName(currentChannelName);
            chattedUser.setNewMessagesCount(currentChannelUserInfo.getNewMessagesCount());
            chattedUser.setLastChattedTime(currentChannelUserInfo.getLastUpdated());
            chattedUsers.add(chattedUser);
            channelInfoIndexMap.put(currentChannelName, chattedUsers.size() - 1);
            channelNames.add(currentChannelName);
        }

        query = new Query();

        query.addCriteria(Criteria.where("_id").in(channelNames));
        List<Channel> channels = channelDAO.runQuery(query, Channel.class);
        List<String> foundChannelNames = new ArrayList<>();
        // logger.info("Chatted Size:" + chattedUsers.size());;

        // int totalRemoved = 0;
        for (int i = 0; i < channels.size(); i++) {
            // logger.info(i + "----" + channels.size());
            currentChannel = channels.get(i);
            currentChannelName = currentChannel.getId();
            Long otherUserId = getOtherMemberOfChannel(currentChannel.getParticipants(), userId);
            if (otherUserId != null && !userIds.contains(otherUserId)) {
                userIds.add(otherUserId);
                index = channelInfoIndexMap.get(currentChannelName);
                chattedUser = chattedUsers.get(index);
                chattedUser.setChannelType(currentChannel.getChannelType());
                chattedUser.setUserId(otherUserId.toString());
                userInfoIndexMap.put(otherUserId.toString(), index);
                chattedUsers.set(index, chattedUser);
                foundChannelNames.add(currentChannelName);
            }
            // else if(userIds.contains(otherUserId.toString())){
            // logger.info("Found extra channel. Removing");
            // index = channelInfoIndexMap.get(currentChannelName);
            // channelInfoIndexMap.remove(currentChannelName);
            // int removedIndex = index.intValue();
            // logger.info("Removing "+ (removedIndex - totalRemoved));
            // logger.info(removedIndex + "----" + totalRemoved);
            // chattedUsers.remove(removedIndex - totalRemoved);
            // channels.remove(i);
            // i--;
            // totalRemoved++;
            // logger.info("Chatted Size:" + chattedUsers.size());;
            // }
        }

        // logger.info(channels.size() + "---" + channelInfoIndexMap.size());
        if (channels.size() != channelInfoIndexMap.size()) {
            logger.info("Channel size not same");
            for (Entry<String, Integer> entry : channelInfoIndexMap.entrySet()) {
                String channel_name = entry.getKey();
                Integer user_index = entry.getValue();
                if (!foundChannelNames.contains(channel_name)) {
                    query = new Query();
                    query.addCriteria(Criteria.where("channelName").is(channel_name)
                            .andOperator(Criteria.where("userId").ne(userId)));
                    ChannelUserInfo otherUserInfo = channelUserInfoDAO.runQuery(query, ChannelUserInfo.class).get(0);
                    if (otherUserInfo != null) {
                        Long otherUserId = otherUserInfo.getUserId();
                        Channel channel = new Channel();
                        channel.setChannelType(ChannelType.CHAT);
                        Long[] participants = new Long[2];
                        participants[0] = otherUserId;
                        participants[1] = userId;
                        channel.setParticipants(participants);
                        channel.setId(channel_name);
                        channelDAO.create(channel);
                        if (channel.getParticipants()[0].equals(channel.getParticipants()[1])) {
                            logger.error("Creating channel with same participants");
                        }
                        userIds.add(otherUserId);
                        chattedUser = chattedUsers.get(user_index);
                        chattedUser.setChannelType(channel.getChannelType());
                        chattedUser.setUserId(otherUserId.toString());
                        userInfoIndexMap.put(otherUserId.toString(), user_index);
                        chattedUsers.set(user_index, chattedUser);

                    }
                }

            }
        }

        int count = 0, flag = 0;
        List<UserBasicInfo> userBasicInfos = fosUtils.getUserBasicInfosFromLongIds(userIds, false);

        if (userBasicInfos != null && !userBasicInfos.isEmpty()) {
            for (UserBasicInfo userBasicInfo : userBasicInfos) {
                if (userBasicInfo != null && userBasicInfo.getUserId() != null) {
                    index = userInfoIndexMap.get(userBasicInfo.getUserId().toString());
                    // logger.info(index);
                    count++;
                    chattedUser = chattedUsers.get(index);
                    chattedUser.setFirstName(userBasicInfo.getFirstName());
                    chattedUser.setFullName(userBasicInfo.getFullName());
                    chattedUser.setLastName(userBasicInfo.getLastName());
                    chattedUser.setProfilePicUrl(userBasicInfo.getProfilePicUrl());
                    chattedUser.setRole(userBasicInfo.getRole());
                    chattedUsers.set(index, chattedUser);
                } else {
                    logger.info("Null User Pojo found:" + count);
                    flag = 1;
                }
            }

        }

        logger.info("Final Count:" + count);
        // Add previous Session Users

//	try {
//                List<Long> prevSessionUsers = new ArrayList<Long>();
//                GetSessionPartnersReq req = new GetSessionPartnersReq();
//                req.setCallingUserId(userId);
//                String url = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT")
//                        + "/session/getSessionPartnersChat?"
//                        + WebUtils.INSTANCE.createQueryStringOfObject(req);
//                ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
//                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//                String jsonString = resp.getEntity(String.class);
//                logger.info(jsonString);
//
//                GetSessionPartnersRes response = new Gson().fromJson(jsonString, GetSessionPartnersRes.class);
//
//                ChattedUser sessionUser;
//                if (response != null && response.getPartners() != null) {
//
//                    // logger.info(output);
//                    for (UserBasicInfo userBasicInfo : response.getPartners()) {
//                        Long attendeeUserId = userBasicInfo.getUserId();
//                        if (!attendeeUserId.equals(userId) && !prevSessionUsers.contains(attendeeUserId)
//                                && !userIds.contains(attendeeUserId)) {
//
//                            sessionUser = new ChattedUser();
//                            prevSessionUsers.add(attendeeUserId);
//
//                            sessionUser.setFirstName(userBasicInfo.getFirstName());
//                            sessionUser.setRole(userBasicInfo.getRole());
//                            sessionUser.setFullName(userBasicInfo.getFullName());
//                            if (!StringUtils.isEmpty(userBasicInfo.getLastName())) {
//                                sessionUser.setLastName(userBasicInfo.getLastName());
//                            }
//                            if (!StringUtils.isEmpty(userBasicInfo.getProfilePicUrl())) {
//                                sessionUser.setProfilePicUrl(userBasicInfo.getProfilePicUrl());
//                            }
//
//                            query = new Query();
//                            query.addCriteria(Criteria.where("participants").in(attendeeUserId)
//                                    .andOperator(Criteria.where("participants").in(userId)));
//                            List<Channel> foundChannels = channelDAO.runQuery(query, Channel.class);
//
//                            if (foundChannels != null && foundChannels.size() == 1) {
//
//                                Channel ourChannel = foundChannels.get(0);
//                                sessionUser.setChannelName(ourChannel.getId());
//                                sessionUser.setChannelType(ourChannel.getChannelType());
//                                sessionUser.setUserId(attendeeUserId.toString());
//
//                                query = new Query();
//                                query.addCriteria(Criteria.where("channelName").is(ourChannel.getId())
//                                        .andOperator(Criteria.where("userId").is(userId)));
//                                List<ChannelUserInfo> channelSessionUserInfos = channelUserInfoDAO.runQuery(query,
//                                        ChannelUserInfo.class);
//                                if (!channelSessionUserInfos.isEmpty() && channelSessionUserInfos != null) {
//                                    ChannelUserInfo currentChannelUserInfo = channelSessionUserInfos.get(0);
//                                    sessionUser.setNewMessagesCount(currentChannelUserInfo.getNewMessagesCount());
//                                    sessionUser.setLastChattedTime(currentChannelUserInfo.getLastUpdated());
//                                }
//
//                            } else {
//                                sessionUser = createNewChannel(sessionUser, attendeeUserId, userId);
//                            }
//
//                            chattedUsers.add(sessionUser);
//                        }
//
//                    }
//                }
//            } catch (Exception ex) {
//                logger.error("Exception while parsing pastSessions", ex);
//            }
        // logger.info("Chatted Size:" + chattedUsers.size());;
        if (flag == 1) {
            for (int i = 0; i < chattedUsers.size(); i++) {
                if (chattedUsers.get(i).getFullName() == null) {
                    chattedUsers.remove(i);
                    i--;
                }
            }
        }

        GetLastChattedUsersResponse response = new GetLastChattedUsersResponse();
        response.setErrorCode(ErrorCode.SUCCESS);
        response.setErrorMessage("Fetched Successfully");
        response.setResult(chattedUsers);

        logger.info(chattedUsers.toString());
        logger.info("Exiting");

        return response;

        // if(!userIds.isEmpty()){
        // userBasicInfos = getUserBasicInfos(userIds);
        // userMap = new HashMap<String, UserBasicInfo>();
        // if (userBasicInfos != null && !userBasicInfos.isEmpty()) {
        // for (UserBasicInfo userBasicInfo : userBasicInfos) {
        // if (userBasicInfo.getUserId() != null) {
        // userMap.put(userBasicInfo.getUserId().toString(), userBasicInfo);
        // }
        // }
        // }
        // }
        // if(!channelUserInfos.isEmpty() && channelUserInfos != null){
        // currentChannelUserInfo = channelUserInfos.get(0);
        // chattedUser.setNewMessagesCount(currentChannelUserInfo.getNewMessagesCount());
        // chattedUser.setLastChattedTime(currentChannelUserInfo.getLastUpdated());
        // }
        // Query query = new Query();
        //
        // query.addCriteria(Criteria.where("participants").in(userId));
        // List<Channel> channels = channelDAO.runQuery(query, Channel.class);
        //
        // List<String> userIds = new ArrayList<String>();
        // ChattedUser chattedUser;
        // for(Channel channel: channels){
        //
        // Long otherUserId = getOtherMemberOfChannel(channel.getParticipants(),
        // userId);
        // if(otherUserId != null){
        // userIds.add(otherUserId.toString());
        // }
        // else
        // channels.remove(channel);
        // }
        //
        // List<UserBasicInfo> userBasicInfos = new ArrayList<UserBasicInfo>();
        // Map<String, UserBasicInfo> userMap = new HashMap<String,
        // UserBasicInfo>();
        // if(!userIds.isEmpty()){
        // userBasicInfos = getUserBasicInfos(userIds);
        // userMap = new HashMap<String, UserBasicInfo>();
        // if (userBasicInfos != null && !userBasicInfos.isEmpty()) {
        // for (UserBasicInfo userBasicInfo : userBasicInfos) {
        // if (userBasicInfo.getUserId() != null) {
        // userMap.put(userBasicInfo.getUserId().toString(), userBasicInfo);
        // }
        // }
        // }
        // }
        //
        // String currentUserId;
        // Channel currentChannel;
        // UserBasicInfo currentUser;
        // List<ChannelUserInfo> channelUserInfos;
        // ChannelUserInfo currentChannelUserInfo;
        // List<ChattedUser> chattedUsers = new ArrayList<ChattedUser>();
        //
        // for(int i=0; i< channels.size(); i++){
        //
        // chattedUser = new ChattedUser();
        // currentUserId = userIds.get(i);
        // currentUser = userMap.get(currentUserId);
        // currentChannel = channels.get(i);
        //
        // //set channel info for the user - lastChattedTime and
        // unreadMessageCount
        // query = new Query();
        // query.addCriteria(Criteria.where("channelName").is(currentChannel.getId())
        // .andOperator(Criteria.where("userId").is(Long.parseLong(currentUserId))));
        // channelUserInfos = channelUserInfoDAO.runQuery(query,
        // ChannelUserInfo.class);
        // if(!channelUserInfos.isEmpty() && channelUserInfos != null){
        // currentChannelUserInfo = channelUserInfos.get(0);
        // chattedUser.setNewMessagesCount(currentChannelUserInfo.getNewMessagesCount());
        // chattedUser.setLastChattedTime(currentChannelUserInfo.getLastUpdated());
        // }
        //
        // //set userbasicinfo for the user
        // chattedUser.setChannelName(currentChannel.getId());
        // chattedUser.setChannelType(currentChannel.getChannelType());
        // chattedUser.setUserId(currentUserId);
        // chattedUser.setFirstName(currentUser.getFirstName());
        // chattedUser.setFullName(currentUser.getFullName());
        // chattedUser.setLastName(currentUser.getLastName());
        // chattedUser.setProfilePicUrl(currentUser.getProfilePicUrl());
        // chattedUser.setRole(currentUser.getRole());
        //
        // //Add to result
        // chattedUsers.add(chattedUser);
        // }
        //
        // //sort based on last Chatted Time
        // Collections.sort(chattedUsers, new Comparator<ChattedUser>() {
        // public int compare(ChattedUser user1, ChattedUser user2) {
        // return user1.getLastChattedTime() > user2.getLastChattedTime() ?
        // -1 : user1.getLastChattedTime() == user1.getLastChattedTime() ? 0 :
        // 1;
        // }
        // });
    }

    // protected void updateElasticSearchTeacherStatus(String elasticSearchUrl,
    // String updateData) {
    // WebResource webResource = client.resource(elasticSearchUrl);
    // ClientResponse response = webResource.type("application/json").post(
    // ClientResponse.class, updateData);
    //
    // String output = response.getEntity(String.class);
    // if (response.getStatus() != 200 && response.getStatus() != 201) {
    // logger.info("sendRequestToElasticSearch"
    // + new RuntimeException(output));
    // throw new RuntimeException(output);
    // } else {
    // logger.info("sendRequestToElasticSearch : " + output);
    // }
    //
    // }
    Callback publishCallBack = new Callback() {

        @Override
        public void successCallback(String channel, Object response) {
            logger.info("Published successfully");
            logger.info(response.toString());
        }

        public void errorCallback(String channel, PubnubError error) {
            logger.info(error.toString());
        }
    };

    public BaseResponse initiateChatWithUser(Long fromUserId, Long toUserId) throws JSONException {

        logger.info("Entering fromUserId:" + fromUserId + " toUserId:" + toUserId);
        Query query = new Query();
        query.addCriteria(
                Criteria.where("participants").in(fromUserId).andOperator(Criteria.where("participants").in(toUserId)));
        List<Channel> channels = channelDAO.runQuery(query, Channel.class);

        ChattedUser chatUser = new ChattedUser();

        UserBasicInfo toUser = fosUtils.getUserBasicInfo(toUserId, false);
        if (toUser != null) {
            logger.info(toUser.toString());
            chatUser.setFirstName(toUser.getFirstName());
            chatUser.setFullName(toUser.getFullName());
            chatUser.setLastName(toUser.getLastName());
            chatUser.setProfilePicUrl(toUser.getProfilePicUrl());
            chatUser.setRole(toUser.getRole());
        }

        if (channels != null && channels.size() == 1) {

            Channel ourChannel = channels.get(0);
            chatUser.setChannelName(ourChannel.getId());
            chatUser.setChannelType(ourChannel.getChannelType());
            chatUser.setUserId(toUserId.toString());

            query = new Query();
            query.addCriteria(Criteria.where("channelName").is(ourChannel.getId())
                    .andOperator(Criteria.where("userId").is(fromUserId)));
            List<ChannelUserInfo> channelUserInfos = channelUserInfoDAO.runQuery(query, ChannelUserInfo.class);
            if (!channelUserInfos.isEmpty() && channelUserInfos != null) {
                ChannelUserInfo currentChannelUserInfo = channelUserInfos.get(0);
                chatUser.setNewMessagesCount(currentChannelUserInfo.getNewMessagesCount());
                chatUser.setLastChattedTime(currentChannelUserInfo.getLastUpdated());
            }

            // JSONObject jsonObject = new JSONObject();
            // jsonObject.put("message", "Join");
            // jsonObject.put("userId", fromUserId);
            // jsonObject.put("channelName", ourChannel.getId());
            // pubnub.publish("vedantu-"+toUserId, jsonObject, publishCallBack);
            InitiateChatResponse response = new InitiateChatResponse();
            response.setErrorCode(ErrorCode.SUCCESS);
            response.setErrorMessage("Found a channel entry. Fetched successfully");
            response.setResult(chatUser);
            logger.info("Exiting " + response.toString());

            return response;
        } else {
            logger.info("Creating new Channel");

            chatUser = createNewChannel(chatUser, toUserId, fromUserId);

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("type", "Join");
            jsonObject.put("userId", fromUserId);
            jsonObject.put("channelName", chatUser.getChannelName());
            pubnub.publish("vedantu-" + toUserId, jsonObject, publishCallBack);

            InitiateChatResponse response = new InitiateChatResponse();
            response.setErrorCode(ErrorCode.SUCCESS);
            response.setErrorMessage("Created and Added channel successfuly");
            response.setResult(chatUser);
            logger.info("Exiting " + response.toString());
            return response;
        }
    }

    public BaseResponse updateLastChattedTime(Long userId, String channelName) {

        Query query = new Query();
        query.addCriteria(Criteria.where("channelName").is(channelName));

        ChannelUserInfo currentChannelUserInfo;
        Long serverTime = System.currentTimeMillis();

        List<ChannelUserInfo> channelUserInfos = channelUserInfoDAO.runQuery(query, ChannelUserInfo.class);
        if (!channelUserInfos.isEmpty() && channelUserInfos != null) {
            for (int i = 0; i < channelUserInfos.size(); i++) {
                currentChannelUserInfo = channelUserInfos.get(i);
                if (currentChannelUserInfo.getUserId().equals(userId)) {
                    currentChannelUserInfo.setNewMessagesCount(0);
                }
                currentChannelUserInfo.setLastUpdated(serverTime);
                channelUserInfoDAO.create(currentChannelUserInfo);
            }
        } else {
            currentChannelUserInfo = new ChannelUserInfo();
            currentChannelUserInfo.setChannelName(channelName);
            currentChannelUserInfo.setChannelType(ChannelType.CHAT);
            currentChannelUserInfo.setLastUpdated(serverTime);
            currentChannelUserInfo.setUserId(userId);
            currentChannelUserInfo.setNewMessagesCount(0);
            channelUserInfoDAO.create(currentChannelUserInfo);
        }

        UpdateLastChattedTimeResponse response = new UpdateLastChattedTimeResponse();
        response.setErrorCode(ErrorCode.SUCCESS);
        response.setErrorMessage("Updated succesfully");
        response.setServerTime(serverTime);
        return response;

    }

    public BaseResponse updateUnreadMessageCount(Long userId, String channelName, int count) {

        logger.info("Entering");
        Query query = new Query();
        query.addCriteria(
                Criteria.where("channelName").is(channelName).andOperator(Criteria.where("userId").is(userId)));

        ChannelUserInfo currentChannelUserInfo;
        List<ChannelUserInfo> channelUserInfos = channelUserInfoDAO.runQuery(query, ChannelUserInfo.class);
        if (!channelUserInfos.isEmpty() && channelUserInfos != null) {
            currentChannelUserInfo = channelUserInfos.get(0);

            // set last chatted time to current time and unreadmessagecount to 0
            // currentChannelUserInfo.setLastChattedTime(System.currentTimeMillis());
            if (count != 0) {
                currentChannelUserInfo.setNewMessagesCount(currentChannelUserInfo.getNewMessagesCount() + count);
            } else {
                currentChannelUserInfo.setNewMessagesCount(0);
            }

            channelUserInfoDAO.create(currentChannelUserInfo);
        } else {
            currentChannelUserInfo = new ChannelUserInfo();
            currentChannelUserInfo.setChannelName(channelName);
            currentChannelUserInfo.setChannelType(ChannelType.CHAT);
            // currentChannelUserInfo.setLastChattedTime(System.currentTimeMillis());
            currentChannelUserInfo.setUserId(userId);
            currentChannelUserInfo.setNewMessagesCount(count);
            channelUserInfoDAO.create(currentChannelUserInfo);
        }

        BaseResponse response = new BaseResponse();
        response.setErrorCode(ErrorCode.SUCCESS);
        response.setErrorMessage("Updated succesfully");
        logger.info("Exiting");
        return response;
    }

    // public BaseResponse setStatusType(Long userId, Role role, StatusType
    // statusType) {
    //
    // logger.info("Entering userId:"+userId + "role:"+role +
    // "statusType:"+statusType);
    //
    // UserStatus currentUserStatus;
    // List<UserStatus> currentUserStatuses;
    //
    // if(!statusType.equals(StatusType.NONE) &&
    // !statusType.equals(StatusType.READY_TO_TEACH)){
    //
    // BaseResponse response = new BaseResponse();
    // response.setErrorCode(ErrorCode.SUCCESS);
    // response.setErrorMessage("Not Allowed to set OFFLINE statusType");
    // Query query = new Query();
    // query.addCriteria(Criteria.where("userId").is(userId));
    // currentUserStatuses = userStatusDAO.runQuery(query, UserStatus.class);
    // if(!currentUserStatuses.isEmpty() && currentUserStatuses != null){
    // currentUserStatus = currentUserStatuses.get(0);
    // userStatusDAO.deleteById(currentUserStatus.getId());
    // logger.info("Removed Entry for User in User Status");
    // }
    //
    // if(role.equals(Role.TEACHER)){
    //
    // String elasticSearchUrl = baseUrl + "vedantu" + "/" + "teachers" + "/"
    // + userId + "/_update";
    // String json;
    // json = "{\"onlineStatus\" : 0}";
    // logger.info("Updating online status of "+ userId + " to 0");
    // String tempQuery = "{\"doc\" : %s }";
    // String updateData = String.format(tempQuery, json);
    // updateElasticSearchTeacherStatus(elasticSearchUrl, updateData);
    // }
    // logger.info("Exiting");
    // return response;
    // }
    //
    // Query query = new Query();
    // query.addCriteria(Criteria.where("userId").is(userId));
    //
    // currentUserStatuses = userStatusDAO.runQuery(query, UserStatus.class);
    // if(!currentUserStatuses.isEmpty() && currentUserStatuses != null){
    // currentUserStatus = currentUserStatuses.get(0);
    //
    // currentUserStatus.setStatusType(statusType);
    // userStatusDAO.create(currentUserStatus);
    // }
    // else{
    // currentUserStatus = new UserStatus(userId, statusType);
    // userStatusDAO.create(currentUserStatus);
    // }
    //
    // if(role.equals(Role.TEACHER)){
    //
    // String elasticSearchUrl = baseUrl + "vedantu" + "/" + "teachers" + "/"
    // + userId + "/_update";
    // String json;
    // if(statusType.equals(StatusType.READY_TO_TEACH)){
    // json = "{\"onlineStatus\" : 1}";
    // logger.info("Updating online status of "+ userId + " to 1");
    // }
    // else{
    // json = "{\"onlineStatus\" : 0}";
    // logger.info("Updating online status of "+ userId + " to 0");
    // }
    // String tempQuery = "{\"doc\" : %s }";
    // String updateData = String.format(tempQuery, json);
    // updateElasticSearchTeacherStatus(elasticSearchUrl, updateData);
    // }
    //
    // BaseResponse response = new BaseResponse();
    // response.setErrorCode(ErrorCode.SUCCESS);
    // response.setErrorMessage("Updated succesfully");
    // return response;
    // }
    //
    // private class HereNowCallBack extends Callback {
    // private JSONObject result;
    // private CountDownLatch latch;
    //
    // HereNowCallBack(CountDownLatch latch) {
    // this.latch = latch;
    // }
    //
    // public JSONObject getResult() {
    // return result;
    // }
    //
    // @Override
    // public void successCallback(String channel, Object message) {
    // result = (JSONObject) message;
    //
    // if (this.latch != null) {
    // this.latch.countDown();
    // }
    // //logger.info(message);
    // }
    //
    // @Override
    // public void errorCallback(String channel, PubnubError error) {
    // System.out.println(error.getErrorString());
    //
    // if (this.latch != null) {
    // this.latch.countDown();
    // }
    // }
    // }
    // public BaseResponse getAllStatusTypes(List<Long> userIds) throws
    // JSONException {
    //
    // logger.info("Entering userIds:"+ userIds.toString());
    // //Pubnub pubnub = new Pubnub(pubNubPublishKey, pubNubSubscribeKey);
    //
    // UserStatus currentUserStatus;
    // Query query;
    // List<UserStatus> currentUserStatuses;
    // List<UserStatus> userStatuses = new ArrayList<UserStatus>();
    //
    //// OnlineUserStatus onlineUserStatus;
    //// HereNowCallBack hereNowCallBack ;
    //
    // for(Long userId: userIds){
    //
    // currentUserStatus = new UserStatus();
    //// onlineUserStatus = new OnlineUserStatus();
    //// final CountDownLatch latch = new CountDownLatch(1);
    //// hereNowCallBack = new HereNowCallBack(latch);
    ////
    //// pubnub.hereNow("vedantu-"+userId, hereNowCallBack);
    ////
    // query = new Query();
    // query.addCriteria(Criteria.where("userId").is(userId));
    ////
    //// try {
    //// latch.await(1, TimeUnit.SECONDS);
    //// } catch (InterruptedException e) {
    //// e.printStackTrace();
    //// }
    ////
    //// JSONObject response = hereNowCallBack.getResult();
    //// if(response != null){
    //// JSONArray uuids = response.getJSONArray("uuids");
    //// if(uuids.toString().contains("\""+ userId +"\"")){
    //// onlineUserStatus.setUserId(userId);
    //// onlineUserStatus.setOnlineStatus(OnlineStatus.ONLINE);
    //// }
    //// else{
    //// onlineUserStatus.setUserId(userId);
    //// onlineUserStatus.setOnlineStatus(OnlineStatus.OFFLINE);
    ////
    //// }
    //// }
    //// else{
    //// onlineUserStatus.setUserId(userId);
    //// onlineUserStatus.setOnlineStatus(OnlineStatus.OFFLINE);
    //// }
    ////
    // currentUserStatuses = userStatusDAO.runQuery(query, UserStatus.class);
    // if(!currentUserStatuses.isEmpty() && currentUserStatuses != null){
    // currentUserStatus = currentUserStatuses.get(0);
    // currentUserStatus.setStatusType(currentUserStatus.getStatusType());
    // }
    // else{
    // currentUserStatus.setUserId(userId);
    // currentUserStatus.setStatusType(StatusType.NONE);
    // }
    //
    // userStatuses.add(currentUserStatus);
    //
    // }
    //
    // GetAllStatusesResponse response = new GetAllStatusesResponse();
    // response.setErrorCode(ErrorCode.SUCCESS);
    // response.setErrorMessage("Fetched Successfully");
    // response.setResult(userStatuses);
    //
    // logger.info(userStatuses.toString());
    // logger.info("Exiting");
    // return response;
    // }
    //
    // public BaseResponse getMyStatusType(Long userId) {
    //
    // Query query = new Query();
    // query.addCriteria(Criteria.where("userId").is(userId));
    // List<UserStatus> currentUserStatuses;
    // UserStatus currentUserStatus = new UserStatus();
    //
    // currentUserStatuses = userStatusDAO.runQuery(query, UserStatus.class);
    // if(!currentUserStatuses.isEmpty() && currentUserStatuses != null){
    // currentUserStatus = currentUserStatuses.get(0);
    // if((currentUserStatus.getStatusType().equals(StatusType.READY_TO_TEACH))
    // &&
    // (System.currentTimeMillis() -currentUserStatus.getLastUpdated()) >70000){
    // currentUserStatus.setStatusType(StatusType.NONE);
    // userStatusDAO.create(currentUserStatus);
    //
    // String elasticSearchUrl = baseUrl + "vedantu" + "/" + "teachers" + "/"
    // + userId + "/_update";
    // String json;
    // json = "{\"onlineStatus\" : 0}";
    // logger.info("Updating online status of "+ userId + " to 0");
    // String tempQuery = "{\"doc\" : %s }";
    // String updateData = String.format(tempQuery, json);
    // updateElasticSearchTeacherStatus(elasticSearchUrl, updateData);
    // }
    // }
    // else{
    // currentUserStatus.setUserId(userId);
    // currentUserStatus.setStatusType(StatusType.NONE);
    // }
    //
    // GetMyStatusResponse response = new GetMyStatusResponse();
    // response.setErrorCode(ErrorCode.SUCCESS);
    // response.setErrorMessage("Fetched Successfully");
    // response.setResult(currentUserStatus);
    //
    // logger.info(currentUserStatus.toString());
    // logger.info("Exiting");
    // return response;
    //
    // }
    public Long getOtherMemberOfChannel(Long[] participants, Long myUserId) {
        if (participants.length > 1) {
            if (participants[0].equals(myUserId)) {
                return participants[1];
            } else {
                return participants[0];
            }
        }

        return null;
    }

    /*
	public List<UserBasicInfo> getUserBasicInfos(List<String> userIds) {

		logger.info("Entering userIds:" + userIds.toString());

		if (userIds == null || userIds.isEmpty()) {
			return null;
		}

		List<UserBasicInfo> users = new ArrayList<UserBasicInfo>();
		String url = userFetchUrl + "?type=user&" + "userIdsList="
				+ StringUtils.arrayToDelimitedString(userIds.toArray(), ",");
		logger.info(url);
		ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
		String output = response.getEntity(String.class);
		if (!StringUtils.isEmpty(output)) {
			try {
				Gson gson = new Gson();
				Type listType = new TypeToken<ArrayList<UserBasicInfo>>() {
				}.getType();
				List<UserBasicInfo> result = gson.fromJson(output, listType);
				if (result != null && !result.isEmpty()) {
					users.addAll(result);
				}
			} catch (Exception ex) {
				logger.error("Exception while parsing userBasicInfos", ex);
			}
		}

		logger.info("Exiting users:" + users.toString());
		return users;
	}
     */
    private ChattedUser createNewChannel(ChattedUser chatUser, Long toUserId, Long fromUserId) {

        logger.info("Adding new channel and channelInfos");
        Channel channel = new Channel();
        channel.setChannelType(ChannelType.CHAT);
        Long[] participants = new Long[2];
        participants[0] = toUserId;
        participants[1] = fromUserId;
        channel.setParticipants(participants);
        channelDAO.create(channel);
        if (channel.getParticipants()[0].equals(channel.getParticipants()[1])) {
            logger.error("Creating channel with same participants");
        }
        ChannelUserInfo channelUserInfo = new ChannelUserInfo();
        channelUserInfo.setChannelName(channel.getId());
        // channelUserInfo.setLastChattedTime(System.currentTimeMillis());
        channelUserInfo.setNewMessagesCount(0);
        channelUserInfo.setChannelType(channel.getChannelType());
        channelUserInfo.setUserId(toUserId);
        channelUserInfo.setLastUpdated(new Long(0));
        channelUserInfoDAO.create(channelUserInfo);
        channelUserInfo.setId(null);
        channelUserInfo.setUserId(fromUserId);
        channelUserInfo.setLastUpdated(new Long(0));
        channelUserInfoDAO.create(channelUserInfo);

        chatUser.setChannelName(channel.getId());
        chatUser.setChannelType(channel.getChannelType());
        chatUser.setLastChattedTime(channelUserInfo.getLastUpdated());
        chatUser.setNewMessagesCount(channelUserInfo.getNewMessagesCount());
        chatUser.setUserId(toUserId.toString());

        return chatUser;
    }

    public PlatformBasicResponse getChatChannelName(Long user1, Long user2) {
        PlatformBasicResponse res = new PlatformBasicResponse();
        Long[] parties = {user1, user2};
        Channel channel = channelDAO.findByParticipants(parties);
        if (channel != null) {
            res.setSuccess(true);
            res.setResponse(channel.getId());
        }
        return res;
    }

    ;
	public BaseResponse replyToOfflineMessage(OfflineMessageRequest request, ChatMessageType chatMessageType) throws JSONException {

        logger.info(request.toString());
        Long from = request.getFromUserId();
        Long to = request.getToUserId();
        String message = request.getMessage();

        Query query = new Query();
        query.addCriteria(Criteria.where("participants").in(from).andOperator(Criteria.where("participants").in(to)));
        List<Channel> foundChannels = channelDAO.runQuery(query, Channel.class);

        if (foundChannels != null && foundChannels.size() == 1) {

            Channel ourChannel = foundChannels.get(0);
            String channelName = ourChannel.getId();

            JSONObject msgPojo = new JSONObject();
            msgPojo.put("type", chatMessageType.toString());
            msgPojo.put("data", message);

            JSONObject pubnubmessage = new JSONObject();
            pubnubmessage.put("type", "Message");
            pubnubmessage.put("msg", msgPojo);
            pubnubmessage.put("targetUserId", to);
            pubnubmessage.put("senderId", from);
            pubnubmessage.put("senderTime", System.currentTimeMillis());
            pubnub.publish("chat:" + channelName, pubnubmessage, publishCallBack);

        }

        BaseResponse response = new BaseResponse();
        response.setErrorCode(ErrorCode.SUCCESS);
        response.setErrorMessage("Updated succesfully");
        return response;

    }

}
