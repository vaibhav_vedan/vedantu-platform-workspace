package com.vedantu.notification.managers;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.async.AsyncTaskName;
import com.vedantu.notification.entity.Email;
import com.vedantu.notification.entity.GupshupDeliveryReport;
import com.vedantu.notification.entity.ManualNotification;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.request.ChangeManualNotificationRequest;
import com.vedantu.notification.request.NotificationFilterReq;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.ManualNotificationRequest;
import com.vedantu.notification.response.ManualNotifDownloadDataRes;
import com.vedantu.notification.response.ManualNotificationDetailsRes;
import com.vedantu.notification.serializers.EmailDAO;
import com.vedantu.notification.serializers.GupshupDeliveryReportDAO;
import com.vedantu.notification.serializers.ManualNotificationDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.enums.CommunicationKind;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

@Service
public class ManualNotificationManager {

    @Autowired
    private ManualNotificationDAO manualNotificationDAO;

    @Autowired
    private EmailDAO emailDAO;

    @Autowired
    private GupshupDeliveryReportDAO gupshupDeliveryReportDAO;

    @Autowired
    private LogFactory logfactory;

    @Autowired
    private EmailManager emailManager;

    @Autowired
    private SMSManager smsManager;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(ManualNotificationManager.class);

    @Autowired
    private DozerBeanMapper mapper;

    public ManualNotification insertData(ManualNotificationRequest manualNotification) {
        ManualNotification notification = mapper.map(manualNotification, ManualNotification.class);
        if (manualNotification.getId() != null) {
            manualNotificationDAO.updateSuccessResult(manualNotification);
        } else {
            notification.setCreatedBy(Long.toString(manualNotification.getCallingUserId()));
            logger.info("\nThe user who created the manual notification data is " + manualNotification.getCallingUserId());
            if (notification.getScheduledTime() == 0) {
                notification.setScheduledTime(System.currentTimeMillis());
            }
            manualNotificationDAO.create(notification);
        }
        return notification;
    }

    public List<ManualNotificationDetailsRes> getAllManualNotificationData(NotificationFilterReq filterReq) {
        List<ManualNotification> manualNotifications = manualNotificationDAO.getAllData(filterReq);
        List<ManualNotificationDetailsRes> notificationDetailsRes = new ArrayList<>();
        Map<String, ManualNotification> manualNotificationMap = new HashMap<>();
        List<String> emailNotifIds = new ArrayList<>();
        List<String> smsNotifIds = new ArrayList<>();
        manualNotifications.forEach(manualNotification -> {
            manualNotificationMap.put(manualNotification.getId(), manualNotification);
            if (CommunicationKind.EMAIL.equals(manualNotification.getCommunicationKind())) {
                emailNotifIds.add(manualNotification.getId());
            } else if (CommunicationKind.SMS.equals(manualNotification.getCommunicationKind())) {
                smsNotifIds.add(manualNotification.getId());
            }
        });
        List<Email> emailNotif = emailDAO.getDeliveryReportForManualNotification(emailNotifIds);
        List<GupshupDeliveryReport> smsNotif = gupshupDeliveryReportDAO.getDeliveryReportForManualNotification(smsNotifIds);
        Map<String, Integer> sentEmailCount = new HashMap<>();
        Map<String, Integer> openedEmailCount = new HashMap<>();
        Map<String, Integer> smsDeliveredCount = new HashMap<>();
        emailNotif.forEach(email -> {
            String manualNotifId = email.getManualNotificationId();
            if (sentEmailCount.containsKey(manualNotifId)) {
                sentEmailCount.put(manualNotifId, sentEmailCount.get(manualNotifId) + 1);
            } else {
                sentEmailCount.put(manualNotifId, 1);
            }
            if (email.isOpened()) {
                if (openedEmailCount.containsKey(manualNotifId)) {
                    openedEmailCount.put(manualNotifId, openedEmailCount.get(manualNotifId) + 1);
                } else {
                    openedEmailCount.put(manualNotifId, 1);
                }
            }
        });
        smsNotif.forEach(sms -> {
            String manualNotifId = sms.getManualNotificationId();
            if (smsDeliveredCount.containsKey(manualNotifId)) {
                smsDeliveredCount.put(manualNotifId, smsDeliveredCount.get(manualNotifId) + 1);
            } else {
                smsDeliveredCount.put(manualNotifId, 1);
            }
        });
        manualNotifications.forEach(manualNotification -> {
            ManualNotificationDetailsRes detailsRes = mapper.map(manualNotification, ManualNotificationDetailsRes.class);
            if (detailsRes.getAllUserIds() != null) {
                if (manualNotification.getCreatedBy() == null || "SYSTEM".equals(manualNotification.getCreatedBy())) {
                    detailsRes.setSentBy("SYSTEM");
                } else {
                    detailsRes.setSentBy(fosUtils.getUserBasicInfo(manualNotification.getCreatedBy(), true).getEmail());
                }
                double allUsers = detailsRes.getAllUserIds().size();
                if (ArrayUtils.isNotEmpty(detailsRes.getEmailIds())) {
                    allUsers += detailsRes.getEmailIds().size();
                }
                detailsRes.setSentCount((int) allUsers);
                if (CommunicationKind.EMAIL.equals(manualNotification.getCommunicationKind())) {
                    double sentUsers = sentEmailCount.containsKey(detailsRes.getId()) ? sentEmailCount.get(detailsRes.getId()) : 0;
                    double openedEmailUsers = detailsRes.getId() != null && openedEmailCount.containsKey(detailsRes.getId()) ? openedEmailCount.get(detailsRes.getId()) : 0;
                    detailsRes.setDeliveryRate((sentUsers / allUsers) * 100);
                    detailsRes.setOpenRate((openedEmailUsers / allUsers) * 100);
                } else if (CommunicationKind.SMS.equals(manualNotification.getCommunicationKind())) {
                    double sentUsers = detailsRes.getId() != null && smsDeliveredCount.containsKey(detailsRes.getId()) ? smsDeliveredCount.get(detailsRes.getId()) : 0;
                    detailsRes.setDeliveryRate((sentUsers / allUsers) * 100);
                }
                notificationDetailsRes.add(detailsRes);

            }
        });

        return notificationDetailsRes;
    }

    public List<ManualNotification> sendScheduledEmailOrSMS() {
        List<ManualNotification> manualNotifications = manualNotificationDAO.getScheduledManualNotification();
        logger.info("\nNeed to communicate " + manualNotifications.size() + " different communication\n");
        manualNotifications.forEach(manualNotification -> {
            ManualNotificationRequest request = mapper.map(manualNotification, ManualNotificationRequest.class);
            List<UserBasicInfo> users = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(manualNotification.getAllUserIds())) {
                users = fosUtils.getUserBasicInfos(manualNotification.getAllUserIds(), true);
                logger.info("\nWe need to communicate to " + users.size() + " number of users\n");
            }
            if (CommunicationKind.EMAIL.equals(request.getCommunicationKind())) {
                sendEmail(manualNotification, users);
            } else {
                sendSMS(manualNotification, users);
            }
            manualNotificationDAO.updateSuccessResult(mapper.map(manualNotification, ManualNotificationRequest.class));
        });
        return manualNotifications;
    }

    private void sendEmail(ManualNotification manualNotification, List<UserBasicInfo> users) {
        List<InternetAddress> emailTo = new ArrayList<>();
        StringBuilder error = new StringBuilder();

        if (ArrayUtils.isNotEmpty(users)) {
            users.forEach(user -> {
                try {
                    if (null != user.getEmail()) {
                        InternetAddress internetAddress = new InternetAddress();
                        internetAddress.setAddress(user.getEmail());
                        internetAddress.setPersonal(user.getFullName());
                        emailTo.add(internetAddress);
                    } else {
                        error.append("Email not found for userId ").append(user.getUserId());
                    }
                } catch (UnsupportedEncodingException ex) {
                    error.append("Error occcured for userId ").append(user.getUserId());
                } finally {
                    if (error.length() > 0) {
                        logger.info(error);
                    } else {
                        logger.info("No error is found.");
                    }
                }
            });
        }

        if (ArrayUtils.isNotEmpty(manualNotification.getEmailIds())) {
            List<String> emailIds = manualNotification.getEmailIds();
            if (ArrayUtils.isNotEmpty(emailIds)) {
                emailIds.forEach(emailId -> {
                    try {
                        emailTo.add(new InternetAddress(emailId));
                    } catch (AddressException e) {
                        logger.error("Error in storing standalone email address for scheduling");
                    }
                });
            }
        }

        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setBody(manualNotification.getBody());
        emailRequest.setSubject(manualNotification.getSubject());
        emailRequest.setType(CommunicationType.MANUAL_NOTIFICATION);
        emailRequest.setClickTrackersEnabled(true);
        emailRequest.setManualNotificationId(manualNotification.getId());
        emailRequest.setBodyScopes(new HashMap<>());

        Map<String, Object> payload = new HashMap<>();
        payload.put("emailRequest", emailRequest);
        payload.put("emailTo", emailTo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_BULK_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        logger.info("Hopefully email is sent successfully");
    }

    private void sendSMS(ManualNotification manualNotification, List<UserBasicInfo> users) {
        ArrayList<String> toSMS = new ArrayList<>();
        StringBuilder error = new StringBuilder();
        users.forEach(user -> toSMS.add("+" + user.getPhoneCode() + user.getContactNumber()));
        BulkTextSMSRequest bulkTextSMSRequest = new BulkTextSMSRequest();
        bulkTextSMSRequest.setBody(manualNotification.getBody());
        bulkTextSMSRequest.setType(CommunicationType.MANUAL_NOTIFICATION);
        bulkTextSMSRequest.setManualNotificationId(manualNotification.getId());
        List<List<String>> smsGroups = Lists.partition(toSMS, 99);
        smsGroups.forEach(smsGroup -> {
            bulkTextSMSRequest.setTo(new ArrayList<>(smsGroup));
            Map<String, Object> payload = new HashMap<>();
            payload.put("bulkTextSMSRequest", bulkTextSMSRequest);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SEND_BULK_SMS, payload);
            asyncTaskFactory.executeTask(params);
        });
        logger.info("\nHopefully SMS is sent successfully");
    }

    public List<ManualNotification> discardScheduledEmailOrSMS(ChangeManualNotificationRequest discardList) {
        return manualNotificationDAO.discardScheduled(discardList);
    }

    public List<ManualNotification> rescheduleEmailOrSMS(ChangeManualNotificationRequest rescheduleList) {
        return manualNotificationDAO.reschedule(rescheduleList);
    }

    public List<ManualNotification> duplicateCommunication(String id) {
        return manualNotificationDAO.getNotificationByIds(id);
    }

    public List<ManualNotifDownloadDataRes> downloadCommunication(String id) throws BadRequestException {
        ManualNotification notification = manualNotificationDAO.getById(id);
        if (notification == null) {
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, id + " not found in ManualNotification table", Level.INFO);
        }
        List<ManualNotifDownloadDataRes> downloadData = new ArrayList<>();

        if (CommunicationKind.EMAIL.equals(notification.getCommunicationKind())) {
            List<Email> emails = emailDAO.getDeliveryReportForManualNotificationById(notification.getId());
            emails.forEach(email -> {
                ManualNotifDownloadDataRes data = new ManualNotifDownloadDataRes();
                Address to = email.getTo();
                if (Boolean.TRUE.equals(email.isOpened())) {
                    data.setOpenedTime(getFormattedDate(email.getLastUpdated()));
                }
                data.setSentTime(getFormattedDate(email.getCreationTime()));
                logger.info("The downloaded data is " + new Gson().toJson(data));
                String toString = to.toString();
                String emailId = toString.substring(toString.indexOf('<') + 1, toString.indexOf('>'));
                logger.info("The address the mail is sent is " + emailId);
                UserBasicInfo info = fosUtils.getUserBasicInfoFromEmail(emailId, true);
                data.setStudentId(Long.toString(info.getUserId()));
                downloadData.add(data);
            });
        } else if (CommunicationKind.SMS.equals(notification.getCommunicationKind())) {
            List<GupshupDeliveryReport> smsList = gupshupDeliveryReportDAO.getDeliveryReportForManualNotification(Arrays.asList(notification.getId()));
            ManualNotification smsNotif = manualNotificationDAO.getById(notification.getId());

            List<String> userIds = smsNotif.getAllUserIds();
            Set<String> uniqueUserIds = new HashSet<>();
            userIds.forEach(userId -> uniqueUserIds.add(userId));
            List<String> uniqueUsers = uniqueUserIds.stream().collect(Collectors.toList());
            List<UserBasicInfo> users = fosUtils.getUserBasicInfos(uniqueUsers, true);
            Map<String, Long> phoneUserMap = new HashMap<>();
            users.forEach(user -> {
                phoneUserMap.put("+" + user.getPhoneCode() + user.getContactNumber(), user.getUserId());
                logger.info("+" + user.getPhoneCode() + user.getContactNumber());
            });
            smsList.forEach(sms -> {
                ManualNotifDownloadDataRes data = new ManualNotifDownloadDataRes();
                logger.info(sms.getPhoneNo());
                data.setStudentId(Long.toString(phoneUserMap.get(sms.getPhoneNo())));
                data.setSentTime(getFormattedDate(sms.getCreationTime()));
                downloadData.add(data);
            });
        } else {
            List<String> allUserIds = notification.getAllUserIds();
            allUserIds.forEach(userId -> {
                ManualNotifDownloadDataRes data = new ManualNotifDownloadDataRes();
                data.setStudentId(userId);
                data.setSentTime(getFormattedDate(notification.getScheduledTime()));
                downloadData.add(data);
            });
        }
        return downloadData;
    }

    private String getFormattedDate(Long epoch) {
        Date date = new Date(epoch);
        DateFormat format = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
        String formattedDate = format.format(date);
        return formattedDate;
    }
}
