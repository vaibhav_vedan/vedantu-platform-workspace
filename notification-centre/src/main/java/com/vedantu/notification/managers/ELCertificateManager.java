package com.vedantu.notification.managers;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@Service
public class ELCertificateManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ELCertificateManager.class);

	private static final String FILE_DEMO_EL_SESSION_CERTIFICATE = ConfigUtils.INSTANCE
			.getStringValue("vedantu.DEMO_EL_SESSION_CERTIFICATE.html");
	public static final String ATTACHMENT_FILE_NAME = "Certificate.pdf";

	private String getHTMLContent(Map<String, Object> scopes, String fileName) throws VException, IOException {

		Writer writer = new StringWriter();
		MustacheFactory mf = new DefaultMustacheFactory();
		InputStream is = null;
		final String confDir = "EmailTemplates";

		String propertiesFilePath = confDir + java.io.File.separator + fileName;

		logger.info("propertiesFilePath : " + propertiesFilePath);
		try {
			is = ConfigUtils.class.getClassLoader().getResourceAsStream(propertiesFilePath);
			Mustache mustache = mf.compile(new InputStreamReader(is, "UTF-8"), "emailContent");
			mustache.execute(writer, scopes);
			writer.flush();
		} catch (Exception e) {
			logger.info("error in mustache:" + e.getMessage());
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
		} finally {
			if (is != null) {
				is.close();
			}
		}
		return writer.toString();
	}

	private File createStudentCertificatePdf(Map<String, Object> bodyScopes, String fileName)
			throws VException, IOException {

		File pdfFile = null;
		try {
			OutputStream out = new FileOutputStream(fileName);
			ITextRenderer renderer = new ITextRenderer();
			String content = getHTMLContent(bodyScopes, FILE_DEMO_EL_SESSION_CERTIFICATE);
			logger.info("Content HTML : "+content);
			renderer.setDocumentFromString(content);
			renderer.layout();
			renderer.createPDF(out);
			out.close();
			pdfFile = new File(fileName);
			return pdfFile;
		} catch (Exception e) {
			logger.info("error in " + e);
			throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
		}
	}

	public File getCertificateFile(Map<String, Object> bodyScopes, String fileName) {
		try {
			if (bodyScopes.get("certificateEligibility") != null) {
				logger.info("attachCertificate Entered : ");
				Boolean certificateEligibility = (Boolean) bodyScopes.get("certificateEligibility");
				if (certificateEligibility) {
					return createStudentCertificatePdf(bodyScopes, fileName);
				}
			}
		} catch (Exception ex) {
			logger.error("Email Attachment Early Learning Certificate : " + ex);
		}

		return null;
	}
}
