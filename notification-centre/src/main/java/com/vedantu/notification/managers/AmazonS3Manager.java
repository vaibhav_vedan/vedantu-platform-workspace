package com.vedantu.notification.managers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Map;
import java.util.UUID;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.cmds.pojo.CMDSImageDetails;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import javax.annotation.PreDestroy;

@Service
public class AmazonS3Manager {

    @Autowired
    private AmazonClient amazonClient;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonS3Manager.class);

    public boolean uploadImage(String key, File file, Map<String, String> metadata, UploadTarget uploadTarget) throws FileNotFoundException, BadRequestException {
    	if (uploadTarget == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"upload target is null");
        }
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String s3location = bucket + java.io.File.separator + env + java.io.File.separator + uploadTarget.getFolderPath();
        return amazonClient.uploadImage(key, file, metadata, s3location);
    }

    public String getImageUrl(String fileName, String bucketName) {
        logger.info("Request : " + fileName);
        String url = amazonClient.getImageUrl(fileName, bucketName);
        logger.info("Response : " + url);
        return url;
    }

    public String getImageUrl(CMDSImageDetails imageDetails) throws BadRequestException {
        UploadTarget uploadTarget = imageDetails.getUploadTarget();
        if (uploadTarget == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"upload target is null");
        }
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + imageDetails.getFileName();
        return amazonClient.getImageUrl(fullKey, bucket);
    }

    public String getImageUrl(String fileName, UploadTarget uploadTarget) throws BadRequestException{
    	if (uploadTarget == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"upload target is null");
        }
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + fileName;
        return amazonClient.getImageUrl(fullKey, bucket);
    }

    public String getPresignedUrl(UploadTarget uploadTarget, String contentType) {
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + UUID.randomUUID();
        return amazonClient.getPreSignedUrl(bucket, fullKey, contentType);
    }
    
    public InputStream getObject(String fileName, UploadTarget uploadTarget) {
        String bucket = ConfigUtils.INSTANCE.getStringValue("aws.bucket");
        String env = ConfigUtils.INSTANCE.getEnvironmentSlug();
        String fullKey = env + java.io.File.separator + uploadTarget.getFolderPath() + java.io.File.separator + fileName;        
        return amazonClient.getObject(fullKey, bucket);
    }    
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (amazonClient != null) {
                amazonClient.cleanUp();
            }
        } catch (Exception e) {
            logger.error("Error in cleaning up amazonClient ", e);
        }
    }       
}
