package com.vedantu.notification.listeners;

import com.amazonaws.services.dynamodbv2.xspec.B;
import com.google.common.base.StandardSystemProperty;
import com.google.common.util.concurrent.RateLimiter;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.notification.entity.UserCommunicationCount;
import com.vedantu.notification.entity.VQuizNotification;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.managers.AwsSQSManager;
import com.vedantu.notification.managers.EmailManager;
import com.vedantu.notification.managers.SMSManager;
import com.vedantu.notification.managers.VQuizNotificationManager;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.notification.serializers.RedisDAO;
import com.vedantu.notification.serializers.UserCommunicationCountDAO;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.pojo.QueueMessagePojo;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.*;

import jdk.nashorn.internal.ir.IdentNode;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;
import javax.mail.internet.InternetAddress;

/**
 * Created by somil on 01/09/17.
 */
@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    @Autowired
    EmailManager emailManager;

    @Autowired
    SMSManager smsManager;

    @Autowired
    private VQuizNotificationManager vQuizNotificationManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    AwsSQSManager awsSQSManager;

    @Autowired
    StatsdClient statsdClient;

    public final RateLimiter limiter = RateLimiter.create(5);

    private final Gson gson = new Gson();

    private final Integer TWO_DAYS_IN_SECONDS = 172800;
    private final Long ONE_HOUR_IN_MILLIS = 3600000L;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private UserCommunicationCountDAO userCommunicationCountDAO;

    private static final List<CommunicationType> avoidEmailFilterList = new ArrayList<>(Arrays.asList(CommunicationType.OTF_SESSION_FEEDBACK, CommunicationType.WAVE_LEADERBOARD_RECOGNITION,
            CommunicationType.WAVE_STREAK_RECOGNITION));

    @Override
    public void onMessage(Message message) {

        TextMessage textMessage = (TextMessage) message;
        logger.info(textMessage);
        try {
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            Long startTime = System.currentTimeMillis();
            SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
            handleMessages(sqsMessageType, textMessage.getText(), textMessage.getJMSMessageID(), textMessage.getJMSTimestamp());
            message.acknowledge();
            if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                logger.info("Record execution type : " + className + " message type" + sqsMessageType.name());
                if(null != sqsMessageType) {
                    statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                    statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                }
            }

        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }
    }

    public void handleMessages(SQSMessageType sqsMessageType, String text, String messageId, Long jmsTimestamp) throws Exception {
        QueueMessagePojo queueMessagePojo = new QueueMessagePojo(sqsMessageType, text);

        logger.info("handleMessages is called at time : " + System.currentTimeMillis());
        logger.info("jmsTimestamp " + jmsTimestamp);



        switch (sqsMessageType) {
            case SEND_EMAIL:
            case OTF_SESSION_SUMMARY_EMAIL:
                EmailRequest emailRequest = new Gson().fromJson(queueMessagePojo.getBody(), EmailRequest.class);
                emailRequest.setSqsMessageId(messageId);
                logger.info("EMAIL SQSMESSAGEID : " + messageId);
                logger.info("SENDING EMAIL" + emailRequest);
                emailManager.sendEmail(emailRequest);
                break;
            case SEND_SMS:
                TextSMSRequest textSMSRequest = new Gson().fromJson(queueMessagePojo.getBody(), TextSMSRequest.class);          
                textSMSRequest.setSqsMessageId(messageId);
                logger.info("SMS SQSMESSAGEID : " + messageId);
                smsManager.sendSms(textSMSRequest);
                break;
            case SEND_BULK_SMS:
                JsonObject sendBulkSmsPayload = gson.fromJson(text, JsonObject.class);
                Type bulkTextSMSRequestType = new TypeToken<BulkTextSMSRequest>(){}.getType();
                BulkTextSMSRequest bulkTextSMSRequest = gson.fromJson(sendBulkSmsPayload.get("bulkTextSMSRequest"), bulkTextSMSRequestType);
                smsManager.sendBulkSms(bulkTextSMSRequest);
                break;
            case SEND_BULK_EMAIL:
                Type emailType = new TypeToken<EmailRequest>(){}.getType();
                EmailRequest bulkTextEmailRequest = gson.fromJson(text, emailType);
                emailManager.sendBulkEmail(bulkTextEmailRequest);
                break;
            case VQUIZ_REMINDER:
                Type notificationType = new TypeToken<List<VQuizNotification>>(){}.getType();
                List<VQuizNotification> vQuizNotificationList = gson.fromJson(text, notificationType);
                vQuizNotificationManager.sendVquizNotification(vQuizNotificationList);
                break;
            default:
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "UNKNOWN MESSAGE TYPE FOR SQS - " + sqsMessageType);


        }
    }
}
