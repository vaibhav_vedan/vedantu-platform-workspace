/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.notification.managers.FirebaseUtilsManager;
import com.vedantu.notification.request.RegisterMobileRegTokenReq;
import com.vedantu.notification.requests.PushNotificationReq;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/firebaseutils")
public class FirebaseUtilsController {

    @Autowired
    private FirebaseUtilsManager firebaseUtilsManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @RequestMapping(value = "/registerMobileRegToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse registerMobileRegToken(@RequestBody RegisterMobileRegTokenReq req) throws BadRequestException {
        req.verify();
        return firebaseUtilsManager.registerMobileRegToken(req);
    }

    @RequestMapping(value = "/removeMobileRegToken", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse removeMobileRegToken(@RequestBody RegisterMobileRegTokenReq req) throws BadRequestException {
        req.verify();
        return firebaseUtilsManager.removeMobileRegToken(req);
    }

    @RequestMapping(value = "/pushNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse pushNotification(@RequestBody PushNotificationReq req) throws Exception {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return firebaseUtilsManager.pushNotification(req);
    }

}
