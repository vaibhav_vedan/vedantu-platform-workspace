package com.vedantu.notification.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.lms.cmds.enums.UploadTarget;
import com.vedantu.notification.managers.AmazonS3Manager;
import com.vedantu.util.PlatformBasicResponse;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/contentUpload")
public class NotificationContentUploadController {

	@Autowired
    private AmazonS3Manager amazonS3Manager;

    @RequestMapping(value = "/getpresignedurl", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Generate a PreSigned url for AWS operation", notes = "String getPreSignedUrl()")
    public PlatformBasicResponse getPreSignedUrl(
            @RequestParam(value = "uploadTarget", required = true) UploadTarget uploadTarget,
            @RequestParam(value = "contentType") String contentType) throws Exception {

        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            String preSignedUrl = amazonS3Manager.getPresignedUrl(uploadTarget, contentType);
            platformBasicResponse.setResponse(preSignedUrl);
            platformBasicResponse.setSuccess(true);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }

    @RequestMapping(value = "/getsignedurl", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @ApiOperation(value = "Generate a signedurl url for AWS operation", notes = "String getsignedurl()")
    public PlatformBasicResponse getsignedurl(
            @RequestParam(value = "uploadTarget", required = true) UploadTarget uploadTarget,
            @RequestParam(value = "fileName") String fileName) throws Exception {

        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();

        try {
            String preSignedUrl = amazonS3Manager.getImageUrl(fileName, uploadTarget);
            platformBasicResponse.setResponse(preSignedUrl);
            platformBasicResponse.setSuccess(true);
        } catch (Exception ex) {
            platformBasicResponse.setSuccess(false);
        }

        return platformBasicResponse;
    }
}
