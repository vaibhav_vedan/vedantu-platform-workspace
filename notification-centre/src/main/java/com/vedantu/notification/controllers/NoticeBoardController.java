package com.vedantu.notification.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.NoticeBoard;
import com.vedantu.notification.managers.NoticeBoardManager;
import com.vedantu.notification.request.ChangeManualNotificationRequest;
import com.vedantu.notification.request.NotificationFilterReq;
import com.vedantu.notification.responses.NoticeBoardRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.CommunicationDataReq;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/noticeBoard")
public class NoticeBoardController {

    @Autowired
    private NoticeBoardManager noticeBoardManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(NoticeBoardController.class);

    @RequestMapping(value = "/populateNoticeBoardData", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<NoticeBoardRes> populateNoticeBoardData(@RequestBody CommunicationDataReq req) throws VException {
//        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return noticeBoardManager.populateNoticeBoardData(req);
    }

    @RequestMapping(value = "/getAllNotices", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<NoticeBoardRes> getAllNotices(@RequestBody NotificationFilterReq filterReq) throws VException {
        sessionUtils.checkIfAllowed(filterReq.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return noticeBoardManager.getAllNotices(filterReq);
    }

    @RequestMapping(value = "/getNoticeById/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public NoticeBoardRes getNotceById(@PathVariable("id")String id) throws BadRequestException {
        return noticeBoardManager.getNoticeById(id);
    }

    @RequestMapping(value = "/updateNoticeBoardDB",method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateNoticeBoardDB(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
                                        @RequestBody String request){
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
        } else if (messgaetype.equals("Notification")) {
            noticeBoardManager.updateNoticeBoardDB();
        }
    }

    @RequestMapping(value = "/discardNotices",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<NoticeBoard> discardNotices(@RequestBody ChangeManualNotificationRequest discardList) throws VException {
            sessionUtils.checkIfAllowed(discardList.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        if(discardList.getChangeList()==null){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Some of the input fields are missing", Level.INFO);
        }
        return noticeBoardManager.discardNotices(discardList);
    }

    @RequestMapping(value = "/rescheduleNotices",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<NoticeBoard> rescheduleNotices(@RequestBody ChangeManualNotificationRequest rescheduleList) throws VException {
        rescheduleList.verify();
            sessionUtils.checkIfAllowed(rescheduleList.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return noticeBoardManager.rescheduleNotices(rescheduleList);
    }

    @RequestMapping(value = "/getAllNoticeBoardData",method = RequestMethod.GET,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<NoticeBoardRes> getAllNoticeBoardData(@RequestBody NotificationFilterReq filterReq) throws VException {
        filterReq.verify();
        sessionUtils.checkIfAllowed(filterReq.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return noticeBoardManager.getAllNoticeBoardData(filterReq);
    }

    @RequestMapping(value = "/getNoticeByUserId/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<NoticeBoardRes> getNoticeByUserId(@PathVariable("userId")String userId) throws BadRequestException {

        logger.info("\nInside controllers of getting notice by user id with user id "+userId);
        return noticeBoardManager.getNoticeUserId(userId);
    }


}
