package com.vedantu.notification.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.VException;
import com.vedantu.notification.async.AsyncTaskName;
import com.vedantu.notification.managers.VQuizNotificationManager;
import com.vedantu.notification.request.VQuizNotificationReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.io.IOException;

/**
 * @author MNPK
 */

@EnableAsync
@RestController
@RequestMapping("/vquiz")
public class VQuizNotificationController {

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(VQuizNotificationController.class);

    @Autowired
    private VQuizNotificationManager vQuizNotificationManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @RequestMapping(value = "/remindMe", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse vQuizRemindMe(@RequestBody VQuizNotificationReq req) throws VException {
        logger.info("VQuizRemindMeReq : " + req);
//        sessionUtils.checkIfAllowed(req.getCallingUserId(), null, Boolean.FALSE);
        req.verify();
        return vQuizNotificationManager.vQuizRemindMe(req);
    }

    @RequestMapping(value = "/sendVQuizRemindMeNotification", method = RequestMethod.POST, consumes = org.springframework.http.MediaType.TEXT_PLAIN_VALUE, produces = org.springframework.http.MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendVQuizRemindMeNotification(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        logger.info(subscriptionRequest.toString());
        if ("SubscriptionConfirmation".equals(messageType)) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if ("Notification".equals(messageType)) {
            logger.info("Notification received - SNS - sendVQuizRemindMeNotification");
//            vQuizNotificationManager.sendVQuizRemindMeNotification();
            try {
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.VQUIZ_NOTIFICATION_REMINDER, null);
                asyncTaskFactory.executeTask(params);
            } catch (Exception e) {
                logger.error(e.getMessage());
            }
        }
    }
}

