package com.vedantu.notification.controllers;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.notification.entity.MobileNotificationData;
import com.vedantu.notification.entity.Notification;
import com.vedantu.notification.managers.NotificationManager;
import com.vedantu.notification.request.MobileNotificationDataRequest;
import com.vedantu.notification.requests.RegisterUserDeviceRequest;
import com.vedantu.notification.requests.CreateNotificationRequest;
import com.vedantu.notification.requests.GetNotificationsReq;
import com.vedantu.notification.requests.MarkSeenByNotificationIdReq;
import com.vedantu.notification.requests.MarkSeenReq;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.responses.BasicRes;
import com.vedantu.notification.responses.GetNotificationsRes;
import com.vedantu.notification.responses.MarkSeenRes;

import java.io.IOException;

@RestController
@RequestMapping("/notification")
/*
 * Controller for Mobile API endpoint
 */
public class NotificationController {
	
	@Autowired
	private NotificationManager notificationManager;

	@Autowired
	private HttpSessionUtils sessionUtils;

	private final ObjectMapper objectMapper = new ObjectMapper().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
	
	/*
	 * API to send an a mobile notification.
	 * @param SendNotificationRequest input JSON POST object
	 * @return json A json with send status
	 */
	@RequestMapping(value = "/sendNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse sendNotification(@RequestBody Notification notifRequest) throws BadRequestException
	{

		notifRequest.verify();
		return notificationManager.sendNotification(notifRequest);
	}
        
        
        @RequestMapping(value = "/createNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse createNotification(@RequestBody CreateNotificationRequest notifRequest) throws BadRequestException
	{

		notifRequest.verify();
		return notificationManager.createNotification(notifRequest);
	}
        
	
	/*
	 * API to save mobile notification data
	 * @param MobileNotificationData input JSON POST object
	 * @return json A json with SMS Id received after successful delivery
	 */
	@RequestMapping(value = "/saveMobileNotificationData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse saveMobileNotificationData(@RequestBody MobileNotificationDataRequest mobileNotifData) throws BadRequestException
	{

		mobileNotifData.verify();
		return notificationManager.saveMobileNotificationData(mobileNotifData);
	}

	@RequestMapping(value = "/v2/saveMobileNotificationData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String saveMobileNotificationData_v2(@RequestBody MobileNotificationDataRequest request) throws VException, IOException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		request.verify();
		BaseResponse response = notificationManager.saveMobileNotificationData(request);
		return objectMapper.writeValueAsString(response);
	}
	
	/*
	 * API to get mobile notification data
	 * @param MobileNotificationData input JSON POST object
	 * @return json A json with SMS Id received after successful delivery
	 */
	@RequestMapping(value = "/getMobileNotificationData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse getMobileNotificationData(@RequestBody MobileNotificationData mobileNotifData)
	{

		//mobileNotifData.verify();
		return notificationManager.getMobileNotificationData(mobileNotifData);
	}
	
	/*
	 * API to register a user mobile device
	 * @param MobileNotification input JSON POST object
	 * @return json A json with status
	 */
	@RequestMapping(value = "/addMobileRegistrationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse registerUserDevice(@RequestBody RegisterUserDeviceRequest userDeviceReq) throws BadRequestException
	{

		userDeviceReq.verify();
		return notificationManager.registerUserDevice(userDeviceReq);
	}

	@RequestMapping(value = "/v2/addMobileRegistrationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String addMobileRegistrationId(@RequestBody RegisterUserDeviceRequest request) throws VException, IOException {
		sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.FALSE);
		request.verify();
		BaseResponse response = notificationManager.registerUserDevice(request);
		return objectMapper.writeValueAsString(response);
	}
	
	/*
	 * API to register a user mobile device
	 * @param MobileNotification input JSON POST object
	 * @return json A json with status
	 */
	@RequestMapping(value = "/removeMobileRegistrationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse removeUserDevice(@RequestBody RegisterUserDeviceRequest userDeviceReq) throws BadRequestException
	{

		userDeviceReq.verify();
		return notificationManager.removeUserDevice(userDeviceReq);
	}

	@RequestMapping(value = "/v2/removeMobileRegistrationId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String removeMobileRegistrationId(@RequestBody RegisterUserDeviceRequest request) throws VException, IOException {
		sessionUtils.checkIfAllowed(request.getUserId(), null, Boolean.FALSE);
		request.verify();
		BaseResponse response = notificationManager.removeUserDevice(request);
		return objectMapper.writeValueAsString(response);
	}
        
        
                
        @RequestMapping(value = "/getNotifications", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public GetNotificationsRes getNotifications(@RequestBody GetNotificationsReq req) throws VException {
                req.verify();
                return notificationManager.getNotifications(req);
	}

	@RequestMapping(value = "/v2/getNotifications", method = RequestMethod.GET)
	@ResponseBody
	public GetNotificationsRes getNotifications_v2(GetNotificationsReq req) throws VException {
		req.verify();
		sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
		return notificationManager.getNotifications(req);
	}
        
        
        @RequestMapping(value = "/markSeen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicRes markSeen(@RequestBody MarkSeenReq req) throws VException {
                req.verify();
                return notificationManager.markSeen(req);
	}

	@RequestMapping(value = "/v2/markSeen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicRes markSeen_v2(@RequestBody MarkSeenReq req) throws VException {
		req.verify();
		sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
		return notificationManager.markSeen(req);
	}
        
                
        @RequestMapping(value = "/markSeenByEntity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MarkSeenRes markSeenByEntity(@RequestBody MarkSeenReq req) throws VException {
                req.verify();
                return notificationManager.markSeenByEntity(req);
	}

	@RequestMapping(value = "/v2/markSeenByEntity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MarkSeenRes markSeenByEntity_v2(@RequestBody MarkSeenReq req) throws VException {
		req.verify();
		sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
		return notificationManager.markSeenByEntity(req);
	}
        
        
        @RequestMapping(value = "/markSeenByType", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MarkSeenRes markSeenByType(@RequestBody MarkSeenReq req) throws VException {
                req.verify();
                return notificationManager.markSeenByNotificationType(req);
	}
                
        
        @RequestMapping(value = "/setMarkSeen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MarkSeenRes setMarkSeen(@RequestBody MarkSeenReq req) throws VException {
                req.verify();
                return notificationManager.setMarkSeen(req);
	}

	@RequestMapping(value = "/v2/setMarkSeen", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MarkSeenRes setMarkSeen_v2(@RequestBody MarkSeenReq req) throws VException {
		req.verify();
		sessionUtils.checkIfAllowed(req.getUserId(), null, Boolean.FALSE);
		return notificationManager.setMarkSeen(req);
	}
        
        @RequestMapping(value = "/markSeenByNotificationIdAndDeviceId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    	@ResponseBody
    	public MarkSeenRes markSeenByNotificationIdAndDeviceId(@RequestBody MarkSeenByNotificationIdReq req) throws VException {
              req.verify();
              return notificationManager.markSeenByNotificationIdAndDeviceId(req);
    	}
}
