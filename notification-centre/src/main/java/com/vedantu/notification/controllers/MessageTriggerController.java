/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.controllers;

import com.vedantu.exception.VException;
import com.vedantu.notification.managers.MessageTriggerManager;
import com.vedantu.notification.request.HandleMessageTriggerReq;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/messagetrigger")
public class MessageTriggerController {

    @Autowired
    MessageTriggerManager messageTriggerManager;

    @RequestMapping(value = "/handleMessageTrigger", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void handleMessageTrigger(@Valid @RequestBody HandleMessageTriggerReq handleMessageTriggerReq) throws VException {
        messageTriggerManager.handleMessageTrigger(handleMessageTriggerReq);
    }

}
