package com.vedantu.notification.controllers;
import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.BlockedPhone;
import com.vedantu.notification.entity.GupshupDeliveryReport;
import com.vedantu.notification.response.blockedPhone.BlockedPhoneRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.*;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;

import com.vedantu.notification.managers.SMSManager;
import com.vedantu.notification.request.GupshupDeliveryReportReq;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.util.PlatformBasicResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/SMS")
/*
 * Controller for SMS API endpoint
 */
public class SMSController {

	@Autowired
	private SMSManager smsManager;
	@Autowired
	public LogFactory logFactory;

	@SuppressWarnings("static-access")
	public Logger logger = logFactory.getLogger(EmailController.class);

	@Autowired
	private HttpSessionUtils sessionUtils;


	/*
	 * API to send a SMS.
	 * @param TextSms input JSON POST object
	 * @return json A json with SMS Id received after successful delivery
	 */
	@RequestMapping(value = "/sendSMS", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse sendSMS(@RequestBody TextSMSRequest smsRequest) throws BadRequestException, IOException, JSONException, InternalServerErrorException
	{
		smsRequest.verify();
		return smsManager.sendSms(smsRequest);

	}

	/*
	 * API to send bulk SMSs.
	 * @param BulkTextSms input JSON POST object
	 * @return json A json with SMS Ids received after successful delivery
	 */
	@RequestMapping(value = "/sendBulkSMS", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BaseResponse sendBulkSMS(@RequestBody BulkTextSMSRequest bulkSmsRequest) throws JSONException, VException
	{
		bulkSmsRequest.verify();
		return smsManager.sendBulkSms(bulkSmsRequest);
	}


        @RequestMapping(value = "/gupshup/deliveryreport", method = RequestMethod.GET)
	@ResponseBody
	public PlatformBasicResponse gupshupDeliveryreport(GupshupDeliveryReportReq req) throws JSONException, VException
	{
		return smsManager.gupshupDeliveryreport(req);
	}

        @RequestMapping(value = "/ncert/sendapplink", method = RequestMethod.POST)
	@ResponseBody
	public PlatformBasicResponse sendncertapplink(@RequestBody TextSMSRequest req) throws JSONException, VException
	{
		return smsManager.sendncertapplink(req);
	}

	@RequestMapping(value = "/getDeliveryReportForManualNotification",method = RequestMethod.GET)
	@ResponseBody
	public List<GupshupDeliveryReport> getDeliveryReportForManualNotification(@RequestBody List<String> manualNotificationIds){
		return smsManager.getDeliveryReportForManualNotification(manualNotificationIds);
	}

	//smsLinkClicked
	@RequestMapping(value = "/smsLinkClicked", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse smsLinkClicked(@RequestParam(value = "id", required = true) String id,
												  @RequestParam(value = "linkRandomId", required = false) String linkRandomId) {
		logger.info("  smsLinkClicked  api     ");
		return smsManager.smsLinkClicked(id, linkRandomId);
	}

	@RequestMapping(value = "/addBlockedPhone", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse addBlockedPhone(@RequestBody TextSMSRequest req) throws VException {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return smsManager.addBlockedPhone(req);
	}

	@RequestMapping(value = "/unblockBlockedPhone", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public PlatformBasicResponse unblockBlockedPhone(@RequestBody TextSMSRequest req) throws Exception {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return smsManager.unblockBlockedPhone(req);
	}

	@RequestMapping(value = "/getBlockedPhones", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BlockedPhoneRes getBlockedPhones(@RequestParam(value = "start") Integer start,
											@RequestParam(value = "size") Integer size) throws Exception {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return smsManager.getBlockedPhones(start, size);
	}

	@RequestMapping(value = "/getBlockedPhone", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BlockedPhone getBlockedPhone(@RequestBody TextSMSRequest req) throws Exception {
		sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
		return smsManager.getBlockedPhone(req);
	}

	@RequestMapping(value = "/unsubscribeSMS", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String unsubscribeSMS(@RequestParam(value = "contactNumber") String encryptedContactNumber,
												@RequestParam(value = "phoneCode", required = false) String encryptedPhoneCode,
												@RequestParam(value = "type", required = false) String type) throws BadRequestException {
		PlatformBasicResponse resp = smsManager.unSubscribeSMS(encryptedContactNumber, encryptedPhoneCode, type);
		if(resp.isSuccess()){
			return "You are successfully unsubscribed";
		}
		else{
			return "There was an error, please try again";
		}

	}


}
