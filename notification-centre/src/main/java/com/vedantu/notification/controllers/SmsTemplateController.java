package com.vedantu.notification.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.SmsTemplate;
import com.vedantu.notification.managers.SmsTemplateManager;
import com.vedantu.notification.request.CreateSmsTemplateReq;
import com.vedantu.notification.response.SmsTemplatesWithFilterRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author kasireddy
 */

@RestController
@RequestMapping("/smsTemplate")
public class SmsTemplateController {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(SmsTemplateController.class);

    @Autowired
    private SmsTemplateManager smsTemplateManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/createSmsTemplate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse createSmsTemplate(@RequestBody CreateSmsTemplateReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        req.verify();
        return smsTemplateManager.createSmsTemplate(req);
    }

    @RequestMapping(value = "/updateSmsTemplate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse updateSmsTemplate(@RequestBody SmsTemplate smsTemplate) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        return smsTemplateManager.updateSmsTemplate(smsTemplate);
    }

    @RequestMapping(value = "/getSmsTemplates", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SmsTemplate> getSmsTemplates() throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        return smsTemplateManager.getSmsTemplates();
    }

    @RequestMapping(value = "/getSmsTemplatesWithFilter", method = RequestMethod.GET, produces = javax.ws.rs.core.MediaType.APPLICATION_JSON)
    public List<SmsTemplatesWithFilterRes> getSmsTemplatesWithFilter(@RequestParam("start") Integer start, @RequestParam("limit") Integer limit, @RequestParam(value = "state", required = false) EntityState state) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        return smsTemplateManager.getSmsTemplatesWithFilter(start, limit, state);
    }

}
