package com.vedantu.notification.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.Email;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.UnsubscribeEmailReq;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.EncryptionUtil;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.notification.managers.EmailManager;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.GetUnsubscribedListReq;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.responses.GetUnsubscribedListRes;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.io.IOException;
import java.util.*;
import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.xml.bind.DatatypeConverter;

import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.http.HttpMethod;
import org.springframework.web.bind.annotation.RequestHeader;

import static com.vedantu.User.Role.ADMIN;

@RestController
@RequestMapping("/email")
/*
 * Controller for Email API endpoint
 */
public class EmailController {

    @Autowired
    private EmailManager emailManager;

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(EmailController.class);

    private final ObjectMapper objectMapper = new ObjectMapper().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
    @Autowired
    HttpSessionUtils sessionUtils;
    /*
	 * API to send an email.
	 * @param Email input JSON POST object
	 * @return json A json with status received after successful delivery
     */
    @RequestMapping(value = "/sendEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse sendEmail(@RequestBody EmailRequest emailRequest) throws BadRequestException, VException, MessagingException, AddressException, IOException, JSONException, InterruptedException {
        emailRequest.verify();
        return emailManager.sendEmail(emailRequest);
    }

    @RequestMapping(value = "/sendislemail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse sendislemail(@RequestParam(value = "email", required = true) String email,
            @RequestParam(value = "tag", required = false) String tag,
            @RequestParam(value = "token", required = true) String token,
            @RequestParam(value = "utm_source", required = false) String utm_source,
            @RequestParam(value = "utm_campaign", required = false) String utm_campaign,
            @RequestParam(value = "utm_term", required = false) String utm_term,
            @RequestParam(value = "utm_medium", required = false) String utm_medium
    ) throws BadRequestException, AddressException, InterruptedException, VException, MessagingException, IOException, JSONException {
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(email));
        HashMap<String, Object> bodyScopes = new HashMap<>();
        String base = "https://isl.vedantu.com";
        if (!ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase().equals("prod")) {
            base = "https://isl-qa.vedantu.com";
        }
        String islRegisterUrl = base + "/?utm_source="
                + utm_source
                + "&utm_campaign=" + utm_campaign
                + "&utm_term=" + utm_term
                + "&utm_medium=" + utm_medium
                + "&token=" + token;
        bodyScopes.put("islRegisterUrl", islRegisterUrl);
        EmailRequest islEmailRequest = new EmailRequest(toList, null, bodyScopes,
                CommunicationType.ISL_REGISTERED_USER_EMAIL, Role.STUDENT);
        Set<String> tags = new HashSet<>();
        if (StringUtils.isNotEmpty(tag)) {
            tags.add(tag);
        }
        tags.add(utm_source);
        tags.add(utm_campaign);
        tags.add(utm_term);
        tags.add(utm_medium);
        islEmailRequest.setTags(tags);
        islEmailRequest.setClickTrackersEnabled(true);
        islEmailRequest.setIncludeHeaderFooter(false);
        ArrayList<InternetAddress> replyTo = new ArrayList<>();
        replyTo.add(new InternetAddress("isl@vedantu.com"));
        islEmailRequest.setReplyTo(replyTo);
//        Thread.sleep(1800);
        emailManager.sendEmail(islEmailRequest);
        return new BaseResponse();
    }

    @RequestMapping(value = "/emailStatusNotification", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void emailStatusNotification(@RequestBody String snsMessage) {
        emailManager.emailStatusNotification(snsMessage);
    }

    /*
	 * API to send bulk emails
	 * @param Email input JSON POST object
	 * @return json A json with status received after successful delivery
     */
    @RequestMapping(value = "/sendBulkEmail", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse sendBulkEmail(@RequestBody EmailRequest emailRequest) throws BadRequestException {

        emailRequest.verify();
        return emailManager.sendBulkEmail(emailRequest);
    }

    /*
	 * API to get current sending quota
	 * @return json A json with sending quota status received
     */
    @RequestMapping(value = "/getSendQuota", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse getSendQuota() {
        return emailManager.getSendQuota();
    }

    /*
	 * API to get send email statistics
	 * @return json A json with sending statistics received
     */
    @RequestMapping(value = "/getSendStatistics", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse getSendStatistics(@RequestParam(value = "lastHours", required = false) Integer lastHours) {
        if (lastHours == null) {
            lastHours = 1;
        }
        return emailManager.getSendStatistics(lastHours);
    }

    /*
	 * API to update email opened statistic
	 * @return json A json with sending statistics received
     */
    @RequestMapping(value = "/emailOpened", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse emailOpened(@RequestParam(value = "id", required = true) String id) {
        logger.info("   emailOpened   api    "+id);
        return emailManager.emailOpened(id);
    }

    @RequestMapping(value = "/v2/emailOpened", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String emailOpened_v2(@RequestParam(value = "id") String id) throws BadRequestException, IOException {
        if(id == null)
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "id is mandatory");
        BaseResponse response = emailManager.emailOpened(id);
        return objectMapper.writeValueAsString(response);
    }

    @RequestMapping(value = "/emailLinkClicked", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse emailLinkClicked(@RequestParam(value = "emailId", required = true) String emailId,
            @RequestParam(value = "linkRandomId", required = false) String linkRandomId) {
        return emailManager.emailLinkClicked(emailId, linkRandomId);
    }

    @RequestMapping(value = "/markUnsubscribeStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markUnsubscribeStatus(@RequestBody UnsubscribeEmailReq req) throws BadRequestException {
        return emailManager.markUnsubscribeStatus(req);
    }

    @RequestMapping(value = "/v2/markUnsubscribeStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markUnsubscribeStatus_v2(@RequestBody UnsubscribeEmailReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return emailManager.markUnsubscribeStatus(req);
    }

    @RequestMapping(value = "/getUnsubscribedList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUnsubscribedListRes getUnsubscribedList(GetUnsubscribedListReq req) throws VException {
        return emailManager.getUnsubscribedList(req);
    }

    @RequestMapping(value = "/v2/getUnsubscribedList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUnsubscribedListRes getUnsubscribedList_v2(GetUnsubscribedListReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return emailManager.getUnsubscribedList(req);
    }

    @RequestMapping(value = "/consumeBulkEmail", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void consumeBulkEmail(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws BadRequestException{
            Gson gson = new Gson();
            AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
            if (messageType.equals("SubscriptionConfirmation")) {
                String json = null;
                logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
                ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
                logger.info(resp.getEntity(String.class));
            } else if (messageType.equals("Notification")) {

                logger.info("Notification received - SNS");
                logger.info(subscriptionRequest.toString());
                emailManager.consumeBulkEmail();
            }
    }

    @RequestMapping(value = "getDeliveryReportForManualNotification", method = RequestMethod.GET)
    @ResponseBody
    public List<Email> getDeliveryReportForManualNotification(@RequestBody List<String> manualNotificationIds){
        return emailManager.getDeliveryReportForManualNotification(manualNotificationIds);
    }

    @RequestMapping(value = "/unsubscribeEmail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String unsubscribeEmail(@RequestParam(value = "email", required = true) String encryptedEmail,
                                                  @RequestParam(value = "type", required = false) String type) throws BadRequestException {

        PlatformBasicResponse response = emailManager.unsubscribeEmail(encryptedEmail, type);
        if(response.isSuccess()){
            return "You are successfully unsubscribed";
        }
        else{
            return "There was an error, please try again";
        }

    }

    @RequestMapping(value = "/removeBounceEntries", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse removeBounceEntries(@RequestBody UnsubscribeEmailReq req) throws VException {
        sessionUtils.checkIfAllowed(sessionUtils.getCallingUserId(), ADMIN, true);
        return emailManager.removeBounceEntries(req);
    }
}
