/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.exception.VException;
import com.vedantu.notification.dao.TeacherDayStatsDAO;
import com.vedantu.notification.managers.MessageUserManager;
import com.vedantu.notification.entity.MessageUser;
import com.vedantu.notification.pojo.TeachersDayStats;
import com.vedantu.notification.pojos.MessageUserRes;
import com.vedantu.notification.request.GetMessageUsersReq;
import com.vedantu.notification.request.TeacherWishesReq;
import com.vedantu.notification.requests.MessageUserReplyReq;
import com.vedantu.notification.response.GetMessageUsersRes;
import com.vedantu.notification.response.MessageUserServletRes;
import com.vedantu.notification.response.TeacherWishesRes;
import com.vedantu.notification.responses.GetMessageLeadersRes;
import com.vedantu.util.PlatformBasicResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.validation.Valid;

import com.vedantu.util.security.HttpSessionUtils;
import lombok.NonNull;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author jeet
 */
@RestController
@RequestMapping("/messageuser")
public class MessageUserController {


    @Autowired
    MessageUserManager messageUserManager;

    @Autowired
    HttpSessionUtils httpSessionUtils;

    @Autowired
    TeacherDayStatsDAO teacherDayStatsDAO;

    @RequestMapping(value = "/addMessageUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessageUserRes addMessageUser(@Valid @RequestBody MessageUser messageUser) throws VException, JSONException {
        return messageUserManager.addMessageUser(messageUser);
	}
        
    @RequestMapping(value = "/messageUserReply", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse messageUserReply(@Valid @RequestBody MessageUserReplyReq request) throws VException {
        return messageUserManager.messageUserReply(request);
    }

	
	@RequestMapping(value = "/getMessageById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public MessageUser getMessageById(@RequestParam(value = "messageId") Long messageId) throws VException {
	    return messageUserManager.getMessageById(messageId);
	}

    @RequestMapping(value = "/v2/getMessageById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public MessageUserServletRes getMessageById_v2(@RequestParam(value = "messageId") Long messageId) throws VException {
        return messageUserManager.getMessageById_v2(messageId);
    }
        
    @RequestMapping(value = "/getMessageUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<MessageUser> getMessageUsers(GetMessageUsersReq request) throws VException {
	    return messageUserManager.getMessageUsers(request);
	}

    @RequestMapping(value = "/v2/getMessageUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetMessageUsersRes getMessageUsers_v2(GetMessageUsersReq request) throws VException, IOException {
        return messageUserManager.getMessageUsers_v2(request);
    }
  
    @RequestMapping(value = "/getMessageLeaders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetMessageLeadersRes getMessageLeaders(GetMessageUsersReq request) throws VException {
        request.verify();
        return messageUserManager.getMessageLeaders(request);
    }

    @RequestMapping(value = "/v2/getMessageLeaders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetMessageLeadersRes getMessageLeaders_v2(GetMessageUsersReq request) throws VException {
        request.verify();
        return messageUserManager.getMessageLeaders_v2(request);
    }

    @RequestMapping(value = "/v3/getMessageLeaders", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TeachersDayStats> getMessageLeadersV3() throws VException {
        return teacherDayStatsDAO.getTopTenForDashboardStats();
    }

    @RequestMapping(value = "/getPostedWishesForTeachers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TeacherWishesRes> getPostedWishesForTeachers(@NonNull @RequestParam(name = "teacherId")Long teacherId,
                                                             @NonNull @RequestParam(name = "start") Integer start) throws VException {

        if (Objects.isNull(httpSessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
        httpSessionUtils.checkIfAllowedList(httpSessionUtils.getCallingUserId(), Arrays.asList(Role.TEACHER, Role.STUDENT), false);

        return messageUserManager.getPostedWishesForTeachers(teacherId, start);
    }

    @RequestMapping(value = "/getPostedWishesForTeacherStudent", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TeacherWishesRes getPostedWishesForTeacherStudent(@NonNull @RequestParam(name = "teacherId")Long teacherId,
                                                                   @NonNull @RequestParam(name = "studentId") Long studentId) throws VException {

        if (Objects.isNull(httpSessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }
        httpSessionUtils.checkIfAllowedList(httpSessionUtils.getCallingUserId(), Arrays.asList(Role.TEACHER, Role.STUDENT), false);

        return messageUserManager.getPostedWishesForTeacherStudent(teacherId, studentId);
    }

    @RequestMapping(value = "/postWishForTeacher", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse postWishForTeacher(@RequestBody TeacherWishesReq teacherWishesReq) throws VException {
        if (Objects.isNull(httpSessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }

        teacherWishesReq.verify();
        Long callingUserId = httpSessionUtils.getCallingUserId();
        httpSessionUtils.checkIfAllowedList(callingUserId, Collections.singletonList(Role.STUDENT), false);
        if (!callingUserId.equals(teacherWishesReq.getStudentId())) {
            return new PlatformBasicResponse();
        }
        return messageUserManager.postWishForTeacher(teacherWishesReq);
    }

}
