package com.vedantu.notification.controllers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.notification.request.AddBatchTelegramGroupRequest;
import com.vedantu.notification.request.GetTelegramDoubtRequest;
import com.vedantu.notification.request.TelegramHookRequest;
import com.vedantu.notification.request.TelegramUserVerificationRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.TelegramDoubtResponse;
import com.vedantu.notification.thirdparty.TelegramActorManager;
import com.vedantu.notification.thirdparty.TelegramObserverManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import java.io.UnsupportedEncodingException;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author parashar
 */

@RestController
@RequestMapping("/telegram")
/*
 * Controller for telegram API endpoint
 */
public class TelegramController {
    
    @Autowired
    private TelegramActorManager telegramActorManager;
    
    @Autowired
    private TelegramObserverManager telegramObserverManager;
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TelegramController.class); 
    
    @RequestMapping(value = "/verifyUser", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody    
    public PlatformBasicResponse verifyTelegramLogin(@RequestBody TelegramUserVerificationRequest telegramUserVerificationRequest) throws BadRequestException, ConflictException, VException{
        telegramUserVerificationRequest.verify();
        
        return telegramActorManager.verifyTelegramLogin(telegramUserVerificationRequest);
    }
    
    @RequestMapping(value = "/hook", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody    
    public void telegramHook(@RequestBody TelegramHookRequest telegramHookRequest){
        //telegramUserVerificationRequest.verify();
        logger.info("telegram request: "+ telegramHookRequest);
        telegramObserverManager.processMessage(telegramHookRequest);
        
    }
    
    @RequestMapping(value = "/addBatchGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody    
    public void addBatchGroup(@RequestBody AddBatchTelegramGroupRequest req) throws BadRequestException, VException, UnsupportedEncodingException{
        //telegramUserVerificationRequest.verify();
        req.verify();
        logger.info("telegram request: "+ req);
        telegramActorManager.createGroup(req);
        
    }    
    
    @RequestMapping(value = "/getDoubts", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<TelegramDoubtResponse> getTelegramDoubtResponse(GetTelegramDoubtRequest req){
        return telegramActorManager.getDoubts(req);
    }
    
    @RequestMapping(value = "/markDoubtClosed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void markDoubtClosed(@RequestParam(name="doubtId", required = true) String doubtId) throws NotFoundException{
        telegramActorManager.markDoubtClosed(doubtId);
    }
    
    @RequestMapping(value = "/requestTelegramLink", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void requestTelegramLink(@RequestParam(name="batchId", required = true) String batchId,@RequestParam(name="userId", required = true ) Long userId) throws NotFoundException, VException, UnsupportedEncodingException{
        
        telegramActorManager.requestTelegramLink(batchId, userId);
    } 
    
    @RequestMapping(value = "/isTelegramBatch", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse isTelegramBatch(@RequestParam(name="batchId", required = true) String batchId) throws NotFoundException{
        
        return telegramActorManager.isTelegramBatch(batchId);
    }   
    
    
}
