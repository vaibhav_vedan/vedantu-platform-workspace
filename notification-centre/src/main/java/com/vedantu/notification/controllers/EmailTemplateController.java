package com.vedantu.notification.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.managers.EmailTemplateManager;
import com.vedantu.notification.request.CreateEmailTemplateReq;
import com.vedantu.notification.response.EmailTemplatesWithFilterRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * @author kasireddy
 */

@RestController
@RequestMapping("/emailTemplate")
public class EmailTemplateController {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(EmailTemplateController.class);

    @Autowired
    private EmailTemplateManager emailTemplateManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/createEmailTemplate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse createEmailTemplate(@RequestBody CreateEmailTemplateReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        req.verify();
        return emailTemplateManager.createEmailTemplate(req);
    }

    @RequestMapping(value = "/getEmailTemplatesWithFilter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EmailTemplatesWithFilterRes> getEmailTemplatesWithFilter(@RequestParam("start") Integer start, @RequestParam("limit") Integer limit, @RequestParam(value = "state", required = false) EntityState state) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
        return emailTemplateManager.getEmailTemplatesWithFilter(start, limit, state);
    }

    @RequestMapping(value = "/getEmailBodyFromEmailTypeAndRole", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public String getEmailBodyFromEmailTypeAndRole(@RequestParam("emailType") CommunicationType emailType, @RequestParam(value = "role", required = false) Role role) throws VException, IOException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return emailTemplateManager.getEmailBodyFromEmailTypeAndRole(emailType, role);
    }
}
