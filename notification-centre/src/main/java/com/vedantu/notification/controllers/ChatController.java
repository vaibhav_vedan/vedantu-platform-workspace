package com.vedantu.notification.controllers;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.util.security.HttpSessionUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.notification.enums.ChatMessageType;
import com.vedantu.notification.managers.ChatManager;
import com.vedantu.notification.request.OfflineMessageRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.GetChatMessageHistoryResponse;
import com.vedantu.util.PlatformBasicResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private ChatManager chatManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    private ObjectMapper objectMapper = new ObjectMapper().setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
    /*
	 * API to get last users chatted with
	 * @return json A json with user list and channel info
     */
    @CrossOrigin
    @RequestMapping(value = "/fetchUsersLastChattedWith", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse fetchUsersLastChattedWith(@RequestParam("userId") Long userId,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size) {
        if (start == null) {
            start = 0;
        }
        if (size == null) {
            size = 20;
        }
        // get userid from session
        Long userIdFromSession = sessionUtils.getCallingUserId();
        if(null == userIdFromSession || !userIdFromSession.equals(userId)){
            return null;
        }
        return chatManager.fetchUsersLastChattedWith(userId, start, size);
    }

    @RequestMapping(value = "/v2/fetchUsersLastChattedWith", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String fetchUsersLastChattedWith_v2(@RequestParam("userId") Long userId,
                                            @RequestParam(value = "start", required = false) Integer start,
                                            @RequestParam(value = "size", required = false) Integer size) throws VException, IOException {
        if (start == null) {
            start = 0;
        }
        if (size == null) {
            size = 20;
        }
        // get userid from session
        Long userIdFromSession = sessionUtils.getCallingUserId();
        if(null == userIdFromSession || !userIdFromSession.equals(userId)){
            return null;
        }
        BaseResponse response = chatManager.fetchUsersLastChattedWith(userId, start, size);

        return objectMapper.writeValueAsString(response);
    }
    /*
	 * API to get save channel info for initiate chat
	 * @return json A json with channel info
     */
    @CrossOrigin
    @RequestMapping(value = "/initiateChatWithUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse initiateChatWithUser(@RequestParam("fromUserId") Long fromUserId,
            @RequestParam("toUserId") Long toUserId) {
        return chatManager.initiateChatWithUser(fromUserId, toUserId);
    }

    @RequestMapping(value = "/v2/initiateChatWithUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String initiateChatWithUser_v2(@RequestParam("fromUserId") Long fromUserId,
                                       @RequestParam("toUserId") Long toUserId) throws VException, IOException {
        BaseResponse response = chatManager.initiateChatWithUser(fromUserId, toUserId);
        return objectMapper.writeValueAsString(response);
    }

    /*
	 * API to save lastCHatted time for a user and update the unreadcount to 0
	 * @return json A json with update status
     */
    @CrossOrigin
    @RequestMapping(value = "/updateLastChattedTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse updateLastChattedTime(@RequestParam("userId") Long userId,
            @RequestParam("channelName") String channelName) {
        return chatManager.updateLastChattedTime(userId, channelName);
    }

    @RequestMapping(value = "/v2/updateLastChattedTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateLastChattedTime_v2(@RequestParam("userId") Long userId,
                                        @RequestParam("channelName") String channelName) throws VException, IOException {
        BaseResponse response = chatManager.updateLastChattedTime(userId, channelName);
        return objectMapper.writeValueAsString(response);
    }

    /*
   	 * API to save unreadMessageCount for a user on his mututal channel
   	 * @return json A json with update status
     */
    @CrossOrigin
    @RequestMapping(value = "/updateUnreadMessageCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse updateUnreadMessageCount(@RequestParam("userId") Long userId,
            @RequestParam("channelName") String channelName, @RequestParam("count") int count) {
        return chatManager.updateUnreadMessageCount(userId, channelName, count);
    }

    @RequestMapping(value = "/v2/updateUnreadMessageCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String updateUnreadMessageCount_v2(@RequestParam("userId") Long userId,
                                           @RequestParam("channelName") String channelName, @RequestParam("count") int count) throws VException, IOException {
        BaseResponse response = chatManager.updateUnreadMessageCount(userId, channelName, count);
        return objectMapper.writeValueAsString(response);
    }

//    @CrossOrigin
    @RequestMapping(value = "/replyToOfflineMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse replyToOfflineMessage(@Valid @RequestBody OfflineMessageRequest request) throws BadRequestException {
        request.verify();
        return chatManager.replyToOfflineMessage(request, ChatMessageType.MESSAGE);
    }

    @RequestMapping(value = "/v2/replyToOfflineMessage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String replyToOfflineMessage_v2(@Valid @RequestBody OfflineMessageRequest request) throws VException, IOException {
        request.verify();
        BaseResponse response = chatManager.replyToOfflineMessage(request, ChatMessageType.MESSAGE);
        return objectMapper.writeValueAsString(response);
    }

    @CrossOrigin
    @RequestMapping(value = "/replyToOfflineMessageFile", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BaseResponse replyToOfflineMessageFile(@Valid @RequestBody OfflineMessageRequest request) throws BadRequestException {
        request.verify();
        return chatManager.replyToOfflineMessage(request, ChatMessageType.FILE);
    }

    @RequestMapping(value = "/v2/replyToOfflineMessageFile", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String replyToOfflineMessageFile_v2(@Valid @RequestBody OfflineMessageRequest request) throws VException, IOException {
        request.verify();
        BaseResponse response = chatManager.replyToOfflineMessage(request, ChatMessageType.FILE);
        return objectMapper.writeValueAsString(response);
    }

    @CrossOrigin
    @RequestMapping(value = "/setAblyChannelActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse setAblyChannelActive(@RequestParam(value = "channel", required = true) String channel) {
        return chatManager.setAblyChannelActive(channel);
    }

    @CrossOrigin
    @RequestMapping(value = "/v2/setAblyChannelActive", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String setAblyChannelActive_v2(@RequestParam(value = "channel") String channel) throws VException, IOException {
        PlatformBasicResponse response =  chatManager.setAblyChannelActive(channel);
        return objectMapper.writeValueAsString(response);
    }

    @CrossOrigin
    @RequestMapping(value = "/getChatChannelName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse getChatChannelName(@RequestParam(value = "fromUserId", required = true) Long fromUserId,@RequestParam(value = "toUserId", required = true) Long toUserId) {
       return chatManager.getChatChannelName(fromUserId,toUserId);
    }

    @CrossOrigin
    @RequestMapping(value = "/v2/getChatChannelName", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getChatChannelName_v2(@RequestParam(value = "fromUserId") Long fromUserId,@RequestParam(value = "toUserId") Long toUserId) throws IOException {
        PlatformBasicResponse response = chatManager.getChatChannelName(fromUserId, toUserId);
        return objectMapper.writeValueAsString(response);
    }
    
    
    @CrossOrigin
    @RequestMapping(value = "/getChatMessageHistory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetChatMessageHistoryResponse getChatMessageHistory(@RequestParam(value = "channel", required = true) String channel, @RequestParam(value = "size", required = true) Integer size, @RequestParam(value = "before", required = false) Long before) throws VException {
        return chatManager.getChatMessageHistory(channel, size, before);
    }

    @CrossOrigin
    @RequestMapping(value = "/v2/getChatMessageHistory", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getChatMessageHistory_v2(@RequestParam(value = "channel", required = true) String channel, @RequestParam(value = "size", required = true) Integer size, @RequestParam(value = "before", required = false) Long before) throws VException, IOException {
        GetChatMessageHistoryResponse response = chatManager.getChatMessageHistory(channel, size, before);
        return objectMapper.writeValueAsString(response);
    }

//    /*
//   	 * API to save status type for a user
//   	 * @return json A json with info about the operation
//   	 */
//    @CrossOrigin
//   	@RequestMapping(value = "/setStatusType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//   	@ResponseBody
//   	public BaseResponse setStatusType(@RequestParam("userId") Long userId, 
//   			@RequestParam("statusType") StatusType statusType, @RequestParam("role") Role role)
//   	{
//   		return chatManager.setStatusType(userId, role, statusType);
//   	}
//    
//    @CrossOrigin
//   	@RequestMapping(value = "/getAllStatusTypes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//   	@ResponseBody
//   	public BaseResponse getAllStatusTypes(@RequestParam("userIds") List<Long> userIds)
//   	{
//   		return chatManager.getAllStatusTypes(userIds);
//   	}
//    
//    @CrossOrigin
//   	@RequestMapping(value = "/getMyStatusType", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//   	@ResponseBody
//   	public BaseResponse getMyStatusType(@RequestParam("userId") Long userId)
//   	{
//   		return chatManager.getMyStatusType(userId);
//   	}
}
