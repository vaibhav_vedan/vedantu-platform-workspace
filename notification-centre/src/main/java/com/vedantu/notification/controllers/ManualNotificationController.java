package com.vedantu.notification.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.ManualNotification;
import com.vedantu.notification.managers.ManualNotificationManager;
import com.vedantu.notification.request.ChangeManualNotificationRequest;
import com.vedantu.notification.request.NotificationFilterReq;
import com.vedantu.notification.requests.ManualNotificationRequest;
import com.vedantu.notification.response.ManualNotifDownloadDataRes;
import com.vedantu.notification.response.ManualNotificationDetailsRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manualnotif")
public class ManualNotificationController {
    @Autowired
    private ManualNotificationManager manualNotificationManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ManualNotificationController.class);

    @RequestMapping(value="insertData",method= RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ManualNotification insertData(@RequestBody ManualNotificationRequest manualNotification) throws VException {
        manualNotification.verify();
//        sessionUtils.checkIfAllowed(manualNotification.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return manualNotificationManager.insertData(manualNotification);
    }

    @RequestMapping(value="/getAllManualNotificationData", method=RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ManualNotificationDetailsRes> getAllManualNotificationData(@RequestBody NotificationFilterReq filterReq) throws VException {
        filterReq.verify();
        sessionUtils.checkIfAllowed(filterReq.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return manualNotificationManager.getAllManualNotificationData(filterReq);
    }

    @RequestMapping(value = "/sendScheduledEmailOrSMS",method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void sendScheduledEmailOrSMS(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
                                                            @RequestBody String request){
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info("ClientResponse : " + resp.toString());
        } else if (messgaetype.equals("Notification")) {
            manualNotificationManager.sendScheduledEmailOrSMS();
        }
    }

    @RequestMapping(value = "/discardScheduledEmailOrSMS",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ManualNotification> discardScheduledEmailOrSMS(@RequestBody ChangeManualNotificationRequest discardList) throws VException {
        discardList.verify();
          sessionUtils.checkIfAllowed(discardList.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return manualNotificationManager.discardScheduledEmailOrSMS(discardList);
    }

    @RequestMapping(value = "/rescheduleEmailOrSMS", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ManualNotification> rescheduleEmailOrSMS(@RequestBody ChangeManualNotificationRequest rescheduleList) throws VException {
        rescheduleList.verify();
             sessionUtils.checkIfAllowed(rescheduleList.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        if(rescheduleList.getRescheduledTime()==null){
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,"No reschedule time found", Level.ERROR);
        }
        return manualNotificationManager.rescheduleEmailOrSMS(rescheduleList);
    }

    @RequestMapping(value = "/duplicateCommunication/{id}",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ManualNotification> duplicateCommunication(@PathVariable("id")String id) throws VException,BadRequestException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return manualNotificationManager.duplicateCommunication(id);
    }

    @RequestMapping(value = "/downloadCommunication/{id}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ManualNotifDownloadDataRes> downloadCommunication(@PathVariable("id")String id) throws BadRequestException {
        return manualNotificationManager.downloadCommunication(id);
    }

}
