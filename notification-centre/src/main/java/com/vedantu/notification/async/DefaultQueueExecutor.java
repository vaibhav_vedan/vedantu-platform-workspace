/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.async;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.managers.AwsSQSManager;
import com.vedantu.notification.managers.EmailManager;
import com.vedantu.notification.managers.SMSManager;
import com.vedantu.notification.managers.VQuizNotificationManager;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSQueue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

/**
 *
 * @author ajith
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Autowired
    public SMSManager sMSManager;
    
    @Autowired
    public AwsSQSManager awsSQSManager;
    
    @Autowired
    public EmailManager emailManager;

    @Autowired
    public VQuizNotificationManager vQuizNotificationManager;

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        switch (taskName) {
            case SEND_NCERT_SOLUTIONS_PLAY_STORE_LINK:
                TextSMSRequest smsRequest = (TextSMSRequest) payload.get("smsRequest");
                sMSManager.sendSms(smsRequest);
                break;
            case SEND_SMS:
                String receipentHandleSMS = (String) payload.get("receipentHandle");
                TextSMSRequest textSMSRequest = (TextSMSRequest) payload.get("textSMSRequest"); 
                sMSManager.sendSms(textSMSRequest);
                try{
                    logger.info("Receipt handle is " + receipentHandleSMS);
                    awsSQSManager.deleteMessageByHandle(awsSQSManager.getQueueURL(SQSQueue.NOTIFICATION_QUEUE), receipentHandleSMS);
                    logger.info("message deleted");                    
                }catch(Exception e){
                    logger.error("Error processing sms request " + textSMSRequest + " ..error : " , e);                    
                }
                break;
            case SEND_EMAIL:
                String receipentHandleEmail = (String) payload.get("receipentHandle");
                EmailRequest emailRequest = (EmailRequest) payload.get("emailRequest");
                emailManager.sendEmail(emailRequest);
                try{
                    logger.info("Receipt handle is " + receipentHandleEmail);
                    awsSQSManager.deleteMessageByHandle(awsSQSManager.getQueueURL(SQSQueue.NOTIFICATION_QUEUE), receipentHandleEmail);
                    logger.info("message deleted");                    
                }catch(Exception e){
                    logger.error("Error processing sms request " + emailRequest + " ..error : " , e);                    
                }               
                break;

            case SEND_BULK_EMAIL:
                EmailRequest bulkEmailRequest=(EmailRequest) payload.get("emailRequest");
                List<InternetAddress> emailTo=(List<InternetAddress>)payload.get("emailTo");
                emailTo.forEach(to->{
                    EmailRequest newEmailReq=new EmailRequest(bulkEmailRequest);
                    newEmailReq.setTo(new ArrayList<>(Arrays.asList(to)));
                    try {
                        emailManager.sendEmail(newEmailReq);
                    } catch (VException e) {
                        logger.error("VException - ",e);
                    } catch (MessagingException e) {
                        logger.error("MessagingException - ",e);
                    } catch (IOException e) {
                        logger.error("IOException - ",e);
                    } catch (JSONException e) {
                        logger.error("JSONException - ",e);
                    } catch (InterruptedException e) {
                        logger.error("InterruptedException - ",e);
                    }
                });
                break;
            case SEND_BULK_SMS:
                BulkTextSMSRequest bulkTextSMSRequest=(BulkTextSMSRequest)payload.get("bulkTextSMSRequest");
                sMSManager.sendBulkSms(bulkTextSMSRequest);
                break;
            case VQUIZ_NOTIFICATION_REMINDER:
                vQuizNotificationManager.sendVQuizRemindMeNotification();
                break;
            default:
                logger.error("Logic not defined for task:" + taskName);
                
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
