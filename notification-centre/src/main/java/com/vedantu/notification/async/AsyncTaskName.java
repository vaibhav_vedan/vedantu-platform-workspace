/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.async;

import com.vedantu.async.IAsyncTaskName;

/**
 *
 * @author ajith
 */
public enum AsyncTaskName implements IAsyncTaskName {
    SEND_NCERT_SOLUTIONS_PLAY_STORE_LINK(AsyncQueueName.DEFAULT_QUEUE),
    SEND_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    SEND_SMS(AsyncQueueName.DEFAULT_QUEUE),
    SEND_BULK_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    SEND_BULK_SMS(AsyncQueueName.DEFAULT_QUEUE),
    VQUIZ_NOTIFICATION_REMINDER(AsyncQueueName.DEFAULT_QUEUE);
    private AsyncQueueName queue;

    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }

    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
