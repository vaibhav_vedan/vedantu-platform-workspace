package com.vedantu.notification.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by somil on 19/07/17.
 */

@Document(collection = "UnsubscribedEmail")
public class UnsubscribedEmail extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String email;

    private String type;

    private String ipAddress;

    public UnsubscribedEmail(String email) {
        this.email = email;
    }


    public UnsubscribedEmail() {
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String EMAIL = "email";
        public static final String IP_ADDRESS = "ipAddress";
    }


    @Override
    public String toString() {
        return "UnsubscribedEmail{" +
                "email='" + email + '\'' +
                "} " + super.toString();
    }
}
