package com.vedantu.notification.entity;

import com.vedantu.notification.enums.ChannelType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class ChannelUserInfo extends AbstractMongoStringIdEntity{
	
	private Long userId;
	private int newMessagesCount;
	private String channelName;
	private ChannelType channelType;
	
	public ChannelUserInfo() {
		super();
	}
	
	public ChannelUserInfo(Long userId, int newMessagesCount, String channelName
			,ChannelType channelType) {
		super();
		this.userId = userId;
		this.newMessagesCount = newMessagesCount;
		this.channelName = channelName;
		this.channelType = channelType;
	}
	
	public Long getUserId() {
		return userId;
	}

	public int getNewMessagesCount() {
		return newMessagesCount;
	}

	public void setNewMessagesCount(int newMessagesCount) {
		this.newMessagesCount = newMessagesCount;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public ChannelType getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

}
