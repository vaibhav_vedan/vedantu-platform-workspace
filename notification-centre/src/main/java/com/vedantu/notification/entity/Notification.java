/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.notification.enums.NotificationSubType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.Comparator;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author somil
 */
@Document(collection = "Notification")
@CompoundIndexes({
    @CompoundIndex(name = "userId_entityId", def = "{'userId' : 1, 'entityId': 1}")
    ,
    @CompoundIndex(name = "userId_1_isSeen_1_lastUpdated_-1", def = "{'userId': 1, 'isSeen': 1, 'lastUpdated': -1}")})
public class Notification extends AbstractMongoStringIdEntity {

    private Long userId;
    private NotificationType type;
    private String header;
    private String body;
    private String footer;
    private String description;
    private Boolean isSeen;
    private Long seenAt;
    private String entityType;
    private Long entityId;
    private String info;
    private Boolean hidden;
    private NotificationSubType subType;
    private String sendTo;
    private String regToken;
    private String deviceId;
    private String parentId;
    private String app;

    public Notification() {
        super();
    }

    public Notification(Long userId, NotificationType type, String header, String body, String footer,
            String description, String info, Boolean hidden) {

        this.userId = userId;
        this.type = type;
        this.header = header;
        this.body = body;
        this.footer = footer;
        this.description = description;
        this.isSeen = false;

        this.info = info;
        this.hidden = hidden;
    }

    public Long getUserId() {

        return userId;
    }

    public void setUserId(Long userId) {

        this.userId = userId;
    }

    public NotificationType getType() {

        return type;
    }

    public void setType(NotificationType type) {

        this.type = type;
    }

    public String getHeader() {

        return header;
    }

    public void setHeader(String header) {

        this.header = header;
    }

    public String getBody() {

        return body;
    }

    public void setBody(String body) {

        this.body = body;
    }

    public String getFooter() {

        return footer;
    }

    public void setFooter(String footer) {

        this.footer = footer;
    }

    public Boolean getIsSeen() {

        return isSeen;
    }

    public void setIsSeen(Boolean isSeen) {

        this.isSeen = isSeen;
    }

    public Long getSeenAt() {

        return seenAt;
    }

    public void setSeenAt(Long seenAt) {

        this.seenAt = seenAt;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public NotificationSubType getSubType() {
        return subType;
    }

    public void setSubType(NotificationSubType subType) {
        this.subType = subType;
    }

    public String getSendTo() {
		return sendTo;
	}

	public void setSendTo(String sendTo) {
		this.sendTo = sendTo;
	}

	public String getRegToken() {
		return regToken;
	}

	public void setRegToken(String regToken) {
		this.regToken = regToken;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getApp() {
		return app;
	}

	public void setApp(String app) {
		this.app = app;
	}

	public void verify() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String IS_SEEN = "isSeen";
        public static final String ENTITY_TYPE = "entityType";
        public static final String ENTITY_ID = "entityId";
        public static final String TYPE = "type";
    }

    public static class NotificationComparator implements Comparator<Notification> {

        @Override
        public int compare(Notification lhs, Notification rhs) {
            if (lhs.getLastUpdated() < rhs.getLastUpdated()) {
                return 1;
            } else if (lhs.getLastUpdated() > rhs.getLastUpdated()) {
                return -1;
            }
            return 0;
        }
    }

//    public com.vedantu.notification.requests.Notification toNotificationPojo() {
//        com.vedantu.notification.requests.Notification pojo = new com.vedantu.notification.requests.Notification();
//        try {
//            BeanUtils.copyProperties(pojo, this);
//        } catch (IllegalAccessException | InvocationTargetException ex) {
//            Logger.getLogger(Notification.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        return pojo;
//    }
}
