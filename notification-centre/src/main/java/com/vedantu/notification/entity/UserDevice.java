package com.vedantu.notification.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;


public class UserDevice extends AbstractMongoStringIdEntity {

	
	private String regId;		
	private String deviceId;
	private Long userId;
	

	public UserDevice() {
		super();
	}
	
	public String getRegId() {
		return regId;
	}

	public void setRegId(String regId) {
		this.regId = regId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public UserDevice(String regId, String deviceId, Long userId) {
		this.regId = regId;
		this.deviceId = deviceId;
		this.userId = userId;
	}

	
	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String REGISTRATIONID = "registrationId";
		public static final String DEVICEID = "deviceId";
		public static final String USERID = "userId";
	}
	
}
