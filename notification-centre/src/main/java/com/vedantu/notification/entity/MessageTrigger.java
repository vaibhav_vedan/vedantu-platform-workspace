/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.notification.enums.TriggerStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author jeet
 */
@Document(collection = "MessageTrigger")
@CompoundIndexes({ @CompoundIndex(name = "studentId_teacherId", def = "{'studentId' : 1, 'teacherId': 1}") })
public class MessageTrigger extends AbstractMongoStringIdEntity {

	private Long messageId;

	private Long studentId;

	private Long teacherId;

	private TriggerStatus status;

	private Long triggerTime;

	private boolean alertSent;

	public MessageTrigger() {
		super();
	}

	public MessageTrigger(Long messageId, Long studentId, Long teacherId,
			TriggerStatus status, Long triggerTime, boolean alertSent) {
		super();
		this.messageId = messageId;
		this.studentId = studentId;
		this.teacherId = teacherId;
		this.status = status;
		this.triggerTime = triggerTime;
		this.alertSent = alertSent;
	}

	public Long getMessageId() {
		return messageId;
	}

	public void setMessageId(Long messageId) {
		this.messageId = messageId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public TriggerStatus getStatus() {
		return status;
	}

	public void setStatus(TriggerStatus status) {
		this.status = status;
	}

	public Long getTriggerTime() {
		return triggerTime;
	}

	public void setTriggerTime(Long triggerTime) {
		this.triggerTime = triggerTime;
	}

	public boolean isAlertSent() {
		return alertSent;
	}

	public void setAlertSent(boolean alertSent) {
		this.alertSent = alertSent;
	}
        
	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String MESSAGE_ID = "messageId";
		public static final String STUDENT_ID = "studentId";
		public static final String TEACHER_ID = "teacherId";
		public static final String STATUS = "status";
		public static final String TRIGGER_TIME = "triggerTime";
		public static final String ALERT_SENT = "alertSent";		
	}
}

