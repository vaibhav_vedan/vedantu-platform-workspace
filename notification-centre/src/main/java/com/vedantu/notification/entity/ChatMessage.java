/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author jeet
 */
@Document(collection = "ChatMessage")
@CompoundIndexes({
    @CompoundIndex(name = "channelName_senderTime", def = "{'channel' : 1 ,'senderTime' : 1 }", background = true)
})
public class ChatMessage extends AbstractMongoStringIdEntity {
    
    private String channel;
    private String messageId;
    private Long serverTime;
    private Long senderTime;
    private Long targetUserId;
    private Long senderId;
    private String senderName;
    private String data;

    public ChatMessage() {
        super();
    }

    public ChatMessage(String channel, Long serverTime, Long senderTime, Long targetUserId, Long senderId, String senderName, String data) {
        super();
        this.channel = channel;
        this.serverTime = serverTime;
        this.senderTime = senderTime;
        this.targetUserId = targetUserId;
        this.senderId = senderId;
        this.senderName = senderName;
        this.data = data;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public Long getServerTime() {
        return serverTime;
    }

    public void setServerTime(Long serverTime) {
        this.serverTime = serverTime;
    }

    public Long getSenderTime() {
        return senderTime;
    }

    public void setSenderTime(Long senderTime) {
        this.senderTime = senderTime;
    }

    public Long getTargetUserId() {
        return targetUserId;
    }

    public void setTargetUserId(Long targetUserId) {
        this.targetUserId = targetUserId;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String messageId) {
        this.messageId = messageId;
    }

    @Override
    public String toString() {
        return "ChatMessage{" + "channel=" + channel + ", messageId=" + messageId + ", serverTime=" + serverTime + ", senderTime=" + senderTime + ", targetUserId=" + targetUserId + ", senderId=" + senderId + ", senderName=" + senderName + ", data=" + data + '}';
    }
    
    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String CHANNEL = "channel";
        public static final String SERVER_TIME = "serverTime";
        public static final String SENDER_TIME = "senderTime";
        public static final String TARGET_USER_ID = "targetUserId";
        public static final String SENDER_ID = "senderId";
        public static final String SENDER_NAME = "senderName";
    }

}
