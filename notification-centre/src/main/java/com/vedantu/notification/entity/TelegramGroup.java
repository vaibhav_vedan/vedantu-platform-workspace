/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class TelegramGroup extends AbstractMongoStringIdEntity {
    private Long telegramId;
    private String title;
    private String type;
    private Set<String> batchIds;
    private String telegramJoinLink;
    private Set<Long> participantUsers = new HashSet<>();
    private Set<Long> bannedUsers = new HashSet<>();
    private Set<Long> exceptionUsers = new HashSet<>();
    
    /**
     * @return the telegramId
     */
    public Long getTelegramId() {
        return telegramId;
    }

    /**
     * @param telegramId the telegramId to set
     */
    public void setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the batchIds
     */
    public Set<String> getBatchIds() {
        return batchIds;
    }

    /**
     * @param batchIds the batchIds to set
     */
    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    /**
     * @return the telegramJoinLink
     */
    public String getTelegramJoinLink() {
        return telegramJoinLink;
    }

    /**
     * @param telegramJoinLink the telegramJoinLink to set
     */
    public void setTelegramJoinLink(String telegramJoinLink) {
        this.telegramJoinLink = telegramJoinLink;
    }

    /**
     * @return the participantUsers
     */
    public Set<Long> getParticipantUsers() {
        return participantUsers;
    }

    /**
     * @param participantUsers the participantUsers to set
     */
    public void setParticipantUsers(Set<Long> participantUsers) {
        this.participantUsers = participantUsers;
    }

    /**
     * @return the bannedUsers
     */
    public Set<Long> getBannedUsers() {
        return bannedUsers;
    }

    /**
     * @param bannedUsers the bannedUsers to set
     */
    public void setBannedUsers(Set<Long> bannedUsers) {
        this.bannedUsers = bannedUsers;
    }

    /**
     * @return the exceptionUsers
     */
    public Set<Long> getExceptionUsers() {
        return exceptionUsers;
    }

    /**
     * @param exceptionUsers the exceptionUsers to set
     */
    public void setExceptionUsers(Set<Long> exceptionUsers) {
        this.exceptionUsers = exceptionUsers;
    }
    
    
}
