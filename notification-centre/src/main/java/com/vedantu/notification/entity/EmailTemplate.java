package com.vedantu.notification.entity;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author kasireddy
 */

@Document(collection = "EmailTemplate")
@CompoundIndexes({
        @CompoundIndex(name = "emailType_1_role_1", def = "{'emailType':1,'role':1}", unique = true, background = true)
})
public class EmailTemplate extends AbstractMongoStringIdEntity {

    private CommunicationType emailType;
    private Role role;
    private Boolean unSubscriptionCheck;
    private Boolean unSubscriptionLink;
    private String description;     // describes about this email. like -> email to inform about session...., like that.


    // TODO: Add body field which will store the html content

    public EmailTemplate(){
        super();
    }

    public EmailTemplate(CommunicationType emailType, Role role, Boolean unSubscriptionCheck, Boolean unSubscriptionLink, String description){
        this.emailType = emailType;
        this.role = role;
        this.unSubscriptionCheck = unSubscriptionCheck;
        this.unSubscriptionLink = unSubscriptionLink;
        this.description = description;
    }

    public CommunicationType getEmailType() {
        return emailType;
    }

    public void setEmailType(CommunicationType emailType) {
        this.emailType = emailType;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getUnSubscriptionCheck() {
        return unSubscriptionCheck;
    }

    public void setUnSubscriptionCheck(Boolean unSubscriptionCheck) {
        this.unSubscriptionCheck = unSubscriptionCheck;
    }

    public Boolean getUnSubscriptionLink() {
        return unSubscriptionLink;
    }

    public void setUnSubscriptionLink(Boolean unSubscriptionLink) {
        this.unSubscriptionLink = unSubscriptionLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants{
        public static final String EMAIL_TYPE = "emailType";
        public static final String ROLE = "role";
        public static final String UNSUBSCRIPTION_CHECK = "unSubscriptionCheck";
        public static final String UNSUBSCRIPTION_LINK = "unSubscriptionLink";
        public static final String DESCRIPTION = "description";
    }
}
