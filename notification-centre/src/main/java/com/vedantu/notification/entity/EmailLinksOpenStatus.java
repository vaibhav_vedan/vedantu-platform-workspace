/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

/**
 *
 * @author ajith
 */
public class EmailLinksOpenStatus {

    private String randomId;
    private String linkUrl;
    private String linkName;
    private boolean clicked;

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    @Override
    public String toString() {
        return "EmailLinksOpenStatus{" + "randomId=" + randomId + ", url=" + linkUrl + ", linkName=" + linkName + ", clicked=" + clicked + '}';
    }

}
