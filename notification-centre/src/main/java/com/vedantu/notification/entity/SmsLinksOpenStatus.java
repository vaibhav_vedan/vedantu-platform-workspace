package com.vedantu.notification.entity;

public class SmsLinksOpenStatus {

    private String randomId;
    private String linkUrl;
    private String linkName;
    private boolean clicked;
    private Long lastUpdated;
    private Long creationTime;

    public String getRandomId() {
        return randomId;
    }

    public void setRandomId(String randomId) {
        this.randomId = randomId;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public Long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Long getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(Long creationTime) {
        this.creationTime = creationTime;
    }

    @Override
    public String toString() {
        return "SmsLinksOpenStatus{" + "randomId=" + randomId + ", linkUrl=" + linkUrl + ", linkName=" + linkName + ", clicked=" + clicked + ", lastUpdated=" + lastUpdated + ", creationTime=" + creationTime + '}';
    }

}
