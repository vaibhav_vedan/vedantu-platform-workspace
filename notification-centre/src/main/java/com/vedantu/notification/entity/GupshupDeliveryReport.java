/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
@Document(collection = "GupshupDeliveryReport")
@CompoundIndexes({
    @CompoundIndex(name = "lastUpdated_1__id_1", def = "{'lastUpdated' : 1, '_id': 1}", background = true)
})
public class GupshupDeliveryReport extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String externalId;
    private Long deliveredTS;
    private String status;
    private String phoneNo;
    private String cause;
    private String smsText;
    private boolean isInternational;
    private boolean highPriority;
    @Indexed(name = "manualNotificationIdIndex", direction = IndexDirection.DESCENDING, background = true)
    private String manualNotificationId;

    private List<SmsLinksOpenStatus> smsLinks;
    private CommunicationType type;
    private Role role;

    private String sqsMessageId;

    public List<SmsLinksOpenStatus> getSmsLinks() {
        if (smsLinks == null) {
            smsLinks = new ArrayList<>();
        }
        return smsLinks;
    }

    public String getSqsMessageId() {
        return sqsMessageId;
    }

    public void setSqsMessageId(String sqsMessageId) {
        this.sqsMessageId = sqsMessageId;
    }

    public void addSmsLink(SmsLinksOpenStatus link) {
        this.getSmsLinks().add(link);
    }

    public void setSmsLinks(List<SmsLinksOpenStatus> smsLinks) {
        this.smsLinks = smsLinks;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Long getDeliveredTS() {
        return deliveredTS;
    }

    public void setDeliveredTS(Long deliveredTS) {
        this.deliveredTS = deliveredTS;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getSmsText() {
        return smsText;
    }

    public void setSmsText(String smsText) {
        this.smsText = smsText;
    }

    public boolean isHighPriority() {
        return highPriority;
    }

    public void setHighPriority(boolean highPriority) {
        this.highPriority = highPriority;
    }

    public boolean getIsInternational() {
        return isInternational;
    }

    public void setIsInternational(boolean isInternational) {
        this.isInternational = isInternational;
    }


    public String getManualNotificationId() {
        return manualNotificationId;
    }

    public void setManualNotificationId(String manualNotificationId) {
        this.manualNotificationId = manualNotificationId;
    }

    public CommunicationType getType() {
        return type;
    }

    public void setType(CommunicationType type) {
        this.type = type;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String MANUAL_NOTIFICATION_ID = "manualNotificationId";
    }
}
