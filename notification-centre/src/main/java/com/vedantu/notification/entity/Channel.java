package com.vedantu.notification.entity;

import com.vedantu.notification.enums.ChannelType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class Channel extends AbstractMongoStringIdEntity{
	
	private Long[] participants;
	private ChannelType channelType;

	
	public Channel() {
		super();
	}

	public Channel(Long[] participants, ChannelType channelType) {
		super();
		this.participants = participants;
		this.channelType = channelType;
	}

	
	public Long[] getParticipants() {
		return participants;
	}

	public void setParticipants(Long[] participants) {
		this.participants = participants;
	}

	

	public ChannelType getChannelType() {
		return channelType;
	}

	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}
}
