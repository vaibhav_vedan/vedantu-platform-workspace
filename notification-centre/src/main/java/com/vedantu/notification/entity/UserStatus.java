package com.vedantu.notification.entity;

import com.vedantu.notification.enums.StatusType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class UserStatus extends AbstractMongoStringIdEntity{
	
	private Long userId;
	private StatusType statusType;
	
	public UserStatus() {
		super();
	}

	public UserStatus(Long userId, StatusType statusType) {
		super();
		this.userId = userId;
		this.statusType = statusType;
	}

	@Override
	public String toString() {
		return "UserStatus userId:" + userId + ", statusType:" + statusType;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public StatusType getStatusType() {
		return statusType;
	}

	public void setStatusType(StatusType statusType) {
		this.statusType = statusType;
	}
	
}
