/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.notification.enums.VedantuApp;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "MobileRegToken")
public class MobileRegToken extends AbstractMongoStringIdEntity {

    private String regToken;
    @Indexed(background = true)
    private String deviceId;
    private VedantuApp app;

    public MobileRegToken() {
        super();
    }

    public String getRegToken() {
        return regToken;
    }

    public void setRegToken(String regToken) {
        this.regToken = regToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public VedantuApp getApp() {
        return app;
    }

    public void setApp(VedantuApp app) {
        this.app = app;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String REG_TOKEN = "regToken";
        public static final String DEVICEID = "deviceId";
        public static final String APP = "app";
    }

}
