package com.vedantu.notification.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "BlockedPhone")
@CompoundIndexes({
        @CompoundIndex(name = "contactNumber_phoneCode", def = "{'contactNumber' : 1, 'phoneCode': 1}", background = true, unique = true),
        @CompoundIndex(name = "creationTime", def = "{'creationTime' : -1}", background = true)})
public class BlockedPhone extends AbstractMongoStringIdEntity {
    private String contactNumber;
    private String phoneCode;
    private Boolean blocked;
    private String type;
    private String ipAddress;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getBlocked() {
        return blocked;
    }

    public void setBlocked(Boolean blocked) {
        this.blocked = blocked;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String CONTACT_NUMBER = "contactNumber";
        public static final String PHONE_CODE = "phoneCode";
        public static final String BLOCKED = "blocked";
        public static final String TYPE = "type";
        public static final String IP_ADDRESS = "ipAddress";
    }
}
