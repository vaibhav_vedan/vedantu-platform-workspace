/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.User.VerificationLinkStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author parashar
 */
@Document(collection = "TelegramVerificationToken")
public class TelegramVerificationToken extends AbstractMongoStringIdEntity {


    public static final int VERIFICATION_CODE_LENGTH = 5;
    
    private Long telegramId;
    private String email;
    private VerificationLinkStatus status;
    
    @Indexed(background = true)
    private String code;    

    /**
     * @return the telegramId
     */
    public Long getTelegramId() {
        return telegramId;
    }

    /**
     * @param telegramId the telegramId to set
     */
    public void setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the status
     */
    public VerificationLinkStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(VerificationLinkStatus status) {
        this.status = status;
    }
    
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }    
    
}
