/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.User.Role;
import com.vedantu.notification.pojo.TelegramUserPojo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author parashar
 */
@Document(collection = "Email")
public class TelegramUser extends AbstractMongoStringIdEntity{
    
    private Long telegramId;
    private Long userId;
    private String email;
    private String username;
    private TelegramUserPojo telegramUserPojo;
    private Set<String> batchIds = new HashSet<>();
    private List<Long> groupIds;
    private Role role = Role.STUDENT;
    
    /**
     * @return the telegramId
     */
    public Long getTelegramId() {
        return telegramId;
    }

    /**
     * @param telegramId the telegramId to set
     */
    public void setTelegramId(Long telegramId) {
        this.telegramId = telegramId;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the batchIds
     */
    public Set<String> getBatchIds() {
        return batchIds;
    }

    /**
     * @param batchIds the batchIds to set
     */
    public void setBatchIds(Set<String> batchIds) {
        this.batchIds = batchIds;
    }

    /**
     * @return the groupIds
     */
    public List<Long> getGroupIds() {
        return groupIds;
    }

    /**
     * @param groupIds the groupIds to set
     */
    public void setGroupIds(List<Long> groupIds) {
        this.groupIds = groupIds;
    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the telegramUserPojo
     */
    public TelegramUserPojo getTelegramUserPojo() {
        return telegramUserPojo;
    }

    /**
     * @param telegramUserPojo the telegramUserPojo to set
     */
    public void setTelegramUserPojo(TelegramUserPojo telegramUserPojo) {
        this.telegramUserPojo = telegramUserPojo;
    }
    
    
    
}
