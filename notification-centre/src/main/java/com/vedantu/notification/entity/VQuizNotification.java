package com.vedantu.notification.entity;

import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.*;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author MNPK
 */

@Getter
@Setter
@Data
@Builder
@NoArgsConstructor
@Document(collection = "VQuizNotification")
@CompoundIndexes({
        @CompoundIndex(name = "userId_1_quizId_1", def = "{'userId': 1, 'quizId': 1}", background = true, unique = true),
        @CompoundIndex(name = "scheduleTime_1_status_1", def = "{'scheduleTime': 1, 'status': 1}", background = true)
})

public class VQuizNotification extends AbstractMongoStringIdEntity {
    public Long userId;
    public String quizId;
    private Long scheduleTime;
    private NotificationStatus status;

    public VQuizNotification(Long userId, String quizId, Long scheduleTime, NotificationStatus status) {
        this.userId = userId;
        this.quizId = quizId;
        this.scheduleTime = scheduleTime;
        this.status = status;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String USER_ID = "userId";
        public static final String QUIZ_ID = "quizId";
        public static final String SCHEDULE_TIME = "scheduleTime";
        public static final String STATUS = "status";
    }
}
