/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class TelegramDoubt extends AbstractMongoStringIdEntity{
    
    private Long telegramMessageId;
    private String message;
    private Long userId;
    private Long telegramUserId;
    private Long telegramChatId;
    private String batchId;
    private List<Long> repliedMessageIds = new ArrayList<>(); 
    private boolean closed = false;
    private Long closedBy;

    /**
     * @return the telegramMessageId
     */
    public Long getTelegramMessageId() {
        return telegramMessageId;
    }

    /**
     * @param telegramMessageId the telegramMessageId to set
     */
    public void setTelegramMessageId(Long telegramMessageId) {
        this.telegramMessageId = telegramMessageId;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the telegramUserId
     */
    public Long getTelegramUserId() {
        return telegramUserId;
    }

    /**
     * @param telegramUserId the telegramUserId to set
     */
    public void setTelegramUserId(Long telegramUserId) {
        this.telegramUserId = telegramUserId;
    }

    /**
     * @return the telegramChatId
     */
    public Long getTelegramChatId() {
        return telegramChatId;
    }

    /**
     * @param telegramChatId the telegramChatId to set
     */
    public void setTelegramChatId(Long telegramChatId) {
        this.telegramChatId = telegramChatId;
    }

    /**
     * @return the repliedMessageIds
     */
    public List<Long> getRepliedMessageIds() {
        return repliedMessageIds;
    }

    /**
     * @param repliedMessageIds the repliedMessageIds to set
     */
    public void setRepliedMessageIds(List<Long> repliedMessageIds) {
        this.repliedMessageIds = repliedMessageIds;
    }

    /**
     * @return the closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param closed the closed to set
     */
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the closedBy
     */
    public Long getClosedBy() {
        return closedBy;
    }

    /**
     * @param closedBy the closedBy to set
     */
    public void setClosedBy(Long closedBy) {
        this.closedBy = closedBy;
    }
    
    
}
