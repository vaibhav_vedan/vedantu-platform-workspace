package com.vedantu.notification.entity;

import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.CommunicationKind;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "ManualNotification")
public class ManualNotification extends AbstractMongoStringIdEntity {

	private CommunicationKind communicationKind;
	private String subject;
	private String body;

	@Indexed(name="scheduledTimeIndex",direction = IndexDirection.DESCENDING,background = true)
	private long scheduledTime;

	private List<String> userIds;
	private List<String> batchIds;
	private List<String> otmBundleIds;
	private List<String> aioPackageIds;
	private List<String> allUserIds;
	private List<String> emailIds;

	private NotificationStatus status;

	private long expiryTime;

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String NOTIFICATION_STATUS="status";
		public static final String SCHEDULED_TIME="scheduledTime";
		public static final String COMMUNICATION_KIND="communicationKind";
		public static final String EXPIRY_TIME="expiryTime";

	}
}
