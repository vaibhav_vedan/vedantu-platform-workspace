/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author jeet
 */
@Document(collection = "AblyActiveChannel")
public class AblyActiveChannel extends AbstractMongoStringIdEntity{
    
    @Indexed(background = true)
    private String channel;
    
    private boolean deleted;
    
    @Indexed(background = true)
    private Long processedTime;
    
    public AblyActiveChannel(){
        super();
    }

    public AblyActiveChannel(String channel, boolean deleted) {
        this.channel = channel;
        this.deleted = deleted;
    }
    
    public AblyActiveChannel(String channel) {
        this.channel = channel;
        this.deleted = false;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public Long getProcessedTime() {
        return processedTime;
    }

    public void setProcessedTime(Long processedTime) {
        this.processedTime = processedTime;
    }

    @Override
    public String toString() {
        return "AblyActiveChannel{" + "channel=" + channel + ", deleted=" + deleted + ", processedTime=" + processedTime + '}';
    }
    
    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String CHANNEL = "channel";
        public static final String DELETED = "deleted";
        public static final String PROCESSED_TIME = "processedTime";
    }
}
