/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.entity;

import com.vedantu.notification.pojo.TelegramChatPojo;
import com.vedantu.notification.pojo.TelegramEntity;
import com.vedantu.notification.pojo.TelegramPhotoPojo;
import com.vedantu.notification.pojo.TelegramUserPojo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.List;

/**
 *
 * @author parashar
 */
public class TelegramMessage extends AbstractMongoStringIdEntity {
    
    private Long message_id;
    private TelegramUserPojo from;
    private TelegramUserPojo left_chat_participant;
    private TelegramUserPojo left_chat_member;
    private TelegramUserPojo new_chat_participant;
    private TelegramUserPojo new_chat_member;
    private List<TelegramUserPojo> new_chat_members;
    private Long date;
    private String text;
    private TelegramChatPojo chat;
    private List<TelegramEntity> entities;
    private String caption;
    private List<TelegramEntity> caption_entities;
    private List<TelegramPhotoPojo> photo;
    private TelegramMessage reply_to_message;
    

    /**
     * @return the message_id
     */
    public Long getMessage_id() {
        return message_id;
    }

    /**
     * @param message_id the message_id to set
     */
    public void setMessage_id(Long message_id) {
        this.message_id = message_id;
    }

    /**
     * @return the from
     */
    public TelegramUserPojo getFrom() {
        return from;
    }

    /**
     * @param from the from to set
     */
    public void setFrom(TelegramUserPojo from) {
        this.from = from;
    }

    /**
     * @return the left_chat_participant
     */
    public TelegramUserPojo getLeft_chat_participant() {
        return left_chat_participant;
    }

    /**
     * @param left_chat_participant the left_chat_participant to set
     */
    public void setLeft_chat_participant(TelegramUserPojo left_chat_participant) {
        this.left_chat_participant = left_chat_participant;
    }

    /**
     * @return the left_chat_member
     */
    public TelegramUserPojo getLeft_chat_member() {
        return left_chat_member;
    }

    /**
     * @param left_chat_member the left_chat_member to set
     */
    public void setLeft_chat_member(TelegramUserPojo left_chat_member) {
        this.left_chat_member = left_chat_member;
    }

    /**
     * @return the new_chat_participant
     */
    public TelegramUserPojo getNew_chat_participant() {
        return new_chat_participant;
    }

    /**
     * @param new_chat_participant the new_chat_participant to set
     */
    public void setNew_chat_participant(TelegramUserPojo new_chat_participant) {
        this.new_chat_participant = new_chat_participant;
    }

    /**
     * @return the new_chat_member
     */
    public TelegramUserPojo getNew_chat_member() {
        return new_chat_member;
    }

    /**
     * @param new_chat_member the new_chat_member to set
     */
    public void setNew_chat_member(TelegramUserPojo new_chat_member) {
        this.new_chat_member = new_chat_member;
    }

    /**
     * @return the new_chat_members
     */
    public List<TelegramUserPojo> getNew_chat_members() {
        return new_chat_members;
    }

    /**
     * @param new_chat_members the new_chat_members to set
     */
    public void setNew_chat_members(List<TelegramUserPojo> new_chat_members) {
        this.new_chat_members = new_chat_members;
    }

    /**
     * @return the date
     */
    public Long getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(Long date) {
        this.date = date;
    }

    /**
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text the text to set
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return the chat
     */
    public TelegramChatPojo getChat() {
        return chat;
    }

    /**
     * @param chat the chat to set
     */
    public void setChat(TelegramChatPojo chat) {
        this.chat = chat;
    }

    /**
     * @return the entities
     */
    public List<TelegramEntity> getEntities() {
        return entities;
    }

    /**
     * @param entities the entities to set
     */
    public void setEntities(List<TelegramEntity> entities) {
        this.entities = entities;
    }

    /**
     * @return the caption
     */
    public String getCaption() {
        return caption;
    }

    /**
     * @param caption the caption to set
     */
    public void setCaption(String caption) {
        this.caption = caption;
    }

    /**
     * @return the caption_entities
     */
    public List<TelegramEntity> getCaption_entities() {
        return caption_entities;
    }

    /**
     * @param caption_entities the caption_entities to set
     */
    public void setCaption_entities(List<TelegramEntity> caption_entities) {
        this.caption_entities = caption_entities;
    }

    /**
     * @return the photo
     */
    public List<TelegramPhotoPojo> getPhoto() {
        return photo;
    }

    /**
     * @param photo the photo to set
     */
    public void setPhoto(List<TelegramPhotoPojo> photo) {
        this.photo = photo;
    }

    @Override
    public String toString() {
        return "TelegramMessage{" + "message_id=" + message_id + ", from=" + from + ", left_chat_participant=" + left_chat_participant + ", left_chat_member=" + left_chat_member + ", new_chat_participant=" + new_chat_participant + ", new_chat_member=" + new_chat_member + ", new_chat_members=" + new_chat_members + ", date=" + date + ", text=" + text + ", chat=" + chat + ", entities=" + entities + ", caption=" + caption + ", caption_entities=" + caption_entities + ", photo=" + photo + ", reply_to_message=" + reply_to_message + '}';
    }

    

    /**
     * @return the reply_to_message
     */
    public TelegramMessage getReply_to_message() {
        return reply_to_message;
    }

    /**
     * @param reply_to_message the reply_to_message to set
     */
    public void setReply_to_message(TelegramMessage reply_to_message) {
        this.reply_to_message = reply_to_message;
    }
    
}
