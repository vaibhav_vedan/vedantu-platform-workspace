package com.vedantu.notification.entity;

import java.util.List;


import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.pojo.ButtonData;
import com.vedantu.notification.request.MobileNotificationDataRequest;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

/*
 * Extra notification data for mobile
 */
public class MobileNotificationData extends AbstractMongoStringIdEntity {

	private String title;
	private String description;
	private String imageUrl;
	private String landingActivity;
	private Long timestamp;
	private String extra;
	private List<ButtonData> buttons;
	private NotificationType type;
	
	public MobileNotificationData() {
		super();
	}
	
	public MobileNotificationData(String title, String description, String imageUrl, 
			String landingActivity, Long timestamp, String extra, List<ButtonData> buttons) {
		this.title = title;
		this.description = description;
		this.imageUrl = imageUrl;
		this.landingActivity = landingActivity;
		this.timestamp = timestamp;
		this.extra = extra;
		this.buttons = buttons;
	}
        
        public MobileNotificationData(MobileNotificationDataRequest req){
                this.title = req.getTitle();
		this.description = req.getDescription();
		this.imageUrl = req.getImageUrl();
		this.landingActivity = req.getLandingActivity();
		this.timestamp = req.getTimestamp();
		this.extra = req.getExtra();
		this.buttons = req.getButtons();
                this.type=req.getType();
        }
	
	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLandingActivity() {
		return landingActivity;
	}

	public void setLandingActivity(String landingActivity) {
		this.landingActivity = landingActivity;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public List<ButtonData> getButtons() {
		return buttons;
	}

	public void setButtons(List<ButtonData> buttons) {
		this.buttons = buttons;
	}

	@Override
	public String toString() {	
		
		return "MobileNotification title:" + title +  ", type:" + type.toString();
		
	}
	
	
	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String TITLE = "title";
		public static final String DESCRIPTION = "description";
		public static final String TIMESTAMP = "timestamp";
		public static final String TYPE = "type";
		public static final String EXTRA = "extra";
		public static final String BUTTONS = "b";

	}
	
}
