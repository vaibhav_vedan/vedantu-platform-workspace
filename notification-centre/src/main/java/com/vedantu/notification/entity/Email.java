package com.vedantu.notification.entity;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import javax.mail.Address;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Email")
@CompoundIndexes({
    @CompoundIndex(name = "lastUpdated_-1", def = "{'lastUpdated': -1}", background = true)
})
public class Email extends AbstractMongoStringIdEntity {

    private Address to;
    private String subject;
    private CommunicationType type;
    private boolean opened;
    private boolean clicked;
    private List<EmailLinksOpenStatus> emailLinks;
    private Set<String> tags;
    private Role role;
    private String sqsMessageId;

    @Indexed(name="manualNotificationIdIndex",direction = IndexDirection.DESCENDING,background = true)
    private String manualNotificationId;

    public String getSqsMessageId() {
        return sqsMessageId;
    }

    public void setSqsMessageId(String sqsMessageId) {
        this.sqsMessageId = sqsMessageId;
    }

    public Address getTo() {
        return to;
    }

    public void setTo(Address to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public CommunicationType getType() {
        return type;
    }

    public void setType(CommunicationType type) {
        this.type = type;
    }

    public boolean isOpened() {
        return opened;
    }

    public void setOpened(boolean opened) {
        this.opened = opened;
    }

    public boolean isClicked() {
        return clicked;
    }

    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }

    public List<EmailLinksOpenStatus> getEmailLinks() {
        if (emailLinks == null) {
            emailLinks = new ArrayList<>();
        }
        return emailLinks;
    }

    public void addEmailLink(EmailLinksOpenStatus link) {
        this.getEmailLinks().add(link);
    }

    public void setEmailLinks(List<EmailLinksOpenStatus> emailLinks) {
        this.emailLinks = emailLinks;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public String getManualNotificationId() {
        return manualNotificationId;
    }

    public void setManualNotificationId(String manualNotificationId) {
        this.manualNotificationId = manualNotificationId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String TO = "to";
        public static final String SUBJECT = "subject";
        public static final String TYPE = "type";
        public static final String LINK_ATTRITUBE_TRACK = "track";
        public static final String LINK_ATTRITUBE_HREF = "href";
        public static final String LINK_ATTRITUBE_TRACK_NAME = "trackName";
        public static final String MANUAL_NOTIFICATION_ID = "manualNotificationId";

    }

    /**
     * @return the role
     */
    public Role getRole() {
        return role;
    }

    /**
     * @param role the role to set
     */
    public void setRole(Role role) {
        this.role = role;
    }

}
