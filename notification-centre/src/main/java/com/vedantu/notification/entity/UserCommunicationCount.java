package com.vedantu.notification.entity;


import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "UserCommunicationCount")
@CompoundIndexes(
        {
                @CompoundIndex(name = "userAddress_1_date_-1_communicationType_1", def = "{userAddress:1, date:-1, communicationType:1}", background = true)
        }
)
public class UserCommunicationCount extends AbstractMongoStringIdEntity {
    private String userAddress; // phone or email
    private String date;    // format is yyyy-MM-dd
    private CommunicationType communicationType;
    private int count;  // per that day

    public UserCommunicationCount(){
        super();
    }

    public UserCommunicationCount(String userAddress, String date, CommunicationType communicationType){
        this.userAddress = userAddress;
        this.date = date;
        this.communicationType = communicationType;
        this.count = 1;
    }

    public String getUserAddress() {
        return userAddress;
    }

    public void setUserAddress(String userAddress) {
        this.userAddress = userAddress;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public CommunicationType getCommunicationType() {
        return communicationType;
    }

    public void setCommunicationType(CommunicationType communicationType) {
        this.communicationType = communicationType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants{
        public static final String USER_ADDRESS = "userAddress";
        public static final String DATE = "date";
        public static final String COMMUNICATION_TYPE = "communicationType";
        public static final String COUNT = "count";
    }

}
