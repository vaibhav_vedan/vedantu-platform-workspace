package com.vedantu.notification.entity;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * @author kasireddy
 */

@Document(collection = "SmsTemplate")
@CompoundIndexes({
        @CompoundIndex(name = "smsType_1_role_1", def = "{'smsType':1, 'role':1}", unique = true, background = true)
})
public class SmsTemplate extends AbstractMongoStringIdEntity {

    private CommunicationType smsType;
    private Role role;
    private String body;
    private Boolean unSubscriptionCheck;
    private Boolean unSubscriptionLink;
    private String description;     // describes about this sms. like -> sms to inform about session...., like that.


    public SmsTemplate() {
        super();
    }

    public SmsTemplate(CommunicationType smsType, Role role, String body, Boolean unSubscriptionCheck, Boolean unSubscriptionLink, String description){
        this.smsType = smsType;
        this.role = role;
        this.body = body;
        this.unSubscriptionCheck = unSubscriptionCheck;
        this.unSubscriptionLink = unSubscriptionLink;
        this.description = description;
    }

    public CommunicationType getSmsType() {
        return smsType;
    }

    public void setSmsType(CommunicationType smsType) {
        this.smsType = smsType;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Boolean getUnSubscriptionCheck() {
        return unSubscriptionCheck;
    }

    public void setUnSubscriptionCheck(Boolean unSubscriptionCheck) {
        this.unSubscriptionCheck = unSubscriptionCheck;
    }

    public Boolean getUnSubscriptionLink() {
        return unSubscriptionLink;
    }

    public void setUnSubscriptionLink(Boolean unSubscriptionLink) {
        this.unSubscriptionLink = unSubscriptionLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants{
        public static final String SMS_TYPE = "smsType";
        public static final String ROLE = "role";
        public static final String BODY = "body";
        public static final String UNSUBSCRIPTION_CHECK = "unSubscriptionCheck";
        public static final String UNSUBSCRIPTION_LINK = "unSubscriptionLink";
        public static final String DESCRIPTION = "description";
    }
}
