package com.vedantu.notification.entity;

import com.vedantu.notification.enums.EmailNotificationSubType;

import com.vedantu.notification.enums.EmailNotificationType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class EmailNotification extends AbstractMongoStringIdEntity {

	private String emailAddress;
	private EmailNotificationType notificationType;
        private EmailNotificationSubType subType;
        private String data;
	private String feedbackId;

    public EmailNotificationSubType getSubType() {
        return subType;
    }

    public void setSubType(EmailNotificationSubType subType) {
        this.subType = subType;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public EmailNotificationType getNotificationType() {
		return notificationType;
	}

	public void setNotificationType(EmailNotificationType notificationType) {
		this.notificationType = notificationType;
	}

	public String getFeedbackId() {
		return feedbackId;
	}

	public void setFeedbackId(String feedbackId) {
		this.feedbackId = feedbackId;
	}

	public EmailNotification(String emailAddress, EmailNotificationType notificationType, EmailNotificationSubType subType, String data, String feedbackId) {
		super();
		this.emailAddress = emailAddress;
		this.notificationType = notificationType;
                this.subType = subType;
                this.data = data;
		this.feedbackId = feedbackId;
	}

	public EmailNotification() {
		super();
		// TODO Auto-generated constructor stub
	}

}
