package com.vedantu.notification.entity;

import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "NoticeBoard")
@CompoundIndexes({
        @CompoundIndex(name = "creationTime_1", def = "{'creationTime': -1}", background = true)
})
public class NoticeBoard extends AbstractMongoStringIdEntity {
    @Indexed(background = true)
    private String studentId;
    private String message;
    private long expiryDate;
    private long scheduledTime;
    private NotificationStatus notificationStatus=NotificationStatus.SCHEDULED;

    @Indexed(background = true)
    private String manualNotificationId;

    public NoticeBoard(){

    }

    public NoticeBoard(String studentId, String message, long scheduledTime, long expiryDate,String manualNotificationId) {
        this.studentId = studentId;
        this.message = message;
        this.scheduledTime=scheduledTime;
        this.expiryDate = expiryDate;
        this.manualNotificationId=manualNotificationId;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(long expiryDate) {
        this.expiryDate = expiryDate;
    }

    public NotificationStatus getNotificationStatus() {
        return notificationStatus;
    }

    public void setNotificationStatus(NotificationStatus notificationStatus) {
        this.notificationStatus = notificationStatus;
    }

    public long getScheduledTime() {
        return scheduledTime;
    }

    public void setScheduledTime(long scheduledTime) {
        this.scheduledTime = scheduledTime;
    }

    public String getManualNotificationId() {
        return manualNotificationId;
    }

    public void setManualNotificationId(String manualNotificationId) {
        this.manualNotificationId = manualNotificationId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String STUDENT_ID = "studentId";
        public static final String NOTIFICATION_STATUS = "notificationStatus";
        public static final String EXPIRY_DATE="expiryDate";
        public static final String SCHEDULED_TIME="scheduledTime";
        public static final String IS_DISCARDED="isDiscarded";
        public static final String MANUAL_NOTIFICATION_ID="manualNotificationId";
    }
}
