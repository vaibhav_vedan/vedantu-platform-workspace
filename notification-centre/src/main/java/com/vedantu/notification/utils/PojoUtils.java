/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.utils;

import com.vedantu.notification.entity.Notification;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author somil
 */
@Service
public class PojoUtils {

    @Autowired
    private DozerBeanMapper mapper;      

    public com.vedantu.notification.requests.Notification convertToNotificationPojo(Notification notification) {
        com.vedantu.notification.requests.Notification _notification = mapper.map(notification, com.vedantu.notification.requests.Notification.class);
        return _notification;
    }

}
