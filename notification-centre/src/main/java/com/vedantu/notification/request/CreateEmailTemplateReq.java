package com.vedantu.notification.request;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class CreateEmailTemplateReq extends AbstractFrontEndReq {

    private CommunicationType emailType;
    private Role role;
    private Boolean unSubscriptionCheck = Boolean.TRUE;
    private Boolean unSubscriptionLink = Boolean.TRUE;
    private String description;

    // TODO: take body which is html content from admin


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(emailType == null)
            errors.add("emailType is mandatory");
        if(StringUtils.isEmpty(description))
            errors.add("description of sms is mandatory");
        return errors;
    }
}
