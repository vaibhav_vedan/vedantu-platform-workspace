package com.vedantu.notification.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/*
 * Incoming request object for sendSMS API
 */
public class UpdateLastChattedTimeRequest extends AbstractFrontEndReq {

	private Long userId;
	private String channelName;
	
	@Override
	public String toString() {
		return "UpdateLastChattedTimeRequest userId:" + userId + ", channelName:" + channelName + "]";
	}

	public UpdateLastChattedTimeRequest() {
		super();
	}

	public UpdateLastChattedTimeRequest(Long userId, String channelName) {
		super();
		this.userId = userId;
		this.channelName = channelName;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public static class Constants{
		public static final String USERID = "userId";
		public static final String CHANNELNAME = "channelName";
	}
	
	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		if (null == userId) {
			errors.add(UpdateLastChattedTimeRequest.Constants.USERID);
		}
		
		if (null == channelName) {
			errors.add(UpdateLastChattedTimeRequest.Constants.CHANNELNAME);
		}
		
		return errors;
	}
}
