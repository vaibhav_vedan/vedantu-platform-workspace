/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

import com.vedantu.notification.pojo.TelegramUserPojo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author parashar
 */
public class TelegramUserVerificationRequest extends AbstractFrontEndReq {
    
    private TelegramUserPojo telegramUserPojo;
    
    
    
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        
        if(telegramUserPojo == null){
            errors.add("telegram user pojo");
            return errors;
        }
        
        if(telegramUserPojo.getId() == null){
            errors.add("telegram user id");
        }
        
        if(StringUtils.isEmpty(telegramUserPojo.getHash())){
            errors.add("telegram user not verified");
        }
        
        if(super.getCallingUserId() == null){
            errors.add("calling user Id");
        }
        return errors;
    }

    /**
     * @return the telegramUserPojo
     */
    public TelegramUserPojo getTelegramUserPojo() {
        return telegramUserPojo;
    }

    /**
     * @param telegramUserPojo the telegramUserPojo to set
     */
    public void setTelegramUserPojo(TelegramUserPojo telegramUserPojo) {
        this.telegramUserPojo = telegramUserPojo;
    }
}
