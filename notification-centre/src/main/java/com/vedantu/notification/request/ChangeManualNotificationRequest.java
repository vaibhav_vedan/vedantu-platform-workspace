package com.vedantu.notification.request;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.apache.logging.log4j.Level;

import java.util.List;

public class ChangeManualNotificationRequest extends AbstractFrontEndReq {
    private List<String> changeList;
    private Long rescheduledTime;
    private Long reschdeuledExpiryDate;

    public List<String> getChangeList() {
        return changeList;
    }

    public void setChangeList(List<String> changeList) {
        this.changeList = changeList;
    }

    public Long getRescheduledTime() {
        return rescheduledTime;
    }

    public void setRescheduledTime(Long rescheduledTime) {
        this.rescheduledTime = rescheduledTime;
    }

    public Long getReschdeuledExpiryDate() {
        return reschdeuledExpiryDate;
    }

    public void setReschdeuledExpiryDate(Long reschdeuledExpiryDate) {
        this.reschdeuledExpiryDate = reschdeuledExpiryDate;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();


        if(rescheduledTime==null && reschdeuledExpiryDate==null && changeList==null){
            errors.add("All entries are mandatory, please enter all the entries");
        }

        if(rescheduledTime!=null && rescheduledTime<System.currentTimeMillis()){
            errors.add("Reschedule time can't be less than current time");
        }

        if(rescheduledTime!=null && reschdeuledExpiryDate!=null && reschdeuledExpiryDate!=0 && rescheduledTime>reschdeuledExpiryDate){
            errors.add("You can't have expiry date less than reschedule time");
        }

        if(reschdeuledExpiryDate!=null && reschdeuledExpiryDate!=0 && reschdeuledExpiryDate<System.currentTimeMillis()){
            errors.add("You can't have expiry date less than current time");
        }
        return errors;
    }
}
