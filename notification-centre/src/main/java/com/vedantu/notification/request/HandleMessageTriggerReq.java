package com.vedantu.notification.request;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import com.vedantu.User.User;
import com.vedantu.notification.entity.MessageUser;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import javax.validation.Valid;

/**
 *
 * @author jeet
 */
public class HandleMessageTriggerReq extends AbstractFrontEndReq {

    private User fromuser;
    private User toUser;
    @Valid
    private MessageUser messageUser;

    public HandleMessageTriggerReq() {
        super();
    }

    public User getFromuser() {
        return fromuser;
    }

    public void setFromuser(User fromuser) {
        this.fromuser = fromuser;
    }

    public User getToUser() {
        return toUser;
    }

    public void setToUser(User toUser) {
        this.toUser = toUser;
    }

    public MessageUser getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(MessageUser messageUser) {
        this.messageUser = messageUser;
    }


}
