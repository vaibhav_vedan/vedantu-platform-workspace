package com.vedantu.notification.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

/**
 * @author MNPK
 */

@Data
public class VQuizNotificationReq extends AbstractFrontEndReq {

    public Long userId;
    public Long startTime;
    public String quizId;
    private Long scheduleTime;


    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId not found");
        }
        if (startTime == null) {
            errors.add("startTime not found");
        }
        if (StringUtils.isEmpty(quizId)) {
            errors.add("quizId not found");
        }
        if (scheduleTime != null) {
            if (System.currentTimeMillis() > scheduleTime) {
                errors.add("scheduleTime should be greater than the currentTime");
            }
            if (scheduleTime > startTime) {
                errors.add("scheduleTime should be less than the startTime");
            }
        }
        return errors;
    }
}
