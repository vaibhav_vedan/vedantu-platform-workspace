package com.vedantu.notification.request;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateSmsTemplateReq extends AbstractFrontEndReq {

    private CommunicationType smsType;
    private Role role;
    private String body;
    private Boolean unSubscriptionCheck = Boolean.TRUE;
    private Boolean unSubscriptionLink = Boolean.TRUE;
    private String description;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if(smsType == null)
            errors.add("smsType is mandatory");
        if(StringUtils.isEmpty(body))
            errors.add("body of template is mandatory.");
        if(StringUtils.isEmpty(description))
            errors.add("description of sms is mandatory");
        return errors;
    }
}
