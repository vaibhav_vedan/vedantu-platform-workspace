/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

import com.vedantu.notification.entity.TelegramMessage;

/**
 *
 * @author parashar
 */
public class TelegramHookRequest {
    
    private Long update_id;
    private TelegramMessage message;

    /**
     * @return the update_id
     */
    public Long getUpdate_id() {
        return update_id;
    }

    /**
     * @param update_id the update_id to set
     */
    public void setUpdate_id(Long update_id) {
        this.update_id = update_id;
    }

    /**
     * @return the message
     */
    public TelegramMessage getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(TelegramMessage message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "TelegramHookRequest{" + "update_id=" + update_id + ", message=" + message + '}';
    }
    
}
