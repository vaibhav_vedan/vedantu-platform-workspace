/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author jeet
 */
public class GetMessageUsersReq extends AbstractFrontEndListReq {
	private Long userId;
	private Long toUserId;
	private String contextType;
        private Long afterTimeMillis;

	public GetMessageUsersReq() {
		super();
	}

	

	public GetMessageUsersReq(Long userId, Long toUserId, String contextType) {
		super();
		this.userId = userId;
		this.toUserId = toUserId;
		this.contextType = contextType;
	}
        
        public GetMessageUsersReq(Long userId, Long toUserId, String contextType, Long afterTimeMillis) {
		super();
		this.userId = userId;
		this.toUserId = toUserId;
		this.contextType = contextType;
                this.afterTimeMillis = afterTimeMillis;
	}

        public Long getAfterTimeMillis() {
            return afterTimeMillis;
        }

        public void setAfterTimeMillis(Long afterTimeMillis) {
            this.afterTimeMillis = afterTimeMillis;
        }

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getToUserId() {
		return toUserId;
	}

	public void setToUserId(Long toUserId) {
		this.toUserId = toUserId;
	}

	public String getContextType() {
		return contextType;
	}

	public void setContextType(String contextType) {
		this.contextType = contextType;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		// As the getMessageUsers api is not protected, contentType is made
		// mandatory.
		List<String> errors = super.collectVerificationErrors();
		if (StringUtils.isEmpty(contextType)) {
			errors.add("contextType");
		}
		return errors;
	}
}

