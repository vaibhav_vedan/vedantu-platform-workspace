package com.vedantu.notification.request;

import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.util.enums.CommunicationKind;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class NotificationFilterReq extends AbstractFrontEndReq {
    private Integer limit;
    private Integer skip;
    private NotificationStatus status;
    private CommunicationKind communicationKind;

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getSkip() {
        return skip;
    }

    public void setSkip(Integer skip) {
        this.skip = skip;
    }

    public NotificationStatus getStatus() {
        return status;
    }

    public void setStatus(NotificationStatus status) {
        this.status = status;
    }

    public CommunicationKind getCommunicationKind() {
        return communicationKind;
    }

    public void setCommunicationKind(CommunicationKind communicationKind) {
        this.communicationKind = communicationKind;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();

        if(skip==null && limit==null){
            errors.add("You need to specify proper pagination values");
        }

        return errors;
    }
}
