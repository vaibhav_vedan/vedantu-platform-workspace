package com.vedantu.notification.request;

import com.vedantu.util.CollectionUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TeacherWishesReq extends AbstractFrontEndReq {
    String message;
    Long studentId;
    Long teacherId;

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(message)) {
            errors.add("Message is empty");
        } else if (message.length() > 160) {
            errors.add("Message text should be of length at most 160 characters");
        }

        if (Objects.isNull(studentId)) {
            errors.add("studentId is null");
        }
        if (Objects.isNull(teacherId)) {
            errors.add("teacherId is null");
        }
        return errors;
    }
}
