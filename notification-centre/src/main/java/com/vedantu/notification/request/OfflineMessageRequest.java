package com.vedantu.notification.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import javax.validation.constraints.Size;

/*
 * Incoming request object for sendSMS API
 */
public class OfflineMessageRequest extends AbstractFrontEndReq {

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String message;
    private Long fromUserId;
    private Long toUserId;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(Long fromUserId) {
        this.fromUserId = fromUserId;
    }

    public Long getToUserId() {
        return toUserId;
    }

    public void setToUserId(Long toUserId) {
        this.toUserId = toUserId;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == message) {
            errors.add("Message");
        }

        if (null == fromUserId) {
            errors.add("FROM");
        }

        if (null == toUserId) {
            errors.add("TO");
        }

        return errors;
    }

}
