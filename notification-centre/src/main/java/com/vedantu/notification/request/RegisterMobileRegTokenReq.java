/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

import com.vedantu.notification.entity.MobileRegToken;
import com.vedantu.notification.enums.VedantuApp;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class RegisterMobileRegTokenReq extends AbstractFrontEndReq {

    private String regToken;
    private String deviceId;
    private VedantuApp app;

    public String getRegToken() {
        return regToken;
    }

    public void setRegToken(String regToken) {
        this.regToken = regToken;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public VedantuApp getApp() {
        return app;
    }

    public void setApp(VedantuApp app) {
        this.app = app;
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (StringUtils.isEmpty(deviceId)) {
            errors.add(MobileRegToken.Constants.DEVICEID);
        }

        if (null == app) {
            errors.add(MobileRegToken.Constants.APP);
        }

        return errors;
    }

}
