package com.vedantu.notification.request;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/*
 * Incoming request object for sendSMS API
 */
public class UpdateUnreadMessageCountRequest extends AbstractFrontEndReq {

	private Long userId;
	private String channelName;
	private Integer count;
	
	@Override
	public String toString() {
		return "UpdateUnreadMessageCountRequest userId:" + userId + ", channelName:" + channelName 
				+ ", count:" + count;
	}

	public UpdateUnreadMessageCountRequest() {
		super();
	}

	public UpdateUnreadMessageCountRequest(Long userId, String channelName, Integer count) {
		super();
		this.userId = userId;
		this.channelName = channelName;
		this.count = count;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public static class Constants {
		public static final String USERID = "userId";
		public static final String CHANNELNAME = "channelName";
		public static final String COUNT = "count";

	}
	
	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
		if (null == userId) {
			errors.add(UpdateUnreadMessageCountRequest.Constants.USERID);
		}
		
		if (null == channelName) {
			errors.add(UpdateUnreadMessageCountRequest.Constants.CHANNELNAME);
		}
		
		if (null == count) {
			errors.add(UpdateUnreadMessageCountRequest.Constants.COUNT);
		}
		
		return errors;
	}
}
