/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

/**
 *
 * @author ajith
 */
public class GupshupDeliveryReportReq {

    private String externalId;
    private Long deliveredTS;
    private String status;
    private String phoneNo;
    private String cause;

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Long getDeliveredTS() {
        return deliveredTS;
    }

    public void setDeliveredTS(Long deliveredTS) {
        this.deliveredTS = deliveredTS;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    @Override
    public String toString() {
        return "GupshupDeliveryReportReq{" + "externalId=" + externalId + ", deliveredTS=" + deliveredTS + ", status=" + status + ", phoneNo=" + phoneNo + ", cause=" + cause + '}';
    }

}
