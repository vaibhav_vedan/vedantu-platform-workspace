/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

import com.vedantu.notification.entity.MobileNotificationData;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.pojo.ButtonData;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author ajith
 */
public class MobileNotificationDataRequest extends AbstractFrontEndReq {

    private String title;
    private String description;
    private String imageUrl;
    private String landingActivity;
    private Long timestamp;
    private String extra;
    private List<ButtonData> buttons;
    private NotificationType type;

    public MobileNotificationDataRequest() {
        super();
    }

    public MobileNotificationDataRequest(String title, String description, String imageUrl,
            String landingActivity, Long timestamp, String extra, List<ButtonData> buttons) {
        this.title = title;
        this.description = description;
        this.imageUrl = imageUrl;
        this.landingActivity = landingActivity;
        this.timestamp = timestamp;
        this.extra = extra;
        this.buttons = buttons;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getLandingActivity() {
        return landingActivity;
    }

    public void setLandingActivity(String landingActivity) {
        this.landingActivity = landingActivity;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getExtra() {
        return extra;
    }

    public void setExtra(String extra) {
        this.extra = extra;
    }

    public List<ButtonData> getButtons() {
        return buttons;
    }

    public void setButtons(List<ButtonData> buttons) {
        this.buttons = buttons;
    }

    @Override
    public String toString() {

        return "MobileNotification title:" + title + ", type:" + type;

    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();

        if (null == title) {
            errors.add(MobileNotificationData.Constants.TITLE);
        }

        if (null == type) {
            errors.add(MobileNotificationData.Constants.TYPE);
        }

        if (null != extra) {
            try {
                new JSONObject(extra);
            } catch (JSONException ex) {
                errors.add(MobileNotificationData.Constants.EXTRA + " should be a valid JSON");
            }
        }

        if (ArrayUtils.isNotEmpty(buttons)) {
            for (ButtonData button : buttons) {
                try {
                    new JSONObject(button.getActionExtra());
                } catch (JSONException ex) {
                    errors.add(MobileNotificationData.Constants.BUTTONS + " should be a valid JSON");
                }

            }
        }
        return errors;
    }

    public static class Constants {

        public static final String TITLE = "title";
        public static final String DESCRIPTION = "description";
        public static final String TIMESTAMP = "timestamp";
        public static final String TYPE = "type";
        public static final String EXTRA = "extra";
        public static final String BUTTONS = "b";

    }

}
