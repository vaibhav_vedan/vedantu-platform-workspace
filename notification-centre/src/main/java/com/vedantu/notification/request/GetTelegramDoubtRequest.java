/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 *
 * @author parashar
 */
public class GetTelegramDoubtRequest extends AbstractFrontEndListReq {
    
    private String batchId;
    private Boolean closed;
    private Long userId;
    

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the closed
     */
    public Boolean getClosed() {
        return closed;
    }

    /**
     * @param closed the closed to set
     */
    public void setClosed(Boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }
    
}
