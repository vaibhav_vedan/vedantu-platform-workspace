/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author parashar
 */
public class AddBatchTelegramGroupRequest extends AbstractFrontEndReq {
    
    private Long chatId;
    private String batchId;

    /**
     * @return the chatId
     */
    public Long getChatId() {
        return chatId;
    }

    /**
     * @param chatId the chatId to set
     */
    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }
    
    @Override
    protected List<String> collectVerificationErrors() {
        
        List<String> errors = super.collectVerificationErrors();
        
        if(chatId == null){
            errors.add("chatId");
        }
        
        if(StringUtils.isEmpty(batchId)){
            errors.add("batchId");
        }
        return errors;
        
    }    
    
}
