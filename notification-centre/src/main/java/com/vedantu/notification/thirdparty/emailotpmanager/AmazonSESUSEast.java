package com.vedantu.notification.thirdparty.emailotpmanager;


import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.vedantu.notification.managers.AmazonClient;
import com.vedantu.notification.serializers.EmailDAO;
import com.vedantu.notification.serializers.EmailNotificationDAO;
import com.vedantu.notification.serializers.UnsubscribedEmailDAO;
import com.vedantu.notification.thirdparty.AmazonSESSDK.AbstractAmazonSESSDKManager;
import com.vedantu.notification.thirdparty.AmazonSESSDK.USEastSESSDKManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;

/*
 * Amazon SES Manager for email delivery
 */
@Service
public class AmazonSESUSEast extends USEastSESSDKManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(com.vedantu.notification.thirdparty.AmazonSESSDKManager.class);

    @Autowired
    public EmailDAO emailDAO;

    private static String AWS_REGION;
    private AmazonSimpleEmailServiceClient client = null;

    @Autowired
    AmazonClient s3client;

    public AmazonSESUSEast() {
        super();
        AWS_REGION = ConfigUtils.INSTANCE.getStringValue("amazon.aws.region.email.otp");

        logger.info("Attempting to connect to the Amazon AWS client...");
        // Connect to Amazon AWS client
        try {
            client = new AmazonSimpleEmailServiceClient();
            Region REGION = Region.getRegion(Regions.fromName(AWS_REGION));
            client.setRegion(REGION);
            logger.info("Connected to Amazon AWS Successfully" + AWS_REGION);

        } catch (Exception e) {
            logger.error("Exception Occured While connecting to AMAZON AWS client", e);
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSESSDKManager connection ", e);
        }
    }
}

