package com.vedantu.notification.thirdparty;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;

import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.interfaces.ICloudEmailManager;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.response.BaseEmailResponse;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.SendEmailResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import javax.annotation.PreDestroy;

/*
 * Amazon SES Manager for email delivery
 */
//@Service
public class AmazonSESSMTPManager implements ICloudEmailManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonSESSMTPManager.class);

    private Session session;
    private Transport transport = null;

    private static String SES_SERVER;
    private static String SES_USERNAME;
    private static String SES_PASSWORD;
    private static int SES_PORT;
    private static InternetAddress FROM;

    public AmazonSESSMTPManager() throws AddressException {

        super();
        SES_SERVER = ConfigUtils.INSTANCE.getStringValue("amazon.ses.smtp.server");
        SES_USERNAME = ConfigUtils.INSTANCE.getStringValue("amazon.ses.username");
        SES_PASSWORD = ConfigUtils.INSTANCE.getStringValue("amazon.ses.password");
        // Port we will connect to on the Amazon SES SMTP endpoint. We are choosing port 25 because we will use
        // STARTTLS to encrypt the connection.
        SES_PORT = ConfigUtils.INSTANCE.getIntValue("amazon.ses.port");
        FROM = InternetAddress.parse(ConfigUtils.INSTANCE.getStringValue("email.from"))[0];

        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", SES_PORT);

        // Set properties indicating that we want to use STARTTLS to encrypt the connection.
        // The SMTP session will begin on an unencrypted connection, and then the client
        // will issue a STARTTLS command to upgrade to an encrypted connection.
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");

        // Set a Session object to represent a mail session with the specified properties. 
        session = Session.getDefaultInstance(props);

        logger.info("Attempting to connect to the Amazon SES SMTP interface...");

        // Connect to Amazon SES using the SMTP username and password.
        try {
            // Set a transport.        
            transport = session.getTransport();
            transport.connect(SES_SERVER, SES_USERNAME, SES_PASSWORD);
            logger.info("Connected to Amazon SES Successfully");

        } catch (Exception e) {
            logger.error("Exception Occured While connecting to AMAZON SES", e);
        }
    }
    
    @PreDestroy
    public void cleanUp() {
        try {
            if (transport != null) {
                transport.close();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSESSMTPManager connection ", e);
        }
    }    

    public BaseResponse sendEmail(EmailRequest email) throws AddressException, MessagingException {

        logger.info("Entering email: " + email.toString());

        // Create a message with the specified information. 
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(FROM);
        Address[] addresses;
        addresses = new Address[email.getTo().size()];
        msg.setRecipients(Message.RecipientType.TO, email.getTo().toArray(addresses));

        if (email.getCc() != null) {
            addresses = new Address[email.getCc().size()];
            msg.setRecipients(Message.RecipientType.CC, email.getCc().toArray(addresses));
        }

        if (email.getBcc() != null) {
            addresses = new Address[email.getBcc().size()];
            msg.setRecipients(Message.RecipientType.BCC, email.getBcc().toArray(addresses));
        }

        if (email.getReplyTo() != null) {
            addresses = new Address[email.getReplyTo().size()];
            msg.setReplyTo(email.getReplyTo().toArray(addresses));
        }

        msg.setText((email.getBody()));

        msg.setSubject(email.getSubject());

        Multipart mp = new MimeMultipart();

        BodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(email.getBody(), "text/html; charset=UTF-8");
        mp.addBodyPart(htmlPart);

        if (email.getAttachment() != null) {
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.setFileName(email.getAttachment().getFileName());
            attachment.setContent(email.getAttachment().getAttachmentData(),
                    email.getAttachment().getApplication());
            mp.addBodyPart(attachment);
        }

        msg.setContent(mp);

        BaseEmailResponse successResp, errResp;
        SendEmailResponse response = new SendEmailResponse();
        ArrayList<BaseEmailResponse> bulkResp = new ArrayList<BaseEmailResponse>();

        if (!transport.isConnected()) {
            try {
                transport.connect(SES_SERVER, SES_USERNAME, SES_PASSWORD);
                logger.info("Connected to Amazon SES Successfully");

            } catch (Exception e) {
                logger.error("Exception Occured While connecting to AMAZON SES", e);
                errResp = new BaseEmailResponse();
                errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
                errResp.setErrorMessage(e.getMessage());
                logger.info("Exiting. errrorResponse: " + errResp.toString());
                return errResp;
            }
        }

        try {
            transport.sendMessage(msg, msg.getAllRecipients());
            successResp = new BaseEmailResponse();
            successResp.setErrorCode(ErrorCode.SUCCESS);
            successResp.setErrorMessage("");
            successResp.setStatus(ErrorCode.SUCCESS.toString());
            successResp.setId("");
            successResp.setRejectReason("");
            bulkResp.add(successResp);
            logger.info("Email sent!");
        } catch (Exception ex) {
            logger.error("Error sending email to:", ex);
            errResp = new BaseEmailResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            errResp.setErrorMessage(ex.getMessage());
            errResp.setStatus(ErrorCode.SERVICE_ERROR.toString());
            errResp.setId("");
            errResp.setRejectReason("Service Error");
            bulkResp.add(errResp);

        }

        response.setErrorCode(ErrorCode.SUCCESS);
        response.setErrorMessage("Emails Sent Successfully");
        response.setStatus(bulkResp);
        logger.info("Exiting. bulkResp: " + bulkResp.toString());
        return response;
    }
}
