package com.vedantu.notification.thirdparty;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.interfaces.ICloudSMSManager;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.SendBulkSMSResponse;
import com.vedantu.notification.response.SendSMSResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.LogFactory;
import javax.annotation.PreDestroy;


/*
 * Amazon Manager for message delivery
 */
@Service
public class AmazonSMSManager implements
        ICloudSMSManager {

    //private static String EXOTEL_BASE_URL;
    //private static String SEND_SMS_URL = "/Sms/send";
    private static String CALLER_ID;

    private AmazonSNSAsync snsClient = null;

    @Autowired
    private LogFactory logFactory;

    //@Autowired
    //private WebCommunicator webCommunicator;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonSMSManager.class);

    public AmazonSMSManager() {
        super();
        try {
            snsClient = AmazonSNSAsyncClientBuilder.standard()
                    .withRegion(Regions.AP_SOUTHEAST_1)
                    .build();
            logger.info("Connected to Amazon AWS SNS Successfully");

        } catch (Exception e) {
            logger.error("Exception Occured While connecting to AMAZON AWS SNS client", e);
        }

    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSMSManager connection ", e);
        }
    }

    public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, String sqsMessageId) throws JSONException {
        //highPriority is not applicable for aws
        logger.info("Entering. to:" + toNumber + " message:" + message);
        PublishResult result;
        try {
            result = snsClient.publish(new PublishRequest()
                    .withMessage(message.trim())
                    .withPhoneNumber(toNumber));
        } catch (Exception e) {
            BaseResponse errResp = new BaseResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            errResp.setErrorMessage(e.toString());

            logger.info("Exiting. errrorResponse: " + errResp.toString());
            return errResp;
        }

        SendSMSResponse successResp = new SendSMSResponse();
        successResp.setErrorCode(ErrorCode.SUCCESS);
        if (result != null) {
            successResp.setSmsId(result.getMessageId());
        }
        successResp.setErrorMessage("");

        logger.info("Exiting. succesResponse: " + successResp.toString());
        return successResp;

    }

    //Not using it right now
    public BaseResponse sendBulkSMS(ArrayList<String> toNumbers, String message, boolean highPriority) throws JSONException {
        //highPriority is not applicable for aws
        logger.info("Entering. to:" + toNumbers.toString() + " message:" + message);

        ArrayList<SendSMSResponse> bulkResp = new ArrayList<SendSMSResponse>();
        SendBulkSMSResponse bulkSuccessResp = new SendBulkSMSResponse();

        for (int i = 0; i < toNumbers.size(); i++) {

            PublishResult result;
            try {
                result = snsClient.publish(new PublishRequest()
                        .withMessage(message.trim())
                        .withPhoneNumber(toNumbers.get(i)));
            } catch (Exception e) {
                BaseResponse errResp = new BaseResponse();
                errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
                errResp.setErrorMessage(e.toString());

                logger.info("Exiting. errrorResponse: " + errResp.toString());
                return errResp;
            }

            SendSMSResponse successResp = new SendSMSResponse();
            successResp.setErrorCode(ErrorCode.SUCCESS);
            if (result != null) {
                successResp.setSmsId(result.getMessageId());
            }
            successResp.setErrorMessage("");
            bulkResp.add(successResp);

        }

        bulkSuccessResp.setErrorMessage("Success");
        bulkSuccessResp.setBulkStatus(bulkResp);
        bulkSuccessResp.setErrorCode(ErrorCode.SUCCESS);
        logger.info("Exiting. succesResponse: " + bulkSuccessResp.toString());
        return bulkSuccessResp;

    }

    @Override
    public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, CommunicationType type, Role role, String sqsMessageId) throws JSONException, UnsupportedEncodingException {
        return null;
    }

    @Override
    public BaseResponse sendBulkSMS(ArrayList<String> toNumber, String message, boolean highPriority, CommunicationType type, Role role) throws JSONException {
        return null;
    }
}
