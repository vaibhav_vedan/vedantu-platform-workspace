package com.vedantu.notification.thirdparty;

import com.microtripit.mandrillapp.lutung.MandrillApi;
import com.microtripit.mandrillapp.lutung.model.LutungGsonUtils;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient;
import com.microtripit.mandrillapp.lutung.view.MandrillMessage.Recipient.Type;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.Email;
import com.vedantu.notification.interfaces.ICloudEmailManager;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.response.BaseEmailResponse;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.SendEmailResponse;
import com.vedantu.notification.serializers.EmailDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.mail.Address;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.logging.log4j.Logger;
import org.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebCommunicator;
import java.util.Arrays;
import org.json.JSONException;
import org.springframework.http.HttpMethod;

/*
 * Mandrill Manager for email delivery
 */
@Service
public class MandrillManager implements
        ICloudEmailManager {

    private static String MANDRILL_KEY;
    private static String MANDRILL_SEND_EMAIL_URL;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    public EmailDAO emailDAO;

    @Autowired
    private WebCommunicator webCommunicator;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MandrillManager.class);
    
    private final Integer timeout = 6000; 

    public MandrillManager() {
        super();
        MANDRILL_KEY = ConfigUtils.INSTANCE.getStringValue("mandrillapp.key");
        MANDRILL_SEND_EMAIL_URL = "https://mandrillapp.com/api/1.0/messages/send.json";

        MANDRILL_KEY = ConfigUtils.INSTANCE.getStringValue("netcore.key");
        MANDRILL_SEND_EMAIL_URL = "https://api.pepipost.com/api/1.0/messages/send.json";
    }

    public BaseResponse sendEmail(EmailRequest email) throws AddressException, JSONException {

        logger.info("Entering email: " + email.toString());

        MandrillApi mandrillApi = new MandrillApi(MANDRILL_KEY);

        MandrillMessage message = new MandrillMessage();
        message.setTrackClicks(true);
        message.setTrackOpens(true);
        message.setSubject(email.getSubject());
        message.setHtml(email.getBody());
        message.setAutoText(true);
        InternetAddress FROM = InternetAddress.parse(ConfigUtils.INSTANCE.getStringValue("email.from"))[0];
        message.setFromEmail(FROM.getAddress());
        message.setFromName(FROM.getPersonal());
        // add recipients
        ArrayList<Recipient> recipients = new ArrayList<>();
        recipients.addAll(fromInternetAddress(email.getTo(), Type.TO));
        recipients.addAll(fromInternetAddress(email.getBcc(), Type.BCC));
        recipients.addAll(fromInternetAddress(email.getCc(), Type.CC));
        message.setTo(recipients);

        message.setPreserveRecipients(email.getTo().size() > 1 ? false : true);
        ArrayList<String> tags = new ArrayList<>();
        tags.add("system-gen-mail");
        message.setTags(tags);
        Email readEmailStatistics = new Email();
        readEmailStatistics.setSubject(email.getSubject());
        readEmailStatistics.setType(email.getType());
        readEmailStatistics.setOpened(false);
        readEmailStatistics.setTags(email.getTags());
        // Add email statistics in db
        if (!(email.getTo() == null || email.getTo().isEmpty())) {
            readEmailStatistics.setTo(email.getTo().get(0));
        }
        readEmailStatistics.setId(null);
        readEmailStatistics.setRole(email.getRole());
        emailDAO.create(readEmailStatistics);
        //try {
        final HashMap<String, Object> params = getParamsWithKey(mandrillApi.getKey());
        params.put("message", message);
        params.put("async", false);

        final String paramsStr = LutungGsonUtils.getGson().toJson(params, params.getClass());
        logger.info("paramsStrJSON: " + paramsStr);

        String res = webCommunicator.jsonFetchWithUrlFetchService(MANDRILL_SEND_EMAIL_URL, paramsStr, HttpMethod.POST, null);
        logger.info("send email response: " + res);
        if (res == null) {
            logger.info("send email response is null: " + res + " for params:" + paramsStr);
            BaseResponse errResp = new BaseResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            return errResp;
        }

        /*      
        
        //for mandrill
        Object json = new JSONTokener(res).nextValue();
        if (json instanceof JSONObject) {
            if (((JSONObject) json).has("code")) {
                BaseResponse errResp = new BaseResponse();
                errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
                errResp.setErrorMessage(((JSONObject) json).getString("Message"));
                logger.info("Exiting. errrorResponse: " + errResp.toString());
                return errResp;
            }

        }

        BaseEmailResponse successResp;
        SendEmailResponse response = new SendEmailResponse();
        logger.info(json.toString());
        ArrayList<BaseEmailResponse> bulkResp = new ArrayList<>();
        for (int i = 0; i < ((JSONArray) json).length(); i++) {
            JSONObject _obj = ((JSONArray) json).getJSONObject(i);
            successResp = new BaseEmailResponse();
            successResp.setErrorCode(ErrorCode.SUCCESS);
            successResp.setErrorMessage("");
            successResp.setEmail(_obj.getString("email"));
            successResp.setStatus(_obj.getString("status"));
            successResp.setId(_obj.getString("_id"));
            if (_obj.has("reject_reason")) {
                successResp.setRejectReason(_obj.getString("reject_reason"));
            }
            bulkResp.add(successResp);
        }
         */
        try {
            SendEmailResponse response = new SendEmailResponse();
            JSONObject _obj = new JSONObject(res);
            BaseEmailResponse successResp = new BaseEmailResponse();
            if (_obj.has("message") && _obj.getString("message").equals("SUCCESS")) {
                response.setErrorCode(ErrorCode.SUCCESS);
                response.setErrorMessage("");
            } else {
                logger.error("Error in sending email " + _obj);
                response.setErrorCode(ErrorCode.SERVICE_ERROR);
                response.setErrorMessage(_obj.getString("errormessage"));
            }
            ArrayList<BaseEmailResponse> status = new ArrayList<>();
            status.add(successResp);
            response.setStatus(status);
            logger.info("Exiting response " + response);
            return response;
        } catch (Exception e) {
            logger.error("Exception Occured" + e.getMessage());
            BaseResponse errResp = new BaseResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            errResp.setErrorMessage(e.getMessage());
            logger.info("Exiting. errrorResponse: " + errResp.toString());
            return errResp;
        }
    }

    private List<Recipient> fromInternetAddress(List<InternetAddress> arrayList, Type type) {
        List<Recipient> recipients = new ArrayList<Recipient>();
        if (arrayList != null) {
            for (Address bcc : arrayList) {
                Recipient recipient = new Recipient();
                InternetAddress iBcc = (InternetAddress) bcc;
                recipient.setEmail(iBcc.getAddress());
                if (iBcc.getPersonal() != null) {
                    recipient.setName(iBcc.getPersonal());
                }
                recipient.setType(type);
                recipients.add(recipient);
            }
        }
        return recipients;
    }

    private HashMap<String, Object> getParamsWithKey(final String key) {
        final HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("key", key);
        return params;

    }

}
