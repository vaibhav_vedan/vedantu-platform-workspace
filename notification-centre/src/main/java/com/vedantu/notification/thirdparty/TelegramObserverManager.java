/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.thirdparty;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;

import com.vedantu.User.Role;

import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.entity.TelegramDoubt;
import com.vedantu.notification.entity.TelegramGroup;
import com.vedantu.notification.entity.TelegramMessage;
import com.vedantu.notification.entity.TelegramUser;
import com.vedantu.notification.managers.AmazonS3Manager;
import com.vedantu.notification.pojo.TelegramUserPojo;
import com.vedantu.notification.request.TelegramHookRequest;
import com.vedantu.notification.response.TelegramResponse;
import com.vedantu.notification.serializers.TelegramDoubtDAO;
import com.vedantu.notification.serializers.TelegramGroupDAO;
import com.vedantu.notification.serializers.TelegramMessageDAO;
import com.vedantu.notification.serializers.TelegramUserDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class TelegramObserverManager {
    
    @Autowired
    private AmazonS3Manager amazonS3Manager;
    
    @Autowired
    private TelegramUserDAO telegramUserDAO;
    
    @Autowired
    private FosUtils fosUtils;
    
    @Autowired
    private TelegramActorManager telegramActorManager;
    
    @Autowired
    private TelegramDoubtDAO telegramDoubtDAO;
    
    @Autowired
    private TelegramMessageDAO telegramMessageDAO;
    
    @Autowired
    private TelegramGroupDAO telegramGroupDAO;    

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TelegramObserverManager.class);
    
    public final static String BOT_TOKEN = ConfigUtils.INSTANCE.getStringValue("telegram.bot.token");
    public final static String telegram_url = ConfigUtils.INSTANCE.getStringValue("telegram.endpoint") + BOT_TOKEN + "/";
    public final static String subscription_url = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    public final static String telegram_bot_name = ConfigUtils.INSTANCE.getStringValue("telegram.bot.name");
    
    public Gson gson = new Gson();
    
    //webhook for handling telegram events
    public void handleEvent(TelegramHookRequest telegramHookRequest){
        
    }
    
    //for sending to telegram channel
    /*public void broadCastMessage(){
        ClientResponse response = WebUtils.INSTANCE.doCall(telegram_url+"sendMessage", HttpMethod.DELETE, BOT_TOKEN);
    }*/
    
    //for sending files
    /*public void sendFile(Long chat_id, File file) throws VException{
        MultiPart multiPart = new MultiPart();
        multiPart.setMediaType(MediaType.MULTIPART_FORM_DATA_TYPE);
        FileDataBodyPart fileDataBodyPart = new FileDataBodyPart("file",
            file,
            MediaType.APPLICATION_OCTET_STREAM_TYPE);
        
        multiPart.bodyPart(fileDataBodyPart);
        
        ClientResponse response = WebUtils.INSTANCE.doPost(telegram_url+"sendFile?chat_id="+chat_id, multiPart);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        TelegramResponse telegramResponse = gson.fromJson(jsonString, TelegramResponse.class);
        if(!telegramResponse.isOk()){
            logger.error("Error in sending file to telegram");
        }
        
    }*/
        
    public void processDoubt(TelegramMessage telegramMessage, boolean isCaption, TelegramGroup telegramGroup){
        TelegramDoubt telegramDoubt = new TelegramDoubt();
        if(isCaption){
            telegramDoubt.setMessage(telegramMessage.getCaption());
        }else{
            telegramDoubt.setMessage(telegramMessage.getText());
        }
        telegramDoubt.setTelegramMessageId(telegramMessage.getMessage_id());
        telegramDoubt.setTelegramUserId(telegramMessage.getFrom().getId());
        telegramDoubt.setTelegramChatId(telegramMessage.getChat().getId());
        TelegramUser telegramUser = telegramUserDAO.getByTelegramId(telegramMessage.getFrom().getId());
        if(telegramUser == null){
            logger.warn("User not registered");
            return;
        }
        telegramGroup.getBatchIds().retainAll(telegramUser.getBatchIds());
        if(ArrayUtils.isEmpty(telegramGroup.getBatchIds())){
            logger.warn("Not part of batch: ");
            return;
        }
        telegramDoubt.setBatchId((new ArrayList<>(telegramGroup.getBatchIds()).get(0)));

        if(telegramUser == null){
            // logger.error("User not found in db for telegram id: "+telegramDoubt.getTelegramUserId());
            return;
        }
        telegramDoubt.setUserId(telegramUser.getUserId());   
        telegramDoubtDAO.save(telegramDoubt, null);
    }
    
    //to process message from groups or individual chats
    public void processMessage(TelegramHookRequest req){
        
        if(req.getMessage() == null){
            logger.info("No messages to process");
            return;
        }
        
        TelegramMessage telegramMessage = req.getMessage();
        telegramMessageDAO.save(telegramMessage, null);
        //save message here
        logger.info("telegram message: "+telegramMessage);
        try{       
            if(telegramMessage.getFrom().getId().equals(telegramMessage.getChat().getId())){
                logger.warn("Personal chat not processing");
                return;
            }
            TelegramGroup telegramGroup = telegramGroupDAO.getByTelegramId(telegramMessage.getChat().getId());
            if((telegramMessage.getChat().getType().toLowerCase().equals("group") || telegramMessage.getChat().getType().toLowerCase().equals("supergroup")) && telegramGroup == null){
                logger.info("no group found for chat id: "+telegramMessage.getChat().getId());
                telegramActorManager.leaveChat(telegramMessage.getChat().getId());
                return;
            }        

            if(StringUtils.isNotEmpty(telegramMessage.getText()) && telegramMessage.getText().contains(telegram_bot_name)){
                logger.info("processing doubt without image");
                processDoubt(telegramMessage, false, telegramGroup);
                return;         
            }

            if(StringUtils.isNotEmpty(telegramMessage.getCaption()) && telegramMessage.getCaption().contains(telegram_bot_name)){
                logger.info("processing doubt with image");
                processDoubt(telegramMessage, true, telegramGroup);  
                return;
            }

            if(telegramMessage.getReply_to_message() != null){
                logger.info("Processing reply");
                processReplies(telegramMessage);
                return;
            }

            if(ArrayUtils.isNotEmpty(telegramMessage.getNew_chat_members())){
                for(TelegramUserPojo telegramUserPojo : telegramMessage.getNew_chat_members()){
                    processJoinGroupEvent(telegramUserPojo.getId(), telegramMessage.getChat().getId());
                }
                return;
            }

            if(telegramMessage.getNew_chat_member() != null){
                processJoinGroupEvent(telegramMessage.getNew_chat_member().getId(), telegramMessage.getChat().getId());
            }

            if(telegramMessage.getLeft_chat_member() != null){
                processLeftGroupEvent(telegramMessage.getLeft_chat_member().getId(), telegramMessage.getChat().getId());
            }
        }catch(Exception ex){
            logger.error("Exception while processing telegram message: "+ex.getMessage());
        }
        
    }
    
    public void processReplies(TelegramMessage telegramMessage){
         
        TelegramDoubt telegramDoubt = telegramDoubtDAO.getDoubtForMessageAndChat(telegramMessage.getReply_to_message().getMessage_id(), telegramMessage.getChat().getId());
        if(telegramDoubt == null){
            logger.info("Not replied to any doubt");
            return;
        }
        
        
        TelegramUser telegramUser = telegramUserDAO.getByTelegramId(telegramMessage.getFrom().getId());
        /*if(telegramUser == null){
            logger.warn("Unregistered user in group: "+telegramMessage.getFrom().getId());
            return;
        }*/
        telegramDoubt.getRepliedMessageIds().add(telegramMessage.getMessage_id());
        telegramDoubtDAO.save(telegramDoubt, null);
        
        if(telegramUser != null && telegramUser.getRole().equals(Role.STUDENT)){
            return;
        }
        try{
            telegramActorManager.forwardMessage(telegramDoubt.getTelegramUserId(), telegramMessage.getChat().getId(), telegramMessage.getMessage_id());
        }catch(Exception ex){
            logger.error("Error in forwarding reply: "+ex.getMessage());
        }
        
    }
    
    //to run when someone join a group
    public void processJoinGroupEvent(Long user_id, Long group_id) throws NotFoundException, VException{
        
        
        TelegramUser telegramUser = telegramUserDAO.getByTelegramId(user_id);
        
        TelegramGroup telegramGroup = telegramGroupDAO.getByTelegramId(group_id);
        
        if(telegramGroup.getExceptionUsers().contains(user_id)){
            return;
        }
        
        if(telegramUser==null){
            kickMemberOutOfGroup(user_id, group_id);
        }else{
            
            // TelegramGroup telegramGroup = telegramGroupDAO.getByTelegramId(group_id);
            Set<String> userBatches = telegramUser.getBatchIds();
            List<String> batchIds = telegramActorManager.getBatchIds(telegramUser.getUserId().toString());
            userBatches.addAll(batchIds);
            Set<String> groupBatches = telegramGroup.getBatchIds();
            userBatches.retainAll(groupBatches);
            if(userBatches.size() == 0 && Role.STUDENT.equals(telegramUser.getRole())){
                if(!telegramGroup.getExceptionUsers().contains(user_id)){
                    kickMemberOutOfGroup(user_id, group_id);
                    telegramGroup.getBannedUsers().add(user_id);
                    //return;
                }
            }else{
                telegramGroup.getParticipantUsers().add(user_id);
            }
            telegramUser.getBatchIds().addAll(userBatches);
            telegramUserDAO.save(telegramUser, null);
            telegramGroupDAO.save(telegramGroup, null);
        }
        
        
    }
    
    //to add exception users
    public void addExceptionUserToGroup(String email, Long group_id) throws NotFoundException{
       
        TelegramUser telegramUser = telegramUserDAO.getByEmail(email);
        
        TelegramGroup telegramGroup = telegramGroupDAO.getByTelegramId(group_id);
        
        telegramGroup.getExceptionUsers().add(telegramUser.getTelegramId());
        telegramGroup.getBannedUsers().remove(telegramUser.getTelegramId());
        
        telegramGroupDAO.save(telegramGroup, null);
        
    }
    
    //to run when someone leave a group
    public void processLeftGroupEvent(Long user_id, Long group_id) throws NotFoundException{
        
        TelegramGroup telegramGroup = telegramGroupDAO.getByTelegramId(group_id);
        telegramGroup.getParticipantUsers().remove(user_id);
        telegramGroupDAO.save(telegramGroup, null);
        
    }
    

    //to kickout a member from a group
    public void kickMemberOutOfGroup(Long user_id, Long group_id) throws VException{
        ClientResponse response = WebUtils.INSTANCE.doCall(telegram_url+"kickChatMember?chat_id="+group_id+"&user_id="+user_id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        TelegramResponse telegramResponse = gson.fromJson(jsonString, TelegramResponse.class);
        if(!telegramResponse.isOk()){
            logger.error("Error in removing chat member: "+user_id+" from telegram chat: "+group_id);
        }
        
    }
    
    //to ban a memeber from a group
    public void banMemberFromGroup(){
        
    }
    
    //to unban a member from a group
    public void unBanMemberFromGroup(Long user_id, Long group_id) throws VException{
        ClientResponse response = WebUtils.INSTANCE.doCall(telegram_url+"unbanChatMember?chat_id="+group_id+"&user_id="+user_id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        TelegramResponse telegramResponse = gson.fromJson(jsonString, TelegramResponse.class);
        if(!telegramResponse.isOk()){
            logger.error("Error in removing chat member: "+user_id+" from telegram chat: "+group_id);
        }
        
        
    }
    
}
