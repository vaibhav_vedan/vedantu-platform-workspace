/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.thirdparty;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.vedantu.notification.entity.ChatMessage;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import io.ably.lib.types.AblyException;
import io.ably.lib.types.Message;
import io.ably.lib.types.PaginatedResult;
import io.ably.lib.types.Param;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import com.google.gson.JsonObject;
import com.vedantu.util.StringUtils;
import io.ably.lib.realtime.CompletionListener;
import io.ably.lib.rest.AblyRest;
import io.ably.lib.rest.Channel;
import io.ably.lib.types.PresenceMessage;
import javax.annotation.PreDestroy;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class AblyChannelsManager {

    public static String API_KEY;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AblyChannelsManager.class);

    private AblyRest ablyRest;

    public AblyChannelsManager() throws AblyException {
        API_KEY = ConfigUtils.INSTANCE.getStringValue("ably.apikey");
        ablyRest = new AblyRest(API_KEY);
//        ablyRealtime.connection.on(ConnectionState.connected, new ConnectionStateListener() {
//            @Override
//            public void onConnectionStateChanged(ConnectionStateChange state) {
//                logger.info("connection to Ably server open");
//            }
//        });
    }

    public List<ChatMessage> getChatMessages(String channelname, Long start, Long end, Integer limit) throws AblyException, JSONException {
        logger.info("getChatMessages to channelname" + channelname+"  start:"+start+"   end"+end+"   limit"+limit);
        List<ChatMessage> messages = new ArrayList();
        Channel channel = ablyRest.channels.get("chat:"+channelname);
        if (limit == null) {
            limit = 1000;
        }
        Param[] options = new Param[]{new Param("limit", String.valueOf(limit))};
        if (start != null) {
            options = Param.push(options, new Param("start", String.valueOf(start)));
        }
        if (end != null) {
            options = Param.push(options, new Param("end", String.valueOf(end)));
        }
        PaginatedResult<Message> result = channel.history(options);
        logger.info("message received in ably "+result.items().length);
        for (Message message : result.items()) {
            ChatMessage chat=convertToChatMessage(message,channelname);
            if(chat!=null){
                messages.add(chat);
            }
        }
        while (result.hasNext()) {
            result=result.next();
            for (Message message : result.items()) {
                ChatMessage chat=convertToChatMessage(message,channelname);
                if(chat!=null){
                    messages.add(chat);
                }
            }
        }
        return messages;
    }
    
//    public PresenceMessage[] getPresenceForChannel(String channelname) throws AblyException{
//        Channel channel = ablyRest.channels.get(channelname);
//        return channel.presence.get();
//
//    }
//    
//    public void publishToChannel(String channelname,JSONObject payload,CompletionListener publishCallBack) throws AblyException {
//        Channel channel = ablyRest.channels.get(channelname);
//        channel.publish("event", payload,publishCallBack);
//    }
    
    private ChatMessage convertToChatMessage(Message message,String channelname) throws JSONException {
        Long serverTime = message.timestamp;
        JsonObject data=(JsonObject) (message.data);
        if(data==null){
            return null;
        }
        JsonElement obj = data.get("type");
        if(obj==null){
            return null;
        }
        String type=obj.getAsString();
        if (!type.equalsIgnoreCase("Message")){
            return null;
        }
        JsonObject msg = data.getAsJsonObject("msg");
        if(msg==null){
            return null;
        }
        ChatMessage chatMessage;
        obj = msg.get("type");
        if(obj==null){
            return null;
        }
        type=obj.getAsString();
        if (type.equalsIgnoreCase("MESSAGE") || type.equalsIgnoreCase("FILE")) {
            chatMessage = new Gson().fromJson(message.data.toString(), ChatMessage.class);
            chatMessage.setData(msg.toString());
            chatMessage.setServerTime(serverTime);
            chatMessage.setMessageId("ably:"+message.id);
            chatMessage.setChannel(channelname);
            return chatMessage;
        } else {
            return null;
        }
    }

  
}
