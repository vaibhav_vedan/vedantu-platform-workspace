package com.vedantu.notification.thirdparty.AmazonSESSDK;


import com.amazonaws.auth.AWSCredentials;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.amazonaws.services.simpleemail.model.*;
import com.vedantu.notification.entity.UnsubscribedEmail;
import com.vedantu.notification.managers.AmazonClient;
import com.vedantu.notification.managers.ELCertificateManager;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.serializers.UnsubscribedEmailDAO;
import com.vedantu.util.*;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.Email;
import com.vedantu.notification.entity.EmailLinksOpenStatus;
import com.vedantu.notification.entity.EmailNotification;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.interfaces.ICloudEmailManager;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.response.BaseEmailResponse;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.EmailOpenedResponse;
import com.vedantu.notification.response.GetSendQuotaResponse;
import com.vedantu.notification.response.GetSendStatisticsResponse;
import com.vedantu.notification.response.SendEmailResponse;
import com.vedantu.notification.serializers.EmailDAO;
import com.vedantu.notification.serializers.EmailNotificationDAO;

import java.net.URLEncoder;
import javax.annotation.PreDestroy;
import javax.mail.util.ByteArrayDataSource;

import org.apache.commons.lang3.RandomStringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.safety.Whitelist;
import org.jsoup.select.Elements;

/*
 * Amazon SES Manager for email delivery
 */
@Service
public class USEastSESSDKManager implements ICloudEmailManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(com.vedantu.notification.thirdparty.AmazonSESSDKManager.class);

    @Autowired
    public ELCertificateManager elCertificateManager;

    @Autowired
    public EmailDAO emailDAO;

    private static Session session;

    private static String AWS_REGION;
    private AmazonSimpleEmailServiceClient client = null;
    private static String EMAIL_TRACKER_ENDPOINT;
    private static String PLATFORM_URL_READ_EMAIL;
    private static InternetAddress FROM;

    @Autowired
    AmazonClient s3client;

    @Autowired
    private EmailNotificationDAO emailNotificationDAO;

    @Autowired
    private UnsubscribedEmailDAO unsubscribedEmailDAO;

    private static final List<CommunicationType> avoidFilterList = new ArrayList<>(Arrays.asList(CommunicationType.EMAIL_VERIFICATION, CommunicationType.FORGOT_PASSWORD,
            CommunicationType.GENERATE_PASSWORD));

    public USEastSESSDKManager() {
        super();
        EMAIL_TRACKER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("EMAIL_TRACKER_ENDPOINT");
        if (StringUtils.isEmpty(EMAIL_TRACKER_ENDPOINT)) {
            EMAIL_TRACKER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
        }
        PLATFORM_URL_READ_EMAIL = EMAIL_TRACKER_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("platform.url.read.email");
        logger.info("\n\n EMAIL_TRACKER_ENDPOINT   " + EMAIL_TRACKER_ENDPOINT);
        logger.info("\n\n PLATFORM_URL_READ_EMAIL   " + PLATFORM_URL_READ_EMAIL);

        Properties props = System.getProperties();
        session = Session.getDefaultInstance(props);

        logger.info("Attempting to connect to the Amazon AWS client...");
        // Connect to Amazon AWS client
        try {
            FROM = InternetAddress.parse(ConfigUtils.INSTANCE.getStringValue("email.from"))[0];
            client = new AmazonSimpleEmailServiceClient();
            Region REGION = Region.getRegion(Regions.US_EAST_1);
            client.setRegion(REGION);
            logger.info("Connected to Amazon AWS Successfully" + AWS_REGION);

        } catch (Exception e) {
            logger.error("Exception Occured While connecting to AMAZON AWS client", e);
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSESSDKManager connection ", e);
        }
    }

    public BaseResponse sendEmail(EmailRequest email) throws AddressException, MessagingException, IOException {
        BaseResponse response;
        logger.info("----------------------------  Entering email  -----------------------------------------------");

        logger.info("Entering email: " + email.toString());
        ArrayList<InternetAddress> toAddressesPreEdit = new ArrayList<InternetAddress>();
        toAddressesPreEdit.addAll(email.getTo());
        removeBouncedOrComplaintRecipient(email);
        removeNonASCIICharFromName(email.getTo());
        List<InternetAddress> toList = checkValidEmail(email.getTo());
        if (toList.size() == 0) {
            logger.info("No Valid emails");
            return null;
        }
        email.setTo(toList);
        logger.info("----------------------------  Now list  -----------------------------------------------");

        logger.info("Now list " + email.getTo() + email.getCc() + email.getBcc());
        List<InternetAddress> toAddressesPostEdit = email.getTo();
        if (!CollectionUtils.isEmpty(toAddressesPostEdit)) {
            response = sendEmailPostRemoveBounce(email);
        } else {
            response = new BaseResponse(ErrorCode.SUCCESS, "No subscribed 'to' addresses found");
        }
        logger.info("----------------------------  response  -----------------------------------------------");

        logger.info("response - " + response.toString());

        // Send the email alert in case of any bounced email
        if (CollectionUtils.isEmpty(toAddressesPostEdit) || (toAddressesPostEdit.size() != toAddressesPreEdit.size())) {
            // Try catch block just to make sure this would not effect the
            // response in any way.
            //try {
            logger.info("-----------------------------  Bounce entries found - toAddressesPostEdit  ----------------------------------------------");
            logger.info("Bounce entries found - toAddressesPostEdit : " + Arrays.toString(toAddressesPostEdit.toArray())
                    + " toAddressesPreEdit - " + Arrays.toString(toAddressesPreEdit.toArray()));

            List<InternetAddress> removedEntries = getRemovedEntries(toAddressesPreEdit, toAddressesPostEdit);
            logger.info("----------------------------- Removed entried   ----------------------------------------------");

            logger.info("Removed entried : " + Arrays.toString(removedEntries.toArray()));
            if (!CollectionUtils.isEmpty(removedEntries)) {
                email.setTo(getBounceAlertAddress());
                email.setBcc(new ArrayList<InternetAddress>());
                email.setCc(new ArrayList<InternetAddress>());

                // Set the bounced email ids in the subject
                logger.info("-----------------------------  BounceIds  ----------------------------------------------");

                email.setSubject(email.getSubject() + " BounceIds : " + Arrays.toString(removedEntries.toArray()));

                sendEmailPostRemoveBounce(email);
            }
            /*} catch (Exception ex) {
                logger.error("Bounce alert failed for request - " + email.toString() + " toAddressesPreEdit - "
                        + Arrays.toString(toAddressesPreEdit.toArray()));
            }*/
        }

        return response;
    }

    private List<InternetAddress> checkValidEmail(List<InternetAddress> to) {
        List<InternetAddress> addressList = new ArrayList<>();
        for (InternetAddress address : to) {
            if (CustomValidator.validEmail(address.getAddress())) {
                addressList.add(address);
            }
        }
        return addressList;
    }

    private void removeNonASCIICharFromName(List<InternetAddress> to) throws UnsupportedEncodingException {
        for (InternetAddress address : to) {
            String personal = address.getPersonal();
            if (address.getPersonal() != null) {
                byte[] b = address.getPersonal().getBytes(StandardCharsets.UTF_8);
                if (b.length > 50) {
                    address.setPersonal("");
                }
            }
        }
    }

    public BaseResponse sendEmailPostRemoveBounce(EmailRequest email) throws AddressException, MessagingException, IOException {

        // Create a message with the specified information.
        MimeMessage msg = new MimeMessage(session);
        logger.info("REGION **************************" + client.getSignerRegionOverride());

        //checking if a custom from address is available
        String fromAddress = ConfigUtils.INSTANCE.getStringValue("email.FROM_ADDRESS." + email.getType());
        logger.info("   fromAddress     ", fromAddress);
        if (StringUtils.isNotEmpty(fromAddress)) {
            FROM = InternetAddress.parse(fromAddress)[0];
            email.setBcc(null);//was doing it earlier for isl emails, so doing it again to avoid breaking anything
        }

        if (CommunicationType.WAVE_LEADERBOARD_RECOGNITION.equals(email.getType()) ||
                CommunicationType.WAVE_STREAK_RECOGNITION.equals(email.getType())) {
            logger.info("wave recognition email:" + email.getFrom().getAddress() + " " + email.getType());
            msg.setFrom(email.getFrom());
        } else {
            msg.setFrom(FROM);
        }

        Address[] addresses = new Address[email.getTo().size()];
        msg.setRecipients(Message.RecipientType.TO, email.getTo().toArray(addresses));

        Email readEmailStatistics = new Email();
        readEmailStatistics.setSubject(email.getSubject());
        readEmailStatistics.setType(email.getType());
        readEmailStatistics.setOpened(false);
        readEmailStatistics.setTags(email.getTags());
        readEmailStatistics.setSqsMessageId(email.getSqsMessageId());
        logger.info("EMAIL MESSAGEID BEFORE SAVING" + email.getSqsMessageId());
        // Add email statistics in db
        if (!(email.getTo() == null || email.getTo().isEmpty())) {
            readEmailStatistics.setTo(email.getTo().get(0));
        }
        readEmailStatistics.setId(null);
        readEmailStatistics.setRole(email.getRole());
        if (email.getManualNotificationId() != null) {
            readEmailStatistics.setManualNotificationId(email.getManualNotificationId());
        }
        emailDAO.create(readEmailStatistics);
        //adding click trackers
        if (email.isClickTrackersEnabled()) {
            logger.info("   clicked    ");
            try {
                logger.info("  try  ");
                Document doc = Jsoup
                        .parseBodyFragment(email.getBody());
                Elements links = doc.getElementsByTag("a");
                logger.info(" links ", doc.getElementsByTag("a"));
                Iterator<Element> it = links.iterator();
                while (it.hasNext()) {
                    logger.info("   while   ");
                    Element link = it.next();
                    if (StringUtils.isNotEmpty(link.attr(Email.Constants.LINK_ATTRITUBE_TRACK))
                            && link.attr(Email.Constants.LINK_ATTRITUBE_TRACK).equals("true")
                            && StringUtils.isNotEmpty(link.attr(Email.Constants.LINK_ATTRITUBE_HREF))) {
                        logger.info("  if  ");
                        String newhref = EMAIL_TRACKER_ENDPOINT + "/notification-centre/email/track?";
                        String href = link.attr(Email.Constants.LINK_ATTRITUBE_HREF);
                        logger.info("      href    " + href);
                        String trackname = link.attr(Email.Constants.LINK_ATTRITUBE_TRACK_NAME);
                        if (StringUtils.isEmpty(trackname)) {
                            trackname = link.text();
                        }
                        logger.info("    trackname   " + trackname);
                        String randomId = RandomStringUtils.randomAlphanumeric(4);
                        newhref += "linkRandomId=" + randomId
                                + "&emailId=" + readEmailStatistics.getId()
                                + "&redirectUrl=" + URLEncoder.encode(href, Constants.DEFAULT_CHARSET);

                        logger.info("  newhref " + newhref);

                        EmailLinksOpenStatus emailLinksOpenStatus = new EmailLinksOpenStatus();
                        emailLinksOpenStatus.setLinkName(trackname);
                        emailLinksOpenStatus.setLinkUrl(href);
                        emailLinksOpenStatus.setRandomId(randomId);
                        readEmailStatistics.addEmailLink(emailLinksOpenStatus);
                        logger.info("   emailLinksOpenStatus  " + emailLinksOpenStatus);
                        link.attr(Email.Constants.LINK_ATTRITUBE_HREF, newhref);
                        logger.info("   constants    " + link.attr(Email.Constants.LINK_ATTRITUBE_HREF, newhref));
                    }
                }
                email.setBody(doc.html());
                logger.info(" \n\n email test    " + email.getBody());
            } catch (Exception e) {
                logger.warn("Error in creating click trackers for "
                        + email.getTo() + ", " + email.getType() + ", error " + e.getMessage());
            }
            logger.info("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
            logger.info("\nAdding information about calling user id from inside clickTrackerEnabled" + readEmailStatistics.getCreatedBy() + "\n");
            logger.info("\n$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$\n");
            if (email.getManualNotificationId() != null) {
                readEmailStatistics.setManualNotificationId(email.getManualNotificationId());
            }
            emailDAO.create(readEmailStatistics);
            logger.info("\n\n\n save  " + readEmailStatistics);
        }

        if (email.getCc() != null && !email.getCc().isEmpty()) {

            addresses = new Address[email.getCc().size()];
            msg.setRecipients(Message.RecipientType.CC, email.getCc().toArray(addresses));
        }

        if (email.getBcc() != null && !email.getBcc().isEmpty()) {
            addresses = new Address[email.getBcc().size()];
            msg.setRecipients(Message.RecipientType.BCC, email.getBcc().toArray(addresses));
        }

        if (email.getReplyTo() != null && !email.getReplyTo().isEmpty()) {
            addresses = new Address[email.getReplyTo().size()];
            msg.setReplyTo(email.getReplyTo().toArray(addresses));
        }

        msg.setText((email.getBody()) + "");

        msg.setSubject(email.getSubject(), "UTF-8");

        Multipart mp = new MimeMultipart();

        BodyPart htmlPart = new MimeBodyPart();
        htmlPart.setContent(email.getBody() + "<img style=\"display: none;\" src=\"" + PLATFORM_URL_READ_EMAIL + "?id="
                + readEmailStatistics.getId() + "\"/>", "text/html; charset=UTF-8");
        mp.addBodyPart(htmlPart);
        if (email.getExtraAttachments() != null && !email.getExtraAttachments().isEmpty()) {
            for (EmailAttachment emailAttachment : email.getExtraAttachments()) {
                MimeBodyPart attachment = new MimeBodyPart();
                attachment.setFileName(emailAttachment.getFileName());
                attachment.setContent(emailAttachment.getAttachmentData(), emailAttachment.getApplication());
                mp.addBodyPart(attachment);
            }
        }
        if (email.getAttachment() != null) {
            MimeBodyPart attachment = new MimeBodyPart();
            attachment.setFileName(email.getAttachment().getFileName());
            attachment.setContent(email.getAttachment().getAttachmentData(), email.getAttachment().getApplication());
            mp.addBodyPart(attachment);
        }

        File file = null;
        if (null != email.getKey()) {
            // get file from s3 and attach pdf
            InputStream inputStream = s3client.getObject(email.getKey(), email.getBucket());

            file = File.createTempFile("result", ".pdf");
            FileUtils.copyInputStreamToFile(inputStream, file);

            if (null != file) {
                MimeBodyPart attachPart = new MimeBodyPart();
                DataSource dataSource = new ByteArrayDataSource(new FileInputStream(file), "application/pdf");
                attachPart.setDataHandler(new DataHandler(dataSource));
                attachPart.setFileName("result.pdf");
                mp.addBodyPart(attachPart);
            }

        }
        logger.info("IsEarlyLearning : " + email.isEarlyLearning());

        if(email.isEarlyLearning()) {
            File attachmentFile = null;
            try {
                String certificateName = System.currentTimeMillis() +"_"+ELCertificateManager.ATTACHMENT_FILE_NAME;
                logger.info("Certificate Name : {}",certificateName);
                attachmentFile = elCertificateManager.getCertificateFile(email.getBodyScopes(), certificateName);
                if (attachmentFile != null) {
                    MimeBodyPart attachPart = new MimeBodyPart();
                    DataSource dataSource = new ByteArrayDataSource(new FileInputStream(attachmentFile), "application/pdf");
                    attachPart.setDataHandler(new DataHandler(dataSource));
                    attachPart.setFileName(ELCertificateManager.ATTACHMENT_FILE_NAME);
                    mp.addBodyPart(attachPart);
                }

            } catch (Exception ex) {
                logger.error("File Fetch Exception in Email Attachment : "+ex);
            }finally {
                if(attachmentFile!=null) {
                    attachmentFile.delete();
                }
            }

        }

        msg.setContent(mp);

        // Send the email.
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        //try {
        msg.writeTo(outputStream);
        RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
        SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
        SendRawEmailResult response;
        response = client.sendRawEmail(rawEmailRequest);
        BaseEmailResponse successResp = new BaseEmailResponse();
        successResp.setErrorCode(ErrorCode.SUCCESS);
        successResp.setErrorMessage("");
        successResp.setStatus(ErrorCode.SUCCESS.toString());
        successResp.setId(response.getMessageId());
        successResp.setRejectReason("");
        logger.info("Email Sent Successfully");
        logger.info("Exiting successResponse:" + successResp.toString());
        if (null != email.getKey() && null != file) {
            file.delete();
        }
        return successResp;
        /*} catch (Exception e) {
            if (e instanceof AmazonSimpleEmailServiceException) {
                logger.info("Error sending email", e);
            } else {
                logger.error("Error sending email", e);
            }
            BaseResponse errResp = new BaseResponse();
            errResp = new BaseEmailResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            errResp.setErrorMessage(e.getMessage());
            logger.info("Exiting errResp:" + errResp.toString());
            return errResp;
        }*/
    }


    private void removeBouncedOrComplaintRecipient(EmailRequest email) {
        List<String> emailAddresses = new ArrayList<>();
        populateEmailListFromInternetAddress(email.getTo(), emailAddresses);
        populateEmailListFromInternetAddress(email.getCc(), emailAddresses);
        populateEmailListFromInternetAddress(email.getBcc(), emailAddresses);
        // populateEmailListFromInternetAddress(email.getReplyTo(),
        // emailAddresses);

        List<String> emailAddressToRemove = new ArrayList<>();
        List<EmailNotification> foundNotifications = emailNotificationDAO.getEntriesToNotSend(emailAddresses);
        if (foundNotifications != null && !foundNotifications.isEmpty()) {
            logger.info("removeBouncedOrComplaintRecipient foundNotifications " + foundNotifications.size());
            for (EmailNotification emailNotification : foundNotifications) {
                emailAddressToRemove.add(emailNotification.getEmailAddress());
            }

        }

        boolean filterUnsubscriber = true;
        if (null != email.getType() && avoidFilterList.contains(email.getType())) {
            filterUnsubscriber = false;
        }

        if (filterUnsubscriber) {
            List<UnsubscribedEmail> foundUnsubscribed = unsubscribedEmailDAO.getEntriesToNotSend(emailAddresses);
            if (com.vedantu.util.CollectionUtils.isNotEmpty(foundUnsubscribed)) {
                logger.info("removeBouncedOrComplaintRecipient foundUnsubscribed " + foundUnsubscribed.size());
                for (UnsubscribedEmail unsubscribedEmail : foundUnsubscribed) {
                    emailAddressToRemove.add(unsubscribedEmail.getEmail());
                }
            }
        }


        if (com.vedantu.util.CollectionUtils.isNotEmpty(emailAddressToRemove)) {
            removeEmailListFromInternetAddress(email.getTo(), emailAddressToRemove);
            removeEmailListFromInternetAddress(email.getCc(), emailAddressToRemove);
            removeEmailListFromInternetAddress(email.getBcc(), emailAddressToRemove);
        }

        // removeEmailListFromInternetAddress(email.getReplyTo(),
        // foundNotifications);
    }

    private void populateEmailListFromInternetAddress(List<InternetAddress> addresses, List<String> emailAddresses) {
        if (addresses != null) {
            for (InternetAddress address : addresses) {
                emailAddresses.add(address.getAddress());
            }
        }
    }

    private void removeEmailListFromInternetAddress(List<InternetAddress> internetAddresses,
                                                    List<String> emailListToRemove) {
        for (String emailAddressToRemove : emailListToRemove) {
            if (internetAddresses != null && !internetAddresses.isEmpty()) {
                Iterator<InternetAddress> it = internetAddresses.iterator();
                while (it.hasNext()) {
                    InternetAddress internetAddress = it.next();
                    if (StringUtils.isEmpty(internetAddress.getAddress())
                            || (!StringUtils.isEmpty(emailAddressToRemove)
                            && internetAddress.getAddress().toLowerCase()
                            .equals(emailAddressToRemove.toLowerCase()))) {
                        logger.info("Removing " + internetAddress);
                        it.remove();
                    }
                }
            } else {
                break;
            }
        }
    }

    public BaseResponse sendBulkEmail(EmailRequest email) throws AddressException, MessagingException {

        logger.info("Entering email: " + email.toString());

        removeBouncedOrComplaintRecipient(email);

        // Create a message with the specified information.
        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(FROM);
        Address[] addresses;

        if (email.getCc() != null) {
            addresses = new Address[email.getCc().size()];
            msg.setRecipients(Message.RecipientType.CC, email.getCc().toArray(addresses));
        }

        if (email.getBcc() != null) {
            addresses = new Address[email.getBcc().size()];
            msg.setRecipients(Message.RecipientType.BCC, email.getBcc().toArray(addresses));
        }

        if (email.getReplyTo() != null) {
            addresses = new Address[email.getReplyTo().size()];
            msg.setReplyTo(email.getReplyTo().toArray(addresses));
        }

        msg.setText((email.getBody()) + "");

        msg.setSubject(email.getSubject(), "UTF-8");

        BodyPart htmlPart = new MimeBodyPart();

        addresses = new Address[email.getTo().size()];
        addresses = email.getTo().toArray(addresses);

        BaseEmailResponse successResp, errResp;
        SendEmailResponse response = new SendEmailResponse();
        ArrayList<BaseEmailResponse> bulkResp = new ArrayList<BaseEmailResponse>();

        Email readEmailStatistics = new Email();
        readEmailStatistics.setSubject(email.getSubject());
        readEmailStatistics.setType(email.getType());
        readEmailStatistics.setOpened(false);
        readEmailStatistics.setRole(email.getRole());
        if (email.getManualNotificationId() != null) {
            readEmailStatistics.setManualNotificationId(email.getManualNotificationId());
        }
        if (email.getCallingUserId() != null) {
            readEmailStatistics.setCreatedBy(Long.toString(email.getCallingUserId()));
        }

        for (Address address : addresses) {

            msg.setRecipient(Message.RecipientType.TO, address);

            // Add attachments
            Multipart mp = new MimeMultipart();

            if (email.getAttachment() != null) {
                MimeBodyPart attachment = new MimeBodyPart();
                attachment.setFileName(email.getAttachment().getFileName());
                attachment.setContent(email.getAttachment().getAttachmentData(),
                        email.getAttachment().getApplication());
                mp.addBodyPart(attachment);
            }

            // Add email statistics in db
            readEmailStatistics.setTo(address);
            readEmailStatistics.setId(null);
            if (email.getManualNotificationId() != null) {
                readEmailStatistics.setManualNotificationId(email.getManualNotificationId());
            }
            emailDAO.create(readEmailStatistics);

            // Create query string for opened email
            htmlPart.setContent(email.getBody() + "<img style=\"display: none;\" src=\"" + PLATFORM_URL_READ_EMAIL
                    + "?id=" + readEmailStatistics.getId() + "\"/>", "text/html; charset=UTF-8");
            mp.addBodyPart(htmlPart);
            msg.setContent(mp);

            // Send the email.
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            try {
                msg.writeTo(outputStream);
                RawMessage rawMessage = new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));
                SendRawEmailRequest rawEmailRequest = new SendRawEmailRequest(rawMessage);
                SendRawEmailResult resp;
                resp = client.sendRawEmail(rawEmailRequest);
                successResp = new BaseEmailResponse();
                successResp.setErrorCode(ErrorCode.SUCCESS);
                successResp.setErrorMessage("");
                successResp.setStatus(ErrorCode.SUCCESS.toString());
                successResp.setId(resp.getMessageId());
                successResp.setRejectReason("");
                logger.info("Email Sent Successfully");
                bulkResp.add(successResp);

            } catch (Exception e) {
                if (e instanceof AmazonSimpleEmailServiceException) {
                    logger.info("Error sending email", e);
                } else {
                    logger.error("Error sending email", e);
                }
                errResp = new BaseEmailResponse();
                errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
                errResp.setErrorMessage(e.getMessage());
                bulkResp.add(errResp);
            }
        }

        response.setErrorCode(ErrorCode.SUCCESS);
        response.setErrorMessage("Emails Sent Successfully");
        response.setStatus(bulkResp);
        logger.info("Exiting. bulkResp: " + bulkResp.toString());
        return response;
    }

    public BaseResponse getSendQuota() {

        logger.info("Entering");

        try {
            GetSendQuotaResult response;
            response = client.getSendQuota();
            GetSendQuotaResponse successResp = new GetSendQuotaResponse();
            successResp.setErrorCode(ErrorCode.SUCCESS);
            successResp.setErrorMessage("");
            successResp.setMax24HourSend(response.getMax24HourSend());
            successResp.setMaxSendRate(response.getMaxSendRate());
            successResp.setSentLast24Hours(response.getSentLast24Hours());
            logger.info("Exiting successResp:" + successResp.toString());
            return successResp;

        } catch (Exception e) {
            BaseResponse errResp = new BaseResponse();
            errResp = new BaseEmailResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            errResp.setErrorMessage(e.getMessage());
            logger.info("Exiting errResp:" + errResp.toString());
            return errResp;
        }

    }

    public BaseResponse getSendStatistics(Integer lastHours) {

        logger.info("Entering");

        try {
            GetSendStatisticsResult response;
            response = client.getSendStatistics();
            GetSendStatisticsResponse successResp = new GetSendStatisticsResponse();
            Date dateBeforeLastHours = new Date(System.currentTimeMillis() - TimeUnit.HOURS.toMillis(lastHours));
            Long bounces = (long) 0, deliveryAttempts = (long) 0, rejects = (long) 0, complaints = (long) 0;

            List<SendDataPoint> dataPoints = response.getSendDataPoints();
            for (SendDataPoint dataPoint : dataPoints) {
                if (dataPoint.getTimestamp().after(dateBeforeLastHours)) {
                    bounces += dataPoint.getBounces();
                    deliveryAttempts += dataPoint.getDeliveryAttempts();
                    rejects += dataPoint.getRejects();
                    complaints += dataPoint.getComplaints();
                }
            }

            successResp.setBounces(bounces);
            successResp.setComplaints(complaints);
            successResp.setDeliveryAttempts(deliveryAttempts);
            successResp.setRejects(rejects);
            successResp.setErrorCode(ErrorCode.SUCCESS);
            successResp.setErrorMessage("");
            logger.info("Exiting successResp:" + successResp.toString());
            return successResp;

        } catch (Exception e) {
            BaseResponse errResp = new BaseResponse();
            errResp = new BaseEmailResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            errResp.setErrorMessage(e.getMessage());
            logger.info("Exiting errResp:" + errResp.toString());
            return errResp;
        }

    }

    public BaseResponse emailOpened(String id) {

        logger.info("     emailOpened  Amazon     ");

        Email email = emailDAO.getById(id);

        if (email != null) {
            email.setOpened(true);
            emailDAO.create(email);
            EmailOpenedResponse successResp = new EmailOpenedResponse(email);
            successResp.setErrorCode(ErrorCode.SUCCESS);
            successResp.setErrorMessage("");
            logger.info("Exiting successResp:" + successResp.toString());
            return successResp;
        } else {
            BaseResponse errResp = new BaseResponse();
            errResp = new BaseEmailResponse();
            errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
            errResp.setErrorMessage("No such email found in the database");
            logger.info("Exiting errResp:" + errResp.toString());
            return errResp;
        }
    }

    private List<InternetAddress> getRemovedEntries(List<InternetAddress> preUpdateList,
                                                    List<InternetAddress> postUpdateList) {
        List<InternetAddress> removedEntries = new ArrayList<>();
        if (preUpdateList != null) {
            if (CollectionUtils.isEmpty(postUpdateList)) {
                removedEntries.addAll(preUpdateList);
            } else {
                for (InternetAddress preUpdateEntry : preUpdateList) {
                    if (!postUpdateList.contains(preUpdateEntry)) {
                        removedEntries.add(preUpdateEntry);
                    }
                }
            }
        }

        return removedEntries;
    }

    private ArrayList<InternetAddress> getBounceAlertAddress() {
        ArrayList<InternetAddress> bounceAddressList = new ArrayList<InternetAddress>();
        try {
            InternetAddress bounceAddress = new InternetAddress(
                    ConfigUtils.INSTANCE.getStringValue("email.bounce.alert.address"));
            bounceAddressList.add(bounceAddress);
        } catch (AddressException ex) {
            logger.error("getBounceAlertAddress - " + ex.getMessage());
        }
        return bounceAddressList;
    }

    public static void main(String[] args) {
        try {
            String s = "<a href='https://isl.vedantu.com/?utm_source=ISL2017&utm_term=EMAIL&utm_campaign=ISL_OLD_COLD_USER&token=59c202431e2c4182c812e5d5' track='true'/>ajith<a>";
            Document doc = Jsoup
                    .parseBodyFragment(s);
            Elements links = doc.getElementsByTag("a");
            Iterator<Element> it = links.iterator();
            while (it.hasNext()) {
                Element link = it.next();
                if (StringUtils.isNotEmpty(link.attr(Email.Constants.LINK_ATTRITUBE_TRACK))
                        && link.attr(Email.Constants.LINK_ATTRITUBE_TRACK).equals("true")
                        && StringUtils.isNotEmpty(link.attr(Email.Constants.LINK_ATTRITUBE_HREF))) {
                    String newhref = "https://tracker.vedantu.com/platform/notification-centre/email/emailOpened?";
                    String href = link.attr(Email.Constants.LINK_ATTRITUBE_HREF);
                    String randomId = RandomStringUtils.randomAlphanumeric(4);
                    newhref += ("linkRandomId=" + randomId
                            + "&emailId=1"
                            + "&redirectUrl=" + URLEncoder.encode(href, Constants.DEFAULT_CHARSET));
                    link.attr(Email.Constants.LINK_ATTRITUBE_HREF, newhref);
                    System.err.println("link " + link.attr(Email.Constants.LINK_ATTRITUBE_HREF));
                }
            }

            String s2 = Jsoup.clean(doc.html(), Whitelist.basic()).replaceAll("&amp;", "&");
            System.err.println(">>> s " + s2);
        } catch (Exception e) {
            System.err.println("Error in creating click trackers for " + e.getMessage());
        }
    }
}


