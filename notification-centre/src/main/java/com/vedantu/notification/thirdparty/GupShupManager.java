package com.vedantu.notification.thirdparty;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.dao.BlockedPhoneDao;
import com.vedantu.notification.entity.BlockedPhone;
import com.vedantu.notification.entity.GupshupDeliveryReport;
import com.vedantu.notification.entity.SmsLinksOpenStatus;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.interfaces.ICloudSMSManager;
import com.vedantu.notification.managers.SMSManager;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.SendSMSResponse;
import com.vedantu.notification.serializers.GupshupDeliveryReportDAO;

import java.util.ArrayList;

import com.vedantu.util.*;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;


/*
 * Exotel Manager for message delivery
 */
@Service
public class GupShupManager implements
        ICloudSMSManager {

    private static String USER_ID;
    private static String PASSWORD;
    private static String SMS_TRACKER_ENDPOINT;
    private static String PLATFORM_URL_READ_SMS;
    public static final WebUtils INSTANCE = new WebUtils();
    @Autowired
    private LogFactory logFactory;

    @Autowired
    private GupshupDeliveryReportDAO gupshupDeliveryReportDAO;

    @Autowired
    private BlockedPhoneDao blockedPhoneDao;

    @Autowired
    private SMSManager smsManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(GupShupManager.class);

    private static final List<CommunicationType> avoidGupshupFilterList = new ArrayList<>(Arrays.asList(CommunicationType.PHONE_VERIFICATION, CommunicationType.FORGOT_PASSWORD,
            CommunicationType.GENERATE_PASSWORD, CommunicationType.SEO_MANAGER_SIGNUP_DOWNLOAD_SMS, CommunicationType.OTF_SESSION_FEEDBACK, CommunicationType.WAVE_LEADERBOARD_RECOGNITION,
            CommunicationType.WAVE_STREAK_RECOGNITION));

    @PostConstruct
    public void init() {
        USER_ID = ConfigUtils.INSTANCE.getStringValue("gupshup.userId");
        PASSWORD = ConfigUtils.INSTANCE.getStringValue("gupshup.password");
        SMS_TRACKER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SMS_TRACKER_ENDPOINT");
        if (com.vedantu.util.StringUtils.isEmpty(SMS_TRACKER_ENDPOINT)) {
            SMS_TRACKER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
        }
        PLATFORM_URL_READ_SMS = SMS_TRACKER_ENDPOINT
                + ConfigUtils.INSTANCE.getStringValue("platform.url.read.sms");
    }

    public BaseResponse sendSMS(String toNumber, String message,
            boolean highPriority, String manualNotificationId, CommunicationType type, Role role, String sqsMessageId) throws UnsupportedEncodingException {
        String responseString;

        //try {
        //change
        GupshupDeliveryReport report = new GupshupDeliveryReport();
        report.setPhoneNo(toNumber);
        report.setHighPriority(highPriority);
        report.setSmsText(message);
        report.setType(type);
        report.setRole(role);
        report.setSqsMessageId(sqsMessageId);
        logger.info("SMS SQSMESSAGEID BEFORE SAVE: " + sqsMessageId);
        gupshupDeliveryReportDAO.create(report);
        logger.info("  storing data  " + report);
        logger.info("Entering. to:" + toNumber + " message:" + message);
        String newUrl = SMS_TRACKER_ENDPOINT + "/notification-centre/SMS/track?";
        String replaceMessage = message;
        if (message != null) {
            try {
                logger.info("  try  ");
                ArrayList links = new ArrayList();
                String regex = "\\(?\\b(https://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";
                Pattern p = Pattern.compile(regex);
                Matcher m = p.matcher(message);
                while (m.find()) {
                    String urlStr = m.group();
                    links.add(urlStr);
                }
                if (links != null) {
                    for (int i = 0; i < links.size(); i++) {
                        String randomId = RandomStringUtils.randomAlphanumeric(4);
                        String href = links.get(i).toString();
                        SmsLinksOpenStatus smsLinksOpenStatus = new SmsLinksOpenStatus();
                        smsLinksOpenStatus.setLinkUrl(href);
                        smsLinksOpenStatus.setRandomId(randomId);
                        report.addSmsLink(smsLinksOpenStatus);
                        newUrl += "id=" + report.getId()
                                + "&linkRandomId=" + randomId
                                + "&redirectUrl=" + URLEncoder.encode(href, Constants.DEFAULT_CHARSET);
                        String newUrlshorter = WebUtils.INSTANCE.shortenUrl(newUrl);
                        replaceMessage = replaceMessage.replace(href, newUrlshorter);
                        newUrl = SMS_TRACKER_ENDPOINT + "/notification-centre/SMS/track?";
                    }
                }

            } catch (Exception e) {
                logger.warn("Error in creating click trackers for " + e.getMessage());
            }
            logger.info("\nAdding information about calling user id from inside clickTrackerEnabled   " + report.getCreatedBy() + "\n");

            gupshupDeliveryReportDAO.create(report);
            logger.info("     final  report : " + report);
        }

        String url = "https://enterprise.smsgupshup.com/GatewayAPI/rest?";
        String data = "method=sendMessage";
        data += "&userid=" + USER_ID; // your loginId
        data += "&password=" + URLEncoder.encode(PASSWORD, "UTF-8"); // your password
        data += "&msg=" + URLEncoder.encode(replaceMessage, "UTF-8");
        data += "&send_to=" + URLEncoder.encode(toNumber, "UTF-8"); // a valid 10 digit phone no.
        data += "&v=1.1";
        data += "&msg_type=TEXT"; // Can by "FLASH" or"UNICODE_TEXT" or "BINARY"
        data += "&auth_scheme=PLAIN";
        data += "&override_dnd=true";

        ClientResponse resp = WebUtils.INSTANCE.doCall(url + data, HttpMethod.GET, null, false, false);
        responseString = resp.getEntity(String.class);
        logger.info("    Gupshup response :    " + responseString);
        if (resp.getStatus() == HttpStatus.OK.value() && StringUtils.isNotEmpty(responseString)) {
            String[] tokens = responseString.split(Pattern.quote("|"));
            if (tokens != null && tokens.length > 0 && StringUtils.isNotEmpty(tokens[0])) {
                String statusText = tokens[0].trim().toLowerCase();
                if ("success".equals(statusText)) {
                    SendSMSResponse successResp = new SendSMSResponse();
                    successResp.setErrorCode(ErrorCode.SUCCESS);
                    successResp.setStatus("success");
                    if (tokens.length > 2) {
                        successResp.setSmsId(tokens[2].trim());
                        // GupshupDeliveryReport report = new GupshupDeliveryReport();
                        //report.setPhoneNo(toNumber);
                        //report.setHighPriority(highPriority);
                        //report.setSmsText(message);
                        logger.info("   report.setSmsText  " + report.getSmsText());
                        report.setExternalId(tokens[2].trim());
                        if (manualNotificationId != null) {
                            report.setManualNotificationId(manualNotificationId);
                        }
                        gupshupDeliveryReportDAO.create(report);
                    }
                    successResp.setErrorMessage("");
                    logger.info("Exiting succesResponse: " + successResp.toString());
                    return successResp;
                } else if ("error".equals(statusText)) {
                    //will handle while monitoring delivery reports
                } else {
                    logger.error("gupshup sms statustext is not as expected, response "
                            + responseString);
                }
            } else {
                logger.error("gupshup sms response is not as expected, response "
                        + responseString);
            }
        }

        BaseResponse errResp = new BaseResponse();
        errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
        errResp.setErrorMessage(responseString);

        logger.info("Exiting. errrorResponse: " + errResp.toString());
        return errResp;

    }

    @Override
    public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, CommunicationType type, Role role, String sqsMessageId) throws UnsupportedEncodingException {
        return sendSMS(toNumber, message, highPriority, null, type, role, sqsMessageId);
    }

    public BaseResponse sendBulkSMS(ArrayList<String> toNumbers, String message, boolean highPriority, String manualNotificationId, CommunicationType type, Role role) throws JSONException {

        logger.info("Bulk sms sending");
        logger.info("Entering. to:" + toNumbers.toString() + " message:" + message);
        //throw new UnsupportedOperationException("bulk SMS not implemented for gupshup sms");
        for (String number : toNumbers) {
            try {
                if (StringUtils.isNotEmpty(number)) {
                    if(null != type && !avoidGupshupFilterList.contains(type)) {
                        TextSMSRequest textSMSRequest = new TextSMSRequest();
                        textSMSRequest.setTo(number);
                        textSMSRequest.setPhoneCode("+91");
                        textSMSRequest.setType(type);
                        boolean sendSMS = smsManager.mongoFilterForSMSCount(textSMSRequest);
                        if (!sendSMS) {
                            logger.info("Daily limit exceeded for " + number  + "and type " + type);
                            continue;
                        }
                    }
                    sendSMS(number, message, highPriority, manualNotificationId, type, role, null);
                }
            } catch (Exception ex) {
                logger.error("Error in sending bulk SMS");
            }
        }
        if (toNumbers.size() >= 10) {
            logger.info("Numbers in sendBulkSMS " + toNumbers.size());
        }
        return new BaseResponse(ErrorCode.SUCCESS, "success");

    }

    @Override
    public BaseResponse sendBulkSMS(ArrayList<String> toNumbers, String message, boolean highPriority, CommunicationType type, Role role) throws JSONException {
        return sendBulkSMS(toNumbers, message, highPriority, null, type, role);
    }

}
