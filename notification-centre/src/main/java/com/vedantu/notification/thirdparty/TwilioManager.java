package com.vedantu.notification.thirdparty;

import com.twilio.Twilio;
import com.twilio.exception.ApiException;
import com.twilio.exception.TwilioException;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.GupshupDeliveryReport;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.interfaces.ICloudSMSManager;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.SendSMSResponse;
import com.vedantu.notification.serializers.GupshupDeliveryReportDAO;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import com.vedantu.notification.serializers.GupshupDeliveryReportDAO;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import org.apache.commons.lang.StringUtils;


/*
 * Twilio Manager for message delivery
 */
@Service
public class TwilioManager implements
		ICloudSMSManager {

	public static String ACCOUNT_SID;
        public static String AUTH_TOKEN;
        public static String FROM_NUMBER;
        public static String MSID;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private GupshupDeliveryReportDAO gupshupDeliveryReportDAO;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(TwilioManager.class);	
	
        public TwilioManager() {
            ACCOUNT_SID = ConfigUtils.INSTANCE.getStringValue("twilio.account_sid");
            AUTH_TOKEN = ConfigUtils.INSTANCE.getStringValue("twilio.auth_token");
            //FROM_NUMBER = ConfigUtils.INSTANCE.getStringValue("twilio.from_number");
            MSID = ConfigUtils.INSTANCE.getStringValue("twilio.msid");
            Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        }

	public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, String sqsMessageId) throws JSONException {

		logger.info("Entering. to:"+toNumber+" message:"+message);

		Message twilioMessage;
		try {
                twilioMessage = Message
                               .creator(new PhoneNumber(toNumber),  // to
                                               MSID,  // from
                                               message).create();
		} catch (ApiException ex) {
			if (ex.getMessage().contains("is not a valid phone number")) {
				BaseResponse errResp = new BaseResponse();
				errResp.setErrorCode(ErrorCode.BAD_REQUEST_ERROR);
				errResp.setErrorMessage(ex.getMessage());
				logger.info("Exiting. twilio errror: ", ex);
				return errResp;
			} else throw ex;
		}
		
		if(StringUtils.isNotEmpty(twilioMessage.getErrorMessage())){
			BaseResponse errResp = new BaseResponse();
			errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			errResp.setErrorMessage(twilioMessage.getErrorMessage());
			logger.info("Exiting. errrorResponse: "+errResp.toString() + " status: "+ twilioMessage.getStatus().toString());
			return errResp;
		}
		
		SendSMSResponse successResp = new SendSMSResponse();
		successResp.setErrorCode(ErrorCode.SUCCESS);
                if(twilioMessage.getStatus()!=null){
                    successResp.setStatus(twilioMessage.getStatus().toString());
                }
		successResp.setSmsId(twilioMessage.getSid());	
		successResp.setErrorMessage(twilioMessage.getErrorMessage());
		
		logger.info("Exiting. succesResponse: "+successResp.toString());

		GupshupDeliveryReport report = new GupshupDeliveryReport();
		report.setPhoneNo(toNumber);
		report.setHighPriority(highPriority);
		report.setSmsText(message);
		report.setIsInternational(true);
//		report.setType(type);
//		report.setRole(role);

		gupshupDeliveryReportDAO.create(report);
		logger.info("  storing data  " + report);

		return successResp;
		
		
	}	
	
	public BaseResponse sendBulkSMS(ArrayList<String> toNumbers, String message, boolean highPriority) throws JSONException {

		logger.info("Entering. to:"+toNumbers.toString()+" message:"+message);
		throw new UnsupportedOperationException("bulk SMS not implemented for international/twilio sms");

	}

	@Override
	public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, CommunicationType type, Role role, String sqsMessageId) throws JSONException, UnsupportedEncodingException {
		return null;
	}

	@Override
	public BaseResponse sendBulkSMS(ArrayList<String> toNumber, String message, boolean highPriority, CommunicationType type, Role role) throws JSONException {
		return null;
	}
}
