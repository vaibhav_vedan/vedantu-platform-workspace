/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.thirdparty;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.notification.entity.TelegramDoubt;
import com.vedantu.notification.entity.TelegramGroup;
import com.vedantu.notification.entity.TelegramUser;
import com.vedantu.notification.enums.TelegramMessageType;
import com.vedantu.notification.pojo.TelegramGetChatPojo;
import com.vedantu.notification.request.AddBatchTelegramGroupRequest;
import com.vedantu.notification.request.GetTelegramDoubtRequest;
import com.vedantu.notification.request.TelegramUserVerificationRequest;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.TelegramDoubtResponse;
import com.vedantu.notification.response.TelegramGetChatResponse;
import com.vedantu.notification.response.TelegramResponse;
import com.vedantu.notification.serializers.TelegramDoubtDAO;
import com.vedantu.notification.serializers.TelegramGroupDAO;
import com.vedantu.notification.serializers.TelegramUserDAO;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.reflect.Type;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.print.DocFlavor;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */

@Service
public class TelegramActorManager {

    @Autowired
    private TelegramUserDAO telegramUserDAO;
    
    @Autowired
    private TelegramGroupDAO telegramGroupDAO;
    
    @Autowired
    private TelegramDoubtDAO telegramDoubtDAO;
    
    @Autowired
    private HttpSessionUtils httpSessionUtils;
    
    @Autowired
    private FosUtils fosUtils;
    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TelegramObserverManager.class);
    public final static String BOT_TOKEN = ConfigUtils.INSTANCE.getStringValue("telegram.bot.token");
    public final static String telegram_url = ConfigUtils.INSTANCE.getStringValue("telegram.endpoint") + BOT_TOKEN + "/";    
    public final static String subscription_url = ConfigUtils.INSTANCE.getStringValue("SUBSCRIPTION_ENDPOINT");
    public final static String user_url = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    public Gson gson = new Gson();
    
    public boolean checkIfAlreadyExists(TelegramUserVerificationRequest req){

        TelegramUser telegramUser = telegramUserDAO.getByTelegramId(req.getTelegramUserPojo().getId());
        
        TelegramUser telegramUser1 = telegramUserDAO.getByUserId(req.getCallingUserId());
        
        if(telegramUser!=null || telegramUser1!=null){
            return true;
        }

        return false;
    }
    
    public PlatformBasicResponse verifyTelegramLogin(TelegramUserVerificationRequest req) throws ConflictException, VException{
        
        if(checkIfAlreadyExists(req)){
            throw new ConflictException(ErrorCode.USER_ALREADY_EXISTS, "user already joined for this account");
        }
        
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(req.getCallingUserId(), true);

        
        TelegramUser telegramUser = new TelegramUser();
        telegramUser.setUserId(userBasicInfo.getUserId());        
        telegramUser.setEmail(userBasicInfo.getEmail());
        telegramUser.setRole(userBasicInfo.getRole());
        telegramUser.setTelegramUserPojo(req.getTelegramUserPojo());
        telegramUser.setTelegramId(req.getTelegramUserPojo().getId());
        telegramUser.setUsername(req.getTelegramUserPojo().getUsername());
        List<String> batchIds = getBatchIds(telegramUser.getUserId().toString());
        telegramUser.setBatchIds(new HashSet<>(batchIds));
        telegramUserDAO.save(telegramUser, req.getCallingUserId().toString());
        logger.info("BatchIds: "+batchIds);
        List<TelegramGroup> groups = telegramGroupDAO.getByBatchIds(batchIds);
        
        /*if(userBasicInfo.getEmail().contains("@vedantu.com")){

            return new PlatformBasicResponse(true, null, null);
        }*/
        
        List<String> joinLinks = new ArrayList<>(); 
        /*String message = "";
        if(ArrayUtils.isNotEmpty(groups)){
            for(TelegramGroup telegramGroup : groups){
                joinLinks.add(telegramGroup.getTelegramJoinLink());
            }

            message = "Here are the links to join groups of your batches: \n";
            String links = StringUtils.join(joinLinks.toArray(), ",\n");
            logger.info("Join links: "+links);
            message = message + links;
            logger.info("Message: "+message);
        }*/
        
        try{
            ClientResponse resp = WebUtils.INSTANCE.doCall(user_url + "/markTelegramLinked?userId="+telegramUser.getUserId(), HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);        
        }catch(Exception ex){
            logger.error("Error in marking user telegram linked"+ex.getMessage());
        }
        
        
        try{
            
            Map<String, Object> bodyScopes = new HashMap<>();
            bodyScopes.put("username", telegramUser.getTelegramUserPojo().getFirst_name());
            String message = getTelegramMessage(bodyScopes, TelegramMessageType.WELCOME_MESSAGE);            
            sendMessage(telegramUser.getTelegramId(), message); // to be made into async task
            
        }catch(Exception ex){
            logger.error("Error in sending message : "+ex.getMessage());
        }
        // create async task to send messages about batch groups
        return new PlatformBasicResponse(true, null, null);
        
    }
    
    public List<String> getBatchIds(String userId) throws VException{
        ClientResponse resp = WebUtils.INSTANCE.doCall(subscription_url+"/batch/getBatchIdsForUser?userId="+userId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<String>>(){}.getType();
        List<String> batchIds = gson.fromJson(jsonString, listType);
        return batchIds;
    }
    
    public void sendMessage(Long chat_id, String text) throws VException, UnsupportedEncodingException{
        logger.info("Sending message on telegram");
        ClientResponse response = WebUtils.INSTANCE.doCall(telegram_url+"sendMessage?chat_id="+chat_id+"&text="+URLEncoder.encode(text,"UTF-8"), HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        TelegramResponse telegramResponse = gson.fromJson(jsonString, TelegramResponse.class);
        if(!telegramResponse.isOk()){
            logger.error("Error in sending message to telegram chat: "+chat_id);
        }        
        
    }  
    
    public void forwardMessage(Long chat_id, Long from_chat_id, Long message_id) throws VException, UnsupportedEncodingException{
        logger.info("Sending message on telegram");
        ClientResponse response = WebUtils.INSTANCE.doCall(telegram_url+"forwardMessage?chat_id="+chat_id+"&from_chat_id="+from_chat_id+"&message_id="+message_id+"&disable_notification=false", HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        logger.info("jsonString respone: "+jsonString);
    }     
    
    public BatchBasicInfo getBatchBasicInfo(String batchId) throws VException{
        ClientResponse response = WebUtils.INSTANCE.doCall(subscription_url+"/batch/basicInfo/"+batchId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        logger.info("jsonString respone: "+jsonString); 
        BatchBasicInfo batchBasicInfo = gson.fromJson(jsonString, BatchBasicInfo.class);
        return batchBasicInfo;
    }
    
    public void leaveChat(Long chat_id) throws VException, UnsupportedEncodingException{
        logger.info("Sending message on telegram");
        ClientResponse response = WebUtils.INSTANCE.doCall(telegram_url+"getChat?chat_id="+chat_id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        TelegramGetChatResponse telegramResponse = gson.fromJson(jsonString, TelegramGetChatResponse.class);
        if(!telegramResponse.isOk()){
            logger.error("Error in leaving group: "+chat_id);
        }        
    }      
    
    public TelegramGetChatPojo getChat(Long chat_id) throws VException, UnsupportedEncodingException{
        logger.info("Sending message on telegram");
        ClientResponse response = WebUtils.INSTANCE.doCall(telegram_url+"getChat?chat_id="+chat_id, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);        
        String jsonString = response.getEntity(String.class);
        TelegramGetChatResponse telegramResponse = gson.fromJson(jsonString, TelegramGetChatResponse.class);
        if(!telegramResponse.isOk()){
            logger.error("Error in getting chat info: "+chat_id);
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Error in retrieving chat group");
        }        
        return telegramResponse.getResult();
    }  
    
    public void createGroup(AddBatchTelegramGroupRequest req) throws VException, UnsupportedEncodingException{
        
        logger.info("Adding group");
        BatchBasicInfo batchBasicInfo = getBatchBasicInfo(req.getBatchId());
        
        if(batchBasicInfo == null){
            throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "Batch not found batchId: "+req.getBatchId());
        }
        
        TelegramGroup telegramGroup = null;
        if(telegramGroupDAO.getByBatchId(req.getBatchId()) != null){
            throw new ConflictException(ErrorCode.BAD_REQUEST_ERROR, "batch already exists for batchId: "+req.getBatchId());
        }
        
        if(telegramGroupDAO.getByTelegramId(req.getChatId()) != null){
            telegramGroup = telegramGroupDAO.getByTelegramId(req.getChatId());
            //throw new ConflictException(ErrorCode.BAD_REQUEST_ERROR, "group already exists for telegramId: "+req.getBatchId());
        }
        
        TelegramGetChatPojo telegramGetChatPojo = getChat(req.getChatId());
        if(telegramGetChatPojo == null){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "chat not found: "+req.getChatId());
        }        
        
        if(StringUtils.isEmpty(telegramGetChatPojo.getType()) || (StringUtils.isNotEmpty(telegramGetChatPojo.getType()) && !telegramGetChatPojo.getType().toLowerCase().equals("supergroup"))){
            throw new ConflictException(ErrorCode.BAD_REQUEST_ERROR, "Non group chat id");
        }
        if(telegramGroup == null){
            telegramGroup = new TelegramGroup();
            Set<String> batchIds = new HashSet<>();
            //batchIds.add(req.getBatchId());
            telegramGroup.setBatchIds(batchIds);  
            telegramGroup.setTelegramId(req.getChatId());
            telegramGroup.setTelegramJoinLink(telegramGetChatPojo.getInvite_link());
            telegramGroup.setType(telegramGetChatPojo.getType());
            telegramGroup.setTitle(telegramGetChatPojo.getTitle());                        
        }
        
        telegramGroup.getBatchIds().add(req.getBatchId());

        telegramGroupDAO.save(telegramGroup, null);
        
    }

    public List<TelegramDoubtResponse> getDoubts(GetTelegramDoubtRequest req){
        List<TelegramDoubt> telegramDoubts = telegramDoubtDAO.getTelegramDoubts(req);
        
        if(ArrayUtils.isEmpty(telegramDoubts)){
            return new ArrayList<>();
        }
        List<TelegramDoubtResponse> result = new ArrayList<>();
        Map<String, TelegramGroup> groupMap = new HashMap<>();
        Map<Long, TelegramUser> userMap = new HashMap<>();
        for(TelegramDoubt telegramDoubt : telegramDoubts){
            TelegramDoubtResponse telegramDoubtResponse = new TelegramDoubtResponse();
            telegramDoubtResponse.setAskedAt(telegramDoubt.getCreationTime());
            telegramDoubtResponse.setBatchId(telegramDoubt.getBatchId());
            telegramDoubtResponse.setClosed(telegramDoubt.isClosed());
            telegramDoubtResponse.setDoubtText(telegramDoubt.getMessage());
            telegramDoubtResponse.setDoubtId(telegramDoubt.getId());
            if(!groupMap.containsKey(telegramDoubt.getBatchId())){
                groupMap.put(telegramDoubt.getBatchId(), telegramGroupDAO.getByBatchId(telegramDoubt.getBatchId()));
            }
            telegramDoubtResponse.setBatchGroupTitle(groupMap.get(telegramDoubt.getBatchId()).getTitle());
            if(!userMap.containsKey(telegramDoubt.getTelegramUserId())){
                userMap.put(telegramDoubt.getTelegramUserId(), telegramUserDAO.getByTelegramId(telegramDoubt.getTelegramUserId()));
            }
            telegramDoubtResponse.setName(userMap.get(telegramDoubt.getTelegramUserId()).getTelegramUserPojo().getFirst_name());
            result.add(telegramDoubtResponse);
        }
        
        return result;
        
    }
    
    public void markDoubtClosed(String doubtId) throws NotFoundException{
        
        TelegramDoubt telegramDoubt = telegramDoubtDAO.getEntityById(doubtId, TelegramDoubt.class);
        if(doubtId == null){
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Doubt not found");
        }
        
        telegramDoubt.setClosed(true);
        telegramDoubt.setClosedBy(httpSessionUtils.getCurrentSessionData().getUserId());
        telegramDoubtDAO.save(telegramDoubt, String.valueOf(httpSessionUtils.getCurrentSessionData().getUserId()));
        
    }
    
    
    public void requestTelegramLink(String batchId, Long userId) throws VException{
        
        TelegramUser telegramUser = telegramUserDAO.getByUserId(userId);
        
        if(telegramUser == null){
            throw new NotFoundException(ErrorCode.TELEGRAM_NOT_LINKED, "telegram user not linked");
        }
        
        List<String> batchIds = getBatchIds(userId.toString());
        if(ArrayUtils.isNotEmpty((batchIds))){
            telegramUser.getBatchIds().addAll(batchIds);
        }
        
        if(!telegramUser.getBatchIds().contains(batchId)){
            throw new ConflictException(ErrorCode.BATCH_ENROLLMENT_NOT_FOUND, "user not part of batch");
        }
        
        TelegramGroup telegramGroup = telegramGroupDAO.getByBatchId(batchId);
        if(telegramGroup == null){
            throw new NotFoundException(ErrorCode.TELEGRAM_BATCH_NOT_FOUND, "Batch group not found");
        }
        
        BatchBasicInfo batchBasicInfo = getBatchBasicInfo(batchId);
        try{
            Map<String, Object> bodyScopes = new HashMap<>();
            bodyScopes.put("batchTitle", batchBasicInfo.getCourseInfo().getTitle());
            bodyScopes.put("batchLink", telegramGroup.getTelegramJoinLink());
            String message = getTelegramMessage(bodyScopes, TelegramMessageType.BATCH_MESSAGE);        
            sendMessage(telegramUser.getTelegramId(), message);
            sendMessage(telegramUser.getTelegramId(), getTelegramMessage(bodyScopes, TelegramMessageType.FOOTER_MESSAGE));
        }catch(Exception ex){
            logger.error("Error in sending message to telegram: ",ex);
        }
    }
    
    public PlatformBasicResponse isTelegramBatch(String batchId) throws NotFoundException{
        TelegramGroup telegramGroup = telegramGroupDAO.getByBatchId(batchId);
        if(telegramGroup == null){
            throw new NotFoundException(ErrorCode.TELEGRAM_BATCH_NOT_FOUND, "telegram group not found");
        }else{
            return new PlatformBasicResponse(true, "", "");
        }
    }
    
    private String getTelegramMessage(Map<String, Object> scopes, TelegramMessageType telegramMessageType) throws IOException{
        Writer writer = new StringWriter();
        MustacheFactory mf = new DefaultMustacheFactory();
        String propertyValue = ConfigUtils.INSTANCE.getStringValue("telegram.message."+telegramMessageType.name());
        Mustache mustache = mf.compile(new StringReader(propertyValue), "emailSubject");
        mustache.execute(writer, scopes);
        writer.flush();
        return writer.toString();
        
    }
    
}
