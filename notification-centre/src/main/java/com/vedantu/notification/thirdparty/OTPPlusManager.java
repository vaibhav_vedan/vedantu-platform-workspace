package com.vedantu.notification.thirdparty;

import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.GupshupDeliveryReport;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.interfaces.ICloudSMSManager;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.serializers.GupshupDeliveryReportDAO;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.logging.log4j.Logger;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

@Service
public class OTPPlusManager implements ICloudSMSManager {

    private static String KEY;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MgageManager.class);

    @PostConstruct
    public void init() {
        KEY = ConfigUtils.INSTANCE.getStringValue("otpPlus.key");
    }

    private int invalidMobileNumberCount = 0;//just a small hack to know the intensity of invalid numbers

    @Autowired
    private GupshupDeliveryReportDAO gupshupDeliveryReportDAO;

    @Override
    public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, CommunicationType type, Role role, String sqsMessageId) throws JSONException, UnsupportedEncodingException {
        String responseString;
        BaseResponse res = new BaseResponse();
        logger.info("Entering. to:" + toNumber + " message:" + message);
        String url = "https://pod1-japi.instaalerts.zone/httpapi/QueryStringReceiver?ver=1.0&key="
                + KEY + "&dest=" + toNumber + "&send=VEDNTU&text=" + URLEncoder.encode(message, "UTF-8");
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, false, false);
        responseString = resp.getEntity(String.class);
        logger.info("OtpPlus response :" + responseString);
        if (!(StringUtils.isNotEmpty(responseString) && (responseString.contains("Statuscode=200") || responseString.contains("Invalid mobile number")))) {
            logger.error("Error in sending OTP trying OtpPlus");
            res.setErrorCode(ErrorCode.SERVICE_ERROR);
            res.setErrorMessage(responseString);
        } else {
            if (StringUtils.isNotEmpty(responseString) && responseString.contains("Invalid mobile number")) {
                invalidMobileNumberCount++;
                if (invalidMobileNumberCount > 1000) {
                    logger.error("reached invalid mobile number count of 1000 for this EC2 instance, resetting the counter. Keep an eye on the frequency of this counter");
                    invalidMobileNumberCount = 0;
                }
            }
            res.setErrorCode(ErrorCode.SUCCESS);
            res.setErrorMessage(responseString);

            GupshupDeliveryReport report = new GupshupDeliveryReport();
            report.setPhoneNo(toNumber);
            report.setHighPriority(highPriority);
            report.setSmsText(message);
            report.setType(type);
            report.setRole(role);

            gupshupDeliveryReportDAO.create(report);
            logger.info("  storing data  " + report);
        }

        return res;
    }

    @Override
    public BaseResponse sendBulkSMS(ArrayList<String> toNumber, String message, boolean highPriority, CommunicationType type, Role role) throws JSONException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
