package com.vedantu.notification.thirdparty;

import javax.annotation.PreDestroy;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClient;
import com.vedantu.notification.serializers.EmailDAO;
import com.vedantu.notification.thirdparty.AmazonSESSDK.AbstractAmazonSESSDKManager;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

/*
 * Amazon SES Manager for email delivery
 */
@Service
public class AmazonSESSDKManager extends AbstractAmazonSESSDKManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AmazonSESSDKManager.class);

    @Autowired
    public EmailDAO emailDAO;

    private static String AWS_REGION;
    private AmazonSimpleEmailServiceClient client = null;

    public AmazonSESSDKManager() {
        super();
        AWS_REGION = ConfigUtils.INSTANCE.getStringValue("amazon.aws.region");

        logger.info("Attempting to connect to the Amazon AWS client...");
        // Connect to Amazon AWS client
        try {
            client = new AmazonSimpleEmailServiceClient();
            Region REGION = Region.getRegion(Regions.fromName(AWS_REGION));
            client.setRegion(REGION);
            ClientConfiguration clientConfiguration = client.getClientConfiguration();
            logger.info("Connected to Amazon AWS Successfully" + AWS_REGION);

        } catch (Exception e) {
            logger.error("Exception Occured While connecting to AMAZON AWS client", e);
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (client != null) {
                client.shutdown();
            }
        } catch (Exception e) {
            logger.error("Error in closing AmazonSESSDKManager connection ", e);
        }
    }
}
