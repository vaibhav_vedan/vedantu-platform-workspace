package com.vedantu.notification.thirdparty;

import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.interfaces.ICloudSMSManager;
import com.vedantu.notification.response.BaseResponse;
import com.vedantu.notification.response.SendBulkSMSResponse;
import com.vedantu.notification.response.SendSMSResponse;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.apache.logging.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebCommunicator;


/*
 * Exotel Manager for message delivery
 */
@Service
public class ExotelManager implements
		ICloudSMSManager {

	private static String EXOTEL_BASE_URL;
	private static String SEND_SMS_URL = "/Sms/send";
	private static String CALLER_ID;
	
	

	@Autowired
	private LogFactory logFactory;
	
	@Autowired
	private WebCommunicator webCommunicator;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(ExotelManager.class);	
	
	public ExotelManager() {
		super();
		EXOTEL_BASE_URL = "https://"
				+ ConfigUtils.INSTANCE.getStringValue("exotel.sid") + ":"
				+ ConfigUtils.INSTANCE.getStringValue("exotel.token")
				+ "@twilix.exotel.in/v1/Accounts/"
				+ ConfigUtils.INSTANCE.getStringValue("exotel.sid");
		CALLER_ID = ConfigUtils.INSTANCE.getStringValue("exotel.caller.id");

	}

	public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, String sqsMessageId) throws JSONException {

		logger.info("Entering. to:"+toNumber+" message:"+message);
		String url = EXOTEL_BASE_URL + SEND_SMS_URL;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("From", CALLER_ID));
		params.add(new BasicNameValuePair("To", toNumber));
		params.add(new BasicNameValuePair("Body", message.trim()));
                if(highPriority) {
                    params.add(new BasicNameValuePair("Priority", "high"));
                }
                logger.info("Params "+params);
		String response = webCommunicator.loadPostWebData(url, params);
		JSONObject jsonObj = XML.toJSONObject(response).getJSONObject("TwilioResponse");	
		
		if(jsonObj.has("RestException")){
			BaseResponse errResp = new BaseResponse();
			JSONObject excep = jsonObj.getJSONObject("RestException");
			errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			errResp.setErrorMessage(excep.getString("Message"));
			
			logger.info("Exiting. errrorResponse: "+errResp.toString());
			return errResp;
		}
		
		SendSMSResponse successResp = new SendSMSResponse();
		JSONObject exotelresp = jsonObj.getJSONObject("SMSMessage");
		successResp.setErrorCode(ErrorCode.SUCCESS);
		successResp.setStatus(exotelresp.getString("Status"));
		successResp.setSmsId(exotelresp.getString("Sid"));	
		successResp.setErrorMessage("");
		
		logger.info("Exiting. succesResponse: "+successResp.toString());
		return successResp;
		
		
	}	
	
	public BaseResponse sendBulkSMS(ArrayList<String> toNumbers, String message, boolean highPriority) throws JSONException {

		logger.info("Entering. to:"+toNumbers.toString()+" message:"+message);
		String url = EXOTEL_BASE_URL + SEND_SMS_URL;
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair("From", CALLER_ID));
		for (int i = 0; i < toNumbers.size(); i++) {

            params.add(new BasicNameValuePair("To[" + i + "]",toNumbers.get(i)));

       }
		params.add(new BasicNameValuePair("Body", message.trim()));
                if(highPriority) {
                    params.add(new BasicNameValuePair("Priority", "high"));
                }
		String response = webCommunicator.loadPostWebData(url, params);
		JSONObject jsonObj = XML.toJSONObject(response).getJSONObject("TwilioResponse");
		BaseResponse errResp = new BaseResponse();
		if(jsonObj.has("RestException")){
			
			JSONObject excep = jsonObj.getJSONObject("RestException");
			errResp.setErrorCode(ErrorCode.SERVICE_ERROR);
			errResp.setErrorMessage(excep.getString("Message"));
			logger.info("Exiting. errrorResponse: "+errResp.toString());
			return errResp;
		}
		
		SendBulkSMSResponse bulkSuccessResp = new SendBulkSMSResponse();
		logger.info(jsonObj.toString());
		ArrayList<SendSMSResponse> bulkResp = new ArrayList<SendSMSResponse>();
		SendSMSResponse successResp;			

		try{
			if(toNumbers.size() == 1){
				successResp = new SendSMSResponse();
				JSONObject exotelresp = jsonObj.getJSONObject("SMSMessage");
				successResp.setErrorCode(ErrorCode.SUCCESS);
				successResp.setStatus(exotelresp.getString("Status"));
				successResp.setSmsId(exotelresp.getString("Sid"));	
				successResp.setErrorMessage("");
				bulkResp.add(successResp);
			}
			else if (toNumbers.size() > 1){			
				JSONArray exotelresp = jsonObj.getJSONArray("SMSMessage");
				for(int i=0; i<exotelresp.length(); i++){
					successResp = new SendSMSResponse();
					successResp.setSmsId(exotelresp.getJSONObject(i).getString("Sid"));
					successResp.setStatus(exotelresp.getJSONObject(i).getString("Status"));
					successResp.setErrorMessage("");
					successResp.setErrorCode(ErrorCode.SUCCESS);
					bulkResp.add(successResp);
				}
			}
		} catch (Exception e){
			logger.info("Error parsing Exotel Response");
		}
		
		bulkSuccessResp.setErrorMessage("Success");
		bulkSuccessResp.setBulkStatus(bulkResp);
		bulkSuccessResp.setErrorCode(ErrorCode.SUCCESS);
		logger.info("Exiting. succesResponse: "+bulkSuccessResp.toString());
		return bulkSuccessResp;

	}

	@Override
	public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, CommunicationType type, Role role, String sqsMessageId) throws JSONException, UnsupportedEncodingException {
		return null;
	}

	@Override
	public BaseResponse sendBulkSMS(ArrayList<String> toNumber, String message, boolean highPriority, CommunicationType type, Role role) throws JSONException {
		return null;
	}
}
