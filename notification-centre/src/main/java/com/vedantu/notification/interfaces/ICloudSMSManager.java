package com.vedantu.notification.interfaces;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.CommunicationType;
import java.util.ArrayList;

import org.json.JSONException;

import com.vedantu.notification.response.BaseResponse;
import java.io.UnsupportedEncodingException;

/*
 * Interface to communicate with the message delivery system
 */
public interface ICloudSMSManager {

    public BaseResponse sendSMS(String toNumber, String message, boolean highPriority, CommunicationType type, Role role, String sqsMessageId) throws JSONException, UnsupportedEncodingException;

    public BaseResponse sendBulkSMS(ArrayList<String> toNumber, String message, boolean highPriority, CommunicationType type, Role role) throws JSONException;

}
