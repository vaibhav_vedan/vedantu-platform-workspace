package com.vedantu.notification.dao;

import com.amazonaws.services.dynamodbv2.xspec.B;
import com.mongodb.Block;
import com.mongodb.DuplicateKeyException;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.BlockedPhone;
import com.vedantu.notification.entity.Email;
import com.vedantu.notification.response.blockedPhone.BlockedPhoneRes;
import com.vedantu.notification.serializers.MongoClientFactory;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.common.recycler.Recycler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;

@Service
public class BlockedPhoneDao extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;
    Logger logger = logFactory.getLogger(BlockedPhoneDao.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public BlockedPhoneDao() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(BlockedPhone p) throws VException {
        try {
            if (p != null) {
                if(p.getCreatedBy()!=null)
                    saveEntity(p,p.getCreatedBy());
                saveEntity(p);
            }
        } catch (DuplicateKeyException ex) {
            throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY, "duplicate error");
        }
    }

    public void save(BlockedPhone p) throws Exception {
        if (p != null) {
            saveEntity(p);
        }
    }

    public BlockedPhone getByContactNumberAndPhoneCode(String contactNumber, String phoneCode) {
        Query query=new Query();
        query.addCriteria(Criteria.where(BlockedPhone.Constants.CONTACT_NUMBER).is(contactNumber));
        if(StringUtils.isNotEmpty(phoneCode)) {
            query.addCriteria(Criteria.where(BlockedPhone.Constants.PHONE_CODE).is(phoneCode));
        }
        query.addCriteria(Criteria.where(BlockedPhone.Constants.BLOCKED).is(true));
        List<BlockedPhone> blockedPhoneList = runQuery(query, BlockedPhone.class);
        if(CollectionUtils.isEmpty(blockedPhoneList)){
            return null;
        }
        else{
            return blockedPhoneList.get(0);
        }
    }


    public BlockedPhone getAnyByContactNumberAndPhoneCode(String contactNumber, String phoneCode) {
        Query query=new Query();
        query.addCriteria(Criteria.where(BlockedPhone.Constants.CONTACT_NUMBER).is(contactNumber));
        if(StringUtils.isNotEmpty(phoneCode)) {
            query.addCriteria(Criteria.where(BlockedPhone.Constants.PHONE_CODE).is(phoneCode));
        }
        List<BlockedPhone> blockedPhoneList = runQuery(query, BlockedPhone.class);
        if(CollectionUtils.isEmpty(blockedPhoneList)){
            return null;
        }
        else{
            return blockedPhoneList.get(0);
        }
    }


    public BlockedPhoneRes getBlockedPhones(Integer start, Integer size) {
        Query query = new Query();
        List<String> includeFields = new ArrayList<>(Arrays.asList(BlockedPhone.Constants.PHONE_CODE, BlockedPhone.Constants.CONTACT_NUMBER, BlockedPhone.Constants.BLOCKED, BlockedPhone.Constants.CREATION_TIME));
        query.addCriteria(Criteria.where(BlockedPhone.Constants.BLOCKED).is(true));
        setFetchParameters(query, start, size);
        query.with(Sort.by(Sort.Direction.DESC, BlockedPhone.Constants.CREATION_TIME));
        List<BlockedPhone> blockedPhoneList = runQuery(query, BlockedPhone.class, includeFields);

        BlockedPhoneRes blockedPhoneRes = new BlockedPhoneRes();
        blockedPhoneRes.setBlockedPhoneList(blockedPhoneList);

        return blockedPhoneRes;
    }
}
