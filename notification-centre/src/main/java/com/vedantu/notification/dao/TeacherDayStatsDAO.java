package com.vedantu.notification.dao;

import com.vedantu.notification.pojo.TeachersDayStats;
import com.vedantu.notification.serializers.MongoClientFactory;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.TextCriteria;
import org.springframework.data.mongodb.core.query.TextQuery;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;


@Service
public class TeacherDayStatsDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = LogFactory.getLogger(TeacherDayStatsDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public TeacherDayStatsDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(TeachersDayStats p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public void updateCountForTeacherComments(Long userId) {
        getMongoOperations().updateFirst(new Query(Criteria.where(TeachersDayStats.Constants._ID).is(userId)),
                new Update()
                        .inc(TeachersDayStats.Constants.WISHES_COUNT, 1),
                TeachersDayStats.class);
    }

    public List<TeachersDayStats> getTopTenForDashboardStats() {
        Query query = new Query();
        query.with(Sort.by(Sort.Direction.DESC, TeachersDayStats.Constants.WISHES_COUNT));
        query.limit(10);
        return runQuery(query, TeachersDayStats.class);
    }

}
