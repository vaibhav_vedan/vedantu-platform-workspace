/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.enums;

/**
 *
 * @author somil
 */
public enum EmailProperty {
    BODY, SUBJECT, FROM, TO, CC, BCC;
}
