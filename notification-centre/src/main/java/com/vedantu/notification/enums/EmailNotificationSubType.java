
package com.vedantu.notification.enums;

public enum EmailNotificationSubType {
	BOUNCE_PERMANENT, BOUNCE_TRANSIENT, BOUNCE_OTHERS, COMPLAINT_SINGLE, COMPLAINT_MULTIPLE
}

