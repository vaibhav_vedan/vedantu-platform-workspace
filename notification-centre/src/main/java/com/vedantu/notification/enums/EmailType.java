package com.vedantu.notification.enums;

/*
 * Enum for SMS Type Identifiers
 */
public enum EmailType {
	STUDENT_VERIFICATION, USER_MESSAGE;
}
