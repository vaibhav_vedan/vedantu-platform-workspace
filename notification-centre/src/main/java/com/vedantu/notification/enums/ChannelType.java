package com.vedantu.notification.enums;

public enum ChannelType {

	CHAT, PRESENCE, NOTIFICATION
}
