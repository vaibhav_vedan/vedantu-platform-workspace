package com.vedantu.notification.enums;

public enum StatusType {

	OFFLINE, READY_TO_TEACH, NONE
}
