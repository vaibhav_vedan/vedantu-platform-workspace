package com.vedantu.notification.enums;

public enum EmailNotificationType {
	BOUNCE, COMPLAINT, DELIVERY
}
