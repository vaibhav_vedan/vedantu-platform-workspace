package com.vedantu.notification.pojo;

import com.vedantu.notification.entity.MobileNotificationData;
import com.vedantu.notification.entity.Notification;
import com.vedantu.notification.enums.NotificationSubType;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class MobileNotification {

	private Long userId;

	private NotificationType type;

	private String header;

	private String body;

	private String footer;

	private String description;

	public MobileNotification(Long userId, NotificationType type, String header, String body, String footer,
			String description, Boolean isSeen, Long seenAt, String entityType, Long entityId, String info,
			Boolean hidden, MobileNotificationData mobileData) {
		super();
		this.userId = userId;
		this.type = type;
		this.header = header;
		this.body = body;
		this.footer = footer;
		this.description = description;
		this.isSeen = isSeen;
		this.seenAt = seenAt;
		this.entityType = entityType;
		this.entityId = entityId;
		this.info = info;
		this.hidden = hidden;
		this.mobileData = mobileData;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	private Boolean isSeen;

	private Long seenAt;

	private String entityType;

	private Long entityId;

	private String info;

	private Boolean hidden;

	private MobileNotificationData mobileData;

	private NotificationSubType subType;

	public MobileNotification() {
		super();
	}

	public MobileNotification(Long userId, NotificationType type, String header, String body, String footer,
			String description, Boolean isSeen, Long seenAt, String entityType, Long entityId, String info,
			Boolean hidden, MobileNotificationData mobileData, NotificationSubType subType) {
		super();
		this.userId = userId;
		this.type = type;
		this.header = header;
		this.body = body;
		this.footer = footer;
		this.description = description;
		this.isSeen = isSeen;
		this.seenAt = seenAt;
		this.entityType = entityType;
		this.entityId = entityId;
		this.info = info;
		this.hidden = hidden;
		this.mobileData = mobileData;
		this.subType = subType;
	}

	public MobileNotification(Notification notifRequest, MobileNotificationData mobileDataTemplate) {
		this.userId = notifRequest.getUserId();
		this.type = notifRequest.getType();
		this.header = notifRequest.getHeader();
		this.body = notifRequest.getBody();
		this.footer = notifRequest.getFooter();
		this.isSeen = notifRequest.getIsSeen();
		this.info = notifRequest.getInfo();
		this.hidden = notifRequest.getHidden();
		this.description = notifRequest.getDescription();
		if (notifRequest.getSeenAt() != null)
			this.seenAt = notifRequest.getSeenAt();
		if (notifRequest.getEntityType() != null)
			this.entityType = notifRequest.getEntityType();
		if (notifRequest.getEntityId() != null)
			this.entityId = notifRequest.getEntityId();
		this.mobileData = mobileDataTemplate;
		if (notifRequest.getSubType() != null)
			this.subType = notifRequest.getSubType();

	}

	public NotificationSubType getSubType() {
		return subType;
	}

	public void setSubType(NotificationSubType subType) {
		this.subType = subType;
	}

	public Long getUserId() {

		return userId;
	}

	public void setUserId(Long userId) {

		this.userId = userId;
	}

	public NotificationType getType() {

		return type;
	}

	public void setType(NotificationType type) {

		this.type = type;
	}

	public String getHeader() {

		return header;
	}

	public MobileNotificationData getMobileData() {
		return mobileData;
	}

	public void setMobileData(MobileNotificationData mobileData) {
		this.mobileData = mobileData;
	}

	public void setHeader(String header) {

		this.header = header;
	}

	public String getBody() {

		return body;
	}

	public void setBody(String body) {

		this.body = body;
	}

	public String getFooter() {

		return footer;
	}

	public void setFooter(String footer) {

		this.footer = footer;
	}

	public Boolean getIsSeen() {

		return isSeen;
	}

	public void setIsSeen(Boolean isSeen) {

		this.isSeen = isSeen;
	}

	public Long getSeenAt() {

		return seenAt;
	}

	public void setSeenAt(Long seenAt) {

		this.seenAt = seenAt;
	}

	public String getEntityType() {
		return entityType;
	}

	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	public Long getEntityId() {
		return entityId;
	}

	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public Boolean getHidden() {
		return hidden;
	}

	public void setHidden(Boolean hidden) {
		this.hidden = hidden;
	}

	@Override
	public String toString() {
		return "MobileNotification [userId=" + userId + ", type=" + type + ", header=" + header + ", body=" + body
				+ ", footer=" + footer + ", description=" + description + ", isSeen=" + isSeen + ", seenAt=" + seenAt
				+ ", entityType=" + entityType + ", entityId=" + entityId + ", info=" + info + ", hidden=" + hidden
				+ ", mobileData=" + mobileData + ", subType=" + subType + "]";
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String USER_ID = "userId";
		public static final String IS_SEEN = "isSeen";
		public static final String ENTITY_TYPE = "entityType";
		public static final String ENTITY_ID = "entityId";
	}

}
