/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojo;

/**
 *
 * @author parashar
 */
public class TelegramGetChatPojo {
    
    private Long id;
    private String title;
    private String invite_link;
    private String type;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the invite_link
     */
    public String getInvite_link() {
        return invite_link;
    }

    /**
     * @param invite_link the invite_link to set
     */
    public void setInvite_link(String invite_link) {
        this.invite_link = invite_link;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    
    
    
}
