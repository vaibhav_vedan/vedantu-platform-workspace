package com.vedantu.notification.pojo;

import org.json.JSONArray;

public class ButtonData {
	
	private String name;
	private String iconUrl;
	private String action;
	private String actionExtra;

	public ButtonData(){
		super();
	}
	public ButtonData(String name, String iconUrl, String action, String actionExtra){
		
		this.name = name;
		this.iconUrl = iconUrl;
		this.action = action;
		this.actionExtra = actionExtra;
		
	}
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getActionExtra() {
		return actionExtra;
	}

	public void setActionExtra(String actionExtra) {
		this.actionExtra = actionExtra;
	}

	@Override
	public String toString() {
		return "ButtonData  name:" + name + ", iconUrl:"
				+ iconUrl + ", action:" + action + ", actionExtra:" + actionExtra.toString();
	}
}
