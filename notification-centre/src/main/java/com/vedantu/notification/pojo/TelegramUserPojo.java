/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojo;

/**
 *
 * @author parashar
 */
public class TelegramUserPojo {
    
    private Long id;
    private boolean is_bot = false;
    private String first_name;
    private String last_name;
    private String username;
    private String language_code;
    private String photo_url;
    private Long auth_date;
    private String hash;

    /**
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the is_bot
     */
    public boolean isIs_bot() {
        return is_bot;
    }

    /**
     * @param is_bot the is_bot to set
     */
    public void setIs_bot(boolean is_bot) {
        this.is_bot = is_bot;
    }

    /**
     * @return the first_name
     */
    public String getFirst_name() {
        return first_name;
    }

    /**
     * @param first_name the first_name to set
     */
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    /**
     * @return the last_name
     */
    public String getLast_name() {
        return last_name;
    }

    /**
     * @param last_name the last_name to set
     */
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the language_code
     */
    public String getLanguage_code() {
        return language_code;
    }

    /**
     * @param language_code the language_code to set
     */
    public void setLanguage_code(String language_code) {
        this.language_code = language_code;
    }

    /**
     * @return the photo_url
     */
    public String getPhoto_url() {
        return photo_url;
    }

    /**
     * @param photo_url the photo_url to set
     */
    public void setPhoto_url(String photo_url) {
        this.photo_url = photo_url;
    }

    /**
     * @return the auth_date
     */
    public Long getAuth_date() {
        return auth_date;
    }

    /**
     * @param auth_date the auth_date to set
     */
    public void setAuth_date(Long auth_date) {
        this.auth_date = auth_date;
    }

    /**
     * @return the hash
     */
    public String getHash() {
        return hash;
    }

    /**
     * @param hash the hash to set
     */
    public void setHash(String hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return "TelegramUserPojo{" + "id=" + id + ", is_bot=" + is_bot + ", first_name=" + first_name + ", last_name=" + last_name + ", username=" + username + ", language_code=" + language_code + ", photo_url=" + photo_url + ", auth_date=" + auth_date + ", hash=" + hash + '}';
    }
    
    
}
