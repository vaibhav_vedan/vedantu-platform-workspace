package com.vedantu.notification.pojo;

import com.vedantu.notification.enums.OnlineStatus;
import com.vedantu.notification.enums.StatusType;

public class OnlineUserStatus {
	
	private Long userId;
	private StatusType statusType;
	private OnlineStatus onlineStatus;
	
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public StatusType getStatusType() {
		return statusType;
	}

	public void setStatusType(StatusType statusType) {
		this.statusType = statusType;
	}

	public OnlineStatus getOnlineStatus() {
		return onlineStatus;
	}

	public void setOnlineStatus(OnlineStatus onlineStatus) {
		this.onlineStatus = onlineStatus;
	}

	public OnlineUserStatus() {
		super();
		// TODO Auto-generated constructor stub
	}

	public OnlineUserStatus(Long userId, StatusType statusType, OnlineStatus onlineStatus) {
		super();
		this.userId = userId;
		this.statusType = statusType;
		this.onlineStatus = onlineStatus;
	}

	@Override
	public String toString() {
		return "OnlineUserStatus userId:" + userId + ", statusType:" + statusType +
				", onlineStatus:" + onlineStatus;
	}

}
