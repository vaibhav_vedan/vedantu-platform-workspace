package com.vedantu.notification.pojo;

import com.vedantu.util.dbentities.mongo.AbstractMongoLongIdEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "TeachersDayStats")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeachersDayStats extends AbstractMongoLongIdEntity {
    private String firstName;
    private String lastName;
    private String fullName;
    private String profilePicUrl;
    private Integer wishesCount;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String FIRST_NAME = "firstName";
        public static final String WISHES_COUNT = "wishesCount";
        public static final String FULL_NAME = "fullName";
        public static final String LAST_NAME = "lastName";
        public static final String PROFILE_PIC_URL = "profilePicUrl";

    }
}