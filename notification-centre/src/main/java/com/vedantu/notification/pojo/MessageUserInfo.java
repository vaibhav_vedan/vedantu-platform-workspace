/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojo;

import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.notification.pojos.MessageUserRes;

/**
 *
 * @author jeet
 */
public class MessageUserInfo {
	private UserBasicInfo user;
	private UserBasicInfo toUser;
	private String contextType;
	private String message;

	public MessageUserInfo() {
		super();
	}

	public MessageUserInfo(UserBasicInfo user, UserBasicInfo toUser, String contextType, String message) {
		super();
		this.user = user;
		this.toUser = toUser;
		this.contextType = contextType;
		this.message = message;
	}

	
        public MessageUserInfo(MessageUserRes messageUser, User user, User toUser) {
		this.user = new UserBasicInfo(user, false);
		this.toUser = new UserBasicInfo(toUser, false);
		this.contextType = messageUser.getContextType();
		this.message = messageUser.getMessageString();
	}

	public UserBasicInfo getUser() {
		return user;
	}

	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	public UserBasicInfo getToUser() {
		return toUser;
	}

	public void setToUser(UserBasicInfo toUser) {
		this.toUser = toUser;
	}

	public String getContextType() {
		return contextType;
	}

	public void setContextType(String contextType) {
		this.contextType = contextType;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
}

