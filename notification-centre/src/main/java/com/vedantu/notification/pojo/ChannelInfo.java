package com.vedantu.notification.pojo;

public class ChannelInfo {
	
	private Long userId;
	private String channelName;
	private Long lastSeen;
	private Long unreadMessageCount;
	
	public ChannelInfo() {
		super();
	}

	public ChannelInfo(Long userId, String channelName, Long lastSeen, Long unreadMessageCount) {
		super();
		this.userId = userId;
		this.channelName = channelName;
		this.lastSeen = lastSeen;
		this.unreadMessageCount = unreadMessageCount;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Long getLastSeen() {
		return lastSeen;
	}

	public void setLastSeen(Long lastSeen) {
		this.lastSeen = lastSeen;
	}

	public Long getUnreadMessageCount() {
		return unreadMessageCount;
	}

	public void setUnreadMessageCount(Long unreadMessageCount) {
		this.unreadMessageCount = unreadMessageCount;
	}
	
}
