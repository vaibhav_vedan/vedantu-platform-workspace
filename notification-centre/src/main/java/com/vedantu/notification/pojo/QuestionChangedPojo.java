/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojo;

/**
 *
 * @author parashar
 */
public class QuestionChangedPojo {
    private String questionNumber;
    private String correctionType;
    private String reevaluationType;

    /**
     * @return the questionNumber
     */
    public String getQuestionNumber() {
        return questionNumber;
    }

    /**
     * @param questionNumber the questionNumber to set
     */
    public void setQuestionNumber(String questionNumber) {
        this.questionNumber = questionNumber;
    }

    /**
     * @return the correctionType
     */
    public String getCorrectionType() {
        return correctionType;
    }

    /**
     * @param correctionType the correctionType to set
     */
    public void setCorrectionType(String correctionType) {
        this.correctionType = correctionType;
    }

    /**
     * @return the reevaluationType
     */
    public String getReevaluationType() {
        return reevaluationType;
    }

    /**
     * @param reevaluationType the reevaluationType to set
     */
    public void setReevaluationType(String reevaluationType) {
        this.reevaluationType = reevaluationType;
    }
}
