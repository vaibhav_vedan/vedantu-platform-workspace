package com.vedantu.notification.pojo;

import com.vedantu.User.Role;
import com.vedantu.notification.enums.ChannelType;

public class ChattedUser {
	
	private String userId;
	private String firstName;
	private String lastName;
	private String fullName;
	private Role role;
	private String profilePicUrl;
	private String channelName;
	private int newMessagesCount;
	private Long lastChattedTime;
	private ChannelType channelType;
	
	public ChattedUser() {
		super();
	}

		
	public ChattedUser(String userId, String firstName, String lastName, String fullName, Role role,
			String profilePicUrl, String channelName, int newMessagesCount, Long lastChattedTime,
			ChannelType channelType) {
		super();
		this.userId = userId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.fullName = fullName;
		this.role = role;
		this.profilePicUrl = profilePicUrl;
		this.channelName = channelName;
		this.newMessagesCount = newMessagesCount;
		this.lastChattedTime = lastChattedTime;
		this.channelType = channelType;
	}


	public ChannelType getChannelType() {
		return channelType;
	}


	public void setChannelType(ChannelType channelType) {
		this.channelType = channelType;
	}


	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getFirstName() {
		return firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return lastName;
	}


	public int getNewMessagesCount() {
		return newMessagesCount;
	}


	public void setNewMessagesCount(int newMessagesCount) {
		this.newMessagesCount = newMessagesCount;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public Role getRole() {
		return role;
	}


	public void setRole(Role role) {
		this.role = role;
	}


	public String getProfilePicUrl() {
		return profilePicUrl;
	}


	public void setProfilePicUrl(String profilePicUrl) {
		this.profilePicUrl = profilePicUrl;
	}


	public String getChannelName() {
		return channelName;
	}


	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	public Long getLastChattedTime() {
		return lastChattedTime;
	}


	public void setLastChattedTime(Long lastChattedTime) {
		this.lastChattedTime = lastChattedTime;
	}


	


	@Override
	public String toString() {
		return "ChattedUsers userId:" + userId + ", firstName:" + firstName + ", lastName:" + lastName + ", fullName:"
				+ fullName + ", role:" + role + ", profilePicUrl:" + profilePicUrl + ", channelName:" + channelName
				+ ", newMessagesCount:" + newMessagesCount + ", lastChattedTime:" + lastChattedTime;
	}
	
	

}
