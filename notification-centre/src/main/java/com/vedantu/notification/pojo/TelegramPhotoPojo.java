/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.pojo;

/**
 *
 * @author parashar
 */
public class TelegramPhotoPojo {
    private String file_id;
    private Integer file_size;
    private Integer width;
    private Integer height;

    /**
     * @return the file_id
     */
    public String getFile_id() {
        return file_id;
    }

    /**
     * @param file_id the file_id to set
     */
    public void setFile_id(String file_id) {
        this.file_id = file_id;
    }

    /**
     * @return the file_size
     */
    public Integer getFile_size() {
        return file_size;
    }

    /**
     * @param file_size the file_size to set
     */
    public void setFile_size(Integer file_size) {
        this.file_size = file_size;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }
    
}
