package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.ManualNotification;
import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.notification.request.ChangeManualNotificationRequest;
import com.vedantu.notification.request.NotificationFilterReq;
import com.vedantu.notification.requests.ManualNotificationRequest;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.CommunicationKind;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.List;

@Service
public class ManualNotificationDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	@Autowired
	private LogFactory logfactory;

	@SuppressWarnings("static-access")
	private Logger logger = logfactory.getLogger(ManualNotificationDAO.class);

	public ManualNotificationDAO() {
		super();
	}

	public void create(ManualNotification notification) {
		if (notification != null) {
			saveEntity(notification,notification.getCreatedBy());
		}
	}

	public ManualNotification getById(String id){
		ManualNotification notification=null;
		if(!StringUtils.isEmpty(id)){
			notification=getEntityById(id,ManualNotification.class);
		}
		return notification;
	}

	public int deleteById(String id){
		int result=0;
		result=deleteEntityById(id,ManualNotification.class);
		return result;
	}

	public void updateSuccessResult(ManualNotificationRequest request){
		Query query=new Query();
		query.addCriteria(Criteria.where(ManualNotification.Constants._ID).is(request.getId()));
		Update update=new Update();
		if(!CommunicationKind.NOTICE_BOARD.equals(request.getCommunicationKind())){
			update.set(ManualNotification.Constants.NOTIFICATION_STATUS, NotificationStatus.SENT);
		}else{
			update.set(ManualNotification.Constants.NOTIFICATION_STATUS, NotificationStatus.SCHEDULED);
		}
		// logger.info("\nUser id who's responsible for successfully updating the result is "+request.getCallingUserId());
		// update.set(ManualNotification.Constants.LAST_UPDATED_BY,request.getCallingUserId());
		updateMulti(query,update,ManualNotification.class);
	}

    public List<ManualNotification> getAllData(NotificationFilterReq filterReq) {
		Query query=new Query();
		if(filterReq.getCommunicationKind()!=null){
			query.addCriteria(Criteria.where(ManualNotification.Constants.COMMUNICATION_KIND).is(filterReq.getCommunicationKind()));
		}
		if(filterReq.getStatus()!=null) {
			query.addCriteria(Criteria.where(ManualNotification.Constants.NOTIFICATION_STATUS).is(filterReq.getStatus()));
		}
		query.skip(filterReq.getSkip());
		query.limit(filterReq.getLimit());
		query.with(Sort.by(Sort.Direction.DESC, ManualNotification.Constants.LAST_UPDATED));

		return runQuery(query,ManualNotification.class);
    }

    public List<ManualNotification> getScheduledManualNotification() {
		long currentTime=System.currentTimeMillis();
		Query query=new Query();
        query.addCriteria(new Criteria().andOperator(
                Criteria.where(ManualNotification.Constants.SCHEDULED_TIME).ne(0),
                Criteria.where(ManualNotification.Constants.SCHEDULED_TIME).lte(currentTime),
                Criteria.where(ManualNotification.Constants.SCHEDULED_TIME).gt(currentTime-(35 * DateTimeUtils.MILLIS_PER_MINUTE ))
        ));
		query.addCriteria(Criteria.where(ManualNotification.Constants.NOTIFICATION_STATUS).is(NotificationStatus.SCHEDULED));
		query.addCriteria(Criteria.where(ManualNotification.Constants.COMMUNICATION_KIND).in(Arrays.asList(CommunicationKind.EMAIL,CommunicationKind.SMS)));
		return runQuery(query,ManualNotification.class);
    }

	public List<ManualNotification> discardScheduled(ChangeManualNotificationRequest discardList) {
		Query query=new Query();
		query.addCriteria(Criteria.where(ManualNotification.Constants._ID).in(discardList.getChangeList()));
		query.addCriteria(Criteria.where(ManualNotification.Constants.SCHEDULED_TIME).ne(0));
		query.addCriteria(Criteria.where(ManualNotification.Constants.NOTIFICATION_STATUS).is(NotificationStatus.SCHEDULED));

		Update update=new Update();
		update.set(ManualNotification.Constants.NOTIFICATION_STATUS,NotificationStatus.DISCARDED);
		update.set(ManualNotification.Constants.LAST_UPDATED_BY,discardList.getCallingUserId());

		updateMulti(query,update,ManualNotification.class);
		return runQuery(query,ManualNotification.class);


	}

	public List<ManualNotification> reschedule(ChangeManualNotificationRequest rescheduleList) {
		Query query=new Query();
		query.addCriteria(Criteria.where(ManualNotification.Constants._ID).in(rescheduleList.getChangeList()));
		query.addCriteria(Criteria.where(ManualNotification.Constants.SCHEDULED_TIME).ne(0));
		query.addCriteria(Criteria.where(ManualNotification.Constants.NOTIFICATION_STATUS).is(NotificationStatus.SCHEDULED));

		Update update=new Update();
		update.set(ManualNotification.Constants.SCHEDULED_TIME,rescheduleList.getRescheduledTime());
		update.set(ManualNotification.Constants.LAST_UPDATED_BY,rescheduleList.getCallingUserId());
		if(rescheduleList.getReschdeuledExpiryDate()!=null){
			update.set(ManualNotification.Constants.EXPIRY_TIME,rescheduleList.getReschdeuledExpiryDate());
		}

		updateMulti(query,update,ManualNotification.class);
		return runQuery(query,ManualNotification.class);
	}

	public List<ManualNotification> getNotificationByIds(String id) {
		Query query=new Query();
		query.addCriteria(Criteria.where(ManualNotification.Constants._ID).is(id));

		return runQuery(query,ManualNotification.class);
	}

	public void changeManualNotificationStatus(List<String> ids, NotificationStatus status){
		Query query=new Query();
		query.addCriteria(Criteria.where(ManualNotification.Constants._ID).in(ids));

		Update update=new Update();
		update.set(ManualNotification.Constants.NOTIFICATION_STATUS,status);

		updateMulti(query,update,ManualNotification.class);
	}
}
