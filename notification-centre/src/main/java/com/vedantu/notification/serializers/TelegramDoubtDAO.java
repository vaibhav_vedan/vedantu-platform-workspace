/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.TelegramDoubt;
import com.vedantu.notification.request.GetTelegramDoubtRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class TelegramDoubtDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TelegramDoubtDAO.class);
    
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    } 
    
    public <E extends AbstractMongoStringIdEntity> void save(E p, String callingUserId) {
           logger.info("ENTRY: " + p);
           if (p != null) {
                   saveEntity(p, callingUserId);
           }
           logger.info("EXIT");
    }    
    
    public TelegramDoubt getDoubtForMessageAndChat(Long messageId, Long chatId){
        Query query = new Query();
        query.addCriteria(Criteria.where("telegramMessageId").is(messageId));
        query.addCriteria(Criteria.where("telegramChatId").is(chatId));  
        return findOne(query, TelegramDoubt.class);
    }
    
    public List<TelegramDoubt> getTelegramDoubts(GetTelegramDoubtRequest req){
        Query query = new Query();
        if(req.getClosed() != null){
            query.addCriteria(Criteria.where("closed").is(req.getClosed()));
        }
        
        if(StringUtils.isNotEmpty(req.getBatchId())){
            query.addCriteria(Criteria.where("batchId").is(req.getBatchId()));
        }
        
        if(req.getUserId() != null){
            query.addCriteria(Criteria.where("userId").is(req.getUserId()));
        }
        
        query.with(Sort.by(Sort.Direction.DESC, "creationTime"));

        setFetchParameters(query, req.getStart(), req.getSize());
        
        return runQuery(query, TelegramDoubt.class);
        
        
    }
    
}
