/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.MessageTrigger;
import com.vedantu.notification.enums.TriggerStatus;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class MessageTriggerDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;    
        

    public MessageTriggerDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
        
    
    public void addMessageTrigger(MessageTrigger messageTrigger) {
        try {
            if (messageTrigger != null) {
                messageTrigger.setStatus(TriggerStatus.ACTIVE);
                saveEntity(messageTrigger);
            }
        } catch (Exception ex) {
		}
    }

    public List<MessageTrigger> getMessageTrigger(Long studentId, Long teacherId) {
        return getMessageTrigger(studentId, teacherId, null, null, null);
    }

    public List<MessageTrigger> getMessageTrigger(Long studentId,
            Long teacherId, Long triggerStartTime, Long triggerEndTime, Boolean alertSent) {
        List<MessageTrigger> messageTriggers = null;
        try {
            Query query = new Query();
           
            
            if (alertSent != null) {
                 query.addCriteria(Criteria.where(MessageTrigger.Constants.ALERT_SENT).is(alertSent));
            }

            if (studentId != null && studentId > 0) {
                query.addCriteria(Criteria.where(MessageTrigger.Constants.STUDENT_ID).is(studentId));
            }

            if (teacherId != null && teacherId > 0) {
                query.addCriteria(Criteria.where(MessageTrigger.Constants.TEACHER_ID).is(teacherId));
            }

            if (triggerStartTime != null && triggerStartTime > 0) {
                query.addCriteria(Criteria.where(MessageTrigger.Constants.TRIGGER_TIME).gte(triggerStartTime));
            }
            if (triggerEndTime != null && triggerEndTime > 0) {
                query.addCriteria(Criteria.where(MessageTrigger.Constants.TRIGGER_TIME).lt(triggerEndTime));
            }

            messageTriggers = runQuery(query, MessageTrigger.class);
        } catch(Exception e){
        }

        return messageTriggers;
    }

    public void markMessageTriggerInactive(MessageTrigger messageTrigger) {
        messageTrigger.setStatus(TriggerStatus.INACTIVE);
        addMessageTrigger(messageTrigger);
    }

    public void markMessageTriggerAlertSent(MessageTrigger messageTrigger) {
        messageTrigger.setAlertSent(true);
        addMessageTrigger(messageTrigger);
    }

}
