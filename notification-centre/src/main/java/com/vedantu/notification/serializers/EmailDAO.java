package com.vedantu.notification.serializers;


import java.util.List;

import com.vedantu.notification.thirdparty.AmazonSESSDKManager;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.notification.entity.Email;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class EmailDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(EmailDAO.class);

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public EmailDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }


	public void create(Email p) {
		try {
			if (p != null) {
				if(p.getCreatedBy()!=null)
					saveEntity(p,p.getCreatedBy());
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}

	public Email getById(String id) {
		Email Email = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				Email = (Email) getEntityById(id, Email.class);
			}
		} catch (Exception ex) {
			Email = null;
		}
		return Email;
	}

	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, Email.class);
		} catch (Exception ex) {
		}

		return result;
	}

    public List<Email> getDeliveryReportForManualNotification(List<String> manualNotificationIds) {
        	Query query=new Query();
        	query.addCriteria(Criteria.where(Email.Constants.MANUAL_NOTIFICATION_ID).in(manualNotificationIds));
        	return runQuery(query,Email.class);
    }

    public List<Email> getDeliveryReportForManualNotificationById(String manualNotificationId) {
        Query query=new Query();
        query.addCriteria(Criteria.where(Email.Constants.MANUAL_NOTIFICATION_ID).is(manualNotificationId));
        return runQuery(query,Email.class);
    }
}
