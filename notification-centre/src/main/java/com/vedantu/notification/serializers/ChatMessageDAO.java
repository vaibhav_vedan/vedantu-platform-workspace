/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import com.vedantu.notification.entity.ChatMessage;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class ChatMessageDAO extends AbstractMongoDAO {
    
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(ChatMessageDAO.class);
    
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public List<ChatMessage> getChatMessges(String channel,Integer size, Long before){
        logger.info("getChatMessges "+",channel:"+channel+",size:"+size+",before:"+before);
        List<ChatMessage> chatMessages = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(ChatMessage.Constants.CHANNEL).is(channel));
        if(before !=null){
            query.addCriteria(Criteria.where(ChatMessage.Constants.SENDER_TIME).lt(before));
        }
        query.with(Sort.by(Sort.Direction.DESC, ChatMessage.Constants.SENDER_TIME));
        if (size != null) {
            query.limit(size);
        }
        logger.info("query " + query);
        chatMessages = runQuery(query, ChatMessage.class);
        logger.info("getChatMessges" + chatMessages);
        return chatMessages;
    }

    public void save(ChatMessage entity) {
        saveEntity(entity);
    }

}
