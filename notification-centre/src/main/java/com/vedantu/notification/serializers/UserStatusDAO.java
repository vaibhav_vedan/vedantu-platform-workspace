package com.vedantu.notification.serializers;


import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.notification.entity.UserStatus;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class UserStatusDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public UserStatusDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
    
	public void create(UserStatus p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}

	public UserStatus getById(String id) {
		UserStatus UserStatus = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				UserStatus = (UserStatus) getEntityById(id, UserStatus.class);
			}
		} catch (Exception ex) {
			UserStatus = null;
		}
		return UserStatus;
	}


	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, UserStatus.class);
		} catch (Exception ex) {
		}

		return result;
	}
}
