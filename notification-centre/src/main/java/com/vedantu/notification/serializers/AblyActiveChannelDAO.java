/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.AblyActiveChannel;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class AblyActiveChannelDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(AblyActiveChannel p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, AblyActiveChannel.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public void upsert(AblyActiveChannel p) {
        if (p != null) {
            Query query = new Query();
            query.addCriteria(Criteria.where(AblyActiveChannel.Constants.CHANNEL).is(
                    p.getChannel()));
            Update update = new Update();
            update.set(AblyActiveChannel.Constants.DELETED, p.isDeleted());
            update.set(AblyActiveChannel.Constants.PROCESSED_TIME, p.getProcessedTime());
            upsertEntity(p, query, update, null);
        }

    }
}
