package com.vedantu.notification.serializers;

import com.twilio.twiml.voice.Sms;
import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.DuplicateEntryException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.SmsTemplate;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SmsTemplateDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(SmsTemplateDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(SmsTemplate smsTemplate, Long callingUserId) throws VException {
        if(smsTemplate != null) {
            try{
                saveEntity(smsTemplate, callingUserId.toString());
            }catch (Exception e){
                throw new DuplicateEntryException(ErrorCode.DUPLICATE_ENTRY, "Duplicate combination for smsType and role");
            }
        }
    }

    public List<SmsTemplate> getSmsTemplatesWithFilter(Integer start, Integer limit, EntityState state){
        Query query = new Query();
        if (state == null) {
            query.addCriteria(Criteria.where(SmsTemplate.Constants.ENTITY_STATE).in(EntityState.ACTIVE, EntityState.INACTIVE));
        } else {
            query.addCriteria(Criteria.where(SmsTemplate.Constants.ENTITY_STATE).is(state));
        }
        setFetchParameters(query, start, limit);
        query.with(Sort.by(Sort.Direction.DESC, SmsTemplate.Constants.LAST_UPDATED));
        logger.info("query: "+query);
        return runQuery(query, SmsTemplate.class);
    }

    public SmsTemplate getSmsTemplateBySmsTypeAndRole(CommunicationType smsType, Role role){
        if(smsType == null || role == null)
            return null;
        Query query = new Query();
        query.addCriteria(Criteria.where(SmsTemplate.Constants.SMS_TYPE).is(smsType));
        query.addCriteria(Criteria.where(SmsTemplate.Constants.ROLE).is(role));
        return findOne(query, SmsTemplate.class);
    }

    public SmsTemplate getSmsTemplateBySmsType(CommunicationType smsType){
        Query query = new Query();
        query.addCriteria(Criteria.where(SmsTemplate.Constants.SMS_TYPE).is(smsType));
        query.addCriteria(Criteria.where(SmsTemplate.Constants.ROLE).exists(false));
        return findOne(query, SmsTemplate.class);
    }
}
