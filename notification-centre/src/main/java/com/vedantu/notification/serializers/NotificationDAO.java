/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.Notification;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 *
 * @author somil
 */
@Service
public class NotificationDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public NotificationDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(Notification p, Long callingUserId) {
            String callingUserIdString = null;
            if(callingUserId!=null) {
                callingUserIdString = callingUserId.toString();
            }
            if (p != null) {
                saveEntity(p, callingUserIdString);
            }
    }

    public Notification getById(String id) {
        Notification Notification = null;
            if (!StringUtils.isEmpty(id)) {
                Notification = getEntityById(id, Notification.class);
            }
        return Notification;
    }


    public List<Notification> getNotifications(Long userId, Integer start, Integer limit) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Notification.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Notification.Constants.IS_SEEN).is(Boolean.FALSE));
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, Notification.Constants.LAST_UPDATED));
        query.with(Sort.by(orderList));

        setFetchParameters(query, start, limit);
        
        List<Notification> notifications = runQuery(query, Notification.class);
        //logger.info("found notifications: " + (CollectionUtils.isNotEmpty(notifications) ? notifications.size() : 0));
        return notifications;

    }

    public Notification getNotificationsById(Long userId, Long id) {

        Query query = new Query();
        query.addCriteria(Criteria.where(Notification.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Notification.Constants.ENTITY_ID).is(id));
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, Notification.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));
        List<Notification> notifications = runQuery(query, Notification.class);
        Notification notification = CollectionUtils.isNotEmpty(notifications) ? notifications.get(0) : null;
        return notification;

    }

    public Notification getNotificationByIdAndUserId(Long userId, String id) {
        Notification notification = null;
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        query.addCriteria(Criteria.where(Notification.Constants.USER_ID).is(userId));
        
        //List<Sort.Order> orderList = new ArrayList<Sort.Order>();
        //orderList.add(new Sort.Order(Sort.Direction.DESC, Notification.Constants.CREATION_TIME));
        //query.with(Sort.by(orderList));
        List<Notification> notifications = runQuery(query, Notification.class);
        notification = CollectionUtils.isNotEmpty(notifications) ? notifications.get(0) : null;
        return notification;

    }

    public Notification getNotificationsByType(Long userId, NotificationType notificationType) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Notification.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Notification.Constants.TYPE).is(notificationType));
        List<Sort.Order> orderList = new ArrayList<>();
        orderList.add(new Sort.Order(Sort.Direction.DESC, Notification.Constants.CREATION_TIME));
        query.with(Sort.by(orderList));
        List<Notification> notifications = runQuery(query, Notification.class);
        Notification notification = CollectionUtils.isNotEmpty(notifications) ? notifications.get(0) : null;
        return notification;
    }

}
