package com.vedantu.notification.serializers;


import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.notification.entity.UserDevice;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class UserDeviceDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public UserDeviceDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
    
	public void create(UserDevice p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}

	public UserDevice getById(String id) {
		UserDevice UserDevice = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				UserDevice = (UserDevice) getEntityById(id, UserDevice.class);
			}
		} catch (Exception ex) {
			UserDevice = null;
		}
		return UserDevice;
	}


	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, UserDevice.class);
		} catch (Exception ex) {
		}

		return result;
	}

}
