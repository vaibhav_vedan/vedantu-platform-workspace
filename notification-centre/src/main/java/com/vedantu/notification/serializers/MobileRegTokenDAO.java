/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.MobileRegToken;
import com.vedantu.notification.enums.VedantuApp;
import com.vedantu.notification.request.RegisterMobileRegTokenReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class MobileRegTokenDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(MobileRegTokenDAO.class);

    public MobileRegTokenDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(MobileRegToken p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error("Error in saving MobileRegToken for id "
                    + p + ", Error " + ex.getMessage());
        }
    }

    public List<MobileRegToken> getTokens(String deviceId, VedantuApp app,
            String mobileRegToken) {
        Query query = new Query();
        if (StringUtils.isNotEmpty(deviceId)) {
            query.addCriteria(Criteria.where(MobileRegToken.Constants.DEVICEID).is(deviceId));
        }
        if (StringUtils.isNotEmpty(mobileRegToken)) {
            query.addCriteria(Criteria.where(MobileRegToken.Constants.REG_TOKEN).is(mobileRegToken));
        }
        if (app != null) {
            query.addCriteria(Criteria.where(MobileRegToken.Constants.APP).is(app));
        }
        return runQuery(query, MobileRegToken.class);
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, MobileRegToken.class);
        } catch (Exception ex) {
            logger.error("Error in deleting MobileRegToken for id "
                    + id + ", Error " + ex.getMessage());
        }

        return result;
    }

    public int deleteTokens(String deviceId, VedantuApp app,
            String mobileRegToken) {
        Query query = new Query();
        if (StringUtils.isNotEmpty(deviceId)) {
            query.addCriteria(Criteria.where(MobileRegToken.Constants.DEVICEID).is(deviceId));
        }
        if (StringUtils.isNotEmpty(mobileRegToken)) {
            query.addCriteria(Criteria.where(MobileRegToken.Constants.REG_TOKEN).is(mobileRegToken));
        }
        if (app != null) {
            query.addCriteria(Criteria.where(MobileRegToken.Constants.APP).is(app));
        }
        return deleteEntities(query, MobileRegToken.class);
    }

}
