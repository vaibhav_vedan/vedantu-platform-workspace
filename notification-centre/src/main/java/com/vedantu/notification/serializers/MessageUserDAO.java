/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.MessageUser;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author jeet
 */
@Service
public class MessageUserDAO extends AbstractMongoDAO {

    @Autowired
    CounterService counterService;

    @Autowired
    private LogFactory logFactory;

    Logger logger = logFactory.getLogger(MessageUserDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public final static String COLLECTION_NAME = "MessageUser";

    public MessageUserDAO() {
        super();
    }

    @Override
    public MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void upsert(MessageUser p) {
        try {
            if (p != null) {
                if (p.getId() == null || p.getId() == 0L) {
                    p.setId(counterService.getNextSequence(MessageUser.class.getSimpleName()));
                }
                super.saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error(ex.getMessage());
        }
    }

    public MessageUser getById(Long id) {
        MessageUser GetRequestsResponse = null;
        try {
            if (id != null) {
                GetRequestsResponse = (MessageUser) super.getEntityById(id,
                        MessageUser.class);
            }
        } catch (Exception ex) {
            GetRequestsResponse = null;
        }
        return GetRequestsResponse;
    }

    public List<MessageUser> getMessages(Long afterTimeMillis, Long userId,
            Long toUserId, String contextType, Integer start, Integer limit) {
        List<MessageUser> results = new ArrayList<>();

        try {
            Query query = new Query();

            if (userId != null && userId > 0) {
                query.addCriteria(Criteria.where(MessageUser.Constants.USER_ID).is(userId));
            }

            if (toUserId != null && toUserId > 0) {
                query.addCriteria(Criteria.where(MessageUser.Constants.TO_USER_ID).is(toUserId));
            }

            if (StringUtils.isNotEmpty(contextType)) {
                query.addCriteria(Criteria.where(MessageUser.Constants.CONTEXT_TYPE).is(contextType));
            }
            if (afterTimeMillis != null && afterTimeMillis > 0) {
                query.addCriteria(Criteria.where(AbstractMongoStringIdEntity.Constants.CREATION_TIME).gte(afterTimeMillis));
            }

            if (limit != null) {
                query.limit(limit);
            } else {
                query.limit(1000);
            }
            if (start != null) {
                query.skip(start);
            } else {
                query.skip(0);
            }
            query.with(Sort.by(Sort.Direction.DESC,
                    AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
            logger.info("query for get messages {}", query);
            results = runQuery(query, MessageUser.class);
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
        }

        return results;
    }

    public MessageUser getLastMessageSMSSent(Long userId, Long toUserId) throws BadRequestException {
        MessageUser res = null;
        if (userId == null || toUserId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId or toUserdId is not present");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(MessageUser.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(MessageUser.Constants.TO_USER_ID).is(toUserId));
        query.addCriteria(Criteria.where(MessageUser.Constants.SMS_SENT).is(true));
        query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        query.limit(1);
        query.skip(0);
        List<MessageUser> results = runQuery(query, MessageUser.class);
        if (ArrayUtils.isNotEmpty(results)) {
            res = results.get(0);
        }
        return res;
    }

}
