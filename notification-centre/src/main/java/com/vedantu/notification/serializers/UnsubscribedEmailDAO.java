package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.UnsubscribedEmail;
import com.vedantu.notification.requests.GetUnsubscribedListReq;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UnsubscribedEmailDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;


        public UnsubscribedEmailDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
    
	public void create(UnsubscribedEmail p) {
			if (p != null) {
				if(StringUtils.isNotEmpty(p.getEmail())) {
					p.setEmail(p.getEmail().toLowerCase().trim());
				}
				saveEntity(p);
			}
	}


	public UnsubscribedEmail getById(String id) {
		UnsubscribedEmail UnsubscribedEmail = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				UnsubscribedEmail = getEntityById(id, UnsubscribedEmail.class);
			}
		} catch (Exception ex) {
			UnsubscribedEmail = null;
		}
		return UnsubscribedEmail;
	}


	public List<UnsubscribedEmail> getEntriesToNotSend(List<String> emailAddresses) {
		Query query = new Query();
		emailAddresses = convertToLowercaseEmails(emailAddresses);
		query.addCriteria(Criteria.where(UnsubscribedEmail.Constants.EMAIL).in(emailAddresses));
		query.addCriteria(Criteria.where(UnsubscribedEmail.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		return runQuery(query, UnsubscribedEmail.class);

	}

	public List<UnsubscribedEmail> getUnsubscribedList(GetUnsubscribedListReq req) {
		Query query = new Query();
		if (StringUtils.isNotEmpty(req.getEmail())) {
			query.addCriteria(Criteria.where(UnsubscribedEmail.Constants.EMAIL).is(req.getEmail()));
		}
		if (req.getState() != null) {
			query.addCriteria(Criteria.where(UnsubscribedEmail.Constants.ENTITY_STATE).is(req.getState()));
		}
		setFetchParameters(query, req);
		return runQuery(query, UnsubscribedEmail.class);
	}        

	public Long getUnsubscribedListCount(GetUnsubscribedListReq req) {
		Query query = new Query();
		if(req.getEmail()!=null){
                    query.addCriteria(Criteria.where(UnsubscribedEmail.Constants.EMAIL).is(req.getEmail()));
                }
                if(req.getState()!=null){
                    query.addCriteria(Criteria.where(UnsubscribedEmail.Constants.ENTITY_STATE).is(req.getState()));
                }
		return queryCount(query, UnsubscribedEmail.class);
	}        
        
        

	public UnsubscribedEmail getByEmail(String email) {
		Query query = new Query();
		email = email.toLowerCase().trim();
		UnsubscribedEmail response = null;
		query.addCriteria(Criteria.where(UnsubscribedEmail.Constants.EMAIL).is(email));
		List<UnsubscribedEmail> result = runQuery(query, UnsubscribedEmail.class);
		if(CollectionUtils.isNotEmpty(result)) {
			response = result.get(0);
		}
		return response;

	}

	private List<String> convertToLowercaseEmails(List<String> emailAddresses) {
    	List<String> convertedList = new ArrayList<>();
    	if(com.vedantu.util.CollectionUtils.isNotEmpty(emailAddresses)) {
    		for(String email: emailAddresses) {
				if (StringUtils.isNotEmpty(email)) {
					email = email.toLowerCase().trim();
					convertedList.add(email);
				}
			}
		}
		return convertedList;
	}

}
