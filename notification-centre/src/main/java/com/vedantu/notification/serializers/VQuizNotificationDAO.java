package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.VQuizNotification;
import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.commons.httpclient.util.DateUtil;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;

/**
 * @author MNPK
 */

@Service
public class VQuizNotificationDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(VQuizNotificationDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public VQuizNotificationDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(VQuizNotification p) {
        logger.info("VQuizNotification : " + p);
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
            logger.error("Error Creating the VQuizNotification : " + ex.getMessage());
        }
    }

    public VQuizNotification getVQuizNotificationByUserIdAndQuizId(Long userId, String quizId) {
        logger.info("userId : " + userId + " quizId : " + quizId);
        VQuizNotification vQuizNotification = null;
        if (userId != null && StringUtils.isNotEmpty(quizId)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(VQuizNotification.Constants.USER_ID).is(userId));
            query.addCriteria(Criteria.where(VQuizNotification.Constants.QUIZ_ID).is(quizId));
            query.fields().include(VQuizNotification.Constants.ID);
            logger.info("query : " + query);
            vQuizNotification = findOne(query, VQuizNotification.class);
        }
        logger.info("vQuizNotification : " + vQuizNotification);
        return vQuizNotification;
    }

    public VQuizNotification getVQuizNotificationByScheduleTimeAndStatus(Long scheduleTime, NotificationStatus status) {
        logger.info("scheduleTime : " + scheduleTime + " status : " + status);
        VQuizNotification vQuizNotification = null;
        if (scheduleTime != null && status != null) {
            Query query = new Query();
            query.addCriteria(Criteria.where(VQuizNotification.Constants.SCHEDULE_TIME).lte(scheduleTime));
            query.addCriteria(Criteria.where(VQuizNotification.Constants.STATUS).is(status));
            logger.info("query : " + query);
            List<VQuizNotification> vQuizNotifications = runQuery(query, VQuizNotification.class);
            logger.info("vQuizNotifications : " + vQuizNotifications);
            if (ArrayUtils.isNotEmpty(vQuizNotifications)) {
                vQuizNotification = vQuizNotifications.get(0);
            }
        }
        logger.info("vQuizNotification : " + vQuizNotification);
        return vQuizNotification;
    }

    public List<VQuizNotification> getVQuizNotifications(Long currentTime, NotificationStatus status, String fetchAfterId, List<String> includeFields, int limit) {
        List<VQuizNotification> vQuizNotifications = new ArrayList<>();
        if (currentTime != null && status != null) {
            Query query = new Query();

            //query
            Long quizStartMaxTime = currentTime + DateTimeUtils.MILLIS_PER_MINUTE * 15;
            Criteria andCriteria = new Criteria().andOperator(Criteria.where(VQuizNotification.Constants.SCHEDULE_TIME).gt(currentTime), Criteria.where(VQuizNotification.Constants.SCHEDULE_TIME).lte(quizStartMaxTime));
            query.addCriteria(andCriteria);
            query.addCriteria(Criteria.where(VQuizNotification.Constants.STATUS).is(status));

            // Sort
            query.with(Sort.by(Sort.Direction.ASC, VQuizNotification.Constants._ID));

            // Range
            if (StringUtils.isNotEmpty(fetchAfterId)) {
                query.addCriteria(Criteria.where(_ID).gt(new ObjectId(fetchAfterId)));
            } else {
                query.addCriteria(Criteria.where(_ID).gt(AbstractMongoDAO.MIN_KEY_OBJECT_ID));
            }

            query.limit(limit);
            includeFieldsForProjection(query, includeFields);

            logger.info(" getVQuizNotifications query : " + query);
            vQuizNotifications = runQuery(query, VQuizNotification.class);
            logger.info("getVQuizNotifications - vQuizNotifications : " + vQuizNotifications);
        }
        return vQuizNotifications;
    }
}
