package com.vedantu.notification.serializers;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.vedantu.notification.entity.EmailNotification;
import com.vedantu.notification.enums.EmailNotificationSubType;
import com.vedantu.notification.enums.EmailNotificationType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;

@Service
public class EmailNotificationDAO extends AbstractMongoDAO {
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EmailNotificationDAO.class);    
    
    @Autowired
    private MongoClientFactory mongoClientFactory;

    public EmailNotificationDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(EmailNotification p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public void createAll(List<EmailNotification> p) {
        try {
            if (p != null) {
                insertAllEntities(p, EmailNotification.class.getSimpleName());
            }
        } catch (Exception ex) {
        }
    }

    public EmailNotification getById(String id) {
        EmailNotification EmailNotification = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                EmailNotification = (EmailNotification) getEntityById(id, EmailNotification.class);
            }
        } catch (Exception ex) {
            EmailNotification = null;
        }
        return EmailNotification;
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, EmailNotification.class);
        } catch (Exception ex) {
        }

        return result;
    }

    public List<EmailNotification> getEntriesToNotSend(List<String> emailAddresses) {
        Query query = new Query();
        Criteria criteriaType = new Criteria();
        Criteria bounceCriteria = Criteria.where("notificationType").is(EmailNotificationType.BOUNCE).and("subType")
                .is(EmailNotificationSubType.BOUNCE_PERMANENT).and("emailAddress").in(includeLowercaseEmails(emailAddresses));
        Criteria complaintCriteria = Criteria.where("notificationType").is(EmailNotificationType.COMPLAINT).and("subType")
                .is(EmailNotificationSubType.COMPLAINT_SINGLE).and("emailAddress").in(includeLowercaseEmails(emailAddresses));
        Criteria complaintMultipleCriteria = Criteria.where("notificationType").is(EmailNotificationType.COMPLAINT).and("subType")
                .is(EmailNotificationSubType.COMPLAINT_MULTIPLE).and("emailAddress").in(includeLowercaseEmails(emailAddresses));
        criteriaType.orOperator(bounceCriteria, complaintCriteria, complaintMultipleCriteria);
        query.addCriteria(criteriaType);
        query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.ENTITY_STATE).ne(EntityState.DELETED));
//        query.addCriteria(Criteria.where("emailAddress").in(includeLowercaseEmails(emailAddresses)));
        logger.info("query "+query);
        return runQuery(query, EmailNotification.class);

    }

    private List<String> includeLowercaseEmails(List<String> emailAddresses) {
        List<String> allEmailIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(emailAddresses)) {
            emailAddresses.forEach(e -> {
                if (!StringUtils.isEmpty(e)) {
                    allEmailIds.add(e);
                    if (!e.toLowerCase().equals(e)) {
                        allEmailIds.add(e.toLowerCase());
                    }
                }
            });
        }

        return allEmailIds;
    }

    public void removeBounceEntries(String email) {
        Query query = new Query();
        query.addCriteria(Criteria.where("emailAddress").is(email.trim().toLowerCase()));

        Update update=new Update();
        update.set(EmailNotification.Constants.ENTITY_STATE, EntityState.DELETED);
        logger.info("Deleting multiple Bounce entities by id having query - {}, update - {}",query,update);
        updateMulti(query,update, EmailNotification.class);
    }
}
