/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.Channel;
import com.vedantu.notification.entity.GupshupDeliveryReport;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class GupshupDeliveryReportDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public GupshupDeliveryReportDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(GupshupDeliveryReport p) {
        try {
            if (p != null) {
                saveEntity(p);
            }
        } catch (Exception ex) {
        }
    }

    public GupshupDeliveryReport getReportByExternalId(String externalId) {
        Query query = new Query(Criteria.where("externalId").is(externalId));
        List<GupshupDeliveryReport> reports = runQuery(query, GupshupDeliveryReport.class);
        if (!reports.isEmpty()) {
            return reports.get(0);
        } else {
            return null;
        }
    }

    public List<GupshupDeliveryReport> getDeliveryReportForManualNotification(List<String> manualNotificationIds) {
        Query query=new Query();
        query.addCriteria(Criteria.where(GupshupDeliveryReport.Constants.MANUAL_NOTIFICATION_ID).in(manualNotificationIds));
        return runQuery(query,GupshupDeliveryReport.class);
    }

    //smsclick
    public GupshupDeliveryReport getById(String id) {
        GupshupDeliveryReport GupshupDeliveryReport = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                GupshupDeliveryReport = (GupshupDeliveryReport) getEntityById(id, GupshupDeliveryReport.class);
            }
        } catch (Exception ex) {
            GupshupDeliveryReport = null;
        }
        return GupshupDeliveryReport;
    }
}
