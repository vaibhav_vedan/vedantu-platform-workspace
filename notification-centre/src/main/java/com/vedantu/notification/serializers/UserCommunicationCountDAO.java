package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.UserCommunicationCount;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserCommunicationCountDAO extends AbstractMongoDAO {

    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(UserCommunicationCountDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    protected MongoOperations getMongoOperations(){
        return mongoClientFactory.getMongoOperations();
    }

    public void save(UserCommunicationCount userCommunicationCount){
        logger.info("ENTRY: Saving UserCommunicationCount doc: "+userCommunicationCount);
        if(userCommunicationCount != null){
            saveEntity(userCommunicationCount);
        }
        logger.info("EXIT");
    }

    public void save(List<UserCommunicationCount> userCommunicationCountList){
        logger.info("ENTRY: save method saving list: "+userCommunicationCountList.toString());
        if(ArrayUtils.isNotEmpty(userCommunicationCountList)){
            insertAllEntities(userCommunicationCountList, "UserCommunicationCount");
        }
        logger.info("EXIT: save");
    }

    public int getCountByAddress(String address, String date, CommunicationType communicationType){
        logger.info("ENTRY: getCountByAddress with address: "+address+", date: "+date+", communicationType: "+communicationType);
        Query query = new Query();
        query.addCriteria(Criteria.where(UserCommunicationCount.Constants.USER_ADDRESS).is(address));
        query.addCriteria(Criteria.where(UserCommunicationCount.Constants.DATE).is(date));
        query.addCriteria(Criteria.where(UserCommunicationCount.Constants.COMMUNICATION_TYPE).is(communicationType));
        UserCommunicationCount userCommunicationCount = findOne(query, UserCommunicationCount.class);
        if(userCommunicationCount == null)
            return 0;
        logger.info("EXIT: getCountByAddress");
        return userCommunicationCount.getCount();
    }

    public Map<String, Integer> getCountByAddressList(List<String> addressList, String date, CommunicationType communicationType){
        logger.info("ENTRY: getCountByAddressList with addressList: "+addressList+", date: "+date+", communicationType: "+communicationType);
        Map<String, Integer> map = new HashMap<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(UserCommunicationCount.Constants.USER_ADDRESS).in(addressList));
        query.addCriteria(Criteria.where(UserCommunicationCount.Constants.DATE).in(date));
        query.addCriteria(Criteria.where(UserCommunicationCount.Constants.COMMUNICATION_TYPE).is(communicationType));
        List<UserCommunicationCount> list = getMongoOperations().find(query, UserCommunicationCount.class);
        logger.info("query results size: "+list.size());
        if(ArrayUtils.isNotEmpty(list)){
            for(UserCommunicationCount userCommunicationCount: list){
                map.put(userCommunicationCount.getUserAddress(), userCommunicationCount.getCount());
            }
        }
        for(String address: addressList){
            if(!map.containsKey(address)){
                map.put(address, 0);
            }
        }
        logger.info("map size: "+map.size());
        logger.info("EXIT: getCountByAddressList");
        return map;
    }

    public void incrementUserDayCount(String userAddress, String date, CommunicationType communicationType){
        logger.info("ENTRY: incrementUserDayCount for single userAddress");
        if(StringUtils.isNotEmpty(userAddress) && StringUtils.isNotEmpty(date) && communicationType != null){
            logger.info("userAddress: "+userAddress+", date: "+date+", communicationType: "+communicationType);
            Query query = new Query();
            query.addCriteria(Criteria.where(UserCommunicationCount.Constants.USER_ADDRESS).is(userAddress));
            query.addCriteria(Criteria.where(UserCommunicationCount.Constants.DATE).is(date));
            query.addCriteria(Criteria.where(UserCommunicationCount.Constants.COMMUNICATION_TYPE).is(communicationType));
            Update update = new Update();
            update.inc(UserCommunicationCount.Constants.COUNT, 1);
            update.set(UserCommunicationCount.Constants.LAST_UPDATED, System.currentTimeMillis());
            upsertEntity(query, update, UserCommunicationCount.class);
        }
        logger.info("EXIT: incrementUserDayCount for single userAddress");
    }

    public void incrementUserDayCount(List<String> addressList, String date, CommunicationType communicationType){
        logger.info("ENTRY: incrementUserDayCount for many addresses");
        if(ArrayUtils.isNotEmpty(addressList) && StringUtils.isNotEmpty(date) && communicationType != null){
            logger.info("addressList: "+addressList+", date: "+date+", communicationType: "+communicationType);
            Query query = new Query();
            query.addCriteria(Criteria.where(UserCommunicationCount.Constants.USER_ADDRESS).in(addressList));
            query.addCriteria(Criteria.where(UserCommunicationCount.Constants.DATE).is(date));
            query.addCriteria(Criteria.where(UserCommunicationCount.Constants.COMMUNICATION_TYPE).is(communicationType));
            Update update = new Update();
            update.inc(UserCommunicationCount.Constants.COUNT, 1);
            update.set(UserCommunicationCount.Constants.LAST_UPDATED, System.currentTimeMillis());
            getMongoOperations().updateMulti(query, update, UserCommunicationCount.class);
        }
        logger.info("EXIT: incrementUserDayCount for many addresses");
    }
}
