/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.notification.entity.TelegramGroup;
import com.vedantu.notification.entity.TelegramUser;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class TelegramGroupDAO extends AbstractMongoDAO{
    
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TelegramGroupDAO.class);
    
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public <E extends AbstractMongoStringIdEntity> void save(E p, String callingUserId) {
            logger.info("ENTRY: " + p);
            if (p != null) {
                    saveEntity(p, callingUserId);
            }
            logger.info("EXIT");
    }    
    
    public TelegramGroup getByTelegramId(Long telegramId){
        Query query = new Query();
        query.addCriteria(Criteria.where("telegramId").is(telegramId));
        query.addCriteria(Criteria.where(TelegramGroup.Constants.ENTITY_STATE).is(EntityState.ACTIVE));        
        return findOne(query, TelegramGroup.class);
    }
    
    public TelegramGroup getByBatchId(String batchId){
        Query query = new Query();
        query.addCriteria(Criteria.where("batchIds").is(batchId));
        query.addCriteria(Criteria.where(TelegramGroup.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return findOne(query, TelegramGroup.class);
    }  
    
    public List<TelegramGroup> getByBatchIds(List<String> batchIds){
        Query query = new Query();
        query.addCriteria(Criteria.where("batchIds").in(batchIds));
        query.addCriteria(Criteria.where(TelegramGroup.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return runQuery(query, TelegramGroup.class);        
    }
    
}
