package com.vedantu.notification.serializers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.notification.entity.ManualNotification;
import com.vedantu.notification.entity.NoticeBoard;
import com.vedantu.notification.entity.Notification;
import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.notification.managers.NoticeBoardManager;
import com.vedantu.notification.request.ChangeManualNotificationRequest;
import com.vedantu.notification.request.NotificationFilterReq;
import com.vedantu.notification.responses.NoticeBoardRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class NoticeBoardDAO extends AbstractMongoDAO {

    public NoticeBoardDAO() {
        super();
    }

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    @Autowired
    private LogFactory logfactory;

    @Autowired
    private ManualNotificationDAO manualNotificationDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(NoticeBoardDAO.class);

    public void create(NoticeBoard noticeBoard){
        try{
            if(noticeBoard!=null){
                if(noticeBoard.getScheduledTime()==0){
                    noticeBoard.setScheduledTime(System.currentTimeMillis());
                }
                saveEntity(noticeBoard);
            }
        }catch (Exception ex){
            logger.error("Error in saving notice board data ", ex);
        }
    }

    public List<NoticeBoard> getAllNotices(NotificationFilterReq filterReq){
        Query query=new Query();
        if(filterReq.getStatus()!=null) {
            query.addCriteria(Criteria.where(NoticeBoard.Constants.NOTIFICATION_STATUS).is(filterReq.getStatus()));
        }
        query.addCriteria(Criteria.where(NoticeBoard.Constants.NOTIFICATION_STATUS).is(NotificationStatus.ACTIVE));
        query.skip(filterReq.getSkip());
        query.limit(filterReq.getLimit());
        logger.info("\nQuery"+query);
        return runQuery(query,NoticeBoard.class);
    }

    public void updateNoticeBoardDB() {
        long currentTime=System.currentTimeMillis();

        /**
         * Update the records for expiry
         */
        Query query=new Query();
        query.addCriteria(Criteria.where(NoticeBoard.Constants.EXPIRY_DATE).lte(currentTime));
        query.addCriteria(Criteria.where(NoticeBoard.Constants.NOTIFICATION_STATUS).is(NotificationStatus.ACTIVE));

        List<NoticeBoard> noticeBoards=runQuery(query,NoticeBoard.class);

        Update update=new Update();
        update.set(NoticeBoard.Constants.NOTIFICATION_STATUS,NotificationStatus.EXPIRED);

        updateMulti(query,update,NoticeBoard.class);

        /**
         * Update record in manual notification for EXPIRED
         */

        List<String> manualNotificationIdsForExpiry=new ArrayList<>();
        noticeBoards.forEach(noticeBoard -> manualNotificationIdsForExpiry.add(noticeBoard.getManualNotificationId()));
        manualNotificationDAO.changeManualNotificationStatus(manualNotificationIdsForExpiry,NotificationStatus.EXPIRED);

        /**
         * Update the records for active
         */
        query=new Query();
        query.addCriteria(Criteria.where(NoticeBoard.Constants.SCHEDULED_TIME).lte(currentTime));
        query.addCriteria(Criteria.where(NoticeBoard.Constants.NOTIFICATION_STATUS).is(NotificationStatus.SCHEDULED));


        noticeBoards=runQuery(query,NoticeBoard.class);

        logger.info("\nQuery for making records active "+query);

        update=new Update();
        update.set(NoticeBoard.Constants.NOTIFICATION_STATUS,NotificationStatus.ACTIVE);
        updateMulti(query,update,NoticeBoard.class);

        /**
         * Update the records in manual notification for ACTIVE
         */

        List<String> manualNotificationIdsForActive=new ArrayList<>();
        noticeBoards.forEach(noticeBoard -> manualNotificationIdsForActive.add(noticeBoard.getManualNotificationId()));
        manualNotificationDAO.changeManualNotificationStatus(manualNotificationIdsForActive,NotificationStatus.ACTIVE);


    }

    public NoticeBoard getNoticeById(String id) throws BadRequestException {
        NoticeBoard notice=getEntityById(id,NoticeBoard.class);
        if(notice==null){
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,"No notice found for id "+id, Level.INFO);
        }
        return notice;
    }

    public List<NoticeBoard> discardNotices(ChangeManualNotificationRequest discardList) {
        Query query=new Query();
        query.addCriteria(Criteria.where(NoticeBoard.Constants.MANUAL_NOTIFICATION_ID).in(discardList.getChangeList()));

        Update update=new Update();
        update.set(NoticeBoard.Constants.NOTIFICATION_STATUS,NotificationStatus.DISCARDED);
        update.set(NoticeBoard.Constants.LAST_UPDATED_BY,discardList.getCallingUserId());

        updateMulti(query,update,NoticeBoard.class);
        List<NoticeBoard> noticeBoards=runQuery(query,NoticeBoard.class);
        String manualNotifId=noticeBoards.get(0).getManualNotificationId();

        discardList.setChangeList(Arrays.asList(manualNotifId));
        manualNotificationDAO.discardScheduled(discardList);
        return noticeBoards;
    }

    public List<NoticeBoard> rescheduleNotices(ChangeManualNotificationRequest rescheduleList) throws BadRequestException {
        if(rescheduleList.getRescheduledTime()!=null && rescheduleList.getReschdeuledExpiryDate()!=null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Both rescheduled expiry time and scheduled time can't be 0",Level.ERROR);
        }

            Query query=new Query();
        query.addCriteria(Criteria.where(NoticeBoard.Constants.MANUAL_NOTIFICATION_ID).in(rescheduleList.getChangeList()));

        Update update=new Update();
        if(rescheduleList.getRescheduledTime()!=null) {
            update.set(NoticeBoard.Constants.SCHEDULED_TIME, rescheduleList.getRescheduledTime());
        }
        if(rescheduleList.getReschdeuledExpiryDate()!=null) {
            update.set(NoticeBoard.Constants.EXPIRY_DATE, rescheduleList.getReschdeuledExpiryDate());
        }
        update.set(NoticeBoard.Constants.LAST_UPDATED_BY,rescheduleList.getCallingUserId());

        updateMulti(query,update,NoticeBoard.class);

        List<NoticeBoard> noticeBoards=runQuery(query,NoticeBoard.class);
        String manualNotifId=noticeBoards.get(0).getManualNotificationId();

        rescheduleList.setChangeList(Arrays.asList(manualNotifId));
        manualNotificationDAO.reschedule(rescheduleList);

        return noticeBoards;
    }

    public List<NoticeBoard> getAllNoticeBoardData(NotificationFilterReq filterReq) {
        Query query=new Query();
        query.skip(filterReq.getSkip());
        query.limit(filterReq.getLimit());
        if(filterReq.getStatus()!=null) {
            query.addCriteria(Criteria.where(ManualNotification.Constants.NOTIFICATION_STATUS).is(filterReq.getStatus()));
        }
        return runQuery(query,NoticeBoard.class);
    }

    public List<NoticeBoard> getNoticeUserId(String userId) {
        Query query=new Query();
        query.addCriteria(Criteria.where(NoticeBoard.Constants.STUDENT_ID).is(userId));
        query.addCriteria(Criteria.where(NoticeBoard.Constants.NOTIFICATION_STATUS).is(NotificationStatus.ACTIVE));
        query.with(Sort.by(Sort.Direction.DESC,NoticeBoard.Constants.CREATION_TIME));
        return runQuery(query,NoticeBoard.class);
    }
}
