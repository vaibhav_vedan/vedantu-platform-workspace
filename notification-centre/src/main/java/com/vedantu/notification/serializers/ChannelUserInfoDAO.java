package com.vedantu.notification.serializers;


import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.notification.entity.ChannelUserInfo;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class ChannelUserInfoDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public ChannelUserInfoDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
    

	public void create(ChannelUserInfo p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}

	public ChannelUserInfo getById(String id) {
		ChannelUserInfo ChannelUserInfo = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				ChannelUserInfo = (ChannelUserInfo) getEntityById(id, ChannelUserInfo.class);
			}
		} catch (Exception ex) {
			ChannelUserInfo = null;
		}
		return ChannelUserInfo;
	}


	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, ChannelUserInfo.class);
		} catch (Exception ex) {
		}

		return result;
	}


}
