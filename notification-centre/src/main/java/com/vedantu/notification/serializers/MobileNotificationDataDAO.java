package com.vedantu.notification.serializers;


import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.notification.entity.MobileNotificationData;
import com.vedantu.notification.entity.Notification;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class MobileNotificationDataDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public MobileNotificationDataDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
    

	public void create(MobileNotificationData p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}

	public MobileNotificationData getById(String id) {
		MobileNotificationData MobileNotificationData = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				MobileNotificationData = (MobileNotificationData) getEntityById(id, MobileNotificationData.class);
			}
		} catch (Exception ex) {
			MobileNotificationData = null;
		}
		return MobileNotificationData;
	}

        public void update(Query q, Update u) {
            try {
                updateFirst(q, u , MobileNotificationData.class);
            } catch (Exception ex) {
            }
        }
        
        
	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, MobileNotificationData.class);
		} catch (Exception ex) {
		}

		return result;
	}
}
