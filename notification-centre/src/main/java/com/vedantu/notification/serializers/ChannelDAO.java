package com.vedantu.notification.serializers;


import java.util.List;

import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.notification.entity.Channel;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;


@Service
public class ChannelDAO extends AbstractMongoDAO {

        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public ChannelDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
    
	public void create(Channel p) {
		try {
			if (p != null) {
				saveEntity(p);
			}
		} catch (Exception ex) {
		}
	}

	public Channel getById(String id) {
		Channel Channel = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				Channel = (Channel) getEntityById(id, Channel.class);
			}
		} catch (Exception ex) {
			Channel = null;
		}
		return Channel;
	}


	public int deleteById(String id) {
		int result = 0;
		try {
			result = deleteEntityById(id, Channel.class);
		} catch (Exception ex) {
		}

        return result;
        }

        public Channel findByParticipants(Long[] parties) {
            Channel channel = null;
            try {
                Query query = new Query(Criteria.where("participants").all(parties));
                query.limit(1);
                List<Channel> channels = runQuery(query, Channel.class);
                if (!channels.isEmpty()) {
                    return channels.get(0);
                }
            } catch (Exception ex) {

            }

            return channel;
        }
}
