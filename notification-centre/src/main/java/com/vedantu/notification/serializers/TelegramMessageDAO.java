/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.notification.entity.TelegramMessage;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class TelegramMessageDAO extends AbstractMongoDAO{
    
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TelegramMessageDAO.class);
    
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    } 
    
    public <E extends AbstractMongoStringIdEntity> void save(E p, String callingUserId) {
           logger.info("ENTRY: " + p);
           if (p != null) {
                   saveEntity(p, callingUserId);
           }
           logger.info("EXIT");
    } 

    public TelegramMessage getByTelegramId(Long messageId, Long chatId){
        Query query = new Query();
        query.addCriteria(Criteria.where("message_id").is(messageId));
        query.addCriteria(Criteria.where("chat.id").is(chatId));
        return findOne(query, TelegramMessage.class);
    }
    
}
