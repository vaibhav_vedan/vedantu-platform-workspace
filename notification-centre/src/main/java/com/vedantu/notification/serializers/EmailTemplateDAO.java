package com.vedantu.notification.serializers;

import com.vedantu.User.Role;
import com.vedantu.exception.DuplicateEntryException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.notification.entity.EmailTemplate;
import com.vedantu.notification.entity.SmsTemplate;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailTemplateDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private Logger logger = logFactory.getLogger(EmailTemplateDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(EmailTemplate emailTemplate, Long callingUserId) throws VException {
        if(emailTemplate != null) {
            try{
                saveEntity(emailTemplate, callingUserId.toString());
            }catch (Exception e){
                throw new VException(ErrorCode.DUPLICATE_ENTRY, "Duplicate entry for emailType and role");
            }
        }
    }

    public List<EmailTemplate> getEmailTemplatesWithFilter(Integer start, Integer limit, EntityState state){
        Query query = new Query();
        if (state == null) {
            query.addCriteria(Criteria.where(EmailTemplate.Constants.ENTITY_STATE).in(EntityState.ACTIVE, EntityState.INACTIVE));
        } else {
            query.addCriteria(Criteria.where(EmailTemplate.Constants.ENTITY_STATE).is(state));
        }
        setFetchParameters(query, start, limit);
        query.with(Sort.by(Sort.Direction.DESC, EmailTemplate.Constants.LAST_UPDATED));
        logger.info("query: "+query);
        return runQuery(query, EmailTemplate.class);
    }

    public EmailTemplate getEmailTemplateByEmailTypeAndRole(CommunicationType emailType, Role role){
        if(emailType == null || role == null)
            return null;
        Query query = new Query();
        query.addCriteria(Criteria.where(EmailTemplate.Constants.EMAIL_TYPE).is(emailType));
        query.addCriteria(Criteria.where(SmsTemplate.Constants.ROLE).is(role));
        return findOne(query, EmailTemplate.class);
    }

    public EmailTemplate getEmailTemplateByEmailType(CommunicationType emailType){
        Query query = new Query();
        query.addCriteria(Criteria.where(EmailTemplate.Constants.EMAIL_TYPE).is(emailType));
        query.addCriteria(Criteria.where(SmsTemplate.Constants.ROLE).exists(false));
        return findOne(query, EmailTemplate.class);
    }
}
