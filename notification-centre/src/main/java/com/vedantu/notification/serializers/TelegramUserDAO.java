/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.serializers;

import com.vedantu.exception.NotFoundException;
import com.vedantu.notification.entity.TelegramUser;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author parashar
 */
@Service
public class TelegramUserDAO extends AbstractMongoDAO{
    
    @Autowired
    LogFactory logFactory;

    Logger logger = logFactory.getLogger(TelegramUserDAO.class);
    
    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }    
    
    public List<TelegramUser> getUsersByBatchId(String batchId){
        
        if(batchId!=null){
            Query query = new Query();
            query.addCriteria(Criteria.where("batchIds").is(batchId));
            return runQuery(query, TelegramUser.class);
        }
        return new ArrayList<>();
    }
    
    public TelegramUser getByTelegramId(Long telegramId){
        Query query = new Query();
        query.addCriteria(Criteria.where("telegramId").is(telegramId));
        query.addCriteria(Criteria.where(TelegramUser.Constants.ENTITY_STATE).is(EntityState.ACTIVE));        
        return findOne(query, TelegramUser.class);
    }
    
    public TelegramUser getByUserId(Long userId){
        Query query = new Query();
        query.addCriteria(Criteria.where("userId").is(userId));
        query.addCriteria(Criteria.where(TelegramUser.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return findOne(query, TelegramUser.class);
    }    
    
    public TelegramUser getByEmail(String email){
        Query query = new Query();
        query.addCriteria(Criteria.where("email").is(email));
        return findOne(query, TelegramUser.class);
        
    }

    public <E extends AbstractMongoStringIdEntity> void save(E p, String callingUserId) {
            logger.info("ENTRY: " + p);
            if (p != null) {
                    saveEntity(p, callingUserId);
            }
            logger.info("EXIT");
    }     
    
}
