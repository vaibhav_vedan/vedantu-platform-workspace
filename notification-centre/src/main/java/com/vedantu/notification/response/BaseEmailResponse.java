package com.vedantu.notification.response;

/*
 * Response object for sendEmail API
 */
public class BaseEmailResponse extends BaseResponse {

    private String email;
    private String status;
    private String id;
    private String rejectReason;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(String rejectReason) {
        this.rejectReason = rejectReason;
    }

    @Override
    public String toString() {
        return "BaseEmailResponse{" + "email=" + email + ", status=" + status + ", id=" + id + ", rejectReason=" + rejectReason + '}';
    }

}
