package com.vedantu.notification.response;

/*
 * Response object for getSendQuota API
 */
public class GetSendStatisticsResponse extends BaseResponse{
	
	private Long deliveryAttempts;
	private Long bounces;
	private Long complaints;
	private Long rejects;

	public Long getDeliveryAttempts() {
		return deliveryAttempts;
	}

	public void setDeliveryAttempts(Long deliveryAttempts) {
		this.deliveryAttempts = deliveryAttempts;
	}

	public Long getBounces() {
		return bounces;
	}

	public void setBounces(Long bounces) {
		this.bounces = bounces;
	}

	public Long getComplaints() {
		return complaints;
	}

	public void setComplaints(Long complaints) {
		this.complaints = complaints;
	}

	public Long getRejects() {
		return rejects;
	}

	public void setRejects(Long rejects) {
		this.rejects = rejects;
	}

	@Override
	public String toString() {
		return "getSendStatistics deliveryAttempts:" + deliveryAttempts + ", bounces:" + bounces 
				+ ", complaints:" + complaints + ", rejects:" + rejects;
	}
	
	
}
