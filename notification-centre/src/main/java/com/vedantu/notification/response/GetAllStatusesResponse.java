package com.vedantu.notification.response;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.notification.entity.UserStatus;

/*
 * Response object for fetchLastUsersChattedWithApi
 */
public class GetAllStatusesResponse extends BaseResponse {
	
	private List<UserStatus> result;
	
	public GetAllStatusesResponse(ArrayList<UserStatus> result) {
		super();
		this.result = result;
	}

	public GetAllStatusesResponse() {
		super();
	}

	public List<UserStatus> getResult() {
		return result;
	}

	public void setResult(List<UserStatus> currentUserStatuses) {
		this.result = currentUserStatuses;
	}

	@Override
	public String toString() {
		return "GetLastUserStatussResponse result:" + result.toString();
	}	

}
