package com.vedantu.notification.response;

import com.vedantu.notification.entity.UserStatus;

/*
 * Response object for fetchLastUsersChattedWithApi
 */
public class GetMyStatusResponse extends BaseResponse {
	
	private UserStatus result;
	
	public GetMyStatusResponse(UserStatus result) {
		super();
		this.result = result;
	}

	public GetMyStatusResponse() {
		super();
	}

	public UserStatus getResult() {
		return result;
	}

	public void setResult(UserStatus currentUserStatuses) {
		this.result = currentUserStatuses;
	}

	@Override
	public String toString() {
		return "GetLastUserStatussResponse result:" + result.toString();
	}	

}
