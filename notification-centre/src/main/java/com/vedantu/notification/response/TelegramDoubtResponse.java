/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.response;

/**
 *
 * @author parashar
 */
public class TelegramDoubtResponse {
    
    private String doubtId;
    private String name;
    private String batchGroupTitle;
    private String batchId;
    private String doubtText;
    private Long askedAt;
    private boolean closed = false;

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the batchGroupTitle
     */
    public String getBatchGroupTitle() {
        return batchGroupTitle;
    }

    /**
     * @param batchGroupTitle the batchGroupTitle to set
     */
    public void setBatchGroupTitle(String batchGroupTitle) {
        this.batchGroupTitle = batchGroupTitle;
    }

    /**
     * @return the doubtText
     */
    public String getDoubtText() {
        return doubtText;
    }

    /**
     * @param doubtText the doubtText to set
     */
    public void setDoubtText(String doubtText) {
        this.doubtText = doubtText;
    }

    /**
     * @return the askedAt
     */
    public Long getAskedAt() {
        return askedAt;
    }

    /**
     * @param askedAt the askedAt to set
     */
    public void setAskedAt(Long askedAt) {
        this.askedAt = askedAt;
    }

    /**
     * @return the closed
     */
    public boolean isClosed() {
        return closed;
    }

    /**
     * @param closed the closed to set
     */
    public void setClosed(boolean closed) {
        this.closed = closed;
    }

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the doubtId
     */
    public String getDoubtId() {
        return doubtId;
    }

    /**
     * @param doubtId the doubtId to set
     */
    public void setDoubtId(String doubtId) {
        this.doubtId = doubtId;
    }
    
}
