package com.vedantu.notification.response;

import java.util.ArrayList;

import javax.mail.internet.InternetAddress;

/*
 * Response object for sendBulkSMS API
 */
public class SendBulkSMSResponse extends BaseResponse {
	
	private ArrayList<SendSMSResponse> bulkStatus;	
	
	public ArrayList<SendSMSResponse> getBulkStatus() {
		return bulkStatus;
	}

	public void setBulkStatus(ArrayList<SendSMSResponse> bulkStatus) {
		this.bulkStatus = bulkStatus;
	}

	@Override
	public String toString() {
		ArrayList<String> status = new ArrayList<String>();
		for(SendSMSResponse response: bulkStatus){
			status.add(response.toString());
		}	
		
		return "SendSMSResponse bulkStatus:" + status.toString();
	}

}
