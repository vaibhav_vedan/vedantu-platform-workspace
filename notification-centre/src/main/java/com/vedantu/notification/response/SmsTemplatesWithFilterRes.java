package com.vedantu.notification.response;

import com.vedantu.User.Role;
import com.vedantu.notification.entity.SmsTemplate;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class SmsTemplatesWithFilterRes {

    private CommunicationType smsType;
    private Role role;
    private String body;
    private Boolean unSubscriptionCheck;
    private Boolean unSubscriptionLink;
    private String description;

    private String id;
    private Long creationTime;
    private String createdBy;
    private String createdByEmail;
    private Long lastUpdated;
    private String lastUpdatedBy;
    private String lastUpdatedByEmail;
    private EntityState entityState;

    public SmsTemplatesWithFilterRes(SmsTemplate smsTemplate, String createdByEmail, String lastUpdatedByEmail){
        this.smsType = smsTemplate.getSmsType();
        this.role = smsTemplate.getRole();
        this.body = smsTemplate.getBody();
        this.unSubscriptionCheck = smsTemplate.getUnSubscriptionCheck();
        this.unSubscriptionLink = smsTemplate.getUnSubscriptionLink();
        this.description = smsTemplate.getDescription();

        this.id = smsTemplate.getId();
        this.creationTime = smsTemplate.getCreationTime();
        this.createdBy = smsTemplate.getCreatedBy();
        this.createdByEmail = createdByEmail;
        this.lastUpdated = smsTemplate.getLastUpdated();
        this.lastUpdatedBy = smsTemplate.getLastUpdatedBy();
        this.lastUpdatedByEmail = lastUpdatedByEmail;
        this.entityState = smsTemplate.getEntityState();
    }


}
