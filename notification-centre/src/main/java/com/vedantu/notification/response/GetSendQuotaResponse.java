package com.vedantu.notification.response;

/*
 * Response object for getSendQuota API
 */
public class GetSendQuotaResponse extends BaseResponse{
	
	private Double max24HourSend;
	private Double maxSendRate;
	private Double sentLast24Hours;
	
	public Double getMax24HourSend() {
		return max24HourSend;
	}

	public void setMax24HourSend(Double max24HourSend) {
		this.max24HourSend = max24HourSend;
	}

	public Double getMaxSendRate() {
		return maxSendRate;
	}

	public void setMaxSendRate(Double maxSendRate) {
		this.maxSendRate = maxSendRate;
	}

	public Double getSentLast24Hours() {
		return sentLast24Hours;
	}

	public void setSentLast24Hours(Double sentLast24Hours) {
		this.sentLast24Hours = sentLast24Hours;
	}

	@Override
	public String toString() {
		return "getSendQuota max24HourSend:" + max24HourSend + ", maxSendRate:" + maxSendRate + ", sentLast24Hours:" + sentLast24Hours;
	}
	
	
}
