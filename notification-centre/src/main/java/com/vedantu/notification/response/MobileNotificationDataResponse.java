package com.vedantu.notification.response;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONObject;

import com.vedantu.notification.entity.MobileNotificationData;
import com.vedantu.notification.enums.NotificationType;
import com.vedantu.notification.pojo.ButtonData;

/*
 * Extra notification data for mobile
 */
public class MobileNotificationDataResponse extends BaseResponse {

	private String title;
	private String description;
	private String imageUrl;
	private String landingActivity;
	private Long timestamp;
	private String extra;
	private List<ButtonData> buttons;
	private NotificationType type;
	
	public MobileNotificationDataResponse() {
		super();
	}
	
	public MobileNotificationDataResponse(String title, String description, String imageUrl, 
			String landingActivity, Long timestamp, String extra, List<ButtonData> buttons) {
		this.title = title;
		this.description = description;
		this.imageUrl = imageUrl;
		this.landingActivity = landingActivity;
		this.timestamp = timestamp;
		this.extra = extra;
		this.buttons = buttons;
	}
	
	public MobileNotificationDataResponse(MobileNotificationData prevMobileData) {
		this.title = prevMobileData.getTitle();
		this.description = prevMobileData.getDescription();
		this.imageUrl = prevMobileData.getImageUrl();
		this.landingActivity = prevMobileData.getLandingActivity();
		this.timestamp = prevMobileData.getTimestamp();
		this.extra = prevMobileData.getExtra();
		this.buttons = prevMobileData.getButtons();
	}

	public NotificationType getType() {
		return type;
	}

	public void setType(NotificationType type) {
		this.type = type;
	}	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getLandingActivity() {
		return landingActivity;
	}

	public void setLandingActivity(String landingActivity) {
		this.landingActivity = landingActivity;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public String getExtra() {
		return extra;
	}

	public void setExtra(String extra) {
		this.extra = extra;
	}

	public List<ButtonData> getButtons() {
		return buttons;
	}

	public void setButtons(List<ButtonData> buttons) {
		this.buttons = buttons;
	}

	@Override
	public String toString() {	
		
		return "MobileNotification title:" + title +  ", description:"
				+ description + ", imageUrl:" + imageUrl + ", landingActivity:" + landingActivity
				 + ", timestamp:" + timestamp + ", extra:" + extra.toString() + ", buttons:" + buttons.toString()
				 + ", type:" + type.toString();
		
	}
	
	public static class Constants {
		public static final String TITLE = "title";
		public static final String DESCRIPTION = "description";
		public static final String TIMESTAMP = "timestamp";
		public static final String TYPE = "type";
	}
	
}
