package com.vedantu.notification.response;

import java.util.ArrayList;

public class SendEmailResponse extends BaseResponse {

    private ArrayList<BaseEmailResponse> status;

    public ArrayList<BaseEmailResponse> getStatus() {
        return status;
    }

    public void setStatus(ArrayList<BaseEmailResponse> status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return super.toString() + " SendEmailResponse{" + "status=" + status + '}';
    }

}
