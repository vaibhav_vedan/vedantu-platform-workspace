package com.vedantu.notification.response;

import javax.mail.Address;

import com.vedantu.notification.entity.Email;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.EmailType;

public class EmailOpenedResponse extends BaseResponse{
	
	public Address to;
	public boolean opened;
	public String id;
	public String subject;	
	public CommunicationType type;
	
	public EmailOpenedResponse(Email email){
		
		this.to = email.getTo();
		this.opened = email.isOpened();
		this.id = email.getId();
		this.subject = email.getSubject();
		this.type = email.getType();
		
	}
	
	public CommunicationType getType() {
		return type;
	}

	public void setType(CommunicationType type) {
		this.type = type;
	}

	public Address getTo() {
		return to;
	}

	public void setTo(Address to) {
		this.to = to;
	}

	public boolean isOpened() {
		return opened;
	}

	public void setOpened(boolean opened) {
		this.opened = opened;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	@Override
	public String toString() {
		return "EmailOpenedResponse id:" + id + ", to:" + to.toString() + ", subject:" + subject
				+ ", type:" + type + ", opened:" + opened;
	}
	
}
