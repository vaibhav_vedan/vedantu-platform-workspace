package com.vedantu.notification.response;

import com.vedantu.exception.ErrorCode;

public class BaseResponse {

	public ErrorCode errorCode;
	public String errorMessage;

	public BaseResponse(ErrorCode errorCode, String errorMessage) {
		this.errorCode = errorCode;
		this.errorMessage = errorMessage;
	}

	public BaseResponse() {
		super();
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}

    @Override
    public String toString() {
        return "BaseResponse{" + "errorCode=" + errorCode + ", errorMessage=" + errorMessage + '}';
    }

}
