package com.vedantu.notification.response;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.InternetAddress;

import com.vedantu.notification.pojo.ChattedUser;

/*
 * Response object for fetchLastUsersChattedWithApi
 */
public class GetLastChattedUsersResponse extends BaseResponse {
	
	private List<ChattedUser> result;
	
	public GetLastChattedUsersResponse(ArrayList<ChattedUser> result) {
		super();
		this.result = result;
	}

	public GetLastChattedUsersResponse() {
		super();
	}

	public List<ChattedUser> getResult() {
		return result;
	}

	public void setResult(List<ChattedUser> result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "GetLastChattedUsersResponse result:" + result.toString();
	}	

}
