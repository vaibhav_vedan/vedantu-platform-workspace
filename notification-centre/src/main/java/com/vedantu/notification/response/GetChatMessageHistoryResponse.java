/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.response;

import com.vedantu.notification.entity.ChatMessage;
import java.util.List;

/**
 *
 * @author jeet
 */
public class GetChatMessageHistoryResponse extends BaseResponse{
    private List<ChatMessage> messages;

    public GetChatMessageHistoryResponse(List<ChatMessage> messages) {
        super();
        this.messages = messages;
    }
    public GetChatMessageHistoryResponse() {
        super();
    }

    public List<ChatMessage> getMessages() {
        return messages;
    }

    public void setMessages(List<ChatMessage> messages) {
        this.messages = messages;
    }

    @Override
    public String toString() {
        return "GetChatMessageHistoryResponse{" + "messages=" + messages + '}';
    }
    
}
