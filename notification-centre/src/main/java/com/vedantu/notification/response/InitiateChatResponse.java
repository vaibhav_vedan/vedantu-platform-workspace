package com.vedantu.notification.response;

import java.util.ArrayList;
import java.util.List;

import javax.mail.internet.InternetAddress;

import com.vedantu.notification.pojo.ChattedUser;

/*
 * Response object for fetchLastUsersChattedWithApi
 */
public class InitiateChatResponse extends BaseResponse {
	
	private ChattedUser result;
	
	public InitiateChatResponse(ChattedUser result) {
		super();
		this.result = result;
	}

	public InitiateChatResponse() {
		super();
	}

	public ChattedUser getResult() {
		return result;
	}

	public void setResult(ChattedUser result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return "GetLastChattedUsersResponse result:" + result.toString();
	}	

}
