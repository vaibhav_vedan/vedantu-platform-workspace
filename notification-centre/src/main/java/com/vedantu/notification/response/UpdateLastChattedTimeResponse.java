package com.vedantu.notification.response;

public class UpdateLastChattedTimeResponse extends BaseResponse{
	
	public Long serverTime;

	public Long getServerTime() {
		return serverTime;
	}

	public void setServerTime(Long serverTime) {
		this.serverTime = serverTime;
	}

	@Override
	public String toString() {
		return "UpdateLastChattedTimeResponse  serverTime=" + serverTime + "]";
	}

	public UpdateLastChattedTimeResponse(Long serverTime) {
		super();
		
		this.serverTime = serverTime;
	}
	
	public UpdateLastChattedTimeResponse() {
		super();
	}
	
	
}
