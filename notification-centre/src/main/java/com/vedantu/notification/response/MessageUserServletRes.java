/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.response;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.util.fos.response.AbstractRes;

/**
 *
 * @author MNPK
 */
public class MessageUserServletRes extends AbstractRes {

    private UserBasicInfo fromUser;
    private UserBasicInfo toUser;
    private String message;

    public MessageUserServletRes() {
        super();
    }

    public MessageUserServletRes(UserBasicInfo fromUser, UserBasicInfo toUser, String message) {
        super();
        this.fromUser = fromUser;
        this.toUser = toUser;
        this.message = message;
    }

    public UserBasicInfo getFromUser() {
        return fromUser;
    }

    public void setFromUser(UserBasicInfo fromUser) {
        this.fromUser = fromUser;
    }

    public UserBasicInfo getToUser() {
        return toUser;
    }

    public void setToUser(UserBasicInfo toUser) {
        this.toUser = toUser;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "MessageUserServletRes{" + "fromUser=" + fromUser + ", toUser=" + toUser + ", message=" + message + '}';
    }

}
