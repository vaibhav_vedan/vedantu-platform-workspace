package com.vedantu.notification.response;

import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.util.enums.CommunicationKind;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ManualNotificationDetailsRes {
    private String id;
    private CommunicationKind communicationKind;
    private String subject;
    private String body;
    private long scheduledTime;
    private long expiryTime;

    private List<String> userIds;
    private List<String> batchIds;
    private List<String> otmBundleIds;
    private List<String> aioPackageIds;
    private List<String> allUserIds;
    private List<String> emailIds;

    private int sentCount;
    private double deliveryRate;
    private double openRate;
    private String sentBy;

    private NotificationStatus status;
}
