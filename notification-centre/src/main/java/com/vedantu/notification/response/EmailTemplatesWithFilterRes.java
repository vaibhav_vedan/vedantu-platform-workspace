package com.vedantu.notification.response;


import com.vedantu.User.Role;
import com.vedantu.notification.entity.EmailTemplate;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import lombok.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EmailTemplatesWithFilterRes {

    private CommunicationType emailType;
    private Role role;
    private Boolean unSubscriptionCheck;
    private Boolean unSubscriptionLink;
    private String description;

    private String id;
    private Long creationTime;
    private String createdBy;
    private String createdByEmail;
    private Long lastUpdated;
    private String lastUpdatedBy;
    private String lastUpdatedByEmail;
    private EntityState entityState;



    public EmailTemplatesWithFilterRes(EmailTemplate emailTemplate, String createdByEmail, String lastUpdatedByEmail){

        this.emailType = emailTemplate.getEmailType();
        this.role = emailTemplate.getRole();
        this.unSubscriptionCheck = emailTemplate.getUnSubscriptionCheck();
        this.unSubscriptionLink = emailTemplate.getUnSubscriptionLink();
        this.description = emailTemplate.getDescription();

        this.id = emailTemplate.getId();
        this.creationTime = emailTemplate.getCreationTime();
        this.createdBy = emailTemplate.getCreatedBy();
        this.createdByEmail = createdByEmail;
        this.lastUpdated  = emailTemplate.getLastUpdated();
        this.lastUpdatedBy = emailTemplate.getLastUpdatedBy();
        this.lastUpdatedByEmail = lastUpdatedByEmail;
        this.entityState = emailTemplate.getEntityState();
    }
}
