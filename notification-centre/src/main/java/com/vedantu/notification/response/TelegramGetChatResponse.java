/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.notification.response;

import com.vedantu.notification.pojo.TelegramGetChatPojo;

/**
 *
 * @author parashar
 */
public class TelegramGetChatResponse extends TelegramResponse {
    
    private TelegramGetChatPojo result;

    /**
     * @return the result
     */
    public TelegramGetChatPojo getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(TelegramGetChatPojo result) {
        this.result = result;
    }
    
    
}
