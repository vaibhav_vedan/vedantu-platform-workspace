package com.vedantu.notification.response;

/*
 * Response object for sendSMS API
 */
public class SendSMSResponse extends BaseResponse{
	
	private String smsId;
	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSmsId() {
		return smsId;
	}

	public void setSmsId(String smsId) {
		this.smsId = smsId;
	}	
	
	@Override
	public String toString() {
		return "SendSMSResponse smsId:" + smsId + ", status:" + status;
	}

}
