package com.vedantu.notification.response.blockedPhone;

import com.vedantu.notification.entity.BlockedPhone;
import com.vedantu.util.fos.response.AbstractRes;

import java.util.List;

public class BlockedPhoneRes extends AbstractRes {
    private List<BlockedPhone> blockedPhoneList;

    public List<BlockedPhone> getBlockedPhoneList() {
        return blockedPhoneList;
    }

    public void setBlockedPhoneList(List<BlockedPhone> blockedPhoneList) {
        this.blockedPhoneList = blockedPhoneList;
    }
}
