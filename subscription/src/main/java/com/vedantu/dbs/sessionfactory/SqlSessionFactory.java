package com.vedantu.dbs.sessionfactory;

import com.mysql.jdbc.AbandonedConnectionCleanupThread;
import com.vedantu.subscription.entities.sql.CoursePlanHourTransaction;
import com.vedantu.subscription.entities.sql.CoursePlanHours;
import com.vedantu.subscription.entities.sql.EnrollmentConsumption;
import com.vedantu.subscription.entities.sql.EnrollmentTransaction;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.sql.HourTransaction;
import com.vedantu.subscription.entities.sql.Refund;
import com.vedantu.subscription.entities.sql.Subscription;
import com.vedantu.subscription.entities.sql.SubscriptionDetails;
import com.vedantu.subscription.entities.sql.SubscriptionRequest;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import javax.annotation.PreDestroy;

@Service
public class SqlSessionFactory {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SqlSessionFactory.class);

    private SessionFactory sessionFactory = null;

    public SqlSessionFactory() {
        logger.info("initializing Sql Session Factory");

        try {
            Configuration configuration = new Configuration();
            String path = "ENV-" + ConfigUtils.INSTANCE.properties.getProperty("environment") + java.io.File.separator + "hibernate.cfg.xml";
            configuration.configure(path);
            configuration.addAnnotatedClass(Subscription.class);
            configuration.addAnnotatedClass(SubscriptionDetails.class);
            configuration.addAnnotatedClass(Refund.class);
            configuration.addAnnotatedClass(SubscriptionRequest.class);
            configuration.addAnnotatedClass(HourTransaction.class);
            configuration.addAnnotatedClass(CoursePlanHours.class);
            configuration.addAnnotatedClass(CoursePlanHourTransaction.class);
            configuration.addAnnotatedClass(EnrollmentConsumption.class);
            configuration.addAnnotatedClass(EnrollmentTransaction.class);
            Properties properties = new Properties();
            properties.put("hibernate.connection.username",ConfigUtils.INSTANCE.properties.getProperty("hibernate.connection.username"));
            properties.put("hibernate.connection.password",ConfigUtils.INSTANCE.properties.getProperty("hibernate.connection.password"));
            configuration.configure(path).addProperties(properties);
            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Exception e) {
            logger.error("error in creating sql connection ",e);
        }
    }

    /**
     * @return the session
     */
    public SessionFactory getSessionFactory() {
        return this.sessionFactory;
    }

    @PreDestroy
    public void cleanUp() {
        try {

//            logger.info("closing PooledDataSources " + C3P0Registry.getNumPooledDataSources());
//            for (Object o : C3P0Registry.getPooledDataSources()) {
//                logger.info("closing " + o.getClass());
//                try {
//                    PooledDataSource p=((PooledDataSource) o);
//                    p.hardReset();
//                    p.close();
//                } catch (Exception e) {
//                    logger.info("Error in closing "+e.getMessage());
//                }
//            }
//            
            if (sessionFactory != null) {
                sessionFactory.close();
            }

            Enumeration<Driver> drivers = DriverManager.getDrivers();

            Driver driver = null;

            // clear drivers
            while (drivers.hasMoreElements()) {
                try {
                    driver = drivers.nextElement();
                    logger.info("deregistering driver " + driver.getMajorVersion());
                    DriverManager.deregisterDriver(driver);
                } catch (SQLException ex) {
                    logger.error("exceoton in driver deregister " + ex.getMessage());
                }
            }

            AbandonedConnectionCleanupThread.shutdown();
//            logger.info("waiting for 5 secs");
//            Thread.sleep(2000);
            logger.info("cleaning done");
        } catch (Exception e) {
            logger.error("Error in closing sql connection ", e);
        }
    }
}
