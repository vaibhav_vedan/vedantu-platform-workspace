package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class ClickToEnrollReq extends AbstractFrontEndReq {

    String bundleId;
    String batchId;
    String userId;

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
