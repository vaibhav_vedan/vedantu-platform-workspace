/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

/**
 *
 * @author parashar
 */
public class StudentCoursePlanInfo {
    
    private String teacherId;
    private String teacherName;
    private Integer intendedHours;
    private Long startDate;
    private String title;

    /**
     * @return the teacherId
     */
    public String getTeacherId() {
        return teacherId;
    }

    /**
     * @param teacherId the teacherId to set
     */
    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    /**
     * @return the teacherName
     */
    public String getTeacherName() {
        return teacherName;
    }

    /**
     * @param teacherName the teacherName to set
     */
    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    /**
     * @return the intendedHours
     */
    public Integer getIntendedHours() {
        return intendedHours;
    }

    /**
     * @param intendedHours the intendedHours to set
     */
    public void setIntendedHours(Integer intendedHours) {
        this.intendedHours = intendedHours;
    }

    /**
     * @return the startDate
     */
    public Long getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }
    
    
}
