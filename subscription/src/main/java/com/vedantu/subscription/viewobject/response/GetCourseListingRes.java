/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 *
 * @author parashar
 */
public class GetCourseListingRes implements Comparable<GetCourseListingRes>{
    
    private List<Long> startTimes;
    private String title;
    private List<Long> boardIds;
    private Long firstStartTime;
    private Integer price;
    private boolean isBundle;
    private String entityId;
    private Set<String> targets;
    private List<String> subjects;
    private Long duration;
    private String tag;

    /**
     * @return the startTimes
     */
    public List<Long> getStartTimes() {
        return startTimes;
    }

    /**
     * @param startTimes the startTimes to set
     */
    public void setStartTimes(List<Long> startTimes) {
        this.startTimes = startTimes;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the subjects
     */
    public List<Long> getBoardIds() {
        return boardIds;
    }

    /**
     * @param subjects the subjects to set
     */
    public void setBoardIds(List<Long> boardIds) {
        this.boardIds = boardIds;
    }

    /**
     * @return the price
     */
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return the isBundle
     */
    public boolean isIsBundle() {
        return isBundle;
    }

    /**
     * @param isBundle the isBundle to set
     */
    public void setIsBundle(boolean isBundle) {
        this.isBundle = isBundle;
    }

    /**
     * @return the entityId
     */
    public String getEntityId() {
        return entityId;
    }

    /**
     * @param entityId the entityId to set
     */
    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    /**
     * @return the firstStartTime
     */
    public Long getFirstStartTime() {
        return firstStartTime;
    }

    /**
     * @param firstStartTime the firstStartTime to set
     */
    public void setFirstStartTime(Long firstStartTime) {
        this.firstStartTime = firstStartTime;
    }

    @Override
    public String toString() {
        return "GetCourseListingRes{" + "startTimes=" + startTimes + ", title=" + title + ", boardIds=" + boardIds + ", firstStartTime=" + firstStartTime + ", price=" + price + ", isBundle=" + isBundle + ", entityId=" + entityId + '}';
    }
    
    

    @Override
    public int compareTo(GetCourseListingRes o) {
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        
        return this.getFirstStartTime().compareTo(o.getFirstStartTime());
    }

    /**
     * @return the targets
     */
    public Set<String> getTargets() {
        return targets;
    }

    /**
     * @param targets the targets to set
     */
    public void setTargets(Set<String> targets) {
        this.targets = targets;
    }

    /**
     * @return the subjects
     */
    public List<String> getSubjects() {
        return subjects;
    }

    /**
     * @param subjects the subjects to set
     */
    public void setSubjects(List<String> subjects) {
        this.subjects = subjects;
    }

    /**
     * @return the duration
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * @return the tags
     */
    public String getTag() {
        return tag;
    }

    /**
     * @param tags the tags to set
     */
    public void setTag(String tag) {
        this.tag = tag;
    }
    
}
