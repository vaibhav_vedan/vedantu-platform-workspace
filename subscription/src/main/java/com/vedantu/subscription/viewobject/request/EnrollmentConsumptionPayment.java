package com.vedantu.subscription.viewobject.request;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.subscription.enums.ConsumptionType;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.enums.EnrollmentTransactionType;

  public class EnrollmentConsumptionPayment {

    private String enrollmentId;
    private String batchId;
    private EnrollmentTransactionContextType enrollmentTransactionContextType;
    private EnrollmentTransactionType enrollmentTransactionType;
    private Long duration;
    private String courseId;
    private EntityStatus consumptionType;
    private boolean customerGrievence;
    private Integer cgAmtP=0;
    private Integer cgAmtNP=0;
    private Integer cgAmtFromDiscount=0;
    private Integer regAmt=0;
    private Integer regAmtP=0;
    private Integer regAmtNP=0;
    private Integer regAmtFromDiscount=0;
    private Integer amt=0;
    private Integer amtP=0;
    private Integer amtNP=0;
    private Integer amtFromDiscount=0;
    private Integer negativeMarginBackAmt=0;
    private boolean negativeMargin;
    private String refundReason;
    private String enrollmentTransactionContextId;

    
    
    public String getEnrollmentTransactionContextId() {
        return enrollmentTransactionContextId;
    }

    public void setEnrollmentTransactionContextId(String enrollmentTransactionContextId) {
        this.enrollmentTransactionContextId = enrollmentTransactionContextId;
    }

    public boolean isNegativeMargin() {
        return negativeMargin;
    }

    public void setNegativeMargin(boolean negativeMargin) {
        this.negativeMargin = negativeMargin;
    }

    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public EnrollmentTransactionContextType getEnrollmentTransactionContextType() {
        return enrollmentTransactionContextType;
    }

    public void setEnrollmentTransactionContextType(EnrollmentTransactionContextType enrollmentTransactionContextType) {
        this.enrollmentTransactionContextType = enrollmentTransactionContextType;
    }

    public EnrollmentTransactionType getEnrollmentTransactionType() {
        return enrollmentTransactionType;
    }

    public void setEnrollmentTransactionType(EnrollmentTransactionType enrollmentTransactionType) {
        this.enrollmentTransactionType = enrollmentTransactionType;
    }

    public Integer getRegAmt() {
        return regAmt;
    }

    public void setRegAmt(Integer regAmt) {
        this.regAmt = regAmt;
    }

    public Integer getRegAmtP() {
        return regAmtP;
    }

    public void setRegAmtP(Integer regAmtP) {
        this.regAmtP = regAmtP;
    }

    public Integer getRegAmtNP() {
        return regAmtNP;
    }

    public void setRegAmtNP(Integer regAmtNP) {
        this.regAmtNP = regAmtNP;
    }

    public Integer getRegAmtFromDiscount() {
        return regAmtFromDiscount;
    }

    public void setRegAmtFromDiscount(Integer regAmtFromDiscount) {
        this.regAmtFromDiscount = regAmtFromDiscount;
    }

    public Integer getAmt() {
        return amt;
    }

    public void setAmt(Integer amt) {
        this.amt = amt;
    }

    public Integer getAmtP() {
        return amtP;
    }

    public void setAmtP(Integer amtP) {
        this.amtP = amtP;
    }

    public Integer getAmtNP() {
        return amtNP;
    }

    public void setAmtNP(Integer amtNP) {
        this.amtNP = amtNP;
    }

    public Integer getAmtFromDiscount() {
        return amtFromDiscount;
    }

    public void setAmtFromDiscount(Integer amtFromDiscount) {
        this.amtFromDiscount = amtFromDiscount;
    }


    /**
     * @return the consumptionType
     */
    public EntityStatus getConsumptionType() {
        return consumptionType;
    }

    /**
     * @param consumptionType the consumptionType to set
     */
    public void setConsumptionType(EntityStatus consumptionType) {
        this.consumptionType = consumptionType;
    }

    @Override
    public String toString() {
        return "EnrollmentConsumptionPayment{" + "enrollmentId=" + enrollmentId + ", batchId=" + batchId + ", enrollmentTransactionContextType=" + enrollmentTransactionContextType + ", enrollmentTransactionType=" + enrollmentTransactionType + ", consumptionType=" + consumptionType + ", regAmt=" + regAmt + ", regAmtP=" + regAmtP + ", regAmtNP=" + regAmtNP + ", regAmtFromDiscount=" + regAmtFromDiscount + ", amt=" + amt + ", amtP=" + amtP + ", amtNP=" + amtNP + ", amtFromDiscount=" + amtFromDiscount + ", negativeMargin=" + negativeMargin + ", enrollmentTransactionContextId=" + enrollmentTransactionContextId + '}';
    }

    /**
     * @return the customerGrievence
     */
    public boolean isCustomerGrievence() {
        return customerGrievence;
    }

    /**
     * @param customerGrievence the customerGrievence to set
     */
    public void setCustomerGrievence(boolean customerGrievence) {
        this.customerGrievence = customerGrievence;
    }

    /**
     * @return the cgAmtP
     */
    public Integer getCgAmtP() {
        return cgAmtP;
    }

    /**
     * @param cgAmtP the cgAmtP to set
     */
    public void setCgAmtP(Integer cgAmtP) {
        this.cgAmtP = cgAmtP;
    }

    /**
     * @return the cgAmtNP
     */
    public Integer getCgAmtNP() {
        return cgAmtNP;
    }

    /**
     * @param cgAmtNP the cgAmtNP to set
     */
    public void setCgAmtNP(Integer cgAmtNP) {
        this.cgAmtNP = cgAmtNP;
    }

    /**
     * @return the duration
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * @return the courseId
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the negativeMarginBackAmt
     */
    public Integer getNegativeMarginBackAmt() {
        return negativeMarginBackAmt;
    }

    /**
     * @param negativeMarginBackAmt the negativeMarginBackAmt to set
     */
    public void setNegativeMarginBackAmt(Integer negativeMarginBackAmt) {
        this.negativeMarginBackAmt = negativeMarginBackAmt;
    }

    /**
     * @return the refundReason
     */
    public String getRefundReason() {
        return refundReason;
    }

    /**
     * @param refundReason the refundReason to set
     */
    public void setRefundReason(String refundReason) {
        this.refundReason = refundReason;
    }

    /**
     * @return the cgAmtFromDiscount
     */
    public Integer getCgAmtFromDiscount() {
        return cgAmtFromDiscount;
    }

    /**
     * @param cgAmtFromDiscount the cgAmtFromDiscount to set
     */
    public void setCgAmtFromDiscount(Integer cgAmtFromDiscount) {
        this.cgAmtFromDiscount = cgAmtFromDiscount;
    }
    
    
}
