package com.vedantu.subscription.viewobject.request;

import com.vedantu.subscription.entities.mongo.BundleTest;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "BundleTest")
public class BundleTestReq    extends AbstractFrontEndReq {

    List<BundleTest> bundleTest;


    public List<BundleTest> getBundleTest() {
        return bundleTest;
    }

    public void setBundleTest(List<BundleTest> bundleTest) {
        this.bundleTest = bundleTest;
    }
}
