package com.vedantu.subscription.viewobject.response;

import com.vedantu.subscription.pojo.VideoBundleContentInfo;
import com.vedantu.subscription.pojo.VideoContentData;

import java.util.List;

public class BundleVideoRes  {
    private VideoContentData children;
    private List<VideoBundleContentInfo> contents;
    private String title;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;

    public VideoContentData getChildren() {
        return children;
    }

    public void setChildren(VideoContentData children) {
        this.children = children;
    }

    public List<VideoBundleContentInfo> getContents() {
        return contents;
    }

    public void setContents(List<VideoBundleContentInfo> contents) {
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
