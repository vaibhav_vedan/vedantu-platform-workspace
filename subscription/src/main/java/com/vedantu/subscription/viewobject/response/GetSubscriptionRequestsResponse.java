package com.vedantu.subscription.viewobject.response;

import java.util.List;

import com.vedantu.subscription.entities.sql.SubscriptionRequest;

public class GetSubscriptionRequestsResponse {

	private Integer count;
	private List<SubscriptionRequest> subscriptionRequestsList;

	public GetSubscriptionRequestsResponse() {
		super();
	}

	public GetSubscriptionRequestsResponse(Integer count, List<SubscriptionRequest> subscriptionRequestsList) {
		super();
		this.count = count;
		this.subscriptionRequestsList = subscriptionRequestsList;
	}

	public Integer getCount() {
		return count;
	}

	public void setCount(Integer count) {
		this.count = count;
	}

	public List<SubscriptionRequest> getSubscriptionRequestsList() {
		return subscriptionRequestsList;
	}

	public void setSubscriptionRequestsList(List<SubscriptionRequest> subscriptionRequestsList) {
		this.subscriptionRequestsList = subscriptionRequestsList;
	}

	@Override
	public String toString() {
		return "GetSubscriptionRequestsResponse [count=" + count + ", subscriptionRequestsList="
				+ subscriptionRequestsList + "]";
	}
}
