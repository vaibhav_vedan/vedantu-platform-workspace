/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author parashar
 */

public class EndSubscriptionReq extends AbstractFrontEndReq {

    private String enrollmentId;
    private boolean checkRefundAmount;
    private String endReason;
    private Long endedTime;

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the checkRefundAmount
     */
    public boolean isCheckRefundAmount() {
        return checkRefundAmount;
    }

    /**
     * @param checkRefundAmount the checkRefundAmount to set
     */
    public void setCheckRefundAmount(boolean checkRefundAmount) {
        this.checkRefundAmount = checkRefundAmount;
    }

    /**
     * @return the endReason
     */
    public String getEndReason() {
        return endReason;
    }

    /**
     * @param endReason the endReason to set
     */
    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public Long getEndedTime() {
        return endedTime;
    }

    public void setEndedTime(Long endedTime) {
        this.endedTime = endedTime;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(enrollmentId)) {
            errors.add("enrollmentId");
        }
        /*if (StringUtils.isEmpty(endReason)) {
            errors.add("endReason");
        }*/
        return errors;
    }

}
