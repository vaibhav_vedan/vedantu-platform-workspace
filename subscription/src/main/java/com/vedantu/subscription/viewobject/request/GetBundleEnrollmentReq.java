package com.vedantu.subscription.viewobject.request;



import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class GetBundleEnrollmentReq extends AbstractFrontEndReq {
    String grade;
    String subject;
    String bundleId;
    private String bundleEnrollmentId;
    private EntityStatus state;
    private String orderId;
    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    public String getGrade() {
        return grade;
    }
 
    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public String getBundleEnrollmentId() {
        return bundleEnrollmentId;
    }

    public void setBundleEnrollmentId(String bundleEnrollmentId) {
        this.bundleEnrollmentId = bundleEnrollmentId;
    }

    public EntityStatus getState() {
        return state;
    }

    public void setState(EntityStatus state) {
        this.state = state;
    }
}

