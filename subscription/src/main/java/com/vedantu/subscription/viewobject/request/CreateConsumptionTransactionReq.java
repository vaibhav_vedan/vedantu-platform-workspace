/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

/**
 *
 * @author parashar
 */
public class CreateConsumptionTransactionReq {
    
    private Long endTime;
    private Integer amtLeft;
    private Integer amtLeftFromDiscount;
    private Integer amtLeftNP;
    private Integer amtLeftP;
    private Integer amtPaid;
    private Integer amtPaidNP;
    private String enrollmentId;
    private Long enrollmentConsumptionId;
    private String enrollmentTransactionContextId;
    private String remarks;

    /**
     * @return the endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the amtLeft
     */
    public Integer getAmtLeft() {
        return amtLeft;
    }

    /**
     * @param amtLeft the amtLeft to set
     */
    public void setAmtLeft(Integer amtLeft) {
        this.amtLeft = amtLeft;
    }

    /**
     * @return the amtLeftFromDiscount
     */
    public Integer getAmtLeftFromDiscount() {
        return amtLeftFromDiscount;
    }

    /**
     * @param amtLeftFromDiscount the amtLeftFromDiscount to set
     */
    public void setAmtLeftFromDiscount(Integer amtLeftFromDiscount) {
        this.amtLeftFromDiscount = amtLeftFromDiscount;
    }

    /**
     * @return the amtLeftNP
     */
    public Integer getAmtLeftNP() {
        return amtLeftNP;
    }

    /**
     * @param amtLeftNP the amtLeftNP to set
     */
    public void setAmtLeftNP(Integer amtLeftNP) {
        this.amtLeftNP = amtLeftNP;
    }

    /**
     * @return the amtLeftP
     */
    public Integer getAmtLeftP() {
        return amtLeftP;
    }

    /**
     * @param amtLeftP the amtLeftP to set
     */
    public void setAmtLeftP(Integer amtLeftP) {
        this.amtLeftP = amtLeftP;
    }

    /**
     * @return the amtPaid
     */
    public Integer getAmtPaid() {
        return amtPaid;
    }

    /**
     * @param amtPaid the amtPaid to set
     */
    public void setAmtPaid(Integer amtPaid) {
        this.amtPaid = amtPaid;
    }

    /**
     * @return the amtPaidNP
     */
    public Integer getAmtPaidNP() {
        return amtPaidNP;
    }

    /**
     * @param amtPaidNP the amtPaidNP to set
     */
    public void setAmtPaidNP(Integer amtPaidNP) {
        this.amtPaidNP = amtPaidNP;
    }

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the enrollmentConsumptionId
     */
    public Long getEnrollmentConsumptionId() {
        return enrollmentConsumptionId;
    }

    /**
     * @param enrollmentConsumptionId the enrollmentConsumptionId to set
     */
    public void setEnrollmentConsumptionId(Long enrollmentConsumptionId) {
        this.enrollmentConsumptionId = enrollmentConsumptionId;
    }

    /**
     * @return the enrollmentTransactionContextId
     */
    public String getEnrollmentTransactionContextId() {
        return enrollmentTransactionContextId;
    }

    /**
     * @param enrollmentTransactionContextId the enrollmentTransactionContextId to set
     */
    public void setEnrollmentTransactionContextId(String enrollmentTransactionContextId) {
        this.enrollmentTransactionContextId = enrollmentTransactionContextId;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
    
}
