package com.vedantu.subscription.viewobject.response;

import com.vedantu.onetofew.pojo.CourseInfo;
import java.awt.List;

public class GetCourseRes extends AbstractListRes<CourseInfo> {
	private int totalCount;

	public GetCourseRes() {
		super();
	}

	public GetCourseRes(List data, int totalCount) {
		super();
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}
}
