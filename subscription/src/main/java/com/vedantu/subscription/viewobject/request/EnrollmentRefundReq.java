/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author parashar
 */
public class EnrollmentRefundReq extends AbstractFrontEndReq {
 
    private String enrollmentId;
    private Long endedTime;
    private String reasonNote;

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the endedTime
     */
    public Long getEndedTime() {
        return endedTime;
    }

    /**
     * @param endedTime the endedTime to set
     */
    public void setEndedTime(Long endedTime) {
        this.endedTime = endedTime;
    }

    /**
     * @return the reasonNote
     */
    public String getReasonNote() {
        return reasonNote;
    }

    /**
     * @param reasonNote the reasonNote to set
     */
    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }
    
    
    
}
