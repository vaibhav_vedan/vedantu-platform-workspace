/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author pranavm
 */
public class GetRegistrationsReq extends AbstractFrontEndReq{
    
    private Long userId;
    private EntityType entityType;
    private RegistrationStatus status;
    private String entityId;
    private Boolean fillEntityInfo = Boolean.TRUE;
    private List<String> entityIdList;
    
    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public RegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(RegistrationStatus status) {
        this.status = status;
    }

    public Boolean getFillEntityInfo() {
        return fillEntityInfo;
    }

    public void setFillEntityInfo(Boolean fillEntityInfo) {
        this.fillEntityInfo = fillEntityInfo;
    }

    public List<String> getEntityIdList() {
        return entityIdList;
    }

    public void setEntityIdList(List<String> entityIdList) {
        this.entityIdList = entityIdList;
    }
}
