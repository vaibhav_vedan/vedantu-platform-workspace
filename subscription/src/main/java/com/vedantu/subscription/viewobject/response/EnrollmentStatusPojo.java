package com.vedantu.subscription.viewobject.response;

import com.vedantu.onetofew.enums.EntityStatus;


public class EnrollmentStatusPojo {
	private String userId;
	private EntityStatus status;

	public EnrollmentStatusPojo() {
		super();
	}

	public EnrollmentStatusPojo(String userId, EntityStatus status) {
		super();
		this.userId = userId;
		this.status = status;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}
}
