/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

/**
 *
 * @author parashar
 */
public class BundleRefundReq extends AbstractFrontEndReq {
    
    private String bundleId;
    private Long userId;
    private Long endedTime;
    private String reasonNote;
    

    /**
     * @return the bundleId
     */
    public String getBundleId() {
        return bundleId;
    }

    /**
     * @param bundleId the bundleId to set
     */
    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    /**
     * @return the userId
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * @return the endedTime
     */
    public Long getEndedTime() {
        return endedTime;
    }

    /**
     * @param endedTime the endedTime to set
     */
    public void setEndedTime(Long endedTime) {
        this.endedTime = endedTime;
    }

    /**
     * @return the reasonNote
     */
    public String getReasonNote() {
        return reasonNote;
    }

    /**
     * @param reasonNote the reasonNote to set
     */
    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }
    
    
    
}
