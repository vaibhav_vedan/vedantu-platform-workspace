package com.vedantu.subscription.viewobject.response.amdashboard;

import java.util.List;
import java.util.Set;

import com.vedantu.listing.pojo.EngagementType;
import com.vedantu.lms.cmds.enums.ContentInfoType;
import com.vedantu.lms.cmds.enums.ContentState;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.LMSType;
import com.vedantu.lms.cmds.enums.UserType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.response.AbstractRes;

public class AMDashboardContentInfoRes extends AbstractRes{

    private UserType userType;
    private String studentId;
    private String studentFullName;
    private String teacherId;
    private String teacherFullName;
    private String contentTitle;
    private String courseName;
    private String subject;
    private Set<String> subjects;
    private String courseId;
    private Long expiryTime;

    private ContentType contentType;
    private ContentInfoType contentInfoType;
    private EngagementType engagementType;
    private ContentState contentState;
    private LMSType lmsType;

    private String contentLink;
    private String studentActionLink;
    private String teacherActionLink;

    private EntityType contextType;

    private String contextId;

    private List<String> tags;

    private AMDashboardTestContentInfoMetadata metadata;

    public AMDashboardContentInfoRes() {
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getStudentFullName() {
        return studentFullName;
    }

    public void setStudentFullName(String studentFullName) {
        this.studentFullName = studentFullName;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherFullName() {
        return teacherFullName;
    }

    public void setTeacherFullName(String teacherFullName) {
        this.teacherFullName = teacherFullName;
    }

    public String getContentTitle() {
        return contentTitle;
    }

    public void setContentTitle(String contentTitle) {
        this.contentTitle = contentTitle;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public Set<String> getSubjects() {
        return subjects;
    }

    public void setSubjects(Set<String> subjects) {
        this.subjects = subjects;
    }

    public String getCourseId() {
        return courseId;
    }

    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    public Long getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Long expiryTime) {
        this.expiryTime = expiryTime;
    }

    public ContentType getContentType() {
        return contentType;
    }

    public void setContentType(ContentType contentType) {
        this.contentType = contentType;
    }

    public ContentInfoType getContentInfoType() {
        return contentInfoType;
    }

    public void setContentInfoType(ContentInfoType contentInfoType) {
        this.contentInfoType = contentInfoType;
    }

    public EngagementType getEngagementType() {
        return engagementType;
    }

    public void setEngagementType(EngagementType engagementType) {
        this.engagementType = engagementType;
    }

    public ContentState getContentState() {
        return contentState;
    }

    public void setContentState(ContentState contentState) {
        this.contentState = contentState;
    }

    public LMSType getLmsType() {
        return lmsType;
    }

    public void setLmsType(LMSType lmsType) {
        this.lmsType = lmsType;
    }

    public String getContentLink() {
        return contentLink;
    }

    public void setContentLink(String contentLink) {
        this.contentLink = contentLink;
    }

    public String getStudentActionLink() {
        return studentActionLink;
    }

    public void setStudentActionLink(String studentActionLink) {
        this.studentActionLink = studentActionLink;
    }

    public String getTeacherActionLink() {
        return teacherActionLink;
    }

    public void setTeacherActionLink(String teacherActionLink) {
        this.teacherActionLink = teacherActionLink;
    }

    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public AMDashboardTestContentInfoMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(AMDashboardTestContentInfoMetadata metadata) {
        this.metadata = metadata;
    }

}
