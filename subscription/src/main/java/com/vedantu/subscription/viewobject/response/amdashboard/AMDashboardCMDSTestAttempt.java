package com.vedantu.subscription.viewobject.response.amdashboard;

import java.util.List;

import com.vedantu.lms.cmds.pojo.CategoryAnalytics;

public class AMDashboardCMDSTestAttempt {
	
	private List<CategoryAnalytics> categoryAnalyticsList;

	public AMDashboardCMDSTestAttempt() {
		super();
	}

	public List<CategoryAnalytics> getCategoryAnalyticsList() {
		return categoryAnalyticsList;
	}

	public void setCategoryAnalyticsList(List<CategoryAnalytics> categoryAnalyticsList) {
		this.categoryAnalyticsList = categoryAnalyticsList;
	}

}
