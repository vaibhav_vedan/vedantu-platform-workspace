package com.vedantu.subscription.viewobject.response;

import com.vedantu.subscription.pojo.OrderedItemNames;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderedItemNameDetailsRes {
    private List<OrderedItemNames> orderedItemNames;
}
