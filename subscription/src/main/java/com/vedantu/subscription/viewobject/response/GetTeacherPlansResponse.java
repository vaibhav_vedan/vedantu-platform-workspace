package com.vedantu.subscription.viewobject.response;

import com.vedantu.dinero.pojo.TeacherSlabPlan;
import java.util.List;

public class GetTeacherPlansResponse {

	private List<TeacherSlabPlan> plans;

	public List<TeacherSlabPlan> getPlans() {
		return plans;
	}

	public void setPlans(List<TeacherSlabPlan> plans) {
		this.plans = plans;
	}

	@Override
	public String toString() {
		return "GetTeacherPlansResponse [plans=" + plans + "]";
	}

	public GetTeacherPlansResponse(List<TeacherSlabPlan> plans) {
		super();
		this.plans = plans;
	}

	public GetTeacherPlansResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

}
