package com.vedantu.subscription.viewobject.request;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.BatchStatus;
import com.vedantu.onetofew.pojo.SessionPlanPojo;
import lombok.Data;

import java.util.List;

/**
 * @author Darshit
 */
@Data
public class SubscriptionCoursesRes {
    private String id;
    private String courseId;
    private String bundleId;
    private List<SessionPlanPojo> sessionPlan;
    private String title;
    private List<String> subjects;
    private Long startDate;
    private Long endDate;
    private List<UserBasicInfo> teachers;
    private Boolean enrolled;
    private Boolean recordedVideo;
    private BatchStatus batchStatus;

    public SubscriptionCoursesRes(String id, String courseId, String bundleId, List<SessionPlanPojo> sessionPlan, String title, List<String> subjects, Long startDate, Long endDate, List<UserBasicInfo> teachers, Boolean enrolled, Boolean recordedVideo, BatchStatus batchStatus) {
        this.id = id;
        this.courseId = courseId;
        this.bundleId = bundleId;
        this.sessionPlan = sessionPlan;
        this.title = title;
        this.subjects = subjects;
        this.startDate = startDate;
        this.endDate = endDate;
        this.teachers = teachers;
        this.enrolled = enrolled;
        this.recordedVideo = recordedVideo;
        this.batchStatus = batchStatus;
    }
}
