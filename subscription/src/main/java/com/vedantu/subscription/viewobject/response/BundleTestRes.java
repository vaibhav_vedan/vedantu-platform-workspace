package com.vedantu.subscription.viewobject.response;

import com.vedantu.subscription.pojo.TestBundleContentInfo;
import com.vedantu.subscription.pojo.TestContentData;

import java.util.List;

public class BundleTestRes {
    private TestContentData children;
    private List<TestBundleContentInfo>  contents;
    private String title;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;

    public TestContentData getChildren() {
        return children;
    }

    public void setChildren(TestContentData children) {
        this.children = children;
    }

    public List<TestBundleContentInfo> getContents() {
        return contents;
    }

    public void setContents(List<TestBundleContentInfo> contents) {
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
