/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.util.fos.request.AbstractReq;

/**
 *
 * @author parashar
 */
public class MakePaymentRequest extends AbstractReq{
    private String enrollmentId;
    private EnrollmentTransactionContextType enrollmentTransactionContextType;
    private String enrollmentTransactionContextId;
    private Long duration = 0l;
    private String batchId;
    private Integer amt = 0;
    private Integer amtP = 0;
    private Integer amtNP = 0;
    private Integer amtFromDiscount = 0;
    private Integer amtToBePaid;
    private String courseId;
    private String orderId;
    private boolean createConsumptionIfnotFound=true;
    
    

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the enrollmentTransactionContextType
     */
    public EnrollmentTransactionContextType getEnrollmentTransactionContextType() {
        return enrollmentTransactionContextType;
    }

    /**
     * @param enrollmentTransactionContextType the enrollmentTransactionContextType to set
     */
    public void setEnrollmentTransactionContextType(EnrollmentTransactionContextType enrollmentTransactionContextType) {
        this.enrollmentTransactionContextType = enrollmentTransactionContextType;
    }

    /**
     * @return the enrollmentTransactionContextId
     */
    public String getEnrollmentTransactionContextId() {
        return enrollmentTransactionContextId;
    }

    /**
     * @param enrollmentTransactionContextId the enrollmentTransactionContextId to set
     */
    public void setEnrollmentTransactionContextId(String enrollmentTransactionContextId) {
        this.enrollmentTransactionContextId = enrollmentTransactionContextId;
    }

    /**
     * @return the batchId
     */
    public String getBatchId() {
        return batchId;
    }

    /**
     * @param batchId the batchId to set
     */
    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    /**
     * @return the amt
     */
    public Integer getAmt() {
        return amt;
    }

    /**
     * @param amt the amt to set
     */
    public void setAmt(Integer amt) {
        this.amt = amt;
    }

    /**
     * @return the amtP
     */
    public Integer getAmtP() {
        return amtP;
    }

    /**
     * @param amtP the amtP to set
     */
    public void setAmtP(Integer amtP) {
        this.amtP = amtP;
    }

    /**
     * @return the amtNP
     */
    public Integer getAmtNP() {
        return amtNP;
    }

    /**
     * @param amtNP the amtNP to set
     */
    public void setAmtNP(Integer amtNP) {
        this.amtNP = amtNP;
    }

    /**
     * @return the amtFromDiscount
     */
    public Integer getAmtFromDiscount() {
        return amtFromDiscount;
    }

    /**
     * @param amtFromDiscount the amtFromDiscount to set
     */
    public void setAmtFromDiscount(Integer amtFromDiscount) {
        this.amtFromDiscount = amtFromDiscount;
    }

    /**
     * @return the amtToBePaid
     */
    public Integer getAmtToBePaid() {
        return amtToBePaid;
    }

    /**
     * @param amtToBePaid the amtToBePaid to set
     */
    public void setAmtToBePaid(Integer amtToBePaid) {
        this.amtToBePaid = amtToBePaid;
    }

    /**
     * @return the courseId
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the duration
     */
    public Long getDuration() {
        return duration;
    }

    /**
     * @param duration the duration to set
     */
    public void setDuration(Long duration) {
        this.duration = duration;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public boolean isCreateConsumptionIfnotFound() {
        return createConsumptionIfnotFound;
    }

    public void setCreateConsumptionIfnotFound(boolean createConsumptionIfnotFound) {
        this.createConsumptionIfnotFound = createConsumptionIfnotFound;
    }
}
