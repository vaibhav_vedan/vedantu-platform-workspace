package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UpdateSubscriptionHoursRequest extends AbstractFrontEndReq{

	private Long subscriptionId;
	private Boolean addition;
	private Long hours;

	public UpdateSubscriptionHoursRequest() {
		super();
	}

	public UpdateSubscriptionHoursRequest(Long subscriptionId, Boolean addition, Long hours) {
		super();
		this.subscriptionId = subscriptionId;
		this.addition = addition;
		this.hours = hours;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Boolean getAddition() {
		return addition;
	}

	public void setAddition(Boolean addition) {
		this.addition = addition;
	}

	public Long getHours() {
		return hours;
	}

	public void setHours(Long hours) {
		this.hours = hours;
	}
}
