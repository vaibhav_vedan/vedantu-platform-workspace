package com.vedantu.subscription.viewobject.response.amdashboard;

import java.util.List;

public class AMDashboardTestContentInfoMetadata {

    private String testId;
    private Long duration;
    private Float totalMarks;
    private Integer noOfQuestions;

    private Integer rank;
    private Float percentage;
    private Float percentile;

    private Long minStartTime;
    private Long maxStartTime;
    private boolean reattemptAllowed;
    private String displayMessageOnEnd;
    
    List<AMDashboardCMDSTestAttempt> testAttempts;


    public String getTestId() {
        return testId;
    }

    public void setTestId(String testId) {
        this.testId = testId;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Float getTotalMarks() {
        return totalMarks;
    }

    public void setTotalMarks(Float totalMarks) {
        this.totalMarks = totalMarks;
    }

    public Integer getNoOfQuestions() {
        return noOfQuestions;
    }

    public void setNoOfQuestions(Integer noOfQuestions) {
        this.noOfQuestions = noOfQuestions;
    }

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Float getPercentage() {
        return percentage;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }

    public Float getPercentile() {
        return percentile;
    }

    public void setPercentile(Float percentile) {
        this.percentile = percentile;
    }

    public Long getMinStartTime() {
        return minStartTime;
    }

    public void setMinStartTime(Long minStartTime) {
        this.minStartTime = minStartTime;
    }

    public Long getMaxStartTime() {
        return maxStartTime;
    }

    public void setMaxStartTime(Long maxStartTime) {
        this.maxStartTime = maxStartTime;
    }

    public boolean isReattemptAllowed() {
        return reattemptAllowed;
    }

    public void setReattemptAllowed(boolean reattemptAllowed) {
        this.reattemptAllowed = reattemptAllowed;
    }

    public String getDisplayMessageOnEnd() {
        return displayMessageOnEnd;
    }

    public void setDisplayMessageOnEnd(String displayMessageOnEnd) {
        this.displayMessageOnEnd = displayMessageOnEnd;
    }

	public List<AMDashboardCMDSTestAttempt> getTestAttempts() {
		return testAttempts;
	}

	public void setTestAttempts(List<AMDashboardCMDSTestAttempt> testAttempts) {
		this.testAttempts = testAttempts;
	}
}
