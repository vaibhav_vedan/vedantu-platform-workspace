package com.vedantu.subscription.viewobject.response;

import com.vedantu.onetofew.pojo.BatchInfo;

public class GetBatchRes extends AbstractListRes<BatchInfo> {
	private int totalCount;

	public GetBatchRes() {
		super();
	}

	public GetBatchRes(int totalCount) {
		super();
		this.totalCount = totalCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}	
}
