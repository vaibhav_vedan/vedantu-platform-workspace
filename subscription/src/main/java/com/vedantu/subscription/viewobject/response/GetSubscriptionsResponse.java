package com.vedantu.subscription.viewobject.response;

import java.util.List;

public class GetSubscriptionsResponse {
	
	private List<GetSubscriptionsRes> subscriptionsList;

	public GetSubscriptionsResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetSubscriptionsResponse(List<GetSubscriptionsRes> subscriptionsList) {
		super();
		this.subscriptionsList = subscriptionsList;
	}

	public List<GetSubscriptionsRes> getSubscriptionsList() {
		return subscriptionsList;
	}

	public void setSubscriptionsList(List<GetSubscriptionsRes> subscriptionsList) {
		this.subscriptionsList = subscriptionsList;
	}

	@Override
	public String toString() {
		return "GetSubscriptionsResponse [subscriptionsList=" + subscriptionsList + "]";
	}
}
