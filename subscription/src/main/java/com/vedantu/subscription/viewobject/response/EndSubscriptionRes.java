/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.util.PlatformBasicResponse;

/**
 *
 * @author parashar
 */
public class EndSubscriptionRes extends PlatformBasicResponse{
    
    private Integer promotionalAmount;
    private Integer nonPromotionalAmount;

    /**
     * @return the promotionalAmount
     */
    public Integer getPromotionalAmount() {
        return promotionalAmount;
    }

    /**
     * @param promotionalAmount the promotionalAmount to set
     */
    public void setPromotionalAmount(Integer promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    /**
     * @return the nonPromotionalAmount
     */
    public Integer getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    /**
     * @param nonPromotionalAmount the nonPromotionalAmount to set
     */
    public void setNonPromotionalAmount(Integer nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }
    
    
    
}
