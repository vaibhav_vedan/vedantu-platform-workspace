/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.fos.request.ReqLimits;
import com.vedantu.util.fos.request.ReqLimitsErMsgs;
import java.util.List;
import javax.validation.constraints.Size;

/**
 *
 * @author pranavm
 */
public class BatchChangeReq extends AbstractFrontEndReq {

    private String email;
    private Long userId;
    private String previousBatchId;
    private String newBatchId;

    @Size(max = ReqLimits.REASON_TYPE_MAX, message = ReqLimitsErMsgs.MAX_REASON_TYPE)
    private String reason;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getPreviousBatchId() {
        return previousBatchId;
    }

    public void setPreviousBatchId(String previousBatchId) {
        this.previousBatchId = previousBatchId;
    }

    public String getNewBatchId() {
        return newBatchId;
    }

    public void setNewBatchId(String newBatchId) {
        this.newBatchId = newBatchId;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    @Override
    public String toString() {
        return "BatchChangeReq{" + "email=" + email + ", userId=" + userId + ", previousBatchId=" + previousBatchId + ", newBatchId=" + newBatchId + ", reason=" + reason + '}';
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(email) && userId == null) {
            errors.add("email/userId");
        }
        if (StringUtils.isEmpty(newBatchId)) {
            errors.add("newBatchId");
        }
        if (StringUtils.isEmpty(previousBatchId)) {
            errors.add("previousBatchId");
        }
        if(ArrayUtils.isEmpty(errors)){
            if(newBatchId.equals(previousBatchId)){
                errors.add("previousBatchId & newBatchId is the same");
            }
        }
        return errors;
    }
}
