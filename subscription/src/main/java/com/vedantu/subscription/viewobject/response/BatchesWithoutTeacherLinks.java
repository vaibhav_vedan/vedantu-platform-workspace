package com.vedantu.subscription.viewobject.response;

import java.util.List;

import com.vedantu.subscription.entities.mongo.Batch;


public class BatchesWithoutTeacherLinks {
	private List<Batch> notExistsOnGTM;

	public BatchesWithoutTeacherLinks(List<Batch> notExistsOnGTM) {
		super();
		this.notExistsOnGTM = notExistsOnGTM;
	}
	
	public List<Batch> getNotExistsOnGTM() {
		return notExistsOnGTM;
	}
	public void setNotExistsOnGTM(List<Batch> notExistsOnGTM) {
		this.notExistsOnGTM = notExistsOnGTM;
	}

}
