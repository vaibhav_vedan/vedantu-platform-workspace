package com.vedantu.subscription.viewobject.response.amdashboard;

import java.util.List;

import com.vedantu.User.User;
import com.vedantu.scheduling.pojo.AMInclassAggregateResp;
import com.vedantu.subscription.viewobject.response.amdashboard.AMDashboardContentInfoRes;
import com.vedantu.util.fos.response.AbstractRes;

public class AMDashboardStudentData extends AbstractRes {

    private User user;
    private AMInclassAggregateResp inClass;
    private List<AMDashboardContentInfoRes> assignments;
    private List<AMDashboardContentInfoRes> tests;

    public AMDashboardStudentData() {
    }

    public AMDashboardStudentData(User user, AMInclassAggregateResp inClass, List<AMDashboardContentInfoRes> a, List<AMDashboardContentInfoRes> t) {
        this.user = user;
        this.inClass = inClass;
        this.assignments = a;
        this.tests = t;
    }

    public AMInclassAggregateResp getInClass() {
		return inClass;
	}

	public void setInClass(AMInclassAggregateResp inClass) {
		this.inClass = inClass;
	}

	public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<AMDashboardContentInfoRes> getAssignments() {
        return assignments;
    }

    public void setAssignments(List<AMDashboardContentInfoRes> assignments) {
        this.assignments = assignments;
    }

    public List<AMDashboardContentInfoRes> getTests() {
        return tests;
    }

    public void setTests(List<AMDashboardContentInfoRes> tests) {
        this.tests = tests;
    }

}
