package com.vedantu.subscription.viewobject.request;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.pojo.SessionPlanPojo;
import com.vedantu.subscription.entities.mongo.Bundle;
import lombok.Data;

import java.util.List;

/**
 * @author Darshit
 */
@Data
public class SubscriptionBundleRes {
    private List<Integer> grade;
    private List<String> subjects;
    private String bundleId;

    public SubscriptionBundleRes(List<Integer> grade, List<String> subjects, String bundleId) {
        this.grade = grade;
        this.subjects = subjects;
        this.bundleId = bundleId;
    }

    public SubscriptionBundleRes(Bundle bundle) {
        this.grade = bundle.getGrade();
        this.subjects = bundle.getSubject();
        this.bundleId = bundle.getId();
    }
}
