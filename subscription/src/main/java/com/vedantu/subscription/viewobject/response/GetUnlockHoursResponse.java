package com.vedantu.subscription.viewobject.response;

import java.util.List;

public class GetUnlockHoursResponse {

	private List<UnlockHoursResponse> sessionHoursList;

	public GetUnlockHoursResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetUnlockHoursResponse(List<UnlockHoursResponse> sessionHoursList) {
		super();
		this.sessionHoursList = sessionHoursList;
	}

	public List<UnlockHoursResponse> getSessionHoursList() {
		return sessionHoursList;
	}

	public void setSessionHoursList(List<UnlockHoursResponse> sessionHoursList) {
		this.sessionHoursList = sessionHoursList;
	}

	@Override
	public String toString() {
		return "GetUnlockHoursResponse [sessionHoursList=" + sessionHoursList + "]";
	}
}
