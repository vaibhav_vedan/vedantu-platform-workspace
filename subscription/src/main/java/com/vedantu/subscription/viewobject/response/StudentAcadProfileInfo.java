/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.User.User;

/**
 *
 * @author parashar
 */
public class StudentAcadProfileInfo extends User {
    
    private Integer enrollmentsForOTO;
    private Integer enrollmentsForOTM;

    /**
     * @return the enrollmentsForOTO
     */
    public Integer getEnrollmentsForOTO() {
        return enrollmentsForOTO;
    }

    /**
     * @param enrollmentsForOTO the enrollmentsForOTO to set
     */
    public void setEnrollmentsForOTO(Integer enrollmentsForOTO) {
        this.enrollmentsForOTO = enrollmentsForOTO;
    }

    /**
     * @return the enrollmentsForOTM
     */
    public Integer getEnrollmentsForOTM() {
        return enrollmentsForOTM;
    }

    /**
     * @param enrollmentsForOTM the enrollmentsForOTM to set
     */
    public void setEnrollmentsForOTM(Integer enrollmentsForOTM) {
        this.enrollmentsForOTM = enrollmentsForOTM;
    }
    
}
