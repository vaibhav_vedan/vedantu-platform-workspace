/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.subscription.entities.mongo.OTMBundle;
import java.util.List;

/**
 *
 * @author parashar
 */
public class AggregatedOTMBundles {
    private List<OTMBundle> bundles;
    private String _id;
    private Long minStartTime;
    private List<Long> startTimes;

    /**
     * @return the bundles
     */
    public List<OTMBundle> getBundles() {
        return bundles;
    }

    /**
     * @param bundles the bundles to set
     */
    public void setBundles(List<OTMBundle> bundles) {
        this.bundles = bundles;
    }

    /**
     * @return the _id
     */
    public String getId() {
        return _id;
    }

    /**
     * @param _id the _id to set
     */
    public void setId(String _id) {
        this._id = _id;
    }

    /**
     * @return the minStartTime
     */
    public Long getMinStartTime() {
        return minStartTime;
    }

    /**
     * @param minStartTime the minStartTime to set
     */
    public void setMinStartTime(Long minStartTime) {
        this.minStartTime = minStartTime;
    }

    /**
     * @return the startTimes
     */
    public List<Long> getStartTimes() {
        return startTimes;
    }

    /**
     * @param startTimes the startTimes to set
     */
    public void setStartTimes(List<Long> startTimes) {
        this.startTimes = startTimes;
    }
    
}
