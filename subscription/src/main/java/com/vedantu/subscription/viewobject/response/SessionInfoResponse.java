package com.vedantu.subscription.viewobject.response;

import java.util.List;

import com.vedantu.User.UserInfo;
import com.vedantu.board.pojo.BoardInfoRes;
import com.vedantu.scheduling.pojo.session.SessionInfo;

public class SessionInfoResponse {

	private UserInfo teacher;
	private UserInfo student;
	private List<SessionInfo> sessionList;
	private BoardInfoRes board;

	public SessionInfoResponse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public SessionInfoResponse(UserInfo teacher, UserInfo student, List<SessionInfo> sessionList, BoardInfoRes board) {
		super();
		this.teacher = teacher;
		this.student = student;
		this.sessionList = sessionList;
		this.board = board;
	}

	public UserInfo getTeacher() {
		return teacher;
	}

	public void setTeacher(UserInfo teacher) {
		this.teacher = teacher;
	}

	public UserInfo getStudent() {
		return student;
	}

	public void setStudent(UserInfo student) {
		this.student = student;
	}

	public List<SessionInfo> getSessionList() {
		return sessionList;
	}

	public void setSessionList(List<SessionInfo> sessionList) {
		this.sessionList = sessionList;
	}

	public BoardInfoRes getBoard() {
		return board;
	}

	public void setBoard(BoardInfoRes board) {
		this.board = board;
	}

	@Override
	public String toString() {
		return "SessionInfoResponse [teacher=" + teacher + ", student=" + student + ", sessionList=" + sessionList
				+ ", board=" + board + "]";
	}
}
