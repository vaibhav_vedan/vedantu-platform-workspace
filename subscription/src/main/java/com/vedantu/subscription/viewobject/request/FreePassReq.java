package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
public class FreePassReq extends AbstractFrontEndReq {
   private String reasonNoteType;
    private String reasonNote;
    private String refNo;
    private Long userId;
    private String bundleId;
    private int amount;
    private String coupenId;
    private int freePassValidDays;
    private int start;


    public String getReasonNoteType() {
        return reasonNoteType;
    }

    public void setReasonNoteType(String reasonNoteType) {
        this.reasonNoteType = reasonNoteType;
    }

    public String getRefNo() {
        return refNo;
    }

    public void setRefNo(String refNo) {
        this.refNo = refNo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getBundleId() {
        return bundleId;
    }

    public void setBundleId(String bundleId) {
        this.bundleId = bundleId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getCoupenId() {
        return coupenId;
    }

    public void setCoupenId(String coupenId) {
        this.coupenId = coupenId;
    }

    public int getFreePassValidDays() {
        return freePassValidDays;
    }

    public void setFreePassValidDays(int freePassValidDays) {
        this.freePassValidDays = freePassValidDays;
    }

    public String getReasonNote() {
        return reasonNote;
    }

    public int getStart() {
        return start;
    }

    public void setStart(int start) {
        this.start = start;
    }

    public void setReasonNote(String reasonNote) {
        this.reasonNote = reasonNote;
    }
    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(userId == null){
            errors.add("Invalid user ID");
        }

        if(StringUtils.isEmpty(bundleId)){
            errors.add("Invalid bundle ID");
        }

        if(freePassValidDays  <= 0){
            errors.add("Invalid bundle Valid Days");
        }
        return errors;
    }

	public FreePassReq(String reasonNoteType, String reasonNote, String refNo, Long userId, String bundleId, int amount,
			String coupenId, int freePassValidDays, int start) {
		super();
		this.reasonNoteType = reasonNoteType;
		this.reasonNote = reasonNote;
		this.refNo = refNo;
		this.userId = userId;
		this.bundleId = bundleId;
		this.amount = amount;
		this.coupenId = coupenId;
		this.freePassValidDays = freePassValidDays;
		this.start = start;
	}
}
