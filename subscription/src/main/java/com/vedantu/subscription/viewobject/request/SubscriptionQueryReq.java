package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

@Data
public class SubscriptionQueryReq extends AbstractFrontEndReq {

    private String emailId;
    private String message;
    private String phoneNumber;


}