/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.scheduling.pojo.UserSessionAttendance;
import java.util.List;

/**
 *
 * @author parashar
 */
public class StudentAcademicOTMInfo {

    private String emailId;
    private String groupName;
    private List<StudentAcademicOneCycleInfo> cycles;

    /**
     * @return the emailId
     */
    public String getEmailId() {
        return emailId;
    }

    /**
     * @param emailId the emailId to set
     */
    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the cycles
     */
    public List<StudentAcademicOneCycleInfo> getCycles() {
        return cycles;
    }

    /**
     * @param cycles the cycles to set
     */
    public void setCycles(List<StudentAcademicOneCycleInfo> cycles) {
        this.cycles = cycles;
    }
    
}
