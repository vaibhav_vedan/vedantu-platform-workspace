package com.vedantu.subscription.viewobject.response;

import java.util.List;

import com.vedantu.subscription.entities.mongo.Batch;


public class BatchesWithoutStudentLinks {
	private List<Batch> notExistsOnDb;
	private List<Batch> notExistsOnGTM;
	
	
	
	public BatchesWithoutStudentLinks(List<Batch> notExistsOnDb, List<Batch> notExistsOnGTM) {
		super();
		this.notExistsOnDb = notExistsOnDb;
		this.notExistsOnGTM = notExistsOnGTM;
	}
	
	public List<Batch> getNotExistsOnDb() {
		return notExistsOnDb;
	}
	public void setNotExistsOnDb(List<Batch> notExistsOnDb) {
		this.notExistsOnDb = notExistsOnDb;
	}
	public List<Batch> getNotExistsOnGTM() {
		return notExistsOnGTM;
	}
	public void setNotExistsOnGTM(List<Batch> notExistsOnGTM) {
		this.notExistsOnGTM = notExistsOnGTM;
	}
}
