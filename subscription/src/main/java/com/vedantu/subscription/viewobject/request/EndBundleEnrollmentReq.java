/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import com.vedantu.util.request.OrderEndType;

import java.util.List;

/**
 *
 * @author ajith
 */
public class EndBundleEnrollmentReq extends AbstractFrontEndReq {

    private String bundleEnrollmentId;
    private String endReason;
    private int promotionalAmount ;
    private int nonPromotionalAmount ;
    private int amtToRefundToDiscountWallet;
    private OrderEndType orderEndType;
    private Long actualRefundDate;

    public String getBundleEnrollmentId() {
        return bundleEnrollmentId;
    }

    public void setBundleEnrollmentId(String bundleEnrollmentId) {
        this.bundleEnrollmentId = bundleEnrollmentId;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public int getPromotionalAmount() {
        return promotionalAmount;
    }

    public void setPromotionalAmount(int promotionalAmount) {
        this.promotionalAmount = promotionalAmount;
    }

    public int getNonPromotionalAmount() {
        return nonPromotionalAmount;
    }

    public void setNonPromotionalAmount(int nonPromotionalAmount) {
        this.nonPromotionalAmount = nonPromotionalAmount;
    }

    public int getAmtToRefundToDiscountWallet() {
        return amtToRefundToDiscountWallet;
    }

    public void setAmtToRefundToDiscountWallet(int amtToRefundToDiscountWallet) {
        this.amtToRefundToDiscountWallet = amtToRefundToDiscountWallet;
    }

    public OrderEndType getOrderEndType() {
        return orderEndType;
    }

    public void setOrderEndType(OrderEndType orderEndType) {
        this.orderEndType = orderEndType;
    }

    public Long getActualRefundDate() {
        return actualRefundDate;
    }
    public void setActualRefundDate(Long actualRefundDate) {
        this.actualRefundDate = actualRefundDate;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (StringUtils.isEmpty(bundleEnrollmentId)) {
            errors.add("bundleEnrollmentId");
        }
        if(amtToRefundToDiscountWallet < 0){
            errors.add("amtToRefundToDiscountWallet should be greater than are equal to 0 ");
        }
        if(nonPromotionalAmount < 0){
            errors.add("nonPromotionalAmount should be greater than are equal to 0 ");
        }
        if(promotionalAmount < 0) {
            errors.add("promotionalAmount should be greater than are equal to 0  ");
        }
//        if (amtToRefundToDiscountWallet == 0 && nonPromotionalAmount == 0 && promotionalAmount == 0) {
//            errors.add("amtToRefundToDiscountWallet,nonPromotionalAmount and promotionalAmount cannot be 0 ");
//        }

        if(actualRefundDate == null || actualRefundDate <= 0 || actualRefundDate>System.currentTimeMillis()){
            errors.add("invalid actualRefundDate");
        }
        return errors;
    }

}
