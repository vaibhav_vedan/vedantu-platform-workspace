package com.vedantu.subscription.viewobject.request;

import com.vedantu.subscription.entities.mongo.BundleVideo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class BundleVideoReq   extends AbstractFrontEndReq {
    List<BundleVideo> bundleVideo;

    public List<BundleVideo> getBundleVideo() {
        return bundleVideo;
    }

    public void setBundleVideo(List<BundleVideo> bundleVideo) {
        this.bundleVideo = bundleVideo;
    }

}
