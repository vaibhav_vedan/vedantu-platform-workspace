/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.onetofew.pojo.UserDashboardEnrollmentPojo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.response.AbstractRes;
import java.util.List;

/**
 *
 * @author jeet
 */
public class GetUserDashboardEnrollmentsRes extends AbstractRes{
    private EntityType entityType;
    private String entityId;
    private String entityTitle;
    private List<UserDashboardEnrollmentPojo> enrollments;
    private Orders orderInfo;
    private List<DashBoardInstalmentInfo> baseIntalments;//instalments in registration or base instalment
    
    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    public List<UserDashboardEnrollmentPojo> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(List<UserDashboardEnrollmentPojo> enrollments) {
        this.enrollments = enrollments;
    }

    public List<DashBoardInstalmentInfo> getBaseIntalments() {
        return baseIntalments;
    }

    public void setBaseIntalments(List<DashBoardInstalmentInfo> baseIntalments) {
        this.baseIntalments = baseIntalments;
    }

    public Orders getOrderInfo() {
        return orderInfo;
    }

    public void setOrderInfo(Orders orderInfo) {
        this.orderInfo = orderInfo;
    }

}
