package com.vedantu.subscription.viewobject.request;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;


import java.util.List;

/**
 * @author Darshit
 */
@Data
@NoArgsConstructor
public class SubscriptionCoursesReq extends AbstractFrontEndListReq {

    private Integer grade;
    private Boolean includeEnrolled;
    private String subject;
    private List<String> bundleIds;
    private List<String> excludeCourseIds;
    private List<String> includeCourseIds;
    private Boolean recordedVideo;
    private String courseTime;
    private CourseTerm searchTerms;
    private String query;
    private List<String> batchIds;
    private List<String> excludeBatchIds;
    private Boolean ascendingOrderStartTimeSort;
    private SubscriptionPackageType subscriptionPackageType = SubscriptionPackageType.TRIAL;

    public SubscriptionCoursesReq(List<String> courseIds, List<String> bundleIds, List<Bundle> regularBundles) {
        this.includeCourseIds=courseIds;
        this.bundleIds=bundleIds;
        if (ArrayUtils.isNotEmpty(regularBundles)) {
            this.subscriptionPackageType=SubscriptionPackageType.REGULAR;
        }
    }

    @Override
    protected List<String> collectVerificationErrors() {

        List<String> errors = super.collectVerificationErrors();
        if (null == grade) {
            errors.add("grade");
        }

        if (getStart() == null) {
            errors.add("start");
        }
        if (getSize() == null) {
            errors.add("size");
        }
        if (getSize() > 50) {
            errors.add("size should be less then 50");
        }
        if( !StringUtils.isEmpty(getQuery())){
            if(getQuery().length() < 4){
                errors.add("add atleast 4 charecter for search");
            }
        }


        return errors;
    }
}
