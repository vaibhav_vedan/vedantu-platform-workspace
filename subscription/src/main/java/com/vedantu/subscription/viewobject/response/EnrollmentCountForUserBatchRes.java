package com.vedantu.subscription.viewobject.response;

import java.util.Date;

public class EnrollmentCountForUserBatchRes {
    private Integer count;
    private Date date;
    private String status;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
