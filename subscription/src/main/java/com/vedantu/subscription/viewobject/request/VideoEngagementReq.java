package com.vedantu.subscription.viewobject.request;

import com.vedantu.User.Role;
import com.vedantu.subscription.viewobject.response.EnrollmentStatusPojo;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;

public class VideoEngagementReq extends AbstractFrontEndReq {
	String userId;
	String  bundleEnrollmentId;
	String  videoId;
	Long duration;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getBundleEnrollmentId() {
		return bundleEnrollmentId;
	}

	public void setBundleEnrollmentId(String bundleEnrollmentId) {
		this.bundleEnrollmentId = bundleEnrollmentId;
	}

	public String getVideoId() {
		return videoId;
	}

	public void setVideoId(String videoId) {
		this.videoId = videoId;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}
}
