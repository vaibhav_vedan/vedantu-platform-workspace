package com.vedantu.subscription.viewobject.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class UnlockHoursRequest extends AbstractFrontEndReq {

	private Long subscriptionId;
	private Long bookDuration;
	private Long sessionId;
	private EntityType contextType;
	private String contextId;

	public UnlockHoursRequest() {
		super();
	}

	public UnlockHoursRequest(Long subscriptionId, Long bookDuration, Long sessionId) {
		super();
		this.subscriptionId = subscriptionId;
		this.bookDuration = bookDuration;
		this.sessionId = sessionId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getBookDuration() {
		return bookDuration;
	}

	public void setBookDuration(Long bookDuration) {
		this.bookDuration = bookDuration;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}
}
