package com.vedantu.subscription.viewobject.request;

import com.vedantu.subscription.enums.game.GameTaskStatus;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;

@Data
public class GamificationToolFilterRequest extends AbstractFrontEndListReq {

    private Long userId;
    private String rewardName;
    private String rewardItem;
    private GameTaskStatus status;
    private Long startTime;
    private Long endTime;
    private Boolean isDownload;
    private String emailId; // Email ID of user to be mailed
    private String name; // Name of user to be mailed
}
