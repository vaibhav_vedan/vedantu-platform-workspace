package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;
@Data
public class SubscriptionProgramReq extends AbstractFrontEndReq {
    private Integer grade;
    private String target;
    private Integer year;
    private boolean displayHidden;
    private boolean includeCurriculum;
}