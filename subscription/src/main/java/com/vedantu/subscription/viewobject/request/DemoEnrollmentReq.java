package com.vedantu.subscription.viewobject.request;

import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.EntityStatus;

public class DemoEnrollmentReq {
	private String demoSessionId;
	private String courseId;
	private String batchId;
	private String teacherId;
	private String studentId;
	private String sessionId;
	private EntityStatus status;
	private EntityType type;

	public DemoEnrollmentReq() {
		super();
	}

	public DemoEnrollmentReq(String demoSessionId, String courseId, String batchId, String teacherId, String studentId,
			String sessionId, EntityStatus status, EntityType type) {
		super();
		this.demoSessionId = demoSessionId;
		this.courseId = courseId;
		this.batchId = batchId;
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.sessionId = sessionId;
		this.status = status;
		this.type = type;
	}

	public String getDemoSessionId() {
		return demoSessionId;
	}

	public void setDemoSessionId(String demoSessionId) {
		this.demoSessionId = demoSessionId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(String teacherId) {
		this.teacherId = teacherId;
	}

	public String getStudentId() {
		return studentId;
	}

	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public EntityStatus getStatus() {
		return status;
	}

	public void setStatus(EntityStatus status) {
		this.status = status;
	}

	public EntityType getType() {
		return type;
	}

	public void setType(EntityType type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "DemoEnrollmentReq [demoSessionId=" + demoSessionId + ", courseId=" + courseId + ", batchId=" + batchId
				+ ", teacherId=" + teacherId + ", studentId=" + studentId + ", sessionId=" + sessionId + ", status="
				+ status + ", type=" + type + "]";
	}
}
