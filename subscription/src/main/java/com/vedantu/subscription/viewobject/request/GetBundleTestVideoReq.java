package com.vedantu.subscription.viewobject.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class GetBundleTestVideoReq extends AbstractFrontEndListReq {
    private Integer grade;
    private String subject;
    private String bundleId;
    private List<String> bundleIds;
    private String title;
    private String userId;
}
