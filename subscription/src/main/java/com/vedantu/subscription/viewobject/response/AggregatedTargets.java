/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import java.util.Set;

/**
 *
 * @author parashar
 */
public class AggregatedTargets {
    
    private Set<String> aggTargets;

    /**
     * @return the targets
     */
    public Set<String> getAggTargets() {
        return aggTargets;
    }

    @Override
    public String toString() {
        return "AggregatedTargets{" + "targets=" + aggTargets + '}';
    }

    /**
     * @param aggTargets the targets to set
     */
    public void setAggTargets(Set<String> aggTargets) {
        this.aggTargets = aggTargets;
    }
    
}
