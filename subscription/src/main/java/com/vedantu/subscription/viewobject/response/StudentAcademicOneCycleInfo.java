/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.lms.cmds.pojo.UserTestResultPojo;
import com.vedantu.scheduling.pojo.UserSessionAttendance;
import java.util.List;

/**
 *
 * @author parashar
 */
public class StudentAcademicOneCycleInfo {
    
    private Float attendance;
    // private 
    private List<UserTestResultPojo> preMainTestResultObj;
    
    private UserTestResultPojo mainTestResultObj;
    private List<UserSessionAttendance> sessionAttendances;


    /**
     * @return the attendance
     */
    public Float getAttendance() {
        return attendance;
    }

    /**
     * @param attendance the attendance to set
     */
    public void setAttendance(Float attendance) {
        this.attendance = attendance;
    }

    /**
     * @return the preMainTestResultObj
     */
    public List<UserTestResultPojo> getPreMainTestResultObj() {
        return preMainTestResultObj;
    }

    /**
     * @param preMainTestResultObj the preMainTestResultObj to set
     */
    public void setPreMainTestResultObj(List<UserTestResultPojo> preMainTestResultObj) {
        this.preMainTestResultObj = preMainTestResultObj;
    }

    /**
     * @return the mainTestResultObj
     */
    public UserTestResultPojo getMainTestResultObj() {
        return mainTestResultObj;
    }

    /**
     * @param mainTestResultObj the mainTestResultObj to set
     */
    public void setMainTestResultObj(UserTestResultPojo mainTestResultObj) {
        this.mainTestResultObj = mainTestResultObj;
    }

    /**
     * @return the sessionAttendances
     */
    public List<UserSessionAttendance> getSessionAttendances() {
        return sessionAttendances;
    }

    /**
     * @param sessionAttendances the sessionAttendances to set
     */
    public void setSessionAttendances(List<UserSessionAttendance> sessionAttendances) {
        this.sessionAttendances = sessionAttendances;
    }
    
}
