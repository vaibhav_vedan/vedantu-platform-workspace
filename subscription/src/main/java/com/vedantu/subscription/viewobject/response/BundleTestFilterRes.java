package com.vedantu.subscription.viewobject.response;

import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.VideoContentData;

import java.util.List;
import java.util.Map;

/**
 * Created by somil on 08/05/17.
 */

public class BundleTestFilterRes {

    private String id;
    private List<Track> webinarCategories;
    private VideoContentData videos;
    private BundleTestRes tests;

    private String title;
    private List<String> subject;
    private List<String> target;
    private Integer grade;
    private Long boardId;
    private BundleState state = BundleState.DRAFT;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<String> tags;
    private List<String> teacherIds;
    private boolean featured;
    // lowest is highest
    private Integer priority;

    private String demoVideoUrl;
    private Integer noOfWebinars;
    private Integer noOfVideos;
    private Integer noOfTests;
    private List<String> courses;
    private Map<String, Object> metadata;

    private int regAmount;
    private int regAllowedDays ;
    private Long validTill ;
    private int validDays ;
    private Boolean tabletIncluded ;
    private Boolean amIncluded ;
    private Boolean unLimitedDoubtsIncluded ;
    private List<String> suggestionPackages;
    private List<AioPackage> packages;

    public List<Track> getWebinarCategories() {
        return webinarCategories;
    }

    public void setWebinarCategories(List<Track> webinarCategories) {
        this.webinarCategories = webinarCategories;
    }

    public VideoContentData getVideos() {
        return videos;
    }

    public void setVideos(VideoContentData videos) {
        this.videos = videos;
    }

    public BundleTestRes getTests() {
        return tests;
    }

    public void setTests(BundleTestRes tests) {
        this.tests = tests;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getSubject() {
        return subject;
    }

    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    public List<String> getTarget() {
        return target;
    }

    public void setTarget(List<String> target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public BundleState getState() {
        return state;
    }

    public void setState(BundleState state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getCutPrice() {
        return cutPrice;
    }

    public void setCutPrice(Integer cutPrice) {
        this.cutPrice = cutPrice;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public List<String> getTeacherIds() {
        return teacherIds;
    }

    public void setTeacherIds(List<String> teacherIds) {
        this.teacherIds = teacherIds;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getDemoVideoUrl() {
        return demoVideoUrl;
    }

    public void setDemoVideoUrl(String demoVideoUrl) {
        this.demoVideoUrl = demoVideoUrl;
    }

    public Integer getNoOfWebinars() {
        return noOfWebinars;
    }

    public void setNoOfWebinars(Integer noOfWebinars) {
        this.noOfWebinars = noOfWebinars;
    }

    public Integer getNoOfVideos() {
        return noOfVideos;
    }

    public void setNoOfVideos(Integer noOfVideos) {
        this.noOfVideos = noOfVideos;
    }

    public Integer getNoOfTests() {
        return noOfTests;
    }

    public void setNoOfTests(Integer noOfTests) {
        this.noOfTests = noOfTests;
    }

    public List<String> getCourses() {
        return courses;
    }

    public void setCourses(List<String> courses) {
        this.courses = courses;
    }

    public Map<String, Object> getMetadata() {
        return metadata;
    }

    public void setMetadata(Map<String, Object> metadata) {
        this.metadata = metadata;
    }

    public int getRegAmount() {
        return regAmount;
    }

    public void setRegAmount(int regAmount) {
        this.regAmount = regAmount;
    }

    public int getRegAllowedDays() {
        return regAllowedDays;
    }

    public void setRegAllowedDays(int regAllowedDays) {
        this.regAllowedDays = regAllowedDays;
    }

    public Long getValidTill() {
        return validTill;
    }

    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    public int getValidDays() {
        return validDays;
    }

    public void setValidDays(int validDays) {
        this.validDays = validDays;
    }

    public Boolean getTabletIncluded() {
        return tabletIncluded;
    }

    public void setTabletIncluded(Boolean tabletIncluded) {
        this.tabletIncluded = tabletIncluded;
    }

    public Boolean getAmIncluded() {
        return amIncluded;
    }

    public void setAmIncluded(Boolean amIncluded) {
        this.amIncluded = amIncluded;
    }

    public Boolean getUnLimitedDoubtsIncluded() {
        return unLimitedDoubtsIncluded;
    }

    public void setUnLimitedDoubtsIncluded(Boolean unLimitedDoubtsIncluded) {
        this.unLimitedDoubtsIncluded = unLimitedDoubtsIncluded;
    }

    public List<String> getSuggestionPackages() {
        return suggestionPackages;
    }

    public void setSuggestionPackages(List<String> suggestionPackages) {
        this.suggestionPackages = suggestionPackages;
    }

    public List<AioPackage> getPackages() {
        return packages;
    }

    public void setPackages(List<AioPackage> packages) {
        this.packages = packages;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
