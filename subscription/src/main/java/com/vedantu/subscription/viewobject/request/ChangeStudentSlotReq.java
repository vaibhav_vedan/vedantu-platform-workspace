/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.onetofew.pojo.OTFSlot;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.StringUtils;

/**
 *
 * @author pranavm
 */
public class ChangeStudentSlotReq extends AbstractFrontEndReq{
    
    private String id;
    private List<OTFSlot> preferredSlots;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<OTFSlot> getPreferredSlots() {
        return preferredSlots;
    }

    public void setPreferredSlots(List<OTFSlot> preferredSlots) {
        this.preferredSlots = preferredSlots;
    }

    @Override
    public String toString() {
        return "ChangeStudentSlotReq{" + "id=" + id + ", preferredSlots=" + preferredSlots + '}';
    }
    
    public List<String> validate() {
        List<String> errors = new ArrayList<String>();
        if (ArrayUtils.isEmpty(preferredSlots)) {
            errors.add("preferredSlots");
        }

        if (StringUtils.isEmpty(id)) {
            errors.add("id");
        }

        return errors;
    }
    
}
