/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

/**
 *
 * @author parashar
 */
public class DashboardInfoForAM {
    private String groupName;
    private Integer activeStudents;
    private Integer totalStudents;

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the activeStudents
     */
    public Integer getActiveStudents() {
        return activeStudents;
    }

    /**
     * @param activeStudents the activeStudents to set
     */
    public void setActiveStudents(Integer activeStudents) {
        this.activeStudents = activeStudents;
    }

    /**
     * @return the totalStudents
     */
    public Integer getTotalStudents() {
        return totalStudents;
    }

    /**
     * @param totalStudents the totalStudents to set
     */
    public void setTotalStudents(Integer totalStudents) {
        this.totalStudents = totalStudents;
    }
    
}
