/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import java.util.List;

/**
 *
 * @author parashar
 */
public class MigrationRequest {

    private String enrollmentId;
    private Integer amountToPay;
    private PaymentType type;
    private Integer amountPaid;
    private Integer pPaid;
    private Integer npPaid;
    private Integer discount;
    private Integer regPaid;
    private Integer regPPaid;
    private Integer regNPPaid;
    private String sessionId;
    private Long startTime;
    private Long endTime;
    private List<String> batchIds;
    private OTMSessionType oTMSessionType;
    private Integer installmentAmount;
    private Integer installmentPAmount;
    private Integer installmentNPAmount;
    private String installmentId;
    private EntityStatus status;
    private EnrollmentState state;
    private Integer discountAmount;
    private Integer installDiscountAmount;
    private boolean isBatch = false;
    private String orderId;
    private Long durationToBeSubtracted;

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the amountToPay
     */
    public Integer getAmountToPay() {
        return amountToPay;
    }

    /**
     * @param amountToPay the amountToPay to set
     */
    public void setAmountToPay(Integer amountToPay) {
        this.amountToPay = amountToPay;
    }

    /**
     * @return the type
     */
    public PaymentType getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(PaymentType type) {
        this.type = type;
    }

    /**
     * @return the amountPaid
     */
    public Integer getAmountPaid() {
        return amountPaid;
    }

    /**
     * @param amountPaid the amountPaid to set
     */
    public void setAmountPaid(Integer amountPaid) {
        this.amountPaid = amountPaid;
    }

    /**
     * @return the pPaid
     */
    public Integer getpPaid() {
        return pPaid;
    }

    /**
     * @param pPaid the pPaid to set
     */
    public void setpPaid(Integer pPaid) {
        this.pPaid = pPaid;
    }

    /**
     * @return the npPaid
     */
    public Integer getNpPaid() {
        return npPaid;
    }

    /**
     * @param npPaid the npPaid to set
     */
    public void setNpPaid(Integer npPaid) {
        this.npPaid = npPaid;
    }

    /**
     * @return the discount
     */
    public Integer getDiscount() {
        return discount;
    }

    /**
     * @param discount the discount to set
     */
    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    /**
     * @return the regPaid
     */
    public Integer getRegPaid() {
        return regPaid;
    }

    /**
     * @param regPaid the regPaid to set
     */
    public void setRegPaid(Integer regPaid) {
        this.regPaid = regPaid;
    }

    /**
     * @return the regPPaid
     */
    public Integer getRegPPaid() {
        return regPPaid;
    }

    /**
     * @param regPPaid the regPPaid to set
     */
    public void setRegPPaid(Integer regPPaid) {
        this.regPPaid = regPPaid;
    }

    /**
     * @return the regNPPaid
     */
    public Integer getRegNPPaid() {
        return regNPPaid;
    }

    /**
     * @param regNPPaid the regNPPaid to set
     */
    public void setRegNPPaid(Integer regNPPaid) {
        this.regNPPaid = regNPPaid;
    }

    /**
     * @return the sessionId
     */
    public String getSessionId() {
        return sessionId;
    }

    /**
     * @param sessionId the sessionId to set
     */
    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the endTime
     */
    public Long getEndTime() {
        return endTime;
    }

    /**
     * @param endTime the endTime to set
     */
    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    /**
     * @return the batchIds
     */
    public List<String> getBatchIds() {
        return batchIds;
    }

    /**
     * @param batchIds the batchIds to set
     */
    public void setBatchIds(List<String> batchIds) {
        this.batchIds = batchIds;
    }

    /**
     * @return the oTMSessionType
     */
    public OTMSessionType getoTMSessionType() {
        return oTMSessionType;
    }

    /**
     * @param oTMSessionType the oTMSessionType to set
     */
    public void setoTMSessionType(OTMSessionType oTMSessionType) {
        this.oTMSessionType = oTMSessionType;
    }

    /**
     * @return the installmentAmount
     */
    public Integer getInstallmentAmount() {
        return installmentAmount;
    }

    /**
     * @param installmentAmount the installmentAmount to set
     */
    public void setInstallmentAmount(Integer installmentAmount) {
        this.installmentAmount = installmentAmount;
    }

    /**
     * @return the installmentPAmount
     */
    public Integer getInstallmentPAmount() {
        return installmentPAmount;
    }

    /**
     * @param installmentPAmount the installmentPAmount to set
     */
    public void setInstallmentPAmount(Integer installmentPAmount) {
        this.installmentPAmount = installmentPAmount;
    }

    /**
     * @return the installmentNPAmount
     */
    public Integer getInstallmentNPAmount() {
        return installmentNPAmount;
    }

    /**
     * @param installmentNPAmount the installmentNPAmount to set
     */
    public void setInstallmentNPAmount(Integer installmentNPAmount) {
        this.installmentNPAmount = installmentNPAmount;
    }

    /**
     * @return the installmentId
     */
    public String getInstallmentId() {
        return installmentId;
    }

    /**
     * @param installmentId the installmentId to set
     */
    public void setInstallmentId(String installmentId) {
        this.installmentId = installmentId;
    }

    /**
     * @return the status
     */
    public EntityStatus getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(EntityStatus status) {
        this.status = status;
    }

    /**
     * @return the state
     */
    public EnrollmentState getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(EnrollmentState state) {
        this.state = state;
    }

    /**
     * @return the discountAmount
     */
    public Integer getDiscountAmount() {
        return discountAmount;
    }

    /**
     * @param discountAmount the discountAmount to set
     */
    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * @return the installDiscountAmount
     */
    public Integer getInstallDiscountAmount() {
        return installDiscountAmount;
    }

    /**
     * @param installDiscountAmount the installDiscountAmount to set
     */
    public void setInstallDiscountAmount(Integer installDiscountAmount) {
        this.installDiscountAmount = installDiscountAmount;
    }


    /**
     * @return the isBatch
     */
    public boolean isIsBatch() {
        return isBatch;
    }

    /**
     * @param isBatch the isBatch to set
     */
    public void setIsBatch(boolean isBatch) {
        this.isBatch = isBatch;
    }

    /**
     * @return the orderId
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * @param orderId the orderId to set
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return "MigrationRequest{" + "enrollmentId=" + enrollmentId + ", amountToPay=" + amountToPay + ", type=" + type + ", amountPaid=" + amountPaid + ", pPaid=" + pPaid + ", npPaid=" + npPaid + ", discount=" + discount + ", regPaid=" + regPaid + ", regPPaid=" + regPPaid + ", regNPPaid=" + regNPPaid + ", sessionId=" + sessionId + ", startTime=" + startTime + ", endTime=" + endTime + ", batchIds=" + batchIds + ", oTMSessionType=" + oTMSessionType + ", installmentAmount=" + installmentAmount + ", installmentPAmount=" + installmentPAmount + ", installmentNPAmount=" + installmentNPAmount + ", installmentId=" + installmentId + ", status=" + status + ", state=" + state + ", discountAmount=" + discountAmount + ", installDiscountAmount=" + installDiscountAmount + ", isBatch=" + isBatch + ", orderId=" + orderId + '}';
    }

    /**
     * @return the durationToBeSubtracted
     */
    public Long getDurationToBeSubtracted() {
        return durationToBeSubtracted;
    }

    /**
     * @param durationToBeSubtracted the durationToBeSubtracted to set
     */
    public void setDurationToBeSubtracted(Long durationToBeSubtracted) {
        this.durationToBeSubtracted = durationToBeSubtracted;
    }

}
