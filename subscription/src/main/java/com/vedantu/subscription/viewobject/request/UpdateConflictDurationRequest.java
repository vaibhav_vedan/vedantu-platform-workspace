package com.vedantu.subscription.viewobject.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;

import java.util.List;
import java.util.Objects;

public class UpdateConflictDurationRequest extends AbstractFrontEndReq{

	private Long subscriptionId;
	private Long previousDuration;
	private Long newDuration;
	private Long sessionId;
	private EntityType contextType;
	private String contextId;

	public UpdateConflictDurationRequest() {
		super();
	}

	public UpdateConflictDurationRequest(Long subscriptionId, Long previousDuration, Long newDuration, Long sessionId) {
		super();
		this.subscriptionId = subscriptionId;
		this.previousDuration = previousDuration;
		this.newDuration = newDuration;
		this.sessionId = sessionId;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getPreviousDuration() {
		return previousDuration;
	}

	public void setPreviousDuration(Long previousDuration) {
		this.previousDuration = previousDuration;
	}

	public Long getNewDuration() {
		return newDuration;
	}

	public void setNewDuration(Long newDuration) {
		this.newDuration = newDuration;
	}

	public Long getSessionId() {
		return sessionId;
	}

	public void setSessionId(Long sessionId) {
		this.sessionId = sessionId;
	}

	public EntityType getContextType() {
		return contextType;
	}

	public void setContextType(EntityType contextType) {
		this.contextType = contextType;
	}

	public String getContextId() {
		return contextId;
	}

	public void setContextId(String contextId) {
		this.contextId = contextId;
	}

	@Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();
		if (StringUtils.isEmpty(contextId)) {
			errors.add("contextId");
		}
		if (Objects.isNull(sessionId) || sessionId <= 0) {
			errors.add("sessionId");
		}
		if (Objects.isNull(previousDuration)) {
			errors.add("previousDuration");
		}
		if (Objects.isNull(newDuration)) {
			errors.add("newDuration");
		}

		return errors;
	}
}
