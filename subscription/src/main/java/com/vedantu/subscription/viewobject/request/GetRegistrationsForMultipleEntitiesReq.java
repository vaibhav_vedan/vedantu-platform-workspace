/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ajith
 */
public class GetRegistrationsForMultipleEntitiesReq extends AbstractFrontEndReq {

    private Long userId;
    private List<RegistrationEntity> entities;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public List<RegistrationEntity> getEntities() {
        return entities;
    }

    public void setEntities(List<RegistrationEntity> entities) {
        this.entities = entities;
    }

    public void addEntity(String entityId, EntityType entityType) {
        if (entities == null) {
            entities = new ArrayList<>();
        }
        entities.add(new RegistrationEntity(entityType, entityId));
    }

    public class RegistrationEntity {

        private EntityType entityType;
        private String entityId;

        public RegistrationEntity() {
        }

        public RegistrationEntity(EntityType entityType, String entityId) {
            this.entityType = entityType;
            this.entityId = entityId;
        }

        public EntityType getEntityType() {
            return entityType;
        }

        public void setEntityType(EntityType entityType) {
            this.entityType = entityType;
        }

        public String getEntityId() {
            return entityId;
        }

        public void setEntityId(String entityId) {
            this.entityId = entityId;
        }

        @Override
        public String toString() {
            return "RegistrationEntity{" + "entityType=" + entityType + ", entityId=" + entityId + '}';
        }

    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (ArrayUtils.isEmpty(entities)) {
            errors.add("entities");
        }
        if (userId == null) {
            errors.add("userId");
        }
        return errors;
    }

}
