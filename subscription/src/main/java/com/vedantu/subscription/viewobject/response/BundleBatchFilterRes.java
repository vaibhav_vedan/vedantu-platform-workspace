package com.vedantu.subscription.viewobject.response;

import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.pojo.bundle.AioPackage;

import java.util.List;
import java.util.Map;

/**
 * Created by Darshit
 */

public class BundleBatchFilterRes {
    private String id;
     private AioPackage packages;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public AioPackage getPackages() {
        return packages;
    }

    public void setPackages(AioPackage packages) {
        this.packages = packages;
    }
}
