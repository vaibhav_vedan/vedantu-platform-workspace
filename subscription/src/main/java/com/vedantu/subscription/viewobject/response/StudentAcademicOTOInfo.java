/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.scheduling.pojo.UserSessionAttendance;
import com.vedantu.lms.cmds.pojo.UserTestResultPojo;
import java.util.List;

/**
 *
 * @author parashar
 */
public class StudentAcademicOTOInfo {
    
    private String coursePlanId;
    private String title;
    private Float attendance;
    private List<UserTestResultPojo> userTestResults;
    private List<UserSessionAttendance> sessionAttendances;

    /**
     * @return the coursePlanId
     */
    public String getCoursePlanId() {
        return coursePlanId;
    }

    /**
     * @param coursePlanId the coursePlanId to set
     */
    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return the attendance
     */
    public Float getAttendance() {
        return attendance;
    }

    /**
     * @param attendance the attendance to set
     */
    public void setAttendance(Float attendance) {
        this.attendance = attendance;
    }

    /**
     * @return the userTestResults
     */
    public List<UserTestResultPojo> getUserTestResults() {
        return userTestResults;
    }

    /**
     * @param userTestResults the userTestResults to set
     */
    public void setUserTestResults(List<UserTestResultPojo> userTestResults) {
        this.userTestResults = userTestResults;
    }

    /**
     * @return the sessionAttendances
     */
    public List<UserSessionAttendance> getSessionAttendances() {
        return sessionAttendances;
    }

    /**
     * @param sessionAttendances the sessionAttendances to set
     */
    public void setSessionAttendances(List<UserSessionAttendance> sessionAttendances) {
        this.sessionAttendances = sessionAttendances;
    }
    

    
}
