package com.vedantu.subscription.viewobject.request;

public class GeneralInterventionReq {
	
	private String acadMentorEmail;
	private Long studentId;
	private String comment;
	
	public GeneralInterventionReq() {
		super();
	}

	public String getAcadMentorEmail() {
		return acadMentorEmail;
	}

	public void setAcadMentorEmail(String acadMentorEmail) {
		this.acadMentorEmail = acadMentorEmail;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
