package com.vedantu.subscription.viewobject.response;

import java.util.List;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SessionModel;

public class GetSubscriptionsRes {

	private UserBasicInfo teacher;
	private UserBasicInfo student;
	private String planId;
	private Long offeringId;
	private SessionModel model;
	private ScheduleType scheduleType;
	private Long totalHours;
	private Long consumedHours;
	private Long lockedHours;
	private Long remainingHours;
	private String teacherCouponId;
	private Long teacherDiscountAmount;
	private Long teacherHourlyRate;
	private String subject;
	private String target;
	private Integer grade;
	private Long subscriptionId;
	private Long subscriptionDetailsId;
	private List<SessionInfo> sessionList;
	private Boolean isActive;
	private Long creationTime;
	private Long startDate;
	private GetTeacherPlansResponse plan;
	private Long noOfWeeks;
	private Long endTime;
	private Long endedBy;
	private String endReason;
	private String title;
	private String note;
	private SubModel subModel;

	public GetSubscriptionsRes() {
		super();
		// TODO Auto-generated constructor stub
	}

	public GetSubscriptionsRes(UserBasicInfo teacher, UserBasicInfo student,
			String planId, Long offeringId, SessionModel model,
			ScheduleType scheduleType, Long totalHours, Long consumedHours,
			Long lockedHours, Long remainingHours, String teacherCouponId,
			Long teacherDiscountAmount, Long teacherHourlyRate, String subject,
			String target, Integer grade, Long subscriptionId,
			Long subscriptionDetailsId, List<SessionInfo> sessionList,
			Boolean isActive, Long creationTime, Long startDate,
			GetTeacherPlansResponse plan, Long noOfWeeks, Long endTime,
			Long endedBy, String endReason, String title, String note) {
		super();
		this.teacher = teacher;
		this.student = student;
		this.planId = planId;
		this.offeringId = offeringId;
		this.model = model;
		this.scheduleType = scheduleType;
		this.totalHours = totalHours;
		this.consumedHours = consumedHours;
		this.lockedHours = lockedHours;
		this.remainingHours = remainingHours;
		this.teacherCouponId = teacherCouponId;
		this.teacherDiscountAmount = teacherDiscountAmount;
		this.teacherHourlyRate = teacherHourlyRate;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.subscriptionId = subscriptionId;
		this.subscriptionDetailsId = subscriptionDetailsId;
		this.sessionList = sessionList;
		this.isActive = isActive;
		this.creationTime = creationTime;
		this.startDate = startDate;
		this.plan = plan;
		this.noOfWeeks = noOfWeeks;
		this.endTime = endTime;
		this.endedBy = endedBy;
		this.endReason = endReason;
		this.title = title;
		this.note = note;
	}

	public GetSubscriptionsRes(UserBasicInfo teacher, UserBasicInfo student,
			String planId, Long offeringId, SessionModel model,
			ScheduleType scheduleType, Long totalHours, Long consumedHours,
			Long lockedHours, Long remainingHours, String teacherCouponId,
			Long teacherDiscountAmount, Long teacherHourlyRate, String subject,
			String target, Integer grade, Long subscriptionId,
			Long subscriptionDetailsId, List<SessionInfo> sessionList,
			Boolean isActive, Long creationTime, Long startDate,
			GetTeacherPlansResponse plan, Long noOfWeeks, Long endTime,
			Long endedBy, String endReason, String title, String note,
			SubModel subModel) {
		super();
		this.teacher = teacher;
		this.student = student;
		this.planId = planId;
		this.offeringId = offeringId;
		this.model = model;
		this.scheduleType = scheduleType;
		this.totalHours = totalHours;
		this.consumedHours = consumedHours;
		this.lockedHours = lockedHours;
		this.remainingHours = remainingHours;
		this.teacherCouponId = teacherCouponId;
		this.teacherDiscountAmount = teacherDiscountAmount;
		this.teacherHourlyRate = teacherHourlyRate;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.subscriptionId = subscriptionId;
		this.subscriptionDetailsId = subscriptionDetailsId;
		this.sessionList = sessionList;
		this.isActive = isActive;
		this.creationTime = creationTime;
		this.startDate = startDate;
		this.plan = plan;
		this.noOfWeeks = noOfWeeks;
		this.endTime = endTime;
		this.endedBy = endedBy;
		this.endReason = endReason;
		this.title = title;
		this.note = note;
		this.subModel = subModel;
	}

	public UserBasicInfo getTeacher() {
		return teacher;
	}

	public void setTeacher(UserBasicInfo teacher) {
		this.teacher = teacher;
	}

	public UserBasicInfo getStudent() {
		return student;
	}

	public void setStudent(UserBasicInfo student) {
		this.student = student;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public Long getConsumedHours() {
		return consumedHours;
	}

	public void setConsumedHours(Long consumedHours) {
		this.consumedHours = consumedHours;
	}

	public Long getLockedHours() {
		return lockedHours;
	}

	public void setLockedHours(Long lockedHours) {
		this.lockedHours = lockedHours;
	}

	public Long getRemainingHours() {
		return remainingHours;
	}

	public void setRemainingHours(Long remainingHours) {
		this.remainingHours = remainingHours;
	}

	public String getTeacherCouponId() {
		return teacherCouponId;
	}

	public void setTeacherCouponId(String teacherCouponId) {
		this.teacherCouponId = teacherCouponId;
	}

	public Long getTeacherDiscountAmount() {
		return teacherDiscountAmount;
	}

	public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
		this.teacherDiscountAmount = teacherDiscountAmount;
	}

	public Long getTeacherHourlyRate() {
		return teacherHourlyRate;
	}

	public void setTeacherHourlyRate(Long teacherHourlyRate) {
		this.teacherHourlyRate = teacherHourlyRate;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public ScheduleType getScheduleType() {
		return scheduleType;
	}

	public void setScheduleType(ScheduleType scheduleType) {
		this.scheduleType = scheduleType;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getSubscriptionDetailsId() {
		return subscriptionDetailsId;
	}

	public void setSubscriptionDetailsId(Long subscriptionDetailsId) {
		this.subscriptionDetailsId = subscriptionDetailsId;
	}

	public List<SessionInfo> getSessionList() {
		return sessionList;
	}

	public void setSessionList(List<SessionInfo> sessionList) {
		this.sessionList = sessionList;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getCreationTime() {
		return creationTime;
	}

	public void setCreationTime(Long creationTime) {
		this.creationTime = creationTime;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public GetTeacherPlansResponse getPlan() {
		return plan;
	}

	public void setPlan(GetTeacherPlansResponse plan) {
		this.plan = plan;
	}

	public Long getNoOfWeeks() {
		return noOfWeeks;
	}

	public void setNoOfWeeks(Long noOfWeeks) {
		this.noOfWeeks = noOfWeeks;
	}

	public Long getEndTime() {
		return endTime;
	}

	public void setEndTime(Long endTime) {
		this.endTime = endTime;
	}

	public Long getEndedBy() {
		return endedBy;
	}

	public void setEndedBy(Long endedBy) {
		this.endedBy = endedBy;
	}

	public String getEndReason() {
		return endReason;
	}

	public void setEndReason(String endReason) {
		this.endReason = endReason;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public SubModel getSubModel() {
		return subModel;
	}

	public void setSubModel(SubModel subModel) {
		this.subModel = subModel;
	}

	@Override
	public String toString() {
		return "GetSubscriptionsRes [teacher=" + teacher + ", student="
				+ student + ", planId=" + planId + ", offeringId=" + offeringId
				+ ", model=" + model + ", scheduleType=" + scheduleType
				+ ", totalHours=" + totalHours + ", consumedHours="
				+ consumedHours + ", lockedHours=" + lockedHours
				+ ", remainingHours=" + remainingHours + ", teacherCouponId="
				+ teacherCouponId + ", teacherDiscountAmount="
				+ teacherDiscountAmount + ", teacherHourlyRate="
				+ teacherHourlyRate + ", subject=" + subject + ", target="
				+ target + ", grade=" + grade + ", subscriptionId="
				+ subscriptionId + ", subscriptionDetailsId="
				+ subscriptionDetailsId + ", sessionList=" + sessionList
				+ ", isActive=" + isActive + ", creationTime=" + creationTime
				+ ", startDate=" + startDate + ", plan=" + plan
				+ ", noOfWeeks=" + noOfWeeks + ", endTime=" + endTime
				+ ", endedBy=" + endedBy + ", endReason=" + endReason
				+ ", title=" + title + ", note=" + note + ", subModel="
				+ subModel + "]";
	}
}
