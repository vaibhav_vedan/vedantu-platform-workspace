package com.vedantu.subscription.viewobject.request;

import com.vedantu.User.Role;
import com.vedantu.subscription.viewobject.response.EnrollmentStatusPojo;

import java.util.List;

import com.vedantu.util.fos.request.AbstractFrontEndReq;

public class EnrollmentReq extends AbstractFrontEndReq {
	private List<EnrollmentStatusPojo> enrollments;
	private String batchId;
	private String courseId;
	private Role role;

	public EnrollmentReq() {
		super();
	}

	public List<EnrollmentStatusPojo> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(List<EnrollmentStatusPojo> enrollments) {
		this.enrollments = enrollments;
	}

	public String getBatchId() {
		return batchId;
	}

	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}
}
