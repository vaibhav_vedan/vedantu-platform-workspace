package com.vedantu.subscription.viewobject.response;

import java.util.List;

import com.vedantu.scheduling.pojo.session.SessionInfo;

public class ScheduleResponse {
	private List<SessionInfo> sessionInfo;
	private Boolean scheduled;
	private String error;
	
	public ScheduleResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public ScheduleResponse(List<SessionInfo> sessionInfo, Boolean scheduled, String error) {
		super();
		this.sessionInfo = sessionInfo;
		this.scheduled = scheduled;
		this.error = error;
	}
	
	public List<SessionInfo> getSessionInfo() {
		return sessionInfo;
	}

	public void setSessionInfo(List<SessionInfo> sessionInfo) {
		this.sessionInfo = sessionInfo;
	}

	public Boolean getScheduled() {
		return scheduled;
	}

	public void setScheduled(Boolean scheduled) {
		this.scheduled = scheduled;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "ScheduleResponse [sessionInfo=" + sessionInfo + ", scheduled=" + scheduled + ", error=" + error + "]";
	}

}
