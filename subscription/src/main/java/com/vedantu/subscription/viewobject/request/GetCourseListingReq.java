/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import java.util.List;

/**
 *
 * @author parashar
 */
public class GetCourseListingReq extends AbstractFrontEndListReq{
    
    private String grade;
    private String target;
    private Long lastStartTime;
    private CourseTerm courseTerm;
    private Integer skipCount = 0;
    

    /**
     * @return the grade
     */
    public String getGrade() {
        return grade;
    }

    /**
     * @param grade the grade to set
     */
    public void setGrade(String grade) {
        this.grade = grade;
    }

    /**
     * @return the target
     */
    public String getTarget() {
        return target;
    }

    /**
     * @param target the target to set
     */
    public void setTarget(String target) {
        this.target = target;
    }
    
    /**
     * @return the lastStartTime
     */
    public Long getLastStartTime() {
        return lastStartTime;
    }

    /**
     * @param lastStartTime the lastStartTime to set
     */
    public void setLastStartTime(Long lastStartTime) {
        this.lastStartTime = lastStartTime;
    }

    
    @Override
	protected List<String> collectVerificationErrors() {
		List<String> errors = super.collectVerificationErrors();

		if (StringUtils.isEmpty(grade)) {
			errors.add("grade");
		}
                
                if (lastStartTime == null || lastStartTime < 0){
                    errors.add("Invalid lastStartTime");
                }
                                
		return errors;
	}

    /**
     * @return the courseTerm
     */
    public CourseTerm getCourseTerm() {
        return courseTerm;
    }

    /**
     * @param courseTerm the courseTerm to set
     */
    public void setCourseTerm(CourseTerm courseTerm) {
        this.courseTerm = courseTerm;
    }

    /**
     * @return the skipCount
     */
    public Integer getSkipCount() {
        return skipCount;
    }

    /**
     * @param skipCount the skipCount to set
     */
    public void setSkipCount(Integer skipCount) {
        this.skipCount = skipCount;
    }
    
}
