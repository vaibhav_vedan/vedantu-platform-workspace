package com.vedantu.subscription.viewobject.request;

import com.vedantu.subscription.enums.HourTransactionType;
import com.vedantu.subscription.enums.HourType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

public class GetHourTransactionReq extends AbstractFrontEndListReq {

	private Long subscriptionId;
	private Long subscriptionDetailsId;
	private HourType debitFrom;
	private HourType creditTo;
	private HourTransactionType transactionType;
	private Long transactionRefNo;

	public GetHourTransactionReq() {
		super();
	}

	public GetHourTransactionReq(Long subscriptionId, Long subscriptionDetailsId, HourType debitFrom, HourType creditTo,
			HourTransactionType transactionType, Long transactionRefNo, Integer start, Integer size) {
		super(start, size);
		this.subscriptionId = subscriptionId;
		this.subscriptionDetailsId = subscriptionDetailsId;
		this.debitFrom = debitFrom;
		this.creditTo = creditTo;
		this.transactionType = transactionType;
		this.transactionRefNo = transactionRefNo;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getSubscriptionDetailsId() {
		return subscriptionDetailsId;
	}

	public void setSubscriptionDetailsId(Long subscriptionDetailsId) {
		this.subscriptionDetailsId = subscriptionDetailsId;
	}

	public HourType getDebitFrom() {
		return debitFrom;
	}

	public void setDebitFrom(HourType debitFrom) {
		this.debitFrom = debitFrom;
	}

	public HourType getCreditTo() {
		return creditTo;
	}

	public void setCreditTo(HourType creditTo) {
		this.creditTo = creditTo;
	}

	public HourTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(HourTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public Long getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(Long transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}
}
