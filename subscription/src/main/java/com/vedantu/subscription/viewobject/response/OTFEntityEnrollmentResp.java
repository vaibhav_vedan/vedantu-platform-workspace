package com.vedantu.subscription.viewobject.response;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.EnrollmentType;

import java.util.List;

public class OTFEntityEnrollmentResp {

    private EntityType entityType;
    private String entityId;
    private String entityTitle;
    private List<EnrollmentPojo> enrollments;
    private List<BaseInstalmentInfo> baseIntalments;//instalments in registration or base instalment
    private Boolean isLocked = false;
    private EnrollmentType contextType;
    private int grade;

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getEntityTitle() {
        return entityTitle;
    }

    public void setEntityTitle(String entityTitle) {
        this.entityTitle = entityTitle;
    }

    public List<EnrollmentPojo> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(List<EnrollmentPojo> enrollments) {
        this.enrollments = enrollments;
    }

    public List<BaseInstalmentInfo> getBaseIntalments() {
        return baseIntalments;
    }

    public void setBaseIntalments(List<BaseInstalmentInfo> baseIntalments) {
        this.baseIntalments = baseIntalments;
    }

    public Boolean getIsLocked() {
        return isLocked;
    }

    public void setIsLocked(Boolean locked) {
        isLocked = locked;
    }

    public EnrollmentType getContextType() {
        return contextType;
    }

    public void setContextType(EnrollmentType contextType) {
        this.contextType = contextType;
    }

    public int getGrade() {
        return grade;
    }

    public void setGrade(int grade) {
        this.grade = grade;
    }
}
