package com.vedantu.subscription.viewobject.response;

import com.vedantu.subscription.entities.mongo.Enrollment;

import java.util.List;

public class BatchGroupedEnrollments {

    private String batchId;
    private List<Enrollment> enrollments;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public List<Enrollment> getEnrollments() {
        return enrollments;
    }

    public void setEnrollments(List<Enrollment> enrollments) {
        this.enrollments = enrollments;
    }
}
