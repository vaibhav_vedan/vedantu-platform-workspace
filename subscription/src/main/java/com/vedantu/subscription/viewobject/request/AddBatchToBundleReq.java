package com.vedantu.subscription.viewobject.request;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.Language;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import lombok.Data;

import java.util.List;
import java.util.Set;

@Data
public class AddBatchToBundleReq {
    private String batchId;
    private String courseId;
    private String courseTitle;
    private Set<String> grades;
    private List<String> targets;
    private Set<String> subjects;
    private List<CourseTerm> courseTerms;
    private  Boolean recordedVideo;
    private  Long startTime;
    private  Long endTime;
    private Language language = Language.DEFAULT;
    private List<SubscriptionPackageType> packageTypes;

    public AddBatchToBundleReq(String batchId, String courseId, Set<String> subjects,
                               List<CourseTerm> courseTerms, Set<String> grades, Boolean recordedVideo, String courseTitle,
                               List<String> targets, Language language, List<SubscriptionPackageType> packageTypes) {
        this.batchId = batchId;
        this.courseId = courseId;
        this.subjects = subjects;
        this.courseTerms = courseTerms;
        this.grades = grades;
        this.recordedVideo = recordedVideo;
        this.courseTitle = courseTitle;
        this.targets = targets;
        this.language = language;
        this.packageTypes = packageTypes;
    }

}
