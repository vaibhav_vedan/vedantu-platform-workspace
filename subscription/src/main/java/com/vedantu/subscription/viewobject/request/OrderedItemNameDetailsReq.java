package com.vedantu.subscription.viewobject.request;

import com.vedantu.subscription.pojo.OrderedItemNames;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderedItemNameDetailsReq extends AbstractFrontEndReq {
    private List<OrderedItemNames> orderedItemNames;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors=super.collectVerificationErrors();

        if(ArrayUtils.isEmpty(orderedItemNames)){
            errors.add("You cann't request for nothing, please specify the correct set of items");
        }else{
            List<OrderedItemNames> invalidOrderedItems = orderedItemNames
                    .stream()
                    .filter(item ->
                            StringUtils.isEmpty(item.getEntityId()) ||
                                    StringUtils.isEmpty(item.getOrderId()) ||
                                    item.getEntityType() == null
                    )
                    .collect(Collectors.toList());

            if(ArrayUtils.isNotEmpty(invalidOrderedItems)){
                errors.add("There are some entries which have either entityId or entityType or orderId as null "
                        +invalidOrderedItems);
            }
        }
        return errors;
    }
}
