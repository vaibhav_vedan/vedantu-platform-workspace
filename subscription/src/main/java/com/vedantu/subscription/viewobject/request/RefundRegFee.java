/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.request;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import java.util.List;

/**
 *
 * @author ajith
 */
public class RefundRegFee extends AbstractFrontEndReq {

    private Long userId;
    private String entityId;
    private EntityType entityType;
    private Boolean refundInfoOnly = false;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public Boolean getRefundInfoOnly() {
        return refundInfoOnly;
    }

    public void setRefundInfoOnly(Boolean refundInfoOnly) {
        this.refundInfoOnly = refundInfoOnly;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (entityId == null) {
            errors.add("entityId");
        }
        if (userId == null) {
            errors.add("userId");
        }
        if (entityType == null) {
            errors.add("entityType");
        } else if (!EntityType.OTF_COURSE_REGISTRATION.equals(entityType)
                && !EntityType.OTM_BUNDLE_REGISTRATION.equals(entityType)
                && !EntityType.OTM_BUNDLE_ADVANCE_PAYMENT.equals(entityType)) {
            errors.add("only allowed for course,bundle,adv payment refund");
        }
        return errors;
    }
}
