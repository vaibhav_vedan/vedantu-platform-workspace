package com.vedantu.subscription.viewobject.request;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.subscription.enums.bundle.CourseType;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

/**
 * @author mano
 */
@Data
public class BundleSearchReq extends AbstractFrontEndReq {
    private String searchTerm;
    private CourseType courseType;
    private String subject;
    private List<Integer> grades;
    private List<String> targets;
    private List<CourseTerm> courseTerms;
    private Integer start;
    private Integer limit;
    private String promotionTag;
}
