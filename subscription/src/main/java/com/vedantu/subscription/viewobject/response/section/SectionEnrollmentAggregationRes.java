package com.vedantu.subscription.viewobject.response.section;

import lombok.Data;

@Data
public class SectionEnrollmentAggregationRes {
    private String id;
    private Long enrollmentsCount;
}
