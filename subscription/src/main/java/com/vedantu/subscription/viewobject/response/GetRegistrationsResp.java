/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.viewobject.response;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.util.dbentitybeans.mongo.AbstractMongoStringIdEntityBean;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author pranavm
 */
public class GetRegistrationsResp extends AbstractMongoStringIdEntityBean{
    private Long userId;
    private EntityType entityType;
    private String entityId;
    private String orderId;
    private Integer bulkPriceToPay;
    private List<OTMBundleEntityInfo> entities;
    private List<BaseInstalmentInfo> instalmentDetails;
    private RegistrationStatus status = RegistrationStatus.REGISTERED;
    private List<String> deliverableEntityIds=new ArrayList<>();
    private Integer duration;
    private OTFBundleInfo bundleInfo;
    private List<BatchBasicInfo> batchInfos;
    private List<CourseBasicInfo> courseInfos;

    public List<BatchBasicInfo> getBatchInfos() {
        return batchInfos;
    }

    public void setBatchInfos(List<BatchBasicInfo> batchInfos) {
        this.batchInfos = batchInfos;
    }

    public List<CourseBasicInfo> getCourseInfos() {
        return courseInfos;
    }

    public void setCourseInfos(List<CourseBasicInfo> courseInfos) {
        this.courseInfos = courseInfos;
    }


    public OTFBundleInfo getBundleInfo() {
        return bundleInfo;
    }

    public void setBundleInfo(OTFBundleInfo bundleInfo) {
        this.bundleInfo = bundleInfo;
    }
    

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getBulkPriceToPay() {
        return bulkPriceToPay;
    }

    public void setBulkPriceToPay(Integer bulkPriceToPay) {
        this.bulkPriceToPay = bulkPriceToPay;
    }

    public List<OTMBundleEntityInfo> getEntities() {
        return entities;
    }

    public void setEntities(List<OTMBundleEntityInfo> entities) {
        this.entities = entities;
    }

    public List<BaseInstalmentInfo> getInstalmentDetails() {
        return instalmentDetails;
    }

    public void setInstalmentDetails(List<BaseInstalmentInfo> instalmentDetails) {
        this.instalmentDetails = instalmentDetails;
    }

    public RegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(RegistrationStatus status) {
        this.status = status;
    }

    public List<String> getDeliverableEntityIds() {
        return deliverableEntityIds;
    }

    public void setDeliverableEntityIds(List<String> deliverableEntityIds) {
        this.deliverableEntityIds = deliverableEntityIds;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
