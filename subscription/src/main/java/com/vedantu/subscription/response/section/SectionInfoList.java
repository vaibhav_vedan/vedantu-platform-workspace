package com.vedantu.subscription.response.section;

import com.vedantu.subscription.enums.TeacherType;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import java.util.List;
import java.util.Map;

@Data
public class SectionInfoList extends AbstractFrontEndListReq {
    String batchId;
    List<SectionInfo> sectionInfos;
    Map<String,String> teacherIdSectionNameMap;
    Map<String, TeacherType> teacherIdTypeMap;
}
