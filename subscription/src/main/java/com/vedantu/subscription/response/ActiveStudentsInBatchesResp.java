package com.vedantu.subscription.response;

import com.vedantu.onetofew.pojo.ActiveUserIdsByBatchIdsResp;
import com.vedantu.onetofew.pojo.BatchChangeTime;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Builder
@ToString
public class ActiveStudentsInBatchesResp {
    private List<ActiveUserIdsByBatchIdsResp> enrollments;
    private String prevLastFetchedId;
}
