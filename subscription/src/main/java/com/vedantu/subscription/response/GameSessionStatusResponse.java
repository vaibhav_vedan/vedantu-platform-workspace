package com.vedantu.subscription.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GameSessionStatusResponse {
    String uiText;
    boolean hasSessionTask;
}
