package com.vedantu.subscription.response.section;

import com.vedantu.onetofew.enums.EnrollmentState;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class StudentSectionMappingRes {
    String batchId;
    String sectionId;
    String sectionName;
    EnrollmentState sectionType;
    String userId;
    String mainTeacherEmailId;
}
