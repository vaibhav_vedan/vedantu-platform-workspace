package com.vedantu.subscription.response;

import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.subscription.entities.mongo.Enrollment;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class EnrollmentHomePageResponse {
    private String entityId;
    private BatchBasicInfo batch;
    private CourseBasicInfo course;
}
