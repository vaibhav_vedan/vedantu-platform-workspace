package com.vedantu.subscription.response.section;

import com.vedantu.onetofew.enums.EnrollmentState;
import lombok.Data;

@Data
public class ActiveTAsForBatchRes {
    String batchId;
    String sectionId;
    String userId;
    String taName;
    String sectionName;
    EnrollmentState sectionType;
}
