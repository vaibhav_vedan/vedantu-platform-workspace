package com.vedantu.subscription.response.section;

import lombok.Data;

@Data
public class BatchSectionAggregationRes {
    private String id;
    private Long sectionsCount;
}
