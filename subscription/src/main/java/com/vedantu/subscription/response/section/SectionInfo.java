package com.vedantu.subscription.response.section;

import com.vedantu.onetofew.pojo.section.SectionPojo;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.util.dbentitybeans.mongo.EntityState;

import java.util.List;
import java.util.Objects;

public class SectionInfo extends SectionPojo{

    private long enrollmentCount;
    private EntityState entityState;

    public EntityState getEntityState() {
        return entityState;
    }

    public void setEntityState(EntityState entityState) {
        this.entityState = entityState;
    }

    public long getEnrollmentCount() {
        return enrollmentCount;
    }

    public void setEnrollmentCount(long enrollmentCount) {
        this.enrollmentCount = enrollmentCount;
    }

    public SectionInfo(){
        super();
        this.enrollmentCount = 0;
    }

    public SectionInfo(long enrollmentCount, SectionPojo sectionPojo){
        this.enrollmentCount = enrollmentCount;
        this.setSectionId(sectionPojo.getSectionId());
        this.setSectionName(sectionPojo.getSectionName());
        this.setSectionType(sectionPojo.getSectionType());
        this.setEmailIds(sectionPojo.getEmailIds());
        this.setSize(sectionPojo.getSize());
    }

    public SectionInfo(Section s, List<String> eIds, long enrollCt){
        this.setSectionId(s.getId());
        this.setSectionName(s.getSectionName());
        this.setSize(s.getSize());
        this.setSectionType(s.getSectionType());

        if(Objects.nonNull(eIds) && !eIds.isEmpty())
            this.setEmailIds(eIds);
        else
            this.setEmailIds(null);

        this.setEnrollmentCount(enrollCt);
    }
}
