package com.vedantu.subscription.response.section;

import lombok.Data;

@Data
public class SectionMetaDataRes {
    String sectionName;
    String taName;
    String userId;
    String batchId;
}
