package com.vedantu.subscription.response;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by Dpadhya
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubscriptionUpdateResp  {




    private Long enrollmentStartDate;
    private Integer validMonths;
    private Boolean isEnrollmentCreated ;
    private String orderId ;


}
