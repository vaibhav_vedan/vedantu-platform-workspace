package com.vedantu.subscription.response;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.util.CollectionUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class EnrolledCoursesHomepage {

    public enum courseTerms {
        /**
         * LONG TERM, SUBSCRIPTION
         */
        LONG_TERM,
        /**
         * SHORT_TERM
         */
        SHORT_TERM,
        /**
         * TEST
         */
        TEST_SERIES,
        /**
         * MC COURSES
         */
        MICRO_COURSES,
        /**
         * REST OF THEM
         */
        REST;

        private static courseTerms getSearchTerm(List<CourseTerm> courseSearchTerms) {
            if (courseSearchTerms == null) {
                return REST;
            }
            if (courseSearchTerms.contains(CourseTerm.LONG_TERM)) {
                return LONG_TERM;
            }
            if (courseSearchTerms.contains(CourseTerm.SHORT_TERM)) {
                return SHORT_TERM;
            }
            if (courseSearchTerms.contains(CourseTerm.TEST_SERIES)) {
                return TEST_SERIES;
            }
            if (courseSearchTerms.contains(CourseTerm.MICRO_COURSES)) {
                return MICRO_COURSES;
            }
            // Rest of the course terms
            return REST;
        }
    }

    private String enrollmentId;
    private EntityStatus status;
    private String courseId;
    private String batchId;
    private String courseTitle;
    private Set<String> normalizeSubjects;
    List<BoardTeacherPair> boardTeacherPairs;
    private courseTerms mainSearchTerm;
    private List<CourseTerm> allCourseSearchTerms;
    private Long startTime;
    private Long endTime;

    public static List<EnrolledCoursesHomepage> generateFromEnrollments(List<Enrollment> enrollments, Map<String, CourseBasicInfo> courseMap, Map<String, BatchBasicInfo> batchMap) {
        List<EnrolledCoursesHomepage> enrolledCourses = new ArrayList<>();
        if (CollectionUtils.isEmpty(enrollments)) {
            return enrolledCourses;
        }

        for (Enrollment enrollment : enrollments) {
            EnrolledCoursesHomepage enrolledCourse = new EnrolledCoursesHomepage();
            enrolledCourse.setCourseId(enrollment.getCourseId());
            enrolledCourse.setStatus(enrollment.getStatus());
            enrolledCourse.setBatchId(enrollment.getBatchId());

            CourseBasicInfo courseInfo = courseMap.get(enrollment.getCourseId());
            BatchBasicInfo batchInfo = batchMap.get(enrollment.getBatchId());
            enrolledCourse.setStartTime(batchInfo.getStartTime());
            enrolledCourse.setEndTime(batchInfo.getEndTime());
            enrolledCourse.setCourseTitle(courseInfo.getTitle());
            enrolledCourse.setBoardTeacherPairs(batchInfo.getBoardTeacherPairs());
            enrolledCourse.setNormalizeSubjects(courseInfo.getNormalizeSubjects());
            enrolledCourse.setAllCourseSearchTerms(batchInfo.getSearchTerms());
            enrolledCourse.setMainSearchTerm(courseTerms.getSearchTerm(batchInfo.getSearchTerms()));
            enrolledCourses.add(enrolledCourse);
        }

        return enrolledCourses;
    }


}
