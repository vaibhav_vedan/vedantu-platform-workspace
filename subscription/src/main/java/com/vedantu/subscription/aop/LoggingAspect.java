/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.aop;

import com.vedantu.util.aop.AbstractAOPLayer;
import com.vedantu.util.logstash.LogstashLayout;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;


@Component
@Aspect
public class LoggingAspect extends AbstractAOPLayer{

    @Before("allCtrlMethods()")
    public void beforeExecutionForCtrlMethods(JoinPoint jp) {
        if (RequestContextHolder.getRequestAttributes() == null) {
            return;
        }
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes.getRequest() == null) {
            return;
        }

        HttpServletRequest request = servletRequestAttributes.getRequest();
        LogstashLayout.addToMDC(request);

    }

    //tracking dao methods calls
    @Pointcut("within(com.vedantu.subscription.dao..*)")
    public void allDAOMethods() {
    }    

}
