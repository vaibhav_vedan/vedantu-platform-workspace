/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.aop;

import java.util.Enumeration;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 *
 * @author ajith
 */
public class AppServletContextListener
        implements ServletContextListener {

    @Override
    public void contextDestroyed(ServletContextEvent arg0) {
//        System.out.println("---------ServletContextListener destroyed, doing some clean up work----------");
//        try {
//            com.mysql.jdbc.AbandonedConnectionCleanupThread.shutdown();
//        } catch (Throwable t) {
//        }
//        // This manually deregisters JDBC driver, which prevents Tomcat 7 from complaining about memory leaks
//        Enumeration<java.sql.Driver> drivers = java.sql.DriverManager.getDrivers();
//        while (drivers.hasMoreElements()) {
//            java.sql.Driver driver = drivers.nextElement();
//            try {
//                java.sql.DriverManager.deregisterDriver(driver);
//            } catch (Throwable t) {
//            }
//        }
//        try {
//            Thread.sleep(2000L);
//        } catch (Exception e) {
//        }
    }

    //Run this before web application is started
    @Override
    public void contextInitialized(ServletContextEvent arg0) {
//        System.out.println("---------ServletContextListener started, nothing to do in this method----------");
    }
}
