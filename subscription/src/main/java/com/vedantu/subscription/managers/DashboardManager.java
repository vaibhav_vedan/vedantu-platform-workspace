package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import com.vedantu.subscription.request.GetCoursePlansReq;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.pojo.UserTestResultPojo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchDashboardInfo;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.EnrollmentStateChangeTime;
import com.vedantu.onetofew.pojo.GTTAttendeeDetailsInfo;
import com.vedantu.onetofew.pojo.GTTAttendeeSessionInfo;
import com.vedantu.onetofew.pojo.OTFSessionInfo;
import com.vedantu.onetofew.pojo.SessionPlanPojo;
import com.vedantu.onetofew.request.GetBatchesForDashboardReq;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.CoursePlanDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.CoursePlan;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.pojo.TeacherCourseStateCount;
import com.vedantu.subscription.viewobject.response.StudentAcademicOTMInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.subscription.viewobject.response.DashboardInfoForAM;
import com.vedantu.util.ArrayUtils;
import com.vedantu.subscription.viewobject.response.DashboardInfoForAM;
import com.vedantu.subscription.viewobject.response.StudentAcademicOTOInfo;
import com.vedantu.subscription.viewobject.response.StudentAcademicOneCycleInfo;
import com.vedantu.subscription.viewobject.response.StudentCoursePlanInfo;
import com.vedantu.subscription.viewobject.response.StudentOTMEnrollmentInfo;
import com.vedantu.scheduling.pojo.UserSessionAttendance;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;
import java.lang.reflect.Type;
import java.util.Comparator;
import org.dozer.DozerBeanMapper;
import org.springframework.http.HttpMethod;

@Service
public class DashboardManager {

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(DashboardManager.class);

	@Autowired
	public EnrollmentDAO enrollmentDAO;

	@Autowired
	private BatchDAO batchDAO;

	@Autowired
	private CourseDAO courseDAO;
        
        @Autowired
        private CoursePlanDAO coursePlanDAO;
        
        @Autowired
        private FosUtils fosUtils;


        @Autowired
        private DozerBeanMapper mapper;

	private final String lmdEndpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
        private final String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
        private static Gson gson = new Gson();
        
	public List<BatchDashboardInfo> getBatches(GetBatchesForDashboardReq req) throws NotFoundException {

		List<BatchDashboardInfo> batchInfos = new ArrayList<>();
		Query query = new Query();

		if (req.getBatchIds() != null) {
			query.addCriteria(Criteria.where(Batch.Constants.ID).in(req.getBatchIds()));
		}
		if (req.getBatchStatus() != null) {
			query.addCriteria(Criteria.where(Batch.Constants.BATCH_STATE).is(req.getBatchStatus()));
		}

		if (req.getFields() != null) {
			for (String field : req.getFields()) {
				query.fields().include(field);
			}
		}
		query.fields().include(Batch.Constants.ID);
		query.fields().include(Batch.Constants.COURSE_ID);
		query.fields().include(Batch.Constants.TEACHER_IDS);
		query.fields().include(Batch.Constants.VISIBILITY_STATE);
		query.fields().include(Batch.Constants.BATCH_STATE);
		query.fields().include(Batch.Constants.SESSION_PLAN);
		query.fields().include(Batch.Constants.BOARD_TEACHER_PAIRS);
		query.fields().include(Batch.Constants.START_TIME);
		query.fields().include(Batch.Constants.END_TIME);

		batchDAO.setFetchParameters(query, req);
		query.with(Sort.by(Sort.Direction.DESC, Batch.Constants.LAST_UPDATED));

		List<Batch> batches = batchDAO.runQuery(query, Batch.class);

		if (ArrayUtils.isEmpty(batches)) {
			return batchInfos;
		}
		Set<String> courseIds = new HashSet<>();
		for (Batch batch : batches) {
			courseIds.add(batch.getCourseId());
		}
		Query courseQuery = new Query();
		if (ArrayUtils.isNotEmpty(courseIds)) {
			courseQuery.addCriteria(Criteria.where(Course.Constants.ID).in(courseIds));
		}
		courseQuery.fields().include(Course.Constants.ID);
		courseQuery.fields().include(Course.Constants.TITLE);
		courseQuery.fields().include(Course.Constants.SUBJECTS);
		courseQuery.fields().include(Course.Constants.TARGETS);
		courseQuery.fields().include(Course.Constants.GRADES);
		courseQuery.fields().include(Course.Constants.CURRICULUM);
		logger.info("courseQuery " + courseQuery.toString());
		List<Course> courses = courseDAO.runQuery(courseQuery, Course.class);
		Map<String, Course> courseMap = new HashMap<>();
		for (Course course : courses) {
			courseMap.put(course.getId(), course);
		}

		return toBatchDashboardInfo(batches, courseMap);

	}

	public BatchDashboardInfo getBatch(String batchId) throws NotFoundException {
		Batch batch = batchDAO.getById(batchId);
		if (batch == null) {
			throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "batch not found");
		}
		String courseId = batch.getCourseId();
		Course course = courseDAO.getById(courseId);
		if (course == null) {
			throw new NotFoundException(ErrorCode.COURSE_NOT_FOUND, "course not found");
		}
		return _toBatchDashboardInfo(batch, course);
	}

	public List<BatchDashboardInfo> toBatchDashboardInfo(List<Batch> batches, Map<String, Course> courseMap) {
		List<BatchDashboardInfo> batchInfos = new ArrayList<>();
		for (Batch batch : batches) {
			Course course = courseMap.get(batch.getCourseId());
			BatchDashboardInfo batchInfo = _toBatchDashboardInfo(batch, course);
			batchInfos.add(batchInfo);
		}
		return batchInfos;
	}

	private BatchDashboardInfo _toBatchDashboardInfo(Batch batch, Course course) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm a");
		dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));

		BatchDashboardInfo batchInfo = new BatchDashboardInfo();
		batchInfo.setBatchId(batch.getId());
		batchInfo.setBatchState(batch.getBatchState());
		if (batch.getStartTime() != null) {
			batchInfo.setBatchStartDate(dateFormat.format(new Date(batch.getStartTime())));
		}
		batchInfo.setCourseId(batch.getCourseId());
		batchInfo.setTeacherIds(batch.getTeacherIds());
		batchInfo.setCourseTitle(course.getTitle());
                
                Set<String> subjects=new HashSet<>();
                if(ArrayUtils.isNotEmpty(batch.getBoardTeacherPairs())){
                    for(BoardTeacherPair pair:batch.getBoardTeacherPairs()){
                        subjects.add(pair.getBoardId().toString());
                    }
                }                
		batchInfo.setSubjects(subjects);
		batchInfo.setCurriculum(course.getCurriculum());
		batchInfo.setTargets(course.getTargets());
		batchInfo.setSessionPlan(batch.getSessionPlan());
		batchInfo.setBoardTeacherPairs(batch.getBoardTeacherPairs());
		batchInfo.setStartTime(batch.getStartTime());
		batchInfo.setEndTime(batch.getEndTime());
		return batchInfo;
	}

	public List<EnrollmentPojo> getEnrollmentsByBatchIds(List<String> batchIds)
			throws BadRequestException, NotFoundException {
		if (ArrayUtils.isEmpty(batchIds)) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "BatchIds are null");
		}
		List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
		Query query = new Query();

		query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
		query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
		List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
		logger.info("enrollments returned : " + enrollments.size());

		if (ArrayUtils.isEmpty(enrollments)) {
			return enrollmentInfos;
		}

		for (Enrollment enrollment : enrollments) {
			EnrollmentPojo enrollmentInfo = mapper.map(enrollment, EnrollmentPojo.class);
			enrollmentInfo.setEnrollmentId(enrollment.getId());
			enrollmentInfos.add(enrollmentInfo);
		}
		return enrollmentInfos;
	}



	public Map<Long, TeacherCourseStateCount> getTeacherEnrollmentCountMapping(Long startTime, Long endTime) {

		Map<Long, TeacherCourseStateCount> idTeacherBatchMap = new HashMap<>();
		Query query = new Query();
		Criteria criteria = new Criteria();
		Criteria creationTime = Criteria.where(Enrollment.Constants.CREATION_TIME).gte(startTime).lt(endTime);
		Criteria stateChangeTimeState = Criteria.where(Enrollment.Constants.STATE_CHANGE_TIME_NEW_STATE)
				.is(EnrollmentState.REGULAR);
		Criteria stateChangeTimeCriteria = Criteria.where(Enrollment.Constants.STATE_CHANGE_TIME_LONG).gte(startTime)
				.lt(endTime);
		Criteria stateChangeTime = new Criteria();
		stateChangeTime.andOperator(stateChangeTimeState, stateChangeTimeCriteria);
		Criteria endTimeCriteria = Criteria.where(Enrollment.Constants.END_TIME).gte(startTime).lt(endTime);
		criteria.orOperator(creationTime, endTimeCriteria, stateChangeTime);
		query.addCriteria(criteria);
		query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
		query.fields().include(Enrollment.Constants.ID);
		query.fields().include(Enrollment.Constants._ID);
		query.fields().include(Enrollment.Constants.BATCH_ID);
		query.fields().include(Enrollment.Constants.STATE);
		query.fields().include(Enrollment.Constants.STATUS);
		query.fields().include(Enrollment.Constants.CREATION_TIME);
		query.fields().include(Enrollment.Constants.END_TIME);
		query.fields().include(Enrollment.Constants.STATE_CHANGE_TIME);
		query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.LAST_UPDATED));
		List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
		Set<String> batchIds = new HashSet<>();
		if (ArrayUtils.isEmpty(enrollments)) {
			return idTeacherBatchMap;
		}
		for (Enrollment enrollment : enrollments) {
			if (StringUtils.isNotEmpty(enrollment.getBatchId())) {
				batchIds.add(enrollment.getBatchId());
			}
		}

		Query batchQuery = new Query();
		batchQuery.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
		batchQuery.fields().include(Batch.Constants.ID);
		batchQuery.fields().include(Batch.Constants._ID);
		batchQuery.fields().include(Batch.Constants.TEACHER_IDS);
		List<Batch> batches = batchDAO.runQuery(batchQuery, Batch.class);
		Map<String, Set<String>> batchTeacherMap = new HashMap<>();
		for (Batch batch : batches) {
			if (ArrayUtils.isNotEmpty(batch.getTeacherIds())) {
				batchTeacherMap.put(batch.getId(), batch.getTeacherIds());
			}
		}
		for (Enrollment enrollment : enrollments) {
			if (batchTeacherMap.containsKey(enrollment.getBatchId())) {
				Set<String> teacherIds = batchTeacherMap.get(enrollment.getBatchId());
				if (ArrayUtils.isNotEmpty(teacherIds)) {
					for (String teacherId : teacherIds) {
						Long id = Long.parseLong(teacherId);
						TeacherCourseStateCount teacherStateCount;
						if (idTeacherBatchMap.containsKey(id)) {
							teacherStateCount = idTeacherBatchMap.get(id);
						} else {
							teacherStateCount = new TeacherCourseStateCount();
							teacherStateCount.setTeacherId(id);
							teacherStateCount.setEndedStudentIds(new HashSet<Long>());
						}

						incrementTeacherCourseCount(enrollment, teacherStateCount, startTime, endTime);
						idTeacherBatchMap.put(id, teacherStateCount);
					}
				}
			}
		}
		return idTeacherBatchMap;
	}

	public void incrementTeacherCourseCount(Enrollment enrollment, TeacherCourseStateCount teacherCoursePlanStateCount,
			Long startTime, Long endTime) {

		Boolean stateChangeExists;
		List<EnrollmentStateChangeTime> stateChangeTime = enrollment.getStateChangeTime();
		if (ArrayUtils.isEmpty(stateChangeTime)) {
			stateChangeExists = Boolean.FALSE;
		} else {
			stateChangeExists = Boolean.TRUE;
		}

		if (enrollment.getStatus().equals(EntityStatus.ENDED) && enrollment.getEndTime() > startTime
				&& enrollment.getEndTime() < endTime) {
			teacherCoursePlanStateCount.setEnded(teacherCoursePlanStateCount.getEnded() + 1);
			teacherCoursePlanStateCount.getEndedStudentIds().add(Long.parseLong(enrollment.getUserId()));
			if (stateChangeExists) {
				for (EnrollmentStateChangeTime enrollmentStateChangeTime : stateChangeTime) {
					if (enrollmentStateChangeTime.getNewState().equals(EnrollmentState.REGULAR)
							&& enrollmentStateChangeTime.getChangeTime() > startTime
							&& enrollmentStateChangeTime.getChangeTime() < endTime) {
						teacherCoursePlanStateCount.setEnrolled(teacherCoursePlanStateCount.getEnrolled() + 1);
					}
				}
			}
		}
		if (enrollment.getState().equals(EnrollmentState.REGULAR)) {
			if (stateChangeExists) {
				for (EnrollmentStateChangeTime enrollmentStateChangeTime : stateChangeTime) {
					if (enrollmentStateChangeTime.getNewState().equals(EnrollmentState.REGULAR)
							&& enrollmentStateChangeTime.getChangeTime() > startTime
							&& enrollmentStateChangeTime.getChangeTime() < endTime) {
						teacherCoursePlanStateCount.setEnrolled(teacherCoursePlanStateCount.getEnrolled() + 1);
					}
				}
			}
		}
		if (enrollment.getCreationTime() != null && enrollment.getCreationTime() > startTime
				&& enrollment.getCreationTime() < endTime) {
			teacherCoursePlanStateCount.setPublished(teacherCoursePlanStateCount.getPublished() + 1);
		}
	}
        
        
        
        public List<UserTestResultPojo> getUserTestResultPojos(List<String> batchIds, String userId) throws VException{
            String batchIdsString = String.join(",", batchIds);
            
            String url = lmdEndpoint + "/cmds/test/getUserTestResultPojos?batchIds="+batchIdsString+"&userId="+userId;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type userTestResultPojoListType = new TypeToken<ArrayList<UserTestResultPojo>>(){}.getType();
            
            List<UserTestResultPojo> result = gson.fromJson(jsonString, userTestResultPojoListType);   
            return result;
        }
        
       
        
    public List<DashboardInfoForAM> getDashboardInfosForAM(Long userId){
        
        List<Batch> batches = batchDAO.getBatchForAM(userId.toString());
        Map<String, Set<String>> groupBatchMap = new HashMap<>();
        List<String> batchIds = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(batches)){
            for(Batch batch : batches){
                batchIds.add(batch.getId());                
                if(!groupBatchMap.containsKey(batch.getGroupName())){
                    groupBatchMap.put(batch.getGroupName(), new HashSet<>());
                }
                groupBatchMap.get(batch.getGroupName()).add(batch.getId());
            }
        }
        
        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsForBatches(batchIds);
        
        Map<String, List<Enrollment>> batchEnrollmentMap = new HashMap<>();
        
        if(ArrayUtils.isNotEmpty(enrollments)){
            for(Enrollment enrollment : enrollments){
                if(!batchEnrollmentMap.containsKey(enrollment.getBatchId())){
                    batchEnrollmentMap.put(enrollment.getBatchId(), new ArrayList<>());
                }
                batchEnrollmentMap.get(enrollment.getBatchId()).add(enrollment);
            }
        }
        
        List<DashboardInfoForAM> dashboardInfoForAMs = new ArrayList<>();
        
        for(Map.Entry<String, Set<String>> entry : groupBatchMap.entrySet()){
            DashboardInfoForAM dashboardInfoForAM = new DashboardInfoForAM();
            dashboardInfoForAM.setGroupName(entry.getKey());
            dashboardInfoForAM.setTotalStudents(0);
            dashboardInfoForAM.setActiveStudents(0);            
            if(ArrayUtils.isNotEmpty(entry.getValue())){
                
                for(String batchId: entry.getValue()){
                    
                    List<Enrollment> enrollmentsForBatch = batchEnrollmentMap.get(batchId);
                    
                    if(ArrayUtils.isNotEmpty(enrollmentsForBatch)){
                        for(Enrollment enrollment : enrollmentsForBatch){
                            if(!Role.STUDENT.equals(enrollment.getRole())){
                                continue;
                            }
                            
                            dashboardInfoForAM.setTotalStudents(dashboardInfoForAM.getTotalStudents() + 1);
                            if(EntityStatus.ACTIVE.equals(enrollment.getStatus())){
                                dashboardInfoForAM.setActiveStudents(dashboardInfoForAM.getActiveStudents()+1);
                            }
                        }
                    }
                    
                }
                
            }
            
            dashboardInfoForAMs.add(dashboardInfoForAM);
                

            
            
        }
        
        return dashboardInfoForAMs;
        
    }    
    
    public List<UserBasicInfo> getUserBasicInfosForAM(String userId, String groupName, EntityStatus entityStatus){
        List<Batch> batches = new ArrayList<>();
        
        int start = 0;
        int size = 100;
        while(true){
            List<Batch> tempBatch = batchDAO.getBatchByGroupNameAndAMId(userId,groupName, start, size);
            if(ArrayUtils.isEmpty(tempBatch)){
                break;
            }
            batches.addAll(tempBatch);
            if(tempBatch.size() < size){
                break;
            }
            start = start + size;
            
        }
        
        List<String> batchIds = new ArrayList<>();
        
        for(Batch batch : batches){
            batchIds.add(batch.getId());
        }
        
        List<Enrollment> enrollments = new ArrayList<>();
        start = 0;
        size = 100;
        while(true){
            List<Enrollment> tempEnrollments = enrollmentDAO.getStudentEnrollmentsForBatches(batchIds, start, size);
            
            if(ArrayUtils.isEmpty(tempEnrollments)){
                break;
            }
            
            enrollments.addAll(tempEnrollments);
            
            if(tempEnrollments.size() < size){
                break;
            }
            
            start = start + size;
            
        }
        
        Set<String> userIds = new HashSet<>();
        
        for(Enrollment enrollment : enrollments){
            if(EntityStatus.ACTIVE.equals(entityStatus)){
                if(EntityStatus.ACTIVE.equals(enrollment.getStatus())){
                    userIds.add(enrollment.getUserId());
                }
            }else{
                userIds.add(enrollment.getUserId());
            }
        }
        
        List<UserBasicInfo> userBasicInfos = fosUtils.getUserBasicInfosSet(userIds, true);
        
        return userBasicInfos;
        
    }
    
        
    public List<GTTAttendeeSessionInfo> getAttendeeDetailsInfo(List<String> batchIds, String userId) throws VException{
            String batchIdsString = String.join(",", batchIds);
            String url = schedulingEndpoint + "/onetofew/session/getAttendeeInfoForBatchAndUser?batchIds="+batchIdsString+"&userId="+userId;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            logger.info("Call response {}",resp);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type userTestResultPojoListType = new TypeToken<ArrayList<GTTAttendeeSessionInfo>>(){}.getType();
            
            List<GTTAttendeeSessionInfo> result = gson.fromJson(jsonString, userTestResultPojoListType);   
            return result;
    }


    public List<UserSessionAttendance> getSessionAttendeeDetailsInfo(List<String> coursePlanIds, String userId) throws VException{
            String coursePlanIdsString = String.join(",", coursePlanIds);
            
            String url = schedulingEndpoint + "/session/getCoursePlanSessionAttendeeInfos?coursePlanIds="+coursePlanIdsString+"&userId="+userId;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type userTestResultPojoListType = new TypeToken<ArrayList<UserSessionAttendance>>(){}.getType();
            
            List<UserSessionAttendance> result = gson.fromJson(jsonString, userTestResultPojoListType);   
            return result;
    }
    
    public List<StudentAcademicOTMInfo> getStudentAcademicOTMInfos(String userId) throws VException{
		logger.info("IN SUBSCRIPTION getStudentAcademicOTMInfos");
		// Fetch all enrollments for userId in batches of 100
		List<Enrollment> enrollments = new ArrayList<>();
		int start = 0;
        int size = 100;

        while(true)
        {
            List<Enrollment> tempEnrollments = enrollmentDAO.getEnrollmentsForUserId(userId, start, size);
            if(ArrayUtils.isEmpty(tempEnrollments))
            {
                break;
            }
            enrollments.addAll(tempEnrollments);
            if(tempEnrollments.size() < size){
                break;
            }
            start = start + size;
        }

        logger.info("Enrollments " + enrollments.toString());
        logger.info("Enrollments size is " + enrollments.size());

        // Fetching batchIds from enrollments
        Set<String> batchIds = new HashSet<>();

        // Filtering out REGULAR Enrollments
        for(Enrollment enrollment : enrollments){
            if(!EnrollmentState.REGULAR.equals(enrollment.getState())){
                continue;
            }
            batchIds.add(enrollment.getBatchId());
        }

        if(ArrayUtils.isEmpty(batchIds)){
            return new ArrayList<>();
        }
        
        List<Batch> batches = batchDAO.getBatchByIds(new ArrayList<>(batchIds));
		logger.info("Batches " + batches.toString());
		logger.info("Batches size " + batches.size());

		// Creating a map with key as batchGroupName and value as unique list(set) of its corresponding batchIds
        Map<String, Set<String>> groupBatchMap = new HashMap<>();

        for(Batch batch : batches){
            if(StringUtils.isEmpty(batch.getGroupName())){
                continue;
            }
            
            if(!groupBatchMap.containsKey(batch.getGroupName())){
                groupBatchMap.put(batch.getGroupName(), new HashSet<>());
            }
            groupBatchMap.get(batch.getGroupName()).add(batch.getId());
        }

        // Fetch userBasicInfo from Redis
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);
        List<StudentAcademicOTMInfo> results = new ArrayList<>();
        logger.info("groupBatchMap : " + groupBatchMap.toString());
        for(Map.Entry<String, Set<String>> entry : groupBatchMap.entrySet()){
            StudentAcademicOTMInfo studentAcademicOTMInfo = new StudentAcademicOTMInfo();
            studentAcademicOTMInfo.setGroupName(entry.getKey());
            studentAcademicOTMInfo.setEmailId(userBasicInfo.getEmail());
            List<UserTestResultPojo> userTestResultPojos = getUserTestResultPojos(new ArrayList<>(entry.getValue()), userId);
            logger.info("LMS call done " + userTestResultPojos.toString());
            List<GTTAttendeeSessionInfo> gTTAttendeeSessionInfos = getAttendeeDetailsInfo(new ArrayList<>(entry.getValue()), userId);
			logger.info("Subscription call done " + gTTAttendeeSessionInfos.toString());
			gTTAttendeeSessionInfos.sort(new Comparator<GTTAttendeeSessionInfo>(){
                @Override
                public int compare(GTTAttendeeSessionInfo o1, GTTAttendeeSessionInfo o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                    //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
                }

            });
            
            
            int gttIndex = 0;
            List<StudentAcademicOneCycleInfo> studentAcademicOneCycleInfos = new ArrayList<>();
            StudentAcademicOneCycleInfo studentAcademicOneCycleInfo = new StudentAcademicOneCycleInfo();
            studentAcademicOneCycleInfo.setPreMainTestResultObj(new ArrayList<>()); 
            List<UserSessionAttendance> sessionAttendances = new ArrayList<>();
            Long timeLimit = null;
            for(UserTestResultPojo userTestResultPojo : userTestResultPojos){
                
                if(EnumBasket.TestTagType.PHASE.equals(userTestResultPojo.getTag()) || EnumBasket.TestTagType.UNIT.equals(userTestResultPojo.getTag())){
                    
                    timeLimit = userTestResultPojo.getTime();
                    int totalSessions = 0;
                    int attendedSessions = 0;
                    while(true){
                        if(gttIndex > gTTAttendeeSessionInfos.size() -1){
                            break;
                        }
                        GTTAttendeeSessionInfo gTTAttendeeSessionInfo = gTTAttendeeSessionInfos.get(gttIndex);
                        if(gTTAttendeeSessionInfo.getStartTime() < timeLimit){
                            UserSessionAttendance userSessionAttendance = new UserSessionAttendance();
                            userSessionAttendance.setContextIds(gTTAttendeeSessionInfo.getBatchIds());
                            userSessionAttendance.setSessionTitle(gTTAttendeeSessionInfo.getTitle());
                            userSessionAttendance.setStartTime(gTTAttendeeSessionInfo.getStartTime());
                            totalSessions++;
                            if(ArrayUtils.isNotEmpty(gTTAttendeeSessionInfo.getJoinTimes())){
                                attendedSessions++;
                                userSessionAttendance.setAttended(true);
                            }
                            sessionAttendances.add(userSessionAttendance);
                            gttIndex++;
                            
                        }else{
                            break;
                        }
                        
                    }
                    if(totalSessions == 0){
                        studentAcademicOneCycleInfo.setAttendance(0f);
                    }else{
                        studentAcademicOneCycleInfo.setAttendance((float)attendedSessions*100/totalSessions);
                    }
                    studentAcademicOneCycleInfo.setMainTestResultObj(userTestResultPojo);
                    studentAcademicOneCycleInfo.setSessionAttendances(sessionAttendances);
                    studentAcademicOneCycleInfos.add(studentAcademicOneCycleInfo);
                    sessionAttendances = new ArrayList<>();
                    studentAcademicOneCycleInfo = new StudentAcademicOneCycleInfo();
                    studentAcademicOneCycleInfo.setPreMainTestResultObj(new ArrayList<>());
                }else{
                    studentAcademicOneCycleInfo.getPreMainTestResultObj().add(userTestResultPojo);
                }
                
            }
            
            if(gttIndex < gTTAttendeeSessionInfos.size()){
                
                int totalSessions = 0;
                int attendedSessions = 0;
                while(true){
                    if(gttIndex == gTTAttendeeSessionInfos.size()){
                        break;
                    }
                    GTTAttendeeSessionInfo gTTAttendeeSessionInfo = gTTAttendeeSessionInfos.get(gttIndex);
                    UserSessionAttendance userSessionAttendance = new UserSessionAttendance();
                    userSessionAttendance.setContextIds(gTTAttendeeSessionInfo.getBatchIds());
                    userSessionAttendance.setSessionTitle(gTTAttendeeSessionInfo.getTitle());
                    userSessionAttendance.setStartTime(gTTAttendeeSessionInfo.getStartTime());                    
                    totalSessions++;
                    if(ArrayUtils.isNotEmpty(gTTAttendeeSessionInfo.getJoinTimes())){
                        userSessionAttendance.setAttended(true);
                        attendedSessions++;
                    }
                    sessionAttendances.add(userSessionAttendance);
                    gttIndex++;   
                }    
                if(totalSessions == 0){
                    studentAcademicOneCycleInfo.setAttendance(0f);
                }else{
                    studentAcademicOneCycleInfo.setAttendance((float)attendedSessions*100/totalSessions);
                }
            }
            
            if(studentAcademicOneCycleInfo.getAttendance() == null){
                studentAcademicOneCycleInfo.setAttendance(0f);
            }
            
            studentAcademicOneCycleInfo.setSessionAttendances(sessionAttendances);
            studentAcademicOneCycleInfos.add(studentAcademicOneCycleInfo);
            studentAcademicOTMInfo.setCycles(studentAcademicOneCycleInfos);
            results.add(studentAcademicOTMInfo);
        }
        
		logger.info("getStudentAcademicOTMInfos results " + results.toString());
        return results;      
        
    }
    
    
    public List<StudentOTMEnrollmentInfo> getStudentOTMEnrollments(String userId){
        
        List<Enrollment> enrollments = new ArrayList<>();
        
        int start = 0;
        int size = 100;
        
        while(true){
            List<Enrollment> tempEnrollments = enrollmentDAO.getEnrollmentsForUserId(userId, start, size);
            
            if(ArrayUtils.isEmpty(tempEnrollments)){
                break;
            }
            
            enrollments.addAll(tempEnrollments);
            
            if(tempEnrollments.size() < size){
                break;
            }
            
            start = start + size;
            
        }
        
        Set<String> batchIds = new HashSet<>();
        for(Enrollment enrollment : enrollments){
            if(EnrollmentState.REGULAR.equals(enrollment.getState())){
                batchIds.add(enrollment.getBatchId());
            }
        }
        
        List<Batch> batches = batchDAO.getBatchByIds(new ArrayList<>(batchIds));
        
        Map<String, UserBasicInfo> userDetails = new HashMap<>();
        
        if(ArrayUtils.isEmpty(batches)){
            return new ArrayList<>();
        }
        List<StudentOTMEnrollmentInfo> studentOTMEnrollmentInfos = new ArrayList<>();
        for(Batch batch : batches){
            
            StudentOTMEnrollmentInfo studentOTMEnrollmentInfo = new StudentOTMEnrollmentInfo();
            studentOTMEnrollmentInfo.setBatchId(batch.getId());
            studentOTMEnrollmentInfo.setGroupName(batch.getGroupName());
            studentOTMEnrollmentInfo.setStartDate(batch.getStartTime());
            if(ArrayUtils.isNotEmpty(batch.getTeacherIds())){
                List<String> teacherIds = new ArrayList<>(batch.getTeacherIds());
                studentOTMEnrollmentInfo.setTeacherId(teacherIds.get(0));
                if(!userDetails.containsKey(studentOTMEnrollmentInfo.getTeacherId())){
                    userDetails.put(studentOTMEnrollmentInfo.getTeacherId(), fosUtils.getUserBasicInfo(studentOTMEnrollmentInfo.getTeacherId(), false));
                }
                studentOTMEnrollmentInfo.setTeacherName(userDetails.get(studentOTMEnrollmentInfo.getTeacherId()).getFullName());
            }

            if(ArrayUtils.isNotEmpty(batch.getSessionPlan())){
                List<String> dailySchedules = new ArrayList<>();
                for(SessionPlanPojo sessionPlanPojo : batch.getSessionPlan()){
                    String timings = String.join(",", sessionPlanPojo.getTimings());
                    dailySchedules.add(timings + " " + sessionPlanPojo.getDay());
                }
                
                studentOTMEnrollmentInfo.setSchedule(dailySchedules);
                
            }
            
            studentOTMEnrollmentInfos.add(studentOTMEnrollmentInfo);
            
        }
        
        return studentOTMEnrollmentInfos;
        
    }
    
    public List<StudentCoursePlanInfo> getStudentCoursePlanInfos(String userId){
        List<CoursePlan> coursePlans = coursePlanDAO.getCoursePlansForUser(Long.parseLong(userId));
        
        if(ArrayUtils.isEmpty(coursePlans)){
            return new ArrayList<>();
        }
        List<StudentCoursePlanInfo> studentCoursePlanInfos = new ArrayList<>();
        Map<Long, UserBasicInfo> userDetails = new HashMap<>();

        for(CoursePlan coursePlan : coursePlans){
            
            StudentCoursePlanInfo studentCoursePlanInfo = new StudentCoursePlanInfo();
            studentCoursePlanInfo.setIntendedHours(coursePlan.getIntendedHours());
            studentCoursePlanInfo.setStartDate(coursePlan.getRegularSessionStartDate());
            studentCoursePlanInfo.setTitle(coursePlan.getTitle());
            
            if(coursePlan.getTeacherId() != null){
                if(!userDetails.containsKey(coursePlan.getTeacherId())){
                    userDetails.put(coursePlan.getTeacherId(), fosUtils.getUserBasicInfo(coursePlan.getTeacherId(), false));
                }
                
                if(userDetails.get(coursePlan.getTeacherId()) != null){
                    studentCoursePlanInfo.setTeacherId(coursePlan.getTeacherId().toString());
                    studentCoursePlanInfo.setTeacherName(userDetails.get(coursePlan.getTeacherId()).getFullName());
                }
                
            }
            studentCoursePlanInfos.add(studentCoursePlanInfo);
            
        }
        
        return studentCoursePlanInfos;
        
        
    }
    
    public List<StudentAcademicOTOInfo> getStudentAcademinOTOInfos(String userId) throws VException{
        List<CoursePlan> coursePlans = coursePlanDAO.getCoursePlansForUser(Long.parseLong(userId));
        
        if(ArrayUtils.isEmpty(coursePlans)){
            return new ArrayList<>();
        }
        List<String> coursePlanIds = new ArrayList<>();
        Map<String, CoursePlan> cMap = new HashMap<>();
        for(CoursePlan coursePlan : coursePlans){
            coursePlanIds.add(coursePlan.getId());
            cMap.put(coursePlan.getId(), coursePlan);
        }
        
        List<UserTestResultPojo> userTestResults = getUserTestResultPojosForOTO(coursePlanIds, userId);
        
        Map<String, List<UserTestResultPojo>> coursePlanTestMap = new HashMap<>();
        
        for(UserTestResultPojo userTestResultPojo : userTestResults){
            if(!coursePlanTestMap.containsKey(userTestResultPojo.getBatchId())){
                coursePlanTestMap.put(userTestResultPojo.getBatchId(), new ArrayList<>());
            }
            
            coursePlanTestMap.get(userTestResultPojo.getBatchId()).add(userTestResultPojo);
            
        }
        logger.info("cplan map : "+cMap);
        List<UserSessionAttendance> userSessionAttendances = getSessionAttendeeDetailsInfo(coursePlanIds, userId);
        
        Map<String, List<UserSessionAttendance>> attendanceMap = new HashMap<>();
        
        if(ArrayUtils.isNotEmpty(userSessionAttendances)){
            for(UserSessionAttendance userSessionAttendance : userSessionAttendances){
                if(ArrayUtils.isEmpty(userSessionAttendance.getContextIds())){
                    continue;
                }
                List<String> contextIds = new ArrayList<>(userSessionAttendance.getContextIds());
                if(!attendanceMap.containsKey(contextIds.get(0))){
                    attendanceMap.put(contextIds.get(0), new ArrayList<>());
                }
                attendanceMap.get(contextIds.get(0)).add(userSessionAttendance);
            }
        }
        logger.info("attentdenceMap: "+attendanceMap);
        List<StudentAcademicOTOInfo> studentAcademicOTOInfos = new ArrayList<>();
        for(String coursePlanId : coursePlanIds){
            StudentAcademicOTOInfo studentAcademicOTOInfo = new StudentAcademicOTOInfo();
            logger.info("Key: "+coursePlanId);
            if(!cMap.containsKey(coursePlanId)){
                continue;
            }
            
            studentAcademicOTOInfo.setCoursePlanId(coursePlanId);
            studentAcademicOTOInfo.setTitle(cMap.get(coursePlanId).getTitle());
            studentAcademicOTOInfo.setUserTestResults(coursePlanTestMap.get(coursePlanId));
            
            if(ArrayUtils.isNotEmpty(attendanceMap.get(coursePlanId))){
                studentAcademicOTOInfo.setSessionAttendances(attendanceMap.get(coursePlanId));
                int total = 0;
                int present = 0;
                for(UserSessionAttendance userSessionAttendance : attendanceMap.get(coursePlanId)){
                    total++;
                    if(userSessionAttendance.isAttended()){
                        present++;
                    }
                }
                
                if(total == 0){
                    studentAcademicOTOInfo.setAttendance(0f);
                }else{
                    studentAcademicOTOInfo.setAttendance((float)present*100/total);
                }
                
            }else{
                studentAcademicOTOInfo.setAttendance(0f);
            }
            
            studentAcademicOTOInfos.add(studentAcademicOTOInfo);
        }
        
        
        return studentAcademicOTOInfos;
        
    }
    
    public List<UserTestResultPojo> getUserTestResultPojosForOTO(List<String> coursePlanIds, String userId) throws VException{
        String coursePlanIdsString = String.join(",", coursePlanIds);

        String url = lmdEndpoint + "/cmds/test/getUserTestResultPojosForOTO?coursePlanIds="+coursePlanIdsString+"&userId="+userId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type userTestResultPojoListType = new TypeToken<ArrayList<UserTestResultPojo>>(){}.getType();

        List<UserTestResultPojo> result = gson.fromJson(jsonString, userTestResultPojoListType);   
        return result;
    }

    public List<StudentCoursePlanInfo> getStudentsCoursePlanInfos(GetCoursePlansReq getCoursePlansReq) {

        List<CoursePlan> coursePlans = coursePlanDAO.getCoursePlanBasicInfos(getCoursePlansReq);
        if (ArrayUtils.isEmpty(coursePlans)) {
            return new ArrayList<>();
        }
        List<StudentCoursePlanInfo> studentCoursePlanInfos = new ArrayList<>();
        Map<Long, UserBasicInfo> userDetails = new HashMap<>();

        for (CoursePlan coursePlan : coursePlans) {

            StudentCoursePlanInfo studentCoursePlanInfo = new StudentCoursePlanInfo();
            studentCoursePlanInfo.setIntendedHours(coursePlan.getIntendedHours());
            studentCoursePlanInfo.setStartDate(coursePlan.getRegularSessionStartDate());
            studentCoursePlanInfo.setTitle(coursePlan.getTitle());

            if (coursePlan.getTeacherId() != null) {
                if (!userDetails.containsKey(coursePlan.getTeacherId())) {
                    userDetails.put(coursePlan.getTeacherId(), fosUtils.getUserBasicInfo(coursePlan.getTeacherId(), false));
                }

                if (userDetails.get(coursePlan.getTeacherId()) != null) {
                    studentCoursePlanInfo.setTeacherId(coursePlan.getTeacherId().toString());
                    studentCoursePlanInfo.setTeacherName(userDetails.get(coursePlan.getTeacherId()).getFullName());
                }

            }
            studentCoursePlanInfos.add(studentCoursePlanInfo);

        }

        return studentCoursePlanInfos;
    }
        
}
