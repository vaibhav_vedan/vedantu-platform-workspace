package com.vedantu.subscription.managers;

import com.vedantu.subscription.dao.GameRewardItemDao;
import com.vedantu.subscription.entities.mongo.GameRewardItem;
import com.vedantu.subscription.pojo.RewardDistributionInfo;
import com.vedantu.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

/**
 * @author mano
 */
@Service
public class GameRewardItemManager {

    @Autowired
    private GameRewardItemDao gameRewardItemDao;

    public void incrementCountForGame(String itemName, String gameId) {
        GameRewardItem entity = gameRewardItemDao.getItemByName(itemName);
        if (entity == null) {
            entity = new GameRewardItem();
            entity.setName(itemName);
            HashSet<RewardDistributionInfo> infos = new HashSet<>();
            RewardDistributionInfo info = new RewardDistributionInfo();
            info.setCount(1);
            info.setGameId(gameId);
            entity.setDistributionInfo(infos);
            gameRewardItemDao.save(entity);
        }
        Set<RewardDistributionInfo> distributionInfo = CollectionUtils
                .defaultIfNull(entity.getDistributionInfo(), HashSet::new);
        RewardDistributionInfo info = new RewardDistributionInfo();
        info.setGameId(gameId);
        if (distributionInfo.contains(info)) {
            Optional<RewardDistributionInfo> optional = distributionInfo.stream().filter(e -> e.getGameId().equals(gameId)).findFirst();
            RewardDistributionInfo rewardDistributionInfo = optional.orElseGet(RewardDistributionInfo::new);
            rewardDistributionInfo.setGameId(gameId);
            rewardDistributionInfo.setCount(rewardDistributionInfo.getCount() + 1);
        } else {
            info.setCount(1);
            distributionInfo.add(info);
        }
        gameRewardItemDao.save(entity);
    }
}
