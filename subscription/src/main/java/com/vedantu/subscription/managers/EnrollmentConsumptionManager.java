package com.vedantu.subscription.managers;

import java.lang.reflect.Field;
import java.util.List;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.User.Role;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.request.OTMConsumptionReq;
import com.vedantu.dinero.request.OTMRefundReq;
import com.vedantu.dinero.response.OTMConsumptionPaymentRes;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.EnrollmentConsumptionDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.EnrollmentTransactionDAO;
import com.vedantu.subscription.dao.RegistrationDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.sql.EnrollmentConsumption;
import com.vedantu.subscription.entities.sql.EnrollmentTransaction;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.enums.EnrollmentTransactionType;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.subscription.viewobject.request.CreateConsumptionTransactionReq;
import com.vedantu.subscription.viewobject.request.EnrollmentRefundReq;
import com.vedantu.subscription.viewobject.request.MakePaymentRequest;
import com.vedantu.util.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.logging.log4j.Logger;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

@Service
public class EnrollmentConsumptionManager {

    @Autowired
    public EnrollmentConsumptionDAO enrollmentConsumptionDAO;

    @Autowired
    public EnrollmentTransactionDAO enrollmentTransactionDAO;

    @Autowired
    public EnrollmentDAO enrollmentDAO;

    @Autowired
    public CourseDAO courseDAO;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private RegistrationDAO registrationDAO;

    @Autowired
    public PaymentManager paymentManager;

    @Autowired
    private SqlSessionFactory sqlSessionfactory;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private LogFactory logFactory;

    Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EnrollmentConsumptionManager.class);

    String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    private static Long maxRefundTime = 5 * DateTimeUtils.MILLIS_PER_WEEK;

    private static Long maxTimeForContentOnly = (long) (15 * DateTimeUtils.MILLIS_PER_DAY);

    public void performEndBatchConsumption(String batchId) {
        List<String> batchIds = new ArrayList<>();
        batchIds.add(batchId);
        List<EnrollmentConsumption> enrollmentConsumptionList = enrollmentConsumptionDAO.getByBatchId(batchIds, null);

        if (ArrayUtils.isNotEmpty(enrollmentConsumptionList)) {
            for (EnrollmentConsumption enrollmentConsumption : enrollmentConsumptionList) {
                try {
                    endBatchConsumptionForEnrollment(enrollmentConsumption);
                } catch (Exception e) {
                    logger.error("END Batch Consumption failed for: " + enrollmentConsumption + " error:" + e.getMessage());
                }
            }
        }
    }

    public void endBatchConsumptionForEnrollment(EnrollmentConsumption enrollmentConsumption) {

        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();
            enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentConsumption.getEnrollmentId(), true, session, LockMode.PESSIMISTIC_WRITE);
            logger.info("EnrollmentConsumption Before: " + enrollmentConsumption);
            EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

            if (!enrollmentConsumption.isBatchConsumptionDone()) {

                enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaidLeft() + enrollmentConsumption.getRegAmtLeft());
                enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft() + enrollmentConsumption.getRegAmtPLeft());
                enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft() + enrollmentConsumption.getRegAmtNPLeft());
                enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft() + enrollmentConsumption.getRegAmtLeftFromDiscount());
                enrollmentTransaction.setConsumptionType(EntityStatus.ENDED);
                enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.END_BATCH_CONSUMPTION);
                enrollmentTransaction.setEnrollmentTransactionContextId(enrollmentConsumption.getEnrollmentId());
                enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());

                enrollmentConsumption.setAmtPaidLeft(0);
                enrollmentConsumption.setAmtPaidNPLeft(0);
                enrollmentConsumption.setAmtPaidPLeft(0);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
                enrollmentConsumption.setRegAmtLeft(0);
                enrollmentConsumption.setRegAmtNPLeft(0);
                enrollmentConsumption.setRegAmtPLeft(0);
                enrollmentConsumption.setRegAmtLeftFromDiscount(0);
                enrollmentConsumption.setBatchConsumptionDone(true);

                takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);

                logger.info("EnrollmentTransaction : " + enrollmentTransaction);
                logger.info("EnrollmentConsumption After : " + enrollmentConsumption);

                enrollmentConsumptionDAO.update(enrollmentConsumption, session);

                if (enrollmentTransaction.getAmtPaid() > 0) {
                    enrollmentTransactionDAO.create(enrollmentTransaction, session);
                }

                OTMConsumptionReq oTMConsumptionReq = new OTMConsumptionReq();
                oTMConsumptionReq.setAmount(enrollmentTransaction.getAmtPaid());
                oTMConsumptionReq.setPromotionalAmt(enrollmentTransaction.getAmtPaidP() + enrollmentTransaction.getAmtPaidFromDiscount());
                oTMConsumptionReq.setNonPromotionalAmt(enrollmentTransaction.getAmtPaidNP());
                oTMConsumptionReq.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.END_BATCH_CONSUMPTION);
                oTMConsumptionReq.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
                transaction.commit();
                makeDineroTransaction(oTMConsumptionReq);
            } else {
                transaction.commit();
            }

        } catch (Exception ex) {
            session.getTransaction().rollback();
            logger.error("Exception while end batch: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void performConsumptionForContentOnlyBatch(String batchId) {
        List<String> batchIds = new ArrayList<>();
        batchIds.add(batchId);
        Long backTime = System.currentTimeMillis() - maxTimeForContentOnly;
        List<EnrollmentConsumption> enrollmentConsumptionList = enrollmentConsumptionDAO.getByBatchIdForContentOnly(batchIds, backTime, null);

        if (ArrayUtils.isNotEmpty(enrollmentConsumptionList)) {
            for (EnrollmentConsumption enrollmentConsumption : enrollmentConsumptionList) {
                try {
                    performConsumptionForContentOnlyBatchEnrollment(enrollmentConsumption);
                } catch (Exception e) {
                    logger.error("END Batch Consumption failed for: " + enrollmentConsumption + " error:" + e.getMessage());
                }
            }
        }
    }

    public void performConsumptionForContentOnlyBatchEnrollment(EnrollmentConsumption enrollmentConsumption) {

        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();
            enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentConsumption.getEnrollmentId(), true, session, LockMode.PESSIMISTIC_WRITE);
            logger.info("EnrollmentConsumption After : " + enrollmentConsumption);

            if (!enrollmentConsumption.isEnrollmentEnded()) {
                //return;

                EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
                int amountToBePaid = enrollmentConsumption.getHourlyrate() * 24;
                updateTransactionForContentConsumption(enrollmentConsumption, enrollmentTransaction, amountToBePaid);
                /*
                enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaidLeft() + enrollmentConsumption.getRegAmtLeft());
                enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft() + enrollmentConsumption.getRegAmtPLeft());
                enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft() + enrollmentConsumption.getRegAmtNPLeft());
                enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft() + enrollmentConsumption.getRegAmtLeftFromDiscount());
                enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.CONTENT_BATCH_CONSUMPTION);
                enrollmentTransaction.setEnrollmentTransactionContextId(enrollmentConsumption.getEnrollmentId());
                enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());

                enrollmentConsumption.setAmtPaidLeft(0);
                enrollmentConsumption.setAmtPaidNPLeft(0);
                enrollmentConsumption.setAmtPaidPLeft(0);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
                enrollmentConsumption.setRegAmtLeft(0);
                enrollmentConsumption.setRegAmtNPLeft(0);
                enrollmentConsumption.setRegAmtPLeft(0);
                enrollmentConsumption.setRegAmtLeftFromDiscount(0);

                 */
                enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.CONTENT_BATCH_CONSUMPTION);
                enrollmentTransaction.setEnrollmentTransactionContextId(DateTimeUtils.getISTDayStartTime(System.currentTimeMillis()).toString());
                enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);

                enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                if (enrollmentTransaction.getAmtPaid() > 0) {
                    enrollmentTransactionDAO.create(enrollmentTransaction, session);
                }

                OTMConsumptionReq oTMConsumptionReq = new OTMConsumptionReq();
                oTMConsumptionReq.setAmount(enrollmentTransaction.getAmtPaid());
                oTMConsumptionReq.setPromotionalAmt(enrollmentTransaction.getAmtPaidP() + enrollmentTransaction.getAmtPaidFromDiscount());
                oTMConsumptionReq.setNonPromotionalAmt(enrollmentTransaction.getAmtPaidNP());
                oTMConsumptionReq.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.END_BATCH_CONSUMPTION);
                oTMConsumptionReq.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
                transaction.commit();
                if (amountToBePaid > 0) {
                    makeDineroTransaction(oTMConsumptionReq);
                }
            } else {
                transaction.commit();
            }

        } catch (Exception ex) {
            session.getTransaction().rollback();
            logger.error("Exception while content only: " + ex.getMessage());
        } finally {
            session.close();
        }
    }

    public void updateTransactionForContentConsumption(EnrollmentConsumption enrollmentConsumption, EnrollmentTransaction enrollmentTransaction, int amount) throws InternalServerErrorException {

        if (enrollmentConsumption.getAmtPaidLeft() <= amount) {
            enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaidLeft());
            enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
            enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());
            enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());
            enrollmentConsumption.setAmtPaidLeft(0);
            enrollmentConsumption.setAmtPaidNPLeft(0);
            enrollmentConsumption.setAmtPaidPLeft(0);
            enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
        } else {
            enrollmentTransaction.setAmtPaid(amount);
            enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - amount);
            if (enrollmentConsumption.getDiscountToAmtRatio() == null) {
                enrollmentConsumption.setDiscountToAmtRatio(0f);
            }
            int discountAmount = (int) (enrollmentConsumption.getDiscountToAmtRatio() * enrollmentTransaction.getAmtPaid());

            if (enrollmentConsumption.getAmtPaidFromDiscountLeft() >= discountAmount) {
                enrollmentTransaction.setAmtPaidFromDiscount(discountAmount);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - enrollmentTransaction.getAmtPaidFromDiscount());
            } else {
                enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
                enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
            }
            int amountLeft = amount - enrollmentTransaction.getAmtPaidFromDiscount();
            if (enrollmentConsumption.getAmtPaidPLeft() >= amountLeft) {
                enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() - amountLeft);
                enrollmentTransaction.setAmtPaidP(amountLeft);
            } else {
                enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());
                enrollmentConsumption.setAmtPaidPLeft(0);
                amountLeft = amountLeft - enrollmentTransaction.getAmtPaidP();

                if (enrollmentConsumption.getAmtPaidNPLeft() >= amountLeft) {
                    enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - amountLeft);
                    enrollmentTransaction.setAmtPaidNP(amountLeft);
                } else {
                    enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());
                    enrollmentConsumption.setAmtPaidNP(0);
                    amountLeft = amountLeft - enrollmentTransaction.getAmtPaidNP();
                    if (amountLeft > enrollmentConsumption.getAmtPaidFromDiscountLeft()) {
                        throw new InternalServerErrorException(ErrorCode.INSUFFICIENT_CREDITS, "Inconsistency in amount");
                    } else {
                        enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - amountLeft);
                        enrollmentTransaction.setAmtPaidFromDiscount(enrollmentTransaction.getAmtPaidFromDiscount() + amountLeft);
                    }
                }

            }

        }

    }

    /* 
    public PlatformBasicResponse makePayment(MakePaymentRequest makePaymentRequest) throws VException{
        EnrollmentConsumptionPayment enrollmentConsumptionPayment = new EnrollmentConsumptionPayment();
        if(!EnrollmentTransactionContextType.REGISTRATION_PAYMENT.equals(makePaymentRequest.getEnrollmentTransactionContextType())){
            Session session = sqlSessionfactory.getSessionFactory().openSession();
            Transaction transaction = session.getTransaction();
            try{
                transaction.begin();
                
                EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(makePaymentRequest.getEnrollmentId(), null, session, LockMode.PESSIMISTIC_WRITE);
                
                if(enrollmentConsumption==null){
                    
                    return new PlatformBasicResponse(false, null, "EnrollmentConsumption not available");
   
                }
                
                if(enrollmentConsumption.getHourlyrate() == null){
                        logger.error("Hourly Rate not available");
                        return new PlatformBasicResponse(false, null, "Hourly rate not available");
                }
                
                if(enrollmentConsumption.getDiscountToAmtRatio()==null){
                    enrollmentConsumption.setDiscountToAmtRatio((float)makePaymentRequest.getAmtFromDiscount()/makePaymentRequest.getAmt());
                }
                
                enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                
                transaction.commit();
                
                
            }catch(Exception ex){
                session.getTransaction().rollback();
                logger.error("Exception: makePayment-  "+ex.getMessage());
            }finally{
                session.close();
            }
            
            enrollmentConsumptionPayment.setAmt(makePaymentRequest.getAmt());
            enrollmentConsumptionPayment.setAmtP(makePaymentRequest.getAmtP());
            enrollmentConsumptionPayment.setAmtNP(makePaymentRequest.getAmtNP());
            enrollmentConsumptionPayment.setAmtFromDiscount(makePaymentRequest.getAmtFromDiscount());
             
        }else {
            return new PlatformBasicResponse(false, null, "Registration payment only possible for CSV enrollment");
        }
        
        enrollmentConsumptionPayment.setCourseId(makePaymentRequest.getCourseId());
        enrollmentConsumptionPayment.setEnrollmentTransactionType(EnrollmentTransactionType.CREDIT);
        enrollmentConsumptionPayment.setEnrollmentTransactionContextType(makePaymentRequest.getEnrollmentTransactionContextType());
        enrollmentConsumptionPayment.setEnrollmentId(makePaymentRequest.getEnrollmentId());
        enrollmentConsumptionPayment.setBatchId(makePaymentRequest.getBatchId());
        enrollmentConsumptionPayment.setEnrollmentTransactionContextId(makePaymentRequest.getEnrollmentTransactionContextId());
        
        boolean result = makeEnrollmentPayment(enrollmentConsumptionPayment, null);
        
        if(result){
            logger.info("payment successful for : "+makePaymentRequest);
        }else{
            logger.info("payment failed for: "+makePaymentRequest);
        }
        
        return new PlatformBasicResponse(result, result?"Successful":null, result?null:"Failed");
        
    }*/
    public EnrollmentConsumption addOrUpdateEnrollmentConsumption(EnrollmentConsumption enrollmentConsumption, Session session) {
        Long id = enrollmentConsumption.getId();
        if (id == null) {
            enrollmentConsumptionDAO.create(enrollmentConsumption, session);
        } else {
            EnrollmentConsumption enrollmentConsumption1 = (EnrollmentConsumption) enrollmentConsumptionDAO.getById(id, true, session);
            enrollmentConsumption = addUpdates(enrollmentConsumption1, enrollmentConsumption);
            enrollmentConsumptionDAO.update(enrollmentConsumption, session);
            session.getTransaction().commit();
            session.close();
        }
        return enrollmentConsumption;
    }

    /*    public void performSessionConsumptionForSession(SessionSnapshot sessionSnapshot) throws VException{
        
        boolean isTrialSession = false;
        if(OTMSessionType.EXTRA_TRIAL.equals(sessionSnapshot.getoTMSessionType())
                || OTMSessionType.TRIAL.equals(sessionSnapshot.getoTMSessionType())){
            
            isTrialSession = true;
            
        }
        
        
        List<EnrollmentConsumption> enrollmentConsumptionList = enrollmentConsumptionDAO.getByBatchId(sessionSnapshot.getBatchIds(), null);
        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsOfBatch(sessionSnapshot.getBatchIds());
        
        logger.info("EnrollmentConsumptionList: "+enrollmentConsumptionList);
        logger.info("Enrollments: "+enrollments);
        logger.info("isTrialSession: "+isTrialSession);
        
        Map<String, Enrollment> enrollmentMap = new HashMap();
        
        for(Enrollment enrollment: enrollments){
            enrollmentMap.put(enrollment.getId(), enrollment);
        }
       
        for(EnrollmentConsumption enrollmentConsumption : enrollmentConsumptionList){
            
            Enrollment enrollment = enrollmentMap.get(enrollmentConsumption.getEnrollmentId());
            logger.info("enrollment "+enrollment);
            logger.info("enrollmentConsumption "+enrollmentConsumption);
            if(enrollment == null){
                logger.error("Enrollment not found for enrollmentConsumption: "+enrollmentConsumption.getEnrollmentId());
                continue;
            }
            
            if(enrollmentConsumption.isEnrollmentEnded()){
                logger.info("Enrollment Ended");
                continue;
            }
            
            if(!isTrialSession){
                //To do add regular and trial check for enrollment
                // && EnrollmentState.REGULAR.equals(enrollment.getState())
                if(enrollmentConsumption.getHourlyrate()==null){
                    logger.error("Hourly rate not available for enrollmentId : "+enrollmentConsumption.getEnrollmentId());    
                    continue;
                }
                
                EnrollmentConsumptionPayment enrollmentConsumptionPayment = new EnrollmentConsumptionPayment();
                enrollmentConsumptionPayment.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
                enrollmentConsumptionPayment.setBatchId(enrollmentConsumption.getBatchId());
                enrollmentConsumptionPayment.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.SESSION_CONSUMPTION);
                enrollmentConsumptionPayment.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentConsumptionPayment.setAmt((int)(enrollmentConsumption.getHourlyrate() * ((float)(sessionSnapshot.getEndTime() - sessionSnapshot.getStartTime())/ DateTimeUtils.MILLIS_PER_HOUR)));
                enrollmentConsumptionPayment.setEnrollmentTransactionContextId(sessionSnapshot.getSessionId());
                
                
                
                
                if(ArrayUtils.isEmpty(enrollment.getStatusChangeTime())){
                    enrollmentConsumptionPayment.setConsumptionType(enrollment.getStatus());
                }else{
                    if(EntityStatus.INACTIVE.equals(enrollment.getStatus())){
                        if(enrollment.getStatusChangeTime().get(enrollment.getStatusChangeTime().size()-1).getChangeTime() >= sessionSnapshot.getStartTime()){
                            enrollmentConsumptionPayment.setConsumptionType(EntityStatus.ACTIVE);
                        }else {
                            enrollmentConsumptionPayment.setConsumptionType(EntityStatus.INACTIVE);
                        }
                    }else{
                        enrollmentConsumptionPayment.setConsumptionType(enrollment.getStatus());
                    }
                }
                
                
                if(EntityStatus.ACTIVE.equals(enrollment.getStatus()) || (EntityStatus.INACTIVE.equals(enrollment.getStatus()) && (enrollmentConsumption.getAmtPaidLeft()-enrollmentConsumption.getNegativeMargin() >= enrollmentConsumptionPayment.getAmt()))){
                    makeEnrollmentPayment(enrollmentConsumptionPayment, null);
                }else{
                    logger.info("Enrollment Inactive");
                }   
            }
            
            if(enrollmentConsumption.getRegAmtLeft()>0){
                EnrollmentConsumptionPayment enrollmentConsumptionPayment1 = new EnrollmentConsumptionPayment();
                enrollmentConsumptionPayment1.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
                enrollmentConsumptionPayment1.setBatchId(enrollmentConsumption.getBatchId());
                enrollmentConsumptionPayment1.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION);
                enrollmentConsumptionPayment1.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                
                makeEnrollmentPayment(enrollmentConsumptionPayment1, null);
                
            }
            
        }


    }*/
    public OTMConsumptionPaymentRes makeDineroTransaction(OTMConsumptionReq otmConsumptionReq) throws VException {
        String url = dineroEndpoint + "/account/makeOTMConsumptionPayment";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(otmConsumptionReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        OTMConsumptionPaymentRes oTMConsumptionPaymentRes = gson.fromJson(jsonString, OTMConsumptionPaymentRes.class);
        return oTMConsumptionPaymentRes;
    }

    public OTMConsumptionPaymentRes makeDineroRefund(OTMRefundReq oTMRefundReq) throws VException {
        String url = dineroEndpoint + "/account/makeOTMRefund";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(oTMRefundReq), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        OTMConsumptionPaymentRes oTMConsumptionPaymentRes = gson.fromJson(jsonString, OTMConsumptionPaymentRes.class);
        return oTMConsumptionPaymentRes;

    }

    /*
    public void modifyEnrollmentConsumption(EnrollmentConsumption enrollmentConsumption, EnrollmentConsumptionPayment enrollmentConsumptionPayment) throws VException{

        if(EnrollmentTransactionContextType.END_ENROLLMENT_CONSUMPTION.equals(enrollmentConsumptionPayment.getEnrollmentTransactionContextType()) 
                || EnrollmentTransactionContextType.END_BATCH_CONSUMPTION.equals(enrollmentConsumptionPayment.getEnrollmentTransactionContextType())){
            enrollmentConsumptionPayment.setAmt(enrollmentConsumption.getAmtPaidLeft() + enrollmentConsumption.getRegAmtLeft());
            enrollmentConsumptionPayment.setAmtP(enrollmentConsumption.getAmtPaidPLeft() + enrollmentConsumption.getRegAmtPLeft());
            enrollmentConsumptionPayment.setAmtNP(enrollmentConsumption.getAmtPaidNPLeft() + enrollmentConsumption.getRegAmtNPLeft());
            enrollmentConsumptionPayment.setAmtFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft() + enrollmentConsumption.getRegAmtLeftFromDiscount());
            enrollmentConsumption.setAmtPaidLeft(0);
            enrollmentConsumption.setAmtPaidPLeft(0);
            enrollmentConsumption.setAmtPaidNPLeft(0);
            enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
            enrollmentConsumption.setRegAmtLeft(0);
            enrollmentConsumption.setRegAmtPLeft(0);
            enrollmentConsumption.setRegAmtNPLeft(0);
            enrollmentConsumption.setEnrollmentEnded(true);
            enrollmentConsumption.setRegAmtLeftFromDiscount(0);
            enrollmentConsumptionPayment.setConsumptionType(EntityStatus.ENDED);
        }else if(EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION.equals(enrollmentConsumptionPayment.getEnrollmentTransactionContextType())){
            enrollmentConsumptionPayment.setRegAmt(enrollmentConsumption.getRegAmtLeft());
            enrollmentConsumptionPayment.setRegAmtP(enrollmentConsumption.getRegAmtPLeft());
            enrollmentConsumptionPayment.setRegAmtNP(enrollmentConsumption.getRegAmtNPLeft());
            enrollmentConsumptionPayment.setRegAmtFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());
            enrollmentConsumption.setRegAmtLeft(0);
            enrollmentConsumption.setRegAmtPLeft(0);
            enrollmentConsumption.setRegAmtNPLeft(0);
            enrollmentConsumption.setRegAmtLeftFromDiscount(0);
            enrollmentConsumptionPayment.setConsumptionType(EntityStatus.ACTIVE);
        }else if(EnrollmentTransactionContextType.SESSION_CONSUMPTION.equals(enrollmentConsumptionPayment.getEnrollmentTransactionContextType())){
            if(enrollmentConsumption.getAmtPaidLeft() < enrollmentConsumptionPayment.getAmt()){
                enrollmentConsumption.setNegativeMargin(enrollmentConsumption.getNegativeMargin() + enrollmentConsumptionPayment.getAmt() - enrollmentConsumption.getAmtPaidLeft());
                enrollmentConsumptionPayment.setAmtP(enrollmentConsumption.getAmtPaidPLeft());
                enrollmentConsumptionPayment.setAmtNP(enrollmentConsumption.getAmtPaidNPLeft());
                enrollmentConsumptionPayment.setAmtFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
                enrollmentConsumption.setAmtPaidPLeft(0);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
                enrollmentConsumption.setAmtPaidNPLeft(0);
                enrollmentConsumption.setAmtPaidLeft(0);
                enrollmentConsumptionPayment.setNegativeMargin(true);
            }else {

                enrollmentConsumptionPayment.setAmtFromDiscount((int)(enrollmentConsumption.getDiscountToAmtRatio() * enrollmentConsumptionPayment.getAmt()));
                enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - enrollmentConsumptionPayment.getAmtFromDiscount());
                int amtLeft = enrollmentConsumptionPayment.getAmt() - enrollmentConsumptionPayment.getAmtFromDiscount();
                if(enrollmentConsumption.getAmtPaidPLeft() < amtLeft){
                    amtLeft = amtLeft - enrollmentConsumption.getAmtPaidPLeft();
                    enrollmentConsumptionPayment.setAmtP(enrollmentConsumption.getAmtPaidPLeft());
                    enrollmentConsumption.setAmtPaidPLeft(0);
                    if(amtLeft > enrollmentConsumption.getAmtPaidNPLeft()){
                        amtLeft = amtLeft - enrollmentConsumption.getAmtPaidNPLeft();
                        enrollmentConsumptionPayment.setAmtNP(enrollmentConsumption.getAmtPaidNPLeft());
                        enrollmentConsumption.setAmtPaidNPLeft(0);
                        if(enrollmentConsumption.getAmtPaidFromDiscountLeft() >= amtLeft){
                            enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft()-amtLeft);
                            enrollmentConsumptionPayment.setAmtFromDiscount(enrollmentConsumptionPayment.getAmtFromDiscount() + amtLeft);
                        }else{
                            throw new VException(ErrorCode.INSUFFICIENT_CREDITS, "Inconsistency in amount");
                        }
                        
                    }else{
                        enrollmentConsumptionPayment.setAmtNP(amtLeft);
                        enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - amtLeft);
                    }    
                }else{
                    enrollmentConsumptionPayment.setAmtP(amtLeft);
                    enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() - amtLeft);
                }
                enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - enrollmentConsumptionPayment.getAmt());
            }
          
        }else if(EnrollmentTransactionContextType.REGISTRATION_REFUND.equals(enrollmentConsumptionPayment.getEnrollmentTransactionContextType())){
            
            if(enrollmentConsumption.getRegAmtLeft()>0){
                enrollmentConsumptionPayment.setRegAmt(enrollmentConsumption.getRegAmtLeft());
                enrollmentConsumptionPayment.setRegAmtFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());
                enrollmentConsumptionPayment.setRegAmtNP(enrollmentConsumption.getRegAmtNPLeft());
                enrollmentConsumptionPayment.setRegAmtP(enrollmentConsumption.getRegAmtPLeft());
                enrollmentConsumption.setRegAmtLeft(0);
                enrollmentConsumption.setRegAmtPLeft(0);
                enrollmentConsumption.setRegAmtNPLeft(0);
                enrollmentConsumption.setRegAmtLeftFromDiscount(0);
            }else{
                enrollmentConsumptionPayment.setCgAmtNP(enrollmentConsumption.getRegAmtPaidNP());
                enrollmentConsumptionPayment.setCgAmtP(enrollmentConsumption.getRegAmtPaidP());
                enrollmentConsumptionPayment.setCgAmtFromDiscount(enrollmentConsumption.getRegAmtPaidFromDiscount());
                enrollmentConsumptionPayment.setCustomerGrievence(true);
            }
        }else if(EnrollmentTransactionContextType.REFUND.equals(enrollmentConsumptionPayment.getEnrollmentTransactionContextType())){
            
            if(enrollmentConsumption.getNegativeMargin()!=null && enrollmentConsumption.getNegativeMargin() > 0){
                if(enrollmentConsumption.getNegativeMargin() > enrollmentConsumption.getAmtPaidLeft()){
                    
                    int negativeLeft = enrollmentConsumption.getNegativeMargin() - enrollmentConsumption.getAmtPaidLeft();
                    enrollmentConsumptionPayment.setAmt(0);
                    if(enrollmentConsumptionPayment.isCustomerGrievence()){
                        
                        //enrollmentConsumptionPayment.setAmtFromDiscount(0);
                        
                        if(negativeLeft > enrollmentConsumptionPayment.getCgAmtFromDiscount() + enrollmentConsumptionPayment.getCgAmtNP() + enrollmentConsumptionPayment.getCgAmtP()){
                            enrollmentConsumptionPayment.setCgAmtFromDiscount(0);
                            enrollmentConsumptionPayment.setCgAmtP(0);
                            enrollmentConsumptionPayment.setCgAmtNP(0);
                        }
                        else{
                            
                            negativeLeft = negativeLeft - enrollmentConsumptionPayment.getCgAmtFromDiscount();
                            
                            if(negativeLeft > enrollmentConsumptionPayment.getCgAmtP()){
                                enrollmentConsumptionPayment.setCgAmtNP(enrollmentConsumptionPayment.getCgAmtNP() - (negativeLeft-enrollmentConsumptionPayment.getCgAmtP()));
                                enrollmentConsumptionPayment.setCgAmtP(0);
                            }else{
                                enrollmentConsumptionPayment.setCgAmtP(enrollmentConsumptionPayment.getCgAmtP() - negativeLeft);
                            }
                        }
                        
                    }
                    
                    enrollmentConsumptionPayment.setAmtFromDiscount(0);
                    enrollmentConsumptionPayment.setAmtP(0);
                    enrollmentConsumptionPayment.setAmtNP(0);
                    
                }else{
                    enrollmentConsumptionPayment.setAmt(enrollmentConsumption.getAmtPaidLeft() - enrollmentConsumption.getNegativeMargin());
                    int amt = enrollmentConsumptionPayment.getAmt();
                    int discountAmt = (int)(enrollmentConsumption.getDiscountToAmtRatio() * amt);
                    if(discountAmt > enrollmentConsumption.getAmtPaidFromDiscountLeft() ){
                        discountAmt = enrollmentConsumption.getAmtPaidFromDiscountLeft();
                    }
                    
                    enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - amt);
                    enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - discountAmt);
                    
                    enrollmentConsumptionPayment.setAmtFromDiscount(discountAmt);
                    int amtLeft = amt - discountAmt;
                    
                    
                    if(enrollmentConsumption.getAmtPaidNPLeft() >= amtLeft){
                        enrollmentConsumptionPayment.setAmtNP(amtLeft);
                        
                        enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - amtLeft);
                    }else{
                        enrollmentConsumptionPayment.setAmtNP(enrollmentConsumption.getAmtPaidNPLeft());
                        
                        enrollmentConsumption.setAmtPaidNPLeft(0);
                        
                    }
                    
                    if(enrollmentConsumption.getAmtPaidPLeft() >= amtLeft - enrollmentConsumptionPayment.getAmtNP()){
                        
                        enrollmentConsumptionPayment.setAmtP(amtLeft - enrollmentConsumptionPayment.getAmtNP());
                        
                        enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() - enrollmentConsumptionPayment.getAmtP());
                    
                    }else{
                        int requiredAmt = amtLeft - enrollmentConsumptionPayment.getAmtNP();

                        enrollmentConsumptionPayment.setAmtP(enrollmentConsumption.getAmtPaidPLeft());
                        enrollmentConsumption.setAmtPaidPLeft(0);
                        
                        requiredAmt = requiredAmt - enrollmentConsumptionPayment.getAmtP();
                        
                        if(enrollmentConsumption.getAmtPaidFromDiscountLeft() >= requiredAmt){
                            enrollmentConsumptionPayment.setAmtFromDiscount( enrollmentConsumptionPayment.getAmtFromDiscount() + requiredAmt);
                            enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - requiredAmt);
                        }else{
                            throw new VException(ErrorCode.INSUFFICIENT_CREDITS, "Inconsistency in amount");                            
                        }
                    }
                                    
                }
            }else {
                    
                enrollmentConsumptionPayment.setAmt(enrollmentConsumption.getAmtPaidLeft());
                enrollmentConsumptionPayment.setAmtFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
                enrollmentConsumptionPayment.setAmtNP(enrollmentConsumption.getAmtPaidNPLeft());
                enrollmentConsumptionPayment.setAmtP(enrollmentConsumption.getAmtPaidPLeft());
                
                enrollmentConsumption.setAmtPaidLeft(0);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
                enrollmentConsumption.setAmtPaidNPLeft(0);
                enrollmentConsumption.setAmtPaidPLeft(0);
                

                
                
            }
            enrollmentConsumptionPayment.setConsumptionType(EntityStatus.ENDED);
            
            
        }

    }
    
    public OTMConsumptionReq createOTMConsumptionReq(EnrollmentConsumptionPayment enrollmentConsumptionPayment, EnrollmentTransactionContextType enrollmentTransactionContextType){
        OTMConsumptionReq otmConsumptionReq = new OTMConsumptionReq();
        otmConsumptionReq.setAmount(enrollmentConsumptionPayment.getAmt());
        otmConsumptionReq.setEnrollmentTransactionContextType(enrollmentTransactionContextType);
        otmConsumptionReq.setEnrollmentId(enrollmentConsumptionPayment.getEnrollmentId());
        if(!EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION.equals(enrollmentTransactionContextType)){
            otmConsumptionReq.setNonPromotionalAmt(enrollmentConsumptionPayment.getAmtNP()==null?0:enrollmentConsumptionPayment.getAmtNP());
            int setPromotionalAmt = 0;
            if(enrollmentConsumptionPayment.getAmt()!=null){
                setPromotionalAmt+=enrollmentConsumptionPayment.getAmtP();
            }

            if(enrollmentConsumptionPayment.getAmtFromDiscount()!=null){
                setPromotionalAmt+=enrollmentConsumptionPayment.getAmtFromDiscount();
            }

            otmConsumptionReq.setPromotionalAmt(setPromotionalAmt);
            
            if(enrollmentConsumptionPayment.getAmt()==null){
                enrollmentConsumptionPayment.setAmt(0);
            }
            
            
            otmConsumptionReq.setAmount(enrollmentConsumptionPayment.getAmt());
            if(enrollmentConsumptionPayment.isNegativeMargin()){
                otmConsumptionReq.setNegativeMargin(true);
                otmConsumptionReq.setNegativeMarginAmount(otmConsumptionReq.getAmount() - (otmConsumptionReq.getPromotionalAmt() + otmConsumptionReq.getNonPromotionalAmt()));
            }
        }else{
            
            otmConsumptionReq.setNonPromotionalAmt(enrollmentConsumptionPayment.getRegAmtNP()==null?0:enrollmentConsumptionPayment.getRegAmtNP());
            int setPromotionalAmt = 0;
            if(enrollmentConsumptionPayment.getRegAmt()!=null){
                setPromotionalAmt+=enrollmentConsumptionPayment.getRegAmtP();
            }

            if(enrollmentConsumptionPayment.getRegAmtFromDiscount()!=null){
                setPromotionalAmt+=enrollmentConsumptionPayment.getRegAmtFromDiscount();
            }

            otmConsumptionReq.setPromotionalAmt(setPromotionalAmt);
            
            if(enrollmentConsumptionPayment.getRegAmt() == null){
                enrollmentConsumptionPayment.setRegAmt(0);
            }
            
            otmConsumptionReq.setAmount(enrollmentConsumptionPayment.getRegAmt());
            if(enrollmentConsumptionPayment.isNegativeMargin()){
                otmConsumptionReq.setNegativeMargin(true);
                otmConsumptionReq.setNegativeMarginAmount(otmConsumptionReq.getAmount() - (otmConsumptionReq.getPromotionalAmt() + otmConsumptionReq.getNonPromotionalAmt()));
            }
        }
        return otmConsumptionReq;
    }

    public OTMRefundReq createOTMRefundReq(EnrollmentConsumptionPayment enrollmentConsumptionPayment, EnrollmentTransactionContextType enrollmentTransactionContextType, Long userId){
        
        OTMRefundReq oTMRefundReq = new OTMRefundReq();
      
        if(enrollmentConsumptionPayment.isCustomerGrievence()){
            oTMRefundReq.setCustomerGrievence(true);
            oTMRefundReq.setAmtCGP(enrollmentConsumptionPayment.getCgAmtP());
            oTMRefundReq.setAmtCGNP(enrollmentConsumptionPayment.getCgAmtNP());
            oTMRefundReq.setAmtCGFromDiscount(enrollmentConsumptionPayment.getCgAmtFromDiscount());
        }
        oTMRefundReq.setUserId(userId);
        oTMRefundReq.setEnrollmentTransactionContextType(enrollmentTransactionContextType);
        oTMRefundReq.setAmount(enrollmentConsumptionPayment.getAmt());
        oTMRefundReq.setAmountFromDiscount(enrollmentConsumptionPayment.getAmtFromDiscount());
        oTMRefundReq.setAmountNP(enrollmentConsumptionPayment.getAmtNP());
        oTMRefundReq.setAmountP(enrollmentConsumptionPayment.getAmtP());
        oTMRefundReq.setEnrollmentId(enrollmentConsumptionPayment.getEnrollmentId());
        return oTMRefundReq;
    }
    
    public EnrollmentTransaction transactionForCG(EnrollmentConsumptionPayment enrollmentConsumptionPayment, EnrollmentConsumption enrollmentConsumption){
        EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
        
        enrollmentTransaction.setAmtLeft(enrollmentConsumption.getAmtPaidLeft());
        enrollmentTransaction.setAmtLeftP(enrollmentConsumption.getAmtPaidPLeft());
        enrollmentTransaction.setAmtLeftNP(enrollmentConsumption.getAmtPaidNPLeft());
        enrollmentTransaction.setAmtLeftFromDiscount(enrollmentConsumption.getAmtPaidFromDiscount());
        enrollmentTransaction.setNegativeMargin(enrollmentConsumptionPayment.isNegativeMargin());
        enrollmentTransaction.setNegativeMarginLeft(enrollmentConsumption.getNegativeMargin());

        enrollmentTransaction.setEnrollmentTransactionType(enrollmentConsumptionPayment.getEnrollmentTransactionType());
        enrollmentTransaction.setEnrollmentTransactionContextType(enrollmentConsumptionPayment.getEnrollmentTransactionContextType());

        enrollmentTransaction.setRegAmtLeft(enrollmentConsumption.getRegAmtLeft());
        enrollmentTransaction.setRegAmtLeftP(enrollmentConsumption.getRegAmtPLeft());
        enrollmentTransaction.setRegAmtLeftNP(enrollmentConsumption.getRegAmtNPLeft());
        enrollmentTransaction.setRegAmtLeftFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());

        enrollmentTransaction.setRegAmtPaidFromDiscount(enrollmentConsumptionPayment.getRegAmtFromDiscount());
        enrollmentTransaction.setRegAmtPaid(enrollmentConsumptionPayment.getRegAmt());
        enrollmentTransaction.setRegAmtPaidNP(enrollmentConsumptionPayment.getRegAmtNP());
        enrollmentTransaction.setRegAmtPaidP(enrollmentConsumptionPayment.getRegAmtP());

        enrollmentTransaction.setAmtPaid(enrollmentConsumptionPayment.getCgAmtNP()+enrollmentConsumptionPayment.getCgAmtP());
        enrollmentTransaction.setAmtPaidP(enrollmentConsumptionPayment.getCgAmtP());
        enrollmentTransaction.setAmtPaidNP(enrollmentConsumptionPayment.getCgAmtNP());

        enrollmentTransaction.setEnrollmentTransactionContextId(enrollmentConsumptionPayment.getEnrollmentTransactionContextId());
        enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());

        enrollmentTransaction.setConsumptionType(enrollmentConsumptionPayment.getConsumptionType());
        enrollmentTransaction.setEnrollmentId(enrollmentConsumptionPayment.getEnrollmentId());

        return enrollmentTransaction;        
    }
    
    public boolean makeEnrollmentPayment(EnrollmentConsumptionPayment enrollmentConsumptionPayment, Session session) throws VException{
        logger.info(enrollmentConsumptionPayment);
        if(enrollmentConsumptionPayment.getEnrollmentTransactionType()!=null && enrollmentConsumptionPayment.getEnrollmentTransactionContextType()!= null) {
            EnrollmentTransactionContextType enrollmentTransactionContextType = enrollmentConsumptionPayment.getEnrollmentTransactionContextType();
            EnrollmentTransactionType enrollmentTransactionType = enrollmentConsumptionPayment.getEnrollmentTransactionType();
            boolean result = false;
            if (session == null) {
                session = sqlSessionfactory.getSessionFactory().openSession();
            }
            
            Transaction transaction = session.getTransaction();
            
            EnrollmentTransaction enrollmentTransaction;
            try {
                
                transaction.begin();
                
                if(enrollmentConsumptionPayment.getEnrollmentId() == null){
                    throw new VException(ErrorCode.ENROLLMENT_NOT_FOUND, "Null enrollmentId");
                }
                
                switch (enrollmentTransactionType) {
                    case DEBIT:

                        EnrollmentConsumption enrollmentConsumption1 = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentConsumptionPayment.getEnrollmentId(), true, session, LockMode.PESSIMISTIC_WRITE);
                        if(enrollmentConsumption1 == null){
                            throw new VException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "EnrollmentConsumption not found for enrollment id: "+enrollmentConsumptionPayment.getEnrollmentId());
                        }
                        modifyEnrollmentConsumption(enrollmentConsumption1, enrollmentConsumptionPayment);
                        
                        logger.info("EnrollmentConsumption: "+enrollmentConsumption1);
                        logger.info("EnrollmentConsumotioPayment: "+enrollmentConsumptionPayment);
                        enrollmentTransaction = createFromEnrollmentConsumption(enrollmentConsumption1, enrollmentConsumptionPayment);
                        
                        logger.info("EnrollmentTransaction: "+enrollmentTransaction);
                        
                        if((enrollmentTransaction.getAmtPaid() != null && enrollmentTransaction.getAmtPaid() > 0)
                          ||(enrollmentTransaction.getRegAmtPaid() != null && enrollmentTransaction.getRegAmtPaid() > 0)
                             || !enrollmentConsumptionPayment.isCustomerGrievence()){
                        
                            enrollmentTransactionDAO.create(enrollmentTransaction, session);
                            enrollmentConsumptionDAO.update(enrollmentConsumption1, session);
                        }
                        
                        if(enrollmentConsumptionPayment.isCustomerGrievence()){
                            EnrollmentTransaction enrollmentTransaction1 = transactionForCG(enrollmentConsumptionPayment, enrollmentConsumption1);
                            logger.info("enrollment Transaction for CG: "+enrollmentTransaction1);
                            enrollmentTransactionDAO.create(enrollmentTransaction1, session);
                        }
                        
                        transaction.commit();
                        
                        logger.info("Checking for transaction context type");
                        logger.info(enrollmentTransactionContextType);
                        
                        OTMConsumptionPaymentRes oTMConsumptionPaymentRes = null;
                        if (EnrollmentTransactionContextType.END_BATCH_CONSUMPTION.equals(enrollmentTransactionContextType) 
                                || EnrollmentTransactionContextType.END_ENROLLMENT_CONSUMPTION.equals(enrollmentTransactionContextType)
                                || EnrollmentTransactionContextType.SESSION_CONSUMPTION.equals(enrollmentTransactionContextType)
                                || EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION.equals(enrollmentTransactionContextType)) {
                            
                            OTMConsumptionReq otmConsumptionReq = createOTMConsumptionReq(enrollmentConsumptionPayment, enrollmentTransactionContextType);
                            logger.info("making dinero transaction: "+otmConsumptionReq);
                            oTMConsumptionPaymentRes = makeDineroTransaction(otmConsumptionReq);
                            //modifyEnrollmentConsumption(enrollmentConsumption1, enrollmentConsumptionPayment);
                        } else if (EnrollmentTransactionContextType.REFUND.equals(enrollmentTransactionContextType)
                                  || EnrollmentTransactionContextType.REGISTRATION_REFUND.equals(enrollmentTransactionContextType)) {
                            OTMRefundReq oTMRefundReq = createOTMRefundReq(enrollmentConsumptionPayment, enrollmentTransactionContextType, Long.parseLong(enrollmentConsumption1.getUserId()));
                            logger.info("OTMRefundReq: "+oTMRefundReq);
                            oTMConsumptionPaymentRes = makeDineroRefund(oTMRefundReq);
                            logger.info("OTMConsumptionPaymentRes: "+oTMConsumptionPaymentRes);
                        }

                        result = true;
                        break;
                    case CREDIT:


                        EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentConsumptionPayment.getEnrollmentId(), true, session, LockMode.PESSIMISTIC_WRITE);
                        if(enrollmentConsumption == null){
                            throw new VException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "EnrollmentConsumption not found for enrollment id: "+enrollmentConsumptionPayment.getEnrollmentId());
                        }
                        if (EnrollmentTransactionContextType.REGISTRATION_PAYMENT.equals(enrollmentTransactionContextType)) {
                            enrollmentConsumption.setRegAmtPaid(enrollmentConsumption.getRegAmtPaid() + enrollmentConsumptionPayment.getRegAmt());
                            enrollmentConsumption.setRegAmtPaidP(enrollmentConsumption.getRegAmtPaidP() + enrollmentConsumptionPayment.getRegAmtP());
                            enrollmentConsumption.setRegAmtPaidNP(enrollmentConsumption.getRegAmtPaidNP() + enrollmentConsumptionPayment.getRegAmtNP());
                            enrollmentConsumption.setRegAmtPaidFromDiscount(enrollmentConsumption.getRegAmtPaidFromDiscount() + enrollmentConsumptionPayment.getRegAmtFromDiscount());
                            enrollmentConsumption.setRegAmtLeft(enrollmentConsumption.getRegAmtLeft() + enrollmentConsumptionPayment.getRegAmt());
                            enrollmentConsumption.setRegAmtPLeft(enrollmentConsumption.getRegAmtPLeft() + enrollmentConsumptionPayment.getRegAmtP());
                            enrollmentConsumption.setRegAmtNPLeft(enrollmentConsumption.getRegAmtNPLeft() + enrollmentConsumptionPayment.getRegAmtNP());
//                            enrollmentConsumption.setDuration(enrollmentConsumptionPayment.getDuration());
                            enrollmentTransaction = createFromEnrollmentConsumption(enrollmentConsumption, enrollmentConsumptionPayment);
                            enrollmentTransactionDAO.create(enrollmentTransaction, session);
                        } else {
                            if (EnrollmentTransactionContextType.BULK_PAYMENT.equals(enrollmentTransactionContextType) &&
                                    enrollmentConsumption.getAmtToPay() != enrollmentConsumptionPayment.getAmt() - enrollmentConsumptionPayment.getAmtFromDiscount() + enrollmentConsumption.getAmtPaid()) {
                                throw new VException(ErrorCode.SERVICE_ERROR, "Bulk amount not equal to required payment for the batch");
                            }

                            enrollmentConsumption.setAmtPaid(enrollmentConsumption.getAmtPaid() + enrollmentConsumptionPayment.getAmt());
                            enrollmentConsumption.setAmtPaidNP(enrollmentConsumption.getRegAmtPaidNP() + enrollmentConsumptionPayment.getAmtNP());
                            enrollmentConsumption.setAmtPaidP(enrollmentConsumption.getAmtPaidP() + enrollmentConsumptionPayment.getAmtP());
                            enrollmentConsumption.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscount() + enrollmentConsumptionPayment.getAmtFromDiscount());
                            enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() + enrollmentConsumptionPayment.getAmt());
                            enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() + enrollmentConsumptionPayment.getAmtP());
                            enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() + enrollmentConsumptionPayment.getAmtNP());
                            enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() + enrollmentConsumptionPayment.getAmtFromDiscount());

                            enrollmentTransaction = createFromEnrollmentConsumption(enrollmentConsumption, enrollmentConsumptionPayment);

                            enrollmentTransactionDAO.create(enrollmentTransaction, session);
                        }
                        
                        logger.info("EnrollmentConsumption: " + enrollmentConsumption);
                        logger.info("EnrollmentConsumptionPayment: " + enrollmentConsumptionPayment);
                        enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                        transaction.commit();
                        result=true;
                        break;
                }
            }catch(Exception e){
                logger.warn("Exception in makeEnrollmentPayment: "+e.getMessage());
                if(session.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {
                    logger.info("transaction rollback: " + e.getMessage());
                    session.getTransaction().rollback();
                }
                result=false;
            }finally {
                session.close();
                return result;
            }
        }else{
            
            logger.info("EnrollmentTransactionContextType or EnrollmentTransactionType not provided");
            
            return false;
        }
    }

    public EnrollmentTransaction createFromEnrollmentConsumption(EnrollmentConsumption enrollmentConsumption, EnrollmentConsumptionPayment enrollmentConsumptionPayment) {
        EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
        
        enrollmentTransaction.setAmtLeft(enrollmentConsumption.getAmtPaidLeft());
        enrollmentTransaction.setAmtLeftP(enrollmentConsumption.getAmtPaidPLeft());
        enrollmentTransaction.setAmtLeftNP(enrollmentConsumption.getAmtPaidNPLeft());
        enrollmentTransaction.setAmtLeftFromDiscount(enrollmentConsumption.getAmtPaidFromDiscount());
        enrollmentTransaction.setNegativeMargin(enrollmentConsumptionPayment.isNegativeMargin());
        enrollmentTransaction.setNegativeMarginLeft(enrollmentConsumption.getNegativeMargin());

        enrollmentTransaction.setEnrollmentTransactionType(enrollmentConsumptionPayment.getEnrollmentTransactionType());
        enrollmentTransaction.setEnrollmentTransactionContextType(enrollmentConsumptionPayment.getEnrollmentTransactionContextType());

        enrollmentTransaction.setRegAmtLeft(enrollmentConsumption.getRegAmtLeft());
        enrollmentTransaction.setRegAmtLeftP(enrollmentConsumption.getRegAmtPLeft());
        enrollmentTransaction.setRegAmtLeftNP(enrollmentConsumption.getRegAmtNPLeft());
        enrollmentTransaction.setRegAmtLeftFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());

        enrollmentTransaction.setRegAmtPaidFromDiscount(enrollmentConsumptionPayment.getRegAmtFromDiscount());
        enrollmentTransaction.setRegAmtPaid(enrollmentConsumptionPayment.getRegAmt());
        enrollmentTransaction.setRegAmtPaidNP(enrollmentConsumptionPayment.getRegAmtNP());
        enrollmentTransaction.setRegAmtPaidP(enrollmentConsumptionPayment.getRegAmtP());

        enrollmentTransaction.setAmtPaid(enrollmentConsumptionPayment.getAmt());
        enrollmentTransaction.setAmtPaidP(enrollmentConsumptionPayment.getAmtP());
        enrollmentTransaction.setAmtPaidNP(enrollmentConsumptionPayment.getAmtNP());
        enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumptionPayment.getAmtFromDiscount());

        enrollmentTransaction.setEnrollmentTransactionContextId(enrollmentConsumptionPayment.getEnrollmentTransactionContextId());
        enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());

        enrollmentTransaction.setConsumptionType(enrollmentConsumptionPayment.getConsumptionType());
        enrollmentTransaction.setEnrollmentId(enrollmentConsumptionPayment.getEnrollmentId());

        return enrollmentTransaction;


    }
     */
    EnrollmentConsumption addUpdates(EnrollmentConsumption oldObj, EnrollmentConsumption newObj) {

        for (Field field : newObj.getClass().getDeclaredFields()) {
            field.setAccessible(true); // if you want to modify private fields
            try {
                logger.info(field.getType() + " " + field.getName() + ": " + field.get(newObj));
                if (field.get(newObj) != null) {
                    field.set(oldObj, field.get(newObj));
                }
            } catch (IllegalArgumentException e) {
                logger.info(e.toString());
            } catch (IllegalAccessException e) {
                logger.info(e.toString());
            }
        }
        oldObj.setLastUpdatedTime(System.currentTimeMillis());
        return oldObj;
    }

    public OTMConsumptionReq createOTMConsumptionReq(EnrollmentConsumption enrollmentConsumption, EnrollmentTransaction enrollmentTransaction) {

        OTMConsumptionReq oTMConsumptionReq = new OTMConsumptionReq();
        if (enrollmentTransaction.getRegAmtPaid() > 0) {
            oTMConsumptionReq.setAmount(enrollmentTransaction.getRegAmtPaid());
            oTMConsumptionReq.setPromotionalAmt(enrollmentTransaction.getRegAmtPaidP() + enrollmentTransaction.getRegAmtPaidFromDiscount());
            oTMConsumptionReq.setNonPromotionalAmt(enrollmentTransaction.getRegAmtPaidNP());
        } else if (enrollmentTransaction.getAmtPaid() > 0) {
            oTMConsumptionReq.setAmount(enrollmentTransaction.getAmtPaid());
            oTMConsumptionReq.setPromotionalAmt(enrollmentTransaction.getAmtPaidP() + enrollmentTransaction.getAmtPaidFromDiscount());
            oTMConsumptionReq.setNonPromotionalAmt(enrollmentTransaction.getAmtPaidNP());
            if (enrollmentTransaction.isNegativeMargin()) {
                oTMConsumptionReq.setNegativeMargin(true);
                oTMConsumptionReq.setNegativeMarginAmount(enrollmentTransaction.getAmtPaid() - oTMConsumptionReq.getPromotionalAmt() - oTMConsumptionReq.getNonPromotionalAmt());
            }
        }

        oTMConsumptionReq.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
        oTMConsumptionReq.setEnrollmentTransactionContextType(enrollmentTransaction.getEnrollmentTransactionContextType());

        return oTMConsumptionReq;

    }

    public EnrollmentConsumption createEnrollmentConsumption(MakePaymentRequest makePaymentRequest) throws VException {

        EnrollmentConsumption enrollmentConsumption = new EnrollmentConsumption();
        enrollmentConsumption.setBatchId(makePaymentRequest.getBatchId());
        enrollmentConsumption.setEnrollmentId(makePaymentRequest.getEnrollmentId());
        enrollmentConsumption.setCourseId(makePaymentRequest.getCourseId());
        enrollmentConsumption.setAmtToPay(makePaymentRequest.getAmtToBePaid());
        enrollmentConsumption.setDuration(makePaymentRequest.getDuration());

        return enrollmentConsumption;

    }

    public void prepareRegPaymentEnrollmentTransaction(EnrollmentTransaction enrollmentTransaction, MakePaymentRequest makePaymentRequest) {

        enrollmentTransaction.setEnrollmentTransactionContextType(makePaymentRequest.getEnrollmentTransactionContextType());
        enrollmentTransaction.setRegAmtPaid(makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount());
        enrollmentTransaction.setRegAmtPaidFromDiscount(makePaymentRequest.getAmtFromDiscount());
        enrollmentTransaction.setRegAmtPaidNP(makePaymentRequest.getAmtNP());
        enrollmentTransaction.setRegAmtPaidP(makePaymentRequest.getAmtP());
        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.CREDIT);

    }

    public void preparePaymentEnrollmentTransaction(EnrollmentTransaction enrollmentTransaction, MakePaymentRequest makePaymentRequest) {

        enrollmentTransaction.setEnrollmentTransactionContextType(makePaymentRequest.getEnrollmentTransactionContextType());
        enrollmentTransaction.setAmtPaid(makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount());
        enrollmentTransaction.setAmtPaidFromDiscount(makePaymentRequest.getAmtFromDiscount());
        enrollmentTransaction.setAmtPaidNP(makePaymentRequest.getAmtNP());
        enrollmentTransaction.setAmtPaidP(makePaymentRequest.getAmtP());
        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.CREDIT);

    }

    public void takeSnapshotEnrollmentConsumption(EnrollmentTransaction enrollmentTransaction, EnrollmentConsumption enrollmentConsumption) {

        enrollmentTransaction.setEnrollmentId(enrollmentConsumption.getEnrollmentId());

        enrollmentTransaction.setAmtLeft(enrollmentConsumption.getAmtPaidLeft());
        enrollmentTransaction.setAmtLeftP(enrollmentConsumption.getAmtPaidPLeft());
        enrollmentTransaction.setAmtLeftNP(enrollmentConsumption.getAmtPaidNPLeft());
        enrollmentTransaction.setAmtLeftFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());

        enrollmentTransaction.setNegativeMarginLeft(enrollmentConsumption.getNegativeMargin());
        enrollmentTransaction.setRegAmtLeft(enrollmentConsumption.getRegAmtLeft());
        enrollmentTransaction.setRegAmtLeftNP(enrollmentConsumption.getRegAmtNPLeft());
        enrollmentTransaction.setRegAmtLeftP(enrollmentConsumption.getRegAmtPLeft());
        enrollmentTransaction.setRegAmtLeftFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());

    }

    public void updateRegistrationBucket(EnrollmentConsumption enrollmentConsumption, MakePaymentRequest makePaymentRequest) throws VException {

        enrollmentConsumption.setRegAmtLeft(makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount());
        enrollmentConsumption.setRegAmtPLeft(makePaymentRequest.getAmtP());
        enrollmentConsumption.setRegAmtNPLeft(makePaymentRequest.getAmtNP());
        enrollmentConsumption.setRegAmtLeftFromDiscount(makePaymentRequest.getAmtFromDiscount());

        enrollmentConsumption.setRegAmtPaid(makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount());
        enrollmentConsumption.setRegAmtPaidFromDiscount(makePaymentRequest.getAmtFromDiscount());
        enrollmentConsumption.setRegAmtPaidNP(makePaymentRequest.getAmtNP());
        enrollmentConsumption.setRegAmtPaidP(makePaymentRequest.getAmtP());

    }

    public void updateNonRegistrationBucket(EnrollmentConsumption enrollmentConsumption, MakePaymentRequest makePaymentRequest) throws VException {

        enrollmentConsumption.setAmtPaid(enrollmentConsumption.getAmtPaid() + makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount());
        enrollmentConsumption.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscount() + makePaymentRequest.getAmtFromDiscount());
        enrollmentConsumption.setAmtPaidP(enrollmentConsumption.getAmtPaidP() + makePaymentRequest.getAmtP());
        enrollmentConsumption.setAmtPaidNP(enrollmentConsumption.getAmtPaidNP() + makePaymentRequest.getAmtNP());

        enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() + makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount());
        enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() + makePaymentRequest.getAmtFromDiscount());
        enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() + makePaymentRequest.getAmtP());
        enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() + makePaymentRequest.getAmtNP());

    }

    public void adjustNegativeMargin(String enrollmentId, String orderId) throws InternalServerErrorException {

        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();

        try {
            transaction.begin();

            EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentId, true, session, LockMode.PESSIMISTIC_WRITE);
            EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
            if (enrollmentConsumption.getNegativeMargin() != null && enrollmentConsumption.getNegativeMargin() > 0 && enrollmentConsumption.getAmtPaidLeft() >= enrollmentConsumption.getNegativeMargin()) {
                logger.info("performing negative margin adjustment");
                enrollmentTransaction.setAmtPaid(enrollmentConsumption.getNegativeMargin());
                enrollmentConsumption.setNegativeMargin(0);
                int amt = enrollmentTransaction.getAmtPaid();
                if (enrollmentConsumption.getDiscountToAmtRatio() != null && enrollmentConsumption.getDiscountToAmtRatio() > 0) {
                    int amtFromDiscount = (int) (enrollmentConsumption.getDiscountToAmtRatio() * enrollmentTransaction.getAmtPaid());
                    if (enrollmentConsumption.getAmtPaidFromDiscountLeft() >= amtFromDiscount) {
                        enrollmentTransaction.setAmtPaidFromDiscount(amtFromDiscount);
                        enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - amtFromDiscount);
                    } else {
                        enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
                        enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
                    }
                    amt = amt - enrollmentTransaction.getAmtPaidFromDiscount();
                }
                if (enrollmentConsumption.getAmtPaidPLeft() >= amt) {
                    enrollmentTransaction.setAmtPaidP(amt);
                    enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() - amt);
                    amt = 0;
                } else {
                    enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());
                    enrollmentConsumption.setAmtPaidPLeft(0);
                    amt = amt - enrollmentTransaction.getAmtPaidP();

                    if (enrollmentConsumption.getAmtPaidNPLeft() >= amt) {
                        enrollmentTransaction.setAmtPaidNP(amt);
                        enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - amt);
                        amt = 0;
                    } else {
                        enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());
                        enrollmentConsumption.setAmtPaidNPLeft(0);
                        amt = amt - enrollmentTransaction.getAmtPaidNP();
                        if (enrollmentConsumption.getAmtPaidFromDiscountLeft() >= amt) {
                            enrollmentTransaction.setAmtPaidFromDiscount(enrollmentTransaction.getAmtPaidFromDiscount() + amt);
                            enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - amt);
                            amt = 0;
                        } else {
                            throw new InternalServerErrorException(ErrorCode.INSUFFICIENT_CREDITS, "Inconsistency in amount");
                        }
                    }

                    enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - enrollmentTransaction.getAmtPaid());

                }
                logger.info("Enrollment Transaction: " + enrollmentTransaction);
                logger.info("Enrollment Consumption: " + enrollmentConsumption);
                enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.NEGATIVE_MARGIN_ADJUSTMENT);
                enrollmentTransaction.setEnrollmentTransactionContextId(orderId);
                enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentTransaction.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
                enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);

                enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                enrollmentTransactionDAO.save(enrollmentTransaction, session, null);

                OTMConsumptionReq oTMConsumptionReq = createOTMConsumptionReq(enrollmentConsumption, enrollmentTransaction);
                oTMConsumptionReq.setNegativeMarginAdjustment(true);
                makeDineroTransaction(oTMConsumptionReq);
            } else if (enrollmentConsumption.getNegativeMargin() != null && enrollmentConsumption.getNegativeMargin() > 0 && enrollmentConsumption.getAmtPaidLeft() < enrollmentConsumption.getNegativeMargin()) {
                enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaidLeft());
                enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());
                enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());
                enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());

                enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.NEGATIVE_MARGIN_ADJUSTMENT);
                enrollmentTransaction.setEnrollmentTransactionContextId(orderId);
                enrollmentTransaction.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
                enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());

                enrollmentConsumption.setAmtPaidLeft(0);
                enrollmentConsumption.setAmtPaidPLeft(0);
                enrollmentConsumption.setAmtPaidNPLeft(0);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
                enrollmentConsumption.setNegativeMargin(enrollmentConsumption.getNegativeMargin() - enrollmentTransaction.getAmtPaid());
                takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);
                enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                enrollmentTransactionDAO.save(enrollmentTransaction, session, null);

                OTMConsumptionReq oTMConsumptionReq = createOTMConsumptionReq(enrollmentConsumption, enrollmentTransaction);
                oTMConsumptionReq.setNegativeMarginAdjustment(true);
                makeDineroTransaction(oTMConsumptionReq);

            }
            transaction.commit();

        } catch (Exception ex) {

            session.getTransaction().rollback();
            logger.error("Exception while negative margin adjustment: "
                    + enrollmentId + " Exception " + ex.getMessage());

        } finally {
            session.close();
        }

    }

    public void updateBatchForEnrollmentConsumption(String enrollmentId, String batchId) {
        enrollmentConsumptionDAO.updateBatchId(enrollmentId, batchId);
    }

    public boolean topUpEnrollmentConsumption(MakePaymentRequest makePaymentRequest, Float ratio) throws BadRequestException, VException {
        if (StringUtils.isEmpty(makePaymentRequest.getEnrollmentId())
                || StringUtils.isEmpty(makePaymentRequest.getOrderId())
                || StringUtils.isEmpty(makePaymentRequest.getCourseId())
                || StringUtils.isEmpty(makePaymentRequest.getBatchId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Insufficient Information in request object");
        }

        boolean result = false;

        Enrollment enrollment = enrollmentDAO.getById(makePaymentRequest.getEnrollmentId());

        OrderInfo orderInfo = paymentManager.getOrderInfo(makePaymentRequest.getOrderId());
        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();
            EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(makePaymentRequest.getEnrollmentId(), true, session, LockMode.PESSIMISTIC_WRITE);
            if (enrollmentConsumption == null) {
                if (makePaymentRequest.isCreateConsumptionIfnotFound()) {
                    enrollmentConsumption = createEnrollmentConsumption(makePaymentRequest);
                    enrollmentConsumption.setHourlyrate(enrollment.getHourlyRate());
                    enrollmentConsumption.setUserId(enrollment.getUserId());
                } else {
                    throw new NotFoundException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "EnrollmentConsumption not found for " + makePaymentRequest.getEnrollmentId());
                }
            }

            EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

            if (EnrollmentTransactionContextType.REGISTRATION_PAYMENT.equals(makePaymentRequest.getEnrollmentTransactionContextType())) {

                prepareRegPaymentEnrollmentTransaction(enrollmentTransaction, makePaymentRequest);
                updateRegistrationBucket(enrollmentConsumption, makePaymentRequest);
                enrollmentTransaction.setEnrollmentTransactionContextId(makePaymentRequest.getOrderId());

            } else if (EnrollmentTransactionContextType.INSTALLMENT_PAYMENT.equals(makePaymentRequest.getEnrollmentTransactionContextType())
                    || EnrollmentTransactionContextType.BULK_PAYMENT.equals(makePaymentRequest.getEnrollmentTransactionContextType())) {

                if (enrollmentConsumption.getDiscountToAmtRatio() == null && ArrayUtils.isNotEmpty(orderInfo.getOrderedItems())) {
                    if (orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount() == 0 && orderInfo.getAmount() == 0) {
                        enrollmentConsumption.setDiscountToAmtRatio(0f);
                    } else {
                        enrollmentConsumption.setDiscountToAmtRatio((float) orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount() / (orderInfo.getAmount() + orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount()));
                    }
                }

                preparePaymentEnrollmentTransaction(enrollmentTransaction, makePaymentRequest);
                updateNonRegistrationBucket(enrollmentConsumption, makePaymentRequest);

                if (EnrollmentTransactionContextType.INSTALLMENT_PAYMENT.equals(makePaymentRequest.getEnrollmentTransactionContextType())) {
                    enrollmentTransaction.setEnrollmentTransactionContextId(makePaymentRequest.getEnrollmentTransactionContextId());
                } else {
                    enrollmentTransaction.setEnrollmentTransactionContextId(makePaymentRequest.getOrderId());
                }

            }
            if (enrollmentTransaction.getEnrollmentTransactionContextId() == null) {
                enrollmentTransaction.setEnrollmentTransactionContextId(makePaymentRequest.getOrderId());
            }
            enrollmentTransaction.setEnrollmentId(enrollmentConsumption.getEnrollmentId());

            logger.info("EnrollmentConsumption: " + enrollmentConsumption);
            logger.info("EnrollmentTransaction: " + enrollmentTransaction);

            takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);

            if (enrollmentConsumption.getId() == null) {
                enrollmentConsumptionDAO.create(enrollmentConsumption, session);
            } else {
                enrollmentConsumptionDAO.update(enrollmentConsumption, session);
            }
            enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
            enrollmentTransactionDAO.save(enrollmentTransaction, session, null);

            transaction.commit();
            result = true;
        } catch (Exception ex) {
            session.getTransaction().rollback();
            logger.error("Exception while making payment: " + ex.getMessage());

        } finally {

            session.close();

        }

        if (result) {

            if (EnrollmentTransactionContextType.INSTALLMENT_PAYMENT.equals(makePaymentRequest.getEnrollmentTransactionContextType())) {
                if (makePaymentRequest.getEnrollmentTransactionContextId() == null) {
                    adjustNegativeMargin(makePaymentRequest.getEnrollmentId(), makePaymentRequest.getOrderId());
                } else {
                    adjustNegativeMargin(makePaymentRequest.getEnrollmentId(), makePaymentRequest.getEnrollmentTransactionContextId());
                }
            } else {
                adjustNegativeMargin(makePaymentRequest.getEnrollmentId(), makePaymentRequest.getOrderId());
            }
        }

        return result;

    }

    /*
    public void createEnrollmentConsumptionFromRegistration(Registration registration, List<Enrollment> enrollmentList){
        
        Map<String, Float> priceRatio = new HashMap<>();
        Map<String, Integer> durationMap = new HashMap<>();
        
        for(OTMBundleEntityInfo oTMBundleEntityInfo : registration.getEntities()){
            priceRatio.put(oTMBundleEntityInfo.getEntityId(), (float)oTMBundleEntityInfo.getPrice()/registration.getBulkPriceToPay());
            durationMap.put(oTMBundleEntityInfo.getEntityId(), oTMBundleEntityInfo.getDuration());
        }
        
        for(Enrollment enrollment : enrollmentList){
            MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
            makePaymentRequest.setBatchId(enrollment.getBatchId());
            makePaymentRequest.setCourseId(enrollment.getCourseId());
            makePaymentRequest.setEnrollmentId(enrollment.getId());
            makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_PAYMENT);
            makePaymentRequest.setOrderId(registration.getOrderId());
            makePaymentRequest.setAmtToBePaid(registration.getBulkPriceToPay());
            makePaymentRequest.setDuration((long)durationMap.get(enrollment.getCourseId()));
            
            try{

            }catch (Exception ex){
                logger.error("Error while creating enrollment consumption for: "+enrollment+" , exception: "+ex.getMessage());
            }
        }
        
    }
     */
    public boolean performConsumptionForTrialEnrollment(Enrollment enrollment, String sessionId) {
        boolean result = false;

        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();
            EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollment.getId(), true, session, LockMode.PESSIMISTIC_WRITE);
            if (enrollmentConsumption == null) {
                logger.error("EnrollmentConsumption not found for : " + enrollment.getId());
                throw new NotFoundException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "EnrollmentConsumption not found for : " + enrollment.getId());
            }

            EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
            enrollmentTransaction.setRegAmtPaid(enrollmentConsumption.getRegAmtLeft());
            enrollmentTransaction.setRegAmtPaidFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());
            enrollmentTransaction.setRegAmtPaidNP(enrollmentConsumption.getRegAmtNPLeft());
            enrollmentTransaction.setRegAmtPaidP(enrollmentConsumption.getRegAmtPLeft());

            enrollmentConsumption.setRegAmtLeft(0);
            enrollmentConsumption.setRegAmtLeftFromDiscount(0);
            enrollmentConsumption.setRegAmtNPLeft(0);
            enrollmentConsumption.setRegAmtPLeft(0);

            enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
            enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
            enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
            enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION);
            enrollmentTransaction.setEnrollmentId(enrollment.getId());
            enrollmentTransaction.setEnrollmentTransactionContextId(sessionId);
            takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);

            enrollmentConsumptionDAO.update(enrollmentConsumption, session);
            if (enrollmentTransaction.getRegAmtPaid() > 0) {
                enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
            }
            transaction.commit();

            OTMConsumptionReq oTMConsumptionReq = createOTMConsumptionReq(enrollmentConsumption, enrollmentTransaction);

            if (enrollmentTransaction.getRegAmtPaid() > 0) {
                makeDineroTransaction(oTMConsumptionReq);
            }

            result = true;
        } catch (Exception ex) {
            logger.error("performConsumptionForTrialEnrollment Exception: " +ex.getMessage());
            session.getTransaction().rollback();

        } finally {

            session.close();
        }

        return result;
    }

    public boolean performConsumptionForRegularEnrollment(Enrollment enrollment, SessionSnapshot sessionSnapshot) {
        boolean result = false;

        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();

            EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollment.getId(), true, session, LockMode.PESSIMISTIC_WRITE);
            if (enrollmentConsumption == null) {
                logger.error("EnrollmentConsumption not found for : " + enrollment.getId());
                throw new NotFoundException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "EnrollmentConsumption not found for : " + enrollment.getId());
            }

            EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

            if (ArrayUtils.isEmpty(enrollment.getStatusChangeTime())) {
                enrollmentTransaction.setConsumptionType(enrollment.getStatus());
            } else {
                if (EntityStatus.INACTIVE.equals(enrollment.getStatus())) {
                    if (enrollment.getStatusChangeTime().get(enrollment.getStatusChangeTime().size() - 1).getChangeTime() >= sessionSnapshot.getStartTime()) {
                        enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                    } else {
                        enrollmentTransaction.setConsumptionType(EntityStatus.INACTIVE);
                    }
                } else {
                    enrollmentTransaction.setConsumptionType(enrollment.getStatus());
                }
            }

            //enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
            enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
            enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
            enrollmentTransaction.setEnrollmentId(enrollment.getId());
            enrollmentTransaction.setEnrollmentTransactionContextId(sessionSnapshot.getSessionId());
            enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.SESSION_CONSUMPTION);

            if (enrollmentConsumption.getHourlyrate() == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Hourly rate not found for: " + enrollment.getId());
            }

            int amountToBeDeducted = (int) (enrollmentConsumption.getHourlyrate() * ((float) (sessionSnapshot.getEndTime() - sessionSnapshot.getStartTime()) / DateTimeUtils.MILLIS_PER_HOUR));

            prepareEnrollmentTransactionForRegularConsumption(enrollmentTransaction, enrollmentConsumption, amountToBeDeducted);
            takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);
            enrollmentConsumptionDAO.update(enrollmentConsumption, session);
            if (enrollmentTransaction.getAmtPaid() > 0) {
                enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
            }

            transaction.commit();

            OTMConsumptionReq oTMConsumptionReq = createOTMConsumptionReq(enrollmentConsumption, enrollmentTransaction);

            makeDineroTransaction(oTMConsumptionReq);

            result = true;
        } catch (Exception ex) {
            logger.error("performConsumptionForRegularEnrollment exception " + ex.getMessage());
            session.getTransaction().rollback();

        } finally {
            session.close();
        }

        return result;
    }

    public void prepareEnrollmentTransactionForRegularConsumption(EnrollmentTransaction enrollmentTransaction, EnrollmentConsumption enrollmentConsumption, int amount) throws InternalServerErrorException {

        enrollmentTransaction.setAmtPaid(amount);

        if (enrollmentConsumption.getAmtPaidLeft() < enrollmentTransaction.getAmtPaid()) {
            enrollmentConsumption.setNegativeMargin(enrollmentConsumption.getNegativeMargin() + enrollmentTransaction.getAmtPaid() - enrollmentConsumption.getAmtPaidLeft());
            enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());
            enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());
            enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
            enrollmentConsumption.setAmtPaidPLeft(0);
            enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
            enrollmentConsumption.setAmtPaidNPLeft(0);
            enrollmentConsumption.setAmtPaidLeft(0);
            enrollmentTransaction.setNegativeMargin(true);
        } else {
            if (enrollmentConsumption.getDiscountToAmtRatio() == null) {
                enrollmentConsumption.setDiscountToAmtRatio(0f);
            }
            int discountAmount = (int) (enrollmentConsumption.getDiscountToAmtRatio() * enrollmentTransaction.getAmtPaid());
            if (enrollmentConsumption.getAmtPaidFromDiscountLeft() >= discountAmount) {
                enrollmentTransaction.setAmtPaidFromDiscount(discountAmount);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - enrollmentTransaction.getAmtPaidFromDiscount());
            } else {
                enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
                enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
            }
            int amtLeft = enrollmentTransaction.getAmtPaid() - enrollmentTransaction.getAmtPaidFromDiscount();
            if (enrollmentConsumption.getAmtPaidPLeft() < amtLeft) {
                amtLeft = amtLeft - enrollmentConsumption.getAmtPaidPLeft();
                enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());
                enrollmentConsumption.setAmtPaidPLeft(0);
                if (amtLeft > enrollmentConsumption.getAmtPaidNPLeft()) {
                    amtLeft = amtLeft - enrollmentConsumption.getAmtPaidNPLeft();
                    enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());
                    enrollmentConsumption.setAmtPaidNPLeft(0);
                    if (enrollmentConsumption.getAmtPaidFromDiscountLeft() >= amtLeft) {
                        enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - amtLeft);
                        enrollmentTransaction.setAmtPaidFromDiscount(enrollmentTransaction.getAmtPaidFromDiscount() + amtLeft);
                    } else {
                        throw new InternalServerErrorException(ErrorCode.INSUFFICIENT_CREDITS, "Inconsistency in amount");
                    }

                } else {
                    enrollmentTransaction.setAmtPaidNP(amtLeft);
                    enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - amtLeft);
                }
            } else {
                enrollmentTransaction.setAmtPaidP(amtLeft);
                enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() - amtLeft);
            }
            enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - enrollmentTransaction.getAmtPaid());
        }

    }

    public void performSessionConsumption(SessionSnapshot sessionSnapshot) {

        boolean checkForTrialConsumption = false;
        if (sessionSnapshot.isTrialSnapShot()) {

            checkForTrialConsumption = true;

        }

        List<EnrollmentConsumption> enrollmentConsumptionList = enrollmentConsumptionDAO.getByBatchId(sessionSnapshot.getBatchIds(), null);
        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsOfBatch(sessionSnapshot.getBatchIds());

        logger.info("EnrollmentConsumptionList: " + enrollmentConsumptionList);
        logger.info("Enrollments: " + enrollments);
        logger.info("CheckForRegistration: " + checkForTrialConsumption);

        Map<String, Enrollment> enrollmentMap = new HashMap();

        for (Enrollment enrollment : enrollments) {
            enrollmentMap.put(enrollment.getId(), enrollment);
        }

        for (EnrollmentConsumption enrollmentConsumption : enrollmentConsumptionList) {

            Enrollment enrollment = enrollmentMap.get(enrollmentConsumption.getEnrollmentId());
            if (enrollment == null) {
                logger.error("Enrollment and EnrollmentConsumption Mismatch: " + enrollmentConsumption.getEnrollmentId());
                continue;
            }

            if (enrollmentConsumption.isEnrollmentEnded()) {
                logger.info("Enrollment Ended");
                continue;
            }

            if (enrollmentConsumption.getRegAmtLeft() > 0) {
                try {
                    performConsumptionForTrialEnrollment(enrollment, sessionSnapshot.getSessionId());
                } catch (Exception ex) {
                    logger.error("Trial Consumption failed for enrollment: " + enrollment.getId()
                            + " Error " + ex.getMessage());
                }
            }

            if (!checkForTrialConsumption) {

                if (OTMSessionType.EXTRA_REGULAR.equals(sessionSnapshot.getoTMSessionType())) {
                    continue;
                }

                if (OTMSessionType.ABRUPT_ENDING.equals(sessionSnapshot.getoTMSessionType())) {
                    continue;
                }

                if (OTMSessionType.NON_REGULAR.equals(sessionSnapshot.getoTMSessionType())) {
                    continue;
                }

                if (OTMSessionType.TEST.equals(sessionSnapshot.getoTMSessionType())) {
                    continue;
                }

                if (OTMSessionType.PTM.equals(sessionSnapshot.getoTMSessionType())) {
                    continue;
                }

                if (enrollmentConsumption.getHourlyrate() == null) {
                    logger.error("Hourly rate not available for enrollment consumption: " + enrollmentConsumption);
                    continue;
                }

                int amtToBeDeducted = (int) (enrollmentConsumption.getHourlyrate() * ((float) (sessionSnapshot.getEndTime() - sessionSnapshot.getStartTime()) / DateTimeUtils.MILLIS_PER_HOUR));

                if (EntityStatus.ACTIVE.equals(enrollment.getStatus()) || (EntityStatus.INACTIVE.equals(enrollment.getStatus()) && (enrollmentConsumption.getAmtPaidLeft() - enrollmentConsumption.getNegativeMargin() >= amtToBeDeducted))) {
                    try {
                        if (amtToBeDeducted > 0) {
                            performConsumptionForRegularEnrollment(enrollment, sessionSnapshot);
                        }
                    } catch (Exception ex) {
                        logger.error("Regular Consumption failed for enrollment: " + enrollment.getId()
                                + " error: " + ex.getMessage());
                    }
                } else {
                    logger.info("Enrollment Inactive");
                }

            }

        }

    }

    public void performSessionConsumptionForMigration() {

    }

    public OTMRefundReq prepareRefundReqForTrialRefund(Enrollment enrollment, EnrollmentConsumption enrollmentConsumption, EnrollmentTransaction enrollmentTransaction) throws InternalServerErrorException {
        OTMRefundReq oTMRefundReq = new OTMRefundReq();
        oTMRefundReq.setRegistration(true);
        if (enrollmentConsumption.getRegAmtLeft() > 0) {
            enrollmentTransaction.setRegAmtPaid(enrollmentConsumption.getRegAmtLeft());
            enrollmentTransaction.setRegAmtPaidFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());
            enrollmentTransaction.setRegAmtPaidNP(enrollmentConsumption.getRegAmtNPLeft());
            enrollmentTransaction.setRegAmtPaidP(enrollmentConsumption.getRegAmtPLeft());
            enrollmentConsumption.setRegAmtLeft(0);
            enrollmentConsumption.setRegAmtPLeft(0);
            enrollmentConsumption.setRegAmtNPLeft(0);
            enrollmentConsumption.setRegAmtLeftFromDiscount(0);
            oTMRefundReq.setAmount(enrollmentTransaction.getRegAmtPaid() - enrollmentTransaction.getRegAmtPaidFromDiscount());
            oTMRefundReq.setAmtFromDiscount(enrollmentTransaction.getRegAmtPaidFromDiscount());
            oTMRefundReq.setPromotionalAmt(enrollmentTransaction.getRegAmtPaidP());
            oTMRefundReq.setNonPromotionalAmt(enrollmentTransaction.getRegAmtPaidNP());
        } else {
            oTMRefundReq.setAmtCGNP(enrollmentConsumption.getRegAmtPaidNP());
            oTMRefundReq.setAmtCGP(enrollmentConsumption.getRegAmtPaidP());
            oTMRefundReq.setAmtCGFromDiscount(enrollmentConsumption.getRegAmtPaidFromDiscount());
            oTMRefundReq.setCustomerGrievence(true);
        }

        return oTMRefundReq;

    }

    public OTMRefundReq getCGRefundReqForRegularRefund(Enrollment enrollment, EnrollmentRefundReq enrollmentRefundReq, EnrollmentConsumption enrollmentConsumption) throws BadRequestException {

        Long currentTime = System.currentTimeMillis();

        OTMRefundReq oTMRefundReq = new OTMRefundReq();

        if (enrollmentRefundReq.getEndedTime() == null) {
            return oTMRefundReq;
        }

        if (currentTime - enrollmentRefundReq.getEndedTime() > maxRefundTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "End time should be within the last 35 days");
        }

        Session session = sqlSessionfactory.getSessionFactory().openSession();
        List<EnrollmentTransaction> enrollmentTransactionList = new ArrayList<>();
        try {
            enrollmentTransactionList = enrollmentTransactionDAO.getForRefundCheck(enrollment.getId(), enrollmentRefundReq.getEndedTime(), session, enrollment.getState());
        } catch (Exception ex) {
            logger.error("Exception: fetchin enrollment transaction" + ex.getMessage());
        } finally {
            session.close();
        }

        logger.info("Enrollment Transactions: " + enrollmentTransactionList);

        try {
            if (enrollmentTransactionList.size() > 0 && EnrollmentState.REGULAR.equals(enrollment.getState())) {
                oTMRefundReq.setCustomerGrievence(true);

                for (EnrollmentTransaction enrollmentTransaction : enrollmentTransactionList) {
                    if (oTMRefundReq.getAmtCGP() == null) {
                        oTMRefundReq.setAmtCGP(enrollmentTransaction.getAmtPaidP());

                    } else {

                        oTMRefundReq.setAmtCGP(oTMRefundReq.getAmtCGP() + enrollmentTransaction.getAmtPaidP());

                    }

                    if (oTMRefundReq.getAmtCGNP() == null) {

                        oTMRefundReq.setAmtCGNP(enrollmentTransaction.getAmtPaidNP());

                    } else {

                        oTMRefundReq.setAmtCGNP(oTMRefundReq.getAmtCGNP() + enrollmentTransaction.getAmtPaidNP());

                    }

                    if (oTMRefundReq.getAmtCGFromDiscount() == null) {

                        oTMRefundReq.setAmtCGFromDiscount(enrollmentTransaction.getAmtPaidFromDiscount());

                    } else {

                        oTMRefundReq.setAmtCGFromDiscount(oTMRefundReq.getAmtCGFromDiscount() + enrollmentTransaction.getAmtPaidFromDiscount());

                    }

                }

            }

        } catch (Exception ex) {
            logger.error("Refund for enrollment id: " + enrollment+" Error "+ex.getMessage());
        }

        return oTMRefundReq;

    }

    public OTMRefundReq prepareRefundReqForRegularRefund(Enrollment enrollment, EnrollmentConsumption enrollmentConsumption, EnrollmentTransaction enrollmentTransaction, OTMRefundReq oTMRefundReq) throws InternalServerErrorException {

        logger.info("RefundReq : " + oTMRefundReq);

        if (enrollmentConsumption.getNegativeMargin() != null && enrollmentConsumption.getNegativeMargin() > 0) {
            if (enrollmentConsumption.getNegativeMargin() > enrollmentConsumption.getAmtPaidLeft()) {

                int negativeLeft = enrollmentConsumption.getNegativeMargin() - enrollmentConsumption.getAmtPaidLeft();
                enrollmentTransaction.setAmtPaid(0);
                if (oTMRefundReq.isCustomerGrievence()) {

                    //enrollmentTransaction.setAmtFromDiscount(0);
                    if (negativeLeft > oTMRefundReq.getAmtCGFromDiscount() + oTMRefundReq.getAmtCGNP() + oTMRefundReq.getAmtCGP()) {
                        oTMRefundReq.setAmtCGFromDiscount(0);
                        oTMRefundReq.setAmtCGP(0);
                        oTMRefundReq.setAmtCGNP(0);
                    } else {

                        negativeLeft = negativeLeft - oTMRefundReq.getAmtCGFromDiscount();

                        if (negativeLeft > oTMRefundReq.getAmtCGP()) {
                            oTMRefundReq.setAmtCGNP(oTMRefundReq.getAmtCGNP() - (negativeLeft - oTMRefundReq.getAmtCGP()));
                            oTMRefundReq.setAmtCGP(0);
                        } else {
                            oTMRefundReq.setAmtCGP(oTMRefundReq.getAmtCGP() - negativeLeft);
                        }
                    }

                }

                enrollmentTransaction.setAmtPaidFromDiscount(0);
                enrollmentTransaction.setAmtPaidP(0);
                enrollmentTransaction.setAmtPaidNP(0);

            } else {
                enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaidLeft() - enrollmentConsumption.getNegativeMargin());
                int amt = enrollmentTransaction.getAmtPaid();
                if (enrollmentConsumption.getDiscountToAmtRatio() == null) {
                    enrollmentConsumption.setDiscountToAmtRatio(0f);
                }
                int discountAmt = (int) (enrollmentConsumption.getDiscountToAmtRatio() * amt);
                if (discountAmt > enrollmentConsumption.getAmtPaidFromDiscountLeft()) {
                    discountAmt = enrollmentConsumption.getAmtPaidFromDiscountLeft();
                }

                enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - amt);
                enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - discountAmt);

                enrollmentTransaction.setAmtPaidFromDiscount(discountAmt);
                int amtLeft = amt - discountAmt;

                if (enrollmentConsumption.getAmtPaidNPLeft() >= amtLeft) {
                    enrollmentTransaction.setAmtPaidNP(amtLeft);

                    enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - amtLeft);
                } else {
                    enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());

                    enrollmentConsumption.setAmtPaidNPLeft(0);

                }

                if (enrollmentConsumption.getAmtPaidPLeft() >= amtLeft - enrollmentTransaction.getAmtPaidNP()) {

                    enrollmentTransaction.setAmtPaidP(amtLeft - enrollmentTransaction.getAmtPaidNP());

                    enrollmentConsumption.setAmtPaidPLeft(enrollmentConsumption.getAmtPaidPLeft() - enrollmentTransaction.getAmtPaidP());

                } else {
                    int requiredAmt = amtLeft - enrollmentTransaction.getAmtPaidNP();

                    enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());
                    enrollmentConsumption.setAmtPaidPLeft(0);

                    requiredAmt = requiredAmt - enrollmentTransaction.getAmtPaidP();

                    if (enrollmentConsumption.getAmtPaidFromDiscountLeft() >= requiredAmt) {
                        enrollmentTransaction.setAmtPaidFromDiscount(enrollmentTransaction.getAmtPaidFromDiscount() + requiredAmt);
                        enrollmentConsumption.setAmtPaidFromDiscountLeft(enrollmentConsumption.getAmtPaidFromDiscountLeft() - requiredAmt);
                    } else {
                        throw new InternalServerErrorException(ErrorCode.INSUFFICIENT_CREDITS, "Inconsistency in amount");
                    }
                }
            }

            oTMRefundReq.setAmount(enrollmentTransaction.getAmtPaid() - enrollmentTransaction.getAmtPaidFromDiscount());
            oTMRefundReq.setPromotionalAmt(enrollmentTransaction.getAmtPaidP());
            oTMRefundReq.setNonPromotionalAmt(enrollmentTransaction.getAmtPaidNP());
            oTMRefundReq.setAmtFromDiscount(enrollmentTransaction.getAmtPaidFromDiscount());
        } else {

            enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaidLeft());
            enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
            enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft());
            enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft());

            enrollmentConsumption.setAmtPaidLeft(0);
            enrollmentConsumption.setAmtPaidFromDiscountLeft(0);
            enrollmentConsumption.setAmtPaidNPLeft(0);
            enrollmentConsumption.setAmtPaidPLeft(0);

            oTMRefundReq.setAmount(enrollmentTransaction.getAmtPaid() - enrollmentTransaction.getAmtPaidFromDiscount());
            oTMRefundReq.setPromotionalAmt(enrollmentTransaction.getAmtPaidP());
            oTMRefundReq.setNonPromotionalAmt(enrollmentTransaction.getAmtPaidNP());
            oTMRefundReq.setAmtFromDiscount(enrollmentTransaction.getAmtPaidFromDiscount());

        }
        enrollmentTransaction.setEnrollmentId(enrollment.getId());
        enrollmentTransaction.setConsumptionType(EntityStatus.ENDED);

        return oTMRefundReq;

    }

    public boolean refundForEnrollment(EnrollmentRefundReq enrollmentRefundReq, Enrollment enrollment) throws BadRequestException, ForbiddenException {

        if (EntityStatus.ENDED.equals(enrollment.getStatus())) {
            return false;
        }

        boolean result = false;
        Batch batch = batchDAO.getById(enrollment.getBatchId());
        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();

        try {
            transaction.begin();

            EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollment.getId(), true, session, LockMode.PESSIMISTIC_WRITE);
            if (enrollmentConsumption == null) {
                logger.error("EnrollmentConsumption not found for : " + enrollment.getId());
                throw new NotFoundException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "EnrollmentConsumption not found for : " + enrollment.getId());
            }

            logger.info("EnrollmentConsumption: " + enrollmentConsumption);
            if (batch.isContentBatch() && (!Objects.equals(enrollmentConsumption.getAmtPaid(), enrollmentConsumption.getAmtPaidLeft()))) {
                result = true;
                logger.info("No refund for consumed content batch");
            } else if (batch.isContentBatch() && enrollmentConsumption.getCreationTime() > batch.getEndTime()) {
                result = true;
                logger.info("No refund for after batch end enrollment");
            } else {

                EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
                enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentTransaction.setEnrollmentId(enrollment.getId());
                List<OTMRefundReq> oTMRefundReqs = new ArrayList<>();
                OTMRefundReq oTMRefundReq = new OTMRefundReq();
                if (EnrollmentState.TRIAL.equals(enrollment.getState())) {
                    enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_REFUND);
                    oTMRefundReq = prepareRefundReqForTrialRefund(enrollment, enrollmentConsumption, enrollmentTransaction);

                } else if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
                    enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REFUND);
                    oTMRefundReq = prepareRefundReqForRegularRefund(enrollment, enrollmentConsumption, enrollmentTransaction, getCGRefundReqForRegularRefund(enrollment, enrollmentRefundReq, enrollmentConsumption));

                }
                logger.info("OTMRefundReq: " + oTMRefundReq);
                if (oTMRefundReq.isCustomerGrievence() && (oTMRefundReq.getAmtCGFromDiscount() + oTMRefundReq.getAmtCGP() + oTMRefundReq.getAmtCGNP()) > 0) {
                    OTMRefundReq oTMRefundReq1 = new OTMRefundReq();
                    oTMRefundReq1.setCustomerGrievence(true);
                    oTMRefundReq1.setAmtCGFromDiscount(oTMRefundReq.getAmtCGFromDiscount());
                    oTMRefundReq1.setAmtCGNP(oTMRefundReq.getAmtCGNP());
                    oTMRefundReq1.setAmtCGP(oTMRefundReq.getAmtCGP());
                    oTMRefundReq1.setReasonNote(enrollmentRefundReq.getReasonNote());
                    oTMRefundReq1.setUserId(Long.valueOf(enrollment.getUserId()));
                    oTMRefundReq1.setEnrollmentId(enrollment.getId());
                    oTMRefundReq1.setEnrollmentTransactionContextType(enrollmentTransaction.getEnrollmentTransactionContextType());

                    oTMRefundReq1.setRegistration(EnrollmentState.TRIAL.equals(enrollment.getState()));

                    oTMRefundReqs.add(oTMRefundReq1);

                }

                if (oTMRefundReq.getAmount() != null && oTMRefundReq.getAmount() > 0) {
                    OTMRefundReq oTMRefundReq1 = new OTMRefundReq();
                    oTMRefundReq1.setAmount(oTMRefundReq.getAmount());
                    oTMRefundReq1.setPromotionalAmt(oTMRefundReq.getPromotionalAmt());
                    oTMRefundReq1.setNonPromotionalAmt(oTMRefundReq.getNonPromotionalAmt());
                    oTMRefundReq1.setAmtFromDiscount(oTMRefundReq.getAmtFromDiscount());
                    oTMRefundReq1.setReasonNote(enrollmentRefundReq.getReasonNote());
                    oTMRefundReq1.setUserId(Long.valueOf(enrollment.getUserId()));
                    oTMRefundReq1.setEnrollmentId(enrollment.getId());
                    oTMRefundReq1.setEnrollmentTransactionContextType(enrollmentTransaction.getEnrollmentTransactionContextType());
                    oTMRefundReq1.setRegistration(EnrollmentState.TRIAL.equals(enrollment.getState()));

                    oTMRefundReqs.add(oTMRefundReq1);
                }

                logger.info("Enrollment Consumption: " + enrollmentConsumption);
                logger.info("Enrollment Transaction: " + enrollmentTransaction);

                takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);
                enrollmentTransaction.setEnrollmentTransactionContextId(enrollment.getId());
                enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                if (enrollmentTransaction.getAmtPaid() > 0 || enrollmentTransaction.getRegAmtPaid() > 0) {
                    enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
                }
                transaction.commit();

                logger.info(oTMRefundReqs);

                if (ArrayUtils.isNotEmpty(oTMRefundReqs)) {
                    for (OTMRefundReq oTMRefundReq1 : oTMRefundReqs) {
                        OTMConsumptionPaymentRes oTMConsumptionPaymentRes = makeDineroRefund(oTMRefundReq1);
                        if (!oTMConsumptionPaymentRes.isSuccess()) {
                            logger.error("Dinero transaction failed for refund of: " + enrollment.getId());
                        }
                    }
                }
                result = true;
            }

        } catch (Exception ex) {
            logger.error("refundForEnrollment Exeption: " + ex.getMessage());
            session.getTransaction().rollback();

        } finally {

            session.close();
        }

        if (result) {
            if (!EntityStatus.ENDED.equals(enrollment.getStatus())) {
                boolean endEnrollment = performEndEnrollmentConsumption(enrollment, EnrollmentTransactionContextType.END_ENROLLMENT_CONSUMPTION);
                if (!endEnrollment) {
                    logger.error("End enrollmentConsumption failed for: " + enrollment);
                }
            }

            performEndEnrollmentTasks(enrollment ,enrollmentRefundReq);

            EnrollmentPojo enrollmentInfo = enrollmentManager.createEnrollmentInfo(enrollment, false);
            if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
                try {
                    String markOrderForfeited = dineroEndpoint + "/payment/markOrderEnded";
                    JSONObject markOrderForfeitedReq = new JSONObject();
                    if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollmentInfo.getEntityType())) {
                        markOrderForfeitedReq.put("entityType", enrollmentInfo.getEntityType());
                        markOrderForfeitedReq.put("entityId", enrollmentInfo.getEntityId());
                    } else {
                        markOrderForfeitedReq.put("entityType", com.vedantu.session.pojo.EntityType.OTF);
                        markOrderForfeitedReq.put("entityId", enrollmentInfo.getBatchId());
                    }
                    markOrderForfeitedReq.put("userId", enrollmentInfo.getUserId());

                    ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                            markOrderForfeitedReq.toString());
                    VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
                    String jsonString_1 = markOrderForfeitedRes.getEntity(String.class);
                    logger.info("Response for markOrderForfeited  : " + jsonString_1);
                } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
                    logger.error("Error in markOrderForfeited for enrollment  " + enrollmentInfo.getId(), ex);
                }
            }

            //List<StatusChangeTime> statuses = enrollment.getStatusChangeTime();
            //StatusChangeTime statusChangeTime = new StatusChangeTime();
            //statusChangeTime.setChangeTime(System.currentTimeMillis());
            //statusChangeTime.setNewStatus(EntityStatus.ACTIVE);
            //statusChangeTime.setPreviousStatus(enrollment.getStatus());
            //enrollment.setStatus(EntityStatus.ENDED);
            //statuses.add(statusChangeTime);
            //enrollmentDAO.save(enrollment);
            //Batch batch = batchDAO.getById(enrollment.getBatchId());
            //batch.getEnrolledStudents().remove(enrollment.getUserId());
            //batchDAO.save(batch);
        }

        return result;

    }
    public void performEndEnrollmentTasks(Enrollment enrollment ,EnrollmentRefundReq enrollmentRefundReq) throws BadRequestException, ForbiddenException {

        OTFEnrollmentReq oTFEnrollmentReq = new OTFEnrollmentReq();
        oTFEnrollmentReq.setBatchId(enrollment.getBatchId());
        oTFEnrollmentReq.setCourseId(enrollment.getCourseId());
        oTFEnrollmentReq.setEndReason(enrollmentRefundReq.getReasonNote());
        oTFEnrollmentReq.setEndedBy(enrollmentRefundReq.getCallingUserId());
        oTFEnrollmentReq.setUserId(enrollment.getUserId());
        oTFEnrollmentReq.setRole(enrollment.getRole());
        oTFEnrollmentReq.setId(enrollment.getId());
        oTFEnrollmentReq.setStatus("ENDED");
        enrollment = enrollmentManager.markStatus(oTFEnrollmentReq);

        // not marking student calendar entries any more
        if (Objects.nonNull(enrollment) && Role.STUDENT.equals(enrollment.getRole())) {
            return;
        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", enrollment.getUserId());
        payload.put("batchId", enrollment.getBatchId());
        payload.put("status", enrollment.getStatus());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTF_UPDATE_CALENDAR, payload);
        asyncTaskFactory.executeTask(params);


    }
    public boolean performEndEnrollmentConsumption(Enrollment enrollment, EnrollmentTransactionContextType enrollmentTransactionContextType) {

        boolean result = false;

        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();

        try {

            transaction.begin();
            EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollment.getId(), true, session, LockMode.PESSIMISTIC_WRITE);
            if (enrollmentConsumption == null) {
                logger.error("EnrollmentConsumption not found for : " + enrollment.getId());
                throw new NotFoundException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "EnrollmentConsumption not found for : " + enrollment.getId());
            }
            EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
            enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
            enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
            enrollmentTransaction.setEnrollmentId(enrollment.getId());
            enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaidLeft() + enrollmentConsumption.getRegAmtLeft());
            enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidPLeft() + enrollmentConsumption.getRegAmtPLeft());
            enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNPLeft() + enrollmentConsumption.getRegAmtNPLeft());
            enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft() + enrollmentConsumption.getRegAmtLeftFromDiscount());
            enrollmentTransaction.setEnrollmentTransactionContextType(enrollmentTransactionContextType);
            enrollmentTransaction.setConsumptionType(EntityStatus.ENDED);
            enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
            enrollmentTransaction.setEnrollmentTransactionContextId(enrollment.getId());

            enrollmentConsumption.setAmtPaidLeft(0);
            enrollmentConsumption.setAmtPaidNPLeft(0);
            enrollmentConsumption.setAmtPaidPLeft(0);
            enrollmentConsumption.setAmtPaidFromDiscountLeft(0);

            enrollmentConsumption.setRegAmtLeft(0);
            enrollmentConsumption.setRegAmtNPLeft(0);
            enrollmentConsumption.setRegAmtPLeft(0);
            enrollmentConsumption.setRegAmtLeftFromDiscount(0);
            enrollmentConsumption.setEnrollmentEnded(true);

            takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);
            enrollmentConsumptionDAO.update(enrollmentConsumption, session);
            if (enrollmentTransaction.getAmtPaid() > 0 || enrollmentTransaction.getRegAmtPaid() > 0) {
                enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
            }

            OTMConsumptionReq oTMConsumptionReq = createOTMConsumptionReq(enrollmentConsumption, enrollmentTransaction);
            transaction.commit();
            if ((oTMConsumptionReq.getAmount() != null && oTMConsumptionReq.getAmount() > 0) || (oTMConsumptionReq.getNonPromotionalAmt() != null && oTMConsumptionReq.getNonPromotionalAmt() != null && oTMConsumptionReq.getPromotionalAmt() + oTMConsumptionReq.getNonPromotionalAmt() > 0)) {
                makeDineroTransaction(oTMConsumptionReq);
            }
            result = true;

        } catch (Exception ex) {
            logger.error("performEndEnrollmentConsumption Exception: " + ex.getMessage());
            session.getTransaction().rollback();

        } finally {

            session.close();
        }

        return result;

    }

    public void createConsumptionTransaction(CreateConsumptionTransactionReq req) {
        EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
        enrollmentTransaction.setCreationTime(req.getEndTime());
        enrollmentTransaction.setLastUpdatedTime(req.getEndTime());
        enrollmentTransaction.setAmtPaid(req.getAmtPaid());
        enrollmentTransaction.setAmtPaidNP(req.getAmtPaidNP());
        enrollmentTransaction.setAmtPaidP(0);
        enrollmentTransaction.setAmtPaidFromDiscount(0);
        enrollmentTransaction.setAmtLeft(req.getAmtLeft());
        enrollmentTransaction.setAmtLeftP(req.getAmtLeftP());
        enrollmentTransaction.setAmtLeftNP(req.getAmtLeftNP());
        enrollmentTransaction.setAmtLeftFromDiscount(req.getAmtLeftFromDiscount());
        enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
        enrollmentTransaction.setEnrollmentConsumptionId(req.getEnrollmentConsumptionId());
        enrollmentTransaction.setEnrollmentId(req.getEnrollmentId());
        enrollmentTransaction.setEnrollmentTransactionContextId(req.getEnrollmentTransactionContextId());
        enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.SESSION_CONSUMPTION);
        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
        enrollmentTransaction.setRegAmtLeft(0);
        enrollmentTransaction.setRegAmtLeftFromDiscount(0);
        enrollmentTransaction.setRegAmtLeftNP(0);
        enrollmentTransaction.setRegAmtLeftP(0);
        enrollmentTransaction.setRegAmtPaid(0);
        enrollmentTransaction.setRegAmtPaidFromDiscount(0);
        enrollmentTransaction.setRegAmtPaidNP(0);
        enrollmentTransaction.setRegAmtPaidP(0);
        enrollmentTransaction.setNegativeMarginLeft(0);
        enrollmentTransaction.setRemarks(req.getRemarks());
        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();
            enrollmentTransactionDAO.create(enrollmentTransaction, session);
            transaction.commit();
        } catch (Exception ex) {
            logger.error("createConsumptionTransaction Error: " + ex.getMessage());
            session.getTransaction().rollback();

        } finally {

            session.close();
        }
    }

    public void createCreditTransaction(CreateConsumptionTransactionReq req) {
        EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
        enrollmentTransaction.setCreationTime(req.getEndTime());
        enrollmentTransaction.setLastUpdatedTime(req.getEndTime());
        enrollmentTransaction.setAmtPaid(req.getAmtPaid());
        enrollmentTransaction.setAmtPaidNP(req.getAmtPaidNP());
        enrollmentTransaction.setAmtPaidP(0);
        enrollmentTransaction.setAmtPaidFromDiscount(0);
        enrollmentTransaction.setAmtLeft(req.getAmtLeft());
        enrollmentTransaction.setAmtLeftP(req.getAmtLeftP());
        enrollmentTransaction.setAmtLeftNP(req.getAmtLeftNP());
        enrollmentTransaction.setAmtLeftFromDiscount(req.getAmtLeftFromDiscount());
        enrollmentTransaction.setEnrollmentConsumptionId(req.getEnrollmentConsumptionId());
        enrollmentTransaction.setEnrollmentId(req.getEnrollmentId());
        enrollmentTransaction.setEnrollmentTransactionContextId(req.getEnrollmentTransactionContextId());
        enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REFUND_ADJUSTMENT);
        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.CREDIT);
        enrollmentTransaction.setRegAmtLeft(0);
        enrollmentTransaction.setRegAmtLeftFromDiscount(0);
        enrollmentTransaction.setRegAmtLeftNP(0);
        enrollmentTransaction.setRegAmtLeftP(0);
        enrollmentTransaction.setRegAmtPaid(0);
        enrollmentTransaction.setRegAmtPaidFromDiscount(0);
        enrollmentTransaction.setRegAmtPaidNP(0);
        enrollmentTransaction.setRegAmtPaidP(0);
        enrollmentTransaction.setNegativeMarginLeft(0);
        enrollmentTransaction.setRemarks(req.getRemarks());
        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();
            enrollmentTransactionDAO.create(enrollmentTransaction, session);
            transaction.commit();
        } catch (Exception ex) {
            logger.error("createCreditTransaction Error: " + ex.getMessage());
            session.getTransaction().rollback();

        } finally {

            session.close();
        }
    }

}
