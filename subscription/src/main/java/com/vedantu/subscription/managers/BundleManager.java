package com.vedantu.subscription.managers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.mail.internet.InternetAddress;

import com.vedantu.subscription.response.*;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.BaseSubscriptionPurchaseEntity;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.RefundPolicy;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.BaseSubscriptionPlanReq;
import com.vedantu.dinero.request.RefundReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.request.SubmitCashChequeReq;
import com.vedantu.dinero.request.ValidateCouponReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.ValidateCouponRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.lms.cmds.enums.VideoType;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.lms.request.GetBundlesRequest;
import com.vedantu.lms.response.HomeFeedBundleResponse;
import com.vedantu.lms.response.MicroCourseCollectionResponse;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.enums.BatchStatus;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.Language;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BatchDashboardInfo;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.BundleEntityInfo;
import com.vedantu.onetofew.pojo.BundleStateChangeTime;
import com.vedantu.onetofew.pojo.BundleValidTillChangeTime;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.EnrollmentStateChangeTime;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.onetofew.pojo.ParentEntityChangeTime;
import com.vedantu.onetofew.pojo.SessionPojoWithRefId;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.onetofew.request.GetBatchesForDashboardReq;
import com.vedantu.onetofew.request.GetInstalmentReq;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.onetofew.request.OTFWebinarSessionReq;
import com.vedantu.onetofew.request.UpdateWebinarSessionsAttendenceReq;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.AIOGroupDAO;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BundleDAO;
import com.vedantu.subscription.dao.BundleEnrolmentDAO;
import com.vedantu.subscription.dao.BundleEntityDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.OTFBundleDAO;
import com.vedantu.subscription.dao.SectionDAO;
import com.vedantu.subscription.dao.SubscriptionProgramDAO;
import com.vedantu.subscription.dao.SubscriptionUpdateDAO;
import com.vedantu.subscription.dao.TrialDAO;
import com.vedantu.subscription.dao.VideoAnalyticsDAO;
import com.vedantu.subscription.entities.mongo.AIOGroup;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.entities.mongo.BundleMinMaxExceptionList;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.OTMBundle;
import com.vedantu.subscription.entities.mongo.PremiumSubscription;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.subscription.entities.mongo.SubscriptionProgram;
import com.vedantu.subscription.entities.mongo.SubscriptionUpdate;
import com.vedantu.subscription.entities.mongo.Trial;
import com.vedantu.subscription.entities.mongo.VideoAnalytics;
import com.vedantu.subscription.enums.BundleContentType;
import com.vedantu.subscription.enums.ContentPriceType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.SubscriptionPlanType;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.CourseType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.BannerPojo;
import com.vedantu.subscription.pojo.BundleAggregation;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.EnrolledBundlesRes;
import com.vedantu.subscription.pojo.MicroCourseBasicInfo;
import com.vedantu.subscription.pojo.TestBundleContentInfo;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.TrialData;
import com.vedantu.subscription.pojo.VideoBundleContentInfo;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.pojo.WebinarBundleContentInfo;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import com.vedantu.subscription.pojo.bundle.Classes;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlan;
import com.vedantu.subscription.request.AIOGroupResp;
import com.vedantu.subscription.request.AddEditBundleReq;
import com.vedantu.subscription.request.AddEditBundleSessionReq;
import com.vedantu.subscription.request.AddEditBundleWebinarReq;
import com.vedantu.subscription.request.AddVideoAnalyticsDataReq;
import com.vedantu.subscription.request.BundleCreateEnrolmentReq;
import com.vedantu.subscription.request.BundleDetailsUpdateReq;
import com.vedantu.subscription.request.CheckBundleEnrollmentReq;
import com.vedantu.subscription.request.EditMultipleBundleReq;
import com.vedantu.subscription.request.GetAIOGroupReq;
import com.vedantu.subscription.request.GetBundleEnrolmentsReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.request.LandingPageCollectionResp;
import com.vedantu.subscription.request.MarkEnrolmentStateReq;
import com.vedantu.subscription.request.PremiumSubscriptionRequest;
import com.vedantu.subscription.request.UpdateAIOGroupReq;
import com.vedantu.subscription.request.UpdateBundleStateBulk;
import com.vedantu.subscription.request.UpdateStatusReq;
import com.vedantu.subscription.request.addSubscriptionProgramReq;
import com.vedantu.subscription.request.deEnrollSubscriptionBatch;
import com.vedantu.subscription.request.getMicroCoursesWithFilter;
import com.vedantu.subscription.request.section.EnrollmentFailureReq;
import com.vedantu.subscription.request.section.SectionEnrollmentReq;
import com.vedantu.subscription.request.section.UpdateBundleBatchEnrollmentsReq;
import com.vedantu.subscription.request.section.UpdateSectionEnrollmentReq;
import com.vedantu.subscription.response.BaseSubscriptionPlanResp;
import com.vedantu.subscription.response.BatchBundlePojo;
import com.vedantu.subscription.response.BundleEnrollmentResp;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.subscription.response.BundleWrapper;
import com.vedantu.subscription.response.FullSubscriptionPlanResp;
import com.vedantu.subscription.response.PremiumSubscriptionResp;
import com.vedantu.subscription.response.SubscriptionPlanInfoResp;
import com.vedantu.subscription.response.SubscriptionProgramResp;
import com.vedantu.subscription.response.SubscriptionUpdateResp;
import com.vedantu.subscription.response.TrialExpirationResp;
import com.vedantu.subscription.response.TrialInfo;
import com.vedantu.subscription.response.VideoInfo;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.viewobject.request.AddBatchToBundleReq;
import com.vedantu.subscription.viewobject.request.BundleSearchReq;
import com.vedantu.subscription.viewobject.request.ClickToEnrollReq;
import com.vedantu.subscription.viewobject.request.EndBundleEnrollmentReq;
import com.vedantu.subscription.viewobject.request.FreePassReq;
import com.vedantu.subscription.viewobject.request.GetBundleEnrollmentReq;
import com.vedantu.subscription.viewobject.request.GetBundleTestVideoReq;
import com.vedantu.subscription.viewobject.request.SubscriptionBundleRes;
import com.vedantu.subscription.viewobject.request.SubscriptionCoursesReq;
import com.vedantu.subscription.viewobject.request.SubscriptionCoursesRes;
import com.vedantu.subscription.viewobject.request.SubscriptionProgramReq;
import com.vedantu.subscription.viewobject.request.SubscriptionQueryReq;
import com.vedantu.subscription.viewobject.response.BundleTestFilterRes;
import com.vedantu.subscription.viewobject.response.BundleTestRes;
import com.vedantu.subscription.viewobject.response.BundleVideoFilterRes;
import com.vedantu.subscription.viewobject.response.BundleVideoRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.request.GetUserEnrollmentsReq;
import com.vedantu.util.request.MarkOrderEndedRes;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

/**
 * Created by somil on 09/05/17.
 */
@Service
public class BundleManager {

    @Autowired
    private BundleDAO bundleDAO;

    @Autowired
    private CourseDAO courseDAO;

    @Autowired
    private BundleTestManager bundleTestManager;

    @Autowired
    private BundleVideoManager bundleVideoManager;

    @Autowired
    private DashboardManager dashboardManager;

    @Autowired
    private BundleEnrolmentDAO bundleEnrolmentDAO;

    @Autowired
    private VideoAnalyticsDAO videoAnalyticsDAO;

    @Autowired
    private SubscriptionProgramDAO subscriptionProgramDAO;

    @Autowired
    private SubscriptionUpdateDAO subscriptionUpdateDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private InstalmentUtilManager instalmentUtilManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    public EnrollmentAsyncManager enrollmentAsyncManager;

    @Autowired
    private BatchManager batchManager;

    @Autowired
    private CourseManager courseManager;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private OTFBundleDAO otfBundleDAO;

    @Autowired
    private AIOGroupDAO aioGroupDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AwsSQSManager sqsManager;

    @Autowired
    private TrialDAO trialDAO;

    @Autowired
    private TrialManager trialManager;

    @Autowired
    private BundleEnrolmentManager bundleEnrolmentManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private BundleEntityDAO bundleEntityDAO;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired BundleEntityManager bundleEntityManager;

    @Autowired
    private PremiumSubscriptionManager premiumSubscriptionManager;

    @Autowired SectionManager sectionManager;

    @Autowired SectionDAO sectionDAO;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BundleManager.class);

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT") + "/onetofew/";
    private static final Long ONBOARDING_TIME = 1584642600000l;
    private static final String BUNDLE_BASIC_INFO = "BUNDLE_BASIC_INFO_";


    private static Gson gson = new Gson();


    private Map<PackageType, Set<String>> getAllBundleAndBatchForBundle(String bundleId, Set<String> processedBundles) {
        Map<PackageType, Set<String>> map = new EnumMap<PackageType, Set<String>>(PackageType.class);
        List<Bundle> bundles = bundleDAO.getBundleById(bundleId, Arrays.asList("packages"));
        if (ArrayUtils.isNotEmpty(bundles)) {
            Bundle bundle = bundles.get(0);
            List<AioPackage> packages = bundle.getPackages();
            for (AioPackage aioPackage : packages) {
                if (aioPackage.getPackageType() == PackageType.OTF) {
                    Set<String> set = CollectionUtils.putIfAbsentAndGet(map, aioPackage.getPackageType(), HashSet::new);
                    set.add(aioPackage.getEntityId());
                } else if (aioPackage.getPackageType() == PackageType.OTM_BUNDLE) {
                    if (processedBundles.add(aioPackage.getEntityId())) {
                        Set<String> set = CollectionUtils.putIfAbsentAndGet(map, aioPackage.getPackageType(), HashSet::new);
                        set.add(aioPackage.getEntityId());
                        Map<PackageType, Set<String>> bundleAndBatchForBundle = getAllBundleAndBatchForBundle(bundleId, processedBundles);
                        for (PackageType packageType : bundleAndBatchForBundle.keySet()) {
                            CollectionUtils.putIfAbsentAndGet(map, aioPackage.getPackageType(), HashSet::new)
                                    .addAll(bundleAndBatchForBundle.getOrDefault(packageType, new HashSet<>()));
                        }
                    }
                }
            }
        }
        return map;
    }

    public BundleInfo getBundle(String id, GetBundlesReq req) throws VException {
        Bundle bundle = null;
        if (ObjectId.isValid(id)) {
            bundle = bundleDAO.getBundleById(id);
        }
        if (bundle == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundle not found " + id);
        }
        BundleInfo bundleInfo = mapper.map(bundle, BundleInfo.class);
        List<BundleEntity> bundleEntities=bundleEntityDAO.getBundleEntitiesByBundleId(bundleInfo.getId(),0,1);
        if(ArrayUtils.isNotEmpty(bundleEntities)){
            BundleEntityInfo bundleEntityInfo=mapper.map(bundleEntities.get(0),BundleEntityInfo.class);
            bundleInfo.setBundleEntityInfos(Collections.singletonList(bundleEntityInfo));
        }
        if (req.getRemoveByReturnContentTypes()) {
            removeByReturnContentTypes(bundleInfo, req.getReturnContentTypes());
            removeByContentPriceTypes(bundleInfo, req.getContentPriceTypes(), req.getFeatured());
        }
        if (req.isCheckAccessControl()) {
            removeByAccessControl(bundleInfo, req.getCallingUserId(), req.getCallingUserRole());
        }

        if (Role.STUDENT.equals(req.getCallingUserRole()) && req.getCallingUserId() != null) {
            addVideoViewedByUserInfo(bundleInfo, req.getCallingUserId());
        }

        BaseInstalment baseInstalment = getBaseInstalment(InstalmentPurchaseEntity.BUNDLE, id);
        if (baseInstalment != null) {
            bundleInfo.setInstalmentDetails(baseInstalment.getInfo());
        }

        Set<String> trialIds = bundle.getTrialIds();
        if (ArrayUtils.isNotEmpty(trialIds)) {
            List<Trial> trials = trialDAO.getByIds(trialIds);
            List<TrialData> trialData =
                    trials
                            .stream()
                            .map(this::mapToTrialData)
                            .sorted(Comparator.comparing(TrialData::getTrialAllowedDays))
                            .collect(Collectors.toList());
            bundleInfo.setTrialData(trialData);
        }

        if(req.isIncludeSubscriptionPlan()){
            List<BaseSubscriptionPlanResp>  baseSubscriptionPlanRespList =   getBaseSubscriptionPlanByBundles(Arrays.asList( id));
            if( ArrayUtils.isNotEmpty( baseSubscriptionPlanRespList)){
                bundleInfo.setBaseSubscriptionPlan(baseSubscriptionPlanRespList);
            }
        }


        return bundleInfo;
    }

    private TrialData mapToTrialData(Trial trial) {
        return mapper.map(trial, TrialData.class);
    }

    public Bundle getBundle(String id) {
        return bundleDAO.getBundleById(id);
    }

    public void setBatchDetails(Bundle bundle)
    {
        Map<String, BatchDetails> batchDetail = bundleEntityDAO.getBatchDetails(Collections.singletonList(bundle.getId()));
        if (batchDetail.containsKey(bundle.getId())) {
            if (bundle.getPackages() == null || bundle.getPackages().isEmpty()) {
                List<AioPackage> packages = new ArrayList<>();
                packages.add(new AioPackage());
                bundle.setPackages(packages);
            }

            bundle.getPackages().get(0).setBatchDetails(batchDetail.get(bundle.getId()));
        }
    }

    public BaseInstalment getBaseInstalment(InstalmentPurchaseEntity instalmentPurchaseEntity, String purchaseEntityId)
            throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseInstalment?instalmentPurchaseEntity=" + instalmentPurchaseEntity
                + "&purchaseEntityId=" + purchaseEntityId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);

        BaseInstalment response = gson.fromJson(jsonString, BaseInstalment.class);
        logger.info("EXIT " + response);
        return response;
    }

    private void removeByAccessControl(BundleInfo bundleInfo, Long callingUserId, Role callingUserRole) {
        if (Role.ADMIN.equals(callingUserRole)) {
            return;
        }

        if (callingUserRole == null) { // public case
            // remove everything except for public
            removeByAccessControlPriceTypes(bundleInfo, Arrays.asList(ContentPriceType.PUBLIC));
            return;
        }

        BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getBundleEnrolment(callingUserId.toString(),
                bundleInfo.getId());
        if (bundleEnrolment != null && EntityStatus.ACTIVE.equals(bundleEnrolment.getStatus())) {
            // returning everything, paid user
            return;
        } else {
            // remove everything except for PUBLIC and FREE
            removeByAccessControlPriceTypes(bundleInfo, Arrays.asList(ContentPriceType.PUBLIC, ContentPriceType.FREE));
            return;
        }

    }

    public BundleInfo addEditBundle(AddEditBundleReq addEditBundleReq) throws VException {
        addEditBundleReq.setPackages(new ArrayList<>());
        Bundle reqBundle = mapper.map(addEditBundleReq, Bundle.class);

        String bundleId = addEditBundleReq.getId();
        Bundle existingBundle = bundleDAO.getBundleById(bundleId);
        if (!StringUtils.isEmpty(bundleId)) {
            logger.info("Saving bundle information for bundle id {}", bundleId);

            if (Objects.isNull(existingBundle)) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundle not found " + bundleId);
            }

            if(!StringUtils.isEmpty(addEditBundleReq.getOrgId())){
                List<String> batchIds =
                        Optional.ofNullable(bundleEntityDAO.getAllDistinctBatchesOfBundle(bundleId))
                                .map(Collection::stream)
                                .orElseGet(Stream::empty)
                                .filter(Objects::nonNull)
                                .collect(Collectors.toList());
                logger.info("All currently active batch ids are - {}",batchIds);
                List<String> orgIds=batchDAO.getAllDistinctOrgIdsFromBatchIds(batchIds);
                logger.info("All distinct orgIds received are - {}",orgIds);
                if(ArrayUtils.isNotEmpty(orgIds)){
                    if(orgIds.size()>0){
                        if(orgIds.size()>1){
                            throw new BadRequestException(ErrorCode.ORG_ID_MISMATCH_IN_PACKAGE,"There's a combination of orgIds in this AIO - "+orgIds);
                        }
                        if(Objects.isNull(orgIds.get(0)) || !orgIds.get(0).equals(addEditBundleReq.getOrgId())){
                            throw new BadRequestException(ErrorCode.ORG_ID_MISMATCH_IN_PACKAGE,"Org id mismatch in package and bundle");
                        }
                    }
                }else if(ArrayUtils.isNotEmpty(batchIds)){
                    throw new BadRequestException(ErrorCode.ORG_ID_MISMATCH_IN_PACKAGE,"No org id present for batch, so can't save batch");
                }
            }

            logger.info("Existing bundle entry {} ", existingBundle);

            removeRestrictedUpdates(reqBundle, existingBundle);
            if (!existingBundle.getState().equals(reqBundle.getState())) {
                reqBundle.getStateChangeTime().add(new BundleStateChangeTime(System.currentTimeMillis(), addEditBundleReq.getCallingUserId(), reqBundle.getState()));
            }
        }

        // Insert and remove all the required trial information
        Set<String> trialIds = trialManager.insertTrialData(addEditBundleReq.getTrialData());
        if (ArrayUtils.isNotEmpty(trialIds)) {
            reqBundle.setTrialIds(trialIds);
        }
        trialManager.markTrialIdDeleted(addEditBundleReq.getTrialIdsForRemoval());

        // Installment validation
        if (Objects.nonNull(addEditBundleReq.getInstalmentDetails())) {
            validateInstallment(addEditBundleReq);
        }

        // updating all the necessary stuffs from the request bundle
        updateTrackIds(reqBundle);
        updateWebinarReferenceIds(reqBundle);
        updateNoOfItems(reqBundle);

        bundleDAO.save(reqBundle, addEditBundleReq.getCallingUserId());
        if (ArrayUtils.isNotEmpty(trialIds)) {
            trialManager.updateBundleContextForTrials(trialIds, reqBundle.getId());
        }

        addBatchesInSubscription(addEditBundleReq, reqBundle);

        // If the installment details inside the request bundle is not empty then proceed
        if (Objects.nonNull(addEditBundleReq.getInstalmentDetails())) {
            BaseInstalment baseInstalment =
                    new BaseInstalment(InstalmentPurchaseEntity.BUNDLE, reqBundle.getId(), addEditBundleReq.getInstalmentDetails());
            logger.info("bundleManager updateBaseInstalment : " + baseInstalment);
            updateBaseInstalment(baseInstalment);
        }
        // No else statement, how to handle when there is no details of installments

        if (ArrayUtils.isNotEmpty( addEditBundleReq.getBaseSubscriptionPlan())) {
            addEditBundleReq.getBaseSubscriptionPlan().forEach(baseSubscriptionPlanReq -> {
                baseSubscriptionPlanReq.setPurchaseEntityId(reqBundle.getId());
                baseSubscriptionPlanReq.setPurchaseEntityType(BaseSubscriptionPurchaseEntity.BUNDLE);
            });
            updateBaseSubscription( addEditBundleReq.getBaseSubscriptionPlan());
        }


        BundleInfo bundleInfo = mapper.map(reqBundle, BundleInfo.class);
        bundleId=reqBundle.getId();

        if (Objects.nonNull(bundleId) &&
                ((Objects.nonNull(existingBundle) && Objects.nonNull(existingBundle.getUnLimitedDoubtsIncluded()) && !existingBundle.getUnLimitedDoubtsIncluded().equals(addEditBundleReq.getUnLimitedDoubtsIncluded()))
                        ||(Objects.nonNull(addEditBundleReq.getUnLimitedDoubtsIncluded()) && addEditBundleReq.getUnLimitedDoubtsIncluded()))) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("bundleId", bundleId);
            payload.put("unlimitedDoubts", addEditBundleReq.getUnLimitedDoubtsIncluded());
            payload.put("callingUserId", addEditBundleReq.getCallingUserId());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SET_UNLIMITED_DOUBTS_BUNDLE, payload);
            asyncTaskFactory.executeTask(params);
        }

        // redisDAO.del(getAioNameCacheKey(bundleInfo.getId()));
        awsSNSManager.removeCachedKeysCrossService(SNSTopic.DINERO_REDIS_OPS, SNSSubject.REMOVE_BUNDLE_NAME_KEYS,
                Collections.singletonList(getAioNameCacheKey(bundleInfo.getId())));

        redisDAO.del(getBundleKeyForRedis(bundleInfo.getId()));
        return bundleInfo;
    }

    private  String getAioNameCacheKey(String key){
        return "AIO_NAME_" + key;
    }

    private void addBatchesInSubscription(AddEditBundleReq req, Bundle bundle) {
        logger.info("req" + req);
        if (Objects.isNull(req.getId()) && Boolean.TRUE.equals(req.getIsSubscription()) && ArrayUtils.isNotEmpty(req.getGrade()) && ArrayUtils.isNotEmpty(req.getTarget()) && ArrayUtils.isNotEmpty(req.getSubject())) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("bundleId", bundle.getId());
            sqsManager.sendToSQS(SQSQueue.SUBSCRIPTION_PACKAGE_OPS, SQSMessageType.ADD_PACKAGES_IN_SUBSCRIPTION, new Gson().toJson(payload));
        }
    }

    public void setMainTagInfoMap(Bundle bundle, Long callingUserId) {
        for (AioPackage aPackage : bundle.getPackages()) {
            if (ArrayUtils.isEmpty(aPackage.getMainTags())) {
                setMainTagsForPackage(aPackage);
            }
        }
        bundleDAO.save(bundle, callingUserId);
    }

    private void setMainTagsForPackage(AioPackage aPackage) {
        String batchId = aPackage.getEntityId();
        Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(Arrays.asList(batchId), false);
        BatchBasicInfo batch = batchMap.get(batchId);
        Map<String, CourseBasicInfo> courseMap = enrollmentManager.getCourseMap(Arrays.asList(batch.getCourseId()), false);
        CourseBasicInfo course = courseMap.get(batch.getCourseId());
        List<String> subjects = new ArrayList<>();
        course.getBoardTeacherPairs().forEach((_req) -> {
            subjects.add(_req.getSubject());
        });
        Language language = batch.getLanguage();
        List<SubscriptionPackageType> subscriptionSubscriptionPackageTypes = Objects.nonNull( batch.getPackageTypes() ) ? batch.getPackageTypes() : new ArrayList<>();
        if(ArrayUtils.isEmpty(subscriptionSubscriptionPackageTypes)) {
            subscriptionSubscriptionPackageTypes.add( SubscriptionPackageType.DEFAULT );
        }
        aPackage.setLanguage( language );
        aPackage.setSubscriptionPackageTypes(subscriptionSubscriptionPackageTypes);
        aPackage.setRecordedVideo(batch.isRecordedVideo());
        aPackage.setSubjects(subjects);
        aPackage.setCourseId(batch.getCourseId());
        aPackage.setStartTime(batch.getStartTime());
        aPackage.setEndTime(batch.getEndTime());
        aPackage.setCourseTitle(course.getTitle());
        if (ArrayUtils.isNotEmpty(batch.getSearchTerms())) {
            aPackage.setSearchTerms(batch.getSearchTerms());
        }
        List<String> mainTags = new ArrayList<>();
        mainTags.add(aPackage.getCourseId());
        mainTags.add(aPackage.getGrade().toString());
        mainTags.addAll(aPackage.getSubjects());
        subscriptionSubscriptionPackageTypes.forEach(type -> {
            mainTags.add( type.toString() );
        });
        mainTags.add( language.toString() );
        mainTags.add(aPackage.getRecordedVideo() ? bundleDAO.AIOPACKAGE_TAGS_RECORDED : bundleDAO.AIOPACKAGE_TAGS_LIVE);
        if (ArrayUtils.isNotEmpty(batch.getSearchTerms())) {
            batch.getSearchTerms().forEach(tearm -> {
                mainTags.add(tearm.toString());
            });
        }
        aPackage.setMainTags(mainTags);
    }

    private Set<String> getCourseAutoEnrollMap(Bundle bundle, Map<String, Boolean> mp,
                                               Map<String, AioPackage> packageMap) {
        Set<String> otmBundleIds = new HashSet<>();
        List<AioPackage> courseDetails = bundle.getPackages();
        if (ArrayUtils.isNotEmpty(courseDetails)) {
            for (AioPackage courseDetail : courseDetails) {

                if (EnrollmentType.AUTO_ENROLL.equals(courseDetail.getEnrollmentType())) {
                    mp.put(courseDetail.getEntityId(), true);
                    packageMap.put(courseDetail.getEntityId(), courseDetail);
                } else {
                    mp.put(courseDetail.getEntityId(), false);
                    packageMap.put(courseDetail.getEntityId(), courseDetail);

                }
                logger.info("adding otmbundleOfBatchesId " + courseDetail.getEntityId());
                otmBundleIds.add(courseDetail.getEntityId());
            }

        }
        logger.info("getCourseAutoEnrollMap for bundle " + otmBundleIds);
        return otmBundleIds;
    }

    private void updateBaseInstalment(BaseInstalment baseInstalment) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/updateBaseInstalment";
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.POST,
                gson.toJson(baseInstalment));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of updateBaseInstalment " + jsonString);
    }

    private void updateBaseSubscription( List<BaseSubscriptionPlanReq> baseSubscriptionPlanReq) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/updateBaseSubscription";
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.POST,
                gson.toJson(baseSubscriptionPlanReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of updateBaseInstalment " + jsonString);
    }




    private void updateNoOfItems(Bundle reqBundle) {
        Integer noOfWebinars = 0;
        List<Track> webinarCategories = reqBundle.getWebinarCategories();
        for (Track track : webinarCategories) {
            if (track.getWebinars() != null) {
                noOfWebinars += track.getWebinars().size();
            }
        }
        reqBundle.setNoOfWebinars(noOfWebinars);

        Integer noOfVideos = 0;
        VideoContentData videoContentData = reqBundle.getVideos();
        if (videoContentData.getContents() != null) {
            noOfVideos += videoContentData.getContents().size();
        }
        if (videoContentData.getChildren() != null && videoContentData.getChildren().size() > 0 && videoContentData.getChildren().get(0) != null) {
            for (VideoContentData child : videoContentData.getChildren().get(0).getChildren()) {
                if (child.getContents() != null) {
                    noOfVideos += child.getContents().size();
                }
            }
        }
        reqBundle.setNoOfVideos(noOfVideos);

        Integer noOfTests = 0;
        TestContentData testContentData = reqBundle.getTests();
        if (testContentData.getContents() != null) {
            noOfTests += testContentData.getContents().size();
        }
        if (testContentData.getChildren() != null && testContentData.getChildren().size() > 0 && testContentData.getChildren().get(0) != null) {
            for (TestContentData child : testContentData.getChildren().get(0).getChildren()) {
                if (child.getContents() != null) {
                    noOfTests += child.getContents().size();
                }
            }
        }
        reqBundle.setNoOfTests(noOfTests);

    }

    public BundleInfo addEditBundleWebinar(AddEditBundleWebinarReq addEditBundleWebinarReq)
            throws BadRequestException, NotFoundException {
        addEditBundleWebinarReq.validate();

        // Fetch the bundle
        Bundle existingEntry = bundleDAO.getBundleById(addEditBundleWebinarReq.getBundleId());
        if (existingEntry == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Bundle not found for id : "
                    + addEditBundleWebinarReq.getBundleId() + " req:" + addEditBundleWebinarReq.toString());
        }

        // Check the track
        Track track = getTrackFromBundle(addEditBundleWebinarReq.getTrackId(), existingEntry);
        if (track == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Track not found for id : "
                    + addEditBundleWebinarReq.getTrackId() + " req:" + addEditBundleWebinarReq.toString());
        }

        Map<String, WebinarBundleContentInfo> existingWebinarReqMap = createReferenceIdWebinarMap(
                addEditBundleWebinarReq.getWebinarBundleContentInfos());

        // Update the existing entries first
        for (WebinarBundleContentInfo entry : track.getWebinars()) {
            if (StringUtils.isEmpty(entry.getContentId()) && entry.getStartTime() > System.currentTimeMillis()
                    && existingWebinarReqMap.containsKey(entry.getReferenceId())) {
                entry = existingWebinarReqMap.get(entry.getReferenceId());
            }
        }

        // Add new webinar from request. Identify based on reference id
        for (WebinarBundleContentInfo webinarBundleContentInfo : addEditBundleWebinarReq
                .getWebinarBundleContentInfos()) {
            if (StringUtils.isEmpty(webinarBundleContentInfo.getReferenceId())) {
                webinarBundleContentInfo.setReferenceId(ObjectId.get().toString());
                track.addWebinar(webinarBundleContentInfo);
            }
        }

        bundleDAO.save(existingEntry, addEditBundleWebinarReq.getCallingUserId());
        BundleInfo bundleInfo = mapper.map(existingEntry, BundleInfo.class);
        return bundleInfo;
    }

    public SessionPojoWithRefId addEditBundleSession(AddEditBundleSessionReq addEditBundleSessionReq)
            throws VException {

        // Validate request
        addEditBundleSessionReq.validate();

        // Fetch the bundle
        Bundle existingBundle = bundleDAO.getBundleById(addEditBundleSessionReq.getBundleId());
        if (existingBundle == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Bundle not found for id : "
                    + addEditBundleSessionReq.getBundleId() + " req:" + addEditBundleSessionReq.toString());
        }

        // Fetch the track
        Track track = getTrackFromBundle(addEditBundleSessionReq.getTrackId(), existingBundle);
        if (track == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Track not found for id : "
                    + addEditBundleSessionReq.getTrackId() + " req:" + addEditBundleSessionReq.toString());
        }

        // Identify the session if reference id exists
        Map<String, WebinarBundleContentInfo> existingWebinarReqMap = createReferenceIdWebinarMap(track.getWebinars());
        WebinarBundleContentInfo existingWebinar = null;
        WebinarBundleContentInfo reqWebinar = addEditBundleSessionReq.getWebinarBundleContentInfo();
        if (!StringUtils.isEmpty(reqWebinar.getReferenceId())) {
            if (!existingWebinarReqMap.containsKey(reqWebinar.getReferenceId())) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                        "No webinar entry found for the given reference id : " + reqWebinar.getReferenceId()
                                + " under track :" + addEditBundleSessionReq.getTrackId() + " req:"
                                + addEditBundleSessionReq.toString());
            }

            existingWebinar = existingWebinarReqMap.get(reqWebinar.getReferenceId());
            logger.info("pre update webinarBundleContentInfo:" + existingWebinar.toString());
        } else {
            // Create reference id as it does not exist
            reqWebinar.setReferenceId(ObjectId.get().toString());
        }

        // Note : Only single session update is supported as of now. Response
        // will only consider the first session
        OTFWebinarSessionReq otfWebinarSessionReq = new OTFWebinarSessionReq(addEditBundleSessionReq);
        List<SessionPojoWithRefId> sessionPojoWithRefIds = scheduleWebinarSessions(otfWebinarSessionReq);
        if (CollectionUtils.isEmpty(sessionPojoWithRefIds)) {
            throw new VException(ErrorCode.SCHEDULING_FAILED,
                    " Session schedule/update failed for req:" + addEditBundleSessionReq.toString());
        }

        // Update the Webinar content based on the response
        reqWebinar.updateSessionResponse(sessionPojoWithRefIds.get(0));
        if (existingWebinar != null) {
            List<WebinarBundleContentInfo> webinars = track.getWebinars();
            webinars.set(webinars.indexOf(existingWebinar), reqWebinar);
        } else {
            track.addWebinar(reqWebinar);
        }

        bundleDAO.save(existingBundle, addEditBundleSessionReq.getCallingUserId());
        BundleInfo bundleInfo = mapper.map(existingBundle, BundleInfo.class);
        logger.info("Updated bundle info:" + bundleInfo.toString());
        return sessionPojoWithRefIds.get(0);
    }

    public List<BundleInfo> getBundleInfos(GetBundlesReq req) {
        List<BundleInfo> response = new ArrayList<>();
        List<Bundle> bundles = bundleDAO.getBundles(req);

        if (CollectionUtils.isNotEmpty(bundles)) {
            for (Bundle bundle : bundles) {
//                logger.info("Bundles from getBundleInfos : " + bundle);
                BundleInfo bundleInfo = mapper.map(bundle, BundleInfo.class);
                removeByReturnContentTypes(bundleInfo, req.getReturnContentTypes());
                response.add(bundleInfo);
            }
        }
        return response;
    }

    public List<BundleInfo> getPublicBundleInfos(GetBundlesReq req) {
        List<BundleState> states = new ArrayList<>();
        states.add(BundleState.ACTIVE);
        req.setStates(states);
        return getBundleInfos(req);
    }

    public BundleEnrolment createEnrolment(BundleCreateEnrolmentReq req) throws VException {
        if (Objects.isNull(req.getState())) {
            req.setState(EnrollmentState.REGULAR);
        }

        if (Objects.isNull(req.getStatus())) {
            req.setStatus(EntityStatus.ACTIVE);
        }

        BundleEnrolment bundleEnrolment =
                bundleEnrolmentDAO.getNonEndedBundleEnrolment(req.getUserId(), req.getEntityId());
        BundleEnrolment newBundleEnrolment = new BundleEnrolment();
        Boolean newEnrollment =true;

        long currentTime = System.currentTimeMillis();
        Bundle bundle = bundleDAO.getBundleById(req.getEntityId());
        if (Objects.nonNull(bundleEnrolment)  &&   (bundleEnrolment.getState().equals(EnrollmentState.TRIAL) || bundleEnrolment.getState().equals(EnrollmentState.FREE_PASS) ||   req.getIsSubscription() == null  || !req.getIsSubscription() )) {
             newEnrollment =false;
            // Case where already enrollment exists(can't be trial at least)
            logger.info("Bundle enrollment already exists.");
            if (bundleEnrolment.getState().equals(req.getState()) && bundleEnrolment.getStatus().equals(req.getStatus())
                    && EntityStatus.ACTIVE.equals(req.getStatus())) {
                throw new BadRequestException(ErrorCode.ALREADY_ENROLLED, "User already enrolled");
            }
            if (EnrollmentState.REGULAR.equals(bundleEnrolment.getState())
                    && EnrollmentState.TRIAL.equals(req.getState())) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Can't change from Regular to Trial");
            }


            if (!bundleEnrolment.getState().equals(req.getState())) {
                EnrollmentStateChangeTime enrollmentStateChangeTime = new EnrollmentStateChangeTime();
                enrollmentStateChangeTime.setChangeTime(currentTime);
                enrollmentStateChangeTime.setChangedBy(req.getCallingUserId());
                enrollmentStateChangeTime.setNewState(req.getState());
                enrollmentStateChangeTime.setPreviousState(bundleEnrolment.getState());
                if (ArrayUtils.isEmpty(bundleEnrolment.getStateChangeTime())) {
                    bundleEnrolment.setStateChangeTime(Arrays.asList(enrollmentStateChangeTime));
                } else {
                    bundleEnrolment.getStateChangeTime().add(enrollmentStateChangeTime);
                }

                // changing all enrollments sectionState for batches within bundle to REGULAR
                UpdateBundleBatchEnrollmentsReq updateReq = new UpdateBundleBatchEnrollmentsReq();
                updateReq.setBundleEnrollmentId(bundleEnrolment.getId());
                updateReq.setBundleId(req.getEntityId());
                updateReq.setUserId(req.getUserId());
                updateReq.setEnrollmentState(req.getState());
                sqsManager.sendToSQS(SQSQueue.SECTION_QUEUE,SQSMessageType.UPDATE_BUNDLE_BATCH_ENROLLMENTS,gson.toJson(updateReq));
            }


            if ( EntityStatus.INACTIVE.equals(bundleEnrolment.getStatus())  &&   EntityStatus.ACTIVE.equals(req.getStatus()) ) {
                StatusChangeTime statusChangeTime = new StatusChangeTime();
                statusChangeTime.setChangeTime(System.currentTimeMillis());
                statusChangeTime.setChangedBy(req.getCallingUserId());
                statusChangeTime.setNewStatus(req.getStatus());
                statusChangeTime.setPreviousStatus(bundleEnrolment.getStatus());
                bundleEnrolment.getStatusChangeTime().add( statusChangeTime);

                updateBatchEnrollments(bundleEnrolment,req.getStatus());

            }

            bundleEnrolment.setStatus(req.getStatus());
            bundleEnrolment.setState(req.getState());
            if(Objects.nonNull( req.getValidMonths()) && req.getValidMonths() != 0){
                Long validTill = currentTime +  (DateTimeUtils.MILLIS_PER_DAY  * 30L  *  req.getValidMonths());
                bundleEnrolment.setIsSubscriptionPlan(true);
                bundleEnrolment.setValidTill(validTill);
                if (Objects.nonNull(bundle.getUnLimitedDoubtsIncluded()) && bundle.getUnLimitedDoubtsIncluded()) {
                    bundleEnrolment.setDoubtsValidTill(validTill);
                }

            }
            bundleEnrolmentDAO.save(bundleEnrolment, null);
            // TODO enable for gamification
            // bundleEnrolmentManager.processBundleEnrolment(bundleEnrolment);
        } else {// Case for fresh enrollment
            if(Objects.nonNull(bundleEnrolment)   &&  bundleEnrolment.getStatus().equals(EntityStatus.ACTIVE)  && !req.isSubscriptionRenew())    {

                Long expiryDate = subscriptionUpdateDAO.getSubscriptionsExpiryDate(bundleEnrolment.getId());

                SubscriptionUpdate subscriptionUpdate = new SubscriptionUpdate(bundleEnrolment.getBundleId(),Long.parseLong( req.getUserId()),expiryDate == 0L ?  bundleEnrolment.getValidTill():expiryDate ,req.getValidMonths(),false,bundleEnrolment.getId(),req.getOrderId());
                subscriptionUpdateDAO.save(subscriptionUpdate, req.getCallingUserId());
                newBundleEnrolment.setSubscriptionUpdateId(subscriptionUpdate.getId());
                return newBundleEnrolment;

            }
            else
            {
                if(Objects.nonNull(bundleEnrolment) ) {
                bundleEnrolment.setStatusChangeTime(getBundleEnrolmentStatusChangeTimes(bundleEnrolment.getStatusChangeTime(),
                        bundleEnrolment.getStatus(), EntityStatus.ENDED, req.getCallingUserId()));

                bundleEnrolment.setStatus(EntityStatus.ENDED);
                bundleEnrolment.setEndReason("SUBSCRIPTION_RENEW");
                bundleEnrolment.setEndedBy(req.getCallingUserId());
                bundleEnrolmentDAO.save(bundleEnrolment, req.getCallingUserId());

            }
                    logger.info("Going for fresh bundle enrollment.");
                newBundleEnrolment = new BundleEnrolment(req.getEntityId(), req.getUserId(), req.getStatus(), req.getState());
                    if(Objects.nonNull( req.getValidMonths()) && req.getValidMonths() != 0){
                        Long validTill = currentTime +  (DateTimeUtils.MILLIS_PER_DAY  * 30L  *  req.getValidMonths());
                        newBundleEnrolment.setValidTill(validTill);
                        if (Objects.nonNull(bundle.getUnLimitedDoubtsIncluded()) && bundle.getUnLimitedDoubtsIncluded()) {
                            newBundleEnrolment.setDoubtsValidTill(validTill);
                        }
                        newBundleEnrolment.setIsSubscriptionPlan(true);
                    }
                    else {

                        if (Objects.nonNull(bundle.getUnLimitedDoubtsIncluded()) && bundle.getUnLimitedDoubtsIncluded()) {
                            if (bundle.getValidDays() > 0) {
                                newBundleEnrolment.setDoubtsValidTill(currentTime + (bundle.getValidDays() * DateTimeUtils.MILLIS_PER_DAY));
                            } else {
                                newBundleEnrolment.setDoubtsValidTill(bundle.getValidTill());
                            }

                        }
                        if (Objects.nonNull(bundle.getValidTill())) {
                            newBundleEnrolment.setValidTill(bundle.getValidTill());
                        } else {
                            newBundleEnrolment.setValidTill(currentTime + (bundle.getValidDays() * DateTimeUtils.MILLIS_PER_DAY));
                        }
                    }
                    if (Objects.nonNull(bundle.getUnLimitedDoubtsIncluded())) {
                        newBundleEnrolment.setUnlimitedDoubts(bundle.getUnLimitedDoubtsIncluded());
                    }
                    if (EnrollmentState.TRIAL.equals(req.getState())) {
                        logger.info("Saving trial enrollment");
                        Trial trial = trialDAO.getById(req.getTrialId());
                        logger.info("Getting trial allowed days {}", trial.getTrialAllowedDays());
                        if (Objects.nonNull(trial)) {
                            Long trailAllowedDays = trial.getTrialAllowedDays();
                            int gracePeriod = trial.getGracePeriod();
                            if (Objects.nonNull(trailAllowedDays) && trailAllowedDays != 0) {
                                logger.info("Setting up trial allowed days.");
                                newBundleEnrolment.setFreePassvalidTill(currentTime + DateTimeUtils.MILLIS_PER_DAY * (trailAllowedDays + gracePeriod));
                                newBundleEnrolment.setTrialId(trial.getId());
                            }
                        }
                    }
                    else{

                        if(bundle.getIsSubscription() != null && bundle.getIsSubscription()) {
                            if(Objects.nonNull(newBundleEnrolment)){
                            }else{
                                sqsManager.sendToSQS(SQSQueue.BUNDLE_OPS, SQSMessageType.CHECK_FOR_EXISTING_TRIAL, gson.toJson(newBundleEnrolment));
                            }



                        }
                    }
                    if (null != bundle.getSearchTerms() && bundle.getSearchTerms().size() > 0) {
                        newBundleEnrolment.setSearchTerms(bundle.getSearchTerms());
                    }
                    bundleEnrolmentDAO.save(newBundleEnrolment, null);

                    if(bundleEnrolment != null) {
                        migrateBatchEnrollments(bundleEnrolment, newBundleEnrolment);
                    }
                }


            }



        if(newEnrollment  && !EnrollmentState.TRIAL.equals(req.getState()) && bundle.getIsSubscription() != null && bundle.getIsSubscription()){
            sqsManager.sendToSQS(SQSQueue.BUNDLE_OPS, SQSMessageType.CHECK_FOR_EXISTING_TRIAL, gson.toJson(newBundleEnrolment),"ENROLL" + newBundleEnrolment.getId());
        }
//        todo enable for gamification
//        if (Boolean.TRUE.equals(newEnrollment)) {
//            bundleEnrolmentManager.processBundleEnrolment(newBundleEnrolment);
//        }


        if(newBundleEnrolment !=null  && newBundleEnrolment.getId()!=null) {
            return newBundleEnrolment ;
        }
        else{
            return bundleEnrolment;
        }
    }

    public void migrateForExistingTrial(BundleEnrolment newBundleEnrolment) throws VException {
        String userId = newBundleEnrolment.getUserId();

        BundleEnrolment oldBundleEnrollment =  bundleEnrolmentDAO.getTrialEnrollmentForSameGrade(newBundleEnrolment);
if(oldBundleEnrollment != null) {
    migrateBatchEnrollments(oldBundleEnrollment, newBundleEnrolment);

    oldBundleEnrollment.setStatusChangeTime(getBundleEnrolmentStatusChangeTimes(oldBundleEnrollment.getStatusChangeTime(),
            oldBundleEnrollment.getStatus(), EntityStatus.ENDED, Long.parseLong(newBundleEnrolment.getUserId())));

    oldBundleEnrollment.setStatus(EntityStatus.ENDED);
    oldBundleEnrollment.setEndedBy(Long.parseLong(newBundleEnrolment.getUserId()));
    oldBundleEnrollment.setEndReason("regulare_updte_to " + newBundleEnrolment.getId());
    bundleEnrolmentDAO.save(oldBundleEnrollment, Long.parseLong(newBundleEnrolment.getUserId()));
}

    }

    @Deprecated
    public void updateWebinarSessionAttendence(List<BundleEnrolment> bundleEnrolments) throws VException {
        if (!CollectionUtils.isEmpty(bundleEnrolments)) {
            for (BundleEnrolment bundleEnrolment : bundleEnrolments) {
                try {
                    updateWebinarSessionAttendence(bundleEnrolment.getBundleId(), bundleEnrolment.getUserId(),
                            bundleEnrolment.getStatus());
                    logger.info("Session enrollment done for bundle enrollment :" + bundleEnrolment.toString());
                } catch (Exception ex) {
                    logger.error("Error occured enroling the student to webinar sessions :" + ex.getMessage()
                            + " enrolment:" + bundleEnrolment.toString());
                }
            }
        }
    }

    @Deprecated
    public void updateWebinarSessionAttendence(BundleEnrolment bundleEnrolment) throws VException {
        updateWebinarSessionAttendence(bundleEnrolment.getBundleId(), bundleEnrolment.getUserId(),
                bundleEnrolment.getStatus());
    }

    @Deprecated
    public void updateWebinarSessionAttendence(String bundleId, String userId, EntityStatus entityStatus)
            throws VException {
        UpdateWebinarSessionsAttendenceReq req = new UpdateWebinarSessionsAttendenceReq();
        req.setBundleId(bundleId);
        req.setStudentId(userId);
        req.setEntityStatus(entityStatus);
        req.setAttendeeConfirmed(Boolean.FALSE);
        updateWebinarSessionAttendence(req);
    }

    public void updateWebinarSessionAttendence(UpdateWebinarSessionsAttendenceReq req) throws VException {
        logger.info("Request:" + req.toString());
        String url = SCHEDULING_ENDPOINT + "session/webinar/attendee/update";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);
    }

    public List<Bundle> getBundles(List<String> bundleIds) {
        return bundleDAO.getBundlesByIds(bundleIds);
    }

    public List<SessionPojoWithRefId> scheduleWebinarSessions(OTFWebinarSessionReq req) throws VException {
        // Fetch the enrollments
        Set<String> enrolledStudents = getEnrolledUserList(req.getBundleId());
        req.setEnrolledStudents(enrolledStudents);
        String url = SCHEDULING_ENDPOINT + "session/webinar/updateBulk";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response : " + jsonString);

        Type otfSessionListType = new TypeToken<List<SessionPojoWithRefId>>() {
        }. getType();

        List<SessionPojoWithRefId> sessions = gson.fromJson(jsonString, otfSessionListType);
        logger.info("Response count :" + sessions == null ? 0 : sessions.size());
        return sessions;
    }

    private static void updateTrackIds(Bundle bundle) {
        List<Track> tracks = bundle.getWebinarCategories();
        if (!CollectionUtils.isEmpty(tracks)) {
            for (Track track : tracks) {
                if (StringUtils.isEmpty(track.getTrackId())) {
                    track.setTrackId(ObjectId.get().toString());
                }
            }
        }
    }

    private static void updateWebinarReferenceIds(Bundle bundle) {
        List<Track> tracks = bundle.getWebinarCategories();
        if (!CollectionUtils.isEmpty(tracks)) {
            for (Track track : tracks) {
                updateWebinarReferenceIds(track.getWebinars());
            }
        }
    }

    private static void updateWebinarReferenceIds(List<WebinarBundleContentInfo> webinars) {
        if (!CollectionUtils.isEmpty(webinars)) {
            for (WebinarBundleContentInfo webinar : webinars) {
                if (StringUtils.isEmpty(webinar.getReferenceId())) {
                    webinar.setReferenceId(ObjectId.get().toString());
                }
            }
        }
    }

    private static Track getTrackFromBundle(String trackId, Bundle bundle) {
        if (bundle != null && !StringUtils.isEmpty(trackId)) {
            List<Track> tracks = bundle.getWebinarCategories();
            if (!CollectionUtils.isEmpty(tracks)) {
                for (Track track : tracks) {
                    if (trackId.equals(track.getTrackId())) {
                        return track;
                    }
                }
            }
        }
        return null;
    }

    private static void removeByReturnContentTypes(BundleInfo bundleInfo, List<BundleContentType> returnTypes) {
        if (returnTypes == null) {
            returnTypes = new ArrayList<>();
        }

        if (!returnTypes.contains(BundleContentType.TEST)) {
            bundleInfo.setTests(null);
        }
        if (!returnTypes.contains(BundleContentType.VIDEO)) {
            bundleInfo.setVideos(null);
        }
        if (!returnTypes.contains(BundleContentType.WEBINAR)) {
            bundleInfo.setWebinarCategories(null);
        }
    }

    private static void removeByContentPriceTypes(BundleInfo bundleInfo, List<ContentPriceType> contentPriceTypes,
                                                  Boolean featured) {
        if (!CollectionUtils.isEmpty(contentPriceTypes)) {
            // Update tracks
            if (!CollectionUtils.isEmpty(bundleInfo.getWebinarCategories())) {
                for (Track track : bundleInfo.getWebinarCategories()) {
                    List<WebinarBundleContentInfo> webinarBundleContentInfos = track.getWebinars();
                    if (!CollectionUtils.isEmpty(webinarBundleContentInfos)) {
                        Iterator<WebinarBundleContentInfo> webinarIterator = webinarBundleContentInfos.iterator();
                        while (webinarIterator.hasNext()) {
                            WebinarBundleContentInfo webinar = webinarIterator.next();
                            if (webinar.getType() == null || !contentPriceTypes.contains(webinar.getType())
                                    || (featured != null && webinar.isFeatured() != featured.booleanValue())) {
                                webinarIterator.remove();
                            }
                        }
                    }
                }
            }

            // Update tests
            filterTestsByContentPriceType(bundleInfo.getTests(), contentPriceTypes, featured);

            // Update videos
            filterVideosByContentPriceType(bundleInfo.getVideos(), contentPriceTypes, featured);
        }
    }

    private static void filterVideosByContentPriceType(VideoContentData videoContentData,
                                                       List<ContentPriceType> contentPriceTypes, Boolean featured) {
        if (!CollectionUtils.isEmpty(contentPriceTypes) && videoContentData != null) {
            List<VideoBundleContentInfo> videos = videoContentData.getContents();
            if (!CollectionUtils.isEmpty(videos)) {
                Iterator<VideoBundleContentInfo> videoIterator = videos.iterator();
                while (videoIterator.hasNext()) {
                    VideoBundleContentInfo video = videoIterator.next();
                    if (video == null || !contentPriceTypes.contains(video.getType())
                            || (featured != null && video.isFeatured() != featured.booleanValue())) {
                        videoIterator.remove();
                    }
                }
            }

            if (!CollectionUtils.isEmpty(videoContentData.getChildren())) {
                for (VideoContentData entry : videoContentData.getChildren()) {
                    filterVideosByContentPriceType(entry, contentPriceTypes, featured);
                }
            }
        }
    }

    private static void filterTestsByContentPriceType(TestContentData testContentData,
                                                      List<ContentPriceType> contentPriceTypes, Boolean featured) {
        if (!CollectionUtils.isEmpty(contentPriceTypes) && testContentData != null) {
            List<TestBundleContentInfo> tests = testContentData.getContents();
            if (!CollectionUtils.isEmpty(tests)) {
                Iterator<TestBundleContentInfo> testIterator = tests.iterator();
                while (testIterator.hasNext()) {
                    TestBundleContentInfo test = testIterator.next();
                    if (test == null || !contentPriceTypes.contains(test.getType())
                            || (featured != null && test.isFeatured() != featured.booleanValue())) {
                        testIterator.remove();
                    }
                }
            }

            if (!CollectionUtils.isEmpty(testContentData.getChildren())) {
                for (TestContentData entry : testContentData.getChildren()) {
                    filterTestsByContentPriceType(entry, contentPriceTypes, featured);
                }
            }
        }
    }

    private void removeRestrictedUpdates(Bundle reqBundle, Bundle existingEntry) {
        if (existingEntry != null) {
            Map<String, WebinarBundleContentInfo> existingWebinarMap = createContentIdWebinarMap(existingEntry);
            List<Track> reqTracks = reqBundle.getWebinarCategories();
            if (!CollectionUtils.isEmpty(reqTracks)) {
                for (Track track : reqTracks) {
                    List<WebinarBundleContentInfo> webinars = track.getWebinars();
                    if (!CollectionUtils.isEmpty(webinars)) {
                        for (WebinarBundleContentInfo webinarBundleContentInfo : webinars) {
                            if (!StringUtils.isEmpty(webinarBundleContentInfo.getContentId())
                                    && existingWebinarMap.containsKey(webinarBundleContentInfo.getContentId())) {
                                webinarBundleContentInfo = existingWebinarMap
                                        .get(webinarBundleContentInfo.getContentId());
                            }
                        }
                    }
                }
            }
        }

        reqBundle.setCreatedBy(existingEntry.getCreatedBy());
        reqBundle.setCreationTime(existingEntry.getCreationTime());
        reqBundle.setIsSubscription(existingEntry.getIsSubscription());
        reqBundle.setIsVassist(existingEntry.getIsVassist());
        if (Objects.nonNull(existingEntry.getWindowRight())) {
            reqBundle.setWindowRight(existingEntry.getWindowRight());
        }
        if (Boolean.TRUE.equals(existingEntry.getIsSubscription())) {
            reqBundle.setGrade(existingEntry.getGrade());
            reqBundle.setTarget(existingEntry.getTarget());
            reqBundle.setSubject(existingEntry.getSubject());
        }
    }

    private Map<String, WebinarBundleContentInfo> createContentIdWebinarMap(Bundle bundle) {
        Map<String, WebinarBundleContentInfo> contentIdWebinarMap = new HashMap<>();
        if (bundle != null) {
            List<Track> tracks = bundle.getWebinarCategories();
            if (!CollectionUtils.isEmpty(tracks)) {
                for (Track track : tracks) {
                    List<WebinarBundleContentInfo> webinars = track.getWebinars();
                    if (!CollectionUtils.isEmpty(webinars)) {
                        for (WebinarBundleContentInfo webinarBundleContentInfo : webinars) {
                            if (!StringUtils.isEmpty(webinarBundleContentInfo.getContentId())
                                    && ContentType.WEBINAR.equals(webinarBundleContentInfo.getContentType())) {
                                contentIdWebinarMap.put(webinarBundleContentInfo.getContentId(),
                                        webinarBundleContentInfo);
                            }
                        }
                    }
                }
            }
        }

        return contentIdWebinarMap;
    }

    private Map<String, WebinarBundleContentInfo> createReferenceIdWebinarMap(List<WebinarBundleContentInfo> webinars) {
        Map<String, WebinarBundleContentInfo> referenceIdWebinarMap = new HashMap<>();
        if (!CollectionUtils.isEmpty(webinars)) {
            for (WebinarBundleContentInfo webinarBundleContentInfo : webinars) {
                if (!StringUtils.isEmpty(webinarBundleContentInfo.getReferenceId())
                        && ContentType.WEBINAR.equals(webinarBundleContentInfo.getContentType())) {
                    referenceIdWebinarMap.put(webinarBundleContentInfo.getReferenceId(), webinarBundleContentInfo);
                }
            }
        }
        return referenceIdWebinarMap;
    }

    public List<BundleEnrollmentResp> getEnrolledBundles(String userId) {
        List<BundleEnrolment> enrolments = bundleEnrolmentDAO.getUsersActiveAndInactiveEnrollment(userId);
        List<BundleEnrollmentResp> response = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(enrolments)) {
            List<String> bundleIds = new ArrayList<>();
            Map<String, BundleEnrolment> bundleIdEnrollment = new HashMap<>();
            for (BundleEnrolment bundleEnrolment : enrolments) {
                bundleIdEnrollment.put(bundleEnrolment.getBundleId(), bundleEnrolment);
                bundleIds.add(bundleEnrolment.getBundleId());
            }
            List<Bundle> bundles = bundleDAO.getBundlesByIds(bundleIds);
            if (CollectionUtils.isNotEmpty(bundles)) {
                for (Bundle bundle : bundles) {
                    BundleEnrollmentResp bundleInfo = mapper.map(bundle, BundleEnrollmentResp.class);
                    bundleInfo.setEnrollmentId(bundleIdEnrollment.get(bundleInfo.getId()).getId());
                    bundleInfo.setEnrollmentState(bundleIdEnrollment.get(bundleInfo.getId()).getState());
                    bundleInfo.setEnrollmentStatus(bundleIdEnrollment.get(bundleInfo.getId()).getStatus());
                    bundleInfo.setEntityExists(bundleEntityDAO.getIfEntitiesExistForBundle(bundle.getId()));
                    if (bundleInfo.getVideos() != null) {
                        for (VideoContentData video : bundleInfo.getVideos().getChildren()) {
                            video.setChildren(null);
                        }
                    }
                    if (bundleInfo.getTests() != null) {
                        for (TestContentData testContentData : bundleInfo.getTests().getChildren()) {
                            testContentData.setChildren(null);
                        }
                    }
                    response.add(bundleInfo);
                }
            }
        }
        return response;
    }

    public List<BundleEnrollmentResp> getNonSubscriptionBundlesPackages(String userId) {
        List<BundleEnrolment> enrolments = bundleEnrolmentDAO.getUsersActiveAndInactiveEnrollment(userId);
        List<BundleEnrollmentResp> response = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(enrolments)) {
            List<String> bundleIds = new ArrayList<>();
            for (BundleEnrolment bundleEnrolment : enrolments) {
                bundleIds.add(bundleEnrolment.getBundleId());
            }
            List<Bundle> bundles = bundleDAO.getNonSubscriptionBundlesPackages(bundleIds);
            if (CollectionUtils.isNotEmpty(bundles)) {
                for (Bundle bundle : bundles) {
                    BundleEnrollmentResp bundleInfo = mapper.map(bundle, BundleEnrollmentResp.class);
                    response.add(bundleInfo);
                }
            }
        }
        return response;
    }

    public List<BundleEnrolmentInfo> getEnrolments(GetBundleEnrolmentsReq req) {
        List<BundleEnrolment> enrolments = bundleEnrolmentDAO.getBundleEnrolmentsFromReq(req);
        List<BundleEnrolmentInfo> results = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(enrolments)) {
            for (BundleEnrolment bundleEnrolment : enrolments) {
                BundleEnrolmentInfo bundleEnrolmentInfo = mapper.map(bundleEnrolment, BundleEnrolmentInfo.class);
                results.add(bundleEnrolmentInfo);
            }
        }
        return results;
    }

    public List<BundleEnrolmentInfo> getActiveBundleEnrolmentsForUsers(GetBundleEnrolmentsReq req) throws VException {
        List<BundleEnrolment> enrolments = bundleEnrolmentDAO.getUsersActiveEnrolments(req);
        List<BundleEnrolmentInfo> results = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(enrolments)) {

            if (enrolments.size() > 1000)
                logger.error("WARNINNG: getActiveBundleEnrolmentsForUsers - Enrollment response size exceeded the limit {}", enrolments.size());

            for (BundleEnrolment bundleEnrolment : enrolments) {
                BundleEnrolmentInfo bundleEnrolmentInfo = mapper.map(bundleEnrolment, BundleEnrolmentInfo.class);
                results.add(bundleEnrolmentInfo);
            }
        }
        return results;
    }

    public Set<String> getEnrolledUserList(String bundleId) {
        GetBundleEnrolmentsReq enrollmentReq = new GetBundleEnrolmentsReq();
        enrollmentReq.setBundleId(bundleId);
        enrollmentReq.setStatus(EntityStatus.ACTIVE);
        List<BundleEnrolmentInfo> enrollments = getEnrolments(enrollmentReq);
        Set<String> enrolledStudents = new HashSet<>();
        if (!CollectionUtils.isEmpty(enrollments)) {
            for (BundleEnrolmentInfo bundleEnrolmentInfo : enrollments) {
                enrolledStudents.add(bundleEnrolmentInfo.getUserId());
            }
        }

        return enrolledStudents;
    }

    public Boolean hasEnrollment(CheckBundleEnrollmentReq checkBundleEnrollmentReq) throws VException {
        List<BundleEnrolmentInfo> bundleEnrolmentInfos = checkEnrolment(checkBundleEnrollmentReq);
        if (ArrayUtils.isNotEmpty(bundleEnrolmentInfos) &&
                ((EnrollmentState.FREE_PASS.equals(bundleEnrolmentInfos.get(0).getState())
                        || EnrollmentState.TRIAL.equals(bundleEnrolmentInfos.get(0).getState()))
                        && EnrollmentState.REGULAR.equals(checkBundleEnrollmentReq.getNewState()))) {
            return false;
        }
        if (checkIfAlreadyEnrolledInBatchOrCourseOfBundle(checkBundleEnrollmentReq)) {
            return true;
        }
        return ArrayUtils.isNotEmpty(bundleEnrolmentInfos);
    }

    public List<BundleEnrolmentInfo> checkEnrolment(CheckBundleEnrollmentReq req) throws VException {
        //getting user id from session utils
        if(sessionUtils.getCurrentSessionData(true).getRole().equals(Role.STUDENT)){
            String userId = String.valueOf(sessionUtils.getCallingUserId());
            req.setUserId(userId);
        }


        List<BundleEnrolment> enrolments = bundleEnrolmentDAO.getBundleEnrolmentsFromReq(req);
        List<BundleEnrolmentInfo> results = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(enrolments)) {
            if (enrolments.size() > 1) {
                logger.error("Multiple enrolments found for same user id and bundle id :" + req.toString());
            }
            BundleEnrolmentInfo bundleEnrolmentInfo = mapper.map(enrolments.get(0), BundleEnrolmentInfo.class);
            results.add(bundleEnrolmentInfo);
        }

        return results;
    }

    public boolean checkIfAlreadyEnrolledInBatchOrCourseOfBundle(CheckBundleEnrollmentReq req) throws BadRequestException {

        // Get bundle and user id
        String bundleId = req.getBundleId();
        String userId = req.getUserId();

        Bundle bundle = bundleDAO.getBundleById(bundleId);

        if(bundle == null){
            return true;
        }

        // Validations in case of trial enrollment(User can avail 1 trial per AIO per grade)
        // already done during `/hasEnrollment` API call inside `/buyItems`
        if (EnrollmentState.TRIAL.equals(req.getNewState())) {

            // Get the grade and validate the same
            Integer grade = Optional.ofNullable(bundle.getGrade())
                    .map(Collection::stream)
                    .orElseGet(Stream::empty)
                    .findFirst()
                    .orElseGet(null);
            if (Objects.isNull(grade)) {
                logger.error("Impossible to have grade as null for the bundle id {}", bundleId);
            }

            // Get all the enrolled bundles taken as trail as one or some point of time
            List<String> bundleIds = bundleEnrolmentDAO.getBundleIdsOfAllKindOfTrialEnrollment(userId);

            // Get all the grades of bundles
            Set<Integer> grades = bundleDAO.getGradesOfAllTheBundleIds(bundleIds);

            // See if there's already an enrollment
            if (grades.contains(grade)) {
                return true;
            }

        }

        if (bundle.getIsSubscription()) {
            return false;
        }

        int start=0,size=1000;
        while(true) {
            List<String> batchIds = new ArrayList<>();
            List<BundleEntity> entities =
                    bundleEntityDAO.getBundleEntitiesByBundleId(bundleId, start, size,
                            Arrays.asList(BundleEntity.Constants.ENTITY_ID,BundleEntity.Constants.PACKAGE_TYPE));
            if(ArrayUtils.isEmpty(entities)){
                break;
            }

            batchIds.addAll(entities.stream().filter(this::isOTFEntity).map(BundleEntity::getEntityId).collect(Collectors.toList()));

            batchIds.addAll(
                    entities.stream()
                            .filter(this::isOTMBundleEntity)
                            .map(BundleEntity::getEntityId)
                            .map(otfBundleDAO::getOTFBundleById)
                            .map(OTMBundle::getEntities)
                            .flatMap(Collection::stream)
                            .filter(this::isOTFEntityOfOTMBundle)
                            .map(OTMBundleEntityInfo::getEntityId)
                            .collect(Collectors.toList())
            );
            logger.info("checkIfAlreadyEnrolledInBatchOrCourseOfBundle - batchIds - {}",batchIds);
            List<String> courseIds=batchDAO.getDistinctCourseIdsFromBatchIds(batchIds);
            logger.info("checkIfAlreadyEnrolledInBatchOrCourseOfBundle - courseIds - {}",courseIds);
            long count = enrollmentDAO.getActiveAndInactiveEnrollmentCountByUserIdAndCourseIds(userId,courseIds);
            if(count>0){
                return true;
            }
            start+=size;
        }

        return false;
    }

    private boolean isOTFEntityOfOTMBundle(OTMBundleEntityInfo otmBundleEntityInfo) {
        return EntityType.OTF.equals(otmBundleEntityInfo.getEntityType());
    }

    private boolean isOTFEntity(BundleEntity bundleEntity) {
        return PackageType.OTF.equals(bundleEntity.getPackageType());
    }

    private boolean isOTMBundleEntity(BundleEntity bundleEntity) {
        return PackageType.OTM_BUNDLE.equals(bundleEntity.getPackageType());
    }
    // Added as a part of LE-1324
    public boolean checkIfAlreadyEnrolledInBatchOrCourseOfBundle(String userId, String bundleId) {

        // Get all the batch ids from the bundle(AIO)
        List<String> batchIds = new ArrayList<String>();
        Bundle bundle = bundleDAO.getBundleById(bundleId);
        List<AioPackage> aioPackages = bundle.getPackages();

        for (AioPackage aioPackage : aioPackages) {
            if (PackageType.OTF.equals(aioPackage.getPackageType())) {
                batchIds.add(aioPackage.getEntityId());
            } else if (PackageType.OTM_BUNDLE.equals(aioPackage.getPackageType())) {
                OTMBundle otmBundle = otfBundleDAO.getOTFBundleById(aioPackage.getEntityId());
                List<OTMBundleEntityInfo> entities = otmBundle.getEntities();
                for (OTMBundleEntityInfo entity : entities) {
                    if (EntityType.OTF.equals(entity.getEntityType())) {
                        batchIds.add(entity.getEntityId());
                    }
                }
            }
        }

        // Get all the batches and ensure that the course conflict doesn't occur
        List<Batch> batches = batchDAO.getBatchByIds(batchIds);
        if (batches != null) {
            List<Enrollment> currentEnrollments = enrollmentDAO.getEnrollmentsForUserId(userId);
            List<String> courseIds = new ArrayList<String>();
            if (currentEnrollments != null) {
                for (Enrollment enrollment : currentEnrollments) {
                    courseIds.add(enrollment.getCourseId());
                }
                for (Batch batch : batches) {
                    if (courseIds.contains(batch.getCourseId())) {
                        logger.error("The student have enrollment in the course having course id " + batch.getCourseId());
                        return true;
                    } else {
                        courseIds.add(batch.getCourseId());
                    }
                }
            }
        }

        return false;
    }

    private static void removeByAccessControlPriceTypes(BundleInfo bundleInfo,
                                                        List<ContentPriceType> contentPriceTypes) {
        if (!CollectionUtils.isEmpty(contentPriceTypes)) {

            if (!CollectionUtils.isEmpty(bundleInfo.getWebinarCategories())) {
                for (Track track : bundleInfo.getWebinarCategories()) {
                    List<WebinarBundleContentInfo> webinarBundleContentInfos = track.getWebinars();
                    if (!CollectionUtils.isEmpty(webinarBundleContentInfos)) {
                        for (WebinarBundleContentInfo webinar : webinarBundleContentInfos) {
                            if (webinar != null && !contentPriceTypes.contains(webinar.getType())) {
                                webinar.setContentId(null);
                                webinar.setUrl(null);
                            }
                        }
                    }
                }
            }

            // Update tests
            filterTestsByAccessControl(bundleInfo.getTests(), contentPriceTypes);

            // Update videos
            filterVideosByAccessControl(bundleInfo.getVideos(), contentPriceTypes);
        }
    }

    private static void filterVideosByAccessControl(VideoContentData videoContentData,
                                                    List<ContentPriceType> contentPriceTypes) {
        if (!CollectionUtils.isEmpty(contentPriceTypes) && videoContentData != null) {
            List<VideoBundleContentInfo> videos = videoContentData.getContents();
            if (!CollectionUtils.isEmpty(videos)) {
                for (VideoBundleContentInfo video : videos) {
                    if (video != null && !contentPriceTypes.contains(video.getType())) {
                        video.setContentId(null);
                        video.setUrl(null);
                        video.setEmbedCode(null);
                    }
                }
            }

            if (!CollectionUtils.isEmpty(videoContentData.getChildren())) {
                for (VideoContentData entry : videoContentData.getChildren()) {
                    filterVideosByAccessControl(entry, contentPriceTypes);
                }
            }
        }
    }

    private static void filterTestsByAccessControl(TestContentData testContentData,
                                                   List<ContentPriceType> contentPriceTypes) {
        if (!CollectionUtils.isEmpty(contentPriceTypes) && testContentData != null) {
            List<TestBundleContentInfo> tests = testContentData.getContents();
            if (!CollectionUtils.isEmpty(tests)) {
                for (TestBundleContentInfo test : tests) {
                    if (test != null && !contentPriceTypes.contains(test.getType())) {
                        test.setContentId(null);
                        test.setUrl(null);
                    }
                }
            }

            if (!CollectionUtils.isEmpty(testContentData.getChildren())) {
                for (TestContentData entry : testContentData.getChildren()) {
                    filterTestsByAccessControl(entry, contentPriceTypes);
                }
            }
        }
    }

    public PlatformBasicResponse addVideoAnalyticsData(AddVideoAnalyticsDataReq addVideoAnalyticsDataReq) {
        VideoAnalytics videoAnalytics = mapper.map(addVideoAnalyticsDataReq, VideoAnalytics.class);
        videoAnalyticsDAO.save(videoAnalytics, addVideoAnalyticsDataReq.getCallingUserId());
        return new PlatformBasicResponse();
    }

    private void addVideoViewedByUserInfo(BundleInfo bundleInfo, Long callingUserId) {
        if (bundleInfo.getVideos() != null) {
            List<VideoAnalytics> videoAnalyticsList = videoAnalyticsDAO.getAnalyticsByUserAndContext(callingUserId,
                    EntityType.BUNDLE, bundleInfo.getId());
            if (CollectionUtils.isNotEmpty(videoAnalyticsList)) {
                Map<String, VideoAnalytics> videoAnalyticsMap = new HashMap<>();
                for (VideoAnalytics videoAnalytics : videoAnalyticsList) {
                    videoAnalyticsMap.put(videoAnalytics.getVideoType() + "-" + videoAnalytics.getVideoId(),
                            videoAnalytics);
                }
                fillVideoAnalyticsData(bundleInfo.getVideos(), videoAnalyticsMap);
            }
        }
    }

    private void fillVideoAnalyticsData(VideoContentData videoContentData,
                                        Map<String, VideoAnalytics> videoAnalyticsMap) {
        if (videoContentData != null) {
            List<VideoBundleContentInfo> videos = videoContentData.getContents();
            if (!CollectionUtils.isEmpty(videos)) {
                for (VideoBundleContentInfo video : videos) {
                    if (video != null) {
                        String mapKey = (video.getVideoType() == null ? VideoType.VZAAR : video.getVideoType()) + "-"
                                + video.getContentId();
                        VideoAnalytics videoAnalytics = videoAnalyticsMap.get(mapKey);
                        if (videoAnalytics != null) {
                            video.setViewed(true);
                        }
                    }
                }
            }

            if (!CollectionUtils.isEmpty(videoContentData.getChildren())) {
                for (VideoContentData entry : videoContentData.getChildren()) {
                    fillVideoAnalyticsData(entry, videoAnalyticsMap);
                }
            }
        }
    }

    public List<VideoInfo> getVideoViewInfoFromAnalytics(List<VideoInfo> videoInfosNotFoundInRedis) {
        return videoAnalyticsDAO.getVideoViewInfoFromAnalytics(videoInfosNotFoundInRedis);
    }

    public PlatformBasicResponse markEnrolmentStateForTrials(MarkEnrolmentStateReq req) throws ConflictException {
        List<BundleEnrolment> bundleEnrolmentList = bundleEnrolmentDAO
                .getActiveTrialEnrolmentsByIds(req.getEnrolmentIds(), EnrollmentState.TRIAL);
        if (CollectionUtils.isNotEmpty(bundleEnrolmentList)) {
            for (BundleEnrolment bundleEnrolment : bundleEnrolmentList) {
                bundleEnrolment.setStatusChangeTime(getBundleEnrolmentStatusChangeTimes(bundleEnrolment.getStatusChangeTime(),
                        bundleEnrolment.getStatus(), req.getEntityStatus(), sessionUtils.getCallingUserId()));
                bundleEnrolment.setStatus(req.getEntityStatus());
                bundleEnrolmentDAO.save(bundleEnrolment, null);

                try {
                    updateWebinarSessionAttendence(bundleEnrolment);
                } catch (VException ex) {
                    logger.error("Error occured unregistering the user for sessions : " + bundleEnrolment.toString());
                }
            }
        }
        return new PlatformBasicResponse();
    }

    private List<StatusChangeTime> getBundleEnrolmentStatusChangeTimes(List<StatusChangeTime> statusChangeTimes,
                                                                       EntityStatus oldStatus, EntityStatus newStatus,
                                                                       Long callingUserId) {
        if (newStatus != null && oldStatus != newStatus) {
            List<StatusChangeTime> list = Optional.ofNullable(statusChangeTimes).orElseGet(ArrayList::new);
            StatusChangeTime statusChangeTime = new StatusChangeTime();
            statusChangeTime.setChangedBy(callingUserId);
            statusChangeTime.setPreviousStatus(oldStatus);
            statusChangeTime.setNewStatus(newStatus);
            statusChangeTime.setChangeTime(System.currentTimeMillis());
            list.add(statusChangeTime);
            return list;
        }
        return statusChangeTimes;
    }

    public List<BundleInfo> getPublicBundleInfosForBundleIds(List<String> bundleIds) {
        List<BundleInfo> response = new ArrayList<>();
        List<Bundle> bundles = bundleDAO.getBundlesByIds(bundleIds);
        if (CollectionUtils.isNotEmpty(bundles)) {
            for (Bundle bundle : bundles) {
                BundleInfo bundleInfo = mapper.map(bundle, BundleInfo.class);
                removeByReturnContentTypes(bundleInfo, null);
                response.add(bundleInfo);
            }
        }
        return response;
    }
    // calling function

    public List<InstalmentInfo> getInstalments(GetInstalmentReq req) throws VException {
        req.verify();
        if (req.getUserId() == null || (com.vedantu.util.StringUtils.isEmpty(req.getEntityId()))) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null or entitid are null");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(req.getUserId().toString()));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS)
                .in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(req.getEntityId()));
        List<BundleEnrolment> enrollments = bundleEnrolmentDAO.runQuery(query, BundleEnrolment.class);
        Set<String> deliverableIds = new HashSet<>();

        if (ArrayUtils.isNotEmpty(enrollments)) {
            if (enrollments.size() > 1) {
                logger.error("multiple bundle enrollments found for userId and bundleId combo " + req.getUserId() + " "
                        + req.getEntityId());
            }
            BundleEnrolment enrollment = enrollments.get(0);
            if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
                deliverableIds.add(enrollment.getId());
            } else if (EnrollmentState.TRIAL.equals(enrollment.getState())) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Enrollment is in Trial State.");
            }
            List<InstalmentInfo> instalmentInfos = instalmentUtilManager
                    .getInstalmentsForDeliverableIds(req.getUserId(), deliverableIds);
            if (ArrayUtils.isNotEmpty(instalmentInfos)) {
                return instalmentInfos;
            }
        }
        return new ArrayList<>();
    }

    public void processInstalmentAfterPayment(InstalmentInfo info) throws VException {

        List<InstalmentInfo> res = paymentManager.processInstalmentAfterPayment(info.getId());
        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", info.getUserId());
        payload.put("bundleId", info.getContextId());
        payload.put("instalments", res);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BUNDLE_FROM_2_ND_INSTALMENT_PAYMENT_EMAIL, payload);
        asyncTaskFactory.executeTask(params);
        postProcessInstalmentAfterPayment(info);
    }

    public void postProcessInstalmentAfterPayment(InstalmentInfo info) throws VException {
        Boolean orderState = paymentManager.resetInstalmentsPostPayment(info.getContextId(), EntityType.BUNDLE,
                info.getUserId().toString());
        if (Boolean.FALSE.equals(orderState)) {
            logger.info("More instalments to be paid by user. Not changing state of Enrollment.");
            // Trigger mail about it.
            return;
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(info.getUserId().toString()));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.INACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(info.getContextId()));

        logger.info("query " + query);
        List<BundleEnrolment> enrollments = bundleEnrolmentDAO.runQuery(query, BundleEnrolment.class);

        if (ArrayUtils.isNotEmpty(enrollments)) {
            if (enrollments.size() > 1) {
                logger.error("multiple bundle enrollments found for userId and bundleId combo "
                        + info.getUserId() + " " + info.getContextId());
            }
            BundleEnrolment enrollment = enrollments.get(0);
            logger.info("marking the inactive enrollment as active");
            enrollment.setStatus(EntityStatus.ACTIVE);
            StatusChangeTime statusChangeTime = new StatusChangeTime();
            statusChangeTime.setChangeTime(System.currentTimeMillis());
            statusChangeTime.setNewStatus(EntityStatus.ACTIVE);
            statusChangeTime.setPreviousStatus(EntityStatus.INACTIVE);
            if (ArrayUtils.isEmpty(enrollment.getStatusChangeTime())) {
                enrollment.setStatusChangeTime(Arrays.asList(statusChangeTime));
            } else {
                enrollment.getStatusChangeTime().add(statusChangeTime);
            }
            UpdateStatusReq updateStatusReq = new UpdateStatusReq();
            updateStatusReq.setBundleEnrollmentId(enrollment.getId());
            updateStatusReq.setBundleId(enrollment.getBundleId());
            updateStatusReq.setStatus(EntityStatus.ACTIVE);
            updateStatusReq.setUserId(enrollment.getUserId());
            updateStatusReq.setUpdateOrder(false);
            markBundleActiveInActive(updateStatusReq);
            bundleEnrolmentDAO.save(enrollment, null);
        }
    }


    String[] autoEnrollBundlesArray={"5e4eebab7394e448163a50e1","5e4eeedcd1c96e3f6c06f012","5e4ef018706d7f2e5c3ef7a6","5e4ef3a6706d7f2e5c3ef7a9","5e4ef6cb553baf7b8e231412","5e4ef81bd1c96e3f6c06f016","5e4ef9d82a677836f0de9a1d","5e4f01f95234be4fbef3b204","5e3164da6c8b2420bae0152a","5e4f05777394e448163a50e6","5e4f082c7394e448163a50e7","5e4f0c8d7394e448163a50e8","5e4f0f9f7394e448163a50ea","5e4f15c9d1c96e3f6c06f019","5e4f1b364c15045a3f468b62","5e6ba151d20f0029a973885b","5e6b9d1333bd0e588bc42ed9","5e6a9aa626ebdc17331d5c0c","5e6a9abcb106f86a9c804613","5e6a9adbb911ec74ccc810ac"};
    List<String> autoEnrollBundlesList=Arrays.asList(autoEnrollBundlesArray);
    public void autoEnrollInBundleCourses(String bundleId, String userId, String enrollmentId) throws VException {

        if(StringUtils.isEmpty(bundleId) || StringUtils.isEmpty(userId) || StringUtils.isEmpty(enrollmentId)){
            throw new BadRequestException(ErrorCode.MISSING_PARAMETER,"Either the value fo bundleId or userId or enrollmentId is missing");
        }

        List<BundleEntity> bundleEntities = bundleEntityDAO.getAutoEnrollEntityOfBundle(bundleId);
        boolean foundAutoEnrollCourse=false;
        if (ArrayUtils.isNotEmpty(bundleEntities)) {
            foundAutoEnrollCourse=true;
            for (BundleEntity bundleEntity : bundleEntities) {
                List<String> batchIds = new ArrayList<>();
                OTFBundleInfo oTFBundleInfo = new OTFBundleInfo();
                if (PackageType.OTM_BUNDLE.equals(bundleEntity.getPackageType())) {
                    oTFBundleInfo = otfBundleManager.getOTFBundle(bundleEntity.getEntityId());
                    if (oTFBundleInfo == null || ArrayUtils.isEmpty(oTFBundleInfo.getEntities())) {
                        logger.warn("invalid otmbundleOfBatchesId  " + bundleEntity.getEntityId());
                        return;
                    }

                    List<OTMBundleEntityInfo> entities = oTFBundleInfo.getEntities();
                    for (OTMBundleEntityInfo entity : entities) {
                        if (EntityType.OTF.equals(entity.getEntityType()) && !StringUtils.isEmpty(entity.getEntityId())) {
                            batchIds.add(entity.getEntityId());
                        }
                    }
                } else {
                    batchIds.add(bundleEntity.getEntityId());
                }
                List<Batch> batchData=batchDAO.getBatchByIds(batchIds,Arrays.asList(Batch.Constants._ID,Batch.Constants.COURSE_ID),null);
                Map<String, String> batchIdCourseIdMap = batchData.stream().collect(Collectors.toMap(Batch::getId, Batch::getCourseId));
                _autoEnrollInBundleCourses(userId, bundleId, bundleEntity, EntityStatus.ACTIVE, enrollmentId,batchIds,oTFBundleInfo,batchIdCourseIdMap);
            }
        }

        if (!foundAutoEnrollCourse && autoEnrollBundlesList.indexOf(bundleId) > -1) {
            logger.info("No autoenroll batch found for " + bundleId + " , userId " + userId);
        }
    }

    public void autoEnrollForAllEnrolledUserIdsInBundle(String bundleId, BundleEntity bundleEntity) throws VException {
        logger.info("--------- \nStarting of auto enrollment process for bundleId {} and bundle package {} " +
                "\n---------",bundleId,bundleEntity.toString());

        List<String> batchIds = new ArrayList<>();
        OTFBundleInfo oTFBundleInfo = new OTFBundleInfo();
        if (PackageType.OTM_BUNDLE.equals(bundleEntity.getPackageType())) {
            oTFBundleInfo = otfBundleManager.getOTFBundle(bundleEntity.getEntityId());
            if (oTFBundleInfo == null || ArrayUtils.isEmpty(oTFBundleInfo.getEntities())) {
                logger.warn("invalid otmbundleOfBatchesId  " + bundleEntity.getEntityId());
                return;
            }

            List<OTMBundleEntityInfo> entities = oTFBundleInfo.getEntities();
            for (OTMBundleEntityInfo entity : entities) {
                if (EntityType.OTF.equals(entity.getEntityType()) && !StringUtils.isEmpty(entity.getEntityId())) {
                    batchIds.add(entity.getEntityId());
                }
            }
        } else {
            batchIds.add(bundleEntity.getEntityId());
        }

        List<Batch> batchData=batchDAO.getBatchByIds(batchIds,Arrays.asList(Batch.Constants._ID,Batch.Constants.COURSE_ID),null);
        Map<String, String> batchIdCourseIdMap = batchData.stream().collect(Collectors.toMap(Batch::getId, Batch::getCourseId));

        List<String> includeFields=Arrays.asList(
                BundleEnrolment.Constants.USER_ID,
                BundleEnrolment.Constants._ID
        );
        int fetchCount = 1000;
        GetBundleEnrolmentsReq req = new GetBundleEnrolmentsReq();
        req.setStatus(EntityStatus.ACTIVE);
        req.setBundleId(bundleId);
        req.setStart(0);
        req.setSize(fetchCount);
        List<BundleEnrolment> enrollments = bundleEnrolmentDAO.getBundleEnrolmentsFromReq(req,includeFields,null);
        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollmentStatus", EntityStatus.ACTIVE);
        payload.put("bundleId", bundleId);
        payload.put("batchIds", batchIds);
        payload.put("bundleEntity", bundleEntity);
        payload.put("oTFBundleInfo", oTFBundleInfo);
        payload.put("batchIdCourseIdMap", batchIdCourseIdMap);
        while (ArrayUtils.isNotEmpty(enrollments)) {
            Lists.partition(enrollments, 100)
                    .stream()
                    .map(this::getEnrollmentIdUserIdMap)
                    .forEach(enrollmentIdUserIdMap -> {
                        payload.put("enrollmentIdUserIdMap", enrollmentIdUserIdMap);
                        sqsManager.sendToSQS(SQSQueue.BUNDLE_AUTO_ENROLL_OPS, SQSMessageType.AUTO_ENROLL_BATCHES, gson.toJson(payload), bundleId);
                    });
            req.setStart(req.getStart() + fetchCount);
            enrollments = bundleEnrolmentDAO.getBundleEnrolmentsFromReq(req,includeFields,null);
        }

        req.setStatus(EntityStatus.INACTIVE);
        req.setStart(0);
        List<BundleEnrolment> inActiveEnrollments = bundleEnrolmentDAO.getBundleEnrolmentsFromReq(req,includeFields,null);
        payload.put("enrollmentStatus", EntityStatus.INACTIVE);
        while (ArrayUtils.isNotEmpty(inActiveEnrollments)) {
            Lists.partition(inActiveEnrollments, 100)
                    .stream()
                    .map(this::getEnrollmentIdUserIdMap)
                    .forEach(enrollmentIdUserIdMap -> {
                        payload.put("enrollmentIdUserIdMap", enrollmentIdUserIdMap);
                        sqsManager.sendToSQS(SQSQueue.BUNDLE_AUTO_ENROLL_OPS, SQSMessageType.AUTO_ENROLL_BATCHES, gson.toJson(payload), bundleId);
                    });
            req.setStart(req.getStart() + fetchCount);
            inActiveEnrollments = bundleEnrolmentDAO.getBundleEnrolmentsFromReq(req,includeFields,null);
        }
        logger.info("End of auto enrollment of bundle with id {}",bundleId);
    }

    private Map<String, String> getEnrollmentIdUserIdMap(List<BundleEnrolment> currentEnrollments) {
        return Optional.ofNullable(currentEnrollments)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .collect(Collectors.toMap(BundleEnrolment::getId, BundleEnrolment::getUserId));
    }

    public void _autoEnrollInBundleCourses(String userId, String bundleId, BundleEntity bundleEntity,
                                           EntityStatus enrollmentStatus, String enrollmentId,List<String> batchIds,
                                           OTFBundleInfo oTFBundleInfo,Map<String, String> batchIdCourseIdMap) throws VException {
        logger.info("\n---_autoEnrollInBundleCourses--- \nuserId {} \nbundleId {} \nbundleEntity {} \nenrollmentStatus {} " +
                        "\nenrollmentId {} \n batchIds {} \noTFBundleInfo {} \nbatchIdCourseIdMap {}", userId ,bundleId, bundleEntity,
                enrollmentStatus,enrollmentId,batchIds,oTFBundleInfo,batchIdCourseIdMap);

        Course course;
        logger.info("Batch ids to be enrolled for bundleId {}, userId {}, batchIds {}",bundleId,userId,batchIds);
        if (ArrayUtils.isEmpty(batchIds)) {
            return;
        }

        for (String batchId : batchIds) {
            logger.info("Enrolling for bundle id {} - batchId - {} - userId {}",bundleId,batchId,userId);
            try {
                String courseId = batchIdCourseIdMap.get(batchId);
                List<Enrollment> enrollments = enrollmentManager.getEnrollment(userId, courseId, null);

                logger.info("Enrollment present for bundleId - {} - batchId - {} - userId - {} is {}",bundleId,batchId,userId,ArrayUtils.isNotEmpty(enrollments));

                if (ArrayUtils.isEmpty(enrollments)) {

                    logger.info("No enrollment present so going for a fresh one...");

                    Enrollment oTFEnrollmentReq = new Enrollment(userId, batchId, null, null, Role.STUDENT, enrollmentStatus, null);
                    oTFEnrollmentReq.setPurchaseContextType(EnrollmentPurchaseContext.BUNDLE);
                    oTFEnrollmentReq.setPurchaseContextId(bundleId);
                    oTFEnrollmentReq.setPurchaseEnrollmentId(enrollmentId);
                    oTFEnrollmentReq.setState(EnrollmentState.REGULAR);
                    if (PackageType.OTM_BUNDLE.equals(bundleEntity.getPackageType())) {
                        oTFEnrollmentReq.setEntityTitle(oTFBundleInfo.getTitle());
                        oTFEnrollmentReq.setEntityType(com.vedantu.session.pojo.EntityType.OTM_BUNDLE);
                        oTFEnrollmentReq.setEntityId(bundleEntity.getEntityId());
                    } else {
                        course = courseManager.getCourse(courseId);
                        oTFEnrollmentReq.setEntityTitle(course.getTitle());
                        oTFEnrollmentReq.setEntityType(EntityType.OTF_COURSE);
                        oTFEnrollmentReq.setEntityId(course.getId());
                    }
                    logger.info(" creating new batch enrollment for bundle id {} with parameter {}",bundleId ,oTFEnrollmentReq);
                    enrollmentManager.updateEnrollment(oTFEnrollmentReq, Long.parseLong(userId));
                    logger.info("After fresh enrollment received enrollment id is {}",oTFEnrollmentReq.getId());

                    logger.info("sharing curriculum");
                    ShareContentEnrollmentReq request = new ShareContentEnrollmentReq();
                    request.setContextId(batchId);
                    request.setStudentId(userId);
                    request.setStartTime(null);
                    sqsManager.sendToSQS(SQSQueue.BUNDLE_CONTENT_SHARE_OPS, SQSMessageType.AUTO_ENROLL_CONTENT_SHARE, gson.toJson(request), "AUTO_ENROLL_CONTENT_SHARE"+userId);
                    // enrollmentAsyncManager.shareContentOnEnrollment(request);

                    logger.info("updateCalendar");
                    Map<String,Object> payload=new HashMap<>();
                    payload.put("userId",userId);
                    payload.put("batchId",oTFEnrollmentReq.getBatchId());
                    payload.put("enrollmentStatus",enrollmentStatus);

                    logger.info("creating gtt entries for sessions before {} + 1day",  System.currentTimeMillis());
                    sqsManager.sendToSQS(SQSQueue.BUNDLE_GTT_CREATION_OPS, SQSMessageType.AUTO_ENROLL_GTT_CREATION, gson.toJson(oTFEnrollmentReq), "NEW_ENROLL_GTT_CREATION" + userId);

                } else {
                    logger.info("Enrollment present so going for the modification of the same...");
                    Enrollment enrollment = enrollments.get(0);
                    if (EntityStatus.INACTIVE.equals(enrollment.getStatus())
                            && EntityStatus.ACTIVE.equals(enrollmentStatus)) {
                        logger.info("updateEnrollment " + enrollment);

                        OTFEnrollmentReq oTFEnrollmentReq = mapper.map(enrollment, OTFEnrollmentReq.class);
                        oTFEnrollmentReq.setStatus(EntityStatus.ACTIVE.name());
                        logger.info("updateEnrollmentreq " + oTFEnrollmentReq);
                        enrollment = enrollmentManager.markStatus(oTFEnrollmentReq);
                        logger.info("enrollment after mark status " + enrollment);

                        logger.info("sharing curriculum");
                        Long changeTime = null;
                        if (ArrayUtils.isNotEmpty(enrollment.getStatusChangeTime())) {
                            for (StatusChangeTime statuschangeTime : enrollment.getStatusChangeTime()) {
                                if (EntityStatus.INACTIVE.equals(statuschangeTime.getNewStatus())) {
                                    changeTime = statuschangeTime.getChangeTime();
                                }
                            }
                        }
                        ShareContentEnrollmentReq request = new ShareContentEnrollmentReq();
                        request.setContextId(enrollment.getBatchId());
                        request.setStudentId(enrollment.getUserId());
                        request.setStartTime(changeTime);
                        sqsManager.sendToSQS(SQSQueue.BUNDLE_CONTENT_SHARE_OPS, SQSMessageType.AUTO_ENROLL_CONTENT_SHARE, gson.toJson(request), "AUTO_ENROLL_CONTENT_SHARE"+userId);

                        logger.info("updateCalendar");
                        if (!Role.STUDENT.equals((enrollment.getRole()))) {
                            Map<String,Object> payload=new HashMap<>();
                            payload.put("userId",userId);
                            payload.put("batchId",oTFEnrollmentReq.getBatchId());
                            payload.put("enrollmentStatus",enrollmentStatus);
                            sqsManager.sendToSQS(SQSQueue.BUNDLE_CALENDAR_OPS, SQSMessageType.AUTO_ENROLL_CALENDAR_POPULATION, gson.toJson(payload), "AUTO_ENROLL_CALENDAR_POPULATION"+userId);
                        }

                    } else {
                        logger.info("Not doing anything as the enrollment is already in state of {}",enrollment.getStatus());
                    }
                }

            } catch (Exception e) {
                logger.warn("error in enrolling the user for auto enrollment" + e.getMessage());
            }
        }
    }

    public void removeEnrollForAllEnrolledUserIdsInBundle(String bundleId, String otmbundleOfBatchesId)
            throws VException {
        logger.info("removeEnrollInBundleCourses for bundleId " + bundleId + ", otmbundleOfBatchesId "
                + otmbundleOfBatchesId);

        int maxSize = 1000, start = 0;
        while (true) {

            List<Enrollment> enrollments = enrollmentDAO.getActiveEnrollmentsForPurchaseContextId(bundleId,
                    otmbundleOfBatchesId, start, maxSize);

            if (ArrayUtils.isEmpty(enrollments)) {
                break;
            }

            for (Enrollment enrollment : enrollments) {
                try {
                    logger.info("doing for enrollment " + enrollment);
                    OTFEnrollmentReq oTFEnrollmentReq = mapper.map(enrollment, OTFEnrollmentReq.class);
                    oTFEnrollmentReq.setStatus(EntityStatus.INACTIVE.name());
                    logger.info(" updateEnrollment " + oTFEnrollmentReq);
                    enrollment = enrollmentManager.markStatus(oTFEnrollmentReq);
                    logger.info("enrollment after mark status " + enrollment);

                    logger.info("updating calender");
                    if (!Role.STUDENT.equals(oTFEnrollmentReq.getRole())) {
                        enrollmentAsyncManager.updateCalendar(enrollment.getUserId(), oTFEnrollmentReq.getBatchId(),
                                EntityStatus.INACTIVE);
                    }
                } catch (Exception e) {
                    logger.warn("error in removing the enrollment the user for autoenrollment" + e.getMessage());
                }
            }
            if (enrollments.size() < maxSize) {
                break;
            }
            start += maxSize;
        }
    }

    public PlatformBasicResponse endBundleEnrollment(EndBundleEnrollmentReq req, Long callingUserId)
            throws VException {
        req.verify();
        String bundleEnrollmentId = req.getBundleEnrollmentId();

        BundleEnrolment enrolment = bundleEnrolmentDAO.getBundleEnrolmentById(bundleEnrollmentId);
        if (enrolment == null) {
            throw new NotFoundException(ErrorCode.BUNDLE_ENROLLMENT_NOT_FOUND, "BUNDLE_ENROLLMENT_NOT_FOUND");
        }

//                if (EnrollmentState.TRIAL.equals(enrolment.getState())){
//                    throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Ending trial enrollment is not supported yet");
//                }
        //TODO implement locking, will fail in race conditions
        if (EntityStatus.ENDED.equals(enrolment.getStatus())) {
            throw new ConflictException(ErrorCode.ALREADY_ENDED, "AIO Enrollment is already ended");
        }

        enrolment.setStatusChangeTime(getBundleEnrolmentStatusChangeTimes(enrolment.getStatusChangeTime(),
                enrolment.getStatus(), EntityStatus.ENDED, callingUserId));

        enrolment.setStatus(EntityStatus.ENDED);
        enrolment.setEndedBy(callingUserId);
        enrolment.setEndReason(req.getEndReason());
        bundleEnrolmentDAO.save(enrolment, callingUserId);
        List<Enrollment> batchEnrollments = enrollmentDAO
                .getEnrollmentsForPurchaseContextIdForUserId(enrolment.getBundleId(), enrolment.getUserId());
        if (ArrayUtils.isNotEmpty(batchEnrollments)) {
//            if (batchEnrollments.size() >= 100) {
//                logger.error(
//                        "reached max limit for end batch enrollments during endBundleEnrollment for bundleEnrollmentId "
//                        + bundleEnrollmentId);
//            }
            Map<String, Object> payload = new HashMap<String, Object>();
            payload.put("enrollment", batchEnrollments);
            asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.END_BATCHS_FOR_BUNDLE, payload));

        }

        if (EnrollmentState.REGULAR.equals(enrolment.getState())) {

            Map<String, Object> payload = new HashMap<String, Object>();
            JSONObject markOrderForfeitedReq = new JSONObject();
            markOrderForfeitedReq.put("entityType", EntityType.BUNDLE);
            markOrderForfeitedReq.put("entityId", enrolment.getBundleId());
            markOrderForfeitedReq.put("userId", enrolment.getUserId());
            markOrderForfeitedReq.put("orderEndType", req.getOrderEndType());

            payload.put("markOrderForfeitedReq", markOrderForfeitedReq);
            payload.put("enrollments", enrolment);
            payload.put("req", req);
            payload.put("callingUserId", callingUserId);

            asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.END_BUNDLE_REFUND, payload));

        } else if (EnrollmentState.TRIAL.equals(enrolment.getState())) {
            //TODO implement refund api call of dinero
        }
        return new PlatformBasicResponse();
    }


    public void bundleRefund(JSONObject markOrderForfeitedReq, BundleEnrolment enrolment, EndBundleEnrollmentReq req, Long callingUserId) throws VException {


        try {
            String markOrderForfeited = DINERO_ENDPOINT + "/payment/markOrderEnded";

            ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                    markOrderForfeitedReq.toString());
            VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
            String jsonString_1 = markOrderForfeitedRes.getEntity(String.class);
            logger.info("Response for markOrderForfeited  : " + jsonString_1);
            MarkOrderEndedRes markOrderEndedRes = gson.fromJson(jsonString_1, MarkOrderEndedRes.class);

            RefundReq refundReq = new RefundReq();
            refundReq.setOrderId(markOrderEndedRes.getOrderId());
            refundReq.setUserId(Long.valueOf(enrolment.getUserId()));
            refundReq.setCallingUserId(req.getCallingUserId());
            refundReq.setRefundPolicy(RefundPolicy.PARTIAL);
            refundReq.setGetRefundInfoOnly(false);
            refundReq.setEntityType(EntityType.BUNDLE);
            refundReq.setEntityId(enrolment.getBundleId());
            refundReq.setNonPromotionalAmount(req.getNonPromotionalAmount());
            refundReq.setPromotionalAmount(req.getPromotionalAmount());
            refundReq.setDiscountLeft(req.getAmtToRefundToDiscountWallet());
            refundReq.setReason(req.getEndReason());
            refundReq.setActualRefundDate(req.getActualRefundDate());

            ClientResponse resp = WebUtils.INSTANCE.doCall(
                    DINERO_ENDPOINT + "/refund", HttpMethod.POST, new Gson().toJson(refundReq));
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String refjson = resp.getEntity(String.class);
            logger.info("EXIT: " + refjson);
            RefundRes res = new Gson().fromJson(refjson, RefundRes.class);

        } catch (JSONException | VException | ClientHandlerException |
                UniformInterfaceException ex) {
            logger.error("Error in markOrderForfeited for enrollment  " + enrolment.getId(), ex);
        }

    }

    public PlatformBasicResponse markBundleActiveInActive(UpdateStatusReq req) throws VException {
        BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getBundleEnrolmentById(req.getBundleEnrollmentId());
        if (bundleEnrolment == null) {
            throw new NotFoundException(ErrorCode.BUNDLE_ENROLLMENT_NOT_FOUND, "BUNDLE_ENROLLMENT_NOT_FOUND");
        }

        if (EntityStatus.ENDED.equals(bundleEnrolment.getStatus())) {
            throw new VException(ErrorCode.ENROLLMENT_ALREADY_ENDED, "Enrollment is Ended can not update the state ");
        }

        if (req.getStatus().equals(bundleEnrolment.getStatus())) {
            throw new VException(ErrorCode.ENROLLMENT_IS_ALREADY_IN_THIS_STATE, "Enrollment is already in this  state ");
        }

        if (EntityStatus.ENDED.toString().equals(req.getStatus().toString())) {
            throw new VException(ErrorCode.ENROLLMENT_CAN_NOT_ENDED, "Enrollment cant be Ended ");
        }


//        Map<String, Enrollment> newEnrollmentMap = new HashMap<>();
//        Map<String, Enrollment> oldEnrollmentMap = new HashMap<>();
        StatusChangeTime statusChangeTime = new StatusChangeTime();
        statusChangeTime.setChangeTime(System.currentTimeMillis());
        statusChangeTime.setChangedBy(req.getCallingUserId());
        statusChangeTime.setNewStatus(req.getStatus());
        statusChangeTime.setPreviousStatus(bundleEnrolment.getStatus());
        if (ArrayUtils.isEmpty(bundleEnrolment.getStatusChangeTime())) {
            bundleEnrolment.setStatusChangeTime(Arrays.asList(statusChangeTime));
        } else {
            bundleEnrolment.getStatusChangeTime().add(statusChangeTime);
        }
        bundleEnrolment.setStatus(req.getStatus());
        updateBatchEnrollments(bundleEnrolment,req.getStatus());
        if (req.getUpdateOrder()) {
            if (EntityStatus.INACTIVE.equals(bundleEnrolment.getStatus())) {
                logger.info("marking the order forfeited if the payment is done by instalment and not fully paid");
                try {
                    String markOrderForfeited = DINERO_ENDPOINT + "/payment/markOrderForfeited";
                    JSONObject markOrderForfeitedReq = new JSONObject();
                    if (!StringUtils.isEmpty(req.getOrderId())) {
                        markOrderForfeitedReq.put("orderId", req.getOrderId());
                    } else {
                        markOrderForfeitedReq.put("entityType", EntityType.BUNDLE);
                        markOrderForfeitedReq.put("entityId", req.getBundleId());
                        markOrderForfeitedReq.put("userId", req.getUserId());
                    }

                    ClientResponse markOrderForfeitedRes = WebUtils.INSTANCE.doCall(markOrderForfeited, HttpMethod.POST,
                            markOrderForfeitedReq.toString());
                    VExceptionFactory.INSTANCE.parseAndThrowException(markOrderForfeitedRes);
                    String jsonString = markOrderForfeitedRes.getEntity(String.class);
                    logger.info("Response for markOrderForfeited  : " + jsonString);
                } catch (Exception ex) {
                    logger.error("Error in markOrderForfeited for batchId  " + req.getBundleId() + ", userId " + req.getUserId(), ex);
                }
            } else if (EntityStatus.ACTIVE.equals(bundleEnrolment.getStatus())) {
                String resetOrderInstallmentState = DINERO_ENDPOINT + "/payment/resetOrderInstallmentState";
                JSONObject resetOrderInstallmentStateReq = new JSONObject();
                resetOrderInstallmentStateReq.put("entityType", EntityType.BUNDLE);
                resetOrderInstallmentStateReq.put("entityId", req.getBundleId());
                resetOrderInstallmentStateReq.put("userId", req.getUserId());
                resetOrderInstallmentStateReq.put("deliverableEntityId", bundleEnrolment.getId());

                ClientResponse resetOrderInstallmentStateRes = WebUtils.INSTANCE.doCall(resetOrderInstallmentState, HttpMethod.POST,
                        resetOrderInstallmentStateReq.toString());
                VExceptionFactory.INSTANCE.parseAndThrowException(resetOrderInstallmentStateRes);
                String jsonString = resetOrderInstallmentStateRes.getEntity(String.class);
                logger.info("Response for resetOrderInstallmentState  : " + jsonString);
            }
        }

        bundleEnrolmentDAO.save(bundleEnrolment, req.getCallingUserId());
        return new PlatformBasicResponse();
    }
    public void updateBatchEnrollments(  BundleEnrolment bundleEnrolment, EntityStatus newStatus) throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_ENROLLMENT_ID).is(bundleEnrolment.getId()));
        query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ENROLLMENT_TYPE).ne(EnrollmentType.UPSELL));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(bundleEnrolment.getUserId()));
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        logger.info("query    " + query);
        logger.info("enrollments    " + enrollments);
        if (ArrayUtils.isNotEmpty(enrollments)) {
            for (Enrollment enrollment : enrollments) {
                try {
                    if (!EntityStatus.ENDED.equals(enrollment.getStatus())) {
                        logger.info("doing for enrollment " + enrollment);
                        OTFEnrollmentReq oTFEnrollmentReq = mapper.map(enrollment, OTFEnrollmentReq.class);
                        oTFEnrollmentReq.setStatus(newStatus.name());
                        logger.info(" updateEnrollment " + oTFEnrollmentReq);
                        Enrollment newEnrollment = enrollmentManager.markStatus(oTFEnrollmentReq);
                        logger.info("enrollment after mark status " + enrollment);
//                        oldEnrollmentMap.put(enrollment.getId(), enrollment);
//                        newEnrollmentMap.put(enrollment.getId(), newEnrollment);
                    }
                } catch (JSONException | VException | ClientHandlerException | UniformInterfaceException ex) {
                    logger.error("Error in MarkBundleActiveInActive for bundle enrollment  " + bundleEnrolment.getBundleId(), ex);
                }
            }
            //		otfBundleManager.enrollmentTriggers(newEnrollmentMap.keySet(), oldEnrollmentMap,newEnrollmentMap);
        }
    }
    public void updateBundleStatus(GetBundleEnrollmentReq req) throws VException {
        BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getBundleEnrolmentById(req.getBundleEnrollmentId());
        if (EntityStatus.ENDED.equals(bundleEnrolment.getStatus())) {
            throw new VException(ErrorCode.ENROLLMENT_ALREADY_ENDED, "Enrollment is Ended can not update the state ");
        }
        if (req.getState().equals(bundleEnrolment.getStatus())) {
            throw new VException(ErrorCode.ENROLLMENT_ALREADY_ENDED, "Enrollment is already in this  state ");
        }
        if (EntityStatus.ENDED.toString().equals(req.getState().toString())) {
            throw new VException(ErrorCode.ENROLLMENT_ALREADY_ENDED, "Enrollment cant be Ended ");
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).is(bundleEnrolment.getBundleId()));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(bundleEnrolment.getUserId()));
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        logger.info("query    " + query);
        logger.info("enrollments    " + enrollments);
        for (Enrollment enrollment : enrollments) {
            if (!EntityStatus.ENDED.equals(enrollment.getStatus())) {
                enrollment.setStatus(req.getState());
                logger.info("req status :- " + req.getState() + "enrolment status " + enrollment.getStatus());
                enrollmentManager.updateEnrollment(enrollment, req.getCallingUserId());
            }
        }
        bundleEnrolment.setStatusChangeTime(getBundleEnrolmentStatusChangeTimes(bundleEnrolment.getStatusChangeTime(),
                bundleEnrolment.getStatus(), req.getState(), req.getCallingUserId()));

        bundleEnrolment.setStatus(req.getState());
        bundleEnrolmentDAO.save(bundleEnrolment, req.getCallingUserId());
    }

    public void terminateFreePassCreateAsync() throws VException {
        Map<String, Object> bp = new HashMap<>();
        AsyncTaskParams bptask = new AsyncTaskParams(AsyncTaskName.TERMINATE_FREE_PASS, bp);
        asyncTaskFactory.executeTask(bptask);
    }

    public List<SubscriptionUpdateResp>  getRechargeDetail(String bundleEnrollmentId) throws VException {
        logger.info("method: getReachargeDetail, bundleEnrollmentId: {}",bundleEnrollmentId);
        List<SubscriptionUpdateResp> resps = new ArrayList<>();
        BundleEnrolment  bundleEnrolment =  bundleEnrolmentDAO.getEntityById(bundleEnrollmentId,BundleEnrolment.class);
        logger.info("bundleEnrolment: {}",bundleEnrolment);
        if(bundleEnrolment.getIsSubscriptionPlan()== null || !bundleEnrolment.getIsSubscriptionPlan()){
            logger.info("bundleEnrolment reps: {}",resps);
            return  resps;
        }
        String baseBundleEnrollmentId = "";
        List<SubscriptionUpdate>   subscriptionUpdates =  subscriptionUpdateDAO.getSubscriptionPlan(bundleEnrolment.getBundleId(),bundleEnrolment.getUserId());
        logger.info("subscriptionUpdates: {}",subscriptionUpdates);
        if ( ArrayUtils.isNotEmpty(subscriptionUpdates)) {
            baseBundleEnrollmentId = subscriptionUpdates.get(0).getOldEnrollmentId();
            logger.info("baseBundleEnrollmentId 1: {}",baseBundleEnrollmentId);
        }else {
            baseBundleEnrollmentId = bundleEnrolment.getId();
            logger.info("baseBundleEnrollmentId 2: {}",baseBundleEnrollmentId);
        }
        SubscriptionPlanInfoResp planResp =  getBaseSubscriptionPlanByDeliverableId(baseBundleEnrollmentId);
        logger.info("planResp : {}",planResp);
        if(planResp != null) {
            resps.add(new SubscriptionUpdateResp(planResp.getCreationTime(), planResp.getValidMonths(), true, planResp.getOrderId()));
        }
        if ( ArrayUtils.isNotEmpty(subscriptionUpdates)) {
            subscriptionUpdates.forEach(subscriptionUpdate -> {
                logger.info("subscriptionUpdate : {}",subscriptionUpdate);
                resps.add(new SubscriptionUpdateResp(subscriptionUpdate.getEnrollmentStartDate(), subscriptionUpdate.getValidMonths(), subscriptionUpdate.getIsEnrollmentCreated(), subscriptionUpdate.getOrderId()));

            });

        }
        logger.info("Final bundleEnrolment reps: {}",resps);
        return resps;

    }

    public SubscriptionUpdate  getSubscriptionUpdateById(String subscriptionUpdateId) throws VException {

        return subscriptionUpdateDAO.getEntityById(subscriptionUpdateId,SubscriptionUpdate.class);

    }

    public void terminateFreePass()
            throws VException {
        Long currentMillis = System.currentTimeMillis();
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.FREE_PASS_VALID_TILL).lt(currentMillis));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).in(EnrollmentState.FREE_PASS, EnrollmentState.TRIAL));
        query.limit(2000);
        for(int i = 0 ;i<25;i++) {
            query.skip(2000* i);
            List<BundleEnrolment> enrollments = bundleEnrolmentDAO.runQuery(query, BundleEnrolment.class);
            for (BundleEnrolment enrollment : enrollments) {


                UpdateStatusReq updateStatusReq = new UpdateStatusReq();
                updateStatusReq.setBundleEnrollmentId(enrollment.getId());
                updateStatusReq.setBundleId(enrollment.getBundleId());
                updateStatusReq.setStatus(EntityStatus.INACTIVE);
                updateStatusReq.setUserId(enrollment.getUserId());
                updateStatusReq.setUpdateOrder(false);


                sqsManager.sendToSQS(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE, SQSMessageType.MARK_TRIAL_BUNDLE_INAVTIVE, gson.toJson(updateStatusReq));


                //   markBundleActiveInActive(updateStatusReq);
            }
        }
    }



    public void markTrialBundleInActive(UpdateStatusReq updateStatusReq) throws VException {
        BundleEnrolment bundleEnrolment =   bundleEnrolmentDAO.getEntityById(updateStatusReq.getBundleEnrollmentId(),BundleEnrolment.class);
        if((EnrollmentState.TRIAL.equals( bundleEnrolment.getState())|| EnrollmentState.FREE_PASS.equals( bundleEnrolment.getState()) )&& EntityStatus.ACTIVE.equals(bundleEnrolment.getStatus())){
            markBundleActiveInActive(updateStatusReq);
        }
    }
    public void updateStatusBybundleId(UpdateStatusReq req) throws VException {
        //BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getNonEndedBundleEnrolment(req.getUserId(), req.getBundleId());
//        logger.info("user id   "  + req.getUserId() + "bundle   " +  req.getBundleId() + " enrolment : -  " + bundleEnrolment.getId() + " state " + req.getStatus());
//        GetBundleEnrollmentReq _req = new GetBundleEnrollmentReq();
//        _req.setState(req.getStatus());
//        _req.setBundleEnrollmentId(bundleEnrolment.getId());
//        _req.setUserId(req.getUserId());
//        _req.setOrderId(req.getOrderId());
//        _req.setBundleId(req.getBundleId());
        //updateBundleStatus(_req);
        if (req.getBundleEnrollmentId() != null) {
            //	req.setBundleEnrollmentId(bundleEnrolment.getId());
            markBundleActiveInActive(req);
        } else {
            throw new BadRequestException(ErrorCode.BUNDLE_ENROLLMENT_NOT_FOUND, "No non-ended bundle enrollment found for bundle id " + req.getBundleId()
                    + "for the user id " + req.getUserId(), Level.INFO);
        }
    }

    public void enrollWithFreePass(FreePassReq req) throws VException {

        ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + req.getUserId().toString(), HttpMethod.GET,
                null, true);
        // handle error correctly
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        User response = new Gson().fromJson(jsonString, User.class);
        if (response == null || !Role.STUDENT.equals(response.getRole())) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Invalid student Id");
        }
        Bundle bundle = bundleDAO.getBundleById(req.getBundleId());
        if (bundle == null) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Invalid bundle Id");
        }
        if (bundle.getValidTill() != null && bundle.getValidTill() < System.currentTimeMillis()) {
            throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Validity is over for this AIO ");
        }
        BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getNonEndedBundleEnrolment(req.getUserId().toString(), req.getBundleId());
        if (bundleEnrolment != null && !EntityStatus.ENDED.equals(bundleEnrolment.getStatus())) {
            throw new VException(ErrorCode.BUNDLE_ENROLLMENT_ALREADY_EXIST, "BUNDLE_ENROLLMENT_ALREADY_EXIST");
        }
        BundleCreateEnrolmentReq bundleReq = new BundleCreateEnrolmentReq();
        bundleReq.setEntityId(req.getBundleId());
        bundleReq.setUserId(req.getUserId().toString());
        bundleReq.setState(EnrollmentState.FREE_PASS);
        bundleReq.setStatus(EntityStatus.ACTIVE);

        bundleEnrolment = createEnrolment(bundleReq);
        bundleEnrolment.setFreePassvalidTill(System.currentTimeMillis() + (Long.valueOf(DateTimeUtils.MILLIS_PER_DAY) * req.getFreePassValidDays()));
        bundleEnrolmentDAO.save(bundleEnrolment, req.getCallingUserId());
        // todo enable for gamification
        // bundleEnrolmentManager.processBundleEnrolment(bundleEnrolment);
        autoEnrollAsyncTask(req.getBundleId(), req.getUserId().toString(), bundleEnrolment.getId());
    }

    public void autoEnrollAsyncTask(String bundleId, String userId, String enrollmentId) throws VException {
        Map<String, Object> bp = new HashMap<>();
        bp.put("userId", userId);
        bp.put("entityId", bundleId);
        bp.put("enrollmentId", enrollmentId);
        AsyncTaskParams bptask = new AsyncTaskParams(AsyncTaskName.BUNDLE_AUTO_ENROLL, bp);
        asyncTaskFactory.executeTask(bptask);
    }

    public void endBundleEnrollmentCron() throws VException {
        List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO.getBundleEnrollmentsToBeEnded();

        if (ArrayUtils.isNotEmpty(bundleEnrolments)) {

            for (BundleEnrolment bundleEnrolment : bundleEnrolments) {

                Long currentMillis = System.currentTimeMillis();
                StatusChangeTime statusChangeTime = new StatusChangeTime();
                statusChangeTime.setChangeTime(currentMillis);
                statusChangeTime.setNewStatus(EntityStatus.ENDED);
                statusChangeTime.setPreviousStatus(EntityStatus.ACTIVE);
                bundleEnrolmentDAO.endBundleEnrolmentByCron(statusChangeTime, bundleEnrolment.getId());
            }
        }
    }

    //	public OrderInfo enrollWithCashAndCheque(FreePassReq req) throws VException {
//		Bundle bundle = bundleDAO.getBundleById(req.getBundleId());
//
//		OrderItem orderItem = new OrderItem();
//		if (bundle == null) {
//			throw new VException(ErrorCode.BUNDLE_NOT_FOUND, "invalid bundle (All in one ) ID " + req.getBundleId());
//		}
//		Query query = new Query();
//		query.addCriteria(Criteria.where(BundleEnrolment.Constants.ID).is(bundle.getId()));
//		query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).ne(EntityStatus.ENDED));
//		query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(req.getUserId().toString()));
//		query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.REGULAR));
//		List<BundleEnrolment> enrollments = bundleEnrolmentDAO.runQuery(query, BundleEnrolment.class);
//
//		if (!enrollments.isEmpty()) {
//			throw new VException(ErrorCode.BUNDLE_ENROLLMENT_ALREADY_EXIST,
//					"Bundle enrollment already exist  " + req.getBundleId());
//		}
//		if (req.getCoupenId() != null && req.getCoupenId() != "") {
//			ValidateCouponReq couponReq = new ValidateCouponReq();
//			couponReq.setCode(req.getCoupenId());
//			couponReq.setEntityId(req.getBundleId());
//			couponReq.setEntityType(EntityType.BUNDLE);
//			couponReq.setTotalAmount(bundle.getPrice());
//			ValidateCouponRes couponRes = checkValidity(couponReq);
//			if (bundle.getPrice() > couponRes.getDiscount() + req.getAmount()) {
//				throw new VException(ErrorCode.INVALID_AMOUNT,
//						"Not enough ammount it should be (in Paisa)  " + bundle.getPrice() + " Discount : -  "
//								+ couponRes.getDiscount() + " you added  " + req.getAmount());
//			}
//			orderItem.setVedantuDiscountCouponId(couponRes.getCouponId());
//		} else {
//			if (bundle.getPrice() > req.getAmount()) {
//				throw new VException(ErrorCode.INVALID_AMOUNT, "Not enough ammount it should be  " + bundle.getPrice());
//			}
//		}
//		SubmitCashChequeReq submitCashChequeReq = new SubmitCashChequeReq();
//		submitCashChequeReq.setUserId(req.getUserId());
//		submitCashChequeReq.setAmount(req.getAmount());
//		submitCashChequeReq.setReasonRefNo(req.getRefNo());
//		submitCashChequeReq.setReasonNoteType(req.getReasonNoteType());
//		submitCashChequeReq.setReasonNote(req.getReasonNote());
//		submitCashCheque(submitCashChequeReq);
//
//		BuyItemsReq buyItemsReq = new BuyItemsReq();
//		List<OrderItem> orderItems = new ArrayList<OrderItem>();
//
//		orderItem.setEntityId(req.getBundleId());
//		orderItem.setEntityType(EntityType.BUNDLE);
//
//		orderItems.add(orderItem);
//		buyItemsReq.setCallingUserId(req.getUserId());
//		buyItemsReq.setCallingUserRole(Role.STUDENT);
//		buyItemsReq.setPaymentType(PaymentType.BULK);
//		buyItemsReq.setUseAccountBalance(true);
//		buyItemsReq.setItems(orderItems);
//		String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
//		ClientResponse resp = WebUtils.INSTANCE.doCall(platformEndpoint + "/payment/buyItems", HttpMethod.POST,
//				new Gson().toJson(buyItemsReq));
//		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//		String json2 = resp.getEntity(String.class);
//		OrderInfo orderInfo = gson.fromJson(json2, OrderInfo.class);
//		autoEnrollAsyncTask(req.getBundleId(), req.getUserId().toString());
//		return orderInfo;
//	}
    public ValidateCouponRes checkValidity(ValidateCouponReq req) throws VException {
        logger.info("ENTRY: " + req);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/coupons/checkValidity", HttpMethod.POST,
                new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT:" + jsonString);
        ValidateCouponRes res = new Gson().fromJson(jsonString, ValidateCouponRes.class);
        return res;
    }

    public void submitCashCheque(SubmitCashChequeReq submitCashChequeReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("Entering " + submitCashChequeReq.toString());
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/account/submitCashCheque", HttpMethod.POST,
                new Gson().toJson(submitCashChequeReq));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

    }

    public void setUnlimitedDoubtsBundle(String bundleId, Boolean unlimiteDoubts, Long callingUserId) throws ConflictException {

        GetBundleEnrolmentsReq req = new GetBundleEnrolmentsReq();
        req.setBundleId(bundleId);
        List<BundleEnrolment> enrollments = bundleEnrolmentDAO.getBundleEnrolmentsFromReq(req);
        if (ArrayUtils.isNotEmpty(enrollments)) {
            for (BundleEnrolment enrolment : enrollments) {
                enrolment.setUnlimitedDoubts(unlimiteDoubts);
                bundleEnrolmentDAO.save(enrolment, callingUserId);
            }
        }

    }

    public void trackToAioDataMigration(FreePassReq req) throws VException {
        GetBundlesReq bundleReq = new GetBundlesReq();
        Boolean flag = true;
        int start = 0;
//int counter =0 ;
        List<Bundle> bundles = new ArrayList<Bundle>();
        while (flag) {
            bundleReq.setStart(start);
            bundleReq.setSize(20);

            bundles = bundleDAO.getBundlesdetail(bundleReq);

            if (!ArrayUtils.isEmpty(bundles)) {
                for (Bundle bundle : bundles) {
                    List<String> courses = bundle.getCourses();
                    List<AioPackage> aioPackages = new ArrayList<AioPackage>();
                    logger.info("\n id :- " + bundle.getId());
                    if (ArrayUtils.isEmpty(bundle.getPackages())) {
                        if (courses != null) {
                            for (String courseDetails : courses) {
                                if (!StringUtils.isEmpty(courseDetails) && courseDetails.contains("autoenroll")
                                        && courseDetails.contains("BATCH_BUNDLE/")) {
                                    int i = courseDetails.indexOf("BATCH_BUNDLE/") + 13;
                                    String otmbundleOfBatchesId = courseDetails.substring(i, i + 24);
                                    //OTFBundleInfo oTFBundleInfo = otfBundleManager.getOTFBundle(otmbundleOfBatchesId);

                                    int gradeId = Integer.parseInt(courseDetails.substring(5, 8).replaceAll("[^0-9]", ""));
                                    AioPackage aioPackage = new AioPackage();
                                    aioPackage.setEnrollmentType(EnrollmentType.AUTO_ENROLL);
                                    aioPackage.setPackageType(PackageType.OTM_BUNDLE);
                                    aioPackage.setEntityId(otmbundleOfBatchesId);
                                    aioPackage.setGrade(gradeId);
                                    aioPackages.add(aioPackage);
                                } else if (!StringUtils.isEmpty(courseDetails) && courseDetails.contains("BATCH_BUNDLE/")) {
                                    int i = courseDetails.indexOf("BATCH_BUNDLE/") + 13;
                                    String otmbundleOfBatchesId = courseDetails.substring(i, i + 24);

                                    int gradeId = Integer.parseInt(courseDetails.substring(5, 8).replaceAll("[^0-9]", ""));
                                    logger.info(courseDetails.substring(5, 8) + "    ");
                                    //OTFBundleInfo oTFBundleInfo = otfBundleManager.getOTFBundle(otmbundleOfBatchesId);
                                    AioPackage aioPackage = new AioPackage();
                                    aioPackage.setEnrollmentType(EnrollmentType.LOCKED);
                                    aioPackage.setPackageType(PackageType.OTM_BUNDLE);
                                    aioPackage.setEntityId(otmbundleOfBatchesId);
                                    aioPackage.setGrade(gradeId);
                                    aioPackages.add(aioPackage);
                                }
                            }
                            logger.info("\n course update for id :- " + bundle.getId());
                        }
                        if (bundle.getVideos() != null && bundle.getVideos().getChildren() != null) {
                            logger.info("\n video update for id :- " + bundle.getId());
                            for (VideoContentData videoContent : bundle.getVideos().getChildren()) {
                                if (videoContent != null && videoContent.getChildren() != null) {

                                    videoContent.setValidFrom(1554100200000L);
                                    videoContent.setValidTill(1585636200000L);

                                }
                                if (videoContent.getTitle() != null) {
                                    videoContent.setSubject(videoContent.getTitle());
                                }
                            }
                        }
                        if (bundle.getTests() != null && bundle.getTests().getChildren() != null) {
                            logger.info("\n test update for id :- " + bundle.getId());
                            for (TestContentData testContent : bundle.getTests().getChildren()) {
                                if (testContent != null) {
                                    testContent.setValidFrom(1554100200000L);
                                    testContent.setValidTill(1585636200000L);
                                }
                                if (testContent.getTitle() != null) {
                                    testContent.setSubject(testContent.getTitle());
                                }
                            }
                        }
                        bundle.setTabletIncluded(false);
                        bundle.setValidTill(1585636200000L);
                        bundle.setUnLimitedDoubtsIncluded(true);
                        bundle.setAmIncluded(true);
                        bundle.setPackages(aioPackages);
                        bundleDAO.save(bundle, Long.getLong(bundle.getLastUpdatedBy()));
                        logger.info("\n bundle save id :- " + bundle.getId());
                    }
                }
                start += 20;
//				counter +=5
            } else {
                flag = false;
            }
        }
    }
//    db.BundleEnrolment.update({}, {
//        $set: {
//            "validTill": NumberLong("1585636200000"),
//                    "unlimitedDoubts": true
//        }
//    },true,true)

    public BundleTestRes getTestPostLogin(GetBundleTestVideoReq getBundleTestVideoReq) throws VException {
        List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO.getUsersActiveAndInactiveEnrollment(getBundleTestVideoReq.getUserId());
        TestContentData testContentData = new TestContentData();
        Map<String, Map<String, TestBundleContentInfo>> subtopicTestMap = new HashMap<>();
        Long currentMillies = System.currentTimeMillis();
        Map<String, EntityStatus> bundleIdStatus = new HashMap<>();
        Map<String, Long> bundleIdLastUpdated = new HashMap<>();
        List<String> bundleIds = new ArrayList<>();
        for (BundleEnrolment bundleEnrolment : bundleEnrolments) {
            bundleIds.add(bundleEnrolment.getBundleId());
            bundleIdStatus.put(bundleEnrolment.getBundleId(), bundleEnrolment.getStatus());
            bundleIdLastUpdated.put(bundleEnrolment.getBundleId(), bundleEnrolment.getLastUpdated());
        }
        getBundleTestVideoReq.setBundleIds(bundleIds);
        BundleTestRes bundleTestRes = new BundleTestRes();
        List<BundleTestFilterRes> getTestDetailsByBundles = bundleTestManager.getTestDetailsByBundles(getBundleTestVideoReq);
        TestContentData children = new TestContentData();
        Boolean isLocked;
        Boolean isActive;
        for (BundleTestFilterRes bundleTestFilterRes : getTestDetailsByBundles) {
            String bundleId = bundleTestFilterRes.getId();
            EntityStatus entityStatus = bundleIdStatus.get(bundleId);
            //Long lastUpdated=bundleIdLastUpdated.get(bundleId);
            TestContentData subject = bundleTestFilterRes.getTests().getChildren();
            if (subject.getChildren() != null) {
                isLocked = false;
                isActive = false;
                if (!(subject.getValidTill() != null && currentMillies > subject.getValidTill())) {
                    if (subject.getValidFrom() != null && currentMillies < subject.getValidFrom()) {
                        isLocked = true;
                    }
                    if (EntityStatus.ACTIVE.equals(entityStatus)) {
                        isActive = true;
                    }
                    for (TestContentData topic : subject.getChildren()) {
                        String topicKey = topic.getTitle();
                        topic.setBundleId(bundleId);
                        if (subtopicTestMap.containsKey(topicKey)) {
                            Map<String, TestBundleContentInfo> contentData = subtopicTestMap.get(topicKey);
                            if (topic.getContents() != null && !topic.getContents().isEmpty()) {
                                for (TestBundleContentInfo testBundleContentInfo : topic.getContents()) {
                                    if (contentData.containsKey(testBundleContentInfo.getContentId())) {
                                        if (!contentData.get(testBundleContentInfo.getContentId()).getIsActive()) {
                                            testBundleContentInfo.setIsActive(isActive);
                                        } else {
                                            testBundleContentInfo.setIsActive(true);
                                        }
                                        if (!contentData.get(testBundleContentInfo.getContentId()).isLocked()) {
                                            testBundleContentInfo.setLocked(isLocked);
                                        }
                                    } else {
                                        testBundleContentInfo.setIsActive(isActive);
                                        testBundleContentInfo.setLocked(isLocked);
                                    }
                                    contentData.put(testBundleContentInfo.getContentId(), testBundleContentInfo);
                                }
                            }
                        } else {
                            if (children.getChildren() == null) {
                                List<TestContentData> child = new ArrayList<>();
                                child.add(topic);
                                children.setChildren(child);
                            } else {
                                children.getChildren().add(topic);
                            }
                            Map<String, TestBundleContentInfo> contentData = new HashMap<>();
                            if (topic.getContents() != null && !topic.getContents().isEmpty()) {
                                for (TestBundleContentInfo testBundleContentInfo : topic.getContents()) {
                                    testBundleContentInfo.setIsActive(isActive);
                                    testBundleContentInfo.setLocked(isLocked);
                                    contentData.put(testBundleContentInfo.getContentId(), testBundleContentInfo);
                                }
                            }
                            subtopicTestMap.put(topicKey, contentData);
                        }
                    }
                }
            }
        }
        if (children.getChildren() != null) {
            for (TestContentData topicContetnt : children.getChildren()) {
                String topicKey = topicContetnt.getTitle();
                topicContetnt.setContents(new ArrayList<>(subtopicTestMap.get(topicKey).values()));
            }
            testContentData.setChildren(children.getChildren());
            bundleTestRes.setChildren(testContentData);
            return bundleTestRes;

        } else {
            return null;
        }
    }

    public BundleVideoRes getVideoPostLogin(GetBundleTestVideoReq getBundleTestVideoReq) throws VException {
        List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO
                .getUsersActiveAndInactiveEnrollment(getBundleTestVideoReq.getUserId());
        VideoContentData videoContentData = new VideoContentData();
        Map<String, VideoContentData> subjectSubtopicMap = new HashMap<String, VideoContentData>();
        Map<String, Map<String, VideoBundleContentInfo>> subtopicTestMap = new HashMap<String, Map<String, VideoBundleContentInfo>>();
        Long currentMillies = System.currentTimeMillis();
        List<String> bundleIds = new ArrayList<String>();
        Map<String, EntityStatus> bundleIdStatus = new HashMap<>();
        Map<String, Long> bundleIdLastUpdated = new HashMap<>();
        for (BundleEnrolment bundleEnrolment : bundleEnrolments) {
            bundleIds.add(bundleEnrolment.getBundleId());
            bundleIdStatus.put(bundleEnrolment.getBundleId(), bundleEnrolment.getStatus());
            bundleIdLastUpdated.put(bundleEnrolment.getBundleId(), bundleEnrolment.getLastUpdated());
        }
        getBundleTestVideoReq.setBundleIds(bundleIds);
        BundleVideoRes bundleVideoRes = new BundleVideoRes();
        VideoContentData children = new VideoContentData();
        //VideoContentData videoByBundle = bundleVideoManager.getVideoByBundles(getBundleTestVideoReq);
        List<BundleVideoFilterRes> getVideoDetailsByBundles = bundleVideoManager.getVideoDetailsByBundles(getBundleTestVideoReq);
        Boolean isLocked;
        Boolean isActive;
        for (BundleVideoFilterRes bundleVideoFilterRes : getVideoDetailsByBundles) {
            String bundleId = bundleVideoFilterRes.getId();
            EntityStatus entityStatus = bundleIdStatus.get(bundleId);
            Long lastUpdated = bundleIdLastUpdated.get(bundleId);
            VideoContentData subject = bundleVideoFilterRes.getVideos().getChildren();
            if (subject.getChildren() != null) {
                if (!(subject.getValidTill() != null && currentMillies > subject.getValidTill())) {
                    isLocked = false;
                    isActive = false;
                    if (subject.getValidFrom() != null && currentMillies < subject.getValidFrom()) {
                        isLocked = true;
                    }
                    if (EntityStatus.ACTIVE.equals(entityStatus)) {
                        isActive = true;
                    }

                    for (VideoContentData topic : subject.getChildren()) {
                        String topicKey = topic.getTitle();
                        topic.setBundleId(bundleId);
                        if (subtopicTestMap.containsKey(topicKey)) {
                            Map<String, VideoBundleContentInfo> contentData = subtopicTestMap.get(topicKey);
                            if (topic.getContents() != null && !topic.getContents().isEmpty()) {
                                for (VideoBundleContentInfo testBundleContentInfo : topic.getContents()) {

                                    if (contentData.containsKey(testBundleContentInfo.getContentId())) {
                                        if (!contentData.get(testBundleContentInfo.getContentId()).getIsActive()) {
                                            testBundleContentInfo.setIsActive(isActive);
                                        } else {
                                            testBundleContentInfo.setIsActive(true);
                                        }
                                        if (contentData.get(testBundleContentInfo.getContentId()).isLocked()) {
                                            testBundleContentInfo.setLocked(isLocked);
                                        }
                                    } else {
                                        testBundleContentInfo.setIsActive(isActive);
                                        testBundleContentInfo.setLocked(isLocked);
                                    }
                                    contentData.put(testBundleContentInfo.getContentId(), testBundleContentInfo);
                                }
                            }
                        } else {
                            if (children.getChildren() == null) {
                                List<VideoContentData> child = new ArrayList<VideoContentData>();
                                child.add(topic);
                                children.setChildren(child);
                            } else {
                                children.getChildren().add(topic);
                            }
                            Map<String, VideoBundleContentInfo> contentData = new HashMap<String, VideoBundleContentInfo>();
                            if (topic.getContents() != null && !topic.getContents().isEmpty()) {
                                for (VideoBundleContentInfo testBundleContentInfo : topic.getContents()) {
                                    testBundleContentInfo.setIsActive(isActive);
                                    testBundleContentInfo.setLocked(isLocked);
                                    contentData.put(testBundleContentInfo.getContentId(), testBundleContentInfo);
                                }
                            }
                            subtopicTestMap.put(topicKey, contentData);
                        }
                    }
                }
            }
        }

        if (children.getChildren() != null) {
            for (VideoContentData topicContetnt : children.getChildren()) {
                String topicKey = topicContetnt.getTitle();
                topicContetnt
                        .setContents(new ArrayList<VideoBundleContentInfo>(subtopicTestMap.get(topicKey).values()));
            }
            videoContentData.setChildren(children.getChildren());
            bundleVideoRes.setChildren(videoContentData);
            return bundleVideoRes;
        } else {
            return null;
        }

    }

    private void validateBatchAndCourseConflict(AddEditBundleReq addEditBundleReq) throws VException {
        /*
         * Validation for taking care of no duplicate batches
         */
        List<AioPackage> aioPackages = addEditBundleReq.getPackages(); // get all the AioPackages
        String bundleOrgId = addEditBundleReq.getOrgId();
        Boolean orgIdExists = !(StringUtils.isEmpty(bundleOrgId));

        Set<String> batchIds = new HashSet<String>(); // initialize this variable for batch id checking
        for (AioPackage aioPackage : aioPackages) { // Loop through all the present AIOs in the request data
            String entityId = aioPackage.getEntityId();
            PackageType packageType = aioPackage.getPackageType();
            if (PackageType.OTF.equals(packageType)) { // If the AIO is an OTF then
                Batch batch = batchDAO.getById(entityId);

                if (batch == null) {
                    throw new BadRequestException(ErrorCode.INVALID_CONTENT_TYPE,
                            entityId + " : No batch exist with this id", Level.FATAL);
                }
                if (batchIds.contains(entityId)) { // invalidate if the batch is already present
                    throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,
                            entityId + " : Duplicate batch numbers are not permitted", Level.FATAL);
                }

                if(!orgIdExists && !(StringUtils.isEmpty(batch.getOrgId()))){
                    // Bundle orgId does not exist but batch orgId exists
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Adding batch with orgId for a bundle which does not have orgId");
                }

                if(orgIdExists){
                    // Bundle orgId exists
                    if(!StringUtils.isEmpty(batch.getOrgId())){
                        // Batch orgId exists
                        if(!(bundleOrgId.equals(batch.getOrgId()))) {
                            // Bundle orgId and batch orgId different, throw exception
                            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "For batch " + entityId + " orgId should be same as bundle orgId");
                        }
                    }
                    else{
                        // Batch orgId does not exist, throw exception
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"For batch " + entityId + " orgId is empty");
                    }
                }

                batchIds.add(entityId); // add all the valid batch id


            } else if (PackageType.OTM_BUNDLE.equals(packageType)) { // else if it is an OTM bundle
                OTFBundleInfo otfBundleInfo = new OTFBundleInfo(); // initialize bundle info
                otfBundleInfo = otfBundleManager.getOTFBundle(entityId); // Get all the info for that OTM bundle
                if (otfBundleInfo == null) {
                    throw new BadRequestException(ErrorCode.INVALID_CONTENT_TYPE,
                            entityId + " : No OTM bundle exist for this id", Level.FATAL);
                }
                if (otfBundleInfo == null || ArrayUtils.isEmpty(otfBundleInfo.getEntities())) { // Validate if the
                    // fetched bundle is
                    // correct
                    throw new BadRequestException(ErrorCode.OTF_INVALID_HANDLE_RECORD_REQ,
                            "OTFBundle can't be null or empty entities are not allowed inside an OTF bundle",
                            Level.FATAL);
                }
                List<OTMBundleEntityInfo> entities = otfBundleInfo.getEntities(); // get all the entities
                for (OTMBundleEntityInfo entity : entities) { // loop through all the entities
                    String currentEntityId = entity.getEntityId();
                    if (EntityType.OTF.equals(entity.getEntityType()) && !StringUtils.isEmpty(currentEntityId)) { // validate
                        // the
                        // entity
                        if (batchIds.contains(currentEntityId)) { // invalidate if the batch id is already present
                            throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,
                                    entityId + " : Duplicate batch numbers are not permitted", Level.FATAL);
                        }
                        batchIds.add(currentEntityId); // add all the valid batch id
                    } /*
                     * Validation to ensure that no bundle of courses are added
                     */ else {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                                entityId + " : is not a bundle of batch or a batch, so it is not permitted",
                                Level.FATAL);
                    }
                }
            }
        }

        /*
         * Validation to ensure that there is no duplicate course
         */
        List<Batch> batches = batchDAO.getBatchByIds(new ArrayList<>(batchIds));
        Set<String> courseIds = new HashSet<String>();
        for (Batch batch : batches) {
            String courseId = batch.getCourseId();
            if (courseIds.contains(courseId)) {
                throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,
                        courseId + " : Duplicate courses are not permitted", Level.INFO);
            }
            courseIds.add(courseId);


        }
    }

    private void validateInstallment(AddEditBundleReq addEditBundleReq) throws BadRequestException {
        if (ArrayUtils.isNotEmpty(addEditBundleReq.getInstalmentDetails())) {
            int totalPrice = addEditBundleReq.getPrice();
            int instalmentSum = 0;
            List<BaseInstalmentInfo> installments = addEditBundleReq.getInstalmentDetails();
            for (BaseInstalmentInfo installment : installments) {
                if (installment.getMinimumAmount() != null && installment.getAmount() != null && installment.getAmount() >= 0 && installment.getMinimumAmount() >= 0 && installment.getAmount() < installment.getMinimumAmount()) {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "installments amount can't be less than the minimum installment amount", Level.ERROR);
                }
                instalmentSum += installment.getAmount();
            }
            logger.info("Total price is " + totalPrice);
            logger.info("Total installment sum is " + instalmentSum);
            if (instalmentSum < totalPrice) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "Sum of installments can't be less than total price", Level.FATAL);
            }
        }

    }

    public List<BatchDashboardInfo> getCourseByBundle(GetBundleTestVideoReq req) throws Throwable {
        BundleInfo bundle = getCachedBundleBasicInfo(req.getBundleId());
        List<String> batchIds = new ArrayList<>();
        HashMap<String, BundleEntity> bundleEntityMap = new HashMap<>();

        if(Objects.nonNull(bundle)) {
            List<BundleEntity> bundleEntities = bundleEntityDAO.getNonUpsellEntitiesForBundle(req.getBundleId(), Objects.isNull(req.getStart()) ? 0 : req.getStart(), req.getSize());
            if (ArrayUtils.isNotEmpty(bundleEntities)) {
                for (BundleEntity bundleEntity : bundleEntities) {
                    if (!EnrollmentType.UPSELL.equals(bundleEntity.getEnrollmentType())) {
                        if (req.getGrade() == null || req.getGrade().equals(bundleEntity.getGrade())) {
                            if (PackageType.OTM_BUNDLE.equals(bundleEntity.getPackageType())) {
                                OTFBundleInfo oTFBundleInfo = otfBundleManager.getOTFBundle(bundleEntity.getEntityId());
                                for (OTMBundleEntityInfo entity : oTFBundleInfo.getEntities()) {
                                    if (EntityType.OTF.equals(entity.getEntityType())
                                            && !StringUtils.isEmpty(entity.getEntityId())) {
                                        batchIds.add(entity.getEntityId());
                                        bundleEntityMap.put(entity.getEntityId(), bundleEntity);
                                    }
                                }
                            } else {
                                batchIds.add(bundleEntity.getEntityId());
                                bundleEntityMap.put(bundleEntity.getEntityId(), bundleEntity);
                            }
                        }
                    }
                }
            }
        }
        GetBatchesForDashboardReq batchReq = new GetBatchesForDashboardReq();
        batchReq.setBatchIds(batchIds);
        List<BatchDashboardInfo> batchDashboardInfos = dashboardManager.getBatches(batchReq);
        if (!ArrayUtils.isEmpty(batchDashboardInfos)) {
            for (BatchDashboardInfo batchDashboardInfo : batchDashboardInfos) {
                BundleEntity bundleEntity = bundleEntityMap.get(batchDashboardInfo.getBatchId());
                BundleEntityInfo bundleEntityInfo=Objects.isNull(bundleEntity)?null:mapper.map(bundleEntity,BundleEntityInfo.class);
                batchDashboardInfo.setAioPackage(bundleEntityInfo);
            }
        }
        return batchDashboardInfos;
    }

    public void migrateEnrollmentData() {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.CREATION_TIME).gt(1515734325000L));

        List<BundleEnrolment> enrollments = bundleEnrolmentDAO.runQuery(query, BundleEnrolment.class);

        for (BundleEnrolment enrollment : enrollments) {
            Query enrollmentQuery = new Query();
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).is(enrollment.getBundleId()));
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(enrollment.getUserId()));
            Update update = new Update();
            update.set(Enrollment.Constants.PURCHASE_ENROLLMENT_ID, enrollment.getId());
            update.set(Enrollment.Constants.LAST_UPDATED, System.currentTimeMillis());
            update.set(Enrollment.Constants.LAST_UPDATED_BY, "SYSTEM");
            enrollmentDAO.updateMultiple(enrollmentQuery, update);

        }
        Query _query = new Query();
        _query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ENDED));
        _query.addCriteria(Criteria.where(BundleEnrolment.Constants.CREATION_TIME).gt(1515734325000L));

        List<BundleEnrolment> endEnrollments = bundleEnrolmentDAO.runQuery(_query, BundleEnrolment.class);

        for (BundleEnrolment enrollment : endEnrollments) {
//			GroupOperation contentCount = Aggregation.group(Enrollment.Constants.USER_ID, Enrollment.Constants.PURCHASE_CONTEXT_ID,
//					Enrollment.Constants.STATUS).count().as("count");
//
//			Aggregation agg = Aggregation.newAggregation(Aggregation.match(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ENDED)), Aggregation.match(Criteria.where(Enrollment.Constants.USER_ID).is(enrollment.getUserId())),Aggregation.match(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).is(enrollment.getBundleId())),contentCount, );

            Query enrollmentQuery = new Query();
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ENDED));
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_CONTEXT_ID).is(enrollment.getBundleId()));
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(enrollment.getUserId()));
            //enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.CREATION_TIME).lt(enrollment.getCreationTime() + 500).gt(enrollment.getCreationTime() - 500));
            List<Enrollment> enrollmentList = enrollmentDAO.runQuery(enrollmentQuery, Enrollment.class);
            Boolean flag = false;
            List<String> batches = new ArrayList<>();
            for (Enrollment enrollment1 : enrollmentList) {
                if (batches.contains(enrollment1.getBatchId())) {
                    flag = true;
                } else {
                    batches.add(enrollment1.getBatchId());
                }
            }
            if (flag) {
                enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.CREATION_TIME).lt(enrollment.getCreationTime() + 500).gt(enrollment.getCreationTime() - 500));
            }
            Update update = new Update();
            update.set(Enrollment.Constants.PURCHASE_ENROLLMENT_ID, enrollment.getId());
            update.set(Enrollment.Constants.LAST_UPDATED, System.currentTimeMillis());
            update.set(Enrollment.Constants.LAST_UPDATED_BY, "SYSTEM");
            enrollmentDAO.updateMultiple(enrollmentQuery, update);
        }
    }


    public void migrateBatchEnrollments(BundleEnrolment oldEnrollment, BundleEnrolment newEnrollment) throws VException {

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.PURCHASE_ENROLLMENT_ID).is(oldEnrollment.getId()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        Update update = new Update();
        update.set(Enrollment.Constants.PURCHASE_ENROLLMENT_ID, newEnrollment.getId());
        update.set(Enrollment.Constants.PURCHASE_CONTEXT_ID, newEnrollment.getBundleId());

        ParentEntityChangeTime parentEntityChangeTimes = new ParentEntityChangeTime();
        parentEntityChangeTimes.setChangeTime(System.currentTimeMillis());
        parentEntityChangeTimes.setNewBundleEnrollmentId(newEnrollment.getId());
        parentEntityChangeTimes.setOldBundleEnrollmentId(oldEnrollment.getId());


        update.addToSet(Enrollment.Constants.PARENT_ENTITY_CHANGE_TIME, parentEntityChangeTimes);

//        if(oldEnrollment.getStatus().equals(EntityStatus.INACTIVE)){
//            update.set(Enrollment.Constants.STATE, EntityStatus.ACTIVE);
//            StatusChangeTime statusChangeTime = new StatusChangeTime();
//            statusChangeTime.setChangeTime(System.currentTimeMillis());
//            statusChangeTime.setNewStatus(EntityStatus.ACTIVE);
//            statusChangeTime.setPreviousStatus(EntityStatus.INACTIVE);
//            update.addToSet(Enrollment.Constants.STATUS_CHANGE_TIME, statusChangeTime);
//        }

        update.set(Enrollment.Constants.LAST_UPDATED, System.currentTimeMillis());


        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        List<String> allBatchIds = Optional.ofNullable(enrollments)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(Enrollment::getBatchId)
                .collect(Collectors.toList());
        List<String> continueBatchIds = bundleEntityDAO.getBatchIdsForBundle(allBatchIds, newEnrollment.getBundleId());


        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(continueBatchIds));
        enrollmentDAO.updateMultiple(query, update);


        // End all the batch enrollment which are not for continue batch enrollments
        Optional.ofNullable(enrollments)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .filter(enrollment -> !continueBatchIds.contains(enrollment.getBatchId()))
                .forEach(enrollment -> triggerMarkEnrollmentStatus(enrollment, EntityStatus.ENDED,"SUBSCRIPTION_TRIAL_ENDED"));


        if (EntityStatus.INACTIVE.equals(oldEnrollment.getStatus())) {
            // Active all the batch enrollment which in for continue batch enrollments
            Optional.ofNullable(enrollments)
                    .map(Collection::stream)
                    .orElseGet(Stream::empty)
                    .filter(enrollment -> continueBatchIds.contains(enrollment.getBatchId()))
                    .forEach(enrollment -> triggerMarkEnrollmentStatus(enrollment, EntityStatus.ACTIVE,""));

        }

        List<Enrollment> toBeUpdatedEnrollments;
        if(ArrayUtils.isNotEmpty(enrollments)) {
            toBeUpdatedEnrollments = enrollments.stream().filter(e -> continueBatchIds.contains(e.getBatchId())).collect(Collectors.toList());
            handleEnrollmentChanges(toBeUpdatedEnrollments, newEnrollment.getUserId());
        }
    }

    public void triggerMarkEnrollmentStatus(Enrollment enrollment, EntityStatus status,String reason) {
        OTFEnrollmentReq oTFEnrollmentReq = mapper.map(enrollment, OTFEnrollmentReq.class);
        oTFEnrollmentReq.setStatus(status.name());
        if(!StringUtils.isEmpty(reason)) {
            oTFEnrollmentReq.setEndReason(reason);
        }
        sqsManager.sendToSQS(SQSQueue.BATCH_ENROLL_STATUS_QUEUE, SQSMessageType.MARK_ENROLLMENT_STATUS, gson.toJson(oTFEnrollmentReq));
    }

    public void handleEnrollmentChanges(List<Enrollment> toBeUpdatedEnrollments,String userId) throws BadRequestException {
        List<String> enrollmentIds = toBeUpdatedEnrollments.stream().
                filter(e->!(e.getStatus().equals(EntityStatus.ENDED))).
                map(AbstractMongoStringIdEntity::getId).collect(Collectors.toList());

        SectionEnrollmentReq _r = new SectionEnrollmentReq();
        int limit = 100;
        int size = enrollmentIds.size();

        for(int i=0; i<size; i+=limit) {
            List<String> subList;
            if(size > i+limit) {
                subList = enrollmentIds.subList(i, (i + limit));
            }
            else {
                subList = enrollmentIds.subList(i, size);
            }

            _r.setEnrollmentIds(subList);
            _r.setUserId(userId);
            handleSectionChange(_r);
            //sqsManager.sendToSQS(SQSQueue.SECTION_QUEUE, SQSMessageType.POST_ENROLLMENT_CHANGES_TASK, gson.toJson(_r));
        }
    }

    public void handleSectionChange(SectionEnrollmentReq req) throws BadRequestException {
        List<String> enrollmentIds = req.getEnrollmentIds();
        String userId = req.getUserId();

        if(ArrayUtils.isEmpty(enrollmentIds) || null == userId)
            return;

        // 1 -- fetch all enrollments
        List<String> includeFields = Arrays.asList(Enrollment.Constants.ID,Enrollment.Constants.BATCH_ID, Enrollment.Constants.USER_ID,Enrollment.Constants.SECTION_ID);
        List<Enrollment> enrollments = enrollmentDAO.getByIdsWithProjection(enrollmentIds,includeFields,0,100);

        // 2 -- get batches data
        List<String> batchIds = enrollments.stream().map(Enrollment::getBatchId).collect(Collectors.toList());
        List<Batch> batches = batchDAO.getBatchByIds(batchIds,Arrays.asList(Batch.Constants._ID,Batch.Constants.HAS_SECTIONS),null);
        Map<String,Boolean> batchSectionExistsMap = new HashMap<>();
        for(Batch b:batches){
            batchSectionExistsMap.put(b.getId(),b.getHasSections());
        }

        // 3 --  sectionState will always be regular as this is triggered only for REGULAR bundle enrollments
        EnrollmentState sectionState = EnrollmentState.REGULAR;

        // 4 -- assign/update sectionIds one by one
        UpdateSectionEnrollmentReq _req =  new UpdateSectionEnrollmentReq();
        List<String> failedBatchIds = new ArrayList<>();

        for(Enrollment e:enrollments){
            if(batchSectionExistsMap.containsKey(e.getBatchId()) && batchSectionExistsMap.get(e.getBatchId()).equals(Boolean.TRUE)){
                //Long seatsCount = enrollmentManager.getAndDecrementSeatsCount(e.getBatchId(), sectionState);
                Section vacantSection = sectionDAO.getVacantSection(e.getBatchId(),sectionState);
                if(Objects.isNull(vacantSection)) {
                    failedBatchIds.add(e.getBatchId());
                    continue;
                }
                sectionManager.adjustSeatsVacancyForSections(e.getSectionId(),null);
                Long seatsCount = vacantSection.getSeatsVacant();
//                if(seatsCount == null){
//                    BatchSectionReq batchSectionReq = new BatchSectionReq();
//                    batchSectionReq.setBatchIds(Arrays.asList(e.getBatchId()));
//                    sectionManager.validateSeatsInRedis(batchSectionReq);
//                    seatsCount = enrollmentManager.getAndDecrementSeatsCount(e.getBatchId(), sectionState);
//                }

                if(seatsCount != null && seatsCount > 0) {
                    _req.setBatchId(e.getBatchId());
                    _req.setUserId(e.getUserId());
                    _req.setEnrollment(e);
                    enrollmentDAO.setSectionToEnrollment(e.getId(),vacantSection.getId(),vacantSection.getSectionType());
//                    boolean status = sectionManager.changeSectionState(_req,sectionState);
//                    if(!status)
//                        failedBatchIds.add(e.getBatchId());
//                    sqsManager.sendToSQS(SQSQueue.SECTION_QUEUE,SQSMessageType.ASSIGN_STUDENT_ENROLLMENT_TO_SECTION,gson.toJson(_req),e.getBatchId());
                }
                else{
                    logger.error("Seats count is {}",seatsCount);
//                    failedBatchIds.add(e.getBatchId());
//                    // section redis keys validation
//                    BatchSectionReq batchSectionReq = new BatchSectionReq();
//                    batchSectionReq.setBatchIds(Arrays.asList(e.getBatchId()));
//                    sqsManager.sendToSQS(SQSQueue.SECTION_QUEUE,SQSMessageType.VALIDATE_SECTION_REDIS_KEYS,gson.toJson(batchSectionReq));
                }
            }
        }

        if(ArrayUtils.isNotEmpty(failedBatchIds)){
            // trigger enrollment failure alerts
            EnrollmentFailureReq enrollmentFailureReq = new EnrollmentFailureReq();
            enrollmentFailureReq.setUserId(userId);
            enrollmentFailureReq.setMessage_1("TRIAL to REGULAR");
            enrollmentFailureReq.setMessage_2("because either all REGULAR sections are full or no REGULAR sections exist");
            enrollmentFailureReq.setBatchIds(failedBatchIds);
            sqsManager.sendToSQS(SQSQueue.SECTION_NOTIFICATIONS_QUEUE,SQSMessageType.ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE,gson.toJson(enrollmentFailureReq));
        }
    }

    public String getRegularBundleId(ClickToEnrollReq req) {
        List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO.getRegularActiveEnrolments( req.getUserId(), 0, 20 );
        List<String> regularBundleIds = new ArrayList<>();
        if(ArrayUtils.isNotEmpty( bundleEnrolments )) {
            bundleEnrolments.forEach( (bundleEnrolment) -> {
                regularBundleIds.add( bundleEnrolment.getBundleId() );
            } );
            List<Bundle> bundles = bundleDAO.getBundlesByBatchIds(regularBundleIds, req.getBatchId(), Arrays.asList( Bundle.Constants._ID ), 0 , 20);
            if(ArrayUtils.isNotEmpty( bundles )) {
                return bundles.get( 0 ).getId();
            }
        }
        return  null;
    }

    public void clickToEnroll(ClickToEnrollReq req) throws VException {


        // get user Id from session
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())){
            String userId = String.valueOf(sessionUtils.getCallingUserId());
            if(null != userId && !userId.equals(req.getUserId())){
                return;
            }

        }
        List<BundleEntity> bundleEntities= bundleEntityDAO.getAllBundleEntityByBundleIdAndEntityId(req.getBundleId(), req.getBatchId(),
                Arrays.asList(BundleEntity.Constants._ID,BundleEntity.Constants.SUBSCRIPTION_PACKAGES_TYPES));
        if (ArrayUtils.isEmpty(bundleEntities)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "no click to enroll batch is available for this AIO");
        }

        //enrolment check
        Batch batch = batchDAO.getById(req.getBatchId());

        // Batch status and visibility check before enrolling
        if(VisibilityState.INVISIBLE.equals(batch.getVisibilityState()) || EntityStatus.INACTIVE.equals(batch.getBatchState()))
            throw new BadRequestException(ErrorCode.ENROLMENT_NOT_ALLOWED, "Batch is not opened for enrolment");

        Course course = courseManager.getCourse(batch.getCourseId());

        BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getNonEndedBundleEnrolment(req.getUserId(),
                req.getBundleId());

        if (bundleEnrolment == null || !EntityStatus.ACTIVE.equals(bundleEnrolment.getStatus())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "There is no active enrollment");
        }

        if(!EnrollmentState.REGULAR.equals(bundleEnrolment.getState())) {
            BundleEntity bundleEntity=bundleEntities.get(0);
            if(ArrayUtils.isNotEmpty(bundleEntity.getSubscriptionPackageTypes()) &&
                    bundleEntity.getSubscriptionPackageTypes().contains(SubscriptionPackageType.REGULAR) &&
                    !bundleEntity.getSubscriptionPackageTypes().contains(SubscriptionPackageType.TRIAL)) {
                throw new BadRequestException(ErrorCode.ENROLMENT_NOT_ALLOWED, "Can't enroll in regular batch");
            }
        }

        List<Enrollment> enrollments = enrollmentManager.getEnrollment(req.getUserId(), batch.getCourseId(), null);
        ShareContentEnrollmentReq shareContentEnrollmentReq = new ShareContentEnrollmentReq();

        if (ArrayUtils.isEmpty(enrollments)) {
            Enrollment oTFEnrollmentReq = new Enrollment(req.getUserId(), req.getBatchId(), null, null, Role.STUDENT,
                    EntityStatus.ACTIVE, null);
            oTFEnrollmentReq.setPurchaseContextType(EnrollmentPurchaseContext.BUNDLE);
            oTFEnrollmentReq.setPurchaseContextId(req.getBundleId());
            oTFEnrollmentReq.setPurchaseEnrollmentId(bundleEnrolment.getId());
            oTFEnrollmentReq.setPurchaseContextEnrollmentType(EnrollmentType.CLICK_TO_ENROLL);
            oTFEnrollmentReq.setState(EnrollmentState.REGULAR);
            oTFEnrollmentReq.setEntityTitle(course.getTitle());
            oTFEnrollmentReq.setEntityType(EntityType.OTF_COURSE);
            oTFEnrollmentReq.setEntityId(course.getId());

            logger.info(" create new Enrollment " + oTFEnrollmentReq);
            boolean r = enrollmentManager.updateEnrollment(oTFEnrollmentReq, Long.parseLong(req.getUserId()));

            logger.info("sharing curriculum");
            shareContentEnrollmentReq.setContextId(batch.getId());
            shareContentEnrollmentReq.setStudentId(req.getUserId());
            shareContentEnrollmentReq.setStartTime(null);

            logger.info("updateCalendar");
            enrollmentAsyncManager.updateCalendar(req.getUserId(), oTFEnrollmentReq.getBatchId(), EntityStatus.ACTIVE);
            logger.info("createGtt detail");
            // Map<String, Object> payload = new HashMap<String, Object>();
            // payload.put("enrollment", oTFEnrollmentReq);
            // asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.CREATE_GTT_ATTENDEES_NEW_ENROLLMENT, payload));
            sqsManager.sendToSQS(SQSQueue.BUNDLE_GTT_CREATION_OPS, SQSMessageType.CLICK_TO_ENROLL_GTT_CREATION,
                    gson.toJson(oTFEnrollmentReq), "NEW_ENROLL_GTT_CREATION" + req.getUserId());

        } else {
            Enrollment enrollment = enrollments.get(0);
            if (EntityStatus.INACTIVE.equals(enrollment.getStatus())) {
                logger.info("updateEnrollment " + enrollment);

                OTFEnrollmentReq oTFEnrollmentReq = mapper.map(enrollment, OTFEnrollmentReq.class);
                oTFEnrollmentReq.setStatus(EntityStatus.ACTIVE.name());
                logger.info("updateEnrollmentreq " + oTFEnrollmentReq);
                enrollment = enrollmentManager.markStatus(oTFEnrollmentReq);
                logger.info("enrollment after mark status " + enrollment);

                logger.info("sharing curriculum");
                Long changeTime = null;
                if (ArrayUtils.isNotEmpty(enrollment.getStatusChangeTime())) {
                    for (StatusChangeTime statuschangeTime : enrollment.getStatusChangeTime()) {
                        if (EntityStatus.INACTIVE.equals(statuschangeTime.getNewStatus())) {
                            changeTime = statuschangeTime.getChangeTime();
                        }
                    }
                }

                shareContentEnrollmentReq.setContextId(enrollment.getBatchId());
                shareContentEnrollmentReq.setStudentId(enrollment.getUserId());
                shareContentEnrollmentReq.setStartTime(changeTime);
                logger.info("updateCalendar");
                enrollmentAsyncManager.updateCalendar(req.getUserId(), oTFEnrollmentReq.getBatchId(),
                        EntityStatus.ACTIVE);
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "You are already enrolled in this course");
            }
        }

        logger.info("sharing curriculum for studentId : {}, batchId : {}",
                shareContentEnrollmentReq.getStudentId(), shareContentEnrollmentReq.getContextId());
        // trigger an sns here for all other events except auto enroll
        enrollmentAsyncManager.shareContentOnEnrollment(shareContentEnrollmentReq);

    }

    public List<BundleInfo> getBundlesNames(GetBundlesReq req) throws VException {
        Query query = new Query(Criteria.where("_id").in(req.getBundleIds()));
        query.fields().include(Bundle.Constants.TITLE);
        List<BundleInfo> response = new ArrayList<>();
        List<Bundle> bundles = bundleDAO.runQuery(query, Bundle.class);
        if (CollectionUtils.isNotEmpty(bundles)) {
            for (Bundle bundle : bundles) {
                BundleInfo bundleInfo = mapper.map(bundle, BundleInfo.class);
                response.add(bundleInfo);
            }
        }
        return response;
    }

    public List<Bundle> searchBundle(BundleSearchReq req) {
        final List<Bundle> bundles = bundleDAO.searchBundle(req);
        final Set<String> ids = bundles.stream().map(Bundle::getId).collect(Collectors.toSet());
        final Set<String> filteredIds = bundleEntityDAO.getFilteredBundleIdForMicroCourse(ids, req, false);
        return bundles.stream().filter(e -> filteredIds.contains(e.getId())).collect(Collectors.toList());
    }

    public PlatformBasicResponse updateAIOBundleTagsDetails(BundleDetailsUpdateReq bundleDetailsUpdateReq) throws IOException, BadRequestException {
        if (bundleDetailsUpdateReq.isEmptyDisplayTags()) {
            bundleDAO.emptyDisplayTagsForMicroCourse();
        }
        Map<String, BundleDetails> bundleDetails = bundleDetailsUpdateReq.getBundleDetails();
        Map<String, Map<String, BatchDetails>> bacthDetails = bundleDetailsUpdateReq.getBacthDetails();
        bundleDAO.updateBundleDetails(bundleDetails);

        for (String bundleId: bundleDetails.keySet()) {
            Map<String, BatchDetails> batchDetailsMap = bacthDetails.get(bundleId);

            List<BundleEntity> entities = bundleEntityDAO.getBundleEntityForEntityId(bundleId, batchDetailsMap.keySet(), PackageType.OTF);

            if (ArrayUtils.isNotEmpty(entities)) {
                for (String entityId : batchDetailsMap.keySet()) {
                    BatchDetails batchDetails = batchDetailsMap.get(entityId);

                    Optional<BundleEntity> optional = entities.stream().filter(e -> entityId.equals(e.getEntityId())).findFirst();
                    if (optional.isPresent()) {
                        BundleEntity entity = optional.get();

                        BatchDetails details = entity.getBatchDetails();

                        // DB entry is null so we save the object completly
                        if (details == null) {
                            entity.setBatchDetails(batchDetails);
                            bundleEntityDAO.save(entity);
                            continue;
                        }

                        long courseHours = batchDetails.getCoursePeriodMillis();
                        if (courseHours > 0) {
                            details.setCoursePeriodMillis(courseHours);
                        }

                        LinkedHashSet<Classes> courseSyllabus = batchDetails.getCourseSyllabus();
                        if (ArrayUtils.isNotEmpty(courseSyllabus)) {
                            details.setCourseSyllabus(courseSyllabus);
                        }

                        long courseStartTime = batchDetails.getCourseStartTime();
                        if (courseStartTime > 0) {
                            details.setCourseStartTime(courseStartTime);
                        }

                        CourseType courseType = batchDetails.getCourseType();
                        if (courseType != null) {
                            details.setCourseType(courseType);
                        }


                        Map<String, Object> teacherInfo = batchDetails.getTeacherInfo();
                        if (teacherInfo != null && teacherInfo.size() > 0) {
                            details.setTeacherInfo(teacherInfo);
                        }

                        bundleEntityDAO.save(entity);
                    }
                }
            }
        }

        return new PlatformBasicResponse();
    }

    public List<BundleAggregation> groupBundlesByDisplayTags(BundleSearchReq req) {
        List<Bundle> bundles = bundleDAO.groupBundlesByDisplayTags(req);
        final Set<String> ids = bundles.stream().map(Bundle::getId).collect(Collectors.toSet());
        final Set<String> filteredIds = bundleEntityDAO.getFilteredBundleIdForMicroCourse(ids, req, false);
        bundles = bundles.stream().filter(e -> filteredIds.contains(e.getId())).collect(Collectors.toList());


//		logger.info("BUNDLE-SIZE-COUNT: " + bundles.size());
        TreeMap<String, Integer> collectionOrder = new TreeMap<>(String::compareToIgnoreCase);
        for (Bundle bundle : bundles) {
            BundleDetails bundleDetails = bundle.getBundleDetails();
            if (bundleDetails != null) {
                Map<String, Integer> ranks = bundleDetails.getDisplayTagsRanks();
                if (ranks != null) {
                    collectionOrder.putAll(ranks);
                }
            }
        }

        Map<String, List<Bundle>> bundleAggregations = new TreeMap<>(String::compareToIgnoreCase);
        for (Bundle bundle : bundles) {
            BundleDetails details = bundle.getBundleDetails();
            if (details == null) {
                continue;
            }
            LinkedHashSet<String> displayTags = details.getDisplayTags();
            for (String displayTag : displayTags) {
                bundleAggregations.putIfAbsent(displayTag, new ArrayList<>());
                List<Bundle> list = bundleAggregations.get(displayTag);
                list.add(bundle);
            }
        }


        List<String> filteredBundles = bundleAggregations.values()
                .stream()
                .flatMap(Collection::stream)
                .map(Bundle :: getId)
                .collect(Collectors.toList());


        Map<String, BatchDetails> batchDetails = bundleEntityDAO.getBatchDetails(filteredBundles);


//		logger.info("BUNDLE-AGGREGATION-COUNT: " + bundleAggregations.size());
        List<BundleAggregation> bundleAggregationList = bundleAggregations.entrySet().stream()
                .map(e -> {
                    BundleAggregation bundleAggregation = new BundleAggregation();
                    bundleAggregation.setId(e.getKey());

//					logger.info("BUNDLE-AGGREGATION-ENTRYSET: " + collectionOrder.size());
                    if (!collectionOrder.isEmpty()) {
                        bundleAggregation.setRank(collectionOrder.get(Objects.nonNull(e.getKey()) ? e.getKey().replaceAll("\\.", "") : null));
                    }
                    bundleAggregation.setData(e.getValue());
                    return bundleAggregation;
                })
                .sorted(Comparator.nullsLast(Comparator.comparing(BundleAggregation::getRank)))
                .collect(Collectors.toList());


        List<BundleAggregation> finalBundleAggregation = new ArrayList<>();

        for (BundleAggregation bundleAggregation : bundleAggregationList)
        {
            BundleAggregation bundleAggregationResponse = new BundleAggregation();
            List<Bundle> bundlesResponse = new ArrayList<>();
            for (Bundle bundle : bundleAggregation.getData())
            {
                if (batchDetails.containsKey(bundle.getId()))
                {
                    if (bundle.getPackages() == null || bundle.getPackages().isEmpty())
                    {
                        List<AioPackage> packages = new ArrayList<>();
                        packages.add(new AioPackage());
                        bundle.setPackages(packages);
                    }
                    bundle.getPackages().get(0).setBatchDetails(batchDetails.get(bundle.getId()));
                }


                if (bundle.getPackages() != null && !bundle.getPackages().isEmpty()
                        && bundle.getPackages().get(0).getBatchDetails() != null
                        && bundle.getBundleDetails() != null && !StringUtils.isEmpty(bundle.getBundleDetails().getBundleImageUrl()))
                    bundlesResponse.add(bundle);
            }
            bundleAggregationResponse.setData(bundlesResponse);
            bundleAggregationResponse.setBanner(bundleAggregation.getBanner());
            bundleAggregationResponse.setId(bundleAggregation.getId());
            bundleAggregationResponse.setRank(bundleAggregation.getRank());
            finalBundleAggregation.add(bundleAggregationResponse);
        }
        return finalBundleAggregation;
    }

    public List<Bundle> getMicroCourseSubscriptionsPackages(BundleSearchReq req) {
        final List<Bundle> bundles = bundleDAO.getMicroCourseSubscriptionsPackages(req);
        final Set<String> ids = bundles.stream().map(Bundle::getId).collect(Collectors.toSet());
        final Set<String> filteredIds = bundleEntityDAO.getFilteredBundleIdForMicroCourse(ids, req, true);
        return bundles.stream().filter(e -> filteredIds.contains(e.getId())).collect(Collectors.toList());
    }

    public List<BundleEnrolment> getDuplicateEnrollment() throws VException {
        return bundleDAO.getDuplicateEnrollment();
    }

    public List<BannerPojo> getMicroCourseBannerPojoFromRedisMobile() {
        try {
            String microCourseBannerPojo = redisDAO.get("MICRO_COURSE_MOBILE_BANNER");
            Type listType = new TypeToken<List<BannerPojo>>() {
            }.getType();
            List<BannerPojo> result = gson.fromJson(microCourseBannerPojo, listType);
            return result;
        } catch (Exception ex) {
            //
        }
        return null;
    }

    public Bundle getBundleById(String id) {

        String cachedKey = getAIOCacheKey(id);
        String resp = null;
        try {
            resp = redisDAO.get(cachedKey);
        } catch (Exception e) {
            logger.error("Error in getting from redis for key: " + cachedKey + " : " + e.getMessage());
        }
        Bundle result;
        if (!StringUtils.isEmpty(resp)) {
            Type listType = new TypeToken<Bundle>() {
            }.getType();
            result = gson.fromJson(resp, listType);
        } else {
            result = bundleDAO.getBundleById(id);
            try {

                redisDAO.setex(cachedKey, gson.toJson(result), 1800);
            } catch (InternalServerErrorException e) {
                logger.error("Error in getting from redis for key: " + cachedKey + " : " + e.getMessage());
            }

        }
        return result;

    }

    public String getAIOCacheKey(String id) {
        return "_AIO_" + id;
    }

    public void updateBundleDetails(Map<String, BundleDetails> bundleDetailsMap, Map<String, Map<String, BatchDetails>> bacthDetailsMap) throws BadRequestException {
        for (String id : bundleDetailsMap.keySet()) {
            Bundle bundle = getBundleById(id);
            if (bundle == null) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bundle not Found " + id);
            }

            {
                BundleDetails details = bundle.getBundleDetails();
                BundleDetails bundleDetails = bundleDetailsMap.get(id);
                if (details == null) {
                    details = new BundleDetails();
                }

                String bundleImageUrl = bundleDetails.getBundleImageUrl();
                if (com.vedantu.util.StringUtils.isNotEmpty(bundleImageUrl)) {
                    details.setBundleImageUrl(bundleImageUrl);
                }

                LinkedHashSet<String> displayTags = bundleDetails.getDisplayTags();
                if (ArrayUtils.isNotEmpty(displayTags)) {
                    details.setDisplayTags(displayTags);
                    details.setDisplayTagsRanks(bundleDetails.getDisplayTagsRanks());
                }

                String promotionTag = bundleDetails.getPromotionTag();
                if (com.vedantu.util.StringUtils.isNotEmpty(promotionTag)) {
                    details.setPromotionTag(promotionTag.trim());
                }
                bundle.setBundleDetails(details);
            }

            {
                Map<String, BatchDetails> batchDetailsMap = bacthDetailsMap.get(id);
                List<AioPackage> packages = bundle.getPackages();

                for (String aioId : batchDetailsMap.keySet()) {
                    BatchDetails batchDetails = batchDetailsMap.get(aioId);

                    Optional<AioPackage> aioPackageOptional = packages.stream().filter(e -> aioId.equals(e.getEntityId())).findFirst();
                    if (aioPackageOptional.isPresent()) {
                        AioPackage aPackage = aioPackageOptional.get();

                        // Should not set Batch details for OTM BUNDLE.
                        if (PackageType.OTM_BUNDLE.equals(aPackage.getPackageType())) {
                            continue;
                        }

                        BatchDetails details = aPackage.getBatchDetails();

                        // DB entry is null so we save the object completly
                        if (details == null) {
                            aPackage.setBatchDetails(batchDetails);
                            continue;
                        }

                        long courseHours = batchDetails.getCoursePeriodMillis();
                        if (courseHours > 0) {
                            details.setCoursePeriodMillis(courseHours);
                        }

                        LinkedHashSet<Classes> courseSyllabus = batchDetails.getCourseSyllabus();
                        if (ArrayUtils.isNotEmpty(courseSyllabus)) {
                            details.setCourseSyllabus(courseSyllabus);
                        }

                        long courseStartTime = batchDetails.getCourseStartTime();
                        if (courseStartTime > 0) {
                            details.setCourseStartTime(courseStartTime);
                        }

                        CourseType courseType = batchDetails.getCourseType();
                        if (courseType != null) {
                            details.setCourseType(courseType);
                        }

                        Map<String, Object> teacherInfo = batchDetails.getTeacherInfo();
                        if (teacherInfo != null && teacherInfo.size() > 0) {
                            details.setTeacherInfo(teacherInfo);
                        }
                    }
                }
            }
            bundleDAO.save(bundle, null);
        }
    }

    public List<SubscriptionCoursesRes> getSubscriptionCourses(SubscriptionCoursesReq req) {

        logger.info("getSubscriptionCourses for user id - {}",req.getCallingUserId());
        List<BundleEnrolment> bundleEnrolmentList = bundleEnrolmentDAO.getUsersActiveAndInactiveEnrollment(req.getCallingUserId().toString());
        List<String> bundleIds = new ArrayList<>();
        List<String> activeBundleList = new ArrayList<>();
        List<String> regularBundleIds = new ArrayList<>();
        for (BundleEnrolment enrolment : bundleEnrolmentList) {
            bundleIds.add(enrolment.getBundleId());
            if (EntityStatus.ACTIVE.equals(enrolment.getStatus())) {
                activeBundleList.add(enrolment.getBundleId());
            }
            if (EnrollmentState.REGULAR.equals(enrolment.getState())) {
                regularBundleIds.add(enrolment.getBundleId());
            }
        }
        List<Bundle> bundles = bundleDAO.getSubscriptionBundlesFilterd( regularBundleIds);
        if (ArrayUtils.isNotEmpty( bundles )) {
            req.setSubscriptionPackageType( SubscriptionPackageType.REGULAR );
        }
        req.setBundleIds(bundleIds);

        GetUserEnrollmentsReq enrollmentsReq = new GetUserEnrollmentsReq(req.getCallingUserId(),true);
        List<Enrollment> enrollmentList = enrollmentDAO.getBatchIdsOfActiveEnrollmentsOfUser(enrollmentsReq);
        List<String> enrolledBatchIds = enrollmentList.stream().map(Enrollment::getBatchId).collect(Collectors.toList());
        logger.info("Enrolled batch ids are - {}",enrolledBatchIds);
        if (Objects.nonNull(req.getIncludeEnrolled())) {
            logger.info("Including or excluding enrolled batch ids");
            if (!req.getIncludeEnrolled()) {
                logger.info("Excluding enrolled batch ids");
                req.setExcludeBatchIds(enrolledBatchIds);
            } else {
                logger.info("Including enrolled batch ids");
                if(ArrayUtils.isNotEmpty(enrolledBatchIds)) {
                    req.setBatchIds(enrolledBatchIds);
                }else{
                    return new ArrayList<>();
                }
            }
        }
        List<BundleEntity> batchesByBundles = bundleEntityDAO.getBatchesByBundles(req);
        logger.info("batchesByBundles - {}",batchesByBundles);
        Map<String, String> batchBundleMap = new HashMap<>();
        for (BundleEntity batchesByBundle : batchesByBundles) {
            if(batchBundleMap.containsKey(batchesByBundle.getEntityId())){
                if(activeBundleList.contains(batchesByBundle.getBundleId())){
                    batchBundleMap.put(batchesByBundle.getEntityId(), batchesByBundle.getBundleId());
                }
            }
            else {
                batchBundleMap.put(batchesByBundle.getEntityId(), batchesByBundle.getBundleId());
            }
        }
        Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(batchBundleMap.keySet(), false);
        List<String> courseIds = batchMap.values().stream().map(BatchBasicInfo::getCourseId).collect(Collectors.toList());
        Map<String, CourseBasicInfo> courseMap = enrollmentManager.getCourseMap(courseIds, false);
        List<SubscriptionCoursesRes> subscriptionCoursesList = new ArrayList<>();
        for (String batchId : batchMap.keySet()) {
            BatchBasicInfo batchInfo = batchMap.get(batchId);
            CourseBasicInfo courseInfo = courseMap.get(batchInfo.getCourseId());
            List<String> subjects = courseInfo.getBoardTeacherPairs()
                    .stream().map(BoardTeacherPair::getSubject).collect(Collectors.toList());

            Long currentTimeMillis = System.currentTimeMillis();
            BatchStatus batchStatus;
            if (batchInfo.getEndTime() > currentTimeMillis) {
                if (batchInfo.getStartTime() > currentTimeMillis) {
                    batchStatus=BatchStatus.UPCOMING;
                } else {
                    batchStatus=BatchStatus.ONGOING;
                }
            } else {
                batchStatus=BatchStatus.FINISH;
            }
            SubscriptionCoursesRes response = new SubscriptionCoursesRes(
                    batchInfo.getId(),
                    batchInfo.getCourseId(),
                    batchBundleMap.get(batchInfo.getBatchId()),
                    batchInfo.getSessionPlan(),
                    courseInfo.getTitle(),
                    subjects,
                    batchInfo.getStartTime(),
                    batchInfo.getEndTime(),
                    batchInfo.getTeachers(),
                    enrolledBatchIds.contains(batchInfo.getBatchId()),
                    batchInfo.isRecordedVideo(),
                    batchStatus
            );
            subscriptionCoursesList.add(response);
        }
        Boolean ascendingOrderStartTimeSortCriteria = req.getAscendingOrderStartTimeSort();
        if(Objects.nonNull(ascendingOrderStartTimeSortCriteria)){
            if(Boolean.TRUE.equals(ascendingOrderStartTimeSortCriteria)){
                subscriptionCoursesList.sort(Comparator.comparingLong(SubscriptionCoursesRes::getStartDate));
            }else{
                subscriptionCoursesList.sort((o1, o2) -> Long.compare(o2.getStartDate(), o1.getStartDate()));
            }
        }
        return subscriptionCoursesList;
    }

    private boolean filterActiveBundleEnrollment(BundleEnrolment bundleEnrolment) {
        return EntityStatus.ACTIVE.equals(bundleEnrolment.getStatus());
    }

    public List<Bundle> getBundles(List<String> bundleIds, List<String> includeFields) {
        return bundleDAO.getBundlesByIds(bundleIds, includeFields);
    }

    public void addBatchToBundle(AddBatchToBundleReq req) throws BadRequestException {

        logger.info("addBatchToBundle for batch id - {}, grades - {}, targets - {}, subjects - {} ",
                req.getBatchId(),req.getGrades(),req.getTargets(),req.getSubjects());

        List<Integer> newGrades = new ArrayList<>();
        List<String> newTargets = req.getTargets();

        if (ArrayUtils.isEmpty(req.getGrades()) || ArrayUtils.isEmpty(req.getTargets()) || ArrayUtils.isEmpty(req.getSubjects())) {
            logger.info("As there's one mandatory data empty so removing all the reference of the batch id");
            removeBatch(req.getBatchId());
        }
        boolean includeAssist = true;
        if (ArrayUtils.isNotEmpty(req.getGrades())) {
            newGrades = req.getGrades().stream().map(Integer::valueOf).collect(Collectors.toList());
        }
        if (ArrayUtils.isNotEmpty(req.getCourseTerms())) {
            if(req.getCourseTerms().contains(CourseTerm.LONG_TERM) ||req.getCourseTerms().contains(CourseTerm.SHORT_TERM)){
                includeAssist=false;
            }
        }

        List<String> bundleIncludeFields = Arrays.asList(Bundle.Constants.GRADE, Bundle.Constants.TARGET,
                Bundle.Constants.SUBJECT, Bundle.Constants.WINDOW_RIGHT, Bundle.Constants.VALID_TILL,Bundle.Constants.IS_SUBSCRIPTION);

        List<String> existingBundleIds = bundleEntityDAO.getAllDistinctSubscriptionBundleIdByBatchId(req.getBatchId());
        logger.info("All the existing bundle ids are {}",existingBundleIds);
        if(ArrayUtils.isNotEmpty(existingBundleIds)){
            List<List<String>> bundleIdsPartition = Lists.partition(existingBundleIds, 100);
            logger.info("No of partition received are {}",bundleIdsPartition.size());
            for (List<String> bundleIdPartition : bundleIdsPartition) {
                List<Bundle> bundleList = bundleDAO.getBundlesByIds(bundleIdPartition, bundleIncludeFields);
                batchProcessBundlePackageAddition(req, newGrades, bundleList);
            }
        }

        int start = 0;
        int size = 100;
        while (true) {
            List<Bundle> bundleList = bundleDAO.getBundleByTags(existingBundleIds,newGrades, newTargets, req.getSubjects(), bundleIncludeFields,includeAssist , start, size);
            logger.info("No of bundle received for start - {}, size - {} is {}",start,size,ArrayUtils.isNotEmpty(bundleList)?bundleList.size():0);
            if (ArrayUtils.isEmpty(bundleList)) {
                break;
            }
            logger.info("- Bundle ids start -");
            bundleList.stream().map(Bundle::getId).forEach(logger::info);
            logger.info("- Bundle ids end -");

            addRemovePackagesFromBundlesByGrades(bundleList, req, Boolean.FALSE, newGrades);
            start += 100;
        }
    }

    private void batchProcessBundlePackageAddition(AddBatchToBundleReq req, List<Integer> newGrades, List<Bundle> bundleList) {
        if(ArrayUtils.isEmpty(bundleList)){
            return;
        }
        if(bundleList.size()>100){
            logger.error("No more than 100 data allowed for batch processing, please strictly follow it, if it is going more then work on it.");
        }
        List<Bundle> needToRemoveBundles = new ArrayList<>();
        List<String> bundleIdsToUpdate = new ArrayList<>();
        for (Bundle bundle : bundleList) {
            bundle.getGrade().retainAll(newGrades);
            bundle.getTarget().retainAll(req.getTargets());
            bundle.getSubject().retainAll(req.getSubjects());
            if (ArrayUtils.isNotEmpty(bundle.getGrade()) && ArrayUtils.isNotEmpty(bundle.getTarget())
                    && ArrayUtils.isNotEmpty(bundle.getSubject())) {
                bundleIdsToUpdate.add(bundle.getId());
            } else {
                needToRemoveBundles.add(bundle);
            }
        }
        if (ArrayUtils.isNotEmpty(bundleIdsToUpdate)) {
            updateBundleEntity(req,bundleIdsToUpdate);
        }

        if (ArrayUtils.isNotEmpty(needToRemoveBundles)) {
            addRemovePackagesFromBundlesByGrades(needToRemoveBundles, req, Boolean.TRUE, newGrades);
        }
    }

    public void editBatchInBundle(AddBatchToBundleReq req) {
        List<Bundle> bundles = new ArrayList<>();
        Boolean processing = Boolean.TRUE;
        int start = 0;
        int size = 100;
        while (Boolean.TRUE.equals(processing)) {
            List<Bundle> bundleList = bundleDAO.getBundlesByPackageId(req.getBatchId(),
                    Arrays.asList(Bundle.Constants.PACKAGES, Bundle.Constants.GRADE, Bundle.Constants.TARGET, Bundle.Constants.SUBJECT, Bundle.Constants.WINDOW_RIGHT, Bundle.Constants.VALID_TILL),
                    start, size);
            if (ArrayUtils.isNotEmpty(bundleList)) {
                bundles.addAll(bundleList);
                start += bundleList.size();
            } else {
                processing = Boolean.FALSE;
            }
        }

        if (ArrayUtils.isNotEmpty(bundles)) {
            updateBundles(bundles, req);
        }
    }

    public void removeBatch(String batchId) throws BadRequestException {
        if(StringUtils.isEmpty(batchId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"batchId can't be empty");
        }
        bundleEntityDAO.removeAllTheReferenceOfSubscriptionBundleEntity(batchId);
    }

    private void updateBundleEntity(AddBatchToBundleReq req, List<String> bundleIds){
        logger.info("updateBundleEntity for bundle ids {}",bundleIds);
        Optional.ofNullable(bundleIds).map(Collection::stream).orElseGet(Stream::empty).forEach(bundleId-> {
            try {
                updateBundle(getCachedBundleBasicInfo(bundleId),req);
            } catch (BadRequestException | InternalServerErrorException e) {
                logger.error("Error in updateBundleEntity for bundleId {} with error {}",bundleId,e);
            }
        });
    }

    private void updateBundle(BundleInfo bundle, AddBatchToBundleReq req) {

        logger.info("Preparing to updateBundle for bundle id {}",bundle.getId());

        List<BundleEntity> packagesToBeRemoved = new ArrayList<>();
        Set<Integer> newGrades = req.getGrades().stream().map(Integer::valueOf).collect(Collectors.toSet());
        bundle.getTarget().retainAll(req.getTargets());
        bundle.getSubject().retainAll(req.getSubjects());
        bundle.getGrade().retainAll(newGrades);
        newGrades.retainAll(bundle.getGrade());
        List<BundleEntity> bundleEntities = bundleEntityDAO.getAllBundleEntityByBundleIdAndEntityId(bundle.getId(),req.getBatchId());
        for (BundleEntity bundleEntity : bundleEntities) {
            Language language = Objects.nonNull(req.getLanguage()) ? req.getLanguage() : Language.DEFAULT;
            List<SubscriptionPackageType> subscriptionPackageTypes = Objects.nonNull(req.getPackageTypes()) ? req.getPackageTypes() : new ArrayList<>();
            if (ArrayUtils.isEmpty(subscriptionPackageTypes)) {
                subscriptionPackageTypes.add(SubscriptionPackageType.DEFAULT);
            }
            newGrades.remove(bundleEntity.getGrade());
            if (!req.getGrades().contains(bundleEntity.getGrade().toString())) {
                packagesToBeRemoved.add(bundleEntity);
                continue;
            }
            List<String> bundleSubjectsClone = new ArrayList<>();
            Set<String> bundleTargetClone = new HashSet<>();

            if (ArrayUtils.isNotEmpty(bundle.getSubject())) {
                bundleSubjectsClone = new ArrayList<>(bundle.getSubject());
                bundleSubjectsClone.retainAll(req.getSubjects());
            }

            if (ArrayUtils.isNotEmpty(bundle.getTarget())) {
                bundleTargetClone = new HashSet<>(bundle.getTarget());
                bundleTargetClone.retainAll(req.getTargets());
            }
            Set<String> mainTags = new HashSet<>(bundleEntity.getMainTags());

            if (Objects.nonNull(bundleEntity.getRecordedVideo())) {
                mainTags.remove(bundleEntity.getRecordedVideo() ? BundleDAO.AIOPACKAGE_TAGS_RECORDED : BundleDAO.AIOPACKAGE_TAGS_LIVE);
            }

            mainTags.removeAll(bundleEntity.getSubjects());
            mainTags.removeAll(Arrays.stream(Language.values()).map(Enum::toString).collect(Collectors.toList()));
            mainTags.removeAll(Arrays.stream(SubscriptionPackageType.values()).map(Enum::toString).collect(Collectors.toList()));
            mainTags.removeAll(Arrays.stream(CourseTerm.values()).map(Enum::toString).collect(Collectors.toList()));

            mainTags.addAll(subscriptionPackageTypes.stream().map(Object::toString).collect(Collectors.toList()));
            mainTags.addAll(req.getCourseTerms().stream().map(Object::toString).collect(Collectors.toList()));
            mainTags.addAll(bundleSubjectsClone);
            mainTags.addAll(bundleTargetClone);

            if (Objects.nonNull(req.getRecordedVideo())) {
                mainTags.add(req.getRecordedVideo() ? BundleDAO.AIOPACKAGE_TAGS_RECORDED : BundleDAO.AIOPACKAGE_TAGS_LIVE);
            }
            mainTags.add(language.toString());

            Update update=new Update();
            update.set(BundleEntity.Constants.LANGUAGE,language);
            update.set(BundleEntity.Constants.SUBSCRIPTION_PACKAGES_TYPES,subscriptionPackageTypes);
            update.set(BundleEntity.Constants.RECORDED_VIDEO,req.getRecordedVideo());
            update.set(BundleEntity.Constants.SUBJECTS,new ArrayList<>(bundleSubjectsClone));
            update.set(BundleEntity.Constants.TARGETS,new HashSet<>(bundleTargetClone));
            update.set(BundleEntity.Constants.START_TIME,req.getStartTime());
            update.set(BundleEntity.Constants.END_TIME,req.getEndTime());
            update.set(BundleEntity.Constants.SEARCH_TERMS,req.getCourseTerms());
            update.set(BundleEntity.Constants.COURSE_TITLE,req.getCourseTitle());
            update.set(BundleEntity.Constants.MAIN_TAGS,mainTags);

            bundleEntityDAO.updateBundleEntityData(bundleEntity.getId(),update);

        }
        if(ArrayUtils.isNotEmpty(packagesToBeRemoved)){
            bundleEntityDAO.deleteMultipleEntitiesById(packagesToBeRemoved.stream().map(BundleEntity::getId).collect(Collectors.toList()));
        }
        if (ArrayUtils.isNotEmpty(newGrades)) {
            logger.info("New grades are present here so need to create new bundle entity for the grades {}",newGrades);
            List<BundleEntity> bundleEntityList =new ArrayList<>();
            for (Integer newGrade : newGrades) {
                bundleEntityList.add(createNewBundleEntity(req,newGrade,bundle));
            }
            logger.info("Bundle entities for additon due to new grades are - {}",bundleEntityList);
            if(ArrayUtils.isNotEmpty(bundleEntityList)){
                bundleEntityDAO.insertAllEntities(bundleEntityList);
            }
        }
    }

    public BundleInfo getCachedBundleBasicInfo(String bundleId) throws BadRequestException, InternalServerErrorException {
        String bundleKeyForRedis = getBundleKeyForRedis(bundleId);
        String bundleData=redisDAO.get(bundleKeyForRedis);
        if(StringUtils.isEmpty(bundleData)){
            Bundle bundle = bundleDAO.getBundleById(bundleId);
            if(Objects.nonNull(bundle)) {
                bundle.setPackages(new ArrayList<>());
                bundle.setVideos(new VideoContentData());
                bundle.setTests(new TestContentData());
                BundleInfo bundleInfo=mapper.map(bundle,BundleInfo.class);
                bundleInfo.setPackagesExists(ArrayUtils.isNotEmpty(bundleEntityDAO.getBundleEntitiesByBundleId(bundleId,0,1,Collections.singletonList(BundleEntity.Constants._ID))));
                redisDAO.setex(bundleKeyForRedis,gson.toJson(bundleInfo),DateTimeUtils.SECONDS_PER_HOUR);
                return bundleInfo;
            }else{
                throw new BadRequestException(ErrorCode.INVALID_PARAMETER,bundleId+ " bundle id is invalid");
            }
        }
        return gson.fromJson(bundleData,BundleInfo.class);
    }

    public List<BundleInfo> getCachedBundleBasicInfoList(List<String> bundleIds) throws InternalServerErrorException {
        List<BundleInfo> bundleInfoList=new ArrayList<>();
        if(ArrayUtils.isEmpty(bundleIds)){
            return bundleInfoList;
        }
        Map<String,String> keyMap=new HashMap<>();
        for(String bundleId:bundleIds){
            keyMap.put(bundleId,getBundleKeyForRedis(bundleId));
        }
        Map<String, String> bundleValues = redisDAO.getValuesForKeys(new HashSet<>(keyMap.values()));
        List<String> nonCachedBundleIds=new ArrayList<>();
        for(String key:keyMap.keySet()){
            if(bundleValues.containsKey(keyMap.get(key))){
                bundleInfoList.add(gson.fromJson(bundleValues.get(keyMap.get(key)),BundleInfo.class));
            }else{
                nonCachedBundleIds.add(key);
            }
        }
        List<Bundle> bundles = bundleDAO.getBundlesByIds(nonCachedBundleIds);
        for(Bundle bundle:bundles){
            BundleInfo bundleInfo=mapper.map(bundle,BundleInfo.class);
            bundleInfo.setPackages(new ArrayList<>());
            bundleInfo.setVideos(new VideoContentData());
            bundleInfo.setTests(new TestContentData());
            redisDAO.set(keyMap.get(bundleInfo.getId()),gson.toJson(bundleInfo));
            bundleInfoList.add(bundleInfo);
        }
        return bundleInfoList;
    }

    public String getBundleKeyForRedis(String bundleId) {
        return BUNDLE_BASIC_INFO +bundleId;
    }

    private void updateBundles(List<Bundle> bundles, AddBatchToBundleReq req) {
        for (Bundle bundle : bundles) {
            List<AioPackage> packagesToBeRemoved = new ArrayList<>();
            Set<Integer> newGrades = req.getGrades().stream().map(Integer::valueOf).collect(Collectors.toSet());
            newGrades.retainAll(bundle.getGrade());
            List<AioPackage> aioPackages = bundle.getPackages();
            for (AioPackage aioPackage : aioPackages) {
                if (aioPackage.getEntityId().equals(req.getBatchId())) {
                    Language language = Objects.nonNull(req.getLanguage()) ? req.getLanguage() : Language.DEFAULT;
                    List<SubscriptionPackageType> subscriptionPackageTypes = Objects.nonNull(req.getPackageTypes()) ? req.getPackageTypes() : new ArrayList<>();
                    if (ArrayUtils.isEmpty(subscriptionPackageTypes)) {
                        subscriptionPackageTypes.add(SubscriptionPackageType.DEFAULT);
                    }
                    newGrades.remove(aioPackage.getGrade());
                    if (!req.getGrades().contains(aioPackage.getGrade().toString())) {
                        packagesToBeRemoved.add(aioPackage);
                    }
                    List<String> bundleSubjectsClone = new ArrayList<>();
                    Set<String> bundleTargetClone = new HashSet<>();

                    if (ArrayUtils.isNotEmpty(bundle.getSubject())) {
                        bundleSubjectsClone = new ArrayList<>(bundle.getSubject());
                        bundleSubjectsClone.retainAll(req.getSubjects());
                    }

                    if (ArrayUtils.isNotEmpty(bundle.getTarget())) {
                        bundleTargetClone = new HashSet<>(bundle.getTarget());
                        bundleTargetClone.retainAll(req.getTargets());
                    }
                    // mainTags may should not contain duplicate values.
                    List<String> mainTags = aioPackage.getMainTags();
                    Set<String> uniqueMainTags = new HashSet<>(mainTags);
                    mainTags.clear();
                    mainTags.addAll(uniqueMainTags);

                    if (!Objects.isNull(aioPackage.getRecordedVideo())) {
                        mainTags.remove(aioPackage.getRecordedVideo() ? BundleDAO.AIOPACKAGE_TAGS_RECORDED : BundleDAO.AIOPACKAGE_TAGS_LIVE);
                    }
                    mainTags.removeAll(aioPackage.getSubjects());
                    mainTags.removeAll(Arrays.stream(Language.values()).map(Enum::toString).collect(Collectors.toList()));
                    mainTags.removeAll(Arrays.stream(SubscriptionPackageType.values()).map(Enum::toString).collect(Collectors.toList()));
                    mainTags.removeAll(Arrays.stream(CourseTerm.values()).map(Enum::toString).collect(Collectors.toList()));

                    aioPackage.setLanguage(language);
                    logger.info("updating mainTags " + mainTags);
                    logger.info("updating req Lang " + req.getLanguage() + " curr lang " + language);
                    aioPackage.setSubscriptionPackageTypes(subscriptionPackageTypes);
                    aioPackage.setRecordedVideo(req.getRecordedVideo());
                    aioPackage.setSubjects(new ArrayList<>(bundleSubjectsClone));
                    aioPackage.setTargets(new HashSet<>(bundleTargetClone));
                    aioPackage.setStartTime(req.getStartTime());
                    aioPackage.setEndTime(req.getEndTime());

                    aioPackage.getSearchTerms().forEach(term -> mainTags.remove(term.toString()));
                    subscriptionPackageTypes.forEach(type -> mainTags.add(type.toString()));
                    aioPackage.setSearchTerms(req.getCourseTerms());
                    aioPackage.setCourseTitle(req.getCourseTitle());
                    aioPackage.getSearchTerms().forEach(term -> mainTags.add(term.toString()));
                    aioPackage.setCourseTitle(req.getCourseTitle());
                    mainTags.addAll(bundleSubjectsClone);
                    mainTags.addAll(bundleTargetClone);
                    if (!Objects.isNull(aioPackage.getRecordedVideo())) {
                        mainTags.add(aioPackage.getRecordedVideo() ? BundleDAO.AIOPACKAGE_TAGS_RECORDED : BundleDAO.AIOPACKAGE_TAGS_LIVE);
                    }
                    mainTags.add(language.toString());
                    if(Objects.nonNull( aioPackage.getGrade() )) {
                        mainTags.add( aioPackage.getGrade().toString() );
                    }
                    aioPackage.setMainTags(mainTags);
                }
            }
            aioPackages.removeAll(packagesToBeRemoved);
            if (ArrayUtils.isNotEmpty(newGrades)) {
                for (Integer newGrade : newGrades) {
                    aioPackages.add(getNewAIOPackage(req, newGrade, bundle));
                }
            }
            bundle.setPackages(aioPackages);
            Query query = new Query();
            query.addCriteria(Criteria.where(Bundle.Constants._ID).is(bundle.getId()));
            Update update = new Update();
            update.set(Bundle.Constants.PACKAGES, bundle.getPackages());
            bundleDAO.updateMultiple(query, update);
        }

    }

    private Boolean allowBatchToAddInBundle(AddBatchToBundleReq req, List<Integer> grades, Bundle bundle) {
        Boolean hasCommonTag = Boolean.FALSE;
        Boolean hasValidStartDate = Boolean.FALSE;

        List<String> bundleTargetsClone = new ArrayList<>(bundle.getTarget());
        List<Integer> bundleGradesClone = new ArrayList<>(bundle.getGrade());
        List<String> bundleSubjectsClone = new ArrayList<>(bundle.getSubject());

        bundleGradesClone.retainAll(grades);
        bundleTargetsClone.retainAll(req.getTargets());
        bundleSubjectsClone.retainAll(req.getSubjects());

        if (ArrayUtils.isNotEmpty(bundleGradesClone) && ArrayUtils.isNotEmpty(bundleTargetsClone) && ArrayUtils.isNotEmpty(bundleSubjectsClone)) {
            hasCommonTag = Boolean.TRUE;
        }


        if (Objects.nonNull(req.getStartTime()) && Objects.nonNull(bundle.getWindowRight()) && Objects.nonNull(bundle.getValidTill())) {
            if (bundle.getWindowRight() <= req.getStartTime() && bundle.getValidTill() >= req.getStartTime()) {
                hasValidStartDate = Boolean.TRUE;
            }
        }
        return (hasCommonTag && hasValidStartDate);
    }

    private void addRemovePackagesFromBundlesByGrades(List<Bundle> bundles, AddBatchToBundleReq req,
                                                      boolean remove, List<Integer> newGrades) {

        logger.info("addRemovePackagesFromBundlesByGrades");
        if(ArrayUtils.isEmpty(bundles)){
            return;
        }

        if(bundles.size()>100){
            logger.error("addRemovePackagesFromBundlesByGrades - No more than 100 data allowed for batch processing, " +
                    "please strictly follow it, if it's exceeding, optimize it more");
        }

        if (remove) {
            logger.info("Removal of batchIds from bundleId and entity id combination");
            bundleEntityDAO.removeAllBundleIdsEntityIdCombination(bundles.stream().map(Bundle::getId).collect(Collectors.toList()), req.getBatchId());
        } else {
            logger.info("Addition of new entities to bundle");
            List<BundleEntity> bundleEntityList =new ArrayList<>();
            for (Bundle bundle : bundles) {
                logger.info("Processing for addition of bundle entity for bundle id - {}",bundle.getId());
                if (allowBatchToAddInBundle(req, newGrades, bundle)) {
                    logger.info("Bundle allowed for entity addition");
                    BundleInfo bundleInfo=mapper.map(bundle,BundleInfo.class);
                    for (Integer grade : newGrades) {
                        logger.info("Processing for grade...");
                        if (bundle.getGrade().contains(grade)) {
                            logger.info("Adding for bundle id - {} and grade - {} and batchId - {}",bundle.getId(),grade,req.getBatchId());
                            bundleEntityList.add(createNewBundleEntity(req, grade, bundleInfo));
                        }
                    }
                }else{
                    logger.info("Bundle not allowed for entity addition");
                }
            }
            logger.info("Addition of new bundle entities of size {}",bundleEntityList.size());
            if(ArrayUtils.isNotEmpty(bundleEntityList)) {
                bundleEntityDAO.insertAllEntities(bundleEntityList);
                logger.info("Received details of bundle entities are - ");
                bundleEntityList.forEach(entity->logger.info("_id - "+entity.getId()+" bundleId - "+entity.getBundleId()+" batchId - "+entity.getEntityId()));
            }
        }
    }

    private BundleEntity createNewBundleEntity(AddBatchToBundleReq req, Integer grade, BundleInfo bundle){
        BundleEntity bundleEntity = new BundleEntity();
        bundleEntity.setGrade(grade);
        bundleEntity.setPackageType(PackageType.OTF);
        bundleEntity.setEntityId(req.getBatchId());
        bundleEntity.setEnrollmentType(EnrollmentType.CLICK_TO_ENROLL);
        bundleEntity.setSubscription(bundle.getIsSubscription());
        Language language = Objects.nonNull( req.getLanguage() ) ? req.getLanguage() : Language.DEFAULT;
        List<SubscriptionPackageType> subscriptionPackageTypes = Objects.nonNull( req.getPackageTypes() ) ? req.getPackageTypes() : new ArrayList<>();
        if(ArrayUtils.isEmpty(subscriptionPackageTypes)) {
            subscriptionPackageTypes.add( SubscriptionPackageType.DEFAULT );
        }
        bundleEntity.setRecordedVideo(req.getRecordedVideo());
        bundleEntity.setBundleId(bundle.getId());
        List<String> bundleSubjectsClone = new ArrayList<>(bundle.getSubject());
        List<String> bundleTargetsClone = new ArrayList<>(bundle.getTarget());
        bundleSubjectsClone.retainAll(req.getSubjects());
        bundleEntity.setSubjects(new ArrayList<>(bundleSubjectsClone));
        bundleEntity.setTargets(new HashSet<>(bundleTargetsClone));
        bundleEntity.setCourseId(req.getCourseId());
        bundleEntity.setStartTime(req.getStartTime());
        bundleEntity.setEndTime(req.getEndTime());
        bundleEntity.setCourseTitle(req.getCourseTitle());
        bundleEntity.setSearchTerms(req.getCourseTerms());
        bundleEntity.setLanguage( language );
        bundleEntity.setSubscriptionPackageTypes(subscriptionPackageTypes);
        List<String> mainTags = new ArrayList<>();
        mainTags.add(req.getCourseId());
        mainTags.add(Integer.toString(grade));
        mainTags.addAll(bundleEntity.getSubjects());
        mainTags.addAll(bundleEntity.getTargets());
        mainTags.add(language.toString());
        mainTags.addAll(subscriptionPackageTypes.stream().map(Object::toString).collect(Collectors.toList()));
        mainTags.addAll(req.getCourseTerms().stream().map(Object::toString).collect(Collectors.toList()));
        mainTags.add(bundleEntity.getRecordedVideo() ? BundleDAO.AIOPACKAGE_TAGS_RECORDED : BundleDAO.AIOPACKAGE_TAGS_LIVE);
        bundleEntity.setMainTags(mainTags);
        return bundleEntity;
    }

    private AioPackage getNewAIOPackage(AddBatchToBundleReq req, Integer grade, Bundle bundle) {
        AioPackage aioPackage = new AioPackage(grade, PackageType.OTF, req.getBatchId(), EnrollmentType.CLICK_TO_ENROLL);
        Language language = Objects.nonNull( req.getLanguage() ) ? req.getLanguage() : Language.DEFAULT;
        List<SubscriptionPackageType> subscriptionPackageTypes = Objects.nonNull( req.getPackageTypes() ) ? req.getPackageTypes() : new ArrayList<>();
        if(ArrayUtils.isEmpty(subscriptionPackageTypes)) {
            subscriptionPackageTypes.add( SubscriptionPackageType.DEFAULT );
        }
        aioPackage.setRecordedVideo(req.getRecordedVideo());
        List<String> bundleSubjectsClone = new ArrayList<>(bundle.getSubject());
        List<String> bundleTargetsClone = new ArrayList<>(bundle.getTarget());
        bundleSubjectsClone.retainAll(req.getSubjects());
        aioPackage.setSubjects(new ArrayList<>(bundleSubjectsClone));
        aioPackage.setTargets(new HashSet<>(bundleTargetsClone));
        aioPackage.setCourseId(req.getCourseId());
        aioPackage.setStartTime(req.getStartTime());
        aioPackage.setEndTime(req.getEndTime());
        aioPackage.setCourseTitle(req.getCourseTitle());
        aioPackage.setSearchTerms(req.getCourseTerms());
        aioPackage.setLanguage( language );
        aioPackage.setSubscriptionPackageTypes(subscriptionPackageTypes);
        List<String> mainTags = new ArrayList<>();
        mainTags.add(req.getCourseId());
        mainTags.add( String.valueOf( grade ) );
        mainTags.addAll(aioPackage.getSubjects());
        mainTags.addAll(aioPackage.getTargets());
        mainTags.add( language.toString() );
        subscriptionPackageTypes.forEach(type -> mainTags.add( type.toString() ));
        req.getCourseTerms().forEach(term -> mainTags.add(term.toString()));

        mainTags.add(aioPackage.getRecordedVideo() ? bundleDAO.AIOPACKAGE_TAGS_RECORDED : bundleDAO.AIOPACKAGE_TAGS_LIVE);
        aioPackage.setMainTags(mainTags);
        return aioPackage;

    }


    public void addPackagesInBundle(String bundleId) {
        Bundle bundle = bundleDAO.getBundleById(bundleId);
        logger.info("Req in addPackagesInBundle for bundleId - {}",bundleId);
        if (Objects.isNull(bundle)) {
            return;
        }
        Boolean allowedBundle = Boolean.TRUE.equals(bundle.getIsSubscription()) && ArrayUtils.isNotEmpty(bundle.getGrade()) && ArrayUtils.isNotEmpty(bundle.getTarget()) && ArrayUtils.isNotEmpty(bundle.getSubject());
        logger.info("allowedBundle " + allowedBundle + " subscriptionId " + bundle.getId());
        if (Boolean.FALSE.equals(allowedBundle)) {
            return;
        }

        for (Integer grade : bundle.getGrade()) {
            logger.info("Adding package for grade: " + grade);
            List<Course> courses = new ArrayList<>();
            Boolean processing = Boolean.TRUE;
            int start = 0;
            int size = 100;
            while (Boolean.TRUE.equals(processing)) {
                List<Course> courseList = courseDAO.getCoursesForSubscription(grade.toString(), bundle.getSubject(), bundle.getTarget(), start, size);
                if (ArrayUtils.isNotEmpty(courseList)) {
                    courses.addAll(courseList);
                    start += courseList.size();
                    logger.info("Fetched courses are {}",courseList.size());
                } else {
                    processing = Boolean.FALSE;
                }
            }
            logger.info("Courses size: " + courses.size());
            if (ArrayUtils.isEmpty(courses)) {
                continue;
            }

            for (Course course : courses) {
                logger.info("Adding courseId in package: " + course.getId() + " in subscription Id" + bundle.getId());
                Set<String> subjects = course.getNormalizeSubjects();
                subjects.retainAll(bundle.getSubject());
                Set<String> targets = course.getNormalizeTargets();
                targets.retainAll(bundle.getTarget());
                List<Batch> batches = new ArrayList<>();

                processing = Boolean.TRUE;
                start = 0;
                size = 100;
                while (Boolean.TRUE.equals(processing)) {
                    List<Batch> batchList = batchDAO.getBatchesForSubscription(course.getId(),bundle.getIsVassist() ,start, size);
                    if (ArrayUtils.isNotEmpty(batchList)) {
                        batches.addAll(batchList);
                        start += batchList.size();
                    } else {
                        processing = Boolean.FALSE;
                    }
                }
                logger.info("Batch size: " + batches.size());
                if (ArrayUtils.isEmpty(batches)) {
                    continue;
                }

                for (Batch batch : batches) {
                    logger.info(bundle.getWindowRight() + "<=" + batch.getStartTime() + "&&" + bundle.getValidTill() + ">=" + batch.getStartTime());
                    if (Objects.nonNull(batch.getStartTime()) && Objects.nonNull(bundle.getWindowRight()) && Objects.nonNull(bundle.getValidTill())) {
                        if (bundle.getWindowRight() <= batch.getStartTime() && bundle.getValidTill() >= batch.getStartTime()) {
                            Language language = Objects.nonNull( batch.getLanguage() ) ? batch.getLanguage() : Language.DEFAULT;
                            List<SubscriptionPackageType> subscriptionPackageTypes = Objects.nonNull( batch.getPackageTypes() ) ? batch.getPackageTypes() : new ArrayList<>();
                            if(ArrayUtils.isEmpty(subscriptionPackageTypes)) {
                                subscriptionPackageTypes.add( SubscriptionPackageType.DEFAULT );
                            }
                            logger.info("Adding batchId in package: " + batch.getId() + " in subscription Id " + bundle.getId());


                            List<String> mainTags = new ArrayList<>();
                            mainTags.add(language.toString());
                            mainTags.addAll(subscriptionPackageTypes.stream().map(Object::toString).collect(Collectors.toList()));
                            mainTags.add(course.getId());
                            mainTags.add(grade.toString());
                            mainTags.addAll(new ArrayList<>(subjects));
                            mainTags.addAll(targets);
                            mainTags.add(batch.isRecordedVideo() ? BundleDAO.AIOPACKAGE_TAGS_RECORDED : BundleDAO.AIOPACKAGE_TAGS_LIVE);
                            mainTags.add(batch.getStartTime().toString());
                            mainTags.add(batch.getEndTime().toString());
                            mainTags.add(batch.getStartTime().toString());
                            mainTags.addAll(convertToStringSearchTerms(batch.getSearchTerms()));

                            BundleEntity bundleEntity = new BundleEntity();
                            bundleEntity.setGrade(grade);
                            bundleEntity.setPackageType(PackageType.OTF);
                            bundleEntity.setEntityId(batch.getId());
                            bundleEntity.setEnrollmentType(EnrollmentType.CLICK_TO_ENROLL);
                            bundleEntity.setLanguage( language );
                            bundleEntity.setSubscriptionPackageTypes(subscriptionPackageTypes);
                            bundleEntity.setRecordedVideo(batch.isRecordedVideo());
                            bundleEntity.setSubjects(new ArrayList<>(subjects));
                            bundleEntity.setTargets(targets);
                            bundleEntity.setCourseId(course.getId());
                            bundleEntity.setStartTime(batch.getStartTime());
                            bundleEntity.setEndTime(batch.getEndTime());
                            bundleEntity.setSearchTerms(batch.getSearchTerms());
                            bundleEntity.setCourseTitle(course.getTitle());
                            bundleEntity.setSubscription(bundle.getIsSubscription());
                            bundleEntity.setBundleId(bundle.getId());
                            bundleEntity.setMainTags(mainTags);

                            try{
                                bundleEntityDAO.save(bundleEntity);
                            }catch (Exception ex){
                                logger.error("Error in saving bundleEntity for data {} with error {}", bundleEntity,ex);
                            }
                        }
                    }
                }
            }
        }

    }

    public List<SubscriptionBundleRes> getSubscriptionBundleInfo(String userId) throws InternalServerErrorException {
        List<BundleEnrolment> bundleEnrolmentList = bundleEnrolmentDAO.getUsersActiveAndInactiveEnrollment(userId);
        List<String> bundleIds =
                Optional.ofNullable(bundleEnrolmentList)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(BundleEnrolment::getBundleId)
                        .collect(Collectors.toList());
        List<Bundle> bundleList = bundleDAO.getSubscriptionBundlesFilterd(bundleIds);
        return bundleList.stream().map(SubscriptionBundleRes::new).collect(Collectors.toList());
    }

    public List<HomeFeedBundleResponse> getBundlesForMobileHomeFeed(GetBundlesRequest getBundlesRequest) throws VException {
        logger.info("GetBundleRequest : {}", getBundlesRequest);
        List<HomeFeedBundleResponse> bundles = bundleDAO.getBundlesForMobileHomeFeed(getBundlesRequest);
        List<String> filteredBundles = bundles.stream().map(HomeFeedBundleResponse::getId).collect(Collectors.toList());
        Map<String, BatchDetails> batchDetails = bundleEntityDAO.getBatchDetails(filteredBundles);

        List<HomeFeedBundleResponse> bundlesResp = new ArrayList<>();
        for (HomeFeedBundleResponse bundle : bundles)
        {
            if (batchDetails.containsKey(bundle.getId())) {

                if (bundle.getPackages() == null || bundle.getPackages().isEmpty()) {
                    List<AioPackage> packages = new ArrayList<>();
                    packages.add(new AioPackage());
                    bundle.setPackages(packages);
                }
                bundle.getPackages().get(0).setBatchDetails(batchDetails.get(bundle.getId()));
                bundlesResp.add(bundle);
            }
        }

        List<HomeFeedBundleResponse> finalBundlesResp = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(getBundlesRequest.getBundleIds()) && ArrayUtils.isNotEmpty(bundles)) {
            for (String bundleId : getBundlesRequest.getBundleIds()) {
                for (HomeFeedBundleResponse r : bundlesResp) {
                    if (r.getId().equals(bundleId)) {
                        finalBundlesResp.add(r);
                    }
                }
            }
        }
        return finalBundlesResp;
    }

    public List<MicroCourseCollectionResponse> getMicroCourseCollectionForHomeFeed(GetBundlesRequest getBundlesRequest) {
        List<String> displayTags = getBundlesRequest.getDisplayTags();
        String grade = getBundlesRequest.getGrade();
        List<MicroCourseCollectionResponse> microCourseCollectionResponses = new ArrayList<>();
        if (!displayTags.isEmpty() && !grade.isEmpty()) {
            for (String tag : displayTags) {
                MicroCourseCollectionResponse microCourseCollection = bundleDAO.getMicroCourseCollection(tag, grade);
                microCourseCollectionResponses.add(microCourseCollection);
            }
        } else {
            logger.error("Invalid GetBundleRequest");
        }
        return microCourseCollectionResponses;
    }

    public List<LandingPageCollectionResp> getGroups(GetAIOGroupReq req) {

        List<AIOGroup> aioGroups = aioGroupDAO.getGroups(req, false);
        List<String> bundleIds = new ArrayList<>();
        List<LandingPageCollectionResp> AIOGroupRespsMap = new ArrayList<>();
        Map<String, AIOGroupResp> AIOIdGroupRespsMap = new HashMap<>();
        if (!ArrayUtils.isEmpty(aioGroups)) {
            aioGroups.forEach(aioGroup -> {
                if (!ArrayUtils.isEmpty(aioGroup.getBundleIds())) {
                    aioGroup.setBundleIds(aioGroup.getBundleIds().subList(0, aioGroup.getBundleIds().size() <= 10 ? aioGroup.getBundleIds().size() : 10));
                    bundleIds.addAll(aioGroup.getBundleIds());
                }
            });
        } //
        else {
            return AIOGroupRespsMap;
        }
        //List<AIOGroupResp> AIOGroupResps = new ArrayList<>();
        List<Bundle> bundles = bundleDAO.getActiveValidBundlesByIds(bundleIds, Arrays.asList(Bundle.Constants.TITLE, Bundle.Constants.BUNDLE_DETAILS, Bundle.Constants.PRICE, Bundle.Constants.SUBJECT, Bundle.Constants.CUT_PRICE));
         Map<String,List<BundleEntity>> bundleEntityMap=  bundleEntityDAO.getBundleIdMapByBundleIds(bundleIds,Arrays.asList(BundleEntity.Constants.BATCH_DETAILS,BundleEntity.Constants.BUNDLE_ID));
        bundles.forEach(bundle -> {
            if ( bundle.getBundleDetails() != null && bundleEntityMap.containsKey(bundle.getId())) {
                BundleEntity microCourse = bundleEntityMap.get(bundle.getId()).get(0);
                AIOGroupResp groupResp = new AIOGroupResp(bundle.getId());
                groupResp.setId(bundle.getId());
                groupResp.setTitle(bundle.getTitle());
                if (microCourse.getBatchDetails() != null) {
                    if (microCourse.getBatchDetails().getCourseType() != null) {
                        groupResp.setCourseType(microCourse.getBatchDetails().getCourseType());
                    }
                    if (microCourse.getBatchDetails().getCourseStartTime() != 0l) {
                        groupResp.setCourseStartTime(microCourse.getBatchDetails().getCourseStartTime());
                    }
                    if (microCourse.getBatchDetails().getTeacherInfo() != null) {
                        groupResp.setTeacherInfo(microCourse.getBatchDetails().getTeacherInfo());
                    }
                }
                if (bundle.getBundleDetails().getBundleImageUrl() != null) {
                    groupResp.setBundleImageUrl(bundle.getBundleDetails().getBundleImageUrl());
                }
                if (bundle.getCutPrice() != null) {
                    groupResp.setCutPrice(bundle.getCutPrice());
                }
                if (bundle.getPrice() != null) {
                    groupResp.setPrice(bundle.getPrice());
                }
                if (bundle.getSubject() != null) {
                    groupResp.setSubjects(bundle.getSubject());
                }

                AIOIdGroupRespsMap.put(bundle.getId(), groupResp);
            }
        });

        aioGroups.forEach(aioGroup -> {
            List<AIOGroupResp> AIoGroupResps = new ArrayList<>();
            aioGroup.getBundleIds().forEach(bundleId -> {
                AIoGroupResps.add(AIOIdGroupRespsMap.get(bundleId));
            });
            AIOGroupRespsMap.add(new LandingPageCollectionResp(aioGroup.getId(), aioGroup.getHeading(), AIoGroupResps));
        });

        return AIOGroupRespsMap;
    }

    public void updateGroups(UpdateAIOGroupReq req) {
        req.getGroups().forEach(group -> {
            aioGroupDAO.save(mapper.map(group, AIOGroup.class), req.getCallingUserId());
        });

    }

    public List<AIOGroupResp> getMicroCoursesWithFilter(getMicroCoursesWithFilter req) {
        List<AIOGroupResp> AIoGroupResps = new ArrayList<>();

        List<String> bundleIds = new ArrayList<>();
        if (!ArrayUtils.isEmpty(req.getGroupIds())) {
            GetAIOGroupReq groupReq = new GetAIOGroupReq();
            groupReq.setGroupIds(req.getGroupIds());
            List<AIOGroup> aioGroups = aioGroupDAO.getGroups(groupReq, true);
            if (!ArrayUtils.isEmpty(aioGroups)) {
                aioGroups.forEach(aioGroup -> {
                    bundleIds.addAll(aioGroup.getBundleIds());
                });
                req.setBundleIds(bundleIds);
            }
        }

        logger.info("bundle ids from aio group {}", bundleIds);
        List<Bundle> bundles = bundleDAO.getMicroCoursesWithFilter(req, Arrays.asList(Bundle.Constants.TITLE, Bundle.Constants.BUNDLE_DETAILS, Bundle.Constants.PRICE, Bundle.Constants.SUBJECT, Bundle.Constants.CUT_PRICE));
        final Set<String> idsFiltered = bundles.stream().map(Bundle::getId).collect(Collectors.toSet());
        logger.info("bundle ids {}", idsFiltered);

        List<BundleEntity> bundleEntities=bundleEntityDAO.getAllMicroCourseEntities(idsFiltered, req);
        final Set<String> entitiesBundleIds = bundleEntities.stream().map(BundleEntity::getBundleId).collect(Collectors.toSet());
        logger.info("bundle entities size {}, bundle ids {}", bundleEntities.size(), entitiesBundleIds);


        Map<String, List<BundleEntity>> microCourseEntityMap =
                Optional.ofNullable(bundleEntities)
                        .orElseGet(ArrayList::new)
                        .stream()
                        .collect(
                                Collectors.groupingBy(
                                        BundleEntity::getBundleId,
                                        Collectors.mapping(Function.identity(), Collectors.toList())
                                )
                        );
        for (Bundle bundle : bundles) {
            if (microCourseEntityMap.containsKey(bundle.getId()) && bundle.getBundleDetails() != null) {
                BundleEntity bundleEntity=microCourseEntityMap.get(bundle.getId()).get(0);
                AIOGroupResp groupResp = new AIOGroupResp(bundle.getId());
                groupResp.setId(bundle.getId());
                groupResp.setTitle(bundle.getTitle());
                if (bundleEntity.getBatchDetails() != null) {
                    if (bundleEntity.getBatchDetails().getCourseType() != null) {
                        groupResp.setCourseType(bundleEntity.getBatchDetails().getCourseType());
                    }
                    if (bundleEntity.getBatchDetails().getCourseStartTime() != 0l) {
                        groupResp.setCourseStartTime(bundleEntity.getBatchDetails().getCourseStartTime());
                    }
                    if (bundleEntity.getBatchDetails().getTeacherInfo() != null) {
                        groupResp.setTeacherInfo(bundleEntity.getBatchDetails().getTeacherInfo());
                    }
                }
                if (bundle.getBundleDetails().getBundleImageUrl() != null) {
                    groupResp.setBundleImageUrl(bundle.getBundleDetails().getBundleImageUrl());
                }
                if (bundle.getCutPrice() != null) {
                    groupResp.setCutPrice(bundle.getCutPrice());
                }
                if (bundle.getPrice() != null) {
                    groupResp.setPrice(bundle.getPrice());
                }
                if (bundle.getSubject() != null) {
                    groupResp.setSubjects(bundle.getSubject());
                }
                AIoGroupResps.add(groupResp);
            }
        }
        return AIoGroupResps;
    }

    public List<AIOGroup> getGroupsByGrade(GetAIOGroupReq req) {
        return aioGroupDAO.getGroups(req, true);
    }

    public List<AIOGroup> getGroupsNameHeading(GetAIOGroupReq req) {
        return aioGroupDAO.getGroups(req, Arrays.asList(AIOGroup.Constants.HEADING));
    }

    public EnrolledBundlesRes getEnrolledBundlesHomeFeed(List<String> bundleIds, String userId) {
        return bundleEnrolmentDAO.getEnrolledBundlesHomeFeed(bundleIds, userId);
    }

    public PlatformBasicResponse editMultipleBundles(EditMultipleBundleReq req) throws BadRequestException, InternalServerErrorException {
        List<String> bundleIds = req.getBundleIds();
        for (String bundleId : bundleIds) {
            Bundle bundle = editBundle(bundleId);
            bundleDAO.save(bundle, req.getCallingUserId());
        }
        return new PlatformBasicResponse();
    }

    private Bundle editBundle(String bundleId) {
        logger.info("Edit Bundle - bundleId :" + bundleId);
        Bundle bundle = bundleDAO.getBundleById(bundleId);

        List<AioPackage> packages = bundle.getPackages();
//        cleanAioPackage(
//        bundle.setPackages(packages);
        List<String> courseIds = new ArrayList<>();
        List<String> batchIds = new ArrayList<>();
        for (AioPackage aioPackage : packages) {
            courseIds.add(aioPackage.getCourseId());
            batchIds.add(aioPackage.getEntityId());
        }
        Map<String, CourseBasicInfo> courseMap = enrollmentManager.getCourseMap(courseIds, false);
        Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(batchIds, false);
        //       Map<String,Integer>    courseCountMap =  getCountMap(packages);
        packages.forEach(aioPackage -> {
            String entityId = aioPackage.getEntityId();
            BatchBasicInfo batch = batchMap.get(entityId);
            Long startTime = batch.getStartTime();
            Long endTime = batch.getEndTime();
            aioPackage.setStartTime(startTime);
            aioPackage.setEndTime(endTime);
            if (null != aioPackage.getMainTags()) {
                aioPackage.getSearchTerms().forEach(term -> {
                    aioPackage.getMainTags().remove(term.toString());
                });
                aioPackage.getMainTags().addAll(convertToStringSearchTerms(batch.getSearchTerms()));
            }
            if (null != aioPackage.getSearchTerms()) {
                aioPackage.setSearchTerms(batch.getSearchTerms());
            }

            CourseBasicInfo courseBasicInfo = courseMap.get(aioPackage.getCourseId());
            if (courseBasicInfo != null) {
                if (!StringUtils.isEmpty(courseBasicInfo.getTitle())) {
                    aioPackage.setCourseTitle(courseBasicInfo.getTitle());
                }
            }
            // aioPackage.setCourseCount(courseCountMap.get(aioPackage.getCourseId()));
        });

        return bundle;
    }

    private Map<String, Integer> getCountMap(List<AioPackage> packages) {
        Map<String, Integer> courseCountMap = new HashMap<>();
        packages.forEach(aioPackage -> {
            if (courseCountMap.containsKey(aioPackage.getCourseId())) {
                Integer count = courseCountMap.get(aioPackage.getCourseId());
                count++;
            } else {
                courseCountMap.put(aioPackage.getCourseId(), 1);
            }

        });
        return courseCountMap;
    }


    private List<AioPackage> cleanAioPackage(List<AioPackage> packages) {
        List<AioPackage> aioPackages = new ArrayList<>();
        Set<String> batchIdSet = new HashSet<>();

        for (AioPackage aioPackage : packages) {
            String batchId = aioPackage.getEntityId();
            if (!batchIdSet.contains(batchId)) {
                batchIdSet.add(batchId);
                aioPackages.add(aioPackage);
            }
        }

        return aioPackages;
    }

    private List<String> convertToStringSearchTerms(List<CourseTerm> searchTerms) {
        List<String> searchTermsList = new ArrayList<>();
        for (CourseTerm courseTerm : searchTerms) {
            searchTermsList.add(courseTerm.toString());
        }
        return searchTermsList;
    }

    public MicroCourseBasicInfo getMicroCourseByBundle(GetBundleTestVideoReq req) throws Throwable {
        MicroCourseBasicInfo microCourseBasicInfo = new MicroCourseBasicInfo();
        BundleInfo bundle = getCachedBundleBasicInfo(req.getBundleId());
        if (bundle == null) {
            return microCourseBasicInfo;
        }
        BundleEntity bundleEntity=bundleEntityDAO.getMicroCourseEntity(req.getBundleId());
        List<String> batchIds = new ArrayList<>();
        if (Objects.nonNull(bundleEntity)) {
            String batchId = bundleEntity.getEntityId();
            batchIds.add(batchId);
            Map<String, BatchBasicInfo> batchBasicInfoMap = enrollmentManager.getBatchMap(batchIds, false);
            BatchBasicInfo batch = batchBasicInfoMap.get(batchId);
            microCourseBasicInfo.setAioPackage(bundleEntity);
            microCourseBasicInfo.setBatchId(batchId);
            microCourseBasicInfo.setCutPrice(bundle.getCutPrice());
            microCourseBasicInfo.setTarget(bundle.getTarget());
            microCourseBasicInfo.setPrice(bundle.getPrice());
            microCourseBasicInfo.setDescription(bundle.getDescription());
            microCourseBasicInfo.setTitle(bundle.getTitle());
            microCourseBasicInfo.setStartTime(batch.getStartTime());
            microCourseBasicInfo.setEndTime(batch.getEndTime());
            microCourseBasicInfo.setDuration(batch.getDuration());
            microCourseBasicInfo.setSessionPlan(batch.getSessionPlan());
            microCourseBasicInfo.setIsRecordedVideo(batch.isRecordedVideo());
            microCourseBasicInfo.setDurationtime(bundle.getDurationtime());
            microCourseBasicInfo.setValidTill(bundle.getValidTill());
            microCourseBasicInfo.setSearchTerms(bundle.getSearchTerms());
        }
        return microCourseBasicInfo;
    }

    public PlatformBasicResponse deEnrollSubscriptionBatch(deEnrollSubscriptionBatch req) {

        // getuser id from session
        if(sessionUtils.isStudent(sessionUtils.getCurrentSessionData())) {
            String userId = String.valueOf(sessionUtils.getCallingUserId());
            if(null != userId && !req.getUserId().equals(userId)){
                return new PlatformBasicResponse();
            }
        }

        List batchIds = new ArrayList();
        batchIds.add(req.getBatchId());
        List<Enrollment> userEnrollments = enrollmentDAO.getEnrollmentByBatchAndUser(batchIds, req.getUserId(), Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE));

        if (ArrayUtils.isNotEmpty(userEnrollments)) {
            Enrollment enrollment = userEnrollments.get(0);
            List<Bundle> bundles = bundleDAO.getBundleById(enrollment.getPurchaseContextId(), Arrays.asList(Bundle.Constants._ID));
            if (ArrayUtils.isNotEmpty(bundles)) {

                try {
//  1 status change log
                    StatusChangeTime statusChangeTime = new StatusChangeTime();

                    statusChangeTime.setChangeTime(System.currentTimeMillis());
                    statusChangeTime.setNewStatus(EntityStatus.ENDED);
                    statusChangeTime.setPreviousStatus(enrollment.getStatus());
                    if (ArrayUtils.isEmpty(enrollment.getStatusChangeTime())) {
                        enrollment.setStatusChangeTime(Arrays.asList(statusChangeTime));
                    } else {
                        enrollment.getStatusChangeTime().add(statusChangeTime);
                    }
                    enrollment.setEndReason("DEENROLLED_BY_USER");
                    enrollment.setEndTime(System.currentTimeMillis());
                    enrollment.setStatus(EntityStatus.ENDED);
                    enrollment.setIsDeEnrolled(true);
                    //2                   save

                    enrollmentDAO.save(enrollment);


// 3                 batch seat update
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("enrollments", Arrays.asList(enrollment));
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_BATCH_SEAT, payload);
                    asyncTaskFactory.executeTask(params);

//  4                calander update


                    logger.info("updating calender");
                    Map<String, Object> calPayload = new HashMap<>();
                    calPayload.put("userId", enrollment.getUserId());
                    calPayload.put("batchId", enrollment.getBatchId());
                    AsyncTaskParams calParams = new AsyncTaskParams(AsyncTaskName.OTF_UNMARK_CALENDAR, calPayload);
                    asyncTaskFactory.executeTask(calParams);


                    //     5 remove gtt

                    logger.info("updating GTT");
                    Map<String, Object> gttPayload = new HashMap<>();
                    gttPayload.put("userId", enrollment.getUserId());
                    gttPayload.put("batchId", enrollment.getBatchId());
                    EnrollmentPojo pojo = new EnrollmentPojo();
                    pojo.setId(enrollment.getId());
                    pojo.setStatus(EntityStatus.ENDED);
                    gttPayload.put("enrolmentInfo", gson.toJson(pojo));
                    AsyncTaskParams gttParams = new AsyncTaskParams(AsyncTaskName.REMOVE_GTT, gttPayload);
                    asyncTaskFactory.executeTask(gttParams);

                    Map<String, Object> enrolPayload = new HashMap<>();
                    enrolPayload.put("enrollment", enrollment);
                    enrolPayload.put("event", BatchEventsOTF.USER_ENROLLED);
                    AsyncTaskParams enrollParams = new AsyncTaskParams(AsyncTaskName.ENROLLMENT_EVENTS_TRIGGER, enrolPayload);
                    asyncTaskFactory.executeTask(enrollParams);

                    logger.info("Sending enrollmentId to sqs queue");
                    Map<String, String> enrollmentIdMap = new HashMap<>();
                    enrollmentIdMap.put("enrollmentId", enrollment.getId());
                    sqsManager.sendToSQS(SQSQueue.END_ENROLLMENT_OPS, SQSMessageType.PUSH_ENDED_ENROLLMENTS, new Gson().toJson(enrollmentIdMap));

                } catch (Exception e) {
                    logger.warn("error in ending the enrollment the user during endBundleEnrollment " + e.getMessage());
                }
            }


        }

        return new PlatformBasicResponse();
    }


    public List<SubscriptionCoursesRes> getSimilarBatchOfSubscription(String userId, String courseId) {
        logger.info("userId - {}, courseId - {}",userId,courseId);
        List<BundleEnrolment> bundleEnrolmentList = bundleEnrolmentDAO.getUsersActiveAndInactiveEnrollment(userId);
        logger.info("All the bundle enrollment received for the students are:");
        List<String> bundleIds = new ArrayList<>();
        List<String> activeBundleList = new ArrayList<>();
        List<String> regularBundleIds = new ArrayList<>();
        for (BundleEnrolment bundleEnrolment : bundleEnrolmentList) {
            logger.info("BundleEnrolment - {}",bundleEnrolment);
            bundleIds.add(bundleEnrolment.getBundleId());
            if (EntityStatus.ACTIVE.equals(bundleEnrolment.getStatus())) {
                activeBundleList.add(bundleEnrolment.getBundleId());
                if (EnrollmentState.REGULAR.equals(bundleEnrolment.getState())) {
                    regularBundleIds.add(bundleEnrolment.getBundleId());
                }
            }
        }
        logger.info("bundleIds - {}",bundleIds);
        logger.info("activeBundleList - {}",activeBundleList);
        logger.info("regularBundleIds - {}",regularBundleIds);
        List<String> courseIds = new ArrayList<>(Collections.singletonList(courseId));
        logger.info("courseIds as of now is {}",courseIds);

        GetUserEnrollmentsReq enrollmentsReq = new GetUserEnrollmentsReq(Long.parseLong(userId),false);
        List<Enrollment> enrollmentList = enrollmentDAO.getBatchIdsOfActiveEnrollmentsOfUser(enrollmentsReq);
        logger.info("All the active enrollment of the user are for the batch ids");
        List<String> enrolledBatchIds = enrollmentList.stream().map(Enrollment::getBatchId).peek(logger::info).collect(Collectors.toList());

        List<Bundle> regularBundles = bundleDAO.getSubscriptionBundlesFilterd(regularBundleIds);
        SubscriptionCoursesReq req = new SubscriptionCoursesReq(courseIds,bundleIds,regularBundles);
        List<BundleEntity> batchesByBundles = bundleEntityDAO.getBatchesByBundles(req);

        Map<String, String> batchBundleMap = new HashMap<>();
        logger.info("Data of batches by bundle");
        for (BundleEntity batchesByBundle : batchesByBundles) {
            logger.info("batchesByBundle - {}",batchesByBundle);
            if (batchBundleMap.containsKey(batchesByBundle.getEntityId())) {
                if (activeBundleList.contains(batchesByBundle.getBundleId())) {
                    batchBundleMap.put(batchesByBundle.getEntityId(), batchesByBundle.getBundleId());
                }
            } else {
                batchBundleMap.put(batchesByBundle.getEntityId(), batchesByBundle.getBundleId());
            }
        }
        Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(batchBundleMap.keySet(), false);
        courseIds.addAll(batchMap.values().stream().map(BatchBasicInfo::getCourseId).collect(Collectors.toList()));
        Map<String, CourseBasicInfo> courseMap = enrollmentManager.getCourseMap(courseIds, false);
        List<SubscriptionCoursesRes> courseListResponse = new ArrayList<>();
        for (String batchId : batchMap.keySet()) {
            BatchBasicInfo batchInfo = batchMap.get(batchId);
            CourseBasicInfo courseInfo = courseMap.get(batchInfo.getCourseId());
            List<String> subjects = courseInfo.getBoardTeacherPairs().stream().map(BoardTeacherPair::getSubject).collect(Collectors.toList());
            Long currentTimeMillis = System.currentTimeMillis();
            BatchStatus batchStatus;
            if (batchInfo.getEndTime() > currentTimeMillis) {
                batchStatus= batchInfo.getStartTime() > currentTimeMillis ? BatchStatus.UPCOMING : BatchStatus.ONGOING;
            } else {
                batchStatus=BatchStatus.FINISH;
            }
            SubscriptionCoursesRes response = new SubscriptionCoursesRes(
                    batchInfo.getId(),
                    batchInfo.getCourseId(),
                    batchBundleMap.get(batchInfo.getBatchId()),
                    batchInfo.getSessionPlan(),
                    courseInfo.getTitle(),
                    subjects,
                    batchInfo.getStartTime(),
                    batchInfo.getEndTime(),
                    batchInfo.getTeachers(),
                    enrolledBatchIds.contains(batchInfo.getBatchId()),
                    batchInfo.isRecordedVideo(),
                    batchStatus
            );

            courseListResponse.add(response);
        }
        return courseListResponse;
    }


    public PlatformBasicResponse addUserIdToBundleMinMaxExceptionList(Long userId) throws BadRequestException, NotFoundException, ForbiddenException {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, false);
        if (userBasicInfo == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User not found");
        }
        PlatformBasicResponse platformBasicResponse = isUserPartOfMinMaxExceptionList(userId);
        if (platformBasicResponse.isSuccess()) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User already existed in BundleMinMaxExceptionList");
        }
        BundleMinMaxExceptionList bundleMinMaxExceptionList = new BundleMinMaxExceptionList();
        if (userBasicInfo.getUserId() != null) {
            bundleMinMaxExceptionList.setUserId(userBasicInfo.getUserId());
        }
        bundleDAO.createBundleMinMaxException(bundleMinMaxExceptionList, userBasicInfo.getUserId());
        return new PlatformBasicResponse(true, "Inserted successfully", null);
    }

    public PlatformBasicResponse isUserPartOfMinMaxExceptionList(Long userId) throws BadRequestException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "UserId can't be empty");
        }
        BundleMinMaxExceptionList bundleMinMaxExceptionList = bundleDAO.getBundleMinMaxExceptionByUserId(userId);
        if (bundleMinMaxExceptionList != null) {
            return new PlatformBasicResponse(true, "User existed", null);
        }
        return new PlatformBasicResponse(false, null, "User not existed");
    }

    public PlatformBasicResponse deleteUserFromBundleMinMaxExceptionList(Long userId) throws BadRequestException, NotFoundException {
        PlatformBasicResponse platformBasicResponse = isUserPartOfMinMaxExceptionList(userId);
        if (!platformBasicResponse.isSuccess()) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "User not existed in BundleMinMaxException");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleMinMaxExceptionList.Constants.USERID).is(userId));
        query.addCriteria(Criteria.where(BundleMinMaxExceptionList.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        Update update = new Update();
        update.set(BundleMinMaxExceptionList.Constants.ENTITY_STATE, EntityState.DELETED);
        bundleDAO.updateFirst(query, update, BundleMinMaxExceptionList.class);
        return new PlatformBasicResponse(true, "deleted successfully", null);
    }

    public String getBundleMessageStrip(String key) {
        String messageStrip = "";
        try {
            messageStrip = redisDAO.get(key);
        } catch (Exception e) {
            logger.info("Could not get messageStrip from Redis");
        }

        return messageStrip;
    }

    public BundleWrapper bundleToBundleWrapper(Bundle bundle) {
        return mapper.map(bundle, BundleWrapper.class);
    }

    public void updateBundleStateBulk(UpdateBundleStateBulk req) throws BadRequestException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Bundle.Constants._ID).in(req.getBundleIds()));
        query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).ne(true));
        Update update = new Update();
        if (req.getNewState() != null) {
            BundleStateChangeTime bundleStateChangeTime = new BundleStateChangeTime(System.currentTimeMillis(), req.getCallingUserId(), req.getNewState());
            //  query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_STATE).ne(req.getNewState()));
            update.addToSet(Bundle.Constants.STATE_CHANGE_TIME, bundleStateChangeTime);
            update.set(Bundle.Constants.BUNDLE_STATE, req.getNewState());
        }
        if (req.getValidTill() != null) {

            BundleValidTillChangeTime bundleValidTillChangeTime = new BundleValidTillChangeTime(System.currentTimeMillis(), req.getCallingUserId(), req.getValidTill());
            update.addToSet(Bundle.Constants.VALID_TILL_CHANGE_TIME, bundleValidTillChangeTime);
            update.set(Bundle.Constants.VALID_TILL, req.getValidTill());
        }
        update.set(Bundle.Constants.LAST_UPDATED, System.currentTimeMillis());
        update.set(Bundle.Constants.LAST_UPDATED_BY, req.getCallingUserId());
        bundleDAO.updateMultiple(query, update);
    }

    public void endBundleBatchEnrollment(Enrollment enrollment) {
        if (enrollment == null) {
            logger.error("can not end enrollment -- enrollment is null");
            return;
        }
        try {
            logger.info("doing for enrollment " + enrollment);
            OTFEnrollmentReq oTFEnrollmentReq = mapper.map(enrollment, OTFEnrollmentReq.class);
            oTFEnrollmentReq.setStatus(EntityStatus.ENDED.name());
            logger.info(" updateEnrollment " + oTFEnrollmentReq);
            enrollment = enrollmentManager.endBatchEnrollmentWithoutAsync(oTFEnrollmentReq);
            logger.info("enrollment after mark status " + enrollment);
            String enrollmentId = enrollment.getId();
            Map<String, String> payload = new HashMap<>();
            payload.put("enrollmentId", enrollmentId);
            sqsManager.sendToSQS(SQSQueue.END_ENROLLMENT_OPS, SQSMessageType.PUSH_ENDED_ENROLLMENTS, new Gson().toJson(payload));

            logger.info("updating calendar");
            if (!Role.STUDENT.equals(enrollment.getRole())) {
                enrollmentAsyncManager.updateCalendar(enrollment.getUserId(), enrollment.getBatchId(), enrollment.getStatus());
            }

            Map<String, String> gttPayload = new HashMap<>();
            gttPayload.put("userId", enrollment.getUserId());
            gttPayload.put("batchId", enrollment.getBatchId());
            EnrollmentPojo pojo = new EnrollmentPojo();
            pojo.setId(enrollment.getId());
            pojo.setStatus(enrollment.getStatus());
            pojo.setCreationTime(enrollment.getCreationTime());
            pojo.setLastUpdated(System.currentTimeMillis());
            gttPayload.put("enrolmentInfo", gson.toJson(pojo));
            sqsManager.sendToSQS(SQSQueue.GTT_ENROLMENT_OPS, SQSMessageType.GTT_ENROLMENT_STATUS_CHANGE_TASK, gson.toJson(gttPayload), enrollment.getUserId());
        } catch (Exception e) {
            logger.warn("error in ending the enrollment during endBundleEnrollment " + e.getMessage());
        }
    }

    public void saveSubscriptionProgram(addSubscriptionProgramReq req) throws Throwable {
        SubscriptionProgram subscriptionProgram = mapper.map(req, SubscriptionProgram.class);
        subscriptionProgramDAO.save(subscriptionProgram, req.getCallingUserId());


    }


    public SubscriptionProgramResp getSubscriptionProgram(SubscriptionProgramReq req) throws Throwable {

        List<SubscriptionProgram> subscriptionPrograms = subscriptionProgramDAO.getPrograms(req.getGrade(), req.getTarget(), req.getYear(), req.isDisplayHidden());
        SubscriptionProgramResp resp = null;
        if (ArrayUtils.isNotEmpty(subscriptionPrograms)) {
            String liteBundleId = "";
            SubscriptionProgram program = subscriptionPrograms.get(0);
            resp = mapper.map(program, SubscriptionProgramResp.class);
            if (ArrayUtils.isNotEmpty(program.getSubscriptionPlans())) {
                List<String> bundleIds = new ArrayList<>();
                String trailBundle = null;
                List<TrialInfo> trialInfosList = new ArrayList<>();
                for (SubscriptionPlan subscriptionPlan : program.getSubscriptionPlans()) {
                    bundleIds.add(subscriptionPlan.getBundleId());
                    if (subscriptionPlan.getIsTrial()) {
                        trailBundle = subscriptionPlan.getBundleId();

                    }
                    if(SubscriptionPlanType.LITE.equals( subscriptionPlan.getPlanType())){
                        liteBundleId=subscriptionPlan.getBundleId();
                    }
                }

                if (ArrayUtils.isNotEmpty(bundleIds)) {

                    Set<Integer> grades = new HashSet<>();
                    Set<String> subjects = new HashSet<>();
                    Set<String> targets = new HashSet<>();
                    List<Bundle> bundleList = bundleDAO.getBundlesByIds(bundleIds, Arrays.asList(Bundle.Constants.GRADE, Bundle.Constants.TARGET, Bundle.Constants.SUBJECT, Bundle.Constants.PRICE, Bundle.Constants.CUT_PRICE, Bundle.Constants.TRIAL_IDS));


                    Map<String, Bundle> bundleMap = new HashMap<>();
                    bundleList.forEach(bundle -> {
                        bundleMap.put(bundle.getId(), bundle);
                        grades.addAll(bundle.getGrade());
                        subjects.addAll(bundle.getSubject());
                        targets.addAll(bundle.getTarget());
                    });


                    if (!StringUtils.isEmpty(liteBundleId)) {

                        if (bundleMap.containsKey(liteBundleId)) {
                            if (ArrayUtils.isNotEmpty(bundleMap.get(liteBundleId).getTrialIds())) {
                                List<Trial> trialList = trialDAO.getByIds(bundleMap.get(liteBundleId).getTrialIds());
                                trialList.forEach(trial -> {
                                    trialInfosList.add(mapper.map(trial, TrialInfo.class));

                                });
                                resp.setTrialList(trialInfosList);
                            }
                        }
                    }

                    resp.getSubscriptionPlans().forEach(subscriptionPlan -> {
                        if (bundleMap.containsKey(subscriptionPlan.getBundleId())) {
                            Bundle bundle = bundleMap.get(subscriptionPlan.getBundleId());
                            subscriptionPlan.setPrice(bundle.getPrice());
                            subscriptionPlan.setCutPrice(bundle.getCutPrice());
                        }
                    });


                    resp.setDisplayGrades(grades);
                    resp.setDisplaySubjects(subjects);
                    resp.setDisplayTargets(targets);
                }
            }


            if (StringUtils.isEmpty(resp.getOffer()) &&  !req.isDisplayHidden()) {
                String offerStrip = getBundleMessageStrip("STRIP_COURSE_DETAIL");
                if (!StringUtils.isEmpty(offerStrip)) {
                    resp.setOffer(offerStrip);
                }
            }
            if(req.isIncludeCurriculum() && !StringUtils.isEmpty(liteBundleId)){
                List<BundleEntity>    bundleBatchFilterResList = bundleDAO.getLongTermCoursesBySubscription(Arrays.asList(liteBundleId));
                if(ArrayUtils.isNotEmpty( bundleBatchFilterResList)){

                    List<String> courseIds = new ArrayList<>();

                    bundleBatchFilterResList.forEach(bundleBatchFilterRes -> {
                        if(!StringUtils.isEmpty(bundleBatchFilterRes.getId())) {
                            courseIds.add(bundleBatchFilterRes.getId());
                        }
                    });
                    List<Course> courseList = courseDAO.getCourseCurriculum(courseIds);
                    Map<String ,List<String>> courseCurriculumMap = new HashMap<>();
                    if(ArrayUtils .isNotEmpty( courseList)){

                        courseList.forEach(course -> {
                            if(ArrayUtils.isNotEmpty( course.getNormalizeSubjects()) && ArrayUtils.isNotEmpty(course.getCurriculum()) ) {
                                List<String> topics = new ArrayList<>();
                                logger.info( "===============1====    " + course.getCurriculum().toString() + "");
                                course.getCurriculum().forEach(curriculumPojo -> {
                                    logger.info( "==============2=====    " + curriculumPojo.toString() + "");
                                    if (ArrayUtils.isNotEmpty(curriculumPojo.getSubTopics())) {
                                        topics.addAll(curriculumPojo.getSubTopics());
                                    }
                                });
                                courseCurriculumMap.put((String) course.getNormalizeSubjects().toArray()[0], topics);

                            }
                        });
                        resp.setCourseCurriculumMap(courseCurriculumMap);
                    }

                }


            }
        }


        return resp;

    }


    public List<String> getSubscriptionProgramForGrade(Integer grade) throws Throwable {

        List<SubscriptionProgram> subscriptionPrograms = subscriptionProgramDAO.getPrograms(grade, null, null, false);
        List<String> targets = new ArrayList<>();
        SubscriptionProgramResp resp = null;
        if (ArrayUtils.isNotEmpty(subscriptionPrograms)) {
            subscriptionPrograms.forEach(program -> {
                targets.add(program.getTarget() + " " + program.getYear().toString());
            });
        }

        return targets;

    }

    public BundleInfo getBundleInfoForOnboarding(String userId, String bundleId) {
        Bundle bundle = getBundle(bundleId);
        if (bundle != null && bundle.getIsSubscription() && bundleEnrolmentDAO.isValidEnrolmentForOnboarding(userId, bundleId, ONBOARDING_TIME)) {
            return mapper.map(bundle, BundleInfo.class);
        }

        return null;
    }


    public boolean hasLongTermEnrollment(String userId) throws Throwable {

        List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO.getUsersNonEndedEnrolment(userId, Arrays.asList(BundleEnrolment.Constants.BUNDLE_ID));
        List<String> bundleIds = new ArrayList<>();
        if (ArrayUtils.isEmpty(bundleEnrolments)) {
            return false;
        }
        bundleEnrolments.forEach(enrollment -> {
            bundleIds.add(enrollment.getBundleId());
        });

        long longTermCount = bundleDAO.getLongTermBundleCount(bundleIds);

        return longTermCount > 0;


    }

    public void _autoEnrollInBundleCoursesParallel(Map<String, String> enrollmentIdUserIdMap, String enrolledBundleId,
                                                   BundleEntity bundleEntity, EntityStatus enrollmentStatus, List<String> batchIds,
                                                   OTFBundleInfo oTFBundleInfo, Map<String, String> batchIdCourseIdMap) {

        enrollmentIdUserIdMap.keySet().parallelStream().forEach(enrollmentId-> {
            try {
                _autoEnrollInBundleCourses(enrollmentIdUserIdMap.get(enrollmentId),enrolledBundleId,bundleEntity,enrollmentStatus,enrollmentId,batchIds,oTFBundleInfo,batchIdCourseIdMap);
            } catch (VException e) {
                logger.error("_autoEnrollInBundleCourses",e);
            }
        });
    }

    public void migrateBundleDataForSegregation() {
        long skip=0;
        int limit=10;
        while(true){
            List<Bundle> bundles=bundleDAO.getBundlesForMigration(skip,limit);
            skip+=limit;
            if(ArrayUtils.isEmpty(bundles)){
                break;
            }
            logger.info("migrateBundleDataForSegregation - skip - {} - limit - {}",skip,limit);
            logger.info("bundleEntities");
            List<BundleEntity> bundleEntities =
                    bundles
                            .stream()
                            .filter(this::isPackagesPresentInBundle)
                            .map(this::mapToBundlePackageReferenceData)
                            .flatMap(Collection::stream)
                            .peek(logger::info)
                            .collect(Collectors.toList());
            bundleEntityDAO.insertAllEntities(bundleEntities);
            logger.info("After inserting all the entities to the database");
            bundleEntities.stream().collect(
                    Collectors.groupingBy(
                            BundleEntity::getBundleId,
                            Collectors.mapping(BundleEntity::getId, Collectors.toSet())
                    )
            ).forEach(bundleDAO::updatePackageReferenceDetailsForBundle);
            bundles.stream().map(Bundle::getId).collect(Collectors.toSet()).forEach(bundleId->bundleDAO.updatePackageReferenceDetailsForBundle(bundleId,new HashSet<>()));
        }
    }

    private boolean isPackagesPresentInBundle(Bundle bundle) {
        return ArrayUtils.isNotEmpty(bundle.getPackages());
    }

    private List<BundleEntity> mapToBundlePackageReferenceData(Bundle bundle) {
        return bundle.getPackages()
                .stream()
                .map(this::getBundlePackageReference)
                .map(obj->obj.populateRequiredBundleData(bundle))
                .collect(Collectors.toList());
    }

    private BundleEntity getBundlePackageReference(AioPackage aioPackage) {
        BundleEntity bundleEntity = mapper.map(aioPackage, BundleEntity.class);
        if(PackageType.OTF.equals(aioPackage.getPackageType())) {
            Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(Collections.singletonList(aioPackage.getEntityId()), false);
            BatchBasicInfo batchBasicInfo = batchMap.get(aioPackage.getEntityId());
            if(Objects.nonNull(batchBasicInfo)){
                bundleEntity.setCourseId(batchBasicInfo.getCourseId());
            }else{
                bundleEntity.setCourseId("DummyId");
            }
        }
        bundleEntity.setCreationTime(System.currentTimeMillis());
        bundleEntity.setCreatedBy(httpSessionUtils.getCallingUserId().toString());
        bundleEntity.setLastUpdated(System.currentTimeMillis());
        bundleEntity.setLastUpdatedBy(httpSessionUtils.getCallingUserId().toString());
        return bundleEntity;
    }

    public Boolean isBatchPartOfSubscriptionAIO(String userId,String batchId) throws BadRequestException {
        if(StringUtils.isEmpty(userId) || StringUtils.isEmpty(batchId))
            return Boolean.FALSE;

        List<String> bundleIds=bundleEnrolmentDAO.getBundleIdsOfAllActiveAndInactiveEnrollmentOfUser(userId);

        if(ArrayUtils.isNotEmpty(bundleIds)) {
            long bundleCount = bundleEntityDAO.getCountOfBatchAndBundlesForSubscription(bundleIds,batchId);
            if (bundleCount > 0)
                return Boolean.TRUE;
        }
        return Boolean.FALSE;
    }




    /*    public Boolean updateSubscription(updateSubscription req) throws VException {


        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();

        int amountToPay = 0;

        BaseSubscriptionPlanResp baseSubscriptionPlanResp= getBaseSubscriptionPlan(req.getNewSubscriptionPlanId());
        if(baseSubscriptionPlanResp == null){
            return false;
        }
        amountToPay = baseSubscriptionPlanResp.getPrice();

        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(baseSubscriptionPlanResp.getPurchaseEntityId());
        buyItemsReqNew.setEntityType(com.vedantu.session.pojo.EntityType.OTF);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setPaymentType(PaymentType.BULK);
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());
        buyItemsReqNew.setUtm_campaign(req.getUtm_campaign());
        buyItemsReqNew.setUtm_content(req.getUtm_content());
        buyItemsReqNew.setUtm_medium(req.getUtm_medium());
        buyItemsReqNew.setUtm_source(req.getUtm_source());
        buyItemsReqNew.setUtm_term(req.getUtm_term());
        buyItemsReqNew.setChannel(req.getChannel());

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);

        if(orderInfo.getNeedRecharge()){
            //orderInfo.get
        }




        return true;
    }*/


    public BaseSubscriptionPlanResp getBaseSubscriptionPlan(String subscriptionPlanId ) throws VException {

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseSubscriptionPlan?subscriptionId=" + subscriptionPlanId ;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of getBaseSubscriptionPlan " + jsonString);

        BaseSubscriptionPlanResp res = new Gson().fromJson(jsonString, BaseSubscriptionPlanResp.class);
        return res;
    }


    public SubscriptionPlanInfoResp getBaseSubscriptionPlanByDeliverableId(String deliverableId ) throws VException {

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseSubscriptionPlanByDeliverableId?deliverableId=" + deliverableId ;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of getBaseSubscriptionPlan " + jsonString);

        SubscriptionPlanInfoResp res = new Gson().fromJson(jsonString, SubscriptionPlanInfoResp.class);
        return res;
    }


    public FullSubscriptionPlanResp getFullSubscriptionPlan(Integer grade, String target, Integer year) throws Throwable {
        FullSubscriptionPlanResp resp =new FullSubscriptionPlanResp();
        List<SubscriptionProgram> subscriptionPrograms = subscriptionProgramDAO.getPrograms(grade, target, year,false);
        Map<String,String> subscriptionPlanBundleMap = new HashMap<>();
        Set<String> bundleIds = new HashSet<>();
        List<String> trailBundles = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(subscriptionPrograms)){
            subscriptionPrograms.get(0).getSubscriptionPlans().forEach(subscriptionPlan -> {
                subscriptionPlanBundleMap.put(subscriptionPlan.getBundleId(),subscriptionPlan.getPlanType().toString());
                if(subscriptionPlan.getIsTrial()){
                    trailBundles.add( subscriptionPlan.getBundleId());
                }
                resp.setSubscriptionOfferStrip( subscriptionPrograms.get(0).getOffer());
            });

            bundleIds.addAll(subscriptionPlanBundleMap.keySet());

            List<BaseSubscriptionPlanResp>  baseSubscriptionPlanRespList =    getBaseSubscriptionPlanByBundles(new ArrayList<>(bundleIds));
            List<Integer> liteMonths = new ArrayList<>();
            Map<String,List<BaseSubscriptionPlanResp> > bundleIdSubscriptionMap = new HashMap<>();

            baseSubscriptionPlanRespList.forEach(baseSubscriptionPlanResp -> {
                liteMonths.add(baseSubscriptionPlanResp.getValidMonths());
            });
            logger.info(liteMonths.toString() + "");

            baseSubscriptionPlanRespList.forEach(baseSubscriptionPlanResp -> {
                //      logger.info(monthCountMap.get(baseSubscriptionPlanResp.getValidMonths()) + "");

                if(liteMonths.contains(baseSubscriptionPlanResp.getValidMonths()) ){
                    logger.info("fullcourse bundle id" + baseSubscriptionPlanResp.getPurchaseEntityId());
                    if (baseSubscriptionPlanResp.getPlanDuration() != null && baseSubscriptionPlanResp.getPlanDuration().equals(BaseSubscriptionDuration.FULLCOURSE)) {
                        Bundle bundle = bundleDAO.getBundleById(baseSubscriptionPlanResp.getPurchaseEntityId());
                        baseSubscriptionPlanResp.setAioValidTill(bundle.getValidTill());
                    }
                    String planType= subscriptionPlanBundleMap.get(baseSubscriptionPlanResp.getPurchaseEntityId());
                    long curr = System.currentTimeMillis();
                    if(baseSubscriptionPlanResp.getAioValidTill()>=curr || baseSubscriptionPlanResp.getAioValidTill()==0){
                        if(bundleIdSubscriptionMap.containsKey(planType)){
                            bundleIdSubscriptionMap.get(planType).add(baseSubscriptionPlanResp);
                        }
                        else{
                            bundleIdSubscriptionMap.put(planType,new ArrayList<>( Arrays.asList( baseSubscriptionPlanResp)));
                        }
                    }



                }

            });
            resp.setSubscriptionPlanMap(bundleIdSubscriptionMap);
            resp.setOfferStrip(getBundleMessageStrip("STRIP_COURSE_DETAIL"));
            HttpSessionData sessionData = sessionUtils.getCurrentSessionData(false);
            boolean displayTrial = true;
            if(sessionData !=null && sessionData.getUserId() !=null ){
                if(bundleEnrolmentDAO.getActiveEnrollmentCount(sessionData.getUserId().toString(),new ArrayList<>(bundleIds)) > 0){
                    displayTrial =false;
                }
                else {
                    List<String> trialBundleIds = new ArrayList<>();
                    List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO.getEnrollmentsOfAllKindOfTrialEnrollment(sessionData.getUserId().toString(), Arrays.asList(BundleEnrolment.Constants.BUNDLE_ID, BundleEnrolment.Constants.TRIAL_ID));
                    if (ArrayUtils.isNotEmpty(bundleEnrolments)) {
                        bundleEnrolments.forEach(bundleEnrolment -> {
                            trialBundleIds.add(bundleEnrolment.getBundleId());
                        });
                        // Get all the grades of bundles
                        Set<Integer> grades = bundleDAO.getGradesOfAllTheBundleIds(trialBundleIds);

                        // See if there's already an enrollment
                        if (grades.contains(grade)) {
                            String trialId = bundleEnrolments.get(0).getTrialId();
                            Trial trial = trialDAO.getById(trialId);
                            Map<String, List<TrialInfo>> trialInfoMap = new HashMap<>();
                            trialInfoMap.put(SubscriptionPlanType.LITE.toString(), Arrays.asList(mapper.map(trial, TrialInfo.class)));
                            resp.setTrialInfoMap(trialInfoMap);
                            resp.setEnrolledTrial(trialId);
                            displayTrial = false;
                        }
                    }
                }
            }
            Map<String,List<TrialInfo>> TrialInfoMap;
            if (ArrayUtils.isNotEmpty(trailBundles) &&  displayTrial) {
                Map<String,List<TrialInfo>> trialInfoMap = new HashMap<>();
                List<Trial> trials =trialDAO.getAllActiveTrialsForBundle(trailBundles);
                List<TrialInfo> trialInfoList = new ArrayList<>();
                List<String> trialId = new ArrayList<>();
                trials.forEach(trial -> {
                    String planType= subscriptionPlanBundleMap.get(trial.getContextId());
                    if(trialInfoMap.containsKey(planType)){

                        TrialInfo trialInfo =mapper.map(trial,TrialInfo.class);
                        logger.info(trialInfo.toString()  );
                        List<TrialInfo> trialInfos= trialInfoMap.get(planType);

                        trialInfos.add(trialInfo);
                    }
                    else{
                        trialInfoMap.put(planType, new ArrayList<>(Arrays.asList( mapper.map(trial,TrialInfo.class))));
                    }
                    trialId.add(trial.getId());
                });
                resp.setTrialInfoMap(trialInfoMap);
                if(sessionData !=null && sessionData.getUserId() !=null ) {
                    resp.setEnrolledTrial(bundleEnrolmentDAO.getTrailEnrollment(sessionData.getUserId().toString() ,trialId));
                }
            }
        }
        return resp;

    }



    public List<BaseSubscriptionPlanResp> getBaseSubscriptionPlanByBundles(List<String > bundleIds ) throws VException {

        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseSubscriptionPlanByBundles";
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.POST,
                new Gson().toJson(bundleIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

        Type listType = new TypeToken<List<BaseSubscriptionPlanResp>>() {
        }. getType();
        List<BaseSubscriptionPlanResp> res = new Gson().fromJson(jsonString, listType);
        return res;
    }

    public void inActiveSubscriptionPlan() throws VException {
        List<String > paramas = new ArrayList<>();
        sqsManager.sendToSQS(SQSQueue.BUNDLE_OPS, SQSMessageType.MARK_SUBSCRIPTION_INAVTIVE, paramas.toString());

    }
    public void markSubscriptionPlanInActive() throws VException {
        List<BundleEnrolment> bundleEnrolmentList = new ArrayList<>();
        List<String> enrollmentIds = new ArrayList<>();
        bundleEnrolmentList = bundleEnrolmentDAO.getExpiredSubscriptions();
        bundleEnrolmentList.forEach(enrollment -> {
            enrollmentIds.add(enrollment.getId());
        });

        if (ArrayUtils.isNotEmpty(enrollmentIds)) {


            List<SubscriptionUpdate> subscriptionUpdates = subscriptionUpdateDAO.getUpdateSubscriptionsId(enrollmentIds);
            List<String> updateIds = new ArrayList<>();
            subscriptionUpdates.forEach(subscriptionUpdate -> {
                updateIds.add(subscriptionUpdate.getOldEnrollmentId());
            });


            for (BundleEnrolment enrollment : bundleEnrolmentList) {


                if (updateIds.contains(enrollment.getId())) {
                    sqsManager.sendToSQS(SQSQueue.BUNDLE_OPS, SQSMessageType.RENEW_SUBSCRIPTION, gson.toJson(enrollment));
                } else {


                    UpdateStatusReq updateStatusReq = new UpdateStatusReq();
                    updateStatusReq.setBundleEnrollmentId(enrollment.getId());
                    updateStatusReq.setBundleId(enrollment.getBundleId());
                    updateStatusReq.setStatus(EntityStatus.INACTIVE);
                    updateStatusReq.setUserId(enrollment.getUserId());
                    updateStatusReq.setUpdateOrder(false);
                    sqsManager.sendToSQS(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE, SQSMessageType.MARK_BUNDLE_ACTIVE_INAVTIVE, gson.toJson(updateStatusReq));
                }


            }


        }



    }

    public void renewSubscriptionPlan(String oldEnrollmentId) throws VException {
      SubscriptionUpdate subscriptionUpdate =   subscriptionUpdateDAO.getExpiredSubscriptions(oldEnrollmentId);

      if (Objects.isNull(subscriptionUpdate)) {
          logger.error("Expired subscription not found to renew for bundle enrolment id: {} at {}",
                  oldEnrollmentId, System.currentTimeMillis());
          return;
      }

      BundleEnrolment oldBundleEnrolment = bundleEnrolmentDAO.getEntityById(oldEnrollmentId,BundleEnrolment.class);


            BundleCreateEnrolmentReq bundleReq = new BundleCreateEnrolmentReq();
            bundleReq.setEntityId(oldBundleEnrolment.getBundleId());
            bundleReq.setUserId(oldBundleEnrolment.getUserId() .toString());
            bundleReq.setState(EnrollmentState.REGULAR);
            bundleReq.setStatus(EntityStatus.ACTIVE);
            bundleReq.setValidMonths(subscriptionUpdate.getValidMonths());
            bundleReq.setSubscriptionRenew(true);
            bundleReq.setIsSubscription(true);
            BundleEnrolment  newEnrollment  =  createEnrolment(bundleReq);

        subscriptionUpdate.setIsEnrollmentCreated(true);
        subscriptionUpdateDAO.save(subscriptionUpdate , subscriptionUpdate.getUserId());

            subscriptionUpdateDAO.updateOldEnrollmetId(oldEnrollmentId,newEnrollment.getId());

        if(!StringUtils.isEmpty(subscriptionUpdate.getOrderId())) {
                     paymentManager.updateDeliverableEntityIdInOrder(subscriptionUpdate.getOrderId(), newEnrollment.getId(), DeliverableEntityType.BUNDLE_ENROLMENT, newEnrollment.getBundleId());
                 }




    }




    public void correctPackageTypeInSubscription(String batchId) throws VException {
        Batch batch = batchDAO.getById(batchId, Arrays.asList( Batch.Constants.PACKAGE_TYPES) );
        logger.info( "correctPackageTypeInSubscription Batch "+ batch );
        if(Objects.isNull( batch )) {
            throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "batch not found with id " + batchId);
        }
        int start = 0;
        int size = 10;
        Boolean processing = Boolean.TRUE;
        List<Bundle> bundles = new ArrayList<>();
        while (Boolean.TRUE.equals(processing)) {
            List<Bundle> bundleList = bundleDAO.getBundlesByPackageId(batchId, Arrays.asList(Bundle.Constants.PACKAGES), start, size);
            if (ArrayUtils.isNotEmpty(bundleList)) {
                bundles.addAll(bundleList);
                start += bundleList.size();
            } else {
                processing = Boolean.FALSE;
            }
        }

        if(ArrayUtils.isNotEmpty( bundles )) {
            for (Bundle bundle: bundles) {
                logger.info( "Total bundle count: "+ bundles.size() );
                List<AioPackage> aioPackages = bundle.getPackages();
                if(ArrayUtils.isNotEmpty( aioPackages )) {
                    logger.info( "Starting: "+ bundle.getId() );
                    for(AioPackage aioPackage : aioPackages) {
                        if(aioPackage.getEntityId().equals( batchId )) {
                            List<SubscriptionPackageType> subscriptionPackageTypes = Objects.nonNull( batch.getPackageTypes() ) ? batch.getPackageTypes() : new ArrayList<>();
                            if(ArrayUtils.isEmpty(subscriptionPackageTypes)) {
                                subscriptionPackageTypes.add( SubscriptionPackageType.DEFAULT );
                            }
                            // mainTags may should not contain duplicate values.
                            List<String> mainTags = aioPackage.getMainTags();
                            Set<String> uniqueMainTags = new HashSet<String>(mainTags);
                            mainTags.clear();
                            mainTags.addAll(uniqueMainTags);
                            for (SubscriptionPackageType subscriptionPackageType : SubscriptionPackageType.values()) {
                                mainTags.remove( subscriptionPackageType.toString() );
                            }
                            subscriptionPackageTypes.forEach(type -> {
                                mainTags.add( type.toString() );
                            });
                            if(Objects.nonNull( aioPackage.getGrade() )) {
                                mainTags.add( String.valueOf( aioPackage.getGrade() ) );
                            }
                            aioPackage.setMainTags(mainTags);
                            aioPackage.setSubscriptionPackageTypes(subscriptionPackageTypes);
                        }
                    }
                    bundle.setPackages( aioPackages );
                    Query query = new Query();
                    query.addCriteria(Criteria.where(Bundle.Constants._ID).is(bundle.getId()));
                    Update update = new Update();
                    update.set(Bundle.Constants.PACKAGES, bundle.getPackages());
                    bundleDAO.updateMultiple(query, update);
                    logger.info( "Finishing: "+ bundle.getId() );
                }
            }
        }
    }


    public TrialExpirationResp getTrialEnrollmentData(String userId) throws Throwable {
        TrialExpirationResp trialExpirationResp = new TrialExpirationResp();
        boolean isPaidUser = hasLongTermEnrollment(userId);
        trialExpirationResp = bundleEnrolmentDAO.getTrialEnrollmentData(userId);
        trialExpirationResp.setIsPaidUser(isPaidUser);

        return trialExpirationResp;
    }
    public void subscriptionQueryMessage( SubscriptionQueryReq req) throws VException, UnsupportedEncodingException {
        Map<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("emailId", req.getEmailId());
        bodyScopes.put("message", req.getMessage());
        bodyScopes.put("phoneNumber", req.getPhoneNumber());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress("vcare@vedantu.com", "VCare"));
        EmailRequest emailRequest = new EmailRequest(toList, bodyScopes, bodyScopes, CommunicationType.SUBSCRIPTION_QUERY_REQUEST, Role.STUDENT);
        communicationManager.sendEmail(emailRequest);

    }

    public List<BundleInfo> getBundleDataForBundleSuggestions(List<String> bundleIds) {
        return Optional.ofNullable(bundleDAO.getBundlesByIdsFilterd(bundleIds))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(this::getBundleInfoForSuggestion)
                .collect(Collectors.toList());
    }

    private BundleInfo getBundleInfoForSuggestion(Bundle bundle) {
        BundleInfo bundleInfo=mapper.map(bundle,BundleInfo.class);
        try {
            bundleInfo.setPackagesExists(ArrayUtils.isNotEmpty(bundleEntityDAO.getBundleEntitiesByBundleId(bundle.getId(),0,1,Collections.singletonList(BundleEntity.Constants._ID))));
        } catch (BadRequestException e) {
            logger.error("There is some problem while setting up packages, please look into it for bundle id - {}",bundle.getId());
            bundleInfo.setPackagesExists(false);
        }
        return bundleInfo;
    }

    public void migrateTrialToRegular(  String oldEnrollmentIds ,String newEnrollmentIds ) throws VException {


        BundleEnrolment oldBundleEnrollment = bundleEnrolmentDAO.getEntityById(oldEnrollmentIds, BundleEnrolment.class);
        BundleEnrolment newBundleEnrolment = bundleEnrolmentDAO.getEntityById(newEnrollmentIds, BundleEnrolment.class);
        if (oldBundleEnrollment == null || newBundleEnrolment == null || EntityStatus.ENDED.equals(oldBundleEnrollment.getStatus()) || !EntityStatus.ACTIVE.equals(newBundleEnrolment.getStatus()) || EnrollmentState.REGULAR.equals(oldBundleEnrollment.getState())) {
            return;
        }
        if (oldBundleEnrollment.getUserId().equals(newBundleEnrolment.getUserId())) {

            migrateBatchEnrollments(oldBundleEnrollment, newBundleEnrolment);

            oldBundleEnrollment.setStatusChangeTime(getBundleEnrolmentStatusChangeTimes(oldBundleEnrollment.getStatusChangeTime(),
                    oldBundleEnrollment.getStatus(), EntityStatus.ENDED, Long.parseLong(newBundleEnrolment.getUserId())));

            oldBundleEnrollment.setStatus(EntityStatus.ENDED);
            oldBundleEnrollment.setEndedBy(Long.parseLong(newBundleEnrolment.getUserId()));
            oldBundleEnrollment.setEndReason("regulare_updte_to " + newBundleEnrolment.getId());
            bundleEnrolmentDAO.save(oldBundleEnrollment, Long.parseLong(newBundleEnrolment.getUserId()));


        }
    }


    public Boolean isValidBundle(String id) {
    	Bundle bundle = null;
    	bundle = bundleDAO.isValidBundle(id);
    	if(null != bundle && !StringUtils.isEmpty(bundle.getId()))
    		return true;
    	else
    		return false;
    }

    public PremiumSubscriptionResp isPremiumSubscriber(String userId, String grade, String board, String stream, String target) {
    	User ubi = fosUtils.getUserInfo(sessionUtils.getCallingUserId(), false);
    	if(StringUtils.isEmpty(userId)) {
    		userId = sessionUtils.getCallingUserId().toString();
    	}
    	if(StringUtils.isEmpty(grade)) {
    		grade = ubi.getStudentInfo().getGrade();
    	}
    	if(StringUtils.isEmpty(board)) {
    		board = ubi.getStudentInfo().getBoard();
    	}
    	if(StringUtils.isEmpty(stream)) {
    		stream = ubi.getStudentInfo().getStream();
    	}
    	if(StringUtils.isEmpty(target)) {
    		target = ubi.getStudentInfo().getTarget();
    	}
    	PremiumSubscriptionResp resp = new PremiumSubscriptionResp();
    	PremiumSubscriptionRequest premiumSubscriptionReq = PremiumSubscriptionRequest.builder()
    			.grade(null != grade?grade:"")
    			.board(null != board?board:"")
    			.stream(null != stream?stream:"")
    			.target(null != target?target:"")
    			.build();
    	List<BatchBundlePojo> premiumBundles = new ArrayList<BatchBundlePojo>();
    	BatchBundlePojo enrolledPremiumBundle = new BatchBundlePojo();
    	premiumBundles = premiumSubscriptionManager.getPremiumBundlesByFilter(premiumSubscriptionReq);
    	List<String> bundleIds = new ArrayList<String>();
    	bundleIds = premiumBundles.stream().map(BatchBundlePojo::getBundleId).collect(Collectors.toList());
    	logger.debug("premiumBundles in isPremiumSubscriber -> "+premiumBundles);
    	List<BundleEnrolment> enrolledBundlesRes = new ArrayList<BundleEnrolment>();
    	if(ArrayUtils.isNotEmpty(bundleIds))
    	enrolledBundlesRes = bundleEnrolmentDAO.getActiveBundleEnrolments(userId, bundleIds, Arrays.asList(
				BundleEnrolment.Constants.ID, BundleEnrolment.Constants.BUNDLE_ID, BundleEnrolment.Constants.STATUS, BundleEnrolment.Constants.FREE_PASS_VALID_TILL));
    	logger.debug("enrolledBundlesRes in isPremiumSubscriber -> "+enrolledBundlesRes);
    	if(ArrayUtils.isNotEmpty(enrolledBundlesRes)) {
    		resp.setIsPremiumSubscriber(ArrayUtils.isNotEmpty(enrolledBundlesRes));
    		resp.setEnrollmentEndTime(enrolledBundlesRes.get(0).getFreePassvalidTill());
    		String bundleId = enrolledBundlesRes.get(0).getBundleId();
    		enrolledPremiumBundle = premiumBundles.stream().filter(p -> p.getBundleId().equals(bundleId)).findFirst().get();
    		if(null != enrolledPremiumBundle && null != enrolledPremiumBundle.getNoOfDaysOfFreeAccess()
    				&& !StringUtils.isEmpty(enrolledPremiumBundle.getPremiumBundleTitle())) {
    			resp.setNoOfDaysOfFreeAccess(enrolledPremiumBundle.getNoOfDaysOfFreeAccess());
    			String title = enrolledPremiumBundle.getPremiumBundleTitle().replace("Batch", "Course");
    			resp.setPremiumBundleTitle(title);
    		}
    	}
    	logger.debug("resp in isPremiumSubscriber -> "+resp);
    	return resp;
    }

    public PlatformBasicResponse enrollNewUserToPremiumBundles(String userId, PremiumSubscriptionRequest premiumSubscriptionRequest) throws VException{
    	Long currentMillis = System.currentTimeMillis();
    	List<PremiumSubscription> premiumBundlesToEnroll = premiumSubscriptionManager.getPremiumBundlesForEnrollment(premiumSubscriptionRequest);
    	logger.debug("premiumBundlesToEnroll -> "+premiumBundlesToEnroll);
    	//List<String> bundleIds = premiumBundlesToEnroll.stream().map(PremiumSubscription::getBundleId).collect(Collectors.toList());
    	if(ArrayUtils.isNotEmpty(premiumBundlesToEnroll)) {
    		premiumBundlesToEnroll.forEach(pBundle -> {
    			if(currentMillis>=pBundle.getValidFrom() && currentMillis < pBundle.getValidTill()) {
    				try {
    					enrollWithFreePass(new FreePassReq("", "", "", Long.valueOf(userId), pBundle.getBundleId(), 0, "", pBundle.getNoOfDaysOfFreeAccess(), 0));
    				} catch (NumberFormatException | VException e) {
    					logger.error("Could not create Premium Bundle Enrollment for new Sign Up User from Mobile APP for user "
    							+userId+ "for Bundle " +pBundle.getBundleId()+ " due to -> "+e);
    				}
    			}});

//    		List<BundleEnrolment> bundleEnrolments = new ArrayList<BundleEnrolment>();
//    		bundleEnrolments = bundleEnrolmentDAO.getBundleEnrolments(userId, bundleIds, Arrays.asList(BundleEnrolment.Constants.BUNDLE_ID,
//    				BundleEnrolment.Constants.USER_ID, BundleEnrolment.Constants._ID, BundleEnrolment.Constants.STATUS));
//    		if(ArrayUtils.isNotEmpty(bundleEnrolments)) {
//    			bundleEnrolments.forEach(bEnrollment -> {
//    				try {
//    					autoEnrollInBundleCourses(bEnrollment.getBundleId(), bEnrollment.getUserId(), bEnrollment.getId());
//    				} catch (VException e) {
//    					logger.error("Could not create Batch Enrollment for new Sign Up User from Mobile APP  for user "
//    							+ bEnrollment.getUserId()+ " and bundleId "+bEnrollment.getBundleId()+ "and bundleEnrollmentId "+bEnrollment.getId()+ " due to -> "+e);
//    				}
//    			});
//    		}
    	}
    	return new PlatformBasicResponse(true, "Success", "");
    }

    public List<String> getBatchIdsByBundleId(String bundleId) {
    	List<BundleEntity> bundleEntities = new ArrayList<BundleEntity>();
    	List<String> batchIds = new ArrayList<String>();
    	try {
			bundleEntities = bundleEntityDAO.getBundleEntitiesByBundleId(bundleId, 0, 20, Arrays.asList(BundleEntity.Constants.ENTITY_ID));
		} catch (BadRequestException e) {
			logger.error("Could not fetch bundleEntities for the bundle Id -> {} due to exception {}", bundleId, e);
		}
    	if(ArrayUtils.isNotEmpty(bundleEntities)) {
    		batchIds = bundleEntities.stream().map(BundleEntity::getEntityId).collect(Collectors.toList());
    	}
    	logger.debug("batchIds from getBatchIdsByBundleId ->" + bundleId+ " :"+batchIds);
    	return batchIds;
    }

    public BundleEnrolment getBundleEnrolledForUser(String bundleId, String userId) {
    	BundleEnrolment bundleEnrollment = new BundleEnrolment();
    	bundleEnrollment = bundleEnrolmentDAO.getNonEndedBundleEnrolment(userId, bundleId);
    	return bundleEnrollment;
    }

}
