package com.vedantu.subscription.managers;

import java.lang.reflect.Field;
import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.subscription.dao.HourTransactionDAO;
import com.vedantu.subscription.entities.sql.HourTransaction;
import com.vedantu.subscription.viewobject.request.GetHourTransactionReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

@Service
public class HourTransactionManager {

	@Autowired
	public HourTransactionDAO hourTransactionDAO;
	
	@Autowired
	private SqlSessionFactory sqlSessionfactory;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(HourTransactionManager.class);

	public HourTransaction addOrUpdateHourTransaction(HourTransaction hourTransaction, Session session) {

		Long id = hourTransaction.getId();

		if (id == null) {
			hourTransactionDAO.create(hourTransaction, session);
		} else {
			HourTransaction hourTransaction1 = (HourTransaction) hourTransactionDAO.getEntityById(id, true,
					HourTransaction.class, session);
			hourTransaction = addUpdates(hourTransaction1, hourTransaction);
			hourTransactionDAO.update(hourTransaction, session);
		}

		return hourTransaction;
	}

	public HourTransaction getHourTransactionById(Long id, Boolean enabled, Session session) {
		return hourTransactionDAO.getById(id, enabled, session);
	}

	public List<HourTransaction> getHourTransaction(AbstractFrontEndListReq req, Session session) {
		return hourTransactionDAO.getEntities(HourTransaction.class, req, true, session);
	}
	
	public List<HourTransaction> getHourTransactions(GetHourTransactionReq req) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		List<HourTransaction> results = null;
		try {
			Criteria cr = session.createCriteria(HourTransaction.class);
			for (Field field : req.getClass().getDeclaredFields()) {
				field.setAccessible(true); 
				try {
					logger.info(field.getType() + " " + field.getName() + ": "  + field.get(req));
					if (field.getName() != "start" && field.getName() != "size" && field.get(req) != null) {
						cr.add(Restrictions.eq(field.getName(), field.get(req)));
					}
				} catch (IllegalArgumentException e) {
					logger.info(e.toString());
				} catch (IllegalAccessException e) {
					logger.info(e.toString());
				}
			}
			cr.addOrder(Order.desc("enabled"));
			cr.addOrder(Order.desc("creationTime"));

			Integer start = req.getStart();
			Integer size = req.getSize();
			if (start == null || start < 0) {
				start = 0;
			}
			if (size == null || size <= 0) {
				size = 20;
			}
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			results = hourTransactionDAO.runQuery(session, cr, HourTransaction.class);
		} catch (Exception e) {
			logger.error("Error: " + e.getMessage());
			throw new SQLException("getHourTransactions Error: " + e.getMessage());
		} finally {
			session.close();
		}

		return results;
	}

	public List<HourTransaction> getHourTransactionBySubscriptionId(Long subscriptionId, Boolean enabled,
			Session session) {
		Criteria cr = session.createCriteria(HourTransaction.class);
		if (enabled != null) {
			if (enabled.equals(true)) {
				cr.add(Restrictions.eq("enabled", true));
			} else {
				cr.add(Restrictions.eq("enabled", false));
			}
		}
		cr.add(Restrictions.eq("subscriptionId", subscriptionId));
		return hourTransactionDAO.runQuery(session, cr, HourTransaction.class);
	}

	public void updateAllHourTransaction(List<HourTransaction> slist, Session session) {
		hourTransactionDAO.updateAll(slist, session);
	}

	HourTransaction addUpdates(HourTransaction oldObj, HourTransaction newObj) {

		for (Field field : newObj.getClass().getDeclaredFields()) {
			field.setAccessible(true); // if you want to modify private fields
			try {
				logger.info(field.getType() + " " + field.getName() + ": "  + field.get(newObj));
				if (field.get(newObj) != null) {
					field.set(oldObj, field.get(newObj));
				}
			} catch (IllegalArgumentException e) {
				logger.info(e.toString());
			} catch (IllegalAccessException e) {
				logger.info(e.toString());
			}
		}
		oldObj.setLastUpdatedTime(System.currentTimeMillis());
		return oldObj;
	}
}
