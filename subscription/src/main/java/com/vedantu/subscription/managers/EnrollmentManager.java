package com.vedantu.subscription.managers;

import static com.vedantu.subscription.util.RedisDAO.SCRIPT_SEATS_AVAILABLE_FOR_BATCH;
import static com.vedantu.util.enums.SQSMessageType.GTT_ENROLMENT_ACTIVATED;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.google.gson.JsonObject;
import com.vedantu.subscription.response.EnrolledCoursesHomepage;
import com.vedantu.util.enums.SNSSubject;
import org.apache.commons.collections.CollectionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.OTMConsumptionReq;
import com.vedantu.dinero.request.OTMRefundAdjustmentReq;
import com.vedantu.dinero.response.OTMRefundAdjustmentRes;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.lms.cmds.pojo.ContentInfoResp;
import com.vedantu.lms.cmds.pojo.SubjectFileCountPojo;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.ActiveUserIdsByBatchIdsResp;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BatchChangeTime;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.CSVEnrollmentPojo;
import com.vedantu.onetofew.pojo.CSVEnrollmentRes;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.EnrollmentStateChangeTime;
import com.vedantu.onetofew.pojo.EnrollmentUpdatePojo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.onetofew.pojo.UserDashboardEnrollmentPojo;
import com.vedantu.onetofew.pojo.section.SectionChangeTime;
import com.vedantu.onetofew.request.GetBatchesForDashboardReq;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.response.session.OTFBatchSessionReportRes;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BundleDAO;
import com.vedantu.subscription.dao.BundleEnrolmentDAO;
import com.vedantu.subscription.dao.BundleEntityDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.CoursePlanDAO;
import com.vedantu.subscription.dao.EnrollmentConsumptionDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.EnrollmentTransactionDAO;
import com.vedantu.subscription.dao.OTFBundleDAO;
import com.vedantu.subscription.dao.RegistrationDAO;
import com.vedantu.subscription.dao.SectionDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.CoursePlan;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.OTMBundle;
import com.vedantu.subscription.entities.mongo.Registration;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.subscription.entities.sql.EnrollmentConsumption;
import com.vedantu.subscription.entities.sql.EnrollmentTransaction;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.enums.EnrollmentTransactionType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.subscription.request.EnrollmentHomePageRequest;
import com.vedantu.subscription.request.PremiumSubscriptionRequest;
import com.vedantu.subscription.request.RegPaymentOTFCourseReq;
import com.vedantu.subscription.request.UserPremiumSubscriptionEnrollmentReq;
import com.vedantu.subscription.request.section.CreateSectionEnrollmentReq;
import com.vedantu.subscription.request.section.EnrollmentFailureReq;
import com.vedantu.subscription.response.ActiveStudentsInBatchesResp;
import com.vedantu.subscription.response.BatchBundlePojo;
import com.vedantu.subscription.response.BundleEnrollmentResp;
import com.vedantu.subscription.response.EnrollmentHomePageResponse;
import com.vedantu.subscription.response.section.ActiveTAsForBatchRes;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.viewobject.request.BatchChangeReq;
import com.vedantu.subscription.viewobject.request.EnrollmentConsumptionPayment;
import com.vedantu.subscription.viewobject.request.EnrollmentRefundReq;
import com.vedantu.subscription.viewobject.request.GetRegistrationsForMultipleEntitiesReq;
import com.vedantu.subscription.viewobject.request.MakePaymentRequest;
import com.vedantu.subscription.viewobject.request.MigrationRequest;
import com.vedantu.subscription.viewobject.response.BoardSubjectPair;
import com.vedantu.subscription.viewobject.response.EndSubscriptionRes;
import com.vedantu.subscription.viewobject.response.GetUserDashboardEnrollmentsRes;
import com.vedantu.subscription.viewobject.response.OTFEntityEnrollmentResp;
import com.vedantu.subscription.viewobject.response.UserBatchInfoRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.request.ActiveUserIdsByBatchIdsReq;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.request.GetUserEnrollmentsReq;

@Service
public class EnrollmentManager {

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Autowired
    public EnrollmentDAO enrollmentDAO;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    public OTFBundleDAO otfBundleDAO;

    @Autowired
    public RegistrationManager registrationManager;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private EnrollmentConsumptionDAO enrollmentConsumptionDAO;

    @Autowired
    private RegistrationDAO registrationDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private CourseDAO courseDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private BatchManager batchManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private EnrollmentConsumptionManager enrollmentConsumptionManager;

    @Autowired
    private EnrollmentTransactionDAO enrollmentTransactionDAO;

    @Autowired
    private SqlSessionFactory sqlSessionfactory;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AwsSQSManager sqsManager;

    @Autowired
    private BundleEnrolmentDAO bundleEnrolmentDAO;

    @Autowired
    private BundleEntityDAO bundleEntityDAO;

    @Autowired
    private SectionManager sectionManager;

    @Autowired
    private SectionDAO sectionDAO;

    @Autowired
    private PremiumSubscriptionManager premiumSubscriptionManager;


    @Autowired
    private CoursePlanDAO coursePlanDAO;

    private Logger logger = LogFactory.getLogger(EnrollmentManager.class);

    private final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final Gson gson = new Gson();
    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static String batchTrialSectionRedisSlug = env + "_BATCH_TRIAL_SECTIONS_";
    private static String batchRegularSectionRedisSlug = env + "_BATCH_REGULAR_SECTIONS_";
    private static int redisExpiry = DateTimeUtils.SECONDS_PER_DAY;
    private static Long maxRefundTime = 5 * DateTimeUtils.MILLIS_PER_WEEK;
    private static int MAX_ENROLLMENTS_FETCH_SIZE = 50;
    private static int MAX_ENROLLED_COURSES_HOME_PAGE = 5;

    private static final String SOCIAL_STUDIES = "Social Studies";
    private static final String SOCIAL_SCIENCE = "Social Science";
    private static final String MATHEMATICS = "Mathematics";
    private static final String MATHS = "Maths";

    public boolean updateEnrollment(Enrollment enrollment, Long callingUserId)
            throws ConflictException, NotFoundException, BadRequestException {

        logger.info("Creating enrollment for user id {}",callingUserId);

        boolean createdNew = true;
        boolean activeNew = false;
        // Update the enrollment
        EntityStatus enrollmentStatus = enrollment.getStatus();
        if (enrollmentStatus == null) {
            enrollmentStatus = EntityStatus.ACTIVE;
            enrollment.setStatus(enrollmentStatus);
        }

        if (enrollment.getType() == null) {
            enrollment.setType(EntityType.STANDARD);
        }

        Boolean triggerAsyncEvents = Boolean.FALSE;
        List<Enrollment> enrollmentTemp = getEnrollment(enrollment.getUserId(), enrollment.getCourseId(), enrollment.getBatchId());
        logger.info("Enrollment present for user id - {} - course id - {} - batch id - {} is {}"
                ,enrollment.getUserId(),enrollment.getCourseId(),enrollment.getBatchId(),ArrayUtils.isNotEmpty(enrollmentTemp));

        if (enrollment.getId() == null && enrollmentTemp.size() > 0) {
            if (EnrollmentState.TRIAL.equals(enrollmentTemp.get(0).getState())
                    && EnrollmentState.REGULAR.equals(enrollment.getState())) {
                // trial enrollment is done, changing the state to REGULAR after payment
                // for now do nothing(ideally check for reg fee payment should have been done.
                // this is an internal call, so not imp)
                enrollment.setId(enrollmentTemp.get(0).getId());
                enrollment.setCreationTime(enrollmentTemp.get(0).getCreationTime());
                enrollment.setCreatedBy(enrollmentTemp.get(0).getCreatedBy());
                EnrollmentStateChangeTime stateChangeTime = new EnrollmentStateChangeTime();
                stateChangeTime.setChangedBy(callingUserId);
                stateChangeTime.setChangeTime(System.currentTimeMillis());
                stateChangeTime.setNewState(enrollment.getState());
                stateChangeTime.setPreviousState(enrollmentTemp.get(0).getState());
                if (ArrayUtils.isEmpty(enrollmentTemp.get(0).getStateChangeTime())) {
                    enrollment.setStateChangeTime(Arrays.asList(stateChangeTime));
                } else {
                    List<EnrollmentStateChangeTime> tempStateChange = enrollmentTemp.get(0).getStateChangeTime();
                    tempStateChange.add(stateChangeTime);
                    enrollment.setStateChangeTime(tempStateChange);
                }
                createdNew = false;
            } else {

                throw new BadRequestException(ErrorCode.ALREADY_ENROLLED, "already enrolled");
            }
            if (EntityStatus.INACTIVE.equals(enrollmentTemp.get(0).getStatus())) {
                triggerAsyncEvents = Boolean.TRUE;
                StatusChangeTime statusChangeTime = new StatusChangeTime();
                statusChangeTime.setChangeTime(System.currentTimeMillis());
                statusChangeTime.setNewStatus(EntityStatus.ACTIVE);
                statusChangeTime.setPreviousStatus(enrollmentTemp.get(0).getStatus());
                enrollment.setStatus(EntityStatus.ACTIVE);
                if (ArrayUtils.isEmpty(enrollment.getStatusChangeTime())) {
                    enrollment.setStatusChangeTime(Arrays.asList(statusChangeTime));
                } else {
                    enrollment.getStatusChangeTime().add(statusChangeTime);
                }
                activeNew = true;
            }

        } else {
            EnrollmentStateChangeTime stateChangeTime = new EnrollmentStateChangeTime();
            stateChangeTime.setChangedBy(callingUserId);
            stateChangeTime.setChangeTime(System.currentTimeMillis());
            stateChangeTime.setNewState(enrollment.getState());
            enrollment.setStateChangeTime(Arrays.asList(stateChangeTime));
            StatusChangeTime statusChangeTime = new StatusChangeTime();
            statusChangeTime.setChangedBy(callingUserId);
            statusChangeTime.setChangeTime(System.currentTimeMillis());
            statusChangeTime.setNewStatus(enrollment.getStatus());
            enrollment.setStatusChangeTime(Arrays.asList(statusChangeTime));
        }

        Batch batch = batchDAO.getById(enrollment.getBatchId());
        if (batch == null) {
            throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "Batch " + enrollment.getBatchId() + " not found");
        }

        long sectionsCount = sectionDAO.getSectionCountForBatch(batch.getId());
        if(!batch.getHasSections() && sectionsCount > 0) {
            batch.setHasSections(Boolean.TRUE);
            batchDAO.save(batch);
        }

        String courseId = enrollment.getCourseId();
        if (StringUtils.isEmpty(courseId)) {
            courseId = batch.getCourseId();
            enrollment.setCourseId(courseId);
        } else {
            if (!courseId.equals(batch.getCourseId())) {
                throw new ConflictException(ErrorCode.BATCH_NOT_PART_OF_COURSE,
                        "batch " + enrollment.getBatchId() + " does not belong to cousre " + courseId);
            }
        }
        if (StringUtils.isEmpty(enrollment.getEntityTitle())) {
            Course course = courseDAO.getById(enrollment.getCourseId());
            if (course != null) {
                enrollment.setEntityTitle(course.getTitle());
                enrollment.setEntityType(com.vedantu.session.pojo.EntityType.OTF_COURSE);
                enrollment.setEntityId(enrollment.getCourseId());
            }
        }
        logger.info("\nEnrollment created is " + gson.toJson(enrollment));

        if(batch.getHasSections().equals(Boolean.TRUE)){

            EnrollmentState sectionState = enrollment.getState();
            if(Objects.nonNull(enrollment.getPurchaseContextType()) && enrollment.getPurchaseContextType().equals(EnrollmentPurchaseContext.BUNDLE)) {
                logger.info("purchaseEnrollmentId {}",enrollment.getPurchaseEnrollmentId());
                BundleEnrolment bundleEnrollment = bundleEnrolmentDAO.getBundleEnrollmentById(enrollment.getPurchaseEnrollmentId(),Arrays.asList(BundleEnrolment.Constants.STATE));
                logger.info("bundleEnrollment {}",bundleEnrollment);

                if(bundleEnrollment != null){
                    logger.info("modifying enrollment state to bundle enrollment state {}",bundleEnrollment.getState());
                    sectionState = bundleEnrollment.getState();
                    if(sectionState.equals(EnrollmentState.FREE_PASS))
                        sectionState = EnrollmentState.TRIAL;
                }
                else
                    throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,"bundleEnrollment null while assigning section for " + enrollment, Level.ERROR);
            }

            Section vacantSection = sectionDAO.getVacantSection(enrollment.getBatchId(),sectionState);
            logger.info("batchId {} section {}",enrollment.getBatchId(),vacantSection);

            boolean isNewEnrollment = enrollmentTemp.isEmpty();
            boolean changingTrialToReg = false;
            if (!isNewEnrollment)
                changingTrialToReg = EnrollmentState.TRIAL.equals(enrollmentTemp.get(0).getState()) && EnrollmentState.REGULAR.equals(sectionState);

            logger.info("isNewEnrollment: {}", isNewEnrollment);
            logger.info("createdNew: {}", createdNew);
            logger.info("changingTrialToReg {}", changingTrialToReg);

            // Seats availability check -- when enrollment is created
            if (isNewEnrollment && createdNew) {
                long seatsRemainingInBatchWithBuffer = -1;
                if(Objects.nonNull(vacantSection))
                    seatsRemainingInBatchWithBuffer = vacantSection.getSeatsVacant();
                        //sectionManager.getSeatsRemainingForBatch(enrollment.getBatchId(), true, Arrays.asList(sectionState));
                if (seatsRemainingInBatchWithBuffer < 0) {
                    // email alert for enrollment failure
                    EnrollmentFailureReq req = new EnrollmentFailureReq();
                    req.setBatchId(enrollment.getBatchId());
                    req.setUserId(enrollment.getUserId());
                    req.setMessage_1(sectionState + " enrollment");
                    req.setMessage_2(" because either all " + sectionState + " sections are full or no " + sectionState + " sections exist.");
                    sqsManager.sendToSQS(SQSQueue.SECTION_NOTIFICATIONS_QUEUE, SQSMessageType.ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE, gson.toJson(req));

                    throw new BadRequestException(ErrorCode.ALL_SECTIONS_FULL, "Either all " + sectionState + " sections are full or no " + sectionState + " sections exist");
                }
            }

            // Seats availability check -- when enrollment to be changed from trial to regular
            if (!isNewEnrollment && !createdNew && changingTrialToReg) {
                long paidSeatsRemainingInBatchWithBuffer = -1;
                if(Objects.nonNull(vacantSection))
                    paidSeatsRemainingInBatchWithBuffer = vacantSection.getSeatsVacant();
                //sectionManager.getSeatsRemainingForBatch(enrollment.getBatchId(), true, Arrays.asList(EnrollmentState.REGULAR));
                if (paidSeatsRemainingInBatchWithBuffer < 0) {
                    // email alert for enrollment failure
                    EnrollmentFailureReq req = new EnrollmentFailureReq();
                    req.setBatchId(enrollment.getBatchId());
                    req.setUserId(enrollment.getUserId());
                    req.setMessage_1("REGULAR enrollment");
                    req.setMessage_2("because either all REGULAR sections are full or no REGULAR sections exist.");
                    sqsManager.sendToSQS(SQSQueue.SECTION_NOTIFICATIONS_QUEUE, SQSMessageType.ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE, gson.toJson(req));

                    throw new BadRequestException(ErrorCode.SECTION_FULL, "Either all regular sections are full or no regular section exist, cannot convert from Trial to Regular");
                }
            }

            // should never happen
            if(Objects.isNull(vacantSection)){
                throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "Section is null",Level.ERROR);
            }

            // validate old section
            if(Objects.nonNull(enrollment.getSectionId()))
                sectionDAO.incrementVacantSeatsForSections(Arrays.asList(enrollment.getSectionId()),1);

            enrollment.setSectionId(vacantSection.getId());
            enrollment.setSectionState(sectionState);
            enrollmentDAO.create(enrollment, callingUserId);

        }
        else
            enrollmentDAO.create(enrollment, callingUserId);

        List<Enrollment> enrollments = new ArrayList<>();
        enrollments.add(enrollment);
        if (createdNew || activeNew) {
            try {
                Map<String, Object> payload = new HashMap<>();
                payload.put("enrollment", enrollment);
                payload.put("event", BatchEventsOTF.USER_ENROLLED);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ENROLLMENT_EVENTS_TRIGGER, payload);
                asyncTaskFactory.executeTask(params);
            } catch (Exception e) {
                logger.info("Error in async event for ENROLLMENT_EVENTS_TRIGGER " + e);
            }
        }
        logger.info("updateEnrollment : enrollemnts" + enrollments.toString());
        updateEnrollments(enrollments);
        if (Boolean.TRUE.equals(triggerAsyncEvents)) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("enrollment", enrollment);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_ENROLLMENT_STATUS_EVENTS, payload);
            asyncTaskFactory.executeTask(params);
            AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.ENROLLMENT_REACTIVATED, payload);
            asyncTaskFactory.executeTask(params2);
        }

        // create gtt for enrollment status changed from inactive to active
        if (activeNew) {
            createPastGttForEnrolment(enrollment);
        }
        return createdNew;
    }

    public void createPastGttForEnrolment(Enrollment enrollment) {
        Map<String, String> enrollmentActivatedData = new HashMap<>();
        EnrollmentPojo pojo = new EnrollmentPojo();
        pojo.setEnrollmentId(enrollment.getId());
        pojo.setStatus(EntityStatus.ACTIVE);
        pojo.setUserId(enrollment.getUserId());
        pojo.setBatchId(enrollment.getBatchId());
        pojo.setCreationTime(enrollment.getCreationTime());
        enrollmentActivatedData.put("enrolmentInfo", gson.toJson(pojo));
        SQSMessageType type = GTT_ENROLMENT_ACTIVATED;
        sqsManager.sendToSQS(type.getQueue(), type, gson.toJson(enrollmentActivatedData), enrollment.getUserId());
    }

    private boolean checkRegistrationStatus(Long userId, String bundleId) {
        List<Registration> registrations = registrationManager.getRegistrations(com.vedantu.session.pojo.EntityType.OTM_BUNDLE_REGISTRATION, bundleId, RegistrationStatus.REGISTERED, userId);
        if (ArrayUtils.isNotEmpty(registrations)) {
            return true;
        } else {
            return false;
        }
    }

    public List<UserBatchInfoRes> getUserBatchInfos(Long userId, Integer start, Integer size) {
        GetEnrollmentsReq req = new GetEnrollmentsReq();
        req.setUserId(userId);
        req.setRemoveDeEnrolled(true);
        req.setStart(start);
        if (size != null && size > 100) {
            size = 100;
        }
        req.setSize(size);
        List<Enrollment> enrollmentList = enrollmentDAO.getEnrollments(req);

        if (ArrayUtils.isEmpty(enrollmentList)) {
            return new ArrayList<>();
        }
        logger.info("enrollments size {}", enrollmentList.size());
        logger.info("enrollments id {}", enrollmentList.stream().map(Enrollment::getId).collect(Collectors.joining(",")));

        Map<String, Batch> batchMap = new HashMap<>();
        Set<String> batchIds = new HashSet<>();
        for (Enrollment enrollment : enrollmentList) {
            batchIds.add(enrollment.getBatchId());
        }

        List<Batch> batchList = batchDAO.getBatchByIds(new ArrayList<>(batchIds));
        logger.info("batches size {}", batchList.size());
        logger.info("batches id {}", batchList.stream().map(Batch::getId).collect(Collectors.joining(",")));

        Set<String> courseIds = new HashSet<>();
        Set<Long> boardIds = new HashSet<>();
        for (Batch batch : batchList) {
            courseIds.add(batch.getCourseId());
            batchMap.put(batch.getId(), batch);
            if(ArrayUtils.isEmpty( batch.getBoardTeacherPairs() )) {
                continue;
            }
            for (BoardTeacherPair boardTeacherPair : batch.getBoardTeacherPairs()) {
                boardIds.add(boardTeacherPair.getBoardId());
            }
        }

        List<Course> courseList = courseDAO.getCoursesBasicInfos(new ArrayList<>(courseIds));
        logger.info("courses size : {}", courseList.size());
        logger.info("courses id {}", courseList.stream().map(Course::getId).collect(Collectors.joining(",")));

        Map<String, Course> courseMap = new HashMap<>();
        for (Course course : courseList) {
            courseMap.put(course.getId(), course);
        }

        Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);

        List<UserBatchInfoRes> userBatchInfoResList = new ArrayList<>();

        for (Map.Entry<String, Batch> entry : batchMap.entrySet()) {

            UserBatchInfoRes userBatchInfoRes = new UserBatchInfoRes();
            userBatchInfoRes.setBatchId(entry.getKey());
            userBatchInfoRes.setCourseId(entry.getValue().getCourseId());
            if (courseMap.get(entry.getValue().getCourseId()) != null) {
                userBatchInfoRes.setCourseName(courseMap.get(entry.getValue().getCourseId()).getTitle());
            } else {
                continue;
            }

            if (entry.getValue().getBoardTeacherPairs() != null) {
                List<BoardSubjectPair> boardSubjectPairList = new ArrayList<>();
                for (BoardTeacherPair boardTeacherPair : entry.getValue().getBoardTeacherPairs()) {
                    BoardSubjectPair boardSubjectPair = new BoardSubjectPair();
                    boardSubjectPair.setBoardId(boardTeacherPair.getBoardId());
                    if (boardMap.get(boardTeacherPair.getBoardId()) != null) {
                        boardSubjectPair.setSubject(boardMap.get(boardTeacherPair.getBoardId()).getName());
                        // continue;
                    }
                    boardSubjectPairList.add(boardSubjectPair);
                }
                userBatchInfoRes.setBoardPairs(boardSubjectPairList);
            }

            userBatchInfoResList.add(userBatchInfoRes);

        }

        return userBatchInfoResList;

    }

    public List<CSVEnrollmentRes> enrollFromCSV(List<CSVEnrollmentPojo> csvEnrollmentPojoList) throws NotFoundException, VException {

        List<CSVEnrollmentRes> csvEnrollmentResList = new ArrayList<>();

        Map<String, List<String>> bundleCourseMap = new HashMap<>();
        Map<String, String> batchCourseMap = new HashMap<>();
        Set<String> bundleIdList = new HashSet<>();
        Set<String> batchIdList = new HashSet<>();
        Set<String> emailList = new HashSet<>();
        for (CSVEnrollmentPojo csvEnrollmentPojo : csvEnrollmentPojoList) {
            bundleIdList.add(csvEnrollmentPojo.getbundleId());
            batchIdList.addAll(csvEnrollmentPojo.getBatchIds());
            emailList.add(csvEnrollmentPojo.getEmail());
        }
        Map<String, UserInfo> emailUserInfoMap = fosUtils.getUserInfosFromEmails(emailList, true);
        if (emailUserInfoMap.size() < emailList.size()) {
            logger.error("Invalid email in the list ");
        }

        logger.info("fetching batches for " + Arrays.toString(batchIdList.toArray()));
        List<Batch> batches = batchDAO.getBatchByIds(new ArrayList<>(batchIdList));
        logger.info("batches size fetched " + batches.size());
        Map<String, Batch> batchMap = new HashMap<String, Batch>();
        for (Batch batch : batches) {
            if (!batchCourseMap.containsKey(batch.getId())) {
                batchCourseMap.put(batch.getId(), batch.getCourseId());
            }
            batchMap.put(batch.getId(), batch);
            logger.info(batchCourseMap);
        }

        List<OTMBundle> otmBundles = otfBundleDAO.getOTMBundlesByIds(new ArrayList<>(bundleIdList));
        Map<String, OTMBundle> otmBundleMap = new HashMap<>();
        Set<String> courseIdsAll = new HashSet<String>();
        Map<String, Course> courseMap = new HashMap<String, Course>();

        for (OTMBundle otmBundle : otmBundles) {
            if (!bundleCourseMap.containsKey(otmBundle.getId())) {
                bundleCourseMap.put(otmBundle.getId(), new ArrayList<String>());
                List<OTMBundleEntityInfo> otmBundleEntityInfoList = otmBundle.getEntities();
                if (otmBundleEntityInfoList != null) {
                    for (OTMBundleEntityInfo otmBundleEntityInfo : otmBundleEntityInfoList) {
                        if (com.vedantu.session.pojo.EntityType.OTF_COURSE.equals(otmBundleEntityInfo.getEntityType())) {
                            courseIdsAll.add(otmBundleEntityInfo.getEntityId());
                            bundleCourseMap.get(otmBundle.getId()).add(otmBundleEntityInfo.getEntityId());
                        }
                    }
                }
            }
            otmBundleMap.put(otmBundle.getId(), otmBundle);
        }

        List<Course> courses = courseDAO.getCoursesBasicInfos(new ArrayList<>(courseIdsAll));
        for (Course course : courses) {
            courseMap.put(course.getId(), course);
        }

        for (CSVEnrollmentPojo csvEnrollmentPojo : csvEnrollmentPojoList) {
            int skipThisEntry = 0;
            UserInfo userInfo = emailUserInfoMap.get(csvEnrollmentPojo.getEmail());
            if (userInfo == null) {
                csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "Incorrect emailId"));
                skipThisEntry = 1;
                continue;
            }

            Long userId = userInfo.getUserId();

            if (!Role.STUDENT.equals(userInfo.getRole())) {
                csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "Only Student email id allowed"));
                skipThisEntry = 1;
                continue;
            }

            if (bundleCourseMap.get(csvEnrollmentPojo.getbundleId()) == null) {
                csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "Invalid bundleId"));
                continue;
            }

            Set<String> courseIds = new HashSet<>(bundleCourseMap.get(csvEnrollmentPojo.getbundleId()));

            if (courseIds.size() != csvEnrollmentPojo.getBatchIds().size()) {
                csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "Invalid number of batchIds"));
                continue;
            }

            logger.info("BatchCourseMap: " + batchCourseMap);
            logger.info("CourseIds: " + courseIds);

            for (String batchId : csvEnrollmentPojo.getBatchIds()) {
                String courseId = batchCourseMap.get(batchId);

                if (courseId == null) {
                    csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "Invalid batchId"));
                    logger.warn("courseId not found for batch " + batchId);
                    skipThisEntry = 1;
                    break;
                }

                if (!courseIds.contains(courseId)) {
                    csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "Incorrect bundleId and batchId combination"));
                    logger.warn("courseId not found in courseIds " + Arrays.toString(courseIds.toArray()));
                    skipThisEntry = 1;
                    break;
                }
                if (checkForAlreadyEnrolled(courseId, userId)) {
                    csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "User already enrolled for one of the course"));
                    logger.warn("user " + userId + " already enrolled  for course " + courseId);
                    skipThisEntry = 1;
                    break;
                }
                courseIds.remove(courseId);
            }

            if (skipThisEntry == 0) {
                if (checkRegistrationStatus(userId, csvEnrollmentPojo.getbundleId())) {

                    List<Enrollment> enrollments = createEnrollmentOTM(userId, csvEnrollmentPojo, batchCourseMap, otmBundleMap.get(csvEnrollmentPojo.getbundleId()).getTitle());

                    List<String> enrollmentIds = new ArrayList<>();

                    for (Enrollment enrollment : enrollments) {
                        enrollmentIds.add(enrollment.getId());
                    }

                    markRegistrationStatus(userId, csvEnrollmentPojo.getbundleId(), enrollmentIds, Arrays.asList(RegistrationStatus.REGISTERED));

                    List<Registration> registrationList = registrationDAO.getRegistrations(com.vedantu.session.pojo.EntityType.OTM_BUNDLE_REGISTRATION, csvEnrollmentPojo.getbundleId(), RegistrationStatus.ENROLLED, userId);

                    if (ArrayUtils.isEmpty(registrationList)) {
                        logger.error("Empty enrolled registration for enrollment: " + enrollments);
                    }

                    Registration registration = registrationList.get(0);

                    //enrollmentConsumptionManager.createEnrollmentConsumptionFromRegistration(registration, enrollments);
                    createEnrollmentConsumption(enrollments, registration);

                    for (Enrollment enrollment : enrollments) {
                        Map<String, Object> payload = new HashMap<String, Object>();
                        payload.put("enrollment", enrollment);
                        asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.CREATE_GTT_ATTENDEES_NEW_ENROLLMENT, payload));
                    }
                    Map<String, Object> payload = new HashMap<String, Object>();
                    payload.put("csvEnrollmentPojo", csvEnrollmentPojo);
                    payload.put("user", emailUserInfoMap.get(csvEnrollmentPojo.getEmail()));
                    asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.BOOK_CALENDAR_EVENTS_TRIGGER, payload));
                    asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.CHECK_REGISTRATION_FOR_ADVANCED_PAYMENT, payload));
                    payload.put("courseMap", courseMap);
                    payload.put("batchCourseMap", batchCourseMap);
                    payload.put("batchMap", batchMap);
                    payload.put("bundleName", otmBundleMap.get(csvEnrollmentPojo.getbundleId()).getTitle());
                    asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.SEND_EMAIL_TRIGGER, payload));
                    payload = new HashMap<>();
                    payload.put("csvEnrollmentPojo", csvEnrollmentPojo);
                    payload.put("user", emailUserInfoMap.get(csvEnrollmentPojo.getEmail()));
                    asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.SHARE_CURRICULUM_EVENTS_TRIGGER, payload));
                    csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, true, ""));
                } else {
                    csvEnrollmentResList.add(new CSVEnrollmentRes(csvEnrollmentPojo, false, "Registration status not available"));
                }
            }
        }
        updateBatchesAfterEnrollment(batchMap);
        return csvEnrollmentResList;

    }

    public boolean checkForAlreadyEnrolled(String courseId, Long userId) {
        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentByCourseAndUser(courseId,
                userId.toString(), Arrays.asList(EntityStatus.ACTIVE));
        if (enrollments.size() > 0) {
            return true;
        }
        return false;
    }

    public void updateBatchesAfterEnrollment(Map<String, Batch> batchIdMap) {
        logger.info("updateBatchesAfterEnrollment: " + batchIdMap);
        for (Map.Entry<String, Batch> entry : batchIdMap.entrySet()) {
            Batch batch = entry.getValue();
            updateMaxEnrollmentBatch(batch);
            batchDAO.save(batch);
        }
    }

    public void markRegistrationStatus(Long userId, String bundleId, List<String> enrollmentIds, List<RegistrationStatus> oldStatus) throws NotFoundException, VException {
        registrationManager.markRegistrationStatus(null, com.vedantu.session.pojo.EntityType.OTM_BUNDLE_REGISTRATION, bundleId, RegistrationStatus.ENROLLED, userId, enrollmentIds, oldStatus);
    }

    public void createEnrollmentTransactionForAdvancedPayment(EnrollmentConsumption enrollmentConsumption) {
        EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

        enrollmentTransaction.setAmtLeft(enrollmentConsumption.getAmtPaidLeft());
        enrollmentTransaction.setAmtLeftNP(enrollmentConsumption.getAmtPaidNPLeft());
        enrollmentTransaction.setAmtLeftP(enrollmentConsumption.getAmtPaidPLeft());
        enrollmentTransaction.setAmtPaid(enrollmentConsumption.getAmtPaid());
        enrollmentTransaction.setAmtPaidNP(enrollmentConsumption.getAmtPaidNP());
        enrollmentTransaction.setAmtPaidP(enrollmentConsumption.getAmtPaidP());
        enrollmentTransaction.setAmtLeftFromDiscount(enrollmentConsumption.getAmtPaidFromDiscountLeft());
        enrollmentTransaction.setAmtPaidFromDiscount(enrollmentConsumption.getAmtPaidFromDiscount());

        enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.BULK_PAYMENT);
        enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
        enrollmentTransaction.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.CREDIT);
        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        try {
            transaction.begin();
            enrollmentTransactionDAO.create(enrollmentTransaction, session);
            transaction.commit();
        } catch (Exception ex) {
            logger.error("Error while creating transaction");
            session.getTransaction().rollback();
        } finally {
            session.close();
        }
    }

    public void createEnrollmentConsumption(List<Enrollment> enrollments, Registration registration) throws VException {

        Map<String, Enrollment> courseEnrollmentMap = new HashMap<>();
        for (Enrollment enrollment : enrollments) {
            courseEnrollmentMap.put(enrollment.getCourseId(), enrollment);
        }

        OrderInfo orderInfo = paymentManager.getOrderInfo(registration.getOrderId());

        if (orderInfo.getAmount() == null) {
            orderInfo.setAmount(0);
        }

        if (orderInfo.getPromotionalAmount() == null) {
            orderInfo.setPromotionalAmount(0);
        }

        if (orderInfo.getNonpromotionalAmount() == null) {
            orderInfo.setNonpromotionalAmount(0);
        }
        int numOfEntities = registration.getEntities().size();
        int i = 0;
        int amtLeft = orderInfo.getAmount();
        int amtPLeft = orderInfo.getPromotionalAmount();
        int amtNPLeft = orderInfo.getNonpromotionalAmount();
        int amtFromDiscountLeft = 0;
        int amountToBePaidLeft = registration.getBulkPriceToPay();
        if (ArrayUtils.isNotEmpty(orderInfo.getOrderedItems())) {
            amtFromDiscountLeft = orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount();
        }
        for (OTMBundleEntityInfo oTMBundleEntityInfo : registration.getEntities()) {
            MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
            makePaymentRequest.setCourseId(courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId()).getCourseId());
            makePaymentRequest.setBatchId(courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId()).getBatchId());
            makePaymentRequest.setEnrollmentId(courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId()).getId());
            makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_PAYMENT);
            makePaymentRequest.setOrderId(registration.getOrderId());
            float ratio = (float) oTMBundleEntityInfo.getPrice() / registration.getBulkPriceToPay();
            logger.info("price: " + oTMBundleEntityInfo.getPrice());
            logger.info("Bulk: " + registration.getBulkPriceToPay());
            logger.info("Ratio: " + ratio);
            logger.info("Amt: " + orderInfo.getAmount() * ratio);
            makePaymentRequest.setAmtP((int) (orderInfo.getPromotionalAmount() * ratio));
            makePaymentRequest.setAmtNP((int) (orderInfo.getNonpromotionalAmount() * ratio));
            makePaymentRequest.setAmtToBePaid((int) (registration.getBulkPriceToPay() * ratio));
            makePaymentRequest.setAmt(makePaymentRequest.getAmtP() + makePaymentRequest.getAmtNP());
            if (ArrayUtils.isNotEmpty(orderInfo.getOrderedItems())) {
                makePaymentRequest.setAmtFromDiscount((int) (orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount() * ratio));
            } else {
                makePaymentRequest.setAmtFromDiscount(0);
            }
            if (i == numOfEntities - 1) {
                makePaymentRequest.setAmt(amtLeft);
                makePaymentRequest.setAmtP(amtPLeft);
                makePaymentRequest.setAmtNP(amtNPLeft);
                makePaymentRequest.setAmtFromDiscount(amtFromDiscountLeft);
                makePaymentRequest.setAmtToBePaid(amountToBePaidLeft);
            }
            amountToBePaidLeft = amountToBePaidLeft - makePaymentRequest.getAmtToBePaid();
            amtLeft = amtLeft - makePaymentRequest.getAmt();
            amtFromDiscountLeft = amtFromDiscountLeft - makePaymentRequest.getAmtFromDiscount();
            amtPLeft = amtPLeft - makePaymentRequest.getAmtP();
            amtNPLeft = amtNPLeft - makePaymentRequest.getAmtNP();
            i++;
            enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, ratio);

        }

        if (registration.isAdvancePaymentConsumed()) {
            orderInfo = paymentManager.getOrderInfo(registration.getAdvancePaymentOrderId());
            amtLeft = orderInfo.getAmount();
            amtPLeft = orderInfo.getPromotionalAmount();
            amtNPLeft = orderInfo.getNonpromotionalAmount();
            amtFromDiscountLeft = 0;
            if (ArrayUtils.isNotEmpty(orderInfo.getOrderedItems())) {
                amtFromDiscountLeft = orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount();
            }
            i = 0;
            for (OTMBundleEntityInfo oTMBundleEntityInfo : registration.getEntities()) {
                MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
                makePaymentRequest.setCourseId(courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId()).getCourseId());
                makePaymentRequest.setBatchId(courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId()).getBatchId());
                makePaymentRequest.setEnrollmentId(courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId()).getId());
                makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.BULK_PAYMENT);
                makePaymentRequest.setOrderId(registration.getAdvancePaymentOrderId());
                float ratio = (float) oTMBundleEntityInfo.getPrice() / registration.getBulkPriceToPay();

                makePaymentRequest.setAmtP((int) (orderInfo.getPromotionalAmount() * ratio));
                makePaymentRequest.setAmtNP((int) (orderInfo.getNonpromotionalAmount() * ratio));
                makePaymentRequest.setAmt(makePaymentRequest.getAmtP() + makePaymentRequest.getAmtNP());
                if (ArrayUtils.isNotEmpty(orderInfo.getOrderedItems())) {
                    makePaymentRequest.setAmtFromDiscount((int) (orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount() * ratio));
                } else {
                    makePaymentRequest.setAmtFromDiscount(0);
                }

                if (i == numOfEntities - 1) {
                    makePaymentRequest.setAmt(amtLeft);
                    makePaymentRequest.setAmtP(amtPLeft);
                    makePaymentRequest.setAmtNP(amtNPLeft);
                    makePaymentRequest.setAmtFromDiscount(amtFromDiscountLeft);
                }

                amtLeft = amtLeft - makePaymentRequest.getAmt();
                amtFromDiscountLeft = amtFromDiscountLeft - makePaymentRequest.getAmtFromDiscount();
                amtPLeft = amtPLeft - makePaymentRequest.getAmtP();
                amtNPLeft = amtNPLeft - makePaymentRequest.getAmtNP();
                i++;

                //since we are creating already above
                makePaymentRequest.setCreateConsumptionIfnotFound(false);

                enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, ratio);

            }
        }

    }

    public void triggerPostEnrollmentEventsForBatchBundle(CSVEnrollmentPojo cSVEnrollmentPojo, String bundleName, Long userId, Map<String, Course> courseMap, Map<String, Batch> batchMap, Map<String, String> batchCourseMap, List<Enrollment> enrollments) {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);

        UserInfo userInfo = new UserInfo();
        userInfo.setEmail(userBasicInfo.getEmail());
        userInfo.setFullName(userBasicInfo.getFullName());
        userInfo.setUserId(userId);

        updateBatchesAfterEnrollment(batchMap);

        cSVEnrollmentPojo.setEmail(userBasicInfo.getEmail());

        Map<String, Object> payload = new HashMap<>();
        payload.put("csvEnrollmentPojo", cSVEnrollmentPojo);
        payload.put("user", userInfo);
        asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.BOOK_CALENDAR_EVENTS_TRIGGER, payload));
        payload.put("courseMap", courseMap);
        payload.put("batchCourseMap", batchCourseMap);
        payload.put("batchMap", batchMap);
        payload.put("bundleName", bundleName);
        asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.SEND_EMAIL_TRIGGER, payload));
        payload = new HashMap<>();
        payload.put("csvEnrollmentPojo", cSVEnrollmentPojo);
        payload.put("user", userInfo);
        asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.SHARE_CURRICULUM_EVENTS_TRIGGER, payload));
        for (Enrollment enrollment : enrollments) {
            payload = new HashMap<String, Object>();
            payload.put("enrollment", enrollment);
            asyncTaskFactory.executeTask(new AsyncTaskParams(AsyncTaskName.CREATE_GTT_ATTENDEES_NEW_ENROLLMENT, payload));
        }

    }

    public List<Enrollment> createEnrollmentForBatchBundle(Long userId, OrderInfo orderInfo, OTFBundleInfo oTFBundleInfo, EnrollmentPurchaseContext purchaseContextType, String purchaseContextId, EnrollmentType purchaseContextEnrollmentType) throws BadRequestException {

        List<Enrollment> enrollments = new ArrayList<>();
        List<String> batchIds = new ArrayList<>();
        List<String> courseIds = new ArrayList<>();
        Map<String, Batch> batchMap = new HashMap<>();
        Map<String, Course> courseMap = new HashMap<>();
        Map<String, String> batchCourseMap = new HashMap<>();
        for (OTMBundleEntityInfo oTMBundleEntityInfo : oTFBundleInfo.getEntities()) {

            Batch batch = batchDAO.getById(oTMBundleEntityInfo.getEntityId());

            batchIds.add(batch.getId());
            batchMap.put(batch.getId(), batch);
            batchCourseMap.put(batch.getId(), batch.getCourseId());
            courseIds.add(batch.getCourseId());

            Enrollment enrollment = new Enrollment(userId.toString(), batch.getId(), batch.getCourseId(), null, Role.STUDENT, EntityStatus.ACTIVE, EntityType.STANDARD);
            if (purchaseContextType != null && purchaseContextId != null && purchaseContextEnrollmentType != null) {
                enrollment.setPurchaseContextEnrollmentType(purchaseContextEnrollmentType);
                enrollment.setPurchaseContextId(purchaseContextId);
                enrollment.setPurchaseContextType(purchaseContextType);
            }
            enrollment.setState(EnrollmentState.REGULAR);
            enrollment.setEntityType(com.vedantu.session.pojo.EntityType.OTM_BUNDLE);
            enrollment.setEntityId(oTFBundleInfo.getId());
            enrollment.setEntityTitle(oTFBundleInfo.getTitle());
            int duration = (int) (batch.getDuration() - batchManager.getDurationToBeSubtracted(batch.getId()));

            if (duration < DateTimeUtils.MILLIS_PER_HOUR) {
                duration = DateTimeUtils.MILLIS_PER_HOUR;
                //throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Duration left can't be less than or equal to zero");
            }
            if (batch.isContentBatch()) {
                duration = (int) (batch.getEndTime() - (System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_DAY));
                if (duration < DateTimeUtils.MILLIS_PER_HOUR) {
                    duration = DateTimeUtils.MILLIS_PER_HOUR;
                }
            }

            int hourlyRate = oTMBundleEntityInfo.getPrice() / (duration / DateTimeUtils.MILLIS_PER_HOUR);
            enrollment.setHourlyRate(hourlyRate);
            enrollments.add(enrollment);
            //enrollmentDAO.save(en);
        }

        List<Course> courses = courseDAO.getCoursesBasicInfos(courseIds);

        if (ArrayUtils.isEmpty(courses)) {
            logger.error("No courses found for bundle : " + oTFBundleInfo);
            throw new BadRequestException(ErrorCode.COURSE_NOT_FOUND, "Courses not found for batches of bundle: " + oTFBundleInfo.getId());
        }
        for (Course course : courses) {
            courseMap.put(course.getId(), course);
        }

        enrollmentDAO.insertAll(enrollments, userId);

        CSVEnrollmentPojo cSVEnrollmentPojo = new CSVEnrollmentPojo();
        cSVEnrollmentPojo.setBatchIds(batchIds);

        triggerPostEnrollmentEventsForBatchBundle(cSVEnrollmentPojo, oTFBundleInfo.getTitle(), userId, courseMap, batchMap, batchCourseMap, enrollments);

        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("userId", userId);
        payload2.put("entityId", oTFBundleInfo.getId());
        payload2.put("enrollments", enrollments);
        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.UPDATE_DELIVERABLE_ENTITY_ID_ON_ENROLL, payload2);
        asyncTaskFactory.executeTask(params2);

        return enrollments;
    }

    public List<Enrollment> createEnrollmentForBatchBundle(Long userId, OrderInfo orderInfo, OTFBundleInfo oTFBundleInfo) throws BadRequestException {
        return createEnrollmentForBatchBundle(userId, orderInfo, oTFBundleInfo, null, null, null);
    }

    public List<Enrollment> createEnrollmentOTM(Long userId, CSVEnrollmentPojo csvEnrollmentPojo,
                                                Map<String, String> batchCourseMap, String bundleTitle) throws VException {
        List<String> batchIds = csvEnrollmentPojo.getBatchIds();
        List<Enrollment> enrollments = new ArrayList<>();
        for (String batchId : batchIds) {
            Enrollment newEnrollment = new Enrollment(userId.toString(), batchId, batchCourseMap.get(batchId), null, Role.STUDENT, EntityStatus.ACTIVE, EntityType.STANDARD);
            newEnrollment.setState(EnrollmentState.TRIAL);
            newEnrollment.setEntityType(com.vedantu.session.pojo.EntityType.OTM_BUNDLE);
            newEnrollment.setEntityId(csvEnrollmentPojo.getbundleId());
            newEnrollment.setEntityTitle(bundleTitle);
            enrollments.add(newEnrollment);
        }
        enrollmentDAO.insertAll(enrollments, null);
        List<Registration> registrationList = registrationDAO.getRegistrations(com.vedantu.session.pojo.EntityType.OTM_BUNDLE_REGISTRATION, csvEnrollmentPojo.getbundleId(), RegistrationStatus.REGISTERED, userId);
        List<String> ids = new ArrayList<>();

        List<Enrollment> enrollmentsToBeUpdated = new ArrayList<>();

        List<Batch> batches = batchDAO.getBatchByIds(batchIds);

        Map<String, Long> courseDurationMap = new HashMap<>();
        Map<String, Batch> batchMap = new HashMap<>();
        for (Batch batch : batches) {
            courseDurationMap.put(batch.getCourseId(), batch.getDuration());
            batchMap.put(batch.getId(), batch);
        }

        if (registrationList == null || registrationList.size() != 1) {
            logger.info("Registration doesn't exist for the user: " + userId + " and bundleId: " + csvEnrollmentPojo.getbundleId());

        } else {

            Registration registration = registrationList.get(0);

            Map<String, Integer> courseAmountToPay = new HashMap<>();

            for (OTMBundleEntityInfo oTMBundleEntityInfo : registration.getEntities()) {

                courseAmountToPay.put(oTMBundleEntityInfo.getEntityId(), oTMBundleEntityInfo.getPrice());
            }

            logger.info("CourseDurationMap: " + courseDurationMap);
            logger.info("CourseAmountMap: " + courseAmountToPay);
            logger.info("registration: " + registration);

            for (Enrollment en : enrollments) {
                logger.info("Enrollment : " + en);
                ids.add(en.getId());

                Integer duration = (int) (courseDurationMap.get(en.getCourseId()) - batchManager.getDurationToBeSubtracted(en.getBatchId()));
                logger.info("Duration subtracted: " + duration);

                Batch batch = batchMap.get(en.getBatchId());

                if (duration < DateTimeUtils.MILLIS_PER_HOUR) {
                    duration = DateTimeUtils.MILLIS_PER_HOUR;
                    //throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Remaining Duration left can't be less than one hour");
                }

                if (batch != null && batch.isContentBatch()) {
                    duration = (int) (batch.getEndTime() - (System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_DAY));
                    if (duration < DateTimeUtils.MILLIS_PER_HOUR) {
                        duration = DateTimeUtils.MILLIS_PER_HOUR;
                    }
                }

                if (duration == null) {
                    logger.error("Unable to set hourlyRate for enrollmentId: " + en.getId());
                    logger.error("Can not find duration for the course: " + en.getCourseId());
                } else {
                    int hourlyRate = courseAmountToPay.get(en.getCourseId()) / (duration / DateTimeUtils.MILLIS_PER_HOUR);
                    en.setHourlyRate(hourlyRate);
                    enrollmentDAO.save(en);
                }
                logger.info(en);
            }
        }

        updateEnrollments(enrollmentsToBeUpdated);
        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("userId", userId);
        payload2.put("entityId", csvEnrollmentPojo.getbundleId());
        payload2.put("enrollments", enrollments);
        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.UPDATE_DELIVERABLE_ENTITY_ID_ON_ENROLL, payload2);
        asyncTaskFactory.executeTask(params2);
        return enrollments;
    }

    public EnrollmentTransaction saveEnrollmentTransactionForRegistration(EnrollmentConsumption enrollmentConsumption) {
        EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

        enrollmentTransaction.setRegAmtLeft(enrollmentConsumption.getRegAmtLeft());
        enrollmentTransaction.setRegAmtLeftNP(enrollmentConsumption.getRegAmtNPLeft());
        enrollmentTransaction.setRegAmtLeftP(enrollmentConsumption.getRegAmtPLeft());
        enrollmentTransaction.setRegAmtPaid(enrollmentConsumption.getRegAmtPaid());
        enrollmentTransaction.setRegAmtPaidNP(enrollmentConsumption.getRegAmtPaidNP());
        enrollmentTransaction.setRegAmtPaidP(enrollmentConsumption.getRegAmtPaidP());
        enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_PAYMENT);
        enrollmentTransaction.setEnrollmentId(enrollmentConsumption.getEnrollmentId());
        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.CREDIT);
        enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
        return enrollmentTransaction;
    }

    public Enrollment markStatus(OTFEnrollmentReq enrollmentReq)
            throws BadRequestException, ForbiddenException {

        // Update the enrollment
        if (StringUtils.isEmpty(enrollmentReq.getStatus())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Status is null" + enrollmentReq.getStatus());
        }
        EntityStatus entityStatus = EntityStatus.valueOf(enrollmentReq.getStatus());
        if (EntityStatus.ENDED.equals(entityStatus)) {
            if (StringUtils.isEmpty(enrollmentReq.getId())) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "enrollmentId is null for ending enrollment");
            }
        }

        Boolean triggerAsyncEvents = Boolean.FALSE;
        Boolean reactivate = Boolean.FALSE;
        String courseId = enrollmentReq.getCourseId();
        Batch batch = null;
        if (StringUtils.isEmpty(courseId) && !StringUtils.isEmpty(enrollmentReq.getBatchId())) {
            batch = batchDAO.getById(enrollmentReq.getBatchId());
            if (batch == null) {
                logger.error("batch not found " + enrollmentReq.getBatchId());
                throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND, "batch not found " + enrollmentReq.getBatchId());
            }
            enrollmentReq.setCourseId(batch.getCourseId());
        }

        Enrollment enrollmentTemp = null;
        if (StringUtils.isNotEmpty(enrollmentReq.getId())) {
            enrollmentTemp = getEnrollmentById(enrollmentReq.getId());
        } else {
            List<Enrollment> enrollmentsExisting = getEnrollment(enrollmentReq.getUserId(), enrollmentReq.getCourseId(),
                    enrollmentReq.getBatchId());
            if (enrollmentsExisting == null || enrollmentsExisting.isEmpty()) {
                throw new BadRequestException(ErrorCode.ENROLLMENT_NOT_FOUND, "Enrollment not found for "
                        + enrollmentReq.getBatchId() + " " + enrollmentReq.getUserId());
            }
            int count = 0;
            for (Enrollment enrollmentVar : enrollmentsExisting) {
                if (!EntityStatus.ENDED.equals(enrollmentVar.getStatus())) {
                    enrollmentTemp = enrollmentVar;
                    count++;
                }
            }
            if (count > 1) {
                throw new BadRequestException(ErrorCode.MULTIPLE_OTF_ENROLLMENTS_FOUND,
                        "multiple non ended enrollments found for req ");
            }
        }

        if (enrollmentTemp == null) {
            throw new BadRequestException(ErrorCode.ALREADY_ENDED, "All enrollment for user is already ended");
        }

        if (batch != null && EntityStatus.ACTIVE.equals(entityStatus)
                && EntityStatus.INACTIVE.equals(enrollmentTemp.getStatus())) {
            reactivate = Boolean.TRUE;
        }

        if (!entityStatus.equals(enrollmentTemp.getStatus())) {
            triggerAsyncEvents = Boolean.TRUE;
            StatusChangeTime statusChangeTime = new StatusChangeTime();
            // statusChangeTime.setChangedBy(enrollmentReq.getCallingUserId());
            statusChangeTime.setChangeTime(System.currentTimeMillis());
            statusChangeTime.setNewStatus(entityStatus);
            statusChangeTime.setPreviousStatus(enrollmentTemp.getStatus());
            if (ArrayUtils.isEmpty(enrollmentTemp.getStatusChangeTime())) {
                enrollmentTemp.setStatusChangeTime(Arrays.asList(statusChangeTime));
            } else {
                enrollmentTemp.getStatusChangeTime().add(statusChangeTime);
            }
            try {
                Map<String, Object> payload = new HashMap<>();
                String message = null;
                String subject = null;
                payload.put("enrollment", enrollmentTemp);
                switch (entityStatus) {
                    case ENDED:
                        subject = BatchEventsOTF.USER_ENDED.name();
                    case ACTIVE:
                        subject = BatchEventsOTF.USER_ACTIVE.name();
                    case INACTIVE:
                        subject = BatchEventsOTF.USER_INACTIVE.name();
                }
                if (enrollmentTemp != null) {
                    EnrollmentPojo enrollmentPojo = mapper.map(enrollmentTemp, EnrollmentPojo.class);
                    message = new Gson().toJson(enrollmentPojo);
                }
                awsSNSManager.triggerSNS(SNSTopicOTF.BATCH_EVENTS_OTF, subject, message);
            } catch (Exception e) {
                logger.info("error in async task for mark status");
            }
        }

        if (EntityStatus.ENDED.equals(entityStatus)) {
            enrollmentTemp.setEndedBy(enrollmentReq.getEndedBy());
            enrollmentTemp.setEndTime(System.currentTimeMillis());
            enrollmentTemp.setEndReason(enrollmentReq.getEndReason());
        }

        enrollmentTemp.setStatus(entityStatus);

        enrollmentDAO.create(enrollmentTemp, null);// will be taken from the thread, so no problem of passing null
        List<Enrollment> enrollmentsToUpdate = new ArrayList<>();
        enrollmentsToUpdate.add(enrollmentTemp);
        logger.info("updateEnrollment : enrollemnts" + enrollmentsToUpdate.toString());
        updateEnrollments(enrollmentsToUpdate);

        Map<String, String> gttPayload = new HashMap<>();
        gttPayload.put("userId", enrollmentTemp.getUserId());
        gttPayload.put("batchId", enrollmentTemp.getBatchId());
        EnrollmentPojo pojo = new EnrollmentPojo();
        pojo.setId(enrollmentTemp.getId());
        pojo.setStatus(enrollmentTemp.getStatus());
        pojo.setCreationTime(enrollmentTemp.getCreationTime());
        pojo.setLastUpdated(System.currentTimeMillis());
        gttPayload.put("enrolmentInfo", gson.toJson(pojo));
        sqsManager.sendToSQS(SQSQueue.GTT_ENROLMENT_OPS, SQSMessageType.GTT_ENROLMENT_STATUS_CHANGE_TASK, gson.toJson(gttPayload), enrollmentTemp.getUserId());

        if (Boolean.TRUE.equals(triggerAsyncEvents)) {
            if (Boolean.TRUE.equals(reactivate)) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("enrollment", enrollmentTemp);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ENROLLMENT_REACTIVATED, payload);
                asyncTaskFactory.executeTask(params);
            }
            Map<String, Object> payload = new HashMap<>();
            payload.put("enrollment", enrollmentTemp);

            sqsManager.sendToSQS(SQSQueue.BATCH_ENROLL_STATUS_QUEUE, SQSMessageType.TRIGGER_ENROLLMENT_STATUS_EVENTS, gson.toJson(payload));
           // AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_ENROLLMENT_STATUS_EVENTS, payload);
            //asyncTaskFactory.executeTask(params);
        }

        return enrollmentTemp;
    }


    public Enrollment endBatchEnrollmentWithoutAsync(OTFEnrollmentReq enrollmentReq)
            throws BadRequestException, ForbiddenException {

        // Update the enrollment
        if (StringUtils.isEmpty(enrollmentReq.getStatus())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Status is null" + enrollmentReq.getStatus());
        }
        EntityStatus entityStatus = EntityStatus.valueOf(enrollmentReq.getStatus());
        if (EntityStatus.ENDED.equals(entityStatus)) {
            if (StringUtils.isEmpty(enrollmentReq.getId())) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "enrollmentId is null for ending enrollment");
            }
        }

        Boolean triggerAsyncEvents = Boolean.FALSE;
        String courseId = enrollmentReq.getCourseId();
        Batch batch = null;
        if (StringUtils.isEmpty(courseId) && !StringUtils.isEmpty(enrollmentReq.getBatchId())) {
            batch = batchDAO.getById(enrollmentReq.getBatchId());
            if (batch == null) {
                logger.error("batch not found " + enrollmentReq.getBatchId());
                throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND, "batch not found " + enrollmentReq.getBatchId());
            }
            enrollmentReq.setCourseId(batch.getCourseId());
        }

        Enrollment enrollmentTemp = null;
        if (StringUtils.isNotEmpty(enrollmentReq.getId())) {
            enrollmentTemp = getEnrollmentById(enrollmentReq.getId());
        }

        if (enrollmentTemp == null) {
            throw new BadRequestException(ErrorCode.ALREADY_ENDED, "All enrollment for user is already ended");
        }


        if (!entityStatus.equals(enrollmentTemp.getStatus())) {
            triggerAsyncEvents = Boolean.TRUE;
            StatusChangeTime statusChangeTime = new StatusChangeTime();
            statusChangeTime.setChangeTime(System.currentTimeMillis());
            statusChangeTime.setNewStatus(entityStatus);
            statusChangeTime.setPreviousStatus(enrollmentTemp.getStatus());
            if (ArrayUtils.isEmpty(enrollmentTemp.getStatusChangeTime())) {
                enrollmentTemp.setStatusChangeTime(Arrays.asList(statusChangeTime));
            } else {
                enrollmentTemp.getStatusChangeTime().add(statusChangeTime);
            }
            try {
                String message = null;
                String subject = null;
                switch (entityStatus) {
                    case ENDED:
                        subject = BatchEventsOTF.USER_ENDED.name();
                    case ACTIVE:
                        subject = BatchEventsOTF.USER_ACTIVE.name();
                    case INACTIVE:
                        subject = BatchEventsOTF.USER_INACTIVE.name();
                }
                if (enrollmentTemp != null) {
                    EnrollmentPojo enrollmentPojo = mapper.map(enrollmentTemp, EnrollmentPojo.class);
                    message = new Gson().toJson(enrollmentPojo);
                }
                awsSNSManager.triggerSNS(SNSTopicOTF.BATCH_EVENTS_OTF, subject, message);
            } catch (Exception e) {
                logger.info("error in async task for mark status");
            }
        }

        if (EntityStatus.ENDED.equals(entityStatus)) {
            enrollmentTemp.setEndedBy(enrollmentReq.getEndedBy());
            enrollmentTemp.setEndTime(System.currentTimeMillis());
            enrollmentTemp.setEndReason(enrollmentReq.getEndReason());
        }

        enrollmentTemp.setStatus(entityStatus);
        enrollmentDAO.create(enrollmentTemp, null);// will be taken from the thread, so no problem of passing null
        List<Enrollment> enrollmentsToUpdate = new ArrayList<>();
        enrollmentsToUpdate.add(enrollmentTemp);
        logger.info("updateEnrollment : enrollemnts" + enrollmentsToUpdate.toString());
        updateEnrollments(enrollmentsToUpdate);
        if (Boolean.TRUE.equals(triggerAsyncEvents)) {

            Map<String, Enrollment> newEnrollmentMap = new HashMap<>();
            newEnrollmentMap.put(enrollmentTemp.getId(), enrollmentTemp);
            otfBundleManager.enrollmentTriggers(newEnrollmentMap.keySet(), new HashMap<>(), newEnrollmentMap, false);
        }
        return enrollmentTemp;
    }


    public void updateEnrollments(List<Enrollment> enrollments) {
        if (enrollments == null) {
            return;
        }

        for (Enrollment enrollment : enrollments) {
            EntityStatus enrollmentStatus = enrollment.getStatus();
            if (enrollmentStatus == null) {
                enrollmentStatus = EntityStatus.ACTIVE;
                enrollment.setStatus(enrollmentStatus);
            }

            // Handle enrolled students of the batch
            String batchId = enrollment.getBatchId();
            String userId = enrollment.getUserId();
            if (!StringUtils.isEmpty(userId)) {
                if (!StringUtils.isEmpty(batchId)) {
                    // Batch enrollment
                    Batch batch = batchDAO.getById(enrollment.getBatchId());
                    if (batch != null) {
                        switch (enrollment.getRole()) {
                            case TEACHER:
                                Set<String> teacherIds = batch.getTeacherIds();
                                if (teacherIds == null) {
                                    teacherIds = new HashSet<>();
                                }
                                switch (enrollmentStatus) {
                                    case ACTIVE:
                                        teacherIds.add(userId);
                                        break;
                                    case INACTIVE:
                                        teacherIds.remove(userId);
                                        break;
                                    default:
                                        break;
                                }
                                batch.setTeacherIds(teacherIds);
                                break;
                            case STUDENT:
                                updateMaxEnrollmentBatch(batch);
                                break;
                            default:
                                break;
                        }
                    }
                    batchDAO.save(batch);
                } else {
                    // Course enrollment
                    Course course = courseDAO.getById(enrollment.getCourseId());
                    if (course != null) {
                        if (Role.TEACHER.equals(enrollment.getRole())) {
                            Set<String> teacherIds = course.getTeacherIds();
                            if (teacherIds == null) {
                                teacherIds = new HashSet<String>();
                            }
                            switch (enrollment.getStatus()) {
                                case ACTIVE:
                                    teacherIds.add(userId);
                                    break;
                                default:
                                    teacherIds.remove(userId);
                                    break;
                            }
                            course.setTeacherIds(teacherIds);
                        }
                        courseDAO.save(course);
                    }
                }
            }
        }
    }

    public void updateMaxEnrollmentBatch(Batch batch) {
        int sizeForIncSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.increase.batchsize.if.less.than");
        int incSeatsBy = ConfigUtils.INSTANCE.getIntValue("otf.batch.increase.batchsize.by");
        
        Long enrollmentCount = enrollmentDAO.getEnrollmentCountInBatch(batch.getId());
        if (((batch.getMaxEnrollment() - enrollmentCount.intValue()) < sizeForIncSeats)) {
            batch.setMaxEnrollment(batch.getMaxEnrollment() + incSeatsBy);
            int maxSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.maxseats.gtt100");
            if (OTFSessionToolType.GTT.equals(batch.getSessionToolType())) {
                maxSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.maxseats.gtt");
            } else if (OTFSessionToolType.GTW.equals(batch.getSessionToolType())) {
                maxSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.maxseats.gtw");
            } else if (OTFSessionToolType.GTT_WEBINAR.equals(batch.getSessionToolType())) {
                maxSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.maxseats.gtt.webinar");
            } else if (OTFSessionToolType.VEDANTU_WAVE.equals(batch.getSessionToolType())) {
                maxSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.maxseats.vedantu_wave");
            } else if (OTFSessionToolType.VEDANTU_WAVE_BIG_WHITEBOARD.equals(batch.getSessionToolType())) {
                maxSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.maxseats.vedantu_wave_big_whiteboard");
            } else if (OTFSessionToolType.VEDANTU_WAVE_OTO.equals(batch.getSessionToolType())) {
                maxSeats = ConfigUtils.INSTANCE.getIntValue("otf.batch.maxseats.vedantu_wave_oto");
            }
            if (batch.getMaxEnrollment() > maxSeats) {
                batch.setMaxEnrollment(maxSeats);
            }
        }
    }

    public List<Enrollment> getEnrollment(String userId, EntityType type) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        if (type != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.TYPE).is(type));
        }
        return enrollmentDAO.runQuery(query, Enrollment.class);
    }

    public List<Enrollment> getAllEnrollment(String userId) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.fields().exclude(Enrollment.Constants.STATE_CHANGE_TIME);
        query.fields().exclude(Enrollment.Constants.STATUS_CHANGE_TIME);
        return enrollmentDAO.runQuery(query, Enrollment.class);
    }

    public List<Enrollment> getEnrollmentsForBatches(String userId, String courseId, Set<String> batchIds, EntityType type) {
        if (StringUtils.isEmpty(userId) && (!ArrayUtils.isEmpty(batchIds) || !StringUtils.isEmpty(courseId))) {
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));

        if (!ArrayUtils.isEmpty(batchIds)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        }
        if (!StringUtils.isEmpty(courseId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(courseId));
        }
        if (type != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.TYPE).is(type));
        }

        return enrollmentDAO.runQuery(query, Enrollment.class);
    }

    public List<Enrollment> getEnrollment(String userId, String courseId, String batchId, EntityType type) {
        if (StringUtils.isEmpty(userId) && (!StringUtils.isEmpty(batchId) || !StringUtils.isEmpty(courseId))) {
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));

        if (!StringUtils.isEmpty(batchId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        }
        if (!StringUtils.isEmpty(courseId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(courseId));
        }
        if (type != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.TYPE).is(type));
        }

        return enrollmentDAO.runQuery(query, Enrollment.class);
    }

    public List<Enrollment> getEnrollment(String userId, String courseId, String batchId) {
        if (StringUtils.isEmpty(userId)) {
            return null;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).ne(EntityStatus.ENDED));

        if (!StringUtils.isEmpty(batchId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        }
        if (!StringUtils.isEmpty(courseId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(courseId));
        }
        logger.info("query: " + query);

        return enrollmentDAO.runQuery(query, Enrollment.class);
    }

    public List<EnrollmentPojo> getEnrollments(GetEnrollmentsReq req, boolean exposeEmail) throws BadRequestException {
        req.verify();
        List<Enrollment> enrollments = enrollmentDAO.getEnrollments(req);
        return createEnrollmentInfos(enrollments, req.getFillBatchCourseInfos(), exposeEmail);
    }

    public List<Enrollment> getEnrollments(String userId, String courseId, String batchId, EntityType type) {
        // if (StringUtils.isEmpty(userId) && (!StringUtils.isEmpty(batchId) ||
        // !StringUtils.isEmpty(courseId))) {
        // return null;
        // }
        Query query = new Query();
        if (!StringUtils.isEmpty(userId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId));
        }
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));

        if (!StringUtils.isEmpty(batchId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batchId));
        }
        if (!StringUtils.isEmpty(courseId)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(courseId));
        }
        if (type != null) {
            query.addCriteria(Criteria.where(Enrollment.Constants.TYPE).is(type));
        }

        return enrollmentDAO.runQuery(query, Enrollment.class);
    }

    public EnrollmentPojo createEnrollmentInfo(Enrollment enrollment, boolean exposeEmail) {
        List<Enrollment> enrollments = Arrays.asList(enrollment);
        List<EnrollmentPojo> enrollmentInfos = createEnrollmentInfos(enrollments, true, exposeEmail);
        EnrollmentPojo enrollmentInfo = null;
        if (ArrayUtils.isNotEmpty(enrollmentInfos)) {
            enrollmentInfo = enrollmentInfos.get(0);
        }
        return enrollmentInfo;
    }

    public List<EnrollmentPojo> createEnrollmentInfos(List<Enrollment> enrollments,
                                                      Boolean fillBatchCourseInfos, boolean exposeEmail) {
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        if (CollectionUtils.isEmpty(enrollments)) {
            return enrollmentInfos;
        }

        Set<String> userIds = new HashSet<>();
        Set<String> batchIds = new HashSet<>();
        Set<String> courseIds = new HashSet<>();
        enrollments.forEach(e -> {
            userIds.add(e.getUserId());
            batchIds.add(e.getBatchId());
            courseIds.add(e.getCourseId());
        });

        Map<String, BatchBasicInfo> batchInfoMap = getBatchMap(batchIds, exposeEmail);
        Map<String, CourseBasicInfo> courseInfoMap = getCourseMap(courseIds, exposeEmail);
        Map<String, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);

        for (Enrollment enrollment : enrollments) {
            UserBasicInfo userBasicInfo = null;
            if (usersMap.containsKey(enrollment.getUserId())) {
                userBasicInfo = usersMap.get(enrollment.getUserId());
            }
            if (fillBatchCourseInfos.equals(Boolean.FALSE)) {
                enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, null, userBasicInfo, usersMap));
            } else {
                enrollmentInfos.add(enrollment.toEnrollmentInfoByBasicInfo(enrollment, batchInfoMap.get(enrollment.getBatchId()),
                        courseInfoMap.get(enrollment.getCourseId()), userBasicInfo, usersMap));
            }
        }

        return enrollmentInfos;
    }

    public Enrollment getEnrollmentById(String id) throws BadRequestException {

        if (StringUtils.isEmpty(id)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "enrollment ID is empty" + id);
        }
        Enrollment enrollments = enrollmentDAO.getById(id);

        return enrollments;
    }

    public EnrollmentPojo createSimplifiedEnrollmentPojo(Enrollment enrollments) {

        Map<String, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMap(Arrays.asList(enrollments.getUserId()), true);
        UserBasicInfo userBasicInfo = null;
        if (usersMap.containsKey(enrollments.getUserId())) {
            userBasicInfo = usersMap.get(enrollments.getUserId());
        }
        Course course = courseDAO.getById(enrollments.getCourseId());
        EnrollmentPojo enrollmentPojo = enrollments.toEnrollmentInfo(enrollments, null, course, userBasicInfo, null);

        return enrollmentPojo;
    }

    public List<EnrollmentPojo> getEnrollmentsByEnrollmentIds(GetEnrollmentsReq req) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
//        if (ArrayUtils.isEmpty(req.getEnrollmentIds())) {
//            return enrollmentInfos;
//        }
        List<Enrollment> enrollments = enrollmentDAO.getEnrollments(req);
        if (ArrayUtils.isEmpty(enrollments)) {
            return enrollmentInfos;
        }
        for (Enrollment enrollment : enrollments) {
            enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, null, null, null));
        }
        return enrollmentInfos;
    }

    public List<EnrollmentPojo> getEnrollmentsDataForSessionStrip(GetEnrollmentsReq req) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsDataForSessionStrip(req);
        if (ArrayUtils.isEmpty(enrollments)) {
            return enrollmentInfos;
        }
        for (Enrollment enrollment : enrollments) {
            enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, null, null, null));
        }
        return enrollmentInfos;
    }

    @Deprecated
    public List<EnrollmentPojo> getActiveStudentEnrollmentsByBatchIds(ArrayList<String> batchIds) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        List<Enrollment> enrollments = enrollmentDAO.getActiveStudentEnrollmentsByBatchIds(batchIds);
        if (ArrayUtils.isEmpty(enrollments)) {
            return enrollmentInfos;
        }
        for (Enrollment enrollment : enrollments) {
            enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, null, null, null));
        }
        return enrollmentInfos;
    }

    public EnrollmentPojo getChangeTimeForContentShare(String id) throws BadRequestException, NotFoundException {
        if (StringUtils.isEmpty(id)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Id is null");
        }
        Enrollment enrollment = enrollmentDAO.getById(id);
        EnrollmentPojo enrollmentPojo = new EnrollmentPojo();
        if (enrollment == null) {
            throw new NotFoundException(ErrorCode.BATCH_ENROLLMENT_NOT_FOUND, "Enrollment Not Found for id :" + id);
        }
        enrollmentPojo.setUserId(enrollment.getUserId());
        enrollmentPojo.setBatchId(enrollment.getBatchId());
        Long changeTime = null;
        if (ArrayUtils.isNotEmpty(enrollment.getStatusChangeTime())) {
            for (StatusChangeTime statuschangeTime : enrollment.getStatusChangeTime()) {
                if (EntityStatus.INACTIVE.equals(statuschangeTime.getNewStatus())) {
                    changeTime = statuschangeTime.getChangeTime();
                }
            }
        }
        enrollmentPojo.setChangeTime(changeTime);
        return enrollmentPojo;
    }

    public List<EnrollmentPojo> getTrialEnrollmentsForBatchList(GetBatchesForDashboardReq req) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        if (ArrayUtils.isEmpty(req.getBatchIds())) {
            return enrollmentInfos;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(req.getBatchIds()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATE).is(EnrollmentState.TRIAL));
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        if (ArrayUtils.isEmpty(enrollments)) {
            return enrollmentInfos;
        }
        for (Enrollment enrollment : enrollments) {
            enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, null, null, null));
        }
        return enrollmentInfos;
    }

    public List<GetUserDashboardEnrollmentsRes> getUserDashboardEnrollments(GetEnrollmentsReq req) throws VException {

        Long userId = req.getUserId();
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null.");
        }
        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(userId, true);
        req.setRole(userInfo.getRole());
        List<GetUserDashboardEnrollmentsRes> response = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).exists(true));
        query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.CREATION_TIME));
        enrollmentDAO.setFetchParameters(query, req.getStart(), req.getSize());
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        Set<String> entityIdsForRegFetch = new HashSet<>();
        Set<String> batchIds = new HashSet<>();

        Map<String, List<UserDashboardEnrollmentPojo>> enrollMap = new HashMap<>();
        GetRegistrationsForMultipleEntitiesReq regReq = new GetRegistrationsForMultipleEntitiesReq();
        regReq.setUserId(userId);
        if (ArrayUtils.isNotEmpty(enrollments)) {
            List<EnrollmentPojo> enrollmentPojos = createEnrollmentInfos(enrollments, true, true);
            for (EnrollmentPojo enrollment : enrollmentPojos) {
                String key = enrollment.getEntityId();
                if (StringUtils.isEmpty(key)) {
                    key = enrollment.getBatchId();
                }
                if (enrollMap.containsKey(key)) {
                    List<UserDashboardEnrollmentPojo> pojoList = enrollMap.get(key);
                    pojoList.add(new UserDashboardEnrollmentPojo(enrollment));
                } else {
                    List<UserDashboardEnrollmentPojo> pojoList = new ArrayList<>();
                    pojoList.add(new UserDashboardEnrollmentPojo(enrollment));
                    enrollMap.put(key, pojoList);
                }
                if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())) {
                    regReq.addEntity(enrollment.getEntityId(), com.vedantu.session.pojo.EntityType.OTM_BUNDLE);
                    entityIdsForRegFetch.add(enrollment.getEntityId());
                    batchIds.add(enrollment.getBatchId());
                } else {
                    regReq.addEntity(enrollment.getBatchId(), com.vedantu.session.pojo.EntityType.OTF);
                    entityIdsForRegFetch.add(enrollment.getBatchId());
                    batchIds.add(enrollment.getBatchId());
                }
            }
            Map<String, List<DashBoardInstalmentInfo>> batchIdBundleIdVsInstInfos = null;
            if (!Role.TEACHER.equals(req.getRole())) {
                batchIdBundleIdVsInstInfos = paymentManager.getIntallmentInfosForContextIds(new ArrayList<>(entityIdsForRegFetch), String.valueOf(userId));
            }

            Map<String, Orders> batchIdBundleIdVsOrderInfos = null;
            if (!Role.TEACHER.equals(req.getRole())) {
                batchIdBundleIdVsOrderInfos = paymentManager.getOrderInfosForEntityIds(new ArrayList<>(entityIdsForRegFetch), String.valueOf(userId));
            }
            List<OTFBatchSessionReportRes> oTFBatchSessionReportList = new ArrayList<>();
            List<ContentInfoResp> contentInfoRespList = new ArrayList<>();

            if (ArrayUtils.isNotEmpty(batchIds)) {
                String queryString = SCHEDULING_ENDPOINT + "/onetofew/session/getOtfBatchSessionReport?userId=" + String.valueOf(userId);
                for (String batch : batchIds) {
                    queryString += "&batchIds=" + batch;
                }
                ClientResponse resp = WebUtils.INSTANCE.doCall(queryString, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                Type listType = new TypeToken<ArrayList<OTFBatchSessionReportRes>>() {
                }.getType();
                oTFBatchSessionReportList = gson.fromJson(jsonString, listType);
                logger.info("got OtfBatchSessionReport" + oTFBatchSessionReportList);

                String queryStringLms = LMS_ENDPOINT + "cmds/contentInfo/getTestInfoForBatches?userId=" + String.valueOf(userId);
                for (String batch : batchIds) {
                    queryStringLms += "&batchIds=" + batch;
                }
                resp = WebUtils.INSTANCE.doCall(queryStringLms, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                jsonString = resp.getEntity(String.class);
                listType = new TypeToken<ArrayList<ContentInfoResp>>() {
                }.getType();
                contentInfoRespList = gson.fromJson(jsonString, listType);
                logger.info("got TestInfoForBatches" + contentInfoRespList);
            }

            Map<String, OTFBatchSessionReportRes> batchSessionReports = new HashMap<>();
            for (OTFBatchSessionReportRes report : oTFBatchSessionReportList) {
                String key = report.getBatchId();
                if (StringUtils.isNotEmpty(key)) {
                    batchSessionReports.put(key, report);
                }
            }

            Map<String, List<ContentInfoResp>> contentInfos = new HashMap<>();
            for (ContentInfoResp report : contentInfoRespList) {
                String key = report.getContextId();
                if (StringUtils.isNotEmpty(key)) {
                    if (contentInfos.containsKey(key)) {
                        List<ContentInfoResp> pojoList = contentInfos.get(key);
                        pojoList.add(report);
                    } else {
                        List<ContentInfoResp> pojoList = new ArrayList<>();
                        pojoList.add(report);
                        contentInfos.put(key, pojoList);
                    }
                }
            }

            for (String mapKey : enrollMap.keySet()) {
                GetUserDashboardEnrollmentsRes res = new GetUserDashboardEnrollmentsRes();
                List<UserDashboardEnrollmentPojo> pojos = enrollMap.get(mapKey);
                if (ArrayUtils.isNotEmpty(pojos)) {
                    res.setEntityType(pojos.get(0).getEnrollmentPojo().getEntityType());
                    res.setEntityId(pojos.get(0).getEnrollmentPojo().getEntityId());
                    res.setEntityTitle(pojos.get(0).getEnrollmentPojo().getEntityTitle());
                    if (!Role.TEACHER.equals(req.getRole())) {
                        if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(res.getEntityType())) {
                            res.setBaseIntalments(batchIdBundleIdVsInstInfos.get(res.getEntityId()));
                            res.setOrderInfo(batchIdBundleIdVsOrderInfos.get(res.getEntityId()));
                        } else {
                            res.setBaseIntalments(batchIdBundleIdVsInstInfos.get(pojos.get(0).getEnrollmentPojo().getBatchId()));
                            res.setOrderInfo(batchIdBundleIdVsOrderInfos.get(pojos.get(0).getEnrollmentPojo().getBatchId()));
                        }
                    }
                    for (UserDashboardEnrollmentPojo pojo : pojos) {
                        String key = pojo.getEnrollmentPojo().getBatchId();
                        if (StringUtils.isNotEmpty(key)) {
                            pojo.setoTFBatchSessionReportRes(batchSessionReports.get(key));
                            pojo.setTestInfos(contentInfos.get(key));
                        }
                    }
                }
                res.setEnrollments(pojos);
                response.add(res);
            }
        }
        return response;
    }

    public List<OTFEntityEnrollmentResp> getUserActiveEnrollments(GetEnrollmentsReq req) throws VException {
        Long userId = req.getUserId();
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null.");
        }

        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(userId, false);
        req.setRole(userInfo.getRole());
        List<OTFEntityEnrollmentResp> response = new ArrayList<>();
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).exists(true));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).ne(EntityStatus.ENDED));
        query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.CREATION_TIME));
        enrollmentDAO.setFetchParameters(query, req.getStart(), req.getSize());
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        Set<String> entityIdsForRegFetch = new HashSet<>();

        Map<String, List<EnrollmentPojo>> enrollMap = new HashMap<>();
        Map<String, EnrollmentPojo> batchEnrollment = new HashMap<>();
        enrollments.forEach(enrollment -> batchEnrollment.put(enrollment.getBatchId(), mapper.map(enrollment, EnrollmentPojo.class)));
        GetRegistrationsForMultipleEntitiesReq regReq = new GetRegistrationsForMultipleEntitiesReq();
        regReq.setUserId(userId);
        if (ArrayUtils.isNotEmpty(enrollments)) {

            Map<String, List<SubjectFileCountPojo>> batchContentMap = new HashMap<>();
            List<String> batchIds = new ArrayList<>();

            for (Enrollment enrollment : enrollments) {
                batchIds.add(enrollment.getBatchId());
            }

            if (req.isContentCountRequired()) {
                try {
                    logger.info("\nContent count is required.");
                    batchContentMap = getBatchShareContentMap(batchIds);
                } catch (Exception ex) {
                    logger.error("Error in fetching content count for batch with exception", ex);
                }
            }
            List<EnrollmentPojo> enrollmentPojos = createEnrollmentInfos(enrollments, true, false);
            for (EnrollmentPojo enrollment : enrollmentPojos) {
                String key = enrollment.getEntityId();

                if (batchContentMap.containsKey(enrollment.getBatchId())) {
                    enrollment.setSubjectFileCountPojos(batchContentMap.get(enrollment.getBatchId()));
                    //enrollment.setNumOfContentShared(batchContentMap.get(enrollment.getBatchId()));
                } else {
                    enrollment.setSubjectFileCountPojos(new ArrayList<>());
                }

                if (StringUtils.isEmpty(key)) {
                    key = enrollment.getBatchId();
                }
                if (enrollMap.containsKey(key)) {
                    List<EnrollmentPojo> pojoList = enrollMap.get(key);
                    pojoList.add(enrollment);
                } else {
                    List<EnrollmentPojo> pojoList = new ArrayList<>();
                    pojoList.add(enrollment);
                    enrollMap.put(key, pojoList);
                }
                if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())) {
                    regReq.addEntity(enrollment.getEntityId(), com.vedantu.session.pojo.EntityType.OTM_BUNDLE);
                    entityIdsForRegFetch.add(enrollment.getEntityId());
                } else {
                    regReq.addEntity(enrollment.getBatchId(), com.vedantu.session.pojo.EntityType.OTF);
                    entityIdsForRegFetch.add(enrollment.getBatchId());
                }
            }
            Map<String, List<BaseInstalmentInfo>> batchIdBundleIdVsInstInfos = null;
            if (!Role.TEACHER.equals(req.getRole())) {
                batchIdBundleIdVsInstInfos = registrationManager.getBaseInstalmentInfosForMultipleEntities(regReq);
                entityIdsForRegFetch.removeAll(batchIdBundleIdVsInstInfos.keySet());
                if (!entityIdsForRegFetch.isEmpty()) {
                    //fetching base instalment infos for remaining
                    List<BaseInstalment> baseInstalments = paymentManager.getBaseInstalments(null, new ArrayList<>(entityIdsForRegFetch));
                    if (ArrayUtils.isNotEmpty(baseInstalments)) {
                        for (BaseInstalment baseInstalment : baseInstalments) {
                            if (ArrayUtils.isNotEmpty(baseInstalment.getInfo())) {
                                batchIdBundleIdVsInstInfos.put(baseInstalment.getPurchaseEntityId(), baseInstalment.getInfo());
                            }
                        }
                    }
                }
            }
            for (String mapKey : enrollMap.keySet()) {
                OTFEntityEnrollmentResp res = new OTFEntityEnrollmentResp();
                List<EnrollmentPojo> pojos = enrollMap.get(mapKey);
                if (ArrayUtils.isNotEmpty(pojos)) {
                    res.setEntityType(pojos.get(0).getEntityType());
                    res.setEntityId(pojos.get(0).getEntityId());
                    res.setEntityTitle(pojos.get(0).getEntityTitle());
                    if (!Role.TEACHER.equals(req.getRole())) {
                        if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(res.getEntityType())) {
                            res.setBaseIntalments(batchIdBundleIdVsInstInfos.get(res.getEntityId()));
                        } else {
                            res.setBaseIntalments(batchIdBundleIdVsInstInfos.get(pojos.get(0).getBatchId()));
                        }
                    }
                }
                res.setEnrollments(pojos);
                response.add(res);
            }
        }
        return response;
    }

    // mget operation performed in pages of 250 - getCachedBatchBasicInfo
    @Deprecated
    public Map<String, BatchBasicInfo> getBatchBasicMapFromRedis(Collection<String> ids) {

        Map<String, BatchBasicInfo> batchMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            List<String> keys = new ArrayList<>();
            for (String id : ids) {
                if (id != null && id != "") {
                    keys.add(redisDAO.getBatchRedisKey(id));
                }
            }
            Map<String, String> valueMap = new HashMap<>();
            try {
                valueMap = redisDAO.getValuesForKeys(keys);
            } catch (Exception e) {
                logger.error("Error in redis " + e.getMessage());
            }
            if (valueMap != null && !valueMap.isEmpty()) {
                for (String id : ids) {
                    if (id != null) {
                        String value = valueMap.get(redisDAO.getBatchRedisKey(id));
                        if (!StringUtils.isEmpty(value)) {
                            BatchBasicInfo batch = gson.fromJson(value, BatchBasicInfo.class);
                            batchMap.put(id, batch);
                        }
                    }
                }
            }
        }
        return batchMap;
    }

    public Map<String, BatchBasicInfo> getBatchMap(Collection<String> ids, boolean exposeEmail) {
        if (CollectionUtils.isEmpty(ids)) {
            return new HashMap<>();
        }
        Map<String, BatchBasicInfo> batchBasicInfoMap = redisDAO.getCachedBatchBasicInfo(new ArrayList<>(ids));

        List<String> fetchList = ids.stream().filter(batchId -> !batchBasicInfoMap.containsKey(batchId)).collect(Collectors.toList());
        logger.info("fetching batch info from persistent store for ids - {}", fetchList);
        if (CollectionUtils.isEmpty(fetchList)) {
            return batchBasicInfoMap;
        }
        List<Batch> fetchedBatches = batchDAO.getBatchByIds(fetchList);

        if (CollectionUtils.isNotEmpty(fetchedBatches)) {
            Map<String, String> toCacheInRedisMap = new HashMap<>();
            Set<String> teacherIds = new HashSet<>();
            Set<Long> boardIds = new HashSet<>();

            for (Batch batch : fetchedBatches) {
                if (batch.getTeacherIds() != null) {
                    teacherIds.addAll(batch.getTeacherIds());
                }
                if (CollectionUtils.isNotEmpty(batch.getBoardTeacherPairs())) {
                    for (BoardTeacherPair subjectBoard : batch.getBoardTeacherPairs()) {
                        boardIds.add(subjectBoard.getBoardId());
                    }
                }
            }

            Map<Long, Board> subjectBoardMap = fosUtils.getBoardInfoMap(boardIds);
            Map<Long, String> boardSubjectMap = new HashMap<>();
            subjectBoardMap.forEach((k,v) -> boardSubjectMap.putIfAbsent(k, v.getName()));

            Map<String, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMap(teacherIds, exposeEmail);
            for (Batch batch : fetchedBatches) {
                BatchBasicInfo basicInfo = batch.toBatchBasicInfo(batch, null, usersMap, boardSubjectMap);
                batchBasicInfoMap.put(batch.getId(), basicInfo);
                toCacheInRedisMap.putIfAbsent(redisDAO.getBatchRedisKey(batch.getId()), gson.toJson(basicInfo));

                if (toCacheInRedisMap.size() >= 250) {
                    cacheBatchBasicInfo(toCacheInRedisMap);
                }
            }

            // cache if tail is not empty
            if (!toCacheInRedisMap.isEmpty()) {
                cacheBatchBasicInfo(toCacheInRedisMap);
            }
        }

        return batchBasicInfoMap;
    }

    private void cacheBatchBasicInfo(Map<String, String> toCacheInRedisMap) {
        try {
            redisDAO.setexKeyPairs(toCacheInRedisMap, redisExpiry);
            toCacheInRedisMap.clear();
        } catch (Exception e) {
            logger.error("error while caching batch basic info : " + e.getMessage());
        }
    }

    public Map<String, CourseBasicInfo> getCourseBasicMapFromRedis(Collection<String> ids) {

        Map<String, CourseBasicInfo> courseMap = new HashMap<>();
        if (CollectionUtils.isNotEmpty(ids)) {
            List<String> keys = new ArrayList<>();
            for (String id : ids) {
                if (id != null && id != "") {
                    keys.add(redisDAO.getCourseRedisKey(id));
                }
            }
            Map<String, String> valueMap = new HashMap<>();
            try {
                valueMap = redisDAO.getValuesForKeys(keys);
            } catch (Exception e) {
                logger.error("Error in redis " + e.getMessage());
            }
            if (valueMap != null && !valueMap.isEmpty()) {
                for (String id : ids) {
                    if (id != null) {
                        String value = valueMap.get(redisDAO.getCourseRedisKey(id));
                        if (!StringUtils.isEmpty(value)) {
                            CourseBasicInfo course = gson.fromJson(value, CourseBasicInfo.class);
                            courseMap.put(id, course);
                        }
                    }
                }
            }
        }
        return courseMap;
    }

    public Map<String, CourseBasicInfo> getCourseMap(Collection<String> ids, boolean exposeEmail) {
        Map<String, CourseBasicInfo> boardsMap = new HashMap<>();
        if (CollectionUtils.isEmpty(ids)) {
            logger.info("ids is null.");
            return boardsMap;
        }
        boardsMap = getCourseBasicMapFromRedis(ids);
        List<String> fetchList = new ArrayList<>();

        if (!boardsMap.isEmpty()) {
            for (String id : ids) {
                if (!boardsMap.keySet().contains(id)) {
                    logger.info("board id" + id);
                    fetchList.add(id);
                }
            }
        } else {
            fetchList.addAll(ids);
        }
        if (ArrayUtils.isNotEmpty(fetchList)) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Course.Constants.ID).in(fetchList));
            List<Course> boards = courseDAO.runQuery(query, Course.class);
            if (boards != null) {
                Map<String, String> redisMap = new HashMap<>();
                Set<String> teacherIds = new HashSet<>();
                Set<Long> boradIds = new HashSet<>();
                for (Course board : boards) {
                    if (board.getTeacherIds() != null) {
                        teacherIds.addAll(board.getTeacherIds());
                    }
                    if (ArrayUtils.isNotEmpty(board.getBoardTeacherPairs())) {
                        board.getBoardTeacherPairs().stream().filter(s -> s.getBoardId() != null).forEach(s -> boradIds.add(s.getBoardId()));
                    }
                }

                Map<String, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMap(teacherIds, exposeEmail);
                for (Course board : boards) {
                    CourseBasicInfo basicInfo = board.toCourseBasicInfo(board, usersMap, getBoardSubjectMap(boradIds));
                    if (ArrayUtils.isNotEmpty(board.getCurriculum())) {
                        basicInfo.setCurriculumPojos(board.getCurriculum());
                    }
                    boardsMap.put(board.getId(), basicInfo);
                    redisMap.put(redisDAO.getCourseRedisKey(board.getId()), gson.toJson(basicInfo));
                }
                try {
                    redisDAO.setexKeyPairs(redisMap, redisExpiry);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    logger.error("error in redis " + e.getMessage());
                }
                logger.info("getBoardsMap size " + boards.size());
            }
        }

        return boardsMap;
    }

    public void deleteRedisKey(String key) {
        if (StringUtils.isNotEmpty(key)) {
            try {
                logger.info("Deleting redis for key " + key);
                redisDAO.del(key);
            } catch (InternalServerErrorException | BadRequestException e) {
                // TODO Auto-generated catch block
                logger.error("error in deleting cache " + e);
            }
        }
    }

    public Map<Long, String> getBoardSubjectMap(Set<Long> boardIds) {
        Map<Long, String> boardSubMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(boardIds)) {
            Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            boardSubMap = boardMap.values().stream().collect(Collectors.toMap(Board::getId, Board::getName));

        }

        return boardSubMap;

    }

    /*
    Add @Valid in controllers
     */
    public EnrollmentPojo changeBatchForEnrollment(BatchChangeReq req) throws BadRequestException, NotFoundException, ForbiddenException {
        req.verify();

        EnrollmentPojo newEnrollmentPojo = null;
        Long userId = req.getUserId();
        if (userId == null) {
            Set<String> emailIds = new HashSet<>();
            String email = req.getEmail().trim().toLowerCase();
            emailIds.add(email);
            Map<String, UserInfo> userInfoMap = fosUtils.getUserInfosFromEmails(emailIds, false);
            if (userInfoMap == null || userInfoMap.isEmpty()) {
                throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found for email : " + req.getEmail());
            }
            userId = userInfoMap.get(email).getUserId();
        }
        List<String> batchIds = new ArrayList<>();
        batchIds.add(req.getPreviousBatchId());
        batchIds.add(req.getNewBatchId());
        List<Batch> batches = batchDAO.getBatchByIds(batchIds);

        if (ArrayUtils.isEmpty(batches)) {
            throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "Batch not found");
        }

        if (batches.size() != 2) {
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,
                    "BATCH_NOT_FOUND ");
        }
        Batch prevBatch = null;
        Batch newBatch = null;
        for (Batch batch : batches) {

            if (req.getPreviousBatchId().equals(batch.getId())) {
                prevBatch = batch;
            } else if (req.getNewBatchId().equals(batch.getId())) {
                newBatch = batch;
            }
        }
        Map<String, Batch> batchMap = new HashMap<>();
        batchMap = batches.stream().collect(Collectors.toMap(Batch::getId, a -> a));

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(req.getPreviousBatchId()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));

        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        if (ArrayUtils.isEmpty(enrollments)) {
            throw new NotFoundException(ErrorCode.ENROLLMENT_NOT_FOUND, "enrollment not found. ");
        }
        if (enrollments.size() > 1) {
            throw new BadRequestException(ErrorCode.MULTIPLE_OTF_ENROLLMENTS_FOUND,
                    "multiple non ended enrollments found for req ");
        }

        Enrollment enrollment = enrollments.get(0);

        EnrollmentPojo oldEnrollmentPojo = enrollment.toEnrollmentInfo(enrollment, null, null, null, null);
        if (newBatch != null) {

            if (!EntityStatus.ACTIVE.equals(newBatch.getBatchState())) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Batch is inactive. id: "
                        + newBatch.getId());
            }

            if (!enrollment.getCourseId().equals(newBatch.getCourseId())) {
                throw new ForbiddenException(ErrorCode.COURSE_ID_DIFFERENT, "COURSE_ID_DIFFERENT "
                        + newBatch.getId());
            }

            enrollment.setBatchId(req.getNewBatchId());

            /* section change scenario */
            boolean sectionChangeNeeded = false;
            if(prevBatch != null && (prevBatch.getHasSections() || newBatch.getHasSections()) )
                sectionChangeNeeded = true;

            if(prevBatch != null && sectionChangeNeeded){
                String previousSectionId = "";
                String newSectionId = "";

                if(enrollment.getSectionId() != null)
                    previousSectionId = enrollment.getSectionId();

                if(newBatch.getHasSections()) {
                    CreateSectionEnrollmentReq _r = new CreateSectionEnrollmentReq();
                    _r.setBatchId(newBatch.getId());
                    _r.setUserId(enrollment.getUserId());
                    _r.setEnrollment(enrollment);
                    _r.setDoDecrement(Boolean.FALSE);
                    boolean status = sectionManager.assignStudentToSection(_r);
                    if (!status) {
                        throw new BadRequestException(ErrorCode.SEATS_FULL_SHUFFLING_NOT_POSSIBLE, "Section seats are full so batch shuffling not possible");
                    }

                    logger.info("ENROLL {}", enrollment);
                    newSectionId = enrollment.getSectionId();
                }

                logger.info("prev {}, new {}",previousSectionId,newSectionId);
                SectionChangeTime sectionChangeTime = new SectionChangeTime();
                sectionChangeTime.setChangedBy(req.getCallingUserId());
                sectionChangeTime.setChangeTime(System.currentTimeMillis());
                sectionChangeTime.setPreviousSectionId(previousSectionId);
                sectionChangeTime.setNewSectionId(newSectionId);
                if(ArrayUtils.isEmpty(enrollment.getSectionChangeTime())){
                    enrollment.setSectionChangeTime(new ArrayList<>());
                }
                enrollment.getSectionChangeTime().add(sectionChangeTime);

                // 1 -- unset sectionId and sectionState in case new batch does not have sections
                if(newBatch.getHasSections().equals(Boolean.FALSE) && newSectionId.isEmpty()){
                    enrollmentDAO.unsetSectionFromEnrollment(enrollment.getId());
                    enrollment.setSectionId(null);
                    enrollment.setSectionState(null);
                }

                // 2 - validate old and new section
                sectionManager.adjustSeatsVacancyForSections(previousSectionId,newSectionId);
            }

            BatchChangeTime batchChangeTime = new BatchChangeTime();
            batchChangeTime.setChangeTime(System.currentTimeMillis());
            batchChangeTime.setReason(req.getReason());
            batchChangeTime.setPreviousBatchId(req.getPreviousBatchId());
            batchChangeTime.setNewBatchId(req.getNewBatchId());
            batchChangeTime.setChangedBy(req.getCallingUserId());
            if (ArrayUtils.isEmpty(enrollment.getBatchChangeTime())) {
                enrollment.setBatchChangeTime(new ArrayList<>());
            }
            enrollment.getBatchChangeTime().add(batchChangeTime);

            enrollmentDAO.create(enrollment, req.getCallingUserId());
            logger.info("Enrollment updated");
            // Update Batch
            updateBatchEnrollments(userId.toString(), prevBatch, false);
            updateBatchEnrollments(userId.toString(), newBatch, true);
            newEnrollmentPojo = enrollment.toEnrollmentInfo(enrollment, null, null, null, null);

            //Update Calendar
            triggerBatchChangeEnrollments(oldEnrollmentPojo, newEnrollmentPojo);
        }

        try {
            if (oldEnrollmentPojo.getPurchaseContextType() == null || !EnrollmentPurchaseContext.BUNDLE.equals(oldEnrollmentPojo.getPurchaseContextType())) {
                enrollmentConsumptionManager.updateBatchForEnrollmentConsumption(enrollment.getId(), req.getNewBatchId());
            }
        } catch (Exception ex) {
            logger.error("Error while updating the enrollment consumption batchId for : " + enrollment + " error: " + ex.getMessage());
        }

        Map<String, String> gttPayload = new HashMap<>();
        gttPayload.put("userId", enrollment.getUserId());
        gttPayload.put("batchId", req.getPreviousBatchId());
        EnrollmentPojo pojo = new EnrollmentPojo();
        pojo.setId(enrollment.getId());
        // mark deleted for old batch gtt
        pojo.setStatus(EntityStatus.ENDED);
        pojo.setCreationTime(enrollment.getCreationTime());
        pojo.setLastUpdated(System.currentTimeMillis());
        gttPayload.put("enrolmentInfo", gson.toJson(pojo));
        sqsManager.sendToSQS(SQSQueue.GTT_ENROLMENT_OPS, SQSMessageType.GTT_ENROLMENT_STATUS_CHANGE_TASK, gson.toJson(gttPayload), enrollment.getUserId());

        gttPayload = new HashMap<>();
        gttPayload.put("userId", enrollment.getUserId());
        gttPayload.put("batchId", req.getNewBatchId());
        pojo = new EnrollmentPojo();
        pojo.setId(enrollment.getId());
        // create gtt for new batch ids
        pojo.setStatus(enrollment.getStatus());
        pojo.setCreationTime(enrollment.getCreationTime());
        pojo.setLastUpdated(System.currentTimeMillis());
        gttPayload.put("enrolmentInfo", gson.toJson(pojo));
        sqsManager.sendToSQS(SQSQueue.GTT_ENROLMENT_OPS, SQSMessageType.GTT_ENROLMENT_STATUS_CHANGE_TASK, gson.toJson(gttPayload), enrollment.getUserId());


        return newEnrollmentPojo;
    }

    public void triggerBatchChangeEnrollments(EnrollmentPojo oldEnrollment, EnrollmentPojo newEnrollment) {
        EnrollmentUpdatePojo pojo = new EnrollmentUpdatePojo();
        pojo.setPreviousEnrollment(oldEnrollment);
        pojo.setNewEnrollment(newEnrollment);
        Map<String, Object> payload = new HashMap<>();
        payload.put("pojo", pojo);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_CALENDAR_FOR_ENROL_EVENT, payload);
        asyncTaskFactory.executeTask(params);

        try {
            Map<String, Object> payload2 = new HashMap<>();
            Enrollment enrollment = new Enrollment(newEnrollment.getUserId(), newEnrollment.getBatchId(), newEnrollment.getCourseId(),
                    null, Role.STUDENT, newEnrollment.getStatus(), newEnrollment.getType());

            payload2.put("enrollment", enrollment);
            payload2.put("event", BatchEventsOTF.USER_ACTIVE);
            AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.ENROLLMENT_EVENTS_TRIGGER, payload2);
            asyncTaskFactory.executeTask(params2);
        } catch (Exception e) {
            logger.info("error in async task for mark status");
        }
    }

    public void updateBatchEnrollments(String userId, Batch batch, boolean add) throws BadRequestException {

        if (batch == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batch is null");
        }
        updateMaxEnrollmentBatch(batch);
        batchDAO.save(batch);
    }

    public PlatformBasicResponse refundForEnrollment(EnrollmentRefundReq enrollmentRefundReq, Enrollment enrollment) throws BadRequestException, ForbiddenException {

        String enrollmentId = enrollmentRefundReq.getEnrollmentId();

        boolean result = false;

        if (enrollmentRefundReq.getEndedTime() == null) {
            enrollmentRefundReq.setEndedTime(System.currentTimeMillis());
        }

        if (EntityStatus.ENDED.equals(enrollment.getStatus())) {
            return new PlatformBasicResponse(false, null, "Enrollment already ended");
        }

        Long currentTime = System.currentTimeMillis();

        if (currentTime - enrollmentRefundReq.getEndedTime() > maxRefundTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "End time should be within the last 35 days");
        }
        if(enrollment.getPurchaseContextType() != null  && enrollment.getPurchaseContextType() == EnrollmentPurchaseContext.BUNDLE ){
            try {
                enrollmentConsumptionManager.performEndEnrollmentTasks(enrollment ,enrollmentRefundReq);
                result=true;
            } catch (Exception e) {
                result=false;
            }
        }
        else {
            result = enrollmentConsumptionManager.refundForEnrollment(enrollmentRefundReq, enrollment);
        }
        ///*result = enrollmentConsumptionManager.makeEnrollmentPayment(enrollmentConsumptionPayment, null);
        if (!result) {
            logger.error("Refund failed for: " + enrollmentId);
        }
        return new PlatformBasicResponse(result, result == true ? "refund successful" : "", result == false ? "refund failed" : "");

    }

    public EndSubscriptionRes getRefundAmountForEnrollment(EnrollmentRefundReq enrollmentRefundReq, Enrollment enrollment) throws BadRequestException, NotFoundException {

        String enrollmentId = enrollmentRefundReq.getEnrollmentId();

        EndSubscriptionRes endSubscriptionRes = new EndSubscriptionRes();

        if (enrollmentRefundReq.getEndedTime() == null) {
            enrollmentRefundReq.setEndedTime(System.currentTimeMillis());
        }

        Long currentTime = System.currentTimeMillis();

        if (currentTime - enrollmentRefundReq.getEndedTime() > maxRefundTime) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "End time should be within the last 35 days");
        }
        Batch batch = batchDAO.getById(enrollment.getBatchId());
        Session session = sqlSessionfactory.getSessionFactory().openSession();
        List<EnrollmentTransaction> enrollmentTransactionList = new ArrayList<>();
        EnrollmentConsumption enrollmentConsumption = null;
        try {
            enrollmentTransactionList = enrollmentTransactionDAO.getForRefundCheck(enrollmentId, enrollmentRefundReq.getEndedTime(), session, enrollment.getState());
            enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentId, true, session, LockMode.NONE);
        } catch (Exception ex) {
            logger.error("Exception: fetching enrollment transaction" + ex.getMessage());

        } finally {
            session.close();
        }

        logger.info("Enrollment Transactions: " + enrollmentTransactionList);

        EnrollmentConsumptionPayment enrollmentConsumptionPayment = new EnrollmentConsumptionPayment();

        if (enrollmentTransactionList.size() > 0 && EnrollmentState.REGULAR.equals(enrollment.getState())) {
            enrollmentConsumptionPayment.setCustomerGrievence(true);

            for (EnrollmentTransaction enrollmentTransaction : enrollmentTransactionList) {
                if (enrollmentConsumptionPayment.getCgAmtP() == null) {

                    enrollmentConsumptionPayment.setCgAmtP(enrollmentTransaction.getAmtPaidP());

                } else {

                    enrollmentConsumptionPayment.setCgAmtP(enrollmentConsumptionPayment.getCgAmtP() + enrollmentTransaction.getAmtPaidP());

                }

                if (enrollmentConsumptionPayment.getCgAmtNP() == null) {

                    enrollmentConsumptionPayment.setCgAmtNP(enrollmentTransaction.getAmtPaidNP());

                } else {

                    enrollmentConsumptionPayment.setCgAmtNP(enrollmentConsumptionPayment.getCgAmtNP() + enrollmentTransaction.getAmtPaidNP());

                }

            }

        }
        if ((!enrollmentConsumptionPayment.isCustomerGrievence()) && EnrollmentState.TRIAL.equals(enrollment.getState())) {
            enrollmentConsumptionPayment.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_REFUND);
        } else {
            enrollmentConsumptionPayment.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REFUND);
        }

        enrollmentConsumptionPayment.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
        enrollmentConsumptionPayment.setEnrollmentId(enrollment.getId());

        if (enrollmentConsumption == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "EnrollmentConsumption not found for: " + enrollment.getId());
        }

        if (batch.isContentBatch()) {

            if (enrollmentConsumption.getAmtPaidLeft() != enrollmentConsumption.getAmtPaid()) {
                endSubscriptionRes.setNonPromotionalAmount(0);
                endSubscriptionRes.setPromotionalAmount(0);
            } else {
                endSubscriptionRes.setNonPromotionalAmount(enrollmentConsumption.getAmtPaidNPLeft());
                endSubscriptionRes.setPromotionalAmount(enrollmentConsumption.getAmtPaidPLeft());
            }
            return endSubscriptionRes;

        }

        float discountToAmtRatio = enrollmentConsumption.getDiscountToAmtRatio() == null ? 0 : enrollmentConsumption.getDiscountToAmtRatio();

        int nonDiscountNegativeMargin = (int) ((1 - discountToAmtRatio) * enrollmentConsumption.getNegativeMargin());

        logger.info("EnrollmentConsumption: " + enrollmentConsumption);

        logger.info("nonDiscountNegativeMargin: " + nonDiscountNegativeMargin);
        if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
            if (enrollmentConsumption.getAmtPaidPLeft() + enrollmentConsumptionPayment.getCgAmtP() >= nonDiscountNegativeMargin) {
                endSubscriptionRes.setPromotionalAmount(enrollmentConsumption.getAmtPaidPLeft() + -nonDiscountNegativeMargin + enrollmentConsumptionPayment.getCgAmtP());
                endSubscriptionRes.setNonPromotionalAmount(enrollmentConsumption.getAmtPaidNPLeft() + enrollmentConsumptionPayment.getCgAmtNP());
            } else {

                endSubscriptionRes.setPromotionalAmount(0);
                endSubscriptionRes.setNonPromotionalAmount(enrollmentConsumption.getAmtPaidNPLeft() + enrollmentConsumption.getRegAmtNPLeft() + enrollmentConsumptionPayment.getCgAmtNP() - (nonDiscountNegativeMargin - enrollmentConsumption.getAmtPaidPLeft() - enrollmentConsumptionPayment.getCgAmtP()));
                if (endSubscriptionRes.getNonPromotionalAmount() < 0) {
                    endSubscriptionRes.setNonPromotionalAmount(0);
                }

            }
        } else {
            endSubscriptionRes.setPromotionalAmount(enrollmentConsumption.getRegAmtPaidP());
            endSubscriptionRes.setNonPromotionalAmount(enrollmentConsumption.getRegAmtPaidNP());
        }

        return endSubscriptionRes;

    }

    public PlatformBasicResponse endBatchConsumption(String batchId) {
        List<String> batchIds = new ArrayList<>();
        batchIds.add(batchId);
        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsForBatches(batchIds);
        String errorString = "";
        for (Enrollment enrollment : enrollments) {

            if (EntityStatus.ENDED.equals(enrollment.getStatus())) {
                continue;
            }

            try {
                boolean result = enrollmentConsumptionManager.performEndEnrollmentConsumption(enrollment, EnrollmentTransactionContextType.END_BATCH_CONSUMPTION);
                if (!result) {
                    errorString += "|| " + enrollment.getId() + " : failed";
                }
            } catch (Exception ex) {
                logger.warn("Exception: End batch consumption failed for: " + enrollment);
                errorString += "|| " + enrollment.getId() + " : failed";
            }

        }

        return new PlatformBasicResponse(StringUtils.isEmpty(errorString), StringUtils.isNotEmpty(errorString) ? errorString : null, StringUtils.isEmpty(errorString) ? "Succesfull" : null);
    }

    public void makeRegistrationPayment(MigrationRequest req) throws NotFoundException, InternalServerErrorException, VException {

        Enrollment enrollment = enrollmentDAO.getById(req.getEnrollmentId());

        if (req.isIsBatch()) {
            Batch batch = batchDAO.getById(enrollment.getBatchId());
            if (batch.getDuration() - req.getDurationToBeSubtracted() < DateTimeUtils.MILLIS_PER_HOUR) {
                batch.setDuration(DateTimeUtils.MILLIS_PER_HOUR + req.getDurationToBeSubtracted());
            }
            enrollment.setHourlyRate((int) (req.getAmountToPay() / ((batch.getDuration() - req.getDurationToBeSubtracted()) / DateTimeUtils.MILLIS_PER_HOUR)));
        } else {
            Course course = courseDAO.getById(enrollment.getCourseId());
            if (course.getDuration() - req.getDurationToBeSubtracted() < DateTimeUtils.MILLIS_PER_HOUR) {
                course.setDuration(DateTimeUtils.MILLIS_PER_HOUR + req.getDurationToBeSubtracted());
            }

            enrollment.setHourlyRate((int) (req.getAmountToPay() / ((course.getDuration() - req.getDurationToBeSubtracted()) / DateTimeUtils.MILLIS_PER_HOUR)));
        }
        enrollmentDAO.save(enrollment);

        MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
        makePaymentRequest.setEnrollmentId(enrollment.getId());
        makePaymentRequest.setAmt(req.getRegPaid());
        makePaymentRequest.setAmtP(req.getRegPPaid());
        makePaymentRequest.setAmtNP(req.getRegNPPaid());
        makePaymentRequest.setAmtFromDiscount(req.getDiscount());
        makePaymentRequest.setOrderId(req.getOrderId());
        makePaymentRequest.setBatchId(enrollment.getBatchId());
        makePaymentRequest.setCourseId(enrollment.getCourseId());
        makePaymentRequest.setAmtToBePaid(req.getAmountToPay());
        makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_PAYMENT);
        enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, 1f);

    }

    public void makeMigrationPayment(MigrationRequest req) throws NotFoundException, InternalServerErrorException, VException {

        if (req.getEnrollmentId() == null) {
            throw new NotFoundException(ErrorCode.ENROLLMENT_NOT_FOUND, "Enrollmentid is null ");
        }

        Enrollment enrollment = enrollmentDAO.getById(req.getEnrollmentId());

        if (enrollment.getHourlyRate() == 0) {
            if (req.isIsBatch()) {
                Batch batch = batchDAO.getById(enrollment.getBatchId());
                if (batch.getDuration() - req.getDurationToBeSubtracted() < DateTimeUtils.MILLIS_PER_HOUR) {
                    batch.setDuration(DateTimeUtils.MILLIS_PER_HOUR + req.getDurationToBeSubtracted());
                }
                enrollment.setHourlyRate((int) (req.getAmountToPay() / ((batch.getDuration() - req.getDurationToBeSubtracted()) / DateTimeUtils.MILLIS_PER_HOUR)));
            } else {
                Course course = courseDAO.getById(enrollment.getCourseId());
                if (course.getDuration() - req.getDurationToBeSubtracted() < DateTimeUtils.MILLIS_PER_HOUR) {
                    course.setDuration(DateTimeUtils.MILLIS_PER_HOUR + req.getDurationToBeSubtracted());
                }
                enrollment.setHourlyRate((int) (req.getAmountToPay() / ((course.getDuration() - req.getDurationToBeSubtracted()) / DateTimeUtils.MILLIS_PER_HOUR)));
            }
            enrollmentDAO.save(enrollment);
        }

        MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
        makePaymentRequest.setEnrollmentId(enrollment.getId());
        makePaymentRequest.setAmt(req.getRegPaid());
        makePaymentRequest.setAmtP(req.getRegPPaid());
        makePaymentRequest.setAmtNP(req.getRegNPPaid());
        makePaymentRequest.setAmtFromDiscount(req.getDiscount());
        makePaymentRequest.setAmtToBePaid(req.getAmountToPay());
        makePaymentRequest.setOrderId(req.getOrderId());
        makePaymentRequest.setBatchId(enrollment.getBatchId());
        makePaymentRequest.setCourseId(enrollment.getCourseId());
        if (req.getInstallmentId() != null) {
            makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.INSTALLMENT_PAYMENT);
            makePaymentRequest.setEnrollmentTransactionContextId(req.getInstallmentId());
        } else {
            makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.BULK_PAYMENT);
        }
        enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, 1f);

    }

    public void sessionConsumption(MigrationRequest req) throws NotFoundException {

        SessionSnapshot sessionSnapshot = new SessionSnapshot();
        sessionSnapshot.setBatchIds(req.getBatchIds());
        sessionSnapshot.setEndTime(req.getEndTime());
        sessionSnapshot.setStartTime(req.getStartTime());
        sessionSnapshot.setSessionId(req.getSessionId());
        sessionSnapshot.setoTMSessionType(req.getoTMSessionType());

        boolean checkForRegistrationConsumption = false;
        if (sessionSnapshot.isTrialSnapShot()) {

            checkForRegistrationConsumption = true;

        }

        //List<EnrollmentConsumption> enrollmentConsumptionList = enrollmentConsumptionDAO.getByBatchId(sessionSnapshot.getBatchIds(), null);
        //List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsOfBatch(sessionSnapshot.getBatchIds());
        Session session = sqlSessionfactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        EnrollmentConsumption enrollmentConsumption = null;
        try {
            transaction.begin();
            enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(req.getEnrollmentId(), null, session, LockMode.PESSIMISTIC_WRITE);
            if (enrollmentConsumption == null) {
                throw new NotFoundException(ErrorCode.ENROLLMENT_CONSUMPTION_NOT_FOUND, "enrollmentConsumption not found for enrollmentId " + req.getEnrollmentId());
            }

            Enrollment enrollment = enrollmentDAO.getById(req.getEnrollmentId());
            //Thread.sleep(1000);
            logger.info("EnrollmentConsumption: " + enrollmentConsumption);
            logger.info("Enrollment: " + enrollment);
            logger.info("CheckForRegistration: " + checkForRegistrationConsumption);

            if (enrollment == null) {
                logger.error("Enrollment and EnrollmentConsumption Mismatch: " + enrollmentConsumption.getEnrollmentId());
                throw new NotFoundException(ErrorCode.ENROLLMENT_NOT_FOUND, "enrollment not found for " + req.getEnrollmentId());
            }

            if (enrollmentConsumption.isEnrollmentEnded()) {
                logger.info("Enrollment Ended");
                return;
            }

            if (!checkForRegistrationConsumption) {
                //To do add regular and trial check for enrollment
                // && EnrollmentState.REGULAR.equals(enrollment.getState())
                if (enrollmentConsumption.getHourlyrate() == null) {
                    logger.error("Hourly rate not available for enrollment consumption: " + enrollmentConsumption);
                    throw new NotFoundException(ErrorCode.ENTITY_NOT_FOUND, "hourly rate not found for enrollmentConsumption, enrollmentId: " + enrollmentConsumption.getEnrollmentId());
                }
                int amtToBeDeducted = (int) (enrollmentConsumption.getHourlyrate() * ((float) (sessionSnapshot.getEndTime() - sessionSnapshot.getStartTime()) / DateTimeUtils.MILLIS_PER_HOUR));

                if ((EntityStatus.ACTIVE.equals(req.getStatus())) || (EntityStatus.INACTIVE.equals(req.getStatus()) && (enrollmentConsumption.getAmtPaidLeft() - enrollmentConsumption.getNegativeMargin() >= amtToBeDeducted))) {
                    EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

                    enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                    enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                    enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                    enrollmentTransaction.setEnrollmentId(enrollment.getId());
                    enrollmentTransaction.setEnrollmentTransactionContextId(sessionSnapshot.getSessionId());
                    enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.SESSION_CONSUMPTION);
                    if (EntityStatus.ACTIVE.equals(req.getStatus())) {
                        enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                    } else {
                        enrollmentTransaction.setConsumptionType(EntityStatus.INACTIVE);
                    }

                    enrollmentConsumptionManager.prepareEnrollmentTransactionForRegularConsumption(enrollmentTransaction, enrollmentConsumption, amtToBeDeducted);
                    enrollmentConsumptionManager.takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);
                    enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                    if (enrollmentTransaction.getAmtPaid() > 0) {
                        enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
                    }
                    OTMConsumptionReq oTMConsumptionReq = enrollmentConsumptionManager.createOTMConsumptionReq(enrollmentConsumption, enrollmentTransaction);
                    enrollmentConsumptionManager.makeDineroTransaction(oTMConsumptionReq);

                } else {
                    logger.info("Enrollment Inactive");
                }
            }

            if (enrollmentConsumption.getRegAmtLeft() > 0) {

                EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();
                enrollmentTransaction.setRegAmtPaid(enrollmentConsumption.getRegAmtLeft());
                enrollmentTransaction.setRegAmtPaidFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());
                enrollmentTransaction.setRegAmtPaidNP(enrollmentConsumption.getRegAmtNPLeft());
                enrollmentTransaction.setRegAmtPaidP(enrollmentConsumption.getRegAmtPLeft());

                enrollmentConsumption.setRegAmtLeft(0);
                enrollmentConsumption.setRegAmtLeftFromDiscount(0);
                enrollmentConsumption.setRegAmtNPLeft(0);
                enrollmentConsumption.setRegAmtPLeft(0);

                enrollmentTransaction.setConsumptionType(EntityStatus.ACTIVE);
                enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                enrollmentTransaction.setEnrollmentTransactionContextId(req.getSessionId());
                enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION);
                enrollmentTransaction.setEnrollmentId(enrollment.getId());
                enrollmentConsumptionManager.takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);

                enrollmentConsumptionDAO.update(enrollmentConsumption, session);
                enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
                //enrollmentConsumptionManager.makeEnrollmentPayment(enrollmentConsumptionPayment1, null);
                OTMConsumptionReq oTMConsumptionReq = enrollmentConsumptionManager.createOTMConsumptionReq(enrollmentConsumption, enrollmentTransaction);
                enrollmentConsumptionManager.makeDineroTransaction(oTMConsumptionReq);

            }

            transaction.commit();
        } catch (Exception ex) {
            logger.error("Error fetching enrollmentConsumption " + ex.getMessage());
        } finally {
            session.close();
        }

    }

    public void statusChange(String enrollmentId, EntityStatus entityStatus, EnrollmentState enrollmentState) throws BadRequestException, NotFoundException, VException, VException {

        Enrollment enrollment = enrollmentDAO.getById(enrollmentId);

        if (EntityStatus.ENDED.equals(entityStatus)) {

            if (EnrollmentState.TRIAL.equals(enrollmentState)) {

                Session session = sqlSessionfactory.getSessionFactory().openSession();
                Transaction transaction = session.getTransaction();
                logger.info("In trial ended status change");
                try {
                    transaction.begin();
                    EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentId, null, session, LockMode.NONE);

                    logger.info("Enrollment Consumption before trial end: " + enrollmentConsumption);
                    EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

                    if (enrollmentConsumption.getRegAmtLeft() > 0) {
                        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                        enrollmentTransaction.setRegAmtPaid(enrollmentConsumption.getRegAmtLeft());
                        enrollmentTransaction.setRegAmtPaidNP(enrollmentConsumption.getRegAmtNPLeft());
                        enrollmentTransaction.setRegAmtPaidP(enrollmentConsumption.getRegAmtPLeft());
                        enrollmentTransaction.setRegAmtPaidFromDiscount(enrollmentConsumption.getRegAmtLeftFromDiscount());
                        enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                        enrollmentTransaction.setEnrollmentId(enrollmentId);
                        enrollmentTransaction.setEnrollmentTransactionContextId(enrollmentId);
                        enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_REFUND);

                    }

                    enrollmentConsumption.setRegAmtLeft(0);
                    enrollmentConsumption.setRegAmtPLeft(0);
                    enrollmentConsumption.setRegAmtNPLeft(0);
                    enrollmentConsumption.setRegAmtLeftFromDiscount(0);

                    enrollmentConsumptionManager.takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);
                    if (enrollmentTransaction.getRegAmtPaid() > 0) {
                        enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
                    }
                    enrollmentConsumptionDAO.update(enrollmentConsumption, session);

                    transaction.commit();
                } catch (Exception ex) {

                    logger.error("Exception: " + ex.getMessage());

                } finally {
                    session.close();
                }

            } else {

                EnrollmentRefundReq enrollmentRefundReq = new EnrollmentRefundReq();
                enrollmentRefundReq.setEnrollmentId(enrollmentId);
                enrollmentRefundReq.setEndedTime(System.currentTimeMillis());
                EndSubscriptionRes endSubscriptionRes = getRefundAmountForEnrollment(enrollmentRefundReq, enrollment);

                OTMRefundAdjustmentReq oTMRefundAdjustmentReq = new OTMRefundAdjustmentReq();
                oTMRefundAdjustmentReq.setAmount(endSubscriptionRes.getNonPromotionalAmount());
                oTMRefundAdjustmentReq.setTrial(false);
                oTMRefundAdjustmentReq.setUserId(Long.parseLong(enrollment.getUserId()));
                oTMRefundAdjustmentReq.setBatchId(enrollment.getBatchId());
                oTMRefundAdjustmentReq.setEnrollmentId(enrollmentId);

                OTMRefundAdjustmentRes oTMRefundAdjustmentRes = paymentManager.makeRefundAdjustment(oTMRefundAdjustmentReq);

                if (oTMRefundAdjustmentRes.getExtraRefundAmount() != null && oTMRefundAdjustmentRes.getExtraRefundAmount() >= 0) {
                    Session session = sqlSessionfactory.getSessionFactory().openSession();
                    Transaction transaction = session.getTransaction();
                    try {
                        transaction.begin();
                        EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentId, null, session, LockMode.NONE);

                        enrollmentConsumption.setAmtPaidNP(enrollmentConsumption.getAmtPaidNP() + oTMRefundAdjustmentRes.getExtraRefundAmount());
                        enrollmentConsumption.setAmtPaid(enrollmentConsumption.getAmtPaid() + oTMRefundAdjustmentRes.getExtraRefundAmount());

                        enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() + oTMRefundAdjustmentRes.getExtraRefundAmount());
                        enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() + oTMRefundAdjustmentRes.getExtraRefundAmount());

                        EnrollmentTransaction enrollmentTransaction = new EnrollmentTransaction();

                        enrollmentTransaction.setAmtLeft(enrollmentConsumption.getAmtPaidLeft());
                        enrollmentTransaction.setAmtLeftNP(enrollmentConsumption.getAmtPaidNPLeft());
                        enrollmentTransaction.setAmtLeftP(enrollmentConsumption.getAmtPaidPLeft());
                        enrollmentTransaction.setEnrollmentTransactionType(EnrollmentTransactionType.CREDIT);
                        enrollmentTransaction.setAmtPaid(oTMRefundAdjustmentRes.getExtraRefundAmount());
                        enrollmentTransaction.setAmtPaidNP(oTMRefundAdjustmentRes.getExtraRefundAmount());
                        enrollmentTransaction.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                        enrollmentTransaction.setEnrollmentId(enrollmentId);
                        enrollmentTransaction.setEnrollmentTransactionContextId(enrollmentId);
                        enrollmentTransaction.setNegativeMarginLeft(enrollmentConsumption.getNegativeMargin());
                        enrollmentTransaction.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REFUND_ADJUSTMENT);
                        enrollmentConsumptionManager.takeSnapshotEnrollmentConsumption(enrollmentTransaction, enrollmentConsumption);
                        enrollmentTransactionDAO.save(enrollmentTransaction, session, null);
                        enrollmentConsumptionDAO.update(enrollmentConsumption, session);

                        float discountToAmtRatio = enrollmentConsumption.getDiscountToAmtRatio();
                        int nonDiscountNegativeMargin = (int) ((1 - discountToAmtRatio) * enrollmentConsumption.getNegativeMargin());
                        int negativeComponent = 0;
                        //if(enrollmentConsumption.getAmtPaidPLeft() < nonDiscountNegativeMargin){
                        //   negativeComponent = nonDiscountNegativeMargin - enrollmentConsumption.getAmtPaidPLeft();
                        //}

                        int refundAmount = enrollmentConsumption.getAmtPaidNPLeft();
                        enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - refundAmount);
                        enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - refundAmount);

                        EnrollmentTransaction enrollmentTransaction1 = new EnrollmentTransaction();
                        enrollmentTransaction1.setAmtLeft(enrollmentConsumption.getAmtPaidLeft());
                        enrollmentTransaction1.setAmtLeftNP(enrollmentConsumption.getAmtPaidNPLeft());
                        enrollmentTransaction1.setAmtLeftP(enrollmentConsumption.getAmtPaidPLeft());
                        enrollmentTransaction1.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                        enrollmentTransaction1.setAmtPaid(refundAmount);
                        enrollmentTransaction1.setAmtPaidNP(refundAmount);
                        enrollmentTransaction1.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                        enrollmentTransaction1.setEnrollmentId(enrollmentId);
                        enrollmentTransaction1.setEnrollmentTransactionContextId(enrollmentId);
                        enrollmentTransaction1.setNegativeMarginLeft(enrollmentConsumption.getNegativeMargin());
                        enrollmentTransaction1.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REFUND);
                        enrollmentConsumption.setEnrollmentEnded(true);
                        enrollmentConsumptionManager.takeSnapshotEnrollmentConsumption(enrollmentTransaction1, enrollmentConsumption);

                        enrollmentTransactionDAO.save(enrollmentTransaction1, session, null);
                        enrollmentConsumptionDAO.update(enrollmentConsumption, session);

                        transaction.commit();
                    } catch (Exception ex) {
                        logger.error("Exception: " + ex.getMessage());
                        session.getTransaction().rollback();
                    } finally {
                        session.close();
                    }

                } else if (oTMRefundAdjustmentRes.getLeftOverAmount() != null && oTMRefundAdjustmentRes.getLeftOverAmount() >= 0) {

                    Session session = sqlSessionfactory.getSessionFactory().openSession();
                    Transaction transaction = session.getTransaction();

                    try {

                        transaction.begin();
                        EnrollmentConsumption enrollmentConsumption = enrollmentConsumptionDAO.getByEnrollmentId(enrollmentId, null, session, LockMode.NONE);

                        int refundAmount = enrollmentConsumption.getAmtPaidNPLeft() - oTMRefundAdjustmentRes.getLeftOverAmount();

                        enrollmentConsumption.setAmtPaidLeft(enrollmentConsumption.getAmtPaidLeft() - refundAmount);
                        enrollmentConsumption.setAmtPaidNPLeft(enrollmentConsumption.getAmtPaidNPLeft() - refundAmount);

                        EnrollmentTransaction enrollmentTransaction1 = new EnrollmentTransaction();
                        enrollmentTransaction1.setAmtLeft(enrollmentConsumption.getAmtPaidLeft());
                        enrollmentTransaction1.setAmtLeftNP(enrollmentConsumption.getAmtPaidNPLeft());
                        enrollmentTransaction1.setAmtLeftP(enrollmentConsumption.getAmtPaidPLeft());
                        enrollmentTransaction1.setEnrollmentTransactionType(EnrollmentTransactionType.DEBIT);
                        enrollmentTransaction1.setAmtPaid(refundAmount);
                        enrollmentTransaction1.setAmtPaidNP(refundAmount);
                        enrollmentTransaction1.setEnrollmentConsumptionId(enrollmentConsumption.getId());
                        enrollmentTransaction1.setEnrollmentId(enrollmentId);
                        enrollmentTransaction1.setNegativeMarginLeft(enrollmentConsumption.getNegativeMargin());
                        enrollmentTransaction1.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REFUND);

                        enrollmentConsumption.setEnrollmentEnded(true);
                        enrollmentConsumptionManager.takeSnapshotEnrollmentConsumption(enrollmentTransaction1, enrollmentConsumption);
                        enrollmentTransactionDAO.save(enrollmentTransaction1, session, null);
                        enrollmentConsumptionDAO.update(enrollmentConsumption, session);

                        transaction.commit();

                    } catch (Exception ex) {
                        //logger.error("Exception: "+ex.getMessage());
                        logger.error("Exception while processing leftover amount " + ex.getMessage());
                        session.getTransaction().rollback();
                    } finally {
                        session.close();
                    }

                }

            }

            boolean result = enrollmentConsumptionManager.performEndEnrollmentConsumption(enrollment, EnrollmentTransactionContextType.END_ENROLLMENT_CONSUMPTION);
            ///*boolean end_consumption = enrollmentConsumptionManager.makeEnrollmentPayment(enrollmentConsumptionPayment, null);
            if (result) {
                logger.info("Successfull End Consumption");
            } else {
                logger.error("Error in end consumption: " + enrollmentId);
            }

        }

    }

    public PlatformBasicResponse processRegPaymentOTFCourse(RegPaymentOTFCourseReq regPaymentOTFCourseReq) throws ConflictException, NotFoundException, BadRequestException, ForbiddenException, VException {

        Enrollment enrollment = enrollmentDAO.getById(regPaymentOTFCourseReq.getEnrollmentId());

        OrderInfo orderInfo = paymentManager.getOrderInfo(regPaymentOTFCourseReq.getOrderId());

        Registration registration = registrationDAO.getRegistrationByOrderId(regPaymentOTFCourseReq.getOrderId());

        int amounToPay = registration.getBulkPriceToPay();

        Long duration = null;

        Batch batch = batchDAO.getById(enrollment.getBatchId());
        if (batch.getDuration() - batchManager.getDurationToBeSubtracted(enrollment.getBatchId()) < DateTimeUtils.MILLIS_PER_HOUR) {
            batch.setDuration(DateTimeUtils.MILLIS_PER_HOUR + batchManager.getDurationToBeSubtracted(enrollment.getBatchId()));
            //throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Duration can't be less than 1 hour");
        }

        if (batch.isContentBatch()) {
            batch.setDuration(batch.getEndTime() - (System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_DAY));
            if (batch.getDuration() < DateTimeUtils.MILLIS_PER_HOUR) {
                batch.setDuration((long) DateTimeUtils.MILLIS_PER_HOUR);
            }
        }
        enrollment.setHourlyRate((int) (amounToPay / ((batch.getDuration() - batchManager.getDurationToBeSubtracted(enrollment.getBatchId())) / DateTimeUtils.MILLIS_PER_HOUR)));
        duration = batch.getDuration();

        updateEnrollment(enrollment, null);

        MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
        makePaymentRequest.setAmt(orderInfo.getAmount());
        makePaymentRequest.setAmtP(orderInfo.getPromotionalAmount());
        makePaymentRequest.setAmtNP(orderInfo.getNonpromotionalAmount());
        makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_PAYMENT);
        makePaymentRequest.setOrderId(regPaymentOTFCourseReq.getOrderId());
        makePaymentRequest.setBatchId(regPaymentOTFCourseReq.getBatchId());
        makePaymentRequest.setEnrollmentId(regPaymentOTFCourseReq.getEnrollmentId());
        makePaymentRequest.setCourseId(regPaymentOTFCourseReq.getCourseId());
        makePaymentRequest.setAmtToBePaid(amounToPay);
        makePaymentRequest.setDuration(duration);
        if (ArrayUtils.isNotEmpty(orderInfo.getOrderedItems())) {
            makePaymentRequest.setAmtFromDiscount((int) (orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount()));
        } else {
            makePaymentRequest.setAmtFromDiscount(0);
        }

        return new PlatformBasicResponse(enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, 1f), null, null);
        //return new PlatformBasicResponse(true, null, null);
    }

    public HashMap<String, List<String>> getUserIdEnrollmentBatchMap(ArrayList<String> userIds, ArrayList<String> batchIds) {
        HashMap<String, List<String>> response = new HashMap<>();
        if (ArrayUtils.isEmpty(userIds) || ArrayUtils.isEmpty(batchIds)) {
            return response;
        }
        List<Enrollment> enrollments = enrollmentDAO.getUserIdEnrollmentBatchMap(userIds, batchIds);
        if (ArrayUtils.isEmpty(enrollments)) {
            return response;
        }
        for (Enrollment enrollment : enrollments) {
            if (!response.containsKey(enrollment.getUserId())) {
                response.put(enrollment.getUserId(), new ArrayList<>());
            }
            List<String> list = response.get(enrollment.getUserId());
            if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())) {
                List<Enrollment> bundleEnrollments = enrollmentDAO.getEnrollmentForEntityId(enrollment.getUserId(), enrollment.getEntityId());
                for (Enrollment entity : bundleEnrollments) {
                    list.add(entity.getBatchId());
                }
            } else {
                list.add(enrollment.getBatchId());
            }
        }
        return response;
    }

    public Map<String, List<SubjectFileCountPojo>> getBatchShareContentMap(List<String> batchIds) throws VException {
        String queryStringLms = LMS_ENDPOINT + "curriculum/getCurriculumFilesForBatch?batchIds=" + String.join(",", batchIds);

        ClientResponse resp = WebUtils.INSTANCE.doCall(queryStringLms, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<Map<String, List<SubjectFileCountPojo>>>() {
        }.getType();
        Map<String, List<SubjectFileCountPojo>> result = gson.fromJson(jsonString, listType);
        return result;
    }

    public PlatformBasicResponse checkEnrollmentForUser(CurriculumEntityName entityName, List<String> entityIds, String userId) {

        if (CurriculumEntityName.OTF_COURSE.equals(entityName)) {
            List<Enrollment> enrollments = enrollmentDAO.getEnrollmentByCoursesAndUser(entityIds, userId, Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
            if (ArrayUtils.isEmpty(enrollments)) {
                return new PlatformBasicResponse(false, "", "");
            }
        } else if (CurriculumEntityName.OTF_BATCH.equals(entityName)) {
            List<Enrollment> enrollments = enrollmentDAO.getEnrollmentByBatchAndUser(entityIds, userId, Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
            if (ArrayUtils.isEmpty(enrollments)) {
                return new PlatformBasicResponse(false, "", "");
            }
        }

        return new PlatformBasicResponse(true, "", "");

    }

    public List<EnrollmentPojo> getRefundAndConsumption(List<EnrollmentPojo> enrollmentPojos) {
        List<String> enrollmentIds = new ArrayList<>();

        if (enrollmentPojos == null || enrollmentPojos.isEmpty()) {
            return enrollmentPojos;
        }
        for (EnrollmentPojo enrollmentPojo : enrollmentPojos) {
            enrollmentIds.add(enrollmentPojo.getEnrollmentId());
        }
        Session session = sqlSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();

        try {
            transaction.begin();
            String hqlQuery = "select ec.enrollmentId,et.enrollmentTransactionContextType ,ec.amtPaid  ,et.amtPaid,ec.regAmtPaid,et.regAmtPaid from  EnrollmentConsumption ec  inner join EnrollmentTransaction et  on et.enrollmentConsumptionId = ec.id   and et.enrollmentTransactionContextType LIKE '%REFUND' where ec.enrollmentId in (:enrollmentId)";
            List<Object[]> list = session.createQuery(hqlQuery).setParameterList("enrollmentId", enrollmentIds).list();
            HashMap<String, Object[]> refundData = new HashMap<>();
            for (Object[] arr : list) {
                if (arr[0] != null && arr[0].toString() != null) {
                    refundData.put(arr[0].toString(), arr);
                }
            }
            for (EnrollmentPojo enrollmentPojo : enrollmentPojos) {
                if (refundData.containsKey(enrollmentPojo.getEnrollmentId())) {
                    Object[] refund = refundData.get(enrollmentPojo.getEnrollmentId());
                    if (EnrollmentTransactionContextType.REFUND.equals(refund[1])) {

                        enrollmentPojo.setPaidAmt(Integer.parseInt(refund[2].toString()));
                        enrollmentPojo.setRefundAmt(Integer.parseInt(refund[3].toString()));
                        enrollmentPojo.setConsumptionAmt(enrollmentPojo.getPaidAmt() - enrollmentPojo.getRefundAmt());
                    } else if (EnrollmentTransactionContextType.REGISTRATION_REFUND.equals(refund[1])) {

                        enrollmentPojo.setPaidAmt(Integer.parseInt(refund[4].toString()));
                        enrollmentPojo.setRefundAmt(Integer.parseInt(refund[5].toString()));
                        enrollmentPojo.setConsumptionAmt(enrollmentPojo.getPaidAmt() - enrollmentPojo.getRefundAmt());

                    }
                }
            }
        } catch (Exception e) {
            logger.error("Error in getting refund data" + e.getMessage());
        } finally {
            session.close();
        }

        return enrollmentPojos;
    }

    public List<OTFEntityEnrollmentResp> getPostLoginEnrollments(GetEnrollmentsReq req) throws VException {
        Long userId = req.getUserId();
        UserBasicInfo userInfo = fosUtils.getUserBasicInfo(userId, false);
        req.setRole(userInfo.getRole());
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null.");
        }
        Map<String, String> alreadyEnrolledEntity = new HashMap<>();
        Map<String, Integer> entityGrade = new HashMap<>();
        Map<String, List<EnrollmentPojo>> enrollMap = new HashMap<>();
        List<OTFEntityEnrollmentResp> response = new ArrayList<>();
        Map<String, String> bundleEnity = new HashMap<>();
        Map<String, OTFEntityEnrollmentResp> entityResMap = new HashMap<>();

        List<Enrollment> enrollments = enrollmentDAO.getNonEndedEnrolledBatchesForStudent(Long.toString(userId),req.getStart(),req.getSize());
        GetRegistrationsForMultipleEntitiesReq regReq = new GetRegistrationsForMultipleEntitiesReq();
        regReq.setUserId(userId);
        if (ArrayUtils.isNotEmpty(enrollments)) {

            Map<String, List<SubjectFileCountPojo>> batchContentMap = new HashMap<>();
            List<String> batchIds = new ArrayList<>();

            for (Enrollment enrollment : enrollments) {
                batchIds.add(enrollment.getBatchId());
                if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollment.getEntityType()) && EnrollmentPurchaseContext.BUNDLE.equals(enrollment.getPurchaseContextType())) {
                    alreadyEnrolledEntity.put(enrollment.getEntityId(), enrollment.getPurchaseContextId());
                }
                if (com.vedantu.session.pojo.EntityType.OTF_COURSE.equals(enrollment.getEntityType()) && EnrollmentPurchaseContext.BUNDLE.equals(enrollment.getPurchaseContextType())) {
                    alreadyEnrolledEntity.put(enrollment.getBatchId(), enrollment.getPurchaseContextId());
                }
            }

            if (req.isContentCountRequired()) {
                try {
                    batchContentMap = getBatchShareContentMap(batchIds);
                } catch (Exception ex) {
                    logger.error("Error in fetching content count for batch with exception", ex);
                }
            }
            List<EnrollmentPojo> enrollmentPojos = createEnrollmentInfos(enrollments, true, false);

            for (EnrollmentPojo enrollment : enrollmentPojos) {
                String key = enrollment.getEntityId();

                if (batchContentMap.containsKey(enrollment.getBatchId())) {
                    enrollment.setSubjectFileCountPojos(batchContentMap.get(enrollment.getBatchId()));
                } else {
                    enrollment.setSubjectFileCountPojos(new ArrayList<>());
                }

                if (StringUtils.isEmpty(key)) {
                    key = enrollment.getBatchId();
                }
                if (enrollMap.containsKey(key)) {
                    List<EnrollmentPojo> pojoList = enrollMap.get(key);
                    pojoList.add(enrollment);
                } else {
                    List<EnrollmentPojo> pojoList = new ArrayList<>();
                    pojoList.add(enrollment);
                    enrollMap.put(key, pojoList);
                }
            }

            for (String mapKey : enrollMap.keySet()) {
                OTFEntityEnrollmentResp res = new OTFEntityEnrollmentResp();
                List<EnrollmentPojo> pojos = enrollMap.get(mapKey);
                if (ArrayUtils.isNotEmpty(pojos)) {
                    res.setEntityType(pojos.get(0).getEntityType());
                    res.setEntityId(pojos.get(0).getEntityId());
                    res.setEntityTitle(pojos.get(0).getEntityTitle());
                }
                res.setEnrollments(pojos);
                response.add(res);
            }
        }

        List<BundleEnrollmentResp> bundles = bundleManager.getNonSubscriptionBundlesPackages(userId.toString());
        List<String> bundleIds = new ArrayList<>();
        List<String> batchIds = new ArrayList<>();
        for (BundleEnrollmentResp bundle : bundles) {
            List<String> includeFields = Arrays.asList(
                    BundleEntity.Constants.ENTITY_ID,
                    BundleEntity.Constants.ENROLLMENT_TYPE,
                    BundleEntity.Constants.PACKAGE_TYPE,
                    BundleEntity.Constants.GRADE
            );
            List<BundleEntity> bundleEntities=bundleEntityDAO.getBundleEntitiesByBundleId(bundle.getId(),0,100, includeFields);
            for (BundleEntity bundleEntity : bundleEntities) {
                if (!StringUtils.isEmpty(bundleEntity.getEntityId())) {
                    if (!alreadyEnrolledEntity.containsKey(bundleEntity.getEntityId())) {
                        OTFEntityEnrollmentResp res = new OTFEntityEnrollmentResp();
                        if (EnrollmentType.LOCKED.equals(bundleEntity.getEnrollmentType())) {
                            res.setIsLocked(true);
                        }
                        if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.toString().equals(bundleEntity.getPackageType().toString())) {
                            res.setEntityType(com.vedantu.session.pojo.EntityType.OTM_BUNDLE);
                            bundleIds.add(bundleEntity.getEntityId());
                        }
                        if (com.vedantu.session.pojo.EntityType.OTF.toString().equals(bundleEntity.getPackageType().toString())) {
                            res.setEntityType(com.vedantu.session.pojo.EntityType.OTF);
                            batchIds.add(bundleEntity.getEntityId());
                        }
                        res.setGrade(bundleEntity.getGrade());
                        res.setEntityId(bundleEntity.getEntityId());
                        entityResMap.put(bundleEntity.getEntityId(), res);
                        bundleEnity.put(bundleEntity.getEntityId(), bundle.getId());
                        res.setContextType(bundleEntity.getEnrollmentType());
                    }
                }
                entityGrade.put(bundleEntity.getEntityId(), bundleEntity.getGrade());
            }
        }
        GetOTFBundlesReq bundlesReq = new GetOTFBundlesReq();

        bundlesReq.setOtfBundleIds(bundleIds);
        List<OTMBundle> oTFBundleInfos = otfBundleDAO.getOTMBundlesByIds(bundleIds);
        Map<String, OTMBundle> bundleMap = new HashMap<>();
        for (OTMBundle oTFBundleInfo : oTFBundleInfos) {
            if (oTFBundleInfo.getEntities() != null) {
                OTFEntityEnrollmentResp res = entityResMap.get(oTFBundleInfo.getId());
                if (res != null) {
                    res.setEntityTitle(oTFBundleInfo.getTitle());
                }
                for (OTMBundleEntityInfo entityInfo : oTFBundleInfo.getEntities()) {
                    if (com.vedantu.session.pojo.EntityType.OTF.equals(entityInfo.getEntityType())
                            && !org.springframework.util.StringUtils.isEmpty(entityInfo.getEntityId())) {

                        batchIds.add(entityInfo.getEntityId());
                    }
                }
                bundleMap.put(oTFBundleInfo.getId(), oTFBundleInfo);
            }
        }

        Map<String, BatchBasicInfo> batchInfoMap = getBatchMap(batchIds, false);
        List<String> courseIds = new ArrayList<String>();
        for (String batchId : batchInfoMap.keySet()) {
            courseIds.add(batchInfoMap.get(batchId).getCourseId());
        }
        Map<String, CourseBasicInfo> courseInfoMap = getCourseMap(courseIds, false);
        for (String batchId : batchInfoMap.keySet()) {
            if (entityResMap.containsKey(batchId)) {
                EnrollmentPojo enrollmentPojo = new EnrollmentPojo();
                CourseBasicInfo course = courseInfoMap.get(batchInfoMap.get(batchId).getCourseId());
                enrollmentPojo.setBatch(batchInfoMap.get(batchId));
                enrollmentPojo.setCourse(course);
                enrollmentPojo.setPurchaseContextId(bundleEnity.get(batchId));
                OTFEntityEnrollmentResp res = entityResMap.get(batchId);
                res.setEntityTitle(course.getTitle());
                res.setEnrollments(Arrays.asList(enrollmentPojo));
            }
        }
        for (String bundleId : bundleMap.keySet()) {
            OTMBundle oTFBundleInfo = bundleMap.get(bundleId);
            OTFEntityEnrollmentResp res = entityResMap.get(bundleId);
            if (res != null) {
                res.setEntityTitle(oTFBundleInfo.getTitle());
            }
            List<EnrollmentPojo> enrollment = new ArrayList<EnrollmentPojo>();

            for (OTMBundleEntityInfo entityInfo : oTFBundleInfo.getEntities()) {
                if (com.vedantu.session.pojo.EntityType.OTF.equals(entityInfo.getEntityType())
                        && !org.springframework.util.StringUtils.isEmpty(entityInfo.getEntityId())) {
                    if (batchInfoMap.containsKey(entityInfo.getEntityId())) {
                        EnrollmentPojo enrollmentPojo = new EnrollmentPojo();
                        enrollmentPojo.setBatch(batchInfoMap.get(entityInfo.getEntityId()));
                        enrollmentPojo.setCourse(courseInfoMap.get(batchInfoMap.get(entityInfo.getEntityId()).getCourseId()));
                        enrollmentPojo.setPurchaseContextId(bundleEnity.get(bundleId));
                        enrollmentPojo.setPurchaseContextType(EnrollmentPurchaseContext.BUNDLE);
                        enrollment.add(enrollmentPojo);
                    }
                }
            }
            if (res != null) {
                res.setEnrollments(enrollment);
            }
        }
        for (OTFEntityEnrollmentResp enrollment : response) {
            if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())) {
                if (entityGrade.containsKey(enrollment.getEntityId())) {
                    enrollment.setGrade(entityGrade.get(enrollment.getEntityId()));
                }
            }
            if (com.vedantu.session.pojo.EntityType.OTF_COURSE.equals(enrollment.getEntityType())) {
                if (entityGrade.containsKey(enrollment.getEnrollments().get(0).getBatch().getId())) {
                    enrollment.setGrade(entityGrade.get(enrollment.getEnrollments().get(0).getBatch().getId()));
                }
            }
        }
        response.addAll(entityResMap.values());
        return response;
    }

    public List<EnrollmentPojo> getActiveAndInactiveEnrollmentsForUserId(String userId) {
        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsForUserId(userId);
        if (ArrayUtils.isEmpty(enrollments)) {
            return new ArrayList<>();
        }
        List<String> ids = new ArrayList<>();
        enrollments.forEach(enrollment -> ids.add(enrollment.getCourseId()));
        List<Course> courses = courseDAO.getCoursesBasicInfos(ids);
        Map<String, Course> courseIdMap = new HashMap<>();
        courses.forEach(course -> courseIdMap.put(course.getId(), course));
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        for (Enrollment enrollment : enrollments) {
            enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, courseIdMap.get(enrollment.getCourseId()), null, null));
        }
        logger.info("getActiveAndInactiveEnrollmentsForUserId response from subscription is " + gson.toJson(enrollmentInfos));
        return enrollmentInfos;
    }

    public List<EnrollmentPojo> getActiveAndInactiveEnrollments(GetEnrollmentsReq getEnrollmentsReq) {

        List<Enrollment> enrollments = enrollmentDAO.getEnrollments(getEnrollmentsReq);
        if (ArrayUtils.isEmpty(enrollments)) {
            return new ArrayList<>();
        }
        List<String> ids = new ArrayList<>();
        enrollments.forEach(enrollment -> ids.add(enrollment.getCourseId()));
        List<Course> courses = courseDAO.getCoursesBasicInfos(ids);
        Map<String, Course> courseIdMap = new HashMap<>();
        courses.forEach(course -> courseIdMap.put(course.getId(), course));
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        for (Enrollment enrollment : enrollments) {
            enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, courseIdMap.get(enrollment.getCourseId()), null, null));
        }
        logger.info("getActiveAndInactiveEnrollmentsForUserId response from subscription is " + gson.toJson(enrollmentInfos));
        return enrollmentInfos;
    }

    public List<String> getBatchIdsOfActiveEnrollmentsOfUser(GetUserEnrollmentsReq req) {
        List<String> batchIds = new ArrayList<>();
        List<Enrollment> enrollments = enrollmentDAO.getBatchIdsOfActiveEnrollmentsOfUser(req);
        if (ArrayUtils.isEmpty(enrollments)) {
            return null;
        }
        for (Enrollment enrollment : enrollments) {
            batchIds.add(enrollment.getBatchId());
        }
        return batchIds;
    }

    public List<ActiveUserIdsByBatchIdsResp> getActiveUserIdsByBatchIds(ActiveUserIdsByBatchIdsReq req) throws BadRequestException {
        List<ActiveUserIdsByBatchIdsResp> enrollmentInfos = new ArrayList<>();
        List<Enrollment> enrollments = enrollmentDAO.getActiveUserIdsByBatchIds(req);
        if (ArrayUtils.isEmpty(enrollments)) {
            return enrollmentInfos;
        }
        for (Enrollment enrollment : enrollments) {
            if(Objects.isNull(enrollment.getSectionId())) {
                enrollmentInfos.add(new ActiveUserIdsByBatchIdsResp(enrollment.getBatchId(), enrollment.getUserId(), enrollment.getId(), enrollment.getBatchChangeTime()));
            }
            else {
                enrollmentInfos.add(new ActiveUserIdsByBatchIdsResp(enrollment.getBatchId(), enrollment.getUserId(), enrollment.getId(), enrollment.getBatchChangeTime(), enrollment.getSectionId()));
            }
        }
        return enrollmentInfos;
    }

    public ActiveStudentsInBatchesResp getActiveStudentsInBatches(List<String> batchIds, String prevLastFetchedId, int limit) throws BadRequestException {
        List<ActiveUserIdsByBatchIdsResp> enrollmentInfos = new ArrayList<>();

        if (limit > AbstractMongoDAO.MAX_ALLOWED_FETCH_SIZE) {
            throw new BadRequestException(ErrorCode.IMPERMISSIBLE_SIZE_PER_REQUEST, "Impermissible request size to fetch enrollments");
        }
        List<Enrollment> fetchedEnrollments = enrollmentDAO.getActiveStudentsInBatches(batchIds, prevLastFetchedId, limit);
        String currLastFetchedId = "";
        for (Enrollment e : fetchedEnrollments) {

            if(Objects.isNull(e.getSectionState()))
                enrollmentInfos.add(new ActiveUserIdsByBatchIdsResp(e.getBatchId(), e.getUserId(), e.getId(), e.getBatchChangeTime()));
            else
                enrollmentInfos.add(new ActiveUserIdsByBatchIdsResp(e.getBatchId(), e.getUserId(), e.getId(), e.getBatchChangeTime(),e.getSectionId()));

            if (e.getId().compareTo(currLastFetchedId) > 0) {
                currLastFetchedId = e.getId();
            }
        }
        // currLastFetchedId = fetchedEnrollments.get(fetchedEnrollments.size() - 1).getId();

        return ActiveStudentsInBatchesResp.builder().enrollments(enrollmentInfos).prevLastFetchedId(currLastFetchedId).build();
    }

    public List<ActiveTAsForBatchRes> getTAsInBatches(List<String> batchIds) throws BadRequestException {
        List<ActiveTAsForBatchRes> response = new ArrayList<>();

        if (batchIds.isEmpty())
            return null;

        // get sections data from batchIds
        List<Section> sections = new ArrayList<>();
        List<Section> temp = new ArrayList<>();
        boolean processing = true;
        int skip = 0;
        int limit = 250;
        while(processing){
            temp = sectionDAO.getSectionsForBatchIds(batchIds, skip, limit);
            if(temp.size() == 0)
              processing = false;
            else {
                skip += temp.size();
                sections.addAll(temp);
            }
        }

        logger.info("Sections {}", sections);

        List<String> taIds = new ArrayList<>();
        sections.forEach(s-> {
            taIds.addAll(s.getTeacherIds());
        });

        logger.info("taIds {}",taIds);
        Map<String, UserBasicInfo> userBasicInfosMap = fosUtils.getUserBasicInfosMap(taIds, false);
        logger.info("userBasicInfosMap {}",userBasicInfosMap);

        for (Section s : sections) {
            for (String tId : s.getTeacherIds()) {
                ActiveTAsForBatchRes res = new ActiveTAsForBatchRes();
                res.setBatchId(s.getBatchId());
                res.setSectionId(s.getId());
                res.setUserId(tId);
                res.setSectionName(s.getSectionName());
                res.setSectionType(s.getSectionType());
                if(userBasicInfosMap.containsKey(tId))
                    res.setTaName(userBasicInfosMap.get(tId).getFirstName());

                response.add(res);
            }
        }
        return  response;
    }

    public ActiveTAsForBatchRes getTAsInfoForBatchIds(List<String> batchIds,String userId){
        if(batchIds.isEmpty() || userId.isEmpty())
            return null;

        Section section = sectionDAO.getSectionsForBatchIdsUserId(batchIds,userId);
        if(section != null){
            for(String tId:section.getTeacherIds()){
                if(tId.equals(userId)){
                    ActiveTAsForBatchRes res = new ActiveTAsForBatchRes();
                    res.setBatchId(section.getBatchId());
                    res.setSectionId(section.getId());
                    res.setUserId(tId);
                    return res;
                }
            }
        }
        return null;
    }

    public List<String> getAllBatchIdsOfUser(String userId) throws BadRequestException {
        List<String> batchIds = new ArrayList<>();


        List<Enrollment> enrollments = enrollmentDAO.getAllBatchIdsOfUser(userId);

        enrollments.forEach(enrollment -> {

            batchIds.add(enrollment.getBatchId());
            enrollment.getBatchChangeTime().forEach(batchChangeTime -> {
                batchIds.add(batchChangeTime.getPreviousBatchId());
            });

        });

        return batchIds;
    }

    public List<String> getEnrolledStudentsInBatch(String batchId, int skip, int limit) throws BadRequestException {

        final int end = skip + limit;
        int maxLimit = 1000;
        int fetchLimit = Math.min(maxLimit, limit);
        List<String> enrolledStudents = new ArrayList<>();
        if (StringUtils.isEmpty(batchId)) {
            return enrolledStudents;
        }

        if (limit > maxLimit) {
            logger.error("Processing a request to fetch enrollments of limit > 1000");
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Impermissible fetch limit for enrollments > " + fetchLimit);
        }

        while (skip < end) {
            List<Enrollment> activeEnrollments = enrollmentDAO
                    .getEnrolledStudentsInBatch(batchId,
                            Collections.singletonList(Enrollment.Constants.USER_ID), skip, fetchLimit);
            if (CollectionUtils.isEmpty(activeEnrollments)) {
                break;
            } else {
                enrolledStudents.addAll(Optional.of(activeEnrollments).orElseGet(ArrayList::new)
                        .stream()
                        .map(Enrollment::getUserId)
                        .collect(Collectors.toList()));
            }
            skip += fetchLimit;
        }

        return enrolledStudents;
    }

    public List<EnrollmentHomePageResponse> getEnrollmentListForHomePage(EnrollmentHomePageRequest request) throws BadRequestException {
        logger.info("getEnrollmentListForHomePage - request - {}",request);
        List<Enrollment> enrollments=enrollmentDAO.getUserActiveAndInactiveEnrollment(request.getUserId(),request.getStart(),
                request.getSize(),Arrays.asList(Enrollment.Constants.COURSE_ID,Enrollment.Constants.BATCH_ID,Enrollment.Constants.ENTITY_ID));
        if(ArrayUtils.isEmpty(enrollments)){
            return new ArrayList<>();
        }
        List<String> batchIds=enrollments.stream().map(Enrollment::getBatchId).collect(Collectors.toList());
        List<String> courseIds=enrollments.stream().map(Enrollment::getCourseId).collect(Collectors.toList());
        Map<String, BatchBasicInfo> batchMap = getBatchMap(batchIds, false);
        Map<String, CourseBasicInfo> courseMap = getCourseMap(courseIds, false);
        return enrollments
                .stream()
                .map(enrollment->new EnrollmentHomePageResponse(enrollment.getEntityId(),batchMap.get(enrollment.getBatchId()),courseMap.get(enrollment.getCourseId())))
                .collect(Collectors.toList());
    }

    public List<String> getAllSubjectsForActiveEnrollments(String userId) {
        Set<String> subjects = new HashSet<>();
        if (StringUtils.isEmpty(userId)) {
            return new ArrayList<>();
        }
        List<Enrollment> activeEnrollments = new ArrayList<>();
        List<Enrollment> temp;
        boolean processing = true;
        int skip = 0;
        int limit = 250;
        while(processing){
            temp = enrollmentDAO.getEnrollmentsForUserId(userId, skip, limit);
            if(temp.size() == 0)
                processing = false;
            else {
                skip += temp.size();
                activeEnrollments.addAll(temp);
            }
        }
        if (ArrayUtils.isEmpty(activeEnrollments)) {
            return new ArrayList<>();
        }
        List<String> courseIds = activeEnrollments.stream().map(Enrollment::getCourseId).peek(logger::info).collect(Collectors.toList());
        Map<String, CourseBasicInfo> courseMap = getCourseMap(courseIds, false);
        for (String courseId : courseIds) {
            List<CourseTerm> searchTerms = courseMap.get(courseId).getSearchTerms();
            if (ArrayUtils.isNotEmpty(searchTerms) && (searchTerms.contains(CourseTerm.LONG_TERM) || searchTerms.contains(CourseTerm.SHORT_TERM))) {
                Set<String> convertedSubjects = Optional.ofNullable(courseMap.get(courseId).getNormalizeSubjects())
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(this::convertSubjectNamesForLearningStats)
                        .collect(Collectors.toSet());
                subjects.addAll(convertedSubjects);
            }
        }
        List<String> orderedSubjects = new ArrayList<>();
        if(subjects.contains(MATHS)) {
            orderedSubjects.add(MATHS);
            subjects.remove(MATHS);
        }
        if(subjects.contains("Physics")) {
            orderedSubjects.add("Physics");
            subjects.remove("Physics");
        }
        if(subjects.contains("Chemistry")) {
            orderedSubjects.add("Chemistry");
            subjects.remove("Chemistry");
        }
        orderedSubjects.addAll(subjects);
        return orderedSubjects;
    }

    private String convertSubjectNamesForLearningStats(String subject) {
        if(MATHEMATICS.equals(subject)) {
            return MATHS;
        } else if (SOCIAL_STUDIES.equals(subject)) {
            return SOCIAL_SCIENCE;
        }
        return subject;
    }

    public List<EnrolledCoursesHomepage> getEnrolledCoursesForHomepage(EnrollmentHomePageRequest req) throws BadRequestException {
        List<Enrollment> fetchedEnrollments = enrollmentDAO.getUserActiveAndInactiveEnrollment(req.getUserId(), 0, MAX_ENROLLMENTS_FETCH_SIZE,
                Arrays.asList(Enrollment.Constants.COURSE_ID, Enrollment.Constants.STATUS, Enrollment.Constants.BATCH_ID,Enrollment.Constants.ENTITY_ID));
        if (CollectionUtils.isEmpty(fetchedEnrollments)){
            return new ArrayList<>();
        }

        List<String> courseIds = fetchedEnrollments.stream().map(Enrollment::getCourseId).collect(Collectors.toList());
        List<String> batchIds= fetchedEnrollments.stream().map(Enrollment::getBatchId).collect(Collectors.toList());
        logger.info("deblog-- enrolled courseids fetched {}", courseIds);
        Map<String, CourseBasicInfo> courseMap = getCourseMap(courseIds, false);
        Map<String, BatchBasicInfo> batchMap = getBatchMap(batchIds, false);
        batchMap.values().forEach(a -> {});
        List<EnrolledCoursesHomepage> enrolledCourses = EnrolledCoursesHomepage.generateFromEnrollments(fetchedEnrollments, courseMap, batchMap);

        return Optional.of(enrolledCourses).orElseGet(ArrayList::new).stream()
                .sorted(Comparator.comparing(EnrolledCoursesHomepage::getStatus)
                        .thenComparing(EnrolledCoursesHomepage::getMainSearchTerm))
                .limit(MAX_ENROLLED_COURSES_HOME_PAGE).collect(Collectors.toList());

    }


    public Map<String, Course> getCourseTitlesForEnrollments(List<String> enrollmentIds) throws BadRequestException {

        if (CollectionUtils.isEmpty(enrollmentIds) && enrollmentIds.size() > 250) {
            throw new BadRequestException(ErrorCode.IMPERMISSIBLE_SIZE_PER_REQUEST, "Impermissible request size to fetch enrollments");
        }

        List<String> includeFields = Arrays.asList(Enrollment.Constants._ID, Enrollment.Constants.COURSE_ID);
        List<Enrollment> enrollments = enrollmentDAO.getByIdsWithProjection(enrollmentIds, includeFields, 0, enrollmentIds.size());

        Map<String, String> courseIdEnrolIdMap = Optional.of(enrollments).orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(Enrollment::getCourseId, Enrollment::getId));

        List<String> courseBasicInfoFields = Arrays.asList(Course.Constants._ID, Course.Constants.TITLE);
        List<Course> courseInfos = courseDAO.getCoursesBasicInfos(new ArrayList<>(courseIdEnrolIdMap.keySet()), courseBasicInfoFields);

        return Optional.of(courseInfos)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toMap(c -> courseIdEnrolIdMap.get(c.getId()), c -> c));
    }

    public String getBatchSectionRedisData(String batchId,EnrollmentState sectionState) throws BadRequestException {
        String rKey = createBatchSectionRedisKey(batchId,sectionState);
        String rValue="";

        // Redis call
        try {
                rValue = redisDAO.get(rKey);
        } catch (Exception e) {
            logger.error("Error in redis " + e.getMessage());
        }
        logger.info("Redis values :{}",rValue);
        if(Objects.isNull(rValue))
            return "";
        return rValue;
    }

    public String createBatchSectionRedisKey(String batchId, EnrollmentState enrollmentState){
        if(enrollmentState.equals(EnrollmentState.TRIAL))
            return batchTrialSectionRedisSlug + batchId;
        return batchRegularSectionRedisSlug + batchId;
    }

    public void syncEnrollmentWithLS(List<String> ids){
        List<Enrollment> enrollments = enrollmentDAO.getByIdsWithoutProjection(ids, 0, 10);
        enrollmentDAO.sendToSqsList(enrollments);
    }

    public List<UserBasicInfo> getTeachersForStudent(String userId) {
        Set<String> teacherIds = new HashSet<>();
        List<UserBasicInfo> resp = new ArrayList<>();
        if (StringUtils.isEmpty(userId)) {
            return resp;
        }

        final String key = "TEACHERS_DAY_WISHES_2020_" + userId;

        try {
            Type setType = new TypeToken<List<UserBasicInfo>>() {
            }.getType();
            resp = new Gson().fromJson(redisDAO.get(key), setType);

            logger.info("conflictSessionGttIds {}", resp);
            if (CollectionUtils.isNotEmpty(resp)) {
                return resp;
            }
        } catch (BadRequestException | InternalServerErrorException e) {
            logger.error("Error while fetching teacherIds for user");
        }


        List<CoursePlan> fetchedCoursePlans = coursePlanDAO.getUserCoursePlans(Long.valueOf(userId),
                Collections.singletonList(CoursePlan.Constants.TEACHER_ID));
        Optional.ofNullable(fetchedCoursePlans).orElseGet(ArrayList::new).forEach(cp -> {
            if (cp.getTeacherId() != null) {
                teacherIds.add(String.valueOf(cp.getTeacherId()));
            }
        });

        List<Enrollment> fetchedEnrollments = enrollmentDAO.getAllUserEnrollments(userId, MAX_ENROLLMENTS_FETCH_SIZE,
                Collections.singletonList(Enrollment.Constants.BATCH_ID));
        List<String> batchIds = Optional.ofNullable(fetchedEnrollments).orElseGet(ArrayList::new).stream()
                .filter(Objects::nonNull).map(Enrollment::getBatchId).distinct().collect(Collectors.toList());
        List<Batch> fetchedBatches = batchDAO.getBatches(batchIds, Collections.singletonList(Batch.Constants.TEACHER_IDS));
        Optional.ofNullable(fetchedBatches)
                .orElseGet(ArrayList::new)
                .stream()
                .filter(b -> Objects.nonNull(b) && CollectionUtils.isNotEmpty(b.getTeacherIds()))
                .map(Batch::getTeacherIds)
                .forEach(teacherIds::addAll);

        resp = fosUtils.getUserBasicInfosSet(teacherIds.stream().limit(MAX_ENROLLMENTS_FETCH_SIZE).collect(Collectors.toSet()),
                false);
        if (CollectionUtils.isEmpty(resp)) {
            return new ArrayList<>();
        }
        resp.forEach(u -> {
            if (Objects.nonNull(u)) {
                u.setContactNumber(null);
            }
        });
        if (CollectionUtils.isNotEmpty(resp)) {
            try {
                redisDAO.setex(key, gson.toJson(resp), 12 * 3600);
            } catch (InternalServerErrorException e) {
                logger.error("Error while testing " + userId);
            }
        }
        return resp;
    }

    public List<BatchBundlePojo> getAppEnrollmentsOfUser(UserPremiumSubscriptionEnrollmentReq request) {

    	List<BatchBundlePojo> batchBundlePojos = new ArrayList<BatchBundlePojo>();
    	List<Enrollment> enrollments = new ArrayList<Enrollment>();
    	Map<String, EnrollmentState> batchEnrollmentStateMap = new HashMap<String, EnrollmentState>();
    	Map<String, EntityStatus> batchEnrollmentStatusMap = new HashMap<String, EntityStatus>();
    	PremiumSubscriptionRequest premiumReq = new PremiumSubscriptionRequest();
    	if (StringUtils.isEmpty(request.getUserId())
    			|| (!Role.STUDENT.equals(request.getUserRole()))) {
    		return batchBundlePojos;

    	}

    	if(StringUtils.isNotEmpty(request.getGrade()))
    		premiumReq.setGrade(request.getGrade());
    	if(StringUtils.isNotEmpty(request.getBoard()))
    		premiumReq.setBoard(request.getBoard());
    	if(StringUtils.isNotEmpty(request.getStream()))
    		premiumReq.setStream(request.getStream());
    	if(StringUtils.isNotEmpty(request.getTarget()))
    		premiumReq.setTarget(request.getTarget());
    	batchBundlePojos = premiumSubscriptionManager.getPremiumBundlesByFilter(premiumReq);
    	logger.debug("getAppEnrollmentsOfUser  batchBundlePojos from getPremiumBundlesByFilter -> "+batchBundlePojos);
    	if(ArrayUtils.isNotEmpty(batchBundlePojos)) {
    		request.setBundleIds(batchBundlePojos.stream().map(BatchBundlePojo::getBundleId).collect(Collectors.toList()));
    		enrollments = enrollmentDAO.getAppEnrollmentsOfUser(request);
    	}
    	logger.debug("getAppEnrollmentsOfUser -> "+enrollments);
    	if(ArrayUtils.isEmpty(enrollments)){
    		return new ArrayList<BatchBundlePojo>();
    	}else {

    		for(BatchBundlePojo b : batchBundlePojos) {
    			logger.debug("Object b -> "+b);
    			List<String> batchIds = new ArrayList<String>();
    			for(Enrollment e: enrollments) {
    				if(b.getBundleId().equalsIgnoreCase(e.getPurchaseContextId())) {
    					batchIds.add(e.getBatchId());
    				}
    				batchEnrollmentStateMap.put(e.getBatchId(), e.getState());
    				batchEnrollmentStatusMap.put(e.getBatchId(), e.getStatus());
    				logger.debug("batchEnrollmentStatusMap -> "+batchEnrollmentStatusMap);
    				logger.debug("batchEnrollmentStateMap -> "+batchEnrollmentStateMap);
    			}
    			b.setBatchIds(batchIds);
    			b.setBatchEnrollmentStateMap(batchEnrollmentStateMap);
    			b.setBatchEnrollmentStatusMap(batchEnrollmentStatusMap);
    		}
    	}
    	logger.debug("after for batchBundlePojos -> "+batchBundlePojos);
    	return batchBundlePojos;
    }

}
