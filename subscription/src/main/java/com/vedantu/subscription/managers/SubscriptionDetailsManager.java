package com.vedantu.subscription.managers;

import java.util.*;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.dao.*;
import com.vedantu.subscription.entities.mongo.*;
import com.vedantu.subscription.pojo.OrderedItemNames;
import com.vedantu.subscription.viewobject.request.OrderedItemNameDetailsReq;
import com.vedantu.subscription.viewobject.response.OrderedItemNameDetailsRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.subscription.entities.sql.SubscriptionDetails;
import com.vedantu.util.LogFactory;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

@Service
public class SubscriptionDetailsManager {

	@Autowired
	public SubscriptionDetailsDAO subscriptionDetailsDAO;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private SqlSessionFactory sqlSessionfactory;

	@Autowired
	private BundleDAO bundleDAO;

	@Autowired
	private CoursePlanDAO coursePlanDAO;

	@Autowired
	private CourseDAO courseDAO;

	@Autowired
	private OTFBundleDAO otfBundleDAO;

	@Autowired
	private BatchDAO batchDAO;

	@Autowired
	private BundlePackageDAO bundlePackageDAO;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionDetailsManager.class);

	public SubscriptionDetails addOrUpdateSubscriptionDetails(SubscriptionDetails subscriptionDetails, Boolean enabled,
			Session session) {

		if (session == null) {
			SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
			Session session2 = sessionFactory.openSession();
			Long id = subscriptionDetails.getId();
			SubscriptionDetails subscriptionDetails1 = null;
			try {
				if (id == null) {
					subscriptionDetailsDAO.create(subscriptionDetails, session);
					return subscriptionDetails;
				} else {
					subscriptionDetails1 = (SubscriptionDetails) subscriptionDetailsDAO.getEntityById(id, enabled,
							SubscriptionDetails.class, session2);
					subscriptionDetails = addUpdates(subscriptionDetails1, subscriptionDetails);
					subscriptionDetailsDAO.update(subscriptionDetails, session2, subscriptionDetails1.getLastUpdatedby());
				}
			} catch (Exception e) {
				logger.error("addOrUpdateSubscriptionDetails Error: " + e.getMessage());
				throw e;
			} finally {
				session2.close();
			}
			return subscriptionDetails1;
		}

		Long id = subscriptionDetails.getId();
		if (id == null) {
			subscriptionDetailsDAO.create(subscriptionDetails, session);
			return subscriptionDetails;
		} else {
			subscriptionDetailsDAO.update(subscriptionDetails, session);
		}

		return subscriptionDetails;
	}

	public SubscriptionDetails getSubscriptionDetailsById(Long id, Boolean enabled, Session session) {
		if (session == null) {
			SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
			Session session2 = sessionFactory.openSession();

			SubscriptionDetails sd = null;
			try {
				sd = subscriptionDetailsDAO.getById(id, enabled, session2);
			} catch (Exception e) {
				logger.error("getSubscriptionDetailsById Error: " + e.getMessage());
				throw e;
			} finally {
				session2.close();
			}
			return sd;
		}
		return subscriptionDetailsDAO.getById(id, enabled, session);
	}

	public List<SubscriptionDetails> getSubscriptionDetails(AbstractFrontEndListReq req) {
		return subscriptionDetailsDAO.getEntities(SubscriptionDetails.class, req, true);
	}

	public List<SubscriptionDetails> getSubscriptionDetailsBySubscriptionId(Long subscriptionId, Boolean enabled,
			Session session) {
                logger.info("ENTRY subscriptionId "+subscriptionId+", enabled "+enabled);
		Criteria cr = session.createCriteria(SubscriptionDetails.class);
		if (enabled != null) {
			if (enabled.equals(true)) {
				cr.add(Restrictions.eq("enabled", true));
			} else {
				cr.add(Restrictions.eq("enabled", false));
			}
		}
		cr.add(Restrictions.eq("subscriptionId", subscriptionId));
		cr.addOrder(Order.desc("creationTime"));
                List<SubscriptionDetails> results=subscriptionDetailsDAO.runQuery(session, cr, SubscriptionDetails.class);
                logger.info("EXIT "+results);
		return results;
	}

	public void updateAllSubscriptionDetails(List<SubscriptionDetails> slist, Session session) {
		subscriptionDetailsDAO.updateAll(slist, session);
	}

	SubscriptionDetails addUpdates(SubscriptionDetails oldObj, SubscriptionDetails newObj) {

		if (newObj.getEnabled() != null) {
			oldObj.setEnabled(newObj.getEnabled());
		}
		if (newObj.getConsumedHours() != null) {
			oldObj.setConsumedHours(newObj.getConsumedHours());
		}
		if (newObj.getLockedHours() != null) {
			oldObj.setLockedHours(newObj.getLockedHours());
		}
		if (newObj.getRemainingHours() != null) {
			oldObj.setRemainingHours(newObj.getRemainingHours());
		}
		if (newObj.getSubscriptionId() != null) {
			oldObj.setSubscriptionId(newObj.getSubscriptionId());
		}
		if (newObj.getTeacherCouponId() != null) {
			oldObj.setTeacherCouponId(newObj.getTeacherCouponId());
		}
//		if (newObj.getLastUpdatedby() != null) {
//			oldObj.setLastUpdatedby(newObj.getLastUpdatedby());
//		}
		if (newObj.getTeacherDiscountAmount() != null) {
			oldObj.setTeacherDiscountAmount(newObj.getTeacherDiscountAmount());
		}
		if (newObj.getTeacherHourlyRate() != null) {
			oldObj.setTeacherHourlyRate(newObj.getTeacherHourlyRate());
		}
		if (newObj.getTotalHours() != null) {
			oldObj.setTotalHours(newObj.getTotalHours());
		}
		oldObj.setLastUpdatedTime(System.currentTimeMillis());
		return oldObj;
	}

    public OrderedItemNameDetailsRes getOrderedItemNames(OrderedItemNameDetailsReq itemNameReq) {
		List<OrderedItemNames> items = itemNameReq.getOrderedItemNames();
		List<String> aioIds=new ArrayList<>();
		List<String> coursePlanIds=new ArrayList<>();
		List<String> batchIds=new ArrayList<>();
		List<String> otmBundleIds = new ArrayList<>();
		List<String> bundlePackageIds=new ArrayList<>();

		items.forEach(item->{
			if(EntityType.BUNDLE.equals(item.getEntityType()) || EntityType.BUNDLE_TRIAL.equals(item.getEntityType())){
				aioIds.add(item.getEntityId());
			}

			if(EntityType.COURSE_PLAN.equals(item.getEntityType()) ||
					EntityType.COURSE_PLAN_REGISTRATION.equals(item.getEntityType()) ||
					EntityType.SUBSCRIPTION_REQUEST.equals(item.getEntityType())){
				coursePlanIds.add(item.getEntityId());
			}

			if(EntityType.OTF.equals(item.getEntityType()) ||
					EntityType.OTF_BATCH_REGISTRATION.equals(item.getEntityType()) ||
					EntityType.OTF_COURSE_REGISTRATION.equals(item.getEntityType()) ||
					EntityType.OTO_COURSE_REGISTRATION.equals(item.getEntityType())){
				batchIds.add(item.getEntityId());
			}

			if(EntityType.OTM_BUNDLE.equals(item.getEntityType()) ||
					EntityType.OTM_BUNDLE_ADVANCE_PAYMENT.equals(item.getEntityType()) ||
					EntityType.OTM_BUNDLE_REGISTRATION.equals(item.getEntityType())){
				otmBundleIds.add(item.getEntityId());
			}

			if (EntityType.BUNDLE_PACKAGE.equals(item.getEntityType()))
			{
				bundlePackageIds.add(item.getEntityId());
			}
		});

		Map<String,String> entityNameMap=new HashMap<>();
		if(ArrayUtils.isNotEmpty(aioIds)){
			List<Bundle> bundles=bundleDAO.getBundlesByIds(aioIds, Arrays.asList(Bundle.Constants._ID,Bundle.Constants.TITLE));
			bundles.forEach(bundle->entityNameMap.put(bundle.getId(),bundle.getTitle()));
		}

		if(ArrayUtils.isNotEmpty(coursePlanIds)){
			List<CoursePlan> coursePlans=coursePlanDAO.getCoursePlansByIds(coursePlanIds,Arrays.asList(CoursePlan.Constants._ID,CoursePlan.Constants.TITLE));
			coursePlans.forEach(coursePlan->entityNameMap.put(coursePlan.getId(),coursePlan.getTitle()));
		}

		if(ArrayUtils.isNotEmpty(batchIds)){
			List<Batch> batches = new ArrayList<>();
			batchIds.forEach(batchId -> batches.add(batchDAO.getById(batchId)));
			List<String> courseIds = new ArrayList<>();
			batches.forEach(batch ->{
				if(batch != null && batch.getCourseId() != null){
				   courseIds.add(batch.getCourseId());
				}});
			List<Course> courses=courseDAO.getCoursesBasicInfos(courseIds,Arrays.asList(Course.Constants._ID,Course.Constants.TITLE));
			courses.forEach(course-> {
				if(course != null && StringUtils.isNotEmpty(course.getTitle())) {
					entityNameMap.put(course.getId(), course.getTitle());
				}
			});
			batches.forEach(batch ->{
				if(batch != null && StringUtils.isNotEmpty(batch.getCourseId())) {
					entityNameMap.put(batch.getId(), entityNameMap.get(batch.getCourseId()));
				}});
		}

		if (ArrayUtils.isNotEmpty(otmBundleIds)){
			List<OTMBundle> otmBundles=otfBundleDAO.getOTMBundlesByIds(otmBundleIds,Arrays.asList(OTMBundle.Constants._ID,OTMBundle.Constants.TITLE));
			otmBundles.forEach(otmBundle -> entityNameMap.put(otmBundle.getId(), otmBundle.getTitle()));
		}

		if(ArrayUtils.isNotEmpty(bundlePackageIds)){
			List<BundlePackage> bundlePackages = new ArrayList<>();
			bundlePackageIds.forEach(bundlePackageId->bundlePackages.add(bundlePackageDAO.getBundlePackageById(bundlePackageId)));
			bundlePackages.forEach(bundlePackage -> entityNameMap.put(bundlePackage.getId(),bundlePackage.getTitle()));
		}

		items.forEach(item-> {
			if (entityNameMap.containsKey(item.getEntityId())) {
				item.setEntityTitle(entityNameMap.get(item.getEntityId()));
			}
		});

		return new OrderedItemNameDetailsRes(items);
	}
}
