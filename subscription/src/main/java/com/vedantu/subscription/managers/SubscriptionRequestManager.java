package com.vedantu.subscription.managers;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.util.InstalmentUtils;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.dao.SubscriptionRequestDAO;
import com.vedantu.subscription.entities.sql.SubscriptionRequest;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.viewobject.request.SubscriptionReqVO;
import com.vedantu.subscription.viewobject.response.GetSubscriptionRequestsResponse;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.subscription.request.BlockSlotScheduleReq;
import com.vedantu.subscription.request.GetSubscriptionRequestReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.subscription.BlockSlotScheduleRes;
import com.vedantu.util.subscription.GetSlotConflictReq;
import com.vedantu.util.subscription.GetSlotConflictRes;
import com.vedantu.util.subscription.ResolveSlotConflictReq;
import com.vedantu.util.subscription.ResolveSlotConflictRes;
import com.vedantu.util.subscription.SubscriptionRequestAction;
import com.vedantu.util.subscription.SubscriptionRequestState;
import com.vedantu.util.subscription.SubscriptionRequestSubState;
import com.vedantu.util.subscription.UpdateSubscriptionRequest;
import java.util.Map;

@Service
public class SubscriptionRequestManager {

	@Autowired
	private SubscriptionRequestDAO subscriptionRequestDAO;

	@Autowired
	private SubscriptionManager subscriptionManager;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private SqlSessionFactory sqlSessionfactory;

	@Autowired
	private InstalmentUtils instalmentUtils;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionRequestManager.class);

	private final String schedulingEndPoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
	public Long expireCronTime = ConfigUtils.INSTANCE.getLongValue("subscriptionRequest.expiryCronTime");
	public Long expiryReminderTime = ConfigUtils.INSTANCE.getLongValue("subscriptionRequest.expiryReminderTime");
	public Long expiryGraceTime = ConfigUtils.INSTANCE.getLongValue("subscriptionRequest.expiryGraceTime");
	public Long expiryOffset = ConfigUtils.INSTANCE.getLongValue("subscriptionRequest.expiryTimeOffset");

	public SubscriptionRequest createSubscriptionRequest(SubscriptionReqVO subscriptionReqVO) throws Exception {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		if (subscriptionReqVO.getSubscriptionRequestId() == null) {
			validateSubscriptionRequest(subscriptionReqVO);
		}
		SubscriptionRequest subscriptionRequest = null;
		try {
			transaction.begin();
			logger.info("transaction begin");
			ScheduleType schedule = null;
			Long startDate = 0l;
			Long endDate = 0l;
			Long noOfWeeeks = 0l;
			String slots = null;
			SessionSchedule sessionSchedule = subscriptionReqVO.getSchedule();
			if (sessionSchedule != null) {
				schedule = ScheduleType.FS;
				endDate = sessionSchedule.getEndDate();
				startDate = sessionSchedule.getStartDate();
				noOfWeeeks = sessionSchedule.getNoOfWeeks();
				slots = new Gson().toJson(sessionSchedule);
			} else {
				schedule = ScheduleType.NFS;
			}

			if (subscriptionReqVO.getSubscriptionRequestId() != null) {
				subscriptionRequest = getSubscriptionRequestById(subscriptionReqVO.getSubscriptionRequestId(), true,
						session);
				subscriptionRequest.setSlots(slots);
				subscriptionRequest.setSchedule(schedule);
				subscriptionRequest.setNoOfWeeks(noOfWeeeks);
				subscriptionRequest.setTotalHours(subscriptionReqVO.getTotalHours());
				subscriptionRequest.setPlanId(subscriptionReqVO.getPlanId());
				subscriptionRequest.setHourlyRate(subscriptionReqVO.getHourlyRate());
			} else {
				subscriptionRequest = new SubscriptionRequest(subscriptionReqVO.getTeacherId(),
						subscriptionReqVO.getStudentId(), subscriptionReqVO.getTitle(), subscriptionReqVO.getPlanId(),
						subscriptionReqVO.getModel(), SubModel.DEFAULT, schedule, subscriptionReqVO.getOfferingId(),
						subscriptionReqVO.getTotalHours(), subscriptionReqVO.getSubject(),
						subscriptionReqVO.getTarget(), subscriptionReqVO.getGrade(),
						subscriptionReqVO.getPaymentDetails(), startDate, endDate, subscriptionReqVO.getBoardId(),
						noOfWeeeks, null, subscriptionReqVO.getNote(), subscriptionReqVO.getHourlyRate(),
						SubscriptionRequestState.DRAFT, SubscriptionRequestAction.PAYMENT_PENDING,
						subscriptionReqVO.getStudentId(), null, slots, null, null, null, Role.STUDENT,
						subscriptionReqVO.getSessionSource());
			}
			//subscriptionRequest.setCallingUserId(subscriptionReqVO.getCallingUserId());

			logger.info("subscription Request to be updated : " + subscriptionRequest.toString());
			subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, subscriptionReqVO.getCallingUserId());

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			// TODO use try resource handling
			session.close();
		}

		logger.info("returning");
		return subscriptionRequest;
	}

	public SubscriptionRequest activateSubscriptionRequest(UpdateSubscriptionRequest req) throws Exception {
		logger.info("ENTRY " + req);
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		SubscriptionRequest subscriptionRequest = null;
		Long id = req.getId();
		try {
			transaction.begin();

			subscriptionRequest = getSubscriptionRequestById(id, true, session);

			if (req.getCallingUserId() == null || !req.getCallingUserId().toString().equals(subscriptionRequest.getCreatedBy())) {
				throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "User: " + req.getCallingUserId()
						+ "is not allowed to Activate SubscriptionRequest: " + subscriptionRequest.getId());
			}

			if (subscriptionRequest.getState() != null
					&& !subscriptionRequest.getState().equals(SubscriptionRequestState.DRAFT)) {
				throw new ConflictException(getErrorCode(subscriptionRequest.getState()),
						"Moving to Active from State: " + subscriptionRequest.getState());
			}

			Long startDate = subscriptionRequest.getStartDate();
			Long currentTime = System.currentTimeMillis();

			// When the schedule is missing, start date wont exist
			if (startDate == null || startDate <= 0l) {
				long temp = System.currentTimeMillis();
				temp = temp - (temp % DateTimeUtils.MILLIS_PER_HOUR);
				startDate = temp  + 2*DateTimeUtils.MILLIS_PER_DAY;
			}

			Long expiryTime = (startDate < (currentTime + expiryOffset)) ? startDate : (currentTime + expiryOffset);
			subscriptionRequest.setState(SubscriptionRequestState.ACTIVE);
			subscriptionRequest.setActionPendingBy(subscriptionRequest.getTeacherId());
			//subscriptionRequest.setCallingUserId(req.getCallingUserId());
			subscriptionRequest.setActivatedAt(currentTime);
			subscriptionRequest.setExpiryTime(expiryTime);
			subscriptionRequest.setStartDate(startDate);
			if (req.getLockBalanceInfo() != null)
				subscriptionRequest.setLockBalanceInfo(req.getLockBalanceInfo());
			if (req.getAmount() != null)
				subscriptionRequest.setAmount(req.getAmount());
			if (req.getPaymentDetails() != null)
				subscriptionRequest.setPaymentDetails(req.getPaymentDetails());
			if (req.getHourlyRate() != null)
				subscriptionRequest.setHourlyRate(req.getHourlyRate());

			// Calendar blocking
			BlockSlotScheduleRes blockSlotScheduleRes = blockCalendar(subscriptionRequest);
			SessionSchedule sessionSchedule = blockSlotScheduleRes.getSchedule();
			if (sessionSchedule != null) {
				if (sessionSchedule.getConflictCount() != null && sessionSchedule.getConflictCount() > 0) {
					subscriptionRequest.setAction(SubscriptionRequestAction.CONFLICT_PENDING);
				} else {
					subscriptionRequest.setAction(SubscriptionRequestAction.TEACHER_AWAITING);
				}
				subscriptionRequest.setSlots(new Gson().toJson(sessionSchedule));
			} else {
				subscriptionRequest.setAction(SubscriptionRequestAction.TEACHER_AWAITING);
			}

			subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, req.getCallingUserId());

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}
		logger.info("EXIT " + subscriptionRequest);
		return subscriptionRequest;
	}

	public SubscriptionRequest cancelSubscriptionRequest(UpdateSubscriptionRequest req) throws Exception {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		SubscriptionRequest subscriptionRequest = null;
		Long id = req.getId();
		try {
			transaction.begin();

			subscriptionRequest = getSubscriptionRequestById(id, true, session);

			if (req.getCallingUserId() == null || !req.getCallingUserId().toString().equals(subscriptionRequest.getCreatedBy())) {
				throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "User: " + req.getCallingUserId()
						+ " is not allowed to Cancel SubscriptionRequest: " + subscriptionRequest.getId());
			}

			checkActiveState(subscriptionRequest, SubscriptionRequestState.CANCELLED);

			// Unblock the Calendar
			unblockCalendar(subscriptionRequest);

			subscriptionRequest.setState(SubscriptionRequestState.CANCELLED);
			subscriptionRequest.setAction(SubscriptionRequestAction.NONE);
			subscriptionRequest.setActionPendingBy(null);
			subscriptionRequest.setReason(req.getReason());
			//subscriptionRequest.setCallingUserId(req.getCallingUserId());
			subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, req.getCallingUserId());

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		return subscriptionRequest;
	}

	public SubscriptionRequest rejectSubscriptionRequest(UpdateSubscriptionRequest req) throws Exception {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		SubscriptionRequest subscriptionRequest = null;
		Long id = req.getId();
		try {
			transaction.begin();

			subscriptionRequest = getSubscriptionRequestById(id, true, session);

			if (req.getCallingUserId() == null||subscriptionRequest.getCreatedBy()==null
					|| (!(subscriptionRequest.getCreatedBy().equals(subscriptionRequest.getTeacherId().toString())
							&& req.getCallingUserId().equals(subscriptionRequest.getStudentId()))
							&& !(subscriptionRequest.getCreatedBy().equals(subscriptionRequest.getStudentId().toString())
									&& req.getCallingUserId().equals(subscriptionRequest.getTeacherId())))) {
				throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "User: " + req.getCallingUserId()
						+ " is not allowed to Reject SubscriptionRequest: " + subscriptionRequest.getId());
			}

			checkActiveState(subscriptionRequest, SubscriptionRequestState.REJECTED);

			// Unblock the Calendar
			unblockCalendar(subscriptionRequest);

			subscriptionRequest.setState(SubscriptionRequestState.REJECTED);
			subscriptionRequest.setAction(SubscriptionRequestAction.NONE);
			subscriptionRequest.setActionPendingBy(null);
			subscriptionRequest.setReason(req.getReason());
			//subscriptionRequest.setCallingUserId(req.getCallingUserId());
			subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, req.getCallingUserId());

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		return subscriptionRequest;
	}

	public SubscriptionRequest acceptSubscriptionRequest(UpdateSubscriptionRequest req) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		SubscriptionRequest subscriptionRequest = null;
		Long id = req.getId();
		try {
			transaction.begin();

			subscriptionRequest = getSubscriptionRequestById(id, true, session);

			if (req.getCallingUserId() == null
					|| (!(subscriptionRequest.getCreatedBy().equals(subscriptionRequest.getTeacherId().toString())
							&& req.getCallingUserId().equals(subscriptionRequest.getStudentId()))
							&& !(subscriptionRequest.getCreatedBy().equals(subscriptionRequest.getStudentId().toString())
									&& req.getCallingUserId().equals(subscriptionRequest.getTeacherId())))) {
				throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "User: " + req.getCallingUserId()
						+ " is not allowed to Accept SubscriptionRequest: " + subscriptionRequest.getId());
			}

			checkActiveState(subscriptionRequest, SubscriptionRequestState.ACCEPTED);

			subscriptionRequest.setState(SubscriptionRequestState.ACCEPTED);
			subscriptionRequest.setAction(SubscriptionRequestAction.NONE);
			subscriptionRequest.setActionPendingBy(null);
			//subscriptionRequest.setCallingUserId(req.getCallingUserId());
			subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, req.getCallingUserId());

			transaction.commit();
		} catch (Throwable e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		return subscriptionRequest;
	}
        
	public SubscriptionRequest updatePaymentDetails(Map<String, String> requestParams)
			throws Throwable {
                if(requestParams.get("subscriptionRequestId")!=null){
                    Long subscriptionRequestId=Long.parseLong(requestParams.get("subscriptionRequestId"));
                    logger.info("Request - " + subscriptionRequestId);
                    SubscriptionRequest subscriptionRequest = getSubscriptionRequest(subscriptionRequestId, null);
                    logger.info("SubscriptionRequest pre update - " + subscriptionRequest.toString());
                    subscriptionRequest.setPaymentDetails(requestParams.get("paymentDetails"));
                    addOrUpdateSubscriptionRequest(subscriptionRequest);
                    logger.info("Response from Subscription: " + subscriptionRequest.toString());
                    return subscriptionRequest;	                    
                }else{
                    throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "subscriptionRequestId not provided");
                }
	}        

	public SubscriptionRequest createSubscriptionFromRequest(Long subscriptionRequestId, Long hoursToSchedule, Long callingUserId)
			throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		SubscriptionRequest subscriptionRequest = null;
		SubscriptionResponse response = null;
		try {
			transaction.begin();

			subscriptionRequest = getSubscriptionRequestById(subscriptionRequestId, true, session);

			response = subscriptionManager.createSubscriptionFromRequest(subscriptionRequest, hoursToSchedule, session);
			subscriptionRequest.setSubscriptionId(response.getSubscriptionId());
			subscriptionRequest.setSubscriptionDetailsId(response.getSubscriptionDetailsId());
			subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, callingUserId);

			transaction.commit();
		} catch (Throwable e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		return subscriptionRequest;
	}

	public SubscriptionRequest addOrUpdateSubscriptionRequest(SubscriptionRequest subscriptionRequest) {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		try {
			transaction.begin();
			Long callingUserId = subscriptionRequest.getCallingUserId()!=null? Long.parseLong(subscriptionRequest.getCallingUserId()) : null;
			subscriptionRequest = subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, callingUserId);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		return subscriptionRequest;
	}

	public SubscriptionRequest getSubscriptionRequest(Long id, Long callingUserId) {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		SubscriptionRequest subscriptionRequest = null;
		try {
			subscriptionRequest = getSubscriptionRequestById(id, true, session);
			updateViewedAt(subscriptionRequest, callingUserId, session);
		} catch (Exception e) {
			logger.info(e.toString());
		} finally {
			session.close();
		}
		return subscriptionRequest;
	}

	public SubscriptionRequest getSubscriptionRequestById(Long id, Boolean enabled, Session session) {
		SubscriptionRequest subscriptionRequest = null;
		try {
			subscriptionRequest = subscriptionRequestDAO.getById(id, enabled, session);
		} catch (Exception e) {
			logger.error("Error: " + e.getMessage());
			throw e;
		}
		return subscriptionRequest;
	}

	public GetSubscriptionRequestsResponse getPendingSubscriptionRequests(Long startTime, Long endTime)
			throws Throwable {
		logger.info("Request : startTime -" + startTime + " endTime - " + endTime);
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		List<SubscriptionRequest> results = null;
		try {
			Criteria cr = session.createCriteria(SubscriptionRequest.class);
			cr.add(Restrictions.ge("creationTime", startTime));
			cr.add(Restrictions.le("creationTime", endTime));
			cr.add(Restrictions.eq("state", SubscriptionRequestState.ACCEPTED));
			cr.add(Restrictions.ne("subState", SubscriptionRequestSubState.SUCCESSFUL));
			cr.addOrder(Order.desc("creationTime"));

			logger.info("criteria : " + cr.toString());
			results = subscriptionRequestDAO.runQuery(session, cr, SubscriptionRequest.class);
		} catch (Exception e) {
			logger.error("getPendingSubscriptionRequests Error: " + e.getMessage());
			throw new VException(ErrorCode.SERVICE_ERROR,
					"getPendingSubscriptionRequests Error: " + e.getMessage());
		} finally {
			session.close();
		}

		GetSubscriptionRequestsResponse response = new GetSubscriptionRequestsResponse((results==null)?0:results.size(), results);
		return response;
	}

	public GetSubscriptionRequestsResponse getSubscriptionRequests(GetSubscriptionRequestReq req) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		List<SubscriptionRequest> results = null;
		try {
			Criteria cr = session.createCriteria(SubscriptionRequest.class);
			for (Field field : req.getClass().getDeclaredFields()) {
				field.setAccessible(true);
				try {
					if (!field.getName().equals("lastUpdatedTime") && field.get(req) != null) {
						cr.add(Restrictions.eq(field.getName(), field.get(req)));
					}
				} catch (IllegalArgumentException | IllegalAccessException e) {
					logger.info(e.toString());
				}
			}

			if (req.getLastUpdatedTime() != null) {
				cr.add(Restrictions.ge("lastUpdatedTime", req.getLastUpdatedTime()));
			}
			cr.addOrder(Order.desc("creationTime"));

			Integer start = req.getStart();
			Integer size = req.getSize();
			if (start == null || start < 0) {
				start = 0;
			}
			if (size == null || size <= 0) {
				size = 20;
			}
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			results = subscriptionRequestDAO.runQuery(session, cr, SubscriptionRequest.class);
		} catch (Exception e) {
			logger.error("getSubscriptionRequests Error: " + e.getMessage());
			throw new VException(ErrorCode.SERVICE_ERROR, "getSubscriptionRequests Error: " + e.getMessage());
		} finally {
			session.close();
		}

		GetSubscriptionRequestsResponse response = new GetSubscriptionRequestsResponse(results.size(), results);
		return response;
	}

	public GetSubscriptionRequestsResponse expireSubscriptionRequests(Long callingUserId) throws Exception {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		Long currentTime = System.currentTimeMillis();
		Long expiryTime = currentTime - expireCronTime;
		List<SubscriptionRequest> results = null;
		try {
			transaction.begin();

			Criteria cr = session.createCriteria(SubscriptionRequest.class);
//			cr.add(Restrictions.ge("expiryTime", expiryTime));
			cr.add(Restrictions.le("expiryTime", currentTime));
			cr.add(Restrictions.eq("state", SubscriptionRequestState.ACTIVE));
			results = subscriptionRequestDAO.runQuery(session, cr, SubscriptionRequest.class);

			for (SubscriptionRequest subscriptionRequest : results) {
				try {
					unblockCalendar(subscriptionRequest);
				} catch (Exception e) {
					logger.error("expireSubscriptionRequests: Unblock Calendar failed for SubscriptionRequestId: "
							+ subscriptionRequest.getId());
					continue;
				}
				subscriptionRequest.setState(SubscriptionRequestState.EXPIRED);
				subscriptionRequest.setAction(SubscriptionRequestAction.NONE);
				subscriptionRequest.setActionPendingBy(null);
			}
			subscriptionRequestDAO.updateAll(results, session, callingUserId);

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		GetSubscriptionRequestsResponse response = new GetSubscriptionRequestsResponse(results.size(), results);
		return response;
	}

	public GetSubscriptionRequestsResponse expiryReminderSubscriptionRequests() throws Exception {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		Long currentTime = System.currentTimeMillis();
		Long expiryTime = currentTime + expiryReminderTime;
		List<SubscriptionRequest> results = null;
		try {
			Criteria cr = session.createCriteria(SubscriptionRequest.class);
			cr.add(Restrictions.le("expiryTime", expiryTime));
			cr.add(Restrictions.ge("expiryTime", currentTime));
			cr.add(Restrictions.eq("state", SubscriptionRequestState.ACTIVE));
			results = subscriptionRequestDAO.runQuery(session, cr, SubscriptionRequest.class);
		} catch (Exception e) {
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}
		GetSubscriptionRequestsResponse response = new GetSubscriptionRequestsResponse(results.size(), results);
		return response;
	}

	void validateSubscriptionRequest(SubscriptionReqVO subscriptionReqVO) throws BadRequestException {
		String error = "";
		if (subscriptionReqVO.getTeacherId() == null) {
			error += "TeacherId";
		}
		if (subscriptionReqVO.getStudentId() == null) {
			error += "StudentId";
		}
		if (subscriptionReqVO.getPlanId() == null) {
			error += "PlanId";
		}
		if (subscriptionReqVO.getSubject() == null) {
			error += "Subject";
		}
		if (subscriptionReqVO.getGrade() == null) {
			error += "Grade";
		}
		if (subscriptionReqVO.getTarget() == null) {
			error += "Target";
		}
		if (subscriptionReqVO.getModel() == null) {
			error += "Model";
		}
		if (subscriptionReqVO.getHourlyRate() == null) {
			error += "HourlyRate";
		}
		if (subscriptionReqVO.getTotalHours() == null) {
			error += "TotalHours";
		}
		Long requestedHours = getRequestedHours(subscriptionReqVO.getSchedule());
		if (requestedHours > subscriptionReqVO.getTotalHours()) {
			logger.error("RequestedHours = " + requestedHours + " are more than the hours = "
					+ subscriptionReqVO.getTotalHours() + " which user paid for");
			throw new BadRequestException(ErrorCode.SUBSCRIPTION_INCORRECT_HOURS,
					"createSubscriptionRequest: RequestedHours: " + requestedHours + " are more than the hours: "
							+ subscriptionReqVO.getTotalHours() + " which user paid for");
		}
		if (!error.equals("")) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "createSubscriptionRequest: " + error);
		}

		logger.info("Validating done : " + error);
	}

	private Long getRequestedHours(SessionSchedule schedule) {
		if (schedule == null) {
			return 0l;
		}
		Long hours = 0l;
		List<SessionSlot> slots = schedule.getSessionSlots();
		if (slots == null) {
			return 0l;
		}
		for (SessionSlot s : slots) {
			hours += (s.getEndTime() - s.getStartTime());
		}
		Long noOfWeeks = 0l;
		if (schedule.getNoOfWeeks() != null) {
			noOfWeeks = schedule.getNoOfWeeks();
		}
		return hours * (noOfWeeks);
	}

	public SubscriptionRequest updateSubscriptionRequestSubState(Long id, SubscriptionRequestSubState subState, Long callingUserId)
			throws Throwable {
		logger.info("updateSubscriptionRequestSubState" + id + " subState" + subState);
		if (id == null || id <= 0l || subState == null) {
			String errorString = "parameters missing for req : " + id + " substate : " + subState;
			logger.error(errorString);
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errorString);
		}

		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		SubscriptionRequest subscriptionRequest = null;
		try {
			transaction.begin();
			subscriptionRequest = getSubscriptionRequestById(id, true, session);
			if (subscriptionRequest == null) {
				String errorString = "updateSubscriptionRequestSubState Subscription requestId missing for id : " + id;
				logger.error(errorString);
				throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, errorString);
			}

			subscriptionRequest.setSubState(subState);
			if (SubscriptionRequestSubState.SUCCESSFUL.equals(subState)) {
				subscriptionRequest.setState(SubscriptionRequestState.SUBSCRIBED);
				subscriptionRequest.setAction(SubscriptionRequestAction.NONE);
				subscriptionRequest.setActionPendingBy(null);
				Long lockedHours = getLockedHours(subscriptionRequest.getSubscriptionId());
				subscriptionManager.activateSubscription(subscriptionRequest.getSubscriptionId(), lockedHours, session, callingUserId);
			}

			subscriptionRequestDAO.addOrUpdateSubscriptionRequest(subscriptionRequest, session, callingUserId);
			transaction.commit();
		} catch (Throwable e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof VException)) {
				logger.error("Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		return subscriptionRequest;
	}

	public void updateViewedAt(SubscriptionRequest subscriptionRequest, Long callingUserId, Session session) {
		if (callingUserId == null)
			return;

		Transaction transaction = session.getTransaction();

		if (callingUserId.equals(subscriptionRequest.getStudentId())) {
			if (subscriptionRequest.getStudentViewedAt() == null) {
				transaction.begin();
				subscriptionRequest.setStudentViewedAt(System.currentTimeMillis());
				transaction.commit();
			}
		} else if (callingUserId.equals(subscriptionRequest.getTeacherId())) {
			if (subscriptionRequest.getTeacherViewedAt() == null) {
				transaction.begin();
				subscriptionRequest.setTeacherViewedAt(System.currentTimeMillis());
				transaction.commit();
			}
		}
	}

	public GetSlotConflictRes getSubscriptionRequestConflicts(Long id) throws VException {
		SubscriptionRequest subscriptionRequest = getSubscriptionRequest(id, null);
		subscriptionRequest = extendExpiryTimeIfRequired(subscriptionRequest);
		return getSubscriptionRequestConflicts(subscriptionRequest);
	}

	public GetSlotConflictRes getSubscriptionRequestConflicts(SubscriptionRequest subscriptionRequest)
			throws VException {
            
                SessionSchedule sessionSchedule=new Gson().fromJson(subscriptionRequest.getSlots(), SessionSchedule.class);
                if(ArrayUtils.isEmpty(sessionSchedule.getSessionSlots())){
                    GetSlotConflictRes nresponse=new GetSlotConflictRes(subscriptionRequest.getStudentId(),
                            subscriptionRequest.getTeacherId(), null, null, null);
                    return nresponse;
                }
		GetSlotConflictReq getSlotConflictReq = new GetSlotConflictReq();
		getSlotConflictReq.setStudentId(subscriptionRequest.getStudentId());
		getSlotConflictReq.setTeacherId(subscriptionRequest.getTeacherId());
		getSlotConflictReq.setSchedule(sessionSchedule);
		getSlotConflictReq.setReferenceId(String.valueOf(subscriptionRequest.getId()));

		logger.info("Scheduling Request: " + getSlotConflictReq.toString());
		String getSlotConflictUrl = schedulingEndPoint + ConfigUtils.INSTANCE.getStringValue("calendar.get.slot.conflicts");
		ClientResponse resp = WebUtils.INSTANCE.doCall(getSlotConflictUrl, HttpMethod.POST,
				new Gson().toJson(getSlotConflictReq));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);

		String getSlotConflictRes = resp.getEntity(String.class);
		GetSlotConflictRes response = new Gson().fromJson(getSlotConflictRes, GetSlotConflictRes.class);

		logger.info("Scheduling Response: " + response.toString());

		// Update subscription request with latest slots
		SessionSchedule schedule = new Gson().fromJson(subscriptionRequest.getSlots(), SessionSchedule.class);
		schedule.setConflictSlotBitSet(response.getConflictSlotBits());
		schedule.setConflictCount(response.getConflictSessions() == null ? 0 : response.getConflictSessions().size());
		schedule.setSlotBitSet(response.getSlotBits());
		schedule.setStartTime(response.getStartTime());
		schedule.setEndTime(response.getEndTime());
		subscriptionRequest.setSlots(new Gson().toJson(schedule));
		addOrUpdateSubscriptionRequest(subscriptionRequest);

		// sending only the conflicts first 35 days conflicts in all cases
		InstalmentUtils.ConflictDetails conflictDetails = instalmentUtils.fillResolvableConflictDetails(schedule);
		if (conflictDetails != null && conflictDetails.conflictSessions != null) {
			response.setConflictSessions(conflictDetails.conflictSessions);
			response.setResolutionStartTime(conflictDetails.resolutionStartTime);
			response.setResolutionEndTime(conflictDetails.resolutionEndTime);
		}
		return response;
	}

	public ResolveSlotConflictRes resolveSubscriptionRequestConflicts(ResolveSlotConflictReq req) throws VException {

		// Fetch subscription request
		String referenceId = req.getReferenceId();
		SubscriptionRequest subscriptionRequest = getSubscriptionRequest(Long.parseLong(referenceId), null);
		checkActiveState(subscriptionRequest, SubscriptionRequestState.ACCEPTED);

		req.setNoOfWeeks(subscriptionRequest.getNoOfWeeks());
		req.setStudentId(String.valueOf(subscriptionRequest.getStudentId()));
		req.setTeacherId(String.valueOf(subscriptionRequest.getTeacherId()));
		SessionSchedule schedule = new Gson().fromJson(subscriptionRequest.getSlots(), SessionSchedule.class);
		req.setStartTime(schedule.getStartTime());
		req.setBitSetLongArray(schedule.getSlotBitSet());
		req.setConflictSlotBits(schedule.getConflictSlotBitSet());

		// Resolve conflicts from request
		logger.info("Scheduling Request: " + req.toString());
		String resolveCalendarUrl = schedulingEndPoint + ConfigUtils.INSTANCE.getStringValue("calendar.resolve.slot.conflicts");
		ClientResponse resp = WebUtils.INSTANCE.doCall(resolveCalendarUrl, HttpMethod.POST, new Gson().toJson(req));
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		ResolveSlotConflictRes response = new Gson().fromJson(jsonString, ResolveSlotConflictRes.class);
		logger.info("Scheduling Response: " + response.toString());

		// Update subscription request
		schedule.setSlotBitSet(response.getSlotBits());
		schedule.setConflictSlotBitSet(response.getConflictSlotBits());
		subscriptionRequest.setSlots(new Gson().toJson(schedule));

		// Get conflicts again to find pending conflict slots
		GetSlotConflictRes getSlotConflictRes = getSubscriptionRequestConflicts(subscriptionRequest);
		response.setPendingConflicts(getSlotConflictRes.getConflictSessions());

		if (response.getPendingConflicts() == null || response.getPendingConflicts().isEmpty()) {
			subscriptionRequest.setAction(SubscriptionRequestAction.TEACHER_AWAITING);
		}
		addOrUpdateSubscriptionRequest(subscriptionRequest);
		logger.info("Response from Subscription: " + jsonString);
		return response;
	}

	public SubscriptionRequest markScheduledSubState(Long subscriptionRequestId) throws VException {
		logger.info("Request - " + subscriptionRequestId);
		SubscriptionRequest subscriptionRequest = getSubscriptionRequest(subscriptionRequestId, null);
		logger.info("SubscriptionRequest pre update - " + subscriptionRequest.toString());
		if (SubscriptionRequestSubState.INIT.equals(subscriptionRequest.getSubState())
				|| SubscriptionRequestSubState.AMOUNT_DEDUCTED.equals(subscriptionRequest.getSubState())
				|| SubscriptionRequestSubState.SUBSCRIPTION_CREATED.equals(subscriptionRequest.getSubState())) {
			subscriptionRequest.setSubState(SubscriptionRequestSubState.SESSIONS_SCHEDULED);
			addOrUpdateSubscriptionRequest(subscriptionRequest);
		}

		logger.info("Response from Subscription: " + subscriptionRequest.toString());
		return subscriptionRequest;
	}

	protected Long getLockedHours(Long subscriptionId) throws VException {
		logger.info("getLockedHours called for Subscription:" + subscriptionId);
		String lockedUrl = schedulingEndPoint + "/session/getSubscriptionScheduledHours?subscriptionId=" + subscriptionId;
		ClientResponse resp = WebUtils.INSTANCE.doCall(lockedUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowFosException(resp);
		Long lockedHrs=0l;
                try{
                    lockedHrs=Long.parseLong(resp.getEntity(String.class));
                }catch(Exception e){
                    //swallow
                }
		logger.info("Response: lockedHrs" + lockedHrs);
		return lockedHrs;
	}

	protected BlockSlotScheduleRes blockCalendar(SubscriptionRequest subscriptionRequest) throws Exception {
		try {
			logger.info("blockCalendar called for SubscriptionRequest: " + subscriptionRequest.toString());
			String blockCalendarUrl = schedulingEndPoint + ConfigUtils.INSTANCE.getStringValue("calendar.block.slot.schedule");
			BlockSlotScheduleReq blockSlotScheduleReq = new BlockSlotScheduleReq();
			blockSlotScheduleReq.setStudentId(String.valueOf(subscriptionRequest.getStudentId()));
			blockSlotScheduleReq.setTeacherId(String.valueOf(subscriptionRequest.getTeacherId()));
			blockSlotScheduleReq.setReferenceId(String.valueOf(subscriptionRequest.getId()));
			SessionSchedule schedule = new Gson().fromJson(subscriptionRequest.getSlots(), SessionSchedule.class);
			schedule.setStartTime(subscriptionRequest.getStartDate());
			blockSlotScheduleReq.setSchedule(schedule);
			ClientResponse resp = WebUtils.INSTANCE.doCall(blockCalendarUrl, HttpMethod.POST,
					new Gson().toJson(blockSlotScheduleReq));
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			String jsonString = resp.getEntity(String.class);
			logger.info("Response from Scheduling: " + jsonString);
			BlockSlotScheduleRes blockSlotScheduleRes = new Gson().fromJson(jsonString, BlockSlotScheduleRes.class);
			return blockSlotScheduleRes;
		} catch (Exception ex) {
			logger.error("activateSubscriptionRequestError - Calendar update error" + subscriptionRequest.toString());
			throw ex;
		}
	}

	protected void unblockCalendar(SubscriptionRequest subscriptionRequest) throws VException {
		logger.info("Unblock Calendar called for SubscriptionRequest: " + subscriptionRequest.toString());
		String unblockCalendarUrl = schedulingEndPoint + ConfigUtils.INSTANCE.getStringValue("calendar.unblock.slot.schedule")
				+ subscriptionRequest.getId();
		ClientResponse resp = WebUtils.INSTANCE.doCall(unblockCalendarUrl, HttpMethod.GET, null);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		logger.info("Response: " + resp.getEntity(String.class));
	}

	protected ErrorCode getErrorCode(SubscriptionRequestState state) {
		ErrorCode errorCode = ErrorCode.INVALID_SUBSCRIPTION_REQUEST_STATE;
		switch (state) {
		case CANCELLED:
			errorCode = ErrorCode.SUBSCRIPTION_REQUEST_ALREADY_CANCELLED;
			break;
		case REJECTED:
			errorCode = ErrorCode.SUBSCRIPTION_REQUEST_ALREADY_REJECTED;
			break;
		case EXPIRED:
			errorCode = ErrorCode.SUBSCRIPTION_REQUEST_ALREADY_EXPIRED;
			break;
		case ACCEPTED:
		case SUBSCRIBED:
			errorCode = ErrorCode.SUBSCRIPTION_REQUEST_ALREADY_ACCEPTED;
			break;
		default:
			break;
		}
		return errorCode;
	}

	protected void checkActiveState(SubscriptionRequest subscriptionRequest, SubscriptionRequestState newState)
			throws VException {
		if (subscriptionRequest.getState() != null
				&& !subscriptionRequest.getState().equals(SubscriptionRequestState.ACTIVE)) {
			logger.info("Moving to " + newState + " from State: " + subscriptionRequest.getState());
			String stateString = subscriptionRequest.getState().name().toLowerCase();
			stateString.toLowerCase();
			throw new ConflictException(getErrorCode(subscriptionRequest.getState()),
					"Action not allowed! Subscription request is already " + stateString);
		}
	}

	protected SubscriptionRequest extendExpiryTimeIfRequired(SubscriptionRequest subscriptionRequest) {
		Long currentTime = System.currentTimeMillis();
		Long expiryTime = subscriptionRequest.getExpiryTime();
		Long startDate = subscriptionRequest.getStartDate();
		Long activatedAt = subscriptionRequest.getActivatedAt();
		Long actualExpiryTime = (startDate < (activatedAt + expiryOffset)) ? startDate : (activatedAt + expiryOffset);

		if (actualExpiryTime.equals(expiryTime) && expiryTime <= (currentTime + expiryGraceTime)) {
			subscriptionRequest.setExpiryTime(expiryTime + expiryGraceTime);
			logger.info("Extended expiryTime of SubscriptionReauest: " + subscriptionRequest.getId() + " by "
					+ expiryGraceTime / DateTimeUtils.MILLIS_PER_HOUR + " hours");
		}
		return subscriptionRequest;
	}
}
