package com.vedantu.subscription.managers;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.UserInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.*;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.pojo.section.SectionChangeTime;
import com.vedantu.onetofew.pojo.section.SectionPojo;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BundleEnrolmentDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.SectionDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.subscription.enums.TeacherType;
import com.vedantu.subscription.pojo.section.BatchSectionPojo;
import com.vedantu.subscription.pojo.section.EnrollmentListPojo;
import com.vedantu.subscription.request.section.*;
import com.vedantu.subscription.response.section.*;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.util.The_Comparator;
import com.vedantu.subscription.viewobject.response.section.SectionEnrollmentAggregationRes;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.pojo.Pair;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class SectionManager{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SectionManager.class);

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private SectionDAO sectionDAO;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private BatchManager batchManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private BundleEnrolmentDAO bundleEnrolmentDAO;

    @Autowired
    private BatchDAO batchDAO;

    private static Gson gson = new Gson();
    public static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private final String SCHEDULING_ENDPOINT =ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static String batchTrialSectionRedisSlug = env + "_BATCH_TRIAL_SECTIONS_";
    private static String batchRegularSectionRedisSlug = env + "_BATCH_REGULAR_SECTIONS_";
    private static String batchEnrollmentsUpdatedCountSlug = env + "_BATCH_ENROLLMENTS_UPDATED_COUNT_";

    // NOTE : THIS SHOULD ONLY BE CALLED FROM SQS or remote call and not through APIs, only after batch creation/updation
    public void createSections(AddSectionReq addSectionReq)throws BadRequestException {
        logger.info("Create sections");
        // Not using verify() as need to append to sentry if exception
        if(Objects.isNull(addSectionReq.getBatchId()) || StringUtils.isEmpty(addSectionReq.getBatchId()))
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"Trying to create sections for empty/null batchId" , Level.ERROR);

        if(Objects.isNull(addSectionReq.getSectionList()) || addSectionReq.getSectionList().isEmpty())
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"Trying to create sections with empty/null sections" , Level.ERROR);

        List<Section> sections = new ArrayList<>();
        Map<String,String> teacherIdSectionNameMap = new HashMap<>();
        Map<String,TeacherType> teacherIdTypeMap = new HashMap<>();

        for(SectionPojo sectionpojo : addSectionReq.getSectionList()){

            boolean isPrimary = true;
            // fetch teacherIds from emailIds
            List<String> teacherIds = new ArrayList<>();
            for(String emailId : sectionpojo.getEmailIds()){
                UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfoFromEmail(emailId,true);
                teacherIds.add(userBasicInfo.getUserId().toString());

                // used while sending email
                teacherIdSectionNameMap.put(userBasicInfo.getUserId().toString(),sectionpojo.getSectionName());

                if(isPrimary) {
                    teacherIdTypeMap.put(userBasicInfo.getUserId().toString(),TeacherType.PRIMARY);
                    isPrimary = false;
                }
                else
                    teacherIdTypeMap.put(userBasicInfo.getUserId().toString(),TeacherType.SECONDARY);
            }

            // set data to section
            Section section = new Section();
            section.setBatchId(addSectionReq.getBatchId());
            section.setSectionName(sectionpojo.getSectionName());
            section.setSectionType(sectionpojo.getSectionType());
            section.setSize(sectionpojo.getSize());
            section.setTeacherIds(teacherIds);
            section.setSeatsVacant(sectionpojo.getSize());

            // add to sections list
            sections.add(section);
        }

        // create sections
        sectionDAO.updateAll(sections);
        logger.info("sections {}",sections);

        //send mail to assigned teachers
        sendEmailToSectionTeachers(teacherIdSectionNameMap,teacherIdTypeMap,addSectionReq.getBatchId());

    }

    public void updateSections(UpdateSectionsReq updateSectionsReq) throws VException, BadRequestException {
        // capture data from req
        List<SectionInfo> newSections = updateSectionsReq.getSectionList();
        String batchId = updateSectionsReq.getBatchId();

        // check if sections enabled for this batch
        batchValidation(batchId);
        // validate all the sections coming from frontend
        validateSections(newSections,batchId);

        // fetch old sections from db
        List<Section> oldSections = new ArrayList<>();
        List<Section> temp;
        int skip = 0;
        int limit = 25;
        boolean processing = true;
        while(processing) {
            temp = sectionDAO.getSectionsForBatchId(batchId, skip, limit, null);
            if(temp.size() == 0)
                processing = false;
            else {
                skip += temp.size();
                oldSections.addAll(temp);
            }
        }
        // fetch idEnrollmentCountMap for old sections
        Map<String,Long> idEnrollmentCountMapForOldSections = getEnrolledCountForSections(batchId);

        logger.info("old sections {}",oldSections);
        logger.info("new sections {}",newSections);

        // make idSectionMap for both old and new sections
        Map<String,Section> idSectionMap_old = new HashMap<>();
        Map<String,SectionInfo> idSectionMap_new = new HashMap<>();
        oldSections.forEach(s -> idSectionMap_old.put(s.getId(),s));
        newSections.forEach(s -> idSectionMap_new.put(s.getSectionId(),s));

        // get old and new sizes and check whether addition or removal of section has happened
        int newSectionsSize = newSections.size();
        int oldSectionsSize = oldSections.size();
        int diff = newSectionsSize-oldSectionsSize;

        // for +/- of sections
        List<SectionInfo> sectionsToBeCreated = new ArrayList<>();
        List<Section> sectionToBeMarkedDeleted = new ArrayList<>();

        // adding all old sections which need to be deleted
        for(Section section: oldSections){
            if(!idSectionMap_new.containsKey(section.getId())) {
                // check for current enrollment count > 0 if false then only add to sectionToBeMarkedDeleted
                long enrollCt = idEnrollmentCountMapForOldSections.get(section.getId());
                if(enrollCt == 0)
                    sectionToBeMarkedDeleted.add(section);
                else
                    throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"Cannot delete this section " + section.getId() + " as there are enrollments for this section");
            }
        }

        // for sending email to new teachers
        Map<String,String> teacherIdSectionNameMap = new HashMap<>();
        Map<String,TeacherType> teacherIdTypeMap = new HashMap<>();

        // updations if any for all existing sections in db
        for(SectionInfo newSection : newSections){

            String id = newSection.getSectionId();
            String name = newSection.getSectionName();
            long size = newSection.getSize();
            EnrollmentState type = newSection.getSectionType();
            List<String> teacherEmailIds = newSection.getEmailIds();

            if(!idSectionMap_old.containsKey(id) && (id == null || id.isEmpty())) {
                sectionsToBeCreated.add(newSection);
                continue;
            }

            Section oldSection = idSectionMap_old.get(id);
            if(Objects.nonNull(oldSection)){

                // sectionName check
                if(!(oldSection.getSectionName().equals(name)))
                    oldSection.setSectionName(name);

                // sectionType check
                long enrollmentCount = idEnrollmentCountMapForOldSections.get(oldSection.getId());
                if( !(oldSection.getSectionType().equals(type))  && enrollmentCount != 0)
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Section type can only be changed if there are no enrollments");
                else
                    oldSection.setSectionType(type);

                long increaseBy = 0;
                // sectionSize check
                if(oldSection.getSize() != size){

                    if(size < oldSection.getSize() && size < enrollmentCount + Section.Constants.ENROLLMENT_OFFSET)
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Section size cannot be lowere than current enrollment + " + Section.Constants.ENROLLMENT_OFFSET);

                    if(oldSection.getSectionType().equals(EnrollmentState.TRIAL) && oldSection.getSize() > Section.Constants.MAX_TRIAL_SECTION_SIZE)
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "TRIAL section size cannot be greater than" + Section.Constants.MAX_TRIAL_SECTION_SIZE);

                    if(oldSection.getSectionType().equals(EnrollmentState.REGULAR) && oldSection.getSize() > Section.Constants.MAX_REGULAR_SECTION_SIZE)
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "TRIAL section size cannot be greater than" + Section.Constants.MAX_REGULAR_SECTION_SIZE);

                    increaseBy = (size - oldSection.getSize());
                    oldSection.setSize(size);
                }

                // teacherIds check
                final List<String> newTeacherIds = getTeacherIdsFromEmailIds(teacherEmailIds);
                final List<String> oldTeacherIds = oldSection.getTeacherIds();
                String newPrimaryTeacherId = newTeacherIds.get(0);
                String oldPrimaryTeacherId = oldTeacherIds.get(0);

                if (!newPrimaryTeacherId.equals(oldPrimaryTeacherId)){
                    teacherIdSectionNameMap.put(newPrimaryTeacherId,oldSection.getSectionName());
                    teacherIdTypeMap.put(newPrimaryTeacherId,TeacherType.PRIMARY);
                }

                List<String> difference = new ArrayList<>(newTeacherIds);
                difference.removeAll(oldTeacherIds);

                if (ArrayUtils.isNotEmpty(difference)){
                    for(String teacherId:difference){
                        teacherIdSectionNameMap.put(teacherId,oldSection.getSectionName());
                        TeacherType teacherType = teacherId.equals(newPrimaryTeacherId) ? TeacherType.PRIMARY : TeacherType.SECONDARY;
                        teacherIdTypeMap.put(teacherId,teacherType);
                    }
                }

                oldSection.setTeacherIds(newTeacherIds);
                sectionDAO.save(oldSection);
                if(increaseBy != 0)
                    sectionDAO.incrementVacantSeatsForSections(Arrays.asList(oldSection.getId()),increaseBy);
            }
        }

        logger.info("sectionsToBeCreated {}", sectionsToBeCreated);
        logger.info("sectionToBeMarkedDeleted {}",sectionToBeMarkedDeleted);

        // ---------------------- create sections if any --------------------------
        if(!sectionsToBeCreated.isEmpty()){
            AddSectionReq addSectionReq = new AddSectionReq();
            addSectionReq.setBatchId(batchId);
            List<SectionPojo> payload = new ArrayList<>(sectionsToBeCreated);
            addSectionReq.setSectionList(payload);
            createSections(addSectionReq);
        }
        // ---------------------- end ---------------------------------------------


        // ---------------------- mark sections state to delete if any ------------
        if(!sectionToBeMarkedDeleted.isEmpty()){
            UpdateSectionsReq deleteSectionReq = new UpdateSectionsReq();
            List<SectionInfo> payloadNew = new ArrayList<>();
            for(Section s : sectionToBeMarkedDeleted)
                payloadNew.add(s.toSectionInfo(s,null,0));
            deleteSectionReq.setBatchId(batchId);
            deleteSectionReq.setSectionList(payloadNew);
            markSectionsDeleted(deleteSectionReq);
        }
        // --------------------- end ------------------------------------------------

        // send emails to new teachers if any
        if(!teacherIdSectionNameMap.isEmpty())
            sendEmailToSectionTeachers(teacherIdSectionNameMap,teacherIdTypeMap,batchId);

    }

    public List<SectionInfo> getSectionsForBatchId(String batchId, Long callingUserId) throws BadRequestException {
        // corner cases
        if(StringUtils.isEmpty(batchId))
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"batchId is empty");

        if(StringUtils.isEmpty(callingUserId.toString()))
            throw  new BadRequestException(ErrorCode.USER_NOT_FOUND,"userId is empty");

        List<SectionInfo> sectionInfos = new ArrayList<>();
        List<Section> sections = new ArrayList<>();
        List<Section> tempSections;

        // get sections from db
        Integer start = 0;
        Integer size = 20;
        Boolean keepProcessing = Boolean.TRUE;

        while(Boolean.TRUE.equals(keepProcessing)){
            tempSections = sectionDAO.getAllSectionsForBatchId(batchId,start,size,null);
            logger.info("tempSections {}",tempSections);
            if(ArrayUtils.isNotEmpty(tempSections)){
                sections.addAll(tempSections);
                start += tempSections.size();
            }
            else
                keepProcessing = Boolean.FALSE;
        }

        Batch batch = batchManager.getBatch(batchId);
        if(batch == null)
            return new ArrayList<>();
        batch.setHasSections(Boolean.FALSE);
        if(!sections.isEmpty())
            batch.setHasSections(Boolean.TRUE);
        batchDAO.save(batch);
        if(!batch.getHasSections())
            return new ArrayList<>();

        // get emailIds from teacherIds
        Map<String,List<String>> sectionIdTeacherEmailIdMap = getSectionIdTeacherEmailIdMap(sections);

        Map<String,Long> idEnrolledCountMap = getEnrolledCountForSections(batchId);
        logger.info("idEnrolledCountMap {}",idEnrolledCountMap);

        // populating to sectionInfos
        for(Section section: sections){
            long enrollCt = 0;

            if(!section.getEntityState().equals(EntityState.DELETED))
                enrollCt = idEnrolledCountMap.get(section.getId());

            sectionInfos.add(section.toSectionInfo(section,sectionIdTeacherEmailIdMap.get(section.getId()),enrollCt));
        }

        return sectionInfos;
    }

    public void markSectionsDeleted(UpdateSectionsReq updateSectionsReq) throws BadRequestException{
        // validate request
        updateSectionsReq.verify();

        // fetch sectionIds which needs to marked deleted
        List<String> sectionIdsToBeProcessed = new ArrayList<>();
        for(SectionInfo s : updateSectionsReq.getSectionList())
            sectionIdsToBeProcessed.add(s.getSectionId());

        // fetch original db sections
        List<Section> dbSections = new ArrayList<>();
        List<Section> temp;
        int skip = 0;
        int limit = 25;
        boolean processing = true;
        while(processing) {
            temp = sectionDAO.getSectionsForBatchId(updateSectionsReq.getBatchId(), skip, limit, null);
            if(temp.size() == 0)
                processing = false;
            else{
                skip += temp.size();
                dbSections.addAll(temp);
            }
        }
        if(Objects.isNull(dbSections))
            return;

        // fetch idEnrollmentCountMap for dbSections
        Map<String,Long> idEnrollmentCountForDbSections = getEnrolledCountForSections(updateSectionsReq.getBatchId());

        // idSection map for old db sections
        Map<String,Section> idSectionMap = new HashMap<>();
        dbSections.forEach(s -> idSectionMap.put(s.getId(),s));

        for(String id: sectionIdsToBeProcessed){
            if(!idSectionMap.containsKey(id))
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"No such section exists for this batch");

            // fetch current enrollment for this section and add check if it can be marked deleted
            long currentEnrollment = idEnrollmentCountForDbSections.get(id);
            if(currentEnrollment == 0) {
                idSectionMap.get(id).setEntityState(EntityState.DELETED);
                idSectionMap.get(id).setSeatsVacant(0);
                sectionDAO.save(idSectionMap.get(id));
            }
            else
                throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"Cannot delete sections with enrollments > 0");
        }

    }

    // for testing purpose, not used any where as of now
    public List<Section> getDeletedSectionsForBatchId(String batchId) {
        return sectionDAO.getDeletedSectionsForBatchId(batchId);
    }

    // called when sections are created and section's teacherEmailIds are updated
    public void sendEmailToSectionTeachers(Map<String,String> teacherIdSectionNameMap, Map<String,TeacherType> teacherIdTypeMap, String batchId){
        if(batchId == null || StringUtils.isEmpty(batchId) || teacherIdSectionNameMap == null || teacherIdSectionNameMap.isEmpty())
            return;
        SectionInfoList sectionInfoList = new SectionInfoList();
        sectionInfoList.setBatchId(batchId);
        sectionInfoList.setTeacherIdSectionNameMap(teacherIdSectionNameMap);
        sectionInfoList.setTeacherIdTypeMap(teacherIdTypeMap);
        awsSQSManager.sendToSQS(SQSQueue.SECTION_QUEUE,SQSMessageType.POST_SECTION_CREATION_EMAIL,gson.toJson(sectionInfoList));
    }

    // called when enrollment is created
    public boolean assignStudentToSection(CreateSectionEnrollmentReq createSectionEnrollmentReq) throws BadRequestException {
        String userId = createSectionEnrollmentReq.getUserId();
        String batchId = createSectionEnrollmentReq.getBatchId();
        String courseID = createSectionEnrollmentReq.getCourseId();
        Enrollment enrollment = createSectionEnrollmentReq.getEnrollment();
        boolean success = true;

        // mandatory validations
        if(Objects.isNull(enrollment))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"enrollment null while while assigning to section for enrollment");
        if(userId == null || userId.isEmpty())
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"userId null/empty while assigning to section for enrollment " + enrollment);
        if(batchId == null || batchId.isEmpty())
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"batchId null/empty while assigning to section for enrollment " + enrollment);

        EnrollmentState sectionState = enrollment.getState();

        if(Objects.nonNull(enrollment.getPurchaseContextType()) && enrollment.getPurchaseContextType().equals(EnrollmentPurchaseContext.BUNDLE)) {
            logger.info("purchaseEnrollmentId {}",enrollment.getPurchaseEnrollmentId());
            BundleEnrolment bundleEnrollment = bundleEnrolmentDAO.getBundleEnrollmentById(enrollment.getPurchaseEnrollmentId(),Arrays.asList(BundleEnrolment.Constants.STATE));
            logger.info("bundleEnrollment {}",bundleEnrollment);

            if(bundleEnrollment != null){
                logger.info("modifying enrollment state to bundle enrollment state {}",bundleEnrollment.getState());
                sectionState = bundleEnrollment.getState();
                if(sectionState.equals(EnrollmentState.FREE_PASS))
                    sectionState = EnrollmentState.TRIAL;
            }
            else
                throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,"bundleEnrollment null while assigning section for " + enrollment,Level.ERROR);
        }

        logger.info("enrollmentState {}",sectionState);
        if(sectionState == null || (!(sectionState.equals(EnrollmentState.TRIAL)) && !(sectionState.equals(EnrollmentState.REGULAR))))
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR,"enrollmentState regular for " + enrollment,Level.ERROR);

        Map<String,Long> idEnrollmentCountMap = new HashMap<>();
        List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes;

        sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(batchId,Arrays.asList(sectionState));
        for (SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes) {
            if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                idEnrollmentCountMap.put(x.getId(), x.getEnrollmentsCount());
        }

        logger.info("sectionEnrollmentAggregationRes {}",sectionEnrollmentAggregationRes.toString());
        logger.info("idEnrollmentCountMap {}",idEnrollmentCountMap);

        List<Section> sections = new ArrayList<>();
        List<Section> temp;
        int skip = 0;
        int limit = 25;
        boolean processing = true;
        while(processing) {
            temp = sectionDAO.getSectionsForBatchId(batchId, skip, limit, Arrays.asList(sectionState));
            if(temp.size() == 0)
                processing = false;
            else{
                sections.addAll(temp);
                skip += temp.size();
            }
        }
        logger.info("sections {}",sections);

        PriorityQueue<Pair<Long,String>> pQueue = new PriorityQueue<>(sections.size(), new The_Comparator());

        for(Section s : sections){

            String sId = s.getId();
            long size = s.getSize();
            long enrollmentsDone = 0;

            if(!idEnrollmentCountMap.isEmpty() && idEnrollmentCountMap.get(sId) != null)
                enrollmentsDone = idEnrollmentCountMap.get(sId);

            logger.info("size : " + size + "enrollmentsDone" + enrollmentsDone);
            if(size == enrollmentsDone || enrollmentsDone > size)
                continue;

            Pair<Long,String> pair = new Pair<>(size-enrollmentsDone,sId);
            logger.info("pair {}",pair);
            pQueue.add(pair);
        }

        Pair<Long,String> sectionToBeAssigned = pQueue.peek();
        logger.info("sectionToBeAssigned {}",sectionToBeAssigned);

        long seatsRemainingWithoutBuffer = getSeatsRemainingForBatch(batchId,false,Arrays.asList(sectionState));
        logger.info("seatsRemainingWithoutBufferVK {}",seatsRemainingWithoutBuffer);

        if(Objects.isNull(sectionToBeAssigned) || sectionToBeAssigned.getVal() == null) {
            logger.warn("Section allotment error for batch " + batchId + " userId " + userId + " sectionToBeAssigned " + sectionToBeAssigned + " for enrollmentId " + enrollment.getId() + " with seats " + seatsRemainingWithoutBuffer);
            success = false;
        }

        if(Objects.nonNull(sectionToBeAssigned) && sectionToBeAssigned.getVal() != null && sectionToBeAssigned.getVal().isEmpty()) {
            logger.warn("sectionId empty while assigning section to enrollment with id " + enrollment.getId() + " for user " + userId);
            success = false;
        }

        if(seatsRemainingWithoutBuffer <= 0){
            logger.info("Enrollment triggered with no seats available for user with enrollmentId " + enrollment.getId());
            success = false;
        }

        if(success){
            try {
                enrollment.setSectionId(sectionToBeAssigned.getVal());
                enrollment.setSectionState(sectionState);
                enrollmentDAO.save(enrollment);
                /*
                * decrement when the field is true or not there
                * */
                if(Objects.isNull(createSectionEnrollmentReq.getDoDecrement()) || createSectionEnrollmentReq.getDoDecrement().equals(Boolean.TRUE))
                    sectionDAO.incrementVacantSeatsForSections(Arrays.asList(enrollment.getSectionId()),-1);
            }
            catch(Exception e){
                logger.warn("ERROR while saving enrollment, message =  " + e.getMessage());
            }
        }
        else {
            // exit from func
            return false;
        }

        // email alerts for seats filling of section
        sendSectionFillingAlerts(batchId);
        return true;
    }

    public void changeSectionEnrollmentToRegular(UpdateSectionEnrollmentReq updateSectionEnrollmentReq) throws BadRequestException {
        String batchId = updateSectionEnrollmentReq.getBatchId();
        String userId = updateSectionEnrollmentReq.getUserId();
        Enrollment enrollment = updateSectionEnrollmentReq.getEnrollment();
        String courseID = updateSectionEnrollmentReq.getCourseId();
        boolean success = true;

        // mandatory validations
        if(Objects.isNull(enrollment))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"enrollment null while while assigning to section for enrollment");
        if(userId == null || userId.isEmpty())
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"userId null/empty while assigning to section for enrollment " + enrollment);
        if(batchId == null || batchId.isEmpty())
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"batchId null/empty while assigning to section for enrollment " + enrollment);

        EnrollmentState sectionState = enrollment.getState();
        logger.info("enrollmentState {}",sectionState);

        if(!sectionState.equals(EnrollmentState.REGULAR)) return;

        List<Section> paidSections = new ArrayList<>();
        List<Section> temp;
        int skip = 0;
        int limit = 25;
        boolean processing = true;
        while(processing) {
            temp = sectionDAO.getSectionsForBatchId(batchId,skip,limit,Arrays.asList(EnrollmentState.REGULAR));
            if(temp.size() == 0)
                processing = false;
            else {
                paidSections.addAll(temp);
                skip += temp.size();
            }
        }

        logger.info("paidSections {}",paidSections);

        if(paidSections.isEmpty()) return;

        List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(batchId,Arrays.asList(EnrollmentState.REGULAR));
        logger.info("idEnrollCountAggregationRes {}",sectionEnrollmentAggregationRes);

        Map<String,Long> idEnrollmentCountMap = new HashMap<>();
        for(SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes){
            if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                idEnrollmentCountMap.put(x.getId(), x.getEnrollmentsCount());
        }

        PriorityQueue<Pair<Long,String>> pQ = new PriorityQueue<>(paidSections.size(), new The_Comparator());

        for(Section s : paidSections){

            String sId = s.getId();
            long size = s.getSize();
            long enrollmentsDone = 0;

            if(!idEnrollmentCountMap.isEmpty() && idEnrollmentCountMap.get(sId) != null)
                enrollmentsDone = idEnrollmentCountMap.get(sId);

            logger.info("size : " + size + "enrollmentsDone: " + enrollmentsDone);
            if(size == enrollmentsDone || enrollmentsDone > size)
                continue;

            Pair<Long,String> pair = new Pair<>(size-enrollmentsDone,sId);
            logger.info("pair {}",pair);
            pQ.add(pair);
        }

        Pair<Long,String> sectionToBeAssigned = pQ.peek();
        logger.info("sectionToBeAssigned {}",sectionToBeAssigned);

        long paidSeatsRemainingWithoutBuffer = getSeatsRemainingForBatch(batchId,false,Arrays.asList(EnrollmentState.REGULAR));


        if(Objects.isNull(sectionToBeAssigned) || sectionToBeAssigned.getVal() == null) {
            logger.warn("Section allotment error for batch " + batchId + " userId " + userId + " sectionToBeAssigned " + sectionToBeAssigned + " for enrollmentId " + enrollment.getId() + " with seats " + paidSeatsRemainingWithoutBuffer);
            success = false;
        }

        if(Objects.nonNull(sectionToBeAssigned) && sectionToBeAssigned.getVal() != null && sectionToBeAssigned.getVal().isEmpty()) {
            logger.warn("sectionId empty while assigning section to enrollment with id " + enrollment.getId() + " for user " + userId);
            success = false;
        }

        if(paidSeatsRemainingWithoutBuffer <= 0){
            logger.warn("Enrollment triggered with no seats available for user with enrollmentId " + enrollment.getId());
            success = false;
        }

        if(success) {
            adjustSeatsVacancyForSections(enrollment.getSectionId(),sectionToBeAssigned.getVal());
            enrollment.setSectionId(sectionToBeAssigned.getVal());
            enrollment.setSectionState(EnrollmentState.REGULAR);
            enrollmentDAO.save(enrollment);

            // email alerts for seats filling of section
            sendSectionFillingAlerts(batchId);
        }
    }

    public Map<String,Long> getEnrolledCountForSections(String batchId){

        Map<String,Long> idEnrollmentCountMap = new HashMap<>();
        List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes;
        sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(batchId,Arrays.asList(EnrollmentState.REGULAR,EnrollmentState.TRIAL));

        logger.info("sectionEnrollmentAggregationRes {}",sectionEnrollmentAggregationRes.toString());

        for (SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes) {
            if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                idEnrollmentCountMap.put(x.getId(), x.getEnrollmentsCount());
        }

        logger.info("idEnrollmentCountMap  {}",idEnrollmentCountMap);
        List<Section> sections = new ArrayList<>();
        List<Section> temp;
        int skip = 0;
        int limit = 25;
        boolean processing = true;
        while(processing) {
            temp = sectionDAO.getSectionsForBatchId(batchId,skip,limit,null);
            if(temp.size() == 0)
                processing = false;
            else{
                sections.addAll(temp);
                skip += temp.size();
            }
        }
        logger.info("sections {}",sections);

        for(Section s : sections){
            String sId = s.getId();
            long size = s.getSize();
            long enrollmentsDone = 0;

            if(!idEnrollmentCountMap.isEmpty() && idEnrollmentCountMap.get(sId) != null)
                enrollmentsDone = idEnrollmentCountMap.get(sId);

            logger.info("size : " + size + "enrollmentsDone" + enrollmentsDone);
            idEnrollmentCountMap.put(sId,enrollmentsDone);
        }
        return idEnrollmentCountMap;
    }

    public Long getSeatsRemainingForBatch(String batchId, Boolean withBuffer, List<EnrollmentState> enrollmentStates) throws BadRequestException {
        if(StringUtils.isEmpty(batchId))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"batchId is empty");

        long totalSeatsRemainingWithoutBuffer = 0L;
        long totalSeatsRemainingWithBuffer = 0L;

        List<Section> sections = new ArrayList<>();
        List<Section> temp;
        int skip = 0;
        int limit = 25;
        boolean processing = true;
        while(processing){
            temp = sectionDAO.getSectionsForBatchId(batchId,skip,limit,enrollmentStates);
            if(temp.size() == 0)
                processing = false;
            else{
                skip += temp.size();
                sections.addAll(temp);
            }
        }
        logger.info("sections {}",sections);

        List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes;
        sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(batchId,enrollmentStates);
        logger.info("sectionEnrollmentAggregationRes {}",sectionEnrollmentAggregationRes);

        Map<String,Long> idEnrollCountMap = new HashMap<>();
        for (SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes) {
            if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                idEnrollCountMap.put(x.getId(), x.getEnrollmentsCount());
        }
        logger.info("idEnrollCountMap {}",idEnrollCountMap);

        for(Section s:sections){
           long size = s.getSize();
           long enrollCount = 0;
           long offset = (long)Math.ceil(size * Section.Constants.SECTION_BUFFER_PERCENT);
           logger.info("offset {}",offset);
           long buffer = size >= Section.Constants.SECTION_OFFSET_FOR_BUFFER ? Section.Constants.SECTION_BUFFER : offset;
           logger.info("buffer {}",buffer);

           if(!idEnrollCountMap.isEmpty() && idEnrollCountMap.get(s.getId()) != null)
            enrollCount = idEnrollCountMap.get(s.getId());

           totalSeatsRemainingWithoutBuffer += (size - enrollCount);
           totalSeatsRemainingWithBuffer += ((size-buffer) - enrollCount);
        }

        logger.info("Seats remaining for this batch without buffer : " + totalSeatsRemainingWithoutBuffer);
        logger.info("Seats remaining for this batch with buffer : " + totalSeatsRemainingWithBuffer);

        if(withBuffer)
            return totalSeatsRemainingWithBuffer;
        return totalSeatsRemainingWithoutBuffer;
    }

    public List<StudentSectionMappingRes> getStudentSectionMappings(String batchId, String userId) throws BadRequestException {

        List<StudentSectionMappingRes> response =  new ArrayList<>();
        if(batchId != null)
            batchValidation(batchId);
        if(userId != null)
            userValidation(userId);
        // corner cases
        if(StringUtils.isEmpty(batchId) || StringUtils.isEmpty(userId))
            return response;

        List<Enrollment> studentEnrollments = new ArrayList<>();
        boolean processing = true;
        int start = 0;
        int limit = 250;
        while (processing){
            List<Enrollment> temp = enrollmentDAO.getSectionEnrollmentsForUserIdBatchId(batchId,userId,start,limit);
            if(temp.size() == 0)
                processing = false;
            else {
                start += temp.size();
                studentEnrollments.addAll(temp);
            }
        }
        logger.info("studentEnrollments {}",studentEnrollments);
        if(studentEnrollments.isEmpty()) return response;

        Set<String> sectionIds = new HashSet<>();
        for (Enrollment studentEnrollment : studentEnrollments) {
            String sectionId = studentEnrollment.getSectionId();
            if(sectionId != null && !StringUtils.isEmpty(sectionId))
                sectionIds.add(sectionId);
        }
        logger.info("sectionIds {}",sectionIds);

        if(!sectionIds.isEmpty()) {

            List<Section> sections = sectionDAO.getSectionsById(new ArrayList<>(sectionIds),null);
            logger.info("sections {}", sections);

            Map<String,Section> idSectionMap = sections.stream().collect(Collectors.toMap(AbstractMongoStringIdEntity::getId, s -> s, (a, b) -> b));
            logger.info("idSectionMap {}",idSectionMap);

            // bId, sId, sName, type, studentId, MT
            for(Enrollment e : studentEnrollments){
                logger.info("in-loop");
                StudentSectionMappingRes s = new StudentSectionMappingRes();
                String sId = e.getSectionId();
                logger.info("sId {}",sId);
                if(sId != null && !sId.isEmpty() && idSectionMap.get(sId) != null){
                    Section section = idSectionMap.get(sId);

                    if(!(e.getSectionState().equals(section.getSectionType())))
                        throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"sectionState for enrollment and section different",Level.ERROR);

                    s.setBatchId(section.getBatchId());
                    s.setSectionId(sId);
                    s.setSectionName(section.getSectionName());
                    s.setSectionType(section.getSectionType());
                    s.setUserId(userId);

                    String mainTeacherId = section.getTeacherIds().get(0);
                    String emailId = fosUtils.getUserBasicInfo(mainTeacherId,true).getEmail();
                    s.setMainTeacherEmailId(emailId);

                    response.add(s);
                }
            }
        }

        return response;
    }

    public List<StudentSectionMappingRes> getTrialStudentSectionMappings(String batchId, String sectionId) throws BadRequestException {
        List<StudentSectionMappingRes> response = new ArrayList<>();

        // corner cases
        if((Objects.isNull(batchId) && Objects.isNull(sectionId)))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "batchId and sectionId both can't be null at the same time");
        if (StringUtils.isEmpty(batchId) && StringUtils.isEmpty(sectionId))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"both batchId and sectionId are empty");
        if(batchId != null)
            batchValidation(batchId);
        if(sectionId != null)
            sectionValidation(sectionId);

        String sId,bId;
        sId = bId = null;

        if(Objects.nonNull(batchId) && !StringUtils.isEmpty(batchId)){
            if(Objects.nonNull(sectionId) && !StringUtils.isEmpty(sectionId))
                sId = sectionId;
            bId = batchId;
        }else
            sId = sectionId;

        List<Enrollment> trialStudentEnrollments = new ArrayList<>();
        boolean processing = true;
        int start = 0;
        int limit = 1000;
        int hardStopCount = 0;
        while (processing){
            List<Enrollment> temp = enrollmentDAO.getSectionEnrollmentsForBatchIdSectionId(bId,sId,Arrays.asList(EnrollmentState.TRIAL),start,limit);
            logger.info("temp.size() {}",temp.size());
            hardStopCount++;
            if(temp.size() == 0 || hardStopCount >= 5)
                processing = false;
            else {
                start += temp.size();
                trialStudentEnrollments.addAll(temp);
            }
        }
        logger.info("trialStudentEnrollments size {}",trialStudentEnrollments.size());

        if(trialStudentEnrollments.isEmpty())
            return response;

        Set<String> sectionIds = new HashSet<>();
        for(Enrollment e:trialStudentEnrollments) {
            logger.info("eId {}",e.getSectionId());
            if(e.getSectionId() != null && (!StringUtils.isEmpty(e.getSectionId())) && e.getSectionState().equals(EnrollmentState.TRIAL))
                sectionIds.add(e.getSectionId());
        }

        logger.info("sectionIds {}",sectionIds);

        if(!sectionIds.isEmpty()) {

            List<Section> trialSections = sectionDAO.getSectionsById(new ArrayList<>(sectionIds),null);
            logger.info("trialSections {}", trialSections);

            Map<String,Section> idSectionMap = new HashMap<>();
            for(Section s : trialSections){
                idSectionMap.put(s.getId(),s);
            }
            logger.info("idSectionMap {}",idSectionMap);

            // bId, sId, sName, type, studentId
            for(Enrollment e : trialStudentEnrollments){
                logger.info("in-loop");
                String s_Id = e.getSectionId();
                logger.info("sId : {}",s_Id);
                if(s_Id != null && !(s_Id.isEmpty()) && idSectionMap.get(s_Id) != null) {

                    StudentSectionMappingRes mapping = new StudentSectionMappingRes();
                    Section s = idSectionMap.get(s_Id);

                    if(!(e.getSectionState().equals(s.getSectionType())))
                        throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"sectionState for enrollment and section different",Level.ERROR);

                    mapping.setBatchId(s.getBatchId());
                    mapping.setSectionId(s.getId());
                    mapping.setSectionName(s.getSectionName());
                    mapping.setSectionType(s.getSectionType());
                    mapping.setUserId(e.getUserId());

                    response.add(mapping);
                }
            }

        }
        return response;

    }

    public List<StudentSectionMappingRes> getPaidStudentSectionMappings(String batchId, String sectionId) throws BadRequestException {
        List<StudentSectionMappingRes> response = new ArrayList<>();

        // corner cases
        if( (Objects.isNull(batchId) && Objects.isNull(sectionId)))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"batchId and sectionId both are null");
        if (StringUtils.isEmpty(batchId) && StringUtils.isEmpty(sectionId))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"both batchId and sectionId are empty");
        if(batchId != null)
            batchValidation(batchId);
        if(sectionId != null)
            sectionValidation(sectionId);

        String sId = null , bId = null;

        if(Objects.nonNull(batchId) && !StringUtils.isEmpty(batchId)){
            if(Objects.nonNull(sectionId) && !StringUtils.isEmpty(sectionId))
                sId = sectionId;
            bId = batchId;
        }else
            sId = sectionId;

        List<Enrollment> studentEnrollments = new ArrayList<>();
        boolean processing = true;
        int start = 0;
        int limit = 250;
        while (processing){
            List<Enrollment> temp = enrollmentDAO.getSectionEnrollmentsForBatchIdSectionId(bId,sId,Arrays.asList(EnrollmentState.REGULAR),start,limit);
            logger.info(" temp.size() {}",temp.size());
            if(temp.size() == 0)
                processing = false;
            else {
                start += temp.size();
                studentEnrollments.addAll(temp);
            }
        }
        //studentEnrollments = enrollmentDAO.getSectionEnrollmentsForBatchIdSectionId(bId,sId,Arrays.asList(EnrollmentState.REGULAR),0,2000);
        logger.info("studentEnrollments {}",studentEnrollments);

        if(studentEnrollments.isEmpty())
            return response;

        Set<String> sectionIds = new HashSet<>();
        for(Enrollment e:studentEnrollments) {
            logger.info("eID {}",e.getSectionId());
            if(e.getSectionId() != null && (!StringUtils.isEmpty(e.getSectionId())) && e.getSectionState().equals(EnrollmentState.REGULAR))
                sectionIds.add(e.getSectionId());
        }

        logger.info("sectionIds {}",sectionIds);

        if(!sectionIds.isEmpty()) {

            List<Section> sections = sectionDAO.getSectionsById(new ArrayList<>(sectionIds),null);
            logger.info("sections {}", sections);

            Map<String,Section> idSectionMap = new HashMap<>();
            for(Section s : sections){
                idSectionMap.put(s.getId(),s);
            }
            logger.info("idSectionMap {}",idSectionMap);

            // bId, sId, sName, type, studentId
            for(Enrollment enrollment : studentEnrollments){
                logger.info("in-loop");
                String s_Id = enrollment.getSectionId();
                logger.info("sId {}",s_Id);
                if(s_Id != null && !s_Id.isEmpty() && idSectionMap.get(s_Id) != null) {
                    StudentSectionMappingRes studentSectionMappingRes = new StudentSectionMappingRes();
                    Section s = idSectionMap.get(s_Id);

                    if(!(enrollment.getSectionState().equals(s.getSectionType())))
                        throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"sectionState for enrollment and section different",Level.ERROR);

                    studentSectionMappingRes.setBatchId(s.getBatchId());
                    studentSectionMappingRes.setSectionId(s.getId());
                    studentSectionMappingRes.setSectionName(s.getSectionName());
                    studentSectionMappingRes.setSectionType(s.getSectionType());
                    studentSectionMappingRes.setUserId(enrollment.getUserId());

                    response.add(studentSectionMappingRes);
                }
            }

        }
        return response;
    }

    public PlatformBasicResponse updateSectionsFromSMI(UpdateSectionsFromSMIReq updateSectionsFromSMIReq) throws VException {

        if(Objects.isNull(updateSectionsFromSMIReq.getReqList()) || updateSectionsFromSMIReq.getReqList().isEmpty())
            throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS,"sections are empty/null");
        if(Objects.isNull(updateSectionsFromSMIReq.getPrimaryBatchId()) || updateSectionsFromSMIReq.getPrimaryBatchId().isEmpty())
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"primary batchId is empty/null");

        updateSectionsFromSMIReq.verify();
        List<StudentSectionMappingRes> toEditMappingsList = updateSectionsFromSMIReq.getReqList();
        String primaryBatchId = updateSectionsFromSMIReq.getPrimaryBatchId();
        batchValidation(primaryBatchId);
//        sessionValidation(primaryBatchId);

        Set<EnrollmentState> actionType = toEditMappingsList.stream().map(StudentSectionMappingRes::getSectionType).collect(Collectors.toSet());
        logger.info("actionType {}",actionType);
        if(actionType.size() > 1)
            throw new BadRequestException(ErrorCode.BOTH_TRIAL_AND_REGULAR_UPDATE_NOT_POSSIBLE_SIMULTANEOUSLY,"Cannot update TRIAL and REGULAR enrollments at the same time!");

        // ---------------------------------------------- SECTION RELATED CHECKS START----------------------------------
        Map<String,Long> idIncrementCountMap = new HashMap<>();
        Set<String> sectionIds = new HashSet<>();
        for(StudentSectionMappingRes mapping : toEditMappingsList){
            if(Objects.nonNull(mapping.getSectionId()) && !mapping.getSectionId().isEmpty())
                sectionIds.add(mapping.getSectionId());
        }
        List<Section> sections = sectionDAO.getSectionsById(new ArrayList<>(sectionIds),null);

        logger.info("sectionIds {}",sectionIds);
        logger.info("sections {}",sections);

        if(sections.size() < sectionIds.size())
            throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS,"Some sections do not exist for some of the sectionIds");

        Map<String,Section> idSectionMap = new HashMap<>();
        for(Section s : sections){
            if(s != null && s.getId() != null && !s.getId().isEmpty()) {
                if(s.getBatchId().equals(primaryBatchId) && s.getSectionType().equals(actionType.iterator().next())) {
                    idSectionMap.put(s.getId(), s);
                    idIncrementCountMap.put(s.getId(), 0L);
                }
                else throw new BadRequestException(ErrorCode.SECTION_NOT_IN_THIS_BATCH,"Provided sectionId and sectionName are of different batch || Trying redistribution for " + s.getSectionType() + " in case of " + actionType.iterator().next() + " update way!");
            }
        }
        logger.info("idSectionMap {}",idSectionMap);
        // ---------------------------------------------- SECTION RELATED CHECKS END -----------------------------------

        // --------------------------------------------- ENROLLMENTS RELATED CHECK START -------------------------------

        // enrollments which will be updated
        List<Enrollment> toUpdateEnrollments = new ArrayList<>();
        Set<String> studentIds = new HashSet<>();

        for (StudentSectionMappingRes studentSectionMappingRes : toEditMappingsList){
            String userId = studentSectionMappingRes.getUserId();
            if(userId != null && !userId.isEmpty())
                studentIds.add(userId);
        }
        logger.info("studentIds {}",studentIds);

        if(studentIds.isEmpty())
            throw new BadRequestException(ErrorCode.STUDENTIDS_EMPTY,"studentIds empty");
        if(studentIds.size() < toEditMappingsList.size())
            throw new BadRequestException(ErrorCode.DUPLICATE_STUDENTIDS,"Some studentIds are duplicate");

        List<Enrollment> enrollments = new ArrayList<>();
        boolean processing = true;
        int start = 0;
        int limit = 250;
        while(processing){
            List<Enrollment> temp = enrollmentDAO.getSectionEnrollmentsForStudentIds(primaryBatchId,new ArrayList<>(studentIds),null,start,limit);
            if(temp.size() == 0)
                processing = false;
            else {
                start += temp.size();
                enrollments.addAll(temp);
            }
        }

        if(enrollments.size() < studentIds.size())
            throw new BadRequestException(ErrorCode.ENROLLMENT_NOT_FOUND_FOR_USERID,"Some userIds do not have enrollments into any section for this batch");

        Map<String, Enrollment> userIdEnrollmentMap = new HashMap<>();
        for (Enrollment enrollment : enrollments) {
            if(enrollment != null && enrollment.getUserId() != null && !enrollment.getUserId().isEmpty())
                userIdEnrollmentMap.put(enrollment.getUserId(), enrollment);
        }
        logger.info("enrollments {}",enrollments);
        logger.info("userIdEnrollmentMap {}",userIdEnrollmentMap);
        // --------------------------------------------- ENROLLMENTS RELATED CHECK END ---------------------------------


        for(StudentSectionMappingRes mapping : toEditMappingsList){

            logger.info("mapping {}",mapping);

            String batchId = mapping.getBatchId();
            String sectionId = mapping.getSectionId();
            String sectionName = mapping.getSectionName();
            EnrollmentState sectionType = mapping.getSectionType();
            String studentId = mapping.getUserId();

            if(Objects.isNull(sectionId) || sectionId.isEmpty())
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"cannot create new sections");

            if(Objects.isNull(batchId)|| batchId.isEmpty())
                throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"batchId is null/empty");

            if(!batchId.equals(primaryBatchId))
                throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"primaryBatchId and batchId not mapping " + batchId);

            if(Objects.isNull(sectionType) || sectionType.toString().isEmpty())
                throw new BadRequestException(ErrorCode.SECTION_TYPE_EMPTY,"sectionType is null/empty");

            if(Objects.isNull(studentId) || studentId.isEmpty())
                throw new BadRequestException(ErrorCode.USERID_IS_EMPTY,"userId is null/empty");

            if((!sectionType.equals(EnrollmentState.TRIAL) && !sectionType.equals(EnrollmentState.REGULAR)))
                throw new BadRequestException(ErrorCode.INVALID_SECTION_TYPE, sectionType + " as section type is not allowed/invalid");

            if(!sectionType.equals(actionType.iterator().next()))
                throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "sectionType and actionType vary");

            if (org.springframework.util.StringUtils.isEmpty(sectionName) || !(com.vedantu.util.StringUtils.isAlphaNumeric(sectionName)))
                throw new BadRequestException(ErrorCode.SECTION_NAME_INCORRECT, "Section name cannot be empty and should be alphanumeric with all lowercase letters without spaces");

            // valid user check
            UserBasicInfo student = fosUtils.getUserBasicInfo(studentId,false);
            if(Objects.isNull(student))
                throw new BadRequestException(ErrorCode.USER_NOT_FOUND,"No such student exists");

            Role role = student.getRole();
            if(Objects.isNull(role))
                throw new BadRequestException(ErrorCode.INVALID_ROLE,"No role exists for this user");

            if(!role.equals(Role.STUDENT))
                throw new BadRequestException(ErrorCode.USER_SHOULD_BE_STUDENT,"User is not student");

            Section s = idSectionMap.get(sectionId);
            logger.info("section -> {}",s);

            if(Objects.isNull(s))
                throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS,"No such section exists");

            // sectionId and sectionName one to one mapping should exist
            if(!s.getSectionName().equals(sectionName))
                throw new BadRequestException(ErrorCode.SECTION_ID_SECTION_NAME_MAPPING_INCORRECT,"sectionId - sectionName mapping incorrect");

            Enrollment e = userIdEnrollmentMap.get(studentId);
            logger.info("enrollment {}",e);

            if(e == null)
                throw new BadRequestException(ErrorCode.ENROLLMENT_NOT_FOUND_FOR_USERID,"No enrollments exist for this user : " + studentId);

            if(!sectionType.equals(e.getSectionState()))
                throw new BadRequestException(ErrorCode.SECTION_TYPE_ENROLLMENT_STATE_MISS_MATCH,"Student enrollment type and Section type different");

            String oldSectionId = e.getSectionId();

            if(Objects.isNull(oldSectionId) || oldSectionId.isEmpty())
                throw new BadRequestException(ErrorCode.NO_SECTION_ASSIGNED_TO_STUDENT,"No section allotment for this enrollment " + e.toString(),Level.ERROR);

            // -- Redistributing section enrollment if any
            // enrollmentState and section state should be the same
            if(!e.getSectionState().equals(sectionType))
                throw new BadRequestException(ErrorCode.TRIAL_TO_REGULAR_NOT_ALLOWED_HERE,"Cannot change section enrollment from TRIAL to REGULAR or vice versa");

            if(!sectionId.equals(oldSectionId)) {
                SectionChangeTime sectionChangeTime = new SectionChangeTime();
                sectionChangeTime.setChangedBy(updateSectionsFromSMIReq.getCallingUserId());
                sectionChangeTime.setChangeTime(System.currentTimeMillis());
                sectionChangeTime.setPreviousSectionId(oldSectionId);
                sectionChangeTime.setNewSectionId(sectionId);
                if(ArrayUtils.isEmpty(e.getSectionChangeTime())){
                    e.setSectionChangeTime(new ArrayList<>());
                }
                e.getSectionChangeTime().add(sectionChangeTime);
                e.setSectionId(sectionId);
                adjustSeatsVacancyForSections(oldSectionId,sectionId);
                toUpdateEnrollments.add(e);
                Long count = idIncrementCountMap.get(sectionId);
                idIncrementCountMap.put(sectionId,++count);
            }
        }

        logger.info("toUpdateEnrollments {}",toUpdateEnrollments);
        if(!toUpdateEnrollments.isEmpty()){
            List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(primaryBatchId,Arrays.asList(actionType.iterator().next()));
            logger.info("idEnrollCountAggregationRes {}",sectionEnrollmentAggregationRes);

            Map<String,Long> idEnrollmentCountMap = new HashMap<>();
            for(SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes){
                if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                    idEnrollmentCountMap.put(x.getId(), x.getEnrollmentsCount());
            }
            logger.info("idEnrollmentCountMap {}",idEnrollmentCountMap);

            for(Enrollment e:toUpdateEnrollments){
                String sectionId = e.getSectionId();
                long size = idSectionMap.get(sectionId).getSize();
                long currentCount = 0;
                long increasedCount = idIncrementCountMap.get(sectionId);

                if(idEnrollmentCountMap.get(sectionId) != null)
                    currentCount = idEnrollmentCountMap.get(sectionId);
                logger.info("sectionId {} , size {}, currentCount {}, increasedCount {}",sectionId,size,currentCount,increasedCount);

                if(increasedCount+currentCount > size)
                    throw new BadRequestException(ErrorCode.REDISTRIBUTION_NOT_POSSIBLE_AS_EXCEEDING_SECTION_SIZE,"Cannot redistribute as size exceeding for section " + sectionId);
                enrollmentDAO.save(e);
            }
        }

        // email alerts for seats filling of section
        sendSectionFillingAlerts(primaryBatchId);
        return new PlatformBasicResponse(true,"Success","");
    }

    public PlatformBasicResponse sectionBulkUpdate(UpdateSectionsFromSMIReq updateSectionsFromSMIReq) throws VException,BadRequestException {

        if(Objects.isNull(updateSectionsFromSMIReq.getReqList()) || updateSectionsFromSMIReq.getReqList().isEmpty())
            throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS,"sections are empty/null");

        if(Objects.isNull(updateSectionsFromSMIReq.getPrimaryBatchId()) || updateSectionsFromSMIReq.getPrimaryBatchId().isEmpty())
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"primary batchId is empty/null");

        updateSectionsFromSMIReq.verify();
        List<StudentSectionMappingRes> toValidateMappingsList = updateSectionsFromSMIReq.getReqList();

        String primaryBatchId = updateSectionsFromSMIReq.getPrimaryBatchId();
        batchValidation(primaryBatchId);
//        sessionValidation(primaryBatchId);

        Set<EnrollmentState> actionType = toValidateMappingsList.stream().map(StudentSectionMappingRes::getSectionType).collect(Collectors.toSet());
        logger.info("actionType {}",actionType);
        if(actionType.size() > 1)
            throw new BadRequestException(ErrorCode.BOTH_TRIAL_AND_REGULAR_UPDATE_NOT_POSSIBLE_SIMULTANEOUSLY,"Cannot update TRIAL and REGULAR enrollments at the same time!");

        // ---------------------------------------------- SECTION RELATED CHECKS START----------------------------------
        Map<String,Long> idIncrementCountMap = new HashMap<>();
        Set<String> sectionIds = new HashSet<>();
        for(StudentSectionMappingRes mapping : toValidateMappingsList){
            if(Objects.nonNull(mapping.getSectionId()) && !mapping.getSectionId().isEmpty())
                sectionIds.add(mapping.getSectionId());
        }
        logger.info("sectionIds {}",sectionIds);

        List<Section> sections = sectionDAO.getSectionsById(new ArrayList<>(sectionIds),null);
        logger.info("sections {}",sections);
        if(sections.size() < sectionIds.size())
            throw new BadRequestException(ErrorCode.INVALID_SECTIONID,"Some sections do not exist for some of the sectionIds");

        Map<String,Section> idSectionMap = new HashMap<>();
        for(Section s : sections){
            if(s != null && s.getId() != null && !s.getId().isEmpty()) {
                if(s.getBatchId().equals(primaryBatchId) && s.getSectionType().equals(actionType.iterator().next())) {
                    idSectionMap.put(s.getId(), s);
                    idIncrementCountMap.put(s.getId(), 0L);
                }
                else throw new BadRequestException(ErrorCode.SECTION_NOT_IN_THIS_BATCH,"Provided sectionId and sectionName are of different batch || Trying redistribution for " + s.getSectionType() + " in case of " + actionType.iterator().next() + " update way!");
            }
        }
        logger.info("idSectionMap {}",idSectionMap);
        // ---------------------------------------------- SECTION RELATED CHECKS END -----------------------------------


        // --------------------------------------------- ENROLLMENTS RELATED CHECK START -------------------------------
        Set<String> studentIds = new HashSet<>();
        for (StudentSectionMappingRes studentSectionMappingRes : toValidateMappingsList){
            String userId = studentSectionMappingRes.getUserId();
            if(userId != null && !userId.isEmpty())
                studentIds.add(userId);
        }

        logger.info("studentIds {}",studentIds);
        if(studentIds.isEmpty())
            throw new BadRequestException(ErrorCode.STUDENTIDS_EMPTY,"studentIds are empty");

        if(studentIds.size() < toValidateMappingsList.size())
            throw new BadRequestException(ErrorCode.DUPLICATE_STUDENTIDS,"Some studentIds are duplicate");

        List<Enrollment> enrollments = new ArrayList<>();
        boolean processing = true;
        int start = 0;
        int lim = 250;
        while(processing){
            List<Enrollment> temp = enrollmentDAO.getSectionEnrollmentsForStudentIds(primaryBatchId,new ArrayList<>(studentIds),null,start,lim);
            if(temp.size() == 0)
                processing = false;
            else {
                start += temp.size();
                enrollments.addAll(temp);
            }
        }

        logger.info("enrollments {}",enrollments);
        if(enrollments.size() < toValidateMappingsList.size())
            throw new BadRequestException(ErrorCode.ENROLLMENT_NOT_FOUND_FOR_USERID,"Some userIds do not have enrollments into any section for this batch");

        if(enrollments.size() > toValidateMappingsList.size())
            throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"More than 1 enrollment for batchId+userId combo exists for some users",Level.ERROR);

        Map<String, Enrollment> userIdEnrollmentMap = new HashMap<>();
        for (Enrollment enrollment : enrollments) {
            if(enrollment != null && enrollment.getUserId() != null && !enrollment.getUserId().isEmpty())
            userIdEnrollmentMap.put(enrollment.getUserId(), enrollment);
        }
        logger.info("userIdEnrollmentMap {}",userIdEnrollmentMap);

        // --------------------------------------------- ENROLLMENTS RELATED CHECK END ---------------------------------

        // enrollments which will be updated
        List<Enrollment> toUpdateEnrollments = new ArrayList<>();

        // bId, sId, sName, sType, studentId, mainTeacherEmailId
        for(StudentSectionMappingRes mapping : toValidateMappingsList){
            String bId = mapping.getBatchId();
            String sId = mapping.getSectionId();
            String sName = mapping.getSectionName();
            EnrollmentState sType = mapping.getSectionType();
            String studentId = mapping.getUserId();
            String mainTeacherEmailId = mapping.getMainTeacherEmailId();

            // all fields validation
            if(Objects.isNull(sId) || sId.isEmpty())
                throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS,"cannot create new sections");

            if(Objects.isNull(bId)|| bId.isEmpty())
                throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"batchId is null/empty");

            if(!bId.equals(primaryBatchId))
                throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"batchId not found " + bId);

            // null check for sectionType
            if(Objects.isNull(sType) || sType.toString().isEmpty())
                throw new BadRequestException(ErrorCode.SECTION_TYPE_EMPTY,"sectionType is null/empty");

            if((!sType.equals(EnrollmentState.TRIAL) && !sType.equals(EnrollmentState.REGULAR)))
                throw new BadRequestException(ErrorCode.INVALID_SECTION_TYPE, sType + " section type is not allowed/invalid");

            if(!sType.equals(actionType.iterator().next()))
                throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR, "sectionType and actionType vary");

            // null check for studentId
            if(Objects.isNull(studentId) || studentId.isEmpty())
                throw new BadRequestException(ErrorCode.USERID_IS_EMPTY,"userId is null/empty");

            if (org.springframework.util.StringUtils.isEmpty(sName) || !(com.vedantu.util.StringUtils.isAlphaNumeric(sName)))
                throw new BadRequestException(ErrorCode.SECTION_NAME_INCORRECT, "Section name cannot be empty and should be alphanumeric with all lowercase letters without spaces");

            // valid user check
            UserBasicInfo student = fosUtils.getUserBasicInfo(studentId,false);
            if(Objects.isNull(student))
                throw new BadRequestException(ErrorCode.USER_NOT_FOUND,"No such student exists");

            Role role = student.getRole();
            if(Objects.isNull(role))
                throw new BadRequestException(ErrorCode.INVALID_ROLE,"No role exists for this user");

            if(!role.equals(Role.STUDENT))
                throw new BadRequestException(ErrorCode.USER_SHOULD_BE_STUDENT,"User is not student");

            Section s = idSectionMap.get(sId);
            logger.info("section -> {}",s);

            if(Objects.isNull(s))
                throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS,"No such section exists");

            // sectionId and sectionName one to one mapping should exist
            if(!s.getSectionName().equals(sName))
                throw new BadRequestException(ErrorCode.SECTION_ID_SECTION_NAME_MAPPING_INCORRECT,"sectionId - sectionName mapping is incorrect for the enrollment");

            Enrollment e = userIdEnrollmentMap.get(studentId);
            logger.info("student enrollment {}",e);

            if(Objects.isNull(e))
                throw new BadRequestException(ErrorCode.ENROLLMENT_NOT_FOUND_FOR_USERID,"No enrollments exist for this user : " + studentId);

            if(!sType.equals(e.getSectionState()))
                throw new BadRequestException(ErrorCode.SECTION_TYPE_ENROLLMENT_STATE_MISS_MATCH,"sectionType and enrollmentState different for user " + studentId);

            String oldSectionId = e.getSectionId();

            if(Objects.isNull(oldSectionId) || oldSectionId.isEmpty())
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"No section allotment for this enrollment " + e.toString(),Level.ERROR);

            // enrollmentState and newSection state should be the same
            if(!e.getSectionState().equals(sType))
                throw new BadRequestException(ErrorCode.FORBIDDEN_ERROR,"Cannot change section enrollment from TRIAL to REGULAR or vice versa");

            if(!sId.equals(oldSectionId)) {
                // update has been done
                SectionChangeTime sectionChangeTime = new SectionChangeTime();
                sectionChangeTime.setChangedBy(updateSectionsFromSMIReq.getCallingUserId());
                sectionChangeTime.setChangeTime(System.currentTimeMillis());
                sectionChangeTime.setPreviousSectionId(oldSectionId);
                sectionChangeTime.setNewSectionId(sId);
                if(ArrayUtils.isEmpty(e.getSectionChangeTime())){
                    e.setSectionChangeTime(new ArrayList<>());
                }
                e.getSectionChangeTime().add(sectionChangeTime);
                e.setSectionId(sId);
                adjustSeatsVacancyForSections(oldSectionId,sId);
                toUpdateEnrollments.add(e);
                Long count = idIncrementCountMap.get(sId);
                idIncrementCountMap.put(sId,++count);
            }
        }

        logger.info("toUpdateEnrollments {}",toUpdateEnrollments);
        if(!toUpdateEnrollments.isEmpty()){
            // seats validation
            List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(primaryBatchId,Arrays.asList(actionType.iterator().next()));
            logger.info("idEnrollCountAggregationRes {}",sectionEnrollmentAggregationRes);

            Map<String,Long> idEnrollmentCountMap = new HashMap<>();
            for(SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes){
                if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                    idEnrollmentCountMap.put(x.getId(), x.getEnrollmentsCount());
            }
            logger.info("idEnrollmentCountMap {}",idEnrollmentCountMap);

            for(Enrollment e:toUpdateEnrollments){
                String sectionId = e.getSectionId();
                long size = idSectionMap.get(sectionId).getSize();
                long currentCount = 0;
                long increasedCount = idIncrementCountMap.get(sectionId);

                if(idEnrollmentCountMap.get(sectionId) != null)
                    currentCount = idEnrollmentCountMap.get(sectionId);
                logger.info("sectionId {} , size {}, currentCount {}, increasedCount {}",sectionId,size,currentCount,increasedCount);

                if(increasedCount+currentCount > size)
                    throw new BadRequestException(ErrorCode.REDISTRIBUTION_NOT_POSSIBLE_AS_EXCEEDING_SECTION_SIZE,"Cannot redistribute as size exceeding for section " + sectionId);
            }

            // updation process starts
            String redisKey = createBatchEnrollmentRedisKey(primaryBatchId);
            try{
                redisDAO.setex(redisKey,Integer.toString(toUpdateEnrollments.size()),600);
            }
            catch(Exception ex){
                logger.error("Error in setting value to redis : " + ex.getMessage());
                return new PlatformBasicResponse(false,"Failure",ex.getMessage().toString());
            }
            int limit = 150;
            for(int idx = 0; idx < toUpdateEnrollments.size(); idx+=limit){
                List<Enrollment> tempEnrollments;

                if(toUpdateEnrollments.size() > idx + limit)
                    tempEnrollments = toUpdateEnrollments.subList(idx, idx + limit);
                else
                    tempEnrollments = toUpdateEnrollments.subList(idx, toUpdateEnrollments.size());

                EnrollmentListPojo enrollmentListPojo = new EnrollmentListPojo();
                enrollmentListPojo.setEnrollments(tempEnrollments);
                enrollmentListPojo.setBatchId(primaryBatchId);
                awsSQSManager.sendToSQS(SQSQueue.SECTION_QUEUE,SQSMessageType.UPDATE_BULK_ENROLLMENTS,gson.toJson(enrollmentListPojo));
            }
        }
        // email alerts for seats filling of section
        sendSectionFillingAlerts(primaryBatchId);
        return new PlatformBasicResponse(true,"Success","");
    }

    public void updateEnrollmentsInBulk(EnrollmentListPojo enrollmentListPojo) {
        List<Enrollment> enrollments = enrollmentListPojo.getEnrollments();
        String batchId = enrollmentListPojo.getBatchId();

        for(Enrollment e:enrollments) {
            enrollmentDAO.save(e);
        }

        // fetch value from redis
        int enrollmentsUpdatedCount = getBatchEnrollmentsUpdatedCountFromRedis(createBatchEnrollmentRedisKey(batchId));

        if(enrollmentsUpdatedCount == -1) {
            logger.error("No redis key available but bulk update sqs called");
            return;
        }

        if(enrollmentsUpdatedCount == 0){
            logger.info("enrollmentsUpdatedCount {}",enrollmentsUpdatedCount);
            deleteRedisKey(createBatchEnrollmentRedisKey(batchId));
        }
        else{

            // alerts for bulk update completion
            if(enrollmentsUpdatedCount == 1)
                communicationManager.sendAlertForBulkUpdateCompletion(batchId);

            String redisKey = createBatchEnrollmentRedisKey(batchId);
            int newRedisValue = enrollmentsUpdatedCount - enrollments.size();
            logger.info("newRedisValue {}",newRedisValue);

            String redisVal = Integer.toString(newRedisValue);
            try{
                redisDAO.setex(redisKey,redisVal,600);
            }
            catch(Exception ex){
                logger.error("Error in setting value to redis : " + ex.getMessage());
            }
        }

    }

    public void updateBundleBatchEnrollments(UpdateBundleBatchEnrollmentsReq req) throws BadRequestException {
        String bundleId = req.getBundleId();
        String bundleEnrollId = req.getBundleEnrollmentId();
        String userId = req.getUserId();
        EnrollmentState newSectionState = req.getEnrollmentState();

        if( bundleId == null || bundleEnrollId == null || userId == null || newSectionState == null || bundleId.isEmpty() ||
                bundleEnrollId.isEmpty() || req.getEnrollmentState().toString().isEmpty() || userId.isEmpty())
            return;

        List<Enrollment> enrollments = enrollmentDAO.getEnrollmentsForPurchaseContextIdForUserId(bundleId,userId);
        List<String> batchIds = new ArrayList<>();
        String message_1 = "TRIAL to REGULAR enrollment";

        logger.info("enrollments.size {}",enrollments.size());
        List<String> batchIdsToValidate = new ArrayList<>();
        for(Enrollment e: enrollments) {
            Long remainingPaidSeats = getSeatsRemainingForBatch(e.getBatchId(),false,Arrays.asList(EnrollmentState.REGULAR));

            Long sectionsCount = sectionDAO.getSectionCountForBatch(e.getBatchId());
            if(sectionsCount == 0){
                logger.info("No sections for this batch");
                Batch batch = batchDAO.getById(e.getBatchId());
                batch.setHasSections(Boolean.FALSE);
                batchDAO.save(batch);
                continue;
            }

            if(remainingPaidSeats <= 0) {
                batchIds.add(e.getBatchId());
                logger.info("REGULAR section seats full but conversion happened from trial to regular " + e.getId());
                continue;
            }

            List<Section> paidSections = new ArrayList<>();
            List<Section> temp;
            int skip = 0;
            int limit = 25;
            boolean processing = true;
            while(processing){
                temp = sectionDAO.getSectionsForBatchId(e.getBatchId(),skip,limit,Arrays.asList(EnrollmentState.REGULAR));
                if(temp.size() == 0)
                    processing = false;
                else{
                    skip += temp.size();
                    paidSections.addAll(temp);
                }

            }
            logger.info("paidSections {}",paidSections);

            if(paidSections.isEmpty()) {
                logger.info("No regular sections left for batch " + e.getBatchId() + " for enrollment " + e.getId());
                continue;
            }

            List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(e.getBatchId(),Arrays.asList(EnrollmentState.REGULAR));
            logger.info("idEnrollCountAggregationRes {}",sectionEnrollmentAggregationRes);

            Map<String,Long> idEnrollmentCountMap = new HashMap<>();
            for(SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes){
                if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                    idEnrollmentCountMap.put(x.getId(), x.getEnrollmentsCount());
            }

            PriorityQueue<Pair<Long,String>> pQ = new PriorityQueue<>(paidSections.size(), new The_Comparator());
            for(Section s : paidSections){

                String sId = s.getId();
                long size = s.getSize();
                long enrollmentsDone = 0;

                if(!idEnrollmentCountMap.isEmpty() && idEnrollmentCountMap.get(sId) != null)
                    enrollmentsDone = idEnrollmentCountMap.get(sId);

                logger.info("size : " + size + " enrollmentsDone: " + enrollmentsDone);
                if(size == enrollmentsDone || enrollmentsDone > size)
                    continue;

                Pair<Long,String> pair = new Pair<>(size-enrollmentsDone,sId);
                logger.info("pair {}",pair);
                pQ.add(pair);
            }

            Pair<Long,String> sectionToBeAssigned = pQ.peek();
            logger.info("sectionToBeAssigned {}",sectionToBeAssigned);

            if(Objects.isNull(sectionToBeAssigned) || sectionToBeAssigned.getVal() == null ) {
                logger.error("Section allotment error for batch " + e.getBatchId() + " userId " + userId + " sectionToBeAssigned " + sectionToBeAssigned + " for enrollmentId " + e.getId());
                continue;
            }
            // validate old and new section
            adjustSeatsVacancyForSections(e.getSectionId(),sectionToBeAssigned.getVal());
            e.setSectionId(sectionToBeAssigned.getVal());
            e.setSectionState(EnrollmentState.REGULAR);
            enrollmentDAO.save(e);

            batchIdsToValidate.add(e.getBatchId());

            // email alerts for seats filling of section
            sendSectionFillingAlerts(e.getBatchId());
        }

        if(!batchIds.isEmpty()){
            EnrollmentFailureReq _req = new EnrollmentFailureReq();
            _req.setUserId(userId);
            _req.setMessage_1(message_1);
            _req.setMessage_2("because either all REGULAR sections are full or no REGULAR sections exist");
            _req.setBatchIds(batchIds);
            awsSQSManager.sendToSQS(SQSQueue.SECTION_NOTIFICATIONS_QUEUE,SQSMessageType.ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE,gson.toJson(_req));
        }
    }


    // -------------- VALIDATIONS RELATED FUNCTIONS BELOW --------------------------------------------------------------

    private void validateSections(List<SectionInfo> sections, String batchId) throws VException,BadRequestException {

        // sessions related validation
        sessionValidation(batchId);

        List<String> primaryEmailIdsAcrossSections = new ArrayList<>();
        Set<String> uniqEmailIds = new HashSet<>();
        int teachersCount = 0;
        for (SectionInfo section : sections) {

            String primaryEmailId = null;
            if( Objects.isNull(section))
                throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS,"section is null");

            if (org.springframework.util.StringUtils.isEmpty(section.getSectionName()) || !(com.vedantu.util.StringUtils.isAlphaNumeric(section.getSectionName())))
                throw new BadRequestException(ErrorCode.SECTION_NAME_INCORRECT, "Section name cannot be empty and should be alphanumeric with all lowercase letters without spaces");

            if(section.getSectionName().length() > Section.Constants.SECTION_NAME_MAX_LENGTH)
                throw new BadRequestException(ErrorCode.SECTION_NAME_TOO_LARGE,"Section should be smaller than " + Section.Constants.SECTION_NAME_MAX_LENGTH);

            if (org.springframework.util.StringUtils.isEmpty(section.getSectionType()))
                throw new BadRequestException(ErrorCode.SECTION_TYPE_EMPTY, "Section type cannot be empty");

            if (!(EnrollmentState.TRIAL.equals(section.getSectionType())) && !(EnrollmentState.REGULAR.equals(section.getSectionType())))
                throw new BadRequestException(ErrorCode.INVALID_SECTION_TYPE, "Invalid Section Type");

            if (section.getSize() < Section.Constants.MINIMUM_SECTION_SIZE)
                throw new BadRequestException(ErrorCode.SECTION_SIZE_OUT_OF_ALLOWED_RANGE, "Minimum section size should be " + Section.Constants.MINIMUM_SECTION_SIZE);

            if (section.getEmailIds().isEmpty())
                throw new BadRequestException(ErrorCode.NO_EMAILID_FOUND, "No CTs assigned");

            if (section.getEmailIds().size() < Section.Constants.MIN_TEACHERS_NEEDED_IN_SECTION)
                throw new BadRequestException(ErrorCode.TEACHERS_COUNT_OUT_OF_ALLOWED_RANGE, Section.Constants.MIN_TEACHERS_NEEDED_IN_SECTION + " Email Ids are mandatory" + section.getSectionId());
            else {

                int originalEmailIdsSize = section.getEmailIds().size();
                int newEmailIdsSize = new HashSet<>(section.getEmailIds()).size();

                if (originalEmailIdsSize != newEmailIdsSize)
                    throw new BadRequestException(ErrorCode.UNIQUE_TEACHERS_NEEDED_AMONG_SECTIONS, "All emailIds should be unique");

                boolean firstEmail = true;
                for (String emailId : section.getEmailIds()) {
                    if (!CommonUtils.isValidEmailId(emailId.trim()))
                        throw new BadRequestException(ErrorCode.INVALID_EMAILID, "Email Id is incorrect" + emailId);

                    // Role check as only teachers are allowed
                    UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfoFromEmail(emailId, true);

                    if(Objects.isNull(userBasicInfo) || Objects.isNull(userBasicInfo.getRole()))
                        throw new BadRequestException(ErrorCode.USER_NOT_FOUND,"No user found with this emailId" + emailId);

                    if (!(userBasicInfo.getRole().equals(Role.TEACHER)))
                        throw new BadRequestException(ErrorCode.ONLY_TEACHER_EMAIL_ALLOWED, "Only teacher emails are allowed");

                    if(firstEmail)
                        primaryEmailId = emailId;
                    firstEmail = false;

                    uniqEmailIds.add(emailId);
                    teachersCount++;
                }

                if (section.getSectionType().equals(EnrollmentState.REGULAR) && section.getSize() > Section.Constants.MAX_REGULAR_SECTION_SIZE)
                    throw new BadRequestException(ErrorCode.SECTION_SIZE_OUT_OF_ALLOWED_RANGE, "Max section size for paid section type should not be more than 500");

                if (section.getSectionType().equals(EnrollmentState.TRIAL) && section.getSize() > Section.Constants.MAX_TRIAL_SECTION_SIZE)
                    throw new BadRequestException(ErrorCode.SECTION_SIZE_OUT_OF_ALLOWED_RANGE, "Max section size for free trial section type should not be more than 60000");

            }

            if(primaryEmailId != null)
                primaryEmailIdsAcrossSections.add(primaryEmailId);
        }
        // NOTE : Unique primary teachers for all the sections within the same batch
        int size_1 = primaryEmailIdsAcrossSections.size();
        int size_2 = new HashSet<>(primaryEmailIdsAcrossSections).size();
        if(size_1 != size_2)
            throw new BadRequestException(ErrorCode.PRIMARY_TEACHER_NEEDS_TO_BE_UNIQUE,"Primary emailId (the first emailId) should be unique for different sections within the same batch");

        if(uniqEmailIds.size() < teachersCount)
            throw new BadRequestException(ErrorCode.UNIQUE_TEACHERS_NEEDED_AMONG_SECTIONS,"All teachers have to unique across sections within a Batch");

    }

    public void batchValidation(String batchId) throws BadRequestException {
        if(Objects.isNull(batchId) || batchId.isEmpty())
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"batchId is empty/null");

        Batch batch =  batchManager.getBatch(batchId);
        logger.info("batch is {}",batch);
        if(Objects.isNull(batch))
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"No such batch exists for " + batchId);

        if(Objects.nonNull(batch.getHasSections()) && !batch.getHasSections())
            throw new BadRequestException(ErrorCode.SECTIONS_NOT_ENABLED_FOR_THIS_BATCH,"This batch does not have sections, operation not possible");
    }

    public void sessionValidation(String batchId) throws VException {

        // fetching latest upcoming session
        ClientResponse res = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + "/onetofew/session/getLatestUpcomingSessionsForBatch?batchId=" + batchId, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(res);
        String jsonString = res.getEntity(String.class);
        Type otfSessionPojoTypeList = new TypeToken<List<OTFSessionPojoUtils>>(){}.getType();
        List<OTFSessionPojoUtils> upcomingSessions = gson.fromJson(jsonString, otfSessionPojoTypeList);
        logger.info("upcomingSessions {}",upcomingSessions);

        if(!upcomingSessions.isEmpty()) {
            logger.info("upcomingSessions.size {}",upcomingSessions.size());
            Long currentTime = System.currentTimeMillis();
            Long upcomingSessionStartTime = upcomingSessions.get(0).getStartTime();
            Long upcomingSessionEndTime = upcomingSessions.get(0).getEndTime();
            Long TwoHrInMillis = Section.Constants.MINIMUM_HOURS_FOR_SECTION_UPDATE_BEFORE_SESSION_START * DateTimeUtils.MILLIS_PER_HOUR;
            logger.info("currentTime {}",currentTime);
            logger.info("upcomingSessionStartTime {}",upcomingSessionStartTime);
            logger.info("TwoHrInMillis {}",TwoHrInMillis);

            if(currentTime > upcomingSessionStartTime && currentTime < upcomingSessionEndTime)
                throw new BadRequestException(ErrorCode.SECTION_UPDATE_NOT_POSSIBLE_DURING_SESSION,"Sections cannot be updated as a session is going on for the batch");
            if(currentTime+TwoHrInMillis >= upcomingSessionStartTime) {
                throw new BadRequestException(ErrorCode.SECTION_UPDATE_NOT_POSSIBLE_2HR_BEFORE_SESSION_START, "Sections cannot be updated " + Section.Constants.MINIMUM_HOURS_FOR_SECTION_UPDATE_BEFORE_SESSION_START + " hrs before session start time");
            }
        }
    }

    public void sectionValidation(String sectionId) throws BadRequestException {
        if(Objects.isNull(sectionId) || sectionId.isEmpty())
            throw new BadRequestException(ErrorCode.SECTION_NOT_FOUND,"sectionId is empty/null");

        Section s = sectionDAO.getById(sectionId);
        if(s == null)
            throw new BadRequestException(ErrorCode.SECTION_NOT_FOUND,"no section exists for such sectionId " + sectionId);
    }

    public void userValidation(String userId) throws BadRequestException {
        if(Objects.isNull(userId) || userId.isEmpty())
            throw new BadRequestException(ErrorCode.USER_NOT_FOUND,"userId is null/empty");

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId,false);
        if(userBasicInfo == null)
            throw new BadRequestException(ErrorCode.USER_NOT_FOUND,"no such user exists with userId " + userId);
    }

    // ---------------- EMAIL RELATED FUNCTIONS BELOW ------------------------------------------------------------------

    private Map<String,List<String>> getSectionIdTeacherEmailIdMap(List<Section> sections) {
        logger.info("getSectionIdTeacherEmailIdMap {}",sections);
        if(Objects.isNull(sections) || sections.isEmpty())
            return new HashMap<>();

        Map<String,List<String>> sectionIdTeacherEmailIdMap = new HashMap<>();

        for(Section section:sections){
            List<String> teacherIds = section.getTeacherIds();
            List<String> emailIds = new ArrayList<>();
            for(String teacherId : teacherIds){
                UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(teacherId,true);
                if(userBasicInfo != null && userBasicInfo.getEmail() != null && !userBasicInfo.getEmail().isEmpty())
                    emailIds.add(userBasicInfo.getEmail());
            }
            sectionIdTeacherEmailIdMap.put(section.getId(),emailIds);
        }
        logger.info("sectionIdTeacherEmailIdMap {}",sectionIdTeacherEmailIdMap);
        return sectionIdTeacherEmailIdMap;
    }

    public List<String> getTeacherIdsFromEmailIds(List<String> emailIds){
        Map<String, UserInfo> userBasicInfoMap = fosUtils.getUserInfosFromEmails(new HashSet<>(emailIds),true);
        List<String> teacherIds = new ArrayList<>();
        for(String emailId : emailIds) {
            UserInfo userInfo = userBasicInfoMap.get(emailId);
            if (Objects.nonNull(userInfo) && Objects.nonNull(userInfo.getUserId())) {
                teacherIds.add(userInfo.getUserId().toString());
            }
        }
        return teacherIds;
    }

    public void sendAlertsToAcadOps(String batchId){
        if(batchId == null || batchId.isEmpty())
            return;

        Map<String,Long> idEnrollmentCountMap = new HashMap<>();
        List<SectionEnrollmentAggregationRes> sectionEnrollmentAggregationRes;
        sectionEnrollmentAggregationRes = enrollmentDAO.getSectionIdEnrollmentCountForSections(batchId,Arrays.asList(EnrollmentState.TRIAL,EnrollmentState.REGULAR));
        logger.info("sectionEnrollmentAggregationRes {}",sectionEnrollmentAggregationRes);

        for (SectionEnrollmentAggregationRes x : sectionEnrollmentAggregationRes) {
            if(x.getId() != null && !(x.getId().isEmpty()) && x.getEnrollmentsCount() != null)
                idEnrollmentCountMap.put(x.getId(), x.getEnrollmentsCount());
        }
        logger.info("idEnrollmentCountMap {}",idEnrollmentCountMap);

        List<Section> sections = new ArrayList<>();
        List<Section> tempData;
        int skip = 0;
        int limit = 25;
        boolean processing = true;
        while(processing){
            tempData = sectionDAO.getSectionsForBatchId(batchId,skip,limit,null);
            if(tempData.size() == 0)
                processing = false;
            else{
                sections.addAll(tempData);
                skip += tempData.size();
            }
        }
        logger.info("sections {}",sections);

        List<SectionInfo> sectionFillingInfos = new ArrayList<>();
        boolean alertNeeded = false;
        for(Section s:sections){
            logger.info("s.getId() {}",s.getId());
            logger.info("idEnrollmentCountMap.get(s.getId() {}",idEnrollmentCountMap.get(s.getId()));

            if(Objects.nonNull(idEnrollmentCountMap.get(s.getId()))) {
                long enrollCt = idEnrollmentCountMap.get(s.getId());
                double size = s.getSize();
                double temp = enrollCt / size * 100;
                long enrollPercent = Math.round(temp);

                logger.info("enrollCt {}", enrollCt);
                logger.info("size {}", size);
                logger.info("eCt/s {}", enrollCt / size);
                logger.info("temp {}", temp);
                logger.info("enrollPercent {}", enrollPercent);

                if (enrollPercent < 50)
                    continue;

                long offset = 0L;

                if (enrollPercent < 75)
                    offset = 50L;
                else if (enrollPercent < 90)
                    offset = 75L;
                else if (enrollPercent < 95)
                    offset = 90L;
                else if (enrollPercent < 100)
                    offset = 95L;
                else if (enrollCt == size)
                    offset = 100L;

                logger.info("offset {}", offset);
                if(offset < 80L)
                    continue;
                alertNeeded = true;
                SectionInfo _s = new SectionInfo();
                _s.setSectionName(s.getSectionName());
                _s.setSectionType(s.getSectionType());
                _s.setSectionId(s.getId());
                _s.setEnrollmentCount(offset);
                sectionFillingInfos.add(_s);
            }
        }
        logger.info("sectionFillingInfos {}", sectionFillingInfos);
        if(alertNeeded) {
            // alert mails sent to acadops when 50%, 75%, 90%, 95% and 100% seats filled for sections of batch
            communicationManager.sendEmailAlertsToOps(batchId, sectionFillingInfos);
        }
    }

    //-------------------------- REDIS RELATED FUNCTIONS BELOW ---------------------------------------------------------------

    private String createBatchEnrollmentRedisKey(String batchId){
        return batchEnrollmentsUpdatedCountSlug + batchId;
    }

    private int getBatchEnrollmentsUpdatedCountFromRedis(String redisKey) {
        String redisResult = null;

        try{
            redisResult = redisDAO.get(redisKey);
        }
        catch (Exception e){
            logger.error("Error in fetching redis value");
        }
        logger.info("redisResult {}",redisResult);

        if(redisResult == null)
            return -1;
        return  Integer.parseInt(redisResult);
    }

    public void deleteRedisKey(String key) {
        if (StringUtils.isNotEmpty(key)) {
            try {
                logger.info("Deleting redis for key " + key);
                redisDAO.del(key);
            } catch (InternalServerErrorException | BadRequestException e) {
                logger.error("error in deleting cache " + e);
            }
        }
    }

    // ----------------------------------- NODE TEAM RELATED FUNCTIONS BELOW -------------------------------------------

    public Map<String, SectionMetaDataRes> getSectionsInfoFromBatchIdsTAids(SectionMetaDataReq sectionMetaDataReq){
        Map<String, SectionMetaDataRes> response = new HashMap<>();
        List<String> batchIds = sectionMetaDataReq.getBatchIds();
        List<String> taIds = sectionMetaDataReq.getTaIds();

        if(ArrayUtils.isEmpty(batchIds) || ArrayUtils.isEmpty(taIds))
            return response;

        Set<String> bIds = new HashSet<>(batchIds);
        Set<String> tIds = new HashSet<>(taIds);

        logger.info("bIds {}",bIds);
        logger.info("tIds {}",tIds);

        List<String> includeFields = Arrays.asList(Section.Constants.SECTION_NAME,Section.Constants.TEACHER_IDS,Section.Constants.BATCH_ID);
        List<Section> temp;
        List<Section> sections = new ArrayList<>();

        boolean processing = true;
        int skip = 0;
        int limit = 250;
        while(processing){
            temp = sectionDAO.getSectionsInfoForTAs(new ArrayList<>(bIds),new ArrayList<>(tIds),skip,limit,includeFields);
            if(temp.size() == 0)
                processing = false;
            else {
                skip += temp.size();
                sections.addAll(temp);
            }
        }

        logger.info("sections {}",sections);

        if(!sections.isEmpty()) {
            logger.info("sections.size {}",sections.size());

            Set<String> ids = new HashSet<>();
            sections.stream().map(Section::getTeacherIds).forEach(ids::addAll);

            Map<String,UserBasicInfo> idUserInfoMap = fosUtils.getUserBasicInfosMap(new ArrayList<>(ids),false);
            logger.info("idUserInfoMap {}",idUserInfoMap);

            for(Section s:sections){
                for(String ta:s.getTeacherIds()){
                    if(taIds.contains(ta) && idUserInfoMap != null && idUserInfoMap.containsKey(ta)) {

                        SectionMetaDataRes res = new SectionMetaDataRes();
                        res.setUserId(ta);
                        res.setTaName(idUserInfoMap.get(ta).getFirstName());
                        res.setSectionName(s.getSectionName());
                        res.setBatchId(s.getBatchId());
                        logger.info("res {}",res);
                        response.put(s.getId(),res);
                    }
                }
            }
        }

        logger.info("response {}",response);
        return response;
    }

    public PlatformBasicResponse isTwoTeacherModelApplicable(List<String> batchIds) {
        Long batchesWithSectionCount = batchDAO.getSectionEnabledBatchesCount(batchIds);

        if(batchesWithSectionCount != batchIds.size())
            return new PlatformBasicResponse(false,"Success","");
        return new PlatformBasicResponse(true,"success","");
    }

    public PlatformBasicResponse isOtfSessionApplicable(List<String> batchIds) {
        Long batchesWithSectionCount = batchDAO.getSectionEnabledBatchesCount(batchIds);

        if(batchesWithSectionCount == 0)
            return new PlatformBasicResponse(true,"success","");
        return new PlatformBasicResponse(false,"success","");
    }


    public void validateSeatsInRedis(BatchSectionReq r) throws BadRequestException {
        for(String batchId:r.getBatchIds()){

            Batch batch = batchDAO.getById(batchId);
            if(batch.getHasSections().equals(Boolean.FALSE))
                continue;

            Long trialSeats = getSeatsRemainingForBatch(batchId,true,Arrays.asList(EnrollmentState.TRIAL));
            Long regularSeats = getSeatsRemainingForBatch(batchId,true,Arrays.asList(EnrollmentState.REGULAR));

            boolean trialNotExists = enrollmentManager.getBatchSectionRedisData(batchId,EnrollmentState.TRIAL).isEmpty();
            boolean regularNotExists = enrollmentManager.getBatchSectionRedisData(batchId,EnrollmentState.REGULAR).isEmpty();

            try {
                if(trialNotExists)
                    redisDAO.setnx(enrollmentManager.createBatchSectionRedisKey(batchId,EnrollmentState.TRIAL),Long.toString(trialSeats),3600);
                else {
                    Long redisVal =  Long.parseLong(redisDAO.get(enrollmentManager.createBatchSectionRedisKey(batchId,EnrollmentState.TRIAL)));
                    logger.info("redisVAL is {}",redisVal);
                    // dbValue - redisValue
                    Long diff = trialSeats-redisVal;
                    logger.info("diff {}",diff);
                    redisDAO.increment(enrollmentManager.createBatchSectionRedisKey(batchId, EnrollmentState.TRIAL), diff);
                }
                if(regularNotExists)
                    redisDAO.setnx(enrollmentManager.createBatchSectionRedisKey(batchId,EnrollmentState.REGULAR),Long.toString(regularSeats),3600);
                else {
                    Long redisVal =  Long.parseLong(redisDAO.get(enrollmentManager.createBatchSectionRedisKey(batchId,EnrollmentState.REGULAR)));
                    logger.info("redisVAL is {}",redisVal);
                    // dbValue - redisValue
                    Long diff = regularSeats-redisVal;
                    logger.info("diff {}",diff);
                    redisDAO.increment(enrollmentManager.createBatchSectionRedisKey(batchId, EnrollmentState.REGULAR), diff);
                }
            } catch (InternalServerErrorException e) {
                logger.error("Error in redis {}",e.getMessage());
            }
        }
    }

    public boolean changeSectionState(UpdateSectionEnrollmentReq req,EnrollmentState sectionState) throws BadRequestException {
        String batchId = req.getBatchId();
        String userId = req.getUserId();
        Enrollment enrollment = req.getEnrollment();
        boolean success = true;

        // 1 -- params validation
        if(null == batchId || null == userId || null == enrollment || null == enrollment.getId())
            return false;

        // 2 -- get respective sections
        List<Section> sections = getSectionsForType(batchId,sectionState);
        if(sections.isEmpty())
            return false;

        // 3 -- get section to be assigned
        Pair<Long,String> sectionToBeAssigned = getSectionToBeAssigned(batchId,sections,sectionState);

        // 4 -- get seats count and validate before assigning section
        long seatsWithoutBuffer = getSeatsRemainingForBatch(batchId,false, Collections.singletonList(sectionState));
        if(Objects.isNull(sectionToBeAssigned) || sectionToBeAssigned.getVal() == null) {
            logger.error("Section allotment error for batch " + batchId + " userId " + userId + " sectionToBeAssigned " + sectionToBeAssigned + " for enrollmentId " + enrollment.getId() + " with seats " + seatsWithoutBuffer);
            success = false;
        }
        if(Objects.nonNull(sectionToBeAssigned) && sectionToBeAssigned.getVal() != null && sectionToBeAssigned.getVal().isEmpty()) {
            logger.error("sectionId empty while assigning section to enrollment with id " + enrollment.getId() + " for user " + userId);
            success = false;
        }
        if(seatsWithoutBuffer < 0){
            logger.info("Enrollment triggered with no seats available for user with enrollmentId " + enrollment.getId());
            success = false;
        }

        // 5 -- section assignment
        if(success) {
            enrollmentDAO.setSectionToEnrollment(enrollment.getId(),sectionToBeAssigned.getVal(),sectionState);

//            // section redis keys validation
//            BatchSectionReq _r = new BatchSectionReq();
//            _r.setBatchIds(Arrays.asList(batchId));
//            awsSQSManager.sendToSQS(SQSQueue.SECTION_QUEUE, SQSMessageType.VALIDATE_SECTION_REDIS_KEYS, gson.toJson(_r));

            sendSectionFillingAlerts(batchId);
        }
        else{
//            try {
//                redisDAO.incr(enrollmentManager.createBatchSectionRedisKey(enrollment.getBatchId(),sectionState));
//            } catch (Exception e) {
//                logger.error("Error in redis " + e.getMessage());
//            }
        }
        return success;
    }

    public List<Section> getSectionsForType(String batchId,EnrollmentState sectionType){
        List<Section> sections = new ArrayList<>();
        List<Section> temp;

        int skip = 0;
        int limit = 25;
        boolean processing = true;

        while(processing) {
            temp = sectionDAO.getSectionsForBatchId(batchId,skip,limit,Collections.singletonList(sectionType));

            if(temp.size() == 0) {
                processing = false;
            }
            else {
                sections.addAll(temp);
                skip += temp.size();
            }
        }
        logger.info("sections.size {}",sections.size());
        return sections;
    }

    public Pair<Long,String> getSectionToBeAssigned(String batchId, List<Section> sections, EnrollmentState sectionState){

        // 1 -- prepare sectionId-EnrollmentCount map
        List<SectionEnrollmentAggregationRes> idEnrollmentPojo = enrollmentDAO.getSectionIdEnrollmentCountForSections(batchId,Collections.singletonList(sectionState));
        Map<String,Long> idEnrollmentCountMap = new HashMap<>();
        for(SectionEnrollmentAggregationRes sectionEnrollmentAggregationRes : idEnrollmentPojo){
            if(StringUtils.isNotEmpty(sectionEnrollmentAggregationRes.getId())  && sectionEnrollmentAggregationRes.getEnrollmentsCount() != null)
                idEnrollmentCountMap.put(sectionEnrollmentAggregationRes.getId(), sectionEnrollmentAggregationRes.getEnrollmentsCount());
        }
        logger.info("idEnrollmentCountMap {}",idEnrollmentCountMap);

        // 2 -- get section with least seats remaining
        PriorityQueue<Pair<Long,String>> pQ = new PriorityQueue<>(sections.size(), new The_Comparator());
        for(Section s : sections){
            String sId = s.getId();
            long size = s.getSize();
            long enrollmentsDone = 0;

            if(!idEnrollmentCountMap.isEmpty() && idEnrollmentCountMap.get(sId) != null)
                enrollmentsDone = idEnrollmentCountMap.get(sId);

            if(size == enrollmentsDone || enrollmentsDone > size)
                continue;

            Pair<Long,String> pair = new Pair<>(size-enrollmentsDone,sId);
            pQ.add(pair);
        }

        Pair<Long,String> sectionToBeAssigned = pQ.peek();
        logger.info("sectionToBeAssigned {}",sectionToBeAssigned);
        return sectionToBeAssigned;
    }


    public void adjustSeatsVacancyForSections(String prevId,String newId){
        if(Objects.nonNull(prevId) && !prevId.isEmpty())
            sectionDAO.incrementVacantSeatsForSections(Arrays.asList(prevId),1);
        if(Objects.nonNull(newId) && !newId.isEmpty())
            sectionDAO.incrementVacantSeatsForSections(Arrays.asList(newId),-1);
    }

    // email alerts for seats filling of section
    public void sendSectionFillingAlerts(String batchId){
        SeatsFillingReq seatsFillingReq = new SeatsFillingReq();
        seatsFillingReq.setBatchId(batchId);
        awsSQSManager.sendToSQS(SQSQueue.SECTION_NOTIFICATIONS_QUEUE,SQSMessageType.ACADOPS_SECTION_FILLING_ALERTS,gson.toJson(seatsFillingReq));
    }

    /**
     * Note: the batchIds passed should be of the same session only.
     */
    public PlatformBasicResponse isTeacherPartOfSection(TeacherInSectionReq req) throws BadRequestException {
        List<String> batchIds = req.getBatchIds();
        String userId = req.getUserId();

        final List<String> includeFields = Arrays.asList(Section.Constants._ID);
        Section section = sectionDAO.getSectionForTeacher(userId,batchIds,includeFields);

        if(Objects.nonNull(section))
            return new PlatformBasicResponse(true,"Success","");
        return new PlatformBasicResponse(false,"Failure","teacher not present in section");
    }
}
