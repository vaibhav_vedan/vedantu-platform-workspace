package com.vedantu.subscription.managers;

import static java.util.stream.Collectors.toList;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import com.vedantu.subscription.util.RedisDAO;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.TeacherBasicInfo;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.LeadSquaredRequest;
import com.vedantu.app.responses.BatchDetailsResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.request.ProcessPaymentReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.pojo.ContentStats;
import com.vedantu.lms.cmds.pojo.LMSStatsForSession;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.enums.*;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.onetofew.pojo.section.SectionPojo;
import com.vedantu.onetofew.request.CreateOTFSessionsReq;
import com.vedantu.onetofew.request.GetBatchesReq;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.onetofew.request.PurchaseOTFReq;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.scheduling.request.session.GetSessionPartnersReq;
import com.vedantu.scheduling.response.session.GetSessionPartnersRes;
import com.vedantu.scheduling.response.session.OTFSessionStats;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.*;
import com.vedantu.subscription.entities.mongo.*;
import com.vedantu.subscription.enums.CourseCategory;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.DurationTime;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.*;
import com.vedantu.subscription.request.*;
import com.vedantu.subscription.request.section.AddSectionReq;
import com.vedantu.subscription.response.BatchDashboardInfo;
import com.vedantu.subscription.response.*;
import com.vedantu.subscription.viewobject.request.AddBatchToBundleReq;
import com.vedantu.subscription.viewobject.request.MakePaymentRequest;
import com.vedantu.subscription.viewobject.response.GetBatchRes;
import com.vedantu.subscription.viewobject.response.GetRegistrationsResp;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CommonUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.Day;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.*;
import com.vedantu.util.enums.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.bson.types.ObjectId;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toList;

@Service
public class BatchManager {

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private CourseDAO courseDAO;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private GroupDAO groupDAO;

    @Autowired
    private BatchSnapshotOTMDAO batchSnapshotOTMDAO;

    @Autowired
    private BatchSnapshotOTMManager batchSnapshotOTMManager;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private BundleEnrolmentDAO bundleEnrolmentDAO;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private RegistrationDAO registrationDAO;

    private static Gson gson = new Gson();

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils userUtils;

    @Autowired
    private RegistrationManager registrationManager;

    @Autowired
    private EnrollmentConsumptionDAO enrollmentConsumptionDAO;

    @Autowired
    private EnrollmentConsumptionManager enrollmentConsumptionManager;

    @Autowired
    private EnrollmentTransactionDAO enrollmentTransactionDAO;

    @Autowired
    private BundleDAO bundleDAO;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private BundleEntityDAO bundleEntityDAO;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private BundleEntityManager bundleEntityManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private PremiumSubscriptionManager premiumSubscriptionManager;

    @Autowired
    private SectionManager sectionManager;
    
    @Autowired
    private SubscriptionUpdateDAO subscriptionUpdateDAO;

    @Autowired
    private RedisDAO redisDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BatchManager.class);

    public static TimeZone indiaTimeZone = TimeZone.getTimeZone("Asia/Kolkata");

    public static List<OTFSessionToolType> SUPPORTED_SESSION_TOOL_TYPES = Arrays.asList(OTFSessionToolType.GTT,
            OTFSessionToolType.GTT_PRO, OTFSessionToolType.GTT100, OTFSessionToolType.GTW, OTFSessionToolType.GTT_WEBINAR);

    private final String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private final String lmdEndpoint = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private final String userEndpoint = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    public BatchInfo addBatch(Batch batch, List<AgendaPojo> agendaPojoReq, Long callingUserId,
                              List<BaseInstalmentInfo> instalmentDetails, List<SectionPojo> sections)
            throws VException {

        Course course = courseDAO.getById(batch.getCourseId());
        if (course == null) {
            throw new NotFoundException(ErrorCode.COURSE_NOT_FOUND, "Course not found " + batch.getCourseId());
        }

        if (!StringUtils.isEmpty(batch.getAcadMentorId()) && StringUtils.isEmpty(batch.getGroupName())) {
            throw new ConflictException(ErrorCode.GROUPNAME_EMPTY, "Empty group name for acad mentor id");
        }

        if (!StringUtils.isEmpty(batch.getAcadMentorId())) {
            List<Batch> batches = batchDAO.getBatchByGroupNameWithDifferentAMId(batch.getGroupName(), batch.getAcadMentorId());
            if (ArrayUtils.isNotEmpty(batches)) {
                throw new ConflictException(ErrorCode.DIFFERENT_AM_NOT_ALLOWED_FOR_SAME_GROUP_NAME, "Different am not allowed for same group name");
            }
        }

        if ((batch.getDuration() / DateTimeUtils.MILLIS_PER_HOUR) < 1) {
            throw new ConflictException(ErrorCode.DURATION_LESS_THAN_1_HOUR, "Batch duration should not be less than 1 hour");
        }

        if (!StringUtils.isEmpty(batch.getAcadMentorId())) {
            UserBasicInfo mentorBasicInfo = userUtils.getUserBasicInfo(batch.getAcadMentorId(), false);

            if (mentorBasicInfo == null) {
                throw new com.vedantu.exception.NotFoundException(ErrorCode.NOT_FOUND_ERROR, "user not found for mentor Id");
            }

            if (!(Role.ADMIN.equals(mentorBasicInfo.getRole()) || Role.STUDENT_CARE.equals(mentorBasicInfo.getRole()))) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "only admin care, student care or teacher role allowed for being mentor");
            }
        }

        logger.info("batch - pre update: course :" + course.toString());
        if ((batch.getStartTime() < course.getLaunchDate()) && (batch.getStartTime() > course.getExitDate())) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "batch start time is outside of course launch date and exit date");
        }

        // No orgId for course while orgId exists for batch, throw exception
        if(StringUtils.isEmpty(course.getOrgId()) && !StringUtils.isEmpty(batch.getOrgId()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Trying to add batch with orgId for a course without orgId");

        // Course has orgId but batch does not have orgId, throw exception
        if(!StringUtils.isEmpty(course.getOrgId()) && StringUtils.isEmpty(batch.getOrgId()))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No orgId for Batch");

        // Both course and batch have orgId but not same so, throw exception
        if(!StringUtils.isEmpty(course.getOrgId()) && !StringUtils.isEmpty(batch.getOrgId()) && !(course.getOrgId().equals(batch.getOrgId())))
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Batch orgId and Course orgId should be same");

        Long currentTime = System.currentTimeMillis();
        Boolean newBatchCreated = Boolean.TRUE;

        boolean isNewBatchActive = batch.getBatchState() == EntityStatus.ACTIVE;
        boolean isNewBatchVisible = batch.getVisibilityState() == VisibilityState.VISIBLE;
        if (!isNewBatchActive || !isNewBatchVisible) {
            batch.setHalfScheduled(true);
        }

        Batch previousBatch = null;
        if (!StringUtils.isEmpty(batch.getId())) {
            validateBatchStartTime(batch.getId(), batch.getStartTime());
            previousBatch = batchDAO.getById(batch.getId());
            newBatchCreated = Boolean.FALSE;
            if (previousBatch.getEndTime() < currentTime) {
                TeacherUpdateForBatch(previousBatch, batch, callingUserId);
                batch.setEndTime(previousBatch.getEndTime());
                //logger.error("add batch update not allowed for with end time passed");
                //     throw new IllegalArgumentException("Except teacher change, rest all fields won't be updated because batch is already ended.");
            }

            if (!EntityStatus.ACTIVE.equals(previousBatch.getBatchState())) {
                logger.error("addBatch BatchAlreadyInactive : " + previousBatch.toString());
                throw new IllegalArgumentException("Batch already inactive.");
            }
            if (!previousBatch.getCourseId().equals(batch.getCourseId())) {
                logger.info("Incorrect course id");
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Incorrect course id");
            }
            batch.setCreatedBy(previousBatch.getCreatedBy());
            batch.setCreationTime(previousBatch.getCreationTime());
            // batch.setEndTime(previousBatch.getEndTime());
        }
        if (!StringUtils.isEmpty(batch.getGroupName())) {
            batch.setGroupSlug(makeSlug(batch.getGroupName()));
        }

        // validate sections first if validation successful then only create new batch
        if(sections != null && !sections.isEmpty()  && newBatchCreated.equals(Boolean.TRUE)) {
            batch.setHasSections(Boolean.TRUE);
            validateSections(sections);
        }

        batchDAO.save(batch);
        logger.info("batch {}",batch);

        // --------------------- SECTION ------------------------------------------------------
        if(batch.getHasSections() && newBatchCreated.equals(Boolean.TRUE) &&
                Objects.nonNull(batch.getId()) && !batch.getId().isEmpty() && sections != null ) {
            AddSectionReq addSectionReq = new AddSectionReq();
            addSectionReq.setBatchId(batch.getId());
            addSectionReq.setSectionList(sections);
            sectionManager.createSections(addSectionReq);
//            awsSQSManager.sendToSQS(SQSQueue.SECTION_QUEUE, SQSMessageType.CREATE_SECTION, gson.toJson(addSectionReq));
        }
        // ---------------------- END OF SECTION ----------------------------------------------

        if (!StringUtils.isEmpty(batch.getGroupName())) {
            groupDAO.addBatch(batch.getId(), batch.getGroupSlug(), batch.getGroupName());
        }

        // Handle session updates
        if (previousBatch == null) {
            //TODO validate student,presenter,taIds roles.
            //TODO teacher cannot be both taId and presenter
            createSessions(batch, agendaPojoReq);
        } else {
            if (previousBatch.getSessionToolType() == null
                    || !previousBatch.getSessionToolType().equals(batch.getSessionToolType())) {
                updateSessionToolType(batch.getId(), batch.getSessionToolType());
            }
        }

        // Handle course updates
        course = calculateCourseUpdates(course);
        courseDAO.save(course);

        // Handle enrollment updates
        updateEnrollments(batch, course, callingUserId);
        BatchInfo batchInfo = getBatchInfo(batch, course, false, true);
        if (previousBatch == null) {
            try {
                Map<String, Object> payload = new HashMap<>();
                payload.put("batchInfo", batchInfo);
                payload.put("event", BatchEventsOTF.BATCH_ACTIVE);
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BATCH_EVENTS_TRIGGER, payload);
                asyncTaskFactory.executeTask(params);
            } catch (Exception e) {
                logger.info("Error in async event for batch creation." + e);
            }
        }
        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("batchId", batch.getId());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BATCH_UPDATED, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for batch updation." + e);
        }

        try {
            List<String> courseTargets = new ArrayList<>();
            boolean batchHasGradeBoardTarget = ArrayUtils.isNotEmpty(course.getGrades()) && ArrayUtils.isNotEmpty(course.getNormalizeTargets()) && ArrayUtils.isNotEmpty(course.getNormalizeSubjects());

            if (batchHasGradeBoardTarget && !batch.isEarlyLearning() && VisibilityState.VISIBLE.equals( batch.getVisibilityState() ) && VisibilityState.VISIBLE.equals( course.getVisibilityState() ) && StringUtils.isEmpty(batch.getOrgId())) {
                courseTargets.addAll( course.getNormalizeTargets() );
                logger.info("Including batch data");
                AddBatchToBundleReq addBatchToBundleReq =
                        new AddBatchToBundleReq( batch.getId(), batch.getCourseId(),
                                course.getNormalizeSubjects(), batch.getSearchTerms(), course.getGrades(),
                                batch.isRecordedVideo(), course.getTitle(), courseTargets, batch.getLanguage(), batch.getPackageTypes() );
                addBatchToBundleReq.setStartTime( batch.getStartTime() );
                addBatchToBundleReq.setEndTime( batch.getEndTime() );
                awsSQSManager.sendToSQS(SQSQueue.SUBSCRIPTION_PACKAGE_OPS, SQSMessageType.ADD_BATCH_IN_SUBSCRIPTION, new Gson().toJson(addBatchToBundleReq));
            } else {
                logger.info("Excluding batch data");
                Map<String, Object> payload = new HashMap<>();
                payload.put("batchId", batch.getId());
                awsSQSManager.sendToSQS(SQSQueue.SUBSCRIPTION_PACKAGE_OPS, SQSMessageType.REMOVE_BATCH_FROM_SUBSCRIPTION, new Gson().toJson(payload));
            }
        } catch (Throwable t) {
            logger.error("Error while sending AsyncTask: ADD_BATCH_TO_BUNDLE", t);
        }

        if (previousBatch == null && !batch.isEarlyLearning()) {
            awsSNSManager.triggerSNS(SNSTopicOTF.AUTO_ENROLL_BATCH, "AUTO_ENROLL_BATCH", batchInfo.getId());
        }

        if (batch.isEarlyLearning()) {
            logger.info("EARLY LEARNING BATCH.");
            logger.info("SENDING BUNDLE CREATION REQ");
            BundleInfo bundleInfo = bundleManager.addEditBundle(populateBundleObject(batch, course, instalmentDetails));
            bundleEntityManager.addEditBundleEntity(populateBundleEntityObjects(bundleInfo, batch, course, instalmentDetails));
            batchInfo.setBundleInfo(bundleInfo);
        }
        return batchInfo;
    }

    private BundleEntityRequest populateBundleEntityObjects(BundleInfo bundleInfo, Batch batch,
                                                                  Course course, List<BaseInstalmentInfo> instalmentDetails) {
        BundleEntityRequest request = new BundleEntityRequest();
        request.setBundleId(bundleInfo.getId());

        request.setPackageType(PackageType.OTF);
        request.setEntityId(batch.getId());
        request.setEnrollmentType(EnrollmentType.AUTO_ENROLL);
        request.setMainTags(new HashSet<>(course.getTags()));
        List<Integer> grades = Optional.ofNullable(course.getGrades())
                .orElseGet(HashSet::new).stream()
                .map(Integer::parseInt)
                .collect(toList());
        request.setGrade(grades.stream().findFirst().orElse(null));
//        request.setCourseId(course.getId());
//        request.setCourseTitle(course.getTitle());
//        request.setSearchTerms(batch.getSearchTerms());
//        request.setTargets(course.getTargets());
//        request.setSubjects(subjects);
//        request.setStartTime(batch.getStartTime());
//        request.setEndTime(batch.getEndTime());

        return request;
    }

    private AddEditBundleReq populateBundleObject(Batch batch, Course course, List<BaseInstalmentInfo> instalmentDetails) {
        AddEditBundleReq bundle = new AddEditBundleReq();
        bundle.setTitle(course.getTitle());
        bundle.setState(BundleState.ACTIVE);
        bundle.setDescription(course.getDescription());
        bundle.setTarget(new ArrayList<>(Optional.ofNullable(course.getTargets()).orElseGet(HashSet::new)));
        List<Integer> grades = Optional.ofNullable(course.getGrades())
                .orElseGet(HashSet::new).stream()
                .map(Integer::parseInt)
                .collect(toList());
        bundle.setGrade(grades);
        ArrayList<String> subjects = new ArrayList<>(Optional.ofNullable(course.getNormalizeSubjects()).orElseGet(HashSet::new));
        bundle.setSubject(subjects);

        bundle.setPrice(batch.getPurchasePrice());
        bundle.setCutPrice(batch.getCutPrice());
        bundle.setValidTill(batch.getEndTime());
        bundle.setEarlyLearning(Boolean.TRUE);
        if(ArrayUtils.isNotEmpty(batch.getEarlyLearningCourses())) {
            bundle.setEarlyLearningCourses(batch.getEarlyLearningCourses());
        }
        bundle.setSearchTerms(batch.getSearchTerms());
        bundle.setLastPurchaseDate(batch.getLastPurchaseDate());
        bundle.setTeacherIds(new ArrayList<>(Optional.ofNullable(batch.getTeacherIds()).orElseGet(HashSet::new)));
        bundle.setInstalmentDetails(instalmentDetails);

        // Default Values (NPE Occur if we omit this process)
        bundle.setWebinarCategories(new ArrayList<>());
        bundle.setTeacherIds(new ArrayList<>());
        bundle.setCourses(new ArrayList<>());
        VideoContentData videos = new VideoContentData();
        videos.setChildren(new ArrayList<>());
        videos.setContents(new ArrayList<>());
        bundle.setVideos(videos);
        TestContentData tests = new TestContentData();
        tests.setChildren(new ArrayList<>());
        tests.setContents(new ArrayList<>());
        bundle.setTests(tests);
        bundle.setIsSubscription(Boolean.FALSE);
        HashMap<String, Object> metadata = new HashMap<>();
        metadata.put("priceCard", new HashMap<>());
        bundle.setMetadata(metadata);
        return bundle;
    }

    public void validateBatchStartTime(String batchId, Long batchStartTime) throws BadRequestException, VException {
        List<String> batchIds = Arrays.asList(batchId);
        ClientResponse response = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/onetofew/session/getOTFSessionForBatchIds", HttpMethod.POST, new Gson().toJson(batchIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String jsonString = response.getEntity(String.class);
        Type otfsessionpojolisttype = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();
        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfsessionpojolisttype);
        if (ArrayUtils.isNotEmpty(sessions)) {
            for (OTFSessionPojoUtils oTFSessionPojoUtils : sessions) {
                if (oTFSessionPojoUtils.getStartTime() < batchStartTime) {
                    throw new BadRequestException(ErrorCode.SESSION_START_TIME_LESS_THAN_BATCH_START_TIME, "batch start time can't be less than its session's startTime");
                }
            }
        }

    }

    public void getStudentStatsFromEnrollments(List<Enrollment> enrollments, BatchDashboardInfo batchDashboardInfo) {

        int activeStudents = 0;
        int studentsPaidBulk = 0;
        for (Enrollment enrollment : enrollments) {

            if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
                studentsPaidBulk++;
            }

            if (EntityStatus.ACTIVE.equals(enrollment.getStatus())) {
                activeStudents++;
            }

        }

        batchDashboardInfo.setNumStudentsActive(activeStudents);
        batchDashboardInfo.setNumStudentsPaidBulk(studentsPaidBulk);

    }

    public Map<String, List<Enrollment>> groupEnrollmentByBatch(List<Enrollment> enrollments) {
        Map<String, List<Enrollment>> batchEnrollmentMap = new HashMap<>();

        for (Enrollment enrollment : enrollments) {
            if (batchEnrollmentMap.containsKey(enrollment.getBatchId())) {
                batchEnrollmentMap.get(enrollment.getBatchId()).add(enrollment);
            } else {
                List<Enrollment> enrollmentList = new ArrayList<>();
                enrollmentList.add(enrollment);
                batchEnrollmentMap.put(enrollment.getBatchId(), enrollmentList);
            }
        }
        return batchEnrollmentMap;

    }

    public List<BatchDashboardInfo> getBatchDashboardInfos(String groupName, Integer start, Integer limit) throws VException {
        List<Batch> batchList = batchDAO.getBatchByGroupName(groupName, start, limit);

        if (ArrayUtils.isEmpty(batchList)) {
            return new ArrayList<>();
        }

        List<BatchDashboardInfo> batchDashboardInfoList = new ArrayList<>();
        Long currentTime = System.currentTimeMillis();
        Set<String> courseIds = new HashSet<>();
        List<String> batchIds = new ArrayList<>();
        for (Batch batch : batchList) {
            courseIds.add(batch.getCourseId());
            batchIds.add(batch.getId());
        }

        List<Course> courseList = courseDAO.getCoursesBasicInfos(new ArrayList<>(courseIds));
        List<Enrollment> enrollmentList = enrollmentDAO.getStudentEnrollmentsForBatches(batchIds);
        Map<String, List<Enrollment>> batchEnrollmentMap = groupEnrollmentByBatch(enrollmentList);
        Map<String, Course> courseMap = new HashMap<>();

        for (Course course : courseList) {
            courseMap.put(course.getId(), course);
        }

        String schedulingUrl = schedulingEndpoint + "/onetofew/session/getSessionStatsForBatches?time=" + currentTime;

        for (String batchId : batchIds) {
            schedulingUrl = schedulingUrl + "&batchIds=" + batchId;
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type otfSessionMapType = new TypeToken<HashMap<String, OTFSessionStats>>() {
        }.getType();
        Map<String, OTFSessionStats> otfSessionStatsMap = gson.fromJson(jsonString, otfSessionMapType);

        String lmsUrl = lmdEndpoint + "/cmds/contentInfo/getContentStatsForBatches?time=" + currentTime;
        for (String batchId : batchIds) {
            lmsUrl = lmsUrl + "&batchIds=" + batchId;
        }

        resp = WebUtils.INSTANCE.doCall(lmsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        jsonString = resp.getEntity(String.class);
        Type contentStatsMapType = new TypeToken<HashMap<String, ContentStats>>() {
        }.getType();
        Map<String, ContentStats> contentStatsMap = gson.fromJson(jsonString, contentStatsMapType);

        for (Batch batch : batchList) {
            BatchDashboardInfo batchDashboardInfo = new BatchDashboardInfo();
            batchDashboardInfo.setBatchId(batch.getId());
            batchDashboardInfo.setCourseId(batch.getCourseId());
            batchDashboardInfo.setCourseTitle(courseMap.get(batch.getCourseId()).getTitle());
            List<Enrollment> enrollments = batchEnrollmentMap.get(batch.getId());
            if (enrollments == null) {
                batchDashboardInfo.setNumStudentsEnrolled(0);
                batchDashboardInfo.setNumStudentsPaidBulk(0);
                batchDashboardInfo.setNumStudentsActive(0);
            } else {
                batchDashboardInfo.setNumStudentsEnrolled(enrollments.size());
                getStudentStatsFromEnrollments(enrollments, batchDashboardInfo);
            }
            OTFSessionStats otfSessionStats = otfSessionStatsMap.get(batch.getId());
            if (otfSessionStats == null) {
                batchDashboardInfo.setNumSessionsPlanned(0);
                batchDashboardInfo.setNumSessionsScheduled(0);
                batchDashboardInfo.setNumExtraSessions(0);
                batchDashboardInfo.setNumSessionsCancelled(0);
                batchDashboardInfo.setNumSessionsRescheduled(0);
                batchDashboardInfo.setNumSessionsDone(0);
            } else {
                batchDashboardInfo.setNumSessionsPlanned(otfSessionStats.getSessionsPlanned());
                batchDashboardInfo.setNumSessionsDone(otfSessionStats.getSessionsDone());
                batchDashboardInfo.setNumSessionsRescheduled(otfSessionStats.getSessionsRescheduled());
                batchDashboardInfo.setNumSessionsCancelled(otfSessionStats.getSessionsCancelled());
                batchDashboardInfo.setNumSessionsScheduled(otfSessionStats.getSessionsScheduled());
                batchDashboardInfo.setNumExtraSessions(otfSessionStats.getExtraSessions());
            }

            ContentStats contentStats = contentStatsMap.get(batch.getId());

            if (contentStats == null) {
                batchDashboardInfo.setNumTestAttempted(0);
                batchDashboardInfo.setNumTestShared(0);
                batchDashboardInfo.setNumAssignAttempted(0);
                batchDashboardInfo.setNumAssignShared(0);
            } else {
                if (contentStats.getBatchCurriculumStatus() != null) {
                    batchDashboardInfo.setCurriculumProgress(contentStats.getBatchCurriculumStatus());
                }
                batchDashboardInfo.setNumTestShared(contentStats.getTestShared());
                batchDashboardInfo.setNumTestAttempted(contentStats.getTestAttempted());
                batchDashboardInfo.setNumAssignShared(contentStats.getAssignShared());
                batchDashboardInfo.setNumAssignAttempted(contentStats.getAssignAttempted());
            }

            batchDashboardInfoList.add(batchDashboardInfo);

        }
        return batchDashboardInfoList;
    }

    public List<StudentEnrolledInfo> getEnrolledStudentInfo(String batchId,int start,int size) throws VException {
        List<String> batchIds = new ArrayList();
        batchIds.add(batchId);
        if(size > 100){
            throw new ForbiddenException(ErrorCode.BAD_REQUEST_ERROR,"Size can not be more than 100");
        }
        List<Enrollment> enrollmentList = enrollmentDAO.getEnrollmentsOfBatch(batchIds,start,size,true);
        if (ArrayUtils.isEmpty(enrollmentList)) {
            return new ArrayList<>();
        }
        List<String> userIds = new ArrayList<>();
        for (Enrollment enrollment : enrollmentList) {
            userIds.add(enrollment.getUserId());
        }

        List<UserBasicInfo> userBasicInfoList = fosUtils.getUserBasicInfos(userIds, true);

        List<StudentEnrolledInfo> studentEnrolledInfoList = new ArrayList<>();

        if (userBasicInfoList != null) {

            for (UserBasicInfo userBasicInfo : userBasicInfoList) {
                if (!userBasicInfo.getRole().equals(Role.STUDENT)) {
                    continue;
                }
                StudentEnrolledInfo studentEnrolledInfo = new StudentEnrolledInfo();
                studentEnrolledInfo.setEmail(userBasicInfo.getEmail());
                studentEnrolledInfo.setPhoneNumber(userBasicInfo.getContactNumber());
                studentEnrolledInfo.setsName(userBasicInfo.getFullName());
                studentEnrolledInfo.setPhoneCode(userBasicInfo.getPhoneCode());
                studentEnrolledInfoList.add(studentEnrolledInfo);
            }
        }

        return studentEnrolledInfoList;

    }

    public List<StudentEnrolledInfo> getActiveEnrolledStudentInfo(String batchId,int start,int size) throws VException {
        List<String> batchIds = new ArrayList<>();
        batchIds.add(batchId);
        if(size > 100){
            throw new ForbiddenException(ErrorCode.BAD_REQUEST_ERROR,"Size can not be more than 100");
        }
        List<Enrollment> enrollmentList = enrollmentDAO.getEnrollmentsForBatches(batchIds,start,size,true);
        if (ArrayUtils.isEmpty(enrollmentList)) {
            return new ArrayList<>();
        }
        List<String> userIds = new ArrayList<>();
        for (Enrollment enrollment : enrollmentList) {
            if (EntityStatus.ACTIVE.equals(enrollment.getStatus())) {
                userIds.add(enrollment.getUserId());
            }
        }
        List<UserBasicInfo> userBasicInfoList = fosUtils.getUserBasicInfos(userIds, true);

        List<StudentEnrolledInfo> studentEnrolledInfoList = new ArrayList<>();

        if (userBasicInfoList != null) {

            for (UserBasicInfo userBasicInfo : userBasicInfoList) {
                if (!userBasicInfo.getRole().equals(Role.STUDENT)) {
                    continue;
                }
                StudentEnrolledInfo studentEnrolledInfo = new StudentEnrolledInfo();
                studentEnrolledInfo.setEmail(userBasicInfo.getEmail());
                studentEnrolledInfo.setPhoneNumber(userBasicInfo.getContactNumber());
                studentEnrolledInfo.setsName(userBasicInfo.getFullName());
                studentEnrolledInfo.setPhoneCode(userBasicInfo.getPhoneCode());
                studentEnrolledInfoList.add(studentEnrolledInfo);
            }
        }

        return studentEnrolledInfoList;

    }

    public List<StudentEnrolledInfo> getRegularEnrolledStudentInfo(String batchId,int start,int size) throws VException {
        List<String> batchIds = new ArrayList<>();
        batchIds.add(batchId);
        if(size > 100){
            throw new ForbiddenException(ErrorCode.BAD_REQUEST_ERROR,"Size can not be more than 100");
        }
        List<Enrollment> enrollmentList = enrollmentDAO.getEnrollmentsForBatches(batchIds,start,size,true);
        if (ArrayUtils.isEmpty(enrollmentList)) {
            return new ArrayList<>();
        }
        List<String> userIds = new ArrayList<>();
        for (Enrollment enrollment : enrollmentList) {
            if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
                userIds.add(enrollment.getUserId());
            }
        }
        List<UserBasicInfo> userBasicInfoList = fosUtils.getUserBasicInfos(userIds, true);

        List<StudentEnrolledInfo> studentEnrolledInfoList = new ArrayList<>();

        if (userBasicInfoList != null) {

            for (UserBasicInfo userBasicInfo : userBasicInfoList) {
                if (!userBasicInfo.getRole().equals(Role.STUDENT)) {
                    continue;
                }
                StudentEnrolledInfo studentEnrolledInfo = new StudentEnrolledInfo();
                studentEnrolledInfo.setEmail(userBasicInfo.getEmail());
                studentEnrolledInfo.setPhoneNumber(userBasicInfo.getContactNumber());
                studentEnrolledInfo.setsName(userBasicInfo.getFullName());
                studentEnrolledInfo.setPhoneCode(userBasicInfo.getPhoneCode());
                studentEnrolledInfoList.add(studentEnrolledInfo);
            }
        }

        return studentEnrolledInfoList;

    }

    public CurriculumProgressInfo getTopicsStats(String batchId) throws VException {
        String server_url = lmdEndpoint + "/curriculum/getTopicsStats?batchId=" + batchId;
        ClientResponse resp = WebUtils.INSTANCE.doCall(server_url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        CurriculumProgressInfo curriculumProgressInfo = gson.fromJson(jsonString, CurriculumProgressInfo.class);
        return curriculumProgressInfo;
    }

    public List<OTMSessionDashboardInfo> getOTMSessionDashboardInfo(String groupName, Integer start, Integer size) throws VException {
        List<Batch> batchList = batchDAO.getBatchByGroupName(groupName, null, null);
        List<String> batchIds = new ArrayList<>();
        List<String> courseIds = new ArrayList<>();
        Map<String, Batch> batchMap = new HashMap<>();
        String server_url = schedulingEndpoint + "/onetofew/session/getOTMSessionDashboardPartialInfo?";
        int andFlag = 0;
        if (start != null) {
            andFlag = 1;
            server_url = server_url + "start=" + start;
        }
        if (size != null) {
            if (andFlag == 0) {
                server_url = server_url + "size=" + size;
            } else {
                server_url = server_url + "&size=" + size;
            }
        }

        if (ArrayUtils.isEmpty(batchList)) {
            return new ArrayList<>();
        }

        for (Batch batch : batchList) {
            server_url = server_url + "&batchIds=" + batch.getId();
            batchIds.add(batch.getId());
            courseIds.add(batch.getCourseId());
            batchMap.put(batch.getId(), batch);
        }

        List<Course> courseList = courseDAO.getCoursesBasicInfos(new ArrayList<>(courseIds));
        Map<String, Course> courseMap = new HashMap<>();

        for (Course course : courseList) {
            courseMap.put(course.getId(), course);
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(server_url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type otmDashboardInfoMapType = new TypeToken<Map<String, OTMSessionDashboardInfo>>() {
        }.getType();
        Map<String, OTMSessionDashboardInfo> otmSessionDashboardInfoMap = gson.fromJson(jsonString, otmDashboardInfoMapType);
        Set<String> sessionIds = new HashSet<>();
        List<Long> boardIds = new ArrayList<>();
        Set<String> teacherIds = new HashSet<>();
        for (Map.Entry<String, OTMSessionDashboardInfo> entry : otmSessionDashboardInfoMap.entrySet()) {
            sessionIds.add(entry.getKey());
            boardIds.add(entry.getValue().getBoardId());
            teacherIds.add(entry.getValue().getTeacherId());
        }

        Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
        Map<String, UserBasicInfo> userBasicInfoMap = fosUtils.getUserBasicInfosMap(teacherIds, true);

        server_url = lmdEndpoint + "/curriculum/getLMSStatsForSession";
        /*int flag = 0;
		for(String sessionId : sessionIds){
			if(flag==0){
				server_url = server_url + "?sessionIds="+sessionId;
				flag=1;
			}else{
				server_url = server_url + "&sessionIds="+sessionId;
			}
		}*/

        resp = WebUtils.INSTANCE.doCall(server_url, HttpMethod.POST, new Gson().toJson(sessionIds));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        jsonString = resp.getEntity(String.class);
        Type lmsStatsMapType = new TypeToken<Map<String, LMSStatsForSession>>() {
        }.getType();
        Map<String, LMSStatsForSession> lmsStatsForSessionMap = gson.fromJson(jsonString, lmsStatsMapType);
        List<OTMSessionDashboardInfo> otmSessionDashboardInfoList = new ArrayList<>();

        for (String sessionId : sessionIds) {
            OTMSessionDashboardInfo otmSessionDashboardInfo = otmSessionDashboardInfoMap.get(sessionId);
            if (otmSessionDashboardInfo == null) {
                //logger.error("No otmSessionDashboardInfo for session: "+sessionId);
                continue;
                //throw new VException(ErrorCode.SERVICE_ERROR, "otmSessionDashboardInfo for session: "+sessionId);
            }

            otmSessionDashboardInfo.setTeacher(userBasicInfoMap.get(otmSessionDashboardInfo.getTeacherId()).getFullName());
            otmSessionDashboardInfo.setSubject(boardMap.get(otmSessionDashboardInfo.getBoardId()).getName());
            if (ArrayUtils.isNotEmpty(otmSessionDashboardInfo.getBatchIds())) {
                List<BatchIdCourseTitlePojo> titles = new ArrayList<>();

                for (String batchId : otmSessionDashboardInfo.getBatchIds()) {
                    BatchIdCourseTitlePojo pojo = new BatchIdCourseTitlePojo();
                    pojo.setBatchId(batchId);
                    if (batchMap.containsKey(batchId)) {
                        String courseId = batchMap.get(batchId).getCourseId();
                        pojo.setCourseId(courseId);
                        if (courseMap.containsKey(courseId)) {
                            pojo.setCourseTitle(courseMap.get(courseId).getTitle());
                        }
                    }

                    titles.add(pojo);
                }
                otmSessionDashboardInfo.setCourseTitles(titles);
            }
//			otmSessionDashboardInfo.setCourseTitle(courseMap.get(batchMap.get(otmSessionDashboardInfo.getBatchId()).getCourseId()).getTitle());
//			otmSessionDashboardInfo.setCourseId(batchMap.get(otmSessionDashboardInfo.getBatchId()).getCourseId());
            LMSStatsForSession lmsStatsForSession = lmsStatsForSessionMap.get(sessionId);
            if (lmsStatsForSession == null) {
                //throw new VException(ErrorCode.SERVICE_ERROR, "lmsStatsForSession for session: "+sessionId);
                otmSessionDashboardInfo.setTestsAttempted(0);
                otmSessionDashboardInfo.setTestsEvaluated(0);
                otmSessionDashboardInfo.setAssignmentAttempted(0);
                otmSessionDashboardInfo.setAssignmentEvaluated(0);
            } else {
                otmSessionDashboardInfo.setTopicDone(lmsStatsForSession.getTopicDone());
                otmSessionDashboardInfo.setTestsShared(lmsStatsForSession.getTestShared());
                otmSessionDashboardInfo.setTestsAttempted(lmsStatsForSession.getAttemptedTests());
                otmSessionDashboardInfo.setTestsEvaluated(lmsStatsForSession.getEvaluatedTests());
                otmSessionDashboardInfo.setAssignmentShared(lmsStatsForSession.getAssingmentShared());
                otmSessionDashboardInfo.setAssignmentAttempted(lmsStatsForSession.getAttemptedAssignment());
                otmSessionDashboardInfo.setAssignmentEvaluated(lmsStatsForSession.getEvaluatedAssignment());
            }
            otmSessionDashboardInfoList.add(otmSessionDashboardInfo);
        }

        Collections.sort(otmSessionDashboardInfoList);

        return otmSessionDashboardInfoList;

    }

    private void updateSessionToolType(String batchId, OTFSessionToolType newSessionToolType) throws VException {
        if (!StringUtils.isEmpty(batchId)) {
            return;
        }

        CreateOTFSessionsReq req = new CreateOTFSessionsReq();
        req.setBatchId(batchId);
        req.setSessionToolType(newSessionToolType);
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/onetofew/session/updateBatchToolType", HttpMethod.POST,
                gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
    }

    public Batch markBatchInactive(String batchId) throws VException {
        logger.info("markBatchInactive add batch" + batchId);
        Batch batch = batchDAO.getById(batchId);
        if (batch == null) {
            logger.info("batch not found for batch id " + batchId);
            throw new IllegalArgumentException("Batch not found");
        }

        Course course = courseDAO.getById(batch.getCourseId());
        if (course == null) {
            logger.info("course not found for batch id " + batch.getId());
            throw new IllegalArgumentException("Course not found");
        }

        logger.info("batch - pre update: course :" + course.toString());

        batch.setBatchState(EntityStatus.INACTIVE);
        batchDAO.save(batch);

        // Deschedule session updates
        descheduleSessions(batchId);

        // Handle course updates
        course = calculateCourseUpdates(course);
        courseDAO.save(course);
        return batch;
    }

    // No additional checks for batch save here.
    public Batch updateBatch(Batch batch) {
        if (batch != null) {
            batchDAO.save(batch);
        }

        logger.info("Updated batch info : " + batch.toString());
        return batch;
    }

    public void createSessions(Batch batch, List<AgendaPojo> agendaPojoList)
            throws VException {
        CreateOTFSessionsReq req = new CreateOTFSessionsReq();

        if (agendaPojoList != null) {
            req.setBatchId(batch.getId());
            req.setSessionToolType(batch.getSessionToolType());
            req.setAgendaPojoList(agendaPojoList);
            req.setBatchInfo(batch.toBatchInfo(batch, null, null));
            ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/onetofew/session/createSessions", HttpMethod.POST,
                    gson.toJson(req));
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);

        }
    }

    public void descheduleSessions(String batchId) throws VException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(schedulingEndpoint + "/onetofew/session/descheduleSessions/"
                + batchId, HttpMethod.POST, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);

    }

    public static boolean allowSessionUpdate(long startTime) {
        long currentTime = System.currentTimeMillis();
        if (startTime < (currentTime + 30 * DateTimeUtils.MILLIS_PER_MINUTE)) {
            return false;
        } else {
            return true;
        }
    }

    public static Calendar getCalendar(long time) {
        Calendar startTimecalendar = Calendar.getInstance();
        startTimecalendar.setTimeZone(indiaTimeZone);
        startTimecalendar.setTime(new Date(time));

        return startTimecalendar;
    }

    public void updateEnrollments(Batch batch, Course course, Long callingUserId) {
        String id = batch.getId();
        boolean batchAdd = StringUtils.isEmpty(id);
        Set<String> teacherIds = new HashSet<>();
        if (batch.getTeacherIds() == null) {
            teacherIds = new HashSet<String>();
        } else {
            teacherIds = new HashSet<>(batch.getTeacherIds());
        }

        if (batchAdd) {
            // Create teacher enrollment
            if (teacherIds != null && !teacherIds.isEmpty()) {
                for (String teacherId : teacherIds) {
                    Enrollment enrollment = new Enrollment(teacherId, batch.getId(), course.getId(), null, Role.TEACHER,
                            EntityStatus.ACTIVE, EntityType.STANDARD);
                    enrollmentDAO.save(enrollment);
                }
            }
        } else {
            // Update teacher enrollment
            Query query = new Query();
            query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(course.getId()));
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(batch.getId()));
            query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.TEACHER));
            List<Enrollment> existingEnrollments = enrollmentDAO.runQuery(query, Enrollment.class);

            // Process the existing enrollments and mark the inactive
            // enrollments
            if (existingEnrollments != null) {
                for (Enrollment enrollment : existingEnrollments) {
                    if (!teacherIds.contains(enrollment.getUserId())) {
                        if (enrollment.getStatus() != null
                                && enrollment.getStatus().equals(EntityStatus.ACTIVE)) {
                            enrollmentTriggerForUser(enrollment, BatchEventsOTF.USER_INACTIVE);
                        }
                        enrollment.setStatus(EntityStatus.INACTIVE);

                    } else {
                        if (enrollment.getStatus() != null
                                && enrollment.getStatus().equals(EntityStatus.INACTIVE)) {
                            enrollmentTriggerForUser(enrollment, BatchEventsOTF.USER_ACTIVE);
                        }
                        enrollment.setStatus(EntityStatus.ACTIVE);
                        teacherIds.remove(enrollment.getUserId());
                    }
                    enrollmentDAO.save(enrollment);
                }
            }

            // Create new enrollments for remaining teachers
            Enrollment enrollment;
            if (teacherIds != null && !teacherIds.isEmpty()) {
                List<Enrollment> modifiedEnrollments = new ArrayList<Enrollment>();
                for (String teacherId : teacherIds) {
                    enrollment = new Enrollment(null, batch.getId(), course.getId(), null, Role.TEACHER,
                            EntityStatus.ACTIVE, EntityType.STANDARD);
                    enrollment.setUserId(teacherId);
                    modifiedEnrollments.add(enrollment);
                    enrollmentTriggerForUser(enrollment, BatchEventsOTF.USER_ENROLLED);
                }
                enrollmentDAO.insertAll(modifiedEnrollments, callingUserId);
            }
        }
    }

    public void enrollmentTriggerForUser(Enrollment enrollment, BatchEventsOTF event) {
        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("enrollment", enrollment);
            payload.put("event", event);
            AsyncTaskParams params = new AsyncTaskParams(
                    AsyncTaskName.ENROLLMENT_EVENTS_TRIGGER, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for ENROLLMENT_EVENTS_TRIGGER " + e);
        }
    }

    public Batch getBatch(String id) throws BadRequestException {
        Batch batch;
        if (!StringUtils.isEmpty(id)) {
            batch = batchDAO.getById(id);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Id is missing");
        }
        return batch;
    }

    public BatchInfo getBatchInfo(Batch batch, boolean exposeEmail) {
        Course course = courseDAO.getById(batch.getCourseId());
        return getBatchInfo(batch, course, false, exposeEmail);
    }

    public BatchInfo getBatchInfo(Batch batch, Course course,
                                  Boolean returnEnrollments, boolean exposeEmail) {
        Set<String> _userIds = new HashSet<>();
        Set<String> teacherIds = batch.getTeacherIds();
        //Set<String> studentIds = batch.getEnrolledStudents();
        if (ArrayUtils.isNotEmpty(teacherIds)) {
            _userIds.addAll(teacherIds);
        }
        //if (ArrayUtils.isNotEmpty(studentIds)) {
        //    _userIds.addAll(studentIds);
        //}

        if (ArrayUtils.isNotEmpty(course.getTeacherIds())) {
            _userIds.addAll(course.getTeacherIds());
        }

        Map<String, UserBasicInfo> usersMap = fosUtils.getUserBasicInfosMap(_userIds, exposeEmail);
        BatchInfo batchInfo = batch.toBatchInfo(batch, usersMap, null);
//		batchInfo.setAgenda(getAgendaPojos(batch, callingUserId,returnStudentInfo));
        batchInfo.setCourseInfo(course.toCourseBasicInfo(course, null, null));
        if (Boolean.TRUE.equals(returnEnrollments)) {
            List<String> batchIds = new ArrayList<>();
            batchIds.add(batch.getId());
            List<EnrollmentPojo> enrollmentPojos = enrollmentManager
                    .createEnrollmentInfos(enrollmentDAO.getEnrollmentsOfBatch(batchIds,0,20,true), false, exposeEmail);
            batchInfo.setEnrollments(enrollmentPojos);
        }
        return batchInfo;
    }

    public BatchBasicInfo getBatchBasicInfo(String id) throws NotFoundException {
        Batch batch = batchDAO.getById(id);
        if (batch == null) {
            throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "batch not found with id " + id);
        }
        Course course = courseDAO.getById(batch.getCourseId());
        BatchBasicInfo batchBasicInfo = batch.toBatchBasicInfo(batch, course, null, null);
        return batchBasicInfo;
    }

    public GetBatchRes getBatches(GetBatchesReq req, boolean exposeEmail) {
        String courseTitle = req.getCourseTitle();
        Query query = new Query();

        String courseId = req.getCourseId();
        if (!StringUtils.isEmpty(courseTitle)) {
            Query courseQuery = new Query();

            int length = courseTitle.length();
            int index = length < 40 ? length : 40;
            courseQuery.addCriteria(Criteria.where(Course.Constants.TITLE).regex(courseTitle.substring(0, index)));
            List<Course> courses = courseDAO.runQuery(courseQuery, Course.class);
            List<String> courseIds = new ArrayList<>();
            if (courses != null) {
                for (Course course : courses) {
                    courseIds.add(course.getId());
                }
                query.addCriteria(Criteria.where(Batch.Constants.COURSE_ID).in(courseIds));
            }
        } else {
            if (!StringUtils.isEmpty(courseId)) {
                query.addCriteria(Criteria.where(Batch.Constants.COURSE_ID).is(courseId));
            }
        }

        Set<String> studentIds = req.getStudentIds();
        if (studentIds != null && !studentIds.isEmpty()) {
            Query enrollmentQuery = new Query();
            List<String> studentListIds = new ArrayList<String>(studentIds);
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
            enrollmentQuery.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(studentListIds.get(0)));
            List<Enrollment> enrollments = enrollmentDAO.runQuery(enrollmentQuery, Enrollment.class);
            if (enrollments != null && !enrollments.isEmpty()) {
                List<String> batchIds = new ArrayList<String>();
                for (Enrollment enrollment : enrollments) {
                    batchIds.add(enrollment.getBatchId());
                }

                query.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
            }
        }

        Set<String> teacherIds = req.getTeacherIds();
        boolean isTeacherCheck = false;
        if (!StringUtils.isEmpty(teacherIds)) {
            query.addCriteria(Criteria.where(Batch.Constants.TEACHER_IDS).in(new HashSet<String>(teacherIds)));
            isTeacherCheck = true;
        }

        Integer purchasePrice = req.getPurchasePrice();
        if (purchasePrice != null) {
            query.addCriteria(Criteria.where(Batch.Constants.PURCHASE_PRICE).gte(purchasePrice));
        }

        if (!Role.ADMIN.equals(req.getCallingUserRole())) {
            if (StringUtils.isEmpty(teacherIds)) {
                query.addCriteria(Criteria.where(Batch.Constants.VISIBILITY_STATE).is(VisibilityState.VISIBLE));
            }
            query.addCriteria(Criteria.where(Batch.Constants.BATCH_STATE).is(EntityStatus.ACTIVE));
            Boolean showInactive = req.getShowInactive();
            if ((showInactive == null || !showInactive) && (studentIds == null || studentIds.isEmpty())) {
                query.addCriteria(Criteria.where(Batch.Constants.END_TIME).gte(System.currentTimeMillis()));
            }
//			if ((req.getSendNoTeachers() == null || !req.getSendNoTeachers()) && !isTeacherCheck) {
//				query.addCriteria(Criteria.where(Batch.Constants.TEACHER_IDS).ne(null)
//						.andOperator(Criteria.where(Batch.Constants.TEACHER_IDS).ne(new HashSet<String>())));
//			}
        } else {
            if (req.getVisibilityState() != null) {
                query.addCriteria(Criteria.where(Batch.Constants.VISIBILITY_STATE).is(req.getVisibilityState()));
            }
        }

        query.with(Sort.by(Sort.Direction.DESC, Batch.Constants.START_TIME));
        logger.info("count query :" + query);
        Long totalCount = batchDAO.queryCount(query, Batch.class);
        Integer start = req.getStart();
        Integer limit = req.getSize();
        if (start == null || start < 0) {
            start = 0;
        }
        if (limit == null || limit <= 0) {
            limit = 20;
        }

        query.skip(start);
        query.limit(limit);
        logger.info("query :" + query);
        List<Batch> batches = (List<Batch>) batchDAO.runQuery(query, Batch.class);
        logger.info("query result size :" + batches != null ? batches.size() : 0);

        List<BatchInfo> batchInfos = new ArrayList<>();
        if (batches != null && !batches.isEmpty()) {
            // fetch users
            Map<String, Course> courseMap = new HashMap<>();
            Course course;
            BatchInfo batchInfo;

            for (Batch batch : batches) {

                course = courseMap.get(batch.getCourseId());
                if (course == null) {
                    course = courseDAO.getById(batch.getCourseId());
                    if (course != null) {
                        courseMap.put(course.getId(), course);
                    }
                }

            }

            // Fetch enrollments
            List<Enrollment> enrollments = new ArrayList<>();
            Long callingUserId = req.getCallingUserId();
            List<String> demoBatchIds = new ArrayList<>();
            List<String> standardBatchIds = new ArrayList<>();
            if (callingUserId != null) {
                enrollments = enrollmentManager.getEnrollment(req.getCallingUserId().toString(), courseId, null, null);
                EntityType enrollmentType = null;
                for (Enrollment enrollment : enrollments) {
                    enrollmentType = enrollment.getType();
                    if (enrollmentType == null) {
                        enrollmentType = EntityType.STANDARD;
                    }
                    if (EntityType.STANDARD.equals(enrollmentType)
                            && !standardBatchIds.contains(enrollment.getBatchId())) {
                        standardBatchIds.add(enrollment.getBatchId());
                    }
                }
            }

            for (Batch batch : batches) {
                course = courseMap.get(batch.getCourseId());
                if (course == null) {
                    course = courseDAO.getById(batch.getCourseId());
                }
                String batchId = batch.getId();
                int count = 0;

                batchInfo = getBatchInfo(batch, course, false, exposeEmail);

                if (demoBatchIds.contains(batchId) && (batch.getStartTime() > System.currentTimeMillis())) {
                    batchInfo.setDemoSession(Boolean.TRUE);
                } else {
                    batchInfo.setDemoSession(Boolean.FALSE);
                }
                batchInfo.setEnrollmentCount(count);

                Role role = req.getCallingUserRole();
                if (role == null) {
                    role = Role.STUDENT;
                }
                if (!StringUtils.isEmpty(callingUserId)) {
                    if (standardBatchIds.contains(batchId)) {
                        batchInfo.setSubscribed(true);
                    }
                }

                batchInfos.add(batchInfo);
            }
        }

        GetBatchRes batchesRes = new GetBatchRes();
        batchesRes.setList(batchInfos);
        batchesRes.setTotalCount(totalCount.intValue());
        return batchesRes;
    }

    public Course calculateCourseUpdates(Course course) {
        // Batch updates should happen before calculation
        if (course == null) {
            return null;
        }

        int startPrice = -1;
        Long currentTime = System.currentTimeMillis();
        Long batchStartDate = course.getExitDate();
        Long batchEndDate = course.getLaunchDate();
        Long maxStartTime = -1l;
        int originalPrice = course.getStartPrice();
        int purchasePrice;
        int maxStartPrice = -1;
        int cutPrice;
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.COURSE_ID).is(course.getId()));
        List<Batch> batches = batchDAO.runQuery(query, Batch.class);
        if (batches != null && !batches.isEmpty()) {
            long totalSeats = 0l;
            for (Batch batch : batches) {
                // start price logic
                if (VisibilityState.VISIBLE.equals(batch.getVisibilityState())
                        && EntityStatus.ACTIVE.equals(batch.getBatchState())) {
                    purchasePrice = batch.getPurchasePrice();
                    cutPrice = batch.getCutPrice();

                    if (batch.getStartTime() > maxStartTime) {
                        maxStartTime = batch.getStartTime();
                        maxStartPrice = batch.getPurchasePrice();
                    }

                    // batch start date and end date logic
                    if (batch.getStartTime() < batchStartDate && batch.getStartTime() > currentTime) {
                        batchStartDate = batch.getStartTime();
                        startPrice = batch.getPurchasePrice();
                    }
                    if (batch.getEndTime() > batchEndDate) {
                        batchEndDate = batch.getEndTime();
                    }

                    totalSeats += batch.getMaxEnrollment();


                }
            }

            course.setTotalSeats(totalSeats);
            long enrollmentCount=enrollmentDAO.getEnrollmentCountInCourse(course.getId());
            course.setStudentsEnrolled(enrollmentCount);
        }


        logger.info("batchStartDate: " + batchStartDate);
        logger.info("batchStartPrice: " + maxStartPrice);
        if (maxStartTime != -1 && maxStartTime < batchStartDate) {
            batchStartDate = maxStartTime;
            course.setBatchStartTime(batchStartDate);
            course.setStartPrice(maxStartPrice);
        } else if (maxStartTime != -1 && maxStartTime >= batchStartDate) {
            course.setBatchStartTime(batchStartDate);
            course.setStartPrice(startPrice);
        }
        if (ArrayUtils.isNotEmpty(batches) && maxStartTime == -1) {
            course.setBatchStartTime(null);
        }

        if (ArrayUtils.isEmpty(batches) && maxStartTime == -1) {
            course.setBatchStartTime(course.getLaunchDate());
        }

        if (course.getStartPrice() == -1 || Boolean.TRUE.equals( course.getEarlyLearning() )) {
            course.setStartPrice(originalPrice);
        }

        course.setBatchEndTime(batchEndDate);
        query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.COURSE_ID).is(course.getId()));
        query.addCriteria(Criteria.where(Batch.Constants.VISIBILITY_STATE).is(VisibilityState.VISIBLE));
        query.addCriteria(Criteria.where(Batch.Constants.BATCH_STATE).is(EntityStatus.ACTIVE));
        Long totalBatches = courseDAO.queryCount(query, Batch.class);
        course.setTotalBatches(totalBatches.intValue());
        query.addCriteria(Criteria.where(Batch.Constants.TEACHER_IDS).ne(null)
                .andOperator(Criteria.where(Batch.Constants.TEACHER_IDS).ne(new HashSet<String>())));
        Long upcomingBatchesCount = courseDAO.queryCount(query, Batch.class);
        course.setUpcomingBatches(upcomingBatchesCount.intValue());
        logger.info("calculateCourseUpdates " + course.toString());
        return course;
    }

    public OTFSessionToolType getSessionTooleType(String batchId) throws BadRequestException {
        Batch batch = getBatch(batchId);
        return batch.getSessionToolType();
    }

    public void updateToolType(UpdateBatchToolTypeReq req)
            throws VException {
        logger.info("Request : " + req.toString());

        // Validate req
        req.validate();

        // Validate batch
        Batch batch = getBatch(req.getBatchId());
        if (batch == null) {
            throw new com.vedantu.exception.NotFoundException(ErrorCode.NOT_FOUND_ERROR,
                    "batch not found : " + req.getBatchId() + " req:" + req.toString());
        }

        if (!EntityStatus.ACTIVE.equals(batch.getBatchState())) {
            throw new BadRequestException(ErrorCode.OTF_BATCH_INACTIVE,
                    "Batch is inactive : " + batch.toString() + " req:" + req.toString());
        }

        // Update session tool type for sessions
        updateSessionToolType(batch.getId(), req.getSessionToolType());

        // Update session tool type for batch
        batch.setSessionToolType(req.getSessionToolType());
        updateBatch(batch);
    }

    public GetSessionPartnersRes getSessionPartners(GetSessionPartnersReq req) {

        List<UserBasicInfo> partners = new ArrayList<>();
        Set<String> partnerIds = batchDAO.getSessionPartners(req.getCallingUserId().toString(),
                req.getCallingUserRole());
        if (ArrayUtils.isNotEmpty(partnerIds)) {
            Map<String, UserBasicInfo> partnersMap = userUtils.getUserBasicInfosMap(partnerIds, false);
            // to preserve the order
            for (String partnerId : partnerIds) {
                if (partnersMap.containsKey(partnerId)) {
                    partners.add(partnersMap.get(partnerId));
                }
            }
        }
        GetSessionPartnersRes res = new GetSessionPartnersRes(partners);
        return res;
    }

    public List<String> getBatchByGroupName(List<String> groupName) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.GROUP_NAME).in(groupName));
        List<Batch> batches = batchDAO.runQuery(query, Batch.class);
        logger.info("BATCHES-COUNT: " + batches.size());
        List<String> batchIds = new ArrayList<>();
        for (Batch c : batches) {
            batchIds.add(c.getId());
        }
        logger.info("COUNT: " + batchIds.size());
        return batchIds;
    }

    public Map<String, String> getCourseTitleForBatchIds(List<String> batchIds) {
        Map<String, String> idToTitleMap = new HashMap<>();
        if (ArrayUtils.isEmpty(batchIds)) {
            logger.info("batchIds is empty");
            return idToTitleMap;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
        query.fields().include(Batch.Constants.COURSE_ID);
        query.fields().include(Batch.Constants.ID);
        query.fields().include(Batch.Constants._ID);
        List<Batch> batches = batchDAO.runQuery(query, Batch.class);
        if (ArrayUtils.isEmpty(batches)) {
            logger.info("batches is empty ");
            return idToTitleMap;
        }
        Set<String> courseIds = new HashSet<>();
        for (Batch batch : batches) {
            if (!StringUtils.isEmpty(batch.getCourseId())) {
                courseIds.add(batch.getCourseId());
            }
        }
        if (ArrayUtils.isEmpty(courseIds)) {
            logger.info("courseIds is empty ");
            return idToTitleMap;
        }
        Query courseQuery = new Query();
        courseQuery.addCriteria(Criteria.where(Course.Constants.ID).in(courseIds));
        courseQuery.fields().include(Course.Constants.TITLE);
        courseQuery.fields().include(Course.Constants.ID);
        courseQuery.fields().include(Course.Constants._ID);
        List<Course> courses = courseDAO.runQuery(courseQuery, Course.class);
        if (ArrayUtils.isEmpty(courses)) {
            logger.info("courses is empty ");
            return idToTitleMap;
        }
        Map<String, String> courseMap = new HashMap<>();
        for (Course course : courses) {
            if (!StringUtils.isEmpty(course.getId())) {
                courseMap.put(course.getId(), course.getTitle());
            }
        }
        for (Batch batch : batches) {
            if (!StringUtils.isEmpty(batch.getCourseId())) {
                idToTitleMap.put(batch.getId(), courseMap.get(batch.getCourseId()));
            }
        }
        return idToTitleMap;
    }

    public GetBatchesRes getBatchBasicInfosFromCourse(String batchId, String courseId) {
        GetBatchesRes resp = new GetBatchesRes();
        if (StringUtils.isEmpty(batchId) && StringUtils.isEmpty(courseId)) {
            return resp;
        }
        if (!StringUtils.isEmpty(batchId)) {
            Batch initBatch = batchDAO.getById(batchId);
            if (initBatch == null || StringUtils.isEmpty(initBatch.getCourseId())) {
                return resp;
            }
            courseId = initBatch.getCourseId();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.COURSE_ID).is(courseId));
        List<Batch> batches = batchDAO.runQuery(query, Batch.class);
        List<BatchBasicInfo> basicInfos = new ArrayList<>();
        Set<String> userIds = new HashSet<>();
        for (Batch batch : batches) {
            if (ArrayUtils.isNotEmpty(batch.getTeacherIds())) {
                userIds.addAll(batch.getTeacherIds());
            }
        }
        Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, false);
        for (Batch batch : batches) {
            BatchBasicInfo basicInfo = batch.toBatchBasicInfo(batch, null, userMap, null);
            basicInfo.setId(batch.getId());
            basicInfos.add(basicInfo);
        }
        resp.setBatches(basicInfos);
        return resp;
    }

    public void triggerSNSForBatch(BatchInfo batchInfo, EntityStatus status) {
        try {
            Map<String, Object> payload = new HashMap<>();

            payload.put("batchInfo", batchInfo);
            if (status.equals(EntityStatus.INACTIVE)) {
                payload.put("event", BatchEventsOTF.BATCH_INACTIVE);
            } else {
                payload.put("event", BatchEventsOTF.BATCH_ACTIVE);
            }
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BATCH_EVENTS_TRIGGER, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for BATCH_EVENTS_TRIGGER " + e);
        }
    }

    public Map<String, BatchBasicInfo> getBatchBasicInfoForBatchIds(List<String> batchIds) {
        Map<String, BatchBasicInfo> idToTitleMap = new HashMap<>();
        if (ArrayUtils.isEmpty(batchIds)) {
            logger.info("batchIds is empty");
            return idToTitleMap;
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
        query.fields().include(Batch.Constants.COURSE_ID);
        query.fields().include(Batch.Constants.ID);
        query.fields().include(Batch.Constants._ID);
        query.fields().include(Batch.Constants.GROUP_NAME);
        List<Batch> batches = batchDAO.runQuery(query, Batch.class);
        if (ArrayUtils.isEmpty(batches)) {
            logger.info("batches is empty ");
            return idToTitleMap;
        }
        Set<String> courseIds = new HashSet<>();
        for (Batch batch : batches) {
            if (!StringUtils.isEmpty(batch.getCourseId())) {
                courseIds.add(batch.getCourseId());
            }
        }
        if (ArrayUtils.isEmpty(courseIds)) {
            logger.info("courseIds is empty ");
            return idToTitleMap;
        }
        Query courseQuery = new Query();
        courseQuery.addCriteria(Criteria.where(Course.Constants.ID).in(courseIds));
        courseQuery.fields().include(Course.Constants.TITLE);
        courseQuery.fields().include(Course.Constants.ID);
        courseQuery.fields().include(Course.Constants._ID);
        List<Course> courses = courseDAO.runQuery(courseQuery, Course.class);
        if (ArrayUtils.isEmpty(courses)) {
            logger.info("courses is empty ");
            return idToTitleMap;
        }
        Map<String, Course> courseMap = new HashMap<>();
        for (Course course : courses) {
            if (!StringUtils.isEmpty(course.getId())) {
                courseMap.put(course.getId(), course);
            }
        }
        for (Batch batch : batches) {
            if (!StringUtils.isEmpty(batch.getCourseId())) {
                idToTitleMap.put(batch.getId(), batch.toBatchBasicInfo(batch, courseMap.get(batch.getCourseId()), null, null));
            }
        }
        return idToTitleMap;
    }

    public static String makeSlug(String tag) {
        if (!StringUtils.isEmpty(tag)) {
            tag = tag.trim();
            tag = tag.toLowerCase();
            tag = tag.replaceAll(" ", "-");
        }
        return tag;
    }

    public BatchBasicInfo getBatchById(String id) throws BadRequestException {
        Batch batch = getBatch(id);
        Course course = courseDAO.getById(batch.getCourseId());
        BatchBasicInfo batchInfo = batch.toBatchBasicInfo(batch, course, null, null);
        return batchInfo;
    }

    public BatchInfo getBatchByIdWithEnrollments(String id) throws BadRequestException {
        Batch batch = getBatch(id);
        //Course course = courseDAO.getById(batch.getCourseId());
        BatchInfo batchInfo = batch.toBatchInfo(batch, null, null);
        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(id));
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        Set<String> studentIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(enrollments)) {
            for (Enrollment enrollment : enrollments) {
                enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, null, null, null));
                studentIds.add(enrollment.getUserId());
            }

        }
        batchInfo.setEnrollments(enrollmentInfos);

        batchInfo.setEnrollmentCount(studentIds.size());

        return batchInfo;
    }

    public List<EnrollmentPojo> getBatchIdsActiveEnrollments(List<String> batchIds, boolean returnTeacherEnrollments) throws BadRequestException {
        Query query = new Query();

        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
        if (!returnTeacherEnrollments) {
            query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        }
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        List<EnrollmentPojo> enrollmentInfos = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(enrollments)) {
            for (Enrollment enrollment : enrollments) {
                enrollmentInfos.add(enrollment.toEnrollmentInfo(enrollment, null, null, null, null));
            }

        }
        return enrollmentInfos;
    }

    public List<BatchBasicInfo> getBatchBasicInfosByIds(List<String> batchIds) {
        return getDetailedBatchInfo(new BatchDetailInfoReq(batchIds));
    }

    public List<BatchBasicInfo> getDetailedBatchInfo(BatchDetailInfoReq req) {
        logger.info("getDetailedBatchInfo request received : {}", req);
        List<BatchBasicInfo> batchesResp = new ArrayList<>();
        List<String> batchIds = req.getBatchIds();

        if (ArrayUtils.isNotEmpty(batchIds)) {
            Map<String, Course> courseMap = new HashMap<>();

            List<Batch> batches = batchDAO.getBatchByIds(batchIds, req.getBatchDataIncludeFields(), req.getBatchDataExcludeFields());
            logger.info("batches - " + batches);
            if (!CollectionUtils.isEmpty(batches)) {
                Set<String> courseIds = batches.stream().map(Batch::getCourseId).collect(Collectors.toSet());

                List<Course> courses = new ArrayList<>();
                if (req.getCourseInfoRequired()) {
                    logger.info("Course information required, so getting the same.");
                    courses = courseDAO.getCourseByIds(courseIds, req.getCourseDataIncludeFields(), req.getCourseDataExcludeFields());
                }
                logger.info("courses - " + courses);

                if (!CollectionUtils.isEmpty(courses)) {
                    courseMap = courses.stream().collect(Collectors.toMap(Course::getId, c -> c));
                }

                for (Batch batch : batches) {
                    BatchBasicInfo batchInfo = batch.toBatchBasicInfo(batch, courseMap.get(batch.getCourseId()), null, null);
                    batchesResp.add(batchInfo);
                }
            }
        }

        return batchesResp;

    }

    public OrderInfo purchaseAndRegisterBatch(PurchaseOTFReq req) throws VException, CloneNotSupportedException {
        req.verify();

        String batchId = req.getEntityId();
        String bundleId = req.getPurchaseContextId();

        if (bundleId != null) {
            verifyIfTheBatchIsInBundle(batchId, bundleId);
        }

        logger.info("finding the amount to pay");
        Batch batch = getBatch(batchId);
        if (batch == null) {
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND, "batch not found");
        }

        Long userId = ConfigUtils.INSTANCE.getLongValue("batch.autoenrollment.userid");
        if (!userId.equals(req.getUserId())) {
            if (otfBundleManager._checkIfAlreadyEnrolled(req.getUserId(), new ArrayList<>(), Arrays.asList(batchId))) {
                throw new ConflictException(ErrorCode.ALREADY_ENROLLED, "User already enrolled in one of the courses");
            }
        }

        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();

        if (batch.getRegistrationFee() == null) {
            throw new BadRequestException(ErrorCode.REGISTRATION_FEE_NOT_FOUND, "No registration fee for bundle");
        }

        int amountToPay = 0;
        amountToPay = batch.getRegistrationFee();
        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(batchId);
        buyItemsReqNew.setEntityType(com.vedantu.session.pojo.EntityType.OTF_BATCH_REGISTRATION);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setPaymentType(PaymentType.BULK);
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());
        buyItemsReqNew.setUtm_campaign(req.getUtm_campaign());
        buyItemsReqNew.setUtm_content(req.getUtm_content());
        buyItemsReqNew.setUtm_medium(req.getUtm_medium());
        buyItemsReqNew.setUtm_source(req.getUtm_source());
        buyItemsReqNew.setUtm_term(req.getUtm_term());
        buyItemsReqNew.setChannel(req.getChannel());

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);

        if (!orderInfo.getNeedRecharge()) {
            processOTFRegFeePayment(orderInfo.getId(), orderInfo.getUserId(), batch);
        }
        return orderInfo;
    }

    public void processOTFRegFeePayment(String orderId, Long userId, Batch batch)
            throws VException {
        processOTFRegFeePayment(orderId, userId, batch, null, null, null);
    }

    public void processOTFRegFeePayment(String orderId, Long userId,
                                        String batchId) throws VException {
        processOTFRegFeePayment(orderId, userId, getBatch(batchId), null, null, null);
    }

    public Long getDurationToBeSubtracted(String batchId) {
        Long resultDuration = 0l;
        Long currentTime = System.currentTimeMillis();
        List<BatchSnapshotOTM> batchSnapshotOTMs = batchSnapshotOTMDAO.getPastBatchSnapshotsForBatch(batchId, currentTime);
        for (BatchSnapshotOTM batchSnapshotOTM : batchSnapshotOTMs) {

            for (SessionSnapshot sessionSnapshot : batchSnapshotOTM.getSessions()) {
                if (sessionSnapshot.getoTMSessionType() == null || OTMSessionType.REGULAR.equals(sessionSnapshot.getoTMSessionType())) {
                    if (sessionSnapshot.getStartTime() < currentTime) {
                        resultDuration += sessionSnapshot.getEndTime() - sessionSnapshot.getStartTime();
                    }
                }
            }

        }
        return resultDuration;
    }

    public void processOTFRegFeePayment(String orderId, Long userId,
                                        Batch batch, EnrollmentPurchaseContext purchaseContextType, String purchaseContextId,
                                        EnrollmentType purchaseContextEnrollmentType)
            throws VException {

        String batchId = batch.getId();
        logger.info("making call for dinero operations");
        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);

        Registration reg = new Registration();
        reg.setEntityType(com.vedantu.session.pojo.EntityType.OTF_BATCH_REGISTRATION);
        reg.setEntityId(batchId);
        reg.setUserId(userId);
        reg.setOrderId(res.getId());
        registrationDAO.save(reg);

        Enrollment enrollment = new Enrollment(userId.toString(),
                batchId, batch.getCourseId(), null, Role.STUDENT, EntityStatus.ACTIVE, null);
        if (purchaseContextEnrollmentType != null && purchaseContextId != null && purchaseContextType != null) {
            enrollment.setPurchaseContextType(purchaseContextType);
            enrollment.setPurchaseContextId(purchaseContextId);
            enrollment.setPurchaseContextEnrollmentType(purchaseContextEnrollmentType);
        }
        enrollment.setState(EnrollmentState.TRIAL);
        enrollmentManager.updateEnrollment(enrollment, userId);

        paymentManager.updateDeliverableEntityIdInOrder(res.getId(), enrollment.getId(), DeliverableEntityType.OTF_BATCH_ENROLLMENT, enrollment.getCourseId());
        Map<String, Object> payload = new HashMap<>();
        payload.put("enrollment", enrollment);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_ENROLLMENT_STATUS_EVENTS, payload);
        asyncTaskFactory.executeTask(params);

        registrationManager.markRegistrationStatus(reg.getId(), null, null, RegistrationStatus.ENROLLED, userId, Arrays.asList(enrollment.getId()), Arrays.asList(RegistrationStatus.REGISTERED));

        reg = registrationDAO.getById(reg.getId());
        logger.info("Registration: " + reg);

        //EnrollmentConsumption enrollmentConsumption = new EnrollmentConsumption();
        //enrollmentConsumption.setRegAmtLeft(res.getAmount());
        //enrollmentConsumption.setRegAmtPaid(res.getAmount());
        //enrollmentConsumption.setRegAmtPLeft(res.getPromotionalAmount());
        //enrollmentConsumption.setRegAmtNPLeft(res.getNonpromotionalAmount());
        //enrollmentConsumption.setRegAmtPaidP(res.getPromotionalAmount());
        //enrollmentConsumption.setRegAmtPaidNP(res.getNonpromotionalAmount());
        //enrollmentConsumption.setAmtToPay(reg.getBulkPriceToPay());
        //enrollmentConsumption.setDuration((long)reg.getDuration());
        if (reg.getDuration() - getDurationToBeSubtracted(batchId) < DateTimeUtils.MILLIS_PER_HOUR) {
            reg.setDuration((int) (getDurationToBeSubtracted(batchId) + DateTimeUtils.MILLIS_PER_HOUR));
            //throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Duration left can't be less than 1 hour");
        }
        int hourlyRate;
        if (!batch.isContentBatch()) {
            hourlyRate = (int) (reg.getBulkPriceToPay() / ((reg.getDuration() - getDurationToBeSubtracted(batchId)) / DateTimeUtils.MILLIS_PER_HOUR));
        } else {
            long timeDiff = batch.getEndTime() - (System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_DAY);
            if (timeDiff < DateTimeUtils.MILLIS_PER_HOUR) {
                timeDiff = DateTimeUtils.MILLIS_PER_HOUR;
            }
            hourlyRate = (int) (reg.getBulkPriceToPay() / (timeDiff / DateTimeUtils.MILLIS_PER_HOUR));
        }
        logger.info("Hourly Rate: " + hourlyRate);
        //enrollmentConsumption.setHourlyrate(enrollmentConsumption.getAmtToPay()/(reg.getDuration()/DateTimeUtils.MILLIS_PER_HOUR));
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants._ID).is(new ObjectId(enrollment.getId())));
        Update update = new Update();
        update.set(Enrollment.Constants.HOURLY_RATE,hourlyRate);
        enrollment.setHourlyRate(hourlyRate);
        enrollmentDAO.updateSingle(query,update);
        //enrollmentManager.updateEnrollment(enrollment, userId);
        MakePaymentRequest makePaymentRequest = new MakePaymentRequest();

        if (ArrayUtils.isNotEmpty(res.getOrderedItems())) {
            //enrollmentConsumption.setRegAmtLeftFromDiscount(res.getOrderedItems().get(0).getVedantuDiscountAmount());
            //enrollmentConsumption.setRegAmtPaidFromDiscount(res.getOrderedItems().get(0).getVedantuDiscountAmount());
            makePaymentRequest.setAmtFromDiscount(res.getOrderedItems().get(0).getVedantuDiscountAmount());
        }

        makePaymentRequest.setAmt(res.getAmount());
        makePaymentRequest.setAmtNP(res.getNonpromotionalAmount());
        makePaymentRequest.setAmtP(res.getPromotionalAmount());
        makePaymentRequest.setBatchId(batchId);
        makePaymentRequest.setCourseId(batch.getCourseId());
        makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.REGISTRATION_PAYMENT);
        makePaymentRequest.setOrderId(orderId);
        makePaymentRequest.setEnrollmentId(enrollment.getId());
        makePaymentRequest.setDuration((long) reg.getDuration());
        makePaymentRequest.setAmtToBePaid(reg.getBulkPriceToPay());

        logger.info("MakePayment Request: " + makePaymentRequest);

        //enrollmentConsumption.setEnrollmentId(enrollment.getId());
        //enrollmentConsumption.setCourseId(batch.getCourseId());
        //enrollmentConsumption.setBatchId(batchId);
        //List<EnrollmentConsumption> enrollmentConsumptions = new ArrayList<>();
        //enrollmentConsumptions.add(enrollmentConsumption);
        //enrollmentConsumptionDAO.insertAll(enrollmentConsumptions, null);
        //EnrollmentTransaction enrollmentTransaction = enrollmentManager.saveEnrollmentTransactionForRegistration(enrollmentConsumption);
        //List<EnrollmentTransaction> enrollmentTransactions = new ArrayList<>();
        //enrollmentTransactions.add(enrollmentTransaction);
        //enrollmentTransactionDAO.insertAll(enrollmentTransactions, null);
        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("enrollment", enrollment);
        payload2.put("orderInfo", res);
        payload2.put("newEnrollmentCreated", Boolean.TRUE);

        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.OTF_ENROLLMENT_EMAIL, payload2);
        asyncTaskFactory.executeTask(params2);
        try {
            enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, 1f);
        } catch (Exception ex) {
            logger.error("topup enrollment: ", ex.getMessage());
        }

    }

    public OrderInfo purchaseBatch(PurchaseOTFReq req) throws VException, CloneNotSupportedException {
        logger.info("purchase batch" + gson.toJson(req));
        req.verify();
        String batchId = req.getEntityId();
        String bundleId = req.getPurchaseContextId();

        if (bundleId != null) {
            verifyIfTheBatchIsInBundle(batchId, bundleId);
        }

        logger.info("finding the amount to pay");
        Batch batch = getBatch(batchId);
        if (batch == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "bundlepackage not found");
        }

        if (batch.getLastPurchaseDate() != null && batch.getLastPurchaseDate() < System.currentTimeMillis()) {
            throw new BadRequestException(ErrorCode.LAST_PURCHASE_DATE_IS_OVER,
                    "LastPurchaseDate is over for this Batch " + batch.getId());
        }

        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();

        if (batch.getRegistrationFee() != null) {
            List<GetRegistrationsResp> regs = registrationManager.getUsersRegistrationForMultipleEntities(req.getUserId(), Arrays.asList(batchId, batch.getCourseId()));
            if (ArrayUtils.isEmpty(regs)) {
                throw new ForbiddenException(ErrorCode.NOT_REGISTERED, "Batch Registration Fee not paid");
            }
            GetRegistrationsResp resp = regs.get(0);
            if (ArrayUtils.isNotEmpty(resp.getInstalmentDetails())) {
                buyItemsReqNew.setBaseInstalmentInfos(resp.getInstalmentDetails());
            }
        }

        int amountToPay = 0;
        amountToPay = batch.getPurchasePrice();
        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(batchId);
        buyItemsReqNew.setEntityType(com.vedantu.session.pojo.EntityType.OTF);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setPaymentType(req.getPaymentType());
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());
        buyItemsReqNew.setUtm_campaign(req.getUtm_campaign());
        buyItemsReqNew.setUtm_content(req.getUtm_content());
        buyItemsReqNew.setUtm_medium(req.getUtm_medium());
        buyItemsReqNew.setUtm_source(req.getUtm_source());
        buyItemsReqNew.setUtm_term(req.getUtm_term());
        buyItemsReqNew.setChannel(req.getChannel());
        if (PaymentType.INSTALMENT.equals(req.getPaymentType())) {
            buyItemsReqNew.setPurchaseEntityType(InstalmentPurchaseEntity.BATCH);
        }
        if (req.getPurchaseContextId() != null) {
            buyItemsReqNew.setPurchaseContextId(req.getPurchaseContextId());
        }

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        logger.info("\nAfter payment the req is " + gson.toJson(req));
        if (!orderInfo.getNeedRecharge()) {
            logger.info("\nInside setting upsell inside enrollment table");
            processBatchAfterPayment(orderInfo.getId(), orderInfo.getUserId(), batch,
                    req.getPurchaseContextType(), req.getPurchaseContextId(), req.getPurchaseContextEnrollmentType());
        }
        return orderInfo;
    }

    private void verifyIfTheBatchIsInBundle(String batchId, String bundleId) throws BadRequestException {
        List<BundleEntity> bundleEntities=bundleEntityDAO.getAllBundleEntityByBundleIdAndEntityId(bundleId,batchId);
        if (ArrayUtils.isEmpty(bundleEntities)) {
            throw new BadRequestException(ErrorCode.COURSE_NOT_FOUND, "The specified batch id is not present in the Bundle(AIO)", Level.ERROR);
        }
    }

    public void processBatchAfterPayment(String orderId, Long userId,
                                         String batchId) throws VException {
        logger.info("\nCalling from original processBatchAfterPayment");
        processBatchAfterPayment(orderId, userId, getBatch(batchId), null, null, null);
    }

    public void processBatchAfterPayment(String orderId, Long userId,
                                         Batch batch, EnrollmentPurchaseContext purchaseContextType, String purchaseContextId,
                                         EnrollmentType purchaseContextEnrollmentType)
            throws VException {

        String batchId = batch.getId();
        logger.info("making call for dinero operations");
        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        processPaymentReq.setPurchaseEntityType(InstalmentPurchaseEntity.BATCH);
        processPaymentReq.setPurchaseEntityId(batchId);
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);

        Enrollment enrollment = new Enrollment(userId.toString(),
                batchId, batch.getCourseId(), null, Role.STUDENT, EntityStatus.ACTIVE, null);
        logger.info("UPSELL Data: " + purchaseContextEnrollmentType + " " + purchaseContextId + " " + purchaseContextType);
        if (purchaseContextEnrollmentType != null && purchaseContextId != null && purchaseContextType != null) {
            logger.info("UPSELL Data: " + purchaseContextEnrollmentType + " " + purchaseContextId + " " + purchaseContextType);
            BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getNonEndedBundleEnrolment(userId.toString(), purchaseContextId);
            if (bundleEnrolment == null) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "user dont have active enrollment in the AIO");
            }
            enrollment.setPurchaseContextType(purchaseContextType);
            enrollment.setPurchaseContextId(purchaseContextId);
            enrollment.setPurchaseContextEnrollmentType(purchaseContextEnrollmentType);
            enrollment.setPurchaseEnrollmentId(bundleEnrolment.getId());
        }
        enrollment.setState(EnrollmentState.REGULAR);
        Boolean createdNew = enrollmentManager.updateEnrollment(enrollment, userId);

        paymentManager.updateDeliverableEntityIdInOrder(res.getId(), enrollment.getId(), DeliverableEntityType.OTF_BATCH_ENROLLMENT, enrollment.getCourseId());

        if (batch.getDuration() - getDurationToBeSubtracted(batchId) < DateTimeUtils.MILLIS_PER_HOUR) {
            batch.setDuration(DateTimeUtils.MILLIS_PER_HOUR + getDurationToBeSubtracted(batchId));
            //throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Duration left can't be less than 1 hour");
        }

        MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
        makePaymentRequest.setEnrollmentId(enrollment.getId());
        makePaymentRequest.setAmt(res.getAmountPaid());
        makePaymentRequest.setAmtP(res.getPromotionalAmount());
        makePaymentRequest.setAmtNP(res.getNonpromotionalAmount());

        if (ArrayUtils.isNotEmpty(res.getOrderedItems()) && res.getOrderedItems().get(0).getVedantuDiscountAmountClaimed() != null) {
            makePaymentRequest.setAmtFromDiscount(res.getOrderedItems().get(0).getVedantuDiscountAmountClaimed());
        } else {
            makePaymentRequest.setAmtFromDiscount(0);
        }

        if (PaymentType.BULK.equals(res.getPaymentType())) {
            makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.BULK_PAYMENT);
            makePaymentRequest.setAmt(res.getAmount());
        } else {
            makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.INSTALLMENT_PAYMENT);

        }
        if (ArrayUtils.isNotEmpty(res.getOrderedItems()) && res.getOrderedItems().get(0).getVedantuDiscountAmount() != null) {
            makePaymentRequest.setAmtToBePaid(res.getAmount() + res.getOrderedItems().get(0).getVedantuDiscountAmount());
        } else {
            makePaymentRequest.setAmtToBePaid(res.getAmount());
        }
        long timeDiff;
        if (!batch.isContentBatch()) {
            timeDiff = batch.getDuration() - getDurationToBeSubtracted(batchId);
        } else {
            timeDiff = batch.getEndTime() - (System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_DAY);
            if (timeDiff < DateTimeUtils.MILLIS_PER_HOUR) {
                timeDiff = DateTimeUtils.MILLIS_PER_HOUR;
            }
        }

        //enrollment.setHourlyRate((int) (makePaymentRequest.getAmtToBePaid() / (timeDiff / DateTimeUtils.MILLIS_PER_HOUR)));
        int hourlyRate = (int) (makePaymentRequest.getAmtToBePaid() / (timeDiff / DateTimeUtils.MILLIS_PER_HOUR));
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants._ID).is(new ObjectId(enrollment.getId())));
        Update update = new Update();
        update.set(Enrollment.Constants.HOURLY_RATE,hourlyRate);
        enrollment.setHourlyRate(hourlyRate);
        enrollmentDAO.updateSingle(query,update);
        //enrollmentManager.updateEnrollment(enrollment, userId);
        makePaymentRequest.setBatchId(batchId);
        makePaymentRequest.setCourseId(batch.getCourseId());
        makePaymentRequest.setOrderId(orderId);

        if (createdNew) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("enrollment", enrollment);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_ENROLLMENT_STATUS_EVENTS, payload);
            asyncTaskFactory.executeTask(params);
        }

        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("enrollment", enrollment);
        payload2.put("orderInfo", res);
        payload2.put("newEnrollmentCreated", createdNew);

        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.OTF_ENROLLMENT_EMAIL, payload2);
        asyncTaskFactory.executeTask(params2);
        try {
            enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, 1f);
        } catch (Exception ex) {
            logger.error("topup enrollment: ", ex.getMessage());
        }

    }

    public void processInstalmentAfterPayment(InstalmentInfo info) throws VException {
        List<InstalmentInfo> res = paymentManager.processInstalmentAfterPayment(info.getId());

        for (InstalmentInfo instalmentInfo : res) {
            if (instalmentInfo.getId().equals(info.getId())) {
                info = instalmentInfo;
                break;
            }
        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", info.getUserId());
        payload.put("batchId", info.getContextId());
        payload.put("instalments", res);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.PAY_INSTALMENT_OTF, payload);
        asyncTaskFactory.executeTask(params);

        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(info.getUserId().toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.INACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(info.getContextId()));
        logger.info("query " + query);

        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);

        com.vedantu.session.pojo.EntityType type = null;
        if (ArrayUtils.isNotEmpty(enrollments)) {
            Boolean orderState = paymentManager.resetInstalmentsPostPayment(info.getContextId(), com.vedantu.session.pojo.EntityType.OTF, info.getUserId().toString());
            if (Boolean.FALSE.equals(orderState)) {
                logger.info("More instalments to be paid by user. Not changing state of Enrollment.");
                //Trigger mail about it.
                return;
            }

        }

        logger.info(res);

        OTFEnrollmentReq enrollmentReq = new OTFEnrollmentReq(info.getUserId().toString(),
                info.getContextId(), null, null, Role.STUDENT, "ACTIVE", null, EnrollmentState.REGULAR);
        Enrollment enrollment = enrollmentManager.markStatus(enrollmentReq);

        MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
        makePaymentRequest.setAmt(info.getTotalAmount());
        makePaymentRequest.setAmtP(info.getTotalPromotionalAmount());
        makePaymentRequest.setAmtNP(info.getTotalNonPromotionalAmount());

        if (info.getVedantuDiscountAmount() != null) {
            makePaymentRequest.setAmtFromDiscount(info.getVedantuDiscountAmount());
        } else {
            makePaymentRequest.setAmtFromDiscount(0);
        }

        makePaymentRequest.setEnrollmentTransactionContextId(info.getId());
        makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.INSTALLMENT_PAYMENT);
        makePaymentRequest.setEnrollmentId(enrollment.getId());
        makePaymentRequest.setCourseId(enrollment.getCourseId());
        makePaymentRequest.setBatchId(enrollment.getBatchId());
        makePaymentRequest.setOrderId(info.getOrderId());
        makePaymentRequest.setCreateConsumptionIfnotFound(false);
        try {
            boolean paymentMade = enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, 1f);
            if (!paymentMade) {
                logger.error("Payment Failed for enrollment: " + enrollment);
            }
        } catch (Exception ex) {
            logger.error("topup enrollment: ", ex.getMessage());
        }
    }

    public void updateSnapshots(List<SessionSnapshot> sessionSnapshots) {
        Map<String, List<SessionSnapshot>> batchSnapshotsMap = new HashMap<>();

        for (SessionSnapshot sessionSnapshot : sessionSnapshots) {

            for (String batchId : sessionSnapshot.getBatchIds()) {
                if (!batchSnapshotsMap.containsKey(batchId)) {
                    batchSnapshotsMap.put(batchId, new ArrayList<>());
                }

                batchSnapshotsMap.get(batchId).add(sessionSnapshot);

            }

        }

        for (Map.Entry<String, List<SessionSnapshot>> entry : batchSnapshotsMap.entrySet()) {
            batchSnapshotOTMManager.updateBatchSnapshot(entry.getKey(), entry.getValue());
        }

    }

    public void updateSnapshot(SessionSnapshot sessionSnapshot) {
        Long monthStartTime = DateTimeUtils.getISTMonthStartTime(sessionSnapshot.getStartTime());
        List<BatchSnapshotOTM> batchSnapshotOTMs = batchSnapshotOTMDAO.getPastBatchSnapshotsOTM(sessionSnapshot.getBatchIds(), monthStartTime, sessionSnapshot.getSessionId());
        if (ArrayUtils.isNotEmpty(batchSnapshotOTMs)) {

            Set<String> batchIds = new HashSet<>();

            for (BatchSnapshotOTM batchSnapshotOTM : batchSnapshotOTMs) {
                batchIds.add(batchSnapshotOTM.getBatchId());
            }

            for (String batchId : sessionSnapshot.getBatchIds()) {
                if (batchIds.contains(batchId)) {
                    continue;
                }
                batchSnapshotOTMManager.updateBatchSnapshotOnMerging(sessionSnapshot, batchId);
            }

        } else {
            logger.warn("No batchsnapshot found for the sessionsnapshot: " + sessionSnapshot);
        }
    }

    public void checkForEndBatchConsumptionAsync() {
        AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.END_BATCH_CONSUMPTION, null);

        asyncTaskFactory.executeTask(asyncTaskParams);
    }

    public void checkForEndBatchConsumption() {

        Long currentTime = System.currentTimeMillis();

        List<Batch> batches = batchDAO.getEndedBatches(currentTime);

        if (ArrayUtils.isNotEmpty(batches)) {

            for (Batch batch : batches) {
                logger.info("End batch consumption happening for: " + batch.getId());
                try {
                    enrollmentConsumptionManager.performEndBatchConsumption(batch.getId());
                } catch (Exception e) {
                    logger.error("End Batch consumption failed for: " + batch + " due to: " + e.getMessage());
                }
            }
        }

    }

    public void checkForContentOnlyBatchConsumptionAsync() {
        AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.CONTENT_BATCH_CONSUMPTION, null);

        asyncTaskFactory.executeTask(asyncTaskParams);
    }

    public void checkForContentOnlyBatchConsumption() {
        List<Batch> batches = batchDAO.getContentOnlyBatch();

        if (ArrayUtils.isNotEmpty(batches)) {
            for (Batch batch : batches) {
                logger.info("Content only batch consumption happening for: " + batch.getId());
                try {
                    enrollmentConsumptionManager.performConsumptionForContentOnlyBatch(batch.getId());
                } catch (Exception e) {
                    logger.error("Content only Batch consumption failed for: " + batch + " due to: " + e.getMessage());
                }
            }
        }

    }

    public List<String> getModularBatchIds() {

        int start = 0;
        int size = 100;
        List<Batch> resultBatches = new ArrayList<>();
        while (true) {

            List<Batch> batches = batchDAO.getModularBatch(start, size);
            if (ArrayUtils.isEmpty(batches)) {
                break;
            }
            resultBatches.addAll(batches);

            if (batches.size() < size) {
                break;
            }

            start = start + size;

        }

        List<String> result = new ArrayList<>();
        for (Batch batch : resultBatches) {
            result.add(batch.getId());
        }

        return result;

    }

    public List<String> getBatchIdsForUser(String userId) {
        int start = 0;
        int size = 100;
        List<Enrollment> enrollments = new ArrayList<>();
        while (true) {
            List<Enrollment> tempEnrollments = enrollmentDAO.getEnrollmentsForUserId(userId, start, size);

            if (tempEnrollments == null) {
                break;
            }

            enrollments.addAll(tempEnrollments);

            if (tempEnrollments.size() < size) {
                break;
            }

            start = start + size;

        }

        List<String> batchIds = new ArrayList<>();

        for (Enrollment enrollment : enrollments) {
            batchIds.add(enrollment.getBatchId());
        }

        return batchIds;

    }

    public List<String> getBatchIdsForAMandGroupName(String groupName, String amId) {
        List<Batch> batches = new ArrayList<>();

        int start = 0;
        int size = 100;
        while (true) {
            List<Batch> tempBatch = batchDAO.getBatchByGroupNameAndAMId(amId, groupName, start, size);
            if (ArrayUtils.isEmpty(tempBatch)) {
                break;
            }
            batches.addAll(tempBatch);
            if (tempBatch.size() < size) {
                break;
            }
            start = start + size;

        }

        List<String> batchIds = new ArrayList<>();

        for (Batch batch : batches) {
            batchIds.add(batch.getId());
        }

        return batchIds;

    }

    public List<BatchBasicInfo> getBatchBasicInfosForGroupName(String groupName) {

        return getBatchBasicInfosForGroupNames(Arrays.asList(groupName));

    }

    public List<BatchBasicInfo> getBatchBasicInfosForGroupNames(List<String> groupNames) {

        int start = 0;
        int size = 100;

        List<Batch> batches = new ArrayList<>();

        while (true) {

            List<Batch> tempBatches = batchDAO.getBatchByGroupNames(groupNames, start, size);
            if (ArrayUtils.isEmpty(tempBatches)) {
                break;
            }
            batches.addAll(tempBatches);

            if (tempBatches.size() < batches.size()) {
                break;
            }

            start = start + size;

        }

        if (ArrayUtils.isEmpty(batches)) {
            return new ArrayList<>();
        }

        List<BatchBasicInfo> batchBasicInfos = new ArrayList<>();

        for (Batch batch : batches) {
            Course course = courseDAO.getById(batch.getCourseId());
            BatchBasicInfo batchBasicInfo = batch.toBatchBasicInfo(batch, course, null, null);
            batchBasicInfos.add(batchBasicInfo);
        }

        return batchBasicInfos;

    }

    public Set<Long> getNonTeachersIds(String batchId) throws BadRequestException {
        Batch batch = batchDAO.getById(batchId);
        if (batch == null) {
            logger.info("batch not found for batch id " + batchId);
            throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND, "batch not found");
        }
        Set<Long> nonTeacherIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(batch.getBoardTeacherPairs())) {
            for (BoardTeacherPair teacherPair : batch.getBoardTeacherPairs()) {
                if (ArrayUtils.isNotEmpty(teacherPair.getTeachers())) {
                    for (BoardTeacher teacher : teacherPair.getTeachers()) {
                        if (!(teacher.getRole() == null
                                || BoardTeacher.BoardTeacherRole.TEACHER.equals(teacher.getRole()))) {
                            nonTeacherIds.add(teacher.getTeacherId());
                        }
                    }
                }
            }
        }
        return nonTeacherIds;
    }


    public BatchInfo TeacherUpdateForBatch(Batch previousBatch, Batch batchReq, Long callingUserId) throws BadRequestException {


        previousBatch.setTeacherIds(batchReq.getTeacherIds());
        previousBatch.setBoardTeacherPairs(batchReq.getBoardTeacherPairs());
        Course course = courseDAO.getById(previousBatch.getCourseId());
        batchDAO.save(previousBatch);

        updateEnrollments(previousBatch, course, callingUserId);
        BatchInfo batchInfo = getBatchInfo(previousBatch, course, false, true);

        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("batchId", previousBatch.getId());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.BATCH_UPDATED, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for batch updation." + e);
        }

        return batchInfo;
    }

    public List<BatchEnrolmentInfo> getEnrolmentsByBatchIds(List<String> batchIds) {
        List<BatchEnrolmentInfo> batchesResp = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(batchIds)) {
            Map<String, Set<String>> enrollmentMap = new HashMap<>();
            Query enrollquery = new Query();
            enrollquery.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(batchIds));
            enrollquery.addCriteria(Criteria.where(Enrollment.Constants.ROLE).in(Role.STUDENT));
            enrollquery.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
            enrollquery.fields().include(Enrollment.Constants.USER_ID);
            enrollquery.fields().include(Enrollment.Constants.BATCH_ID);
            List<Enrollment> enrollments = enrollmentDAO.runQuery(enrollquery, Enrollment.class);

            if (ArrayUtils.isNotEmpty(enrollments)) {
                if (enrollments.size() >= 5000) {
                    logger.info("more than 5000 enrollments found for getEnrolmentsByBatchIds with query " + enrollquery);
                }
                enrollmentMap = enrollments.stream().collect(Collectors.groupingBy(Enrollment::getBatchId, Collectors.mapping(Enrollment::getUserId, Collectors.toSet())));
            }
            for (String batchId : batchIds) {
                if (enrollmentMap.get(batchId) != null) {
                    batchesResp.add(new BatchEnrolmentInfo(batchId, enrollmentMap.get(batchId)));
                }
            }
        }
        return batchesResp;
    }

    public void autoEnrolToBatch(String batchId) throws VException, CloneNotSupportedException, InterruptedException {
        if (StringUtils.isEmpty(batchId)) {
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "No batch Id found", Level.INFO);
        }
        Batch batch = batchDAO.getById(batchId);
        if(batch.isEarlyLearning()){
            return;
        }
        Long userId = ConfigUtils.INSTANCE.getLongValue("batch.autoenrollment.userid");
        String coupon_id = ConfigUtils.INSTANCE.getStringValue("batch.autoenrollment.couponid");
        if (batch != null && !batch.isEarlyLearning()) {
            Enrollment enrollment = enrollmentDAO.getEnrollmentByUserIdAndBatch(userId, batchId, EntityStatus.ACTIVE);
            if (enrollment == null) {
                PurchaseOTFReq purchaseOTFReq = new PurchaseOTFReq();
                purchaseOTFReq.setEntityType(com.vedantu.session.pojo.EntityType.OTF);
                if (batch.getId() != null) {
                    purchaseOTFReq.setEntityId(batchId);
                }
                purchaseOTFReq.setPaymentType(PaymentType.BULK);
                purchaseOTFReq.setVedantuDiscountCouponId(coupon_id);
                purchaseOTFReq.setUserId(userId);
                if (batch.getRegistrationFee() == null) {
                    OrderInfo orderInfo = purchaseBatch(purchaseOTFReq);
                } else {
                    OrderInfo orderInfo1 = purchaseAndRegisterBatch(purchaseOTFReq);
                    if (orderInfo1 != null) {
                        OrderInfo orderInfo2 = purchaseBatch(purchaseOTFReq);
                    }
                }
            } else {
                throw new VException(ErrorCode.ALREADY_ENROLLED, "User already enrolled to batch", Level.ERROR);
            }
        } else {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "batch not found for id: " + batchId);
        }
    }

    public String getCourseIdByUsingUserIdBatchIds(GetCourseIdByUsingUserIdBatchIdsReq req) throws BadRequestException {
        req.verify();
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(req.getBatchIds()));
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(req.getUserId()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        Enrollment e = enrollmentDAO.findOne(query, Enrollment.class);
        if (e != null) {
            return e.getCourseId();
        }
        return "";
    }


    public Bundle getEarlyLearningBundleByBatch(String batchId) throws BadRequestException {
        List<String> bundleIncludeFields = Arrays.asList(Bundle.Constants.ID, Bundle.Constants.TITLE, Bundle.Constants.PRICE);
        BundleEntity bundleEntity=bundleEntityDAO.getEntityById(batchId,BundleEntity.class,Arrays.asList(BundleEntity.Constants._ID,BundleEntity.Constants.BUNDLE_ID));
        List<Bundle> bundles = bundleDAO.getEarlyLearningBundlesByPackageId(bundleEntity.getBundleId(), bundleIncludeFields, 0, 10);
        if (ArrayUtils.isNotEmpty(bundles)) {
            return bundles.get(0);
        }
        throw new BadRequestException(ErrorCode.SERVICE_ERROR, "Early Learning Bundle with given batch id not found");
    }

    public BatchInfo createEarlyLearningBatchForEnrollment(EarlyLearningBatchCreationRequest req) throws VException {

        List<Enrollment> userEnrollment = enrollmentDAO.getEnrollmentByCourseAndUser(req.getCourseId(), req.getCallingUserId().toString(), Arrays.asList(EntityStatus.ACTIVE,EntityStatus.INACTIVE));

        if(ArrayUtils.isNotEmpty(userEnrollment)){
            throw new BadRequestException(ErrorCode.ALREADY_ENROLLED,"The user already have an enrollment for the course");
        }

        Course course=courseDAO.getById(req.getCourseId());
        if (Objects.isNull(course)) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Course not found");
        }

        if(Objects.isNull(course.getEarlyLearning()) || Boolean.FALSE.equals(course.getEarlyLearning())){
            throw new BadRequestException(ErrorCode.NOT_EARLY_LEARNING_COURSE,"Specified course id is not an early learning course");
        }
        BatchReq batchReq=populateBatchBasicInfoForEarlyLearning(course,req);

        Batch batch = Batch.toBatch(batchReq);
        batch.setEarlyLearningUserId(req.getCallingUserId());
        batch.setHasSections(Boolean.FALSE);
        logger.info("Adding batch with purchase price {}",batch.getPurchasePrice());
        BatchInfo batchInfo = addBatch(batch, null, batchReq.getCallingUserId(), null,null);
        if(batch.isEarlyLearning()) {
            Map<String,String> userLead = new HashMap<>();
            userLead.put(LeadSquaredRequest.Constants.STUDENT_ID, String.valueOf(req.getCallingUserId()));
            userLead.put(LeadSquaredRequest.Constants.COURSE_ID,course.getId());
            userLead.put(LeadSquaredRequest.Constants.AIO_ID,batchInfo.getBundleInfo().getId());
            userLead.put(LeadSquaredRequest.Constants.BATCH_ID,batchInfo.getBatchId());
            userLead.put(LeadSquaredRequest.Constants.CUSTOMER_STATUS, "Payment Initiated");
            Map<String, Object> payload = new HashMap<>();
            payload.put("userLeads", userLead);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.GET_DEMO_SESSION_SUPERKIDS, payload);
            asyncTaskFactory.executeTask(params);

        }
        logger.info("Getting batch with purchase price {}",batch.getPurchasePrice());
        return batchInfo;
    }

    public void sendPaymentStartLeadSquareSuperKids(Map<String, String> userLeads) throws VException {

        String studentId = userLeads.get(LeadSquaredRequest.Constants.STUDENT_ID);
        SessionState state =SessionState.SCHEDULED;
        String serverUrl = schedulingEndpoint + "/earlyLearning/getEarlyLearningDemoSessionTime?studentId=" + studentId + "&states=" + state;
        logger.info("LeadSquared serverUrl :"+serverUrl);
        ClientResponse response = WebUtils.INSTANCE.doCall(serverUrl, HttpMethod.GET, null,true);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String output = response.getEntity(String.class);
        if( output != null) {
            Long demoStartTime = Long.valueOf(output);
            if (demoStartTime != null || demoStartTime != 0L) {
                Date date = new Date(demoStartTime);
                DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                format.setTimeZone(TimeZone.getTimeZone("GMT+5:30"));
                String formattedDate = format.format(date);
                userLeads.put(LeadSquaredRequest.Constants.DEMO_SESSION_TIME, formattedDate);
            }
        }
        logger.info("sendPaymentStartLeadSquareSuperKids userLeads : "+userLeads);
        LeadSquaredRequest request = new LeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_SUPERKIDS_COURSE_PAYMENT, null, userLeads, null);
        //String messageGorupId = userLeads.get(LeadSquaredRequest.Constants.STUDENTID) + "_UPSERT";
        awsSQSManager.sendToSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.COURSE_PAYMENT_INITIATION_SUPERKIDS, gson.toJson(request));

    }

    private BatchReq populateBatchBasicInfoForEarlyLearning(Course course, EarlyLearningBatchCreationRequest req) {

        UserBasicInfo user = fosUtils.getUserBasicInfo(req.getCallingUserId(), false);
        StringBuilder groupName=new StringBuilder();
        if(!StringUtils.isEmpty(user.getFirstName())){
            groupName.append(user.getFirstName());
        }

        if(!StringUtils.isEmpty(user.getGrade())){
            groupName.append(user.getGrade());
        }

        if(!StringUtils.isEmpty(course.getTitle())){
            groupName.append(course.getTitle());
        }

        groupName.append(course.getTotalBatches());
        BatchReq batchReq=new BatchReq();
        batchReq.setCourseId(course.getId());
        batchReq.setMaxEnrollment(1);
        batchReq.setStartTime(course.getBatchStartTime());
        batchReq.setEndTime(course.getExitDate());
        batchReq.setVisibilityState(VisibilityState.VISIBLE);
        batchReq.setSessionToolType(OTFSessionToolType.VEDANTU_WAVE);
        batchReq.setDuration(3600000L);
        batchReq.setGroupName(groupName.toString());
        batchReq.setSearchTerms(Collections.singletonList(CourseTerm.EARLY_LEARNING));
        batchReq.setSessionPlan(new ArrayList<>());
        batchReq.setPurchasePrice(course.getStartPrice());
        batchReq.setCallingUserId(req.getCallingUserId());
        batchReq.setEarlyLearning(Boolean.TRUE);
        batchReq.setBatchState(EntityStatus.ACTIVE);
        if(ArrayUtils.isNotEmpty(course.getEarlyLearningCourses())) {
            batchReq.setEarlyLearningCourses(course.getEarlyLearningCourses());
        }
        return batchReq;
    }

    public Batch getBatchWithExcludedFields(String id, List<String> excludedFields) {
        return batchDAO.getBatchWithExcludedFields(id,excludedFields);
    }

    public Long getEnrollmentCountInBatch(String batchId) {
        return enrollmentDAO.getEnrollmentCountInBatch(batchId);
    }

    public void validateSections(List<SectionPojo> sections) throws BadRequestException {

        // No sections exist validation
        if (Objects.isNull(sections) || sections.isEmpty())
            throw new BadRequestException(ErrorCode.NO_SECTION_EXISTS, "No Sections exist");

        List<String> primaryEmailIdsAcrossSections = new ArrayList<>();
        Set<String> uniqEmailIds = new HashSet<>();
        int teachersCount = 0;

        for (SectionPojo section : sections) {

            String primaryEmailId=null;

            if (StringUtils.isEmpty(section.getSectionName()) || !(com.vedantu.util.StringUtils.isAlphaNumeric(section.getSectionName())))
                throw new BadRequestException(ErrorCode.SECTION_NAME_INCORRECT, "Section name cannot be empty and should be alphanumeric with all lowercase letters without spaces");

            if(section.getSectionName().length() > Section.Constants.SECTION_NAME_MAX_LENGTH)
                throw new BadRequestException(ErrorCode.SECTION_NAME_TOO_LARGE,"Section should be smaller than " + Section.Constants.SECTION_NAME_MAX_LENGTH);

            if (StringUtils.isEmpty(section.getSectionType()))
                throw new BadRequestException(ErrorCode.SECTION_TYPE_EMPTY, "Section type cannot be empty");

            if (!(EnrollmentState.TRIAL.equals(section.getSectionType())) && !(EnrollmentState.REGULAR.equals(section.getSectionType())))
                throw new BadRequestException(ErrorCode.INVALID_SECTION_TYPE, "Invalid Section Type");

            if (section.getSize() < Section.Constants.MINIMUM_SECTION_SIZE)
                throw new BadRequestException(ErrorCode.SECTION_SIZE_OUT_OF_ALLOWED_RANGE, "Minimum section size should be " + Section.Constants.MINIMUM_SECTION_SIZE);

            if (section.getEmailIds().isEmpty())
                throw new BadRequestException(ErrorCode.NO_EMAILID_FOUND, "No CTs assigned");

            if (section.getEmailIds().size() < Section.Constants.MIN_TEACHERS_NEEDED_IN_SECTION)
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, Section.Constants.MIN_TEACHERS_NEEDED_IN_SECTION + " Email Ids are mandatory");
            else {

                int originalEmailIdsSize = section.getEmailIds().size();
                int uniqueEmailIdsSize = new HashSet<>(section.getEmailIds()).size();

                if (originalEmailIdsSize != uniqueEmailIdsSize)
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "All emailIds should be unique");

                boolean firstEmail = true;
                for (String emailId : section.getEmailIds()) {
                    if (!CommonUtils.isValidEmailId(emailId.trim()))
                        throw new BadRequestException(ErrorCode.INVALID_EMAILID, "Email Id is incorrect" + emailId);

                    // Role check as only teachers are allowed
                    UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfoFromEmail(emailId, true);

                    if(Objects.isNull(userBasicInfo) || Objects.isNull(userBasicInfo.getRole()))
                        throw new BadRequestException(ErrorCode.USER_NOT_FOUND,"No user found with this emailId");

                    if (!(userBasicInfo.getRole().equals(Role.TEACHER)))
                        throw new BadRequestException(ErrorCode.ONLY_TEACHER_EMAIL_ALLOWED, "Only teacher emails are allowed");

                    if(firstEmail)
                        primaryEmailId = emailId;
                    firstEmail = false;

                    uniqEmailIds.add(emailId);
                    teachersCount++;
                }

                if(uniqueEmailIdsSize > Section.Constants.MAX_TEACHER_EMAILS_ALLOWED)
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Only these " + Section.Constants.MAX_TEACHER_EMAILS_ALLOWED + " many teachere emails are allowed at max");

                if (section.getSectionType().equals(EnrollmentState.REGULAR) && section.getSize() > Section.Constants.MAX_REGULAR_SECTION_SIZE)
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Max section size for paid section type should not be more than " + Section.Constants.MAX_REGULAR_SECTION_SIZE);

                if (section.getSectionType().equals(EnrollmentState.TRIAL) && section.getSize() > Section.Constants.MAX_TRIAL_SECTION_SIZE)
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Max section size for free trial section type should not be more than " + Section.Constants.MAX_TRIAL_SECTION_SIZE);

            }

            if(primaryEmailId != null)
                primaryEmailIdsAcrossSections.add(primaryEmailId);
        }

        // NOTE : Unique primary teachers for all the sections within the same batch
        int size_1 = primaryEmailIdsAcrossSections.size();
        int size_2 = new HashSet<>(primaryEmailIdsAcrossSections).size();
        if(size_1 != size_2)
            throw new BadRequestException(ErrorCode.PRIMARY_TEACHER_NEEDS_TO_BE_UNIQUE,"Primary emailId (the first emailId) should be unique for different sections within the same batch");

        if(uniqEmailIds.size() < teachersCount)
            throw new BadRequestException(ErrorCode.UNIQUE_TEACHERS_NEEDED_AMONG_SECTIONS,"All teachers have to unique accross sections within a Batch");

    }


    private Map<String, List<Batch>> getBundleBatchMap(List<String> bundleIds) {
    	Map<String, List<Batch>> bundleBatchMap = new HashMap<String, List<Batch>>();
    	List<Batch> batchs = new ArrayList<Batch>();
    	for(String b: bundleIds) {
    		batchs = getBatchesFromBundleId(b);
    		bundleBatchMap.put(b, batchs);
    	}
    	logger.debug("getBundleBatchMap from BatchManager -> "+bundleBatchMap);
    	return bundleBatchMap;
    }

    private List<Batch> getBatchesFromBundleId(String b) {

    	List<String> batchIds = new ArrayList<String>();
    	if(com.vedantu.util.StringUtils.isNotEmpty(b))
    		batchIds = bundleManager.getBatchIdsByBundleId(b);

    	List<Batch> batches = new ArrayList<Batch>();
    	if(!ArrayUtils.isEmpty(batchIds)) {
    		batches = batchDAO.getBatches(batchIds, Arrays.asList(Batch.Constants.ID, Batch.Constants.START_TIME
    				, Batch.Constants.END_TIME, Batch.Constants.COURSE_ID, Batch.Constants.SESSION_PLAN, Batch.Constants.TEACHER_IDS));
    	}
    	return batches;
    }

    public BatchDetailsResponse getPresentersAndClassTimingsForApp(List<String> batchIds) {
    	BatchDetailsResponse batchDetailsResponse = new BatchDetailsResponse();
    	List<Batch> batches = new ArrayList<Batch>();
    	batches = batchDAO.getBatches(batchIds, Arrays.asList(Batch.Constants.SESSION_PLAN, Batch.Constants.TEACHER_IDS));
    	Set<String> presenterIds = new HashSet<String>();
    	presenterIds = batches.stream().map(Batch::getTeacherIds).flatMap(Collection::stream).collect(Collectors.toSet());
    	List<SessionPlanPojo> sessionPlans = new ArrayList<SessionPlanPojo>();
    	List<SessionPlanPojo> finalSessionPlans = new ArrayList<SessionPlanPojo>();
    	sessionPlans = batches.stream().map(Batch::getSessionPlan).flatMap(Collection::stream).collect(Collectors.toList());
    	Map<String, List<String>> makingSessionPlan = new HashMap<String, List<String>>();
    	sessionPlans.forEach((s) ->{
    		if(makingSessionPlan.containsKey(s.getDay())) {
    			List<String> classTimings = makingSessionPlan.get(s.getDay());
    			classTimings.addAll(s.getTimings());
    			Collections.sort(classTimings);
    			makingSessionPlan.put(s.getDay(), classTimings);
    			logger.debug("key already present and hence modifying -> "+makingSessionPlan);
    		}else {
    			makingSessionPlan.put(s.getDay(), s.getTimings());
    			logger.debug("new key added -> "+makingSessionPlan);
    		}
    	});
    	makingSessionPlan.forEach((k,v)->{
    		SessionPlanPojo session = new SessionPlanPojo();
    		session.setDay(k);
    		Collections.sort(v);
    		session.setTimings(v);
    		finalSessionPlans.add(session);
    	});

    	logger.debug("sessionPlans final before sorting -> "+sessionPlans);
    	sessionPlans.sort((o1,o2) -> Day.valueOf(o1.getDay()).compareTo(Day.valueOf(o2.getDay())));
    	logger.debug("sessionPlans final after sorting -> "+finalSessionPlans);
    	Map<String, TeacherBasicInfo> teacherBasicInfoMap = new HashMap<String, TeacherBasicInfo>();
    	teacherBasicInfoMap = fosUtils.getTeacherBasicInfoForApp(presenterIds);
    	if(null != teacherBasicInfoMap && !teacherBasicInfoMap.isEmpty()) {
    		batchDetailsResponse.setTeacherBasicInfo(new ArrayList<TeacherBasicInfo>(teacherBasicInfoMap.values()));
    	}
    	finalSessionPlans.sort((o1,o2) -> Day.valueOf(o1.getDay()).compareTo(Day.valueOf(o2.getDay())));
    	batchDetailsResponse.setSessionPlan(finalSessionPlans);

    	return batchDetailsResponse;
    }

    public BatchDetailsResponse getBatchDetailsForApp(String bundleId, String userId) {
    	logger.debug("getBatchDetailsForApp bundleId -> "+bundleId+ " userId = "+userId);
    	BatchDetailsResponse batchDetailsResponse = new BatchDetailsResponse();
    	Bundle bundle = new Bundle();
    	Batch batch = new Batch();
    	Batch maxBatch = new Batch();
    	String title = "";
    	PremiumSubscription premiumSubscription = new PremiumSubscription();
    	BundleEnrolment bundleEnrolment = new BundleEnrolment();
    	if(!StringUtils.isEmpty(bundleId)) {
    		premiumSubscription = premiumSubscriptionManager.getPremiumBundleDetailsByBundleId(bundleId);
    		logger.debug("premiumSubscription inside getBatchDetailsForApp -> "+premiumSubscription);
    	}
    	if(null != premiumSubscription && !StringUtils.isEmpty(premiumSubscription.getTitle())){
    		title = premiumSubscriptionManager.getPremiumBundleDetailsByBundleId(bundleId).getTitle();
    	}

    	final String bundleImageUrl = "https://vmkt.s3-ap-southeast-1.amazonaws.com/AIO+banner+image+compressed.png";

    	List<String> batchIds = new ArrayList<String>();
    	List<Batch> batches = new ArrayList<Batch>();

    	if(com.vedantu.util.StringUtils.isNotEmpty(bundleId)) {
    		batches = getBatchesFromBundleId(bundleId);
    	}

    	if(ArrayUtils.isNotEmpty(batches)) {
    		batchIds = batches.stream().map(Batch::getId).collect(Collectors.toList());
    		batchDetailsResponse = getPresentersAndClassTimingsForApp(batchIds);
    		batchDetailsResponse.setBatchIds(batchIds);
    		batch = batches.stream().min(Comparator.comparingLong(Batch::getStartTime)).orElse(new Batch());
    		batchDetailsResponse.setBatchStartTime(batch.getStartTime());
    		maxBatch = batches.stream().max(Comparator.comparingLong(Batch::getEndTime)).orElse(new Batch());
    		batchDetailsResponse.setBatchEndTime(maxBatch.getEndTime());

    	}
    	if(!StringUtils.isEmpty(userId) && !StringUtils.isEmpty(bundleId)) {
    		bundleEnrolment = bundleEnrolmentDAO.getSortedNonEndedBundleEnrolment(userId, bundleId);
    		logger.debug("bundleId = "+bundleId+ " "+"bundleEnrolment in getBatchDetailsForApp-> "+bundleEnrolment);
    		if(null != bundleEnrolment && !StringUtils.isEmpty(bundleEnrolment.getId()) && null != bundleEnrolment.getStatus()) {
    			logger.debug("inside bundleenrollment not null");
    			batchDetailsResponse.setIsEnrolled(true);
    			batchDetailsResponse.setEnrollmentStatus(bundleEnrolment.getStatus());
    			batchDetailsResponse.setState(bundleEnrolment.getState());
    			if(EnrollmentState.FREE_PASS.equals(bundleEnrolment.getState()) || EnrollmentState.TRIAL.equals(bundleEnrolment.getState())) {
    				batchDetailsResponse.setEnrollmentEndTime(bundleEnrolment.getFreePassvalidTill());
    			}else {
    				Integer finalValue = 0;
    				List<SubscriptionUpdate> subscriptionUpdates = subscriptionUpdateDAO.getSubscriptionsExpiryDateForUserBundle(userId, bundleId);
    				logger.debug("subscriptionUpdates -> "+subscriptionUpdates);
    		        if(ArrayUtils.isNotEmpty(subscriptionUpdates)){
    		        	finalValue = subscriptionUpdates.stream().mapToInt(SubscriptionUpdate::getValidMonths).sum();
    		        }
    		        logger.debug("finalValue of months -> "+finalValue);
    				Long enrollmentEndTime = (bundleEnrolment.getValidTill()+ (finalValue * 30L * DateTimeUtils.MILLIS_PER_DAY));
    				logger.debug("enrollmentEndTime -> "+enrollmentEndTime);
    				batchDetailsResponse.setEnrollmentEndTime(enrollmentEndTime);
    			}
    			
    		}

    	}

    	if(!StringUtils.isEmpty(bundleId)) {
    		bundle = bundleManager.getBundleById(bundleId);
    		logger.debug("bundle -> "+bundle);
    	}

    	if(null != bundle) {
    		logger.debug("bundle.getDescription() -> "+bundle.getDescription());
    		if(!StringUtils.isEmpty(bundle.getDescription()))
    				batchDetailsResponse.setCourseDescription(bundle.getDescription());
    		if(null != bundle.getValidTill())
    				batchDetailsResponse.setBundleValidTill(bundle.getValidTill());
    	}

    	if(null != bundle && !StringUtils.isEmpty(bundle.getDurationtime())) {
    		DurationTime durationTime = bundle.getDurationtime();
    		batchDetailsResponse.setCourseDuration(durationTime.getDurationTimeInApp());
    	}

    	batchDetailsResponse.setPageTitle(title);
    	batchDetailsResponse.setBundleImageUrl(bundleImageUrl);
    	batchDetailsResponse.setBundleId(bundleId);
    	if(null != premiumSubscription && !StringUtils.isEmpty(premiumSubscription.getId()))
    	batchDetailsResponse.setPremiumSubscriptionId(premiumSubscription.getId());

    	return batchDetailsResponse;
    }


    public List<BatchesResponse> getBatchesForHomeFeedCard(UserPremiumSubscriptionEnrollmentReq req) {
    	Long currentTime = System.currentTimeMillis();
    	logger.debug("UserPremiumSubscriptionEnrollmentReq in getBatchesForHomeFeedCard -> "+req);
    	List<BatchesResponse> batchesResponses = new ArrayList<BatchesResponse>();

    	if (StringUtils.isEmpty(req.getUserId())
    			|| (!Role.STUDENT.equals(req.getUserRole()))) {
    		return batchesResponses;

    	}

    	List<BundleOfBatchResponse> bOBResponses = new ArrayList<BundleOfBatchResponse>();
    	List<BundleEnrolment> enrolledBundles = new ArrayList<BundleEnrolment>();
    	List<PremiumSubscriptionResponse> premiumSubscriptionResponses = new ArrayList<PremiumSubscriptionResponse>();
    	List<String> bundleIds = new ArrayList<String>();
    	List<PremiumSubscriptionResponse> requiredBundles = new ArrayList<PremiumSubscriptionResponse>();
    	Map<String, List<Batch>> bundleBatchMap = new HashMap<String, List<Batch>>();
    	PremiumSubscriptionRequest premiumSubscriptionReq = new PremiumSubscriptionRequest();
    	if(null != req) {
    		premiumSubscriptionReq = mapper.map(req, PremiumSubscriptionRequest.class);
    	}
    	logger.debug("premiumSubscriptionReq in getBatchesForHomeFeedCard -> "+premiumSubscriptionReq);
    	premiumSubscriptionResponses = premiumSubscriptionManager.getPremiumBundlesForFilter(premiumSubscriptionReq);
    	logger.debug("premiumSubscriptionResponses in getBatchesForHomeFeedCard ->"+premiumSubscriptionResponses);

    	Map<String, PremiumSubscriptionResponse> bundleIdPremiumBundleInfoMap = new HashMap<String, PremiumSubscriptionResponse>();

    	if(ArrayUtils.isNotEmpty(premiumSubscriptionResponses)) {
    		bundleIds = premiumSubscriptionResponses.stream().map(PremiumSubscriptionResponse::getBundleId).collect(Collectors.toList());
    		if(ArrayUtils.isNotEmpty(bundleIds)) {
    			enrolledBundles = bundleEnrolmentDAO.getActiveBundleEnrolments(req.getUserId(), bundleIds, Arrays.asList(
    					BundleEnrolment.Constants.ID, BundleEnrolment.Constants.BUNDLE_ID, BundleEnrolment.Constants.STATUS, BundleEnrolment.Constants.FREE_PASS_VALID_TILL,
    					BundleEnrolment.Constants.VALID_TILL, BundleEnrolment.Constants.CREATION_TIME));
    		}

    		//All non ended most recently started bundles and upcoming bundles in the ascending order of their start date
    		requiredBundles.addAll(premiumSubscriptionResponses.stream().filter(p -> p.getValidTill().compareTo(currentTime)>0).collect(Collectors.toList()));

    		if(ArrayUtils.isNotEmpty(enrolledBundles)) {
    			final List<String> enrolledBundleIds = enrolledBundles.stream().map(BundleEnrolment::getBundleId).collect(Collectors.toList());
    			logger.debug("enrolledBundleIds in getBatchesForHomeFeedCard ->"+enrolledBundleIds);
    			requiredBundles.addAll(premiumSubscriptionResponses.stream().filter(p -> (p.getValidTill().compareTo(currentTime)>0 || enrolledBundleIds.contains(p.getBundleId()))).collect(Collectors.toList()));
    			if(ArrayUtils.isNotEmpty(requiredBundles)) {
    				requiredBundles.sort(Comparator.comparingLong(PremiumSubscriptionResponse::getValidFrom));
    			}
    			logger.debug("requiredBundles in getBatchesForHomeFeedCard -> "+requiredBundles);

    		}


    	}
    	logger.debug("enrolledBundles in getBatchesForHomeFeedCard -> "+enrolledBundles);
    	List<String> requiredBundleIds = new ArrayList<String>();
    	if(ArrayUtils.isNotEmpty(requiredBundles)) {
    		requiredBundleIds = requiredBundles.stream().map(PremiumSubscriptionResponse::getBundleId).collect(Collectors.toList());

    	}
    	for(PremiumSubscriptionResponse p: requiredBundles) {
    		bundleIdPremiumBundleInfoMap.put(p.getBundleId(), p);
    	}
    	logger.debug("bundleIdPremiumBundleInfoMap in getBatchesForHomeFeedCard -> "+bundleIdPremiumBundleInfoMap);
    	if(ArrayUtils.isNotEmpty(requiredBundleIds)) {
    		bundleBatchMap = getBundleBatchMap(requiredBundleIds);
    	}

    	logger.debug("bundleBatchMap in getBatchesForHomeFeedCard -> "+bundleBatchMap);
    	if(null != bundleBatchMap && !bundleBatchMap.isEmpty()) {

    		for(String bundleId: bundleBatchMap.keySet()) {
    			logger.debug("bundleId in for - loop ->"+bundleId);
    			BundleOfBatchResponse bundleOfBatchResponse = new BundleOfBatchResponse();
    			List<Batch> batches = bundleBatchMap.get(bundleId);
    			List<Bundle> bundle = new ArrayList<Bundle>();
    			Set<String> presenterIds = new HashSet<String>();
    			Batch batch = new Batch();
    			if(ArrayUtils.isNotEmpty(batches)) {
    				batch = batches.stream().min(Comparator.comparingLong(Batch::getStartTime)).orElse(new Batch());
    				batches.stream().forEach(b -> presenterIds.addAll(b.getTeacherIds()));
    			}
    			if(!StringUtils.isEmpty(bundleId)) {
    				bundle = bundleDAO.getBundleById(bundleId, Arrays.asList(Bundle.Constants.DURATION_TIME, Bundle.Constants._ID));
    			}
    			logger.debug("bundle in for - loop ->"+bundle);

    			Map<String, TeacherBasicInfo> teacherBasicInfos = new HashMap<String, TeacherBasicInfo>();
    			teacherBasicInfos =	fosUtils.getTeacherBasicInfoForApp(presenterIds);
    			bundleOfBatchResponse.setBundleId(bundleId);

    			if(ArrayUtils.isNotEmpty(bundle) && null != bundle.get(0) && null != bundle.get(0).getDurationtime()) {
    				DurationTime courseDuration = bundle.get(0).getDurationtime();
    				bundleOfBatchResponse.setCourseDuration(courseDuration.getDurationTimeInApp());
    			}

    			if(null != batch && null != batch.getStartTime()) {
    				bundleOfBatchResponse.setBatchStartTime(batch.getStartTime());
    			}
    			bundleOfBatchResponse.setDaysOfClass("Mon - Sun");

    			if(null != batch && ArrayUtils.isNotEmpty(batch.getSessionPlan()) && ArrayUtils.isNotEmpty(batch.getSessionPlan().get(0).getTimings()))
    				bundleOfBatchResponse.setTimings(batch.getSessionPlan().get(0).getTimings().get(0));
    			if(null != teacherBasicInfos && !teacherBasicInfos.isEmpty())
    				bundleOfBatchResponse.setTeacherBasicInfo(new ArrayList<TeacherBasicInfo>(teacherBasicInfos.values()));
    			if(ArrayUtils.isNotEmpty(enrolledBundles) && !StringUtils.isEmpty(bundleId)) {
    				for(BundleEnrolment be: enrolledBundles) {
    					logger.debug("be in enrolledBundles ->"+be+ "    ->"+bundleId);
    					if(bundleId.equalsIgnoreCase(be.getBundleId())) {
    						if(null != be.getStatus()) {
    							bundleOfBatchResponse.setIsEnrolled(true);
    							bundleOfBatchResponse.setEnrollmentStatus(be.getStatus());
    							logger.debug("bundleOfBatchResponse inside enrolledBundles in getBatchesForHomeFeedCard -> "+bundleOfBatchResponse);
    						}

    					}else {
    						bundleOfBatchResponse.setIsEnrolled(false);
    					}
    				}
    			}
    			if(null != bundleIdPremiumBundleInfoMap && !bundleIdPremiumBundleInfoMap.isEmpty() && null != bundleIdPremiumBundleInfoMap.get(bundleId)) {
    				logger.debug("bundleIdPremiumBundleInfoMap.get(bundleId) -> "+bundleIdPremiumBundleInfoMap.get(bundleId)+ "     bundle id "+bundleId);
    				if(null != bundleIdPremiumBundleInfoMap.get(bundleId).getValidFrom() && null != bundleIdPremiumBundleInfoMap.get(bundleId).getValidTill()) {
    					bundleOfBatchResponse.setBundleValidFrom(bundleIdPremiumBundleInfoMap.get(bundleId).getValidFrom());
    					bundleOfBatchResponse.setBundleValidTill(bundleIdPremiumBundleInfoMap.get(bundleId).getValidTill());
    					logger.debug("bundleOfBatchResponse in bundleIdPremiumBundleInfoMap in getBatchesForHomeFeedCard -> "+bundleIdPremiumBundleInfoMap);
    				}
    				if(!StringUtils.isEmpty(bundleIdPremiumBundleInfoMap.get(bundleId).getTitle())) {
    					bundleOfBatchResponse.setPremiumBundleTitle(bundleIdPremiumBundleInfoMap.get(bundleId).getTitle());
    				}
    				if(null != bundleIdPremiumBundleInfoMap.get(bundleId).getCourseCategory()) {
    					bundleOfBatchResponse.setCourseCategory(bundleIdPremiumBundleInfoMap.get(bundleId).getCourseCategory());
    				}
    				logger.debug("bundleOfBatchResponse  before leaving for loop   -> "+bundleOfBatchResponse);

    				bOBResponses.add(bundleOfBatchResponse);
    			}
    		}
    	}


    	logger.debug("bOBResponses   -> "+bOBResponses);
    	List<BundleOfBatchResponse> longTrackList = new ArrayList<BundleOfBatchResponse>();
    	List<BundleOfBatchResponse> fastTrackList = new ArrayList<BundleOfBatchResponse>();
    	List<BundleOfBatchResponse> crashCourseList = new ArrayList<BundleOfBatchResponse>();
    	List<BundleOfBatchResponse> examPrepList = new ArrayList<BundleOfBatchResponse>();
    	List<BundleOfBatchResponse> midSemPrepList = new ArrayList<BundleOfBatchResponse>();
    	List<BundleOfBatchResponse> revisionCourse = new ArrayList<BundleOfBatchResponse>();


    	for(BundleOfBatchResponse bob: bOBResponses) {
    		CourseCategory category = bob.getCourseCategory();
    		switch(category) {
    		case LONG_TRACK:{
    			longTrackList.add(bob);
    			break;
    		}
    		case CRASH_COURSE:{
    			crashCourseList.add(bob);
    			break;
    		}
    		case FAST_TRACK:{
    			fastTrackList.add(bob);
    			break;
    		}
    		case EXAM_PREP:{
    			examPrepList.add(bob);
    			break;
    		}
    		case MID_SEM_PREP:{
    			midSemPrepList.add(bob);
    			break;
    		}
    		case REVISION_COURSE:{
    			revisionCourse.add(bob);
    			break;
    		}
    		}
    	}

    	if(ArrayUtils.isNotEmpty(longTrackList)) {
    		logger.debug("long track found");
    		BatchesResponse batchesResponse = new BatchesResponse();
    		longTrackList = longTrackList.subList(0, longTrackList.size()>4?3:longTrackList.size());
    		longTrackList.sort(Comparator.comparingLong(BundleOfBatchResponse::getBundleValidFrom));
    		batchesResponse.setBundleOfBatchesResponses(longTrackList);
    		batchesResponse.setSectionTitle(longTrackList.get(0).getPremiumBundleTitle());
    		batchesResponse.setCourseCategory(CourseCategory.LONG_TRACK);
    		batchesResponses.add(batchesResponse);
    	}
    	if(ArrayUtils.isNotEmpty(fastTrackList)) {
    		logger.debug("fast track found");
    		BatchesResponse batchesResponse = new BatchesResponse();
    		fastTrackList = fastTrackList.subList(0, fastTrackList.size()>4?3:fastTrackList.size());
    		fastTrackList.sort(Comparator.comparingLong(BundleOfBatchResponse::getBundleValidFrom));
    		batchesResponse.setBundleOfBatchesResponses(fastTrackList);
    		batchesResponse.setSectionTitle(fastTrackList.get(0).getPremiumBundleTitle());
    		batchesResponse.setCourseCategory(CourseCategory.FAST_TRACK);
    		batchesResponses.add(batchesResponse);
    	}
    	if(ArrayUtils.isNotEmpty(crashCourseList)) {
    		logger.debug("crash course found");
    		BatchesResponse batchesResponse = new BatchesResponse();
    		crashCourseList.sort(Comparator.comparingLong(BundleOfBatchResponse::getBundleValidFrom));
    		crashCourseList = crashCourseList.subList(0, crashCourseList.size()>4?3:crashCourseList.size());
    		batchesResponse.setBundleOfBatchesResponses(crashCourseList);
    		batchesResponse.setSectionTitle(crashCourseList.get(0).getPremiumBundleTitle());
    		batchesResponse.setCourseCategory(CourseCategory.CRASH_COURSE);
    		batchesResponses.add(batchesResponse);
    	}
    	if(ArrayUtils.isNotEmpty(examPrepList)) {
    		BatchesResponse batchesResponse = new BatchesResponse();
    		examPrepList = examPrepList.subList(0, examPrepList.size()>4?3:examPrepList.size());
    		examPrepList.sort(Comparator.comparingLong(BundleOfBatchResponse::getBundleValidFrom));
    		batchesResponse.setBundleOfBatchesResponses(examPrepList);
    		batchesResponse.setCourseCategory(CourseCategory.EXAM_PREP);
    		batchesResponse.setSectionTitle(examPrepList.get(0).getPremiumBundleTitle());
    		batchesResponses.add(batchesResponse);
    	}
    	if(ArrayUtils.isNotEmpty(midSemPrepList)) {
    		BatchesResponse batchesResponse = new BatchesResponse();
    		midSemPrepList = midSemPrepList.subList(0, midSemPrepList.size()>4?3:midSemPrepList.size());
    		midSemPrepList.sort(Comparator.comparingLong(BundleOfBatchResponse::getBundleValidFrom));
    		batchesResponse.setBundleOfBatchesResponses(midSemPrepList);
    		batchesResponse.setSectionTitle(midSemPrepList.get(0).getPremiumBundleTitle());
    		batchesResponse.setCourseCategory(CourseCategory.MID_SEM_PREP);
    		batchesResponses.add(batchesResponse);
    	}
    	if(ArrayUtils.isNotEmpty(revisionCourse)) {
    		BatchesResponse batchesResponse = new BatchesResponse();
    		revisionCourse = revisionCourse.subList(0, revisionCourse.size()>4?3:revisionCourse.size());
    		revisionCourse.sort(Comparator.comparingLong(BundleOfBatchResponse::getBundleValidFrom));
    		batchesResponse.setBundleOfBatchesResponses(revisionCourse);
    		batchesResponse.setSectionTitle(revisionCourse.get(0).getPremiumBundleTitle());
    		batchesResponse.setCourseCategory(CourseCategory.REVISION_COURSE);
    		batchesResponses.add(batchesResponse);
    	}

    	return batchesResponses;

    }
    
}

