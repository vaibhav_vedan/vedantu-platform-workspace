package com.vedantu.subscription.managers;

import com.vedantu.exception.*;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.onetofew.enums.SessionState;
import com.vedantu.scheduling.pojo.calendar.AddCalendarEntryReq;
import com.vedantu.scheduling.pojo.calendar.CalendarReferenceType;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.util.*;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;
import lombok.NonNull;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.http.HttpMethod;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchFirstRegularSessionPojo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.EnrollmentUpdatePojo;
import com.vedantu.scheduling.pojo.CalendarEntry;
import com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState;
import com.vedantu.scheduling.request.session.UpdateMultipleSlotsReq;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.pojo.ExportTrialEnrollmentDueTime;
import com.vedantu.subscription.viewobject.response.BasicRes;
import static com.vedantu.util.DateTimeUtils.TIME_ZONE_IN;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.HashSet;
import java.util.Map;
import java.util.TimeZone;
import java.util.stream.Collectors;
import org.dozer.DozerBeanMapper;
import javax.mail.internet.AddressException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class EnrollmentAsyncManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private RegistrationManager registrationManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private FosUtils fosUtils;

    private Logger logger = logFactory.getLogger(EnrollmentAsyncManager.class);
    final Gson gson = new Gson();
    private static final String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final int CAL_ENTRIES_BATCH_SIZE = 100;

    @Autowired
    private DozerBeanMapper mapper;

    private static final long MAX_DAYS_AFTER_TRIAL_WITHOUT_PAYMENT = 7;

    private static final long MAX_DAYS_AFTER_DUE_DATE_WITHOUT_PAYMENT = 10;

    public void shareContentOnEnrollment(ShareContentEnrollmentReq req) throws VException {
        req.verify();
        logger.info("Sharing content for userId - {} and batchId - {}",req.getStudentId(),req.getContextId());
        logger.info("shareContentOnEnrollment - {}",req);
        ClientResponse resp = WebUtils.INSTANCE.doCall(LMS_ENDPOINT + "/curriculum/shareContentOnEnrollment",
                HttpMethod.POST, gson.toJson(req), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
    }

    public List<OTFSessionPojoUtils> getLatestSession(List<String> batchIds) {
        String url = schedulingEndpoint + "/onetofew/session/getOTFSessionForBatchIds";
        ClientResponse response = WebUtils.INSTANCE.doCall(url,
                HttpMethod.POST, gson.toJson(batchIds));
        String jsonString = response.getEntity(String.class);
        Type otfsessionpojolisttype = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();
        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfsessionpojolisttype);
        return sessions;

    }

    private List<OTFSessionPojoUtils> getSessions(String batchId,Integer start,Integer size) throws VException {
        if (StringUtils.isEmpty(batchId)) {
            throw new BadRequestException(com.vedantu.exception.ErrorCode.BAD_REQUEST_ERROR, "batchId is empty");
        }

        String getUpcomingSessionsUrl = schedulingEndpoint + "/onetofew/session/get?batchId=" + batchId;

        ClientResponse resp = WebUtils.INSTANCE.doCall(getUpcomingSessionsUrl, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type otfSessionListType = new TypeToken<List<OTFSessionPojoUtils>>() {
        }.getType();
        List<OTFSessionPojoUtils> sessions = gson.fromJson(jsonString, otfSessionListType);
        return sessions;
    }

    private static List<SessionSlot> getSessionSlots(List<OTFSessionPojoUtils> sessions) {
        // Collect slots from otf agenda
        List<SessionSlot> sessionSlots = new ArrayList<>();

        if (!CollectionUtils.isEmpty(sessions)) {
            for (OTFSessionPojoUtils session : sessions) {

                if (session != null && SessionState.SCHEDULED.equals(session.getState())) {
                    sessionSlots.add(session.createSessionSlot());
                }
            }
        }

        return sessionSlots;
    }

    public void updateCalendar(String batchId, String userId) throws VException {
        logger.info("Update Calendar for OTM : ");
        // boolean remove = EntityStatus.ACTIVE.equals(enrollmentInfo.getStatus());//remove is technically added

//        String url = ontofewEndpoint + "batch/" + enrollmentInfo.getBatchIds();
//        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
//        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
//        String jsonString = resp.getEntity(String.class);
//        logger.info(jsonString);
//        BatchInfo batchInfo = new Gson().fromJson(jsonString, BatchInfo.class);
        List<OTFSessionPojoUtils> sessions = getSessions(batchId,0,20);
        List<SessionSlot> sessionSlots = getSessionSlots(sessions);
        // Block the schedule for the sessions
        List<AddCalendarEntryReq> addCalendarEntryReqList = new ArrayList<>();
        for (SessionSlot sessionSlot : sessionSlots) {
            AddCalendarEntryReq calendarEntryReq = new AddCalendarEntryReq(userId,
                    sessionSlot.getStartTime(), sessionSlot.getEndTime(),
                    com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState.SESSION,
                    CalendarReferenceType.OTF_BATCH, batchId);
            addCalendarEntryReqList.add(calendarEntryReq);
//            if (remove) {
/*            } else {
calendarManager.unmarkCalendarEntries(updateMultipleSlotsReq);
}*/
        }

        markCalendarEntries(addCalendarEntryReqList);

    }

    public void markCalendarEntries(List<AddCalendarEntryReq> calendarEntryReqList) throws NotFoundException {

//        logger.info("entering mark state for slots user");
//        String markStateUrl = "/calendarEntry/multiple";
//        logger.info("post url is " + schedulingEndpoint + markStateUrl);
//        ClientResponse markedCalendarEntries = WebUtils.INSTANCE.doCall(schedulingEndpoint + markStateUrl,
//                HttpMethod.POST, gson.toJson(calendarEntryReqList));
//
//        String output = markedCalendarEntries.getEntity(String.class);
//        logger.info("output from mark calendar entry in platform with basics " + output);
//        Type basicResType = new TypeToken<BasicRes>() {
//        }.getType();
//        BasicRes basicRes = new Gson().fromJson(output, basicResType);
//
//        if (!basicRes.getSuccess()) {
//            throw new NotFoundException(ErrorCode.BAD_REQUEST_ERROR, "Calendar entried were not booked");
//        }
        Map<String, Object> payload = new HashMap<>();
        payload.put("mark", Boolean.TRUE);
        payload.put("req", calendarEntryReqList);
        awsSQSManager.sendToSQS(SQSQueue.CALENDAR_OPS, SQSMessageType.MARK_CALENDAR_ENTRIES, gson.toJson(payload));
    }

    // TODO-- push to CALENDAR_OPS queue without calling this api for unmarking/marking
    public void updateCalendarScheduling(EnrollmentUpdatePojo pojo) throws VException {
        if (!Objects.equals(EntityStatus.ACTIVE, pojo.getNewEnrollment().getStatus())) {
            if (!Objects.equals(Role.TEACHER, pojo.getNewEnrollment().getRole())) {
                return;
            }
        }
        String url = schedulingEndpoint + "/onetofew/session/updateAsyncEnrollmentTask";
        ClientResponse response = WebUtils.INSTANCE.doCall(url,
                HttpMethod.POST, gson.toJson(pojo));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String jsonString = response.getEntity(String.class);

    }

    public void checkTrialEnrollmentsAsync(int start, int size) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("start", (start));
        payload.put("size", size);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CHECK_TRIAL_ENROLLMENT_DUES, payload);
        asyncTaskFactory.executeTask(params);
    }

    public void exportTrialEnrollments(String email, String name, List<String> ccList) throws VException, SecurityException, IllegalArgumentException, IllegalAccessException,
            AddressException, IOException, NoSuchFieldException {

        int start = 0;
        int size = 100;

        Set<String> userIds = new HashSet<>();
        List<ExportTrialEnrollmentDueTime> exportTrials = new ArrayList<>();

        SimpleDateFormat sdf = new SimpleDateFormat("dd MMM yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        while (true) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
            query.addCriteria(Criteria.where(Enrollment.Constants.STATE).is(EnrollmentState.TRIAL));

            query.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants.CREATION_TIME));
            enrollmentDAO.setFetchParameters(query, start, size);
            List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
            start += size;
            if (ArrayUtils.isNotEmpty(enrollments)) {
                Set<String> entityIds = new HashSet<>();
                Set<String> batchIds = new HashSet<>();
                for (Enrollment enrollmentPojo : enrollments) {

                    batchIds.add(enrollmentPojo.getBatchId());
                    if (EntityType.OTM_BUNDLE.equals(enrollmentPojo.getEntityType()) || EntityType.OTM_BUNDLE_REGISTRATION.equals(enrollmentPojo.getEntityType())) {
                        entityIds.add(enrollmentPojo.getEntityId());
                        continue;
                    }

                    entityIds.add(enrollmentPojo.getBatchId());
                }

                List<BaseInstalment> baseInstalments = paymentManager.getBaseInstalments(null,
                        new ArrayList<>(entityIds));
                Map<String, BaseInstalmentInfo> dueDates = new HashMap<>();
                if (ArrayUtils.isNotEmpty(baseInstalments)) {
                    for (BaseInstalment baseInstalment : baseInstalments) {
                        dueDates.put(baseInstalment.getPurchaseEntityId(), baseInstalment.getFirstDueBaseInstalmentInfo());
                    }
                }

                Map<String, Long> batchIdSessionEndTimeMap = getFirstRegularSessionForBatchIds(batchIds);

                logger.info("dueDates map " + dueDates.toString());

                logger.info("checking each trail active enrollment");
                for (Enrollment enrollmentPojo : enrollments) {
                    ExportTrialEnrollmentDueTime exportPojo = new ExportTrialEnrollmentDueTime();
                    exportPojo.setUserId(enrollmentPojo.getUserId());
                    exportPojo.setBatchId(enrollmentPojo.getBatchId());
                    if (enrollmentPojo.getEntityType() == null) {
                        exportPojo.setEntityType("");
                    } else {
                        exportPojo.setEntityType(enrollmentPojo.getEntityType().name());
                    }
                    exportPojo.setEntityId(enrollmentPojo.getEntityId());
                    exportPojo.setEnrollmentId(enrollmentPojo.getId());
                    exportPojo.setTitle(enrollmentPojo.getEntityTitle());
                    userIds.add(enrollmentPojo.getUserId());

                    Long dueTime = null;
                    Long userId = Long.parseLong(enrollmentPojo.getUserId());
                    if (!EntityStatus.ACTIVE.equals(enrollmentPojo.getStatus())) {
                        continue;
                    }
                    List<BaseInstalmentInfo> baseInstalmentInfos = registrationManager.getApplicableBaseInstalment(userId, enrollmentPojo.getEntityId());
                    if (ArrayUtils.isNotEmpty(baseInstalmentInfos)) {
                        Collections.sort(baseInstalmentInfos, (BaseInstalmentInfo o1, BaseInstalmentInfo o2) -> o1.getDueTime().compareTo(o2.getDueTime()));
                        dueTime = baseInstalmentInfos.get(0).getDueTime();
                    }
                    if (dueTime == null && dueDates.get(enrollmentPojo.getEntityId()) != null && dueDates.get(enrollmentPojo.getEntityId()).getDueTime() > 0) {
                        dueTime = dueDates.get(enrollmentPojo.getEntityId()).getDueTime();
                    }
                    String batchId = enrollmentPojo.getBatchId();
                    if (!(EntityType.OTM_BUNDLE.equals(enrollmentPojo.getEntityType()) || EntityType.OTM_BUNDLE_REGISTRATION.equals(enrollmentPojo.getEntityType()))) {
                        if (dueTime == null && dueDates.get(batchId) != null && dueDates.get(batchId).getDueTime() > 0) {

                            dueTime = dueDates.get(batchId).getDueTime();

                        }
                    }

                    if (dueTime == null && batchIdSessionEndTimeMap.containsKey(batchId)) {
                        dueTime = batchIdSessionEndTimeMap.get(batchId);
                    } else if (dueTime == null || dueTime < enrollmentPojo.getCreationTime()) {
                        logger.info("no base instalments found for " + batchId);
                        dueTime = enrollmentPojo.getCreationTime()
                                + (MAX_DAYS_AFTER_TRIAL_WITHOUT_PAYMENT * DateTimeUtils.MILLIS_PER_DAY * 1l);
                    }
                    logger.info("dueTime is" + dueTime);

                    exportPojo.setDueTime(sdf.format(new Date(dueTime)));
                    exportTrials.add(exportPojo);

                }
            } else {
                break;
            }

        }
        Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(userIds, true);

        String[] fieldsArray = {"enrollmentId", "userId", "email", "dueTime", "title", "entityType", "batchId",
            "entityId"};
        List<String> fields = Arrays.asList(fieldsArray);

        String data = "";

        for (String field : fields) {
            data += field + ",";
        }
        data += "\n";

        for (ExportTrialEnrollmentDueTime exportTrial : exportTrials) {
            UserBasicInfo user = userMap.get(exportTrial.getUserId());
            String userEmail = "";
            if (user != null) {
                userEmail = user.getEmail();
            }
            exportTrial.setEmail(userEmail);
            if (ArrayUtils.isNotEmpty(fields)) {
                for (String fieldName : fields) {
                    Field field = ExportTrialEnrollmentDueTime.class.getField(fieldName);
                    Object fieldObj = field.get(exportTrial);
                    data += (fieldObj != null ? fieldObj.toString() : " ") + ",";
                }
            }
            data += "\n";
        }

        communicationManager.sendUserDataEmail(email, name, ccList, data, "TrialEnrollment", "TrialEnrollmentData");

    }

    public void checkTrialEnrollments(int start, int size) throws VException {
//logger.info("checkTrialEnrollments  start " + start + " size " + size);
        Long currentTime = System.currentTimeMillis();
        Long maxPostDueTime = (MAX_DAYS_AFTER_DUE_DATE_WITHOUT_PAYMENT * DateTimeUtils.MILLIS_PER_DAY * 1l);
        logger.info("maxPostDueTime : " + maxPostDueTime);
        currentTime = currentTime - maxPostDueTime;
        logger.info("currentTime : " + currentTime);
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.STUDENT));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATE).is(EnrollmentState.TRIAL));

        query.with(Sort.by(Sort.Direction.ASC, Enrollment.Constants.CREATION_TIME));
        enrollmentDAO.setFetchParameters(query, start, size);
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);

        if (ArrayUtils.isNotEmpty(enrollments)) {
            Set<String> entityIds = new HashSet<>();
            Set<String> batchIds = new HashSet<>();
            for (Enrollment enrollmentPojo : enrollments) {

                batchIds.add(enrollmentPojo.getBatchId());
                if (EntityType.OTM_BUNDLE.equals(enrollmentPojo.getEntityType()) || EntityType.OTM_BUNDLE_REGISTRATION.equals(enrollmentPojo.getEntityType())) {
                    entityIds.add(enrollmentPojo.getEntityId());
                    continue;
                }

                entityIds.add(enrollmentPojo.getBatchId());
            }

            List<BaseInstalment> baseInstalments = paymentManager.getBaseInstalments(null,
                    new ArrayList<>(entityIds));
            Map<String, BaseInstalmentInfo> dueDates = new HashMap<>();
            if (ArrayUtils.isNotEmpty(baseInstalments)) {
                for (BaseInstalment baseInstalment : baseInstalments) {
                    dueDates.put(baseInstalment.getPurchaseEntityId(), baseInstalment.getFirstDueBaseInstalmentInfo());
                }
            }

            Map<String, Long> batchIdSessionEndTimeMap = getFirstRegularSessionForBatchIds(batchIds);

            logger.info("dueDates map " + dueDates.toString());

            logger.info("checking each trail active enrollment");
            for (Enrollment enrollmentPojo : enrollments) {
                Long dueTime = null;
                Long userId = Long.parseLong(enrollmentPojo.getUserId());
                if (!EntityStatus.ACTIVE.equals(enrollmentPojo.getStatus())) {
                    continue;
                }
                List<BaseInstalmentInfo> baseInstalmentInfos = registrationManager.getApplicableBaseInstalment(userId, enrollmentPojo.getEntityId());
                if (ArrayUtils.isNotEmpty(baseInstalmentInfos)) {
                    Collections.sort(baseInstalmentInfos, (BaseInstalmentInfo o1, BaseInstalmentInfo o2) -> o1.getDueTime().compareTo(o2.getDueTime()));
                    dueTime = baseInstalmentInfos.get(0).getDueTime();
                }
                if (dueTime == null && dueDates.get(enrollmentPojo.getEntityId()) != null && dueDates.get(enrollmentPojo.getEntityId()).getDueTime() > 0) {
                    dueTime = dueDates.get(enrollmentPojo.getEntityId()).getDueTime();
                }
                String batchId = enrollmentPojo.getBatchId();
                if (!(EntityType.OTM_BUNDLE.equals(enrollmentPojo.getEntityType()) || EntityType.OTM_BUNDLE_REGISTRATION.equals(enrollmentPojo.getEntityType()))) {
                    if (dueTime == null && dueDates.get(batchId) != null && dueDates.get(batchId).getDueTime() > 0) {

                        dueTime = dueDates.get(batchId).getDueTime();

                    }
                }

                if (dueTime == null && batchIdSessionEndTimeMap.containsKey(batchId)) {
                    dueTime = batchIdSessionEndTimeMap.get(batchId);
                } else if (dueTime == null || dueTime < enrollmentPojo.getCreationTime()) {
                    logger.info("no base instalments found for " + batchId);
                    dueTime = enrollmentPojo.getCreationTime()
                            + (MAX_DAYS_AFTER_TRIAL_WITHOUT_PAYMENT * DateTimeUtils.MILLIS_PER_DAY * 1l);
                }
                logger.info("dueTime is" + dueTime);
                if (currentTime > (dueTime)) {// let it throw error for duetime
                    // null
                    logger.info("Found " + enrollmentPojo + " past the due for instalment " + dueDates.get(batchId)
                            + ", marking his enrollment inactive");
//                    if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollmentPojo.getEntityType()) || com.vedantu.session.pojo.EntityType.OTM_BUNDLE_REGISTRATION.equals(enrollmentPojo.getEntityType())) {
                    try {
                        otfBundleManager.markEnrollmentStatus(Long.parseLong(enrollmentPojo.getUserId()), enrollmentPojo.getEntityId(), EntityStatus.INACTIVE);
                    } catch (NotFoundException e) {
                        logger.info("Enrollment already marked inactive");
                        continue;
                    } catch (Exception e) {
                        logger.error("Error in marking enrollment inactive for userId " + enrollmentPojo.getUserId()
                                + " enrollmentId : " + enrollmentPojo.getId() + " with error " + e);
                        continue;
                    }

                    try {
                        communicationManager.sendOTFTrialPaymentBlocked(enrollmentPojo);
                    } catch (Exception ex) {
                        logger.error("Error in sending sendOTFTrialPaymentBlocked", ex);
                    }
                }

            }
            Map<String, Object> payload = new HashMap<>();
            payload.put("start", (start + size));
            payload.put("size", size);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.CHECK_TRIAL_ENROLLMENT_DUES, payload);
            asyncTaskFactory.executeTask(params);
        }
    }

    public void markInactive(Enrollment enrollmentPojo) {
        try {
            otfBundleManager.markEnrollmentStatus(Long.parseLong(enrollmentPojo.getUserId()), enrollmentPojo.getEntityId(), EntityStatus.INACTIVE);
        } catch (NotFoundException e) {
            logger.info("Enrollment already marked inactive");
            return;
        } catch (Exception e) {
            logger.error("Error in marking enrollment inactive for userId " + enrollmentPojo.getUserId()
                    + " enrollmentId : " + enrollmentPojo.getId() + " with error " + e);
            return;
        }

        try {
            communicationManager.sendOTFTrialPaymentBlocked(enrollmentPojo);
        } catch (Exception ex) {
            logger.error("Error in sending sendOTFTrialPaymentBlocked", ex);
        }
    }

    public Map<String, Long> getFirstRegularSessionForBatchIds(Set<String> batchIds) throws VException {
        Map<String, Long> batchIdSessionMap = new HashMap<>();
        String queryString = "";
        if (ArrayUtils.isNotEmpty(batchIds)) {
            for (String id : batchIds) {
                queryString += ("&batchIds=" + id);
            }
        }
        String url = schedulingEndpoint + "/onetofew/session/getFirstRegularSessionForBatch?" + queryString;

        ClientResponse response = WebUtils.INSTANCE.doCall(url,
                HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String jsonString = response.getEntity(String.class);
        Type listType = new TypeToken<List<BatchFirstRegularSessionPojo>>() {
        }.getType();
        List<BatchFirstRegularSessionPojo> infos = gson.fromJson(jsonString, listType);

        if (ArrayUtils.isNotEmpty(infos)) {
            //stream().collect(Collectors.toMap(EnrollmentPojo::getUserId, EnrollmentPojo::getId));
            batchIdSessionMap = infos.stream().collect(Collectors.toMap(BatchFirstRegularSessionPojo::getBatchId, BatchFirstRegularSessionPojo::getEndTime));
        }

        return batchIdSessionMap;
    }

    public void removeGTT(String userId, String batchId, EnrollmentPojo enrollmentPojo) throws VException {
        if (StringUtils.isEmpty(userId) || StringUtils.isEmpty(batchId)) {
            throw new IllegalArgumentException("userId/batchId can't be null or empty");
        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", userId);
        payload.put("batchId", batchId);
        enrollmentPojo.setLastUpdated(System.currentTimeMillis());
        payload.put("enrolmentInfo", gson.toJson(enrollmentPojo));
        awsSQSManager.sendToSQS(SQSQueue.GTT_ENROLMENT_OPS, SQSMessageType.MARK_DELETED_FOR_DEENROLL, gson.toJson(payload), userId);

    }

    public void updateCalendar(String userId, String batchId, EntityStatus status) throws VException {
        logger.info("updateCalendar - userId - {} - batchId - {} status - {}",userId,batchId,status);
        boolean remove = EntityStatus.ACTIVE.equals(status);//remove is technically added
        List<OTFSessionPojoUtils> sessions = getSessions(batchId,0,20);
        try {
            UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(
                    Long.parseLong(userId), getSessionSlots(sessions, null, remove), CalendarEntrySlotState.SESSION,
                    !remove, CalendarReferenceType.OTF_BATCH, batchId);
            if (remove) {
                markCalendarEntries(updateMultipleSlotsReq);
            } else {
                unmarkCalendarEntries(updateMultipleSlotsReq);
            }
        } catch (VException ex) {
            logger.error("Error blocking calendar for otf batch - " + ex.toString());
        }

    }


    public void unmarkCalendar(String userId, String batchId) throws VException {
        List<OTFSessionPojoUtils> sessions = getSessions(batchId,0,200);
        try {
            UpdateMultipleSlotsReq updateMultipleSlotsReq = new UpdateMultipleSlotsReq(
                    Long.parseLong(userId), getSessionSlots(sessions, null,false), CalendarEntrySlotState.SESSION,
                    true, CalendarReferenceType.OTF_BATCH, batchId);

                unmarkCalendarEntries(updateMultipleSlotsReq);

        } catch (VException ex) {
            logger.error("Error blocking calendar for otf batch - " + ex.toString());
        }

    }

    private static List<SessionSlot> getSessionSlots(List<OTFSessionPojoUtils> sessions, String teacherId, Boolean scheduleCheck) {
        // Collect slots from otf agenda
        List<SessionSlot> sessionSlots = new ArrayList<>();

        if (!org.springframework.util.CollectionUtils.isEmpty(sessions)) {
            for (OTFSessionPojoUtils session : sessions) {
                if (session != null
                        && (StringUtils.isEmpty(teacherId) || teacherId.equals(session.getPresenter()))) {
                    if (Boolean.TRUE.equals(scheduleCheck) && !SessionState.SCHEDULED.equals(session.getState())) {
                        continue;
                    }
                    sessionSlots.add(session.createSessionSlot());
                }
            }
        }

        return sessionSlots;
    }

    public void markCalendarEntries(UpdateMultipleSlotsReq updateMultipleSlotsReq) throws VException {
        logger.info("Request : " + updateMultipleSlotsReq.toString());
        if (org.springframework.util.CollectionUtils.isEmpty(updateMultipleSlotsReq.collectErrors())) {
            logger.info("Invalid request : " + updateMultipleSlotsReq.toString());
            return;
        }

        List<AddCalendarEntryReq> request = new ArrayList<>();
        for (Long userId : updateMultipleSlotsReq.getUserIds()) {
            for (SessionSlot sessionSlot : updateMultipleSlotsReq.getSessionSlots()) {
                AddCalendarEntryReq calendarEntryReq = new AddCalendarEntryReq(String.valueOf(userId),
                        sessionSlot.getStartTime(), sessionSlot.getEndTime(),
                        com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState.SESSION,
                        updateMultipleSlotsReq.getReferenceType(), updateMultipleSlotsReq.getReferenceId());
                request.add(calendarEntryReq);
            }
        }

        markCalendarEntries(request);
    }

    public void unmarkCalendarEntries(UpdateMultipleSlotsReq updateMultipleSlotsReq) throws VException {
        logger.info("Request : " + updateMultipleSlotsReq.toString());
        if (org.springframework.util.CollectionUtils.isEmpty(updateMultipleSlotsReq.collectErrors())) {
            logger.info("Invalid request : " + updateMultipleSlotsReq.toString());
            return;
        }

        // TODO : Needs optimization. Will pick it up later
        for (Long userId : updateMultipleSlotsReq.getUserIds()) {
            for (SessionSlot sessionSlot : updateMultipleSlotsReq.getSessionSlots()) {
                AddCalendarEntryReq calendarEntryReq = new AddCalendarEntryReq(String.valueOf(userId),
                        sessionSlot.getStartTime(), sessionSlot.getEndTime(),
                        com.vedantu.scheduling.pojo.calendar.CalendarEntrySlotState.SESSION,
                        updateMultipleSlotsReq.getReferenceType(), updateMultipleSlotsReq.getReferenceId());
                unmarkCalendarEntries(calendarEntryReq);
            }
        }
    }

    public BasicRes unmarkCalendarEntries(AddCalendarEntryReq calendarEntryReq) {
        logger.info("Request : " + calendarEntryReq.toString());
        String getCalendarEntriesUrl = "/calendarEntries";
        String params = "?startTime=" + calendarEntryReq.getStartTime() + "&endTime=" + calendarEntryReq.getEndTime()
                + "&userId=" + calendarEntryReq.getUserId();

        String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
        ClientResponse getCalendarEntries = WebUtils.INSTANCE
                .doCall(schedulingEndpoint + getCalendarEntriesUrl + params, HttpMethod.GET, null);

        String outputCalendarIds = getCalendarEntries.getEntity(String.class);
        logger.info("output from get calendar entry in platform with basics " + outputCalendarIds);
        Type calendarEntryListType = new TypeToken<List<CalendarEntry>>() {
        }.getType();
        List<CalendarEntry> calendarEntriesList = new Gson().fromJson(outputCalendarIds, calendarEntryListType);
        BasicRes basicRes = new BasicRes();
        basicRes.setSuccess(true);

        long start = calendarEntryReq.getStartTime();
        long end = calendarEntryReq.getEndTime();
        for (CalendarEntry calendarEntry : calendarEntriesList) {
            start = calendarEntryReq.getStartTime();
            end = calendarEntryReq.getEndTime();

            String removeStateUrl = "/calendarEntry/" + calendarEntry.getId() + "/removeSlots";
            if (calendarEntry.getDayStartTime() >= calendarEntryReq.getStartTime()) {
                start = calendarEntry.getDayStartTime();
            }
            if (calendarEntryReq.getEndTime() >= calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY) {
                end = calendarEntry.getDayStartTime() + DateTimeUtils.MILLIS_PER_DAY;
            }
            params = "?startTime=" + start + "&endTime=" + end + "&slotState=" + calendarEntryReq.getState();

            ClientResponse removedCalendarSlots = WebUtils.INSTANCE.doCall(schedulingEndpoint + removeStateUrl + params,
                    HttpMethod.POST, null);
            String output = removedCalendarSlots.getEntity(String.class);
            logger.info("output from remove calendar entry slots in platform with basics " + output);

        }
        return basicRes;
    }

    public void createGttAttendeeDetails(Enrollment enrollment) throws VException {
        EnrollmentPojo enrollmentInfo = enrollmentManager.createEnrollmentInfo(enrollment, false);
        EnrollmentUpdatePojo req = new EnrollmentUpdatePojo();
        req.setNewEnrollment(enrollmentInfo);
        String url = schedulingEndpoint + "/onetofew/session/createAsyncGttAttendeeTask";
        ClientResponse response = WebUtils.INSTANCE.doCall(url,
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(response);
        String jsonString = response.getEntity(String.class);
    }

    // TODO push to a queue or trigger an sns - this thread might run for a long time depending on the number of course enrollments
    // don't want to use a queue here which involves a listening thread pool - as this event -course title modification- is very less likely to happen
    public void updateEntityTitleForCourseEnrolments(String courseId, String updatedEntityTitle) {
        List<Enrollment> fetchedEnrollments;
        String currLastFetchedId = "";
        while (true) {
            fetchedEnrollments = enrollmentDAO.getCourseEnrollments(courseId, Arrays.asList(EntityState.ACTIVE, EntityState.INACTIVE),
                    currLastFetchedId, Collections.singletonList(Enrollment.Constants._ID), AbstractMongoDAO.MAX_ALLOWED_FETCH_SIZE);

            if (CollectionUtils.isEmpty(fetchedEnrollments)) {
                break;
            }

            List<String> enrolmentIds = new ArrayList<>();
            for (Enrollment e : fetchedEnrollments) {
                enrolmentIds.add(e.getId());
                if (e.getId().compareTo(currLastFetchedId) > 0) {
                    currLastFetchedId = e.getId();
                }
            }

            // update entity title in enrollments
            enrollmentDAO.updateEntityTitle(enrolmentIds, updatedEntityTitle);
            if (fetchedEnrollments.size() < AbstractMongoDAO.MAX_ALLOWED_FETCH_SIZE) {
                logger.info("fetched set of docs size - {}", fetchedEnrollments.size());
                break;
            }
        }
    }
}
