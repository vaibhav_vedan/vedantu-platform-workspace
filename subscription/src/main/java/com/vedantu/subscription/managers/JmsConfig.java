/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.managers;

import com.amazon.sqs.javamessaging.SQSConnection;
import com.amazon.sqs.javamessaging.SQSConnectionFactory;
import com.amazon.sqs.javamessaging.SQSSession;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.vedantu.aws.AwsCloudWatchManager;
import com.vedantu.subscription.listeners.SQSListener;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.annotation.PreDestroy;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import java.util.*;

/**
 * @author parashar
 */

@Configuration
@EnableJms
public class JmsConfig {

    @Autowired
    private SQSListener sQSListener;

    private SQSConnection connection;

    private static final Logger logger = LogManager.getRootLogger();

    private final SQSConnectionFactory sqsConnectionFactory = SQSConnectionFactory.builder()
            .withRegion(Region.getRegion(Regions.EU_WEST_1))
            .withNumberOfMessagesToPrefetch(10).build();
    private final String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private final boolean useSqsForEnv = (!(StringUtils.isEmpty(env) || env.equals("LOCAL")));

    private List<DefaultMessageListenerContainer> containers = new ArrayList<>();

    private Set<SQSQueue> queueListToCreateAlarm = new HashSet<>();

    @Autowired
    private AwsCloudWatchManager awsCloudWatchManager;

    @Bean
    public SQSConnection sQSConnection() throws JMSException {

        String env = ConfigUtils.INSTANCE.getStringValue("environment");
        //System.out.println("creating conn");


        if (useSqsForEnv) {

            try {
                connection = sqsConnectionFactory.createConnection();
                Session session = connection.createSession(false, SQSSession.CLIENT_ACKNOWLEDGE);

                MessageConsumer dmlc = session.createConsumer(session.createQueue(SQSQueue.CONSUMPTION_QUEUE.getQueueName(env)));
                MessageConsumer triggerConsumption = session.createConsumer(session.createQueue(SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getQueueName(env)));
                MessageConsumer acadmentor = session.createConsumer(session.createQueue(SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE.getQueueName(env)));
                MessageConsumer bundleOps = session.createConsumer(session.createQueue(SQSQueue.BUNDLE_OPS.getQueueName(env)));
                MessageConsumer bundleOps2 = session.createConsumer(session.createQueue(SQSQueue.BUNDLE_OPS_2.getQueueName(env)));
                MessageConsumer batchEnrollStatusQueue = session.createConsumer(session.createQueue(SQSQueue.BATCH_ENROLL_STATUS_QUEUE.getQueueName(env)));
                MessageConsumer bundleNewEnrollOps = session.createConsumer(session.createQueue(SQSQueue.BUNDLE_NEW_ENROLL_OPS.getQueueName(env)));
                MessageConsumer bundleEnrollStatusQueue = session.createConsumer(session.createQueue(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE.getQueueName(env)));
                MessageConsumer bundleAutoEnrollOps = session.createConsumer(session.createQueue(SQSQueue.BUNDLE_AUTO_ENROLL_OPS.getQueueName(env)));
                MessageConsumer bundleCalendarOps = session.createConsumer(session.createQueue(SQSQueue.BUNDLE_CALENDAR_OPS.getQueueName(env)));
                // MessageConsumer bundleContentShareOps = session.createConsumer(session.createQueue(SQSQueue.BUNDLE_CONTENT_SHARE_OPS.getQueueName(env)));
                MessageConsumer gameJourneyOps = session.createConsumer(session.createQueue(SQSQueue.GAME_JOURNEY_OPS.getQueueName(env)));
//                MessageConsumer gameJourneySessionOps = session.createConsumer(session.createQueue(SQSQueue.GAME_JOURNEY_SESSION_OPS.getQueueName(env)));
                MessageConsumer gameJourneySessionDownloadOps = session.createConsumer(session.createQueue(SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS.getQueueName(env)));
                MessageConsumer gameJourneyTestOps = session.createConsumer(session.createQueue(SQSQueue.GAME_JOURNEY_TEST_OPS.getQueueName(env)));
                MessageConsumer subscriptionPackageOps = session.createConsumer(session.createQueue(SQSQueue.SUBSCRIPTION_PACKAGE_OPS.getQueueName(env)));
                MessageConsumer gamificationToolDownloadCSVOps = session.createConsumer(session.createQueue(SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD.getQueueName(env)));
                MessageConsumer communicateInBulkQueue = session.createConsumer(session.createQueue(SQSQueue.COMMUNICATE_IN_BULK_QUEUE.getQueueName(env)));
                dmlc.setMessageListener(sQSListener);
                triggerConsumption.setMessageListener(sQSListener);
                acadmentor.setMessageListener(sQSListener);
                bundleOps.setMessageListener(sQSListener);
                bundleOps2.setMessageListener(sQSListener);
                batchEnrollStatusQueue.setMessageListener(sQSListener);
                bundleNewEnrollOps.setMessageListener(sQSListener);
                bundleEnrollStatusQueue.setMessageListener(sQSListener);
                bundleAutoEnrollOps.setMessageListener(sQSListener);
                bundleCalendarOps.setMessageListener(sQSListener);
                // bundleContentShareOps.setMessageListener(sQSListener);
                gameJourneyOps.setMessageListener(sQSListener);
                subscriptionPackageOps.setMessageListener(sQSListener);
                gamificationToolDownloadCSVOps.setMessageListener(sQSListener);
//                gameJourneySessionOps.setMessageListener(sQSListener);
                gameJourneySessionDownloadOps.setMessageListener(sQSListener);
                gameJourneyTestOps.setMessageListener(sQSListener);
                communicateInBulkQueue.setMessageListener(sQSListener);


                //Add queues to create alarm
                queueListToCreateAlarm.add(SQSQueue.CONSUMPTION_QUEUE);
                queueListToCreateAlarm.add(SQSQueue.CONSUMPTION_DL);
                queueListToCreateAlarm.add(SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE);
                queueListToCreateAlarm.add(SQSQueue.TRIGGER_SESSION_CONSUMPTION_DL);
                queueListToCreateAlarm.add(SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE);
                queueListToCreateAlarm.add(SQSQueue.ACAD_MENTOR_DASHBOARD_DL);
                queueListToCreateAlarm.add(SQSQueue.BUNDLE_OPS);
                queueListToCreateAlarm.add(SQSQueue.BUNDLE_OPS_DL);
                queueListToCreateAlarm.add(SQSQueue.BATCH_ENROLL_STATUS_QUEUE);
                queueListToCreateAlarm.add(SQSQueue.BATCH_ENROLL_STATUS_QUEUE_DL);
                queueListToCreateAlarm.add(SQSQueue.BUNDLE_NEW_ENROLL_OPS);
                queueListToCreateAlarm.add(SQSQueue.BUNDLE_NEW_ENROLL_OPS_DL);
                queueListToCreateAlarm.add(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE);
                queueListToCreateAlarm.add(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE_DL);  queueListToCreateAlarm.add(SQSQueue.BUNDLE_OPS_2);
                queueListToCreateAlarm.add(SQSQueue.BUNDLE_OPS_2_DL);
                queueListToCreateAlarm.add(SQSQueue.GAME_JOURNEY_OPS);
                queueListToCreateAlarm.add(SQSQueue.GAME_JOURNEY_OPS_DL);
                queueListToCreateAlarm.add(SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS);
                queueListToCreateAlarm.add(SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS_DL);
                queueListToCreateAlarm.add(SQSQueue.GAME_JOURNEY_TEST_OPS);
                queueListToCreateAlarm.add(SQSQueue.GAME_JOURNEY_TEST_OPS_DL);
                queueListToCreateAlarm.add(SQSQueue.SUBSCRIPTION_PACKAGE_OPS);
                queueListToCreateAlarm.add(SQSQueue.SUBSCRIPTION_PACKAGE_OPS_DL);
                queueListToCreateAlarm.add(SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD);
                queueListToCreateAlarm.add(SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD_DL);
                queueListToCreateAlarm.add(SQSQueue.COMMUNICATE_IN_BULK_QUEUE);
                queueListToCreateAlarm.add(SQSQueue.COMMUNICATE_IN_BULK_DL_QUEUE);

                connection.start();
                awsCloudWatchManager.createAlarms(queueListToCreateAlarm);
                logger.info("JMS Bean created");

            } catch (Exception e) {
                logger.error(e);
            }

            return connection;
        } else {
            return null;
        }
    }


    public DefaultMessageListenerContainer generateDMLC(SQSQueue queue, MessageListener _sqsListener) {
        if (useSqsForEnv) {
            logger.info("creating dmlc for " + queue);
            DefaultMessageListenerContainer dmlc = new DefaultMessageListenerContainer();
            dmlc.setConnectionFactory(sqsConnectionFactory);
            dmlc.setDestinationName(queue.getQueueName(env));
            dmlc.setMessageListener(_sqsListener);
            dmlc.setConcurrency(queue.getMaxConcurrency());//dmlc.setConcurrency("3-10");
            dmlc.setSessionAcknowledgeMode(Session.CLIENT_ACKNOWLEDGE);
            containers.add(dmlc);

            return dmlc;
        } else {
            return null;
        }
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (connection != null) {
                connection.close();
            }
            if (ArrayUtils.isNotEmpty(containers)) {
                containers.stream().filter(Objects::nonNull).forEachOrdered((dmlc) -> {
                    logger.info("destroying dmlc for " + dmlc.getDestinationName());
                    dmlc.destroy();
                });
            }
        } catch (Exception e) {
            logger.error("Error in closing sqs connection ", e);
        }
    }
}