/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.*;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OTMRefundAdjustmentRes;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.PayInstalmentRes;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.util.*;
import com.vedantu.util.enums.ResponseCode;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author pranavm
 */
@Service
public class PaymentManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PaymentManager.class);

    @Autowired
    private DozerBeanMapper mapper;

    private final Gson gson = new Gson();

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    public void updateBaseInstalment(BaseInstalment baseInstalment) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/updateBaseInstalment";
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.POST,
                gson.toJson(baseInstalment));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("response of updateBaseInstalment " + jsonString);
    }

    public BaseInstalment getBaseInstalment(InstalmentPurchaseEntity instalmentPurchaseEntity,
            String purchaseEntityId, Long userId)
            throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseInstalment?instalmentPurchaseEntity=" + instalmentPurchaseEntity
                + "&purchaseEntityId=" + purchaseEntityId;
        if (userId != null) {
            url += "&userId=" + userId;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);

        String jsonString = resp.getEntity(String.class);

        BaseInstalment response = gson.fromJson(jsonString, BaseInstalment.class);
        logger.info("EXIT " + response);
        return response;
    }

    public List<BaseInstalment> getBaseInstalments(InstalmentPurchaseEntity instalmentPurchaseEntity,
            List<String> purchaseEntityIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String queryString = "purchaseEntityIds=" + org.springframework.util.StringUtils.collectionToDelimitedString(purchaseEntityIds, ",");
        if (instalmentPurchaseEntity != null) {
            queryString += "&instalmentPurchaseEntity=" + instalmentPurchaseEntity;
        }
        String url = "/payment/getBaseInstalments?" + queryString;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<BaseInstalment>>() {
        }.getType();
        List<BaseInstalment> response = gson.fromJson(jsonString, listType);
        logger.info("EXIT " + response);
        return response;
    }

    public Map<String, List<DashBoardInstalmentInfo>> getIntallmentInfosForContextIds(List<String> ids, String userId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String queryString = "userId=" + userId + "&ids=" + org.springframework.util.StringUtils.collectionToDelimitedString(ids, ",");
        String url = "/payment/getInstallmentsByContextIds?" + queryString;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<Map<String, List<DashBoardInstalmentInfo>>>() {
        }.getType();
        Map<String, List<DashBoardInstalmentInfo>> response = gson.fromJson(jsonString, listType);
        logger.info("EXIT " + response);
        return response;
    }

    public Map<String, Orders> getOrderInfosForEntityIds(List<String> ids, String userId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String queryString = "userId=" + userId + "&ids=" + org.springframework.util.StringUtils.collectionToDelimitedString(ids, ",");
        String url = "/payment/getOrdersByEntityIds?" + queryString;
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<Orders>>() {
        }.getType();
        List<Orders> response = gson.fromJson(jsonString, listType);
        logger.info("response from dinero " + response);
        Map<String, Orders> orderInfoMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(response)) {
            for (Orders orderInfo : response) {
                List<OrderedItem> orderItemInfos = orderInfo.getItems();
                if (ArrayUtils.isNotEmpty(orderItemInfos)) {
                    OrderedItem item = orderItemInfos.get(0);
                    String key = item.getEntityId();
                    if (StringUtils.isNotEmpty(key)) {
                        orderInfoMap.put(key, orderInfo);
                    }
                }
            }
        }
        return orderInfoMap;
    }

    public OrderInfo checkAndGetAccountInfoForPayment(BuyItemsReqNew req) throws VException {
        req.verify();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/checkAndGetAccountInfoForPayment",
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        OrderInfo res = gson.fromJson(jsonString, OrderInfo.class);
        return res;
    }

    public OrderInfo processOrderAfterPayment(String id, ProcessPaymentReq req) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/processOrderAfterPayment/" + id,
                HttpMethod.POST, gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        OrderInfo res = gson.fromJson(jsonString, OrderInfo.class);
        return res;
    }

    public List<InstalmentInfo> processInstalmentAfterPayment(String id) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/processInstalmentAfterPayment/" + id,
                HttpMethod.POST, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        Type listType = new TypeToken<ArrayList<InstalmentInfo>>() {
        }.getType();
        List<InstalmentInfo> instalments = gson.fromJson(jsonString, listType);
        return instalments;
    }

    public PayInstalmentRes payInstalment(PayInstalmentReq req) throws VException, IOException {
        req.verify();
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/payInstalment", HttpMethod.POST,
                gson.toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        PayInstalmentRes response = gson.fromJson(jsonString, PayInstalmentRes.class);
        logger.info("PayInstalmentRes :" + jsonString);
        if (Boolean.TRUE.equals(req.getNewFlow()) && !response.getNeedRecharge()
                && response.getInstalmentInfo() != null) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("instalmentInfo", response.getInstalmentInfo());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.PROCESS_INSTALMENT_PAYMENT,
                    payload);
            asyncTaskFactory.executeTask(params);
        }
        return response;
    }

    public InstalmentInfo getLastestPaidInstalment(String orderId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/getLatestPaidInstalment?orderId=" + orderId, HttpMethod.GET,
                null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        InstalmentInfo instalmentInfo = gson.fromJson(jsonString, InstalmentInfo.class);

        return instalmentInfo;
    }

    public OrderInfo getOrderInfo(String orderId) throws NotFoundException, VException {
        List<String> orderIds = new ArrayList<>();
        orderIds.add(orderId);
        List<OrderInfo> orderInfos = getOrderInfo(orderIds);
        if (ArrayUtils.isEmpty(orderInfos)) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "");
        }

        return orderInfos.get(0);

    }

    public List<OrderInfo> getOrderInfo(List<String> orderIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT") + "/payment/getOrderInfos";

        int init = 0;

        if (ArrayUtils.isEmpty(orderIds)) {
            return new ArrayList<>();
        }

        for (String orderId : orderIds) {
            if (init == 0) {
                dineroEndpoint += "?orderIds=" + orderId;
                init = 1;
            } else {
                dineroEndpoint += "&orderIds=" + orderId;
            }
        }

        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type orderInfoListType = new TypeToken<ArrayList<OrderInfo>>() {
        }.getType();
        List<OrderInfo> orderInfoList = gson.fromJson(jsonString, orderInfoListType);

        return orderInfoList;
    }

    public Boolean resetInstalmentsPostPayment(String entityId, EntityType entityType, String userId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        JSONObject requestParams = new JSONObject();
        requestParams.put("entityId", entityId);
        requestParams.put("userId", userId);
        requestParams.put("entityType", entityType.name());
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/resetOrderInstallmentStatePostPayment",
                HttpMethod.POST, requestParams.toString());
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        BasicResponse state = gson.fromJson(jsonString, BasicResponse.class);
        if (ResponseCode.SUCCESS.equals(state.getResponseCode())) {
            return true;
        } else {
            return false;
        }
    }

    public List<InstalmentInfo> getInstalmentsForOrderIds(List<String> orderIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        List<InstalmentInfo> instalments = new ArrayList<>();
        if (ArrayUtils.isEmpty(orderIds)) {
            return instalments;
        }
        String queryString = "";
        if (ArrayUtils.isNotEmpty(orderIds)) {
            for (String id : orderIds) {
                queryString += ("&orderIds=" + id);
            }
        }
        String url = dineroEndpoint + "/payment/getInstalmentsForOrderIds?" + queryString;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<InstalmentInfo>>() {
        }.getType();
        List<InstalmentInfo> slist = gson.fromJson(jsonString, listType);
        logger.info("instalments " + jsonString);

        return slist;
    }

    public OTMRefundAdjustmentRes makeRefundAdjustment(OTMRefundAdjustmentReq oTMRefundAdjustmentReq) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

        String url = dineroEndpoint + "/account/makeRefundAdjustment";

        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, gson.toJson(oTMRefundAdjustmentReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        OTMRefundAdjustmentRes oTMRefundAdjustmentRes = gson.fromJson(jsonString, OTMRefundAdjustmentRes.class);
        return oTMRefundAdjustmentRes;

    }

    public void updateDeliverableData(UpdateDeliverableDataReq updateDeliverableDataReq) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/payment/updateDeliverableData",
                HttpMethod.POST, gson.toJson(updateDeliverableDataReq));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info(jsonString);
    }

    public void updateDeliverableEntityIdInOrder(String orderId, String deliverableEntityId, DeliverableEntityType type, String courseId)
            throws VException {
        UpdateDeliverableEntityIdInOrderReq req = new UpdateDeliverableEntityIdInOrderReq(orderId, deliverableEntityId, type);
        req.setCourseId(courseId);
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + "/payment/updateDeliverableEntityIdInOrder",
                HttpMethod.POST,
                gson.toJson(req));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Exiting: " + jsonString);
    }

}
