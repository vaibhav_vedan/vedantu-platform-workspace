package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.request.ProcessPaymentReq;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.enums.EntityType;
import com.vedantu.onetofew.pojo.CourseInfo;
import com.vedantu.onetofew.pojo.StudentSlotPreferencePojo;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.request.GetCoursesReq;
import com.vedantu.onetofew.request.PurchaseOTFReq;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.BundlePackageDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.RegistrationDAO;
import com.vedantu.subscription.dao.StudentSlotPreferenceDAO;
import com.vedantu.subscription.entities.mongo.BundlePackage;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.Registration;
import com.vedantu.subscription.entities.mongo.StudentSlotPreference;
import com.vedantu.subscription.viewobject.request.ChangeStudentSlotReq;
import com.vedantu.subscription.pojo.CourseMobileRes;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.viewobject.response.GetCourseRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;
import java.lang.reflect.Type;
import org.dozer.DozerBeanMapper;
import java.util.Arrays;
import java.util.Comparator;
import org.elasticsearch.common.lang3.StringUtils;
import org.springframework.http.HttpMethod;
import springfox.documentation.spring.web.json.Json;

@Service
public class CourseManager {

    @Autowired
    private CourseDAO courseDAO;

    @Autowired
    private BatchManager batchManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private StudentSlotPreferenceDAO studentSlotPreferenceDAO;

    @Autowired
    private RegistrationDAO registrationDAO;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private BundlePackageDAO bundlePackageDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CourseManager.class);

    @Autowired
    private FosUtils fosUtils;

    private final Gson gson = new Gson();

    private static String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private static String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    private static final String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("web.baseUrl");
    private static final String COURSE_URL = ConfigUtils.INSTANCE.getStringValue("otf_course_url");
    private static final String BUNDLE_PACKAGE_URL = ConfigUtils.INSTANCE.getStringValue("bundle_package_url");

    private static final int MOBILE_COURSE_LISTING_TIMEOUT = ConfigUtils.INSTANCE.getIntValue("mobile.course.listing.timeout.seconds");

    public Course course(Course course) throws BadRequestException {
        boolean courseAdd = StringUtils.isEmpty(course.getId());
        if (course.getParentCourseId() != null) {
            if (course.getParentCourseId().trim().compareTo("") == 0) {
                course.setParentCourseId(null);
            }
        } else {
            course.setParentCourseId(null);
        }
        if (courseAdd && course.getVisibilityState() == null) {
            course.setVisibilityState(VisibilityState.INVISIBLE);
        }

        // Should be invoked before course save.
        if (!courseAdd) {
            // handleSchedulingChanges(course);
            Course tempCourse = courseDAO.getEntityById(course.getId(), Course.class);
            Long currentTime = System.currentTimeMillis();
            if (tempCourse != null && tempCourse.getExitDate() != null && tempCourse.getExitDate() < currentTime) {
                // if time already passed, dont allow to set it
                course.setExitDate(tempCourse.getExitDate());
            } else if (course.getExitDate() != null && course.getExitDate() < currentTime) {
                throw new BadRequestException(ErrorCode.EXIT_DATE_LESS_THAN_CURRENT_TIME, "new exit date can't be less than current time");
            }
            course.setCreatedBy(tempCourse.getCreatedBy());
            course.setCreationTime(tempCourse.getCreationTime());
            if (Objects.nonNull(tempCourse) && !tempCourse.getTitle().equals(course.getTitle())) {
                Map<String, Object> entityTitleChangePayload = new HashMap<>();
                entityTitleChangePayload.put("courseId", course.getId());
                entityTitleChangePayload.put("updatedTitle", course.getTitle());

                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_ENTITY_TITLE_COURSE_ENROLLMENTS, entityTitleChangePayload);
                asyncTaskFactory.executeTask(params);
            }
        }

        // Add or Update course
        course = batchManager.calculateCourseUpdates(course);
        courseDAO.save(course);
        //handleCourseEnrollments(course, courseAdd);
        try {
            Map<String, Object> payload = new HashMap<>();
            payload.put("courseId", course.getId());
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.COURSE_UPDATED, payload);
            asyncTaskFactory.executeTask(params);
        } catch (Exception e) {
            logger.info("Error in async event for batch updation." + e);
        }
        return course;
    }

    public Course getCourse(String id) throws BadRequestException {
        return getCourse(id,null,null);
    }
    public Course getCourse(String id,List<String> includeFields,List<String> excludeFields) throws BadRequestException {
        Course course = null;
        if (!StringUtils.isEmpty(id)) {
            course = courseDAO.getById(id,includeFields,excludeFields);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "id is missing");
        }
        return course;
    }

    public List<Course> getCoursesBasicInfos(List<String> courseIds) {
        return courseDAO.getCoursesBasicInfos(courseIds);
    }

    public List<String> getCoursesByGrades(List<String> grades) {
        return courseDAO.getCoursesByGrades(grades);
    }

    public GetCourseRes getCourses(GetCoursesReq req, boolean exposeEmail) {
        List<Course> courses = courseDAO.getCourses(req);
        Long totalCount = courseDAO.getCoursesCount(req);
        List<CourseInfo> courseInfos = new ArrayList<>();
        Set<String> users = new HashSet<>();
        Set<String> temp = new HashSet<>();

        logger.info(courses);

        if (courses != null) {
            for (Course course : courses) {
                temp = course.getTeacherIds();
                if (temp != null && !temp.isEmpty()) {
                    users.addAll(temp);
                }
            }

            Map<String, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMap(users, exposeEmail);

            for (Course course : courses) {
                courseInfos.add(course.toCourseInfo(course, userMap));
            }
        }

        // Populate subscribed
        Long callingUserId = req.getCallingUserId();
        if (callingUserId != null) {
            List<Enrollment> enrollments = enrollmentManager.getEnrollment(req.getCallingUserId().toString(),
                    EntityType.STANDARD);
            if (enrollments != null && !enrollments.isEmpty()) {
                List<String> courseIds = new ArrayList<>();
                for (Enrollment enrollment : enrollments) {
                    courseIds.add(enrollment.getCourseId());
                }

                for (CourseInfo courseInfo : courseInfos) {
                    if (courseIds.contains(courseInfo.getId())) {
                        courseInfo.setSubscribed(true);
                    }
                }
            }
        }

        GetCourseRes courseRes = new GetCourseRes();
        courseRes.setList(courseInfos);
        courseRes.setTotalCount(totalCount.intValue());
        return courseRes;
    }

    private void handleCourseEnrollments(Course course, boolean courseAdd) {
        Set<String> teacherIds = course.getTeacherIds();
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).is(course.getId()));
        query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(null));
        query.addCriteria(Criteria.where(Enrollment.Constants.ROLE).is(Role.TEACHER));
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        Map<String, Enrollment> enrollmentMap = new HashMap<String, Enrollment>();

        // Process the existing enrollments and mark the inactive
        // enrollments
        if (enrollments != null) {
            for (Enrollment enrollment : enrollments) {
                enrollment.setStatus(EntityStatus.INACTIVE);
                enrollmentMap.put(enrollment.getUserId(), enrollment);
            }
        }

        // Create new enrollments for remaining teachers
        for (String teacherId : teacherIds) {
            Enrollment enrollment;
            if (enrollmentMap.containsKey(teacherId)) {
                enrollment = enrollmentMap.get(teacherId);
                enrollment.setStatus(EntityStatus.ACTIVE);
            } else {
                enrollment = new Enrollment(teacherId, null, course.getId(), null, Role.TEACHER, EntityStatus.ACTIVE,
                        EntityType.STANDARD);
            }
            enrollmentMap.put(teacherId, enrollment);
        }

        // Update in db
        for (Map.Entry<String, Enrollment> enrollmentKey : enrollmentMap.entrySet()) {
            logger.info("CourseEnrollment update " + enrollmentKey.getValue().toString());
            enrollmentDAO.save(enrollmentKey.getValue());
        }
    }

    public void updateStudentPreferenceSessionSlot(StudentSlotPreferencePojo req) throws BadRequestException {

        if (StringUtils.isEmpty(req.getUserId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId");
        }

        Query query = new Query();
        if (!StringUtils.isEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(StudentSlotPreference.Constants.ENTITY_ID).is(req.getEntityId()));
        }
        if (req.getEntityType() != null) {
            query.addCriteria(Criteria.where(StudentSlotPreference.Constants.ENTITY_TYPE).is(req.getEntityType()));
        }
        query.addCriteria(Criteria.where(StudentSlotPreference.Constants.USER_ID).is(req.getUserId()));
        List<StudentSlotPreference> preferences = studentSlotPreferenceDAO.runQuery(query, StudentSlotPreference.class);
        StudentSlotPreference pref = null;
        if (ArrayUtils.isNotEmpty(preferences)) {
            logger.info("updating pref slots for " + req);
            pref = preferences.get(0);
            if (ArrayUtils.isNotEmpty(req.getPreferredSlots())) {
                pref.setPreferredSlots(req.getPreferredSlots());
            }
            if (req.getPostPayment() != null) {
                pref.setPostPayment(req.getPostPayment());
            }
        } else {
            logger.info("new pref slots for " + req);
            pref = mapper.map(req, StudentSlotPreference.class);
        }
        if (ArrayUtils.isEmpty(pref.getPreferredSlots())) {
            return;
        }
        studentSlotPreferenceDAO.save(pref);
    }

    public List<StudentSlotPreferencePojo> fetchStudentSlotPreference(String entityId,
            List<String> studentIds, Integer start, Integer size, boolean exposeEmail) {

        List<StudentSlotPreferencePojo> resps = new ArrayList<>();
        Query query = new Query();

        if (!StringUtils.isEmpty(entityId)) {
            query.addCriteria(Criteria.where(StudentSlotPreference.Constants.ENTITY_ID).is(entityId));
        }
        if (ArrayUtils.isNotEmpty(studentIds)) {
            query.addCriteria(Criteria.where(StudentSlotPreference.Constants.USER_ID).in(studentIds));
        }
        query.with(Sort.by(Sort.Direction.DESC, StudentSlotPreference.Constants.CREATION_TIME));

        studentSlotPreferenceDAO.setFetchParameters(query, start, size);

        List<StudentSlotPreference> pojos = studentSlotPreferenceDAO.runQuery(query, StudentSlotPreference.class);
        if (ArrayUtils.isNotEmpty(pojos)) {
            Set<String> userIds = new HashSet<>();
            for (StudentSlotPreference pref : pojos) {
                userIds.add(pref.getUserId());
            }
            Map<String, UserBasicInfo> userMap
                    = fosUtils.getUserBasicInfosMap(userIds, exposeEmail);
            for (StudentSlotPreference pref : pojos) {
                StudentSlotPreferencePojo pojo = mapper.map(pref, StudentSlotPreferencePojo.class);
                pojo.setUserInfo(userMap.get(pref.getUserId()));
                pojo.setId(pref.getId());
                resps.add(pojo);
            }

        }
        return resps;
    }

    public StudentSlotPreferencePojo changeStudentPreferenceSessionSlot(ChangeStudentSlotReq req) throws BadRequestException, NotFoundException {
        req.verify();

        StudentSlotPreference pref = studentSlotPreferenceDAO.getById(req.getId());
        if (pref == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "id not found");
        }
        logger.info("update preferred slots of id : " + req.getId() + " to " + req.getPreferredSlots());
        pref.setPreferredSlots(req.getPreferredSlots());
        studentSlotPreferenceDAO.save(pref);
        StudentSlotPreferencePojo pojo = mapper.map(pref, StudentSlotPreferencePojo.class);

        return pojo;
    }

    public OrderInfo purchaseAndRegisterCourse(PurchaseOTFReq req) throws VException, CloneNotSupportedException {
        req.verify();
        String courseId = req.getEntityId();

        logger.info("finding the amount to pay");
        Course course = getCourse(courseId);
        if (course == null) {
            throw new BadRequestException(ErrorCode.NOT_FOUND_ERROR, "course not found");
        }

        if (otfBundleManager._checkIfAlreadyEnrolled(req.getUserId(), new ArrayList<>(), Arrays.asList(courseId))) {
            throw new ConflictException(ErrorCode.ALREADY_ENROLLED, "User already enrolled in one of the courses");
        }
        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();

        if (course.getRegistrationFee() == null) {
            throw new BadRequestException(ErrorCode.REGISTRATION_FEE_NOT_FOUND, "No registration fee for bundle");
        }

        int amountToPay = 0;
        amountToPay = course.getRegistrationFee();
        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(courseId);
        buyItemsReqNew.setEntityType(com.vedantu.session.pojo.EntityType.OTF_COURSE_REGISTRATION);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setPaymentType(PaymentType.BULK);
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());
//        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);

        if (!orderInfo.getNeedRecharge()) {
            processCourseRegFeePayment(orderInfo.getId(), orderInfo.getUserId(),
                    course);
        }
        return orderInfo;
    }

    public void processCourseRegFeePayment(String orderId, Long userId,
            String courseId) throws VException {
        processCourseRegFeePayment(orderId, userId, getCourse(courseId));
    }

    public void processCourseRegFeePayment(String orderId, Long userId,
            Course course)
            throws VException {

        String courseId = course.getId();
        logger.info("making call for dinero operations");
        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);

        Registration reg = new Registration();
        reg.setEntityType(com.vedantu.session.pojo.EntityType.OTF_COURSE_REGISTRATION);
        reg.setEntityId(courseId);
        reg.setUserId(userId);
        reg.setOrderId(res.getId());
        registrationDAO.save(reg);

    }

    public String getMobileCoursesKey(String board, String grade) {
        if (StringUtils.isNotEmpty(board) && StringUtils.isNotEmpty(grade)) {
            return env + "_" + board.toUpperCase() + "_" + grade.toUpperCase() + "_MOBILE_COURSES";
        } else if (StringUtils.isNotEmpty(grade)) {
            return env + "_" + grade.toUpperCase() + "_MOBILE_COURSES";
        } else {
            return null;
        }
    }

    public List<Course> getAllFutureCourses(String board, String grade) {
        List<Course> courses = new ArrayList<>();

        int start = 0;
        int size = 100;
        while (true) {
            List<Course> tempCourses = courseDAO.getFutureCourseForQuery(board, grade, start, size);

            if (ArrayUtils.isEmpty(tempCourses)) {
                break;
            }

            courses.addAll(tempCourses);

            if (tempCourses.size() < size) {
                break;
            }
            start = start + size;

        }

        return courses;
    }

    public List<Course> getAllPastCourses(String board, String grade) {
        List<Course> courses = new ArrayList<>();
        Long currentTime = System.currentTimeMillis();

        int start = 0;
        int size = 100;
        while (true) {
            List<Course> tempCourses = courseDAO.getPastCourseForQuery(board, grade, start, size);

            if (ArrayUtils.isEmpty(tempCourses)) {
                break;
            }

            courses.addAll(tempCourses);

            if (tempCourses.size() < size) {
                break;
            }
            start = start + size;

        }

        return courses;
    }

    public List<BundlePackage> getAllPastBundlePackages(String board, String grade) {
        List<BundlePackage> bundlePackages = new ArrayList<>();
        Long currentTime = System.currentTimeMillis();

        int start = 0;
        int size = 100;
        while (true) {
            List<BundlePackage> tempCourses = bundlePackageDAO.getPastBundlePackages(board, grade, start, size);

            if (ArrayUtils.isEmpty(tempCourses)) {
                break;
            }

            bundlePackages.addAll(tempCourses);

            if (tempCourses.size() < size) {
                break;
            }
            start = start + size;

        }

        return bundlePackages;
    }

    public List<BundlePackage> getAllFutureBundlePackages(String board, String grade) {
        List<BundlePackage> bundlePackages = new ArrayList<>();
        Long currentTime = System.currentTimeMillis();

        int start = 0;
        int size = 100;
        while (true) {
            List<BundlePackage> tempCourses = bundlePackageDAO.getFutureBundlePackages(board, grade, start, size);

            if (ArrayUtils.isEmpty(tempCourses)) {
                break;
            }

            bundlePackages.addAll(tempCourses);

            if (tempCourses.size() < size) {
                break;
            }
            start = start + size;

        }

        return bundlePackages;
    }

    public List<CourseMobileRes> getAllFutureWebinars(String board, String grade) {
        List<CourseMobileRes> courseMobileReses = new ArrayList<>();
        try {
            String url = PLATFORM_ENDPOINT + "/cms/webinar/getWebinarsForGradeBoard?grade=" + grade + "&start=0&size=300";
            if (!StringUtils.isEmpty(board)) {
                url = url + "&board=" + board;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String respString = resp.getEntity(String.class);
            Type listType = new TypeToken<List<CourseMobileRes>>() {
            }.getType();
            courseMobileReses = gson.fromJson(respString, listType);

        } catch (Exception ex) {
            logger.error("Error in getting webinars: " + ex.getMessage());
        }

        return courseMobileReses;
    }

    public List<CourseMobileRes> getAllPastWebinars(String board, String grade) {
        List<CourseMobileRes> courseMobileReses = new ArrayList<>();
        try {
            String url = PLATFORM_ENDPOINT + "/cms/webinar/getPastWebinarsForGradeBoard?grade=" + grade + "&start=0&size=300";
            if (!StringUtils.isEmpty(board)) {
                url = url + "&board=" + board;
            }
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String respString = resp.getEntity(String.class);
            Type listType = new TypeToken<List<CourseMobileRes>>() {
            }.getType();
            courseMobileReses = gson.fromJson(respString, listType);

        } catch (Exception ex) {
            logger.error("Error in getting webinars: " + ex.getMessage());
        }

        return courseMobileReses;
    }

    public CourseMobileRes toCourseMobileRes(Course course, Map<Long, Board> boardMap, Map<String, Enrollment> enrollMap) {
        CourseMobileRes courseMobileRes = new CourseMobileRes();
        courseMobileRes.setDuration(course.getDuration().intValue());
        courseMobileRes.setPrice(course.getStartPrice());
        courseMobileRes.setStartsFrom(course.getBatchStartTime());
        courseMobileRes.setTitle(course.getTitle());
        courseMobileRes.setId(course.getId());
        courseMobileRes.setType(com.vedantu.session.pojo.EntityType.OTF_COURSE);
        Set<String> subjects = new HashSet<>();
        if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {

            for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                if (boardMap.containsKey(boardTeacherPair.getBoardId())) {
                    subjects.add(boardMap.get(boardTeacherPair.getBoardId()).getName().toUpperCase());
                }
            }

        }
        courseMobileRes.setWebLink(FOS_ENDPOINT + String.format(COURSE_URL, course.getId()));
        courseMobileRes.setSubjects(subjects);
        if (enrollMap != null && enrollMap.get(course.getId()) != null) {
            courseMobileRes.setRegisteredOn(enrollMap.get(course.getId()).getCreationTime());
        }
        return courseMobileRes;

    }

    public CourseMobileRes toCourseMobileRes(BundlePackage bundlePackage, Map<Long, Board> boardMap) {
        CourseMobileRes courseMobileRes = new CourseMobileRes();
        // courseMobileRes.setDuration(bundlePackage.getDuration().intValue());
        courseMobileRes.setPrice(bundlePackage.getPrice());
        courseMobileRes.setStartsFrom(bundlePackage.getStartTime());
        courseMobileRes.setTitle(bundlePackage.getTitle());
        courseMobileRes.setId(bundlePackage.getId());
        courseMobileRes.setType(com.vedantu.session.pojo.EntityType.BUNDLE_PACKAGE);
        courseMobileRes.setDuration(bundlePackage.getDuration());
        Set<String> subjects = new HashSet<>();
        if (ArrayUtils.isNotEmpty(bundlePackage.getBoardIds())) {

            for (Long boardId : bundlePackage.getBoardIds()) {
                if (boardMap.containsKey(boardId)) {
                    subjects.add(boardMap.get(boardId).getName().toUpperCase());
                }
            }

        }
        courseMobileRes.setSubjects(subjects);
        courseMobileRes.setWebLink(FOS_ENDPOINT + String.format(BUNDLE_PACKAGE_URL, bundlePackage.getId()));

        return courseMobileRes;

    }

    public List<CourseMobileRes> getAllFutureCourseMobileRes(String board, String grade) {
        List<Course> courses = getAllFutureCourses(board, grade);
        logger.info("Courses: " + courses);
        List<BundlePackage> bundlePackages = getAllFutureBundlePackages(board, grade);
        logger.info("BundlePackages: " + bundlePackages);
        List<CourseMobileRes> webinarRes = getAllFutureWebinars(board, grade);
        logger.info("Webianrs : " + webinarRes);
        Set<Long> boardIds = new HashSet<>();

        for (Course course : courses) {
            if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                    boardIds.add(boardTeacherPair.getBoardId());
                }
            }
        }

        for (BundlePackage bundlePackage : bundlePackages) {
            if (ArrayUtils.isNotEmpty(bundlePackage.getBoardIds())) {
                boardIds.addAll(bundlePackage.getBoardIds());
            }
        }

        Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);

        List<CourseMobileRes> result = new ArrayList<>();

        for (Course course : courses) {
            result.add(toCourseMobileRes(course, boardMap, null));
        }

        for (BundlePackage bundlePackage : bundlePackages) {
            result.add(toCourseMobileRes(bundlePackage, boardMap));
        }

        for (CourseMobileRes cmr : webinarRes) {
//            if(com.vedantu.session.pojo.EntityType.WEBINAR.equals(cmr.getType())) {
                result.add(cmr);
//            }
        }
        
        result.sort(new Comparator<CourseMobileRes>() {
            @Override
            public int compare(CourseMobileRes o1, CourseMobileRes o2) {
                return o1.getStartsFrom().compareTo(o2.getStartsFrom());
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });

        return result;
    }

    public boolean areSubjectsCompatible(CourseMobileRes courseMobileRes, Set<String> subjects) {
        boolean result = false;
        Set<String> newSubjects = new HashSet<>(subjects);
        logger.info("subjects: " + newSubjects);

        if (ArrayUtils.isEmpty(courseMobileRes.getSubjects())) {
            return result;
        }

        newSubjects.retainAll(courseMobileRes.getSubjects());
        logger.info("after intersection subjects: " + newSubjects);
        if (newSubjects.size() > 0) {
            result = true;
        }

        return result;
    }

    public List<CourseMobileRes> getAllPastCourseMobileRes(String board, String grade) {

        List<Course> courses = getAllPastCourses(board, grade);
        logger.info("Courses: " + courses);
        List<BundlePackage> bundlePackages = getAllPastBundlePackages(board, grade);
        logger.info("BundlePackages: " + bundlePackages);
        List<CourseMobileRes> webinarRes = new ArrayList<>();//getAllPastWebinars(board, grade)
        logger.info("Webianrs : " + webinarRes);

        Set<Long> boardIds = new HashSet<>();

        for (Course course : courses) {
            if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                    boardIds.add(boardTeacherPair.getBoardId());
                }
            }
        }

        for (BundlePackage bundlePackage : bundlePackages) {
            if (ArrayUtils.isNotEmpty(bundlePackage.getBoardIds())) {
                boardIds.addAll(bundlePackage.getBoardIds());
            }
        }

        Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);

        List<CourseMobileRes> result = new ArrayList<>();

        for (Course course : courses) {
            result.add(toCourseMobileRes(course, boardMap, null));
        }

        for (BundlePackage bundlePackage : bundlePackages) {
            result.add(toCourseMobileRes(bundlePackage, boardMap));
        }

        result.addAll(webinarRes);

        result.sort(new Comparator<CourseMobileRes>() {
            @Override
            public int compare(CourseMobileRes o1, CourseMobileRes o2) {
                return o2.getStartsFrom().compareTo(o1.getStartsFrom());
                //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

        });

        return result;
    }

    public List<CourseMobileRes> getCourseMobileRes(String board, String grade, Set<String> subjects, Boolean free) {

//        String key = getMobileCoursesKey(board, grade);

        List<CourseMobileRes> courseMobileReses = null;
//        if (StringUtils.isNotEmpty(key)) {
//            logger.info("Redis for key for mobile courses: " + key);
//            try {
//                String stringResp = redisDAO.get(key);
//
//                Type listType = new TypeToken<List<CourseMobileRes>>() {
//                }.getType();
//                courseMobileReses = gson.fromJson(stringResp, listType);
//            } catch (Exception ex) {
//                logger.error("Error in getting redis cahced courses for grade and board: " + ex.getMessage());
//            }
//            logger.info("Redis result : " + courseMobileReses);
//        }

        if (courseMobileReses == null) {
            courseMobileReses = new ArrayList<>();
            List<CourseMobileRes> futureCourseMobileReses = getAllFutureCourseMobileRes(board, grade);
            logger.info("future courses: " + futureCourseMobileReses);
            List<CourseMobileRes> pastCourseMobileReses = getAllPastCourseMobileRes(board, grade);
            logger.info("past courses: " + pastCourseMobileReses);
            if (ArrayUtils.isNotEmpty(futureCourseMobileReses)) {
                courseMobileReses.addAll(futureCourseMobileReses);
            }

            if (ArrayUtils.isNotEmpty(pastCourseMobileReses)) {
                courseMobileReses.addAll(pastCourseMobileReses);
            }

//            if (ArrayUtils.isNotEmpty(courseMobileReses)) {
//                Long currentTime = System.currentTimeMillis();
//
//                Long nextEndTime = courseMobileReses.get(0).getStartsFrom();
//
//                if (nextEndTime > currentTime) {
//
//                    if (courseMobileReses.get(0).getDuration() != null) {
//
//                        int endTime = (int) (nextEndTime - currentTime + (long) courseMobileReses.get(0).getDuration()) / 1000;
//
////                        if (endTime < courseListingTimeout) {
////                            timeout = endTime;
////                        }
//                    }
//
//                }
//
//            }
//            try {
//                redisDAO.setex(key, gson.toJson(courseMobileReses), MOBILE_COURSE_LISTING_TIMEOUT);
//            } catch (Exception ex) {
//                logger.error("Error in setting redis cahced courses for grade and board: " + ex.getMessage());
//            }

        } else if (courseMobileReses.isEmpty()) {
            return new ArrayList<>();
        }

        Set<String> upperCaseSubjects = new HashSet<>();
        if (ArrayUtils.isNotEmpty(subjects)) {
            for (String subject : subjects) {
                upperCaseSubjects.add(subject.toUpperCase());
            }
            List<CourseMobileRes> filteredResult = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(courseMobileReses)) {

                for (CourseMobileRes courseMobileRes : courseMobileReses) {

                    boolean isFiltered = areSubjectsCompatible(courseMobileRes, upperCaseSubjects);

                    if (isFiltered) {
                        filteredResult.add(courseMobileRes);
                    }

                }

                courseMobileReses = filteredResult;
                logger.info("After filtering: " + courseMobileReses);
            }

        }

        logger.info("Free : " + free);

        List<CourseMobileRes> result = new ArrayList<>();

        if (Boolean.TRUE.equals(free)) {
            for (CourseMobileRes courseMobileRes : courseMobileReses) {
                if (!com.vedantu.session.pojo.EntityType.WEBINAR.equals(courseMobileRes.getType())) {
                    continue;
                }
                result.add(courseMobileRes);
            }
            return result;
        }

        logger.info("CourseMobileReses - " + courseMobileReses);

        return courseMobileReses;
    }

    public List<CourseMobileRes> getAllRegisteredWebinars(String userId) {
        List<CourseMobileRes> courseMobileReses = new ArrayList<>();
        try {
            String url = PLATFORM_ENDPOINT + "/cms/webinar/getWebinarsForUser?userId=" + userId + "&start=0&size=100";
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String respString = resp.getEntity(String.class);
            Type listType = new TypeToken<List<CourseMobileRes>>() {
            }.getType();
            courseMobileReses = gson.fromJson(respString, listType);
            if (ArrayUtils.isNotEmpty(courseMobileReses) && courseMobileReses.size() >= 100) {
                logger.error("Max fetch for getAllRegisteredWebinars for mobile api reached its limit, may be limit has to be increased now");
            }
        } catch (Exception ex) {
            logger.error("Error in getting webinars: " + ex.getMessage());
        }
        return courseMobileReses;
    }

    public List<CourseMobileRes> getCoursesForEnrollments(Long userId, Set<String> subjects, Boolean free) {

        GetEnrollmentsReq req = new GetEnrollmentsReq();
        req.setUserId(userId);
        req.setStatus(EntityStatus.ACTIVE);
        req.setSize(100);
        List<Enrollment> enrollments = new ArrayList<>();
        logger.info("Free : " + free);
        if (free == null || Boolean.FALSE.equals(free)) {
            enrollments = enrollmentDAO.getEnrollments(req);
        }

        Set<String> courseIds = new HashSet<>();

        List<Course> courses = new ArrayList<>();
        List<CourseMobileRes> courseMobileReses = new ArrayList<>();
        if (!ArrayUtils.isEmpty(enrollments)) {
            Set<Long> boardIds = new HashSet<>();
            Map<String, Enrollment> enrollMap = new HashMap<>();
            for (Enrollment enrollment : enrollments) {
                courseIds.add(enrollment.getCourseId());
                enrollMap.put(enrollment.getCourseId(), enrollment);
            }

            courses = courseDAO.getCoursesBasicInfos(new ArrayList<>(courseIds), Arrays.asList(Course.Constants.DURATION, Course.Constants.START_PRICE, Course.Constants.BATCH_START_TIME,
                    Course.Constants.TITLE, Course.Constants.BOARD_TEACHER_PAIRS));

            if (ArrayUtils.isNotEmpty(courses)) {
                for (Course course : courses) {

                    if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                        for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                            boardIds.add(boardTeacherPair.getBoardId());
                        }
                    }
                }

                Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);

                for (Course course : courses) {

                    CourseMobileRes courseMobileRes = toCourseMobileRes(course, boardMap, enrollMap);

                    courseMobileReses.add(courseMobileRes);

                }

            }

        }

        List<CourseMobileRes> webinarCourses = getAllRegisteredWebinars(userId.toString());

        logger.info(webinarCourses);

        courseMobileReses.addAll(webinarCourses);

        Set<String> upperCaseSubjects = new HashSet<>();
        if (ArrayUtils.isNotEmpty(subjects)) {
            for (String subject : subjects) {
                upperCaseSubjects.add(subject.toUpperCase());
            }
            List<CourseMobileRes> filteredResult = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(courseMobileReses)) {

                for (CourseMobileRes courseMobileRes : courseMobileReses) {

                    boolean isFiltered = areSubjectsCompatible(courseMobileRes, upperCaseSubjects);

                    if (isFiltered) {
                        filteredResult.add(courseMobileRes);
                    }

                }

                courseMobileReses = filteredResult;
            }

        }
        return courseMobileReses;
    }

    public List<CourseMobileRes> getCoursesForFeed(String board, String grade, int size){
        
        List<CourseMobileRes> courseMobileReses = getCourseMobileRes(null, grade, null, null);
        Long currentTime = System.currentTimeMillis();
        Long nextTwoDayTime = currentTime + (long)2*DateTimeUtils.MILLIS_PER_DAY;
        if(ArrayUtils.isEmpty(courseMobileReses)){
            return new ArrayList<>();
        }
        
        List<CourseMobileRes> result = new ArrayList<>();
        
        for(CourseMobileRes courseMobileRes : courseMobileReses){
            if(courseMobileRes.getStartsFrom() != null && courseMobileRes.getStartsFrom() > currentTime && courseMobileRes.getStartsFrom() < nextTwoDayTime){
                result.add(courseMobileRes);
            }
            if(result.size() == size){
                break;
            }
        }
        
        return result;
    }
    
}
