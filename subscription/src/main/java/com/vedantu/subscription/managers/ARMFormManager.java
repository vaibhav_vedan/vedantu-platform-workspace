package com.vedantu.subscription.managers;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.dao.ARMFormDAO;
import com.vedantu.subscription.entities.mongo.ARMForm;
import com.vedantu.subscription.request.AddARMFormReq;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.response.ARMFormInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import org.dozer.DozerBeanMapper;

@Service
public class ARMFormManager {
	
	@Autowired
    private ARMFormDAO aRMFormDAO;
	
	@Autowired
    private LogFactory logFactory;

    private static final Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ARMFormManager.class);

    @Autowired
    private DozerBeanMapper mapper;
    
    @Autowired
    private CommunicationManager communicationManager;
    
    public ARMFormInfo addARMForm(AddARMFormReq req) throws VException, UnsupportedEncodingException{
    	
     	req.verify();
     	ARMForm armForm= new ARMForm(req);
     	Boolean added = true;
     	if(StringUtils.isNotEmpty(req.getId())){
     		
     		ARMForm getForm = aRMFormDAO.getARMFormById(req.getId());
                if(getForm == null){
                    throw new NotFoundException(ErrorCode.ARM_ID_NOT_FOUND, "Arm form not found for Id: "+req.getId());
                }
     		armForm.setCreationTime(getForm.getCreationTime());
     		armForm.setCreatedBy(getForm.getCreatedBy());
     		added = false;
                if(getForm.isIsClosed()){
                    return mapper.map(getForm, ARMFormInfo.class);
                }
                
     	}

    	aRMFormDAO.save(armForm, req.getCallingUserId());
    	ARMFormInfo armFormInfo = mapper.map(armForm, ARMFormInfo.class);
    	armFormInfo.setAdded(added);
    	logger.info("armForm updated for  " + armFormInfo.getStudentId());
        
        if(StringUtils.isNotEmpty(req.getAdminComment()) && StringUtils.isNotEmpty(req.getId())){
            HashMap<String, Object> bodyScope = new HashMap<>();
            String baseUrl = ConfigUtils.INSTANCE.getStringValue("web.baseUrl"); 
            bodyScope.put("urlBase", baseUrl);
            bodyScope.put("ARMformId", req.getId());
            bodyScope.put("comment", req.getAdminComment());
            communicationManager.sendMailForARMComment(bodyScope);
        }
        
		return armFormInfo;
    	
    }
    
    public ARMForm editDeliverableARMForm(String id,DeliverableEntityType deliverableEntityType,String deliverableEntityId,Long callingUserId) throws NotFoundException{
    	if(id!=null){
	    	ARMForm armForm = aRMFormDAO.getARMFormById(id);
	    	if(armForm == null){
	    		throw new NotFoundException(ErrorCode.ARM_ID_NOT_FOUND,"ARM Form ID not found" + id);
	    	}
	    	armForm.setDeliverableEntityId(deliverableEntityId);
	    	armForm.setDeliverableEntityType(deliverableEntityType);
	    	aRMFormDAO.save(armForm, callingUserId);
	    	logger.info("Updated deliverableId in ARMForm" + armForm);
	    	return armForm;
    	}
    	else {
    		logger.info("armform_id is null");
    		return null;
    	}
    }
    
    public ARMFormInfo getARMForm(String id) throws NotFoundException{
    	ARMForm armForm = aRMFormDAO.getARMFormById(id);
    	if(armForm == null){
    		throw new NotFoundException(ErrorCode.ARM_ID_NOT_FOUND,"ARM Form ID not found" + id);
    	}
    	ARMFormInfo armFormInfo = mapper.map(armForm,ARMFormInfo.class);
    	return armFormInfo;
    	
    }
    
    public List<ARMFormInfo> getARMForms(ExportCoursePlansReq req){
    	List<ARMFormInfo> res = new ArrayList<>();
        List<ARMForm> armForms = aRMFormDAO.getARMForms(req);
        if (ArrayUtils.isNotEmpty(armForms)) {
            for (ARMForm armForm : armForms) {
                res.add(mapper.map(armForm, ARMFormInfo.class));
            }
        }
        else {
        	logger.info("no ARM Forms");
        }
        return res;
    }
    
    public List<ARMFormInfo> getARMFormByAdmin(Long adminId){
    	List<ARMFormInfo> res = new ArrayList<>();
    	List<ARMForm> armForms = aRMFormDAO.getARMFormByAdminCreationTime(adminId);
    	if (ArrayUtils.isNotEmpty(armForms)) {
            for (ARMForm armForm : armForms) {
                res.add(mapper.map(armForm, ARMFormInfo.class));
            }
        }
        else {
        	logger.info("no ARM Forms for adminId" + adminId);
        }
        return res;
    	
    }
    
    public List<ARMFormInfo> getARMFormByStartDate(Long startDate){
    	List<ARMFormInfo> res = new ArrayList<>();
    	List<ARMForm> armForms = aRMFormDAO.getARMFormByStartDate(startDate);
    	if (ArrayUtils.isNotEmpty(armForms)) {
            for (ARMForm armForm : armForms) {
                res.add(mapper.map(armForm, ARMFormInfo.class));
            }
        }
        else {
        	logger.info("no ARM Forms for startDate" + startDate);
        }
    	return res;
    }
    
    public List<ARMFormInfo> getARMFormByStudentId(Long studentId) throws BadRequestException{
    	if(studentId==null){
    		throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"studentId is null");
    	}
    	List<ARMFormInfo> res = new ArrayList<>();
    	List<ARMForm> armForms = aRMFormDAO.getARMFormByStudentId(studentId);
    	if (ArrayUtils.isNotEmpty(armForms)) {
            for (ARMForm armForm : armForms) {
                res.add(mapper.map(armForm, ARMFormInfo.class));
            }
        }
        else {
        	logger.info("no ARM Forms for studentId" + studentId);
        }
        return res;
    	
    }
    
    public ARMFormInfo closeARMForm(String id) throws NotFoundException{
        ARMForm aRMForm = aRMFormDAO.getARMFormById(id);
        if(aRMForm == null){
    		throw new NotFoundException(ErrorCode.ARM_ID_NOT_FOUND,"ARM Form ID not found" + id);            
        }
        
        aRMForm.setIsClosed(true);
        
        aRMFormDAO.save(aRMForm, null);
        
        return mapper.map(aRMForm, ARMFormInfo.class);
        
    }

}
