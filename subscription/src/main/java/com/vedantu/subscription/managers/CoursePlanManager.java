/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.vedantu.User.User;
import com.vedantu.User.UserInfo;
import com.vedantu.board.pojo.Board;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.session.pojo.*;
import com.vedantu.subscription.request.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.DashBoardInstalmentInfo;
import com.vedantu.dinero.pojo.OrderedItem;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.request.GetOrdersReq;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.enums.ContentType;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.CoursePlanDAO;
import com.vedantu.subscription.dao.CoursePlanHourTransactionDAO;
import com.vedantu.subscription.dao.CoursePlanHoursDAO;
import com.vedantu.subscription.dao.StructuredCourseDAO;
import com.vedantu.subscription.entities.mongo.CoursePlan;
import com.vedantu.subscription.entities.mongo.StructuredCourse;
import com.vedantu.subscription.entities.sql.CoursePlanHourTransaction;
import com.vedantu.subscription.entities.sql.CoursePlanHours;
import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.subscription.enums.HourTransactionContext;
import com.vedantu.subscription.enums.HourTransactionType;
import com.vedantu.subscription.enums.HourType;
import com.vedantu.subscription.pojo.CoursePlanSQSAlert;
import com.vedantu.subscription.pojo.CourseStateChangeTime;
import com.vedantu.subscription.pojo.OTOCurriculumPojo;
import com.vedantu.subscription.pojo.TeacherCourseStateCount;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.CoursePlanDashboardSessionInfo;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.subscription.response.GetCoursePlanBalanceRes;
import com.vedantu.subscription.response.GetCoursePlanReminderListRes;
import com.vedantu.subscription.response.StructuredCourseInfo;
import com.vedantu.subscription.response.UserDashboardCoursePlanRes;
import com.vedantu.subscription.viewobject.request.CompleteSessionRequest;
import com.vedantu.subscription.viewobject.request.UnlockHoursRequest;
import com.vedantu.subscription.viewobject.request.UpdateConflictDurationRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SessionModel;
import java.lang.reflect.Type;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.bson.types.ObjectId;
import org.springframework.http.HttpMethod;

/**
 *
 * @author ajith
 */
@Service
public class CoursePlanManager {

    @Autowired
    private CoursePlanDAO coursePlanDAO;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private StructuredCourseDAO structuredCourseDAO;

    @Autowired
    private CoursePlanHoursDAO coursePlanHoursDAO;

    @Autowired
    private CoursePlanHourTransactionDAO coursePlanHourTransactionDAO;

    @Autowired
    private ARMFormManager armFormManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private SqlSessionFactory sqlSessionfactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    private static final Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CoursePlanManager.class);

    @Autowired
    private DozerBeanMapper mapper;
    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");
    private static final String PLATFORM_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");

    public CoursePlanInfo addEditCoursePlan(AddEditCoursePlanReq addEditCoursePlanReq)
            throws BadRequestException, ForbiddenException, NotFoundException, ConflictException {
        CoursePlan coursePlan;
        Set<String> contentErrors = validateContentIds(addEditCoursePlanReq.getCurriculum());
        if (ArrayUtils.isNotEmpty(contentErrors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "error in testIds : " + contentErrors);
        }
        if (addEditCoursePlanReq.getId() == null) {
            addEditCoursePlanReq.verify();
            coursePlan = new CoursePlan();
            coursePlan.setState(CoursePlanEnums.CoursePlanState.PUBLISHED);
            coursePlan.setArmFormId(addEditCoursePlanReq.getArmFormId());// auto
            // publishing
            // it
            CourseStateChangeTime courseChangeTime = new CourseStateChangeTime();
            courseChangeTime.setChangeTime(System.currentTimeMillis());
            courseChangeTime.setNewState(coursePlan.getState());
            courseChangeTime.setChangedBy(addEditCoursePlanReq.getCallingUserId());
            coursePlan.setStateChangeTime(Arrays.asList(courseChangeTime));
            toCoursePlan(coursePlan, addEditCoursePlanReq);
        } else {
            coursePlan = coursePlanDAO.getEntityById(addEditCoursePlanReq.getId(), CoursePlan.class);
            if (coursePlan == null) {
                throw new NotFoundException(ErrorCode.COURSE_PLAN_NOT_FOUND,
                        "course plan not found " + addEditCoursePlanReq.getId());
            }
            if (coursePlan.getState().order > CoursePlanEnums.CoursePlanState.ENROLLED.order) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                        "Course already past enrolled state, use specific apis");
            }
            if (CoursePlanEnums.CoursePlanState.ENROLLED.equals(coursePlan.getState())) {
                // only editing curriculum and compact schedule
                coursePlan.setCompactSchedule(addEditCoursePlanReq.getCompactSchedule());
                // TODO special case for now, remove this option to editing
                // later
                coursePlan.setCurriculum(addEditCoursePlanReq.getCurriculum());
            } else {
                addEditCoursePlanReq.verify();
                toCoursePlan(coursePlan, addEditCoursePlanReq);
            }
        }
        int regularSessionsPayInRate = calculateRegularSessionsPayInRate(coursePlan);
        logger.info("regularSessionsPayInRate " + regularSessionsPayInRate);
        logger.info("payoutrate " + coursePlan.getTeacherPayoutRate());
        if (coursePlan.getRegularSessionSchedule() != null
                && coursePlan.getRegularSessionSchedule().calculateTotalHours() > 0l
                && regularSessionsPayInRate < coursePlan.getTeacherPayoutRate()) {
            throw new ForbiddenException(ErrorCode.REGULAR_PAYIN_RATE_LESS_THAN_PAYOUT_RATE, "payin rate is less than payout rate");
        }

        int trialSessionsPayInRate = calculateTrialSessionsPayInRate(coursePlan);
        logger.info("trialSessionsPayInRate " + trialSessionsPayInRate);
        logger.info("payoutrate " + coursePlan.getTeacherPayoutRate());
        if (coursePlan.getTrialSessionSchedule() != null
                && coursePlan.getTrialSessionSchedule().calculateTotalHours() > 0l
                && trialSessionsPayInRate < coursePlan.getTeacherPayoutRate()) {
//                        throw new ForbiddenException(ErrorCode.TRIAL_PAYIN_RATE_LESS_THAN_PAYOUT_RATE, "payin rate is less than payout rate");
        }

        coursePlanDAO.save(coursePlan, addEditCoursePlanReq.getCallingUserId());

        armFormManager.editDeliverableARMForm(addEditCoursePlanReq.getArmFormId(), DeliverableEntityType.COURSE_PLAN,
                coursePlan.getId(), addEditCoursePlanReq.getCallingUserId());
        CoursePlanInfo coursePlanInfo = mapper.map(coursePlan, CoursePlanInfo.class);
        return coursePlanInfo;
    }

    private void toCoursePlan(CoursePlan coursePlan, AddEditCoursePlanReq addEditCoursePlanReq)
            throws NotFoundException, BadRequestException, ConflictException {
        coursePlan.setTitle(addEditCoursePlanReq.getTitle());
        coursePlan.setTarget(addEditCoursePlanReq.getTarget());
        coursePlan.setDescription(addEditCoursePlanReq.getDescription());
        coursePlan.setFaq(addEditCoursePlanReq.getFaq());
        coursePlan.setTags(addEditCoursePlanReq.getTags());
        coursePlan.setKeyValues(addEditCoursePlanReq.getKeyValues());
        coursePlan.setDemoVideoUrl(addEditCoursePlanReq.getDemoVideoUrl());
        coursePlan.setCompactSchedule(addEditCoursePlanReq.getCompactSchedule());
        coursePlan.setNoOfWeeks(addEditCoursePlanReq.getNoOfWeeks());
        coursePlan.setAdminData(addEditCoursePlanReq.getAdminData());
        if (coursePlan.getState().order < CoursePlanEnums.CoursePlanState.TRIAL_PAYMENT_DONE.order) {
            coursePlan.setStudentId(addEditCoursePlanReq.getStudentId());
            coursePlan.setTeacherId(addEditCoursePlanReq.getTeacherId());
            coursePlan.setSubject(addEditCoursePlanReq.getSubject());
            coursePlan.setBoardId(addEditCoursePlanReq.getBoardId());
            coursePlan.setGrade(addEditCoursePlanReq.getGrade());
            coursePlan.setSuggestedTeacherIds(addEditCoursePlanReq.getSuggestedTeacherIds());
            coursePlan.setParentCourseId(addEditCoursePlanReq.getParentCourseId());
            coursePlan.setTeacherPayoutRate(addEditCoursePlanReq.getTeacherPayoutRate());
            coursePlan.setTrialRegistrationFee(addEditCoursePlanReq.getTrialRegistrationFee());
            coursePlan.setTrialSessionSchedule(addEditCoursePlanReq.getTrialSessionSchedule());
            if (StringUtils.isNotEmpty(coursePlan.getParentCourseId())) {
                StructuredCourse structuredCourse = structuredCourseDAO
                        .getStructuredCourse(coursePlan.getParentCourseId());
                if (structuredCourse == null) {
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Parent course not found");
                }
                coursePlan.setTrialRegistrationFee(structuredCourse.getRegistrationFee());
                logger.info("checking if there is already an active course plan");
                // List<CoursePlan> coursePlans =
                // coursePlanDAO.getNonDraftCoursePlans(coursePlan.getParentCourseId(),
                // coursePlan.getStudentId());
                // if (ArrayUtils.isNotEmpty(coursePlans)) {
                // for (CoursePlan plan : coursePlans) {
                // if (coursePlan.getId() == null ||
                // !coursePlan.getId().equals(plan.getId())) {
                // throw new
                // ConflictException(ErrorCode.MULTIPLE_ACTIVE_COURSE_PLANS_FOUND,
                // "For same parent course multiple active course plans found");
                // }
                // }
                // }
            }
        }
        if (coursePlan.getState().order < CoursePlanEnums.CoursePlanState.ENROLLED.order) {
            coursePlan.setRegularSessionSchedule(addEditCoursePlanReq.getRegularSessionSchedule());
            coursePlan.setPrice(addEditCoursePlanReq.getPrice());
            int hrlyrate = calculateRegularSessionsPayInRate(coursePlan);
            if (hrlyrate > 0) {
                coursePlan.setHourlyRate(hrlyrate);
            }
            coursePlan.setInstalmentsInfo(addEditCoursePlanReq.getInstalmentsInfo());
            coursePlan.setIntendedHours(addEditCoursePlanReq.getIntendedHours());
        }
        coursePlan.setCurriculum(addEditCoursePlanReq.getCurriculum());
        if (coursePlan.getRegularSessionSchedule() != null) {
            coursePlan.setScheduleType(ScheduleType.FS);
            List<SessionSlot> slots = coursePlan.getRegularSessionSchedule().getSessionSlots();
            if (ArrayUtils.isNotEmpty(slots)) {
                Collections.sort(slots, new Comparator<SessionSlot>() {
                    @Override
                    public int compare(SessionSlot o1, SessionSlot o2) {
                        return o1.getStartTime().compareTo(o2.getStartTime());
                    }
                });
                coursePlan.setStartDate(slots.get(0).getStartTime());
                coursePlan.setRegularSessionStartDate(slots.get(0).getStartTime());
                coursePlan.setSessionEndDate(slots.get(slots.size() - 1).getEndTime());
                for (SessionSlot slot : slots) {
                    slot.setModel(SessionModel.OTO);
                }
            }
        } else {
            coursePlan.setScheduleType(ScheduleType.NFS);
            coursePlan.setTotalCourseHours(addEditCoursePlanReq.getTotalCourseHours());
        }
        if (coursePlan.getTrialSessionSchedule() != null) {
            List<SessionSlot> slots = coursePlan.getTrialSessionSchedule().getSessionSlots();
            if (ArrayUtils.isNotEmpty(slots)) {
                coursePlan.setStartDate(slots.get(0).getStartTime());
                for (SessionSlot slot : slots) {
                    slot.setModel(SessionModel.TRIAL);
                }
            }
        }

        // checking for price-reg fee =instalments total
        if (ArrayUtils.isNotEmpty(coursePlan.getInstalmentsInfo())) {
            Integer totalCost = 0;
            for (BaseInstalmentInfo instalmentInfo : coursePlan.getInstalmentsInfo()) {
                totalCost += instalmentInfo.getAmount();
            }
            Integer trialFee = coursePlan.getTrialRegistrationFee() == null ? 0 : coursePlan.getTrialRegistrationFee();
            Integer p = coursePlan.getPrice() == null ? 0 : coursePlan.getPrice();
            if ((p - trialFee) != totalCost) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                        "(price - trialRegistrationFee) != instalmentTotal");
            }
        }

    }

    public CoursePlanInfo editInstalmentSchedule(EditInstalmentAmtAndSchedule req)
            throws VException, NotFoundException, Exception {
        req.verify();
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(req.getCoursePlanId());
        if (coursePlan == null) {
            throw new NotFoundException(ErrorCode.COURSE_PLAN_NOT_FOUND,
                    "course plan not found " + req.getCoursePlanId());
        }
        if (coursePlan.getState().order > CoursePlanEnums.CoursePlanState.ENROLLED.order) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                    "course plan current state " + coursePlan.getState());
        }

        // if (ArrayUtils.isNotEmpty(req.getFutureInstalmentsInfo())) {
        // List<BaseInstalmentInfo> futureInstalments =
        // req.getFutureInstalmentsInfo();
        // if (ArrayUtils.isNotEmpty(coursePlan.getInstalmentsInfo())) {
        // for (BaseInstalmentInfo baseInstalmentInfo :
        // coursePlan.getInstalmentsInfo()) {
        // if (baseInstalmentInfo.getDueTime() < currentTime) {
        // futureInstalments.add(baseInstalmentInfo);
        // }
        // }
        // }
        // coursePlan.setInstalmentsInfo(futureInstalments);
        //
        // Integer totalCost = 0;
        // for (BaseInstalmentInfo instalmentInfo :
        // coursePlan.getInstalmentsInfo()) {
        // totalCost += instalmentInfo.getAmount();
        // }
        // Integer trialFee = coursePlan.getTrialRegistrationFee() == null ? 0 :
        // coursePlan.getTrialRegistrationFee();
        // Integer p = coursePlan.getPrice() == null ? 0 :
        // coursePlan.getPrice();
        // if ((p - trialFee) != totalCost) {
        // throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "(price -
        // trialRegistrationFee) != instalmentTotal");
        // }
        // }
        Long nextDueTime = req.getNextDueTime();
        if (nextDueTime == null && ArrayUtils.isNotEmpty(coursePlan.getInstalmentsInfo())) {
            Collections.sort(coursePlan.getInstalmentsInfo(), new Comparator<BaseInstalmentInfo>() {
                @Override
                public int compare(BaseInstalmentInfo o1, BaseInstalmentInfo o2) {
                    return o1.getDueTime().compareTo(o2.getDueTime());
                }
            });
            nextDueTime = coursePlan.getInstalmentsInfo().get(0).getDueTime();
        }

        List<SessionSlot> finalSlots = new ArrayList<>();
        List<SessionSlot> reqSlots = req.getFromNextInstalmentSchedule().getSessionSlots();
        if (nextDueTime != null) {
            for (SessionSlot slot : reqSlots) {
                if (slot.getStartTime() < nextDueTime) {
                    throw new ConflictException(ErrorCode.SESSION_SLOT_BEFORE_NEXT_INSTALMENT, gson.toJson(slot));
                }
            }
            logger.info("adding slots from current and previous instalments");
            if (coursePlan.getRegularSessionSchedule() != null
                    && ArrayUtils.isNotEmpty(coursePlan.getRegularSessionSchedule().getSessionSlots())) {
                for (SessionSlot slot : coursePlan.getRegularSessionSchedule().getSessionSlots()) {
                    if (slot.getStartTime() < nextDueTime) {
                        finalSlots.add(slot);
                    }
                }
            } else {
                SessionSchedule schedule = new SessionSchedule();
                coursePlan.setRegularSessionSchedule(schedule);
            }
        }

        logger.info("adding req slots from next instalments to finalSlots array");
        finalSlots.addAll(reqSlots);
        coursePlan.getRegularSessionSchedule().setSessionSlots(finalSlots);

        logger.info("changing the startdate and regular session date");
        Collections.sort(reqSlots, new Comparator<SessionSlot>() {
            @Override
            public int compare(SessionSlot o1, SessionSlot o2) {
                return o1.getStartTime().compareTo(o2.getStartTime());
            }
        });
        if (coursePlan.getRegularSessionStartDate().equals(coursePlan.getStartDate())) {
            coursePlan.setStartDate(reqSlots.get(0).getStartTime());
        }
        coursePlan.setRegularSessionStartDate(reqSlots.get(0).getStartTime());

        List<SessionSlot> slots = coursePlan.getRegularSessionSchedule().getSessionSlots();
        if (ArrayUtils.isNotEmpty(slots)) {
            Collections.sort(slots, new Comparator<SessionSlot>() {
                @Override
                public int compare(SessionSlot o1, SessionSlot o2) {
                    return o1.getStartTime().compareTo(o2.getStartTime());
                }
            });

            coursePlan.setSessionEndDate(slots.get(slots.size() - 1).getEndTime());
        }

        logger.info("saving the courseplan");
        coursePlanDAO.save(coursePlan, req.getCallingUserId());

        List<SessionSlot> slotsToBlock = new ArrayList<>();
        switch (coursePlan.getState()) {
            case ENROLLED:
                slotsToBlock.addAll(reqSlots); // slots after the nextdue needs to
                // be blocked
                break;
            case TRIAL_PAYMENT_DONE:
                slotsToBlock.addAll(finalSlots);// all requested slots can be
                // changed
            case PUBLISHED:
            case DRAFT:
                slotsToBlock.addAll(finalSlots);
                if (coursePlan.getTrialSessionSchedule() != null
                        && ArrayUtils.isNotEmpty(coursePlan.getTrialSessionSchedule().getSessionSlots())) {
                    slotsToBlock.addAll(coursePlan.getTrialSessionSchedule().getSessionSlots());
                }
        }
        logger.info("setting the slots to block as regular session slots");
        SessionSchedule _schedule = new SessionSchedule();
        _schedule.setSessionSlots(slotsToBlock);
        coursePlan.setRegularSessionSchedule(_schedule);
        return mapper.map(coursePlan, CoursePlanInfo.class);
    }

    public CoursePlanInfo getCoursePlan(String id) throws NotFoundException {
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(id);
        if (coursePlan == null) {
            throw new NotFoundException(ErrorCode.COURSE_PLAN_NOT_FOUND, "course plan not found " + id);
        }
        CoursePlanInfo coursePlanInfo = mapper.map(coursePlan, CoursePlanInfo.class);
        fillCoursePlanHours(coursePlan, coursePlanInfo);
        return coursePlanInfo;
    }

    public CoursePlanInfo getCoursePlan(String id, boolean fillDetails, boolean exposeEmail) throws NotFoundException,VException {
        CoursePlanInfo coursePlanInfo = getCoursePlan(id);

        boolean hideSensitiveInfo = false;
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || (!Role.ADMIN.equals(sessionData.getRole()))) {
            hideSensitiveInfo = true;
        }

        if (fillDetails) {
            List<Long> userIds = new ArrayList<>();
            Long teacherId = coursePlanInfo.getTeacherId();
            Long studentId = coursePlanInfo.getStudentId();
            userIds.add(teacherId);
            userIds.add(studentId);
            Map<Long, User> userInfos = fosUtils.getUserInfosMap(userIds, exposeEmail);

            List<String> userIdsString = new ArrayList<>();
            userIdsString.add(teacherId.toString());
            userIdsString.add(studentId.toString());

            String userIdsCommaSeparated = String.join(",", userIdsString);

            // Network call to platform to get CumulativeRatingsMap
            // only 2 userIds in call so no issue with max length for get request
            String url = PLATFORM_ENDPOINT + "reviews/getCumulativeRatingsMap?userIdsCommaSeparated=" + userIdsCommaSeparated + "&entityType=" + com.vedantu.util.EntityType.USER;
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(response);

            String jsonString = response.getEntity(String.class);
            Type cumulativeRatingsType = new TypeToken<HashMap<String, CumilativeRating>>() {
            }.getType();

            Map<String, CumilativeRating> ratingsMap = gson.fromJson(jsonString, cumulativeRatingsType);
            logger.info("ratingsMap {}", ratingsMap);
            logger.info("stringResponse {}", jsonString);
            UserInfo teacherUserInfo = new UserInfo(userInfos.get(teacherId), ratingsMap.get(teacherId.toString()), exposeEmail);
            UserInfo studentUserInfo = new UserInfo(userInfos.get(studentId), ratingsMap.get(studentId.toString()), exposeEmail);

            if (hideSensitiveInfo) {
                teacherUserInfo.setInfo(null);
                teacherUserInfo.setSocialInfo(null);

                studentUserInfo.setInfo(null);
                studentUserInfo.setSocialInfo(null);
            }

            coursePlanInfo.setStudent(studentUserInfo);
            coursePlanInfo.setTeacher(teacherUserInfo);

            // populate session infos
            populateSessionInfos(coursePlanInfo,userInfos,exposeEmail);
        }
        return coursePlanInfo;
    }

    public CoursePlanBasicInfo getCoursePlanBasicInfo(String id) throws NotFoundException {
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(id);
        if (coursePlan == null) {
            throw new NotFoundException(ErrorCode.COURSE_PLAN_NOT_FOUND, "course plan not found " + id);
        }
        CoursePlanBasicInfo coursePlanBasicInfo = mapper.map(coursePlan, CoursePlanBasicInfo.class);
        fillCoursePlanHours(coursePlan, coursePlanBasicInfo);
        return coursePlanBasicInfo;
    }

    public CoursePlanBasicInfo getCoursePlanTitleById(String id) throws NotFoundException {
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanTitleById(id);
        if (coursePlan == null) {
            throw new NotFoundException(ErrorCode.COURSE_PLAN_NOT_FOUND, "course plan not found " + id);
        }
        CoursePlanBasicInfo coursePlanBasicInfo = mapper.map(coursePlan, CoursePlanBasicInfo.class);
        return coursePlanBasicInfo;
    }

    public List<CoursePlanBasicInfo> getBasicInfosForHomePage(CoursePlanBasicInfoReq req) throws NotFoundException {
        List<CoursePlan> coursePlans = coursePlanDAO.getBasicInfosForHomePage(req);

        return coursePlans.stream()
                .map(e -> mapper.map(e, CoursePlanBasicInfo.class))
                .collect(Collectors.toList());
    }

    public CoursePlanBasicInfo getCoursePlanBasicInfoWithHours(String id) {
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(id);
        CoursePlanBasicInfo coursePlanBasicInfo = mapper.map(coursePlan, CoursePlanBasicInfo.class);
        fillCoursePlanHours(coursePlan, coursePlanBasicInfo);
        return coursePlanBasicInfo;
    }

    private void fillCoursePlanHours(CoursePlan coursePlan, CoursePlanBasicInfo coursePlanBasicInfo) {
        if (coursePlan.getState().order > CoursePlanEnums.CoursePlanState.PUBLISHED.order) {
            CoursePlanHours coursePlanHours = coursePlanHoursDAO.getByCoursePlanId(coursePlan.getId());
            if (coursePlanHours != null) {
                _fillCoursePlanHours(coursePlanBasicInfo, coursePlanHours);
            } else {
                CoursePlanHours _coursePlanHours = new CoursePlanHours(coursePlan.getId(), 0, 0, 0, 0);
                coursePlanHoursDAO.create(_coursePlanHours, coursePlan.getStudentId().toString());
                _fillCoursePlanHours(coursePlanBasicInfo, _coursePlanHours);
                logger.error("No course plan hours found for " + coursePlan.getId() + ", creating them");
            }
        }
    }

    private void _fillCoursePlanHours(CoursePlanBasicInfo coursePlanBasicInfo, CoursePlanHours coursePlanHours) {
        coursePlanBasicInfo.setConsumedHours(coursePlanHours.getConsumedHours());
        coursePlanBasicInfo.setRemainingHours(coursePlanHours.getRemainingHours());
        coursePlanBasicInfo.setTotalHours(coursePlanHours.getTotalHours());
        coursePlanBasicInfo.setLockedHours(coursePlanHours.getLockedHours());
    }

    public List<CoursePlanInfo> getCoursePlanInfos(GetCoursePlansReq req,
                                                   boolean fillUserDetails,
                                                   boolean exposeEmail) throws VException {
        req.verify();
        List<CoursePlanInfo> res = new ArrayList<>();
        List<CoursePlan> coursePlans = coursePlanDAO.getCoursePlanBasicInfos(req);
        List<String> coursePlanIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(coursePlans)) {
            for (CoursePlan coursePlan : coursePlans) {
                res.add(mapper.map(coursePlan, CoursePlanInfo.class));
                coursePlanIds.add(coursePlan.getId());
            }
            List<CoursePlanHours> coursePlanHours = coursePlanHoursDAO.getByCoursePlanIds(coursePlanIds);
            Map<String, CoursePlanHours> _map = new HashMap<>();
            if (ArrayUtils.isNotEmpty(coursePlanHours)) {
                for (CoursePlanHours coursePlanHours1 : coursePlanHours) {
                    _map.put(coursePlanHours1.getCoursePlanId(), coursePlanHours1);
                }
                for (CoursePlanInfo coursePlanInfo : res) {
                    CoursePlanHours _coursePlanHours = _map.get(coursePlanInfo.getId());
                    if (_coursePlanHours != null) {
                        _fillCoursePlanHours(coursePlanInfo, _coursePlanHours);
                    }
                }
            }
        }

        if (fillUserDetails && ArrayUtils.isNotEmpty(res)) {
            populateUserDetails(res,exposeEmail);
        }
        return res;
    }

    public Map<String, CoursePlanInfo> getCousePlansInfosByIds(List<String> coursePlanIdsReq) throws VException {
        Map<String, CoursePlanInfo> response = new HashMap();
        if (ArrayUtils.isEmpty(coursePlanIdsReq)) {
            return response;
        }
        List<CoursePlan> coursePlans = coursePlanDAO.getCoursePlansByIds(coursePlanIdsReq);
        List<CoursePlanInfo> coursePlanInfos = new ArrayList<>();
        List<String> coursePlanIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(coursePlans)) {
            for (CoursePlan coursePlan : coursePlans) {
                coursePlanInfos.add(mapper.map(coursePlan, CoursePlanInfo.class));
                coursePlanIds.add(coursePlan.getId());
            }
            List<CoursePlanHours> coursePlanHours = coursePlanHoursDAO.getByCoursePlanIds(coursePlanIds);
            Map<String, CoursePlanHours> _map = new HashMap<>();
            if (ArrayUtils.isNotEmpty(coursePlanHours)) {
                for (CoursePlanHours coursePlanHours1 : coursePlanHours) {
                    _map.put(coursePlanHours1.getCoursePlanId(), coursePlanHours1);
                }
                for (CoursePlanInfo coursePlanInfo : coursePlanInfos) {
                    CoursePlanHours _coursePlanHours = _map.get(coursePlanInfo.getId());
                    if (_coursePlanHours != null) {
                        _fillCoursePlanHours(coursePlanInfo, _coursePlanHours);
                    }
                    response.put(coursePlanInfo.getId(), coursePlanInfo);
                }
            }
        }
        return response;
    }

    public List<UserDashboardCoursePlanRes> getUserDashboardCoursePlans(Long userId) throws VException {
        List<UserDashboardCoursePlanRes> response = new ArrayList<>();
        if (userId == null) {
            return response;
        }
        GetCoursePlansReq req = new GetCoursePlansReq();
        req.setStudentId(userId);
        List<CoursePlanInfo> coursePlanInfos = getCoursePlanInfos(req,false,false);
        Set<String> entityIdsForRegFetch = new HashSet<>();
        Set<Long> teacherIds = new HashSet<>();
        for (CoursePlanInfo info : coursePlanInfos) {
            entityIdsForRegFetch.add(info.getId());
            teacherIds.add(info.getTeacherId());
        }
        Map<Long, UserBasicInfo> userInfoMap = fosUtils.getUserBasicInfosMapFromLongIds(teacherIds, true);

        Map<String, List<DashBoardInstalmentInfo>> installmentInfos = null;
        installmentInfos = paymentManager.getIntallmentInfosForContextIds(new ArrayList<>(entityIdsForRegFetch), String.valueOf(userId));
        Map<String, Orders> orderInfos = paymentManager.getOrderInfosForEntityIds(new ArrayList<>(entityIdsForRegFetch), String.valueOf(userId));
        Map<String, CoursePlanDashboardSessionInfo> sessionInfos = null;
        String queryString = "ids=" + org.springframework.util.StringUtils.collectionToDelimitedString(entityIdsForRegFetch, ",");
        String url = "/session/getCoursePlanDashboardSessionInfos?" + queryString;
        ClientResponse resp = WebUtils.INSTANCE.doCall(SCHEDULING_ENDPOINT + url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<Map<String, CoursePlanDashboardSessionInfo>>() {
        }.getType();
        sessionInfos = gson.fromJson(jsonString, listType);
        for (CoursePlanInfo info : coursePlanInfos) {
            info.setTeacher(userInfoMap.get(info.getTeacherId()));
            UserDashboardCoursePlanRes res = new UserDashboardCoursePlanRes(info);
            String key = info.getId();
            if (StringUtils.isNotEmpty(key)) {
                if (installmentInfos.containsKey(key)) {
                    res.setDashBoardInstalmentInfos(installmentInfos.get(key));
                }
                if (sessionInfos.containsKey(key)) {
                    res.setCoursePlanDashboardSessionInfo(sessionInfos.get(key));
                }
                if (orderInfos.containsKey(key)) {
                    res.setOrderInfo(orderInfos.get(key));
                }
            }
            response.add(res);
        }

        return response;
    }

    public CoursePlanInfo markRegisteredForParentCourse(MarkRegisteredForParentCourseReq req) throws VException {
        req.verify();
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(req.getCourseId());
        coursePlan.setRegisteredForParentCourse(Boolean.TRUE);
        coursePlanDAO.save(coursePlan, req.getCallingUserId());
        return mapper.map(coursePlan, CoursePlanInfo.class);
    }

    public CoursePlanInfo getActiveCoursePlanByParentIdAndStudentId(Long studentId, String parentCourseId)
            throws VException {
        List<CoursePlan> coursePlans = coursePlanDAO.getNonDraftCoursePlans(parentCourseId, studentId);
        if (ArrayUtils.isNotEmpty(coursePlans)) {
            if (coursePlans.size() > 1) {
                logger.error("Multiple course plans found for parentId " + parentCourseId + ", userId " + studentId);
            }
            CoursePlan coursePlan = coursePlans.get(0);
            return mapper.map(coursePlan, CoursePlanInfo.class);
        } else {
            return null;
        }
    }

    public PlatformBasicResponse addEditStructuredCourse(AddEditStructuredCourseReq addEditStructuredCourseReq)
            throws BadRequestException {
        addEditStructuredCourseReq.verify();
        StructuredCourse structuredCourse;
        if (addEditStructuredCourseReq.getId() == null) {
            structuredCourse = new StructuredCourse();
        } else {
            structuredCourse = structuredCourseDAO.getStructuredCourse(addEditStructuredCourseReq.getId());
        }
        structuredCourse.setTitle(addEditStructuredCourseReq.getTitle());
        structuredCourse.setDescription(addEditStructuredCourseReq.getDescription());
        structuredCourse.setBoardId(addEditStructuredCourseReq.getBoardId());
        structuredCourse.setRegistrationFee(addEditStructuredCourseReq.getRegistrationFee());
        structuredCourseDAO.save(structuredCourse, addEditStructuredCourseReq.getCallingUserId());
        return new PlatformBasicResponse();
    }

    public StructuredCourseInfo getStructuredCourse(String id) {
        StructuredCourse structuredCourse = structuredCourseDAO.getStructuredCourse(id);
        StructuredCourseInfo res = mapper.map(structuredCourse, StructuredCourseInfo.class);
        return res;
    }

    public List<StructuredCourseInfo> getStructuredCourses(GetStructuredCoursesReq req) {
        List<StructuredCourse> courses = structuredCourseDAO.getStructuredCourses(req);
        List<StructuredCourseInfo> res = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(courses)) {
            for (StructuredCourse course : courses) {
                if (course != null) {
                    res.add(mapper.map(course, StructuredCourseInfo.class));
                }
            }
        }

        List<Long> boardIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(res)) {
            for(StructuredCourseInfo courseInfo : res) {
                    boardIds.add(courseInfo.getBoardId());
                }

            Map<Long, Board> boardMap = fosUtils.getBoardInfoMap(boardIds);
            for(StructuredCourseInfo courseInfo : res) {
                if (boardMap.containsKey(courseInfo.getBoardId())) {
                        courseInfo.setSubject(boardMap.get(courseInfo.getBoardId()).getSlug());
                    }
                }
        }
        return res;
    }

    public PlatformBasicResponse markCoursePlanState(MarkCoursePlanStateReq req) throws VException {
        req.verify();
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(req.getCoursePlanId());
        if (coursePlan == null) {
            throw new NotFoundException(ErrorCode.COURSE_PLAN_NOT_FOUND,
                    "course plan not found " + req.getCoursePlanId());
        }
        CoursePlanEnums.CoursePlanState currentstate = coursePlan.getState();
        if (currentstate.order < req.getState().order) {
            coursePlan.setState(req.getState());
            if (CoursePlanEnums.CoursePlanState.ENDED.equals(req.getState())) {
                coursePlan.setEndReason(req.getReason());
                coursePlan.setEndTime(System.currentTimeMillis());
                coursePlan.setEndedBy(req.getCallingUserId());
                coursePlan.setEndType(req.getEndType());
            }
            if (currentstate.order != req.getState().order) {
                CourseStateChangeTime courseChangeTime = new CourseStateChangeTime();
                courseChangeTime.setChangeTime(System.currentTimeMillis());
                courseChangeTime.setNewState(req.getState());
                courseChangeTime.setChangedBy(req.getCallingUserId());
                courseChangeTime.setPreviousState(currentstate);
                if (ArrayUtils.isEmpty(coursePlan.getStateChangeTime())) {
                    coursePlan.setStateChangeTime(Arrays.asList(courseChangeTime));
                } else {
                    List<CourseStateChangeTime> tempList = coursePlan.getStateChangeTime();
                    tempList.add(courseChangeTime);
                }
            }
            coursePlanDAO.save(coursePlan, req.getCallingUserId());
            if (CoursePlanEnums.CoursePlanState.PUBLISHED.equals(currentstate)) {
                logger.info("getting CoursePlanHours documents if exists with coursePlan Id : " + coursePlan.getId());
                CoursePlanHours coursePlanHoursCheck = coursePlanHoursDAO.getByCoursePlanId(coursePlan.getId());
                if (coursePlanHoursCheck == null) {
                    logger.info("creating the course plan hours");
                    CoursePlanHours coursePlanHours = new CoursePlanHours(coursePlan.getId(), 0, 0, 0, 0);
                    coursePlanHoursDAO.create(coursePlanHours,
                            (req.getCallingUserId() != null) ? req.getCallingUserId().toString() : null);
                }
            }
        } else {
            // just for a few days, will remove later, want to know which
            // scenarios it is failing
            logger.error("invalid state change from " + currentstate + " to " + req.getState());
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED,
                    "invalid state change from " + currentstate + " to " + req.getState());
        }
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        CoursePlanInfo coursePlanInfo = mapper.map(coursePlan, CoursePlanInfo.class);
        platformBasicResponse.setResponse(gson.toJson(coursePlanInfo));
        return platformBasicResponse;
    }

    public PlatformBasicResponse addCoursePlanHoursToQueue(AddCoursePlanHoursReq request) {
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_TRANSFER_HOURS_PRIOR_SESSION_BOOK;
        String groupId = request.getCoursePlanId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(request), groupId);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse addCoursePlanHours(AddCoursePlanHoursReq addCoursePlanHoursReq) throws Exception {

        addCoursePlanHoursReq.verify();

        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();

        int NUMBER_OF_TRIES = 2;
        for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();
                logger.info("try :" + retry);
                // if (retry != 0)
                CoursePlanHours coursePlanHours = coursePlanHoursDAO
                        .getByCoursePlanId(addCoursePlanHoursReq.getCoursePlanId(), session);

                if (coursePlanHours == null) {
                    throw new NotFoundException(ErrorCode.COURSE_PLAN_HOURS_NOT_FOUND, "course plan hrs not found");
                }

                Integer totalHours = coursePlanHours.getTotalHours();
                Integer totalRemainingHours = coursePlanHours.getRemainingHours();
                Integer hrsToAdd = addCoursePlanHoursReq.getHoursToAdd();

                totalRemainingHours += hrsToAdd;
                totalHours += hrsToAdd;

                HourTransactionType type = HourTransactionType.COURSE_PLAN_ENROLLED;
                if (Boolean.TRUE.equals(addCoursePlanHoursReq.getInstalmentPayment())) {
                    type = HourTransactionType.PAID_INSTALMENT;
                }

                List<CoursePlanHourTransaction> hourTransactions = new ArrayList<>();

                coursePlanHours.setRemainingHours(totalRemainingHours);
                coursePlanHours.setTotalHours(totalHours);
                coursePlanHoursDAO.update(coursePlanHours, session, null);
                CoursePlanHourTransaction hourTransaction = new CoursePlanHourTransaction(
                        HourTransactionContext.COURSE_PLAN, coursePlanHours.getCoursePlanId(), HourType.DEFAULT,
                        HourType.REMAINING, hrsToAdd, type, addCoursePlanHoursReq.getTransactionRefNo(),
                        coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
                hourTransactions.add(hourTransaction);
                coursePlanHourTransactionDAO.updateAll(hourTransactions, session);
                transaction.commit();
                break;
            } catch (Exception e) {
                if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                    logger.info("bookSession Rollback: " + e.getMessage());
                    session.getTransaction().rollback();
                }
                if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
                        && !(e instanceof ForbiddenException) && !(e instanceof BadRequestException)) {
                    logger.info("bookSession Rollback: " + e.getMessage());
                }
                if (retry == (NUMBER_OF_TRIES - 1)) {
                    throw e;
                }
            } finally {
                session.close();
            }

        }

        return new PlatformBasicResponse(true, null, null);
    }

    public PlatformBasicResponse transferBookSessionHoursToQueue(CoursePlanSessionRequest request) {
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_BOOK;
        String groupId = request.getEntityId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(request), groupId);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse transferBookSessionHours(CoursePlanSessionRequest coursePlanSessionRequest) throws BadRequestException {
        coursePlanSessionRequest.verify();
        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();

        int NUMBER_OF_TRIES = 2;
        for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();
                logger.info("try :" + retry);
                // if (retry != 0)
                CoursePlanHours coursePlanHours = coursePlanHoursDAO
                        .getByCoursePlanId(coursePlanSessionRequest.getEntityId(), session);

                Integer totalRemainingHours = coursePlanHours.getRemainingHours();
                Integer totalLockedHours = coursePlanHours.getLockedHours();
                Integer newLockedHours = coursePlanSessionRequest.getHours().intValue();

                totalRemainingHours -= newLockedHours;
                totalLockedHours += newLockedHours;

                List<CoursePlanHourTransaction> hourTransactions = new ArrayList<>();

                coursePlanHours.setLockedHours(totalLockedHours);
                coursePlanHours.setRemainingHours(totalRemainingHours);
                // subscriptionRes.setCallingUserId(sessionRequest.getCallingUserId());
                coursePlanHoursDAO.update(coursePlanHours, session, null);
                CoursePlanHourTransaction hourTransaction = new CoursePlanHourTransaction(
                        HourTransactionContext.COURSE_PLAN, coursePlanHours.getCoursePlanId(), HourType.REMAINING,
                        HourType.LOCKED, newLockedHours, HourTransactionType.SESSION_BOOKED,
                        coursePlanSessionRequest.getSessionId().toString(),
                        coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
                // hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
                hourTransactions.add(hourTransaction);

                coursePlanHourTransactionDAO.updateAll(hourTransactions, session);
                transaction.commit();
                break;
            } catch (Exception e) {
                if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                    logger.info("bookSession Rollback: " + e.getMessage());
                    session.getTransaction().rollback();
                }
                if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
                        && !(e instanceof ForbiddenException) && !(e instanceof BadRequestException)) {
                    logger.info("bookSession Rollback: " + e.getMessage());
                }
                if (retry == (NUMBER_OF_TRIES - 1)) {
                    throw e;
                }
            } finally {
                session.close();
            }

        }

        return new PlatformBasicResponse(true, null, null);
    }

    public PlatformBasicResponse transferCancelSessionHoursToQueue(TransferCancelSessionHoursReq req) {
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_CANCEL;
        String groupId = req.getCoursePlanId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(req), groupId);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse transferCancelSessionHours(TransferCancelSessionHoursReq req)
            throws BadRequestException, NotFoundException, ForbiddenException {
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(req.getCoursePlanId());

        if (coursePlan == null) {
            logger.info("coursePlan with id: " + req.getCoursePlanId() + " doesn't exist");
            throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
                    "Course Plan with id: " + req.getCoursePlanId() + " doesn't exist");
        } else if (CoursePlanEnums.CoursePlanState.ENDED.equals(coursePlan.getState())) {
            throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
                    "Course Plan with id: " + coursePlan.getId() + " already ended");
        }

        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();

        int NUMBER_OF_TRIES = 2;
        for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
            if (!session.isOpen()) {
                session = sessionFactory.openSession();
            }
            Transaction transaction = session.getTransaction();
            try {
                transaction.begin();
                logger.info("try :" + retry);
                CoursePlanHours coursePlanHours = coursePlanHoursDAO.getByCoursePlanId(req.getCoursePlanId(), session);

                Integer totalRemainingHours = coursePlanHours.getRemainingHours();
                Integer totalLockedHours = coursePlanHours.getLockedHours();
                Integer newLockedHours = req.getHours();
                if (totalLockedHours >= newLockedHours) {
                    totalRemainingHours += newLockedHours;
                    totalLockedHours -= newLockedHours;
                    List<CoursePlanHourTransaction> hourTransactions = new ArrayList<>();

                    coursePlanHours.setLockedHours(totalLockedHours);
                    coursePlanHours.setRemainingHours(totalRemainingHours);
                    // subscriptionRes.setCallingUserId(sessionRequest.getCallingUserId());
                    coursePlanHoursDAO.update(coursePlanHours, session, null);
                    CoursePlanHourTransaction hourTransaction = new CoursePlanHourTransaction(
                            HourTransactionContext.COURSE_PLAN, coursePlanHours.getCoursePlanId(), HourType.LOCKED,
                            HourType.REMAINING, newLockedHours, HourTransactionType.SESSION_CANCELLED,
                            req.getSessionId().toString(),
                            coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
                    // hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
                    hourTransactions.add(hourTransaction);

                    coursePlanHourTransactionDAO.updateAll(hourTransactions, session);

                } else {
                    logger.info("Cancelling more hours: " + newLockedHours + " than actually locked: "
                            + totalLockedHours + " for session");
                    throw new BadRequestException(ErrorCode.SUBSCRIPTION_REQUESTED_HOURS_INCORRECT,
                            "Cancelling more hours: " + newLockedHours + " than actually locked: " + totalLockedHours
                            + " for session");
                }
                transaction.commit();
                break;
            } catch (Exception e) {
                if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                    logger.info("cancelSession Rollback: " + e.getMessage());
                    session.getTransaction().rollback();
                }
                if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
                        && !(e instanceof ForbiddenException) && !(e instanceof BadRequestException)) {
                    logger.error("cancelSession Rollback: " + e.getMessage());
                }
                if (retry == (NUMBER_OF_TRIES - 1)) {
                    throw e;
                }
            } finally {
                session.close();
            }
        }
        return new PlatformBasicResponse(true, null, null);
    }

    public PlatformBasicResponse completeCoursePlanSessionToQueue(CompleteSessionRequest sessionRequest) {
        SQSMessageType messageType = SQSMessageType.COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_COMPLETE;
        String contextId = sessionRequest.getContextId();
        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(sessionRequest), contextId);
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse completeCoursePlanSession(CompleteSessionRequest sessionRequest)
            throws NotFoundException, ForbiddenException, BadRequestException {
        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();

        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(sessionRequest.getContextId());

        if (coursePlan == null) {
            logger.info("coursePlan with id: " + sessionRequest.getContextId() + " doesn't exist");
            throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
                    "Course Plan with id: " + sessionRequest.getContextId() + " doesn't exist");
        } else if (CoursePlanEnums.CoursePlanState.ENDED.equals(coursePlan.getState())) {
            throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
                    "Course Plan with id: " + coursePlan.getId() + " already ended");
        }

        try {
            transaction.begin();

            CoursePlanHours coursePlanHours = coursePlanHoursDAO.getByCoursePlanId(sessionRequest.getContextId(),
                    session);

            logger.info("completeSession subscription before updating " + coursePlanHours);

            Integer totalLockedHours = coursePlanHours.getLockedHours();
            Integer totalConsumedHours = coursePlanHours.getConsumedHours();
            Integer totalRemainingHours = coursePlanHours.getRemainingHours();
            Integer billingDuration = sessionRequest.getBillingDuration().intValue();
            Integer sessionScheduledDuration = sessionRequest.getSessionScheduledDuration().intValue();
            Integer initHours = totalRemainingHours + totalLockedHours;
            logger.info("totalConsumedHours:" + totalConsumedHours);
            logger.info("totalLockedHours:" + totalLockedHours);
            logger.info("totalRemainingHours:" + totalRemainingHours);
            logger.info("sessionScheduledDuration:" + sessionScheduledDuration);
            logger.info("billingDuration:" + billingDuration);
            if (sessionScheduledDuration >= billingDuration && totalLockedHours >= billingDuration
                    && totalLockedHours >= sessionScheduledDuration) {
                totalConsumedHours += billingDuration;
                totalLockedHours -= sessionScheduledDuration;
                totalRemainingHours = totalRemainingHours + sessionScheduledDuration - billingDuration;

                List<CoursePlanHourTransaction> hourTransactions = new ArrayList<>();
                
                if((totalRemainingHours + totalLockedHours) < 3*DateTimeUtils.MILLIS_PER_HOUR && initHours >= 3*DateTimeUtils.MILLIS_PER_HOUR){
                    checkCoursePlanHoursAboutToEndAsync(coursePlanHours.getCoursePlanId());
                }
                
                logger.info("Finally");
                logger.info("totalConsumedHours:" + totalConsumedHours);
                logger.info("totalLockedHours:" + totalLockedHours);
                logger.info("totalRemainingHours:" + totalRemainingHours);
                
                coursePlanHours.setLockedHours(totalLockedHours);
                coursePlanHours.setConsumedHours(totalConsumedHours);
                coursePlanHours.setRemainingHours(totalRemainingHours);
                // subscriptionRes.setCallingUserId(sessionRequest.getCallingUserId());
                coursePlanHoursDAO.update(coursePlanHours, session, null);
                CoursePlanHourTransaction hourTransaction1 = new CoursePlanHourTransaction(
                        HourTransactionContext.COURSE_PLAN, coursePlanHours.getCoursePlanId(), HourType.LOCKED,
                        HourType.CONSUMED, billingDuration, HourTransactionType.SESSION_ENDED,
                        sessionRequest.getSessionId().toString(),
                        coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
                CoursePlanHourTransaction hourTransaction2 = new CoursePlanHourTransaction(
                        HourTransactionContext.COURSE_PLAN, coursePlanHours.getCoursePlanId(), HourType.LOCKED,
                        HourType.REMAINING, sessionScheduledDuration - billingDuration,
                        HourTransactionType.SESSION_ENDED, sessionRequest.getSessionId().toString(),
                        coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
                // hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
                hourTransactions.add(hourTransaction1);
                hourTransactions.add(hourTransaction2);

                coursePlanHourTransactionDAO.updateAll(hourTransactions, session);

            } else {
                String errorMsg = "Session taken for more sessionScheduledDuration:" + sessionScheduledDuration
                        + ", billingDuration:" + billingDuration + " than hours: " + totalLockedHours
                        + " purchased under CoursePlan: " + coursePlanHours.getCoursePlanId()
                        + ", sessionId " + sessionRequest.getSessionId();
                logger.error(errorMsg);
                throw new BadRequestException(ErrorCode.SESSION_DURATION_LIMIT_EXCEEDED, errorMsg);
            }

            transaction.commit();
        } catch (Exception e) {
            if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                logger.info("completeSession Rollback: " + e.getMessage());
                session.getTransaction().rollback();
            }
            if (!(e instanceof NotFoundException) && !(e instanceof ForbiddenException)
                    && !(e instanceof BadRequestException) && !(e instanceof ConstraintViolationException)) {
                logger.error("completeSession Rollback: " + e.getMessage());
            }
            throw e;
        } finally {
            session.close();
        }
        return new PlatformBasicResponse(true, null, null);

    }

    public PlatformBasicResponse updateDurationConflict(UpdateConflictDurationRequest request) throws Throwable {
        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();

        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(request.getContextId());

        if (coursePlan == null) {
            logger.error("updateDurationConflict: coursePlan with id: " + request.getContextId() + " doesn't exist");
            // throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
            // "Course Plan with id: " + request.getContextId() + " doesn't
            // exist");
            return new PlatformBasicResponse();
        } else if (CoursePlanEnums.CoursePlanState.ENDED.equals(coursePlan.getState())) {
            logger.error("updateDurationConflict: coursePlan with id: " + request.getContextId() + " already ended");
            // throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
            // "Course Plan with id: " + coursePlan.getId() + " already ended");
            return new PlatformBasicResponse();
        }
        try {
            transaction.begin();

            CoursePlanHours coursePlanHours = coursePlanHoursDAO.getByCoursePlanId(request.getContextId(), session);

            logger.info("completeSession subscription before updating " + coursePlanHours);

            Integer totalRemainingHours = coursePlanHours.getRemainingHours();
            Integer totalConsumedHours = coursePlanHours.getConsumedHours();
            int durationDiff = (int) (request.getNewDuration() - request.getPreviousDuration());

            totalConsumedHours += durationDiff;
            totalRemainingHours -= durationDiff;

            coursePlanHours.setRemainingHours(totalRemainingHours);
            coursePlanHours.setConsumedHours(totalConsumedHours);
            // subscriptionRes.setCallingUserId(request.getCallingUserId());
            coursePlanHoursDAO.update(coursePlanHours, session, null);
            List<CoursePlanHourTransaction> hourTransactions = new ArrayList<>();

            CoursePlanHourTransaction hourTransaction = null;
            if (durationDiff > 0) {
                hourTransaction = new CoursePlanHourTransaction(HourTransactionContext.COURSE_PLAN,
                        coursePlanHours.getCoursePlanId(), HourType.REMAINING, HourType.CONSUMED, durationDiff,
                        HourTransactionType.SESSION_DURATION_CONFLICT, request.getSessionId().toString(),
                        coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
            } else {
                hourTransaction = new CoursePlanHourTransaction(HourTransactionContext.COURSE_PLAN,
                        coursePlanHours.getCoursePlanId(), HourType.CONSUMED, HourType.REMAINING,
                        Math.abs(durationDiff), HourTransactionType.SESSION_DURATION_CONFLICT,
                        request.getSessionId().toString(),
                        coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
            }
            hourTransactions.add(hourTransaction);
            // hourTransaction.setCallingUserId(request.getCallingUserId());
            coursePlanHourTransactionDAO.updateAll(hourTransactions, session);

            transaction.commit();
        } catch (Exception e) {
            if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
                logger.info("updateDurationConflict Rollback: " + e.getMessage());
                session.getTransaction().rollback();
            }
            if (!(e instanceof NotFoundException) && !(e instanceof ForbiddenException)) {
                logger.error("updateDurationConflict Rollback: " + e.getMessage());
            }
            throw e;
        } finally {
            session.close();
        }

        // SessionInfoResponse sessionInfoResponse =
        // getSessionInfoResponse(subscriptionRes);
        return new PlatformBasicResponse(true, null, null);
    }

    public PlatformBasicResponse unlockHoursToQueue(List<UnlockHoursRequest> unlockHoursRequests) {
        if (ArrayUtils.isNotEmpty(unlockHoursRequests)) {
            SQSMessageType messageType = SQSMessageType.COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_UNLOCK;
            List<UnlockHoursRequest> errorSendingToQueueRequests = new ArrayList<>();
            for (UnlockHoursRequest request : unlockHoursRequests) {
                try {
                    if (request != null) {
                        String groupId = request.getContextId();
                        awsSQSManager.sendToSQS(messageType.getQueue(), messageType, gson.toJson(request), groupId);
                    }
                } catch (Exception ignored) {
                    errorSendingToQueueRequests.add(request);
                }
            }

            for (UnlockHoursRequest request : errorSendingToQueueRequests) {
                unlockHours(request);
            }
        }
        return new PlatformBasicResponse();
    }

    public PlatformBasicResponse unlockHours(UnlockHoursRequest unlockHoursRequest) {

        SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        Long callingUserId = unlockHoursRequest.getCallingUserId();

        List<CoursePlanHours> coursePlanHoursList = new ArrayList<CoursePlanHours>();
        try {
            transaction.begin();

            List<CoursePlanHourTransaction> hourTransactions = new ArrayList<>();
            List<String> coursePlanIds = new ArrayList<>();
            String id = unlockHoursRequest.getContextId();
            if (StringUtils.isEmpty(id) || unlockHoursRequest.getBookDuration() == null) {
                logger.info("unlockHours:" + id + "bookDuration:" + unlockHoursRequest.getBookDuration());
                return new PlatformBasicResponse();
            }
            CoursePlanHours coursePlanHours = null;
            coursePlanHours = coursePlanHoursDAO.getByCoursePlanId(id, session);
            if (coursePlanHours == null) {
                logger.info("coursePlan id: " + id + " doesn't exist");
                return new PlatformBasicResponse();
            }
            coursePlanIds.add(id);

            Integer totalRemainingHours = coursePlanHours.getRemainingHours();
            Integer totalLockedHours = coursePlanHours.getLockedHours();
            Integer bookDuration = unlockHoursRequest.getBookDuration().intValue();

            if (totalLockedHours >= bookDuration) {
                totalRemainingHours += bookDuration;
                totalLockedHours -= bookDuration;

                coursePlanHours.setLockedHours(totalLockedHours);
                coursePlanHours.setRemainingHours(totalRemainingHours);

                CoursePlanHourTransaction hourTransaction = new CoursePlanHourTransaction(
                        HourTransactionContext.COURSE_PLAN, coursePlanHours.getCoursePlanId(), HourType.LOCKED,
                        HourType.REMAINING, bookDuration, HourTransactionType.SESSION_EXPIRED,
                        unlockHoursRequest.getSessionId().toString(),
                        coursePlanHours.getConsumedHours(), coursePlanHours.getLockedHours(), coursePlanHours.getRemainingHours());
                // hourTransaction.setCallingUserId(unlockHoursRequest.getCallingUserId());
                hourTransactions.add(hourTransaction);
            }

            coursePlanHoursList.add(coursePlanHours);
            coursePlanHoursDAO.updateAll(coursePlanHoursList, session);
            coursePlanHourTransactionDAO.updateAll(hourTransactions, session);

            transaction.commit();
        } catch (Exception e) {
            logger.error("unlockHours Error: " + e.getMessage());
            throw e;
        } finally {
            session.close();
        }

        return new PlatformBasicResponse(true, null, null);
    }

    public List<CoursePlanInfo> getByCreationTime(ExportCoursePlansReq req) {
        List<CoursePlanInfo> res = new ArrayList<>();
        List<CoursePlan> coursePlans = coursePlanDAO.getByCreationTime(req);
        if (ArrayUtils.isNotEmpty(coursePlans)) {
            for (CoursePlan coursePlan : coursePlans) {
                res.add(mapper.map(coursePlan, CoursePlanInfo.class));
            }
        }
        return res;
    }

    public Map<Long, TeacherCourseStateCount> getTeacherCoursePlanMapping(Long startTime, Long endTime) {

        Map<Long, TeacherCourseStateCount> idCoursePlanMap = new HashMap<>();
        List<CoursePlan> coursePlans = coursePlanDAO.getByLastUpdated(startTime, endTime);
        if (ArrayUtils.isNotEmpty(coursePlans)) {
            for (CoursePlan coursePlan : coursePlans) {
                TeacherCourseStateCount teacherCoursePlanStateCount;
                Long teacherId = coursePlan.getTeacherId();
                if (idCoursePlanMap.containsKey(teacherId)) {
                    teacherCoursePlanStateCount = idCoursePlanMap.get(teacherId);
                } else {
                    teacherCoursePlanStateCount = new TeacherCourseStateCount();
                    teacherCoursePlanStateCount.setTeacherId(teacherId);
                    teacherCoursePlanStateCount.setEndedStudentIds(new HashSet<Long>());
                }

                incrementTeacherCoursePlanCount(coursePlan, teacherCoursePlanStateCount, startTime, endTime);
                idCoursePlanMap.put(teacherId, teacherCoursePlanStateCount);
            }
        }
        return idCoursePlanMap;
    }

    public void incrementTeacherCoursePlanCount(CoursePlan coursePlan,
            TeacherCourseStateCount teacherCoursePlanStateCount, Long startTime, Long endTime) {
        Boolean stateChangeExists;
        List<CourseStateChangeTime> courseStateChangeTimes = coursePlan.getStateChangeTime();
        if (ArrayUtils.isEmpty(courseStateChangeTimes)) {
            stateChangeExists = Boolean.FALSE;
        } else {
            stateChangeExists = Boolean.TRUE;
        }
        switch (coursePlan.getState()) {
            case PUBLISHED:
            case TRIAL_PAYMENT_DONE:
            case TRIAL_SESSIONS_DONE:
                if (stateChangeExists) {
                    for (CourseStateChangeTime courseStateChangeTime : courseStateChangeTimes) {
                        if (courseStateChangeTime.getNewState().equals(CoursePlanEnums.CoursePlanState.PUBLISHED)
                                && courseStateChangeTime.getChangeTime() < endTime
                                && courseStateChangeTime.getChangeTime() > startTime) {
                            teacherCoursePlanStateCount.setPublished(teacherCoursePlanStateCount.getPublished() + 1);
                        }
                    }
                } else {
                    if (coursePlan.getCreationTime() != null && coursePlan.getCreationTime() < endTime
                            && coursePlan.getCreationTime() > startTime) {
                        teacherCoursePlanStateCount.setPublished(teacherCoursePlanStateCount.getPublished() + 1);
                    }
                }
                break;
            case ENROLLED:
                if (stateChangeExists) {
                    for (CourseStateChangeTime courseStateChangeTime : courseStateChangeTimes) {
                        if (courseStateChangeTime.getNewState().equals(CoursePlanEnums.CoursePlanState.ENROLLED)
                                && courseStateChangeTime.getChangeTime() < endTime
                                && courseStateChangeTime.getChangeTime() > startTime) {
                            teacherCoursePlanStateCount.setEnrolled(teacherCoursePlanStateCount.getEnrolled() + 1);
                        }
                        if (courseStateChangeTime.getNewState().equals(CoursePlanEnums.CoursePlanState.PUBLISHED)
                                && courseStateChangeTime.getChangeTime() < endTime
                                && courseStateChangeTime.getChangeTime() > startTime) {
                            teacherCoursePlanStateCount.setPublished(teacherCoursePlanStateCount.getPublished() + 1);
                        }
                    }
                }
                break;
            case ENDED:
                if (coursePlan.getEndTime() != null) {
                    if (coursePlan.getEndTime() < endTime && coursePlan.getEndTime() > startTime) {
                        teacherCoursePlanStateCount.setEnded(teacherCoursePlanStateCount.getEnded() + 1);
                        teacherCoursePlanStateCount.getEndedStudentIds().add(coursePlan.getStudentId());
                    }
                }
                if (stateChangeExists) {
                    for (CourseStateChangeTime courseStateChangeTime : courseStateChangeTimes) {
                        if (courseStateChangeTime.getNewState().equals(CoursePlanEnums.CoursePlanState.ENROLLED)
                                && courseStateChangeTime.getChangeTime() < endTime
                                && courseStateChangeTime.getChangeTime() > startTime) {
                            teacherCoursePlanStateCount.setEnrolled(teacherCoursePlanStateCount.getEnrolled() + 1);
                        }
                        if (courseStateChangeTime.getNewState().equals(CoursePlanEnums.CoursePlanState.PUBLISHED)
                                && courseStateChangeTime.getChangeTime() < endTime
                                && courseStateChangeTime.getChangeTime() > startTime) {
                            teacherCoursePlanStateCount.setPublished(teacherCoursePlanStateCount.getPublished() + 1);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    public List<CoursePlanHours> getActiveCoursePlanHours(int start, int size) {
        Set<String> coursePlanIds = coursePlanDAO.getActiveCoursePlans(start, size);
        logger.info("Course plan ids:" + (coursePlanIds == null ? "" : Arrays.toString(coursePlanIds.toArray())));
        return coursePlanHoursDAO.getActiveCoursePlanHours(coursePlanIds);
    }

    private int calculateRegularSessionsPayInRate(CoursePlan coursePlan) {
        int payInRate = 0;
        Long regularHours = 0l;

        if (coursePlan.getPrice() == null) {
            return 0;
        }

        if (coursePlan.getRegularSessionSchedule() != null) {
            regularHours = coursePlan.getRegularSessionSchedule().calculateTotalHours();
        }

        Integer price = coursePlan.getPrice();
        if (coursePlan.getTrialRegistrationFee() != null && coursePlan.getTrialRegistrationFee() > 0) {
            price -= coursePlan.getTrialRegistrationFee();
        }

        if ((regularHours) != 0l) {
            payInRate = (int) (price / ((float) (regularHours) / DateTimeUtils.MILLIS_PER_HOUR));
        }
        return payInRate;
    }

    private int calculateTrialSessionsPayInRate(CoursePlan coursePlan) {
        int payInRate = 0;
        Long trialHrs = 0l;

        if (coursePlan.getTrialSessionSchedule() != null) {
            trialHrs = coursePlan.getTrialSessionSchedule().calculateTotalHours();
        }

        Integer price = 0;
        if (coursePlan.getTrialRegistrationFee() != null && coursePlan.getTrialRegistrationFee() > 0) {
            price = coursePlan.getTrialRegistrationFee();
        }

        if ((trialHrs) != 0l) {
            payInRate = (int) (price / ((float) (trialHrs) / DateTimeUtils.MILLIS_PER_HOUR));
        }
        return payInRate;
    }

    public List<GetCoursePlanReminderListRes> getCoursePlanTrialSessionList(GetCoursePlanReminderListReq req) throws BadRequestException {
        req.verify();
        List<CoursePlan> coursePlans = coursePlanDAO.getCoursePlanReminderList(req);
        return toGetCoursePlanReminderListResList(coursePlans);
    }

    public List<GetCoursePlanReminderListRes> toGetCoursePlanReminderListResList(List<CoursePlan> coursePlans) {
        List<GetCoursePlanReminderListRes> resps = new ArrayList<>();
        if (ArrayUtils.isEmpty(coursePlans)) {
            return resps;
        }
        for (CoursePlan coursePlan : coursePlans) {
            GetCoursePlanReminderListRes res = toGetCoursePlanReminderRes(coursePlan);
            if (res != null) {
                resps.add(res);
            }
        }
        return resps;
    }

    public GetCoursePlanReminderListRes toGetCoursePlanReminderRes(CoursePlan coursePlan) {
        if (coursePlan != null) {
            GetCoursePlanReminderListRes res = new GetCoursePlanReminderListRes();
            res.setCoursePlanId(coursePlan.getId());
            if (ArrayUtils.isNotEmpty(coursePlan.getInstalmentsInfo())
                    && coursePlan.getInstalmentsInfo().get(0) != null) {
                res.setInstalmentDueTime(coursePlan.getInstalmentsInfo().get(0).getDueTime());
            }
            res.setStudentId(coursePlan.getStudentId());
            res.setTeacherId(coursePlan.getTeacherId());
            res.setCoursePlanStartDate(coursePlan.getStartDate());
            res.setCoursePlanTitle(coursePlan.getTitle());
            if (coursePlan.getTrialSessionSchedule() != null
                    && ArrayUtils.isNotEmpty(coursePlan.getTrialSessionSchedule().getSessionSlots())
                    && coursePlan.getTrialSessionSchedule().getSessionSlots().get(0) != null) {
                res.setTrialTime(coursePlan.getTrialSessionSchedule().getSessionSlots().get(0).getStartTime());
            }
            res.setState(coursePlan.getState());
            //	res.setCoursePlanLink(coursePlanLink);
            return res;
        } else {
            return null;
        }
    }

    public List<GetCoursePlanReminderListRes> getCoursePlansWithTrialSessions(GetCoursePlanReminderListReq req) {
        List<GetCoursePlanReminderListRes> response = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req.getCoursePlanIds())) {
            List<CoursePlan> coursePlans = coursePlanDAO.getCoursePlansByIds(req);
            if (ArrayUtils.isNotEmpty(coursePlans)) {
                return toGetCoursePlanReminderListResList(coursePlans);
            }
        }
        return response;
    }

    //end course plan
    public GetCoursePlanBalanceRes getCoursePlanBalanceRes(String coursePlanId) throws NotFoundException, ConflictException {
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(coursePlanId);
        if (coursePlan == null) {
            throw new NotFoundException(ErrorCode.COURSE_PLAN_NOT_FOUND, "course plan not found " + coursePlanId);
        }

        if (coursePlan.getHourlyRate() == null) {
            throw new ConflictException(ErrorCode.COURSE_PLAN_HOURLY_RATE_NOT_FOUND, "hrly rate not found coursePlanId " + coursePlanId);
        }

        GetOrdersReq getOrdersReq = new GetOrdersReq();
        getOrdersReq.setEntityType(EntityType.COURSE_PLAN);
        getOrdersReq.setStart(0);
        getOrdersReq.setSize(100);//just a big number
        getOrdersReq.setPaymentStatuses(Arrays.asList(PaymentStatus.PAID, PaymentStatus.PARTIALLY_PAID));
        getOrdersReq.setEntityId(coursePlanId);
        ClientResponse resp = WebUtils.INSTANCE
                .doCall(DINERO_ENDPOINT + "/payment/getOrders", HttpMethod.POST, new Gson().toJson(getOrdersReq));
        String listResp = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Orders>>() {
        }.getType();
        List<Orders> orders = gson.fromJson(listResp, listType);
        if (ArrayUtils.isEmpty(orders)) {
            throw new NotFoundException(ErrorCode.ORDER_NOT_FOUND, "orders not found for coursePlanId " + coursePlanId);
        } else {

            if (orders.size() > 1) {
                logger.error("Multiple paid orders found for course plan id " + coursePlanId);
            }
            Orders order = orders.get(0);
            OrderedItem orderedItem = order.getItems().get(0);

            float totalBillingDuration = 0;//in hrs

            ClientResponse resp2 = WebUtils.INSTANCE
                    .doCall(SCHEDULING_ENDPOINT
                            + "/session/getEndedSessionAttendeesOfContext?contextType="
                            + EntityType.COURSE_PLAN.name() + "&contextId=" + coursePlanId, HttpMethod.GET, null);
            String listResp2 = resp2.getEntity(String.class);
            Type listType2 = new TypeToken<ArrayList<SessionAttendee>>() {
            }.getType();
            List<SessionAttendee> attendees = gson.fromJson(listResp2, listType2);
            if (ArrayUtils.isNotEmpty(attendees)) {
                for (SessionAttendee attendee : attendees) {
                    if(Role.STUDENT.equals(attendee.getRole())){
                        if(attendee.getBillingPeriod() != null){
                            totalBillingDuration += attendee.getBillingPeriod();
                        }
                    }
                }
                totalBillingDuration = totalBillingDuration / 3600000;
            }
            
            
            if(order.getAmount().equals(0)){
                GetCoursePlanBalanceRes res = new GetCoursePlanBalanceRes();
                res.setTotalBillingDurationInHrs(totalBillingDuration);
                return res;
            }

            int amountPaid, amountToPay;//does not contain discount amount, will use to calculate instalments paid ratio
            int nonPromotionalAmountPaid, promotionalAmountPaid;
            int discountAllowed = 0;
            if (orderedItem.getVedantuDiscountAmount() != null) {
                discountAllowed = orderedItem.getVedantuDiscountAmount();
            }
            if (PaymentType.INSTALMENT.equals(order.getPaymentType())) {
                amountPaid = order.getAmountPaid();
                amountToPay = order.getAmount();
                nonPromotionalAmountPaid = order.getNonPromotionalAmountPaid();
                promotionalAmountPaid = order.getPromotionalAmountPaid();
            } else {
                amountPaid = order.getAmount();
                amountToPay = order.getAmount();
                nonPromotionalAmountPaid = order.getNonPromotionalAmount();
                promotionalAmountPaid = order.getPromotionalAmount();
            }
            if ((nonPromotionalAmountPaid + promotionalAmountPaid) != amountPaid) {
                logger.error("nppaid+ppaid!=amountPiad nonPromotionalAmountPaid "
                        + nonPromotionalAmountPaid + " promotionalAmountPaid "
                        + promotionalAmountPaid + ", amountPaid " + amountPaid
                        + ", coursePlanId " + coursePlanId);
            }

            int discountClaimed = discountAllowed * amountPaid / amountToPay;
            float discountRatio = Float.valueOf(discountAllowed) / (amountToPay + discountAllowed);

            int totalDiscAndNormalAmountConsumed = Float.valueOf(totalBillingDuration * coursePlan.getHourlyRate()).intValue();//in paisa
            int discountAmountConsumed = Float.valueOf(totalDiscAndNormalAmountConsumed * discountRatio).intValue();
            int normalAmtConsumed = totalDiscAndNormalAmountConsumed - discountAmountConsumed;
            int pConsumed, npConsumed;
            if (normalAmtConsumed < promotionalAmountPaid) {
                pConsumed = normalAmtConsumed;
                npConsumed = 0;
            } else {
                pConsumed = promotionalAmountPaid;
                npConsumed = normalAmtConsumed - promotionalAmountPaid;
            }
            GetCoursePlanBalanceRes res = new GetCoursePlanBalanceRes();
            res.setTotalBillingDurationInHrs(totalBillingDuration);
            res.setHourlyRate(coursePlan.getHourlyRate());
            res.setAmountPaid(amountPaid);
            res.setNpPaid(nonPromotionalAmountPaid);
            res.setpPaid(promotionalAmountPaid);
            res.setAmountLeft(amountPaid - normalAmtConsumed);
            res.setNpLeft(nonPromotionalAmountPaid - npConsumed);
            res.setpLeft(promotionalAmountPaid - pConsumed);
            res.setDiscountRatio(discountRatio);
            res.setDiscountAllowed(discountAllowed);
            res.setDiscountClaimed(discountClaimed);
            res.setDiscountLeft(discountClaimed - discountAmountConsumed);
            return res;
        }
    }

    public static void main(String[] args) {
        float totalBillingDuration = 3.6f;
        int nonPromotionalAmountPaid = 2500, promotionalAmountPaid = 500;
        int discountAllowed = 3000;
        int amountPaid = nonPromotionalAmountPaid + promotionalAmountPaid, amountToPay = 7000;//does not contain discount amount, will use to calculate instalments paid ratio
        int hourlyRate = 300;

        int discountClaimed = discountAllowed * amountPaid / amountToPay;
        float discountRatio = Float.valueOf(discountAllowed) / (amountToPay + discountAllowed);

        int totalDiscAndNormalAmountConsumed = Float.valueOf(totalBillingDuration * hourlyRate).intValue();//in paisa
        int discountAmountConsumed = Float.valueOf(totalDiscAndNormalAmountConsumed * discountRatio).intValue();
        int normalAmtConsumed = totalDiscAndNormalAmountConsumed - discountAmountConsumed;
        int pConsumed, npConsumed;
        if (normalAmtConsumed < promotionalAmountPaid) {
            pConsumed = normalAmtConsumed;
            npConsumed = 0;
        } else {
            pConsumed = promotionalAmountPaid;
            npConsumed = normalAmtConsumed - promotionalAmountPaid;
        }
        GetCoursePlanBalanceRes res = new GetCoursePlanBalanceRes();
        res.setTotalBillingDurationInHrs(totalBillingDuration);
        res.setHourlyRate(hourlyRate);
        res.setAmountPaid(amountPaid);
        res.setNpPaid(nonPromotionalAmountPaid);
        res.setpPaid(promotionalAmountPaid);
        res.setAmountLeft(amountPaid - normalAmtConsumed);
        res.setNpLeft(nonPromotionalAmountPaid - npConsumed);
        res.setpLeft(promotionalAmountPaid - pConsumed);
        res.setDiscountRatio(discountRatio);
        res.setDiscountAllowed(discountAllowed);
        res.setDiscountClaimed(discountClaimed);
        res.setDiscountLeft(discountClaimed - discountAmountConsumed);
        System.err.println(">>> res" + res);
    }
    
        
    private Set<String> validateContentIds(List<OTOCurriculumPojo> curr){
        Set<String> errors = new HashSet<>();
        Set<String> testIds = new HashSet<>();
        fetchAllTestIds(curr,testIds);
        for(String testId : testIds){
            if(!ObjectId.isValid(testId)){
                errors.add(testId);
            }
        }
        return errors;
    }
 
    private void fetchAllTestIds(List<OTOCurriculumPojo> pojos,Set<String> testIds){
        if(ArrayUtils.isNotEmpty(pojos)){
            for(OTOCurriculumPojo pojo : pojos){
                if(ArrayUtils.isNotEmpty(pojo.getContents())){
                    pojo.getContents().stream().filter(s -> (s.getTestId() != null && ContentType.TEST.equals(s.getContentType())))
                .forEach((s) -> testIds.add(s.getTestId()));
                }
                if(ArrayUtils.isNotEmpty(pojo.getChildren())){
                    fetchAllTestIds(pojo.getChildren(),testIds);
                }
            }
            
        }
    }
    
    public void checkCoursePlanHoursAboutToEndAsync(String id){
        Map<String, Object>  payload = new HashMap<>();
        payload.put("id", id);
        try{
            AsyncTaskParams asyncTaskParams = new AsyncTaskParams(AsyncTaskName.PROCESS_COURSE_PLAN_HOURS, payload);
            asyncTaskFactory.executeTask(asyncTaskParams);
        }catch(Exception ex){
            logger.error("Error in pushing to async task for less than 3 courseplan hours: "+ex.getMessage());
        }
        
    }
    
    public void checkCoursePlanHoursAboutToEnd(String id){
        
        
        CoursePlan coursePlan = coursePlanDAO.getCoursePlanById(id);
        
        UserBasicInfo teacherInfo = fosUtils.getUserBasicInfo(coursePlan.getTeacherId(), true);
        UserBasicInfo studentInfo = fosUtils.getUserBasicInfo(coursePlan.getStudentId(), true);
        
        CoursePlanSQSAlert coursePlanSQSAlert = new CoursePlanSQSAlert();
        coursePlanSQSAlert.setUserId(coursePlan.getStudentId().toString());
        coursePlanSQSAlert.setSessionEndTime(coursePlan.getSessionEndDate());
        coursePlanSQSAlert.setStudentContactNumber(studentInfo.getContactNumber());
        coursePlanSQSAlert.setStudentEmailId(studentInfo.getEmail());
        coursePlanSQSAlert.setTeacherContactNumber(teacherInfo.getContactNumber());
        coursePlanSQSAlert.setTeacherEmailId(teacherInfo.getEmail());
        
        awsSQSManager.triggerAsyncSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.COURSE_PLAN_LESS_THAN_3HOURS, gson.toJson(coursePlanSQSAlert), id);
        
    }

    public void populateUserDetails(List<CoursePlanInfo> coursePlanInfos, boolean exposeEmail){
        Set<Long> userIds = new HashSet<>();
        for (CoursePlanInfo coursePlanInfo : coursePlanInfos) {
            userIds.add(coursePlanInfo.getStudentId());
            userIds.add(coursePlanInfo.getTeacherId());
        }

        Map<Long, UserBasicInfo> userMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, exposeEmail);
        for (CoursePlanInfo coursePlanInfo : coursePlanInfos) {
            if (userMap.containsKey(coursePlanInfo.getStudentId())) {
                coursePlanInfo.setStudent(userMap.get(coursePlanInfo.getStudentId()));
            }
            if (userMap.containsKey(coursePlanInfo.getTeacherId())) {
                coursePlanInfo.setTeacher(userMap.get(coursePlanInfo.getTeacherId()));
            }
        }
    }

    public void populateSessionInfos(CoursePlanInfo coursePlanInfo,Map<Long, User> userInfos, boolean exposeEmail){
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                SCHEDULING_ENDPOINT + "/session/getSessions?contextId=" + coursePlanInfo.getId()
                        + "&includeAttendees=true&sortType=START_TIME_DESC&size=25&contextType=COURSE_PLAN",
                HttpMethod.GET, null);
        String sessionListResp = resp.getEntity(String.class);
        Type SessionListType = new TypeToken<ArrayList<SessionInfo>>() {
        }.getType();
        List<SessionInfo> sessionsList = gson.fromJson(sessionListResp, SessionListType);

        // adding the teacher and student infos
        if (ArrayUtils.isNotEmpty(sessionsList)) {
            for (SessionInfo sessionInfo : sessionsList) {
                if (sessionInfo != null && ArrayUtils.isNotEmpty(sessionInfo.getAttendees())) {
                    for (UserSessionInfo userSessionInfo : sessionInfo.getAttendees()) {
                        userSessionInfo.updateUserDetails(
                                new UserBasicInfo(userInfos.get(userSessionInfo.getUserId()), exposeEmail));
                    }
                }
            }
        }
        coursePlanInfo.setSessionList(sessionsList);
    }

    public void validateCoursePlanAccess(CoursePlanBasicInfo coursePlanInfo) throws ForbiddenException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData != null && coursePlanInfo != null) {
            Long userId = sessionData.getUserId();
            if ((Role.STUDENT.equals(sessionData.getRole()) && !coursePlanInfo.getStudentId().equals(userId))
                    || (Role.TEACHER.equals(sessionData.getRole()) && !coursePlanInfo.getTeacherId().equals(userId))) {
                throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not allowed to access " + coursePlanInfo.getId());
            }
        }
    }
}
