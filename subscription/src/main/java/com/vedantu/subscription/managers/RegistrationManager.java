/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.RefundPolicy;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.request.RefundReq;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.onetofew.request.AddAdvancePaymentOrderIdReq;
import com.vedantu.onetofew.request.AddRegistrationReq;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.RegistrationDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.Registration;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.subscription.viewobject.request.GetRegistrationsForMultipleEntitiesReq;
import com.vedantu.subscription.viewobject.request.GetRegistrationsReq;
import com.vedantu.subscription.viewobject.request.RefundRegFee;
import com.vedantu.subscription.viewobject.response.GetRegistrationsResp;
import com.vedantu.subscription.viewobject.response.GetUserDashboardRegistrationsResp;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class RegistrationManager {

    @Autowired
    private RegistrationDAO registrationDAO;

    @Autowired
    private CourseDAO courseDAO;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private final Logger logger = logFactory.getLogger(RegistrationManager.class);

    @Autowired
    private DozerBeanMapper mapper;

    private final Gson gson = new Gson();

    public List<Registration> getRegistrations(EntityType type, String entityId, RegistrationStatus status,
            Long userId) {
        return registrationDAO.getRegistrations(type, entityId, status, userId);
    }

    public PlatformBasicResponse markRegistrationStatus(String regId, EntityType type, String entityId, RegistrationStatus newstatus,
            Long userId, List<String> deliverableEntityIds, List<RegistrationStatus> oldStatuses) throws NotFoundException, BadRequestException, VException {
        Registration registration;
        if (regId != null) {
            registration = registrationDAO.getById(regId);
            if (registration == null) {
                throw new NotFoundException(ErrorCode.REGISTRATION_NOT_FOUND, "registration not found " + regId);
            }
        } else {
            List<Registration> regs = registrationDAO.getRegistrationsMultipleStatus(type, entityId, oldStatuses, userId);
            if (regs.isEmpty()) {
                throw new NotFoundException(ErrorCode.REGISTRATION_NOT_FOUND, "registration not found "
                        + type + " " + entityId + " " + userId);
            }
            if (regs.size() > 1) {
                logger.error("Multiple registrations found for regs :" + regs.get(0).getId());
            }
            registration = regs.get(0);
        }
        registration.setStatus(newstatus);
        if (ArrayUtils.isNotEmpty(deliverableEntityIds)) {
            registration.setDeliverableEntityIds(deliverableEntityIds);
        }
        if (RegistrationStatus.ENROLLED.equals(newstatus) && (EntityType.OTF_BATCH_REGISTRATION.equals(registration.getEntityType())
                || EntityType.OTF_COURSE_REGISTRATION.equals(registration.getEntityType()))) {
            //FIXING the bulk price,hrs,instalment str when trial enrollment is created 
            //for otf course/batch reg is created

            if (ArrayUtils.isEmpty(deliverableEntityIds)) {
                throw new BadRequestException(ErrorCode.ENROLLMENT_NOT_FOUND, "Enrollments not provided");
            }
            String enrollmentId = deliverableEntityIds.get(0);

            Enrollment enrollment = enrollmentDAO.getById(enrollmentId);
            String batchId = enrollment.getBatchId();
            Batch batch = batchDAO.getById(batchId, Arrays.asList("purchasePrice", "duration"));
            registration.setBulkPriceToPay(batch.getPurchasePrice());
            if (batch.getDuration() != null) {
                registration.setDuration(batch.getDuration().intValue());
            }
            List<BaseInstalmentInfo> instalments = getInstalmentDetails(batchId, InstalmentPurchaseEntity.BATCH, registration.getUserId());
            if (ArrayUtils.isNotEmpty(instalments)) {
                registration.setInstalmentDetails(instalments);
            }
        }
        registrationDAO.save(registration);
        return new PlatformBasicResponse();
    }

    public void checkAndUpdateAdvancedPaymentRegistration(EntityType type, String entityId, Long userId) throws BadRequestException {
        List<Registration> regs = registrationDAO.getRegistrations(type, entityId, null, userId, false);

        if (ArrayUtils.isEmpty(regs)) {
            logger.info("No Advanced payment for userId: " + userId + " and bundleId: " + entityId);
        } else {
            Registration registration = regs.get(0);

            List<String> enrollmentIds = registration.getDeliverableEntityIds();
            registration.setAdvancePaymentConsumed(true);

            enrollmentDAO.updateStatus(enrollmentIds);

            if (ArrayUtils.isEmpty(enrollmentIds)) {
                throw new BadRequestException(ErrorCode.ENROLLMENT_NOT_FOUND, "Enrollments not provided");
            }

            registrationDAO.save(registration);

        }

    }

    public Registration addRegistration(AddRegistrationReq req) throws VException {
        Registration registration = mapper.map(req, Registration.class);
        if (EntityType.OTM_BUNDLE_REGISTRATION.equals(req.getEntityType())) {
            List<OTMBundleEntityInfo> entities = registration.getEntities();
            if (ArrayUtils.isNotEmpty(entities)) {
                List<String> ids = new ArrayList<>();
                entities.stream().filter((info) -> (StringUtils.isNotEmpty(info.getEntityId()))).forEachOrdered((info) -> {
                    ids.add(info.getEntityId());
                });
                Map<String, Long> durationMap = new HashMap<>();
                if (EntityType.OTF_COURSE.equals(entities.get(0).getEntityType())) {
                    List<Course> courses = courseDAO.getCoursesBasicInfos(ids, Arrays.asList("duration"));
                    if (ArrayUtils.isNotEmpty(courses)) {
                        courses.forEach((course) -> {
                            durationMap.put(course.getId(), course.getDuration());
                        });
                    }
                } else if (EntityType.OTF.equals(entities.get(0).getEntityType())) {
                    List<Batch> batches = batchDAO.getBatches(ids, Arrays.asList("duration"));
                    if (ArrayUtils.isNotEmpty(batches)) {
                        batches.forEach((batch) -> {
                            durationMap.put(batch.getId(), batch.getDuration());
                        });
                    }
                }

                entities.forEach((info) -> {
                    if (durationMap.get(info.getEntityId()) != null) {
                        info.setDuration(durationMap.get(info.getEntityId()).intValue());
                    } else {
                        logger.error("No duration found for " + info.getEntityId() + ", type " + info.getEntityType());
                    }
                });
            }
            List<BaseInstalmentInfo> instalments = getInstalmentDetails(req.getEntityId(), InstalmentPurchaseEntity.OTF_BUNDLE, req.getUserId());
            if (ArrayUtils.isNotEmpty(instalments)) {
                registration.setInstalmentDetails(instalments);
            }
        }
        registrationDAO.save(registration);
        return registration;
    }

    private List<BaseInstalmentInfo> getInstalmentDetails(String entityId,
            InstalmentPurchaseEntity entityType, Long userId) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getBaseInstalment?instalmentPurchaseEntity=" + entityType.name()
                + "&purchaseEntityId=" + entityId;
        if (userId != null) {
            url += "&userId=" + userId;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        BaseInstalment response = gson.fromJson(jsonString, BaseInstalment.class);
        if (response != null && response.getInfo() != null) {
            return response.getInfo();
        } else {
            return null;
        }
    }

    private Map<String, OrderInfo> getOrderInfoMapByOrderIds(Set<String> orderIds) throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        String url = "/payment/getOrdersByOrderIds?orderIds=" + org.springframework.util.StringUtils.collectionToDelimitedString(orderIds, ",");
        ClientResponse resp = WebUtils.INSTANCE.doCall(dineroEndpoint + url, HttpMethod.GET, null);

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<List<OrderInfo>>() {
        }.getType();
        List<OrderInfo> response = gson.fromJson(jsonString, listType);
        Map<String, OrderInfo> orderInfoMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(response)) {
            for (OrderInfo orderInfo : response) {
                String key = orderInfo.getId();
                orderInfoMap.put(key, orderInfo);
            }
        }
        return orderInfoMap;
    }

    public List<GetRegistrationsResp> getRegistrationsForUser(GetRegistrationsReq req) throws BadRequestException, VException {

        if (req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null");
        }
        List<GetRegistrationsResp> response = new ArrayList<>();
        List<Registration> registrations = registrationDAO.getRegistrations(req.getEntityType(),
                req.getEntityId(), RegistrationStatus.REGISTERED, req.getUserId());
        Set<String> bundleIds = new HashSet<>();
        Set<String> courseIds = new HashSet<>();
        Set<String> batchIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(registrations)) {
//            registrations.stream().filter(s -> !StringUtils.isEmpty(s.getEntityId())).forEach(s -> entityIds.add(s.getEntityId()));
            for (Registration registration : registrations) {
                if (StringUtils.isNotEmpty(registration.getEntityId())) {
                    if (EntityType.OTM_BUNDLE.equals(registration.getEntityType())
                            || EntityType.OTM_BUNDLE_REGISTRATION.equals(registration.getEntityType())
                            || EntityType.OTM_BUNDLE_ADVANCE_PAYMENT.equals(registration.getEntityType())) {
                        bundleIds.add(registration.getEntityId());
                    }
                    if (EntityType.OTF_COURSE.equals(registration.getEntityType())
                            || EntityType.OTF_COURSE_REGISTRATION.equals(registration.getEntityType())) {
                        courseIds.add(registration.getEntityId());
                    }
                    if (EntityType.OTF.equals(registration.getEntityType())
                            || EntityType.OTF_BATCH_REGISTRATION.equals(registration.getEntityType())) {
                        batchIds.add(registration.getEntityId());
                    }

                }
                if (ArrayUtils.isNotEmpty(registration.getEntities())) {
                    for (OTMBundleEntityInfo bundleEntity : registration.getEntities()) {
                        if (EntityType.OTF_COURSE.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_COURSE_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            courseIds.add(bundleEntity.getEntityId());
                        }
                        if (EntityType.OTF.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_BATCH_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            batchIds.add(bundleEntity.getEntityId());
                        }
                    }
                }

            }
            Map<String, OTFBundleInfo> infoMap = new HashMap<>();

            if (ArrayUtils.isNotEmpty(bundleIds)) {
                GetOTFBundlesReq bundleReq = new GetOTFBundlesReq();
                bundleReq.setOtfBundleIds(new ArrayList<>(bundleIds));
                List<OTFBundleInfo> infos = otfBundleManager.getOTFBundleInfos(bundleReq);
                if (ArrayUtils.isNotEmpty(infos)) {
                    infoMap = infos.stream().collect(Collectors.toMap(OTFBundleInfo::getId, c -> c));
                }
            }
            Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(batchIds, false);
            Map<String, CourseBasicInfo> courseMap = enrollmentManager.getCourseMap(courseIds, false);

            for (Registration reg : registrations) {
                GetRegistrationsResp res = mapper.map(reg, GetRegistrationsResp.class);
                List<CourseBasicInfo> courseInfos = new ArrayList<>();
                List<BatchBasicInfo> batchInfos = new ArrayList<>();

                if (StringUtils.isNotEmpty(reg.getEntityId())) {
                    if (EntityType.OTM_BUNDLE.equals(reg.getEntityType())
                            || EntityType.OTM_BUNDLE_REGISTRATION.equals(reg.getEntityType())
                            || EntityType.OTM_BUNDLE_ADVANCE_PAYMENT.equals(reg.getEntityType())) {

                        res.setBundleInfo(infoMap.get(reg.getEntityId()));
                    }
                    if (EntityType.OTF_COURSE.equals(reg.getEntityType())
                            || EntityType.OTF_COURSE_REGISTRATION.equals(reg.getEntityType())) {
                        courseInfos.add(courseMap.get(reg.getEntityId()));
                    }
                    if (EntityType.OTF.equals(reg.getEntityType())
                            || EntityType.OTF_BATCH_REGISTRATION.equals(reg.getEntityType())) {
                        batchInfos.add(batchMap.get(reg.getEntityId()));
                    }
                }
                if (ArrayUtils.isNotEmpty(reg.getEntities())) {
                    for (OTMBundleEntityInfo bundleEntity : reg.getEntities()) {
                        if (EntityType.OTF_COURSE.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_COURSE_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            courseInfos.add(courseMap.get(bundleEntity.getEntityId()));
                        }
                        if (EntityType.OTF.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_BATCH_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            batchInfos.add(batchMap.get(bundleEntity.getEntityId()));
                        }
                    }
                }
                res.setBatchInfos(batchInfos);
                res.setCourseInfos(courseInfos);
                response.add(res);
            }
        } else {
            logger.info("No registrations Found");
        }
        return response;
    }

    public List<GetUserDashboardRegistrationsResp> getRegistrationsForUserDashboard(GetRegistrationsReq req) throws BadRequestException, VException {

        if (req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null");
        }
        List<GetUserDashboardRegistrationsResp> response = new ArrayList<>();
        List<RegistrationStatus> list = new ArrayList<>();
        list.add(RegistrationStatus.REGISTERED);
        list.add(RegistrationStatus.REFUNDED);
        List<Registration> registrations = registrationDAO.getRegistrations(null, req.getUserId(), list);
        Set<String> bundleIds = new HashSet<>();
        Set<String> courseIds = new HashSet<>();
        Set<String> batchIds = new HashSet<>();
        Set<String> orderIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(registrations)) {
            for (Registration registration : registrations) {
                if (StringUtils.isNotEmpty(registration.getEntityId())) {
                    if (EntityType.OTM_BUNDLE.equals(registration.getEntityType())
                            || EntityType.OTM_BUNDLE_REGISTRATION.equals(registration.getEntityType())
                            || EntityType.OTM_BUNDLE_ADVANCE_PAYMENT.equals(registration.getEntityType())) {
                        bundleIds.add(registration.getEntityId());
                    }
                    if (EntityType.OTF_COURSE.equals(registration.getEntityType())
                            || EntityType.OTF_COURSE_REGISTRATION.equals(registration.getEntityType())) {
                        courseIds.add(registration.getEntityId());
                    }
                    if (EntityType.OTF.equals(registration.getEntityType())
                            || EntityType.OTF_BATCH_REGISTRATION.equals(registration.getEntityType())) {
                        batchIds.add(registration.getEntityId());
                    }
                }
                if (StringUtils.isNotEmpty(registration.getOrderId())) {
                    orderIds.add(registration.getOrderId());
                }
                if (ArrayUtils.isNotEmpty(registration.getEntities())) {
                    for (OTMBundleEntityInfo bundleEntity : registration.getEntities()) {
                        if (EntityType.OTF_COURSE.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_COURSE_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            courseIds.add(bundleEntity.getEntityId());
                        }
                        if (EntityType.OTF.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_BATCH_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            batchIds.add(bundleEntity.getEntityId());
                        }
                    }
                }

            }
            Map<String, OTFBundleInfo> infoMap = new HashMap<>();

            if (ArrayUtils.isNotEmpty(bundleIds)) {
                GetOTFBundlesReq bundleReq = new GetOTFBundlesReq();
                bundleReq.setOtfBundleIds(new ArrayList<>(bundleIds));
                List<OTFBundleInfo> infos = otfBundleManager.getOTFBundleInfos(bundleReq);
                if (ArrayUtils.isNotEmpty(infos)) {
                    infoMap = infos.stream().collect(Collectors.toMap(OTFBundleInfo::getId, c -> c));
                }
            }
            Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(batchIds, true);
            Map<String, CourseBasicInfo> courseMap = enrollmentManager.getCourseMap(courseIds, true);
            Map<String, OrderInfo> orderInfoMap = getOrderInfoMapByOrderIds(orderIds);

            for (Registration reg : registrations) {
                GetUserDashboardRegistrationsResp res = mapper.map(reg, GetUserDashboardRegistrationsResp.class);
                List<CourseBasicInfo> courseInfos = new ArrayList<>();
                List<BatchBasicInfo> batchInfos = new ArrayList<>();

                if (StringUtils.isNotEmpty(reg.getEntityId())) {
                    if (EntityType.OTM_BUNDLE.equals(reg.getEntityType())
                            || EntityType.OTM_BUNDLE_REGISTRATION.equals(reg.getEntityType())
                            || EntityType.OTM_BUNDLE_ADVANCE_PAYMENT.equals(reg.getEntityType())) {

                        res.setBundleInfo(infoMap.get(reg.getEntityId()));
                    }
                    if (EntityType.OTF_COURSE.equals(reg.getEntityType())
                            || EntityType.OTF_COURSE_REGISTRATION.equals(reg.getEntityType())) {
                        courseInfos.add(courseMap.get(reg.getEntityId()));
                    }
                    if (EntityType.OTF.equals(reg.getEntityType())
                            || EntityType.OTF_BATCH_REGISTRATION.equals(reg.getEntityType())) {
                        batchInfos.add(batchMap.get(reg.getEntityId()));
                    }
                }
                if (ArrayUtils.isNotEmpty(reg.getEntities())) {
                    for (OTMBundleEntityInfo bundleEntity : reg.getEntities()) {
                        if (EntityType.OTF_COURSE.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_COURSE_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            courseInfos.add(courseMap.get(bundleEntity.getEntityId()));
                        }
                        if (EntityType.OTF.equals(bundleEntity.getEntityType())
                                || EntityType.OTF_BATCH_REGISTRATION.equals(bundleEntity.getEntityType())) {
                            batchInfos.add(batchMap.get(bundleEntity.getEntityId()));
                        }
                    }
                }
                String orderId = reg.getOrderId();
                if (StringUtils.isNotEmpty(orderId)) {
                    res.setOrderInfo(orderInfoMap.get(orderId));
                }
                res.setBatchInfos(batchInfos);
                res.setCourseInfos(courseInfos);
                response.add(res);
            }
        } else {
            logger.info("No registrations Found");
        }
        return response;
    }

    public PlatformBasicResponse addAdvancePaymentOrderId(AddAdvancePaymentOrderIdReq req) throws NotFoundException, BadRequestException, VException {
        req.verify();
        List<Registration> registrations = registrationDAO.getRegistrations(req.getEntityType(), req.getEntityId(), RegistrationStatus.REGISTERED, req.getUserId());
        if (ArrayUtils.isEmpty(registrations)) {
            throw new NotFoundException(ErrorCode.REGISTRATION_NOT_FOUND, "registration not found to update advacePaymentOrderId: " + req.getOrderId());
        } else {
            if (registrations.size() > 1) {
                logger.error("multiple registrations forund for addAdvancePaymentOrderId " + req);
            }
            Registration registration = registrations.get(0);
            registration.setAdvancePaymentOrderId(req.getOrderId());
            registrationDAO.save(registration);
        }

        return new PlatformBasicResponse();
    }

    public GetRegistrationsResp getRegistrationsForUserAndEntity(GetRegistrationsReq req) throws VException {
        if (req.getUserId() == null || StringUtils.isEmpty(req.getEntityId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "invalid userId/entityId");
        }
        GetRegistrationsResp res = null;

        List<Registration> regs = registrationDAO.getRegistrations(req.getEntityId(), req.getUserId());
        if (ArrayUtils.isNotEmpty(regs)) {
            Collections.sort(regs, new Comparator<Registration>() {
                @Override
                public int compare(Registration o1, Registration o2) {
                    return o2.getCreationTime().compareTo(o1.getCreationTime());
                }
            });
            res = mapper.map(regs.get(0), GetRegistrationsResp.class);
        }
        return res;
    }

    public Map<String, Registration> getRegistrationsForMultipleEntities(GetRegistrationsForMultipleEntitiesReq req) throws BadRequestException {
        req.verify();
        Set<String> batchIds = new HashSet<>();
        Set<String> validEntityIds = new HashSet<>();
        for (GetRegistrationsForMultipleEntitiesReq.RegistrationEntity entity : req.getEntities()) {
            if (EntityType.OTF.equals(entity.getEntityType())) {
                batchIds.add(entity.getEntityId());
            }
            validEntityIds.add(entity.getEntityId());
        }

        //fetching courseids since the reg might have happened on course as well;
        List<Batch> batches = batchDAO.getBatches(new ArrayList<>(batchIds), Arrays.asList("courseId"));
        Map<String, String> batchCourseIdMap = new HashMap<>();
        if (ArrayUtils.isNotEmpty(batches)) {
            for (Batch batch : batches) {
                validEntityIds.add(batch.getCourseId());
                batchCourseIdMap.put(batch.getId(), batch.getCourseId());
            }
        }

        List<Registration> registrations = registrationDAO.getRegistrations(validEntityIds, req.getUserId(), Arrays.asList(RegistrationStatus.ENROLLED, RegistrationStatus.REGISTERED));
        if (ArrayUtils.isNotEmpty(registrations)) {
            //sorting in ascending order
            Collections.sort(registrations, new Comparator<Registration>() {
                @Override
                public int compare(Registration o1, Registration o2) {
                    return o1.getCreationTime().compareTo(o2.getCreationTime());
                }
            });
            Map<String, Registration> entityIdVsRegMap = new HashMap<>();
            for (Registration registration : registrations) {
                entityIdVsRegMap.put(registration.getEntityId(), registration);
            }

            Map<String, Registration> reqEntityIdVsRegMap = new HashMap<>();
            for (GetRegistrationsForMultipleEntitiesReq.RegistrationEntity entity : req.getEntities()) {
                if (entityIdVsRegMap.containsKey(entity.getEntityId())) {
                    reqEntityIdVsRegMap.put(entity.getEntityId(), entityIdVsRegMap.get(entity.getEntityId()));
                } else {
                    if (EntityType.OTF.equals(entity.getEntityType())) {
                        String courseId = batchCourseIdMap.get(entity.getEntityId());
                        if (courseId != null && entityIdVsRegMap.containsKey(courseId)) {
                            reqEntityIdVsRegMap.put(entity.getEntityId(), entityIdVsRegMap.get(courseId));
                        }
                    }
                }
            }
            return reqEntityIdVsRegMap;
        }
        return null;
    }

    public Map<String, List<BaseInstalmentInfo>> getBaseInstalmentInfosForMultipleEntities(GetRegistrationsForMultipleEntitiesReq req) throws BadRequestException {
        Map<String, Registration> regMap = getRegistrationsForMultipleEntities(req);
        Map<String, List<BaseInstalmentInfo>> baseInstalmentsMap = new HashMap<>();
        if (regMap == null) {
            return baseInstalmentsMap;
        }
        for (String entityId : regMap.keySet()) {
            Registration registration = regMap.get(entityId);
            if (ArrayUtils.isNotEmpty(registration.getInstalmentDetails())) {
                baseInstalmentsMap.put(entityId, registration.getInstalmentDetails());
            }
        }
        return baseInstalmentsMap;
    }

    public List<BaseInstalmentInfo> getApplicableBaseInstalment(Long userId, String entityId) throws BadRequestException {
        GetRegistrationsForMultipleEntitiesReq req = new GetRegistrationsForMultipleEntitiesReq();
        req.setUserId(userId);
        req.addEntity(entityId, null);
        Map<String, Registration> entityIdVsRegMap = getRegistrationsForMultipleEntities(req);
        if (entityIdVsRegMap != null && entityIdVsRegMap.containsKey(entityId) && ArrayUtils.isNotEmpty(entityIdVsRegMap.get(entityId).getInstalmentDetails())) {
            return entityIdVsRegMap.get(entityId).getInstalmentDetails();
        } else {
            return null;
        }
    }

    public List<GetRegistrationsResp> getUsersRegistrationForMultipleEntities(Long userId, List<String> entityIds) throws BadRequestException {
        GetRegistrationsForMultipleEntitiesReq req = new GetRegistrationsForMultipleEntitiesReq();
        req.setUserId(userId);
        if (ArrayUtils.isNotEmpty(entityIds)) {
            for (String entityId : entityIds) {
                req.addEntity(entityId, null);
            }
        }
            Map<String, Registration> entityIdVsRegMap = getRegistrationsForMultipleEntities(req);
        List<GetRegistrationsResp> regResponse = new ArrayList<>();

        if (entityIdVsRegMap != null && !entityIdVsRegMap.isEmpty() && entityIdVsRegMap.values() != null) {
            for (Registration registration : entityIdVsRegMap.values()) {
                if (registration != null) {
                    GetRegistrationsResp resp = mapper.map(registration, GetRegistrationsResp.class);
                    regResponse.add(resp);
                }

            }
        }
        return regResponse;
    }

    public RefundRes refundRegFee(RefundRegFee req) throws VException, CloneNotSupportedException {
        req.verify();
        RefundReq refundReq = new RefundReq();
        refundReq.setUserId(req.getUserId());
        refundReq.setCallingUserId(req.getCallingUserId());
        refundReq.setRefundPolicy(RefundPolicy.FULL_REFUND);
        refundReq.setGetRefundInfoOnly(req.getRefundInfoOnly());
        refundReq.setEntityType(req.getEntityType());
        refundReq.setEntityId(req.getEntityId());
        RefundRes res = refundRegFee(refundReq);
        return res;
    }

    public RefundRes refundRegFee(RefundReq req)
            throws VException {
        String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                dineroEndpoint + "/refund", HttpMethod.POST, new Gson().toJson(req));
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("EXIT: " + jsonString);
        RefundRes res = new Gson().fromJson(jsonString, RefundRes.class);

        if (Boolean.TRUE.equals(req.getGetRefundInfoOnly())) {
            return res;
        }
        switch (req.getEntityType()) {
            case OTF_COURSE_REGISTRATION:
            case OTM_BUNDLE_REGISTRATION:
                markRegistrationStatus(null, req.getEntityType(), req.getEntityId(), RegistrationStatus.REFUNDED,
                        req.getUserId(), null, Arrays.asList(RegistrationStatus.REGISTERED));
                break;
            default:
                break;
        }

        return res;
    }
}
