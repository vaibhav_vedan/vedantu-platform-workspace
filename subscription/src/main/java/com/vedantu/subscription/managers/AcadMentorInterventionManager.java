package com.vedantu.subscription.managers;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.dao.AcadMentorInterventionDAO;
import com.vedantu.subscription.entities.mongo.AcadMentorIntervention;
import com.vedantu.subscription.viewobject.request.GeneralInterventionReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.json.JSONObject;

@Service
public class AcadMentorInterventionManager {

    @Autowired
    private AcadMentorInterventionDAO acadMentorInterventionDAO;

    @Autowired
    private AcadMentorStudentsManager acadMentorStudentsManager;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private LogFactory logFactory;

    private static final Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AcadMentorInterventionManager.class);

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");
    private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
    private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

    public AcadMentorInterventionManager() {
        super();
    }

    public PlatformBasicResponse scanAndSaveInterventions(Long fromTime) throws VException, UnsupportedEncodingException {
        List<UserBasicInfo> acadMentors = this.getAllAcadMentors();
        if (acadMentors.size() >= 75) {
            logger.error("acadMentors more than 100 almost, change the code");
        }
        for (UserBasicInfo acadMentor : acadMentors) {
            JSONObject sqsReq = new JSONObject();
            sqsReq.put("acadMentorId", acadMentor.getUserId());
            sqsReq.put("fromTime", fromTime);
            awsSQSManager.sendToSQS(SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE, SQSMessageType.SCAN_FOR_AM_INTERVENTIONS, sqsReq.toString());
        }
        return new PlatformBasicResponse();
    }

    public List<AcadMentorIntervention> scanForInterventionsForAMStudents(Long acadMentorId, Long fromTime) throws VException {
        List<AcadMentorIntervention> acadMentorInterventions = new ArrayList<>();
        List<Long> studentsOfAcadMentor = acadMentorStudentsManager.getStudentIdsByAcadMentor(acadMentorId);
        if (ArrayUtils.isNotEmpty(studentsOfAcadMentor)) {
            logger.info("students to scan " + studentsOfAcadMentor.size());
            for (Long studentId : studentsOfAcadMentor) {
                logger.info("scanForInterventionsForAMStudent " + studentId);
                if (getBadAttendancePastThreeSessionsIntervention(fromTime, studentId, acadMentorId) != null) {
                    acadMentorInterventions.add(getBadAttendancePastThreeSessionsIntervention(fromTime, studentId, acadMentorId));
                }
                if (getBadAttendancePastMonthIntervention(fromTime, studentId, acadMentorId) != null) {
                    acadMentorInterventions.add(getBadAttendancePastMonthIntervention(fromTime, studentId, acadMentorId));
                }
                if (getEngagementIntervention(fromTime, studentId, acadMentorId) != null) {
                    acadMentorInterventions.add(getEngagementIntervention(fromTime, studentId, acadMentorId));
                }
                if (getAssignmentIntervention(fromTime, studentId, acadMentorId) != null) {
                    acadMentorInterventions.add(getAssignmentIntervention(fromTime, studentId, acadMentorId));
                }
                if (getLowPercentileIntervention(fromTime, studentId, acadMentorId) != null) {
                    acadMentorInterventions.add(getLowPercentileIntervention(fromTime, studentId, acadMentorId));
                }
            }

            if (acadMentorInterventions.size() > 0) {
                logger.info("acadMentorInterventions.size(): " + acadMentorInterventions.size());
            }
            acadMentorInterventionDAO.save(acadMentorInterventions);
        }
        return acadMentorInterventions;
    }

    public Map< Long, List<AcadMentorIntervention>> retrieveOpenInterventions(String acadMentorEmail) throws UnsupportedEncodingException, VException {
        List<AcadMentorIntervention> openInterventions = acadMentorInterventionDAO.retrieveOpenInterventions(getUserBasicInfoFromEmail(acadMentorEmail).getUserId());
        logger.info("Logger info openInterventions" + openInterventions);
        Map< Long, List<AcadMentorIntervention>> studentInterventions = new HashMap<>();
        for (AcadMentorIntervention openIntervention : openInterventions) {
            if (studentInterventions.containsKey(openIntervention.getStudentId())) {
                List<AcadMentorIntervention> interventions = studentInterventions.get(openIntervention.getStudentId());
                interventions.add(openIntervention);
                studentInterventions.put(openIntervention.getStudentId(), interventions);
            } else {
                List<AcadMentorIntervention> interventions = new ArrayList<>();
                interventions.add(openIntervention);
                studentInterventions.put(openIntervention.getStudentId(), interventions);
            }
        }
        logger.info("Logger info studentInterventions" + studentInterventions);

        return studentInterventions;
    }

    public List<AcadMentorIntervention> retrieveInterventionsByStudent(Long studentId, Integer start, Integer size) {
        //TODO if the user accessing is a Teacher/ACADMentor, see if the student is alloted to this acadmentor 
        return acadMentorInterventionDAO.retrieveInterventionsByStudent(studentId, start, size);
    }

    public void closeIntervention(AcadMentorIntervention acadMentorIntervention) {
        acadMentorIntervention.setAcadMentorCommentTime(System.currentTimeMillis());
        acadMentorInterventionDAO.closeIntervention(acadMentorIntervention);
    }
    
    public AcadMentorIntervention generalIntervention(GeneralInterventionReq generalInterventionReq) throws UnsupportedEncodingException, VException {
    	AcadMentorIntervention acadMentorIntervention = new AcadMentorIntervention();
    	acadMentorIntervention.setAcadMentorId(getUserBasicInfoFromEmail(generalInterventionReq.getAcadMentorEmail()).getUserId());
    	acadMentorIntervention.setStudentId(generalInterventionReq.getStudentId());
    	acadMentorIntervention.setAcadMentorComment(generalInterventionReq.getComment());
    	acadMentorIntervention.setInterventionReason("General");
    	acadMentorIntervention.setClosed(true);
    	acadMentorIntervention.setAcadMentorCommentTime(System.currentTimeMillis());  
    	
    	acadMentorInterventionDAO.saveOne(acadMentorIntervention);
    	return acadMentorIntervention;
    }

    private AcadMentorIntervention getBadAttendancePastThreeSessionsIntervention(Long creationTime, Long studentId, Long acadMentorId) throws VException {
    	String url = SCHEDULING_ENDPOINT + "/am-inclass/hasBadAttendanceInPast3Sessions/" + studentId + "?creationTime=" + creationTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        logger.info("/hasBadAttendanceInPast3Sessions returned: " + respString);
        if ("true".equals(respString)) {
            AcadMentorIntervention acadMentorIntervention = new AcadMentorIntervention();
            acadMentorIntervention.setStudentId(studentId);
            acadMentorIntervention.setAcadMentorId(acadMentorId);
            acadMentorIntervention.setInterventionReason("The student has missed at least 3 consecutive classes.");
            logger.info("AcadMentorIntervention to save: StudentId:" + acadMentorIntervention.getStudentId() + ", AcadMentorId:" + acadMentorIntervention.getAcadMentorId());
            return acadMentorIntervention;
        }
        return null;
    }
    
    private AcadMentorIntervention getBadAttendancePastMonthIntervention(Long creationTime, Long studentId, Long acadMentorId) throws VException {    	
    	String url = SCHEDULING_ENDPOINT + "/am-inclass/hasBadAttendanceInPastMonth/" + studentId + "?creationTime=" + creationTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        logger.info("/hasBadAttendanceInPastMonth returned: " + respString);
        if ("true".equals(respString)) {
            AcadMentorIntervention acadMentorIntervention = new AcadMentorIntervention();
            acadMentorIntervention.setStudentId(studentId);
            acadMentorIntervention.setAcadMentorId(acadMentorId);
            acadMentorIntervention.setInterventionReason("The student has less than 30 percent attendance in the past month");
            logger.info("AcadMentorIntervention to save: StudentId:" + acadMentorIntervention.getStudentId() + ", AcadMentorId:" + acadMentorIntervention.getAcadMentorId());
            return acadMentorIntervention;
        }
        return null;
    }

    private AcadMentorIntervention getEngagementIntervention(Long creationTime, Long studentId, Long acadMentorId) throws VException {
    	String url = SCHEDULING_ENDPOINT + "/am-inclass/isLessThan20PercentEngagement/" + studentId + "?creationTime=" + creationTime;
    	ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        logger.info("/isLessThan20PercentEngagement returned: " + respString);
        if ("true".equals(respString)) {
            AcadMentorIntervention acadMentorIntervention = new AcadMentorIntervention();
            acadMentorIntervention.setStudentId(studentId);
            acadMentorIntervention.setAcadMentorId(acadMentorId);
            acadMentorIntervention.setInterventionReason("Engagement is 20 percent or less in the past 3 sessions.");
            logger.info("AcadMentorIntervention to save: StudentId:" + acadMentorIntervention.getStudentId() + ", AcadMentorId:" + acadMentorIntervention.getAcadMentorId());
            return acadMentorIntervention;
        }
        return null;
    }

    private AcadMentorIntervention getAssignmentIntervention(Long creationTime, Long studentId, Long acadMentorId) throws VException {
        String url = LMS_ENDPOINT + "/isLastThreeAssignmentsInterventionNeeded/" + studentId + "?creationTime=" + creationTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        logger.info("/isLastThreeAssignmentsInterventionNeeded returned: " + respString);
        if ("true".equals(respString)) {
            AcadMentorIntervention acadMentorIntervention = new AcadMentorIntervention();
            acadMentorIntervention.setStudentId(studentId);
            acadMentorIntervention.setAcadMentorId(acadMentorId);
            acadMentorIntervention.setInterventionReason("An assignment submitted after 2 unsubmitted assignments.");
            logger.info("AcadMentorIntervention to save: StudentId:" + acadMentorIntervention.getStudentId() + ", AcadMentorId:" + acadMentorIntervention.getAcadMentorId());
            return acadMentorIntervention;
        }
        return null;
    }

    private AcadMentorIntervention getLowPercentileIntervention(Long creationTime, Long studentId, Long acadMentorId) throws VException {
        String url = LMS_ENDPOINT + "/isInLastTenPercentileInTests/" + studentId + "?creationTime=" + creationTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        logger.info("/isInLastTenPercentileInTests returned: " + respString);

        if (respString.equals("true")) {
            AcadMentorIntervention acadMentorIntervention = new AcadMentorIntervention();
            acadMentorIntervention.setStudentId(studentId);
            acadMentorIntervention.setAcadMentorId(acadMentorId);
            acadMentorIntervention.setInterventionReason("Is in last 10 percentile of students in last tests.");
            logger.info("AcadMentorIntervention to save: StudentId:" + acadMentorIntervention.getStudentId() + ", AcadMentorId:" + acadMentorIntervention.getAcadMentorId());
            return acadMentorIntervention;
        }
        return null;
    }

    private List<UserBasicInfo> getAllAcadMentors() throws VException {
        String url = USER_ENDPOINT + "/getAllAcadMentors?start=0&size=100";
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("AcadMentors list:" + jsonString);
        List<UserBasicInfo> acadMentors = new ArrayList(Arrays.asList(new Gson().fromJson(jsonString, UserBasicInfo[].class)));
        return acadMentors;
    }

    private UserBasicInfo getUserBasicInfoFromEmail(final String userEmail) throws VException, UnsupportedEncodingException {
        String url = USER_ENDPOINT + "/" + "getUserBasicInfoByEmail" + "?email=" + URLEncoder.encode(userEmail, "UTF-8") + "&exposeEmail=" + true;
        logger.info("Logger: url composed is: " + url);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        UserBasicInfo userBasicInfo = new Gson().fromJson(respString, UserBasicInfo.class);
        logger.info("Logger: Callresponse userBasicInfo: " + userBasicInfo);
        return userBasicInfo;
    }

}
