package com.vedantu.subscription.managers;

import org.springframework.stereotype.Component;

@Component
public class TestManager {

	public TestManager() {
		super();
	}
	
	public String getString(){
		return "hello";
	}
	
}
