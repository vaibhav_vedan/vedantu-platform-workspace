package com.vedantu.subscription.managers;

import com.google.common.collect.ImmutableSet;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserInfo;
import com.vedantu.exception.*;
import com.vedantu.lms.cmds.pojo.LMSTestInfo;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.scheduling.pojo.session.OTMSessionType;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.subscription.dao.GameJourneyDAO;
import com.vedantu.subscription.dao.GameTemplateDao;
import com.vedantu.subscription.entities.mongo.GameJourney;
import com.vedantu.subscription.entities.mongo.GameTemplate;
import com.vedantu.subscription.enums.game.*;
import com.vedantu.subscription.pojo.game.*;
import com.vedantu.subscription.request.GameSetupRequest;
import com.vedantu.subscription.request.RewardStatusChangeRequest;
import com.vedantu.subscription.response.*;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.viewobject.request.GamificationToolFilterRequest;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.annotation.Nullable;
import javax.mail.internet.InternetAddress;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class GameManager {

    private static final Logger logger = LogFactory.getLogger(GameManager.class);

    @Autowired
    private GameJourneyDAO gameJourneyDAO;

    @Autowired
    private GameTemplateDao gameTemplateDao;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private GameRewardItemManager gameRewardItemManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private AwsSQSManager sqsManager;

    @Autowired
    private AwsS3Manager awsS3Manager;

    @Autowired
    private CommunicationManager communicationManager;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss:aa");
    private static final String REDIS_KEY_AB_GAMEJOURNEY = "AB_GAMEJOURNEY_KEY";
    private static final String REDIS_KEY_REWARD_RATIO_VAL = "REWARD_RATIO_VAL";
    public static final String REDIS_KEY_USER_JOURNEY_PREFIX = "REDIS_USER_GAME_JOURNEY_";
    public static final String TIME_ZONE_IN = "Asia/Kolkata";
    private static final String ENV = ConfigUtils.INSTANCE.getStringValue("environment").toLowerCase();
    private static final String S3_BUCKET = ConfigUtils.INSTANCE.getStringValue("gamification.tool.csv.download.s3.bucket").toLowerCase();
    private static final Type token = new TypeToken<List<UserInfo>>() {
    }.getType();

    /**
     * CREATING DEFAULT CONSTANTS FOR A/B IF REDIS IS DOWN
     */
    private static Long abCounter = 0L;

    public static final Gson GSON = new Gson();

    public GameSetupResponse gameOnboarding(GameSetupRequest request) throws BadRequestException, InternalServerErrorException {
        // Stop Onboarding to Game for new Users
        if (true) {
            GameSetupResponse response = new GameSetupResponse();
            response.setEligibility(Eligibility.INELIGIBLE);
            return response;
        }
        request.verify();
        Long userId = request.getUserId();
        String enrollmentId = request.getEntityId();
        logger.info("entered onboarding for userId: {} with enrollmentId: {}", userId, enrollmentId);
        String activeGameTemplateId = gameTemplateDao.getActiveGameTemplateId();
        logger.info("active game id: {}", activeGameTemplateId);
        GameJourney journey = gameJourneyDAO.getJourneyUser(userId, activeGameTemplateId);

        GameSetupResponse response = new GameSetupResponse();

        try {
            if (journey != null) {
                response.setEligibility(journey.getEligibility());
                if (journey.getEligibility() == Eligibility.ELIGIBLE) {

                    logger.info("eligible journey already exists in db");
                    logger.info("enrollmentIds in db: {}", journey.getEnrollmentIds());

                    // ensured if we make a user to eligible state from db
                    if (CollectionUtils.isEmpty(journey.getEnrollmentIds())) {
                        journey.setEnrollmentIds(new HashSet<>());
                    }

                    if (journey.getEnrollmentIds().contains(enrollmentId)) {
                        logger.info("enrollmentId already exists in db");
                        response.setGameTaskList(journey.getGameTaskList());
                        return response;
                    }

                    logger.info("adding enrollmentId: {}", enrollmentId);
                    journey.getEnrollmentIds().add(enrollmentId);
                    gameJourneyDAO.addEnrollmentIds(journey);
                    response.setGameTaskList(journey.getGameTaskList());

                }
                return response;
            } else {
                logger.info("game journey not found in db for userId: {}", userId);
                journey = addGameJourney(userId, enrollmentId, request.getBatchIds(), request.getEnrollmentType());

                if (journey == null) {
                    return null;
                }
                response.setEligibility(journey.getEligibility());
                if (journey.getEligibility() == Eligibility.INELIGIBLE) {
                    return response;
                }
                response.setGameTaskList(journey.getGameTaskList());
            }

        } catch (Exception e) {
            logger.info("exception in gameOnboarding", e);
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        return response;
    }

    public GameJourney addGameJourney(Long userId, String enrollmentId, Set<String> batchIds, EnrollmentState enrollmentType) throws BadRequestException, InternalServerErrorException {

        logger.info("inside addGameJourney for userId: {}", userId);
        GameJourney journey = new GameJourney();
        journey.setUserId(userId);

        GameTemplate gameTemplate = gameTemplateDao.getActiveGameTemplate();
        journey.setGameId(gameTemplate.getId());
        try {
            Long val = redisDAO.incr(REDIS_KEY_AB_GAMEJOURNEY);
            val = val == null ? incrementValueForAb() : val;
            if (val % 2 != 0) {
                logger.info("{} not eligible for game as the value of redis key {} is {}", userId, REDIS_KEY_AB_GAMEJOURNEY, val);
                journey.setEligibility(Eligibility.INELIGIBLE);
                gameJourneyDAO.save(journey);
                setRedisValueForJourney(userId, journey);
                return journey;
            }
        } catch (Exception e) {
            redisDAO.del(getGameJourneyRedisKey(userId));
            logger.error("exception while A/B checking for user onboarding for game: ", e);
            return null;
        }

        logger.info("user is eligible for the game. Setting up game journey for userId: {}", userId);
        journey.setEligibility(Eligibility.ELIGIBLE);
        journey.setEnrollmentType(enrollmentType);
        List<GameTask> gameTaskList;
        try {
            gameTaskList = gameTemplate.setupGameTask();
            journey.setGameTaskList(gameTaskList);
            journey.setEnrollmentIds(ImmutableSet.of(enrollmentId));
            journey.setBatchIds(batchIds);
            logger.info("adding game journey to GameJourney collection");
            gameJourneyDAO.save(journey);
            setRedisValueForJourney(userId, journey);
        } catch (Exception e) {
            redisDAO.del(getGameJourneyRedisKey(userId));
            logger.info("exception occured in setupGameJourney");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        return journey;
    }


    public GameJourneyResponse getGameJourneyResponse(Long userId) {

        logger.info("getting journey for UI request for userId: {}", userId);
        GameJourneyResponse response = new GameJourneyResponse();
        response.setUserId(userId);

        try {

            String[] fields = {
                    GameJourney.Constants.GAME_TASKS_NAME, GameJourney.Constants.ELIGIBILITY,
                    GameJourney.Constants.GAME_TASKS_STATUS, GameJourney.Constants.GAME_TASKS_REWARDDETAILS
            };
            GameJourney journey = gameJourneyDAO.getJourneyForEligibleUser(userId, gameTemplateDao.getActiveGameTemplateId(),
                    fields);
            if (journey == null) {
                logger.info("no game journey exists for this user");
                response.setEligibility(Eligibility.INELIGIBLE);
                return response;
            }
            response.setEligibility(Eligibility.ELIGIBLE);
            response.setGameTasks(journey.getGameTaskList());
        } catch (Exception e) {
            logger.info("exception occured in getGameJourneyResponse");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        return response;
    }

    public List<ClaimedUsersResponse> getClaimedUsers(GamificationToolFilterRequest request) {

        logger.info("entered claimedUsers for gamification tool");
        List<ClaimedUsersResponse> claimedUsersResponseList = new ArrayList<>();

        if (request.getIsDownload() != null && request.getIsDownload()) {
            if (StringUtils.isEmpty(request.getEmailId())) {
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "email id is missing");
            }
            if (StringUtils.isEmpty(request.getName())) {
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "name is missing");
            }
            String message = GSON.toJson(request);
            logger.info("sending to GAMIFICATION_TOOL_CSV_DOWNLOAD queue: {}", message);
            sqsManager.sendToSQS(SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD, SQSMessageType.GAMIFICATION_TOOL_DOWNLOAD, message,
                    request.getEmailId());
            return claimedUsersResponseList;
        }

        try {
            List<GameJourney> claimedUsers = gameJourneyDAO.getClaimedUsers(request, gameTemplateDao.getActiveGameTemplateId());
            logger.info("response received");
            if (ArrayUtils.isEmpty(claimedUsers)) {
                logger.info("no claimed users");
                return claimedUsersResponseList;
            }

            claimedUsersResponseList = unwindClaimedUsers(claimedUsers, request);

        } catch (Exception e) {
            logger.info("exception occured in getClaimedUsers");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        return claimedUsersResponseList;
    }

    private List<ClaimedUsersResponse> unwindClaimedUsers(List<GameJourney> claimedUsers, GamificationToolFilterRequest request) {

        logger.info("unwinding claimedUsers");
        List<ClaimedUsersResponse> claimedUsersResponseList = new ArrayList<>();

        try {
            for (GameJourney journey : claimedUsers) {

                List<GameTask> rewardsList;

                // The following filter should always be kept above all the filters since it unwinds the document and returns only the tasks with rewards
                // filtering with status here again because lets say a user has reward1 CLAIMED and reward2 SHIPPED, then db filter with CLAIMED will give a document for this user having both CLAIMED and SHIPPED rewards
                if (request.getStatus() == null || "".equals(request.getStatus().toString())) {
                    rewardsList = journey.getGameTaskList().stream().filter(e -> (e.getStatus() == GameTaskStatus.CLAIMED || e.getStatus() == GameTaskStatus.SHIPPED)).collect(Collectors.toList());
                } else {
                    rewardsList = journey.getGameTaskList().stream().filter(e -> e.getStatus() == request.getStatus()).collect(Collectors.toList());
                }

                // unwinding rewards for start time and end time filter
                if (request.getStartTime() != null && request.getEndTime() != null) {
                    rewardsList = rewardsList.stream().filter(e -> e.getCompletedTime() >= request.getStartTime() && e.getCompletedTime() <= request.getEndTime()).collect(Collectors.toList());
                }

                // unwinding rewards for Reward Item
                if (!StringUtils.isEmpty(request.getRewardItem())) {
                    rewardsList = rewardsList.stream().filter(e -> e.getGameRewardDetails().getGameRewardItem().equals(request.getRewardItem())).collect(Collectors.toList());
                }

                // unwinding rewards for Reward Name - REWARD1,REWARD2
                if (!StringUtils.isEmpty(request.getRewardName())) {
                    rewardsList = rewardsList.stream().filter(e -> e.getName().equals(request.getRewardName())).collect(Collectors.toList());
                }

                for (GameTask gameTask : rewardsList) {
                    ClaimedUsersResponse claimedUsersResponse = new ClaimedUsersResponse();
                    claimedUsersResponse.setUserId(journey.getUserId());
                    claimedUsersResponse.setRewardName(gameTask.getName());
                    claimedUsersResponse.setStatus(gameTask.getStatus());
                    claimedUsersResponse.setRewardItem(gameTask.getGameRewardDetails().getGameRewardItem());
                    claimedUsersResponse.setAwb(gameTask.getGameRewardDetails().getAwb());
                    claimedUsersResponse.setClaimedTime(gameTask.getCompletedTime());
                    claimedUsersResponseList.add(claimedUsersResponse);
                }

            }
        } catch (Exception e) {
            logger.info("exception occured in unwindClaimedUsers");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        return claimedUsersResponseList;
    }

    public void downloadClaimedUsersData(GamificationToolFilterRequest request) throws IOException {

        logger.info("response from queue received");
        List<GameJourney> batchData;
        List<ClaimedUsersResponse> batchUnwindData;
        int start = 0;
        int size = 100;
        request.setSize(size);

        Path page = Files.createTempFile("page", ".csv");

        try {
            Map<String, String> headerMap = getHeadersForCSV();
            List<String> header = new ArrayList<>(headerMap.values());
            Files.write(page, String.join(",", header).concat(System.lineSeparator()).getBytes(),
                    StandardOpenOption.CREATE, StandardOpenOption.APPEND);
            String activeGameTemplateId = gameTemplateDao.getActiveGameTemplateId();

            while (true) {
                logger.info("start: {}", start);
                request.setStart(start);
                batchData = gameJourneyDAO.getClaimedUsers(request, activeGameTemplateId);
                if (CollectionUtils.isEmpty(batchData)) {
                    logger.info("no further more claimed users to be queried");
                    break;
                }
                batchUnwindData = unwindClaimedUsers(batchData, request);

                List<Long> userIds = batchUnwindData.stream().map(ClaimedUsersResponse::getUserId).collect(Collectors.toList());
                Map<Long, UserInfo> longUserInfoMap = getUserInfoMap(userIds, request);

                byte[] csvBytes = getClaimedUsersCSVBytes(batchUnwindData, headerMap, longUserInfoMap);

                Files.write(page, csvBytes, StandardOpenOption.CREATE, StandardOpenOption.APPEND);
                start += size;

                if (batchData.size() < size) {
                    logger.info("no further more claimed users to be queried");
                    break;
                }

                batchData.clear();
            }

            logger.info("entered finally to upload csv file in bucket: {}", S3_BUCKET);
            String key = ENV + "/temp/csv/" + System.currentTimeMillis() + ".csv";
            awsS3Manager.uploadFile(S3_BUCKET, key, page.toFile());
            logger.info(page.toAbsolutePath());
            String s3URL = awsS3Manager.getPublicBucketDownloadUrl(S3_BUCKET, key);
            logger.info("S3 download link: {}", s3URL);

            EmailRequest email = new EmailRequest();
            email.setBody("Please find the attached csv file\n" + s3URL);
            String subject = ConfigUtils.INSTANCE.getStringValue("environment") + ": "
                    + "Your Gamification Data is ready!";
            email.setSubject(subject);
            email.setType(CommunicationType.SYSTEM_INFO);
            ArrayList<InternetAddress> toList = new ArrayList<>();
            toList.add(new InternetAddress(request.getEmailId(), request.getName()));
            email.setTo(toList);
            communicationManager.sendEmailViaRest(email);

        } catch (Exception e) {
            logger.error("exception occured in downloadClaimedUsersData", e);
        } finally {
            if (Files.deleteIfExists(page)) {
                logger.info("{} deleted", page);
            }
        }

    }

    private Map<Long, UserInfo> getUserInfoMap(List<Long> userIds, GamificationToolFilterRequest request) {

        logger.info("getting userInfoMap");
        if (CollectionUtils.isEmpty(userIds)) {
            logger.info("no userIds given");
            return new HashMap<>();
        }

        String commaSeparatedUserIds = userIds.stream().map(String::valueOf).collect(Collectors.joining(","));
        String url = ConfigUtils.INSTANCE.getUserEndpoint() + "/getUserLocationInfos?userIds=" + commaSeparatedUserIds + "&callingUserId=" + request.getCallingUserId();

        try {
            ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            if (response.getStatus() == HttpStatus.OK.value()) {
                String entity = response.getEntity(String.class);

                List<UserInfo> userInfoList = GSON.fromJson(entity, token);
                Map<Long, UserInfo> map = new HashMap<>();
                for (UserInfo userInfo : userInfoList) {
                    map.put(userInfo.getUserId(), userInfo);
                }

                return map;
            } else {
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "getUserLocationInfos API call failed in getUserInfoMap");
            }
        } catch (Exception e) {
            logger.error("exception in getUserInfoMap", e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }


    }

    private Map<String, String> getHeadersForCSV() {
        logger.info("getting header for CSV");
        Map<String, String> map = new LinkedHashMap<>();
        map.put("claimedTime", "claimedTime");
        map.put("userId", "userId");
        map.put("studentName", "studentName");
        map.put("rewardName", "rewardName");
        map.put("rewardItem", "rewardItem");
        map.put("address", "address");
        map.put("awb", "awb");
        map.put("status", "status");
        return map;
    }

    private byte[] getClaimedUsersCSVBytes(List<ClaimedUsersResponse> batchUnwindData, Map<String, String> headerMap, Map<Long, UserInfo> longUserInfoMap) {

        List<Map<String, String>> list = new ArrayList<>();
        for (ClaimedUsersResponse response : batchUnwindData) {
            Map<String, String> map = new LinkedHashMap<>();

            try {
                for (Map.Entry<String, String> entry : headerMap.entrySet()) {
                    String key = entry.getKey();
                    if ("claimedTime".equals(key)) {
                        dateFormat.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
                        String val = dateFormat.format(new Date(response.getClaimedTime()));
                        map.put(key, val);
                    } else if ("userId".equals(key)) {
                        String val = response.getUserId().toString();
                        map.put(key, val);
                    } else if ("studentName".equals(key)) {
                        Long userId = response.getUserId();
                        UserInfo userInfo = longUserInfoMap.get(userId);
                        if (userInfo == null) {
                            logger.error("no user info available in db");
                            return new byte[]{};
                        }
                        String fullName = StringUtils.isEmpty(userInfo.getFullName()) ? "" : userInfo.getFullName();
                        map.put(key, fullName);
                    } else if ("rewardName".equals(key)) {
                        String val = response.getRewardName();
                        map.put(key, val);
                    } else if ("rewardItem".equals(key)) {
                        String val = response.getRewardItem();
                        map.put(key, val);
                    } else if ("address".equals(key)) {
                        Long userId = response.getUserId();
                        UserInfo userInfo = longUserInfoMap.get(userId);
                        if (userInfo == null) {
                            logger.error("no user info available in db");
                            return new byte[]{};
                        }
                        String address1 = userInfo.getLocationInfo().getStreetAddress();
                        String address2 = userInfo.getLocationInfo().getStreetAddressLine2();
                        String city = userInfo.getLocationInfo().getCity();
                        String state = userInfo.getLocationInfo().getState();
                        String country = userInfo.getLocationInfo().getCountry();
                        String postalCode = userInfo.getLocationInfo().getPincode();

                        String[] array = {address1, address2, city, state, country, postalCode};
                        Stream<String> stream = Arrays.stream(array);
                        String address = stream.filter(e -> !StringUtils.isEmpty(e)).collect(Collectors.joining(","));
                        map.put(key, address);
                    } else if ("awb".equals(key)) {
                        String val = Objects.isNull(response.getAwb()) ? "" : response.getAwb();
                        map.put(key, val);
                    } else if ("status".equals(key)) {
                        String val = response.getStatus().toString();
                        map.put(key, val);
                    }
                }
                list.add(map);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
            }

        }

        StringBuilder builder = new StringBuilder();
        for (Map<String, String> map : list) {
            builder.append(map.values()
                    .stream()
                    .map(e -> "\"" + e + "\"")
                    .collect(Collectors.joining(",")))
                    .append(System.lineSeparator());
        }

        return builder.toString().getBytes();

    }

    /*
     * returns latest active task from Redis.
     * returns null if redis is down or list is empty
     * */
    private @Nullable
    GameTask getActiveTask(GameJourney journey) {
        logger.info("activeTask is null");
        if (journey == null || journey.getEligibility() == Eligibility.INELIGIBLE) {
            logger.info("no journey available or user is ineligible");
            return null;
        }
        return getCurrentIncompleteTask(journey);
    }

    public GameTask getCurrentIncompleteTask(GameJourney journey) {

        if (journey == null || journey.getEligibility() == Eligibility.INELIGIBLE) {
            logger.info("no journey available or user is ineligible");
        }

        List<GameTask> gameTaskList = getIncompleteTasks(journey);
        if (ArrayUtils.isEmpty(gameTaskList)) {
            return null;
        }
        Optional<GameTask> gameTask = gameTaskList.stream().filter(e -> e.getStatus() == GameTaskStatus.INCOMPLETE).findFirst();

        return gameTask.orElse(null);
    }

    private List<GameSubtask> getIncompleteSubtasks(GameTask gameTask) {
        List<GameSubtask> subtaskList = gameTask.getSubtaskList().stream().filter(e -> e.getStatus() == GameTaskStatus.INCOMPLETE).collect(Collectors.toList());
        logger.info("incomplete subtasks for {} are {}", gameTask.getName(), subtaskList);
        return ArrayUtils.isEmpty(subtaskList) ? new ArrayList<>() : subtaskList;
    }

    private List<GameTask> getIncompleteTasks(GameJourney journey) {

        if (journey == null || journey.getEligibility() == Eligibility.INELIGIBLE) {
            logger.info("no journey available or user is ineligible");
            return new ArrayList<>();
        }

        if (CollectionUtils.isEmpty(journey.getGameTaskList())) {
            logger.error("no game tasks found in journey");
            return new ArrayList<>();
        }

        List<GameTask> gameTaskList = journey.getGameTaskList().stream().filter(e -> e.getStatus() == GameTaskStatus.INCOMPLETE).collect(Collectors.toList());
        return ArrayUtils.isEmpty(gameTaskList) ? new ArrayList<>() : gameTaskList;
    }

    @SuppressWarnings("UnusedReturnValue")
    public PlatformBasicResponse processSessionActivityBatch(List<GameSessionActivity> activities) {
        for (GameSessionActivity activity : activities) {
            try {
                logger.info("Session Activity {}", activity);
                sessionActivity(activity);
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return new PlatformBasicResponse();
    }

    @SuppressWarnings("UnusedReturnValue")
    public PlatformBasicResponse sessionActivity(GameSessionActivity attendeeDetails) throws BadRequestException, InternalServerErrorException {
        Long userId = attendeeDetails.getUserId();

        if (attendeeDetails.getSessionType() == null) {
            OTMSessionType sessionType = getSessionType(attendeeDetails.getSessionId());
            if (sessionType == null) {
                logger.warn("no session type found for session {}", attendeeDetails.getSessionId());
                return new PlatformBasicResponse();
            } else {
                attendeeDetails.setSessionType(sessionType);
            }
        }
        if (attendeeDetails.getSessionType() != OTMSessionType.REGULAR &&
                attendeeDetails.getSessionType() != OTMSessionType.EXTRA_REGULAR) {
            logger.info("session is of type {}, so ignoring", attendeeDetails.getSessionType());
            return new PlatformBasicResponse();
        }

        GameJourney journey = gameJourneyDAO.getJourneyForEligibleUser(userId, gameTemplateDao.getActiveGameTemplateId());
        if (journey == null) {
            return new PlatformBasicResponse();
        }
        GameTask activeTask = getActiveTask(journey);
        if (activeTask == null) {
            return new PlatformBasicResponse();
        }
        logger.info("SESSION ACTIVITY {}", attendeeDetails);
        logger.info("ACTIVE TASK {}", activeTask.getName());
        GameTemplate gameTemplate = gameTemplateDao.getActiveGameTemplate();
        Optional<GameTaskTemplate> taskTemplate = gameTemplate.gameTaskTemplateForTask(activeTask.getName());
        List<GameSubtask> subtasks = getIncompleteSubtasks(activeTask);
        logger.info("SUB TASKS {}", subtasks);


        for (GameSubtask subtask : subtasks) {
            if (((subtask.getName() == GameSubtaskName.DOWNLOAD) && attendeeDetails.getTaskType() != GameSessionActivity.TaskType.DOWNLOAD) ||
                    ((subtask.getName() != GameSubtaskName.DOWNLOAD) && attendeeDetails.getTaskType() == GameSessionActivity.TaskType.DOWNLOAD)) {
                continue;
            }
            Optional<GameSubtaskTemplate> subtaskTemplate = taskTemplate
                    .orElseThrow(() -> new VRuntimeException(ErrorCode.SERVICE_ERROR, "TASK TEMPLATE NOT FOUND"))
                    .gameSubtaskTemplateForName(subtask.getName());

            boolean sessionSubtaskCompleted = isSessionSubtaskCompleted(subtask.getName(), attendeeDetails,
                    subtaskTemplate.orElseThrow(() -> new VRuntimeException(ErrorCode.SERVICE_ERROR, "SUBTASK TEMPLATE NOT FOUND")),
                    journey
            );
            logger.info("SESSION SUBTASK COMPLETED {} {}", subtask, sessionSubtaskCompleted);
            if (sessionSubtaskCompleted) {
                GameMetaData metaData = new GameMetaData();
                metaData.setContextId(attendeeDetails.getSessionId());
                metaData.setEntity(GameEntity.GTT_SESSION);
                if (GameSubtaskName.DOWNLOAD == subtask.getName()) {
                    logger.info("SETTING START TIME FOR {}", subtask.getName());
                    metaData.setStartTime(attendeeDetails.getSessionNotesDownloadTime());
                } else if (GameSubtaskName.REPLAY == subtask.getName()) {
                    logger.info("SETTING START TIME FOR {}", subtask.getName());
                    metaData.setStartTime(attendeeDetails.getLastReplayWatchedTime());
                } else {
                    logger.info("SETTING SESSION START TIME FOR {}", subtask.getName());
                    metaData.setStartTime(attendeeDetails.getSessionStartTime());
                }

                saveGameJourney(journey, activeTask.getName(), metaData, subtask.getName());
            }
        }
        return new PlatformBasicResponse();
    }

    private OTMSessionType getSessionType(String sessionId) {
        String url = ConfigUtils.INSTANCE.getSchedulingEndpoint() + "/onetofew/session/cached/sessiontype/" + sessionId;
        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        if (response.getStatus() == HttpStatus.OK.value() || response.getStatus() == HttpStatus.CREATED.value()) {
            String entity = response.getEntity(String.class);
            OTFSessionPojoUtils sessionInfo = GSON.fromJson(entity, OTFSessionPojoUtils.class);
            if (sessionInfo != null) {
                return sessionInfo.getOtmSessionType();
            }
        }
        return null;
    }

    private void saveGameJourney(GameJourney journey, String activeTask, GameMetaData metaData,
                                 GameSubtaskName subTaskName) throws BadRequestException, InternalServerErrorException {
        logger.info("entered saveGameJourney");

        try {
            if (checkIfActivityIsUsedAlready(metaData, subTaskName, journey)) {
                logger.info("ACTIVITY ALREADY USED");
                return;
            }

            long lastTaskTime = journey.getLastTaskCompleted();

            long lastUsedEntityCreationTime = journey.getMaxActivityCreation(metaData.getEntity(),
                    new HashSet<>(Arrays.asList(GameSubtaskName.SINGLE_CLASS, GameSubtaskName.TWO_CONSECUTIVE_CLASS)));

            logger.info("lastTaskCompleted: {}", lastTaskTime);
            logger.info("lastUsedEntityCreationTime {}", lastUsedEntityCreationTime);
            Long metaDataStartTime = metaData.getStartTime();
            logger.info("Activity Creation {}", metaDataStartTime);
            if ((subTaskName == GameSubtaskName.TWO_CONSECUTIVE_CLASS ||
                    subTaskName == GameSubtaskName.SINGLE_CLASS) &&
                    metaDataStartTime < lastUsedEntityCreationTime
            ) {
                logger.info("CANNOT USE PREVIOUS SESSIONS");
                return;
            }

            if (metaDataStartTime >= lastTaskTime) {
                GameTask task = journey.getGameTaskForName(activeTask);
                if (task == null) {
                    return; // already completed
                }
                GameSubtask gameSubTask = task.getGameSubTaskForName(subTaskName);
                if (gameSubTask == null) {
                    return; // already completed
                }

                gameSubTask.setCompletedTime(System.currentTimeMillis());
                gameSubTask.setStatus(GameTaskStatus.COMPLETE);
                if (!ArrayUtils.isEmpty(gameSubTask.getMeta())) {
                    gameSubTask.getMeta().add(metaData);
                } else {
                    List<GameMetaData> metaDataList = new ArrayList<>();
                    metaDataList.add(metaData);
                    gameSubTask.setMeta(metaDataList);
                }

                List<GameTaskHistory> histories = new ArrayList<>();
                if (isAllSubTaskComplete(task)) {
                    logger.info("All Subtasks completed for task {} for user {}", activeTask, journey.getUserId());
                    GameTaskHistory history = new GameTaskHistory();
                    history.setCurrStatus(GameTaskStatus.COMPLETE);
                    history.setPrevStatus(task.getStatus());
                    histories.add(history);

                    task.setCompletedTime(System.currentTimeMillis());
                    task.setStatus(GameTaskStatus.COMPLETE);

                    GameTask nextActiveTask = journey.getNextActiveTask(activeTask);
                    if (nextActiveTask.getType() == GameTaskType.REWARD) {
                        RewardStatusChangeRequest request = new RewardStatusChangeRequest();
                        request.setRewardName(nextActiveTask.getName());
                        request.setUserId(journey.getUserId());
                        request.setCurrentStatus(nextActiveTask.getStatus());
                        request.setSecureString(ConfigUtils.INSTANCE.getStringValue("game.gift.secure.string"));
                        updateRewardStatus(request, journey);
                    }
                }
                task.setGameTaskHistoryList(histories);
                logger.info("new journey data: {}", journey);
                logger.info("Saving Journey For User {}", journey.getUserId());
                gameJourneyDAO.save(journey);
                setRedisValueForJourney(journey.getUserId(), journey);
            } else {
                logger.info("activity occured before the last completed task time");

            }
        } catch (Exception e) {
            redisDAO.del(getGameJourneyRedisKey(journey.getUserId()));
            logger.info("exception in saveGameJourney");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }


    private boolean checkIfActivityIsUsedAlready(GameMetaData metaData, GameSubtaskName subTaskName, GameJourney journey) {
        try {
            if (subTaskName == GameSubtaskName.SINGLE_CLASS || subTaskName == GameSubtaskName.TWO_CONSECUTIVE_CLASS || subTaskName == GameSubtaskName.TEST
                    || subTaskName == GameSubtaskName.DOWNLOAD) {

                List<GameMetaData> metaDataListDB = new ArrayList<>();
                List<GameTask> gameTaskList = journey.getGameTaskList().stream().filter(e -> e.getType() == GameTaskType.TASK).collect(Collectors.toList());
                for (GameTask gameTask : gameTaskList) {
                    List<GameSubtask> subtaskList = gameTask.getSubtaskList();
                    for (GameSubtask gameSubtask : subtaskList) {
                        if (!Objects.equals(gameSubtask.getName(), subTaskName)) {
                            continue;
                        }
                        if (!ArrayUtils.isEmpty(gameSubtask.getMeta())) {
                            metaDataListDB.addAll(gameSubtask.getMeta());
                        }

                    }
                }

                logger.info("number of metadata in db: {}", metaDataListDB.size());
                logger.info("printing metadata from db: {}", metaDataListDB);
                boolean isPresent = metaDataListDB.stream().anyMatch(e -> e.getEntity() == metaData.getEntity() && Objects.equals(e.getContextId(), metaData.getContextId()));
                if (isPresent) {
                    logger.info("metadata already present in db and hence the subtask won't be counted");
                    return true;
                }
            }
        } catch (Exception e) {
            logger.info("exception occured in checkIfActivityIsUsedAlready");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
        return false;
    }

    private boolean isSessionSubtaskCompleted(GameSubtaskName subtaskName, GameSessionActivity activity,
                                              GameSubtaskTemplate subtaskTemplate, GameJourney journey) {
        switch (subtaskName) {
            case SINGLE_CLASS: {
                try {
                    return checkSessionAttendance(activity, subtaskTemplate);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
                }
            }
            case TWO_CONSECUTIVE_CLASS: {
                try {
                    if (checkSessionAttendance(activity, subtaskTemplate)) {
                        String url = ConfigUtils.INSTANCE.getSchedulingEndpoint() + "/gttattendee/previous/session?enrollmentId=" +
                                activity.getEnrollmentId() + "&userId=" + activity.getUserId() + "&sessionStartTime=" + activity.getSessionStartTime();
                        ClientResponse response = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
                        if (response.getStatus() == HttpStatus.OK.value()) {
                            String entity = response.getEntity(String.class);
                            GameSessionActivity prevInfo = GSON.fromJson(entity, GameSessionActivity.class);
                            logger.info("FOUND PREVIOUS SESSION {}", prevInfo);
                            if (journey != null) {
                                boolean activityUsed = journey.getGameTaskList().stream().filter(Objects::nonNull)
                                        .map(GameTask::getSubtaskList).filter(Objects::nonNull)
                                        .flatMap(List::stream).filter(Objects::nonNull)
                                        .map(GameSubtask::getMeta).filter(Objects::nonNull)
                                        .flatMap(List::stream).filter(Objects::nonNull)
                                        .anyMatch(e -> e.getEntity() == GameEntity.GTT_SESSION &&
                                                e.getContextId().equals(prevInfo.getSessionId()));
                                logger.info("ACTIVITY USED ALREADY {}", activityUsed);
                                boolean attendance = checkSessionAttendance(prevInfo, subtaskTemplate);
                                logger.info("SESSION ATTENDENCE {}", attendance);
                                return attendance && !activityUsed;
                            } else {
                                return false;
                            }
                        } else {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
                }
            }
            case DOWNLOAD: {
                return true;
            }
            case REPLAY: {
                try {
                    long duration = activity.getLastReplayWatchedDuration();
                    long total = activity.getSessionEndTime() - activity.getSessionStartTime();
                    float percent = ((float) duration) / ((float) total);
                    logger.info("TIME IN REPLAY {}", duration);
                    logger.info("TOTAL TIME FOR REPLAY {}", total);
                    logger.info("PERCENT FOR REPLAY {}", percent);
                    return percent > (subtaskTemplate.getCriteria().getCompletionPercent() / 100);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                    throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
                }
            }
            default:
                return false;
        }
    }

    private boolean checkSessionAttendance(GameSessionActivity info, GameSubtaskTemplate subtaskTemplate) {
        double completionPercent = subtaskTemplate.getCriteria().getCompletionPercent();
        long timeInSession = info.getTimeInSession() == null ? 0 : info.getTimeInSession();
        if (timeInSession > 0) {
            if (info.getSessionStartTime() == null || info.getSessionEndTime() == null) {
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "session start time or end time is null");
            }
            if (info.getSessionStartTime() == 0 || info.getSessionEndTime() == 0) {
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "session start time or end time is 0");
            }
            if (info.getSessionStartTime() > info.getSessionEndTime()) {
                throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "session start time greater than end time");
            }
        } else if (completionPercent == 0) {
            return info.isAttended();
        }
        long total = info.getSessionEndTime() - info.getSessionStartTime();
        float percent = ((float) timeInSession) / ((float) total);
        logger.info("TIME IN SESSION {}", timeInSession);
        logger.info("TOTAL TIME FOR SESSION {}", total);
        logger.info("PERCENT FOR SESSION {}", percent);
        return percent > (completionPercent / 100);
    }

    public void checkValidTest(LMSTestInfo lmsTestInfo) {

        logger.info("checking for if Test is valid for game");
        try {
            logger.info("test Attempt Id: {}", lmsTestInfo.getTestAttemptId());
            logger.info("attemptDuration: {}", lmsTestInfo.getAttemptDuration());
            logger.info("testDuration: {}", lmsTestInfo.getTestDuration());

            double durationPerc = lmsTestInfo.getAttemptDuration() * 100.0D / lmsTestInfo.getTestDuration();
            logger.info("duration perc: {}", durationPerc);
            GameJourney journey = gameJourneyDAO.getJourneyForEligibleUser(lmsTestInfo.getUserId(), gameTemplateDao.getActiveGameTemplateId());
            if (journey == null) {
                return;
            }
            GameTask activeTask = getActiveTask(journey);
            if (activeTask == null) {
                return;
            }
            List<GameSubtask> incompleteSubtasks = getIncompleteSubtasks(activeTask);
            Optional<GameSubtask> subtask = incompleteSubtasks.stream().filter(e -> e.getName() == GameSubtaskName.TEST).findAny();
            if (!subtask.isPresent()) {
                return;
            }


            Optional<GameTaskTemplate> gameTaskTemplate = gameTemplateDao.getActiveGameTemplate().getTaskTemplates().stream().filter(e -> e.getName().equals(activeTask.getName())).findAny();
            if (gameTaskTemplate.isPresent()) {
                Optional<GameSubtaskTemplate> gameSubtaskTemplate = gameTaskTemplate.get().getSubtaskTemplates().stream().filter(e -> e.getName().equals(GameSubtaskName.TEST)).findAny();
                if (gameSubtaskTemplate.isPresent()) {
                    double requiredPerc = gameSubtaskTemplate.get().getCriteria().getCompletionPercent();
                    logger.info("required_perc: {}", requiredPerc);
                    if (durationPerc >= requiredPerc) {
                        logger.info("test attempt percent requirement met");
                        GameMetaData metaData = new GameMetaData();
                        metaData.setEntity(GameEntity.TEST);
                        metaData.setStartTime(lmsTestInfo.getTestStartTime());
                        metaData.setContextId(lmsTestInfo.getTestAttemptId());
                        saveGameJourney(journey, activeTask.getName(), metaData, GameSubtaskName.TEST);
                    } else {
                        logger.info("test attempt percent requirement not met");
                    }
                }
            }
        } catch (Exception e) {
            logger.info("exception occured in checkValidTest");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }
    }

    public RewardStatusChangeResponse updateRewardStatus(RewardStatusChangeRequest request) throws BadRequestException, InternalServerErrorException {

        GameJourney journey = gameJourneyDAO.getJourneyForEligibleUser(request.getUserId(), gameTemplateDao.getActiveGameTemplateId());
        if (journey == null || journey.getEligibility() == Eligibility.INELIGIBLE) {
            logger.info("no journey available or user is ineligible for the game");
            return null;
        }

        return updateRewardStatus(request, journey);
    }

    /**
     * @param request -
     * @param journey Shared state journey object
     * @return RewardStatusChangeResponse
     */
    public RewardStatusChangeResponse updateRewardStatus(RewardStatusChangeRequest request, GameJourney journey) {

        RewardStatusChangeResponse response = new RewardStatusChangeResponse();
        response.setRewardName(request.getRewardName());

        try {
            logger.info("reward from request body: {}", request.getRewardName());
            Optional<GameTask> optgameTask = journey.getGameTaskList().stream().filter(e -> Objects.equals(e.getName(), request.getRewardName())).findAny();
            if (!optgameTask.isPresent()) {
                logger.info("the rewardname doesn't exist in db");
                response.setPrevStatus(null);
                response.setCurrStatus(null);
                return response;
            }
            GameTask gameTask = optgameTask.get();
            logger.info("game task: {}", gameTask.getName());
            GameTaskHistory history = new GameTaskHistory();
            GameTaskStatus prevStatus = gameTask.getStatus();

            if (!Objects.equals(prevStatus, request.getCurrentStatus())) {
                logger.info("current status db: {}", prevStatus);
                logger.info("current status from API request: {}", request.getCurrentStatus());
                throw new VRuntimeException(ErrorCode.REWARD_STATUS_ERROR, "current reward status in db is not same as requested");
            }

            history.setPrevStatus(prevStatus);
            response.setPrevStatus(prevStatus);
            GameTaskStatus currStatus = prevStatus;
            logger.info("previous status: {}", prevStatus);
            switch (prevStatus) {
                case DISABLED:
                    if (Objects.equals(request.getSecureString(), ConfigUtils.INSTANCE.getStringValue("game.gift.secure.string"))) {
                        currStatus = GameTaskStatus.LOCKED;
                    }
                    break;
                case LOCKED:
                    httpSessionUtils.checkIfAllowed(request.getUserId(), Role.STUDENT, false);
                    currStatus = GameTaskStatus.UNLOCKED;
                    String item = allocateGift(request.getRewardName());
                    logger.info("{} receives the item: {}", request.getUserId(), item);
                    GameRewardDetails rewardDetails = new GameRewardDetails();
                    rewardDetails.setGameRewardItem(item);
                    gameTask.setGameRewardDetails(rewardDetails);
                    response.setItem(item);
                    break;
                case UNLOCKED:
                    currStatus = GameTaskStatus.CLAIMED;
                    response.setItem(gameTask.getGameRewardDetails().getGameRewardItem());
                    gameTask.setCompletedTime(System.currentTimeMillis());
                    break;
                case CLAIMED:
                    httpSessionUtils.checkIfAllowed(request.getUserId(), Role.ADMIN, true);
                    gameTask.getGameRewardDetails().setAwb(request.getAwb());
                    history.setAwb(request.getAwb());
                    currStatus = GameTaskStatus.SHIPPED;
                    break;
                case SHIPPED:
                    httpSessionUtils.checkIfAllowed(request.getUserId(), Role.ADMIN, true);
                    if (Objects.equals(request.getAwb(), gameTask.getGameRewardDetails().getAwb())) {
                        logger.info("same awb number {} as of db, hence returning", request.getAwb());
                        return response;
                    }
                    gameTask.getGameRewardDetails().setAwb(request.getAwb());
                    history.setAwb(request.getAwb());
                    currStatus = GameTaskStatus.SHIPPED;
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + prevStatus);
            }

            response.setCurrStatus(currStatus);
            gameTask.setStatus(currStatus);
            history.setCurrStatus(currStatus);
            history.setUpdatedTime(System.currentTimeMillis());
            if (!ArrayUtils.isEmpty(gameTask.getGameTaskHistoryList())) {
                gameTask.getGameTaskHistoryList().add(history);
            } else {
                List<GameTaskHistory> gameTaskHistoryList = new ArrayList<>();
                gameTaskHistoryList.add(history);
                gameTask.setGameTaskHistoryList(gameTaskHistoryList);
            }
            logger.info("reward history: {}", gameTask.getGameTaskHistoryList());
            gameJourneyDAO.save(journey);

        } catch (VRuntimeException e1) {
            throw e1;
        } catch (Exception e) {
            logger.info("exception occured in updateRewardStatus");
            logger.error(e.getMessage(), e);
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, e.getMessage());
        }

        return response;
    }

    private String allocateGift(String rewardName) {
        try {
            GameTemplate template = gameTemplateDao.getActiveGameTemplate();
            final Map<String, Map<String, Integer>> rewardRatio = Collections.unmodifiableMap(template.getRewardRatio());
            final Map<String, Integer> ratioMap = Collections.unmodifiableMap(rewardRatio.get(rewardName));


            String rewardValKey = REDIS_KEY_REWARD_RATIO_VAL + "_" + rewardName;
            Map<String, String> ratioValues = redisDAO.getAllHashFieldValues(rewardValKey);
            ratioValues = ratioValues == null ? new HashMap<>() : ratioValues;
            String itemName = null;

            double totalRatio = ratioMap.values().stream().filter(Objects::nonNull).mapToDouble(i -> i).sum();
            double totalItemAllocationCount = ratioValues.values().stream().filter(Objects::nonNull).mapToDouble(Integer::parseInt).sum();
            for (Map.Entry<String, Integer> entry : ratioMap.entrySet()) {
                itemName = entry.getKey();
                Integer value = entry.getValue();
                String count = ratioValues.get(itemName);
                if (StringUtils.isNotEmpty(count)) {
                    double ratioPercent = value.doubleValue() / totalRatio;
                    double allocationPercent = Double.parseDouble(count) / totalItemAllocationCount;
                    if (ratioPercent >= allocationPercent) {
                        ratioValues.put(itemName, String.valueOf((Integer.parseInt(count) + 1)));
                        break;
                    }
                } else {
                    ratioValues.put(itemName, "1");
                    break;
                }
            }


            redisDAO.hmset(rewardValKey, ratioValues);
            gameRewardItemManager.incrementCountForGame(itemName, template.getId());
            return itemName;
        } catch (Exception e) {
            logger.error("exception in allocateGift");
            logger.error(e.getMessage(), e);
        }
        throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "CAN'T ALLOCATE GIFT " + rewardName);
    }

    private static Long incrementValueForAb() {
        logger.info("getting local value for Journey AB");
        return abCounter++;
    }

    public PlatformBasicResponse createGameTemplate(GameTemplate template) throws InternalServerErrorException {
        gameTemplateDao.save(template);
        return new PlatformBasicResponse();
    }

    public boolean isAllSubTaskComplete(GameTask gameTask) {
        return gameTask.getSubtaskList().stream().allMatch(e -> e.getStatus() == GameTaskStatus.COMPLETE);
    }

    public List<RewardStatusChangeResponse> bulkUpdateAwb(List<RewardStatusChangeRequest> rewardStatusChangeRequestList) throws BadRequestException, InternalServerErrorException {

        if (CollectionUtils.isEmpty(rewardStatusChangeRequestList)) {
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "list is null or empty");
        }

        List<RewardStatusChangeResponse> list = new ArrayList<>();
        for (RewardStatusChangeRequest request : rewardStatusChangeRequestList) {
            if (request.getUserId() == null) {
                logger.info("no userId provided for bulkUpdateAwb");
                continue;
            }
            if (StringUtils.isEmpty(request.getCurrentStatus().toString()) || StringUtils.isEmpty(GameTaskStatus.valueOf(request.getCurrentStatus().toString()).toString())) {
                logger.info("no current status provided for userId: {}", request.getUserId());
                continue;
            }
            if (StringUtils.isEmpty(request.getAwb())) {
                logger.info("no awb provided for userId: {}", request.getUserId());
                continue;
            }
            if (StringUtils.isEmpty(request.getRewardName())) {
                logger.info("no reward name provided for userId: {}", request.getUserId());
                continue;
            }
            GameJourney journey = gameJourneyDAO.getJourneyForEligibleUser(request.getUserId(), gameTemplateDao.getActiveGameTemplateId());
            if (journey == null || journey.getEligibility() == Eligibility.INELIGIBLE) {
                logger.info("no journey available or user is ineligible");
            }
            RewardStatusChangeResponse response = updateRewardStatus(request, journey);
            response.setUserId(response.getUserId());
            list.add(response);
        }
        return list;
    }

    public String getGameJourneyRedisKey(Long userId) {
        return GameManager.REDIS_KEY_USER_JOURNEY_PREFIX + userId;
    }

    private GameJourneyRedis getGameJourneyRedisObject(GameJourney savedJourney) {
        if (savedJourney == null) {
            return null;
        }
        GameJourneyRedis journeyCache = new GameJourneyRedis();
        if (savedJourney.getEligibility() == Eligibility.ELIGIBLE) {
            List<GameTask> gameTasks = savedJourney.getGameTaskList().stream()
                    .filter(e -> e.getType() == GameTaskType.TASK)
                    .collect(Collectors.toList());
            List<GameJourneyRedis.Task> tasks = new ArrayList<>();
            Map<String, String> metaMap = new HashMap<>();
            for (GameTask gameTask : gameTasks) {
                if (gameTask.getStatus() == GameTaskStatus.INCOMPLETE) {
                    GameJourneyRedis.Task task = new GameJourneyRedis.Task();
                    task.setName(gameTask.getName());
                    List<GameSubtask> incompleteSubTasks = gameTask.getSubtaskList().stream()
                            .filter(e -> e.getStatus() == GameTaskStatus.INCOMPLETE)
                            .collect(Collectors.toList());

                    List<GameJourneyRedis.SubTask> subTasks = new ArrayList<>();
                    for (GameSubtask incompletedSubTask : incompleteSubTasks) {
                        GameJourneyRedis.SubTask subTask = new GameJourneyRedis.SubTask();
                        subTask.setName(incompletedSubTask.getName());
                        subTasks.add(subTask);
                    }
                    task.setSubTasks(subTasks);
                    tasks.add(task);
                }
                for (GameSubtask subtask : gameTask.getSubtaskList()) {
                    if (subtask.getStatus() == GameTaskStatus.COMPLETE && (
                            subtask.getName() == GameSubtaskName.SINGLE_CLASS ||
                                    subtask.getName() == GameSubtaskName.TWO_CONSECUTIVE_CLASS
                    )) {
                        for (GameMetaData gameMetaData : subtask.getMeta()) {
                            metaMap.put(gameMetaData.getContextId(), gameTask.getName());
                        }
                    }
                }
            }
            journeyCache.setIncompleteTasks(tasks);
            journeyCache.setSessionMeta(metaMap);
        }
        return journeyCache;
    }

    private void setRedisValueForJourney(Long userId, GameJourney journey) throws InternalServerErrorException {
        GameJourneyRedis gameJourneyRedisObject = getGameJourneyRedisObject(journey);
        if (gameJourneyRedisObject != null) {
            redisDAO.setex(getGameJourneyRedisKey(userId), GSON.toJson(gameJourneyRedisObject), DateTimeUtils.SECONDS_PER_DAY * 15);
        }
    }

    private void setRedisValueForJourney(Long userId, GameJourneyRedis journey) throws InternalServerErrorException {
        if (journey != null) {
            redisDAO.setex(getGameJourneyRedisKey(userId), GSON.toJson(journey), DateTimeUtils.SECONDS_PER_DAY * 15);
        }
    }

    public GameSessionStatusResponse getSessionStatus(Long userId, String sessionId, Long endTime) throws BadRequestException, InternalServerErrorException {
        OTMSessionType sessionType = getSessionType(sessionId);
        if (sessionType == OTMSessionType.REGULAR || sessionType == OTMSessionType.EXTRA_REGULAR) {
            String json = redisDAO.get(getGameJourneyRedisKey(userId));
            GameJourneyRedis journeyCache = null;
            try {
                if (StringUtils.isNotEmpty(json)) {
                    journeyCache = GSON.fromJson(json, GameJourneyRedis.class);
                } else {
                    String activeGameTemplateId = gameTemplateDao.getActiveGameTemplateId();
                    GameJourney journey = gameJourneyDAO.getJourneyForEligibleUser(userId, activeGameTemplateId);
                    if (journey != null && activeGameTemplateId.equalsIgnoreCase(journey.getGameId())) {
                        journeyCache = getGameJourneyRedisObject(journey);
                        if (journeyCache != null) {
                            setRedisValueForJourney(userId, journeyCache);
                        }
                    }
                }

                if (journeyCache != null) {

                    Map<String, String> sessionMeta = journeyCache.getSessionMeta();
                    if (sessionMeta != null  && sessionMeta.containsKey(sessionId)) {
                        String taskName = sessionMeta.get(sessionId);
                        String uiText = getInClassUiText(taskName);
                        return GameSessionStatusResponse.builder()
                                .hasSessionTask(true)
                                .uiText(uiText).build();
                    }

                    List<GameJourneyRedis.Task> incompleteTasks = journeyCache.getIncompleteTasks();
                    if (ArrayUtils.isNotEmpty(incompleteTasks)) {
                        GameJourneyRedis.Task incompleteTask = incompleteTasks.get(0);
                        if (incompleteTask != null) {
                            List<GameJourneyRedis.SubTask> incompleteSubtasks = incompleteTask.getSubTasks();

                            Optional<GameJourneyRedis.SubTask> sessionTasks = Optional.ofNullable(incompleteSubtasks)
                                    .orElseGet(ArrayList::new).stream()
                                    .filter(e -> e.getName() == GameSubtaskName.SINGLE_CLASS ||
                                            GameSubtaskName.TWO_CONSECUTIVE_CLASS == e.getName()).findAny();
                            if (sessionTasks.isPresent()) {
                                String uiText = getInClassUiText(incompleteTask.getName());
                                return GameSessionStatusResponse.builder().hasSessionTask(true).uiText(uiText).build();
                            }
                        }
                    }
                }
            } catch (Exception e) {
                logger.error(e.getMessage(), e);
            }
        }
        return GameSessionStatusResponse.builder().hasSessionTask(false).build();
    }

    private String getInClassUiText(String incompleteTask) {
        switch (incompleteTask) {
            case "TASK1":
                return "Welcome to your first class!";
            case "TASK2":
                return "Welcome to your second class!";
            default:
                throw new VRuntimeException(ErrorCode.ILLEGAL_STATE, "Unknown task name - " + incompleteTask);
        }
    }
}
