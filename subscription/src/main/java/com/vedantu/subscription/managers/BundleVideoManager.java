package com.vedantu.subscription.managers;

import com.vedantu.cmds.pojo.VideoContent;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.dao.BundleTestDAO;
import com.vedantu.subscription.dao.BundleVideoDAO;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleVideo;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.viewobject.request.GetBundleTestVideoReq;
import com.vedantu.subscription.viewobject.response.BundleVideoFilterRes;
import com.vedantu.subscription.viewobject.response.BundleVideoRes;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.data.mongodb.core.MongoOperations;

import java.util.ArrayList;
import java.util.List;

@Service
public class BundleVideoManager {


    @Autowired
    private BundleVideoDAO bundleVideoDAO;


    @Autowired
    private BundleTestDAO bundleDAO;

    @Autowired
    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(BundleVideoManager.class);

    public void setBundleVideo(BundleVideo bundleVideo) throws NotFoundException, VException {
        bundleVideoDAO.save(bundleVideo);

    }

    public List<BundleVideo> getBundleVideo(GetBundleTestVideoReq bundleVideo) throws NotFoundException, VException {


        return bundleVideoDAO.getVideo(bundleVideo);

    }



    public BundleVideoRes getVideoByBundle(GetBundleTestVideoReq bundleVideo) throws NotFoundException, VException {

        return bundleVideoDAO.getVideoByBundle(bundleVideo);

    }

    public VideoContentData getVideoByBundles(GetBundleTestVideoReq bundleVideo) throws NotFoundException, VException {

            return bundleVideoDAO.getVideoByBundles(bundleVideo);

    }

    public List<BundleVideoFilterRes> getVideoDetailsByBundles(GetBundleTestVideoReq bundleVideo){
        return bundleVideoDAO.getVideoDetailsByBundles(bundleVideo);
    }

}
