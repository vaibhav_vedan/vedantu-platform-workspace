/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.EnrollmentConsumptionDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.EnrollmentTransactionDAO;
import com.vedantu.subscription.dao.OTFBundleDAO;
import com.vedantu.subscription.dao.RegistrationDAO;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class InstalmentUtilManager {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private FosUtils fosUtils;

    private final String dineroEndpoint = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(InstalmentUtilManager.class);

    private static Gson gson = new Gson();

    public List<InstalmentInfo> getInstalmentsForDeliverableIds(Long userId, Set<String> deliverableIds) throws VException {
        if (userId == null || ArrayUtils.isEmpty(deliverableIds)) {
            return null;
        }
        String queryString = "";
        if (ArrayUtils.isNotEmpty(deliverableIds)) {
            for (String id : deliverableIds) {
                queryString += ("&deliverableIds=" + id);
            }
        }
        String url = dineroEndpoint + "/payment/getInstalments?userId=" + userId;
        if (!StringUtils.isEmpty(queryString)) {
            url += queryString;
        }
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<InstalmentInfo>>() {
        }.getType();
        List<InstalmentInfo> slist = gson.fromJson(jsonString, listType);
        logger.info("instalments " + jsonString);
        return slist;
    }

}
