package com.vedantu.subscription.managers;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.exception.*;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.enums.NotificationStatus;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.notification.requests.ManualNotificationRequest;
import com.vedantu.notification.responses.NoticeBoardRes;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BundleDAO;
import com.vedantu.subscription.dao.BundleEnrolmentDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.OTFBundleDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.OTMBundle;
import com.vedantu.util.*;
import com.vedantu.util.enums.CommunicationKind;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import com.vedantu.util.request.CommunicationDataReq;
import com.vedantu.util.response.CommunicationDataRes;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.exception.ConstraintViolationException;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.notification.requests.TextSMSRequest;
import com.vedantu.scheduling.pojo.session.SessionInfo;
import com.vedantu.scheduling.request.proposal.CancelSubscriptionProposalsReq;
import com.vedantu.scheduling.request.session.EndSessionReq;
import com.vedantu.scheduling.request.session.MultipleSessionScheduleReq;
import com.vedantu.scheduling.request.session.SubscriptioncoursePlanCancelSessionsReq;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.session.pojo.SessionState;
import com.vedantu.session.pojo.SessionPayoutType;
import com.vedantu.subscription.dao.LoanAgreementDAO;
import com.vedantu.subscription.dao.SubscriptionDAO;
import com.vedantu.subscription.entities.mongo.LoanAgreement;
import com.vedantu.subscription.entities.sql.HourTransaction;
import com.vedantu.subscription.entities.sql.Refund;
import com.vedantu.subscription.entities.sql.Subscription;
import com.vedantu.subscription.entities.sql.SubscriptionDetails;
import com.vedantu.subscription.entities.sql.SubscriptionRequest;
import com.vedantu.subscription.enums.HourTransactionType;
import com.vedantu.subscription.enums.HourType;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.subscription.request.AcceptLoanAgreementReq;
import com.vedantu.subscription.request.CancelSubscriptionRequest;
import com.vedantu.subscription.request.CancelSubscriptionSessionRequest;
import com.vedantu.subscription.request.GetSubscriptionsReq;
import com.vedantu.subscription.request.RenewSubscriptionForInstalmentReq;
import com.vedantu.subscription.request.SessionRequest;
import com.vedantu.subscription.request.SubscriptionVO;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.SessionResponse;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.subscription.viewobject.request.CompleteSessionRequest;
import com.vedantu.subscription.viewobject.request.GetCancelSubscriptionReq;
import com.vedantu.subscription.viewobject.request.GetSessionInfoRequest;
import com.vedantu.subscription.viewobject.request.UnlockHoursRequest;
import com.vedantu.subscription.viewobject.request.UpdateConflictDurationRequest;
import com.vedantu.subscription.viewobject.request.UpdateSubscriptionHoursRequest;
import com.vedantu.subscription.viewobject.response.GetHourlyRateResponse;
import com.vedantu.subscription.viewobject.response.GetSubscriptionsRes;
import com.vedantu.subscription.viewobject.response.GetSubscriptionsResponse;
import com.vedantu.subscription.viewobject.response.GetUnlockHoursResponse;
import com.vedantu.subscription.viewobject.response.SessionInfoResponse;
import com.vedantu.subscription.viewobject.response.UnlockHoursResponse;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.subscription.SubscriptionState;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

@Service
public class SubscriptionManager {

	@Autowired
	private SubscriptionDAO subscriptionDAO;
        
        @Autowired
        private LoanAgreementDAO loanAgreementDAO;

	@Autowired
	private SqlSessionFactory sqlSessionfactory;

	@Autowired
	private SubscriptionDetailsManager subscriptionDetailsManager;

	@Autowired
	private RefundManager refundManager;

	@Autowired
	private HourTransactionManager hourTransactionManager;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private FosUtils fosUtils;

	@Autowired
	private EnrollmentDAO enrollmentDAO;

	@Autowired
	private BatchDAO batchDAO;

	@Autowired
	private OTFBundleDAO otfBundleDAO;

	@Autowired
	private BundleDAO bundleDAO;

	@Autowired
	private BundleEnrolmentDAO bundleEnrolmentDAO;

	@Autowired
	private DozerBeanMapper mapper;

	@Autowired
	private AsyncTaskFactory asyncTaskFactory;

	@Autowired
	private AwsSQSManager awsSQSManager;

	private static Gson gson = new Gson();;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionManager.class);

	private String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");

	private String addRcbEndpoint = ConfigUtils.INSTANCE.getStringValue("rcb.add.url");

	private String schedulingEndPoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

	private final String NOTIFICATION_ENDPOINT=ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
	private final String USER_ENDPOINT=ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

	public SubscriptionResponse createSubscription(SubscriptionVO subscriptionVO) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		validateSubscription(subscriptionVO);
		Subscription subscription = null;
		SubscriptionDetails subscriptionDetails = null;
		Long subscriptionId = 0l;
		try {
			transaction.begin();

			Long boardId = subscriptionVO.getBoardId();
			String subject = subscriptionVO.getSubject();
			String title = subscriptionVO.getGrade() + "th " + subscriptionVO.getTarget() + " " + subject;
			subscription = new Subscription(subscriptionVO.getTeacherId(), subscriptionVO.getStudentId(),
					subscriptionVO.getPlanId(), subscriptionVO.getModel(), subscriptionVO.getScheduleType(),
					subscriptionVO.getOfferingId(), subscriptionVO.getTotalHours(), 0l, 0l,
					subscriptionVO.getTotalHours(), subject, subscriptionVO.getTarget(), subscriptionVO.getGrade(),
					subscriptionVO.getTeacherHourlyRate(), title, 0l, boardId, 0l, null, null, null,
					subscriptionVO.getNote(), subscriptionVO.getSubModel(), SubscriptionState.ACTIVE,
					subscriptionVO.getSessionSource());
			// subscription.setCallingUserId(subscriptionVO.getCallingUserId());

			logger.debug("Subscription DB entry Started: ");
			subscription = subscriptionDAO.addOrUpdateSubscription(subscription, session,
					subscriptionVO.getCallingUserId());
			logger.debug("Subscription DB entry Ended: ");

			subscriptionDetails = new SubscriptionDetails(subscription.getId(), subscriptionVO.getTotalHours(), 0l, 0l,
					subscriptionVO.getTotalHours(), subscriptionVO.getTeacherCouponId(),
					subscriptionVO.getTeacherDiscountAmount(), subscriptionVO.getTeacherHourlyRate());
			// subscriptionDetails.setCallingUserId(subscriptionVO.getCallingUserId());

			logger.debug("SubscriptionDetails DB entry Started: ");
			subscriptionDetails = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetails, null,
					session);
			logger.debug("SubscriptionDetails DB entry Ended: ");

			HourTransaction hourTransaction1 = new HourTransaction(subscription.getId(), subscriptionDetails.getId(),
					HourType.DEFAULT, HourType.REMAINING, subscriptionDetails.getRemainingHours(),
					HourTransactionType.SUBSCRIPTION_BOOKED, subscriptionDetails.getId().toString());
			hourTransaction1.setCallingUserId(subscriptionDetails.getCallingUserId());
			hourTransactionManager.addOrUpdateHourTransaction(hourTransaction1, session);

			subscriptionId = subscription.getId();
			Long startSessionTime = 0l;
			Long noOfWeeks = 0l;

			List<SessionInfo> sList = new ArrayList<>();
			if (subscriptionVO.getSchedule() != null && subscriptionVO.getSchedule().getSessionSlots() != null
					&& !subscriptionVO.getSchedule().getSessionSlots().isEmpty()) {

				if (subscriptionVO.getSchedule().getNoOfWeeks() == null) {
					subscriptionVO.getSchedule().setNoOfWeeks(1l);
				}

				noOfWeeks = subscriptionVO.getSchedule().getNoOfWeeks();
				Collections.sort(subscriptionVO.getSchedule().getSessionSlots());
				startSessionTime = subscriptionVO.getSchedule().getSessionSlots().get(0).getStartTime();

				// Call Scheduling subsystem for scheduling the slots
				try {
					MultipleSessionScheduleReq sessionRequest = getSessionRequest(subscription, subscriptionVO);
					sList = scheduleSession(sessionRequest);
				} catch (VException ex) {
					logger.error("Exception occured during scheduling the sessions : " + subscription.toString()
							+ "Schedule : " + subscriptionVO.getSchedule(), ex);
					throw new ConflictException(ErrorCode.SCHEDULING_FAILED,
							"Subscription not created as Scheduling sessions failed: " + subscriptionVO.toString());
				}
			}

			Long lockedHours = getLockedHours(sList);
			Long remainingHours = subscriptionVO.getTotalHours() - lockedHours;

			subscription.setLockedHours(lockedHours);
			subscription.setRemainingHours(remainingHours);
			subscription.setStartDate(startSessionTime);
			subscription.setNoOfWeeks(noOfWeeks);
			subscriptionDetails.setLockedHours(lockedHours);
			subscriptionDetails.setRemainingHours(remainingHours);
			logger.debug("SubscriptionDetails Updated DB entry Started: ");
			subscriptionDetails = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetails, true,
					session);
			logger.debug("SubscriptionDetails Updated DB entry Ended: ");
			logger.debug("Subscription Updated DB entry Started: ");
			subscription = subscriptionDAO.addOrUpdateSubscription(subscription, session,
					subscriptionVO.getCallingUserId());
			logger.debug("SubscriptionDetails Updated DB entry Ended: ");

			HourTransaction hourTransaction2 = new HourTransaction(subscription.getId(), subscriptionDetails.getId(),
					HourType.REMAINING, HourType.LOCKED, subscriptionDetails.getLockedHours(),
					HourTransactionType.SUBSCRIPTION_BOOKED, subscriptionDetails.getId().toString());
			hourTransaction2.setCallingUserId(subscriptionDetails.getCallingUserId());
			hourTransactionManager.addOrUpdateHourTransaction(hourTransaction2, session);

			transaction.commit();

			SubscriptionResponse subscriptionResponse = getSubscriptionResponse(subscription, subscriptionDetails,
					null);

			// Adding RCB for trial session
			if (subscriptionResponse != null) {
				logger.info(subscriptionResponse.toString());
				if (subscriptionResponse.getModel() != null
						&& subscriptionResponse.getModel().equals(SessionModel.TRIAL)) {
					try {

						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
						dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
						String sessionDate = dateFormat
								.format(subscriptionVO.getSchedule().getSessionSlots().get(0).getStartTime());

						SimpleDateFormat timeFormat = new SimpleDateFormat("hh:mm a");
						timeFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
						String sessionTime = timeFormat
								.format(subscriptionVO.getSchedule().getSessionSlots().get(0).getStartTime());

						// TODO remove this when the optimization of
						// subscription buyItems is done
						UserBasicInfo studentInfo = fosUtils.getUserBasicInfo(subscriptionVO.getStudentId(), true);

						String defaultMessage = studentInfo.getFirstName()
								+ " has scheduled a trial session with you for class "
								+ subscriptionResponse.getGrade().toString() + " " + subscriptionResponse.getSubject()
								+ " at " + sessionTime + " on " + sessionDate + ".";

						logger.info("Student name:" + defaultMessage);
						JsonObject jsonObject = new JsonObject();
						jsonObject.addProperty("boardId", subscriptionVO.getBoardId());
						jsonObject.addProperty("callingUserId", subscriptionVO.getCallingUserId());
						jsonObject.addProperty("grade", subscriptionVO.getGrade().longValue());
						if (subscriptionVO.getNote() == null || subscriptionVO.getNote().isEmpty()) {
							jsonObject.addProperty("message", defaultMessage);
						} else {
							jsonObject.addProperty("message", defaultMessage + " Note: " + subscriptionVO.getNote());
						}
						jsonObject.addProperty("studentId", subscriptionVO.getStudentId());
						// jsonObject.addProperty("requestSource",
						// "AUTO_ADDITION_TRIAL");
						jsonObject.addProperty("target", subscriptionVO.getTarget());
						jsonObject.addProperty("teacherId", subscriptionVO.getTeacherId());

						ClientResponse fosResp = WebUtils.INSTANCE.doCall(platformEndpoint + addRcbEndpoint,
								HttpMethod.POST, jsonObject.toString(), true);
						VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
						String resp = fosResp.getEntity(String.class);
						logger.info(resp.toString());
					} catch (Exception e) {
						logger.error("Unable to add rcb for TRIAL session: " + subscriptionVO.toString(), e);
					}
				}
			}

			logger.info("createSubscription Response:" + subscriptionResponse.toString());
			return subscriptionResponse;
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("createSubscription Rollback: " + e.getMessage());
				session.getTransaction().rollback();

				if (subscriptionId != null && subscriptionId > 0l) {
					List<SessionInfo> slist = cancelSubscriptionSessions(subscriptionId,
							subscriptionVO.getCallingUserId(), "Subscription Creation failed");
					if (slist != null) {
						logger.info("createSubscription Rollback, deschedulingSessionsResponse: " + slist.toString());
					}
				}
			}
			if (!(e instanceof ConflictException)) {
				logger.error("createSubscription Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}
	}

	private Long getLockedHours(List<SessionInfo> slist) {
		Long duration = 0l;
		if (slist != null) {
			for (SessionInfo sessionInfo : slist) {
				duration += (sessionInfo.getEndTime() - sessionInfo.getStartTime());
			}
		}
		return duration;
	}

	private Long getRequestedHours(SessionSchedule schedule) {
		if (schedule == null) {
			return 0l;
		}
		Long hours = 0l;
		List<SessionSlot> slots = schedule.getSessionSlots();
		if (slots == null || slots.isEmpty()) {
			return 0l;
		}
		for (SessionSlot s : slots) {
			hours += (s.getEndTime() - s.getStartTime());
		}
		Long noOfWeeks = 0l;
		if (schedule.getNoOfWeeks() != null) {
			noOfWeeks = schedule.getNoOfWeeks();
		}
		return hours * (noOfWeeks);
	}

	public SubscriptionResponse addorUpdateHrsForInstalment(
			RenewSubscriptionForInstalmentReq renewSubscriptionForInstalmentReq) throws Throwable {
		logger.info("ENTRY: " + renewSubscriptionForInstalmentReq);
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		Long subscriptionDetailsId = renewSubscriptionForInstalmentReq.getSubscriptionDetailsId();
		Long millisToAdd = renewSubscriptionForInstalmentReq.getMillisToAdd();
		Long millisToLock = renewSubscriptionForInstalmentReq.getMillisToLock();

		SubscriptionDetails subscriptionDetails = subscriptionDetailsManager
				.getSubscriptionDetailsById(subscriptionDetailsId, true, session);
		if (subscriptionDetails == null) {
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_DETAILS_NOT_FOUND,
					"Subscription Details not found " + subscriptionDetailsId);
		}
		Subscription subscription = getSubscriptionById(subscriptionDetails.getSubscriptionId(), true, session);
		if (subscription == null) {
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
					"Subscription not found " + subscriptionDetails.getSubscriptionId());
		}
		try {
			transaction.begin();
			if (millisToAdd != null) {
				subscriptionDetails.setRemainingHours(subscriptionDetails.getRemainingHours() + millisToAdd);
				subscriptionDetails.setTotalHours(subscriptionDetails.getTotalHours() + millisToAdd);

				subscription.setRemainingHours(subscription.getRemainingHours() + millisToAdd);
				subscription.setTotalHours(subscription.getTotalHours() + millisToAdd);

				// TODO make Long referenceNo in HourTransaction to string, so
				// string referenceIds can be added
				HourTransaction hourTransaction = new HourTransaction(subscription.getId(), subscriptionDetails.getId(),
						HourType.DEFAULT, HourType.REMAINING, millisToAdd, HourTransactionType.PAID_INSTALMENT,
						renewSubscriptionForInstalmentReq.getInstalmentId());
				// hourTransaction.setCallingUserId(renewSubscriptionForInstalmentReq.getCallingUserId());
				hourTransactionManager.addOrUpdateHourTransaction(hourTransaction, session);
			} else if (millisToLock != null) {
				if (millisToLock > subscriptionDetails.getRemainingHours()) {
					throw new ConflictException(ErrorCode.SUBSCRIPTION_HOURS_INSUFFICIENT,
							"SUBSCRIPTION_HOURS_INSUFFICIENT " + subscriptionDetailsId);
				}
				subscriptionDetails.setRemainingHours(subscriptionDetails.getRemainingHours() - millisToLock);
				subscriptionDetails.setLockedHours(subscriptionDetails.getLockedHours() + millisToLock);

				subscription.setRemainingHours(subscription.getRemainingHours() - millisToLock);
				subscription.setLockedHours(subscription.getLockedHours() + millisToLock);

				// TODO make Long referenceNo in HourTransaction to string, so
				// string referenceIds can be added
				HourTransaction hourTransaction = new HourTransaction(subscription.getId(), subscriptionDetails.getId(),
						HourType.REMAINING, HourType.LOCKED, millisToLock, HourTransactionType.PAID_INSTALMENT,
						renewSubscriptionForInstalmentReq.getInstalmentId());
				// hourTransaction.setCallingUserId(renewSubscriptionForInstalmentReq.getCallingUserId());
				hourTransactionManager.addOrUpdateHourTransaction(hourTransaction, session);
			}

			subscriptionDetails = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetails, true,
					session);
			subscription = subscriptionDAO.addOrUpdateSubscription(subscription, session,
					renewSubscriptionForInstalmentReq.getCallingUserId());
			transaction.commit();
			SubscriptionResponse subscriptionResponse = getSubscriptionResponse(subscription, subscriptionDetails,
					null);
			logger.info("EXIT " + subscriptionResponse);
			return subscriptionResponse;
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			throw e;
		} finally {
			session.close();
		}
	}

	public CancelSubscriptionResponse cancelSubscription(CancelSubscriptionRequest cancelSubscriptionRequest)
			throws Throwable {
		logger.info("ENTRY " + cancelSubscriptionRequest);
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		Long id = cancelSubscriptionRequest.getSubscriptionId();
		List<SessionInfo> slist = null;
		Subscription subscription = null;
		Long totalRefund = 0l;
		List<Long> sdIds = new ArrayList<>();
		try {
			transaction.begin();

			// Fetch subscription and validate
			subscription = getSubscriptionById(id, null, session);
			if (subscription == null) {
				logger.info("Subscription with id: " + id + " doesn't exist");
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"Subscription with id: " + id + " doesn't exist");
			} else if (subscription.getEnabled() != null && subscription.getEnabled().equals(false)) {
				throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
						"Subscription with id: " + id + " already cancelled");
			}

			// Call cancelSessions API in scheduling subsystem.
			// Note : Scheduling throws an error if there is an active sessions
			slist = cancelSubscriptionSessions(id, cancelSubscriptionRequest.getCallingUserId(),
					cancelSubscriptionRequest.getRefundReason());

			// cancelling the proposals
			cancelSubscriptionProposals(id, cancelSubscriptionRequest.getCallingUserId());

			List<SubscriptionDetails> sDetails = subscriptionDetailsManager.getSubscriptionDetailsBySubscriptionId(id,
					true, session);
			List<HourTransaction> hourTransactions = new ArrayList<>();
			for (SubscriptionDetails i : sDetails) {
				totalRefund += ((i.getTotalHours() - i.getConsumedHours()) * i.getTeacherHourlyRate())
						/ DateTimeUtils.MILLIS_PER_HOUR;
				sdIds.add(i.getId());
				// i.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());

				HourTransaction hourTransaction = new HourTransaction(subscription.getId(), i.getId(), HourType.LOCKED,
						HourType.REMAINING, i.getLockedHours(), HourTransactionType.SUBSCRIPTION_ENDED,
						i.getId().toString());
				// hourTransaction.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());
				hourTransactions.add(hourTransaction);

				i.setEnabled(false);
				i.setRemainingHours(i.getRemainingHours() + i.getLockedHours());
				i.setLockedHours(0l);
			}
			subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
			subscription.setEnabled(false);
			subscription.setState(SubscriptionState.ENDED);
			subscription.setEndTime(System.currentTimeMillis());
			subscription.setEndReason(cancelSubscriptionRequest.getRefundReason());
			// subscription.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());
			subscription.setEndedBy(cancelSubscriptionRequest.getCallingUserId());
			subscription.setRemainingHours(subscription.getRemainingHours() + subscription.getLockedHours());
			subscription.setLockedHours(0l);
			subscription = subscriptionDAO.addOrUpdateSubscription(subscription, session,
					cancelSubscriptionRequest.getCallingUserId());

			Refund refund = new Refund(id, totalRefund, cancelSubscriptionRequest.getRefundReason());
			// refund.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());
			refundManager.addOrUpdateRefund(refund, session);

			hourTransactionManager.updateAllHourTransaction(hourTransactions, session);

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("cancelSubscription Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
					&& !(e instanceof ForbiddenException)) {
				logger.error("cancelSubscription Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		CancelSubscriptionResponse cancelSubscriptionResponse = new CancelSubscriptionResponse(totalRefund.intValue(),
				subscription.getTotalHours(), subscription.getRemainingHours() + subscription.getLockedHours(), 0l,
				subscription.getHourlyRate(), cancelSubscriptionRequest.getRefundReason(), sdIds,
				subscription.getStudentId(), subscription.getTeacherId(), cancelSubscriptionRequest.getCallingUserId(),
				subscription.getTitle(), slist, subscription.getSubModel(), subscription.getModel());
		logger.info("cancelSubscription Response:" + cancelSubscriptionResponse.toString());
		return cancelSubscriptionResponse;
	}

	public CancelSubscriptionResponse cancelTrialSubscription(CancelSubscriptionRequest cancelSubscriptionRequest)
			throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		Long id = cancelSubscriptionRequest.getSubscriptionId();
		List<SessionInfo> slist = null;
		Subscription subscription = null;
		Long totalRefund = 0l;
		List<Long> sdIds = new ArrayList<Long>();

		try {
			transaction.begin();

			subscription = getSubscriptionById(id, null, session);
			if (subscription == null) {
				logger.info("Subscription with id: " + id + " doesn't exist");
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"Subscription with id: " + id + " doesn't exist");
			} else if (subscription.getEnabled() != null && subscription.getEnabled().equals(false)) {
				throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
						"Subscription with id: " + id + " already cancelled");
			}

			List<SubscriptionDetails> sDetails = subscriptionDetailsManager.getSubscriptionDetailsBySubscriptionId(id,
					true, session);
			List<HourTransaction> hourTransactions = new ArrayList<HourTransaction>();
			for (SubscriptionDetails i : sDetails) {
				totalRefund += ((i.getTotalHours() - i.getConsumedHours()) * i.getTeacherHourlyRate())
						/ DateTimeUtils.MILLIS_PER_HOUR;
				sdIds.add(i.getId());
				// i.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());

				HourTransaction hourTransaction = new HourTransaction(subscription.getId(), i.getId(), HourType.LOCKED,
						HourType.REMAINING, i.getLockedHours(), HourTransactionType.SUBSCRIPTION_ENDED,
						i.getId().toString());
				// hourTransaction.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());
				hourTransactions.add(hourTransaction);

				i.setEnabled(false);
				i.setRemainingHours(i.getRemainingHours() + i.getLockedHours());
				i.setLockedHours(0l);
			}
			subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
			subscription.setEnabled(false);
			subscription.setState(SubscriptionState.ENDED);
			subscription.setEndTime(System.currentTimeMillis());
			subscription.setEndReason(cancelSubscriptionRequest.getRefundReason());
			// subscription.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());
			subscription.setEndedBy(cancelSubscriptionRequest.getCallingUserId());
			subscription.setRemainingHours(subscription.getRemainingHours() + subscription.getLockedHours());
			subscription.setLockedHours(0l);
			subscription = subscriptionDAO.addOrUpdateSubscription(subscription, session,
					cancelSubscriptionRequest.getCallingUserId());

			// TODO : Need to check how the sessions are getting cancelled

			Refund refund = new Refund(id, totalRefund, cancelSubscriptionRequest.getRefundReason());
			// refund.setCallingUserId(cancelSubscriptionRequest.getCallingUserId());
			refundManager.addOrUpdateRefund(refund, session);

			hourTransactionManager.updateAllHourTransaction(hourTransactions, session);

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("cancelTrialSubscription Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
					&& !(e instanceof ForbiddenException)) {
				logger.error("cancelTrialSubscription Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		CancelSubscriptionResponse cancelSubscriptionResponse = new CancelSubscriptionResponse(totalRefund.intValue(),
				subscription.getTotalHours(), subscription.getRemainingHours(), subscription.getLockedHours(),
				subscription.getHourlyRate(), cancelSubscriptionRequest.getRefundReason(), sdIds,
				subscription.getStudentId(), subscription.getTeacherId(), cancelSubscriptionRequest.getCallingUserId(),
				subscription.getTitle(), slist, subscription.getSubModel(), subscription.getModel());
		logger.info("cancelTrialSubscription Response:" + cancelSubscriptionResponse.toString());
		return cancelSubscriptionResponse;
	}

	@Deprecated
	// It does not make sense to revert a cancel subscription calls and
	// rescheduling all the sessions will lead to more problems
	// Cancel subscription need to be rewritten
	public SubscriptionResponse revertCancelSubscription(GetCancelSubscriptionReq req) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		Subscription subscriptionRes = null;
		Long id = req.getSubscriptionId();

		try {
			transaction.begin();

			subscriptionRes = getSubscriptionById(id, false, session);
			if (subscriptionRes != null) {
				List<SessionInfo> slist = req.getSessionList();
				List<SessionSlot> slots = new ArrayList<SessionSlot>();
				for (SessionInfo i : slist) {
					SessionSlot s = new SessionSlot(i.getStartTime(), i.getEndTime(), i.getTopic(), i.getTitle(),
							i.getDescription(), false);
					slots.add(s);
				}

				SessionRequest sessionRequest = new SessionRequest(subscriptionRes.getStudentId(),
						subscriptionRes.getTeacherId(), subscriptionRes.getId(), subscriptionRes.getModel(), slots,
						null, Long.parseLong(subscriptionRes.getCallingUserId()),
						Long.parseLong(subscriptionRes.getSubject()), "revertCancelSubscription");
				// List<SessionInfo> scheduleResponse =
				// scheduleSession(sessionRequest);
				// if (scheduleResponse != null) {
				// slist = scheduleResponse.getSessionInfo();
				// logger.info("scheduleSession Response:" +
				// scheduleResponse.toString());
				// }
				// if (scheduleResponse == null || slist == null ||
				// slist.isEmpty()) {
				// logger.info("Session scheduling failed in
				// revertCancelSubscription");
				// throw new ConflictException(ErrorCode.SCHEDULING_FAILED,
				// "Session scheduling failed in revertCancelSubscription: " +
				// slist);
				// }
			}

			List<Long> sDetailsIds = req.getSubscriptionDetailsIds();
			List<SubscriptionDetails> sDetails = new ArrayList<SubscriptionDetails>();

			if (subscriptionRes != null && sDetailsIds != null && !sDetailsIds.isEmpty()) {
				for (Long sId : sDetailsIds) {
					SubscriptionDetails sd = subscriptionDetailsManager.getSubscriptionDetailsById(sId, false, session);
					if (sd != null) {
						sDetails.add(sd);
					}
				}

				for (SubscriptionDetails i : sDetails) {
					i.setEnabled(true);
				}
				subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
				subscriptionRes.setEnabled(true);
				subscriptionRes.setState(SubscriptionState.ACTIVE);
				subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
						req.getCallingUserId());

				transaction.commit();
				logger.info("Response:" + subscriptionRes.toString());
			}
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("revertCancelSubscription Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof ConflictException)) {
				logger.error("revertCancelSubscription Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		// SessionInfoResponse sessionInfoResponse =
		// getSessionInfoResponse(subscriptionRes);
		SubscriptionResponse subscriptionResponse = getSubscriptionResponse(subscriptionRes);
		logger.info("revertCancelSubscription Response:" + subscriptionResponse.toString());
		return subscriptionResponse;
	}

	public SessionResponse bookSession(SessionRequest sessionRequest) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		List<SessionInfo> scheduledSessionInfos = new ArrayList<SessionInfo>();

		Subscription subscriptionRes = null;

		Long id = sessionRequest.getSubscriptionId();
		subscriptionRes = getSubscriptionById(id, null, session);

		if (subscriptionRes == null) {
			logger.info("Subscription with id: " + id + " doesn't exist");
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
					"Subscription with id: " + id + " doesn't exist");
		} else if (subscriptionRes.getEnabled() != null && subscriptionRes.getEnabled().equals(false)) {
			throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
					"Subscription with id: " + id + " already cancelled");
		}

		logger.info("bookSession subscription before updating " + subscriptionRes);

		sessionRequest.setTeacherId(subscriptionRes.getTeacherId());
		sessionRequest.setStudentId(subscriptionRes.getStudentId());
		sessionRequest.setModel(subscriptionRes.getModel());
		sessionRequest.setBoardId(subscriptionRes.getBoardId());
		List<SessionSlot> slots = sessionRequest.getSlots();
		if (slots != null) {
			for (SessionSlot s : slots) {
				if (s != null) {
					s.setTitle(subscriptionRes.getTitle());
				} else {
					logger.warn("Null values in session slots for " + sessionRequest);
				}
			}
		}
		sessionRequest.setSlots(slots);

		Long scheduledHours = sessionRequest.getHours();
		if (scheduledHours > subscriptionRes.getRemainingHours()) {
			logger.info("RequestedHours: " + scheduledHours + " are more than the hours: "
					+ subscriptionRes.getRemainingHours() + " Subscription: " + subscriptionRes.getId());
			throw new BadRequestException(ErrorCode.SUBSCRIPTION_REQUESTED_HOURS_INCORRECT,
					"RequestedHours: " + scheduledHours + " are more than the hours: "
							+ subscriptionRes.getRemainingHours() + " Subscription: " + subscriptionRes.getId());
		}

		MultipleSessionScheduleReq multipleSessionScheduleReq = new MultipleSessionScheduleReq(
				subscriptionRes.getSubject(), null, subscriptionRes.getTitle(), null, sessionRequest.getSlots(),
				SessionPayoutType.PAID, sessionRequest.getSubscriptionId(),
				MultipleSessionScheduleReq.createStudentList(subscriptionRes.getStudentId()),
				subscriptionRes.getTeacherId(), subscriptionRes.fetchFeatureSource(), subscriptionRes.getModel(),
				subscriptionRes.fetchRequestSource(), sessionRequest.getCallingUserId(),
				subscriptionRes.getOfferingId(), sessionRequest.getProposalId());

		// Avoid calendar blocking for IL
		if (SessionModel.IL.equals(multipleSessionScheduleReq.getSessionModel())) {
			multipleSessionScheduleReq.setNoCalendarCheck(Boolean.TRUE);
		}

		scheduledSessionInfos = scheduleSession(multipleSessionScheduleReq);
		if (CollectionUtils.isEmpty(scheduledSessionInfos)) {
			logger.info("Session scheduling failed in bookSession: " + scheduledSessionInfos);
			throw new ConflictException(ErrorCode.SESSION_NOT_SCHEDULED,
					"Session scheduling failed in bookSession: " + scheduledSessionInfos);
		}
		Long sessionId = scheduledSessionInfos.get(0).getId();

		// TODO: Temporary approach for race condition. Needs to be changed!
		int NUMBER_OF_TRIES = 2;
		for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			Transaction transaction = session.getTransaction();
			try {
				transaction.begin();
				logger.info("try :" + retry);
				if (retry != 0)
					subscriptionRes = getSubscriptionById(id, null, session);

				Long totalRemainingHours = subscriptionRes.getRemainingHours();
				Long totalLockedHours = subscriptionRes.getLockedHours();
				Long newLockedHours = sessionRequest.getHours();

				totalRemainingHours -= newLockedHours;
				totalLockedHours += newLockedHours;

				List<SubscriptionDetails> sDetails = subscriptionDetailsManager
						.getSubscriptionDetailsBySubscriptionId(id, true, session);

				Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
					@Override
					public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
						return o1.getCreationTime().compareTo(o2.getCreationTime());
					}
				});

				List<HourTransaction> hourTransactions = new ArrayList<HourTransaction>();
				for (SubscriptionDetails i : sDetails) {
					Long remainingHours = i.getRemainingHours();
					// i.setCallingUserId(sessionRequest.getCallingUserId());
					if (remainingHours >= newLockedHours) {
						i.setRemainingHours(remainingHours - newLockedHours);
						i.setLockedHours(i.getLockedHours() + newLockedHours);

						HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.REMAINING, HourType.LOCKED, newLockedHours, HourTransactionType.SESSION_BOOKED,
								sessionId.toString());
						// hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
						hourTransactions.add(hourTransaction);
						break;
					} else {
						i.setRemainingHours(0l);
						i.setLockedHours(i.getLockedHours() + remainingHours);
						newLockedHours -= remainingHours;

						HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.REMAINING, HourType.LOCKED, remainingHours, HourTransactionType.SESSION_BOOKED,
								sessionId.toString());
						// hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
						hourTransactions.add(hourTransaction);
					}
				}
				subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
				subscriptionRes.setLockedHours(totalLockedHours);
				subscriptionRes.setRemainingHours(totalRemainingHours);
				// subscriptionRes.setCallingUserId(sessionRequest.getCallingUserId());
				subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
						sessionRequest.getCallingUserId());

				hourTransactionManager.updateAllHourTransaction(hourTransactions, session);
				transaction.commit();
				break;
			} catch (Exception e) {
				if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
					logger.info("bookSession Rollback: " + e.getMessage());
					session.getTransaction().rollback();
				}
				if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
						&& !(e instanceof ForbiddenException) && !(e instanceof BadRequestException)) {
					logger.info("bookSession Rollback: " + e.getMessage());
				}
				if (retry == (NUMBER_OF_TRIES - 1)) {
					throw e;
				}
			} finally {
				session.close();
			}

		}

		// SessionInfoResponse sessionInfoResponse =
		// getSessionInfoResponse(subscriptionRes);
		SubscriptionResponse sr = getSubscriptionResponse(subscriptionRes);
		SessionResponse sessionResponse = new SessionResponse(scheduledSessionInfos, sr);
		logger.info("bookSession Response:" + sessionResponse.toString());
		return sessionResponse;
	}

	// Assuming only one session is passed in sessionRequest. Using
	// sessionRequest is very confusing here as it contain slots and we dont
	// know how many sessions to cancel
	public SessionResponse cancelSession(CancelSubscriptionSessionRequest cancelSessionRequest) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		Subscription subscriptionRes = null;

		Long id = cancelSessionRequest.getSubscriptionId();
		subscriptionRes = getSubscriptionById(id, null, session);
		if (subscriptionRes == null) {
			logger.info("Subscription with id: " + id + " doesn't exist");
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
					"Subscription with id: " + id + " doesn't exist");
		} else if (subscriptionRes.getEnabled() != null && subscriptionRes.getEnabled().equals(false)) {
			throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
					"Subscription with id: " + id + " already cancelled");
		}

		logger.info("cancelSession subscription before updating " + subscriptionRes);

		// Call scheduling subsystem to Cancel the session.
		// Error codes should be thrown by Scheduling subsystem.
		SessionInfo sessionInfo = cancelSession(cancelSessionRequest.getSessionId(),
				cancelSessionRequest.getCallingUserId(),
                                cancelSessionRequest.getReason(),
                                cancelSessionRequest.getCallingUserRole());

		// TODO: Temporary approach for race condition. Needs to be changed!
		int NUMBER_OF_TRIES = 2;
		for (int retry = 0; retry < NUMBER_OF_TRIES; retry++) {
			if (!session.isOpen()) {
				session = sessionFactory.openSession();
			}
			Transaction transaction = session.getTransaction();
			try {
				transaction.begin();
				logger.info("try :" + retry);
				if (retry != 0)
					subscriptionRes = getSubscriptionById(id, null, session);

				Long totalRemainingHours = subscriptionRes.getRemainingHours();
				Long totalLockedHours = subscriptionRes.getLockedHours();
				Long newLockedHours = sessionInfo.getEndTime() - sessionInfo.getStartTime();

				List<SubscriptionDetails> sDetails = subscriptionDetailsManager
						.getSubscriptionDetailsBySubscriptionId(id, true, session);

				Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
					@Override
					public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
						return o2.getCreationTime().compareTo(o1.getCreationTime());
					}
				});

				if (totalLockedHours >= newLockedHours) {
					totalRemainingHours += newLockedHours;
					totalLockedHours -= newLockedHours;
					List<HourTransaction> hourTransactions = new ArrayList<>();
					for (SubscriptionDetails i : sDetails) {
						Long remainingHours = i.getRemainingHours();
						// i.setCallingUserId(cancelSessionRequest.getCallingUserId());
						Long lockedHours = i.getLockedHours();
						if (lockedHours >= newLockedHours) {
							i.setRemainingHours(remainingHours + newLockedHours);
							i.setLockedHours(lockedHours - newLockedHours);

							HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
									HourType.LOCKED, HourType.REMAINING, newLockedHours,
									HourTransactionType.SESSION_CANCELLED,
									cancelSessionRequest.getSessionId().toString());
							// hourTransaction.setCallingUserId(cancelSessionRequest.getCallingUserId());
							hourTransactions.add(hourTransaction);
							break;
						} else {
							i.setRemainingHours(remainingHours + lockedHours);
							i.setLockedHours(0l);
							newLockedHours -= lockedHours;

							HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
									HourType.LOCKED, HourType.REMAINING, newLockedHours,
									HourTransactionType.SESSION_CANCELLED,
									cancelSessionRequest.getSessionId().toString());
							// hourTransaction.setCallingUserId(cancelSessionRequest.getCallingUserId());
							hourTransactions.add(hourTransaction);
						}
					}
					subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
					subscriptionRes.setLockedHours(totalLockedHours);
					subscriptionRes.setRemainingHours(totalRemainingHours);
					// subscriptionRes.setCallingUserId(cancelSessionRequest.getCallingUserId());
					subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
							cancelSessionRequest.getCallingUserId());

					hourTransactionManager.updateAllHourTransaction(hourTransactions, session);
				} else {
					logger.info("Cancelling more hours: " + newLockedHours + " than actually locked: "
							+ totalLockedHours + " for session");
					throw new BadRequestException(ErrorCode.SUBSCRIPTION_REQUESTED_HOURS_INCORRECT,
							"Cancelling more hours: " + newLockedHours + " than actually locked: " + totalLockedHours
									+ " for session");
				}
				transaction.commit();
				break;
			} catch (Exception e) {
				if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
					logger.info("cancelSession Rollback: " + e.getMessage());
					session.getTransaction().rollback();
				}
				if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
						&& !(e instanceof ForbiddenException) && !(e instanceof BadRequestException)) {
					logger.error("cancelSession Rollback: " + e.getMessage());
				}
				if (retry == (NUMBER_OF_TRIES - 1)) {
					throw e;
				}
			} finally {
				session.close();
			}
		}

		// SessionInfoResponse sessionInfoResponse =
		// getSessionInfoResponse(subscriptionRes);
		SubscriptionResponse sr = getSubscriptionResponse(subscriptionRes);
		SessionResponse sessionResponse = new SessionResponse(sessionInfo, sr);
		logger.info("cancelSession Response:" + sessionResponse.toString());
		return sessionResponse;
	}

	// This is not used and reschedule session has nothing to do with
	// Subscription. Reschedule should be handled entirely by scheduling
	// subsystem
	@Deprecated
	public SessionResponse rescheduleSession(SessionRequest sessionRequest) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		List<SessionInfo> slist = null;
		Subscription subscriptionRes = null;
		try {
			transaction.begin();

			Long id = sessionRequest.getSubscriptionId();
			subscriptionRes = getSubscriptionById(id, null, session);
			if (subscriptionRes == null) {
				logger.info("Subscription with id: " + id + " doesn't exist");
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"Subscription with id: " + id + " doesn't exist");
			} else if (subscriptionRes.getEnabled() != null && subscriptionRes.getEnabled().equals(false)) {
				throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
						"Subscription with id: " + id + " already cancelled");
			}

			// This should call cancelSession
			SessionInfo sessionInfo = cancelSession(sessionRequest.getSessionId(),
                                sessionRequest.getCallingUserId(),
					sessionRequest.getReason(),
                                        sessionRequest.getCallingUserRole());
			if (sessionInfo != null) {
				logger.info("scheduleSession Response:" + slist.toString());
			}

			Boolean cancelled = true;
			Boolean sessionActive = false;
			for (SessionInfo s : slist) {
				if (s.getState() != null) {
					if (s.getState().equals(SessionState.ACTIVE)) {
						sessionActive = true;
					}
					if (!s.getState().equals(SessionState.CANCELED)) {
						cancelled = false;
						break;
					}
				}
			}

			if (sessionActive.equals(true)) {
				throw new ConflictException(ErrorCode.SESSION_ALREADY_ACTIVE, "Trying to cancel an ACTIVE session");
			}
			if (cancelled.equals(false)) {
				throw new ConflictException(ErrorCode.SESSION_ALREADY_CANCELED,
						"Session not cancelled, sessionId: " + sessionRequest.getSessionId());
			}

			SessionInfo sInfo = slist.get(0);
			Long oldHours = sInfo.getEndTime() - sInfo.getStartTime();

			sessionRequest.setStudentId(subscriptionRes.getStudentId());
			sessionRequest.setTeacherId(subscriptionRes.getTeacherId());
			sessionRequest.setModel(subscriptionRes.getModel());
			sessionRequest.setSessionId(null);
			sessionRequest.setBoardId(subscriptionRes.getBoardId());

			// ScheduleResponse scheduleResponse =
			// scheduleSession(sessionRequest);
			// if (scheduleResponse != null) {
			// slist = scheduleResponse.getSessionInfo();
			// logger.info("scheduleSession Response:" +
			// scheduleResponse.toString());
			// }
			// if (scheduleResponse == null || slist == null || slist.isEmpty())
			// {
			// logger.info("Session scheduling failed in rescheduleSession: " +
			// slist);
			// throw new ConflictException(ErrorCode.SCHEDULING_FAILED,
			// "Session scheduling failed in rescheduleSession: " + slist);
			// }
			//
			sInfo = slist.get(0);
			Long newHours = sInfo.getEndTime() - sInfo.getStartTime();

			Long totalRemainingHours = subscriptionRes.getRemainingHours();
			Long totalLockedHours = subscriptionRes.getLockedHours();
			Long hourDiff = oldHours - newHours;

			List<SubscriptionDetails> sDetails = subscriptionDetailsManager.getSubscriptionDetailsBySubscriptionId(id,
					true, session);

			Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
				@Override
				public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
					return o2.getCreationTime().compareTo(o1.getCreationTime());
				}
			});

			List<HourTransaction> hourTransactions = new ArrayList<HourTransaction>();
			if (hourDiff >= 0 && totalLockedHours >= hourDiff) {
				totalRemainingHours += hourDiff;
				totalLockedHours -= hourDiff;
				for (SubscriptionDetails i : sDetails) {
					Long remainingHours = i.getRemainingHours();
					// i.setCallingUserId(sessionRequest.getCallingUserId());
					Long lockedHours = i.getLockedHours();
					if (lockedHours >= hourDiff) {
						i.setRemainingHours(remainingHours + hourDiff);
						i.setLockedHours(lockedHours - hourDiff);

						HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.LOCKED, HourType.REMAINING, hourDiff, HourTransactionType.SESSION_RESCHEDULED,
								sessionRequest.getSessionId().toString());
						// hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
						hourTransactions.add(hourTransaction);
						break;
					} else {
						i.setRemainingHours(remainingHours + lockedHours);
						i.setLockedHours(0l);
						hourDiff -= lockedHours;

						HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.LOCKED, HourType.REMAINING, lockedHours,
								HourTransactionType.SESSION_RESCHEDULED, sessionRequest.getSessionId().toString());
						// hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
						hourTransactions.add(hourTransaction);
					}
				}
				subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
				subscriptionRes.setLockedHours(totalLockedHours);
				subscriptionRes.setRemainingHours(totalRemainingHours);
				// subscriptionRes.setCallingUserId(sessionRequest.getCallingUserId());
				subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
						sessionRequest.getCallingUserId());
			} else if (hourDiff < 0 && totalRemainingHours >= Math.abs(hourDiff)) {
				hourDiff = Math.abs(hourDiff);
				totalRemainingHours -= hourDiff;
				totalLockedHours += hourDiff;
				for (SubscriptionDetails i : sDetails) {
					Long remainingHours = i.getRemainingHours();
					// i.setCallingUserId(sessionRequest.getCallingUserId());
					Long lockedHours = i.getLockedHours();
					if (remainingHours >= hourDiff) {
						i.setRemainingHours(remainingHours - hourDiff);
						i.setLockedHours(lockedHours + hourDiff);

						HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.REMAINING, HourType.LOCKED, Math.abs(hourDiff),
								HourTransactionType.SESSION_RESCHEDULED, sessionRequest.getSessionId().toString());
						// hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
						hourTransactions.add(hourTransaction);
						break;
					} else {
						i.setLockedHours(remainingHours + lockedHours);
						i.setRemainingHours(0l);
						hourDiff -= remainingHours;

						HourTransaction hourTransaction = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.REMAINING, HourType.LOCKED, remainingHours,
								HourTransactionType.SESSION_RESCHEDULED, sessionRequest.getSessionId().toString());
						// hourTransaction.setCallingUserId(sessionRequest.getScheduledBy());
						hourTransactions.add(hourTransaction);
					}
				}
				subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
				subscriptionRes.setLockedHours(totalLockedHours);
				subscriptionRes.setRemainingHours(totalRemainingHours);
				// subscriptionRes.setCallingUserId(sessionRequest.getCallingUserId());
				subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
						sessionRequest.getCallingUserId());
			} else {
				logger.info("Cancelling more hours: " + Math.abs(hourDiff) + " than actually locked: "
						+ totalLockedHours + " for session");
				throw new BadRequestException(ErrorCode.SUBSCRIPTION_REQUESTED_HOURS_INCORRECT,
						"Cancelling more hours: " + Math.abs(hourDiff) + " than actually locked: " + totalLockedHours
								+ " for session");
			}

			hourTransactionManager.updateAllHourTransaction(hourTransactions, session);

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("rescheduleSession Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof ConflictException) && !(e instanceof NotFoundException)
					&& !(e instanceof ForbiddenException) && !(e instanceof BadRequestException)) {
				logger.error("rescheduleSession Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		// SessionInfoResponse sessionInfoResponse =
		// getSessionInfoResponse(subscriptionRes);
		SubscriptionResponse sr = getSubscriptionResponse(subscriptionRes);
		SessionResponse sessionResponse = new SessionResponse(slist, sr);
		logger.info("rescheduleSession Response:" + sessionResponse.toString());
		return sessionResponse;
	}

	public Subscription completeSession(CompleteSessionRequest sessionRequest) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		Subscription subscriptionRes = null;
		try {
			transaction.begin();

			Long id = sessionRequest.getSubscriptionId();
			subscriptionRes = getSubscriptionById(id, null, session);
			if (subscriptionRes == null) {
				logger.info("Subscription with id: " + id + " doesn't exist");
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"Subscription with id: " + id + " doesn't exist");
			} else if (subscriptionRes.getEnabled() != null && subscriptionRes.getEnabled().equals(false)) {
				throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
						"Subscription with id: " + id + " already cancelled");
			}

			logger.info("completeSession subscription before updating " + subscriptionRes);

			Long totalLockedHours = subscriptionRes.getLockedHours();
			Long totalConsumedHours = subscriptionRes.getConsumedHours();
			Long totalRemainingHours = subscriptionRes.getRemainingHours();
			Long billingDuration = sessionRequest.getBillingDuration();
			Long sessionScheduledDuration = sessionRequest.getSessionScheduledDuration();

			List<SubscriptionDetails> sDetails = subscriptionDetailsManager.getSubscriptionDetailsBySubscriptionId(id,
					true, session);
			List<HourTransaction> hourTransactions = new ArrayList<HourTransaction>();
			Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
				@Override
				public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
					return o1.getCreationTime().compareTo(o2.getCreationTime());
				}
			});

			logger.info("totalConsumedHours:" + totalConsumedHours);
			logger.info("totalLockedHours:" + totalLockedHours);
			logger.info("totalRemainingHours:" + totalRemainingHours);
			logger.info("sessionScheduledDuration:" + sessionScheduledDuration);
			logger.info("billingDuration:" + billingDuration);
			if (sessionScheduledDuration >= billingDuration && totalLockedHours >= billingDuration
					&& totalLockedHours >= sessionScheduledDuration) {
				totalConsumedHours += billingDuration;
				totalLockedHours -= sessionScheduledDuration;
				totalRemainingHours = totalRemainingHours + sessionScheduledDuration - billingDuration;
				for (SubscriptionDetails i : sDetails) {
					Long lockedHours = i.getLockedHours();
					// i.setCallingUserId(sessionRequest.getCallingUserId());
					if (lockedHours >= sessionScheduledDuration) {
						i.setLockedHours(lockedHours - sessionScheduledDuration);
						i.setConsumedHours(i.getConsumedHours() + billingDuration);
						i.setRemainingHours(i.getRemainingHours() + sessionScheduledDuration - billingDuration);

						HourTransaction hourTransaction1 = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.LOCKED, HourType.CONSUMED, billingDuration, HourTransactionType.SESSION_ENDED,
								sessionRequest.getSessionId().toString());
						// hourTransaction1.setCallingUserId(sessionRequest.getCallingUserId());
						HourTransaction hourTransaction2 = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.LOCKED, HourType.REMAINING, sessionScheduledDuration - billingDuration,
								HourTransactionType.SESSION_ENDED, sessionRequest.getSessionId().toString());
						// hourTransaction2.setCallingUserId(sessionRequest.getCallingUserId());
						hourTransactions.add(hourTransaction1);
						hourTransactions.add(hourTransaction2);
						break;
					} else {
						i.setLockedHours(0l);
						i.setConsumedHours(i.getConsumedHours() + lockedHours);
						billingDuration -= lockedHours;
						sessionScheduledDuration -= lockedHours;

						HourTransaction hourTransaction1 = new HourTransaction(subscriptionRes.getId(), i.getId(),
								HourType.LOCKED, HourType.CONSUMED, lockedHours, HourTransactionType.SESSION_ENDED,
								sessionRequest.getSessionId().toString());
						// hourTransaction1.setCallingUserId(sessionRequest.getCallingUserId());
						hourTransactions.add(hourTransaction1);
					}
				}
				subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
				logger.info("Finally");
				logger.info("totalConsumedHours:" + totalConsumedHours);
				logger.info("totalLockedHours:" + totalLockedHours);
				logger.info("totalRemainingHours:" + totalRemainingHours);
				subscriptionRes.setLockedHours(totalLockedHours);
				subscriptionRes.setConsumedHours(totalConsumedHours);
				subscriptionRes.setRemainingHours(totalRemainingHours);
				// subscriptionRes.setCallingUserId(sessionRequest.getCallingUserId());
				subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
						sessionRequest.getCallingUserId());

				hourTransactionManager.updateAllHourTransaction(hourTransactions, session);
			} else {
				logger.error("Session taken for more sessionScheduledDuration:" + sessionScheduledDuration
						+ ", billingDuration:" + billingDuration + " than hours: " + totalLockedHours
						+ " purchased under Subscription: " + id);
				throw new BadRequestException(ErrorCode.SESSION_DURATION_LIMIT_EXCEEDED,
						"Session taken for more sessionScheduledDuration:" + sessionScheduledDuration
								+ ", billingDuration:" + billingDuration + " than hours: " + totalLockedHours
								+ " purchased under Subscription: " + id);
			}

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("completeSession Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof NotFoundException) && !(e instanceof ForbiddenException)
					&& !(e instanceof BadRequestException) && !(e instanceof ConstraintViolationException)) {
				logger.error("completeSession Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		// SessionInfoResponse sessionInfoResponse =
		// getSessionInfoResponse(subscriptionRes);
		// SubscriptionResponse sr = getSubscriptionResponse(subscriptionRes,
		// sessionInfoResponse);
		logger.info("completeSession Response:" + subscriptionRes.toString());
		return subscriptionRes;
	}

	public CancelSubscriptionResponse getRefundInfo(Long id) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		Subscription subscription = null;
		Long totalRefund = 0l;
		List<Long> sdIds = new ArrayList<Long>();
		try {
			subscription = getSubscriptionById(id, null, session);

			if (subscription == null) {
				logger.info("Subscription with id: " + id + " doesn't exist");
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"Subscription with id: " + id + " doesn't exist");
			} else if (subscription.getEnabled() != null && subscription.getEnabled().equals(false)) {
				throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
						"Subscription with id: " + id + " already cancelled");
			} else {
				// TODO shouldn't we use teacherhourly rate for each sdetailsId
				totalRefund = ((subscription.getTotalHours() - subscription.getConsumedHours())
						* subscription.getHourlyRate()) / DateTimeUtils.MILLIS_PER_HOUR;

				List<SubscriptionDetails> sDetails = subscriptionDetailsManager
						.getSubscriptionDetailsBySubscriptionId(id, true, session);
				for (SubscriptionDetails i : sDetails) {
					sdIds.add(i.getId());
				}
			}
		} catch (Exception e) {
			logger.info("getRefundInfo Error: " + e.getMessage());
			if (!(e instanceof NotFoundException) && !(e instanceof ForbiddenException)) {
				logger.error("getRefundInfo Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		CancelSubscriptionResponse cancelSubscriptionResponse = new CancelSubscriptionResponse(totalRefund.intValue(),
				subscription.getTotalHours(), subscription.getRemainingHours() + subscription.getLockedHours(), 0l,
				subscription.getHourlyRate(), null, sdIds, subscription.getStudentId(), subscription.getTeacherId(),
				null, subscription.getTitle(), null, subscription.getSubModel(), subscription.getModel());
		logger.info("getRefundInfo Response:" + cancelSubscriptionResponse.toString());
		return cancelSubscriptionResponse;
	}

	public SubscriptionResponse updateDurationConflict(UpdateConflictDurationRequest request) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		Subscription subscriptionRes = null;
		try {
			transaction.begin();

			Long id = request.getSubscriptionId();
			subscriptionRes = getSubscriptionById(id, null, session);
			if (subscriptionRes == null) {
				logger.info("Subscription with id: " + id + " doesn't exist");
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"Subscription with id: " + id + " doesn't exist");
			} else if (subscriptionRes.getEnabled() != null && subscriptionRes.getEnabled().equals(false)) {
				throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
						"Subscription with id: " + id + " already cancelled");
			}

			Long totalRemainingHours = subscriptionRes.getRemainingHours();
			Long totalConsumedHours = subscriptionRes.getConsumedHours();
			Long durationDiff = request.getNewDuration() - request.getPreviousDuration();

			List<SubscriptionDetails> sDetails = subscriptionDetailsManager.getSubscriptionDetailsBySubscriptionId(id,
					true, session);

			Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
				@Override
				public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
					return o2.getCreationTime().compareTo(o1.getCreationTime());
				}
			});

			totalConsumedHours += durationDiff;
			totalRemainingHours -= durationDiff;
			SubscriptionDetails lastSD = sDetails.get(0);
			lastSD.setConsumedHours(lastSD.getConsumedHours() + durationDiff);
			lastSD.setRemainingHours(lastSD.getRemainingHours() - durationDiff);
			// lastSD.setCallingUserId(request.getCallingUserId());

			subscriptionDetailsManager.addOrUpdateSubscriptionDetails(lastSD, true, session);
			subscriptionRes.setRemainingHours(totalRemainingHours);
			subscriptionRes.setConsumedHours(totalConsumedHours);
			// subscriptionRes.setCallingUserId(request.getCallingUserId());
			subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
					request.getCallingUserId());

			HourTransaction hourTransaction = null;
			if (durationDiff > 0) {
				hourTransaction = new HourTransaction(subscriptionRes.getId(), lastSD.getId(), HourType.REMAINING,
						HourType.CONSUMED, durationDiff, HourTransactionType.SESSION_DURATION_CONFLICT,
						request.getSessionId().toString());
			} else {
				hourTransaction = new HourTransaction(subscriptionRes.getId(), lastSD.getId(), HourType.CONSUMED,
						HourType.REMAINING, Math.abs(durationDiff), HourTransactionType.SESSION_DURATION_CONFLICT,
						request.getSessionId().toString());
			}
			// hourTransaction.setCallingUserId(request.getCallingUserId());
			hourTransactionManager.addOrUpdateHourTransaction(hourTransaction, session);

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("updateDurationConflict Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof NotFoundException) && !(e instanceof ForbiddenException)) {
				logger.error("updateDurationConflict Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		// SessionInfoResponse sessionInfoResponse =
		// getSessionInfoResponse(subscriptionRes);
		SubscriptionResponse sr = getSubscriptionResponse(subscriptionRes);
		logger.info("updateDurationConflict Response:" + sr.toString());
		return sr;
	}

	public SubscriptionResponse updateSubscriptionHours(UpdateSubscriptionHoursRequest request) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();

		Subscription subscriptionRes = null;
		try {
			transaction.begin();

			Long id = request.getSubscriptionId();
			subscriptionRes = getSubscriptionById(id, null, session);
			if (subscriptionRes == null) {
				logger.info("Subscription with id: " + id + " doesn't exist");
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"Subscription with id: " + id + " doesn't exist");
			} else if (subscriptionRes.getEnabled() != null && subscriptionRes.getEnabled().equals(false)) {
				throw new ForbiddenException(ErrorCode.SUBSCRIPTION_INACTIVE,
						"Subscription with id: " + id + " already cancelled");
			}

			Long totalRemainingHours = subscriptionRes.getRemainingHours();
			Long totalHours = subscriptionRes.getTotalHours();
			Long hourDiff = request.getHours();

			List<SubscriptionDetails> sDetails = subscriptionDetailsManager.getSubscriptionDetailsBySubscriptionId(id,
					true, session);

			Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
				@Override
				public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
					return o2.getCreationTime().compareTo(o1.getCreationTime());
				}
			});

			if (request.getAddition() != null && request.getAddition().equals(true)) {
				totalRemainingHours += hourDiff;
				totalHours += hourDiff;
			} else {
				totalRemainingHours -= hourDiff;
				totalHours -= hourDiff;
			}
			SubscriptionDetails lastSD = sDetails.get(0);
			if (request.getAddition() != null && request.getAddition().equals(true)) {
				lastSD.setTotalHours(lastSD.getTotalHours() + hourDiff);
				lastSD.setRemainingHours(lastSD.getRemainingHours() + hourDiff);
			} else {
				lastSD.setTotalHours(lastSD.getTotalHours() - hourDiff);
				lastSD.setRemainingHours(lastSD.getRemainingHours() - hourDiff);
			}
			// lastSD.setCallingUserId(request.getCallingUserId());

			subscriptionDetailsManager.addOrUpdateSubscriptionDetails(lastSD, true, session);
			subscriptionRes.setRemainingHours(totalRemainingHours);
			subscriptionRes.setTotalHours(totalHours);
			// subscriptionRes.setCallingUserId(request.getCallingUserId());
			subscriptionRes = subscriptionDAO.addOrUpdateSubscription(subscriptionRes, session,
					request.getCallingUserId());

			transaction.commit();
		} catch (Exception e) {
			if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
				logger.info("updateSubscriptionHours Rollback: " + e.getMessage());
				session.getTransaction().rollback();
			}
			if (!(e instanceof NotFoundException) && !(e instanceof ForbiddenException)) {
				logger.error("updateDurationConflict Rollback: " + e.getMessage());
			}
			throw e;
		} finally {
			session.close();
		}

		// SessionInfoResponse sessionInfoResponse =
		// getSessionInfoResponse(subscriptionRes);
		SubscriptionResponse sr = getSubscriptionResponse(subscriptionRes);
		logger.info("updateSubscriptionHours Response:" + sr.toString());
		return sr;
	}

	public Subscription getSubscriptionById(Long id, Boolean enabled, Session session) {

		Subscription subscription = null;
		if (session == null) {
			SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
			Session session2 = sessionFactory.openSession();
			try {
				subscription = subscriptionDAO.getById(id, enabled, session2);
			} catch (Exception e) {
				logger.info("getSubscriptionById Error: " + e.getMessage());
				throw e;
			} finally {
				session2.close();
			}
		} else {
			try {
				subscription = subscriptionDAO.getById(id, enabled, session);
			} catch (Exception e) {
				logger.info("getSubscriptionById Error: " + e.getMessage());
				throw e;
			}
		}
		return subscription;
	}

	public GetHourlyRateResponse getHourlyRate(Long id) throws NotFoundException {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		Subscription subscription = null;
		try {
			subscription = subscriptionDAO.getById(id, null, session);
		} catch (Exception e) {
			logger.info("getHourlyRate Error: " + e.getMessage());
			throw e;
		} finally {
			session.close();
		}

		GetHourlyRateResponse getHourlyRateResponse = null;
		if (subscription != null) {
			getHourlyRateResponse = new GetHourlyRateResponse(subscription.getHourlyRate(),
					subscription.getRemainingHours(), subscription.getLockedHours(), subscription.getConsumedHours(),
					subscription.getTotalHours(), subscription.getEnabled());
		} else {
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND, "Incorrect SubscriptionId:" + id);
		}
		logger.info("getHourlyRateResponse Response:" + getHourlyRateResponse.toString());
		return getHourlyRateResponse;
	}

	public GetUnlockHoursResponse unlockHours(List<UnlockHoursRequest> unlockHoursRequests) {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Transaction transaction = session.getTransaction();
		Long callingUserId = null;
		if (unlockHoursRequests != null && !unlockHoursRequests.isEmpty()) {
			callingUserId = unlockHoursRequests.get(0).getCallingUserId();
		}

		List<UnlockHoursResponse> unlockHoursResponses = new ArrayList<UnlockHoursResponse>();
		List<Subscription> subscriptions = new ArrayList<Subscription>();
		try {
			transaction.begin();

			List<HourTransaction> hourTransactions = new ArrayList<HourTransaction>();
			List<Long> subscriptionIds = new ArrayList<Long>();
			for (UnlockHoursRequest unlockHoursRequest : unlockHoursRequests) {
				Long id = unlockHoursRequest.getSubscriptionId();
				if (id == null || unlockHoursRequest.getBookDuration() == null) {
					logger.info("unlockHours:" + id + "bookDuration:" + unlockHoursRequest.getBookDuration());
					continue;
				}
				Subscription subscription = null;
				subscription = subscriptionDAO.getById(id, null, session);
				if (subscription == null) {
					logger.info("Subscription id: " + id + " doesn't exist");
					continue;
				}
				subscriptionIds.add(id);

				Long totalRemainingHours = subscription.getRemainingHours();
				Long totalLockedHours = subscription.getLockedHours();
				Long bookDuration = unlockHoursRequest.getBookDuration();

				List<SubscriptionDetails> sDetails = subscriptionDetailsManager
						.getSubscriptionDetailsBySubscriptionId(id, true, session);

				Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
					@Override
					public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
						return o2.getCreationTime().compareTo(o1.getCreationTime());
					}
				});

				List<Long> sdIds = new ArrayList<Long>();
				Long totalRefund = 0l;
				if (totalLockedHours >= bookDuration) {
					totalRemainingHours += bookDuration;
					totalLockedHours -= bookDuration;
					for (SubscriptionDetails i : sDetails) {
						Long remainingHours = i.getRemainingHours();
						Long lockedHours = i.getLockedHours();
						if (lockedHours >= bookDuration) {
							i.setRemainingHours(remainingHours + bookDuration);
							i.setLockedHours(lockedHours - bookDuration);

							HourTransaction hourTransaction = new HourTransaction(subscription.getId(), i.getId(),
									HourType.LOCKED, HourType.REMAINING, bookDuration,
									HourTransactionType.SESSION_EXPIRED, unlockHoursRequest.getSessionId().toString());
							// hourTransaction.setCallingUserId(unlockHoursRequest.getCallingUserId());
							hourTransactions.add(hourTransaction);
							break;
						} else {
							i.setRemainingHours(remainingHours + lockedHours);
							i.setLockedHours(0l);
							bookDuration -= lockedHours;

							HourTransaction hourTransaction = new HourTransaction(subscription.getId(), i.getId(),
									HourType.LOCKED, HourType.REMAINING, lockedHours,
									HourTransactionType.SESSION_EXPIRED, unlockHoursRequest.getSessionId().toString());
							// hourTransaction.setCallingUserId(unlockHoursRequest.getCallingUserId());
							hourTransactions.add(hourTransaction);
						}
					}

					if (subscription.getModel() != null && (subscription.getModel().equals(SessionModel.TRIAL)
							|| subscription.getModel().equals(SessionModel.IL)
							|| (subscription.getModel().equals(SessionModel.OTO)
									&& (subscription != null && subscription.getSubModel() != null
											&& subscription.getSubModel().equals(SubModel.LOOSE))))) {
						for (SubscriptionDetails i : sDetails) {
							i.setEnabled(false);
							totalRefund += ((i.getTotalHours() - i.getConsumedHours()) * i.getTeacherHourlyRate())
									/ DateTimeUtils.MILLIS_PER_HOUR;
							sdIds.add(i.getId());
						}
					}
				}
				subscriptionDetailsManager.updateAllSubscriptionDetails(sDetails, session);
				subscription.setLockedHours(totalLockedHours);
				subscription.setRemainingHours(totalRemainingHours);

				CancelSubscriptionResponse cancelSubscriptionResponse = null;
				if (subscription.getModel() != null
						&& (subscription.getModel().equals(SessionModel.TRIAL)
								|| subscription.getModel().equals(SessionModel.IL))
						|| (subscription.getModel().equals(SessionModel.OTO)
								&& (subscription != null && subscription.getSubModel() != null
										&& subscription.getSubModel().equals(SubModel.LOOSE)))) {
					subscription.setEnabled(false);
					subscription.setEndTime(System.currentTimeMillis());
					subscription.setEndReason(subscription.getModel() + "Session Expired");

					Refund refund = new Refund(id, totalRefund, subscription.getModel() + "Session Expired");
					refundManager.addOrUpdateRefund(refund, session);

					cancelSubscriptionResponse = new CancelSubscriptionResponse(totalRefund.intValue(),
							subscription.getTotalHours(),
							subscription.getRemainingHours() + subscription.getLockedHours(), 0l,
							subscription.getHourlyRate(), subscription.getModel() + "Session Expired", sdIds,
							subscription.getStudentId(), subscription.getTeacherId(), null, subscription.getTitle(),
							null, subscription.getSubModel(), subscription.getModel());
				}

				UnlockHoursResponse u = new UnlockHoursResponse(subscription.getId(), subscription.getLockedHours(),
						subscription.getRemainingHours(), cancelSubscriptionResponse);
				unlockHoursResponses.add(u);
				subscriptions.add(subscription);
			}
			subscriptionDAO.updateAll(subscriptions, session, callingUserId);
			hourTransactionManager.updateAllHourTransaction(hourTransactions, session);

			transaction.commit();
		} catch (Exception e) {
			logger.error("unlockHours Error: " + e.getMessage());
			throw e;
		} finally {
			session.close();
		}

		GetUnlockHoursResponse getUnlockHoursResponse = new GetUnlockHoursResponse(unlockHoursResponses);
		logger.info("unlockHours Response:" + getUnlockHoursResponse.toString());
		return getUnlockHoursResponse;
	}

	public SubscriptionResponse getSubscription(Long id, Boolean enabled, Session session) throws Throwable {
		logger.info("ENTRY: subscriptionId " + id + ", enabled: " + enabled);
		Subscription subscription = null;
		SessionFactory sessionFactory = null;
		Session session2 = null;
		if (session == null) {
			sessionFactory = sqlSessionfactory.getSessionFactory();
			session2 = sessionFactory.openSession();
			try {
				subscription = subscriptionDAO.getById(id, enabled, session2);
			} catch (Exception e) {
				logger.info("getSubscription Error: " + e.getMessage());
				throw new SQLException("getSubscription Error: " + e.getMessage());
			} finally {
				session2.close();
			}
		} else {
			subscription = subscriptionDAO.getById(id, enabled, session);
		}

		if (subscription == null) {
			logger.info("SubscriptionId: " + id + " doesn't exist");
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND, "SubscriptionId: " + id + " doesn't exist");
		}

		SubscriptionResponse subscriptionResponse = getSubscriptionResponse(subscription);
		logger.info("getSubscription Response:" + subscriptionResponse.toString());
		return subscriptionResponse;
	}

	public Subscription getSubscriptionBasicInfo(Long id) throws Throwable {

		Subscription subscription = null;
		SessionFactory sessionFactory = null;
		Session session2 = null;
		sessionFactory = sqlSessionfactory.getSessionFactory();
		session2 = sessionFactory.openSession();
		try {
			subscription = subscriptionDAO.getById(id, session2);
		} catch (Exception e) {
			logger.info("getSubscription Error: " + e.getMessage());
			throw new SQLException("getSubscription Error: " + e.getMessage());
		} finally {
			session2.close();
		}

		if (subscription == null) {
			logger.info("SubscriptionId: " + id + " doesn't exist");
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND, "SubscriptionId: " + id + " doesn't exist");
		}

		logger.info("EXIT:" + subscription);
		return subscription;
	}

	public List<Subscription> getSubscriptionBasicInfoList(List<Long> ids) throws Throwable {

		List<Subscription> subscriptions = null;
		SessionFactory sessionFactory = null;
		Session session2 = null;
		sessionFactory = sqlSessionfactory.getSessionFactory();
		session2 = sessionFactory.openSession();
		try {
			subscriptions = subscriptionDAO.getByIds(ids, session2);
		} catch (Exception e) {
			logger.info("getSubscriptionBasicInfoList Error: " + e.getMessage());
			throw new SQLException("getSubscriptionBasicInfoList Error: " + e.getMessage());
		} finally {
			session2.close();
		}

		logger.info("EXIT:" + subscriptions);
		return subscriptions;
	}

	public GetSubscriptionsResponse getSubscriptions(GetSubscriptionsReq req) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		List<Subscription> results = null;
		try {
			Criteria cr = session.createCriteria(Subscription.class);
			if (req.getActive() != null) {
				if (req.getActive().equals(true)) {
					cr.add(Restrictions.eq("state", SubscriptionState.ACTIVE));
				} else {
					cr.add(Restrictions.eq("state", SubscriptionState.ENDED));
				}
			}
			if (req.getTeacherId() != null) {
				cr.add(Restrictions.eq("teacherId", req.getTeacherId()));
			}
			if (req.getStudentId() != null) {
				cr.add(Restrictions.eq("studentId", req.getStudentId()));
			}
			if (req.getModel() != null) {
				cr.add(Restrictions.eq("model", req.getModel()));
			}
			if (req.getBeforeTime() != null) {
				cr.add(Restrictions.le("creationTime", req.getBeforeTime()));
			}
			if (req.getAfterTime() != null) {
				cr.add(Restrictions.ge("creationTime", req.getAfterTime()));
			}
			if (req.getSubModel() != null) {
				cr.add(Restrictions.eq("subModel", req.getSubModel()));
			} else {
				cr.add(Restrictions.or(Restrictions.eq("subModel", SubModel.DEFAULT),
						Restrictions.eq("subModel", SubModel.SESSIONS3CHALLENGE)));
			}

			if (req.getLastUpdatedTime() != null) {
				cr.add(Restrictions.ge("lastUpdatedTime", req.getLastUpdatedTime()));
			}

			cr.add(Restrictions.ne("state", SubscriptionState.INACTIVE));
			cr.addOrder(Order.desc("enabled"));
			cr.addOrder(Order.desc("creationTime"));

			Integer start = req.getStart();
			Integer size = req.getSize();
			if (start == null || start < 0) {
				start = 0;
			}
			if (size == null || size <= 0) {
				size = 20;
			}
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			logger.info("query for getSubscriptions " + cr);
			results = subscriptionDAO.runQuery(session, cr, Subscription.class);
		} catch (Exception e) {
			logger.error("getSubscriptions Error: " + e.getMessage());
			throw new SQLException("getSubscriptions Error: " + e.getMessage());
		} finally {
			session.close();
		}

		List<GetSubscriptionsRes> srList = new ArrayList<>();
		for (Subscription subscription : results) {
			GetSubscriptionsRes subscriptionResponse = getSubscriptionsResponse(subscription);
			if (subscriptionResponse != null) {
				srList.add(subscriptionResponse);
			}
		}
		Collections.sort(srList, new Comparator<GetSubscriptionsRes>() {
			@Override
			public int compare(GetSubscriptionsRes o1, GetSubscriptionsRes o2) {
				return Boolean.compare(o2.getIsActive(), o1.getIsActive());
			}
		});
		GetSubscriptionsResponse res = new GetSubscriptionsResponse(srList);
		return res;
	}

	public List<Subscription> getSubscriptionsBasicInfo(GetSubscriptionsReq req) throws Throwable {
		logger.info("Request received for SubscriptionsBasicInfo " + req);
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		List<Subscription> results = null;
		try {
			Criteria cr = session.createCriteria(Subscription.class);
			if (req.getActive() != null) {
				if (req.getActive().equals(true)) {
					cr.add(Restrictions.eq("state", SubscriptionState.ACTIVE));
				} else {
					cr.add(Restrictions.eq("state", SubscriptionState.ENDED));
				}
			}
			if (req.getTeacherId() != null) {
				cr.add(Restrictions.eq("teacherId", req.getTeacherId()));
			}
			if (req.getStudentId() != null) {
				cr.add(Restrictions.eq("studentId", req.getStudentId()));
			}
			if (req.getModel() != null) {
				cr.add(Restrictions.eq("model", req.getModel()));
			}
			if (req.getBeforeTime() != null) {
				cr.add(Restrictions.le("creationTime", req.getBeforeTime()));
			}
			if (req.getAfterTime() != null) {
				cr.add(Restrictions.ge("creationTime", req.getAfterTime()));
			}
			if (req.getSubModel() != null) {
				cr.add(Restrictions.eq("subModel", req.getSubModel()));
			} else {
				cr.add(Restrictions.or(Restrictions.eq("subModel", SubModel.DEFAULT),
						Restrictions.eq("subModel", SubModel.SESSIONS3CHALLENGE)));
			}

			if (req.getLastUpdatedTime() != null) {
				cr.add(Restrictions.ge("lastUpdatedTime", req.getLastUpdatedTime()));
			}

			cr.add(Restrictions.ne("state", SubscriptionState.INACTIVE));
			cr.addOrder(Order.desc("enabled"));
			cr.addOrder(Order.desc("creationTime"));

			Integer start = req.getStart();
			Integer size = req.getSize();
			if (start == null || start < 0) {
				start = 0;
			}
			if (size == null || size <= 0) {
				size = 20;
			}
			cr.setFirstResult(start);
			cr.setMaxResults(size);
			results = subscriptionDAO.runQuery(session, cr, Subscription.class);
		} catch (Exception e) {
			logger.error("getSubscriptionsBasicInfo Error: " + e.getMessage());
			throw new SQLException("getSubscriptionsBasicInfo Error: " + e.getMessage());
		} finally {
			session.close();
		}
		return results;
	}

	public SubscriptionResponse createSubscriptionFromRequest(SubscriptionRequest subscriptionRequest,
			Long hoursToSchedule, Session session) {
		Subscription subscription = null;
		SubscriptionDetails subscriptionDetails = null;
		logger.info("ENTRY : " + subscriptionRequest.toString() + " ,hoursToSchedule: " + hoursToSchedule);

		String teacherCouponId = null;
		Long teacherDiscountAmount = 0l;
		PaymentType paymentType = PaymentType.BULK;
		try {
			Gson gson = new Gson();
			JsonObject json = gson.fromJson(subscriptionRequest.getPaymentDetails(), JsonObject.class);
			if (json.get("teacherCouponId") != null) {
				teacherCouponId = json.get("teacherCouponId").getAsString();
			}
			if (json.get("teacherDiscountAmount") != null) {
				teacherDiscountAmount = json.get("teacherDiscountAmount").getAsLong();
			}
			if (json.get("paymentType") != null && PaymentType.valueOf(json.get("paymentType").getAsString()) != null) {
				paymentType = PaymentType.valueOf(json.get("paymentType").getAsString());
			}
		} catch (Exception e) {
			logger.info("Failed to get coupons " + e.toString());
		}

		Long subscriptionHrs = subscriptionRequest.getTotalHours();
		if (PaymentType.INSTALMENT.equals(paymentType)) {
			// TODO hoursToSchedule in the request to create subscription ?????
			subscriptionHrs = hoursToSchedule;
		}

		subscription = new Subscription(subscriptionRequest.getTeacherId(), subscriptionRequest.getStudentId(),
				subscriptionRequest.getPlanId(), subscriptionRequest.getModel(), subscriptionRequest.getSchedule(),
				subscriptionRequest.getOfferingId(), subscriptionHrs, 0l, 0l, subscriptionHrs,
				subscriptionRequest.getSubject(), subscriptionRequest.getTarget(), subscriptionRequest.getGrade(),
				subscriptionRequest.getHourlyRate(), subscriptionRequest.getTitle(), subscriptionRequest.getStartDate(),
				subscriptionRequest.getBoardId(), subscriptionRequest.getNoOfWeeks(), null, null, null,
				subscriptionRequest.getNote(), subscriptionRequest.getSubModel(), SubscriptionState.INACTIVE);
		// subscription.setCallingUserId(subscriptionRequest.getCallingUserId());

		Long callingUserId = subscriptionRequest.getCallingUserId() != null
				? Long.parseLong(subscriptionRequest.getCallingUserId())
				: null;

		subscription = subscriptionDAO.addOrUpdateSubscription(subscription, session, callingUserId);

		subscriptionDetails = new SubscriptionDetails(subscription.getId(), subscriptionHrs, 0l, 0l, subscriptionHrs,
				teacherCouponId, teacherDiscountAmount, subscriptionRequest.getHourlyRate());
		subscriptionDetails.setCallingUserId(subscriptionRequest.getCallingUserId());

		subscriptionDetails = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetails, true,
				session);

		HourTransaction hourTransaction = new HourTransaction(subscription.getId(), subscriptionDetails.getId(),
				HourType.DEFAULT, HourType.REMAINING, subscriptionDetails.getRemainingHours(),
				HourTransactionType.SUBSCRIPTION_BOOKED, subscriptionDetails.getId().toString());
		hourTransaction.setCallingUserId(subscriptionDetails.getCallingUserId());
		hourTransactionManager.addOrUpdateHourTransaction(hourTransaction, session);

		SubscriptionResponse response = new SubscriptionResponse();
		response.setSubscriptionId(subscription.getId());
		response.setSubscriptionDetailsId(subscriptionDetails.getId());
		logger.info("Response: " + response.toString());
		return response;
	}

	public void activateSubscription(Long id, Long lockedHours, Session session, Long callingUserId) throws VException {
		Subscription subscription = getSubscriptionById(id, null, session);
		if (subscription == null) {
			throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND, "SubscriptionId: " + id + "doesn't exist");
		}

		Long totalRemainingHours = subscription.getRemainingHours();

		if (totalRemainingHours >= lockedHours) {

			List<SubscriptionDetails> subscriptionDetailsList = subscriptionDetailsManager
					.getSubscriptionDetailsBySubscriptionId(id, null, session);
			if (subscriptionDetailsList == null || subscriptionDetailsList.isEmpty()) {
				throw new NotFoundException(ErrorCode.SUBSCRIPTION_NOT_FOUND,
						"SubscriptionDetails null or empty for Subscription: " + id);
			}

			subscription.setLockedHours(lockedHours);
			subscription.setRemainingHours(totalRemainingHours - lockedHours);
			subscription.setState(SubscriptionState.ACTIVE);

			for (SubscriptionDetails subscriptionDetails : subscriptionDetailsList) {

				Long remainingHours = subscriptionDetails.getRemainingHours();
				if (remainingHours >= lockedHours) {
					subscriptionDetails.setLockedHours(lockedHours);
					subscriptionDetails.setRemainingHours(remainingHours - lockedHours);

					HourTransaction hourTransaction = new HourTransaction(subscription.getId(),
							subscriptionDetails.getId(), HourType.REMAINING, HourType.LOCKED,
							subscriptionDetails.getLockedHours(), HourTransactionType.SUBSCRIPTION_BOOKED,
							subscriptionDetails.getId().toString());
					hourTransaction.setCallingUserId(subscriptionDetails.getCallingUserId());
					hourTransactionManager.addOrUpdateHourTransaction(hourTransaction, session);
					break;
				} else {
					subscriptionDetails.setLockedHours(remainingHours);
					subscriptionDetails.setRemainingHours(0l);
					lockedHours -= remainingHours;

					HourTransaction hourTransaction = new HourTransaction(subscription.getId(),
							subscriptionDetails.getId(), HourType.REMAINING, HourType.LOCKED,
							subscriptionDetails.getLockedHours(), HourTransactionType.SUBSCRIPTION_BOOKED,
							subscriptionDetails.getId().toString());
					hourTransaction.setCallingUserId(subscriptionDetails.getCallingUserId());
					hourTransactionManager.addOrUpdateHourTransaction(hourTransaction, session);
				}
			}

			subscriptionDAO.addOrUpdateSubscription(subscription, session, callingUserId);
			subscriptionDetailsManager.updateAllSubscriptionDetails(subscriptionDetailsList, session);

		} else {
			logger.info("Hours to be locked:" + lockedHours + "are less than " + totalRemainingHours
					+ "for Subscription: " + id);
			throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Hours to be locked:" + lockedHours
					+ "are less than " + totalRemainingHours + "for Subscription: " + id);
		}
	}

	Subscription addUpdates(Subscription oldObj, Subscription newObj) {

		if (newObj.getEnabled() != null) {
			oldObj.setEnabled(newObj.getEnabled());
		}
		if (newObj.getModel() != null) {
			oldObj.setModel(newObj.getModel());
		}
		if (newObj.getPlanId() != null) {
			oldObj.setPlanId(newObj.getPlanId());
		}
		if (newObj.getSchedule() != null) {
			oldObj.setSchedule(newObj.getSchedule());
		}
		if (newObj.getTeacherId() != null) {
			oldObj.setTeacherId(newObj.getTeacherId());
		}
		if (newObj.getStudentId() != null) {
			oldObj.setStudentId(newObj.getStudentId());
		}
		// if (newObj.getLastUpdatedby() != null) {
		// oldObj.setLastUpdatedby(newObj.getLastUpdatedby());
		// }
		return oldObj;
	}

	SubscriptionResponse getSubscriptionResponse(Subscription subscription) {

		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		List<SubscriptionDetails> sDetails = subscriptionDetailsManager
				.getSubscriptionDetailsBySubscriptionId(subscription.getId(), true, session);

		session.close();

		Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
			@Override
			public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
				return o2.getCreationTime().compareTo(o1.getCreationTime());
			}
		});

		String coupons = "";
		Long teacherDiscountAmount = 0l;
		for (SubscriptionDetails i : sDetails) {
			coupons += i.getTeacherCouponId() + ",";
			if (i.getTeacherDiscountAmount() != null) {
				teacherDiscountAmount += i.getTeacherDiscountAmount();
			}
		}
		if (coupons != "")
			coupons = coupons.substring(0, coupons.length() - 1);

		SubscriptionResponse subscriptionResponse = new SubscriptionResponse(subscription.getTeacherId(),
				subscription.getStudentId(), subscription.getPlanId(), subscription.getOfferingId(),
				subscription.getModel(), subscription.getSchedule(), subscription.getTotalHours(),
				subscription.getConsumedHours(), subscription.getLockedHours(), subscription.getRemainingHours(),
				coupons, teacherDiscountAmount, subscription.getHourlyRate(), subscription.getSubject(),
				subscription.getBoardId(), subscription.getTarget(), subscription.getGrade(), subscription.getId(),
				null, null, subscription.getEnabled(), subscription.getCreationTime(), subscription.getStartDate(),
				null, subscription.getNoOfWeeks(), subscription.getEndTime(), subscription.getEndedBy(),
				subscription.getEndReason(), subscription.getTitle(), subscription.getNote(),
				subscription.getSubModel());
		return subscriptionResponse;
	}

	GetSubscriptionsRes getSubscriptionsResponse(Subscription subscription) {

		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();
		Set<Long> userIds = new HashSet<>();
		if (subscription.getStudentId() != null) {
			userIds.add(subscription.getStudentId());
		}
		if (subscription.getTeacherId() != null) {
			userIds.add(subscription.getTeacherId());
		}
		Map<Long, UserBasicInfo> userInfosMap = fosUtils.getUserBasicInfosMapFromLongIds(userIds, true);
		UserBasicInfo teacher = userInfosMap.get(subscription.getTeacherId());
		UserBasicInfo student = userInfosMap.get(subscription.getStudentId());

		List<SubscriptionDetails> sDetails = subscriptionDetailsManager
				.getSubscriptionDetailsBySubscriptionId(subscription.getId(), true, session);

		session.close();

		Collections.sort(sDetails, new Comparator<SubscriptionDetails>() {
			@Override
			public int compare(SubscriptionDetails o1, SubscriptionDetails o2) {
				return o2.getCreationTime().compareTo(o1.getCreationTime());
			}
		});

		String coupons = "";
		Long teacherDiscountAmount = 0l;
		for (SubscriptionDetails i : sDetails) {
			coupons += i.getTeacherCouponId() + ",";
			if (i.getTeacherDiscountAmount() != null) {
				teacherDiscountAmount += i.getTeacherDiscountAmount();
			}
		}
		if (StringUtils.isNotEmpty(coupons)) {
			coupons = coupons.substring(0, coupons.length() - 1);
		}

		GetSubscriptionsRes getSubscriptionsRes = new GetSubscriptionsRes(teacher, student, subscription.getPlanId(),
				subscription.getOfferingId(), subscription.getModel(), subscription.getSchedule(),
				subscription.getTotalHours(), subscription.getConsumedHours(), subscription.getLockedHours(),
				subscription.getRemainingHours(), coupons, teacherDiscountAmount, subscription.getHourlyRate(),
				subscription.getSubject(), subscription.getTarget(), subscription.getGrade(), subscription.getId(),
				null, null, subscription.getEnabled(), subscription.getCreationTime(), subscription.getStartDate(),
				null, subscription.getNoOfWeeks(), subscription.getEndTime(), subscription.getEndedBy(),
				subscription.getEndReason(), subscription.getTitle(), subscription.getNote(),
				subscription.getSubModel());
		return getSubscriptionsRes;
	}

	SubscriptionResponse getSubscriptionResponse(Subscription subscription, SubscriptionDetails subscriptionDetails,
			SessionInfoResponse sessionInfoResponse) {

		SubscriptionResponse subscriptionResponse = new SubscriptionResponse(subscription.getTeacherId(),
				subscription.getStudentId(), subscription.getPlanId(), subscription.getOfferingId(),
				subscription.getModel(), subscription.getSchedule(), subscription.getTotalHours(),
				subscription.getConsumedHours(), subscription.getLockedHours(), subscription.getRemainingHours(),
				subscriptionDetails.getTeacherCouponId(), subscriptionDetails.getTeacherDiscountAmount(),
				subscriptionDetails.getTeacherHourlyRate(), subscription.getSubject(), subscription.getBoardId(),
				subscription.getTarget(), subscription.getGrade(), subscription.getId(), subscriptionDetails.getId(),
				null, subscription.getEnabled(), subscription.getCreationTime(), subscription.getStartDate(), null,
				subscription.getNoOfWeeks(), subscription.getEndTime(), subscription.getEndedBy(),
				subscription.getEndReason(), subscription.getTitle(), subscription.getNote(),
				subscription.getSubModel());
		return subscriptionResponse;
	}

	public MultipleSessionScheduleReq getSessionRequest(Subscription subscription, SubscriptionVO subscriptionVO) {
		logger.info("ENTRY ");
		SessionSchedule schedule = subscriptionVO.getSchedule();
		List<SessionSlot> slots = schedule.getSessionSlots();
		for (SessionSlot s : slots) {
			if (s.getTitle() == null) {
				s.setTitle(
						subscription.getGrade() + "th " + subscription.getTarget() + " " + subscription.getSubject());
			}
		}
		slots = addSessionsForAllWeeks(slots, schedule.getNoOfWeeks());
		MultipleSessionScheduleReq sessionRequest = new MultipleSessionScheduleReq(subscription.getSubject(), null,
				subscription.getTitle(), null, slots, SessionPayoutType.PAID, subscription.getId(),
				MultipleSessionScheduleReq.createStudentList(subscription.getStudentId()), subscription.getTeacherId(),
				subscription.fetchFeatureSource(), subscription.getModel(), subscription.fetchRequestSource(),
				(subscription.getCreatedBy() != null) ? Long.parseLong(subscription.getCreatedBy()) : null,
				subscription.getOfferingId(), null);

		// Avoid calendar blocking for IL
		if (SessionModel.IL.equals(sessionRequest.getSessionModel())) {
			sessionRequest.setNoCalendarCheck(Boolean.TRUE);
		}
		logger.info("EXIT " + sessionRequest);
		return sessionRequest;
	}

	public List<SessionSlot> addSessionsForAllWeeks(List<SessionSlot> sessionSlots, Long noOfWeeks) {
		List<SessionSlot> newslots = new ArrayList<SessionSlot>();
		for (Long i = 0l; i < noOfWeeks; ++i) {
			for (SessionSlot s : sessionSlots) {
				SessionSlot newSlot = new SessionSlot(s);
				newSlot.setStartTime(s.getStartTime() + i * DateTimeUtils.MILLIS_PER_WEEK);
				newSlot.setEndTime(s.getEndTime() + i * DateTimeUtils.MILLIS_PER_WEEK);
				newslots.add(newSlot);
			}
		}
		return newslots;
	}

	public List<SessionInfo> scheduleSession(MultipleSessionScheduleReq sessionRequest) throws VException {
		logger.info("ENTRY " + sessionRequest);
		List<SessionInfo> scheduledSessionsList = new ArrayList<>();
		Type sessionInfoType = new TypeToken<List<SessionInfo>>() {
		}.getType();
		if (sessionRequest.getSlots() != null) {
			String createMultipleSessionsUrl = schedulingEndPoint
					+ ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.create.multiple");
			ClientResponse resp = WebUtils.INSTANCE.doCall(createMultipleSessionsUrl, HttpMethod.POST,
					gson.toJson(sessionRequest));
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
			String jsonString = resp.getEntity(String.class);
			logger.info(jsonString);
			if (StringUtils.isNotEmpty(jsonString)) {
				List<SessionInfo> respList = new Gson().fromJson(jsonString, sessionInfoType);
				if (!CollectionUtils.isEmpty(respList)) {
					scheduledSessionsList.addAll(respList);
				}
			}
			logger.info("Session info Response count : " + Arrays.toString(scheduledSessionsList.toArray()));
		}
		logger.info("EXIT " + Arrays.toString(scheduledSessionsList.toArray()));
		return scheduledSessionsList;
	}

	private List<SessionInfo> cancelSubscriptionSessions(Long subscriptionId, Long userId, String reason)
			throws JSONException, VException {
		List<SessionInfo> slist = new ArrayList<>();
		Type SessionInfoType = new TypeToken<List<SessionInfo>>() {
		}.getType();
		SubscriptioncoursePlanCancelSessionsReq subscriptionCancelSessionsReq = new SubscriptioncoursePlanCancelSessionsReq(subscriptionId,
				reason, userId, null);

		ClientResponse fosResp = WebUtils.INSTANCE.doCall(
				schedulingEndPoint
						+ ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.cancel.subscription.sessions"),
				HttpMethod.POST, gson.toJson(subscriptionCancelSessionsReq));
		VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
		String resp = fosResp.getEntity(String.class);
		slist = new Gson().fromJson(resp, SessionInfoType);
		return slist;
	}

	private void cancelSubscriptionProposals(Long subscriptionId, Long userId) throws JSONException, VException {
		CancelSubscriptionProposalsReq req = new CancelSubscriptionProposalsReq();
		req.setSubscriptionId(subscriptionId);
		req.setCallingUserId(userId);

		ClientResponse fosResp = WebUtils.INSTANCE.doCall(
				schedulingEndPoint
						+ ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.cancel.subscription.proposals"),
				HttpMethod.POST, gson.toJson(req));
		VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
	}

	private SessionInfo cancelSession(Long sessionId, Long userId, String reason,Role callingUserRole) throws JSONException, VException {
		EndSessionReq endSessionReq = new EndSessionReq(sessionId, reason, userId, callingUserRole);
		ClientResponse fosResp = WebUtils.INSTANCE.doCall(
				schedulingEndPoint + ConfigUtils.INSTANCE.getStringValue("scheduling.api.session.cancel.session"),
				HttpMethod.POST, gson.toJson(endSessionReq));
		VExceptionFactory.INSTANCE.parseAndThrowFosException(fosResp);
		String resp = fosResp.getEntity(String.class);
		logger.info("Cancel session Response : " + resp);
		return gson.fromJson(resp, SessionInfo.class);
	}

	// Sessions should never be sent along with subscription info. Few changes
	// should be made in frontend
	// This will always return null. Sessions under a subscription will be
	// fetched from different location
	// public SessionInfoResponse getSessionInfoResponse(Subscription
	// subscription) {
	// SessionInfoResponse sessionInfoResponse = null;
	// if (subscription != null) {
	// // try {
	// // GetSubscriptionSessionsReq getSubscriptionSessionsReq = new
	// // GetSubscriptionSessionsReq(
	// // subscription.getId(), callingUserId, callingUserRole, start,
	// // size);
	// //
	// // sessionInfoResponse = gson.fromJson(res,
	// // SessionInfoResponse.class);
	// // JsonObject jsonObject = gson.fromJson(res, JsonObject.class);
	// // JsonObject student = jsonObject.get("student").getAsJsonObject();
	// // JsonObject teacher = jsonObject.get("teacher").getAsJsonObject();
	// // JsonArray sessionList =
	// // jsonObject.get("sessions").getAsJsonArray();
	// //
	// sessionInfoResponse.getStudent().setStudentInfo(gson.fromJson(student.get("info"),
	// // StudentInfo.class));
	// //
	// sessionInfoResponse.getTeacher().setTeacherInfo(gson.fromJson(teacher.get("info"),
	// // TeacherInfo.class));
	// // Type SessionListType = new TypeToken<ArrayList<SessionInfo>>() {
	// // }.getType();
	// // List<SessionInfo> slist = gson.fromJson(sessionList.toString(),
	// // SessionListType);
	// // sessionInfoResponse.setSessionList(slist);
	// // } catch (Exception e) {
	// // logger.error("getSessionInfoResponse failed" + e.getMessage());
	// // }
	// }
	// return sessionInfoResponse;
	// }

	// User details will be filled in platform.
	// TODO handle all the places where this is used
	SessionInfoResponse getUser(Long studentId, Long teacherId, String subject) {
		SessionInfoResponse sessionInfoResponse = null;
		GetSessionInfoRequest req = null;
		Long boardId = null;
		if (subject != null) {
			boardId = Long.parseLong(subject);
		}

		if (studentId == null && teacherId == null && boardId == null) {
			logger.info("getUser: Called with all params null");
			throw new IllegalArgumentException("getUser: getting Users failed");
		}

		//
		// req = new GetSessionInfoRequest(null, teacherId, studentId, boardId);
		// ClientResponse fosResp = WebUtils.INSTANCE.doCall(fosEndpoint +
		// "/subscription/sessioninfo", HttpMethod.POST,
		// new Gson().toJson(req));
		//
		// try {
		// Gson gson = new Gson();
		// String res = fosResp.getEntity(String.class);
		// if (fosResp.getStatus() != 200) {
		// throw new Exception("getUser: getting Users failed");
		// }
		// sessionInfoResponse = new Gson().fromJson(res,
		// SessionInfoResponse.class);
		// JsonObject jsonObject = gson.fromJson(res, JsonObject.class);
		// JsonObject student = jsonObject.get("student").getAsJsonObject();
		// JsonObject teacher = jsonObject.get("teacher").getAsJsonObject();
		// sessionInfoResponse.getStudent().setStudentInfo(gson.fromJson(student.get("info"),
		// StudentInfo.class));
		// sessionInfoResponse.getTeacher().setTeacherInfo(gson.fromJson(teacher.get("info"),
		// TeacherInfo.class));
		// } catch (Exception e) {
		// logger.info("getUser: Getting Users failed" + e.getMessage());
		// }

		return sessionInfoResponse;
	}

	void validateSubscription(SubscriptionVO subscriptionVO) throws VException {
		String error = "";
		if (subscriptionVO.getTeacherId() == null) {
			error += "TeacherId";
		}
		if (subscriptionVO.getStudentId() == null) {
			error += "StudentId";
		}
		if (subscriptionVO.getPlanId() == null) {
			error += "PlanId";
		}
		if (subscriptionVO.getSubject() == null) {
			error += "Subject";
		}
		// TODO will be removed once IL request sends proper grade and target
		if (subscriptionVO.getGradeAndTargetReq()) {
			if (subscriptionVO.getGrade() == null) {
				error += "Grade";
			}
			if (subscriptionVO.getTarget() == null) {
				error += "Target";
			}
		}
		if (subscriptionVO.getModel() == null) {
			error += "Model";
		}
		if (subscriptionVO.getTeacherHourlyRate() == null) {
			error += "TeacherHourlyRate";
		}
		if (subscriptionVO.getTotalHours() == null) {
			error += "TotalHours";
		}
		// TODO add check for FS and schedule and also noofweeks 0

		Long requestedHours = getRequestedHours(subscriptionVO.getSchedule());
		if (requestedHours > subscriptionVO.getTotalHours()) {
			logger.info("RequestedHours: " + requestedHours + " are more than the hours: "
					+ subscriptionVO.getTotalHours() + " which user paid for");
			throw new BadRequestException(ErrorCode.SUBSCRIPTION_REQUESTED_HOURS_INCORRECT,
					"createSubscription: RequestedHours: " + requestedHours + " are more than the hours: "
							+ subscriptionVO.getTotalHours() + " which user paid for");
		}
		if (!error.equals("")) {
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "createSubscription: " + error);
		}
	}

	// public List<Subscription> migrateSubscriptionState() {
	// SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
	// Session session = sessionFactory.openSession();
	// Transaction transaction = session.getTransaction();
	//
	// List<Subscription> results = null;
	// try {
	// transaction.begin();
	// Criteria cr = session.createCriteria(Subscription.class);
	// results = subscriptionDAO.runQuery(session, cr, Subscription.class);
	// if (results != null && !results.isEmpty()) {
	// for (Subscription subscription : results) {
	// if (Boolean.TRUE.equals(subscription.getEnabled())) {
	// logger.info("Enabled subscription - " + subscription.toString());
	// subscription.setState(SubscriptionState.ACTIVE);
	// } else {
	// logger.info("disabled subscription - " + subscription.toString());
	// subscription.setState(SubscriptionState.ENDED);
	// }
	//
	// subscriptionDAO.addOrUpdateSubscription(subscription, session);
	// }
	// logger.info("result count : " + results.size());
	// }
	//
	// transaction.commit();
	// return results;
	// } catch (Throwable e) {
	// if (transaction.getStatus().equals(TransactionStatus.ACTIVE)) {
	// logger.info("Rollback: " + e.getMessage());
	// session.getTransaction().rollback();
	// }
	// if (!(e instanceof VException)) {
	// logger.error("Rollback: " + e.getMessage());
	// }
	// throw e;
	// } finally {
	// session.close();
	// }
	// }

	public Long getSubscriptionsCount(GetSubscriptionsReq req) throws Throwable {
		SessionFactory sessionFactory = sqlSessionfactory.getSessionFactory();
		Session session = sessionFactory.openSession();

		List<Subscription> results = null;
		try {
			Criteria cr = session.createCriteria(Subscription.class);
			if (req.getActive() != null) {
				if (req.getActive().equals(true)) {
					cr.add(Restrictions.eq("state", SubscriptionState.ACTIVE));
				} else {
					cr.add(Restrictions.eq("state", SubscriptionState.ENDED));
				}
			}
			if (req.getTeacherId() != null) {
				cr.add(Restrictions.eq("teacherId", req.getTeacherId()));
			}
			if (req.getStudentId() != null) {
				cr.add(Restrictions.eq("studentId", req.getStudentId()));
			}
			if (req.getModel() != null) {
				cr.add(Restrictions.eq("model", req.getModel()));
			}
			if (req.getBeforeTime() != null) {
				cr.add(Restrictions.le("creationTime", req.getBeforeTime()));
			}
			if (req.getAfterTime() != null) {
				cr.add(Restrictions.ge("creationTime", req.getAfterTime()));
			}
			if (req.getSubModel() != null) {
				cr.add(Restrictions.eq("subModel", req.getSubModel()));
			} else {
				cr.add(Restrictions.or(Restrictions.eq("subModel", SubModel.DEFAULT),
						Restrictions.eq("subModel", SubModel.SESSIONS3CHALLENGE)));
			}

			if (req.getLastUpdatedTime() != null) {
				cr.add(Restrictions.ge("lastUpdatedTime", req.getLastUpdatedTime()));
			}

			cr.add(Restrictions.ne("state", SubscriptionState.INACTIVE));
			cr.addOrder(Order.desc("enabled"));
			cr.addOrder(Order.desc("creationTime"));

			Integer start = req.getStart();
			Integer size = req.getSize();
			// if (start == null || start < 0) {
			// start = 0;
			// }
			// if (size == null || size <= 0) {
			// size = 20;
			// }
			// cr.setFirstResult(start);
			// cr.setMaxResults(size);

			cr.setProjection(Projections.rowCount());
			Long count = (Long) cr.uniqueResult();
			// results = subscriptionDAO.runQuery(session, cr,
			// Subscription.class);
			return count;
		} catch (Exception e) {
			logger.error("getSubscriptions Error: " + e.getMessage());
			throw new SQLException("getSubscriptions Error: " + e.getMessage());
		} finally {
			session.close();
		}

		// List<GetSubscriptionsRes> srList = new
		// ArrayList<GetSubscriptionsRes>();
		// for (Subscription subscription : results) {
		// GetSubscriptionsRes subscriptionResponse =
		// getSubscriptionsResponse(subscription);
		// srList.add(subscriptionResponse);
		// }
		// Collections.sort(srList, new Comparator<GetSubscriptionsRes>() {
		// public int compare(GetSubscriptionsRes o1, GetSubscriptionsRes o2) {
		// return Boolean.compare(o2.getIsActive(), o1.getIsActive());
		// }
		// });
		// GetSubscriptionsResponse res = new GetSubscriptionsResponse(srList);
		// return res;
	}

	public CommunicationDataRes communicateInBulk(CommunicationDataReq req) throws VException{
		req.verify();
		logger.info("##################START - communicateInBulk#######################");

		if(req.getCommunicationKind()==null){
			throw new BadRequestException(ErrorCode.MANDATORY_VALUE_NOT_FOUND,"You need to specify the type of communication");
		}

		logger.info("Communication kind is defined as "+req.getCommunicationKind());

		if(CommunicationKind.SMS.equals(req.getCommunicationKind())){
			if(req.getBody().length()>160){
				throw new BadRequestException(ErrorCode.MESSAGE_SIZE_EXCEEDED,"Message size of SMS couldn't exceed 160 characters");
			}
			logger.info("Message size is within the limit");
		}

		if(CommunicationKind.NOTICE_BOARD.equals(req.getCommunicationKind())){
			if(req.getBody().length()>240){
				throw new BadRequestException(ErrorCode.MESSAGE_SIZE_EXCEEDED,"Message size of SMS couldn't exceed 160 characters");
			}
			if(req.getExpiryTime()==0){
                throw new BadRequestException(ErrorCode.MESSAGE_SIZE_EXCEEDED,"Expiry time in mandatory in case of notice board");
            }
			logger.info("Notice board size is within the limit");
		}

		List<String> allUserIds=new ArrayList<>();

		if(req.getUserIds()!=null) {
			allUserIds.addAll(req.getUserIds());
			logger.info("userIds are in the data");
		}

		if(req.getBatchIds()!=null){
			getAllUsersEnrolledInBatches(req,allUserIds);
			logger.info("batchIds are there in the data");
		}

		if(req.getOtmBundleIds()!=null){
			getAllUsersEnrolledInOTMBundles(req,allUserIds);
			logger.info("otmBundleIds are there in data");
		}

		if(req.getAioPackageIds()!=null){
			getAllUsersEnrolledInAIOPackages(req,allUserIds);
			logger.info("aioPackageIds are there in data");
		}

		if(req.getEmailIds()!=null){
			logger.info("standalone email ids are present.");
		}

		// refactoring needed
		if(allUserIds!=null || ArrayUtils.isNotEmpty(req.getEmailIds())){
			List<UserBasicInfo> users=ArrayUtils.isNotEmpty(allUserIds)?getUserDetails(allUserIds):new ArrayList<>();
			logger.info("All user ids given are valid.");
			logger.info("Updating db for the data of manual notification before communication");
			Long dateTime=System.currentTimeMillis();
			if(req.getScheduledTime()==0){
				req.setScheduledTime(dateTime);
			}
			if(CommunicationKind.NOTICE_BOARD.equals(req.getCommunicationKind())){
				if(req.getExpiryTime()<=req.getScheduledTime()){
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Schedule time can't be greater or equal to the expiry time");
				}
				if(req.getExpiryTime()!=0 && req.getExpiryTime()<System.currentTimeMillis()){
					throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"You can't expire a notice in past");
				}
			}
			ManualNotificationRequest response=updateManualNotificationDB(dateTime,req,allUserIds,null);
            if((response!=null && response.getScheduledTime()==dateTime) || CommunicationKind.NOTICE_BOARD.equals(req.getCommunicationKind())) {
				List<InternetAddress> emailTo = new ArrayList<>();
				ArrayList<String> toSMS = new ArrayList<>();
				StringBuilder error = new StringBuilder();
				if(!CommunicationKind.NOTICE_BOARD.equals(req.getCommunicationKind())){
                    if(ArrayUtils.isNotEmpty(users)){
						users.forEach(user -> {
							try {
								InternetAddress internetAddress = new InternetAddress();
								internetAddress.setAddress(user.getEmail());
								internetAddress.setPersonal(user.getFullName());
								emailTo.add(internetAddress);
								toSMS.add("+" + user.getPhoneCode() + user.getContactNumber());
								logger.info("For user " + user.getFullName() +
										" email address added is " + user.getEmail() +
										" phone code is " + "+" + user.getPhoneCode() + user.getContactNumber());
							} catch (UnsupportedEncodingException ex) {
								error.append("Error occcured for userId " + user.getUserId());
							} finally {
								if (error != null || error.length() > 0) {
									logger.info(error);
								} else {
									logger.info("No error is found.");
								}
							}
						});
					}
                    if(ArrayUtils.isNotEmpty(req.getEmailIds())){
                    	List<String> emailIds=req.getEmailIds();
                    	emailIds.forEach(emailId->{
							try {
								emailTo.add(new InternetAddress(emailId));
							} catch (AddressException e) {
								logger.error("Error in storing standalone email address");
							}
						});
					}
                }
				String manualNotificationId = response.getId();
				if (CommunicationKind.EMAIL.equals(req.getCommunicationKind())) {
				    logger.info("Sending Email");
					sendEmail(req, emailTo, manualNotificationId);
				} else if (CommunicationKind.SMS.equals(req.getCommunicationKind())) {
                    logger.info("Sending SMS");
					sendSMS(req, toSMS, manualNotificationId);
				} else if(CommunicationKind.NOTICE_BOARD.equals(req.getCommunicationKind())){
                    logger.info("Sending data to Notice Board");
					req.setAllUsers(allUserIds);
					req.setManualNotificationId(manualNotificationId);
					populateNoticeBoardDB(req);
				}
                updateManualNotificationDB(dateTime, req, allUserIds, response.getId());

			}
			logger.info("##############END - communicateInBulk#################");
			CommunicationDataRes res=new CommunicationDataRes(req,users);

			return res;
		}
		return null;
	}

	private void getAllUsersEnrolledInBatches(CommunicationDataReq req,List<String> allUserIds) throws BadRequestException {
		List<String> batchUserIds=new ArrayList<>();
		List<Batch> batches=batchDAO.getBatches(req.getBatchIds(),new ArrayList<>());
		List<String> fetchedValidBatchIds=new ArrayList<>();
		for(Batch batch:batches){
			fetchedValidBatchIds.add(batch.getId());
		}
		if(!fetchedValidBatchIds.containsAll(req.getBatchIds())){
			throw new BadRequestException(ErrorCode.BATCH_NOT_FOUND,"Some batches in the list are invalid");
		}

		List<Enrollment> enrollments=enrollmentDAO.getActiveAndInactiveStudentEnrollmentsByBatchIds(req.getBatchIds());
		for(Enrollment enrollment:enrollments){
			batchUserIds.add(enrollment.getUserId());
		}
		allUserIds.addAll(batchUserIds);
	}

	private void getAllUsersEnrolledInOTMBundles(CommunicationDataReq req,List<String> allUserIds) throws BadRequestException {

		List<String> otmBundleUserIds = new ArrayList<>();
		List<OTMBundle> otmBundles = otfBundleDAO.getOTMBundlesByIds( req.getOtmBundleIds() );
		List<String> fetchedValidOTMBundleId = new ArrayList<>();
		List<String> batchIds = new ArrayList<>();
		List<OTMBundleEntityInfo> entities = new ArrayList<>();
		for (OTMBundle otmBundle : otmBundles) {
			fetchedValidOTMBundleId.add( otmBundle.getId() );
			entities.addAll( otmBundle.getEntities() );
		}
		if (!fetchedValidOTMBundleId.containsAll( req.getOtmBundleIds() )) {
			throw new BadRequestException( ErrorCode.OTM_BUNDLE_NOT_FOUND, "Some of the otm bundle ids are invalid" );
		}
		for(OTMBundleEntityInfo entity:entities){
			if(EntityType.OTF.equals(entity.getEntityType())){
				batchIds.add(entity.getEntityId());
			}
		}

		List<Enrollment> enrollments=enrollmentDAO.getActiveAndInactiveStudentEnrollmentsByBatchIds(batchIds);
		Map<String,Boolean> otmBundleMap=new HashMap<>();
		fetchedValidOTMBundleId.forEach(id->otmBundleMap.put(id,true));
		for(Enrollment enrollment:enrollments){
			if(otmBundleMap.containsKey(enrollment.getEntityId()) && EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())){
				otmBundleUserIds.add(enrollment.getUserId());
			}
		}
		allUserIds.addAll(otmBundleUserIds);
	}

	private void getAllUsersEnrolledInAIOPackages(CommunicationDataReq req,List<String> allUserIds) throws BadRequestException {
		List<String> aioPackageUserIds=new ArrayList<>();
		List<Bundle> bundles=bundleDAO.getBundlesByIds(req.getAioPackageIds());
		List<String> fetchValidBundleIds=new ArrayList<>();
		for(Bundle bundle:bundles){
			fetchValidBundleIds.add(bundle.getId());
		}
		if(!fetchValidBundleIds.containsAll(req.getAioPackageIds())){

			throw new BadRequestException(ErrorCode.BUNDLE_NOT_FOUND,"Some of the bundle/aio ids are invalid");
		}
		List<BundleEnrolment> bundleEnrolments=bundleEnrolmentDAO.getActiveBundleEnrolmentsByBundleIds(fetchValidBundleIds);
		for(BundleEnrolment bundleEnrolment:bundleEnrolments){
			aioPackageUserIds.add(bundleEnrolment.getUserId());
		}
		allUserIds.addAll(aioPackageUserIds);
	}

	private List<UserBasicInfo> getUserDetails(List<String> userIds) throws VException {
		Set<String> uniqueUserIds=new HashSet<>();
		userIds.forEach(userId->uniqueUserIds.add(userId));
		List<String> uniqueUsers=uniqueUserIds.stream().collect(Collectors.toList());
		List<UserBasicInfo> users=fosUtils.getUserBasicInfos(uniqueUsers,true);
		List<UserBasicInfo> usersToRemove=new ArrayList<>();
		users.forEach(user->{
			if(user==null){
				usersToRemove.add(user);
			}
		});
		users.removeAll(usersToRemove);
		logger.info("No of users removed is "+usersToRemove.size());
		logger.info("Unique users fetched from the user basic info is - "+gson.toJson(users));
		logger.info("The unique users id is "+uniqueUsers);
		logger.info("Size retrieved is "+users.size()+" and our original data size is "+uniqueUsers.size());
		if(ArrayUtils.isEmpty(users) || uniqueUsers.size()!=users.size()){
			Set<String> retrievedUniqueUserIds=new HashSet<>();
			users.forEach(user->retrievedUniqueUserIds.add(Long.toString(user.getUserId())));
			logger.info("The users which are not valid are " );
			uniqueUserIds.forEach(userId->{
				if(!retrievedUniqueUserIds.contains(userId)){
					logger.info(userId);
				}
			});

			throw new VException(ErrorCode.INVALID_USER_ID,"There are some userIds which are invalid");
		}
		userIds.clear();
		userIds.addAll(uniqueUserIds);
		return users;
	}
	private void sendEmail(CommunicationDataReq req, List<InternetAddress> emailTo,String manualNotificationId){
		EmailRequest emailRequest=new EmailRequest();
		emailRequest.setBody(req.getBody().trim());
		emailRequest.setSubject(req.getSubject());
		emailRequest.setType(CommunicationType.MANUAL_NOTIFICATION);
		emailRequest.setClickTrackersEnabled(true);
		emailRequest.setCallingUserId(req.getCallingUserId());
		emailRequest.setManualNotificationId(manualNotificationId);
		emailRequest.setBodyScopes(new HashMap<>());
		List<List<InternetAddress>> emailAddressGroup = Lists.partition(emailTo,1000);
		emailAddressGroup.forEach(emailToBatch ->{
			emailRequest.setTo(emailToBatch);
			awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_BULK_EMAIL, gson.toJson(emailRequest));
		});
		logger.info("Hopefully email is sent successfully");
	}

	public void sendSMS(CommunicationDataReq req,ArrayList<String> toSMS,String manualNotificationId){
		BulkTextSMSRequest bulkTextSMSRequest=new BulkTextSMSRequest();
		bulkTextSMSRequest.setBody(req.getBody());
		bulkTextSMSRequest.setType(CommunicationType.MANUAL_NOTIFICATION);
		bulkTextSMSRequest.setCallingUserId(req.getCallingUserId());
		bulkTextSMSRequest.setCallingUserRole(req.getCallingUserRole());
		bulkTextSMSRequest.setManualNotificationId(manualNotificationId);
		List<List<String>> smsGroups=Lists.partition(toSMS,99);
		smsGroups.forEach(smsGroup->{
			bulkTextSMSRequest.setTo(new ArrayList<>(smsGroup));
			Map<String, Object> payload = new HashMap<>();
			payload.put("bulkTextSMSRequest",bulkTextSMSRequest);
			awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_BULK_SMS, gson.toJson(payload));
		});
		logger.info("Hopefully SMS is sent successfully");
	}

	public ManualNotificationRequest updateManualNotificationDB(Long dateTime,CommunicationDataReq req,List<String> allUserIds,String id){
		ManualNotificationRequest notificationRequest=mapper.map(req,ManualNotificationRequest.class);
		logger.info("The calling user id for updating manual notification db is "+req.getCallingUserId());
		notificationRequest.setCallingUserId(req.getCallingUserId());
		notificationRequest.setCallingUserRole(req.getCallingUserRole());
		notificationRequest.setAllUserIds(allUserIds);
		if(id!=null){
			logger.info("The id is not null so going for updating of database");
			notificationRequest.setId(id);
			if(CommunicationKind.NOTICE_BOARD.equals(req.getCommunicationKind())){
				logger.info("Going for updating notice board data");
				if(req.getScheduledTime()==dateTime){
					notificationRequest.setScheduledTime(req.getScheduledTime());
				}
				logger.info("Setting notification status scheduled no matter what");
				notificationRequest.setStatus(NotificationStatus.SCHEDULED);
            }else{
				logger.info("In case of SMS or Email mark it sent.");
				notificationRequest.setScheduledTime(req.getScheduledTime());
				notificationRequest.setStatus(NotificationStatus.SENT);
            }
		}else{
			notificationRequest.setScheduledTime(req.getScheduledTime());
			if(req.getScheduledTime()==dateTime){
				notificationRequest.setStatus(NotificationStatus.FAILED);
			}else {
				notificationRequest.setStatus(NotificationStatus.SCHEDULED);
			}
		}
		if(req.getExpiryTime()!=0){
			notificationRequest.setExpiryTime(req.getExpiryTime());
		}
		ClientResponse resp = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT + "/manualnotif/insertData", HttpMethod.POST,
				new Gson().toJson(notificationRequest));
		try {
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		} catch (VException e) {
			e.printStackTrace();
		}
		String jsonString = resp.getEntity(String.class);
		Type manualNotificationType = new TypeToken<ManualNotificationRequest>() {
		}.getType();
		ManualNotificationRequest notification = new Gson().fromJson(jsonString, manualNotificationType);

		logger.info("Response from notification-centre " + jsonString);
		return notification;

	}

	private void populateNoticeBoardDB(CommunicationDataReq req) {
		ClientResponse resp = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT + "/noticeBoard/populateNoticeBoardData", HttpMethod.POST,
				new Gson().toJson(req));
		try {
			VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		} catch (VException e) {
			e.printStackTrace();
		}
		String jsonString = resp.getEntity(String.class);
		Type noticeBoardResType = new TypeToken<List<NoticeBoardRes>>() {
		}.getType();
		List<NoticeBoardRes> noticeBoardRes=new Gson().fromJson(jsonString,noticeBoardResType);
		logger.info("Response from notification-centre " + jsonString);
	}

        public PlatformBasicResponse acceptLoanAgreement(AcceptLoanAgreementReq req) throws VException{
            PlatformBasicResponse res = new PlatformBasicResponse();           
            try {
                ClientResponse cResp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/getUserById?userId=" + req.getUserId(),
                    HttpMethod.GET, null, true);
                String jsonResp = cResp.getEntity(String.class);
                User user = gson.fromJson(jsonResp, User.class);
                
                if(user == null) {
                    res.setSuccess(Boolean.FALSE);
                    res.setResponse("Invalid Link");
                    return res;
                }
                
                LoanAgreement agreement = null;
                
                BundleEnrolment bundleEnrolment = bundleEnrolmentDAO.getBundleEnrolment(req.getUserId().toString(), req.getEntityId());
                if(bundleEnrolment != null) {
                    agreement = loanAgreementDAO.getLoanAgreementDetails(req.getUserId(), req.getEntityId());
                    if(agreement == null) {
                        agreement = new LoanAgreement();
                        agreement.setEntityId(req.getEntityId());
                        agreement.setEntityType(EntityType.BUNDLE);
                        agreement.setUserId(req.getUserId());
                        loanAgreementDAO.save(agreement, null);

                        TextSMSRequest textSMSRequest = new TextSMSRequest(user.getContactNumber(), user.getPhoneCode(), null,
                                CommunicationType.LOAN_AGREEMENT_ACCEPT_SMS, user.getRole());      

                        ClientResponse resp = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT + "/SMS/sendSMS", HttpMethod.POST,
                                    new Gson().toJson(textSMSRequest));
                        VExceptionFactory.INSTANCE.parseAndThrowException(resp);   
                        res.setResponse("Thanks for giving your confirmation! You will receive a message on your Vedantu registered number, confirming the same.");
                    } else {
                        res.setResponse("You have already accepted the loan agreement.");   
                    }                 
                    res.setSuccess(Boolean.TRUE);                     
                } else {
                    logger.error("Bundle not found for loan agreement request", req);
                    res.setSuccess(Boolean.FALSE);
                    res.setResponse("Invalid Link");
                }
            } catch (Throwable t) {
                logger.error("Error in acceptLoanAgreement", t);
                res.setSuccess(Boolean.FALSE);
                res.setResponse("Error while accept load agreement");
            }
            return res;
        }

	public CommunicationDataRes communicateInBulkNew(CommunicationDataReq req) throws VException {
		logger.info("Verifying the CommunicationDataReq : " + req);
		req.verify();

		int checkMultiInputFieldsCount = 0;

		SQSMessageType messageType = null;

		//check UserIds are valid or not.
		if (ArrayUtils.isNotEmpty(req.getUserIds())) {
			logger.info("requested UserIds : " + req.getUserIds());
			req.setUserIds(checkGivenUserIdsValidOrNot(req.getUserIds()));
			checkMultiInputFieldsCount += 1;
			messageType = SQSMessageType.BULK_USERIDS_REQ;
		}


		//bachIds are valid or not
		if (ArrayUtils.isNotEmpty(req.getBatchIds())) {
			logger.info("requested batchIds : " + req.getBatchIds());
			if (checkMultiInputFieldsCount > 0) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Don't Use multiple input fields, you should use any one of these User Ids, Email Ids, Batch Ids, Bundle of batches or AIO package ID(s)");
			}
			req.setBatchIds(checkGivenBathcIdsValidOrNot(req.getBatchIds()));
			checkMultiInputFieldsCount += 1;
			messageType = SQSMessageType.BULK_BATCHIDS_REQ;
		}


		//check otmBundleIds are valid or not.
		if (ArrayUtils.isNotEmpty(req.getOtmBundleIds())) {
			logger.info("requesting otm Bundle Ids : " + req.getOtmBundleIds());
			if (checkMultiInputFieldsCount > 0) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Don't Use multiple input fields, you should use any one of these User Ids, Email Ids, Batch Ids, Bundle of batches or AIO package ID(s)");
			}
			req.setOtmBundleIds(checkGivenOtmBundledsValidOrNot(req.getOtmBundleIds()));
			checkMultiInputFieldsCount += 1;
			messageType = SQSMessageType.BULK_OTM_BUNDLEIDS_REQ;
		}

		//check aioPackageIds are valid or not.
		if (ArrayUtils.isNotEmpty(req.getAioPackageIds())) {
			logger.info("requesting aio package Ids : " + req.getAioPackageIds());
			if (checkMultiInputFieldsCount > 0) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Don't Use multiple input fields, you should use any one of these User Ids, Email Ids, Batch Ids, Bundle of batches or AIO package ID(s)");
			}
			req.setAioPackageIds(checkGivenAioPackageIdsValidOrNot(req.getAioPackageIds()));
			checkMultiInputFieldsCount += 1;
			messageType = SQSMessageType.BULK_AIO_PACKAGEIDS_REQ;
		}

		//check emailIds are valid or not
		if (ArrayUtils.isNotEmpty(req.getEmailIds())) {
			logger.info("requesting email Ids : " + req.getEmailIds());
			if (checkMultiInputFieldsCount > 0) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Don't Use multiple input fields, you should use any one of these User Ids, Email Ids, Batch Ids, Bundle of batches or AIO package ID(s)");
			}
			req.setEmailIds(checkGivenEmailIdsValiedOrNot(req.getEmailIds()));
			messageType = SQSMessageType.BULK_EMAILIDS_REQ;
		}

		awsSQSManager.sendToSQS(SQSQueue.COMMUNICATE_IN_BULK_QUEUE, messageType, gson.toJson(req));

		return new CommunicationDataRes(req,null);
	}

	public List<String> checkGivenUserIdsValidOrNot(List<String> userIds) throws VException {
		Integer batchWiseFetchingSize = 500;
		Set<String> uniqueUserIdsSet = new HashSet<>();
		userIds.forEach(userId -> uniqueUserIdsSet.add(userId));
		List<String> uniqueUserIds = uniqueUserIdsSet.stream().collect(Collectors.toList());
		List<String> fetchingIds = new ArrayList<>();
		for (int count = 0; count < uniqueUserIds.size(); count++) {
			fetchingIds.add(uniqueUserIds.get(count));
			if ((count != 0 && count % batchWiseFetchingSize == 0) || count == (uniqueUserIds.size() - 1)) {
				if (!checkGivenUserIdsValidOrNotByCallingUserSubsystem(fetchingIds)) {
					throw new VException(ErrorCode.INVALID_USER_ID, "There are some userIds which are invalid");
				}
				fetchingIds = new ArrayList<>();
			}
		}
		logger.info("userIds : " + uniqueUserIds);
		return uniqueUserIds;
	}

	public Boolean checkGivenUserIdsValidOrNotByCallingUserSubsystem(List<String> userIds) throws VException {
		ClientResponse resp = WebUtils.INSTANCE.doCall(USER_ENDPOINT + "/checkGivenUserIdsValidOrNot", HttpMethod.POST, new Gson().toJson(userIds), Boolean.TRUE);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String jsonString = resp.getEntity(String.class);
		Boolean responce = new Gson().fromJson(jsonString, Boolean.class);
		logger.info("responce : " + responce);
		return responce;
	}

	public List<String> checkGivenBathcIdsValidOrNot(List<String> batchIds) throws VException {
		Integer batchWiseFetchingSize = 500;
		Set<String> uniqueBatchIdsSet = new HashSet<>();
		batchIds.forEach(userId -> uniqueBatchIdsSet.add(userId));
		List<String> uniqueBatchIds = uniqueBatchIdsSet.stream().collect(Collectors.toList());
		List<String> fetchingIds = new ArrayList<>();
		for (int count = 0; count < uniqueBatchIds.size(); count++) {
			fetchingIds.add(uniqueBatchIds.get(count));
			if ((count != 0 && count % batchWiseFetchingSize == 0) || count == (uniqueBatchIds.size() - 1)) {
				if (!checkGivenBathcIdsValidOrNotByCallingBatchDAO(fetchingIds)) {
					throw new VException(ErrorCode.NOT_FOUND_ERROR, "There are some batchIds which are invalid");
				}
				fetchingIds = new ArrayList<>();
			}
		}
		logger.info("batchIds : " + uniqueBatchIds);
		return uniqueBatchIds;
	}

	public Boolean checkGivenBathcIdsValidOrNotByCallingBatchDAO(List<String> batchIds) {
		List<Batch> batches = batchDAO.getBatches(batchIds, Arrays.asList(Batch.Constants.ID));
		if (batches != null) {
			if (batches.size() == batchIds.size()) {
				return true;
			} else {
				List<String> fetchedBatchIds = new ArrayList<>();
				batches.forEach(batch -> fetchedBatchIds.add(batch.getId()));
				batchIds.forEach(batchId -> {
					if (!fetchedBatchIds.contains(batchId)) {
						logger.info("batchId does not exist in Batch : " + batchId);
					}
				});
			}
		}
		return false;
	}

	public List<String> checkGivenOtmBundledsValidOrNot(List<String> otmBundleIds) throws VException {
		Integer batchWiseFetchingSize = 500;
		Set<String> uniqueOtmBundleIdsSet = new HashSet<>();
		otmBundleIds.forEach(otmBundleId -> uniqueOtmBundleIdsSet.add(otmBundleId));
		List<String> uniqueOtmBundleIds = uniqueOtmBundleIdsSet.stream().collect(Collectors.toList());
		List<String> fetchingIds = new ArrayList<>();
		for (int count = 0; count < uniqueOtmBundleIds.size(); count++) {
			fetchingIds.add(uniqueOtmBundleIds.get(count));
			if ((count != 0 && count % batchWiseFetchingSize == 0) || count == (uniqueOtmBundleIds.size() - 1)) {
				if (!checkGivenOtmBundledsValidOrNotByCallingOtmBundleDAO(fetchingIds)) {
					throw new VException(ErrorCode.NOT_FOUND_ERROR, "There are some otm Bundle Ids which are invalid");
				}
				fetchingIds = new ArrayList<>();
			}
		}
		logger.info("otmBundleIds : " + uniqueOtmBundleIds);
		return uniqueOtmBundleIds;
	}

	public Boolean checkGivenOtmBundledsValidOrNotByCallingOtmBundleDAO(List<String> otmBundleIdsIds) {
		List<OTMBundle> otmBundles = otfBundleDAO.getOTMBundlesByIds(otmBundleIdsIds, Arrays.asList(OTMBundle.Constants.ID));
		if (otmBundles != null) {
			if (otmBundles.size() == otmBundleIdsIds.size()) {
				return true;
			} else {
				List<String> fetchedOtmBundleIds = new ArrayList<>();
				otmBundles.forEach(otmBundle -> fetchedOtmBundleIds.add(otmBundle.getId()));
				otmBundleIdsIds.forEach(otmBundleId -> {
					if (!fetchedOtmBundleIds.contains(otmBundleId)) {
						logger.info("otm bundle does not exist in Batch : " + otmBundleId);
					}
				});
			}
		}
		return false;
	}

	public List<String> checkGivenAioPackageIdsValidOrNot(List<String> aioPackageIds) throws VException {
		Integer batchWiseFetchingSize = 500;
		Set<String> uniqueAioPackageIdsSet = new HashSet<>();
		aioPackageIds.forEach(aioPackageId -> uniqueAioPackageIdsSet.add(aioPackageId));
		List<String> uniqueAioPackageIds = uniqueAioPackageIdsSet.stream().collect(Collectors.toList());
		List<String> fetchingIds = new ArrayList<>();
		for (int count = 0; count < uniqueAioPackageIds.size(); count++) {
			fetchingIds.add(uniqueAioPackageIds.get(count));
			if ((count != 0 && count % batchWiseFetchingSize == 0) || count == (uniqueAioPackageIds.size() - 1)) {
				if (!checkGivenAioPackageIdsValidOrNotByCallingOtmBundleDAO(fetchingIds)) {
					throw new VException(ErrorCode.NOT_FOUND_ERROR, "There are some AIO Ids which are invalid");
				}
				fetchingIds = new ArrayList<>();
			}
		}
		logger.info("aioPackageIds : " + uniqueAioPackageIds);
		return uniqueAioPackageIds;
	}

	public Boolean checkGivenAioPackageIdsValidOrNotByCallingOtmBundleDAO(List<String> aioPackageIds) {
		List<Bundle> aioPackages = bundleDAO.getBundlesByIds(aioPackageIds, Arrays.asList(Bundle.Constants.ID));
		if (aioPackages != null) {
			if (aioPackages.size() == aioPackageIds.size()) {
				return true;
			} else {
				List<String> fetchedAioPackageIds = new ArrayList<>();
				aioPackages.forEach(aioPackage -> fetchedAioPackageIds.add(aioPackage.getId()));
				aioPackageIds.forEach(aioPackageId -> {
					if (!fetchedAioPackageIds.contains(aioPackageId)) {
						logger.info("bundle does not exist in Batch : " + aioPackageIds);
					}
				});
			}
		}
		return false;
	}

	public List<String> checkGivenEmailIdsValiedOrNot(List<String> emailIds) throws VException {
		Set<String> uniqueEmailIdsSet = new HashSet<>();
		emailIds.forEach(aioPackageId -> uniqueEmailIdsSet.add(aioPackageId));
		List<String> uniqueEmailIds = uniqueEmailIdsSet.stream().collect(Collectors.toList());

		//put the check if you want to check the email exist in vedantu domain or not.

		for (String emailId : uniqueEmailIds) {
			if (!CustomValidator.validEmail(emailId)) {
				throw new VException(ErrorCode.INVALID_PARAMETER, "There are some EmailIds which are invalid");
			}
		}
		logger.info("email Ids : " + uniqueEmailIds);
		return uniqueEmailIds;
	}

	public void communicateInBulkFromSQS(SQSMessageType sqsMessageType, String text) {
		CommunicationDataReq req = new Gson().fromJson(text, CommunicationDataReq.class);
		logger.info("CommunicationDataReq : " + req);
		List<String> allUserIds = new ArrayList<>();
		List<String> allUserEmailIds = new ArrayList<>();
		switch (sqsMessageType) {
			case BULK_USERIDS_REQ:
				allUserIds = req.getUserIds();
				break;
			case BULK_EMAILIDS_REQ:
				allUserEmailIds = req.getEmailIds();
				break;
			case BULK_BATCHIDS_REQ:
				allUserIds = getUserIdsByUsingBatchIds(req.getBatchIds());
				break;
			case BULK_OTM_BUNDLEIDS_REQ:
				allUserIds = getUserIdsByUsingOtmBundleIds(req.getOtmBundleIds());
				break;
			case BULK_AIO_PACKAGEIDS_REQ:
				allUserIds = getUserIdsByUsingAioPackageIds(req.getAioPackageIds());
				break;
			default:
				throw new VRuntimeException(ErrorCode.ILLEGAL_STATE, "unknown messegetype for communication: " + sqsMessageType);
		}

		if (ArrayUtils.isNotEmpty(allUserIds) || ArrayUtils.isNotEmpty(allUserEmailIds)) {

			List<InternetAddress> emailTo = new ArrayList<>();
			ArrayList<String> toSMS = new ArrayList<>();
			List<String> userIds = new ArrayList<>();
			StringBuilder error = new StringBuilder();
			List<UserBasicInfo> users;

			if (ArrayUtils.isNotEmpty(allUserIds)) {
				users = getUserDetailsUsingUserIds(allUserIds);
				if (ArrayUtils.isNotEmpty(users)) {
					users.forEach(user -> {
						try {
							if(user != null && StringUtils.isNotEmpty(user.getEmail()) && StringUtils.isNotEmpty(user.getContactNumber()) && StringUtils.isNotEmpty(user.getPhoneCode())){
								InternetAddress internetAddress = new InternetAddress();
								internetAddress.setAddress(user.getEmail().trim());
								internetAddress.setPersonal(user.getFullName().trim());
								userIds.add(user.getUserId().toString());
								emailTo.add(internetAddress);
								toSMS.add("+" + user.getPhoneCode() + user.getContactNumber());
								logger.info("For user " + user.getFullName() +
										" email address added is " + user.getEmail() +
										" phone code is " + "+" + user.getPhoneCode() + user.getContactNumber());
							}else{
								logger.warn("email or contactNumber or phoneCode not Found in Communicate in bulk"+ user);
							}
						} catch (UnsupportedEncodingException ex) {
							error.append("Error occured for userId ").append(user.getUserId());
						} finally {
							if (error.length() > 0) {
								logger.info(error);
							} else {
								logger.info("No error is found.");
							}
						}
					});
					if (error.length() > 0) {
						logger.warn("error while email adding into InternetAddress : " + error.toString());
					}
				}
			} else if (ArrayUtils.isNotEmpty(allUserEmailIds)) {
				allUserEmailIds.forEach(emailId -> {
					try {
						emailTo.add(new InternetAddress(emailId.trim()));
					} catch (AddressException e) {
						logger.warn("Error in storing standalone email address");
					}
				});
			}


			long dateTime = System.currentTimeMillis();
			if (req.getScheduledTime() == 0) {
				req.setScheduledTime(dateTime);
			}
			if (req.getScheduledTime() != 0 && req.getExpiryTime() != 0 && req.getScheduledTime() >= req.getExpiryTime()) {
				logger.warn("Schedule time can't be greater or equal to the expiry time");
				return;
			}
			ManualNotificationRequest response = updateManualNotificationDB(dateTime, req, userIds, null);
			if (response != null && response.getScheduledTime() == dateTime) {
				String manualNotificationId = response.getId();
				switch (req.getCommunicationKind()){
					case EMAIL:
						logger.info("Sending Email");
						sendEmail(req, emailTo, manualNotificationId);
						break;
					case SMS:
						logger.info("Sending SMS");
						sendSMS(req, toSMS, manualNotificationId);
						break;
					case NOTICE_BOARD:
						logger.info("Sending data to Notice Board");
						req.setAllUsers(userIds);
						req.setManualNotificationId(manualNotificationId);
						populateNoticeBoardDB(req);
						break;
				}
				updateManualNotificationDB(dateTime, req, allUserIds, response.getId());
			}
		}
	}

	public List<UserBasicInfo> getUserDetailsUsingUserIds(List<String> userIds) {
		return Lists.partition(userIds.stream().distinct().collect(Collectors.toList()), 500)
				.stream()
				.map(e -> fosUtils.getUserBasicInfos(e, true))
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}

	public List<String> getUserIdsByUsingBatchIds(List<String> batchIds){
		List<String> batchUserIds = new ArrayList<>();
		for (int count = 0; count < batchIds.size(); count++) {
			List<Enrollment> enrollments = enrollmentDAO.getActiveStudentEnrollmentsUserIdsByBatchIds(batchIds, Collections.singletonList(Enrollment.Constants.USER_ID));
			logger.info("enrollments : " + enrollments);
			enrollments.stream().map(Enrollment::getUserId).forEach(batchUserIds::add);
		}
		return batchUserIds;
	}

	public List<String> getUserIdsByUsingOtmBundleIds(List<String> otmBundleIds) {
		List<String> otmBundleUserIds = new ArrayList<>();
		List<OTMBundle> otmBundles = otfBundleDAO.getOTMBundlesByIds(otmBundleIds, Arrays.asList(OTMBundle.Constants.ID, OTMBundle.Constants.ENTITIES));
		List<String> fetchedValidOTMBundleId = new ArrayList<>();
		List<String> batchIds = new ArrayList<>();
		List<OTMBundleEntityInfo> entities = new ArrayList<>();
		for (OTMBundle otmBundle : otmBundles) {
			fetchedValidOTMBundleId.add(otmBundle.getId());
			entities.addAll(otmBundle.getEntities());
		}
		for (OTMBundleEntityInfo entity : entities) {
			if (EntityType.OTF.equals(entity.getEntityType())) {
				batchIds.add(entity.getEntityId());
			}
		}
		List<Enrollment> enrollments = enrollmentDAO.getActiveStudentEnrollmentsUserIdsByBatchIds(batchIds, Arrays.asList(Enrollment.Constants.USER_ID, Enrollment.Constants.ENTITY_ID, Enrollment.Constants.ENTITY_TYPE));
		Map<String, Boolean> otmBundleMap = new HashMap<>();
		fetchedValidOTMBundleId.forEach(id -> otmBundleMap.put(id, true));
		for (Enrollment enrollment : enrollments) {
			if (otmBundleMap.containsKey(enrollment.getEntityId()) && EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())) {
				otmBundleUserIds.add(enrollment.getUserId());
			}
		}
		return otmBundleUserIds;
	}

	public List<String> getUserIdsByUsingAioPackageIds(List<String> aioPackageIds) {
		List<String> aioPackageUserIds = new ArrayList<>();
		for (int aioCount = 0; aioCount < aioPackageIds.size(); aioCount++) {
			String aioPakcageId = aioPackageIds.get(aioCount);
			Integer start = 0, size = 500;
			while (true) {
				List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO.getActiveBundleEnrolmentsByBundleIds(Arrays.asList(aioPakcageId), Arrays.asList(BundleEnrolment.Constants.USER_ID), start, size);
				for (BundleEnrolment bundleEnrolment : bundleEnrolments) {
					aioPackageUserIds.add(bundleEnrolment.getUserId());
				}
				if (size != bundleEnrolments.size()) {
					break;
				}
				start += size;
			}
		}
		return aioPackageUserIds;
	}
}
