package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.subscription.dao.BundleDAO;
import com.vedantu.subscription.dao.BundleEnrolmentDAO;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.request.GameSetupRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Objects;

@Service
public class BundleEnrolmentManager {

    @Autowired
    private BundleDAO bundleDAO;

    @Autowired
    private BundleEnrolmentDAO bundleEnrolmentDAO;

    @Autowired
    private AwsSQSManager sqsManager;

    private final Logger logger = LogFactory.getLogger(BundleEnrolmentManager.class);

    private static final long BUNDLE_ENROLMENT_CREATION_TIME_FOR_GAME = 1594578600000L;

    private static final Gson GSON = new Gson();

    private static final EnumSet<EnrollmentState> ALLOWED_STATES_FOR_GAME = EnumSet.of(EnrollmentState.REGULAR);

    @PostConstruct
    private void init() {
//        List<Bson> pipeline = Collections.singletonList(Aggregates.match(Filters.or(Filters.in("operationType", Arrays.asList("insert", "update", "replace")))));
//        bundleEnrolmentDAO.watchDocument(this::processBundleEnrolment, BundleEnrolment.class, pipeline);
    }

    public void processBundleEnrolment(BundleEnrolment bundleEnrolment) {
        try {

            logger.info("WATCHED BUNDLE ENROLMENT {}", bundleEnrolment);
            if (ALLOWED_STATES_FOR_GAME.contains(bundleEnrolment.getState())  &&
                    bundleEnrolment.getEntityState() == EntityState.ACTIVE) {
                String bundleId = bundleEnrolment.getBundleId();

                List<Bundle> bundles = bundleDAO.getBundleById(bundleId, Arrays.asList("_id", "isSubscription", "grade"));
                if (ArrayUtils.isNotEmpty(bundles)) {
                    Bundle bundle = bundles.get(0);
                    if (Boolean.TRUE.equals(bundle.getIsSubscription())) {

                        if (bundleEnrolment.getCreationTime() < BUNDLE_ENROLMENT_CREATION_TIME_FOR_GAME) {
                            return;
                        }

                        GameSetupRequest pojo = new GameSetupRequest();
                        pojo.setUserId(Long.valueOf(bundleEnrolment.getUserId()));
                        pojo.setEntityId(bundleEnrolment.getId());
                        pojo.setEntityType(GameSetupRequest.EntityType.BUNDLE_ENROLMENT);
                        pojo.setGrades(bundle.getGrade());
                        pojo.setEnrollmentType(bundleEnrolment.getState());
                        String message = GSON.toJson(pojo);
                        sqsManager.sendToSQS(SQSQueue.GAME_JOURNEY_OPS, SQSMessageType.GAME_ONBOARD, message,
                                bundleEnrolment.getUserId());
                    }
                }
            }
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

}
