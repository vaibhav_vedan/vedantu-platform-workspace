package com.vedantu.subscription.managers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.dao.RefundDAO;
import com.vedantu.subscription.entities.sql.Refund;
//import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;

@Service
public class RefundManager {

	@Autowired
	public RefundDAO refundDAO;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(RefundManager.class);

	public Refund addOrUpdateRefund(Refund refund,Session session) {

		Long id = refund.getId();

		if (id == null) {
			refundDAO.create(refund, session);
		} else {
			Refund refund1 = (Refund) refundDAO.getEntityById(id, true,
					Refund.class, session);
			refund = addUpdates(refund1, refund);
			refundDAO.update(refund, session);
		}

		return refund;
	}

	public Refund getRefundById(Long id, Boolean enabled, Session session) {
		return refundDAO.getById(id, enabled, session);
	}

	public List<Refund> getRefundBySubscriptionId(Long subscriptionId, Boolean enabled, Session session) {
		Criteria cr = session.createCriteria(Refund.class);
		if (enabled != null) {
			if (enabled.equals(true)) {
				cr.add(Restrictions.eq("enabled", true));
			} else {
				cr.add(Restrictions.eq("enabled", false));
			}
		}
		cr.add(Restrictions.eq("subscriptionId", subscriptionId));
		return refundDAO.runQuery(session, cr, Refund.class);
	}

	public void updateAllRefund(List<Refund> slist, Session session) {
		refundDAO.updateAll(slist, session);
	}

	Refund addUpdates(Refund oldObj, Refund newObj) {

		if (newObj.getEnabled() != null) {
			oldObj.setEnabled(newObj.getEnabled());
		}
		if (newObj.getSubscriptionId() != null) {
			oldObj.setSubscriptionId(newObj.getSubscriptionId());
		}
		if (newObj.getRefundAmount() != null) {
			oldObj.setRefundAmount(newObj.getRefundAmount());
		}
		if (newObj.getRefundReason() != null) {
			oldObj.setRefundReason(newObj.getRefundReason());
		}
		oldObj.setLastUpdatedTime(System.currentTimeMillis());
		return oldObj;
	}
}
