package com.vedantu.subscription.managers;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.InsertAcadMentorStudentsReq;
import com.vedantu.onetofew.pojo.ModifyAcadMentorOfStudentReq;
import com.vedantu.subscription.dao.AcadMentorStudentsDAO;
import com.vedantu.subscription.entities.mongo.AcadMentorStudents;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@Service
public class AcadMentorStudentsManager {

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    @Autowired
    private AcadMentorStudentsDAO acadMentorStudentsDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AcadMentorStudentsManager.class);

    public AcadMentorStudentsManager() {
        super();
    }

    public long assignAcadMentorStudents(InsertAcadMentorStudentsReq acadMentorStudentsReq) throws VException, UnsupportedEncodingException {
        User acadMentor = getUserFromEmail(acadMentorStudentsReq.getAcadMentorEmail());
        logger.info("Logger: acadMentor - "+ acadMentor);
        if (acadMentor != null && acadMentor.getTeacherInfo() != null && acadMentor.getTeacherInfo().getSubRole() != null && acadMentor.getTeacherInfo().getSubRole().contains(SubRole.ACADMENTOR)) {
            Set<String> studentEmails = acadMentorStudentsReq.getStudentEmails();
            List<AcadMentorStudents> acadMentorStudentsList = new ArrayList<>();
            for (String studentEmail : studentEmails) {
                UserBasicInfo studentBasicInfo = getUserBasicInfoFromEmail(studentEmail);
                if (studentBasicInfo != null && studentBasicInfo.getUserId() != null && Role.STUDENT.equals(studentBasicInfo.getRole())) {
                    AcadMentorStudents acadMentorStudents = new AcadMentorStudents(acadMentor.getId(), studentBasicInfo.getUserId());
                    acadMentorStudentsList.add(acadMentorStudents);
                } else {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student does not exist.");
                }
            }
            acadMentorStudentsDAO.insertAcadMentorStudentsList(acadMentorStudentsList);
            return studentEmails.size();
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Acad Mentor does not exist.");
        }

    }

    public long modifyAcadMentorStudents(ModifyAcadMentorOfStudentReq modifyAcadMentorOfStudentReq) throws VException, UnsupportedEncodingException {
        User acadMentor = getUserFromEmail(modifyAcadMentorOfStudentReq.getAcadMentorEmail());
        logger.info("acadMentor " + acadMentor);
        if (acadMentor != null && acadMentor.getTeacherInfo() != null && acadMentor.getTeacherInfo().getSubRole() != null && acadMentor.getTeacherInfo().getSubRole().contains(SubRole.ACADMENTOR)) {
            List<User> students = getUsersFromUserIds(modifyAcadMentorOfStudentReq.getStudentIds());
            boolean allValidStudents = true;
            if (students.size() == modifyAcadMentorOfStudentReq.getStudentIds().size()) {
                for (User student : students) {
                    if (student == null || student.getRole() == null || !(student.getRole().equals(Role.STUDENT))) {
                        allValidStudents = false;
                        break;
                    }
                }
            } else {
                allValidStudents = false;
            }
            if (allValidStudents) {
                for (Long studentId : modifyAcadMentorOfStudentReq.getStudentIds()) {
                    AcadMentorStudents acadMentorStudent = new AcadMentorStudents(acadMentor.getId(), studentId);
                    acadMentorStudentsDAO.upsertAcadMentorStudent(acadMentorStudent, modifyAcadMentorOfStudentReq.getCallingUserId());
                }
                return students.size();
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "One or more of the student Ids invalid");
            }

        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User not an acad mentor");
        }
    }

    public long removeStudents(InsertAcadMentorStudentsReq acadMentorStudentsReq) throws VException, UnsupportedEncodingException {
        User acadMentor = getUserFromEmail(acadMentorStudentsReq.getAcadMentorEmail());
        if (acadMentor != null && acadMentor.getTeacherInfo() != null && acadMentor.getTeacherInfo().getSubRole() != null && acadMentor.getTeacherInfo().getSubRole().contains(SubRole.ACADMENTOR)) {
            Set<String> studentEmails = acadMentorStudentsReq.getStudentEmails();
            List<AcadMentorStudents> acadMentorStudentsList = new ArrayList<>();
            for (String studentEmail : studentEmails) {
                UserBasicInfo studentBasicInfo = getUserBasicInfoFromEmail(studentEmail);
                if (studentBasicInfo != null && Role.STUDENT.equals(studentBasicInfo.getRole())) {
                    AcadMentorStudents acadMentorStudents = new AcadMentorStudents(acadMentor.getId(), studentBasicInfo.getUserId());
                    acadMentorStudentsList.add(acadMentorStudents);
                } else {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student does not exist");
                }
            }

            for (AcadMentorStudents acadMentorStudents : acadMentorStudentsList) {
                acadMentorStudentsDAO.removeStudents(acadMentorStudents);
            }
            
            return studentEmails.size();

        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User not an acad mentor");
        }

    }

    public List<User> getStudentsByAcadMentor(String acadMentorEmail, Integer skip, Integer limit) throws VException, UnsupportedEncodingException {
        User acadMentor = getUserFromEmail(acadMentorEmail);
        if (acadMentor != null && acadMentor.getTeacherInfo() != null && acadMentor.getTeacherInfo().getSubRole() != null && acadMentor.getTeacherInfo().getSubRole().contains(SubRole.ACADMENTOR)) {
            List<Long> studentIds = acadMentorStudentsDAO.getStudentsByAcadMentor(acadMentor.getId(), skip, limit);
            return getUsersFromUserIds(studentIds);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User not acad mentor");
        }

    }

    public List<Long> getStudentIdsByAcadMentor(Long acadMentorId) {
        return acadMentorStudentsDAO.getStudentsByAcadMentor(acadMentorId);
    }

    public UserBasicInfo getUserBasicInfoFromEmail(final String userEmail) throws VException, UnsupportedEncodingException {
        String url = USER_ENDPOINT + "/" + "getUserBasicInfoByEmail" + "?email=" + URLEncoder.encode(userEmail, "UTF-8") + "&exposeEmail=" + true;
        logger.info("Logger: url composed is" + url);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        UserBasicInfo userBasicInfo = new Gson().fromJson(respString, UserBasicInfo.class);
        logger.info("Logger: Callresponse userBasicInfo: " + userBasicInfo);
        return userBasicInfo;
    }

    public User getUserFromEmail(String email) throws VException, UnsupportedEncodingException {
        String url = USER_ENDPOINT + "/" + "getUserByEmail" + "?email=" + URLEncoder.encode(email, "UTF-8");
        logger.info("Logger: url composed is" + url);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        User user = new Gson().fromJson(respString, User.class);
        logger.info("Logger: Callresponse user: " + user);
        return user;
    }

    public List<User> getUsersFromUserIds(List<Long> userIds) throws VException {
        String url = USER_ENDPOINT + "/" + "getUsersById" + "?exposeEmail=" + true;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.POST, new Gson().toJson(userIds), true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        return new ArrayList(Arrays.asList(new Gson().fromJson(respString, User[].class)));
    }

    public List<User> getAcadMentors(Integer skip, Integer limit) throws VException {

        List<Long> acadMentors = acadMentorStudentsDAO.getAcadMentors(skip,limit);
        return getUsersFromUserIds(acadMentors);
    }

    public List<AcadMentorStudents> getAcadMentors(List<Long> studentIds) throws VException {

        List<AcadMentorStudents> acadMentors = acadMentorStudentsDAO.getAcadMentors(studentIds);
        return acadMentors;
    }
}
