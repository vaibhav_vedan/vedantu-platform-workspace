package com.vedantu.subscription.managers;

import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.subscription.dao.BundleTestDAO;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.BundleTest;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.viewobject.request.GetBundleTestVideoReq;
import com.vedantu.subscription.viewobject.response.BundleTestFilterRes;
import com.vedantu.subscription.viewobject.response.BundleTestRes;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

@Service
public class BundleTestManager {



    @Autowired
    private BundleTestDAO bundleTestDAO;

    @Autowired
    private BundleTestDAO bundleDAO;

    @Autowired
    private LogFactory logFactory;
    private Logger logger = logFactory.getLogger(BundleTestManager.class);

    public void setBundleTest(BundleTest bundleTest) throws NotFoundException, VException {
        bundleTestDAO.save(bundleTest);

    }
    public List<BundleTest> getBundleTest(GetBundleTestVideoReq bundleTest) throws NotFoundException, VException {

        return  bundleTestDAO.getTest(bundleTest);

    }
    public  BundleTestRes getTestByBundle(GetBundleTestVideoReq bundleTest) throws NotFoundException, VException {


        return bundleTestDAO.getTestByBundle(bundleTest);

    }



    public List<BundleTestFilterRes> getTestDetailsByBundles(GetBundleTestVideoReq getBundleTestVideoReq) throws VException {
        return bundleDAO.getTestDetailsByBundles(getBundleTestVideoReq);
    }
}
