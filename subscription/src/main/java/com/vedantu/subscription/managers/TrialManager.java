package com.vedantu.subscription.managers;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.dao.TrialDAO;
import com.vedantu.subscription.entities.mongo.Trial;
import com.vedantu.subscription.pojo.TrialData;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class TrialManager {
    @Autowired
    private LogFactory logFactory;

    @Autowired
    private TrialDAO trialDAO;

    @Autowired
    private DozerBeanMapper mapper;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(TrialManager.class);

    public Set<String> insertTrialData(List<TrialData> trialData) {
        List<Trial> trials =
                Optional.ofNullable(trialData)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .filter(this::isNewTrialData)
                        .map(this::getNewBundleTrials)
                        .collect(Collectors.toList());

        trialDAO.insertAllEntities(trials);
        Set<String> existingTrials =
                Optional.ofNullable(trialData)
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(TrialData::getId)
                        .filter(Objects::nonNull)
                        .collect(Collectors.toSet());
        Set<String> newTrials=trials.stream().map(Trial::getId).collect(Collectors.toSet());
        existingTrials.addAll(newTrials);
        return existingTrials;
    }

    private boolean isNewTrialData(TrialData trialData) {
        return Objects.isNull(trialData.getId());
    }

    private Trial getNewBundleTrials(TrialData trialData) {
        Trial trial=mapper.map(trialData,Trial.class);
        trial.setContextType(EntityType.BUNDLE);
        return trial;
    }

    public void markTrialIdDeleted(List<String> trialIdsForRemoval) {
        trialDAO.markTrialIdDeleted(trialIdsForRemoval);
    }

    public void updateContextId(Set<String> trialIds, String contextId) {
        trialDAO.updateContextId(trialIds,contextId);
    }

    public void updateBundleContextForTrials(Set<String> trialIds, String bundleId) throws BadRequestException {
        if(ArrayUtils.isEmpty(trialIds) || StringUtils.isEmpty(bundleId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Neither [trialIds] nor [bundleId] could be null");
        }
        List<Trial> trials=trialDAO.getAllActiveTrialsForBundle(trialIds,bundleId);

        validateTrialData(trials);

        updateContextId(trialIds,bundleId);
    }

    private void validateTrialData(List<Trial> trials) throws BadRequestException {
        logger.info("validateTrialData - trials - {}",trials);
        int distinctTrialDays = trials.stream().map(Trial::getTrialAllowedDays).collect(Collectors.toSet()).size();
        if(distinctTrialDays!=trials.size()){
            logger.info("distinctTrialDays - {} --- trials.size() -- {}",distinctTrialDays,trials.size());
            throw new BadRequestException(ErrorCode.NON_DISTINCT_TRIAL_DAYS,"Non distinct trial days are there for the trial.");
        }
        Collections.sort(trials, Comparator.comparing(Trial::getTrialAllowedDays));
        for (int i = 1; i < trials.size(); i++) {
            if(trials.get(i).getTrialAmount()<=trials.get(i-1).getTrialAmount()){
                String errorMessage=("It is not recommended to have a trial with more days with less than or equal amount compared " +
                        "to a trail with less no of days.");
                throw new BadRequestException(ErrorCode.IMPROPER_AMOUNT_ASSIGNMENT_FOR_TRIAL,errorMessage);
            }
        }
    }
}
