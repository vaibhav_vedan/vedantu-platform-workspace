package com.vedantu.subscription.managers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.onetofew.request.GetUserMessageReq;
import com.vedantu.subscription.dao.UserMessageDAO;
import com.vedantu.subscription.entities.mongo.UserMessage;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Service
public class UserMessageManager {

	@Autowired
	public UserMessageDAO userMessageDAO;

	public UserMessage userMessage(UserMessage userMessage) {
		if (userMessage == null || (StringUtils.isEmpty(userMessage.getEmailId())
				&& StringUtils.isEmpty(userMessage.getMobileNumber()))) {
			return null;
		}

		UserMessage result = userMessageDAO.create(userMessage);
		return result;
	}

	public UserMessage getUserMessage(String id) {
		if (StringUtils.isEmpty(id)) {
			return null;
		}

		UserMessage userMessage = userMessageDAO.getById(id);
		return userMessage;
	}

	public List<UserMessage> getUserMessages(GetUserMessageReq req) {
		Query query = new Query();
		if (!StringUtils.isEmpty(req.getUserId())) {
			query.addCriteria(Criteria.where(UserMessage.Constants.USER_ID).is(req.getUserId()));
		}
		if (!StringUtils.isEmpty(req.getEmailId())) {
			query.addCriteria(Criteria.where(UserMessage.Constants.EMAIL_ID).is(req.getEmailId()));
		}
		if (!StringUtils.isEmpty(req.getMobileNumber())) {
			query.addCriteria(Criteria.where(UserMessage.Constants.MOBILE_NUMBER).is(req.getMobileNumber()));
		}
		if (req.getCreatedAfter() != null) {
			query.addCriteria(Criteria.where(AbstractMongoStringIdEntity.Constants.CREATION_TIME).gte(req.getCreatedAfter()));
		}

		Integer start = req.getStart();
		Integer limit = req.getSize();
		if (start == null || start < 0) {
			start = 0;
		}

		if (limit == null || limit <= 0) {
			limit = 20;
		}

		query.skip(start);
		query.limit(limit);
		List<UserMessage> results = userMessageDAO.runQuery(query, UserMessage.class);
		return results;
	}
}
