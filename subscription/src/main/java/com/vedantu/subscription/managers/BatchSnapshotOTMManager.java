package com.vedantu.subscription.managers;


import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BatchSnapshotOTMDAO;
import com.vedantu.subscription.entities.mongo.BatchSnapshotOTM;
import com.vedantu.subscription.enums.BatchSnapshotType;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.util.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import com.google.gson.reflect.TypeToken;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.subscription.entities.mongo.Batch;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.TimeZone;

@Service
public class BatchSnapshotOTMManager {

    @Autowired
    BatchDAO batchDAO;

    @Autowired
    BatchSnapshotOTMDAO batchSnapshotOTMDAO;

    private static Gson gson = new Gson();

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSNSManager awsSNSManager;
    
    private Logger logger = logFactory.getLogger(BatchSnapshotOTMManager.class);

    public static TimeZone indiaTimeZone = TimeZone.getTimeZone("Asia/Kolkata");


    private final String schedulingEndpoint = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

    

    public void createBatchSnapshotInitially(Long time) throws VException{
        
        //Long currentTime = System.currentTimeMillis();
        Long currentTime = DateTimeUtils.getISTDayStartTime(time);
        
        List<Batch> batches = batchDAO.getBatchesCreatedBetween(currentTime - DateTimeUtils.MILLIS_PER_DAY, currentTime);
        
        if(ArrayUtils.isEmpty(batches)){
            return;
        }
        
        for(Batch batch : batches){
            String batchId = batch.getId();
            Long startTime = currentTime;
            Long endTime = DateTimeUtils.getISTMonthEndTime(currentTime);
            Long startMonthTime = DateTimeUtils.getISTMonthStartTime(currentTime);
            int batchSnapshotSize = batchSnapshotOTMDAO.getBatchSnapshotsCountForBatchId(batchId, startMonthTime);

            if(batchSnapshotSize > 0){
                logger.info("Monthly BatchSnapshot already created for batchId: " + batchId);
                return;
            }


            try{
                String requestUrl = schedulingEndpoint + "/onetofew/session/getOTFSessionSnapshots?batchId="+batchId+"&startTime="+startTime+"&endTime="+endTime;

                ClientResponse resp = WebUtils.INSTANCE.doCall(requestUrl, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                Type sessionSnapshotListType = new TypeToken<List<SessionSnapshot>>() {
                }.getType();

                List<SessionSnapshot> list = gson.fromJson(jsonString, sessionSnapshotListType);
                BatchSnapshotOTM batchSnapshotOTM = new BatchSnapshotOTM();
                batchSnapshotOTM.setBatchId(batchId);
                batchSnapshotOTM.setBatchSnapshotType(BatchSnapshotType.INITIAL);
                batchSnapshotOTM.setMonthStartTime(startMonthTime);
                batchSnapshotOTM.setSessions(list);
                
                if(batchSnapshotOTM.getSessions().size()>0){
                    batchSnapshotOTMDAO.create(batchSnapshotOTM, null);
                }
            }catch(Exception ex){
               logger.error("Exception: "+ex.getMessage());
               logger.error("Batch Snapshot creation failed for :"+batchId);                
            }
        }
    }

    public void createBatchSnapshotMonthly(Long currentTime) throws VException{
        
        List<Batch> batches = batchDAO.getActiveBatches();
        
        if(ArrayUtils.isEmpty(batches)){
            return;
        }
        
        for(Batch batch : batches){
            String batchId = batch.getId();
            //Long currentTime = System.currentTimeMillis();
            //Long startTime = DateTimeUtils.getISTDayStartTime(currentTime);

            Long endTime = DateTimeUtils.getISTMonthEndTime(currentTime);
            Long startMonthTime = DateTimeUtils.getISTMonthStartTime(currentTime);

            //List<BatchSnapshotOTM> batchSnapshotOTMList = batchSnapshotOTMDAO.getBatchSnapshotsForBatchId(batchId, startMonthTime);
            int batchSnapshotSize = batchSnapshotOTMDAO.getBatchSnapshotsCountForBatchId(batchId, startMonthTime);

            if(batchSnapshotSize > 0){
                logger.info("Monthly BatchSnapshot already created for batchId: " + batchId);
                continue;
            }


            try{
                String requestUrl = schedulingEndpoint + "/onetofew/session/getOTFSessionSnapshots?batchId="+batchId+"&startTime="+startMonthTime+"&endTime="+endTime;

                ClientResponse resp = WebUtils.INSTANCE.doCall(requestUrl, HttpMethod.GET, null, true);
                VExceptionFactory.INSTANCE.parseAndThrowException(resp);
                String jsonString = resp.getEntity(String.class);
                Type sessionSnapshotListType = new TypeToken<List<SessionSnapshot>>() {
                }.getType();

                List<SessionSnapshot> list = gson.fromJson(jsonString, sessionSnapshotListType);
                BatchSnapshotOTM batchSnapshotOTM = new BatchSnapshotOTM();
                batchSnapshotOTM.setBatchId(batchId);
                batchSnapshotOTM.setBatchSnapshotType(BatchSnapshotType.MONTHLY);
                batchSnapshotOTM.setMonthStartTime(startMonthTime);
                batchSnapshotOTM.setSessions(list);
                if(batchSnapshotOTM.getSessions().size() > 0){
                    batchSnapshotOTMDAO.create(batchSnapshotOTM, null);
                }
            }catch(Exception ex){
                logger.error("Exception: "+ex.getMessage());
                logger.error("Batch Snapshot creation failed for :"+batchId);
            }
        }
    }
    
    
    public void createSnapshots(Long time) throws VException{
        Long monthStartTime = DateTimeUtils.getISTMonthStartTime(time);
        Long dayStartTime = DateTimeUtils.getISTDayStartTime(time);
        boolean monthly = false;
        if(Objects.equals(dayStartTime, monthStartTime)){
            monthly = true;
        }
        
        createBatchSnapshotInitially(time);
        if(monthly){
            createBatchSnapshotMonthly(time);
        }
    }
        
    public void checkForOTFSessions(Long time){

        Long currentTime = DateTimeUtils.getISTHourStartTime(time);
        Long backTime = currentTime - 3*DateTimeUtils.MILLIS_PER_HOUR;

        List<BatchSnapshotOTM> batchSnapshotOTMList = batchSnapshotOTMDAO.getBatchSnapshotWithSessionInTime(backTime, currentTime);
        logger.info("BatchSnapshotList: "+batchSnapshotOTMList);
        
        if(batchSnapshotOTMList==null){
            logger.info("No batch snapshot to process");
            return;
        }
        
        for(BatchSnapshotOTM batchSnapshotOTM : batchSnapshotOTMList){
            if(ArrayUtils.isEmpty(batchSnapshotOTM.getSessions())){
                continue;
            }
            for(SessionSnapshot sessionSnapshot : batchSnapshotOTM.getSessions()){
                if(sessionSnapshot.getEndTime() >= backTime && sessionSnapshot.getEndTime() < currentTime){
                    SNSTopicOTF sNSTopicOTF = SNSTopicOTF.OTM_SNAPSHOT_SESSION_ENDED;
                    List<String> batchIds = new ArrayList<>();
                    batchIds.add(batchSnapshotOTM.getBatchId());
                    sessionSnapshot.setBatchIds(batchIds);
                    //OTFSessionEnrollmentPojo oTFSessionEnrollmentPojo = new OTFSessionEnrollmentPojo(oTFSession.getId(),oTFSession.getBatchId(), oTFSession.getStartTime(), oTFSession.getEndTime());
                    awsSNSManager.triggerSNS(sNSTopicOTF,"SESSION_CONSUMPTION", gson.toJson(sessionSnapshot));
                }
            }
        }
    }
    
    

        
    public void checkForOTFSessions(){

        Long currentTime = System.currentTimeMillis();
        Long backTime = currentTime - 3*DateTimeUtils.MILLIS_PER_HOUR;

        List<BatchSnapshotOTM> batchSnapshotOTMList = batchSnapshotOTMDAO.getBatchSnapshotWithSessionInTime(backTime, currentTime);

        for(BatchSnapshotOTM batchSnapshotOTM : batchSnapshotOTMList){
            for(SessionSnapshot sessionSnapshot : batchSnapshotOTM.getSessions()){
                if(sessionSnapshot.getEndTime() > backTime && sessionSnapshot.getEndTime() < currentTime){
                    SNSTopicOTF sNSTopicOTF = SNSTopicOTF.OTM_SNAPSHOT_SESSION_ENDED;
                    //OTFSessionEnrollmentPojo oTFSessionEnrollmentPojo = new OTFSessionEnrollmentPojo(oTFSession.getId(),oTFSession.getBatchId(), oTFSession.getStartTime(), oTFSession.getEndTime());
                    awsSNSManager.triggerSNS(sNSTopicOTF, gson.toJson(sessionSnapshot), "SESSION_END");
                }
            }
        }
    }
    
    public void updateBatchSnapshot(String batchId, List<SessionSnapshot> sessionSnapshots){
        logger.info("Updating batch snapshot: ");
        Long time = System.currentTimeMillis();
        Long monthStartTime = DateTimeUtils.getISTMonthStartTime(time);
        Map<Long, List<SessionSnapshot>> startTimeSessionSnapshot = new HashMap<>();
        
        sessionSnapshots.forEach((sessionSnapshot) -> {
            Long startTime = DateTimeUtils.getISTMonthStartTime(sessionSnapshot.getStartTime());
            
            if(!startTimeSessionSnapshot.containsKey(startTime)){
                startTimeSessionSnapshot.put(startTime, new ArrayList<>());
            }
            
            startTimeSessionSnapshot.get(startTime).add(sessionSnapshot);
        });
        
        startTimeSessionSnapshot.entrySet().forEach((entry) -> {
            if(entry.getKey() <= monthStartTime){
            
                BatchSnapshotOTM batchSnapshotOTM = batchSnapshotOTMDAO.getBatchSnapshotOTM(batchId, entry.getKey());
                if(batchSnapshotOTM == null){
                    batchSnapshotOTM = new BatchSnapshotOTM();
                    batchSnapshotOTM.setBatchId(batchId);
                    batchSnapshotOTM.setBatchSnapshotType(BatchSnapshotType.MONTHLY);
                    batchSnapshotOTM.setMonthStartTime(entry.getKey());
                    batchSnapshotOTM.setSessions(new ArrayList<>());
                }
                if (batchSnapshotOTM.getSessions().size() <= 0) {
                    batchSnapshotOTM.getSessions().addAll(entry.getValue());
                    batchSnapshotOTMDAO.save(batchSnapshotOTM);
                }
                
            }
        });
        
    }
    
    public void updateBatchSnapshotOnMerging(SessionSnapshot sessionSnapshot, String batchId){
        Long monthStartTime = DateTimeUtils.getISTMonthStartTime(sessionSnapshot.getStartTime());
        BatchSnapshotOTM batchSnapshotOTM = batchSnapshotOTMDAO.getBatchSnapshotOTM(batchId, monthStartTime);
        if(batchSnapshotOTM == null){
            batchSnapshotOTM = new BatchSnapshotOTM();
            batchSnapshotOTM.setBatchId(batchId);
            batchSnapshotOTM.setBatchSnapshotType(BatchSnapshotType.MONTHLY);
            batchSnapshotOTM.setMonthStartTime(monthStartTime);
            batchSnapshotOTM.setSessions(new ArrayList<>());
        }
        
        batchSnapshotOTM.getSessions().add(sessionSnapshot);
        batchSnapshotOTMDAO.save(batchSnapshotOTM);
        
    }


}
