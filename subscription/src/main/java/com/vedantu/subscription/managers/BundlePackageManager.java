package com.vedantu.subscription.managers;

import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.response.OrderInfo;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BundleEnrolmentDAO;
import com.vedantu.subscription.dao.BundlePackageDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.BundlePackage;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.BundlePackageDetailsInfo;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.pojo.EntityInfo;
import com.vedantu.subscription.request.AddEditBundlePackageReq;
import com.vedantu.subscription.request.BundleCreateEnrolmentReq;
import com.vedantu.subscription.request.GetBundlePackagesReq;
import com.vedantu.subscription.viewobject.request.MakePaymentRequest;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import org.dozer.DozerBeanMapper;

/**
 * Created by somil on 09/05/17.
 */
@Service
public class BundlePackageManager {

    @Autowired
    private BundlePackageDAO bundlePackageDAO;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private EnrollmentConsumptionManager enrollmentConsumptionManager;

    @Autowired
    private BatchManager batchManager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private BundleEnrolmentDAO bundleEnrolmentDAO;

    @Autowired
    private BundleEnrolmentManager bundleEnrolmentManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BundlePackageManager.class);

    @Autowired
    private DozerBeanMapper mapper;

    public BundlePackageInfo getBundlePackage(String id) throws NotFoundException {
        BundlePackage bundlePackage = bundlePackageDAO.getBundlePackageById(id);
        if (bundlePackage == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundle not found " + id);
        }
        BundlePackageInfo bundlePackageInfo = mapper.map(bundlePackage, BundlePackageInfo.class);
        return bundlePackageInfo;
    }

    public void updatedBundlePackageStartTime(BundlePackage bundlePackage) {

        if (ArrayUtils.isNotEmpty(bundlePackage.getEntities())) {
            Set<String> batchIds = new HashSet<>();

            for (EntityInfo entityInfo : bundlePackage.getEntities()) {
                batchIds.add(entityInfo.getEntityId());
            }

            List<Batch> batches = batchDAO.getBatchByIds(new ArrayList<>(batchIds));

            if (ArrayUtils.isNotEmpty(batches)) {

                Long bundleStartTime = batches.get(0).getStartTime();
                Set<Long> boardIds = new HashSet<>();
                Integer duration = 0;
                for (Batch batch : batches) {
                    if (batch.getStartTime() < bundleStartTime) {
                        bundleStartTime = batch.getStartTime();
                    }

                    if (ArrayUtils.isNotEmpty(batch.getBoardTeacherPairs())) {
                        for (BoardTeacherPair boardTeacherPair : batch.getBoardTeacherPairs()) {
                            boardIds.add(boardTeacherPair.getBoardId());
                        }
                    }
                    if (batch.getDuration() != null) {
                        duration += batch.getDuration().intValue();
                    }

                }

                bundlePackage.setBoardIds(boardIds);
                bundlePackage.setStartTime(bundleStartTime);
                bundlePackage.setDuration(duration);

            }

        }

    }

    public BundlePackageInfo addEditBundlePackage(AddEditBundlePackageReq addEditBundleReq) throws NotFoundException {
        BundlePackage bundlePackage = mapper.map(addEditBundleReq, BundlePackage.class);
        if (StringUtils.isNotEmpty(addEditBundleReq.getId())) {
            BundlePackage bundlePackageExisting = bundlePackageDAO.getBundlePackageById(addEditBundleReq.getId());
            if (bundlePackageExisting == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundle not found " + addEditBundleReq.getId());
            }
            bundlePackage.setCreatedBy(bundlePackageExisting.getCreatedBy());
            bundlePackage.setCreationTime(bundlePackageExisting.getCreationTime());
        }

        updatedBundlePackageStartTime(bundlePackage);
        //bundlePackage = mapper.map(addEditBundleReq, BundlePackage.class);
        // toBundlePackage(bundlePackage, addEditBundleReq);
        bundlePackageDAO.save(bundlePackage, addEditBundleReq.getCallingUserId());
        logger.info("Bundle package post update:" + bundlePackage.toString());
        BundlePackageInfo bundlePackageInfo = mapper.map(bundlePackage, BundlePackageInfo.class);
        return bundlePackageInfo;
    }

    public void toBundlePackage(BundlePackage bundlePackage, AddEditBundlePackageReq addEditBundlePackageReq) {
        bundlePackage.setEntities(addEditBundlePackageReq.getEntities());
        bundlePackage.setBoardId(addEditBundlePackageReq.getBoardId());
        bundlePackage.setDescription(addEditBundlePackageReq.getDescription());
        bundlePackage.setFeatured(addEditBundlePackageReq.isFeatured());
        bundlePackage.setGrade(addEditBundlePackageReq.getGrade());
        bundlePackage.setTitle(addEditBundlePackageReq.getTitle());
        bundlePackage.setTarget(addEditBundlePackageReq.getTarget());
        bundlePackage.setKeyValues(addEditBundlePackageReq.getKeyValues());
        bundlePackage.setPrice(addEditBundlePackageReq.getPrice());
        bundlePackage.setTags(addEditBundlePackageReq.getTags());
        bundlePackage.setPriority(addEditBundlePackageReq.getPriority());
        bundlePackage.setVisibilityState(addEditBundlePackageReq.getVisibilityState());
    }

    public List<BundlePackageInfo> getBundlePackageInfos(GetBundlePackagesReq req) {
        List<BundlePackageInfo> response = new ArrayList<>();
        List<BundlePackage> bundlePackages = bundlePackageDAO.getBundlePackages(req);
        if (CollectionUtils.isNotEmpty(bundlePackages)) {
            for (BundlePackage bundlePackage : bundlePackages) {
                BundlePackageInfo bundlePackageInfo = mapper.map(bundlePackage, BundlePackageInfo.class);
                response.add(bundlePackageInfo);
            }
        }
        return response;
    }

    public List<BundleEnrolment> createEnrolments(BundleCreateEnrolmentReq req) throws VException {
        if (req.getState() == null) {
            req.setState(EnrollmentState.REGULAR);
        }
        if (req.getStatus() == null) {
            req.setStatus(EntityStatus.ACTIVE);
        }
        BundlePackageInfo bundlePackage = getBundlePackage(req.getEntityId());
        List<String> bundleIds = new ArrayList<>();
        for (EntityInfo entityInfo : bundlePackage.getEntities()) {
            if (!EntityType.BUNDLE.equals(entityInfo.getEntityType())) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Not allowed");
            }
            bundleIds.add(entityInfo.getEntityId());
        }
        List<String> alreadyExistingEnrolledBundle = new ArrayList<>();

        List<BundleEnrolment> bundleEnrolments = bundleEnrolmentDAO.getBundleEnrolments(req.getUserId(), bundleIds);
        if (CollectionUtils.isNotEmpty(bundleEnrolments)) {

            for (BundleEnrolment bundleEnrolment : bundleEnrolments) {
                if (EnrollmentState.REGULAR.equals(bundleEnrolment.getState()) && EnrollmentState.TRIAL.equals(req.getState())) {
                    throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Can't change from Regular to Trial");
                }
            }

            for (BundleEnrolment bundleEnrolment : bundleEnrolments) {
                alreadyExistingEnrolledBundle.add(bundleEnrolment.getBundleId());
                if (!bundleEnrolment.getState().equals(req.getState()) || !bundleEnrolment.getStatus().equals(req.getStatus())) {
                    bundleEnrolment.setStatus(req.getStatus());
                    bundleEnrolment.setState(req.getState());
                    bundleEnrolmentDAO.save(bundleEnrolment, null);
                    // todo enable for gamification
                    // bundleEnrolmentManager.processBundleEnrolment(bundleEnrolment);
                }
            }
        }

        bundleIds.removeAll(alreadyExistingEnrolledBundle);
//		if (CollectionUtils.isEmpty(bundleIds)) {
//			throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Not allowed, all bundles already enrolled");
//		}
        List<BundleEnrolment> bundleEnrolmentList = new ArrayList<>();
        for (String bundleId : bundleIds) {
            BundleEnrolment bundleEnrolment = new BundleEnrolment(bundleId, req.getUserId(), req.getStatus(),
                    req.getState());
            bundleEnrolmentList.add(bundleEnrolment);
        }

        if (CollectionUtils.isNotEmpty(bundleEnrolmentList)) {
            bundleEnrolmentDAO.insertAllEntities(bundleEnrolmentList, BundleEnrolment.class.getSimpleName());
        }
        if (CollectionUtils.isNotEmpty(bundleEnrolments)) {
            bundleEnrolmentList.addAll(bundleEnrolments);
        }

        // Enroll the student in sessions
        bundleManager.updateWebinarSessionAttendence(bundleEnrolmentList);

        return bundleEnrolmentList;
    }

    public BundlePackageDetailsInfo getBundlePackageDetails(String id) throws VException {
        BundlePackage bundlePackage = bundlePackageDAO.getBundlePackageById(id);
        if (bundlePackage == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundle not found " + id);
        }
        BundlePackageDetailsInfo bundlePackageInfo = mapper.map(bundlePackage, BundlePackageDetailsInfo.class);
        List<String> bundleIds = new ArrayList<>();
        for (EntityInfo entityInfo : bundlePackage.getEntities()) {
            if (!EntityType.BUNDLE.equals(entityInfo.getEntityType())) {
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Not allowed");
            }
            bundleIds.add(entityInfo.getEntityId());
        }

        List<Bundle> bundles = bundleManager.getBundles(bundleIds);
        List<BundleInfo> bundleInfos = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(bundles)) {
            for (Bundle bundle : bundles) {
                BundleInfo bundleInfo = mapper.map(bundle, BundleInfo.class);
                bundleInfos.add(bundleInfo);
            }
        }
        bundlePackageInfo.setEntities(bundleInfos);

        return bundlePackageInfo;
    }

    public List<BundlePackageDetailsInfo> getBundlePackageDetailsList(GetBundlePackagesReq req)
            throws ForbiddenException {

        List<BundlePackageDetailsInfo> bundlePackageDetailsInfos = new ArrayList<>();
        List<String> bundleIds = new ArrayList<>();

        List<BundlePackage> bundlePackages = bundlePackageDAO.getBundlePackages(req);
        if (CollectionUtils.isNotEmpty(bundlePackages)) {
            for (BundlePackage bundlePackage : bundlePackages) {
                for (EntityInfo entityInfo : bundlePackage.getEntities()) {
                    if (EntityType.BUNDLE.equals(entityInfo.getEntityType())) {
                        bundleIds.add(entityInfo.getEntityId());
                    }
                }
            }

            List<Bundle> bundles = bundleManager.getBundles(bundleIds);
            // List<BundleInfo> bundleInfos = new ArrayList<>();
            Map<String, BundleInfo> bundleMap = new HashMap<>();
            if (CollectionUtils.isNotEmpty(bundles)) {
                for (Bundle bundle : bundles) {
                    BundleInfo bundleInfo = mapper.map(bundle, BundleInfo.class);
                    bundleMap.put(bundleInfo.getId(), bundleInfo);
                }
            }

            for (BundlePackage bundlePackage : bundlePackages) {
                BundlePackageDetailsInfo bundlePackageInfo = mapper.map(bundlePackage, BundlePackageDetailsInfo.class);
                List<BundleInfo> entities = new ArrayList<>();
                bundlePackageInfo.setEntities(entities);
                for (EntityInfo entityInfo : bundlePackage.getEntities()) {
                    BundleInfo bundleInfo = bundleMap.get(entityInfo.getEntityId());
                    if (bundleInfo != null) {
                        entities.add(bundleInfo);
                    }
                }
                bundlePackageDetailsInfos.add(bundlePackageInfo);

            }
        }
        return bundlePackageDetailsInfos;
    }

    public List<BundlePackageInfo> getBundlesForEntity(GetBundlePackagesReq req) {
        List<BundlePackageInfo> response = new ArrayList<>();
        List<BundlePackage> bundlePackages = bundlePackageDAO.getBundlePackagesForEntity(req);
        if (CollectionUtils.isNotEmpty(bundlePackages)) {
            for (BundlePackage bundlePackage : bundlePackages) {
                BundlePackageInfo bundlePackageInfo = mapper.map(bundlePackage, BundlePackageInfo.class);
                response.add(bundlePackageInfo);
            }
        }
        return response;
    }

    public void processBundlePurchaseAfterPayment(OrderInfo orderInfo) throws VException {

        //orderInfo = paymentManager.getOrderInfo(orderInfo.getId());
        //List<String> batches = new ArrayList<>();
        Long userId = orderInfo.getUserId();

        if (orderInfo.getPurchasingEntities() == null) {
            logger.error("Purchasing entities not found for order: " + orderInfo);
        }

        int numOfEntities = orderInfo.getPurchasingEntities().size();
        int i = 0;
        int amountSum = 0;
        int amountPSum = 0;
        int amountNPSum = 0;
        int discountSum = 0;

        int discountAmount = 0;

        if (ArrayUtils.isNotEmpty(orderInfo.getOrderedItems()) && orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount() != null) {
            discountAmount = orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount();
        }

        if (orderInfo.getPromotionalAmount() == null) {
            orderInfo.setPromotionalAmount(0);

        }

        if (orderInfo.getNonpromotionalAmount() == null) {
            orderInfo.setNonpromotionalAmount(0);

        }

        int totalAmount = 0;

        List<Batch> batches = new ArrayList<>();
        for (PurchasingEntity purchasingEntity : orderInfo.getPurchasingEntities()) {
            //batches.add(purchasingEntity.getEntityId());
            Batch batch = batchDAO.getById(purchasingEntity.getEntityId());
            batches.add(batch);
            totalAmount += batch.getPurchasePrice();
        }

        for (Batch batch : batches) {

            //Batch batch = batchDAO.getById(purchasingEntity.getEntityId());
            float ratio = (float) batch.getPurchasePrice() / totalAmount;

            Enrollment enrollment = enrollmentDAO.getEnrollmentByUserIdAndBatch(userId, batch.getId(), null);

            if (enrollment == null) {
                logger.error("Enrollment not found for bundle package for userId: " + userId + " and batchId: " + batch.getId());
                continue;
            }

            int duration = (int) (batch.getDuration() - batchManager.getDurationToBeSubtracted(batch.getId()));

            if (duration < DateTimeUtils.MILLIS_PER_HOUR) {
                duration = DateTimeUtils.MILLIS_PER_HOUR;
                //logger.error("Left Duration can't be less than one hour : "+enrollment.getId());
                //continue;
            }

            if (batch.isContentBatch()) {
                duration = (int) (batch.getEndTime() - (System.currentTimeMillis() + 15 * DateTimeUtils.MILLIS_PER_DAY));
                if (duration < DateTimeUtils.MILLIS_PER_HOUR) {
                    duration = DateTimeUtils.MILLIS_PER_HOUR;
                }
            }

            MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
            makePaymentRequest.setEnrollmentId(enrollment.getId());
            makePaymentRequest.setBatchId(batch.getId());
            makePaymentRequest.setCourseId(enrollment.getCourseId());
            makePaymentRequest.setOrderId(orderInfo.getId());
            //makePaymentRequest.setAmt((int)(ratio*orderInfo.getAmount()));
            makePaymentRequest.setAmtP((int) (ratio * orderInfo.getPromotionalAmount()));
            makePaymentRequest.setAmtNP((int) (ratio * orderInfo.getNonpromotionalAmount()));
            makePaymentRequest.setAmt(makePaymentRequest.getAmtP() + makePaymentRequest.getAmtNP());
            makePaymentRequest.setAmtFromDiscount((int) (ratio * discountAmount));
            makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.BULK_PAYMENT);
            makePaymentRequest.setDuration(batch.getDuration());

            if (i == numOfEntities - 1) {
                makePaymentRequest.setAmt(orderInfo.getAmount() - amountSum);
                makePaymentRequest.setAmtP(orderInfo.getPromotionalAmount() - amountPSum);
                makePaymentRequest.setAmtNP(orderInfo.getNonpromotionalAmount() - amountNPSum);
                makePaymentRequest.setAmtFromDiscount(discountAmount - discountSum);
            }

            enrollment.setHourlyRate((int) ((makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount()) / (duration / DateTimeUtils.MILLIS_PER_HOUR)));
            enrollmentDAO.save(enrollment);

            amountSum += makePaymentRequest.getAmt();
            amountPSum += makePaymentRequest.getAmtP();
            amountNPSum += makePaymentRequest.getAmtNP();
            discountSum += makePaymentRequest.getAmtFromDiscount();
            makePaymentRequest.setAmtToBePaid(makePaymentRequest.getAmt() + makePaymentRequest.getAmtFromDiscount());

            boolean result = enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, ratio);
            i++;
            if (!result) {
                logger.error("EnrollmentConsumption topup failed for : " + enrollment);
            }

        }

    }

    public PlatformBasicResponse checkForEnrollmentInBundleBatch(String bundleId, String userId) {
        BundlePackage bundlePackage = bundlePackageDAO.getBundlePackageById(bundleId);
        List<EntityStatus> entityStatuses = Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE);
        Set<String> batchIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(bundlePackage.getEntities())) {
            for (EntityInfo entityInfo : bundlePackage.getEntities()) {
                batchIds.add(entityInfo.getEntityId());
            }
        }
        List<Batch> batches = batchDAO.getBatchByIds(new ArrayList<>(batchIds));
        if (ArrayUtils.isNotEmpty(batches)) {
            for (Batch batch : batches) {
                List<Enrollment> enrollments = enrollmentDAO.getEnrollmentByCourseAndUser(batch.getCourseId(), userId, entityStatuses);
                if (ArrayUtils.isNotEmpty(enrollments)) {
                    return new PlatformBasicResponse(false, "", ErrorCode.ALREADY_ENROLLED.toString());
                }
            }

            for (Batch batch : batches) {
//                int noOfStudents = batch.getEnrolledStudents() == null ? 0 : batch.getEnrolledStudents().size();
//                if (batch.getMaxEnrollment() < noOfStudents + 1) {
//                    return new PlatformBasicResponse(false, "", ErrorCode.BATCH_ALREADY_FULL.toString());
                    //logger.error("batch already full");
                    //throw new ForbiddenException(ErrorCode.BATCH_ALREADY_FULL, "Batch already full "
                    //     + batch.getId());
//                }
            }
        }

        return new PlatformBasicResponse(true, "", "");
    }

}
