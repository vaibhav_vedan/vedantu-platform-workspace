package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.dao.AcadMentorStudentsDAO;
import com.vedantu.subscription.dao.LOAMAmbassadorSubscriptionDAO;
import com.vedantu.subscription.entities.mongo.AcadMentorStudents;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class LOAMAmbassadorSubscriptionManager {

    @Autowired
    LOAMAmbassadorSubscriptionDAO loamAmbassadorSubscriptionDAO;

    @Autowired
    private AcadMentorStudentsDAO acadMentorStudentsDAO;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LOAMAmbassadorSubscriptionManager.class);

    private static final String USER_ENDPOINT_LOAM = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT_LOAM");

    public List<Long> loam_getStudentsByAcadMentor(String acadMentorEmail) throws VException, UnsupportedEncodingException
    {
        //Hitting the user service just to get the id of acadmentor
        User acadMentor = loam_getUserFromEmail(acadMentorEmail);

        if (acadMentor != null && acadMentor.getTeacherInfo() != null && acadMentor.getTeacherInfo().getSubRole() != null
                && (acadMentor.getTeacherInfo().getSubRole().contains(SubRole.ACADMENTOR) || acadMentor.getTeacherInfo().getSubRole().contains(SubRole.SUPERMENTOR)) )
        {
            Set<String> studentRequiredFields = new HashSet<>();
            loam_addrequiredFieldsForStudent(studentRequiredFields);
            List<Long> studentIds = loamAmbassadorSubscriptionDAO.loam_getStudentsByAcadMentor(acadMentor.getId(), studentRequiredFields, true);
            return studentIds;
        }
        else
        {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User not acad mentor");
        }
    }

    private User loam_getUserFromEmail(String email) throws VException, UnsupportedEncodingException {
        String url = USER_ENDPOINT_LOAM + "loam_getUserByEmail" + "?email=" + URLEncoder.encode(email, "UTF-8");
        logger.info("Logger: url composed is" + url);
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        User user = new Gson().fromJson(respString, User.class);
        logger.info("Logger: Callresponse user: " + user);
        return user;
    }

    private void loam_addrequiredFieldsForStudent(Set<String> studentRequiredFields) {
        studentRequiredFields.add(AcadMentorStudents.Constants.STUDENTID);
    }


    public List<Batch> loam_getBatchData(Long fromTime, Long thruTime, Integer start, Integer size)
    {
        List<Batch> batches = new ArrayList<>();

        Set<String> requiredFields = new HashSet<>();
       /* //Projection on fields to fetch
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addRequiredFieldsForContentInfo(requiredFields);*/

        batches = loamAmbassadorSubscriptionDAO.loam_getBatchData(fromTime, thruTime,requiredFields,true, start, size);

        return batches;
    }

    public List<Course> loam_getCourseData(Long fromTime, Long thruTime, Integer start, Integer size)
    {
        List<Course> courses = new ArrayList<>();

        Set<String> requiredFields = new HashSet<>();

        /* //Projection on fields to fetch
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addRequiredFieldsForContentInfo(requiredFields);*/

        courses = loamAmbassadorSubscriptionDAO.loam_getCourseData(fromTime, thruTime,requiredFields,true, start, size);

        return courses;
    }

    public List<Enrollment> loam_getEnrollmentData(Long fromTime, Long thruTime, Integer start, Integer size) {
        List<Enrollment> enrollments = new ArrayList<>();

        Set<String> requiredFields = new HashSet<>();
       /* //Projection on fields to fetch
        loam_addAbstractMongoEntityFields(requiredFields);
        loam_addRequiredFieldsForContentInfo(requiredFields);*/

        enrollments = loamAmbassadorSubscriptionDAO.loam_getEnrollmentData(fromTime, thruTime,requiredFields,true, start, size);

        return enrollments;
    }

    private void loam_addAbstractMongoEntityFields(Set<String> requiredFields) {
        requiredFields.add(AbstractMongoEntity.Constants._ID);
        requiredFields.add(AbstractMongoEntity.Constants.CREATION_TIME);
        requiredFields.add(AbstractMongoEntity.Constants.CREATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED);
        requiredFields.add(AbstractMongoEntity.Constants.ID);
        requiredFields.add(AbstractMongoEntity.Constants.LAST_UPDATED_BY);
        requiredFields.add(AbstractMongoEntity.Constants.ENTITY_STATE);
    }

}
