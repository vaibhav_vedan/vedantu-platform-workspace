package com.vedantu.subscription.managers;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapperSingletonWrapper;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.spi.inject.Errors;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.board.pojo.Board;
import com.vedantu.dinero.enums.InstalmentPurchaseEntity;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.pojo.BaseInstalment;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.pojo.Orders;
import com.vedantu.dinero.pojo.PurchasingEntity;
import com.vedantu.dinero.request.BuyItemsReqNew;
import com.vedantu.dinero.request.ContextWiseNextDueInstalmentsReq;
import com.vedantu.dinero.request.ProcessPaymentReq;
import com.vedantu.dinero.request.UpdateDeliverableDataReq;
import com.vedantu.dinero.response.ContextWiseNextDueInstalmentsRes;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchChangeTime;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.EnrollmentStateChangeTime;
import com.vedantu.onetofew.pojo.EnrollmentUpdatePojo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.onetofew.pojo.StudentSlotPreferencePojo;
import com.vedantu.onetofew.request.AddAdvancePaymentOrderIdReq;
import com.vedantu.onetofew.request.AddEditOTFBundleReq;
import com.vedantu.onetofew.request.AddRegistrationReq;
import com.vedantu.onetofew.request.GetInstalmentReq;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.onetofew.request.PurchaseOTFBundleReq;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BundleDAO;
import com.vedantu.subscription.dao.CourseDAO;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.dao.OTFBundleDAO;
import com.vedantu.subscription.dao.RegistrationDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.OTMBundle;
import com.vedantu.subscription.entities.mongo.Registration;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.subscription.response.CheckIfRegFeePaidRes;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.viewobject.request.GetCourseListingReq;
import com.vedantu.subscription.viewobject.response.AggregatedOTMBundles;
import com.vedantu.subscription.viewobject.response.GetCourseListingRes;
import com.vedantu.subscription.viewobject.request.BundleRefundReq;
import com.vedantu.subscription.viewobject.request.EndSubscriptionReq;
import com.vedantu.subscription.viewobject.request.EnrollmentRefundReq;
import com.vedantu.subscription.viewobject.response.AggregatedTargets;
import com.vedantu.subscription.viewobject.request.MakePaymentRequest;
import com.vedantu.subscription.viewobject.response.EndSubscriptionRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.request.GetEnrollmentsReq;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.http.HttpMethod;

import static com.vedantu.util.enums.SQSMessageType.GTT_ENROLMENT_ACTIVATED;

@Service
public class OTFBundleManager {

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    public EnrollmentAsyncManager enrollmentAsyncManager;

    @Autowired
    private CourseDAO courseDAO;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private CourseManager courseManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private RegistrationDAO registrationDAO;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private RegistrationManager registrationManager;

    @Autowired
    private OTFBundleDAO otfBundleDAO;

    @Autowired
    private EnrollmentConsumptionManager enrollmentConsumptionManager;

    @Autowired
    private RedisDAO redisDAO;

    @Autowired
    private InstalmentUtilManager instalmentUtilManager;

    private static Gson gson = new Gson();

    @Autowired
    private LogFactory logFactory;
    
    @Autowired
    private BundleDAO bundleDAO;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFBundleManager.class);

    public static TimeZone indiaTimeZone = TimeZone.getTimeZone("Asia/Kolkata");
    public static final String env = ConfigUtils.INSTANCE.getStringValue("environment");

    private final Mapper mapper = DozerBeanMapperSingletonWrapper.getInstance();
    private static final String DINERO_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("DINERO_ENDPOINT");

    public OTFBundleInfo getOTFBundle(String id) throws VException {
        return getOTFBundle(id, null);
    }

    public OTFBundleInfo getOTFBundle(String id, Long userId) throws VException {
        //userId is to specific data of instalments and more 
        OTMBundle otfBundle = otfBundleDAO.getOTFBundleById(id);
        if (otfBundle == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundle not found " + id);
        }
        OTFBundleInfo otfBundleInfo = mapper.map(otfBundle, OTFBundleInfo.class);
        otfBundleInfo.setCourseInclusionInfos(otfBundle.getCourseInclusionInfos());
        otfBundleInfo.setTestimonials(otfBundle.getTestimonials());
        otfBundleInfo.setScheduleInfos(otfBundle.getScheduleInfos());
        otfBundleInfo.setTeacherInfos(otfBundle.getTeacherInfos());
        BaseInstalment baseInstalment = paymentManager.getBaseInstalment(InstalmentPurchaseEntity.OTF_BUNDLE, id, userId);
        if (baseInstalment != null) {
            otfBundleInfo.setInstalmentDetails(baseInstalment.getInfo());
        }

        List<String> courseIds = new ArrayList<>();
        for (OTMBundleEntityInfo oTMBundleEntityInfo : otfBundle.getEntities()) {
            courseIds.add(oTMBundleEntityInfo.getEntityId());
        }

        List<Course> courses = courseDAO.getCoursesBasicInfos(courseIds);
        Set<Long> boardIds = new HashSet<>();

        for (Course course : courses) {
            for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                boardIds.add(boardTeacherPair.getBoardId());
            }
        }

        Map<Long, Board> subjectMap = fosUtils.getBoardInfoMap(boardIds);
        Set<String> subjects = new HashSet<>();
        for (Long boardId : boardIds) {
            if (subjectMap.get(boardId) != null) {
                subjects.add(subjectMap.get(boardId).getName());
            }
        }

        otfBundleInfo.setCourseNames(new ArrayList<>(subjects));

        return otfBundleInfo;
    }

    public OTFBundleInfo addEditOTFBundle(AddEditOTFBundleReq addEditBundleReq) throws NotFoundException, ForbiddenException, BadRequestException, VException {
        addEditBundleReq.verify();
        Set<String> courseIds = new HashSet<>();
        Set<String> batchIds = new HashSet<>();
        validateBaseInstalment(addEditBundleReq.getInstalmentDetails());
        OTMBundle otfBundle = mapper.map(addEditBundleReq, OTMBundle.class);
        if (StringUtils.isNotEmpty(addEditBundleReq.getId())) {
            OTMBundle otfBundleExisting = otfBundleDAO.getOTFBundleById(addEditBundleReq.getId());
            if (otfBundleExisting == null) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "bundle not found " + addEditBundleReq.getId());
            }
            otfBundle.setCreatedBy(otfBundleExisting.getCreatedBy());
            otfBundle.setCreationTime(otfBundleExisting.getCreationTime());
        }

        otfBundle.setCourseInclusionInfos(addEditBundleReq.getCourseInclusionInfos());
        otfBundle.setScheduleInfos(addEditBundleReq.getScheduleInfos());
        otfBundle.setTeacherInfos(addEditBundleReq.getTeacherInfos());
        otfBundle.setTestimonials(addEditBundleReq.getTestimonials());
        if (ArrayUtils.isNotEmpty(otfBundle.getEntities())) {
            for (OTMBundleEntityInfo entityInfo : otfBundle.getEntities()) {
                if (!(EntityType.OTF_COURSE.equals(entityInfo.getEntityType()) || EntityType.OTF.equals(entityInfo.getEntityType()))) {
                    throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "Only course and batches allowed.");
                }
                if (EntityType.OTF_COURSE.equals(entityInfo.getEntityType()) && StringUtils.isNotEmpty(entityInfo.getEntityId())) {
                    courseIds.add(entityInfo.getEntityId());
                }
                if (EntityType.OTF.equals(entityInfo.getEntityType()) && StringUtils.isNotEmpty(entityInfo.getEntityId())) {
                    batchIds.add(entityInfo.getEntityId());
                }
            }
            if (ArrayUtils.isNotEmpty(courseIds)) {
                Query query = new Query();
                query.addCriteria(Criteria.where(Course.Constants.ID).in(courseIds));
                List<Course> courses = courseDAO.runQuery(query, Course.class);
                if (ArrayUtils.isNotEmpty(courses)) {
                    for (Course basicInfo : courses) {
                        if (courseIds.contains(basicInfo.getId())) {
                            courseIds.remove(basicInfo.getId());
                        }
                    }
                }
                if (courseIds.size() > 0) {
                    throw new NotFoundException(ErrorCode.COURSE_NOT_FOUND, "courses not found : " + courseIds.toArray().toString());
                }
            }

            if (ArrayUtils.isNotEmpty(batchIds)) {
                Query query = new Query();
                query.addCriteria(Criteria.where(Batch.Constants.ID).in(batchIds));
                List<Batch> batches = batchDAO.runQuery(query, Batch.class);
                if (ArrayUtils.isNotEmpty(batches)) {
                    for (Batch batch : batches) {
                        if (batchIds.contains(batch.getId())) {
                            batchIds.remove(batch.getId());
                        }
                    }
                }

                if (batchIds.size() > 0) {
                    throw new NotFoundException(ErrorCode.BATCH_NOT_FOUND, "Batch not found : " + batchIds.toArray().toString());
                }

            }

        }
        // toOTFBundle(otfBundle, addEditBundleReq);
        otfBundleDAO.save(otfBundle, addEditBundleReq.getCallingUserId());
        logger.info("Bundle package post update:" + otfBundle.toString());
        OTFBundleInfo otfBundleInfo = mapper.map(otfBundle, OTFBundleInfo.class);
        otfBundleInfo.setCourseInclusionInfos(otfBundle.getCourseInclusionInfos());
        otfBundleInfo.setTestimonials(otfBundle.getTestimonials());
        otfBundleInfo.setScheduleInfos(otfBundle.getScheduleInfos());
        otfBundleInfo.setTeacherInfos(otfBundle.getTeacherInfos());
        List<BaseInstalmentInfo> baseInstalmentInfos = addEditBundleReq.getInstalmentDetails();
        BaseInstalment baseInstalment = new BaseInstalment(InstalmentPurchaseEntity.OTF_BUNDLE, otfBundleInfo.getId(),
                baseInstalmentInfos);
        paymentManager.updateBaseInstalment(baseInstalment);
        otfBundleInfo.setInstalmentDetails(baseInstalmentInfos);

        return otfBundleInfo;
    }

    public void validateBaseInstalment(List<BaseInstalmentInfo> baseInstalmentInfos) throws BadRequestException {
        int count = 0;

        if (baseInstalmentInfos == null) {
            return;
        }

        Set<Long> installmentDueTimes = new HashSet<>();
        for (BaseInstalmentInfo baseInstalmentInfo : baseInstalmentInfos) {
            if (count != 0 && (baseInstalmentInfo.getDueTime() == null || baseInstalmentInfo.getDueTime() <= 0L)) {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Due date must be specified");
            }
            installmentDueTimes.add(baseInstalmentInfo.getDueTime());
            count++;
        }
        if (installmentDueTimes.size() != baseInstalmentInfos.size()) {
            throw new BadRequestException(ErrorCode.SAME_DUE_TIME_FOR_INSTALMENTS_NOT_ALLOWED, "Due time can't be same for two installments");
        }
    }

    public void toOTFBundle(OTMBundle otfBundle, AddEditOTFBundleReq addEditOTFBundleReq) {
        otfBundle.setEntities(addEditOTFBundleReq.getEntities());
        otfBundle.setBoardIds(addEditOTFBundleReq.getBoardIds());
        otfBundle.setDescription(addEditOTFBundleReq.getDescription());
        otfBundle.setFeatured(addEditOTFBundleReq.isFeatured());
        otfBundle.setGrade(addEditOTFBundleReq.getGrade());
        otfBundle.setTitle(addEditOTFBundleReq.getTitle());
        otfBundle.setTarget(addEditOTFBundleReq.getTarget());
        otfBundle.setKeyValues(addEditOTFBundleReq.getKeyValues());
        otfBundle.setPrice(addEditOTFBundleReq.getPrice());
        otfBundle.setTags(addEditOTFBundleReq.getTags());
        otfBundle.setPriority(addEditOTFBundleReq.getPriority());
        otfBundle.setRegistrationFee(addEditOTFBundleReq.getRegistrationFee());
        otfBundle.setAllowedSlots(addEditOTFBundleReq.getAllowedSlots());
    }

    public List<OTFBundleInfo> getOTFBundleInfos(GetOTFBundlesReq req) throws VException {
        List<OTFBundleInfo> response = new ArrayList<>();
        List<OTMBundle> otfBundles = otfBundleDAO.getOTFBundles(req);
        if (CollectionUtils.isNotEmpty(otfBundles)) {
            List<String> otfBundleIds = new ArrayList<>();
            for (OTMBundle otfBundle : otfBundles) {
                otfBundleIds.add(otfBundle.getId());
                OTFBundleInfo otfBundleInfo = mapper.map(otfBundle, OTFBundleInfo.class);
                otfBundleInfo.setCourseInclusionInfos(otfBundle.getCourseInclusionInfos());
                otfBundleInfo.setTestimonials(otfBundle.getTestimonials());
                otfBundleInfo.setScheduleInfos(otfBundle.getScheduleInfos());
                otfBundleInfo.setTeacherInfos(otfBundle.getTeacherInfos());
                response.add(otfBundleInfo);
            }

            List<BaseInstalment> baseInstalments = paymentManager.
                    getBaseInstalments(InstalmentPurchaseEntity.OTF_BUNDLE, otfBundleIds);
            if (baseInstalments != null && !baseInstalments.isEmpty()) {
                Map<String, BaseInstalment> baseInstalmentMap = new HashMap<>();
                for (BaseInstalment baseInstalment : baseInstalments) {
                    baseInstalmentMap.put(baseInstalment.getPurchaseEntityId(), baseInstalment);
                }
                for (OTFBundleInfo otfBundleInfo : response) {
                    BaseInstalment baseInstalment = baseInstalmentMap.get(otfBundleInfo.getId());
                    if (baseInstalment != null) {
                        otfBundleInfo.setInstalmentDetails(baseInstalment.getInfo());
                    }
                }

            }

        }
        return response;
    }

    public List<OTFBundleInfo> getOTFBundleInfosForBatch(GetOTFBundlesReq req) throws VException {
        List<OTFBundleInfo> response = new ArrayList<>();
        List<OTMBundle> otfBundles = otfBundleDAO.getOTFBundlesForBatch(req);
        if (CollectionUtils.isNotEmpty(otfBundles)) {
            List<String> otfBundleIds = new ArrayList<>();
            for (OTMBundle otfBundle : otfBundles) {
                otfBundleIds.add(otfBundle.getId());
                OTFBundleInfo otfBundleInfo = mapper.map(otfBundle, OTFBundleInfo.class);
                otfBundleInfo.setCourseInclusionInfos(otfBundle.getCourseInclusionInfos());
                otfBundleInfo.setTestimonials(otfBundle.getTestimonials());
                otfBundleInfo.setScheduleInfos(otfBundle.getScheduleInfos());
                otfBundleInfo.setTeacherInfos(otfBundle.getTeacherInfos());
                response.add(otfBundleInfo);
            }

            List<BaseInstalment> baseInstalments = paymentManager.
                    getBaseInstalments(InstalmentPurchaseEntity.OTF_BUNDLE, otfBundleIds);
            if (baseInstalments != null && !baseInstalments.isEmpty()) {
                Map<String, BaseInstalment> baseInstalmentMap = new HashMap<>();
                for (BaseInstalment baseInstalment : baseInstalments) {
                    baseInstalmentMap.put(baseInstalment.getPurchaseEntityId(), baseInstalment);
                }
                for (OTFBundleInfo otfBundleInfo : response) {
                    BaseInstalment baseInstalment = baseInstalmentMap.get(otfBundleInfo.getId());
                    if (baseInstalment != null) {
                        otfBundleInfo.setInstalmentDetails(baseInstalment.getInfo());
                    }
                }

            }

        }
        return response;
    }

    public OrderInfo purchaseAndRegisterOTFBundle(PurchaseOTFBundleReq req) throws VException, CloneNotSupportedException {
        req.verify();
        String otfBundleId = req.getOtfBundleId();

        logger.info("finding the amount to pay");
        OTFBundleInfo otfBundleInfo = getOTFBundle(otfBundleId);
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "bundlepackage not found");
        }

        if (checkIfAlreadyEnrolled(req.getUserId(), otfBundleInfo)) {
            throw new ConflictException(ErrorCode.ALREADY_ENROLLED, "User already enrolled in one of the courses");
        }

        List<Registration> registrations = registrationDAO.getRegistrations(null, otfBundleId, RegistrationStatus.REGISTERED, req.getUserId());

        if (ArrayUtils.isNotEmpty(registrations)) {
            throw new ConflictException(ErrorCode.ALREADY_REGISTERED, "user already registered for otfbundleId");
        }

        // TODO check for availability for trial slots and throw error
        // TODO check for availability for regular slots and throw error
        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();

        if (otfBundleInfo.getRegistrationFee() == null) {
            throw new BadRequestException(ErrorCode.REGISTRATION_FEE_NOT_FOUND, "No registration fee for bundle");
        }

        int amountToPay = 0;
        amountToPay = otfBundleInfo.getRegistrationFee();
        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(otfBundleId);
        buyItemsReqNew.setEntityType(EntityType.OTM_BUNDLE_REGISTRATION);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        buyItemsReqNew.setPaymentType(PaymentType.BULK);
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());
//        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);

        List<PurchasingEntity> entities = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(otfBundleInfo.getEntities())) {
            for (OTMBundleEntityInfo otfEntity : otfBundleInfo.getEntities()) {
                PurchasingEntity entity = new PurchasingEntity(otfEntity.getEntityType(), otfEntity.getEntityId());
                entity.setPrice(otfEntity.getPrice());
                entities.add(entity);
            }
        }
        buyItemsReqNew.setPurchasingEntities(entities);
//	        buyItemsReqNew.setPurchasingEntities(req.getPurchasingEntities());

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        StudentSlotPreferencePojo pojo = new StudentSlotPreferencePojo();
        pojo.setUserId(req.getUserId().toString());
        pojo.setEntityType(EntityType.OTM_BUNDLE_REGISTRATION);
        pojo.setEntityId(otfBundleId);
        pojo.setPreferredSlots(req.getPreferredSlots());

        if (!orderInfo.getNeedRecharge()) {
            processOTFBundlePurchaseAfterRegFeePayment(orderInfo.getId(), orderInfo.getUserId(),
                    otfBundleInfo, req.getPurchasingEntities());
            pojo.setPostPayment(Boolean.TRUE);
        } else {
            pojo.setPostPayment(Boolean.FALSE);
        }
        if (ArrayUtils.isNotEmpty(req.getPreferredSlots())) {
            courseManager.updateStudentPreferenceSessionSlot(pojo);
        }

        return orderInfo;
    }

    public void processOTFBundlePurchaseAfterRegFeePayment(String orderId, Long userId,
            OTFBundleInfo otfBundleInfo, List<PurchasingEntity> entitiesToBuy)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);

        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", userId);
        payload.put("otfBundleInfo", otfBundleInfo);
        float paidAmount = (res.getAmount() == null ? 0 : res.getAmount() / 100);
        payload.put("paidAmount", paidAmount);
        payload.put("communicationType", CommunicationType.POST_OTM_BUNDLE_REGISTRATION);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.POST_OTM_BUNDLE_REGISTRATION_EMAIL, payload, DateTimeUtils.MILLIS_PER_MINUTE * 1l);
        asyncTaskFactory.executeTask(params);

        StudentSlotPreferencePojo pojo = new StudentSlotPreferencePojo();
        pojo.setUserId(userId.toString());
        pojo.setEntityType(EntityType.OTM_BUNDLE_REGISTRATION);
        pojo.setEntityId(otfBundleInfo.getId());
        pojo.setPostPayment(Boolean.TRUE);
        courseManager.updateStudentPreferenceSessionSlot(pojo);

        AddRegistrationReq addRegistrationReq = new AddRegistrationReq();
        addRegistrationReq.setEntityType(EntityType.OTM_BUNDLE_REGISTRATION);
        addRegistrationReq.setEntityId(otfBundleInfo.getId());
        addRegistrationReq.setBulkPriceToPay(otfBundleInfo.getPrice());
        addRegistrationReq.setUserId(userId);
        addRegistrationReq.setOrderId(orderId);
        addRegistrationReq.setEntities(otfBundleInfo.getEntities());
        registrationManager.addRegistration(addRegistrationReq);
    }

    public OrderInfo purchaseOTFBundleByAdvancePayment(PurchaseOTFBundleReq req) throws VException {
        req.verify();
        String otfBundleId = req.getOtfBundleId();

        logger.info("finding the amount to pay");
        OTFBundleInfo otfBundleInfo = getOTFBundle(otfBundleId);
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "bundlepackage not found");
        }

        checkIfRegFeePaid(req, otfBundleInfo);
        List<Registration> registrations = registrationManager.getRegistrations(null, otfBundleId, RegistrationStatus.ENROLLED, req.getUserId());

        if (ArrayUtils.isNotEmpty(registrations)) {
            throw new ConflictException(ErrorCode.ADVANCE_PAYMENT_NOT_ALLOWED, "Enrollment already done. id : " + otfBundleId);
        }

        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();
        if (otfBundleInfo.getPrice() == null) {
            logger.error("price is null in bundlepackage : " + otfBundleId);
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bundle price is null");
        }
        int amountToPay = otfBundleInfo.getPrice();

        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(otfBundleId);
        buyItemsReqNew.setEntityType(EntityType.OTM_BUNDLE_ADVANCE_PAYMENT);
//        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());
        if (req.getPaymentType() == null) {
            req.setPaymentType(PaymentType.BULK);
        }
        buyItemsReqNew.setPaymentType(req.getPaymentType());
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());

        List<PurchasingEntity> entities = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(otfBundleInfo.getEntities())) {
            for (OTMBundleEntityInfo otfEntity : otfBundleInfo.getEntities()) {
                PurchasingEntity entity = new PurchasingEntity(otfEntity.getEntityType(), otfEntity.getEntityId());
                entity.setPrice(otfEntity.getPrice());
                entities.add(entity);
            }
        }
        buyItemsReqNew.setPurchasingEntities(entities);

        if (PaymentType.INSTALMENT.equals(req.getPaymentType())) {
            throw new BadRequestException(ErrorCode.INSTALMENT_NOT_FOUND, "Instalment not allowed");
        }
        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        if (!orderInfo.getNeedRecharge()) {
            processOtfBundlePurchaseAfterAdvancePayment(orderInfo.getId(), orderInfo.getUserId(), otfBundleInfo, req.getPurchasingEntities());
        }
        return orderInfo;
    }

    public CheckIfRegFeePaidRes checkIfRegFeePaid(PurchaseOTFBundleReq req) throws VException {
        req.verify();
        String otfBundleId = req.getOtfBundleId();

        logger.info("finding the amount to pay");
        OTFBundleInfo otfBundleInfo = getOTFBundle(otfBundleId);
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "otf bundle not found");
        }

        return checkIfRegFeePaid(req, otfBundleInfo);
    }

    public void processOtfBundlePurchaseAfterAdvancePayment(String orderId, Long userId, OTFBundleInfo otfBundleInfo, List<PurchasingEntity> entitiesToBuy)
            throws VException {
        logger.info("making call for dinero operations");
        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        processPaymentReq.setPurchaseEntityId(otfBundleInfo.getId());
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);

        AddAdvancePaymentOrderIdReq addAdvancePaymentOrderIdReq = new AddAdvancePaymentOrderIdReq();
        addAdvancePaymentOrderIdReq.setEntityType(EntityType.OTM_BUNDLE_REGISTRATION);
        addAdvancePaymentOrderIdReq.setEntityId(otfBundleInfo.getId());
        addAdvancePaymentOrderIdReq.setUserId(userId);
        addAdvancePaymentOrderIdReq.setOrderId(orderId);
        registrationManager.addAdvancePaymentOrderId(addAdvancePaymentOrderIdReq);

        //Email about it
        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", userId);
        payload.put("otfBundleInfo", otfBundleInfo);
        float paidAmount = (res.getAmount() == null ? 0 : res.getAmount() / 100);
        payload.put("paidAmount", paidAmount);
        payload.put("communicationType", CommunicationType.POST_OTM_BUNDLE_PURCHASE);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.POST_OTM_BUNDLE_REGISTRATION_EMAIL, payload, DateTimeUtils.MILLIS_PER_MINUTE * 1l);
        asyncTaskFactory.executeTask(params);

    }

    public CheckIfRegFeePaidRes checkIfRegFeePaid(PurchaseOTFBundleReq req, OTFBundleInfo otfBundleInfo) throws ForbiddenException {
        ClientResponse resp = WebUtils.INSTANCE.doCall(
                DINERO_ENDPOINT + "/payment/getOrdersRegisteredWithoutDeliverableEntityIdNonRefunded?entityId="
                + req.getOtfBundleId() + "&userId=" + req.getUserId() + "&entityType=OTM_BUNDLE_REGISTRATION",
                HttpMethod.GET, null);
        String listResp = resp.getEntity(String.class);
        Type listType = new TypeToken<ArrayList<Orders>>() {
        }.getType();
        List<Orders> orders = gson.fromJson(listResp, listType);
        CheckIfRegFeePaidRes res = new CheckIfRegFeePaidRes();
        String orderId = null;
        if (ArrayUtils.isEmpty(orders)) {
            if (otfBundleInfo.getRegistrationFee() != null) {
                throw new ForbiddenException(ErrorCode.NOT_REGISTERED, "Bundle Registration Fee not paid");
            }
        } else {
            orderId = orders.get(0).getId();
        }
        res.setOrderId(orderId);
        res.setRegFeePaid(true);
        return res;
    }

    public boolean checkIfBatchBundle(OTFBundleInfo oTFBundleInfo) {
        List<OTMBundleEntityInfo> oTMBundleEntityInfos = oTFBundleInfo.getEntities();

        if (ArrayUtils.isNotEmpty(oTMBundleEntityInfos)) {
            if (EntityType.OTF.equals(oTMBundleEntityInfos.get(0).getEntityType())) {
                return true;
            }
        }

        return false;
    }

    public boolean checkIfAlreadyEnrolledInCourse(Long userId, OTFBundleInfo oTFBundleInfo) {

        List<EntityStatus> statuses = new ArrayList<>();
        statuses.add(EntityStatus.ACTIVE);
        statuses.add(EntityStatus.INACTIVE);
        if (ArrayUtils.isNotEmpty(oTFBundleInfo.getEntities())) {
            for (OTMBundleEntityInfo oTMBundleEntityInfo : oTFBundleInfo.getEntities()) {
                Batch batch = batchDAO.getById(oTMBundleEntityInfo.getEntityId());

                if (batch == null) {
                    continue;
                }

                List<Enrollment> enrollments = enrollmentDAO.getEnrollmentByCourseAndUser(batch.getCourseId(), userId.toString(), statuses);

                if (ArrayUtils.isNotEmpty(enrollments)) {
                    return true;
                }

                enrollments = enrollmentDAO.getEnrollmentByBatchAndUser(batch.getId(), userId.toString(), statuses);

                if (ArrayUtils.isNotEmpty(enrollments)) {
                    return true;
                }

            }
        }

        return false;
    }

    public OrderInfo purchaseOTMBundle(PurchaseOTFBundleReq req) throws VException {
    	// Verify that the request is a valid one
        req.verify();
        
        /*
         * Commented out as the same validation was added in the request for efficiency.
        if (req.getPaymentType() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Payment Type not found");
        }
        */
        
        /*
         * Check if the OTF bundle you want to find exists or not, if not you can't make payment,
         * so better to throw an error saying it doesn't exist
         */
        String otfBundleId = req.getOtfBundleId();
        String purchaseContextId=req.getPurchaseContextId(); 

        if(purchaseContextId!=null) {
        	verifyIfOTMBundleIdInBundle(otfBundleId,purchaseContextId);
        	
        }

        logger.info("finding the amount to pay");
        OTFBundleInfo otfBundleInfo = getOTFBundle(otfBundleId, req.getUserId());
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND, "bundlepackage not found");
        }
        List<BaseInstalmentInfo> instalments = null;
        int bulkPrice = 0;
        /*
         * As per the logic, if a content inside a OTM bundle is not a batch then it is needed that a registration fees
         * need to be paid, the following `if` statement checks for the following. 
         */
        if (!checkIfBatchBundle(otfBundleInfo)) {
            checkIfRegFeePaid(req, otfBundleInfo);
            List<Registration> registrations = registrationManager.getRegistrations(null, otfBundleId, RegistrationStatus.ENROLLED, req.getUserId());

            if (ArrayUtils.isNotEmpty(registrations)) {
                //sorting in desc by creation time
                Collections.sort(registrations, new Comparator<Registration>() {
                    @Override
                    public int compare(Registration o1, Registration o2) {
                        return o2.getCreationTime().compareTo(o1.getCreationTime());
                    }
                });
                if (ArrayUtils.isNotEmpty(registrations.get(0).getInstalmentDetails())) {
                    instalments = registrations.get(0).getInstalmentDetails();
                }
                if (registrations.get(0).getBulkPriceToPay() != null && registrations.get(0).getBulkPriceToPay() > 0) {
                    bulkPrice = registrations.get(0).getBulkPriceToPay();
                }
            } else {
                throw new ForbiddenException(ErrorCode.NOT_REGISTERED, "Bundle Registration does not exist");
            }
        } else {
        	/*
        	 * It checks for if a user is already enrolled in a course or batch individually and throws 
        	 * an error accordingly.
        	 */
            if (checkIfAlreadyEnrolledInCourse(req.getUserId(), otfBundleInfo)) {
                throw new BadRequestException(ErrorCode.ALREADY_ENROLLED, "User already enrolled in one of the courses");
            }
        }

        if (ArrayUtils.isEmpty(instalments)) {
            instalments = otfBundleInfo.getInstalmentDetails();
        }
        if (bulkPrice <= 0) {
            bulkPrice = otfBundleInfo.getPrice();
        }

        if (checkIfAlreadyEnrolledInCourse(req.getUserId(), otfBundleInfo)) {
            throw new BadRequestException(ErrorCode.ALREADY_ENROLLED, "User already enrolled in one of the courses");
        }

        BuyItemsReqNew buyItemsReqNew = new BuyItemsReqNew();
        if (otfBundleInfo.getPrice() == null) {
            logger.error("price is null in bundlepackage : " + otfBundleId);
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bundle price is null");
        }
        int amountToPay = bulkPrice;

        logger.info("asking dinero if the user has paid or does he have balance to make payment of " + amountToPay);
        buyItemsReqNew.setAmountToPay(amountToPay);
        buyItemsReqNew.setEntityId(otfBundleId);
        buyItemsReqNew.setEntityType(EntityType.OTM_BUNDLE);
//        buyItemsReqNew.setThrowErrorIfAlreadyPaid(Boolean.TRUE);
        buyItemsReqNew.setUserId(req.getUserId());
        buyItemsReqNew.setRedirectUrl(req.getRedirectUrl());
        buyItemsReqNew.setVedantuDiscountCouponId(req.getVedantuDiscountCouponId());

        buyItemsReqNew.setPaymentType(req.getPaymentType());
        buyItemsReqNew.setIpAddress(req.getIpAddress());
        buyItemsReqNew.setUserAgent(req.getUserAgent());
        
        List<PurchasingEntity> entities = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(otfBundleInfo.getEntities())) {
            for (OTMBundleEntityInfo otfEntity : otfBundleInfo.getEntities()) {
                PurchasingEntity entity = new PurchasingEntity(otfEntity.getEntityType(), otfEntity.getEntityId());
                entity.setPrice(otfEntity.getPrice());
                entities.add(entity);
            }
        }
        buyItemsReqNew.setPurchasingEntities(entities); //to be set everytime
        if (PaymentType.INSTALMENT.equals(req.getPaymentType())
                && ArrayUtils.isEmpty(instalments)) {
            throw new BadRequestException(ErrorCode.NO_BASE_INSTALMENTS_FOUND, "No base instalments found");
        }
        buyItemsReqNew.setBaseInstalmentInfos(instalments);

        OrderInfo orderInfo = paymentManager.checkAndGetAccountInfoForPayment(buyItemsReqNew);
        if (!orderInfo.getNeedRecharge()) {
            processOtmBundlePurchaseAfterPayment(orderInfo.getId(), orderInfo.getUserId(), otfBundleInfo, req.getPurchasingEntities(),req.getPurchaseContextType(), purchaseContextId,
                    req.getPurchaseContextEnrollmentType());
        }
        return orderInfo;
    }

    private void verifyIfOTMBundleIdInBundle(String otfBundleId, String purchaseContextId) throws BadRequestException {
    	List<Bundle> bundles=bundleDAO.getBundleByIdAndOTMBundleId(purchaseContextId, otfBundleId);
    	if(bundles==null || bundles.size()==0) {
    		throw new BadRequestException(ErrorCode.BUNDLE_PACKAGE_NOT_FOUND,"The OTF bundle is not present in the Bundle(AIO)", Level.ERROR);
    	}
	}

	public boolean checkIfAlreadyEnrolledForBundleBatch(List<Enrollment> enrollments, String bundleId) {

        if (ArrayUtils.isEmpty(enrollments)) {
            return false;
        }

        for (Enrollment enrollment : enrollments) {
            if (!EntityStatus.ENDED.equals(enrollment.getStatus())) {
                return true;
            }
        }
        return false;
    }

    public void processOtmBundlePurchaseAfterPayment(String orderId, Long userId, OTFBundleInfo otfBundleInfo, List<PurchasingEntity> entitiesToBuy)
            throws VException {
        processOtmBundlePurchaseAfterPayment(orderId,userId,otfBundleInfo,entitiesToBuy,null,null,null);
    }

    public void processOtmBundlePurchaseAfterPayment(String orderId, Long userId,OTFBundleInfo otfBundleInfo, List<PurchasingEntity> entitiesToBuy, EnrollmentPurchaseContext purchaseContextType, String purchaseContextId, EnrollmentType purchaseContextEnrollmentType)
            throws VException {

        logger.info("making call for dinero operations");

        ProcessPaymentReq processPaymentReq = new ProcessPaymentReq();
        processPaymentReq.setUserId(userId);
        processPaymentReq.setOrderId(orderId);
        processPaymentReq.setPurchaseEntityType(InstalmentPurchaseEntity.OTF_BUNDLE);
        processPaymentReq.setPurchaseEntityId(otfBundleInfo.getId());
        OrderInfo res = paymentManager.processOrderAfterPayment(orderId, processPaymentReq);
        logger.info(res);

        logger.info("userId " + userId);
        logger.info("otfBundleInfoId  " + otfBundleInfo.getId());
        boolean newEnrollmentsCreated = markEnrollmentPostPayment(userId, otfBundleInfo.getId());

        GetEnrollmentsReq req = new GetEnrollmentsReq();
        req.setEntityType(EntityType.OTM_BUNDLE);
        req.setEntityId(otfBundleInfo.getId());
        req.setUserId(userId);
        req.setStatuses(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
        List<Enrollment> enrollments = enrollmentDAO.getEnrollments(req);
        Map<String, Enrollment> courseEnrollmentMap = new HashMap<>();

        if (checkIfBatchBundle(otfBundleInfo)) {
            if (!checkIfAlreadyEnrolledForBundleBatch(enrollments, otfBundleInfo.getId())) {
                enrollments = enrollmentManager.createEnrollmentForBatchBundle(userId, res, otfBundleInfo,purchaseContextType, purchaseContextId,
                        purchaseContextEnrollmentType);
            } else {
                logger.error("Non ended Enrollments already there for userId: " + userId + " and bundleId: " + otfBundleInfo.getId());
                throw new BadRequestException(ErrorCode.ALREADY_ENROLLED, "User already enrolled for bundle: " + otfBundleInfo);
            }

            for (Enrollment enrollment : enrollments) {
                if (courseEnrollmentMap.containsKey((enrollment.getBatchId()))) {
                    logger.error("Multiple enrollments for userId: " + userId + " and batchId: " + enrollment.getBatchId());
                    continue;
                }
                courseEnrollmentMap.put(enrollment.getBatchId(), enrollment);
            }

        } else {

            for (Enrollment enrollment : enrollments) {
                if (courseEnrollmentMap.containsKey(enrollment.getCourseId())) {
                    logger.error("Multiple enrollments for userId: " + userId + " and courseId: " + enrollment.getCourseId());
                    continue;
                }
                courseEnrollmentMap.put(enrollment.getCourseId(), enrollment);
            }
        }
        int discountTotal = 0;
        if (ArrayUtils.isNotEmpty(res.getOrderedItems())) {
            if (res.getOrderedItems().get(0).getVedantuDiscountAmountClaimed() != null) {
                discountTotal = res.getOrderedItems().get(0).getVedantuDiscountAmountClaimed();
            }
        }
        int amountPTotal = 0;
        int amountNPTotal = 0;
        int amountDiscountTotal = 0;
        for (int i = 0; i < otfBundleInfo.getEntities().size(); i++) {

            OTMBundleEntityInfo oTMBundleEntityInfo = otfBundleInfo.getEntities().get(i);
            MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
            float ratio = (float) oTMBundleEntityInfo.getPrice() / otfBundleInfo.getPrice();
            if (PaymentType.BULK.equals(res.getPaymentType())) {
                makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.BULK_PAYMENT);
            } else {
                makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.INSTALLMENT_PAYMENT);
            }
            if (i < otfBundleInfo.getEntities().size() - 1) {
                int amountPToPay = (int) (ratio * res.getPromotionalAmount());
                int amountNPToPay = (int) (ratio * res.getNonpromotionalAmount());
                int amountDiscount = (int) (ratio * discountTotal);
                amountPTotal += amountPToPay;
                amountNPTotal += amountNPToPay;
                amountDiscountTotal += amountDiscount;

                makePaymentRequest.setAmt(amountPToPay + amountNPToPay);
                makePaymentRequest.setAmtP(amountPToPay);
                makePaymentRequest.setAmtNP(amountNPToPay);
                makePaymentRequest.setAmtToBePaid(oTMBundleEntityInfo.getPrice());
                makePaymentRequest.setAmtFromDiscount(amountDiscount);

            } else {

                int amountPToPay = res.getPromotionalAmount() - amountPTotal;
                int amountNPToPay = res.getNonpromotionalAmount() - amountNPTotal;
                int amountDiscount = discountTotal - amountDiscountTotal;

                makePaymentRequest.setAmt(amountPToPay + amountNPToPay);
                makePaymentRequest.setAmtP(amountPToPay);
                makePaymentRequest.setAmtNP(amountNPToPay);
                makePaymentRequest.setAmtToBePaid(oTMBundleEntityInfo.getPrice());
                makePaymentRequest.setAmtFromDiscount(amountDiscount);

            }

            Enrollment enrollment = courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId());
            if (enrollment == null) {
                logger.error("Enrollment not found for course: " + oTMBundleEntityInfo.getEntityId());
                continue;
            }
            makePaymentRequest.setCourseId(enrollment.getCourseId());
            makePaymentRequest.setBatchId(enrollment.getBatchId());
            makePaymentRequest.setEnrollmentId(enrollment.getId());
            makePaymentRequest.setOrderId(res.getId());

            try {
                boolean result = enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, ratio);
                if (!result) {
                    logger.error("EnrollmentConsumption update failed after registration for: " + enrollment.getId());
                }
            } catch (Exception ex) {
                logger.error(ex.getMessage());
                logger.error("EnrollmentConsumption Update failed after registration for: " + enrollment.getId());
            }

        }

        Map<String, Object> payload2 = new HashMap<>();
        payload2.put("userId", userId);

        payload2.put("orderId", orderId);
        payload2.put("entityId", otfBundleInfo.getId());

        payload2.put("bundleInfo", otfBundleInfo);
        AsyncTaskParams params2 = new AsyncTaskParams(AsyncTaskName.UPDATE_DELIVERABLE_ENTITY_ID_ON_PAYMENT, payload2);
        asyncTaskFactory.executeTask(params2);

        //Email about it
        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", userId);
        payload.put("otfBundleInfo", otfBundleInfo);
        Integer amountPaid = res.getAmount();
        if (PaymentType.INSTALMENT.equals(res.getPaymentType())) {
            amountPaid = res.getAmountPaid();
        }
        payload.put("paymentType", res.getPaymentType());
        payload.put("amountPaid", amountPaid);
        payload.put("newEnrollmentsCreated", newEnrollmentsCreated);
        payload.put("orderId", res.getId());
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTM_BUNDLE_ENROLLMENT_POST_BULK_OR_INSTALMENT_PAYMENT_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

    }

    public boolean markEnrollmentPostPayment(Long userId, String entityId) throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(entityId));

        logger.info("query " + query);
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        boolean newEnrollmentsCreated = false;
        logger.info(enrollments);
        if (ArrayUtils.isNotEmpty(enrollments)) {
            Map<String, Enrollment> oldEnrollmentMap = new HashMap<>();
            Map<String, Enrollment> newEnrollmentMap = new HashMap<>();
            boolean triggerEnrollments = Boolean.FALSE;
            boolean updateEnrollments = Boolean.FALSE;
            for (Enrollment enrollment : enrollments) {

                if (EnrollmentState.TRIAL.equals(enrollment.getState())) {
                    EnrollmentStateChangeTime stateChangeTime = new EnrollmentStateChangeTime();
                    stateChangeTime.setChangeTime(System.currentTimeMillis());
                    stateChangeTime.setNewState(EnrollmentState.REGULAR);
                    stateChangeTime.setPreviousState(EnrollmentState.TRIAL);
                    List<EnrollmentStateChangeTime> tempStateChange = enrollment.getStateChangeTime();
                    tempStateChange.add(stateChangeTime);
                    enrollment.setStateChangeTime(tempStateChange);
                    enrollment.setState(EnrollmentState.REGULAR);
                    updateEnrollments = Boolean.TRUE;
                    logger.info("setting updateEnrollments to true");
                }
                if (EntityStatus.INACTIVE.equals(enrollment.getStatus())) {
                    oldEnrollmentMap.put(enrollment.getId(), enrollment);
                    updateEnrollments = Boolean.TRUE;
                    triggerEnrollments = Boolean.TRUE;
                    enrollment.setStatus(EntityStatus.ACTIVE);
                    StatusChangeTime statusChangeTime = new StatusChangeTime();
                    statusChangeTime.setChangeTime(System.currentTimeMillis());
                    statusChangeTime.setNewStatus(EntityStatus.ACTIVE);
                    statusChangeTime.setPreviousStatus(EntityStatus.INACTIVE);
                    if (ArrayUtils.isEmpty(enrollment.getStatusChangeTime())) {
                        enrollment.setStatusChangeTime(Arrays.asList(statusChangeTime));
                    } else {
                        enrollment.getStatusChangeTime().add(statusChangeTime);
                    }
                }

                newEnrollmentMap.put(enrollment.getId(), enrollment);
                if (updateEnrollments) {
                    enrollmentDAO.save(enrollment);
                    updateEnrollments = Boolean.FALSE;

                    // create gtt for enrollment status changed from inactive to active
                    enrollmentManager.createPastGttForEnrolment(enrollment);
                }
            }

            logger.info("triggerEnrollments " + triggerEnrollments);
            if (triggerEnrollments) {
                Map<String, Object> payload = new HashMap<>();
                payload.put("enrollment", newEnrollmentMap.values().iterator().next());
                AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.ENROLLMENT_REACTIVATED, payload);
                asyncTaskFactory.executeTask(params);
                enrollmentTriggers(newEnrollmentMap.keySet(), oldEnrollmentMap, newEnrollmentMap,true);
            }

        } else {
            //TODO create enrollments if the registration flow is not there
            newEnrollmentsCreated = true;
        }
        return newEnrollmentsCreated;
    }

    public void enrollmentTriggers(Set<String> keys, Map<String, Enrollment> oldEnrollmentMap, Map<String, Enrollment> newEnrollmentMap,Boolean withAsync) {
        List<Enrollment> enrollments = new ArrayList<>();
        logger.info("keys " + keys);
        logger.info("oldEnrollmentMap " + oldEnrollmentMap);
        logger.info("newEnrollmentMap " + newEnrollmentMap);
        if (!keys.isEmpty()) {
            for (String key : keys) {
                Long changeTime = null;
                Enrollment enrollment = newEnrollmentMap.get(key);
                if (EntityStatus.ACTIVE.equals(enrollment.getStatus())) {
                    if (ArrayUtils.isNotEmpty(enrollment.getStatusChangeTime())) {
                        for (StatusChangeTime statuschangeTime : enrollment.getStatusChangeTime()) {
                            if (EntityStatus.INACTIVE.equals(statuschangeTime.getNewStatus())) {
                                changeTime = statuschangeTime.getChangeTime();
                            }
                        }
                    }
                    ShareContentEnrollmentReq request = new ShareContentEnrollmentReq();
                    request.setContextId(enrollment.getBatchId());
                    request.setStudentId(enrollment.getUserId());
                    request.setStartTime(changeTime);
                    if(withAsync) {
                        Map<String, Object> payload = new HashMap<>();

                        payload.put("shareContentReq", request);
                        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.SHARE_CURRICULUM, payload);
                        asyncTaskFactory.executeTask(params);
                    }else{
                        try {
                            enrollmentAsyncManager.shareContentOnEnrollment(request);
                        } catch (VException e) {
                            logger.error(e);
                        }
                    }
                }
                enrollments.add(enrollment);

                EnrollmentUpdatePojo pojo = new EnrollmentUpdatePojo();
                if (oldEnrollmentMap.containsKey(key)) {
                    pojo.setPreviousEnrollment(mapper.map(oldEnrollmentMap.get(key), EnrollmentPojo.class));
                }
                pojo.setNewEnrollment(mapper.map(enrollment, EnrollmentPojo.class));
                if(withAsync) {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("pojo", pojo);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_CALENDAR_FOR_ENROL_EVENT, payload);
                    asyncTaskFactory.executeTask(params);
                }
                else {
                    try {
                        enrollmentAsyncManager.updateCalendarScheduling(pojo);
                    } catch (VException e) {
                        logger.error(e);
                    }
                }
            }
            //Trigger everythng here
            if (ArrayUtils.isNotEmpty(enrollments)) {
                if(withAsync) {
                    Map<String, Object> payload = new HashMap<>();
                    payload.put("enrollments", enrollments);
                    AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.UPDATE_BATCH_SEAT, payload);
                    asyncTaskFactory.executeTask(params);
                }
                else{
                    enrollmentManager.updateEnrollments(enrollments);
                }
            }

        }
    }

    public List<InstalmentInfo> getInstalments(GetInstalmentReq req) throws VException {

        req.verify();

        if (req.getUserId() == null || (StringUtils.isEmpty(req.getEntityId()) && StringUtils.isEmpty(req.getBatchId()))) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null or entities are null");
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(req.getUserId().toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        if (StringUtils.isNotEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(req.getEntityId()));
        }
        if (StringUtils.isNotEmpty(req.getBatchId())) {
            query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).is(req.getBatchId()));
        }

        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        Set<String> deliverableIds = new HashSet<>();

        for (Enrollment enrollment : enrollments) {
            if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
                deliverableIds.add(enrollment.getId());
            } else if (EnrollmentState.TRIAL.equals(enrollment.getState())) {
                throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Enrollment is in Trial State.");
            }
        }
        List<InstalmentInfo> instalmentInfos = instalmentUtilManager.getInstalmentsForDeliverableIds(req.getUserId(), deliverableIds);
        if (ArrayUtils.isNotEmpty(instalmentInfos)) {
            return instalmentInfos;
        }
        return new ArrayList<>();
    }

    public List<ContextWiseNextDueInstalmentsRes> getContextWiseNextDueInstalments(ContextWiseNextDueInstalmentsReq req)
            throws VException {
        req.verify();
        if (ArrayUtils.isEmpty(req.getDeliverableIds())) {
            Query query = new Query();
            query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(req.getUserId().toString()));
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
            if (ArrayUtils.isNotEmpty(req.getBundleIds())) {
                query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).in(req.getBundleIds()));
            }
            if (ArrayUtils.isNotEmpty(req.getBatchIds())) {
                query.addCriteria(Criteria.where(Enrollment.Constants.BATCH_ID).in(req.getBatchIds()));
            }

            List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
            Set<String> _deliverableIds = new HashSet<>();

            for (Enrollment enrollment : enrollments) {
                if (EnrollmentState.REGULAR.equals(enrollment.getState())) {
                    _deliverableIds.add(enrollment.getId());
                } else if (EnrollmentState.TRIAL.equals(enrollment.getState())) {
                    throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Enrollment is in Trial State.");
                }
            }
            req.setDeliverableIds(new ArrayList<>(_deliverableIds));
        }
        if (ArrayUtils.isNotEmpty(req.getDeliverableIds())) {
            String queryString = WebUtils.INSTANCE.createQueryStringOfObject(req);
            String url = DINERO_ENDPOINT + "/payment/getContextWiseNextDueInstalments?" + queryString;
            ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null);
            VExceptionFactory.INSTANCE.parseAndThrowException(resp);
            String jsonString = resp.getEntity(String.class);
            Type listType = new TypeToken<ArrayList<ContextWiseNextDueInstalmentsRes>>() {
            }.getType();
            List<ContextWiseNextDueInstalmentsRes> slist = gson.fromJson(jsonString, listType);
            return slist;
        } else {
            return null;
        }
    }

    //only for bundle atm
    public void processInstalmentAfterPayment(InstalmentInfo info) throws VException {
        logger.info("making call for dinero operations");
        Long userId = info.getUserId();
        String bundleId = info.getContextId();

        Query query = new Query();
        query.addCriteria(Criteria.where(Registration.Constants.ENTITY_ID).is(bundleId));
        query.addCriteria(Criteria.where(Registration.Constants.USER_ID).is(userId));

        Registration registration = registrationDAO.findOne(query, Registration.class);
        GetEnrollmentsReq req = new GetEnrollmentsReq();
        req.setEntityType(EntityType.OTM_BUNDLE);
        req.setEntityId(bundleId);
        req.setUserId(userId);
        req.setStatuses(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
        List<Enrollment> enrollments = enrollmentDAO.getEnrollments(req);
        List<InstalmentInfo> res = paymentManager.processInstalmentAfterPayment(info.getId());

        for (InstalmentInfo instalmentInfo : res) {
            if (instalmentInfo.getId().equals(info.getId())) {
                info = instalmentInfo;
                break;
            }
        }

        if (registration != null) {
            Map<String, Enrollment> courseEnrollmentMap = new HashMap<>();

            for (Enrollment enrollment : enrollments) {
                if (courseEnrollmentMap.containsKey(enrollment.getCourseId())) {
                    logger.error("Multiple enrollments for userId: " + userId + " and courseId: " + enrollment.getCourseId());
                    continue;
                }
                courseEnrollmentMap.put(enrollment.getCourseId(), enrollment);
            }

            int amountPBeforeLastCourse = 0;
            int amountNPBeforeLastCourse = 0;
            int amountDiscountBeforeLastCourse = 0;
            for (int i = 0; i < registration.getEntities().size(); i++) {

                OTMBundleEntityInfo oTMBundleEntityInfo = registration.getEntities().get(i);
                MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
                makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.INSTALLMENT_PAYMENT);
                makePaymentRequest.setEnrollmentTransactionContextId(info.getId());
                makePaymentRequest.setOrderId(info.getOrderId());

                float ratio = (float) oTMBundleEntityInfo.getPrice() / registration.getBulkPriceToPay();

                if (i < registration.getEntities().size() - 1) {
                    int amountPToPay = 0;
                    int amountNPToPay = 0;
                    int amountDiscount = 0;
                    if (info.getTotalPromotionalAmount() != null) {
                        amountPToPay = (int) (ratio * info.getTotalPromotionalAmount());
                    }
                    if (info.getTotalNonPromotionalAmount() != null) {
                        amountNPToPay = (int) (ratio * info.getTotalNonPromotionalAmount());
                    }
                    if (info.getVedantuDiscountAmount() != null) {
                        amountDiscount = (int) (ratio * info.getVedantuDiscountAmount());
                    }
                    amountPBeforeLastCourse += amountPToPay;
                    amountNPBeforeLastCourse += amountNPToPay;
                    amountDiscountBeforeLastCourse += amountDiscount;

                    makePaymentRequest.setAmt(amountPToPay + amountNPToPay);
                    makePaymentRequest.setAmtP(amountPToPay);
                    makePaymentRequest.setAmtNP(amountNPToPay);
                    makePaymentRequest.setAmtFromDiscount(amountDiscount);

                } else {
                    int amountPToPay = 0;
                    int amountNPToPay = 0;
                    int amountDiscount = 0;

                    if (info.getTotalPromotionalAmount() != null) {
                        amountPToPay = info.getTotalPromotionalAmount() - amountPBeforeLastCourse;
                    }
                    if (info.getTotalNonPromotionalAmount() != null) {
                        amountNPToPay = info.getTotalNonPromotionalAmount() - amountNPBeforeLastCourse;
                    }
                    if (info.getVedantuDiscountAmount() != null) {
                        amountDiscount = info.getVedantuDiscountAmount() - amountDiscountBeforeLastCourse;
                    }

                    makePaymentRequest.setAmt(amountPToPay + amountNPToPay);
                    makePaymentRequest.setAmtP(amountPToPay);
                    makePaymentRequest.setAmtNP(amountNPToPay);
                    makePaymentRequest.setAmtFromDiscount(amountDiscount);

                }

                makePaymentRequest.setCourseId(oTMBundleEntityInfo.getEntityId());

                Enrollment enrollment = courseEnrollmentMap.get(oTMBundleEntityInfo.getEntityId());
                if (enrollment == null) {
                    logger.error("Enrollment not found for course: " + oTMBundleEntityInfo.getEntityId());
                    continue;
                }
                makePaymentRequest.setBatchId(enrollment.getBatchId());
                makePaymentRequest.setEnrollmentId(enrollment.getId());
                makePaymentRequest.setCreateConsumptionIfnotFound(false);

                try {
                    boolean result = enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, ratio);
                    if (!result) {
                        logger.error("EnrollmentConsumption update failed after registration for: " + enrollment.getId());
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                    logger.error("EnrollmentConsumption Update failed after registration for: " + enrollment.getId());
                }

            }
        } else {
            OrderInfo orderInfo = paymentManager.getOrderInfo(info.getOrderId());
            Map<String, Enrollment> batchEnrollmentMap = new HashMap<>();

            if (orderInfo == null) {
                throw new BadRequestException(ErrorCode.ORDER_NOT_FOUND, "Order not found for : " + info);
            }

            for (Enrollment enrollment : enrollments) {
                if (batchEnrollmentMap.containsKey(enrollment.getCourseId())) {
                    logger.error("Multiple enrollments for userId: " + userId + " and courseId: " + enrollment.getCourseId());
                    continue;
                }
                batchEnrollmentMap.put(enrollment.getBatchId(), enrollment);
                if (ArrayUtils.isNotEmpty(enrollment.getBatchChangeTime())) {
                    Collections.sort(enrollment.getBatchChangeTime(), new Comparator<BatchChangeTime>() {
                        @Override
                        public int compare(BatchChangeTime b1, BatchChangeTime b2) {
                            return b1.getChangeTime().compareTo(b2.getChangeTime());
                        }
                    });
                    batchEnrollmentMap.put(enrollment.getBatchChangeTime().get(0).getPreviousBatchId(), enrollment);
                }
            }

            int totalAmount = orderInfo.getAmount();

            int totalDiscountAmount = 0;

            if (ArrayUtils.isNotEmpty(orderInfo.getOrderedItems()) && orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount() != null) {
                totalDiscountAmount = orderInfo.getOrderedItems().get(0).getVedantuDiscountAmount();
            }

            totalAmount += totalDiscountAmount;
            int amountPBeforeLastCourse = 0;
            int amountNPBeforeLastCourse = 0;
            int amountDiscountBeforeLastCourse = 0;
            for (int i = 0; i < orderInfo.getPurchasingEntities().size(); i++) {

                PurchasingEntity purchasingEntity = orderInfo.getPurchasingEntities().get(i);
                MakePaymentRequest makePaymentRequest = new MakePaymentRequest();
                makePaymentRequest.setEnrollmentTransactionContextType(EnrollmentTransactionContextType.INSTALLMENT_PAYMENT);
                makePaymentRequest.setEnrollmentTransactionContextId(info.getId());
                makePaymentRequest.setOrderId(info.getOrderId());

                float ratio = (float) purchasingEntity.getPrice() / totalAmount;

                if (i < orderInfo.getPurchasingEntities().size() - 1) {
                    int amountPToPay = 0;
                    int amountNPToPay = 0;
                    int amountDiscount = 0;
                    if (info.getTotalPromotionalAmount() != null) {
                        amountPToPay = (int) (ratio * info.getTotalPromotionalAmount());
                    }
                    if (info.getTotalNonPromotionalAmount() != null) {
                        amountNPToPay = (int) (ratio * info.getTotalNonPromotionalAmount());
                    }
                    if (info.getVedantuDiscountAmount() != null) {
                        amountDiscount = (int) (ratio * info.getVedantuDiscountAmount());
                    }
                    amountPBeforeLastCourse += amountPToPay;
                    amountNPBeforeLastCourse += amountNPToPay;
                    amountDiscountBeforeLastCourse += amountDiscount;

                    makePaymentRequest.setAmt(amountPToPay + amountNPToPay);
                    makePaymentRequest.setAmtP(amountPToPay);
                    makePaymentRequest.setAmtNP(amountNPToPay);
                    makePaymentRequest.setAmtFromDiscount(amountDiscount);

                } else {
                    int amountPToPay = 0;
                    int amountNPToPay = 0;
                    int amountDiscount = 0;

                    if (info.getTotalPromotionalAmount() != null) {
                        amountPToPay = info.getTotalPromotionalAmount() - amountPBeforeLastCourse;
                    }
                    if (info.getTotalNonPromotionalAmount() != null) {
                        amountNPToPay = info.getTotalNonPromotionalAmount() - amountNPBeforeLastCourse;
                    }
                    if (info.getVedantuDiscountAmount() != null) {
                        amountDiscount = info.getVedantuDiscountAmount() - amountDiscountBeforeLastCourse;
                    }

                    makePaymentRequest.setAmt(amountPToPay + amountNPToPay);
                    makePaymentRequest.setAmtP(amountPToPay);
                    makePaymentRequest.setAmtNP(amountNPToPay);
                    makePaymentRequest.setAmtFromDiscount(amountDiscount);

                }

                Enrollment enrollment = batchEnrollmentMap.get(purchasingEntity.getEntityId());

                if (enrollment == null) {
                    logger.error("Enrollment not found for batch: " + purchasingEntity.getEntityId());
                    continue;
                }
                makePaymentRequest.setCourseId(enrollment.getCourseId());
                makePaymentRequest.setBatchId(enrollment.getBatchId());
                makePaymentRequest.setEnrollmentId(enrollment.getId());
                makePaymentRequest.setCreateConsumptionIfnotFound(false);

                try {
                    boolean result = enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, ratio);
                    if (!result) {
                        logger.error("EnrollmentConsumption update failed after registration for: " + enrollment.getId());
                    }
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                    logger.error("EnrollmentConsumption Update failed after registration for: " + enrollment.getId());
                }

            }

        }

        Map<String, Object> payload = new HashMap<>();
        payload.put("userId", info.getUserId());
        payload.put("otfBundleId", info.getContextId());
        payload.put("instalments", res);
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.OTM_BUNDLE_FROM_2_ND_INSTALMENT_PAYMENT_EMAIL, payload);
        asyncTaskFactory.executeTask(params);

        query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(info.getUserId().toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.INACTIVE));
        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(info.getContextId()));
        logger.info("query " + query);

        enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        EntityType type = null;
        if (ArrayUtils.isNotEmpty(enrollments)) {
            Boolean orderState = paymentManager.resetInstalmentsPostPayment(info.getContextId(), EntityType.OTM_BUNDLE, info.getUserId().toString());
            if (Boolean.FALSE.equals(orderState)) {
                logger.info("More instalments to be paid by user. Not changing state of Enrollment.");
                //Trigger mail about it.
                return;
            }
        }
        logger.info(res);
        markEnrollmentPostPayment(info.getUserId(), info.getContextId());
        //Email about it

    }

    public void markEnrollmentStatus(Long userId, String entityId, EntityStatus newStatus) throws NotFoundException, BadRequestException {

        if (userId == null || newStatus == null || StringUtils.isEmpty(entityId)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Required parameter null");
        }
        Query query = new Query();
        if (EntityStatus.ACTIVE.equals(newStatus)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.INACTIVE));
        } else if (EntityStatus.INACTIVE.equals(newStatus)) {
            query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).is(EntityStatus.ACTIVE));
        }
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
//        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(entityId));

        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        if (ArrayUtils.isNotEmpty(enrollments)) {
            Map<String, Enrollment> oldEnrollmentMap = new HashMap<>();
            Map<String, Enrollment> newEnrollmentMap = new HashMap<>();
            Set<String> batchIds = new HashSet<>();
            boolean updateEnrollments = Boolean.FALSE;
            for (Enrollment enrollment : enrollments) {
                oldEnrollmentMap.put(enrollment.getId(), enrollment);
                updateEnrollments = Boolean.TRUE;

                StatusChangeTime statusChangeTime = new StatusChangeTime();
                statusChangeTime.setChangeTime(System.currentTimeMillis());
                statusChangeTime.setNewStatus(newStatus);
                statusChangeTime.setPreviousStatus(enrollment.getStatus());
                if (ArrayUtils.isEmpty(enrollment.getStatusChangeTime())) {
                    enrollment.setStatusChangeTime(Arrays.asList(statusChangeTime));
                } else {
                    enrollment.getStatusChangeTime().add(statusChangeTime);
                }
                enrollment.setStatus(newStatus);

                newEnrollmentMap.put(enrollment.getId(), enrollment);
                enrollmentDAO.save(enrollment);

            }
            if (updateEnrollments) {
                enrollmentTriggers(oldEnrollmentMap.keySet(), oldEnrollmentMap, newEnrollmentMap,true);
            }

        } else {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "no enrollment to change status");
        }
    }

    public PlatformBasicResponse refundForBundle(BundleRefundReq bundleRefundReq) {
        GetEnrollmentsReq req = new GetEnrollmentsReq();
        req.setEntityType(EntityType.OTM_BUNDLE);
        req.setEntityId(bundleRefundReq.getBundleId());
        req.setUserId(bundleRefundReq.getUserId());
        req.setStatuses(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
        List<Enrollment> enrollmentList = enrollmentDAO.getEnrollments(req);
        String errorString = "";

        for (Enrollment enrollment : enrollmentList) {
            EnrollmentRefundReq enrollmentRefundReq = new EnrollmentRefundReq();
            enrollmentRefundReq.setEnrollmentId(enrollment.getId());
            enrollmentRefundReq.setEndedTime(bundleRefundReq.getEndedTime());
            enrollmentRefundReq.setReasonNote(bundleRefundReq.getReasonNote());
            try {
                PlatformBasicResponse result = enrollmentManager.refundForEnrollment(enrollmentRefundReq, enrollment);
                if (!result.isSuccess()) {
                    errorString = errorString + enrollment.getId() + ": " + result.getErrorString() + ", ";
                } else {
                    List<StatusChangeTime> statusList = enrollment.getStatusChangeTime();
                    StatusChangeTime statusChangeTime = new StatusChangeTime();
                    statusChangeTime.setChangeTime(System.currentTimeMillis());
                    statusChangeTime.setNewStatus(EntityStatus.ENDED);
                    statusChangeTime.setPreviousStatus(enrollment.getStatus());
                    statusList.add(statusChangeTime);
                    enrollment.setEndReason(enrollmentRefundReq.getReasonNote());
                    enrollment.setStatusChangeTime(statusList);
                    enrollment.setStatus(EntityStatus.ENDED);
                    enrollmentDAO.save(enrollment);
                }
            } catch (Exception ex) {
                logger.error("Refund for enrollment failed: " + enrollment.getId() + ", error " + ex.getMessage());
                errorString = errorString + enrollment.getId() + ", ";
            }
        }

        if (!StringUtils.isEmpty(errorString)) {
            return new PlatformBasicResponse(false, null, errorString);
        }

        return new PlatformBasicResponse(true, "Refund successfull for all enrollments", null);

    }

    public EndSubscriptionRes endSubscription(EndSubscriptionReq endSubscriptionReq) throws VException {
        EndSubscriptionRes endSubscriptionRes = new EndSubscriptionRes();
        endSubscriptionRes.setSuccess(false);

        logger.info("End Subscription Request: " + endSubscriptionReq);
        endSubscriptionReq.verify();

        Enrollment enrollment = enrollmentDAO.getById(endSubscriptionReq.getEnrollmentId());
        if (enrollment == null) {
            throw new NotFoundException(ErrorCode.ENROLLMENT_NOT_FOUND, "enrollment not found");
        }
        if (EntityStatus.ENDED.equals(enrollment.getStatus())) {
            throw new ConflictException(ErrorCode.ENROLLMENT_ALREADY_ENDED, "enrollment is already ended");
        }

        if (EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())) {

            BundleRefundReq bundleRefundReq = new BundleRefundReq();
            bundleRefundReq.setBundleId(enrollment.getEntityId());
            bundleRefundReq.setEndedTime(endSubscriptionReq.getEndedTime());
            bundleRefundReq.setUserId(Long.parseLong(enrollment.getUserId()));
            bundleRefundReq.setReasonNote(endSubscriptionReq.getEndReason());

            if (endSubscriptionReq.isCheckRefundAmount()) {
                endSubscriptionRes = getRefundAmountForBundle(bundleRefundReq);
            } else {
                PlatformBasicResponse platformBasicResponse = refundForBundle(bundleRefundReq);
                endSubscriptionRes.setSuccess(platformBasicResponse.isSuccess());
                endSubscriptionRes.setErrorString(platformBasicResponse.getErrorString());
                endSubscriptionRes.setResponse(platformBasicResponse.getResponse());
            }
        } else {
            EnrollmentRefundReq enrollmentRefundReq = new EnrollmentRefundReq();
            enrollmentRefundReq.setEndedTime(endSubscriptionReq.getEndedTime());
            enrollmentRefundReq.setEnrollmentId(enrollment.getId());
            enrollmentRefundReq.setReasonNote(endSubscriptionReq.getEndReason());

            if (endSubscriptionReq.isCheckRefundAmount()) {
                endSubscriptionRes = enrollmentManager.getRefundAmountForEnrollment(enrollmentRefundReq, enrollment);
            } else {
                PlatformBasicResponse platformBasicResponse = enrollmentManager.refundForEnrollment(enrollmentRefundReq, enrollment);
                endSubscriptionRes.setSuccess(platformBasicResponse.isSuccess());
                endSubscriptionRes.setErrorString(platformBasicResponse.getErrorString());
                endSubscriptionRes.setResponse(platformBasicResponse.getResponse());
            }

        }
        return endSubscriptionRes;
    }

    public EndSubscriptionRes getRefundAmountForBundle(BundleRefundReq bundleRefundReq) throws VException {
        GetEnrollmentsReq req = new GetEnrollmentsReq();
        req.setEntityType(EntityType.OTM_BUNDLE);
        req.setEntityId(bundleRefundReq.getBundleId());
        req.setUserId(bundleRefundReq.getUserId());
        List<Enrollment> enrollmentList = enrollmentDAO.getEnrollmentsForEntities(bundleRefundReq.getUserId().toString(),
                EntityType.OTM_BUNDLE, bundleRefundReq.getBundleId(), Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE), true);
        int refundAmountP = 0;
        int refundAmountNP = 0;
        for (Enrollment enrollment : enrollmentList) {
            EnrollmentRefundReq enrollmentRefundReq = new EnrollmentRefundReq();
            enrollmentRefundReq.setEnrollmentId(enrollment.getId());
            enrollmentRefundReq.setEndedTime(bundleRefundReq.getEndedTime());
            EndSubscriptionRes endSubscriptionRes = enrollmentManager.getRefundAmountForEnrollment(enrollmentRefundReq, enrollment);
            refundAmountP += endSubscriptionRes.getPromotionalAmount();
            refundAmountNP += endSubscriptionRes.getNonPromotionalAmount();
        }
        EndSubscriptionRes endSubscriptionRes = new EndSubscriptionRes();
        endSubscriptionRes.setPromotionalAmount(refundAmountP);
        endSubscriptionRes.setNonPromotionalAmount(refundAmountNP);
        return endSubscriptionRes;
    }

    public Boolean checkIfAlreadyEnrolled(Long userId, OTFBundleInfo otfBundleInfo) throws BadRequestException {
        if (otfBundleInfo == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "bundleInfo is null");
        }
        List<String> courseIds = new ArrayList<>();
        List<String> batchIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(otfBundleInfo.getEntities())) {
            for (OTMBundleEntityInfo bundleEntity : otfBundleInfo.getEntities()) {
                if (EntityType.OTF_COURSE.equals(bundleEntity.getEntityType())
                        || EntityType.OTF_COURSE_REGISTRATION.equals(bundleEntity.getEntityType())) {
                    courseIds.add(bundleEntity.getEntityId());
                }
                if (EntityType.OTF.equals(bundleEntity.getEntityType())
                        || EntityType.OTF_BATCH_REGISTRATION.equals(bundleEntity.getEntityType())) {
                    batchIds.add(bundleEntity.getEntityId());
                }
            }
        }
        return _checkIfAlreadyEnrolled(userId, courseIds, batchIds);

    }

    public Boolean _checkIfAlreadyEnrolled(Long userId, List<String> courseIds, List<String> batchIds) throws BadRequestException {
        if (userId == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId is null");
        }
        if (ArrayUtils.isEmpty(batchIds) && ArrayUtils.isEmpty(courseIds)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "entityIds is null");
        }
        Set<String> ids = new HashSet<>();

        if (ArrayUtils.isNotEmpty(batchIds)) {
            List<Batch> batches = batchDAO.getBatches(batchIds, Arrays.asList(Batch.Constants.COURSE_ID));
            if (ArrayUtils.isNotEmpty(batches)) {
                batches.forEach(a -> ids.add(a.getCourseId()));
            }
        }
        if (ArrayUtils.isNotEmpty(courseIds)) {
            ids.addAll(courseIds);
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.COURSE_ID).in(ids));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).ne(EntityStatus.ENDED));
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        if (ArrayUtils.isNotEmpty(enrollments)) {
            return true;
        }
        return false;

    }

    public List<GetCourseListingRes> toCourseListingRes(List<Course> courses, Map<Long, Board> subjectMap) {
        List<GetCourseListingRes> getCourseListingResList = new ArrayList<>();
        for (Course course : courses) {
            GetCourseListingRes getCourseListingRes = new GetCourseListingRes();
            getCourseListingRes.setEntityId(course.getId());
            getCourseListingRes.setIsBundle(false);
            getCourseListingRes.setPrice(course.getStartPrice());
            getCourseListingRes.setDuration(course.getDuration());
            List<Long> boardIds = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                    boardIds.add(boardTeacherPair.getBoardId());
                }
            }
            getCourseListingRes.setBoardIds(boardIds);
            getCourseListingRes.setTitle(course.getTitle());
            getCourseListingRes.setTargets(course.getTargets());
            if (ArrayUtils.isNotEmpty(course.getSearchTerms())) {
                if (course.getSearchTerms().contains(CourseTerm.CO_CURRICULAR)) {
                    getCourseListingRes.setTag(CourseTerm.CO_CURRICULAR.toString());
                }
                if (StringUtils.isEmpty(getCourseListingRes.getTag()) && course.getSearchTerms().contains(CourseTerm.CONTENT_BASED)) {
                    getCourseListingRes.setTag(CourseTerm.CONTENT_BASED.toString());
                }
                if (StringUtils.isEmpty(getCourseListingRes.getTag()) && course.getSearchTerms().contains(CourseTerm.POPULAR)) {
                    getCourseListingRes.setTag(CourseTerm.POPULAR.toString());
                }
            }
            List<Long> startTimes = new ArrayList<>();
            Long currentTime = System.currentTimeMillis();
            if (StringUtils.isEmpty(getCourseListingRes.getTag())) {
                if (course.getBatchStartTime() != null && course.getBatchStartTime() > currentTime) {
                    getCourseListingRes.setTag("UPCOMING");
                }
            }
            startTimes.add(course.getBatchStartTime());
            getCourseListingRes.setStartTimes(startTimes);
            getCourseListingRes.setSubjects(new ArrayList<>());
            if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {

                    if (subjectMap.get(boardTeacherPair.getBoardId()) == null) {
                        continue;
                    }

                    getCourseListingRes.getSubjects().add(subjectMap.get(boardTeacherPair.getBoardId()).getName());
                }
            }
            getCourseListingRes.setFirstStartTime(course.getBatchStartTime());
            getCourseListingResList.add(getCourseListingRes);
        }
        return getCourseListingResList;
    }

    public List<String> getTargetsForListing() {

        AggregatedTargets targetsBundle = otfBundleDAO.getAggregatedTargets();
        logger.info(targetsBundle);
        AggregatedTargets targetsCourse = courseDAO.getAggregatedTargets();
        logger.info(targetsCourse);

        Set<String> resultTargets = new HashSet<>();

        if (targetsBundle != null && CollectionUtils.isNotEmpty(targetsBundle.getAggTargets())) {
            resultTargets.addAll(targetsBundle.getAggTargets());
        }

        if (targetsCourse != null && CollectionUtils.isNotEmpty(targetsCourse.getAggTargets())) {
            resultTargets.addAll(targetsCourse.getAggTargets());
        }

        return new ArrayList<>(resultTargets);
    }

    public String getCachedBatchKey(String batchId) {
        return env + "_BATCH_MONGO_" + batchId;
    }

    public String getCachedCourseKey(String courseId) {
        return env + "_COURSE_MONGO_" + courseId;
    }

    public Long getDurationForOTMBundle(OTMBundle oTMBundle) {
        Long result = 0l;

        List<String> batchIds = new ArrayList<>();
        List<String> courseIds = new ArrayList<>();

        for (OTMBundleEntityInfo oTMBundleEntityInfo : oTMBundle.getEntities()) {
            if (EntityType.OTF_COURSE.equals(oTMBundleEntityInfo.getEntityType())) {
                courseIds.add(oTMBundleEntityInfo.getEntityId());
            } else if (EntityType.OTF.equals(oTMBundleEntityInfo.getEntityType())) {
                batchIds.add(oTMBundleEntityInfo.getEntityId());
            }
        }

        if (ArrayUtils.isNotEmpty(batchIds)) {
            Set<String> batchIdSet = new HashSet<>(batchIds);
            try {
                List<String> cachedBatchKeys = new ArrayList<>();
                for (String batchId : batchIds) {
                    cachedBatchKeys.add(getCachedBatchKey(batchId));
                }
                Map<String, String> batchesStringMap = redisDAO.getValuesForKeys(cachedBatchKeys);
                if (!batchesStringMap.isEmpty()) {
                    for (String batchId : batchIds) {
                        String batchString = batchesStringMap.get(getCachedBatchKey(batchId));
                        if (batchString != null) {
                            Batch batch = new Gson().fromJson(batchString, Batch.class);
                            if (batch.getDuration() != null) {
                                result = result + batch.getDuration();
                                batchIdSet.remove(batchId);
                            }
                        }
                    }
                }

            } catch (Exception ex) {
                logger.error("Exception while fetching cached batches: " + ex.getMessage());
            }

            if (ArrayUtils.isNotEmpty(batchIdSet)) {
                List<Batch> batches = batchDAO.getBatchByIds(new ArrayList<>(batchIdSet));
                for (Batch batch : batches) {
                    try {
                        redisDAO.set(getCachedBatchKey(batch.getId()), new Gson().toJson(batch));
                    } catch (Exception ex) {
                        logger.error("Exception while caching batche: " + ex.getMessage());
                    }
                    if (batch.getDuration() != null) {
                        result = result + batch.getDuration();
                    }
                }
            }

        } else if (ArrayUtils.isNotEmpty(courseIds)) {
            Set<String> courseIdSet = new HashSet<>(courseIds);
            try {
                List<String> cachedCourseKeys = new ArrayList<>();
                for (String courseId : courseIds) {
                    cachedCourseKeys.add(getCachedCourseKey(courseId));
                }
                Map<String, String> coursesStringMap = redisDAO.getValuesForKeys(cachedCourseKeys);
                if (!coursesStringMap.isEmpty()) {
                    for (String courseId : courseIds) {
                        String courseString = coursesStringMap.get(getCachedCourseKey(courseId));
                        if (courseString != null) {
                            Course course = new Gson().fromJson(courseString, Course.class);
                            if (course.getDuration() != null) {
                                result = result + course.getDuration();
                                courseIdSet.remove(courseId);
                            }
                        }
                    }
                }

            } catch (Exception ex) {
                logger.error("Exception while fetching cached courses: " + ex.getMessage());
            }

            if (ArrayUtils.isNotEmpty(courseIdSet)) {
                List<Course> courses = courseDAO.getCoursesBasicInfos(new ArrayList<>(courseIdSet));
                for (Course course : courses) {
                    try {
                        redisDAO.set(getCachedCourseKey(course.getId()), new Gson().toJson(course));
                    } catch (Exception ex) {
                        logger.error("Exception while caching course: " + ex.getMessage());
                    }
                    if (course.getDuration() != null) {
                        result = result + course.getDuration();
                    }
                }
            }
        }

        return result;
    }

    public List<GetCourseListingRes> toCourseListingResForBundles(List<AggregatedOTMBundles> aggregatedOTMBundles, Map<Long, Board> subjectMap) {
        List<GetCourseListingRes> getCourseListingResList = new ArrayList<>();
        for (AggregatedOTMBundles aggregatedOTMBundle : aggregatedOTMBundles) {

            if (ArrayUtils.isEmpty(aggregatedOTMBundle.getBundles())) {
                continue;
            }

            GetCourseListingRes getCourseListingRes = new GetCourseListingRes();
            getCourseListingRes.setEntityId(aggregatedOTMBundle.getBundles().get(0).getId());
            getCourseListingRes.setIsBundle(true);
            getCourseListingRes.setDuration(getDurationForOTMBundle(aggregatedOTMBundle.getBundles().get(0)));
            Set<String> targets = new HashSet<>();
            targets.add(aggregatedOTMBundle.getBundles().get(0).getTarget());
            getCourseListingRes.setTargets(targets);
            getCourseListingRes.setPrice(aggregatedOTMBundle.getBundles().get(0).getPrice());
            getCourseListingRes.setBoardIds(aggregatedOTMBundle.getBundles().get(0).getBoardIds());
            getCourseListingRes.setSubjects(new ArrayList<>());
            if (ArrayUtils.isNotEmpty(getCourseListingRes.getBoardIds())) {
                for (Long boardId : getCourseListingRes.getBoardIds()) {
                    if (subjectMap.get(boardId) == null) {
                        continue;
                    }
                    getCourseListingRes.getSubjects().add(subjectMap.get(boardId).getName());
                }
            }
            getCourseListingRes.setTitle(aggregatedOTMBundle.getBundles().get(0).getTitle());
            getCourseListingRes.setStartTimes(aggregatedOTMBundle.getStartTimes());
            getCourseListingRes.setFirstStartTime(aggregatedOTMBundle.getMinStartTime());
            if (ArrayUtils.isNotEmpty(aggregatedOTMBundle.getBundles().get(0).getSearchTerms())) {
                if (aggregatedOTMBundle.getBundles().get(0).getSearchTerms().contains(CourseTerm.CO_CURRICULAR)) {
                    getCourseListingRes.setTag(CourseTerm.CO_CURRICULAR.toString());
                }
                if (StringUtils.isEmpty(getCourseListingRes.getTag()) && aggregatedOTMBundle.getBundles().get(0).getSearchTerms().contains(CourseTerm.CONTENT_BASED)) {
                    getCourseListingRes.setTag(CourseTerm.CONTENT_BASED.toString());
                }
                if (StringUtils.isEmpty(getCourseListingRes.getTag()) && aggregatedOTMBundle.getBundles().get(0).getSearchTerms().contains(CourseTerm.POPULAR)) {
                    getCourseListingRes.setTag(CourseTerm.POPULAR.toString());
                }
            }
            Long currentTime = System.currentTimeMillis();
            if (StringUtils.isEmpty(getCourseListingRes.getTag())) {
                if (getCourseListingRes.getFirstStartTime() != null && getCourseListingRes.getFirstStartTime() > currentTime) {
                    getCourseListingRes.setTag("UPCOMING");
                }
            }
            getCourseListingResList.add(getCourseListingRes);
        }

        return getCourseListingResList;

    }

    public List<GetCourseListingRes> getPastBundleListing(GetCourseListingReq req, List<Long> boardIds) {
        if (req.getSkipCount() == null) {
            req.setSkipCount(0);
        }
        Set<Long> boardIdsAll = new HashSet<>();
        List<Course> courses = courseDAO.getPastCourseForListing(req, boardIds);

        if (ArrayUtils.isEmpty(courses)) {
            courses = new ArrayList<>();
        } else {
            for (Course course : courses) {

                if (ArrayUtils.isEmpty(course.getBoardTeacherPairs())) {
                    continue;
                }

                for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                    if (boardTeacherPair.getBoardId() == null) {
                        continue;
                    }
                    boardIdsAll.add(boardTeacherPair.getBoardId());
                }
            }
        }
        List<AggregatedOTMBundles> bundles = otfBundleDAO.getPastOTMBundleForListing(req, boardIds);

        if (ArrayUtils.isEmpty(bundles)) {
            bundles = new ArrayList<>();
        } else {
            for (AggregatedOTMBundles aggregatedOTMBundles : bundles) {
                if (ArrayUtils.isEmpty(aggregatedOTMBundles.getBundles())) {
                    continue;
                }

                for (OTMBundle bundle : aggregatedOTMBundles.getBundles()) {
                    if (ArrayUtils.isNotEmpty(bundle.getBoardIds())) {
                        boardIdsAll.addAll(bundle.getBoardIds());
                    }
                }
            }
        }

        Map<Long, Board> subjectMap = fosUtils.getBoardInfoMap(boardIdsAll);

        List<GetCourseListingRes> coursesResult = toCourseListingRes(courses, subjectMap);
        List<GetCourseListingRes> otmBundlesResult = toCourseListingResForBundles(bundles, subjectMap);

        coursesResult.addAll(otmBundlesResult);
        Collections.reverse(coursesResult);
        logger.info(coursesResult);
        Collections.sort(coursesResult);
        logger.info(coursesResult);
        Collections.reverse(coursesResult);

        logger.info(coursesResult);

        if (req.getLastStartTime() > System.currentTimeMillis()) {
            req.setSkipCount(0);
        }

        return coursesResult.stream().skip(req.getSkipCount()).limit(req.getSize()).collect(Collectors.toList());
    }

    public List<GetCourseListingRes> getBundleListing(GetCourseListingReq req, List<Long> boardIds) throws NotFoundException {
        Long currentTime = System.currentTimeMillis();
        if (req.getSkipCount() == null) {
            req.setSkipCount(0);
        }
        if (req.getLastStartTime() == 0) {
            req.setLastStartTime(currentTime);
        }
        List<GetCourseListingRes> results = new ArrayList<>();
        Set<Long> boardIdsAll = new HashSet<>();
        if (req.getLastStartTime() >= currentTime) {
            List<Course> courses = courseDAO.getCourseForListing(req, boardIds);

            if (ArrayUtils.isEmpty(courses)) {
                courses = new ArrayList<>();
            } else {
                for (Course course : courses) {

                    if (ArrayUtils.isEmpty(course.getBoardTeacherPairs())) {
                        continue;
                    }

                    for (BoardTeacherPair boardTeacherPair : course.getBoardTeacherPairs()) {
                        if (boardTeacherPair.getBoardId() == null) {
                            continue;
                        }
                        boardIdsAll.add(boardTeacherPair.getBoardId());
                    }
                }
            }
            List<AggregatedOTMBundles> bundles = otfBundleDAO.getOTMBundleForListing(req, boardIds);

            if (ArrayUtils.isEmpty(bundles)) {
                bundles = new ArrayList<>();
            } else {
                for (AggregatedOTMBundles aggregatedOTMBundles : bundles) {
                    if (ArrayUtils.isEmpty(aggregatedOTMBundles.getBundles())) {
                        continue;
                    }

                    for (OTMBundle bundle : aggregatedOTMBundles.getBundles()) {
                        if (ArrayUtils.isNotEmpty(bundle.getBoardIds())) {
                            boardIdsAll.addAll(bundle.getBoardIds());
                        }
                    }
                }
            }

            Map<Long, Board> subjectMap = fosUtils.getBoardInfoMap(boardIdsAll);

            List<GetCourseListingRes> coursesResult = toCourseListingRes(courses, subjectMap);
            List<GetCourseListingRes> otmBundlesResult = toCourseListingResForBundles(bundles, subjectMap);

            coursesResult.addAll(otmBundlesResult);

            Collections.sort(coursesResult);

            logger.info(coursesResult);

            results = coursesResult.stream().skip(req.getSkipCount()).limit(req.getSize()).collect(Collectors.toList());
        }
        if (results.size() < req.getSize()) {

            GetCourseListingReq getCourseListingReq = mapper.map(req, GetCourseListingReq.class);

            getCourseListingReq.setSize(req.getSize() - results.size());

            List<GetCourseListingRes> pastResults = getPastBundleListing(getCourseListingReq, boardIds);

            results.addAll(pastResults);

        }

        return results;

    }

    public void updateDeliverableEntityId(String orderId, Long userId, String entityId, OTFBundleInfo bundleInfo) throws VException {
        Query query = new Query();
        query.addCriteria(Criteria.where(Enrollment.Constants.USER_ID).is(userId.toString()));
        query.addCriteria(Criteria.where(Enrollment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        query.addCriteria(Criteria.where(Enrollment.Constants.ENTITY_ID).is(entityId));

        logger.info("query " + query);
        List<Enrollment> enrollments = enrollmentDAO.runQuery(query, Enrollment.class);
        updateDeliverableEntityId(orderId, userId, entityId, bundleInfo, enrollments);
    }

    public void updateDeliverableEntityId(String orderId, Long userId, String entityId, List<Enrollment> enrollments) throws VException {
        OTFBundleInfo bundleInfo = getOTFBundle(entityId);
        updateDeliverableEntityId(orderId, userId, entityId, bundleInfo, enrollments);
    }

    public void updateDeliverableEntityId(String orderId, Long userId, String entityId, OTFBundleInfo bundleInfo, List<Enrollment> enrollments) throws VException {
        UpdateDeliverableDataReq updateDeliverableDataReq = new UpdateDeliverableDataReq();
        updateDeliverableDataReq.setOrderId(orderId);
        updateDeliverableDataReq.setUserId(userId);
        updateDeliverableDataReq.setEntityId(entityId);

        Map<String, String> courseMap = new HashMap<>();
        Map<String, String> batchMap = new HashMap<>();
        logger.info(enrollments);
        if (ArrayUtils.isNotEmpty(enrollments)) {
            for (Enrollment enroll : enrollments) {
                courseMap.put(enroll.getCourseId(), enroll.getId());
                batchMap.put(enroll.getBatchId(), enroll.getId());
            }

            if (ArrayUtils.isNotEmpty(bundleInfo.getEntities())) {
                for (OTMBundleEntityInfo info : bundleInfo.getEntities()) {
                    if (EntityType.OTF_COURSE.equals(info.getEntityType())) {
                        updateDeliverableDataReq.addEntityDeliverableMap(info.getEntityId(), courseMap.get(info.getEntityId()));
                    } else {
                        updateDeliverableDataReq.addEntityDeliverableMap(info.getEntityId(), batchMap.get(info.getEntityId()));
                    }
                }
            }

            if (updateDeliverableDataReq.getEntityDeliverableMap() != null && !updateDeliverableDataReq.getEntityDeliverableMap().isEmpty()) {
                paymentManager.updateDeliverableData(updateDeliverableDataReq);
            }
        }
    }

    public OTFBundleInfo getOTFBundleInfoByGroupName(String groupName) throws NotFoundException, ConflictException {
        List<OTMBundle> oTMBundles = otfBundleDAO.getBundleByGroupName(groupName);

        if (ArrayUtils.isEmpty(oTMBundles)) {
            throw new NotFoundException(ErrorCode.OTM_BUNDLE_NOT_FOUND, groupName);
        }

        if (oTMBundles.size() > 1) {
            throw new ConflictException(ErrorCode.MULTIPLE_OTM_BUNDLE_FOUND, groupName);
        }

        OTMBundle oTMBundle = oTMBundles.get(0);

        OTFBundleInfo bundleInfo = mapper.map(oTMBundle, OTFBundleInfo.class);

        return bundleInfo;

    }

    public List<OTMBundle> getOTFBundleByIds(List<String> otmBundleIds) {
        if(ArrayUtils.isEmpty(otmBundleIds)){
            return new ArrayList<>();
        }
        return otfBundleDAO.getOTMBundlesByIds(otmBundleIds);
    }
}
