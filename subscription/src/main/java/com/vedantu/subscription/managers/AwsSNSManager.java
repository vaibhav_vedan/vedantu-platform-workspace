package com.vedantu.subscription.managers;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.google.gson.Gson;
import com.vedantu.aws.AbstractAwsSNSManager;
import com.vedantu.aws.pojo.CronTopic;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SNSSubject;
import com.vedantu.util.enums.SNSTopic;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AwsSNSManager extends AbstractAwsSNSManager {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSNSManager.class);

    private String env;
    private String arn;

    private AmazonSNSAsync snsClient;

    private static Gson gson = new Gson();

    public AwsSNSManager() {
        env = ConfigUtils.INSTANCE.getStringValue("environment");
        arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
            snsClient = AmazonSNSAsyncClientBuilder.standard()
                    .withRegion(Regions.AP_SOUTHEAST_1)
                    .build();
            CreateTopicRequest createTopicRequest = new CreateTopicRequest(SNSTopicOTF.OTM_SNAPSHOT_SESSION_ENDED.toString() + "_" + env.toUpperCase());
            CreateTopicResult createTopicResult = snsClient.createTopic(createTopicRequest);
        }
    }

    public void triggerSNS(SNSTopicOTF topic, String subject, String message) {

        try {
            String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic + "_" + env.toUpperCase();
            PublishRequest publishRequest = new PublishRequest(topicArn, message, subject);
            PublishResult publishResult = snsClient.publish(publishRequest);
            logger.info("Published SNS, MessageId - " + publishResult.getMessageId());
        } catch (Exception ex) {
            logger.error("Error creating the sns session event : ", ex);
        }


    }

    @Override
    public void createTopics() {
        createSNSTopic(SNSTopicOTF.AUTO_ENROLL_BATCH);
        logger.info("creating topic-- update course events");
        createSNSTopic(SNSTopic.UPDATE_COURSE_EVENTS);
    }

    @Override
    public void createSubscriptions() {

        //createCronSubscription(CronTopic.CRON_CHIME_2HOURLY, "bundle/terminateFreePass");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "batch/contentBatchConsumption");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "batch/endBatchConsumption");
        createCronSubscription(CronTopic.CRON_CHIME_DAILY, "bundle/terminateFreePass");
        createCronSubscription(SNSTopicOTF.AUTO_ENROLL_BATCH, "batch/autoEnrolToBatch");

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    public void removeCachedKeysCrossService(SNSTopic topic, SNSSubject subject, List<String> keysToRemove) {
        triggerSNS(topic, subject.name(), gson.toJson(keysToRemove));
    }


}

