/**
 * 
 */
package com.vedantu.subscription.managers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.exception.VException;
import com.vedantu.subscription.controllers.PremiumSubscriptionController;
import com.vedantu.subscription.dao.PremiumSubscriptionDAO;
import com.vedantu.subscription.entities.mongo.PremiumSubscription;
import com.vedantu.subscription.request.AddPremiumSubscriptionRequest;
import com.vedantu.subscription.request.PremiumSubscriptionRequest;
import com.vedantu.subscription.response.BatchBundlePojo;
import com.vedantu.subscription.response.PremiumSubscriptionResponse;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;

/**
 * @author subarna
 *
 */
@Service
public class PremiumSubscriptionManager {

	@Autowired
	private HttpSessionUtils httpSessionUtils;

	@Autowired
	private DozerBeanMapper mapper;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PremiumSubscriptionController.class);

	@Autowired
	private PremiumSubscriptionDAO premiumSubscriptionDAO;

	public PlatformBasicResponse createPremiumSubscription(AddPremiumSubscriptionRequest premiumSubscriptionReq) throws VException {
		String callingUserId = httpSessionUtils.getCallingUserId().toString();
		premiumSubscriptionDAO.saveOrUpdate(addRequestToEntityConverter(premiumSubscriptionReq), callingUserId);
		return new PlatformBasicResponse(true, "Success", ""); 

	}

	public PlatformBasicResponse modifyPremiumSubscriptionById(PremiumSubscriptionRequest premiumSubscriptionReq) throws VException {
		String callingUserId = httpSessionUtils.getCallingUserId().toString();
		PremiumSubscription p = new PremiumSubscription();
		p = addRequestToEntityConverter(premiumSubscriptionReq);
		p.setId(premiumSubscriptionReq.getId());
		logger.debug("PremiumSubscriptionManager modifyPremiumSubscriptionById p -> "+p);
		premiumSubscriptionDAO.saveOrUpdate(p, callingUserId);
		return new PlatformBasicResponse(true, "Success", ""); 
	}

	public PlatformBasicResponse deletePremiumSubscriptionById(String id) {

		int result = 0;
		try {
			result = premiumSubscriptionDAO.deleteEntityById(id, PremiumSubscription.class);
		} catch (Exception ex) {
			logger.error("Error in deleting Curriculum with id: " + id, ex);
		}
		if(result==0) {
			return new PlatformBasicResponse(false, "Failure", "Failed to delete entity from DB");
		}else {
			return new PlatformBasicResponse(true, "Success", "");
		}

	}



	public List<PremiumSubscriptionResponse> viewPremiumSubscription(PremiumSubscriptionRequest premiumSubscriptionReq) {

		List<PremiumSubscription> premiumSubscriptions = new ArrayList<PremiumSubscription>();
		premiumSubscriptions = premiumSubscriptionDAO.viewPremiumSubscription(premiumSubscriptionReq);
		logger.debug("PremiumSubscriptionManager viewPremiumSubscription premiumSubscriptions -> "+premiumSubscriptions);
		List<PremiumSubscriptionResponse> premiumSubscriptionResponses = new ArrayList<PremiumSubscriptionResponse>();
		if(ArrayUtils.isNotEmpty(premiumSubscriptions)) {
			for(PremiumSubscription p : premiumSubscriptions) {
				premiumSubscriptionResponses.add(mapper.map(p, PremiumSubscriptionResponse.class));
			}
		}
		logger.debug("PremiumSubscriptionManager  viewPremiumSubscription  premiumSubscriptionResponses-> "+premiumSubscriptionResponses);
		return premiumSubscriptionResponses;
	}


	public List<BatchBundlePojo> getPremiumBundlesByFilter(PremiumSubscriptionRequest premiumSubscriptionReq) {

		List<BatchBundlePojo> batchBundlePojos = new ArrayList<BatchBundlePojo>();
		List<PremiumSubscription> premiumSubscriptions = new ArrayList<PremiumSubscription>();
		premiumSubscriptions = premiumSubscriptionDAO.getPremiumBundlesByFilter(premiumSubscriptionReq);
		logger.debug("PremiumSubscriptionManager getPremiumBundlesByFilter premiumSubscriptions -> "+premiumSubscriptions);
		if(ArrayUtils.isNotEmpty(premiumSubscriptions)) {
			for(PremiumSubscription p : premiumSubscriptions) {
				BatchBundlePojo batchBundlePojo = new BatchBundlePojo();
				batchBundlePojo.setBundleId(p.getBundleId());
				batchBundlePojo.setPremiumBundleTitle(p.getTitle());
				batchBundlePojo.setCourseCategory(p.getCourseCategory());
				batchBundlePojo.setNoOfDaysOfFreeAccess(p.getNoOfDaysOfFreeAccess());
				batchBundlePojos.add(batchBundlePojo);
			}
		}
		return batchBundlePojos;
	}

	public List<PremiumSubscription> getPremiumBundlesForEnrollment(PremiumSubscriptionRequest premiumSubscriptionReq){
		List<PremiumSubscription> premiumSubscriptions = new ArrayList<PremiumSubscription>();
		premiumSubscriptions = premiumSubscriptionDAO.getPremiumBundlesForEnrollment(premiumSubscriptionReq, Arrays.asList(PremiumSubscription.Constants.BUNDLE_ID,
				PremiumSubscription.Constants.NO_OF_DAYS_OF_FREE_ACCESS, PremiumSubscription.Constants.VALID_FROM, PremiumSubscription.Constants.VALID_TILL));
		logger.debug("PremiumSubscriptionManager getPremiumBundlesForEnrollment premiumSubscriptions -> "+premiumSubscriptions);
		return premiumSubscriptions;
	}

	private PremiumSubscription addRequestToEntityConverter(AddPremiumSubscriptionRequest p) {

		return PremiumSubscription.builder()
				.title(p.getTitle())
				.grade(p.getGrade())
				.board(p.getBoard())
				.target(p.getTarget())
				.stream(p.getStream())
				.medium(p.getMedium())
				.courseCategory(p.getCourseCategory())
				.validFrom(p.getValidFrom())
				.validTill(p.getValidTill())
				.noOfDaysOfFreeAccess(p.getNoOfDaysOfFreeAccess())
				.bundleId(p.getBundleId())
				.build();
	}

	private PremiumSubscription addRequestToEntityConverter(PremiumSubscriptionRequest p) {

		return PremiumSubscription.builder()
				.title(p.getTitle())
				.grade(p.getGrade())
				.board(p.getGrade())
				.target(p.getTarget())
				.stream(p.getStream())
				.medium(p.getMedium())
				.courseCategory(p.getCourseCategory())
				.validFrom(p.getValidFrom())
				.validTill(p.getValidTill())
				.noOfDaysOfFreeAccess(p.getNoOfDaysOfFreeAccess())
				.bundleId(p.getBundleId())
				.build();
	}

	public PremiumSubscription getPremiumBundleDetailsByBundleId(String bundleId) {
		PremiumSubscription premiumSubscription = new PremiumSubscription();
		
		if(StringUtils.isNotEmpty(bundleId)) {
			premiumSubscription = premiumSubscriptionDAO.getPremiumBundleDetailsByBundleId(bundleId);
		}
		return premiumSubscription;
	}

	public List<PremiumSubscriptionResponse> getPremiumBundlesForFilter(PremiumSubscriptionRequest premiumSubscriptionReq) {

		List<PremiumSubscription> premiumSubscriptions = new ArrayList<PremiumSubscription>();
		premiumSubscriptions = premiumSubscriptionDAO.getPremiumBundlesByFilter(premiumSubscriptionReq);
		logger.debug("PremiumSubscriptionManager getPremiumBundlesByFilter premiumSubscriptions -> "+premiumSubscriptions);
		List<PremiumSubscriptionResponse> premiumSubscriptionResponses = new ArrayList<PremiumSubscriptionResponse>();
		if(ArrayUtils.isNotEmpty(premiumSubscriptions)) {
			for(PremiumSubscription p : premiumSubscriptions) {
				premiumSubscriptionResponses.add(mapper.map(p, PremiumSubscriptionResponse.class));
			}
		}
		logger.debug("PremiumSubscriptionManager  viewPremiumSubscription  premiumSubscriptionResponses-> "+premiumSubscriptionResponses);
		return premiumSubscriptionResponses;
	}
	
	public PremiumSubscription findPremiumSubscriptionDetailsById(String id) {
		
		PremiumSubscription premiumSubscription = new PremiumSubscription();
		try {
			premiumSubscription = premiumSubscriptionDAO.findById(id);
		} catch (VException e) {
			logger.error("PremiumSubscription does not exist for the given input id -> "+id);
		}
		return premiumSubscription;
		
	}

}
