package com.vedantu.subscription.managers;

import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileInputStream;

@Service
public class AwsS3Manager {

    private Logger logger = LogFactory.getLogger(AwsS3Manager.class);

    private AmazonS3Client s3Client;

    public AwsS3Manager() {
        s3Client = new AmazonS3Client();
    }

    public Boolean uploadFile(String bucket, String key, File contentFile) {
        Boolean isUploadContentFileSuccessfull = false;

        try {
            s3Client.putObject(bucket, key, contentFile);

            isUploadContentFileSuccessfull = true;
        } catch (Exception ex) {
            isUploadContentFileSuccessfull = false;
        }

        return isUploadContentFileSuccessfull;
    }

    public Boolean uploadFile(String bucket, String key, File contentFile, ObjectMetadata metadata) {
        Boolean isUploadContentFileSuccessfull;

        try {
            s3Client.putObject(bucket, key, new FileInputStream(contentFile), metadata);

            isUploadContentFileSuccessfull = true;
        } catch (Exception ex) {
            isUploadContentFileSuccessfull = false;
        }

        return isUploadContentFileSuccessfull;
    }

    public String getPublicBucketDownloadUrl(String bucket, String key) {
        return s3Client.getUrl(bucket, key).toExternalForm();
    }

    @PreDestroy
    public void cleanUp() {
        try {
            if (s3Client != null) {
                s3Client.shutdown();
            }
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in shutting down s3Client ", e);
        }
    }
}