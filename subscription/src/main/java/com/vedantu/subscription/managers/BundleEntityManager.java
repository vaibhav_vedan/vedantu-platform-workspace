package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BoardTeacherPair;
import com.vedantu.onetofew.pojo.CourseBasicInfo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.dao.BatchDAO;
import com.vedantu.subscription.dao.BundleEntityDAO;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.entities.mongo.OTMBundle;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.BundleEntityRequestInfo;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.request.BundleEntityBulkInsertionRequest;
import com.vedantu.subscription.request.BundleEntityRequest;
import com.vedantu.subscription.request.GetBundlePackageReferenceRequest;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class BundleEntityManager {

    @Autowired
    private BundleEntityDAO bundleEntityDAO;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private AwsSQSManager sqsManager;

    @Autowired
    private BatchDAO batchDAO;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private DozerBeanMapper mapper;

    @Autowired
    private RedisDAO redisDAO;

    private final Logger logger = LogFactory.getLogger(BundleEntityManager.class);

    private static final Gson gson = new Gson();
    private static final String AIOPACKAGE_TAGS_LIVE ="LIVE";
    private static final String AIOPACKAGE_TAGS_RECORDED ="RECORDED";

    public BundleEntity addEditBundleEntity(BundleEntityRequest request) throws VException {

        BundleEntity bundleEntity=null;
        String id = request.getId();
        String bundleId = request.getBundleId();
        BundleInfo bundleInfo=bundleManager.getCachedBundleBasicInfo(bundleId);
        boolean shouldAutoEnroll=EnrollmentType.AUTO_ENROLL.equals(request.getEnrollmentType()) && StringUtils.isEmpty(request.getId());

        logger.info("bundleInfo - {}\nentityId - {}",bundleInfo,id);

        BatchBasicInfo batch=null;
        boolean entityIsBatch = PackageType.OTF.equals(request.getPackageType());
        if(entityIsBatch){
            String batchId = request.getEntityId();
            logger.info("The entity we are going to save is a batch with batch id - {}",batchId);
            Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(Collections.singletonList(batchId), false);
            batch = batchMap.get(batchId);
            if(Objects.isNull(batch)){
                throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"The entity id specified is invalid for a batch");
            }
        }else{
            logger.info("The entity we are going to save is a bundle of batch with entity id - {}",request.getEntityId());
        }

        if(!StringUtils.isEmpty(id)){
            logger.info("We are going to modify the entity, so validating for existing entities");
            bundleEntity=bundleEntityDAO.getEntityById(id);
            validateForExistingEntities(request, bundleEntity);
            shouldAutoEnroll=EnrollmentType.AUTO_ENROLL.equals(request.getEnrollmentType()) && !EnrollmentType.AUTO_ENROLL.equals(bundleEntity.getEnrollmentType());

        }else{
            logger.info("We are going to save a new entity");
            List<BundleEntity> bundleEntities = bundleEntityDAO.getAllBundleEntityByBundleIdAndEntityId(bundleId, request.getEntityId());
            if(bundleInfo.getIsSubscription()){
                logger.info("Bundle level validation for subscription");
                bundleEntity =
                        Optional.ofNullable(bundleEntities)
                                .map(Collection::stream)
                                .orElseGet(Stream::empty)
                                .filter(entity->request.getGrade().equals(entity.getGrade()))
                                .findFirst()
                                .orElse(null);
                if(Objects.nonNull(bundleEntity)){
                    throw new BadRequestException(ErrorCode.ALREADY_EXISTS,"The data you are trying to save already exists for entity id-grade combination");
                }
            }else{
                logger.info("Bundle level validation for non subscription AIO");
                if(ArrayUtils.isNotEmpty(bundleEntities)){
                    throw new BadRequestException(ErrorCode.ALREADY_EXISTS,"For normal AIO, something like this entity id already exists");
                }
            }
        }

        validateForAutoEnrollCountIfNecessary(request, bundleEntity, bundleInfo);

        if (bundleInfo.getIsSubscription()) {
            logger.info("Validation when it is a subscription");
            validateInformationForSubscription(request.getId(),request.getEntityId(),request.getGrade(),request.getBundleId());
        } else {
            logger.info("Validation when it is not a subscription");
            if(Objects.isNull(bundleEntity)){
                logger.info("Validation for new data");
                validateBatchInformationForBundleEntity(bundleId,request.getEntityId(), request.getPackageType(), bundleInfo.getOrgId(),true);
            }else {
                logger.info("Validation for old data");
                validateBatchInformationForBundleEntity(bundleId, bundleEntity.getEntityId(), bundleEntity.getPackageType(), bundleInfo.getOrgId());
            }
        }

        if(Objects.isNull(bundleEntity)){
            logger.info("Creating new bundle entity");
            bundleEntity =mapper.map(request, BundleEntity.class);
            if(bundleInfo.getIsSubscription()){
                logger.info("Setting up main tags for subscription.");
                setMainTagsForBundleEntity(bundleEntity);
            }else{
                logger.info("Adding required info for non subscription.");
                if(Objects.nonNull(batch)) {
                    logger.info("Added batch id");
                    bundleEntity.setCourseId(batch.getCourseId());
                }
            }
            logger.info("Saving the entity");
            bundleEntityDAO.save(bundleEntity);
        }else{
            logger.info("Updating the existing data for id - {}",id);
            Update update=new Update();
            if(bundleInfo.getIsSubscription()){
                logger.info("Setting up main tags for subscription.");
                bundleEntity.setGrade(request.getGrade());
                update= setMainTagsForBundleEntity(bundleEntity);
            }
            update.set(BundleEntity.Constants.GRADE,request.getGrade());
            update.set(BundleEntity.Constants.ENROLLMENT_TYPE,request.getEnrollmentType());
            logger.info("Updating the entity");
            bundleEntityDAO.updateBundleEntityData(id,update);
        }

        if(shouldAutoEnroll){
            autoEnrollIntoPackage(bundleId, bundleEntity);
        }

        redisDAO.del(bundleManager.getBundleKeyForRedis(bundleId));

        return bundleEntity;
    }

    private void validateInformationForSubscription(String exclusionId, String entityId, Integer grade, String bundleId) throws BadRequestException {
        long count = bundleEntityDAO.getCountOfEntitiesWithPropertyExcludingOneIfPossible(exclusionId, entityId, grade, bundleId);
        if(count>0){
            throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,"Trying to include duplicate entry with entityId "+entityId+" grade "+grade+" bundleId "+bundleId);
        }
    }


    public List<BundleEntity> addEditBundleEntityInBulk(BundleEntityBulkInsertionRequest request) throws VException {

        String bundleId = request.getBundleId();
        logger.info("Insertion of data for bundle id - {}",bundleId);
        BundleInfo bundleInfo=bundleManager.getCachedBundleBasicInfo(bundleId);
        List<BundleEntityRequestInfo> bundleEntities = request.getBundleEntities();

        if(bundleInfo.getIsSubscription()){
            logger.info("Validation of data for subscription");
            validateDataForSubscription(bundleInfo,bundleEntities);
        }else{
            logger.info("Validation of data for normal AIO");
            validateDataForNormalAIO(bundleId,bundleEntities);
        }

        List<BundleEntityRequestInfo> newOTFRequests=
                bundleEntities.stream().filter(this::getNewOTFData).collect(Collectors.toList());
        logger.info("newOTFRequests - {}",newOTFRequests);

        List<BundleEntityRequestInfo> newOTMBundleRequests=
                bundleEntities.stream().filter(this::getNewOTMData).collect(Collectors.toList());
        logger.info("newOTMBundleRequests - {}",newOTMBundleRequests);

        List<BundleEntityRequestInfo> editOTFRequests=
                bundleEntities.stream().filter(this::getEditOTFData).collect(Collectors.toList());
        logger.info("editOTFRequests - {}",editOTFRequests);

        List<BundleEntityRequestInfo> editOTMBundleRequest=
                bundleEntities.stream().filter(this::getEditOTMData).collect(Collectors.toList());
        logger.info("editOTMBundleRequest - {}",editOTMBundleRequest);

        if (bundleInfo.getIsSubscription()) {
            logger.info("Validation of individual data for subscription AIO before insertion.");
            validateIndividualDataForSubscriptionAIOInsertion(bundleId,newOTFRequests,editOTFRequests);
        } else {
            logger.info("Validation of individual data for non-subscription AIO before insertion.");
            validateIndividualDataForNormalAIOInsertion(bundleId, bundleInfo.getOrgId(), newOTFRequests, newOTMBundleRequests, editOTFRequests, editOTMBundleRequest);
        }

        Map<String, String> batchIdCourseIdMap =
                Optional.ofNullable(batchDAO.getCourseIdsForBatchIds(bundleEntities.stream().filter(this::isOTFPackage).map(BundleEntityRequestInfo::getEntityId).collect(Collectors.toList())))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .collect(Collectors.toMap(Batch::getId, Batch::getCourseId));

        Map<String, BundleEntity> bundleEntityIdMap =
                Optional.ofNullable(bundleEntityDAO.getByIds(bundleEntities.stream().map(BundleEntityRequestInfo::getId).filter(CommonUtils.not(StringUtils::isEmpty)).collect(Collectors.toList())))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .collect(Collectors.toMap(BundleEntity::getId, Function.identity()));

        List<BundleEntity> results=new ArrayList<>();
        List<BundleEntity> entityForAutoEnroll=new ArrayList<>();

        for (BundleEntityRequestInfo requestInfo : newOTFRequests) {
            BundleEntity bundleEntity =mapper.map(requestInfo, BundleEntity.class);
            bundleEntity.setBundleId(bundleId);
            if(bundleInfo.getIsSubscription()){
                logger.info("Setting up main tags for subscription.");
                setMainTagsForBundleEntity(bundleEntity);
            }else{
                logger.info("Adding required info for non subscription.");
                String courseId=batchIdCourseIdMap.get(bundleEntity.getEntityId());
                if(!StringUtils.isEmpty(courseId)) {
                    logger.info("Added batch id");
                    bundleEntity.setCourseId(courseId);
                }
            }
            logger.info("Saving the entity");
            bundleEntityDAO.save(bundleEntity);
            results.add(bundleEntity);
            if(EnrollmentType.AUTO_ENROLL.equals(requestInfo.getEnrollmentType())){
                entityForAutoEnroll.add(bundleEntity);
            }
        }
        for (BundleEntityRequestInfo requestInfo : newOTMBundleRequests) {
            BundleEntity bundleEntity =mapper.map(requestInfo, BundleEntity.class);
            bundleEntity.setBundleId(bundleId);
            logger.info("Saving the entity");
            bundleEntityDAO.save(bundleEntity);
            results.add(bundleEntity);
            if(EnrollmentType.AUTO_ENROLL.equals(requestInfo.getEnrollmentType())){
                entityForAutoEnroll.add(bundleEntity);
            }
        }
        for (BundleEntityRequestInfo requestInfo : editOTFRequests) {
            String id = requestInfo.getId();
            BundleEntity bundleEntity=bundleEntityIdMap.get(id);
            logger.info("Updating the existing data for id - {}", id);
            Update update=new Update();
            if(bundleInfo.getIsSubscription()){
                logger.info("Setting up main tags for subscription.");
                bundleEntity.setGrade(requestInfo.getGrade());
                update= setMainTagsForBundleEntity(bundleEntity);
            }
            update.set(BundleEntity.Constants.GRADE,requestInfo.getGrade());
            update.set(BundleEntity.Constants.ENROLLMENT_TYPE,requestInfo.getEnrollmentType());
            logger.info("Updating the entity");
            bundleEntityDAO.updateBundleEntityData(id,update);
            results.add(bundleEntity);
            if(EnrollmentType.AUTO_ENROLL.equals(requestInfo.getEnrollmentType()) && !EnrollmentType.AUTO_ENROLL.equals(bundleEntity.getEnrollmentType())){
                entityForAutoEnroll.add(bundleEntity);
            }
        }
        for (BundleEntityRequestInfo requestInfo : editOTMBundleRequest) {
            String id = requestInfo.getId();
            BundleEntity bundleEntity=bundleEntityIdMap.get(id);
            logger.info("Updating the existing data for id - {}", id);
            Update update=new Update();
            update.set(BundleEntity.Constants.GRADE,requestInfo.getGrade());
            update.set(BundleEntity.Constants.ENROLLMENT_TYPE,requestInfo.getEnrollmentType());
            logger.info("Updating the entity");
            bundleEntityDAO.updateBundleEntityData(id,update);
            results.add(bundleEntity);
            if(EnrollmentType.AUTO_ENROLL.equals(requestInfo.getEnrollmentType()) && !EnrollmentType.AUTO_ENROLL.equals(bundleEntity.getEnrollmentType())){
                entityForAutoEnroll.add(bundleEntity);
            }
        }

        if(ArrayUtils.isNotEmpty(entityForAutoEnroll)){
            logger.info("Auto enrolling entities");
            for (BundleEntity bundleEntity : entityForAutoEnroll) {
                autoEnrollIntoPackage(bundleId,bundleEntity);
            }
        }

        redisDAO.del(bundleManager.getBundleKeyForRedis(bundleId));
        return results;
    }

    private void validateIndividualDataForSubscriptionAIOInsertion(String bundleId, List<BundleEntityRequestInfo> newOTFRequests, List<BundleEntityRequestInfo> editOTFRequests) throws BadRequestException {
        for (BundleEntityRequestInfo request : newOTFRequests) {
            validateInformationForSubscription(request.getId(),request.getEntityId(),request.getGrade(),bundleId);
        }
        for (BundleEntityRequestInfo request : editOTFRequests) {
            validateInformationForSubscription(request.getId(),request.getEntityId(),request.getGrade(),bundleId);
        }
    }

    private void validateIndividualDataForNormalAIOInsertion(String bundleId, String orgId, List<BundleEntityRequestInfo> newOTFRequests, List<BundleEntityRequestInfo> newOTMBundleRequests, List<BundleEntityRequestInfo> editOTFRequests, List<BundleEntityRequestInfo> editOTMBundleRequest) throws VException {
        for (BundleEntityRequestInfo requestInfo : newOTFRequests) {
            logger.info("Validation for newOTFRequests");
            validateBatchInformationForBundleEntity(bundleId,requestInfo.getEntityId(), requestInfo.getPackageType(), orgId,true);
        }
        for (BundleEntityRequestInfo requestInfo : newOTMBundleRequests) {
            logger.info("Validation for newOTMBundleRequests");
            validateBatchInformationForBundleEntity(bundleId,requestInfo.getEntityId(), requestInfo.getPackageType(), orgId,true);
        }
        for (BundleEntityRequestInfo requestInfo : editOTFRequests) {
            logger.info("Validation for editOTFRequests");
            validateBatchInformationForBundleEntity(bundleId, requestInfo.getEntityId(), requestInfo.getPackageType(), orgId);
        }
        for (BundleEntityRequestInfo requestInfo : editOTMBundleRequest) {
            logger.info("Validation for editOTMBundleRequest");
            validateBatchInformationForBundleEntity(bundleId, requestInfo.getEntityId(), requestInfo.getPackageType(), orgId);
        }
    }

    private void validateDataForNormalAIO(String bundleId, List<BundleEntityRequestInfo> bundleEntities) throws BadRequestException {
        int size=bundleEntities.size();
        logger.info("Saving entity data for the AIO with size - {}",size);
        List<String> entityIds=bundleEntities.stream().map(BundleEntityRequestInfo::getEntityId).collect(Collectors.toList());
        Map<String, BundleEntity> entityMap =
                Optional.ofNullable(bundleEntityDAO.getAllBundleIdEntityIdData(bundleId,entityIds, Arrays.asList(BundleEntity.Constants.ENTITY_ID,BundleEntity.Constants.PACKAGE_TYPE,BundleEntity.Constants.ENROLLMENT_TYPE)))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .collect(Collectors.toMap(BundleEntity::getId, Function.identity()));
        entityIds=entityMap.values().stream().map(BundleEntity::getEntityId).collect(Collectors.toList());
        for(int i=0;i<size;++i){
            BundleEntityRequestInfo entity = bundleEntities.get(i);
            logger.info("Validating for entity with entity id - {}",entity.getEntityId());
            logger.info("The entire entity data is - \n{}",entity);
            if(PackageType.OTM_BUNDLE.equals(entity.getPackageType()) && (EnrollmentType.UPSELL.equals(entity.getEnrollmentType()) || EnrollmentType.CLICK_TO_ENROLL.equals(entity.getEnrollmentType()))){
                throw new BadRequestException(ErrorCode.ACTION_NOT_ALLOWED,"For a OTM_BUNDLE the enrollment type can't be click to enroll or upsell, it can only be auto enroll or locked");
            }
            if(StringUtils.isEmpty(entity.getId())){
                logger.info("Validating for new data");
                if(entityIds.contains(entity.getEntityId())) {
                    throw new BadRequestException(ErrorCode.ALREADY_EXISTS, "Already data exists for entityId " + entity.getEntityId());
                }
            }else {
                logger.info("Validating for existing data");
                BundleEntity bundleEntity = entityMap.get(entity.getId());
                validateForExistingDataForBulkInsertion(entity, bundleEntity);
            }
            logger.info("Validating for duplication in existing data in payload");
            for(int j=i+1;j<size;++j){
                BundleEntityRequestInfo entityForComparision = bundleEntities.get(j);
                if(entity.getEntityId().equals(entityForComparision.getEntityId())){
                    logger.info("Duplication occurred due to the payload - {}",entityForComparision);
                    throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,"There is duplicate entityId in the request - "+entity.getEntityId());
                }
            }
        }
        List<String> otmBundleIds = bundleEntities.stream().filter(this::isOTMBundle).map(BundleEntityRequestInfo::getEntityId).collect(Collectors.toList());
        logger.info("otmBundleIds - {}",otmBundleIds);
        List<String> allBatchIds = bundleEntities.stream().filter(this::isOTFPackage).map(BundleEntityRequestInfo::getEntityId).collect(Collectors.toList());
        logger.info("batchIds - {}",allBatchIds);
        List<String> allBatchIdsCorrespondingToOTMBundle=
                Optional.ofNullable(otfBundleManager.getOTFBundleByIds(otmBundleIds))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(OTMBundle::getEntities)
                        .flatMap(Collection::stream)
                        .filter(this::isOTFEntity)
                        .map(OTMBundleEntityInfo::getEntityId)
                        .collect(Collectors.toList());
        logger.info("allBatchIdsCorrespondingToOTMBundle - {}",allBatchIdsCorrespondingToOTMBundle);
        allBatchIds.addAll(allBatchIdsCorrespondingToOTMBundle);
        logger.info("allBatchIds - {}",allBatchIds);
        Map<String, List<String>> courseIdBatchIdsMap =
                Optional.ofNullable(batchDAO.getCourseIdsForBatchIds(allBatchIds))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .collect(
                                Collectors.groupingBy(
                                    Batch::getCourseId,
                                    Collectors.mapping(Batch::getId,Collectors.toList())
                                )
                        );
        courseIdBatchIdsMap.forEach(this::logCourseIdBatchIdMap);
        String courseId = courseIdBatchIdsMap.keySet().stream().filter(key -> courseIdBatchIdsMap.get(key).size() > 1).findFirst().orElse(null);
        if(!StringUtils.isEmpty(courseId)){
            throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,"Duplicate entries we are trying to insert for course id "+courseId);
        }

    }

    private void logCourseIdBatchIdMap(String courseId, List<String> batchIds) {
        logger.info("courseId - {}, batchIds - {}",courseId,batchIds);
    }

    private boolean isOTFPackage(BundleEntityRequestInfo bundleEntityRequestInfo) {
        return PackageType.OTF.equals(bundleEntityRequestInfo.getPackageType());
    }

    private boolean isOTMBundle(BundleEntityRequestInfo bundleEntityRequestInfo) {
        return PackageType.OTM_BUNDLE.equals(bundleEntityRequestInfo.getPackageType());
    }

    private void validateDataForSubscription(BundleInfo bundle, List<BundleEntityRequestInfo> bundleEntities) throws BadRequestException {
        int size=bundleEntities.size();
        logger.info("Preparing to save bulk data of size - {}",size);
        List<String> batchIds=bundleEntities.stream().map(BundleEntityRequestInfo::getEntityId).collect(Collectors.toList());
        logger.info("As only batches are allowed for subscription, so all the batch ids are - {}",batchIds);
        String bundleId=bundle.getId();
        Map<String, BundleEntity> entityMap =
                Optional.ofNullable(bundleEntityDAO.getAllBundleIdEntityIdData(bundleId,batchIds, Arrays.asList(BundleEntity.Constants.ENTITY_ID,BundleEntity.Constants.PACKAGE_TYPE,BundleEntity.Constants.GRADE,BundleEntity.Constants.ENROLLMENT_TYPE)))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .collect(Collectors.toMap(BundleEntity::getId, Function.identity()));
        Map<String, List<Integer>> entityIdGradesMap =
                entityMap.values().stream().collect(Collectors.groupingBy(BundleEntity::getEntityId, Collectors.mapping(BundleEntity::getGrade, Collectors.toList())));
        int extraCount = 0;
        for(int i=0;i<size;++i){
            BundleEntityRequestInfo entity = bundleEntities.get(i);
            logger.info("Enity has entity id - {}",entity.getEntityId());
            logger.info("Validating data for entity with entity details - \n{}",entity);
            if(PackageType.OTM_BUNDLE.equals(entity.getPackageType())){
                throw new BadRequestException(ErrorCode.OTM_BUNDLE_CANT_BE_PART_OF_SUBSCRIPTION,"You cann't add OTM Bundles to subscription");
            }
            if(StringUtils.isEmpty(entity.getId())){
                logger.info("Saving a new entity data");
                List<Integer> grades = entityIdGradesMap.get(entity.getEntityId());
                logger.info("Validating for a new entity having the grades - {} and for grades - {}",entity.getGrade(),grades);
                if(ArrayUtils.isNotEmpty(grades) && grades.contains(entity.getGrade())) {
                    throw new BadRequestException(ErrorCode.ALREADY_EXISTS, "Already data exists for entityId-grade combination as " + entity.getEntityId());
                }
                if(EnrollmentType.AUTO_ENROLL.equals(entity.getEnrollmentType())){
                    ++extraCount;
                }
            }else{
                logger.info("Updating an existing entity data");
                logger.info("Enity has entity id - {}",entity.getEntityId());
                logger.info("Validating data for entity with entity details - \n{}",entity);
                BundleEntity bundleEntity = entityMap.get(entity.getId());
                validateForExistingDataForBulkInsertion(entity, bundleEntity);
                if(EnrollmentType.AUTO_ENROLL.equals(entity.getEnrollmentType()) && !bundleEntity.getEnrollmentType().equals(entity.getEnrollmentType())){
                    logger.info("Status changed to auto enroll");
                    ++extraCount;
                }else if(EnrollmentType.AUTO_ENROLL.equals(bundleEntity.getEnrollmentType()) && !bundleEntity.getEnrollmentType().equals(entity.getEnrollmentType())){
                    logger.info("Status changed from auto enroll");
                    --extraCount;
                }
            }
            logger.info("Verifying similarity of all the bundle we are trying to insert.");
            for(int j=i+1;j<size;++j){
                BundleEntityRequestInfo entityForComparision = bundleEntities.get(j);
                if(entity.getEntityId().equals(entityForComparision.getEntityId()) &&
                entity.getGrade().equals(entityForComparision.getGrade())){
                    logger.info("Exception occured for the entity - {}", entityForComparision);
                    throw new BadRequestException(ErrorCode.DUPLICATE_ENTRY,"There are duplicate entityId-grade combination in the request - "+entity.getEntityId()+"-"+entity.getGrade());
                }
            }
        }
        long countOfAutoEnrolledPackagesOfBundle = bundleEntityDAO.getCountOfAutoEnrolledPackagesOfBundle(bundleId);
        int subjectsSize = bundle.getSubject().size();
        if (subjectsSize < countOfAutoEnrolledPackagesOfBundle + extraCount) {
            logger.info("countOfAutoEnrolledPackagesOfBundle - {}, subjectsSize - {}, extraCount - {}",countOfAutoEnrolledPackagesOfBundle,subjectsSize,extraCount);
            throw new BadRequestException(ErrorCode.AUTO_ENROLL_PACKAGE_ADDITION_NOT_ALLOWED, "Auto enroll " +
                    "bundles not allowed as no of subjects in bundles are less than the count of auto enrolled entities");
        }
    }

    private void validateForExistingDataForBulkInsertion(BundleEntityRequestInfo entity, BundleEntity bundleEntity) throws BadRequestException {
        if(Objects.isNull(bundleEntity)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Invalid id to find bundle entity");
        }
        if(!bundleEntity.getPackageType().equals(entity.getPackageType())){
            throw new BadRequestException(ErrorCode.PACKAGE_TYPE_MISMATCH,"Package type can't be changed again and again, please validate before clicking");
        }
        if(!bundleEntity.getEntityId().equals(entity.getEntityId())){
            throw new BadRequestException(ErrorCode.ENTITY_ID_MISMATCH,"If you want to change the entity id then delete and create a new one");
        }
    }

    private boolean getEditOTMData(BundleEntityRequestInfo bundleEntityRequestInfo) {
        return !StringUtils.isEmpty(bundleEntityRequestInfo.getId()) && PackageType.OTM_BUNDLE.equals(bundleEntityRequestInfo.getPackageType());
    }

    private boolean getEditOTFData(BundleEntityRequestInfo bundleEntityRequestInfo) {
        return !StringUtils.isEmpty(bundleEntityRequestInfo.getId()) && PackageType.OTF.equals(bundleEntityRequestInfo.getPackageType());
    }

    private boolean getNewOTMData(BundleEntityRequestInfo bundleEntityRequestInfo) {
        return StringUtils.isEmpty(bundleEntityRequestInfo.getId()) && PackageType.OTM_BUNDLE.equals(bundleEntityRequestInfo.getPackageType());
    }

    private boolean getNewOTFData(BundleEntityRequestInfo bundleEntityRequestInfo) {
        return StringUtils.isEmpty(bundleEntityRequestInfo.getId()) && PackageType.OTF.equals(bundleEntityRequestInfo.getPackageType());
    }

    private void validateForAutoEnrollCountIfNecessary(BundleEntityRequest request, BundleEntity bundleEntity, BundleInfo bundleInfo) throws BadRequestException {
        if(bundleInfo.getIsSubscription() && EnrollmentType.AUTO_ENROLL.equals(request.getEnrollmentType())) {
            logger.info("Validation required for count...");
            long countOfAutoEnrolledPackagesOfBundle = bundleEntityDAO.getCountOfAutoEnrolledPackagesOfBundle(bundleInfo.getId());
            int subjectsSize = bundleInfo.getSubject().size();
            int extraCount = (Objects.nonNull(bundleEntity) && !request.getEnrollmentType().equals(bundleEntity.getEnrollmentType())) || Objects.isNull(bundleEntity)  ? 1 : 0;
            if (subjectsSize < countOfAutoEnrolledPackagesOfBundle + extraCount) {
                throw new BadRequestException(ErrorCode.AUTO_ENROLL_PACKAGE_ADDITION_NOT_ALLOWED, "Auto enroll " +
                        "bundles not allowed as no of subjects in bundles are less than the count of auto enrolled entities");
            }
        }
    }

    private void validateForExistingEntities(BundleEntityRequest request, BundleEntity bundleEntity) throws BadRequestException {
        if(Objects.isNull(bundleEntity)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Invalid id to find bundle entity");
        }
        if(!bundleEntity.getPackageType().equals(request.getPackageType())){
            throw new BadRequestException(ErrorCode.PACKAGE_TYPE_MISMATCH,"Package type can't be changed again, please validate before clicking");
        }
        if(!bundleEntity.getEntityId().equals(request.getEntityId())){
            throw new BadRequestException(ErrorCode.ENTITY_ID_MISMATCH,"If you want to change the entity id then delete and create a new one");
        }
    }

    private Update setMainTagsForBundleEntity(BundleEntity bundleEntity) {
        Update update=new Update();
        String batchId = bundleEntity.getEntityId();
        Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(Collections.singletonList(batchId), false);
        BatchBasicInfo batch = batchMap.get(batchId);
        Map<String, CourseBasicInfo> courseMap = enrollmentManager.getCourseMap(Collections.singletonList(batch.getCourseId()), false);
        CourseBasicInfo course = courseMap.get(batch.getCourseId());
        List<String> subjects = course.getBoardTeacherPairs().stream().map(BoardTeacherPair::getSubject).collect(Collectors.toList());

        bundleEntity.setRecordedVideo(batch.isRecordedVideo());
        update.set(BundleEntity.Constants.RECORDED_VIDEO,batch.isRecordedVideo());

        bundleEntity.setSubjects(subjects);
        update.set(BundleEntity.Constants.SUBJECTS,subjects);

        bundleEntity.setCourseId(batch.getCourseId());
        update.set(BundleEntity.Constants.COURSE_ID,batch.getCourseId());


        bundleEntity.setStartTime(batch.getStartTime());
        update.set(BundleEntity.Constants.START_TIME,batch.getStartTime());


        bundleEntity.setEndTime(batch.getEndTime());
        update.set(BundleEntity.Constants.END_TIME,batch.getEndTime());

        bundleEntity.setCourseTitle(course.getTitle());
        update.set(BundleEntity.Constants.COURSE_TITLE,course.getTitle());

        bundleEntity.setSubscription(true);
        update.set(BundleEntity.Constants.IS_SUBSCRIPTION,true);

        bundleEntity.setSubscriptionPackageTypes(batch.getPackageTypes());
        update.set(BundleEntity.Constants.SUBSCRIPTION_PACKAGES_TYPES,batch.getPackageTypes());

        if (ArrayUtils.isNotEmpty(batch.getSearchTerms())) {
            bundleEntity.setSearchTerms(batch.getSearchTerms());
            update.set(BundleEntity.Constants.SEARCH_TERMS,batch.getSearchTerms());
        }

        // Main tags set up
        List<String> mainTags = new ArrayList<>();
        mainTags.add(bundleEntity.getCourseId());
        mainTags.add(bundleEntity.getGrade().toString());
        mainTags.addAll(bundleEntity.getSubjects());
        mainTags.addAll(Optional.ofNullable(batch.getPackageTypes()).map(Collection::stream).orElseGet(Stream::empty).map(Enum::toString).collect(Collectors.toList()));
        mainTags.add(bundleEntity.getRecordedVideo() ? AIOPACKAGE_TAGS_RECORDED : AIOPACKAGE_TAGS_LIVE);
        if (ArrayUtils.isNotEmpty(batch.getSearchTerms())) {
            mainTags.addAll(batch.getSearchTerms().stream().map(CourseTerm::toString).collect(Collectors.toList()));
        }
        bundleEntity.setMainTags(mainTags);
        update.set(BundleEntity.Constants.MAIN_TAGS,mainTags);

        return update;
    }

    private void validateBatchInformationForBundleEntity(String bundleId, String entityId, PackageType packageType,
                                                         String orgId) throws VException {
        validateBatchInformationForBundleEntity(bundleId,entityId,packageType,orgId,false);
    }
    private void validateBatchInformationForBundleEntity(String bundleId, String entityId, PackageType packageType,
                                                         String orgId,boolean newData) throws VException {
        boolean orgIdExists=!(StringUtils.isEmpty(orgId));
        logger.info("bundleId - {}, entityId - {}, packageType - {}, orgId - {}, orgIdExists - {}, newData - {}",bundleId,entityId,packageType,orgId,orgIdExists,newData);

        List<String> existingCourseIds=new ArrayList<>();
        if(newData){
            logger.info("Validation for new data...");
            List<String> otmBundleIds =
                    Optional.ofNullable(bundleEntityDAO.getAllOTMBundleEntities(bundleId, 0, 100))
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .map(BundleEntity::getEntityId)
                            .collect(Collectors.toList());

            List<String> allExistingBatchIdsOfOTMBundle=
                    Optional.ofNullable(otfBundleManager.getOTFBundleByIds(otmBundleIds))
                            .map(Collection::stream)
                            .orElseGet(Stream::empty)
                            .map(OTMBundle::getEntities)
                            .flatMap(Collection::stream)
                            .filter(this::isOTFEntity)
                            .map(OTMBundleEntityInfo::getEntityId)
                            .collect(Collectors.toList());

            existingCourseIds = batchDAO.getAllDistinctCourseIdsFromBatchIds(allExistingBatchIdsOfOTMBundle);
        }
        logger.info("All the courseId we got are - {}",existingCourseIds);

        if (PackageType.OTF.equals(packageType)) {
            logger.info("Validating for OTF package");
            Batch batch = batchDAO.getById(entityId);
            if (Objects.isNull(batch)) {
                throw new BadRequestException(ErrorCode.INVALID_CONTENT_TYPE, entityId + " : No batch exist with this id");
            }

            if(newData){
                long count = bundleEntityDAO.getCountOfBundleIdCourseIdCombination(bundleId, batch.getCourseId());
                logger.info("Validation for duplicate entry for course id with value of count - {}",count);
                if(count>0){
                    throw new BadRequestException(ErrorCode.ALREADY_BATCH_EXISTS_FOR_THE_COURSE,"There is a batch already there for this course with id "+batch.getCourseId());
                }
            }

            if(newData && existingCourseIds.contains(batch.getCourseId())){
                throw new BadRequestException(ErrorCode.ALREADY_EXISTS,"Some or the other batch of the same course is " +
                        "present in one of the bundle fo batch for the course id - "+batch.getCourseId());
            }

            if(!orgIdExists && !StringUtils.isEmpty(batch.getOrgId())){
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"Adding batch with orgId for a bundle which does not have orgId");
            }

            if(orgIdExists){
                if(!StringUtils.isEmpty(batch.getOrgId())){
                    if(!(orgId.equals(batch.getOrgId()))) {
                        throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "For batch " + entityId + " orgId should be same as bundle orgId");
                    }
                } else{
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"For batch " + entityId + " orgId is empty");
                }
            }

        } else if (PackageType.OTM_BUNDLE.equals(packageType)) {
            logger.info("Validating for OTM bundle");
            OTFBundleInfo otfBundleInfo = otfBundleManager.getOTFBundle(entityId);
            if (Objects.isNull(otfBundleInfo)) {
                throw new BadRequestException(ErrorCode.INVALID_CONTENT_TYPE, entityId + " : No OTM bundle exist for this id");
            }
            if (ArrayUtils.isEmpty(otfBundleInfo.getEntities())) {
                throw new BadRequestException(ErrorCode.OTF_INVALID_HANDLE_RECORD_REQ,
                        "OTFBundle can't be null or empty entities are not allowed inside an OTF bundle");
            }
            List<OTMBundleEntityInfo> entities = otfBundleInfo.getEntities();
            List<String> allBatchIdsForOTMBundle = entities.stream().filter(this::isOTFEntity).map(OTMBundleEntityInfo::getEntityId).collect(Collectors.toList());
            Map<String, BatchBasicInfo> batchMap = enrollmentManager.getBatchMap(allBatchIdsForOTMBundle, false);
            for (OTMBundleEntityInfo entity : entities) {
                String currentEntityId = entity.getEntityId();
                if (isOTFEntity(entity) && !StringUtils.isEmpty(currentEntityId)) {
                    BatchBasicInfo batch=batchMap.get(currentEntityId);
                    if(newData && existingCourseIds.contains(batch.getCourseId())){
                        throw new BadRequestException(ErrorCode.ALREADY_EXISTS,"Some or the other batch of the same course is " +
                                "present in one of the bundle fo batch for the course id - "+batch.getCourseId());
                    }
                    long count = bundleEntityDAO.getCountOfBundleIdCourseIdCombination(bundleId, batch.getCourseId());
                    if(count>0){
                        throw new BadRequestException(ErrorCode.PACKAGE_ALREADY_PRESENT,"A package is already present " +
                                "with the same parameters present in this bundle of batch having course id "+batchMap.get(currentEntityId).getCourseId());
                    }
                }else {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,
                            entityId + " : is not a bundle of batch or a batch, so it is not permitted");
                }
            }
        }
    }

    private void autoEnrollIntoPackage(String bundleId, BundleEntity bundleEntity) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("bundleId", bundleId);
        payload.put("bundleEntity", bundleEntity);
        String courseId = null;
        if (PackageType.OTF.equals(bundleEntity.getPackageType())) {
            Map<String, BatchBasicInfo> batchInfoMap = enrollmentManager.getBatchMap(Collections.singletonList(bundleEntity.getEntityId()), false);
            courseId = batchInfoMap.get(bundleEntity.getEntityId()).getCourseId();
        }
        logger.info("sending auto enroll task to bundleOps queue");
        logger.info("AUTO_ENROLL_ON_COURSE_ADDITION_IN_BUNDLE operation for bundleId {} with payload {}",bundleId,payload.toString());
        sqsManager.sendToSQS(SQSQueue.BUNDLE_AUTO_ENROLL_OPS, SQSMessageType.AUTO_ENROLL_ON_COURSE_ADDITION_IN_BUNDLE, gson.toJson(payload), courseId);
    }

    public PlatformBasicResponse removeBundleEntity(List<String> ids) throws BadRequestException {
        if(ArrayUtils.isEmpty(ids)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Ids for bundle package reference removal can't be null");
        }
        bundleEntityDAO.deleteMultipleEntitiesById(ids);
        return new PlatformBasicResponse();
    }

    public List<BundleEntity> getBundleEntityDataForBundle(GetBundlePackageReferenceRequest request) throws BadRequestException {
        return bundleEntityDAO.filterForBundleEntityDataForBundle(request.getBundleId(),request.getEnrollmentType(),
                request.getEntityId(),request.getCourseType(),request.getGrade(),request.getCourseId(),request.getStart(),request.getSize(),
                null);    }

    public List<BundleEntity> getBundleEntityDataForMultipleBundles(GetBundlePackageReferenceRequest request) throws BadRequestException {
        return bundleEntityDAO.getPackageDataForMultipleBundles(request.getBundleIds(),request.getStart(),request.getSize(),null);
    }

    private Boolean isOTFEntity(OTMBundleEntityInfo entity) {
        return EntityType.OTF.equals(entity.getEntityType());
    }

    public List<BundleEntity> getBundleEntityDataForBundleForSalesConversion(com.vedantu.subscription.pojo.bundle.GetBundlePackageReferenceRequest request) throws BadRequestException {
        return bundleEntityDAO.getBundleEntityDataForBundleForSalesConversion(request);
    }

}
