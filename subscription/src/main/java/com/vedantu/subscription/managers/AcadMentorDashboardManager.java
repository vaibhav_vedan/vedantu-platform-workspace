package com.vedantu.subscription.managers;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.scheduling.pojo.AMInclassAggregateResp;
import com.vedantu.subscription.viewobject.response.amdashboard.AMDashboardContentInfoRes;
import com.vedantu.subscription.viewobject.response.amdashboard.AMDashboardStudentData;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.WebUtils;

@Service
public class AcadMentorDashboardManager {

	private static final String LMS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("LMS_ENDPOINT");
	private static final String SCHEDULING_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("SCHEDULING_ENDPOINT");

    @Autowired
    private AcadMentorStudentsManager acadMentorStudentsManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AcadMentorDashboardManager.class);

    public List<AMDashboardStudentData> fetchDataByAcadMentor(String acadMentorEmail, Long fromTime, Long thruTime) throws VException, UnsupportedEncodingException {
        logger.info("Logger: Getting list of students by acadMentorEmail " + acadMentorEmail);
        List<User> students = acadMentorStudentsManager.getStudentsByAcadMentor(acadMentorEmail, 0, 500);
        logger.info("Logger: students: " + students);
        List<AMDashboardStudentData> studentDashboardDataList = new ArrayList<>();
        List<String> studentDashboardDataStrings = new ArrayList<>();
        for (User student : students) {
        	AMInclassAggregateResp inClassData = fetchInClassData(student.getId(), fromTime, thruTime);
            List<AMDashboardContentInfoRes> assignmentData = fetchAssignmentData(student.getId(), fromTime, thruTime);
            List<AMDashboardContentInfoRes> testData = fetchTestData(student.getId(), fromTime, thruTime);
            logger.info("Logger: AssignmentData returned: " + assignmentData);
            logger.info("Logger: TestData returned: " + testData);

            AMDashboardStudentData studentDashboardData = new AMDashboardStudentData(student, inClassData, assignmentData, testData);			
			
            studentDashboardDataList.add(studentDashboardData);
        }

        logger.info("List of studentDashboard strings: " + studentDashboardDataStrings);
        // return studentDashboardDataStrings;
        return studentDashboardDataList;
    }
    
    private AMInclassAggregateResp fetchInClassData(Long studentId, Long fromTime, Long thruTime) throws VException {
		String url = SCHEDULING_ENDPOINT + "/am-inclass/getInClassAggregate/" + studentId + "?" + "fromTime=" + fromTime + "&" + "thruTime=" + thruTime;
		ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
		VExceptionFactory.INSTANCE.parseAndThrowException(resp);
		String respString = resp.getEntity(String.class);
		return new Gson().fromJson(respString, AMInclassAggregateResp.class);
	}

    private List<AMDashboardContentInfoRes> fetchAssignmentData(Long studentId, Long fromTime, Long thruTime) throws VException {
        String url = LMS_ENDPOINT + "/updatecontentinfo/getAssignmentByStudent/" + studentId + "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        return new ArrayList(Arrays.asList(new Gson().fromJson(respString, AMDashboardContentInfoRes[].class)));
    }

    private List<AMDashboardContentInfoRes> fetchTestData(Long studentId, Long fromTime, Long thruTime) throws VException {
        String url = LMS_ENDPOINT + "/updatecontentinfo/getTestByStudent/" + studentId + "?fromTime=" + fromTime + "&thruTime=" + thruTime;
        ClientResponse resp = WebUtils.INSTANCE.doCall(url, HttpMethod.GET, null, true);
        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String respString = resp.getEntity(String.class);
        return new ArrayList(Arrays.asList(new Gson().fromJson(respString, AMDashboardContentInfoRes[].class)));
    }

}
