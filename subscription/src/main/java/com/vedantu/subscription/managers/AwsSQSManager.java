package com.vedantu.subscription.managers;

import com.amazonaws.auth.policy.Policy;
import com.amazonaws.auth.policy.Principal;
import com.amazonaws.auth.policy.Resource;
import com.amazonaws.auth.policy.Statement;
import com.amazonaws.auth.policy.Statement.Effect;
import com.amazonaws.auth.policy.actions.SQSActions;
import com.amazonaws.auth.policy.conditions.ConditionFactory;
import com.amazonaws.metrics.AwsSdkMetrics;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSAsync;
import com.amazonaws.services.sns.AmazonSNSAsyncClientBuilder;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.SubscribeResult;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.CreateQueueResult;
import com.amazonaws.services.sqs.model.GetQueueAttributesRequest;
import com.amazonaws.services.sqs.model.GetQueueAttributesResult;
import com.amazonaws.services.sqs.model.QueueAttributeName;
import com.amazonaws.services.sqs.model.SetQueueAttributesRequest;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.aws.AbstractAwsSQSManagerNew;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.HashMap;
import java.util.Map;

@Service
public class AwsSQSManager extends AbstractAwsSQSManagerNew {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;


    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AwsSQSManager.class);

    public static String arn;
    public static String env;

    private AmazonSNSAsync snsClient;

    private Map<String, String> queueUrlMap = new HashMap<>();

    public AwsSQSManager() {
        super();
        logger.info("initializing AwsSQSManager");
        try {
            env = ConfigUtils.INSTANCE.getStringValue("environment");
            arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");
            snsClient = AmazonSNSAsyncClientBuilder.standard()
                    .withRegion(Regions.AP_SOUTHEAST_1)
                    .build();
            logger.info("Initialized SNS client");
            if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
                try {
                    CreateQueueRequest cqr = new CreateQueueRequest(SQSQueue.NOTIFICATION_QUEUE
                            .getQueueName(env));
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(cqr);
                    logger.info("created queue for challenges");
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }


                try {
                    CreateQueueRequest session_consumption_dl = new CreateQueueRequest(SQSQueue.CONSUMPTION_DL.getQueueName(env));
                    //session_consumption_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "false");
                    //session_consumption_dl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.CONSUMPTION_QUEUE.getVisibilityTimeout());
                    sqsClient.createQueue(session_consumption_dl);
                    //String consumptionQueueUrl = result.getQueueUrl();


                    CreateQueueRequest session_consumption = new CreateQueueRequest(SQSQueue.CONSUMPTION_QUEUE.getQueueName(env));
                    //session_consumption.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "false");
                    session_consumption.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.CONSUMPTION_QUEUE.getVisibilityTimeout());
                    CreateQueueResult result = sqsClient.createQueue(session_consumption);
                    String consumptionQueueUrl = result.getQueueUrl();
                    queueUrlMap.put(SQSQueue.CONSUMPTION_QUEUE.getQueueName(env), consumptionQueueUrl);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }


                try {
                    logger.info("create queue for consumption of session");
                    CreateQueueRequest session_consumption_trigger_dl = new CreateQueueRequest(SQSQueue.TRIGGER_SESSION_CONSUMPTION_DL.getQueueName(env));
                    //session_consumption_trigger_dl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getVisibilityTimeout());
                    //session_consumption_trigger_dl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "false");
                    sqsClient.createQueue(session_consumption_trigger_dl);
                    //String triggerConsumptionQueueUrl = createQueueResult.getQueueUrl();


                    CreateQueueRequest session_consumption_trigger = new CreateQueueRequest(SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getQueueName(env));
                    session_consumption_trigger.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getVisibilityTimeout());
                    //session_consumption_trigger.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "false");
                    CreateQueueResult createQueueResult = sqsClient.createQueue(session_consumption_trigger);
                    String triggerConsumptionQueueUrl = createQueueResult.getQueueUrl();

                    logger.info("Created queue for consumption of trigger session consumption");

                    queueUrlMap.put(SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getQueueName(env), triggerConsumptionQueueUrl);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }


                try {
                    logger.info("create queue for acadmentor dashboard");

                    CreateQueueRequest acadmentor_dashboard_dl = new CreateQueueRequest(SQSQueue.ACAD_MENTOR_DASHBOARD_DL.getQueueName(env));
                    sqsClient.createQueue(acadmentor_dashboard_dl);


                    CreateQueueRequest acadmentor_dashboard = new CreateQueueRequest(SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE.getQueueName(env));
                    acadmentor_dashboard.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE.getVisibilityTimeout());
                    CreateQueueResult acadmentor_dashboard_queueResult = sqsClient.createQueue(acadmentor_dashboard);
                    String acadmentor_dashboard_queue_url = acadmentor_dashboard_queueResult.getQueueUrl();

                    logger.info("created queue for acadmentor dashboard");

                    queueUrlMap.put(SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE.getQueueName(env), acadmentor_dashboard_queue_url);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // create BUNDLE_OPS queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_OPS_DL.getQueueName(env));
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_OPS_DL.getQueueName(env));

                    // create queue for BUNDLE_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_OPS.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_OPS.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_OPS
                            .getQueueName(env), SQSQueue.BUNDLE_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                try {
                    // create BUNDLE_OPS queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_NEW_ENROLL_OPS_DL.getQueueName(env));

                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_NEW_ENROLL_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_NEW_ENROLL_OPS_DL.getQueueName(env));

                    // create queue for BUNDLE_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_NEW_ENROLL_OPS.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_NEW_ENROLL_OPS.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_NEW_ENROLL_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_NEW_ENROLL_OPS
                            .getQueueName(env), SQSQueue.BUNDLE_NEW_ENROLL_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // create BUNDLE_OPS queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE_DL.getQueueName(env));

                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE_DL.getQueueName(env));

                    // create queue for BUNDLE_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE
                            .getQueueName(env), SQSQueue.BUNDLE_ENROLL_STATUS_QUEUE_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                try {
                    // create BUNDLE_OPS queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BATCH_ENROLL_STATUS_QUEUE_DL.getQueueName(env));

                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BATCH_ENROLL_STATUS_QUEUE_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BATCH_ENROLL_STATUS_QUEUE_DL.getQueueName(env));

                    // create queue for BUNDLE_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BATCH_ENROLL_STATUS_QUEUE.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BATCH_ENROLL_STATUS_QUEUE.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BATCH_ENROLL_STATUS_QUEUE.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BATCH_ENROLL_STATUS_QUEUE
                            .getQueueName(env), SQSQueue.BATCH_ENROLL_STATUS_QUEUE_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // create BUNDLE_OPS queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_OPS_2_DL.getQueueName(env));
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_OPS_2_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_OPS_2_DL.getQueueName(env));

                    // create queue for BUNDLE_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_OPS_2.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_OPS_2.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_OPS_2.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_OPS_2
                            .getQueueName(env), SQSQueue.BUNDLE_OPS_2_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                /*----------------BUNDLE AUTO ENROLLMENT OPERATIONS------------------*/
                try {
                    // create BUNDLE_AUTO_ENROLL_OPS_DL queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_AUTO_ENROLL_OPS_DL.getQueueName(env));
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_AUTO_ENROLL_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_AUTO_ENROLL_OPS_DL.getQueueName(env));

                    // create queue for BUNDLE_AUTO_ENROLL_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_AUTO_ENROLL_OPS.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_AUTO_ENROLL_OPS.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_AUTO_ENROLL_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_AUTO_ENROLL_OPS
                            .getQueueName(env), SQSQueue.BUNDLE_AUTO_ENROLL_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // create BUNDLE_CALENDAR_OPS_DL queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_CALENDAR_OPS_DL.getQueueName(env));
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_CALENDAR_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_CALENDAR_OPS_DL.getQueueName(env));

                    // create queue for BUNDLE_CALENDAR_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_CALENDAR_OPS.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_CALENDAR_OPS.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_CALENDAR_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_CALENDAR_OPS
                            .getQueueName(env), SQSQueue.BUNDLE_CALENDAR_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // create BUNDLE_CONTENT_SHARE_OPS_DL queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_CONTENT_SHARE_OPS_DL.getQueueName(env));
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_CONTENT_SHARE_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_CONTENT_SHARE_OPS_DL.getQueueName(env));

                    // create queue for BUNDLE_CONTENT_SHARE_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_CONTENT_SHARE_OPS.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_CONTENT_SHARE_OPS.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_CONTENT_SHARE_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_CONTENT_SHARE_OPS
                            .getQueueName(env), SQSQueue.BUNDLE_CONTENT_SHARE_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // create BUNDLE_GTT_CREATION_OPS_DL queue dead letter
                    CreateQueueRequest bundleOpsDl = new CreateQueueRequest(SQSQueue.BUNDLE_GTT_CREATION_OPS_DL.getQueueName(env));
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    bundleOpsDl.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_GTT_CREATION_OPS_DL.getVisibilityTimeout());
                    sqsClient.createQueue(bundleOpsDl);
                    logger.info("queue created : " + SQSQueue.BUNDLE_GTT_CREATION_OPS_DL.getQueueName(env));

                    // create queue for BUNDLE_GTT_CREATION_OPS
                    CreateQueueRequest bundleOps = new CreateQueueRequest(SQSQueue.BUNDLE_GTT_CREATION_OPS.getQueueName(env));
                    bundleOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.BUNDLE_GTT_CREATION_OPS.getVisibilityTimeout());
                    bundleOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(bundleOps);
                    logger.info("created queue:" + SQSQueue.BUNDLE_GTT_CREATION_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.BUNDLE_GTT_CREATION_OPS
                            .getQueueName(env), SQSQueue.BUNDLE_GTT_CREATION_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }


                // -------------------------------- GAME JOURNEY --------------------------------------
                try {
                    // -- init -- queue creation and config for base instalments
                    logger.info("creating dead letter queue for game journey operations");
                    CreateQueueRequest deadLetterRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_OPS_DL
                            .getQueueName(env));
                    deadLetterRequest.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    deadLetterRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.GAME_JOURNEY_OPS_DL.getVisibilityTimeout());
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(deadLetterRequest);
                    logger.info("dead letter queue for game journey operations created" + SQSQueue.GAME_JOURNEY_OPS_DL.getQueueName(env));
                    logger.info("creating queue for bundle instalment operations");
                    CreateQueueRequest queueRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_OPS
                            .getQueueName(env));
                    queueRequest.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    queueRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.GAME_JOURNEY_OPS.getVisibilityTimeout());
                    queueRequest.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(queueRequest);
                    logger.info("queue for game journey operations created" + SQSQueue.GAME_JOURNEY_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.GAME_JOURNEY_OPS
                            .getQueueName(env), SQSQueue.GAME_JOURNEY_OPS_DL
                            .getQueueName(env), 6);

                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }
                // -------------------------------- GAME JOURNEY SESSION --------------------------------------
                try {
                    // -- init -- queue creation and config for base instalments
                    logger.info("creating dead letter queue for game journey operations");
                    CreateQueueRequest deadLetterRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_SESSION_STANDARD_DL
                            .getQueueName(env));
                    deadLetterRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.GAME_JOURNEY_SESSION_STANDARD_DL.getVisibilityTimeout());
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(deadLetterRequest);
                    logger.info("dead letter queue for game journey operations created" + SQSQueue.GAME_JOURNEY_SESSION_STANDARD_DL.getQueueName(env));
                    logger.info("creating queue for bundle instalment operations");
                    CreateQueueRequest queueRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_SESSION_STANDARD
                            .getQueueName(env));
                    queueRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.GAME_JOURNEY_SESSION_STANDARD.getVisibilityTimeout());
                    sqsClient.createQueue(queueRequest);
                    logger.info("queue for game journey operations created" + SQSQueue.GAME_JOURNEY_SESSION_STANDARD.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.GAME_JOURNEY_SESSION_STANDARD
                            .getQueueName(env), SQSQueue.GAME_JOURNEY_SESSION_STANDARD_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                // -------------------------------- GAME JOURNEY SESSION --------------------------------------
                try {
                    // -- init -- queue creation and config for base instalments
                    logger.info("creating dead letter queue for game journey operations");
                    CreateQueueRequest deadLetterRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS_DL
                            .getQueueName(env));
                    deadLetterRequest.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    deadLetterRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS_DL.getVisibilityTimeout());
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(deadLetterRequest);
                    logger.info("dead letter queue for game journey operations created" + SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS_DL.getQueueName(env));
                    logger.info("creating queue for bundle instalment operations");
                    CreateQueueRequest queueRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS
                            .getQueueName(env));
                    queueRequest.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    queueRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS.getVisibilityTimeout());
                    queueRequest.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(queueRequest);
                    logger.info("queue for game journey operations created" + SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS
                            .getQueueName(env), SQSQueue.GAME_JOURNEY_SESSION_DOWNLOAD_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                // -------------------------------- GAME TEST --------------------------------------
                try {
                    // -- init -- queue creation and config for base instalments
                    logger.info("creating dead letter queue for game journey operations");
                    CreateQueueRequest deadLetterRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_TEST_OPS_DL
                            .getQueueName(env));
                    deadLetterRequest.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    deadLetterRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.GAME_JOURNEY_TEST_OPS_DL.getVisibilityTimeout());
                    //cqr.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(deadLetterRequest);
                    logger.info("dead letter queue for game journey operations created" + SQSQueue.GAME_JOURNEY_TEST_OPS_DL.getQueueName(env));
                    logger.info("creating queue for bundle instalment operations");
                    CreateQueueRequest queueRequest = new CreateQueueRequest(SQSQueue.GAME_JOURNEY_TEST_OPS
                            .getQueueName(env));
                    queueRequest.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    queueRequest.addAttributesEntry(QueueAttributeName.VisibilityTimeout.toString(), SQSQueue.GAME_JOURNEY_TEST_OPS.getVisibilityTimeout());
                    queueRequest.addAttributesEntry(QueueAttributeName.ContentBasedDeduplication.name(), "true");
                    sqsClient.createQueue(queueRequest);
                    logger.info("queue for game journey operations created" + SQSQueue.GAME_JOURNEY_TEST_OPS.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.GAME_JOURNEY_TEST_OPS
                            .getQueueName(env), SQSQueue.GAME_JOURNEY_TEST_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // queue for END_ENROLMENT_OPS
                    CreateQueueRequest endEnrollmentOPS = new CreateQueueRequest(SQSQueue.END_ENROLLMENT_OPS.getQueueName(env));
                    endEnrollmentOPS.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.END_ENROLLMENT_OPS.getVisibilityTimeout());
                    endEnrollmentOPS.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(endEnrollmentOPS);
                    logger.info("sqs queue created -- end enrollment operations" + SQSQueue.END_ENROLLMENT_OPS.getQueueName(env));

                    // dead letter queue for END_ENROLLMENT_OPS
                    CreateQueueRequest endEnrollmentOpsDl = new CreateQueueRequest(SQSQueue.END_ENROLLMENT_OPS_DL.getQueueName(env));
                    endEnrollmentOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(endEnrollmentOpsDl);
                    logger.info("dead letter queue created -- end enrollment operations" + SQSQueue.END_ENROLLMENT_OPS_DL.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.END_ENROLLMENT_OPS
                            .getQueueName(env), SQSQueue.END_ENROLLMENT_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // subscription package operations queue
                    CreateQueueRequest subscriptionPackageOps = new CreateQueueRequest(SQSQueue.SUBSCRIPTION_PACKAGE_OPS.getQueueName(env));
                    subscriptionPackageOps.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SUBSCRIPTION_PACKAGE_OPS.getVisibilityTimeout());
                    subscriptionPackageOps.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(subscriptionPackageOps);
                    logger.info("sqs queue created -- subscriptionPackage operations" + SQSQueue.SUBSCRIPTION_PACKAGE_OPS.getQueueName(env));

                    // dead letter queue for subscription package operations
                    CreateQueueRequest subscriptionPackageOpsDl = new CreateQueueRequest(SQSQueue.SUBSCRIPTION_PACKAGE_OPS_DL.getQueueName(env));
                    subscriptionPackageOpsDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(subscriptionPackageOpsDl);
                    logger.info("dead letter queue created -- end enrollment operations" + SQSQueue.SUBSCRIPTION_PACKAGE_OPS_DL.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.SUBSCRIPTION_PACKAGE_OPS
                            .getQueueName(env), SQSQueue.SUBSCRIPTION_PACKAGE_OPS_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                try {
                    // Gamification Tool CSV Download Params Queue
                    CreateQueueRequest gamificationToolDownloadCSV = new CreateQueueRequest(SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD.getQueueName(env));
                    gamificationToolDownloadCSV.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD.getVisibilityTimeout());
                    gamificationToolDownloadCSV.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(gamificationToolDownloadCSV);
                    logger.info("sqs queue created -- gamificationToolDownloadCSV " + SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD.getQueueName(env));

                    // dead letter queue for subscription package operations
                    CreateQueueRequest gamificationToolDownloadCSVDl = new CreateQueueRequest(SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD_DL.getQueueName(env));
                    gamificationToolDownloadCSVDl.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                    sqsClient.createQueue(gamificationToolDownloadCSVDl);
                    logger.info("dead letter queue created -- gamificationToolDownloadCSV" + SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD_DL.getQueueName(env));

                    assignDeadLetterQueue(SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD
                            .getQueueName(env), SQSQueue.GAMIFICATION_TOOL_CSV_DOWNLOAD_DL
                            .getQueueName(env), 6);
                } catch (Exception e) {
                    logger.error(e.getMessage(), e);
                }

                // communicate In Bulk Queue
                CreateQueueRequest communicateInBulkQueue = new CreateQueueRequest(SQSQueue.COMMUNICATE_IN_BULK_QUEUE.getQueueName(env));
                communicateInBulkQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.COMMUNICATE_IN_BULK_QUEUE.getVisibilityTimeout());
                communicateInBulkQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(communicateInBulkQueue);
                logger.info("sqs queue created -- communicate In bulk " + SQSQueue.COMMUNICATE_IN_BULK_QUEUE.getQueueName(env));

                // dead letter queue for communicate in Bulk
                CreateQueueRequest communicateInBulkDlQueue = new CreateQueueRequest(SQSQueue.COMMUNICATE_IN_BULK_DL_QUEUE.getQueueName(env));
                communicateInBulkDlQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
                sqsClient.createQueue(communicateInBulkDlQueue);
                logger.info("dead letter queue created -- communicate In Bulk Dl Queue" + SQSQueue.COMMUNICATE_IN_BULK_DL_QUEUE.getQueueName(env));

                assignDeadLetterQueue(SQSQueue.COMMUNICATE_IN_BULK_QUEUE
                        .getQueueName(env), SQSQueue.COMMUNICATE_IN_BULK_DL_QUEUE
                        .getQueueName(env), 3);


            }
        } catch (Exception e) {
            logger.error("Error in initializing AwsSQSManager " + e.getMessage());
        }

        // -------------------------- SECTION  Queue creation -----------------------------------
        try {
            // Section Queue
            CreateQueueRequest sectionQ = new CreateQueueRequest(SQSQueue.SECTION_QUEUE.getQueueName(env));
            sectionQ.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SECTION_QUEUE.getVisibilityTimeout());
            sectionQ.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
            sqsClient.createQueue(sectionQ);
            logger.info("sqs queue created -- sectionQ " + SQSQueue.SECTION_QUEUE.getQueueName(env));

            // dead letter queue for Section
            CreateQueueRequest sectionDQ = new CreateQueueRequest(SQSQueue.SECTION_DL_QUEUE.getQueueName(env));
            sectionDQ.addAttributesEntry(QueueAttributeName.FifoQueue.name(), "true");
            sqsClient.createQueue(sectionDQ);
            logger.info("dead letter queue created -- sectionDQ" + SQSQueue.SECTION_DL_QUEUE.getQueueName(env));

            assignDeadLetterQueue(SQSQueue.SECTION_QUEUE
                    .getQueueName(env), SQSQueue.SECTION_DL_QUEUE
                    .getQueueName(env), 6);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        try {
            // Section Notifications Queue
            CreateQueueRequest sectionNotificationsQ = new CreateQueueRequest(SQSQueue.SECTION_NOTIFICATIONS_QUEUE.getQueueName(env));
            sectionNotificationsQ.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), SQSQueue.SECTION_NOTIFICATIONS_QUEUE.getVisibilityTimeout());
            sqsClient.createQueue(sectionNotificationsQ);
            logger.info("sqs queue created -- sectionNotificationQ " + SQSQueue.SECTION_NOTIFICATIONS_QUEUE.getQueueName(env));

            // dead letter queue for Section
            CreateQueueRequest sectionNotificationsDQ = new CreateQueueRequest(SQSQueue.SECTION_NOTIFICATIONS_DL_QUEUE.getQueueName(env));
            sqsClient.createQueue(sectionNotificationsDQ);
            logger.info("dead letter queue created -- sectionNotificationsDQ" + SQSQueue.SECTION_NOTIFICATIONS_DL_QUEUE.getQueueName(env));

            assignDeadLetterQueue(SQSQueue.SECTION_NOTIFICATIONS_QUEUE
                    .getQueueName(env), SQSQueue.SECTION_NOTIFICATIONS_DL_QUEUE
                    .getQueueName(env), 6);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

        // --------------------------------- END of SECTION ----------------------------------

        try {
            // Course Plan Hours Transaction Queue
            SQSQueue mainQueue = SQSQueue.COURSE_PLAN_HOURS_TRANSACTION;
            CreateQueueRequest coursePlanHoursTxnQueue = new CreateQueueRequest(mainQueue.getQueueName(env));
            coursePlanHoursTxnQueue.addAttributesEntry(QueueAttributeName.VisibilityTimeout.name(), mainQueue.getVisibilityTimeout());
            coursePlanHoursTxnQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), String.valueOf(mainQueue.getFifo()));
            sqsClient.createQueue(coursePlanHoursTxnQueue);
            logger.info("sqs queue created -- coursePlanHoursTxnQueue " + mainQueue.getQueueName(env));

            // dead letter queue for subscription package operations
            SQSQueue deadLetterQueue = SQSQueue.COURSE_PLAN_HOURS_TRANSATION_DL;
            CreateQueueRequest coursePlanHoursTxnDlQueue = new CreateQueueRequest(deadLetterQueue.getQueueName(env));
            coursePlanHoursTxnDlQueue.addAttributesEntry(QueueAttributeName.FifoQueue.name(), String.valueOf(deadLetterQueue.getFifo()));
            sqsClient.createQueue(coursePlanHoursTxnDlQueue);
            logger.info("dead letter queue created -- coursePlanHoursTxnQueue" + deadLetterQueue.getQueueName(env));

            assignDeadLetterQueue(mainQueue.getQueueName(env), deadLetterQueue.getQueueName(env), 10);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
    }

    public String getTopicArn(String topic) {
        return "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic + "_" + env.toUpperCase();
    }

    public String getTopicNonEnvArn(String topic) {
        return "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic;
    }

    private void assignDeadLetterQueue(String src_queue_name, String dl_queue_name, Integer maxReceive) {
        if (maxReceive == null) {
            maxReceive = 4;
        }
        // Get dead-letter queue ARN
        String dl_queue_url = sqsClient.getQueueUrl(dl_queue_name)
                .getQueueUrl();

        GetQueueAttributesResult queue_attrs = sqsClient.getQueueAttributes(
                new GetQueueAttributesRequest(dl_queue_url)
                        .withAttributeNames("QueueArn"));

        String dl_queue_arn = queue_attrs.getAttributes().get("QueueArn");

        // Set dead letter queue with redrive policy on source queue.
        String src_queue_url = sqsClient.getQueueUrl(src_queue_name)
                .getQueueUrl();

        SetQueueAttributesRequest request = new SetQueueAttributesRequest()
                .withQueueUrl(src_queue_url)
                .addAttributesEntry("RedrivePolicy",
                        "{\"maxReceiveCount\":\"" + maxReceive + "\", \"deadLetterTargetArn\":\""
                                + dl_queue_arn + "\"}");

        sqsClient.setQueueAttributes(request);
        logger.info("assigned dead letter queue: " + dl_queue_name + "  to:" + src_queue_name);

    }

    public void setPermissions(String sqsArn, String queueUrl) {
        String topicArn = "arn:aws:sns:ap-southeast-1:" + arn + ":*";
        Statement statement = new Statement(Effect.Allow)
                .withActions(SQSActions.SendMessage)
                .withPrincipals(new Principal("*"))
                .withConditions(ConditionFactory.newSourceArnCondition(topicArn))
                .withResources(new Resource(sqsArn));
        Policy policy = new Policy("SubscriptionPermission")
                .withStatements(statement);

        HashMap<String, String> attributes = new HashMap<String, String>();
        attributes.put("Policy", policy.toJson());
        SetQueueAttributesRequest request = new SetQueueAttributesRequest(queueUrl, attributes);

        sqsClient.setQueueAttributes(request);
    }

    public void createSubscription(String topicArn, String queueName) {
        String queueUrl = queueUrlMap.get(queueName);
        GetQueueAttributesRequest queueAttributesRequest = new GetQueueAttributesRequest(queueUrl).withAttributeNames("All");
        GetQueueAttributesResult queueAttributesResult = sqsClient.getQueueAttributes(queueAttributesRequest);
        Map<String, String> sqsAttributeMap = queueAttributesResult.getAttributes();
        String sqsArn = sqsAttributeMap.get("QueueArn");
        SubscribeRequest subscribeRequest = new SubscribeRequest(topicArn, "sqs", sqsArn);
        SubscribeResult subscribeResult = snsClient.subscribe(subscribeRequest);
        logger.info("Subscription created successfully for queue: " + queueName);

    }

    public void triggerAsyncSQS(SQSQueue queue, SQSMessageType messageType, String message, String groupId) {
        Map<String, Object> payload = new HashMap<>();
        payload.put("queue", queue);
        payload.put("messageType", messageType);
        payload.put("message", message);
        payload.put("groupId", groupId);
        if (StringUtils.isEmpty(env) || env.equals("LOCAL")) {
            logger.info("Env is local, so not sending any notification via sns");
            return;
        }
        AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.TRIGGER_SQS, payload);
        asyncTaskFactory.executeTask(params);
    }


    @PostConstruct
    public void createSNSSubscription() {
        if (!(StringUtils.isEmpty(env) || env.equals("LOCAL"))) {
            setPermissions(getQueueArn(SQSQueue.CONSUMPTION_QUEUE), getQueueURL(SQSQueue.CONSUMPTION_QUEUE));
            setPermissions(getQueueArn(SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE), getQueueURL(SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE));
            assignDeadLetterQueue(SQSQueue.CONSUMPTION_QUEUE.getQueueName(env), SQSQueue.CONSUMPTION_DL.getQueueName(env), 4);
            assignDeadLetterQueue(SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getQueueName(env), SQSQueue.TRIGGER_SESSION_CONSUMPTION_DL.getQueueName(env), 4);
            assignDeadLetterQueue(SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE.getQueueName(env), SQSQueue.ACAD_MENTOR_DASHBOARD_DL.getQueueName(env), 4);
            createSubscription(getTopicArn(SNSTopicOTF.OTM_SNAPSHOT_SESSION_ENDED.toString()), SQSQueue.CONSUMPTION_QUEUE.getQueueName(env));
            createSubscription(getTopicNonEnvArn(SNSTopicOTF.CRON_CHIME_3HOURLY.toString()), SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getQueueName(env));
            createSubscription(getTopicNonEnvArn(SNSTopicOTF.CRON_CHIME_DAILY.toString()), SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getQueueName(env));
            createSubscription(getTopicNonEnvArn(SNSTopicOTF.CRON_CHIME_DAILY.toString()), SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE.getQueueName(env));
        }

    }

    @PreDestroy
    @Override
    public void cleanUp() {
        try {
            if (snsClient != null) {
                snsClient.shutdown();
            }
            if (sqsClient != null) {
                sqsClient.shutdown();
            }
            com.amazonaws.http.IdleConnectionReaper.shutdown();
            AwsSdkMetrics.unregisterMetricAdminMBean();
        } catch (Exception e) {
            logger.error("Error in closing AmazonSMSManager connection ", e);
        }
    }
}
