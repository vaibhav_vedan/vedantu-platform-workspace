package com.vedantu.subscription.managers;

import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.dao.BundleTestDAO;
import com.vedantu.subscription.dao.BundleVideoDAO;
import com.vedantu.subscription.dao.VideoEngagementDAO;
import com.vedantu.subscription.entities.mongo.BundleVideo;
import com.vedantu.subscription.entities.mongo.VideoEngagement;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.viewobject.request.GetBundleTestVideoReq;
import com.vedantu.subscription.viewobject.request.VideoEngagementReq;
import com.vedantu.subscription.viewobject.response.BundleVideoFilterRes;
import com.vedantu.subscription.viewobject.response.BundleVideoRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VideoEngagementManager {


    @Autowired
    private VideoEngagementDAO videoEngagementDAO;

    @Autowired
    private DozerBeanMapper mapper;

    public void setVideoEngagement(VideoEngagementReq req){
       List<VideoEngagement> videoEngagementList =   videoEngagementDAO.getEngagementByReq(req);
        VideoEngagement videoEngagement;
       if(ArrayUtils.isEmpty(videoEngagementList)){
           videoEngagement=  mapper.map(req, VideoEngagement.class);
       }else {
           videoEngagement=videoEngagementList.get(0);
           videoEngagement.addDuration(req.getDuration());
       }

        videoEngagementDAO.save(videoEngagement , req.getCallingUserId());
    }

}
