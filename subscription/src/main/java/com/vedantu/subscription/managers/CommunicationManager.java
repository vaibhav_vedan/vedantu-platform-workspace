/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.mchange.v2.collection.MapEntry;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.enums.PaymentStatus;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.leadsquared.pojo.ExternalLeadSquaredRequest;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.EmailAttachment;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.pojo.OTFSlot;
import com.vedantu.onetofew.pojo.StudentSlotPreferencePojo;
import com.vedantu.subscription.entities.mongo.*;
import com.vedantu.subscription.enums.TeacherType;
import com.vedantu.subscription.request.BatchDetailInfoReq;
import com.vedantu.subscription.request.section.EnrollmentFailureReq;
import com.vedantu.subscription.response.section.SectionInfo;
import com.vedantu.subscription.response.section.SectionInfoList;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import static com.vedantu.util.DateTimeUtils.TIME_ZONE_IN;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.LeadSquaredAction;
import com.vedantu.util.enums.LeadSquaredDataType;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.*;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import com.vedantu.util.pojo.Pair;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class CommunicationManager {

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private AwsSQSManager awsSQSManager;

    @Autowired
    private OTFBundleManager otfBundleManager;
    
    @Autowired
    private BundleManager bundleManager;    

    @Autowired
    private CourseManager courseManager;
    
    @Autowired
    private BatchManager batchManager;

    @Autowired
    private PaymentManager paymentManager;

    private Logger logger = logFactory.getLogger(CommunicationManager.class);
    final Gson gson = new Gson();

    private final String platformEndpoint = ConfigUtils.INSTANCE.getStringValue("PLATFORM_ENDPOINT");
    String NOTIFICATION_ENDPOINT=ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
    private final String EMAILIDS_FOR_SMI_ALERTS = ConfigUtils.INSTANCE.getStringValue("EMAILIDS_FOR_SMI_ALERTS");

    public String sendEmail(EmailRequest request) throws VException {
        logger.info("Inside sendEmail" + gson.toJson(request));
        awsSQSManager.sendToSQS(SQSQueue.NOTIFICATION_QUEUE, SQSMessageType.SEND_EMAIL, gson.toJson(request));
        return null;
    }
    
    public void sendMailForARMComment(HashMap<String, Object> bodyScope) throws UnsupportedEncodingException, VException{
        InternetAddress arm_comment_added_email1 = new InternetAddress("pradip.mahendra@vedantu.com","Pradip");
        InternetAddress arm_comment_added_email2 = new InternetAddress("pushplata.verma@vedantu.com","Pushplata");
        //InternetAddress arm_comment_added_email = new InternetAddress("ankit.parashar+007@vedantu.com", "Ankit Parashar");
        CommunicationType communicationType = CommunicationType.ARM_COMMENT_ADDED;
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(arm_comment_added_email2);
        toList.add(arm_comment_added_email1);
        EmailRequest emailRequest = new EmailRequest(toList, bodyScope, bodyScope, communicationType, Role.ADMIN);
        sendEmail(emailRequest);
        
    }
    
    public void sendMailToOTMBundleEnrolledStudents(HashMap<String, Object> bodyScope, String email, String fullName) throws VException, UnsupportedEncodingException {
        CommunicationType emailType = CommunicationType.OTM_BUNDLE_TRIAL_ENROLLMENT;
        ArrayList<InternetAddress> toList = new ArrayList<>();
        HashMap<String, Object> subjectScopes = new HashMap<>();
        subjectScopes.put("bundleName", bodyScope.get("bundleName"));
        toList.add(new InternetAddress(email, fullName));
        EmailRequest emailRequest = new EmailRequest(toList, subjectScopes, bodyScope, emailType, Role.STUDENT);
        sendEmail(emailRequest);
    }

    public void sendMailForOTMBundleEnrollmentAfterBulkOrInstalmentPayment(Long userId,
            OTFBundleInfo oTFBundleInfo, Integer amountPaid, boolean newEnrollmentsCreated,
            PaymentType paymentType, String orderId) throws UnsupportedEncodingException, VException {
        CommunicationType communicationType;

        final String p0 = newEnrollmentsCreated ? "new" : "old";
        logger.info("sending email for post bundle {} enrolment for {} payment type {}", p0, userId, paymentType );
        if (newEnrollmentsCreated) {//usually no registration is there and direct regular enrollment is created
            if (PaymentType.INSTALMENT.equals(paymentType)) {
                communicationType = CommunicationType.OTM_BUNDLE_NEW_ENROLLMENT_FIRST_INSTALMENT_PAYMENT;
            } else {
                communicationType = CommunicationType.OTM_BUNDLE_NEW_ENROLLMENT_BULK_PAYMENT;//fallback
            }
        } else {
            if (PaymentType.INSTALMENT.equals(paymentType)) {
                communicationType = CommunicationType.OTM_BUNDLE_OLD_ENROLLMENT_FIRST_INSTALMENT_PAYMENT;
            } else {
                communicationType = CommunicationType.OTM_BUNDLE_OLD_ENROLLMENT_BULK_PAYMENT;//fallback
            }
        }

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(userId, true);
        if (userBasicInfo == null || oTFBundleInfo == null) {
            logger.error("sendOTFEnrollmentEmail for otm bundle - userBasicInfo or oTFBundleInfo  info not found, userId " + userId);
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        bodyScopes.put("otmbundleName", oTFBundleInfo.getTitle());

        Float _amountPaid = 0f;
        if (amountPaid != null) {
            _amountPaid = (float) amountPaid / 100;
        }
        bodyScopes.put("amountPaid", _amountPaid);

        SimpleDateFormat datefb = new SimpleDateFormat("EEE, dd MMM yyyy");
        datefb.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String paymentDate = datefb.format(new Date());
        bodyScopes.put("paymentDate", paymentDate);
        if (PaymentType.INSTALMENT.equals(paymentType)) {
            Long dueTime = null;
            Long paidTime = null;
            Float paidAmount = null;
            Long nextDueTime = null;
            List<InstalmentInfo> instalments = paymentManager.getInstalmentsForOrderIds(Arrays.asList(orderId));
            Collections.sort(instalments, (InstalmentInfo o1, InstalmentInfo o2) -> o1.getDueTime().compareTo(o2.getDueTime()));
            for (InstalmentInfo instalmentInfo : instalments) {
                if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
                    dueTime = instalmentInfo.getDueTime();
                    paidTime = instalmentInfo.getPaidTime();
                    paidAmount = instalmentInfo.getTotalAmount() * 1.0f / 100;

                }
                if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())
                        || PaymentStatus.PAYMENT_SUSPENDED.equals(instalmentInfo.getPaymentStatus())) {
                    nextDueTime = instalmentInfo.getDueTime();
                    break;
                }

            }

            if (paidTime != null) {
                bodyScopes.put("paymentDate", datefb.format(new Date(paidTime)));
            }
            if (dueTime != null) {
                bodyScopes.put("dueDate", datefb.format(new Date(dueTime)));
            }
            if (paidAmount != null) {
                bodyScopes.put("amountPaid", paidAmount);
            }
            if (nextDueTime != null) {
                bodyScopes.put("nextDueDate", datefb.format(new Date(nextDueTime)));
            }
        }

//        bodyScopes.put("instalmentDueDate", null);//TODO
        String mySubscriptionsLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription";
        bodyScopes.put("mySubscriptionsLink", mySubscriptionsLink);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                userBasicInfo.getRole());
        if (PaymentType.INSTALMENT.equals(paymentType)) {
            request.setIncludeHeaderFooter(Boolean.FALSE);
        }
        sendEmail(request);
    }

    public void sendInstalmentPaidEmailFron2ndInstalment(Long userId,
            String instBundleId, List<InstalmentInfo> instalments)
            throws VException, UnsupportedEncodingException {

        CommunicationType communicationType = CommunicationType.INSTALMENT_PAID_OTF;
        UserBasicInfo student = fosUtils.getUserBasicInfo(userId, true);
        if (student == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found for sendInstalmentPaidEmailFron2ndInstalment " + userId);
        }

        OTFBundleInfo oTFBundleInfo = otfBundleManager.getOTFBundle(instBundleId);
        if (oTFBundleInfo == null) {
            throw new NotFoundException(ErrorCode.OTM_BUNDLE_NOT_FOUND, "otmbundle not found for  sendInstalmentPaidEmailFron2ndInstalment " + instBundleId);
        }

        HashMap<String, Object> scopeParams = new HashMap<>();

        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription";
        scopeParams.put("subscriptionLink", subscriptionLink);
        scopeParams.put("subscriptionTitle", oTFBundleInfo.getTitle());
        scopeParams.put("studentName", student.getFullName());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        Long dueTime = null;
        Long paidTime = null;
        Float paidAmount = null;
        Long nextDueTime = null;
        Collections.sort(instalments, (InstalmentInfo o1, InstalmentInfo o2) -> o1.getDueTime().compareTo(o2.getDueTime()));
        for (InstalmentInfo instalmentInfo : instalments) {
            if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
                dueTime = instalmentInfo.getDueTime();
                paidTime = instalmentInfo.getPaidTime();
                paidAmount = instalmentInfo.getTotalAmount() * 1.0f / 100;

            }
            if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())
                    || PaymentStatus.PAYMENT_SUSPENDED.equals(instalmentInfo.getPaymentStatus())) {
                nextDueTime = instalmentInfo.getDueTime();
                break;
            }

        }

        if (paidTime != null) {
            scopeParams.put("paymentDate", sdf.format(new Date(paidTime)));
        }
        if (dueTime != null) {
            scopeParams.put("dueDate", sdf.format(new Date(dueTime)));
        }
        if (paidAmount != null) {
            scopeParams.put("amountPaid", paidAmount);
        }
        if (nextDueTime != null) {
            scopeParams.put("nextDueDate", sdf.format(new Date(nextDueTime)));
        }

//        scopeParams.put("instalments", instalments);
//
//        logger.info(instalments);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }
    
    
    public void sendInstalmentPaidEmailFron2ndInstalmentForBundle(Long userId,
            String bundleId, List<InstalmentInfo> instalments)
            throws VException, UnsupportedEncodingException {

        CommunicationType communicationType = CommunicationType.INSTALMENT_PAID_OTF;
        UserBasicInfo student = fosUtils.getUserBasicInfo(userId, true);
        if (student == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found for sendInstalmentPaidEmailFron2ndInstalment " + userId);
        }

        Bundle bundle = bundleManager.getBundle(bundleId);
        if (bundle == null) {
            throw new NotFoundException(ErrorCode.BUNDLE_NOT_FOUND, "bundle not found for  sendInstalmentPaidEmailFron2ndInstalment " + bundleId);
        }

        HashMap<String, Object> scopeParams = new HashMap<>();

        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription";
        scopeParams.put("subscriptionLink", subscriptionLink);
        scopeParams.put("subscriptionTitle", bundle.getTitle());
        scopeParams.put("studentName", student.getFullName());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));

        Long dueTime = null;
        Long paidTime = null;
        Float paidAmount = null;
        Long nextDueTime = null;
        Collections.sort(instalments, (InstalmentInfo o1, InstalmentInfo o2) -> o1.getDueTime().compareTo(o2.getDueTime()));
        for (InstalmentInfo instalmentInfo : instalments) {
            if (PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())) {
                dueTime = instalmentInfo.getDueTime();
                paidTime = instalmentInfo.getPaidTime();
                paidAmount = instalmentInfo.getTotalAmount() * 1.0f / 100;

            }
            if (PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus())
                    || PaymentStatus.PAYMENT_SUSPENDED.equals(instalmentInfo.getPaymentStatus())) {
                nextDueTime = instalmentInfo.getDueTime();
                break;
            }

        }

        if (paidTime != null) {
            scopeParams.put("paymentDate", sdf.format(new Date(paidTime)));
        }
        if (dueTime != null) {
            scopeParams.put("dueDate", sdf.format(new Date(dueTime)));
        }
        if (paidAmount != null) {
            scopeParams.put("amountPaid", paidAmount);
        }
        if (nextDueTime != null) {
            scopeParams.put("nextDueDate", sdf.format(new Date(nextDueTime)));
        }

//        scopeParams.put("instalments", instalments);
//
//        logger.info(instalments);
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }    

    public void sendOTFTrialPaymentBlocked(Enrollment enrollmentInfo)
            throws UnsupportedEncodingException, VException {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(enrollmentInfo.getUserId(), true);
        if (StringUtils.isEmpty(enrollmentInfo.getEntityTitle())) {
            Course course = courseManager.getCourse(enrollmentInfo.getCourseId());
            enrollmentInfo.setEntityTitle(course.getTitle());
        }

        if (userBasicInfo == null) {
            logger.error("sendOTFTrialPaymentReminder - userBasicInfo not found");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());

        bodyScopes.put("subscriptionTitle", enrollmentInfo.getEntityTitle());
        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription";
        bodyScopes.put("subscriptionLink", subscriptionLink);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));

        ArrayList<InternetAddress> bccList = new ArrayList<>();
        bccList.add(new InternetAddress("nisharg.g@vedantu.com", "Nisharg"));

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes,
                CommunicationType.OTF_TRIAL_PAYMENT_BLOCKED, userBasicInfo.getRole());
        request.setBcc(bccList);
        sendEmail(request);
    }

    public void sendOTFReactivatedMail(Enrollment enrollmentInfo)
            throws UnsupportedEncodingException, VException {
        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(enrollmentInfo.getUserId(), true);
        if (StringUtils.isEmpty(enrollmentInfo.getEntityTitle())) {
            Course course = courseManager.getCourse(enrollmentInfo.getCourseId());
            enrollmentInfo.setEntityTitle(course.getTitle());
        }

        if (userBasicInfo == null) {
            logger.error("sendOTFTrialPaymentReminder - userBasicInfo not found");
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());

        bodyScopes.put("subscriptionTitle", enrollmentInfo.getEntityTitle());
        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription";
        bodyScopes.put("subscriptionLink", subscriptionLink);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));

        ArrayList<InternetAddress> bccList = new ArrayList<>();

        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes,
                CommunicationType.POST_PAYMENT_OTF_RE_ACTIVE, userBasicInfo.getRole());
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendOTMBundlePurchaseEmail(Long userId, OTFBundleInfo otfBundle, float paidAmount,
            CommunicationType communicationType) throws AddressException, VException, IOException {
        UserBasicInfo user = fosUtils.getUserBasicInfo(userId, true);
        if (user == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + userId);
        }
        if (otfBundle == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "otfbundle is null.");
        }
        
        
        HashMap<String, Object> bodyScopes = new HashMap<>();
        List<StudentSlotPreferencePojo> pref = courseManager.fetchStudentSlotPreference(otfBundle.getId(),Arrays.asList(userId.toString()),null,null, false);
        if (ArrayUtils.isNotEmpty(pref)) {
            if (ArrayUtils.isNotEmpty(pref.get(0).getPreferredSlots())) {
                OTFSlot slot = pref.get(0).getPreferredSlots().get(0);
                bodyScopes.put("startDate", slot.getStartDay());
                bodyScopes.put("dayType", slot.getDayType());
                bodyScopes.put("timing", slot.getSlotTimes() == null ? "" : slot.getSlotTimes().get(0));
            }
        }

        bodyScopes.put("studentName", user.getFullName());
        bodyScopes.put("studentId", userId);
        bodyScopes.put("contactNumber", user.getContactNumber());
        bodyScopes.put("phoneCode", user.getPhoneCode());
        bodyScopes.put("email", user.getEmail());
        bodyScopes.put("courseTitle", otfBundle.getTitle());
        bodyScopes.put("courseId", otfBundle.getId());
        if (paidAmount > 0) {
            bodyScopes.put("paidAmount", paidAmount);
        }
        
        String updateSlotLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-many/update-slot/"+otfBundle.getId()+"?updateSlot=true";
        bodyScopes.put("updateSlotLink",updateSlotLink);
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(new Date());
        bodyScopes.put("dateTime", dateTime);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(user.getEmail(), user.getFullName()));
        EmailRequest emailRequest = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType, user.getRole());
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(emailRequest);

        try {
            HashMap<String, String> lsParams = new HashMap<>();
            lsParams.put("userEmail", user.getEmail());
            lsParams.put("userPhone", user.getContactNumber());
            lsParams.put("entityType", "OTM_BUNDLE");
            lsParams.put("entityId", otfBundle.getId());
            lsParams.put("entityTitle", otfBundle.getTitle());
            String timings = "startDate : " + (String) bodyScopes.get("startDate") + " timing : " + (String) bodyScopes.get("timing");
            lsParams.put("timing", timings);
            if (paidAmount > 0) {
                lsParams.put("paidAmount", String.valueOf(paidAmount));
            } else {
                lsParams.put("paidAmount", "");
            }

            ExternalLeadSquaredRequest lsReq = null;
            if (CommunicationType.POST_OTM_BUNDLE_REGISTRATION.equals(communicationType)) {
                lsReq = new ExternalLeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_REGISTER, lsParams);
            } else if (CommunicationType.POST_OTM_BUNDLE_PURCHASE.equals(communicationType)) {
                lsReq = new ExternalLeadSquaredRequest(LeadSquaredAction.POST_LEAD_DATA, LeadSquaredDataType.LEAD_ACTIVITY_ENROLL, lsParams);
            }

            ClientResponse postToLeadSquared = WebUtils.INSTANCE.doCall(platformEndpoint + "/leadsquared/pushLeadSquaredActivity",
                    HttpMethod.POST, gson.toJson(lsReq));
            VExceptionFactory.INSTANCE.parseAndThrowException(postToLeadSquared);

        } catch (Exception e) {
            logger.error("Error in sending lead activity to leadsquare" + e);
        }

    }

    public void sendUserDataEmail(String emailId, String name, List<String> ccList, String exportUserData, String fileName,
            String subject) throws IOException, AddressException, VException {

        EmailRequest email = new EmailRequest();
        email.setBody("Please find the attached doc");
        if (StringUtils.isEmpty(subject)) {
            subject = "Here is the system details in the attached document";
        }

        subject = ConfigUtils.INSTANCE.getStringValue("environment") + ": "
                + subject;

        email.setSubject(subject);

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(emailId, name));
        email.setTo(toList);

        email.setType(CommunicationType.SYSTEM_INFO);

        ArrayList<InternetAddress> cc = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(ccList)) {
            for (String ccName : ccList) {
                cc.add(new InternetAddress(ccName, ccName));
            }
            email.setCc(cc);
        }

        EmailAttachment attachment = new EmailAttachment();
        attachment.setApplication("application/csv");
        attachment.setAttachmentData(exportUserData);

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String dateTime = sdf.format(System.currentTimeMillis());

        attachment.setFileName(fileName + ":" + dateTime + ".csv");
        email.setAttachment(attachment);

        sendEmailViaRest(email);
    }

    public String sendEmailViaRest(EmailRequest request) throws VException {
        String notificationEndpoint = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");
        ClientResponse resp = WebUtils.INSTANCE.doCall(notificationEndpoint + "/email/sendEmail", HttpMethod.POST,
                new Gson().toJson(request));

        VExceptionFactory.INSTANCE.parseAndThrowException(resp);
        String jsonString = resp.getEntity(String.class);
        logger.info("Response from notification-centre " + jsonString);
        return jsonString;
    }
    
    public void sendOTFEnrollmentEmail(Enrollment enrollmentInfo, OrderInfo orderInfo, Boolean createdNew) throws UnsupportedEncodingException, VException {
        CommunicationType communicationType;

        if (createdNew) {//usually no registration is there and direct regular enrollment is created
            if (orderInfo != null && PaymentType.INSTALMENT.equals(orderInfo.getPaymentType())) {
                communicationType = CommunicationType.OTF_BATCH_NEW_ENROLLMENT_FIRST_INSTALMENT_PAYMENT;
            } else {
                communicationType = CommunicationType.OTF_BATCH_NEW_ENROLLMENT_BULK_PAYMENT;//fallback
            }
        } else {
            if (orderInfo != null && PaymentType.INSTALMENT.equals(orderInfo.getPaymentType())) {
                communicationType = CommunicationType.OTF_BATCH_OLD_ENROLLMENT_FIRST_INSTALMENT_PAYMENT;
            } else {
                communicationType = CommunicationType.OTF_BATCH_OLD_ENROLLMENT_BULK_PAYMENT;//fallback
            }
        }

        UserBasicInfo userBasicInfo = fosUtils.getUserBasicInfo(enrollmentInfo.getUserId(), true);
        Course courseInfo = courseManager.getCourse(enrollmentInfo.getCourseId());
        if (userBasicInfo == null || courseInfo == null) {
            logger.error("sendOTFEnrollmentEmail - userBasicInfo pr course basic info not found, enrollmentId " + enrollmentInfo.getId());
            return;
        }

        HashMap<String, Object> bodyScopes = new HashMap<>();
        bodyScopes.put("studentName", userBasicInfo.getFirstName());
        bodyScopes.put("courseName", courseInfo.getTitle());

        if (orderInfo != null) {
            Integer amountPaid = orderInfo.getAmount();
            if (PaymentType.INSTALMENT.equals(orderInfo.getPaymentType())) {
                amountPaid = orderInfo.getAmountPaid();
            }
            bodyScopes.put("amountPaid", (amountPaid / 100));
        }

        SimpleDateFormat datefb = new SimpleDateFormat("EEE, dd MMM yyyy");
        datefb.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        String paymentDate = datefb.format(new Date());
        bodyScopes.put("paymentDate", paymentDate);

//        bodyScopes.put("instalmentDueDate", null);//TODO
        if (ArrayUtils.isNotEmpty(courseInfo.getTargets())) {
            List<String> targets = new ArrayList<>(courseInfo.getTargets());
            String batchLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/1-to-few/" + targets.get(0)
                    + "/course/" + courseInfo.getId() + "/batch-details/list?enrolled=true&batchId="
                    + enrollmentInfo.getBatchId();
            bodyScopes.put("batchLink", batchLink);
        }

        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(userBasicInfo.getEmail(), userBasicInfo.getFullName()));
        EmailRequest request = new EmailRequest(toList, bodyScopes, bodyScopes, communicationType,
                userBasicInfo.getRole());
        sendEmail(request);
    }
    
    
        public void sendInstalmentPaidEmailOTF(Long userId,String batchId,List<InstalmentInfo> infos)
            throws VException, UnsupportedEncodingException {

        CommunicationType communicationType = CommunicationType.INSTALMENT_PAID_OTF;
        UserBasicInfo student = fosUtils.getUserBasicInfo(userId, true);
        if (student == null) {
            throw new NotFoundException(ErrorCode.USER_NOT_FOUND, "user not found " + userId);
        }

        HashMap<String, Object> scopeParams = new HashMap<>();
        Batch batch = batchManager.getBatch(batchId);
        Course courseInfo = courseManager.getCourse(batch.getCourseId());
        String subscriptionLink = ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription";
        scopeParams.put("subscriptionLink", subscriptionLink);
        scopeParams.put("subscriptionTitle", courseInfo.getTitle());
        scopeParams.put("studentName", student.getFullName());

        SimpleDateFormat sdf = new SimpleDateFormat("EEE, dd MMM, yyyy z");
        sdf.setTimeZone(TimeZone.getTimeZone(TIME_ZONE_IN));
        Long dueTime = null;
        Long paidTime = null;
        Float paidAmount = null;
        Long nextDueTime = null;
        Collections.sort(infos, (InstalmentInfo o1, InstalmentInfo o2) -> o1.getDueTime().compareTo(o2.getDueTime()));
        for (InstalmentInfo instalmentInfo : infos) {
            if(PaymentStatus.PAID.equals(instalmentInfo.getPaymentStatus())){
                dueTime = instalmentInfo.getDueTime();
                paidTime = instalmentInfo.getPaidTime();
                paidAmount = instalmentInfo.getTotalAmount()*1.0f / 100;
            
            }
            if(PaymentStatus.NOT_PAID.equals(instalmentInfo.getPaymentStatus()) || 
                    PaymentStatus.PAYMENT_SUSPENDED.equals(instalmentInfo.getPaymentStatus())){
                nextDueTime = instalmentInfo.getDueTime();
                break;
            }
            
        }

        if(paidTime != null){
            scopeParams.put("paymentDate", sdf.format(new Date(paidTime)));
        }
        if(dueTime != null){
            scopeParams.put("dueDate", sdf.format(new Date(dueTime)));
        }
        if(paidAmount != null){
            scopeParams.put("amountPaid", paidAmount);
        }
        if(nextDueTime != null){
            scopeParams.put("nextDueDate", sdf.format(new Date(nextDueTime)));
        }

        logger.info(infos.toString());
        ArrayList<InternetAddress> toList = new ArrayList<>();
        toList.add(new InternetAddress(student.getEmail(), student.getFullName()));
        EmailRequest request = new EmailRequest(toList, scopeParams, scopeParams, communicationType, Role.STUDENT);
        request.setIncludeHeaderFooter(Boolean.FALSE);
        sendEmail(request);
    }

    public void sendBulkEmail(EmailRequest emailRequest) {
        ClientResponse emailResponse = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT + "/email/sendEmail", HttpMethod.POST,
                new Gson().toJson(emailRequest));
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(emailResponse);
        } catch (VException e) {
            e.printStackTrace();
        }
        String jsonStringFromBulkEmail = emailResponse.getEntity(String.class);
        logger.info("\nResponse from notification-centre " + jsonStringFromBulkEmail);
    }

    public void sendBulkSMS(BulkTextSMSRequest bulkTextSMSRequest) {
        ClientResponse SMSResponse = WebUtils.INSTANCE.doCall(NOTIFICATION_ENDPOINT + "/SMS/sendBulkSMS", HttpMethod.POST,
                new Gson().toJson(bulkTextSMSRequest));
        try {
            VExceptionFactory.INSTANCE.parseAndThrowException(SMSResponse);
        } catch (VException e) {
            e.printStackTrace();
        }
        String jsonStringFromBulkSMS = SMSResponse.getEntity(String.class);
        logger.info("\nResponse from notification-centre " + jsonStringFromBulkSMS);
    }

    public void sendEmailToTeacherForSectionAllotment(SectionInfoList sectionInfoList) throws VException {
        if(sectionInfoList.getTeacherIdSectionNameMap() == null || sectionInfoList.getTeacherIdSectionNameMap().isEmpty())
            return;

        BatchDetailInfoReq batchDetailInfoReq = new BatchDetailInfoReq();
        batchDetailInfoReq.setBatchIds(Arrays.asList(sectionInfoList.getBatchId()));
        batchDetailInfoReq.setBatchDataIncludeFields(Arrays.asList(Batch.Constants._ID,Batch.Constants.COURSE_ID));
        batchDetailInfoReq.setCourseDataIncludeFields(Arrays.asList(BatchBasicInfo.Constants.GRADES));
        batchDetailInfoReq.setCourseInfoRequired(true);
        List<BatchBasicInfo> batch = batchManager.getDetailedBatchInfo(batchDetailInfoReq);
        logger.info("batch {}",batch);

        if(batch == null)
            return;

        String grades = String.join(",",batch.get(0).getCourseInfo().getGrades());

        for(Map.Entry mapEle : sectionInfoList.getTeacherIdSectionNameMap().entrySet()){

            String sectionName = mapEle.getValue().toString();
            String teacherId = mapEle.getKey().toString();

            UserBasicInfo teacher = fosUtils.getUserBasicInfo(mapEle.getKey().toString(),true);
            String teacherName = teacher.getFirstName();
            String emailId = teacher.getEmail();
            TeacherType teacherType = sectionInfoList.getTeacherIdTypeMap().get(teacherId);
            logger.info("teacherType {}",teacherType);
            prepareEmail(emailId,teacherName,sectionName,grades,teacherType,batch.get(0).getBatchId());
        }
    }

    public void prepareEmail(String emailId, String teacherName, String sectionName, String grades, TeacherType teacherType, String batchId) throws VException {
        // preparing the email template and send it
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        HashMap<String,Object> bodyScopes = new HashMap<>();
        bodyScopes.put("name",teacherName);
        bodyScopes.put("teacherType",teacherType);
        bodyScopes.put("grade",grades);
        bodyScopes.put("sectionName",sectionName);
        bodyScopes.put("batchId",batchId);

        emailRequest.setBodyScopes(bodyScopes);
        emailRequest.setRole(Role.TEACHER);
        emailRequest.setType(CommunicationType.NOTIFY_TEACHER_FOR_SECTION_ALLOTMENT);

        try {
            if(emailId != null && !emailId.isEmpty()) {
                emailRequest.setTo(Arrays.asList(new InternetAddress(emailId)));
                sendEmail(emailRequest);
            }
        }
        catch(VException | AddressException exc){
            logger.error("Exception " + exc);
        }
    }

    public void sendEmailAlertsToOps(String batchId, List<SectionInfo> sectionFillingInfos){

        if(batchId == null || sectionFillingInfos.isEmpty())
            return;

        // preparing the email template and send it
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        HashMap<String,Object> bodyScopes = new HashMap<>();
        bodyScopes.put("batchId",batchId);
        bodyScopes.put("sectionFillingInfos",sectionFillingInfos);

        emailRequest.setBodyScopes(bodyScopes);
        emailRequest.setType(CommunicationType.ALERT_ACADOPS_FOR_SECTION_FILLING);
        emailRequest.setRole(Role.ADMIN);

        String emailIds = EMAILIDS_FOR_SMI_ALERTS;
        List<InternetAddress> emailAddresses = new ArrayList<>();
        logger.info("emailIds-> {}",emailIds);
        
        String[] temp = emailIds.split(",");
        for(String id : temp) {
            try {
                emailAddresses.add(new InternetAddress(id));
            } catch (AddressException e) {
                logger.info("Error while adding emailIds = " + e.getMessage());
            }
        } 
        try {
            if(emailAddresses != null && !emailAddresses.isEmpty()) {
                emailRequest.setTo(emailAddresses);
                sendEmail(emailRequest);
            }
        }
        catch(Exception exc){
            logger.error("Exception " + exc);
        }
    }

    public void sendAlertForBulkUpdateCompletion(String batchId) {
        if(batchId == null || batchId.isEmpty())
            return;

        // preparing the email template and send it
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        HashMap<String,Object> bodyScopes = new HashMap<>();
        bodyScopes.put("batchId",batchId);

        emailRequest.setRole(Role.ADMIN);
        emailRequest.setBodyScopes(bodyScopes);
        emailRequest.setType(CommunicationType.ALERT_ACADOPS_FOR_SECTION_BULK_UPDATE_COMPLETION);
        
        String emailIds = EMAILIDS_FOR_SMI_ALERTS;
        List<InternetAddress> emailAddresses = new ArrayList<>();
        logger.info("emailIds-> {}",emailIds);

        String[] ids = emailIds.split(",");
        for(String id : ids) {
            try {
                emailAddresses.add(new InternetAddress(id));
            } catch (AddressException e) {
                logger.info("Error while adding emailIds = " + e.getMessage());
            }
        }

        try {
            if(emailAddresses != null && !emailAddresses.isEmpty()) {
                emailRequest.setTo(emailAddresses);
                sendEmail(emailRequest);
            }
        }
        catch(Exception exc){
            logger.error("Exception " + exc);
        }

    }

    public void sendEmailAlertsForEnrollmentFailure(EnrollmentFailureReq req) {
        EmailRequest emailRequest = new EmailRequest();
        emailRequest.setIncludeHeaderFooter(Boolean.FALSE);

        HashMap<String,Object> bodyScopes = new HashMap<>();
        List<EnrollmentFailureReq> emailInfos = new ArrayList<>();

        if(req.getBatchId() == null){
            if(req.getBatchIds().isEmpty()) return;
            for(String id: req.getBatchIds()){
                EnrollmentFailureReq _req = new EnrollmentFailureReq();
                _req.setBatchId(id);
                _req.setUserId(req.getUserId());
                _req.setMessage_1(req.getMessage_1());
                _req.setMessage_2(req.getMessage_2());
                emailInfos.add(_req);
            }
        }
        else{
            EnrollmentFailureReq _req = new EnrollmentFailureReq();
            _req.setUserId(req.getUserId());
            _req.setMessage_1(req.getMessage_1());
            _req.setMessage_2(req.getMessage_2());
            _req.setBatchId(req.getBatchId());
            emailInfos.add(_req);
        }
        bodyScopes.put("emailInfos",emailInfos);
        emailRequest.setRole(Role.ADMIN);
        emailRequest.setBodyScopes(bodyScopes);
        emailRequest.setType(CommunicationType.ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE);
        
        String emailIds = EMAILIDS_FOR_SMI_ALERTS;
        List<InternetAddress> emailAddresses = new ArrayList<>();
        logger.info("emailIds-> {}",emailIds);

        String[] ids = emailIds.split(",");
        for(String id : ids) {
            try {
                emailAddresses.add(new InternetAddress(id));
            } catch (AddressException e) {
                logger.info("Error while adding emailIds = " + e.getMessage());
            }
        }

        try {
            if(emailAddresses != null && !emailAddresses.isEmpty()) {
                emailRequest.setTo(emailAddresses);
                sendEmail(emailRequest);
            }
        }
        catch(Exception exc){
            logger.error("Exception " + exc);
        }
    }
}
