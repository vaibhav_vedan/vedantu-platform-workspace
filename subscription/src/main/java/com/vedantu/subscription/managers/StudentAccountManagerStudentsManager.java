package com.vedantu.subscription.managers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.onetofew.pojo.InsertStudentAccountManagerStudentsReq;
import com.vedantu.onetofew.pojo.ModifyStudentAccountManagerOfStudentsReq;
import com.vedantu.subscription.dao.StudentAccountManagerStudentsDAO;
import com.vedantu.subscription.entities.mongo.StudentAccountManagerStudents;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Service
public class StudentAccountManagerStudentsManager {

    private static final String USER_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("USER_ENDPOINT");

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(StudentAccountManagerStudentsManager.class);

    @Autowired
    private StudentAccountManagerStudentsDAO studentAccountManagerStudentsDAO;

    @Autowired
    private AcadMentorStudentsManager acadMentorStudentsManager;

    public StudentAccountManagerStudentsManager() {
        super();
    }

    public long assignStudentAccountManagerStudents(InsertStudentAccountManagerStudentsReq insertStudentAccountManagerStudentsReq) throws VException, UnsupportedEncodingException {
        User studentAccountManager = acadMentorStudentsManager.getUserFromEmail(insertStudentAccountManagerStudentsReq.getStudentAccountManagerEmail());
        logger.info("Logger: studentAccountManager - "+ studentAccountManager);
        if (studentAccountManager != null && studentAccountManager.getRole() != null && studentAccountManager.getRole().equals(Role.ADMIN)){
            Set<String> studentEmails = insertStudentAccountManagerStudentsReq.getStudentEmails();
            List<StudentAccountManagerStudents> studentAccountManagerStudentsList = new ArrayList<>();
            for (String studentEmail : studentEmails) {
                UserBasicInfo studentBasicInfo = acadMentorStudentsManager.getUserBasicInfoFromEmail(studentEmail);
                if (studentBasicInfo != null && studentBasicInfo.getUserId() != null && Role.STUDENT.equals(studentBasicInfo.getRole())) {
                    StudentAccountManagerStudents studentAccountManagerStudents = new StudentAccountManagerStudents(studentAccountManager.getId(),studentBasicInfo.getUserId());
                    studentAccountManagerStudentsList.add(studentAccountManagerStudents);
                }else {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student does not exist.");
                }
            }
            studentAccountManagerStudentsDAO.insertStudentAccountManagerStudentsList(studentAccountManagerStudentsList);
            return studentEmails.size();
        }else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student Account Manager does not exist.");
        }
    }

    public long modifyStudentAccountManagerStudents(ModifyStudentAccountManagerOfStudentsReq modifyStudentAccountManagerOfStudentsReq )throws VException, UnsupportedEncodingException {
        User studentAccountManager = acadMentorStudentsManager.getUserFromEmail(modifyStudentAccountManagerOfStudentsReq.getStudentAccountManagerEmail());
        logger.info("Logger: studentAccountManager - "+ studentAccountManager);
        if (studentAccountManager != null && studentAccountManager.getRole() != null && studentAccountManager.getRole().equals(Role.ADMIN)){
            List<User> students = acadMentorStudentsManager.getUsersFromUserIds(modifyStudentAccountManagerOfStudentsReq.getStudentIds());
            boolean allValidStudents = true;
            if (students.size() == modifyStudentAccountManagerOfStudentsReq.getStudentIds().size()) {
                for (User student : students) {
                    if (student == null || student.getRole() == null || !(student.getRole().equals(Role.STUDENT))) {
                        allValidStudents = false;
                        break;
                    }
                }
            } else {
                allValidStudents = false;
            }
            if (allValidStudents) {
                for (Long studentId : modifyStudentAccountManagerOfStudentsReq.getStudentIds()) {
                    StudentAccountManagerStudents studentAccountManagerStudents = new StudentAccountManagerStudents(studentAccountManager.getId(),studentId);
                    studentAccountManagerStudentsDAO.upsertStudentAccountManagerStudent(studentAccountManagerStudents, modifyStudentAccountManagerOfStudentsReq.getCallingUserId());
                }
                return students.size();
            } else {
                throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "One or more of the student Ids invalid");
            }
        }else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student Account Manager does not exist.");
        }

    }

    public long removeStudents(InsertStudentAccountManagerStudentsReq studentAccountManagerStudentsReq) throws VException, UnsupportedEncodingException {
        User studentAccountManager = acadMentorStudentsManager.getUserFromEmail(studentAccountManagerStudentsReq.getStudentAccountManagerEmail());
        logger.info("Logger: studentAccountManager - "+ studentAccountManager);
        if (studentAccountManager != null && studentAccountManager.getRole() != null && studentAccountManager.getRole().equals(Role.ADMIN)){
            Set<String> studentEmails = studentAccountManagerStudentsReq.getStudentEmails();
            List<StudentAccountManagerStudents> studentAccountManagerStudentsList = new ArrayList<>();
            for (String studentEmail : studentEmails) {
                UserBasicInfo studentBasicInfo = acadMentorStudentsManager.getUserBasicInfoFromEmail(studentEmail);
                if (studentBasicInfo != null && Role.STUDENT.equals(studentBasicInfo.getRole())) {
                    StudentAccountManagerStudents studentAccountManagerStudents = new StudentAccountManagerStudents(studentAccountManager.getId(), studentBasicInfo.getUserId());
                    studentAccountManagerStudentsList.add(studentAccountManagerStudents);
                } else {
                    throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Student does not exist");
                }
            }

            for (StudentAccountManagerStudents studentAccountManagerStudents : studentAccountManagerStudentsList) {
                studentAccountManagerStudentsDAO.removeStudents(studentAccountManagerStudents);
            }

            return studentEmails.size();

        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User not an Student Account Manager");
        }

    }

    public List<User> getStudentsByStudentAccountManager(String studentAccountManagerEmail) throws VException, UnsupportedEncodingException {
        if(StringUtils.isEmpty(studentAccountManagerEmail)){
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Please Enter the Student Account Manager Email");
        }
        User studentAccountManager = acadMentorStudentsManager.getUserFromEmail(studentAccountManagerEmail);
        logger.info("Logger: studentAccountManager - "+ studentAccountManager);
        if (studentAccountManager != null && studentAccountManager.getRole() != null && studentAccountManager.getRole().equals(Role.ADMIN)){
            List<Long> studentIds = studentAccountManagerStudentsDAO.getStudentsByStudentAccountManager(studentAccountManager.getId());
            return acadMentorStudentsManager.getUsersFromUserIds(studentIds);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "User not Student Account Manager");
        }

    }

    public List<StudentAccountManagerStudents> getSAMByStudentsId(List<String> studentIdList) {

        return studentAccountManagerStudentsDAO.getSAMByStudentsId(studentIdList);
    }
}
