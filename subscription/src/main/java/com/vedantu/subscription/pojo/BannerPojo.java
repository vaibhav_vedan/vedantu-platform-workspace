package com.vedantu.subscription.pojo;

import com.vedantu.subscription.enums.MicroCourseType;
import lombok.Data;

@Data
public class BannerPojo {
    private String imageUrl;
    private MicroCourseType courseType;
    private String collectionName;
}
