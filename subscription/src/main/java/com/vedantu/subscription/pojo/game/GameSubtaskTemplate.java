package com.vedantu.subscription.pojo.game;

import com.vedantu.subscription.enums.game.GameSubtaskName;
import lombok.Data;

/**
 * @author mano
 */
@Data
public class GameSubtaskTemplate {
    private GameSubtaskName name;
    private GameTaskCriteria criteria;
}
