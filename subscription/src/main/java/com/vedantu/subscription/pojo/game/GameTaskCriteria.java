package com.vedantu.subscription.pojo.game;

import lombok.Data;

/**
 * @author mano
 */
@Data
public class GameTaskCriteria {
    private double completionPercent;
}
