package com.vedantu.subscription.pojo.game;

import com.vedantu.subscription.enums.game.GameSubtaskName;
import lombok.Data;

import java.util.List;
import java.util.Optional;

/**
 * @author mano
 */
@Data
public class GameTaskTemplate {
    private String name;
    private List<GameSubtaskTemplate> subtaskTemplates;

    public Optional<GameSubtaskTemplate> gameSubtaskTemplateForName(GameSubtaskName subtaskName) {
        return subtaskTemplates.stream().filter(e -> e.getName() == subtaskName).findAny();
    }
}
