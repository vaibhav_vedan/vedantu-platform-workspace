package com.vedantu.subscription.pojo;


import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.SessionPlanPojo;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.enums.bundle.DurationTime;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import lombok.Data;

import java.util.List;

@Data
public class MicroCourseBasicInfo {

	private String batchId;
	private Long startTime;
	private Long duration;
	private Long endTime;
	private List<SessionPlanPojo> sessionPlan;
	private BundleEntity aioPackage;
	private String description;
	private int price;
	private int cutPrice;
	private String title;
	private Boolean isRecordedVideo;
	private List<String> target;
	private DurationTime durationtime;
	private Long validTill;
	private List<CourseTerm> searchTerms;

}
