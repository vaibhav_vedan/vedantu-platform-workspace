package com.vedantu.subscription.pojo;

import com.vedantu.subscription.entities.mongo.Bundle;
import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
public class BundleAggregation {
    @Id
    private String id;
    private Integer rank;
    private List<BannerPojo> banner;
    private List<Bundle> data;
}
