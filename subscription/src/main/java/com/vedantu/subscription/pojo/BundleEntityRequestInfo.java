package com.vedantu.subscription.pojo;

import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.util.StringUtils;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Objects;

@Data
@NoArgsConstructor
public class BundleEntityRequestInfo {
    private String id;
    private Integer grade;
    private PackageType packageType;
    private String entityId;
    private EnrollmentType enrollmentType;

    public boolean validData(){
        return Objects.nonNull(grade) && Objects.nonNull(packageType) && Objects.nonNull(enrollmentType) && StringUtils.isNotEmpty(entityId);
    }

    @Override
    public String toString() {
        return "BundleEntityRequestInfo{" +
                "\n    id='" + id + '\'' +
                ",\n    grade=" + grade +
                ",\n    packageType=" + packageType +
                ",\n    entityId='" + entityId + '\'' +
                ",\n    enrollmentType=" + enrollmentType +
                '}';
    }
}
