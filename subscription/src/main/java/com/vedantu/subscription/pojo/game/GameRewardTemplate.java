package com.vedantu.subscription.pojo.game;

import lombok.Data;

import java.util.List;

/**
 * @author mano
 */
@Data
public class GameRewardTemplate {
    private String name;
    private List<String> tasksToComplete;
    private List<GameRewardPojo> availableRewards;
}
