package com.vedantu.subscription.pojo.section;

import com.vedantu.subscription.entities.mongo.Enrollment;
import lombok.Data;

import java.util.List;

@Data
public class EnrollmentListPojo {
    String batchId;
    List<Enrollment> enrollments;
}
