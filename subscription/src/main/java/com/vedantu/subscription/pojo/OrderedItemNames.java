package com.vedantu.subscription.pojo;

import com.vedantu.session.pojo.EntityType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderedItemNames {
    private String orderId;
    private String entityId;
    private EntityType entityType;
    private String entityTitle;
}
