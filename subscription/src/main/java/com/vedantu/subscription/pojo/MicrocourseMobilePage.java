package com.vedantu.subscription.pojo;

import lombok.Data;

@Data
public class MicrocourseMobilePage {
    private String url;
    private String id;
}
