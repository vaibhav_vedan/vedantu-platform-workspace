package com.vedantu.subscription.pojo;

import lombok.Data;

import java.util.List;

@Data
public class MicrocourseMobileResp {
    private List<BundleAggregation> list;
    private List<BannerPojo> banner;
    private String messageStrip;
    private String messageStripUrl;
}
