package com.vedantu.subscription.pojo.game;

import com.vedantu.subscription.enums.game.GameSubtaskName;
import lombok.Data;

import java.util.List;
import java.util.Map;

/**
 * @author mano
 */
@Data
public class GameJourneyRedis {

    private String name;
    private List<Task> incompleteTasks;
    // key -> session id, only stores session meta data : val -> task name
    private Map<String, String> sessionMeta;


    @Data
    public static final class Task {
        private List<SubTask> subTasks;
        private String name;
    }

    @Data
    public static final class SubTask {
        private GameSubtaskName name;
    }

}
