package com.vedantu.subscription.pojo;

import lombok.Data;
import java.util.Set;


@Data
public class MicroCourseSessionData {
    private Long startTime;
    private Long endTime;
    private String title;
    private Set<String> batchIds;
}
