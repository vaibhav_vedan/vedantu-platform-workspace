package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Objects;

@Data
@NoArgsConstructor
public class GetBundlePackageReferenceRequest extends AbstractFrontEndListReq {
    private String bundleId;
    private Integer grade;
    private PackageType courseType;
    private String entityId;
    private String courseId;
    private EnrollmentType enrollmentType;

    private List<String> bundleIds;
    @Override
    protected List<String> collectVerificationErrors() {
        final List<String> errors = super.collectVerificationErrors();
        if(ArrayUtils.isEmpty(bundleIds) && StringUtils.isEmpty(bundleId)){
            errors.add("[bundleId] or [bundleIds] cann't be null");
        }
        if(Objects.isNull(getStart()) && Objects.isNull(getSize())){
            errors.add("[start] or [size] cann't be null");
        }
        return errors;
    }
}
