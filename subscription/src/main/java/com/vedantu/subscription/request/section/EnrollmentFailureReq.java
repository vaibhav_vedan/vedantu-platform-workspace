package com.vedantu.subscription.request.section;

import lombok.Data;

import java.util.List;

@Data
public class EnrollmentFailureReq {
    String batchId;
    String userId;
    String message_1;
    String message_2;
    List<String> batchIds;
}
