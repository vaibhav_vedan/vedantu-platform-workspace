package com.vedantu.subscription.request;

import com.vedantu.subscription.pojo.MicroCourseSessionData;
import lombok.Data;

import java.util.List;

@Data
public class MicroCourseSMSReq {

    private List<MicroCourseSessionData> sessionDataList;
}
