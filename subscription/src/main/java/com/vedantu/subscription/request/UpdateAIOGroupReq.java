package com.vedantu.subscription.request;


import com.vedantu.subscription.entities.mongo.AIOGroup;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import java.util.List;

/**
 * Created by Dpadhya
 */
public class UpdateAIOGroupReq extends AbstractFrontEndListReq {

	List<AIOGroup> groups ;

	public List<AIOGroup> getGroups() {
		return groups;
	}

	public void setGroups(List<AIOGroup> groups) {
		this.groups = groups;
	}

	@Override
	protected List<String> collectVerificationErrors() {

		List<String> errors = super.collectVerificationErrors();
//		if (null == bundleIds) {
//			errors.add("bundleIds");
//		}
//
//		if (null == groupName) {
//			errors.add("groupName");
//		}
//
//		if (null == grade) {
//			errors.add("grade");
//		}
//
//		if (bundleIds.size() >10) {
//			errors.add("bundleIds size should be below or equal 10");
//		}

		return errors;
	}
}
