package com.vedantu.subscription.request;

import com.vedantu.subscription.pojo.BundleEntityRequestInfo;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class BundleEntityBulkInsertionRequest extends AbstractFrontEndReq {
    private String bundleId;
    private List<BundleEntityRequestInfo> bundleEntities;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(StringUtils.isEmpty(bundleId)){
            errors.add("[bundleId] cann't be empty");
        }

        if(ArrayUtils.isEmpty(bundleEntities)){
            errors.add("At least one entity need to be selected for this action to be successful");
        }

        if(ArrayUtils.isNotEmpty(bundleEntities) && bundleEntities.size()>100){
            errors.add("No more than 100 entities are allowed while bulk addition");
        }

        if(ArrayUtils.isNotEmpty(bundleEntities)){
            for (BundleEntityRequestInfo bundleEntity : bundleEntities) {
                if(!bundleEntity.validData()){
                    errors.add("There is a validity problem with data - "+bundleEntity);
                    break;
                }
            }
        }

        return errors;
    }
}
