package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;

@Data
public class ActiveEnrollmentCountReq extends AbstractFrontEndReq {
    String userId;
    String commaSeparatedBatchIds;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(userId == null || userId.isEmpty())
            errors.add("userId is null/empty");
        if(commaSeparatedBatchIds == null || commaSeparatedBatchIds.isEmpty())
            errors.add("commaSeparatedBatchIds is null/empty");

        return errors;
    }
}
