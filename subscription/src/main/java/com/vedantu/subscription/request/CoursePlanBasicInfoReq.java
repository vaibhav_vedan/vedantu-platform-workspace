package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Getter;
import lombok.Setter;

import java.util.Set;

/**
 * @author mano
 */
@Getter
@Setter
public class CoursePlanBasicInfoReq extends AbstractFrontEndListReq {
    private Set<String> coursePlanIds;
}
