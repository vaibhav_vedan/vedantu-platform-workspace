package com.vedantu.subscription.request;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class EnrollmentHomePageRequest extends AbstractFrontEndListReq {
    private Long userId;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(Objects.isNull(userId)){
            errors.add("[userId] is empty");
        }

        if(Objects.isNull(getStart())){
            errors.add("[start] is a non null value");
        }

        if(Objects.isNull(getSize())){
            errors.add("[size] cann't be null");
        }

        if(Objects.nonNull(getStart()) && Objects.nonNull(getSize()) && getStart()-getSize()>10){
            errors.add("keep the size between 50");
        }
        return errors;
    }
}
