package com.vedantu.subscription.request;

import com.vedantu.subscription.enums.game.GameTaskStatus;
import lombok.Data;

@Data
public class RewardStatusChangeRequest {
    private Long userId;
    private GameTaskStatus currentStatus;
    private String rewardName;
    private String secureString;
    private String awb;
}
