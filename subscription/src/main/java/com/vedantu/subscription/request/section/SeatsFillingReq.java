package com.vedantu.subscription.request.section;

import lombok.Data;

@Data
public class SeatsFillingReq {
    String batchId;
}
