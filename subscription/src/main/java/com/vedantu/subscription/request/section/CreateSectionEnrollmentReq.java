package com.vedantu.subscription.request.section;

import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.Section;
import lombok.Data;

@Data
public class CreateSectionEnrollmentReq {
    String userId;
    String batchId;
    String courseId;
    Enrollment enrollment;
    Section vacantSection;
    Boolean doDecrement;
}
