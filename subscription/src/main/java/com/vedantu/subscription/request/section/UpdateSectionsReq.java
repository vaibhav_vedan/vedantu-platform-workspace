package com.vedantu.subscription.request.section;

import com.vedantu.onetofew.pojo.AgendaPojo;
import com.vedantu.onetofew.pojo.section.SectionPojo;
import com.vedantu.subscription.response.section.SectionInfo;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Objects;

@Valid
public class UpdateSectionsReq extends AbstractFrontEndListReq{

    @NotNull
    @NotBlank(message = "batchId cannot be empty")
    private String batchId;

    @NotNull
    private List<SectionInfo> sectionList;

    private AgendaPojo upcomingSession;

    public AgendaPojo getUpcomingSession() {
        return upcomingSession;
    }

    public void setUpcomingSession(AgendaPojo upcomingSession) {
        this.upcomingSession = upcomingSession;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public List<SectionInfo> getSectionList() {
        return sectionList;
    }

    public void setSectionList(List<SectionInfo> sectionList) {
        this.sectionList = sectionList;
    }

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(CollectionUtils.isEmpty(sectionList))
            errors.add("sections cannot be null/empty");

        if(Objects.isNull(batchId) || StringUtils.isEmpty(batchId))
            errors.add("batchId cannot be null/empty");

        return errors;

    }
}
