package com.vedantu.subscription.request;

import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractReq;
import lombok.Data;

import java.util.List;
import java.util.Set;

/**
 * @author mano
 */
@Data
public class GameSetupRequest extends AbstractReq {

    public enum EntityType {BUNDLE_ENROLMENT, BATCH_ENROLMENT}

    private EntityType entityType;
    private String entityId;
    private Long userId;
    private Set<String> batchIds;
    private List<Integer> grades;
    private EnrollmentState enrollmentType;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();
        if (userId == null) {
            errors.add("userId");
        }
        if (StringUtils.isEmpty(entityId)) {
            errors.add("entityId");
        }
        if (entityType == null) {
            errors.add("entityType");
        }
        return errors;
    }
}
