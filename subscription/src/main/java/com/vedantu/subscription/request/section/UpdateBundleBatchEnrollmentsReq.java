package com.vedantu.subscription.request.section;

import com.vedantu.onetofew.enums.EnrollmentState;
import lombok.Data;

@Data
public class UpdateBundleBatchEnrollmentsReq {
    String bundleEnrollmentId;
    String bundleId;
    String userId;
    EnrollmentState enrollmentState;
}
