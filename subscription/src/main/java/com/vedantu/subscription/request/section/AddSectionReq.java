package com.vedantu.subscription.request.section;
import com.vedantu.onetofew.pojo.section.SectionPojo;
import com.vedantu.util.StringUtils;
import com.vedantu.util.fos.request.AbstractFrontEndReq;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class AddSectionReq extends AbstractFrontEndReq {
    String batchId;
    List<SectionPojo> sectionList;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(Objects.isNull(batchId))
            errors.add("batchId is null");

        if(StringUtils.isEmpty(batchId))
            errors.add("batchId is empty");

        if(Objects.isNull(sectionList))
            errors.add("sectionsList is null");

        if(sectionList.isEmpty())
            errors.add("sectionsList is empty");

        return errors;
    }
}
