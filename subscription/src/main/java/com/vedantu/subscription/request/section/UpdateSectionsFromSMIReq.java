package com.vedantu.subscription.request.section;

import com.vedantu.exception.BadRequestException;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.subscription.response.section.StudentSectionMappingRes;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;

import java.util.List;
import java.util.Objects;

@Data
public class UpdateSectionsFromSMIReq extends AbstractFrontEndListReq {
    List<StudentSectionMappingRes> reqList;
    String primaryBatchId;

    @Override
    protected List<String> collectVerificationErrors() {
        List<String> errors = super.collectVerificationErrors();

        if(reqList.size() > Section.Constants.MAX_UPDATE_SECTIONS_COUNT)
            errors.add("Cannot update more than " + Section.Constants.MAX_UPDATE_SECTIONS_COUNT + "section enrollments at a time");

        return errors;
    }
}
