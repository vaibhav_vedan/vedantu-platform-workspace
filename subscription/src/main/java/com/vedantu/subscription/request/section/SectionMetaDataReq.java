package com.vedantu.subscription.request.section;

import com.vedantu.util.fos.request.AbstractFrontEndListReq;
import lombok.Data;

import java.util.List;

@Data
public class SectionMetaDataReq extends AbstractFrontEndListReq {
    List<String> taIds;
    List<String> batchIds;
}
