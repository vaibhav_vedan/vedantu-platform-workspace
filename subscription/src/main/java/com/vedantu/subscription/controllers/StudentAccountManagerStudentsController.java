package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.pojo.InsertStudentAccountManagerStudentsReq;
import com.vedantu.onetofew.pojo.ModifyStudentAccountManagerOfStudentsReq;
import com.vedantu.subscription.entities.mongo.StudentAccountManagerStudents;
import com.vedantu.subscription.managers.StudentAccountManagerStudentsManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/studentaccountmanagerstudents")
public class StudentAccountManagerStudentsController {

    @Autowired
    HttpSessionUtils sessionUtils;
    @Autowired
    private StudentAccountManagerStudentsManager studentAccountManagerStudentsManager;
    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(StudentAccountManagerStudentsController.class);

    @RequestMapping(value = "/assignStudentAccountManagerStudents", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse assignStudentAccountManagerStudents(@RequestBody InsertStudentAccountManagerStudentsReq insertStudentAccountManagerStudentsReq) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        insertStudentAccountManagerStudentsReq.verify();
        long noUpdated = studentAccountManagerStudentsManager.assignStudentAccountManagerStudents(insertStudentAccountManagerStudentsReq);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setResponse("" + noUpdated);
        return platformBasicResponse;
    }


    @RequestMapping(value = "/modifyStudentAccountManagerStudents", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse modifyStudentAccountManagerStudents(@RequestBody ModifyStudentAccountManagerOfStudentsReq modifyStudentAccountManagerOfStudentsReq) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        modifyStudentAccountManagerOfStudentsReq.verify();
        long noUpdated = studentAccountManagerStudentsManager.modifyStudentAccountManagerStudents(modifyStudentAccountManagerOfStudentsReq);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setResponse("" + noUpdated);
        return platformBasicResponse;
    }

    @RequestMapping(value = "/removeStudents", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse removeStudents(@RequestBody InsertStudentAccountManagerStudentsReq studentAccountManagerStudentsReq) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        studentAccountManagerStudentsReq.verify();
        long noUpdated = studentAccountManagerStudentsManager.removeStudents(studentAccountManagerStudentsReq);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setResponse("" + noUpdated);
        return platformBasicResponse;
    }

    @RequestMapping(value = "/{studentAccountManagerEmail}/getStudents", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> getStudents(@PathVariable("studentAccountManagerEmail") String studentAccountManagerEmail) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        return studentAccountManagerStudentsManager.getStudentsByStudentAccountManager(studentAccountManagerEmail);
    }

    @RequestMapping(value = "/getSAMByStudentsId", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<StudentAccountManagerStudents> getSAMByStudentsId(@RequestBody List<String> studentIdList) throws VException, UnsupportedEncodingException {
        return studentAccountManagerStudentsManager.getSAMByStudentsId(studentIdList);
    }

}
