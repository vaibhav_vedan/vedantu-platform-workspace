package com.vedantu.subscription.controllers;

import java.util.Arrays;
import java.util.List;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.util.request.CommunicationDataReq;
import com.vedantu.util.response.CommunicationDataRes;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.vedantu.subscription.entities.sql.Subscription;
import com.vedantu.subscription.entities.sql.SubscriptionDetails;
import com.vedantu.subscription.managers.SubscriptionDetailsManager;
import com.vedantu.subscription.managers.SubscriptionManager;
import com.vedantu.subscription.request.AcceptLoanAgreementReq;
import com.vedantu.subscription.request.CancelSubscriptionRequest;
import com.vedantu.subscription.request.CancelSubscriptionSessionRequest;
import com.vedantu.subscription.request.GetSubscriptionsReq;
import com.vedantu.subscription.request.RenewSubscriptionForInstalmentReq;
import com.vedantu.subscription.request.SessionRequest;
import com.vedantu.subscription.viewobject.request.CompleteSessionRequest;
import com.vedantu.subscription.viewobject.request.GetCancelSubscriptionReq;
import com.vedantu.subscription.request.SubscriptionVO;
import com.vedantu.subscription.response.CancelSubscriptionResponse;
import com.vedantu.subscription.response.SessionResponse;
import com.vedantu.subscription.viewobject.request.UnlockHoursRequest;
import com.vedantu.subscription.viewobject.request.UpdateConflictDurationRequest;
import com.vedantu.subscription.viewobject.request.UpdateSubscriptionHoursRequest;
import com.vedantu.subscription.viewobject.response.GetHourlyRateResponse;
import com.vedantu.subscription.viewobject.response.GetSubscriptionsResponse;
import com.vedantu.subscription.viewobject.response.GetUnlockHoursResponse;
import com.vedantu.subscription.response.SubscriptionResponse;
import com.vedantu.util.LogFactory;


@RestController
@RequestMapping("subscription")
public class SubscriptionController {

    @Autowired
    private SubscriptionManager subscriptionManager;

    @Autowired
    private SubscriptionDetailsManager subscriptionDetailsManager;
    @Autowired
    private HttpSessionUtils sessionUtils;
    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SubscriptionController.class);

    @RequestMapping(value = "/createSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionResponse createSubscription(@RequestBody SubscriptionVO subscriptionVO) throws Throwable {
        SubscriptionResponse response = subscriptionManager.createSubscription(subscriptionVO);
        return response;
    }

    @RequestMapping(value = "getSubscription/{id}", method = RequestMethod.GET)
    @ResponseBody
    public SubscriptionResponse getSubscription(@PathVariable("id") Long id) throws Throwable {
        logger.info("Request received for getting Subscription for db_id: " + id);
        SubscriptionResponse response = subscriptionManager.getSubscription(id, true, null);
        logger.info("getSubscription Response:" + response.toString());
        return response;
    }

    @RequestMapping(value = "/getSubscriptionBasicInfo/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Subscription getSubscriptionBasicInfo(@PathVariable("id") Long id) throws Throwable {
        logger.info("ENTRY: " + id);
        Subscription response = subscriptionManager.getSubscriptionBasicInfo(id);
        logger.info("Response:" + response);
        return response;
    }

    @RequestMapping(value = "/getSubscriptionBasicInfoList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Subscription> getSubscriptionBasicInfoList(@RequestBody List<Long> subscriptionIds) throws Throwable {
        List<Subscription> response = subscriptionManager.getSubscriptionBasicInfoList(subscriptionIds);
        return response;
    }

    @RequestMapping(value = {"getSubscriptions"}, method = RequestMethod.GET)
    @ResponseBody
    public GetSubscriptionsResponse getSubscriptions(GetSubscriptionsReq req) throws Throwable {
        GetSubscriptionsResponse response = subscriptionManager.getSubscriptions(req);
        return response;
    }

    @RequestMapping(value = {"getSubscriptionsBasicInfo"}, method = RequestMethod.GET)
    @ResponseBody
    public List<Subscription> getSubscriptionsBasicInfo(GetSubscriptionsReq req) throws Throwable {
        List<Subscription> response = subscriptionManager.getSubscriptionsBasicInfo(req);
        return response;
    }

    @RequestMapping(value = "/addorUpdateHrsForInstalment", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionResponse addorUpdateHrsForInstalment(@RequestBody RenewSubscriptionForInstalmentReq renewSubscriptionForInstalmentReq) throws Throwable {
        logger.info("ENTRY:" + renewSubscriptionForInstalmentReq);
        renewSubscriptionForInstalmentReq.verify();
        SubscriptionResponse response = subscriptionManager
                .addorUpdateHrsForInstalment(renewSubscriptionForInstalmentReq);
        logger.info("EXIT:" + response.toString());
        return response;
    }

    @RequestMapping(value = "/cancelSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CancelSubscriptionResponse cancelSubscription(
            @RequestBody CancelSubscriptionRequest cancelSubscriptionRequest) throws Throwable {
        logger.info(
                "Request received for disabling Subscription for id: " + cancelSubscriptionRequest);
        CancelSubscriptionResponse resp = null;
        if (cancelSubscriptionRequest.getRefundInfoCall() != null
                && cancelSubscriptionRequest.getRefundInfoCall().equals(true)) {
            resp = subscriptionManager.getRefundInfo(cancelSubscriptionRequest.getSubscriptionId());
        } else {
            resp = subscriptionManager.cancelSubscription(cancelSubscriptionRequest);
        }
        return resp;
    }

    @RequestMapping(value = "/cancelTrialSubscription", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CancelSubscriptionResponse cancelTrialSubscription(
            @RequestBody CancelSubscriptionRequest cancelSubscriptionRequest) throws Throwable {
        logger.info(
                "Request received for disabling Subscription for id: " + cancelSubscriptionRequest.getSubscriptionId());
        CancelSubscriptionResponse resp = subscriptionManager.cancelTrialSubscription(cancelSubscriptionRequest);
        return resp;
    }

    @Transactional
    @RequestMapping(value = "/revertCreateSubscription/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public CancelSubscriptionResponse revertCreateSubscription(@PathVariable("id") Long id) throws Throwable {
        logger.info("Request received for Reverting creation of Subscription for id: " + id);
        CancelSubscriptionRequest cancelSubscriptionRequest = new CancelSubscriptionRequest(null, id,
                "revertCreateSubscription", null);
        CancelSubscriptionResponse resp = subscriptionManager.cancelSubscription(cancelSubscriptionRequest);
        return resp;
    }

    @RequestMapping(value = "revertRenewalSubscription/{id}", method = RequestMethod.DELETE)
    @ResponseBody
    public SubscriptionDetails revertRenewalSubscription(@PathVariable("id") Long id) throws Exception {
        logger.info("Request received for reverting renewal of a SubscriptionId: " + id);
        SubscriptionDetails subscriptionDetailsRes = subscriptionDetailsManager.getSubscriptionDetailsById(id, true,
                null);
        if (subscriptionDetailsRes != null) {
            subscriptionDetailsRes.setEnabled(false);
            subscriptionDetailsRes = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetailsRes,
                    true, null);
            logger.info("Response:" + subscriptionDetailsRes.toString());
        }
        return subscriptionDetailsRes;
    }

    @Transactional
    @RequestMapping(value = "revertCancelSubscription/{id}", method = RequestMethod.POST)
    @ResponseBody
    public SubscriptionResponse revertCancelSubscription(@RequestBody GetCancelSubscriptionReq req,
            @PathVariable("id") Long id) throws Throwable {
        logger.info("Request received for disabling Subscription for id: " + id);
        req.setSubscriptionId(id);
        SubscriptionResponse response = subscriptionManager.revertCancelSubscription(req);
        return response;
    }

    @RequestMapping(value = "/bookSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionResponse bookSession(@RequestBody SessionRequest sessionRequest) throws Throwable {
        logger.info("Book Session Request:" + sessionRequest.toString());
        SessionResponse response;
        response = subscriptionManager.bookSession(sessionRequest);
        logger.info("Book Session Response:" + response.toString());
        return response;
    }

    @RequestMapping(value = "/completeSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Subscription completeSession(@RequestBody CompleteSessionRequest sessionRequest) throws Throwable {
        logger.info("Complete Session Request:" + sessionRequest.toString());
        Subscription reponse;
        reponse = subscriptionManager.completeSession(sessionRequest);
        logger.info("Complete Session Response:" + reponse.toString());
        return reponse;
    }

    @RequestMapping(value = "/cancelSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionResponse cancelSession(@RequestBody CancelSubscriptionSessionRequest cancelSessionRequest) throws Throwable {
        logger.info("Cancel Session Request:" + cancelSessionRequest);
        SessionResponse response = subscriptionManager.cancelSession(cancelSessionRequest);
        logger.info("Cancel Session Response:" + response.toString());
        return response;
    }

    @RequestMapping(value = "/rescheduleSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionResponse rescheduleSession(@RequestBody SessionRequest sessionRequest) throws Throwable {
        logger.info("Reschedule Session Request:" + sessionRequest.toString());
        SessionResponse response;
        response = subscriptionManager.rescheduleSession(sessionRequest);
        logger.info("Reschedule Session Response:" + response.toString());
        return response;
    }

    @RequestMapping(value = "updateDurationConflict", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionResponse updateDurationConflict(@RequestBody UpdateConflictDurationRequest request)
            throws Throwable {
        logger.info(
                "Request received for updating duration conflict for subscriptionId: " + request.getSubscriptionId());
        SubscriptionResponse response = subscriptionManager.updateDurationConflict(request);
        logger.info(" Response:" + response.toString());
        return response;
    }

    @RequestMapping(value = "updateSubscriptionHours", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionResponse updateSubscriptionHours(@RequestBody UpdateSubscriptionHoursRequest request)
            throws Throwable {
        logger.info(
                "Request received for updating duration conflict for subscriptionId: " + request.getSubscriptionId());
        SubscriptionResponse response = subscriptionManager.updateSubscriptionHours(request);
        logger.info("Response:" + response.toString());
        return response;
    }

    @RequestMapping(value = "getHourlyRate/{id}", method = RequestMethod.GET)
    @ResponseBody
    public GetHourlyRateResponse getHourlyRate(@PathVariable("id") Long id) throws Throwable {
        logger.info("Request received for getting hourly rate for subscriptionId: " + id);
        GetHourlyRateResponse response = subscriptionManager.getHourlyRate(id);
        logger.info("getHourlyRate Response:" + response.toString());
        return response;
    }

    @RequestMapping(value = "unlockHours", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GetUnlockHoursResponse unlockHours(@RequestBody List<UnlockHoursRequest> unlockHoursRequests)
            throws Throwable {
        logger.info("Request received for unlockHours: " + unlockHoursRequests);
        GetUnlockHoursResponse response = subscriptionManager.unlockHours(unlockHoursRequests);
        logger.info("unlockHours Response:" + response.toString());
        return response;
    }

//    @RequestMapping(value = "migrateSubscriptionState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public List<Subscription> migrateSubscriptionState() throws Throwable {
//        logger.info("Request received for migrateSubscriptionState: " + System.currentTimeMillis());
//        List<Subscription> response = subscriptionManager.migrateSubscriptionState();
//        logger.info("migrateSubscriptionState Response:" + System.currentTimeMillis());
//        return response;
//    }

    @RequestMapping(value = "/getSubscriptionCount", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getSubscriptionCount(GetSubscriptionsReq req) throws Throwable {

        Long response = subscriptionManager.getSubscriptionsCount(req);
        return response;
    }

    @RequestMapping(value = "/communicateInBulkOld", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CommunicationDataRes communicateInBulk(@RequestBody CommunicationDataReq req) throws VException {

        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        return subscriptionManager.communicateInBulk(req);
    }
    
    @RequestMapping(value = "/acceptLoanAgreement", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String acceptLoanAgreement(AcceptLoanAgreementReq req) throws VException {
        req.verify();
        return subscriptionManager.acceptLoanAgreement(req).getResponse();
    }

    @RequestMapping(value = "/communicateInBulk", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CommunicationDataRes communicateInBulkNew(@RequestBody CommunicationDataReq req) throws VException {
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        return subscriptionManager.communicateInBulkNew(req);
    }

}
