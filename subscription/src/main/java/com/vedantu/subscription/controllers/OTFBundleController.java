package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.dinero.request.ContextWiseNextDueInstalmentsReq;
import com.vedantu.dinero.request.PayInstalmentReq;
import com.vedantu.dinero.response.ContextWiseNextDueInstalmentsRes;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.PayInstalmentRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.NotFoundException;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.request.AddEditOTFBundleReq;
import com.vedantu.onetofew.request.GetInstalmentReq;
import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.onetofew.request.PurchaseOTFBundleReq;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.managers.EnrollmentManager;
import com.vedantu.subscription.managers.OTFBundleManager;
import com.vedantu.subscription.managers.PaymentManager;
import com.vedantu.subscription.request.ProcessOtfBundlePostPaymentReq;
import com.vedantu.subscription.response.CheckIfRegFeePaidRes;
import com.vedantu.subscription.viewobject.request.GetCourseListingReq;
import com.vedantu.subscription.viewobject.response.GetCourseListingRes;
import com.vedantu.subscription.viewobject.request.EndSubscriptionReq;
import com.vedantu.subscription.viewobject.response.EndSubscriptionRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import java.io.IOException;
import java.util.ArrayList;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
@RequestMapping("otfBundle")
public class OTFBundleController {

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(OTFBundleController.class);

    @RequestMapping(value = "/addEditOTFBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTFBundleInfo addEditOTFBundle(@RequestBody AddEditOTFBundleReq addEditBundleReq) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        addEditBundleReq.verify();
        return otfBundleManager.addEditOTFBundle(addEditBundleReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTFBundleInfo getOTFBundle(@PathVariable("id") String id) throws VException {
        return otfBundleManager.getOTFBundle(id);
    }

    @RequestMapping(value = "/public/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OTFBundleInfo getPublicOTFBundle(@PathVariable("id") String id) throws VException {
        return otfBundleManager.getOTFBundle(id);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFBundleInfo> getOTFBundleInfos(@ModelAttribute GetOTFBundlesReq req) throws VException {
        return otfBundleManager.getOTFBundleInfos(req);
    }

    @RequestMapping(value = "/getBundleForBatch", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFBundleInfo> getOTFBundleInfosForBatch(@ModelAttribute GetOTFBundlesReq req) throws VException {
        return otfBundleManager.getOTFBundleInfosForBatch(req);
    }    
    
    @RequestMapping(value = "/public/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<OTFBundleInfo> getPublicOTFBundleInfos(@ModelAttribute GetOTFBundlesReq req) throws VException {
        return otfBundleManager.getOTFBundleInfos(req);
    }

    @RequestMapping(value = "/purchaseAndRegisterOTFBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo purchaseAndRegisterOTFBundle(@RequestBody PurchaseOTFBundleReq req) throws VException, CloneNotSupportedException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return otfBundleManager.purchaseAndRegisterOTFBundle(req);
    }

    @RequestMapping(value = "/purchaseOTFBundleByAdvancePayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo purchaseOTFBundleByAdvancePayment(@RequestBody PurchaseOTFBundleReq req) throws VException, CloneNotSupportedException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return otfBundleManager.purchaseOTFBundleByAdvancePayment(req);
    }

    @RequestMapping(value = "/checkIfRegFeePaid", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CheckIfRegFeePaidRes checkIfRegFeePaid(@RequestBody PurchaseOTFBundleReq req) throws VException, CloneNotSupportedException {
        return otfBundleManager.checkIfRegFeePaid(req);
    }

    @RequestMapping(value = "/processOTFBundlePurchaseAfterRegFeePayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void processOTFBundlePurchaseAfterRegFeePayment(@RequestBody ProcessOtfBundlePostPaymentReq req) throws VException, CloneNotSupportedException {
        otfBundleManager.processOTFBundlePurchaseAfterRegFeePayment(req.getOrderId(), req.getUserId(), otfBundleManager.getOTFBundle(req.getOtfBundleId()), null);
    }

    @RequestMapping(value = "/processOtfBundlePurchaseAfterAdvancePayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void processOtfBundlePurchaseAfterAdvancePayment(@RequestBody ProcessOtfBundlePostPaymentReq req) throws VException, CloneNotSupportedException {
        otfBundleManager.processOtfBundlePurchaseAfterAdvancePayment(req.getOrderId(), req.getUserId(), otfBundleManager.getOTFBundle(req.getOtfBundleId()), null);
    }

    @RequestMapping(value = "/processOtmBundlePurchaseAfterPayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void processOtmBundlePurchaseAfterPayment(@RequestBody ProcessOtfBundlePostPaymentReq req) throws VException, CloneNotSupportedException {
        otfBundleManager.processOtmBundlePurchaseAfterPayment(req.getOrderId(), req.getUserId(), otfBundleManager.getOTFBundle(req.getOtfBundleId()), null);
    }

    @RequestMapping(value = "/purchaseOTMBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public OrderInfo purchaseOTMBundle(@RequestBody PurchaseOTFBundleReq req) throws VException, CloneNotSupportedException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return otfBundleManager.purchaseOTMBundle(req);
    }

    @RequestMapping(value = "/getInstalments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<InstalmentInfo> getInstalments(GetInstalmentReq req) throws VException {
        return otfBundleManager.getInstalments(req);
    }

    @RequestMapping(value = "/processInstalmentAfterPayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void processInstalmentAfterPayment(@RequestBody InstalmentInfo req) throws VException, CloneNotSupportedException {
        otfBundleManager.processInstalmentAfterPayment(req);
    }

    @RequestMapping(value = "/markEnrollmentStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void markEnrollmentStatus(@RequestParam(name = "userId") Long userId,
            @RequestParam(name = "entityId") String entityId,
            @RequestParam(name = "newStatus") EntityStatus newStatus) throws VException, CloneNotSupportedException {
        otfBundleManager.markEnrollmentStatus(userId, entityId, newStatus);
    }

    @RequestMapping(value = "payInstalmentForBundle", method = RequestMethod.POST)
    @ResponseBody
    public PayInstalmentRes payInstalmentForBundle(@RequestBody PayInstalmentReq req) throws VException, IOException {
        sessionUtils.checkIfAllowedList(req.getUserId(), null, Boolean.TRUE);
        req.setNewFlow(Boolean.TRUE);
        return paymentManager.payInstalment(req);
    }

    @RequestMapping(value = "listByGroupName", method = RequestMethod.GET)
    @ResponseBody
    public void listByGroupName(@RequestParam(name = "groupName") String groupName){
        
    }
    
    @RequestMapping(value = "/public/getCourseListings", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<GetCourseListingRes> getCourseListings(GetCourseListingReq getCourseListingReq, @RequestParam(name="boardIds" , required = false) List<Long> boardIds) throws BadRequestException, NotFoundException{
        getCourseListingReq.verify();
        
        return otfBundleManager.getBundleListing(getCourseListingReq, boardIds);
        
    }
    
    @RequestMapping(value = "/public/getTargets", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getTargets() throws BadRequestException, NotFoundException{
        //getCourseListingReq.verify();
        
        return otfBundleManager.getTargetsForListing();
        
    }  
    
    @RequestMapping(value = "/endSubscription", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EndSubscriptionRes getSubscriptionCount(@RequestBody EndSubscriptionReq endSubscriptionReq) throws Throwable {
        //throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "End subscription not allowed");
        sessionUtils.checkIfAllowed(endSubscriptionReq.getCallingUserId(),Role.ADMIN, Boolean.TRUE);
        return otfBundleManager.endSubscription(endSubscriptionReq);
    }
    
    @RequestMapping(value = "getContextWiseNextDueInstalments", method = RequestMethod.GET)
    @ResponseBody
    public List<ContextWiseNextDueInstalmentsRes> getContextWiseNextDueInstalments(ContextWiseNextDueInstalmentsReq req)
            throws VException {
        return otfBundleManager.getContextWiseNextDueInstalments(req);
    } 

    @RequestMapping(value = "/getBundleInfoByGroupName", method = RequestMethod.GET)
    @ResponseBody    
    public OTFBundleInfo getOTFBundleInfoByGroupName(@RequestParam(name="groupName", required = true) String groupName) throws NotFoundException, ConflictException{
        
        return otfBundleManager.getOTFBundleInfoByGroupName(groupName);
    }
    
}
