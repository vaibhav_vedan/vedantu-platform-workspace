package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.managers.BundleEntityManager;
import com.vedantu.subscription.request.BundleEntityBulkInsertionRequest;
import com.vedantu.subscription.request.BundleEntityFilterRequest;
import com.vedantu.subscription.request.BundleEntityRequest;
import com.vedantu.subscription.request.GetBundlePackageReferenceRequest;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/bundleEntity")
public class BundleEntityController {

    @Autowired
    private BundleEntityManager bundleEntityManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @RequestMapping(value = "/addEditBundleEntity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleEntity addEditBundleEntity(@RequestBody BundleEntityRequest request) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        request.verify();
        return bundleEntityManager.addEditBundleEntity(request);
    }

    @RequestMapping(value = "/addEditBundleEntityInBulk", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEntity> addEditBundleEntityInBulk(@RequestBody BundleEntityBulkInsertionRequest request) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        request.verify();
        return bundleEntityManager.addEditBundleEntityInBulk(request);
    }

    @RequestMapping(value = "/removeBundleEntity", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse removeBundleEntity(@RequestBody List<String> ids) throws VException {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return bundleEntityManager.removeBundleEntity(ids);
    }

    @RequestMapping(value = "/getBundleEntityDataForBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEntity> getBundleEntityDataForBundle(@RequestBody GetBundlePackageReferenceRequest request)
            throws VException {
        request.verify();
        return bundleEntityManager.getBundleEntityDataForBundle(request);
    }

    @RequestMapping(value = "/getBundleEntityDataForMultipleBundles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEntity> getBundleEntityDataForMultipleBundles(@RequestBody GetBundlePackageReferenceRequest request)
            throws VException {
        request.verify();
        return bundleEntityManager.getBundleEntityDataForMultipleBundles(request);
    }

    @RequestMapping(value = "/getBundleEntityDataForBundleForSalesConversion", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEntity> getBundleEntityDataForBundleForSalesConversion(@RequestBody com.vedantu.subscription.pojo.bundle.GetBundlePackageReferenceRequest request)
            throws VException {
        request.verify();
        return bundleEntityManager.getBundleEntityDataForBundleForSalesConversion(request);
    }
}
