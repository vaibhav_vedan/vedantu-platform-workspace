package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.dinero.response.OrderInfo;
import java.util.List;

import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.managers.BundlePackageManager;
import com.vedantu.subscription.pojo.BundlePackageDetailsInfo;
import com.vedantu.subscription.pojo.BundlePackageInfo;
import com.vedantu.subscription.request.AddEditBundlePackageReq;
import com.vedantu.subscription.request.BundleCreateEnrolmentReq;
import com.vedantu.subscription.request.GetBundlePackagesReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by somil on 09/05/17.
 */
@RestController
@RequestMapping("/bundlePackage")
public class BundlePackageController {

    @Autowired
    private BundlePackageManager bundlePackageManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BundlePackageController.class);

    @RequestMapping(value = "/addEditBundlePackage", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundlePackageInfo addEditBundlePackage(@RequestBody AddEditBundlePackageReq addEditBundleReq) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        addEditBundleReq.verify();
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundlePackageManager.addEditBundlePackage(addEditBundleReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundlePackageInfo getBundlePackage(@PathVariable("id") String id) throws NotFoundException {
        return bundlePackageManager.getBundlePackage(id);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageInfo> getBundlePackageInfos(@ModelAttribute GetBundlePackagesReq req) throws VException {
        return bundlePackageManager.getBundlePackageInfos(req);
    }

    @RequestMapping(value = "/get/public", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageInfo> getPublicBundlePackageInfos(@ModelAttribute GetBundlePackagesReq req) throws VException {
        return bundlePackageManager.getBundlePackageInfos(req);
    }

    @RequestMapping(value = "/createEnrolments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrolment> createEnrolments(@RequestBody BundleCreateEnrolmentReq req) throws VException {
        return bundlePackageManager.createEnrolments(req);
    }


    @RequestMapping(value = "/getBundlePackageDetails/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundlePackageDetailsInfo getBundlePackageDetails(@PathVariable("id") String id) throws VException {
        return bundlePackageManager.getBundlePackageDetails(id);
    }

    @RequestMapping(value = "/getBundlePackageDetailsList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageDetailsInfo> getBundlePackageDetailsList(GetBundlePackagesReq req) throws VException {
        return bundlePackageManager.getBundlePackageDetailsList(req);
    }

    @RequestMapping(value = "/getBundlesForEntity", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundlePackageInfo> getBundlesForEntity(GetBundlePackagesReq req) throws VException {
        return bundlePackageManager.getBundlesForEntity(req);
    }
    
    @RequestMapping(value = "/processBundlePurchaseAfterPayment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void processBundlePurchaseAfterPayment(@RequestBody OrderInfo orderInfo) throws VException {
        bundlePackageManager.processBundlePurchaseAfterPayment(orderInfo);
    }
        
    @RequestMapping(value = "/checkForEnrollmentInBundleBatch", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse checkForEnrollmentInBundleBatch(@RequestParam(value = "bundleId", required = true) String bundleId,
                                                                @RequestParam(value = "userId", required = true) String userId){
        
        
        return bundlePackageManager.checkForEnrollmentInBundleBatch(bundleId, userId);
    }
    
}

