/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.dao.CoursePlanHoursDAO;
import com.vedantu.subscription.request.*;
import com.vedantu.subscription.response.CoursePlanBasicInfo;
import com.vedantu.subscription.response.CoursePlanInfo;
import com.vedantu.subscription.response.GetCoursePlanReminderListRes;
import com.vedantu.subscription.entities.sql.CoursePlanHours;
import com.vedantu.subscription.managers.CoursePlanManager;
import com.vedantu.subscription.pojo.TeacherCourseStateCount;
import com.vedantu.subscription.response.GetCoursePlanBalanceRes;
import com.vedantu.subscription.response.StructuredCourseInfo;
import com.vedantu.subscription.response.UserDashboardCoursePlanRes;
import com.vedantu.subscription.viewobject.request.CompleteSessionRequest;
import com.vedantu.subscription.viewobject.request.UnlockHoursRequest;
import com.vedantu.subscription.viewobject.request.UpdateConflictDurationRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("/courseplan")
public class CoursePlanController {

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private CoursePlanHoursDAO coursePlanHoursDAO;

    @Autowired
    HttpSessionUtils sessionUtils;

    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CoursePlanController.class);

    @RequestMapping(value = "/addEditCoursePlan", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanInfo addEditCoursePlan(@RequestBody AddEditCoursePlanReq addEditCoursePlanReq) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException {
        return coursePlanManager.addEditCoursePlan(addEditCoursePlanReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanInfo getCoursePlan(@PathVariable("id") String id) throws NotFoundException, ForbiddenException, VException {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        CoursePlanInfo coursePlanInfo = coursePlanManager.getCoursePlan(id, true, exposeEmail);
        coursePlanManager.validateCoursePlanAccess(coursePlanInfo);
        return coursePlanInfo;
    }

    @RequestMapping(value = "/basicInfo/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanBasicInfo getCoursePlanBasicInfo(@PathVariable("id") String id) throws NotFoundException {
        return coursePlanManager.getCoursePlanBasicInfo(id);
    }

    @RequestMapping(value = "/basicInfoWithHours/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanBasicInfo getCoursePlanBasicInfoWithHours(@PathVariable("id") String id) {
        return coursePlanManager.getCoursePlanBasicInfoWithHours(id);
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CoursePlanInfo> getCoursePlanInfos(GetCoursePlansReq req) throws VException {

        boolean exposeEmail = false;
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();

        if (sessionData != null && (Role.ADMIN.equals(sessionData.getRole())
                || Role.STUDENT_CARE.equals(sessionData.getRole()))) {
            exposeEmail = true;
        }
        return coursePlanManager.getCoursePlanInfos(req,true,exposeEmail);
    }

    @RequestMapping(value = "/getCousePlansInfosByIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<String, CoursePlanInfo> getCousePlansInfosByIds(@RequestParam(value = "coursePlanIds") List<String> coursePlanIds) throws VException {
        return coursePlanManager.getCousePlansInfosByIds(coursePlanIds);
    }

    @RequestMapping(value = "/markRegisteredForParentCourse", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanInfo markRegisteredForParentCourse(@RequestBody MarkRegisteredForParentCourseReq req) throws VException {
        return coursePlanManager.markRegisteredForParentCourse(req);
    }

    @RequestMapping(value = "/getActiveCoursePlanByParentIdAndStudentId", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanInfo getActiveCoursePlanByParentIdAndStudentId(@RequestParam(value = "studentId") Long studentId,
            @RequestParam(value = "parentCourseId") String parentCourseId)
            throws VException {
        return coursePlanManager.getActiveCoursePlanByParentIdAndStudentId(studentId, parentCourseId);
    }

    @RequestMapping(value = "/addEditStructuredCourse", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addEditStructuredCourse(@RequestBody AddEditStructuredCourseReq addEditOTOCourseReq) throws BadRequestException, VException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.STUDENT_CARE), Boolean.TRUE);
        return coursePlanManager.addEditStructuredCourse(addEditOTOCourseReq);
    }

    @RequestMapping(value = "/structuredCourse/{id}", method = RequestMethod.GET)
    @ResponseBody
    public StructuredCourseInfo getStructuredCourse(@PathVariable("id") String id) {
        return coursePlanManager.getStructuredCourse(id);
    }

    @RequestMapping(value = "/structuredCourses", method = RequestMethod.GET)
    @ResponseBody
    public List<StructuredCourseInfo> getStructuredCourse(GetStructuredCoursesReq req) {
        return coursePlanManager.getStructuredCourses(req);
    }

    @RequestMapping(value = "/addCoursePlanHours", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addCoursePlanHours(@RequestBody AddCoursePlanHoursReq req) throws VException, Exception {
        return coursePlanManager.addCoursePlanHoursToQueue(req);
    }

    @RequestMapping(value = "/transferBookSessionHours", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse transferBookSessionHours(@RequestBody CoursePlanSessionRequest coursePlanSessionRequest) throws VException {
        coursePlanSessionRequest.verify();
        return coursePlanManager.transferBookSessionHoursToQueue(coursePlanSessionRequest);
    }

    @RequestMapping(value = "/transferCancelSessionHours", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse transferCancelSessionHours(@RequestBody TransferCancelSessionHoursReq req) throws VException {
        return coursePlanManager.transferCancelSessionHoursToQueue(req);
    }

    @RequestMapping(value = "/completeCoursePlanSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse completeCoursePlanSession(@RequestBody CompleteSessionRequest sessionRequest) throws Throwable {
        logger.info("Complete Session Request:" + sessionRequest.toString());
        return coursePlanManager.completeCoursePlanSessionToQueue(sessionRequest);
    }

    @RequestMapping(value = "/updateDurationConflict", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateDurationConflict(@RequestBody UpdateConflictDurationRequest sessionRequest) throws Throwable {
        logger.info("Complete Session Request:" + sessionRequest.toString());
        sessionRequest.verify();
        return coursePlanManager.updateDurationConflict(sessionRequest);
    }

    @RequestMapping(value = "/unlockHours", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse unlockHours(@RequestBody List<UnlockHoursRequest> req) throws VException {
        return coursePlanManager.unlockHoursToQueue(req);
    }

    @RequestMapping(value = "/markCoursePlanState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markCoursePlanState(@RequestBody MarkCoursePlanStateReq req) throws VException {
        return coursePlanManager.markCoursePlanState(req);
    }

    @RequestMapping(value = "/getByCreationTime", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CoursePlanInfo> getByCreationTime(ExportCoursePlansReq req) throws VException {
        return coursePlanManager.getByCreationTime(req);
    }

    @RequestMapping(value = "/editInstalmentSchedule", method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanInfo editInstalmentSchedule(@RequestBody EditInstalmentAmtAndSchedule req) throws
            VException, NotFoundException, Exception {
        return coursePlanManager.editInstalmentSchedule(req);
    }

    @RequestMapping(value = "/getTeacherCoursePlanMapping", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Map<Long, TeacherCourseStateCount> getTeacherCoursePlanMapping(@RequestParam("startTime") Long startTime,
            @RequestParam("endTime") Long endTime) throws VException {
        if (startTime == null || endTime == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "start or endTime not given");
        }

        return coursePlanManager.getTeacherCoursePlanMapping(startTime, endTime);
    }

    @RequestMapping(value = "/getActiveCoursePlanHours", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CoursePlanHours> getActiveCoursePlanHours(@RequestParam(value = "start", required = true) int start,
            @RequestParam(value = "size", required = true) int size) throws VException {
        return coursePlanManager.getActiveCoursePlanHours(start, size);
    }

    @RequestMapping(value = "/getCoursePlanTrialSessionList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<GetCoursePlanReminderListRes> getCoursePlanTrialSessionList(GetCoursePlanReminderListReq req) throws VException {
        return coursePlanManager.getCoursePlanTrialSessionList(req);
    }

    @RequestMapping(value = "/getCoursePlansWithTrialSessions", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<GetCoursePlanReminderListRes> getCoursePlansWithTrialSessions(GetCoursePlanReminderListReq req) throws VException {
        return coursePlanManager.getCoursePlansWithTrialSessions(req);
    }

    @RequestMapping(value = "/getCoursePlanPriceSansRegFee", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getCoursePlanPriceSansRegFee(@RequestParam(name = "coursePlanId", required = true) String coursePlanId) throws NotFoundException {
        CoursePlanBasicInfo coursePlanBasicInfo = coursePlanManager.getCoursePlanBasicInfo(coursePlanId);
        PlatformBasicResponse res = new PlatformBasicResponse();
        Integer price = coursePlanBasicInfo.getPrice();
        if (price == null) {
            price = 0;
        }
        if (coursePlanBasicInfo.getTrialRegistrationFee() != null) {
            price -= coursePlanBasicInfo.getTrialRegistrationFee();
        }
        res.setResponse(String.valueOf(price));
        return res;
    }

    @RequestMapping(value = "/getCoursePlanBalanceRes", method = RequestMethod.GET)
    @ResponseBody
    public GetCoursePlanBalanceRes getCoursePlanBalanceRes(@RequestParam(name = "coursePlanId", required = true) String coursePlanId) throws NotFoundException, ConflictException {
        GetCoursePlanBalanceRes res = coursePlanManager.getCoursePlanBalanceRes(coursePlanId);
        return res;
    }

    @RequestMapping(value = "/getUserDashboardCoursePlans", method = RequestMethod.GET)
    @ResponseBody
    public List<UserDashboardCoursePlanRes> getUserDashboardCoursePlans(@RequestParam(name = "userId", required = true) Long userId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return coursePlanManager.getUserDashboardCoursePlans(userId);
    }

    @RequestMapping(value = "/getTitle/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CoursePlanBasicInfo getCoursePlanTitle(@PathVariable("id") String id) throws NotFoundException {
        return coursePlanManager.getCoursePlanTitleById(id);
    }

    @RequestMapping(value = "/getBasicInfosForHomePage", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<CoursePlanBasicInfo> getBasicInfosForHomePage(@ModelAttribute CoursePlanBasicInfoReq req) throws NotFoundException, BadRequestException, ForbiddenException {
        req.verify();
        sessionUtils.checkIfUserLoggedIn();
        if (req.getCoursePlanIds() == null || req.getCoursePlanIds().isEmpty()) {
            return new ArrayList<>();
        }
        if (req.getCoursePlanIds().size() > 20) {
            logger.error("more that 20 docs are queried for home page apis in course plan, total ids queried {}", req.getCoursePlanIds().size());
        }
        return coursePlanManager.getBasicInfosForHomePage(req);
    }
}
