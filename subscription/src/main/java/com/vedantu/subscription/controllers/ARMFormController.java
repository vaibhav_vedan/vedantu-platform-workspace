package com.vedantu.subscription.controllers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.managers.ARMFormManager;
import com.vedantu.subscription.request.AddARMFormReq;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.response.ARMFormInfo;
import com.vedantu.util.LogFactory;
import java.io.UnsupportedEncodingException;
import javax.validation.Valid;

@RestController
@RequestMapping("/armform")
public class ARMFormController {
	
	@Autowired
    private ARMFormManager aRMFormManager;
	
    @Autowired
    private LogFactory logFactory;
    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(ARMFormController.class);
    
    @RequestMapping(value = "/addARMForm", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ARMFormInfo addARMForm(@Valid @RequestBody AddARMFormReq addARMFormReq) throws VException, UnsupportedEncodingException {
        return aRMFormManager.addARMForm(addARMFormReq);
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ARMFormInfo getARMForm(@PathVariable("id") String id) throws NotFoundException {
        return aRMFormManager.getARMForm(id);
    }
    
    @RequestMapping(value = "/getARMForms", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ARMFormInfo> getARMForms(ExportCoursePlansReq req) throws VException {
        return aRMFormManager.getARMForms(req);
    }
    
    @RequestMapping(value = "/getARMFormByAdmin/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ARMFormInfo> getARMFormByAdmin(@PathVariable("id") Long id) throws NotFoundException {
        return aRMFormManager.getARMFormByAdmin(id);
    }
    
    @RequestMapping(value = "/getARMFormByStartDate/{startDate}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ARMFormInfo> getARMFormByStartDate(@PathVariable("startDate") Long startDate) throws NotFoundException {
        return aRMFormManager.getARMFormByStartDate(startDate);
    }

    @RequestMapping(value = "/getARMFormByStudentId/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ARMFormInfo> getARMFormByStudentId(@PathVariable("id") Long id) throws NotFoundException, BadRequestException {
        return aRMFormManager.getARMFormByStudentId(id);
    }
    
    @RequestMapping(value = "/closeARMForm/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ARMFormInfo closeARMForm(@PathVariable("id") String id) throws NotFoundException, BadRequestException {
        return aRMFormManager.closeARMForm(id);
    }    

}
