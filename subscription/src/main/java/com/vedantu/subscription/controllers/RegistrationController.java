/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.dinero.request.RefundRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.request.AddAdvancePaymentOrderIdReq;
import com.vedantu.onetofew.request.AddRegistrationReq;
import com.vedantu.onetofew.request.MarkRegistrationStatusReq;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.subscription.managers.RegistrationManager;
import com.vedantu.subscription.viewobject.request.GetRegistrationsReq;
import com.vedantu.subscription.viewobject.request.RefundRegFee;
import com.vedantu.subscription.viewobject.response.GetRegistrationsResp;
import com.vedantu.subscription.viewobject.response.GetUserDashboardRegistrationsResp;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author ajith
 */
@RestController
@RequestMapping("registration")
public class RegistrationController {

    @Autowired
    private RegistrationManager registrationManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(RegistrationController.class);

    @Autowired
    private HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/addRegistration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addRegistration(@RequestBody AddRegistrationReq req) throws NotFoundException, BadRequestException, VException {
        req.verify();
        registrationManager.addRegistration(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/markRegistrationStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markRegistrationStatus(@RequestBody MarkRegistrationStatusReq req) throws NotFoundException, BadRequestException, VException {
        req.verify();
        if (RegistrationStatus.valueOf(req.getNewstatus()) == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "invalid status");
        }
        List<RegistrationStatus> oldStatusList = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req.getOldStatuses())) {
            for (String status : req.getOldStatuses()) {
                oldStatusList.add(RegistrationStatus.valueOf(status));
            }
        }
        return registrationManager.markRegistrationStatus(req.getRegId(), req.getEntityType(),
                req.getEntityId(), RegistrationStatus.valueOf(req.getNewstatus()), req.getUserId(), req.getDeliverableEntityIds(), oldStatusList);
    }

    @RequestMapping(value = "/getRegistrationsForUser", method = RequestMethod.GET)
    @ResponseBody
    public List<GetRegistrationsResp> getRegistrationsForUser(GetRegistrationsReq req) throws VException {
        if (req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "invalid userId");
        }
        return registrationManager.getRegistrationsForUser(req);
    }

    @RequestMapping(value = "/getRegistrationsForUserDashboard", method = RequestMethod.GET)
    @ResponseBody
    public List<GetUserDashboardRegistrationsResp> getRegistrationsForUserDashboard(GetRegistrationsReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        if (req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "invalid userId");
        }
        return registrationManager.getRegistrationsForUserDashboard(req);
    }

    @RequestMapping(value = "/addAdvancePaymentOrderId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addAdvancePaymentOrderId(@RequestBody AddAdvancePaymentOrderIdReq req) throws NotFoundException, BadRequestException, VException {
        req.verify();
        return registrationManager.addAdvancePaymentOrderId(req);
    }

    @RequestMapping(value = "/getRegistrationsForUserAndEntity", method = RequestMethod.GET)
    @ResponseBody
    public GetRegistrationsResp getRegistrationsForUserAndEntity(GetRegistrationsReq req) throws VException {
        return registrationManager.getRegistrationsForUserAndEntity(req);
    }

    @RequestMapping(value = "/getApplicableBaseInstalment", method = RequestMethod.GET)
    @ResponseBody
    public List<BaseInstalmentInfo> getApplicableBaseInstalment(@RequestParam(value = "userId") Long userId, @RequestParam(value = "entityId") String entityId) throws VException {
        return registrationManager.getApplicableBaseInstalment(userId, entityId);
    }

    @RequestMapping(value = "/getUsersRegistrationForMultipleEntities", method = RequestMethod.GET)
    @ResponseBody
    public List<GetRegistrationsResp> getUsersRegistrationForMultipleEntities(GetRegistrationsReq req) throws VException {
        if (req.getUserId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "invalid userId");
        }
        return registrationManager.getUsersRegistrationForMultipleEntities(req.getUserId(), req.getEntityIdList());
    }

    @RequestMapping(value = "/refundRegFee", method = RequestMethod.POST)
    @ResponseBody
    public RefundRes refundRegFee(@RequestBody RefundRegFee req) throws
            VException, CloneNotSupportedException {
        sessionUtils.checkIfAllowedList(req.getUserId(), null, Boolean.TRUE);
        return registrationManager.refundRegFee(req);
    }

}
