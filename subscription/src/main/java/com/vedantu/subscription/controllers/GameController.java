package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.pojo.LMSTestInfo;
import com.vedantu.subscription.entities.mongo.GameTemplate;
import com.vedantu.subscription.managers.GameManager;
import com.vedantu.subscription.request.GameSetupRequest;
import com.vedantu.subscription.request.RewardStatusChangeRequest;
import com.vedantu.subscription.response.*;
import com.vedantu.subscription.viewobject.request.GamificationToolFilterRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/game")
public class GameController {

    @Autowired
    private GameManager gameManager;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    private Logger logger = LogFactory.getLogger(GameController.class);

    @RequestMapping(value = "/onboarding", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody

    public GameSetupResponse gameOnboarding(@RequestBody GameSetupRequest gameSetupRequest)
            throws VException {
        logger.info("entered gameOnboarding method...");
        return gameManager.gameOnboarding(gameSetupRequest);
    }

    @RequestMapping(value = "/getGameJourney/{userId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody

    public GameJourneyResponse getGameJourney(@PathVariable("userId") Long userId)
            throws VException {
        logger.info("entered getGameJourney");
        return gameManager.getGameJourneyResponse(userId);
    }

    @RequestMapping(value = "/getClaimedUsers", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<ClaimedUsersResponse> getGameJourney(@ModelAttribute GamificationToolFilterRequest req)
            throws VException {
        httpSessionUtils.checkIfAllowed(httpSessionUtils.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        logger.info("entered getClaimedUsers");
        logger.info("request params: start: " + req.toString());
        return gameManager.getClaimedUsers(req);
    }


    @RequestMapping(value = "/update/rewardstatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public RewardStatusChangeResponse updateRewardStatus(@RequestBody RewardStatusChangeRequest rewardStatusChangeRequest)
            throws VException {
        logger.info("updateRewardStatus");
        return gameManager.updateRewardStatus(rewardStatusChangeRequest);
    }

    @RequestMapping(value = "/bulkupdate/rewardstatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RewardStatusChangeResponse> bulkUpdateRewardStatus(@RequestBody List<RewardStatusChangeRequest> rewardStatusChangeRequests)
            throws VException {
        // takes care of csv uploaded in bulk
        logger.info("bulkUpdateRewardStatus");
        return gameManager.bulkUpdateAwb(rewardStatusChangeRequests);
    }

    @RequestMapping(value = "/template/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse createTemplate(@RequestBody GameTemplate template)
            throws VException {
        logger.info("Game template creation " + template);
        return gameManager.createGameTemplate(template);
    }

    @RequestMapping(value = "/validateTestAttempt", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public LMSTestInfo validateTestAttempt(@RequestBody LMSTestInfo lmsTestInfo) throws VException, CloneNotSupportedException, InterruptedException {
        logger.info("entered validateTestAttempt");

        gameManager.checkValidTest(lmsTestInfo);
        return lmsTestInfo;
    }

    @RequestMapping(value = "/session/status/{userId}", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public GameSessionStatusResponse getSessionStatus(@PathVariable("userId") Long userId,
                                                      @RequestParam("sessionId") String sessionId,
                                                      @RequestParam("endTime") Long endTime) throws VException, CloneNotSupportedException, InterruptedException {
        logger.info("entered getSessionStatus");
        return gameManager.getSessionStatus(userId, sessionId, endTime);
    }
}
