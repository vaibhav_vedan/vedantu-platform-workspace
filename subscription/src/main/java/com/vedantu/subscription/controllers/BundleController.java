package com.vedantu.subscription.controllers;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.request.UserData;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dinero.request.PayInstalmentReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.PayInstalmentRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.request.GetBundlesRequest;
import com.vedantu.lms.response.HomeFeedBundleResponse;
import com.vedantu.lms.response.MicroCourseCollectionResponse;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.BatchDashboardInfo;
import com.vedantu.onetofew.pojo.SessionPojoWithRefId;
import com.vedantu.onetofew.request.GetInstalmentReq;
import com.vedantu.subscription.dao.BundleDAO;
import com.vedantu.subscription.entities.mongo.AIOGroup;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.BundleTest;
import com.vedantu.subscription.entities.mongo.BundleVideo;
import com.vedantu.subscription.entities.mongo.SubscriptionUpdate;
import com.vedantu.subscription.managers.BundleManager;
import com.vedantu.subscription.managers.BundleTestManager;
import com.vedantu.subscription.managers.BundleVideoManager;
import com.vedantu.subscription.managers.PaymentManager;
import com.vedantu.subscription.managers.VideoEngagementManager;
import com.vedantu.subscription.pojo.BannerPojo;
import com.vedantu.subscription.pojo.BundleAggregation;
import com.vedantu.subscription.pojo.BundleInfo;
import com.vedantu.subscription.pojo.EnrolledBundlesRes;
import com.vedantu.subscription.pojo.GetEnrolledBundlesReq;
import com.vedantu.subscription.pojo.MicroCourseBasicInfo;
import com.vedantu.subscription.pojo.MicrocourseMobilePage;
import com.vedantu.subscription.pojo.MicrocourseMobileResp;
import com.vedantu.subscription.pojo.VideoDetails;
import com.vedantu.subscription.request.AIOGroupResp;
import com.vedantu.subscription.request.AddEditBundleReq;
import com.vedantu.subscription.request.AddEditBundleSessionReq;
import com.vedantu.subscription.request.AddEditBundleWebinarReq;
import com.vedantu.subscription.request.AddVideoAnalyticsDataReq;
import com.vedantu.subscription.request.BundleCreateEnrolmentReq;
import com.vedantu.subscription.request.BundleDetailsUpdateReq;
import com.vedantu.subscription.request.CheckBundleEnrollmentReq;
import com.vedantu.subscription.request.EditMultipleBundleReq;
import com.vedantu.subscription.request.GetAIOGroupReq;
import com.vedantu.subscription.request.GetBundleEnrolmentsReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.request.LandingPageCollectionResp;
import com.vedantu.subscription.request.MarkEnrolmentStateReq;
import com.vedantu.subscription.request.PremiumSubscriptionRequest;
import com.vedantu.subscription.request.UpdateAIOGroupReq;
import com.vedantu.subscription.request.UpdateBundleStateBulk;
import com.vedantu.subscription.request.UpdateStatusReq;
import com.vedantu.subscription.request.addSubscriptionProgramReq;
import com.vedantu.subscription.request.deEnrollSubscriptionBatch;
import com.vedantu.subscription.request.getMicroCoursesWithFilter;
import com.vedantu.subscription.response.BundleEnrollmentResp;
import com.vedantu.subscription.response.BundleEnrolmentInfo;
import com.vedantu.subscription.response.BundleWrapper;
import com.vedantu.subscription.response.FullSubscriptionPlanResp;
import com.vedantu.subscription.response.PremiumSubscriptionResp;
import com.vedantu.subscription.response.SubscriptionProgramResp;
import com.vedantu.subscription.response.SubscriptionUpdateResp;
import com.vedantu.subscription.response.TrialExpirationResp;
import com.vedantu.subscription.response.VideoInfo;
import com.vedantu.subscription.util.VzaarManager;
import com.vedantu.subscription.viewobject.request.BundleSearchReq;
import com.vedantu.subscription.viewobject.request.BundleTestReq;
import com.vedantu.subscription.viewobject.request.BundleVideoReq;
import com.vedantu.subscription.viewobject.request.ClickToEnrollReq;
import com.vedantu.subscription.viewobject.request.EndBundleEnrollmentReq;
import com.vedantu.subscription.viewobject.request.FreePassReq;
import com.vedantu.subscription.viewobject.request.GetBundleTestVideoReq;
import com.vedantu.subscription.viewobject.request.SubscriptionBundleRes;
import com.vedantu.subscription.viewobject.request.SubscriptionCoursesReq;
import com.vedantu.subscription.viewobject.request.SubscriptionCoursesRes;
import com.vedantu.subscription.viewobject.request.SubscriptionProgramReq;
import com.vedantu.subscription.viewobject.request.SubscriptionQueryReq;
import com.vedantu.subscription.viewobject.request.VideoEngagementReq;
import com.vedantu.subscription.viewobject.response.BundleTestRes;
import com.vedantu.subscription.viewobject.response.BundleVideoRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;


/**
 * Created by somil on 09/05/17.
 */
@RestController
@RequestMapping("/bundle")
public class BundleController {

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private VideoEngagementManager videoEngagementManager;

    @Autowired
    private BundleDAO bundleDAO;

    @Autowired
    private BundleTestManager bundleTestManager;

    @Autowired
    private BundleVideoManager bundleVideoManager;

    @Autowired
    private VzaarManager vzaarManager;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private DozerBeanMapper mapper;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BundleController.class);

    @RequestMapping(value = "/addEditBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleInfo addEditBundle(@RequestBody AddEditBundleReq addEditBundleReq)
            throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        addEditBundleReq.verify();
        return bundleManager.addEditBundle(addEditBundleReq);
    }

    @RequestMapping(value = "/addEditBundleWebinar", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleInfo addEditBundleWebinar(@RequestBody AddEditBundleWebinarReq addEditBundleWebinarReq)
            throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return bundleManager.addEditBundleWebinar(addEditBundleWebinarReq);
    }

    @RequestMapping(value = "/addEditBundleSession", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SessionPojoWithRefId addEditBundleSession(@RequestBody AddEditBundleSessionReq addEditBundleSessionReq)
            throws VException {
        return bundleManager.addEditBundleSession(addEditBundleSessionReq);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleInfo getBundle(@PathVariable("id") String id,
                                @ModelAttribute GetBundlesReq req) throws NotFoundException, VException {
        return bundleManager.getBundle(id, req);
    }

    @RequestMapping(value = "/getFullBundle/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleWrapper getBundle(@PathVariable("id") String id) throws NotFoundException, VException {
        Bundle bundle = bundleManager.getBundle(id);
        if (Objects.isNull(bundle)) {
                   throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "Can not find bundle with id : " + id);
                    }
        bundleManager.setBatchDetails(bundle);
        BundleWrapper bundleWrapper = bundleManager.bundleToBundleWrapper(bundle);
        String messageStrip = bundleManager.getBundleMessageStrip("STRIP_COURSE_DETAIL");
        String messageStripUrl = bundleManager.getBundleMessageStrip("COURSE_DETAIL_URL");
        bundleWrapper.setMessageStrip(messageStrip);
        bundleWrapper.setMessageStripUrl(messageStripUrl);
        return bundleWrapper;
    }

    @RequestMapping(value = "/get", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleInfo> getBundleInfos(@ModelAttribute GetBundlesReq req) throws VException {
        return bundleManager.getBundleInfos(req);
    }

    @RequestMapping(value = "/get/public", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleInfo> getPublicBundleInfos(@ModelAttribute GetBundlesReq req) throws VException {
        return bundleManager.getPublicBundleInfos(req);
    }

    @RequestMapping(value = "/createEnrolment", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleEnrolment createEnrolment(@RequestBody BundleCreateEnrolmentReq req) throws VException {
        return bundleManager.createEnrolment(req);
    }

    @RequestMapping(value = "/getVideoDetails/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public VideoDetails getVideoDetails(@PathVariable("id") Long id) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return vzaarManager.getVideoDetails(id);
    }

    @RequestMapping(value = "/getEnrolledBundles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrollmentResp> getEnrolledBundles(@RequestParam(value = "userId") String userId) throws VException {
        sessionUtils.checkIfAllowed(Long.parseLong(userId), null, true);
        return bundleManager.getEnrolledBundles(userId);
    }

    @RequestMapping(value = "/getEnrolments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrolmentInfo> getEnrolments(@ModelAttribute GetBundleEnrolmentsReq req) throws VException {
        logger.info("request " + req);
        return bundleManager.getEnrolments(req);
    }

    @RequestMapping(value = "/getBundleInfoForOnboarding", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleInfo getBundleInfoForOnboarding(@RequestParam(value = "userId") String userId, @RequestParam(value = "bundleId") String bundleId) throws VException {
        return bundleManager.getBundleInfoForOnboarding(userId, bundleId);
    }

    @RequestMapping(value = "/getActiveEnrolments", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrolmentInfo> getActiveEnrolments(@RequestBody GetBundleEnrolmentsReq req) throws VException {

        logger.info("request " + req);
        return bundleManager.getEnrolments(req);
    }

    @RequestMapping(value = "/checkEnrolment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrolmentInfo> checkEnrolment(@ModelAttribute CheckBundleEnrollmentReq checkBundleEnrollmentReq) throws VException {
        return bundleManager.checkEnrolment(checkBundleEnrollmentReq);
    }

    @RequestMapping(value = "/hasEnrolment", method = RequestMethod.GET)
    @ResponseBody
    public Boolean hasEnrolment(@ModelAttribute CheckBundleEnrollmentReq checkBundleEnrollmentReq) throws VException {
        return bundleManager.hasEnrollment(checkBundleEnrollmentReq);
    }

    @RequestMapping(value = "/addVideoAnalyticsData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addVideoAnalyticsData(@RequestBody AddVideoAnalyticsDataReq addVideoAnalyticsDataReq) throws VException {
        return bundleManager.addVideoAnalyticsData(addVideoAnalyticsDataReq);
    }

    @RequestMapping(value = "/getVideoViewInfoFromAnalytics", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<VideoInfo> getVideoViewInfoFromAnalytics(@RequestBody List<VideoInfo> videoInfosNotFoundInRedis) {
        return bundleManager.getVideoViewInfoFromAnalytics(videoInfosNotFoundInRedis);
    }

    @RequestMapping(value = "/markEnrolmentStateForTrials", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse markEnrolmentStateForTrials(@RequestBody MarkEnrolmentStateReq req) throws VException {
        return bundleManager.markEnrolmentStateForTrials(req);
    }

    @RequestMapping(value = "/get/getPublicBundleInfosForBundleIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleInfo> getPublicBundleInfosForBundleIds(@RequestParam(value = "bundleIds") List<String> bundleIds) throws VException {
        return bundleManager.getPublicBundleInfosForBundleIds(bundleIds);
    }

    //instalment related apis
    @RequestMapping(value = "/getInstalments", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<InstalmentInfo> getInstalments(GetInstalmentReq req) throws VException {
        return bundleManager.getInstalments(req);
    }

    @RequestMapping(value = "/processInstalmentAfterPayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void processInstalmentAfterPayment(@RequestBody InstalmentInfo req) throws VException, CloneNotSupportedException {
        bundleManager.processInstalmentAfterPayment(req);
    }

    @RequestMapping(value = "payInstalmentForBundle", method = RequestMethod.POST)
    @ResponseBody
    public PayInstalmentRes payInstalmentForBundle(@RequestBody PayInstalmentReq req) throws VException, IOException {
        sessionUtils.checkIfAllowedList(req.getUserId(), null, Boolean.TRUE);
        req.setNewFlow(Boolean.TRUE);
        return paymentManager.payInstalment(req);
    }

    @RequestMapping(value = "/autoEnrollInBundleCourses", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse autoEnrollInBundleCourses(@RequestBody Map<String, String> requestParams) throws Throwable {
        bundleManager.autoEnrollInBundleCourses(requestParams.get("bundleId"), requestParams.get("userId"), requestParams.get("enrollmentId"));
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/endBundleEnrollment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse endBundleEnrollment(@RequestBody EndBundleEnrollmentReq req) throws Throwable {
        //throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "End subscription not allowed");
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        return bundleManager.endBundleEnrollment(req, sessionData.getUserId());
    }

    @RequestMapping(value = "/updateStatus", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateStatus(@RequestBody UpdateStatusReq req) throws VException {
        logger.info(" data : -  " + req.getStatus() + "   __  " + req.getBundleEnrollmentId());
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        bundleManager.markBundleActiveInActive(req);
    }

    @RequestMapping(value = "/addVideo", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addVideo(@RequestBody BundleVideoReq bundleVideos) throws Throwable {
        sessionUtils.checkIfAllowed(bundleVideos.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        for (BundleVideo bundleVideo : bundleVideos.getBundleVideo()) {
            bundleVideoManager.setBundleVideo(bundleVideo);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getBundleVideos", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleVideo> getBundleVideos(@RequestBody GetBundleTestVideoReq getBundleTestVideoReq) throws Throwable {
        sessionUtils.checkIfAllowed(getBundleTestVideoReq.getCallingUserId(), Role.ADMIN, Boolean.TRUE);

        return bundleVideoManager.getBundleVideo(getBundleTestVideoReq);
    }

    @RequestMapping(value = "/addTest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addTest(@RequestBody BundleTestReq bundleTests) throws Throwable {

        sessionUtils.checkIfAllowed(bundleTests.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        for (BundleTest bundleTest : bundleTests.getBundleTest()) {

            bundleTestManager.setBundleTest(bundleTest);
        }
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getBundleTest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleTest> getBundleTest(@RequestBody GetBundleTestVideoReq getBundleTestVideoReq) throws Throwable {

        sessionUtils.checkIfAllowed(getBundleTestVideoReq.getCallingUserId(), Role.ADMIN, Boolean.TRUE);

        return bundleTestManager.getBundleTest(getBundleTestVideoReq);
    }

    @RequestMapping(value = "/enrollFreepass", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void enrollFreepass(@RequestBody FreePassReq req) throws VException {
        req.verify();
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        //sessionUtils.checkIfAllowed(req.getUserId(), Role.STUDENT, Boolean.FALSE);
        bundleManager.enrollWithFreePass(req);
    }

    @RequestMapping(value = "/terminateFreePass", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void terminateFreePass(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
                                  @RequestBody String request) throws VException {
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info("ClientResponse : " + resp.toString());
        } else if (messgaetype.equals("Notification")) {
            bundleManager.terminateFreePassCreateAsync();
        }
    }

    @RequestMapping(value = "/updateStatusBybundleId", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateStatusBybundleId(@RequestBody UpdateStatusReq req) throws VException {
        bundleManager.updateStatusBybundleId(req);
    }

//    @RequestMapping(value = "/endBundleEnrollmentCron", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public void endBundleEnrollmentCron(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
//                                        @RequestBody String request) throws Throwable {
//        Gson gson = new Gson();
//        gson.toJson(request);
//        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
//        if (messgaetype.equals("SubscriptionConfirmation")) {
//            String json = null;
//            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
//            logger.info("ClientResponse : " + resp.toString());
//        } else if (messgaetype.equals("Notification")) {
//            bundleManager.endBundleEnrollmentCron();
//        }
//    }

    @RequestMapping(value = "/getTestByBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleTestRes getTestByBundle(@RequestBody GetBundleTestVideoReq getBundleTestVideoReq) throws Throwable {

        return bundleTestManager.getTestByBundle(getBundleTestVideoReq);
    }

    @RequestMapping(value = "/getVideoByBundle", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleVideoRes getVideoByBundle(@RequestBody GetBundleTestVideoReq getBundleTestVideoReq) throws Throwable {

        return bundleVideoManager.getVideoByBundle(getBundleTestVideoReq);
    }

    @RequestMapping(value = "/getCourseByBundle", method = RequestMethod.GET)
    @ResponseBody
    public List<BatchDashboardInfo> getCourseByBundle(@ModelAttribute GetBundleTestVideoReq req) throws Throwable {
        return bundleManager.getCourseByBundle(req);
    }

    @RequestMapping(value = "/getBundles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleInfo> getBundles(@RequestBody GetBundlesReq req) throws VException {
        return bundleManager.getBundleDataForBundleSuggestions(req.getBundleIds());
    }

    @RequestMapping(value = "/getBundlesNames", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleInfo> getBundlesNames(@RequestBody GetBundlesReq req) throws VException {

        return bundleManager.getBundlesNames(req);
    }

    @RequestMapping(value = "/trackToAioDataMigration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void trackToAioDataMigration(@RequestBody FreePassReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.ADMIN, Boolean.TRUE);
        bundleManager.trackToAioDataMigration(req);
    }

    @RequestMapping(value = "/getTestPostLogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleTestRes getTestPostLogin(@RequestBody GetBundleTestVideoReq getBundleTestVideoReq) throws VException {
        return bundleManager.getTestPostLogin(getBundleTestVideoReq);
    }

    @RequestMapping(value = "/getVideoPostLogin", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BundleVideoRes getVideoPostLogin(@RequestBody GetBundleTestVideoReq getBundleTestVideoReq) throws VException {
        return bundleManager.getVideoPostLogin(getBundleTestVideoReq);
    }

    @RequestMapping(value = "/migrateEnrollmentData", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void migrateEnrollmentData() throws VException {
        bundleManager.migrateEnrollmentData();
    }

    @RequestMapping(value = "/clickToEnroll", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void clickToEnroll(@RequestBody ClickToEnrollReq req) throws VException {
        bundleManager.clickToEnroll(req);
    }






    @RequestMapping(value = "/setVideoEngagement", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void setVideoEngagement(@RequestBody VideoEngagementReq req) throws VException {
        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.STUDENT, Boolean.FALSE);
        videoEngagementManager.setVideoEngagement(req);
    }

    @RequestMapping(value = "/v1/search", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Bundle> searchBundle(@RequestBody BundleSearchReq req) {
        return bundleManager.searchBundle(req);
    }

    @RequestMapping(value = "/update/aio/bundle-details", method = RequestMethod.POST)
    public PlatformBasicResponse updateAIOBundleTagsDetails(@RequestBody BundleDetailsUpdateReq req) throws IOException, VException {
        return bundleManager.updateAIOBundleTagsDetails(req);
    }

    @RequestMapping(value = "/microcourse/group/display-tags", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BundleAggregation> groupBundlesByDisplayTags(@RequestBody BundleSearchReq req) {
        if (ArrayUtils.isEmpty(req.getCourseTerms())) {
            ArrayList<CourseTerm> courseTerms = new ArrayList<>();
            courseTerms.add(CourseTerm.MICRO_COURSES);
            req.setCourseTerms(courseTerms);
        }
        return bundleManager.groupBundlesByDisplayTags(req);
    }

    @RequestMapping(value = "/microcourse/subscription/packages", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Bundle> getMicroCourseSubscriptionsPackages(@RequestBody BundleSearchReq req) {
        if (ArrayUtils.isEmpty(req.getCourseTerms())) {
            ArrayList<CourseTerm> courseTerms = new ArrayList<>();
            courseTerms.add(CourseTerm.MICRO_COURSES);
            req.setCourseTerms(courseTerms);
        }
        return bundleManager.getMicroCourseSubscriptionsPackages(req);
    }

    @RequestMapping(value = "/microcourse/mobile/display-tags", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MicrocourseMobileResp groupBundlesMobileByDisplayTags(@RequestBody BundleSearchReq req) {
        Long userId = sessionUtils.getCallingUserId();

        if (userId != null) {
            UserBasicInfo user = fosUtils.getUserBasicInfo(userId, false);
            if (user != null && StringUtils.isNotEmpty(user.getGrade())) {
                ArrayList<Integer> grades = new ArrayList<>();

                grades.add(Integer.parseInt(user.getGrade()));
                req.setGrades(grades);
            }
        }

        if (ArrayUtils.isEmpty(req.getCourseTerms())) {
            ArrayList<CourseTerm> courseTerms = new ArrayList<>();
            courseTerms.add(CourseTerm.MICRO_COURSES);
            req.setCourseTerms(courseTerms);
        }
        List<BundleAggregation> list = bundleManager.groupBundlesByDisplayTags(req);
        List<BannerPojo> banner = bundleManager.getMicroCourseBannerPojoFromRedisMobile();
        String messageStrip = bundleManager.getBundleMessageStrip("STRIP_COURSE_LANDING");
        String messageStripUrl = bundleManager.getBundleMessageStrip("COURSE_LANDING_URL");
        MicrocourseMobileResp resp = new MicrocourseMobileResp();
        resp.setList(list);
        resp.setBanner(banner);
        resp.setMessageStrip(messageStrip);
        resp.setMessageStripUrl(messageStripUrl);
        return resp;
    }

    @RequestMapping(value = "/microcourse/mobile/page", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public MicrocourseMobilePage microCourseMobilePage() throws VException {
        Long callingUserId = sessionUtils.getCallingUserId();
        MicrocourseMobilePage page = new MicrocourseMobilePage();
        if (callingUserId != null) {
            sessionUtils.checkIfAllowed(callingUserId, Role.STUDENT, Boolean.TRUE);
            String FOS_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("FOS_ENDPOINT");

            long mod = callingUserId % 10;

            switch ((int) mod) {
                case 1:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses");
                    break;
                case 2:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_intrested");
                    break;
                case 3:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_intrested");
                    break;
                case 4:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_masterteacher");
                    break;
                case 5:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_masterteacher");
                    break;
                case 6:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_price");
                    break;
                case 7:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_classes");
                    break;
                case 8:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_classes");
                    break;
                case 9:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_tag");
                    break;
                case 0:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses_tag");
                    break;
                default:
                    page.setUrl(FOS_ENDPOINT + "mobile/micro_courses");
                    break;
            }
            page.setId("1");
        }

        return page;
    }

    @RequestMapping(value = "/getDuplicateEnrollment", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrolment> getDuplicateEnrollment() throws VException {

        return bundleManager.getDuplicateEnrollment();
    }

    @RequestMapping(value = "/getSubscriptionCourses", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SubscriptionCoursesRes> getSubscriptionCourses(@ModelAttribute SubscriptionCoursesReq req) throws VException {
        req.verify();
        return bundleManager.getSubscriptionCourses(req);
    }

    @Deprecated
    @RequestMapping(value = "/addPackagesInSubscription", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addPackagesInBundle(@RequestParam(name = "bundleId", required = true) String bundleId,
                                    @RequestParam(name = "secretKey", required = true) String secretKey) throws ForbiddenException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || !("uuZByTsaZHdJLwE5W9SxyEu4UWPQ3rhvt8H123".equals(secretKey))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not LoggedIn or invalid secret key");
        }
        bundleManager.addPackagesInBundle(bundleId);
    }

    @Deprecated
    @RequestMapping(value = "/correctPackageTypeInSubscription", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void correctPackageTypeInSubscription(@RequestParam(name = "batchId", required = true) String batchId,
                                                 @RequestParam(name = "secretKey", required = true) String secretKey) throws VException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || !("uuZByTsaZHdJLwE5W9SxyEu4UWPQ3rhvt8H123".equals(secretKey))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not LoggedIn or invalid secret key");
        }
        bundleManager.correctPackageTypeInSubscription(batchId);
    }

    @RequestMapping(value = "/getSubscriptionBundleInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SubscriptionBundleRes> getSubscriptionBundleInfo(@RequestParam(name = "userId", required = true) String userId) throws VException {
        return bundleManager.getSubscriptionBundleInfo(userId);
    }

    @RequestMapping(value = "/getBundlesForMobileHomeFeed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<HomeFeedBundleResponse> getBundlesForMobileHomeFeed(@RequestBody GetBundlesRequest getBundlesRequest) throws VException {
        return bundleManager.getBundlesForMobileHomeFeed(getBundlesRequest);
    }

    @RequestMapping(value = "/getMicroCourseCollectionForHomeFeed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<MicroCourseCollectionResponse> getMicroCourseCollectionForHomeFeed(@RequestBody GetBundlesRequest getBundlesRequest) throws VException {
        return bundleManager.getMicroCourseCollectionForHomeFeed(getBundlesRequest);
    }

    @RequestMapping(value = "/getGroups", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<LandingPageCollectionResp> getGroups(GetAIOGroupReq req) throws VException {

        return bundleManager.getGroups(req);
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateGroup(@RequestBody UpdateAIOGroupReq req) throws VException {
        bundleManager.updateGroups(req);
    }

    @RequestMapping(value = "/getMicroCoursesWithFilter", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AIOGroupResp> getMicroCoursesWithFilter(@RequestBody getMicroCoursesWithFilter req) throws VException {
        req.verify();
        return bundleManager.getMicroCoursesWithFilter(req);
    }

    @RequestMapping(value = "/getGroupsByGrade", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AIOGroup> getGroupsByGrade(@ModelAttribute GetAIOGroupReq req) throws VException {
        return bundleManager.getGroupsByGrade(req);
    }

    @RequestMapping(value = "/getGroupsNameHeading", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AIOGroup> getGroupsNameHeading(@ModelAttribute GetAIOGroupReq req) throws VException {
        return bundleManager.getGroupsNameHeading(req);
    }

    /**
     * Do expose this API for public, this is for internal use only.
     *
     * @param req
     * @param secretKey
     * @return
     * @throws com.vedantu.exception.VException
     */
    @Deprecated
    @RequestMapping(value = "/postProcessInstalmentAfterPayment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse postProcessInstalmentAfterPayment(@RequestBody InstalmentInfo req,
                                                                   @RequestParam(name = "secretKey", required = true) String secretKey) throws VException {
        HttpSessionData sessionData = httpSessionUtils.getCurrentSessionData();
        if (sessionData == null || !("uuZByTsaZHdJLwE5W9SxyEu4UWPQ3rhvt8H".equals(secretKey))) {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "Not LoggedIn or invalid secret key");
        }
        bundleManager.postProcessInstalmentAfterPayment(req);
        return new PlatformBasicResponse();
    }

    @RequestMapping(value = "/getActiveBundleEnrolmentsForUsers", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BundleEnrolmentInfo> getActiveBundleEnrolmentsForUsers(@RequestBody GetBundleEnrolmentsReq req) throws VException {

        logger.info("request " + req);
        return bundleManager.getActiveBundleEnrolmentsForUsers(req);
    }

    @RequestMapping(value = "/getEnrolledBundlesHomeFeed", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EnrolledBundlesRes getEnrolledBundles(@RequestBody GetEnrolledBundlesReq getEnrolledBundlesReq) {
        return bundleManager.getEnrolledBundlesHomeFeed(getEnrolledBundlesReq.getBundleIds(), getEnrolledBundlesReq.getUserId());
    }

    @RequestMapping(value = "/editMultipleBundles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse editMultipleBundles(@RequestBody EditMultipleBundleReq req) throws VException {

        logger.info("request " + req);
        return bundleManager.editMultipleBundles(req);
    }

    @RequestMapping(value = "/getMicroCourseByBundle", method = RequestMethod.GET)
    @ResponseBody
    public MicroCourseBasicInfo getMicroCourseByBundle(@ModelAttribute GetBundleTestVideoReq req) throws Throwable {
        return bundleManager.getMicroCourseByBundle(req);
    }

    @RequestMapping(value = "/deEnrollSubscriptionBatch", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse deEnrollSubscriptionBatch(@RequestBody deEnrollSubscriptionBatch req) throws VException {

        logger.info("request " + req);
        return bundleManager.deEnrollSubscriptionBatch(req);
    }


    @RequestMapping(value = "/getSimilarBatchOfSubscription", method = RequestMethod.GET)
    @ResponseBody
    public List<SubscriptionCoursesRes> getSimilarBatchOfSubscription(@RequestParam(name = "userId") String userId, @RequestParam(name = "courseId") String courseId) throws Throwable {
        return bundleManager.getSimilarBatchOfSubscription(userId, courseId);
    }


    @RequestMapping(value = "/addUserIdToBundleMinMaxExceptionList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse addUserIdToBundleMinMaxExceptionList(@RequestParam(value = "userId", required = true) Long userId) throws Throwable {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundleManager.addUserIdToBundleMinMaxExceptionList(userId);
    }

    @RequestMapping(value = "/isUserPartOfMinMaxExceptionList", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse isUserPartOfMinMaxExceptionList(@RequestParam(value = "userId", required = true) Long userId) throws Throwable {
        return bundleManager.isUserPartOfMinMaxExceptionList(userId);
    }

    @RequestMapping(value = "/deleteUserFromBundleMinMaxExceptionList", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse deleteUserFromBundleMinMaxExceptionList(@RequestParam(value = "userId", required = true) Long userId) throws Throwable {
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return bundleManager.deleteUserFromBundleMinMaxExceptionList(userId);
    }

    @RequestMapping(value = "/updateBundleStateBulk", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void updateBundleStateBulk(@RequestBody UpdateBundleStateBulk req) throws Throwable {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        bundleManager.updateBundleStateBulk(req);
    }

    @RequestMapping(value = "/saveSubscriptionProgram", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void saveSubscriptionProgram(@RequestBody addSubscriptionProgramReq req) throws Throwable {
        req.verify();
        httpSessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        bundleManager.saveSubscriptionProgram(req);
    }

    @RequestMapping(value = "/getSubscriptionProgram", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionProgramResp getSubscriptionProgram( @ModelAttribute SubscriptionProgramReq req) throws Throwable {

        return bundleManager.getSubscriptionProgram(req);
    }


    @RequestMapping(value = "/getSubscriptionProgramForGrade", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getSubscriptionProgramForGrade(@RequestParam(value = "grade", required = true) Integer grade) throws Throwable {

        return bundleManager.getSubscriptionProgramForGrade(grade);
    }

    @RequestMapping(value = "/hasLongTermEnrollment", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public boolean hasLongTermEnrollment(@RequestParam(value = "userId", required = true) String userId) throws Throwable {

        return bundleManager.hasLongTermEnrollment(userId);
    }

    @RequestMapping(value = "/migrateBundleDataForSegregation", method = RequestMethod.GET)
    @ResponseBody
    public void migrateBundleDataForSegregation() {
        bundleManager.migrateBundleDataForSegregation();
    }

    @RequestMapping(value = "/isBatchPartOfSubscriptionAIO", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean isBatchPartOfSubscriptionAIO(@RequestParam(value = "userId", required = true) String userId,@RequestParam(value = "batchId",required = true) String batchId) throws VException {
        // API restriction
        List<Role> allowedRoles = new ArrayList<>();
        allowedRoles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(Long.parseLong(userId),allowedRoles,false);
        return bundleManager.isBatchPartOfSubscriptionAIO(userId,batchId);
    }
    //    @RequestMapping(value = "/updateSubscription", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public boolean updateSubscription(@RequestBody updateSubscription req) throws Throwable {
//
//        return bundleManager.updateSubscription(    req);
//    }

    @RequestMapping(value = "/getFullSubscriptionPlan", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public FullSubscriptionPlanResp getFullSubscriptionPlan(@RequestParam(value = "grade", required = true) Integer grade, @RequestParam(value = "target", required = true) String target, @RequestParam(value = "year", required = true) Integer year) throws Throwable {

        return bundleManager.getFullSubscriptionPlan(  grade, target, year);
    }


    @RequestMapping(value = "/inActiveSubscriptionPlan", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void inActiveSubscriptionPlan(@RequestHeader(value = "x-amz-sns-message-type") String messgaetype,
                                         @RequestBody String request) throws Throwable {
        Gson gson = new Gson();
        gson.toJson(request);
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messgaetype.equals("SubscriptionConfirmation")) {
            String json = null;
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info("ClientResponse : " + resp.toString());
        } else if (messgaetype.equals("Notification")) {

            bundleManager.inActiveSubscriptionPlan();
        }

    }

    @RequestMapping(value = "/trial/enrollment/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public TrialExpirationResp getTrialEnrollmentData(@PathVariable(value = "id") String userId) throws Throwable {
        return bundleManager.getTrialEnrollmentData(userId);

    }


    @RequestMapping(value = "/subscriptionQueryMessage", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse subscriptionQueryMessage(@RequestBody SubscriptionQueryReq req) throws VException, UnsupportedEncodingException {


        bundleManager.subscriptionQueryMessage(req);
        return new PlatformBasicResponse();
    }

//    @RequestMapping(value = "/trigetTrialTerminate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public void trigetTrialTerminate()
//            throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
//        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
//       bundleManager.terminateFreePassCreateAsync();
//    }

    @RequestMapping(value = "/migrateTrialToRegular", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void migrateTrialToRegular(@RequestParam(value = "oldEnrollmentId")String oldEnrollmentId , @RequestParam(value = "newEnrollmentId")String newEnrollmentId)
            throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        bundleManager.migrateTrialToRegular(oldEnrollmentId,newEnrollmentId);
    }

    @RequestMapping(value = "/isValidBundle", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Boolean isValidBundle(@RequestParam(name="id", required = true) String id) throws VException{
    	return bundleManager.isValidBundle(id);
    }

    @RequestMapping(value = "/isPremiumSubscriber", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PremiumSubscriptionResp isPremiumSubscriber(@ModelAttribute UserData userData) throws VException{
    	//sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.FALSE);
    	return bundleManager.isPremiumSubscriber(userData.getUserId(), userData.getGrade(), userData.getBoard(), userData.getStream(), userData.getTarget());
    }

    @RequestMapping(value = "/enrollNewUserToPremiumBundles", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse enrollNewUserToPremiumBundles(@RequestParam(name = "userId", required = true) String userId,
    		@RequestBody PremiumSubscriptionRequest premiumSubscriptionRequest) throws VException{
    	if(null == premiumSubscriptionRequest || StringUtils.isEmpty(premiumSubscriptionRequest.getGrade()))
        	throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Grade cannot be null or empty");
    	if(StringUtils.isEmpty(userId)) {
    		userId = sessionUtils.getCallingUserId().toString();
    	}
    	logger.info("Received request for auto enrolling app user to premium bundles");
    	return bundleManager.enrollNewUserToPremiumBundles(userId, premiumSubscriptionRequest);
    }

    @RequestMapping(value = "/getRechargeDetail", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<SubscriptionUpdateResp> getRechargeDetail(@RequestParam(value = "bundleEnrollmentId", required = true) String bundleEnrollmentId)
            throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return bundleManager.getRechargeDetail(bundleEnrollmentId);
    }

    @RequestMapping(value = "/getSubscriptionUpdateById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SubscriptionUpdate getSubscriptionUpdateById(@RequestParam(value = "subscriptionUpdateId", required = true) String subscriptionUpdateId)
            throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return bundleManager.getSubscriptionUpdateById(subscriptionUpdateId);
    }
}




