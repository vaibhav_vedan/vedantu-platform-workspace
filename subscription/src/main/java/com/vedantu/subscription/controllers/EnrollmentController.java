package com.vedantu.subscription.controllers;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.subscription.listeners.SNSSubscriptionHandler;
import com.vedantu.subscription.response.EnrolledCoursesHomepage;
import com.vedantu.util.WebUtils;
import com.vedantu.util.enums.SNSSubject;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.async.AsyncTaskFactory;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.UnauthorizedException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.CurriculumEntityName;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.ActiveUserIdsByBatchIdsResp;
import com.vedantu.onetofew.pojo.CSVEnrollmentReq;
import com.vedantu.onetofew.pojo.CSVEnrollmentRes;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.GetUserIdEnrollmentBatchMapReq;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.onetofew.request.GetBatchesForDashboardReq;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.subscription.async.AsyncTaskName;
import com.vedantu.subscription.dao.EnrollmentDAO;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.managers.BatchSnapshotOTMManager;
import com.vedantu.subscription.managers.EnrollmentAsyncManager;
import com.vedantu.subscription.managers.EnrollmentConsumptionManager;
import com.vedantu.subscription.managers.EnrollmentManager;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.subscription.request.EnrollmentHomePageRequest;
import com.vedantu.subscription.request.RegPaymentOTFCourseReq;
import com.vedantu.subscription.request.UserPremiumSubscriptionEnrollmentReq;
import com.vedantu.subscription.response.ActiveStudentsInBatchesResp;
import com.vedantu.subscription.response.BatchBundlePojo;
import com.vedantu.subscription.response.EnrollmentHomePageResponse;
import com.vedantu.subscription.response.section.ActiveTAsForBatchRes;
import com.vedantu.subscription.viewobject.request.BatchChangeReq;
import com.vedantu.subscription.viewobject.request.CreateConsumptionTransactionReq;
import com.vedantu.subscription.viewobject.request.EnrollmentRefundReq;
import com.vedantu.subscription.viewobject.request.MakePaymentRequest;
import com.vedantu.subscription.viewobject.request.MigrationRequest;
import com.vedantu.subscription.viewobject.response.EnrollmentCountForUserBatchRes;
import com.vedantu.subscription.viewobject.response.GetUserDashboardEnrollmentsRes;
import com.vedantu.subscription.viewobject.response.OTFEntityEnrollmentResp;
import com.vedantu.subscription.viewobject.response.UserBatchInfoRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.request.ActiveUserIdsByBatchIdsReq;
import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.request.GetUserEnrollmentsReq;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;

import io.swagger.annotations.ApiOperation;


@RestController
@RequestMapping("enroll")
public class EnrollmentController {

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private EnrollmentConsumptionManager enrollmentConsumptionManager;

    @Autowired
    private EnrollmentDAO enrollmentDAO;

    @Autowired
    private BatchSnapshotOTMManager batchSnapshotOTMManager;

    @Autowired
    private EnrollmentAsyncManager enrollmentAsyncManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private AsyncTaskFactory asyncTaskFactory;

    @Autowired
    private SNSSubscriptionHandler snsSubscriptionHandler;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EnrollmentController.class);

    //TODO admin check 
    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public EnrollmentPojo enrollment(@RequestBody Enrollment enrollment)
            throws IllegalAccessException, ConflictException, NotFoundException, BadRequestException, ForbiddenException {
        logger.info(" enrollment req " + enrollment.toString());
        List<String> errors = validate(enrollment);
        if (!errors.isEmpty()) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errors.toString());
        }
        boolean createdNew = enrollmentManager.updateEnrollment(enrollment, null);
        EnrollmentPojo enrollmentPojo = enrollmentManager.createEnrollmentInfo(enrollment, true);
        enrollmentPojo.setCreatedNew(createdNew);
        return enrollmentPojo;
    }

    //TODO admin check 
    @RequestMapping(value = "/markStatus", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public EnrollmentPojo markStatus(@RequestBody OTFEnrollmentReq enrollment) throws
            IllegalAccessException, BadRequestException, ForbiddenException {
        List<String> errors = validate(enrollment);
        if (ArrayUtils.isNotEmpty(errors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errors.toString());
        }
        Enrollment enrollmentRes = enrollmentManager.markStatus(enrollment);
        return enrollmentManager.createEnrollmentInfo(enrollmentRes, true);
    }

    @RequestMapping(value = "/enrollFromCsv", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public List<CSVEnrollmentRes> enrollFromCSV(@RequestBody CSVEnrollmentReq csvEnrollmentReq) throws
            IllegalAccessException, VException {
        csvEnrollmentReq.validate();
        List<CSVEnrollmentRes> enrollmentRes = enrollmentManager.enrollFromCSV(csvEnrollmentReq.getCsvEnrollmentPojoList());
        return enrollmentRes;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getEnrollments(@RequestParam("userId") String userId , @RequestParam(value= "excludeBatchCourseInfos", required = false) boolean excludeBatchCourseInfos) {
        logger.info("getEnrollment req with id" + userId);
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        List<Enrollment> enrollments = enrollmentManager.getAllEnrollment(userId);
        List<EnrollmentPojo> enrollmentPojos = enrollmentManager.createEnrollmentInfos(enrollments, !excludeBatchCourseInfos, exposeEmail);
        //TO DO : Replace Role check with flag recive from UI
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (Role.ADMIN.equals(sessionData.getRole()) || Role.STUDENT_CARE.equals(sessionData.getRole())) {
            return enrollmentManager.getRefundAndConsumption(enrollmentPojos);
        } else {
            return enrollmentPojos;
        }
    }

    @RequestMapping(value = "/getAllSubjectsForActiveEnrollments", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getAllSubjectsForActiveEnrollments(@RequestParam(value = "userId") String userId) throws BadRequestException {
        logger.info("Get subjects for active enrollments for user {}", userId);
        Long callingUserId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(Role.STUDENT.equals(role) && (Objects.isNull(callingUserId) || !callingUserId.equals(Long.parseLong(userId)))){
            throw new BadRequestException(ErrorCode.ACCESS_NOT_ALLOWED,callingUserId+" is not allowed to access "+userId+"'s data");
        }
        return enrollmentManager.getAllSubjectsForActiveEnrollments(userId);
    }

    @RequestMapping(value = "/getUserIdEnrollmentBatchMapViaPOST", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public HashMap<String, List<String>> getUserIdEnrollmentBatchMapViaPOST(@RequestBody GetUserIdEnrollmentBatchMapReq req) {
        return enrollmentManager.getUserIdEnrollmentBatchMap(req.getUserIds(), req.getBatchIds());
    }

    @RequestMapping(value = "/getEnrollmentForBatch", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public EnrollmentPojo getEnrollmentForBatch(@RequestParam("userId") String userId,
                                                @RequestParam("batchId") String batchId) throws NotFoundException {
        List<Enrollment> enrollments = enrollmentManager.getEnrollment(userId, null, batchId, null);
        List<EnrollmentPojo> enrollmentPojos = enrollmentManager.createEnrollmentInfos(enrollments, true, false);
        if (enrollmentPojos.size() > 1) {
            logger.error("Multiple active enrollments found for batchId " + batchId + ", userId " + userId);
        }
        if (enrollmentPojos.isEmpty()) {
            throw new NotFoundException(ErrorCode.BATCH_ENROLLMENT_NOT_FOUND, "no enrollemnt found");
        }
        return enrollmentPojos.get(0);
    }

    @RequestMapping(value = "/getEnrollmentForBatches", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getEnrollmentForBatches(@RequestParam("userId") String userId,
                                                @RequestParam("batchIds") Set<String> batchIds) throws NotFoundException {
        logger.info("getEnrollmentForBatches user id {} , batch ids {}", userId, String.join(",", batchIds));
        List<Enrollment> enrollments = enrollmentManager.getEnrollmentsForBatches(userId, null, batchIds, null);
        return enrollmentManager.createEnrollmentInfos(enrollments, true, false);
    }

    @RequestMapping(value = "/enrollments", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getEnrollments(GetEnrollmentsReq req) throws BadRequestException {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        List<EnrollmentPojo> enrollmentInfos = enrollmentManager.getEnrollments(req, exposeEmail);
        return enrollmentInfos;
    }

    //TODO add admin check
    @RequestMapping(value = "/getEnrollmentById", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public EnrollmentPojo getEnrollmentById(@RequestParam("id") String id) throws BadRequestException {
        logger.info("getEnrollmentById req with id" + id);
        Enrollment enrollment = enrollmentManager.getEnrollmentById(id);
        if (enrollment != null) {
            return enrollmentManager.createSimplifiedEnrollmentPojo(enrollment);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "No enrollments found");
        }
    }

    @RequestMapping(value = "/getChangeTimeForContentShare", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public EnrollmentPojo getChangeTimeForContentShare(@RequestParam("id") String id) throws BadRequestException, NotFoundException {
        logger.info("getChangeTimeForContentShare req with id" + id);
        return enrollmentManager.getChangeTimeForContentShare(id);
    }

    @RequestMapping(value = "/getEnrollmentsByEnrollmentIds", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getEnrollmentsByEnrollmentIds(GetEnrollmentsReq req) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = enrollmentManager.getEnrollmentsByEnrollmentIds(req);
        return enrollmentInfos;
    }

    @RequestMapping(value = "/getEnrollmentsDataForSessionStrip", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getEnrollmentsDataForSessionStrip(GetEnrollmentsReq req) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = enrollmentManager.getEnrollmentsDataForSessionStrip(req);
        return enrollmentInfos;
    }

    @RequestMapping(value = "/getBatchIdsOfActiveEnrollmentsOfUser", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getBatchIdsOfActiveEnrollmentsOfUser(GetUserEnrollmentsReq req) throws BadRequestException {
        return enrollmentManager.getBatchIdsOfActiveEnrollmentsOfUser(req);
    }

    @RequestMapping(value = "/getTrialEnrollmentsForBatchList", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getTrialEnrollmentsForBatchList(GetBatchesForDashboardReq req) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = enrollmentManager.getTrialEnrollmentsForBatchList(req);
        return enrollmentInfos;
    }

    @RequestMapping(value = "/getActiveStudentEnrollmentsByBatchIds", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    @Deprecated
    public List<EnrollmentPojo> getActiveStudentEnrollmentsByBatchIds(@RequestParam(value = "batchIds") ArrayList<String> batchIds) throws BadRequestException {
        List<EnrollmentPojo> enrollmentInfos = enrollmentManager.getActiveStudentEnrollmentsByBatchIds(batchIds);
        return enrollmentInfos;
    }

    @RequestMapping(value = "/getUserActiveEnrollments", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTFEntityEnrollmentResp> getUserActiveEnrollments(GetEnrollmentsReq req) throws VException {
        sessionUtils.checkIfAllowedList(req.getUserId(), Arrays.asList(Role.STUDENT, Role.TEACHER), Boolean.TRUE);
        return enrollmentManager.getUserActiveEnrollments(req);
    }

    @RequestMapping(value = "/refundForEnrollment", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse refundForEnrollment(@RequestBody EnrollmentRefundReq enrollmentRefundReq) throws BadRequestException, ForbiddenException {
        //throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "End subscription not allowed");        
        if (enrollmentRefundReq.getEnrollmentId() == null || enrollmentRefundReq.getEndedTime() == null) {
            return new PlatformBasicResponse(false, null, "enrollmentId not provided");
        }

        Enrollment enrollment = enrollmentDAO.getById(enrollmentRefundReq.getEnrollmentId());

        if (enrollment == null) {
            return new PlatformBasicResponse(false, null, "Invalid enrollment Id");
        }

        if (com.vedantu.session.pojo.EntityType.OTM_BUNDLE.equals(enrollment.getEntityType())) {
            return new PlatformBasicResponse(false, null, "refund not allowed for OTM_Bundle enrollment");
        }

        List<StatusChangeTime> statusList = enrollment.getStatusChangeTime();
        StatusChangeTime statusChangeTime = new StatusChangeTime();
        statusChangeTime.setChangeTime(System.currentTimeMillis());
        statusChangeTime.setNewStatus(EntityStatus.ENDED);
        statusChangeTime.setPreviousStatus(enrollment.getStatus());
        statusList.add(statusChangeTime);
        enrollment.setEndReason(enrollmentRefundReq.getReasonNote());
        enrollment.setStatusChangeTime(statusList);
        enrollment.setStatus(EntityStatus.ENDED);

        PlatformBasicResponse platformBasicResponse = enrollmentManager.refundForEnrollment(enrollmentRefundReq, enrollment);

        if (platformBasicResponse.isSuccess()) {

            enrollmentDAO.save(enrollment);

        }

        return platformBasicResponse;
    }

    @RequestMapping(value = "/getUserDashboardEnrollments", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<GetUserDashboardEnrollmentsRes> getUserDashboardEnrollments(GetEnrollmentsReq req) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, true);
        return enrollmentManager.getUserDashboardEnrollments(req);
    }

    @RequestMapping(value = "/checkTrialEnrollments", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void checkTrialEnrollments() throws VException {
        enrollmentAsyncManager.checkTrialEnrollmentsAsync(0, 100);
    }

    @RequestMapping(value = "/exportTrialEnrollments", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse exportTrialEnrollments(@RequestParam(value = "ccList", required = false) ArrayList<String> ccList) throws VException {

        HttpSessionData sessionData = sessionUtils.getCurrentSessionData();
        if (Role.ADMIN.equals(sessionData.getRole())) {
            Map<String, Object> payload = new HashMap<>();
            payload.put("email", (sessionData.getEmail()));
            payload.put("name", sessionData.getFirstName());
            if (ArrayUtils.isEmpty(ccList)) {
                ccList = new ArrayList<>();
            }
            payload.put("ccList", ccList);
            AsyncTaskParams params = new AsyncTaskParams(AsyncTaskName.EXPORT_TRIAL_ENROLLMENTS, payload);
            asyncTaskFactory.executeTask(params);
        }
        return new PlatformBasicResponse();

    }

    public List<String> validate(Enrollment enrollment) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(enrollment.getUserId())) {
            errors.add(Enrollment.Constants.USER_ID);
        }
        if (enrollment.getRole() == null) {
            errors.add(Enrollment.Constants.ROLE);
        }
        if (enrollment.getStatus() == null) {
            errors.add(Enrollment.Constants.STATUS);
        }
        return errors;
    }

    public List<String> validate(OTFEnrollmentReq enrollment) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(enrollment.getUserId())) {
            errors.add(Enrollment.Constants.USER_ID);
        }
        if (enrollment.getRole() == null) {
            errors.add(Enrollment.Constants.ROLE);
        }
        if (StringUtils.isEmpty(enrollment.getStatus())) {
            errors.add(Enrollment.Constants.STATUS);
        }
        return errors;
    }

    @RequestMapping(value = "/getBatchInfoForUser", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<UserBatchInfoRes> getCourseInfo(@RequestParam(name = "start", required = false, defaultValue = "0") Integer start,
                                                @RequestParam(name = "size", required = false, defaultValue = "20") Integer size) throws VException {
        sessionUtils.checkIfUserLoggedIn();
        Long userId = sessionUtils.getCallingUserId();
        return enrollmentManager.getUserBatchInfos(userId, start, size);
    }

    @RequestMapping(value = "/createEnrollmentConsumption", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse createEnrollmentConsumption(@RequestBody RegPaymentOTFCourseReq regPaymentOTFCourseReq) throws VException {
        return enrollmentManager.processRegPaymentOTFCourse(regPaymentOTFCourseReq);
    }

    @RequestMapping(value = "/performSessionConsumption", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public void performSessionConsumption(@RequestBody SessionSnapshot sessionSnapshot) throws VException {
        enrollmentConsumptionManager.performSessionConsumption(sessionSnapshot);
    }

    @RequestMapping(value = "/createMonthlyBatchSnapshot", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void createMonthlyBatchSnapshot(@RequestParam(name = "batchId") String batchId) throws VException {
        batchSnapshotOTMManager.createBatchSnapshotMonthly(System.currentTimeMillis());
    }

    @RequestMapping(value = "/createInitiallyBatchSnapshot", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void createInitialBatchSnapshot(@RequestParam(name = "batchId") String batchId) throws VException {
        batchSnapshotOTMManager.createBatchSnapshotInitially(System.currentTimeMillis());
    }

    @RequestMapping(value = "/endBatchConsumption", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void endBatchConsumption(@RequestParam(name = "batchId") String batchId) throws VException {
        enrollmentConsumptionManager.performEndBatchConsumption(batchId);
    }

    @RequestMapping(value = "/changeBatchForEnrollment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public EnrollmentPojo changeBatchForEnrollment(@Valid @RequestBody BatchChangeReq req) throws VException {
        sessionUtils.checkIfAllowedList(req.getCallingUserId(), Arrays.asList(Role.ADMIN), Boolean.TRUE);
        return enrollmentManager.changeBatchForEnrollment(req);
    }

    @Deprecated
    @RequestMapping(value = "/makeRegistrationPayment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void makeRegistrationPayment(@RequestBody MigrationRequest req) throws
            VException {
        enrollmentManager.makeRegistrationPayment(req);
    }

    @Deprecated
    @RequestMapping(value = "/makeMigrationPayment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void makeFirstPayment(@RequestBody MigrationRequest req) throws
            VException {
        enrollmentManager.makeMigrationPayment(req);
    }

    @Deprecated
    @RequestMapping(value = "/performSessionConsumptionForSession", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void makeSessionConsumption(@RequestBody MigrationRequest req) throws
            VException {
        enrollmentManager.sessionConsumption(req);
    }

    @Deprecated
    @RequestMapping(value = "/createConsumptionTransaction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void createConsumptionTransaction(@RequestBody CreateConsumptionTransactionReq req) throws
            VException {
        enrollmentConsumptionManager.createConsumptionTransaction(req);
    }

    @Deprecated
    @RequestMapping(value = "/createTopUpEnrollmentConsumption", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse createTopUpEnrollmentConsumption(@RequestBody MakePaymentRequest makePaymentRequest) throws BadRequestException, VException {
        return new PlatformBasicResponse(enrollmentConsumptionManager.topUpEnrollmentConsumption(makePaymentRequest, 1f), null, null);
    }

    @Deprecated
    @RequestMapping(value = "/createCreditTransaction", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void createCreditTransaction(@RequestBody CreateConsumptionTransactionReq req) throws
            VException {
        enrollmentConsumptionManager.createCreditTransaction(req);
    }

    @RequestMapping(value = "/checkOTFSessions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public void checkOTFSessions() throws
            VException {
        batchSnapshotOTMManager.checkForOTFSessions(System.currentTimeMillis());
    }

    @RequestMapping(value = "/checkForUserEnrollment", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse checkForUserEnrollment(@RequestParam(name = "entityName", required = true) CurriculumEntityName curriculumEntityName,
                                                        @RequestParam(name = "entityIds", required = true) List<String> entityIds,
                                                        @RequestParam(name = "userId", required = true) String userId) {

        return enrollmentManager.checkEnrollmentForUser(curriculumEntityName, entityIds, userId);

    }

    @RequestMapping(value = "/getActiveEnrollmentCountForUserInBatchIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public EnrollmentCountForUserBatchRes getActiveEnrollmentCountForUserInBatchIds(
            @RequestParam(name = "batchIds", required = true) List<String> batchIds,
            @RequestParam(name = "userId", required = true) String userId,
            HttpServletRequest request) throws VException {

        sessionUtils.isAllowedApp(request);

//        JSONObject obj = new JSONObject();
        EnrollmentCountForUserBatchRes obj = new EnrollmentCountForUserBatchRes();
        long count = enrollmentDAO.getActiveEnrollmentCountForUserInBatchIds(batchIds, userId);
        obj.setCount(Long.valueOf(count).intValue());
//        obj.put("count", Long.valueOf(count).intValue());
        logger.info("GET-Active-Enrollment-CountForUserInBatchIds");
        logger.info(obj);
        return obj;
    }

    @RequestMapping(value= "/getActiveEnrollmentsForUserIdInBatchIds", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Enrollment getEnrollmentsForUserIdInBatchIds(@RequestParam(value="userId")String userId,
                                                        @RequestParam(value="batchIds")String batchIds){
        String[] temp = batchIds.split(",");
        List <String> bIds = Arrays.asList(temp);
        return enrollmentDAO.getEnrollmentsForUserIdInBatchIds(bIds,userId);
    }

    @RequestMapping(value = "/getPostLoginEnrollments", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public List<OTFEntityEnrollmentResp> getPostLoginEnrollments(@RequestBody GetEnrollmentsReq req) throws
            VException {
        return enrollmentManager.getPostLoginEnrollments(req);
    }

    @RequestMapping(value = "/getActiveAndInactiveEnrollmentsForUserId", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getActiveAndInactiveEnrollmentsForUserId(@RequestParam(value = "userId") String userId) {
        return enrollmentManager.getActiveAndInactiveEnrollmentsForUserId(userId);
    }

    @RequestMapping(value = "/getActiveAndInactiveEnrollments", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<EnrollmentPojo> getActiveAndInactiveEnrollmentsForUserId(@RequestBody GetEnrollmentsReq getEnrollmentsReq) {
        return enrollmentManager.getActiveAndInactiveEnrollments(getEnrollmentsReq);
    }
    
    @RequestMapping(value = "/getActiveUserIdsByBatchIds", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<ActiveUserIdsByBatchIdsResp> getActiveUserIdsByBatchIds(@RequestBody ActiveUserIdsByBatchIdsReq req ) throws BadRequestException {
        req.verify();
        return enrollmentManager.getActiveUserIdsByBatchIds(req);
    }


    @RequestMapping(value = "/getActiveStudentsInBatches", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ActiveStudentsInBatchesResp getActiveStudentsInBatches(@RequestParam(value = "batchIds") List<String> batchIds,
                                                                  @RequestParam(value = "prevLastFetchedId", required = false) String prevLastFetchedId,
                                                                  @RequestParam(value = "limit") int limit) throws BadRequestException {

        return enrollmentManager.getActiveStudentsInBatches(batchIds, prevLastFetchedId, limit);
    }

    @RequestMapping(value = "/getTAsInBatches", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<ActiveTAsForBatchRes> getTAsInBatches(@RequestParam(value = "batchIds") String batchIds) throws BadRequestException {
        String[] temp = batchIds.split(",");
        List <String> bIds = Arrays.asList(temp);
        return enrollmentManager.getTAsInBatches(bIds);
    }

    @RequestMapping(value = "/getTAsInfoForBatchIds", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public ActiveTAsForBatchRes getTAsInfoForBatchIds(@RequestParam(value = "batchIds") String batchIds,
                                                      @RequestParam(value = "userId") String userId) throws BadRequestException {
        String[] temp = batchIds.split(",");
        List <String> bIds = Arrays.asList(temp);
        return enrollmentManager.getTAsInfoForBatchIds(bIds,userId);
    }

    @RequestMapping(value = "/getAllBatchIdsOfUser", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getAllBatchIdsOfUser(@RequestParam(value = "userId") String userId)throws BadRequestException {

        return enrollmentManager.getAllBatchIdsOfUser(userId);
    }

    @RequestMapping(value = "/getEnrolledStudentsInBatch", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getEnrolledStudentsInBatch(@RequestParam(value = "batchId") String batchId,
                                                   @RequestParam(value = "start") int start,
                                                   @RequestParam(value = "size") int size) throws BadRequestException {
        // TODO add auth for this api
        if (size > 1000) {

            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Impermissible fetch size for enrollments");
        }
        return enrollmentManager.getEnrolledStudentsInBatch(batchId, start, size);

    }

    @RequestMapping(value = "/getEnrollmentCountInBatch", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Long getEnrollmentCountInBatch(@RequestParam(value = "batchId") String batchId) throws BadRequestException {
        return enrollmentDAO.getEnrollmentCountInBatch(batchId);

    }

    @RequestMapping(value = "/getEnrollmentListForHomePage",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<EnrollmentHomePageResponse> getEnrollmentListForHomePage(@ModelAttribute EnrollmentHomePageRequest request) throws VException {
        request.verify();
        Long callingUserId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if(Role.STUDENT.equals(role) && (Objects.isNull(callingUserId) || !callingUserId.equals(request.getUserId()))){
            throw new BadRequestException(ErrorCode.ACCESS_NOT_ALLOWED,callingUserId+" is not allowed to access "+request.getUserId()+"'s data");
        }
        return enrollmentManager.getEnrollmentListForHomePage(request);
    }

    @ApiOperation("Fetch course infos for a given list of enrolments")
    @RequestMapping(value = "/getCourseTitlesForEnrollments", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, Course> getCourseTitlesForEnrollments(@RequestParam(value = "enrollmentIds") List<String> enrollmentIds) throws BadRequestException {
        return enrollmentManager.getCourseTitlesForEnrollments(enrollmentIds);
    }

    @RequestMapping(value = "/getEnrolledCoursesForHomepage",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<EnrolledCoursesHomepage> getEnrolledCoursesForHomepage(@ModelAttribute EnrollmentHomePageRequest request) throws VException {
        request.verify();
        Long callingUserId = sessionUtils.getCallingUserId();
        Role role = sessionUtils.getCallingUserRole();
        if (Role.STUDENT.equals(role) && (Objects.isNull(callingUserId) || !callingUserId.equals(request.getUserId()))) {
            throw new BadRequestException(ErrorCode.ACCESS_NOT_ALLOWED, callingUserId + " is not allowed to access " + request.getUserId() + "'s data");
        }
        return enrollmentManager.getEnrolledCoursesForHomepage(request);
    }
    
    //migration
    @RequestMapping(value = "/syncEnrollmentWithLs", method = RequestMethod.GET)
    @ResponseBody
    public void syncEnrollmentWithLs(@RequestParam(value = "enrollmentIds") List<String> enrollmentIds,
                                     HttpServletRequest servletRequest) throws VException {
        sessionUtils.isAllowedApp(servletRequest);
        enrollmentManager.syncEnrollmentWithLS(enrollmentIds);

    }
    
    @RequestMapping(value = "/getAppEnrollmentsOfUser",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BatchBundlePojo> getAppEnrollmentsOfUser(@RequestBody UserPremiumSubscriptionEnrollmentReq request) throws VException {
   
    		request.setUserId(sessionUtils.getCurrentSessionData().getUserId().toString());
    
    		request.setUserRole(sessionUtils.getCurrentSessionData().getRole());

        return enrollmentManager.getAppEnrollmentsOfUser(request);
    }

    @ApiOperation("Fetch teacher ids for user enrollments")
    @RequestMapping(value = "/getTeachersForStudent", method = RequestMethod.GET)
    @ResponseBody
    public List<UserBasicInfo> getTeachersForStudent(@RequestParam(value = "studentId") String studentId) throws VException {
        if (Objects.isNull(sessionUtils.getCurrentSessionData())) {
            throw new UnauthorizedException(ErrorCode.NOT_LOGGED_IN, "Not logged in");
        }

        Long callingUserId = sessionUtils.getCallingUserId();
        sessionUtils.checkIfAllowedList(callingUserId, Collections.singletonList(Role.STUDENT), false);
        if (!callingUserId.equals(Long.valueOf(studentId))) {
            return new ArrayList<>();
        }
        return enrollmentManager.getTeachersForStudent(studentId);
    }

}
