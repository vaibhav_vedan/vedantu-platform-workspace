/**
 * 
 */
package com.vedantu.subscription.controllers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.Role;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.subscription.entities.mongo.PremiumSubscription;
import com.vedantu.subscription.managers.PremiumSubscriptionManager;
import com.vedantu.subscription.request.AddPremiumSubscriptionRequest;
import com.vedantu.subscription.request.PremiumSubscriptionRequest;
import com.vedantu.subscription.response.BatchBundlePojo;
import com.vedantu.subscription.response.PremiumSubscriptionResponse;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.StringUtils;
import com.vedantu.util.security.HttpSessionUtils;

/**
 * @author subarna
 *
 */
@RestController
@RequestMapping("/premium")
public class PremiumSubscriptionController {

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    
    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(PremiumSubscriptionController.class);
    
    @Autowired
    private PremiumSubscriptionManager premiumSubscriptionManager;

    
    @RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse createPremiumSubscription(@RequestBody AddPremiumSubscriptionRequest premiumSubscriptionReq) throws VException {
    	logger.info("PremiumSubscriptionController createPremiumSubscription AddPremiumSubscriptionRequest -> "+premiumSubscriptionReq);
    	premiumSubscriptionReq.verify();
        httpSessionUtils.checkIfAllowed(httpSessionUtils.getCallingUserId(), Role.ADMIN, true);
        return premiumSubscriptionManager.createPremiumSubscription(premiumSubscriptionReq);
    }
    
    
    @RequestMapping(value = "/modify", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse modifyPremiumSubscriptionById(@RequestBody PremiumSubscriptionRequest premiumSubscriptionReq) throws VException {
    	logger.info("PremiumSubscriptionController modifyPremiumSubscription PremiumSubscriptionRequest-> "+premiumSubscriptionReq);
        httpSessionUtils.checkIfAllowed(httpSessionUtils.getCallingUserId(), Role.ADMIN, true);
        if(null == premiumSubscriptionReq || StringUtils.isEmpty(premiumSubscriptionReq.getId()))
        	throw new VException(ErrorCode.BAD_REQUEST_ERROR, "ID cannot be null or empty");
        return premiumSubscriptionManager.modifyPremiumSubscriptionById(premiumSubscriptionReq);
    }
    
    @RequestMapping(value = "/delete", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public PlatformBasicResponse deletePremiumSubscriptionById(@RequestParam(name = "id", required = true) String id) throws VException {
    	logger.info("PremiumSubscriptionController modifyPremiumSubscription id -> "+id);
        httpSessionUtils.checkIfAllowed(httpSessionUtils.getCallingUserId(), Role.ADMIN, true);
        if(StringUtils.isEmpty(id))
        	throw new VException(ErrorCode.BAD_REQUEST_ERROR, "ID cannot be null or empty");
        return premiumSubscriptionManager.deletePremiumSubscriptionById(id);
    }
    

    @RequestMapping(value = "/view", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PremiumSubscriptionResponse> viewPremiumSubscription(@ModelAttribute PremiumSubscriptionRequest premiumSubscriptionReq) throws VException {
    	logger.info("PremiumSubscriptionController viewPremiumSubscription premiumSubscriptionReq -> "+premiumSubscriptionReq);
        httpSessionUtils.checkIfAllowed(httpSessionUtils.getCallingUserId(), Role.ADMIN, true);
        if(null == premiumSubscriptionReq || StringUtils.isEmpty(premiumSubscriptionReq.getGrade()))
        	throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Grade cannot be null or empty");
        return premiumSubscriptionManager.viewPremiumSubscription(premiumSubscriptionReq);
    }
    
    @RequestMapping(value = "/bundles", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<BatchBundlePojo> getPremiumBundlesByFilter(@ModelAttribute PremiumSubscriptionRequest premiumSubscriptionReq) throws VException {
    	logger.info("PremiumSubscriptionController getPremiumBundlesByFilter premiumSubscriptionReq -> "+premiumSubscriptionReq);
    	if(null == premiumSubscriptionReq || StringUtils.isEmpty(premiumSubscriptionReq.getGrade()))
        	throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Grade cannot be null or empty");
    	return premiumSubscriptionManager.getPremiumBundlesByFilter(premiumSubscriptionReq);
    }
    
    @RequestMapping(value = "/findById", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public PremiumSubscription findById(@RequestParam(name = "id", required = true) String id) throws VException {
        return premiumSubscriptionManager.findPremiumSubscriptionDetailsById(id);
    }
}
