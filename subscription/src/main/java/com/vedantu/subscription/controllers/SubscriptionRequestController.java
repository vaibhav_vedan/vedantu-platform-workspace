package com.vedantu.subscription.controllers;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.subscription.entities.sql.SubscriptionRequest;
import com.vedantu.subscription.managers.SubscriptionRequestManager;
import com.vedantu.subscription.request.GetSubscriptionRequestReq;
import com.vedantu.subscription.viewobject.request.SubscriptionReqVO;
import com.vedantu.subscription.viewobject.response.GetSubscriptionRequestsResponse;
import com.vedantu.util.LogFactory;
import com.vedantu.util.subscription.GetSlotConflictRes;
import com.vedantu.util.subscription.ResolveSlotConflictReq;
import com.vedantu.util.subscription.ResolveSlotConflictRes;
import com.vedantu.util.subscription.UpdateSubscriptionRequest;
import com.vedantu.util.subscription.UpdateSubscriptionRequestSubState;

import java.util.Map;

@RestController
@RequestMapping("subscriptionRequest")
public class SubscriptionRequestController {

	@Autowired
	private SubscriptionRequestManager subscriptionRequestManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionRequestController.class);

	@RequestMapping(value = "/create", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest createSubscriptionRequest(@RequestBody SubscriptionReqVO subscriptionReqVO)
			throws Throwable {
		logger.info("Request:" + subscriptionReqVO.toString());
		try {
			SubscriptionRequest response = subscriptionRequestManager.createSubscriptionRequest(subscriptionReqVO);
			logger.info("Response:" + response.toString());
			return response;
		} catch (Exception ex) {
			logger.error("createSubscriptionRequest ex :" + ex.getMessage() + " string :" + ex.toString());
			throw ex;
		}
	}

	@RequestMapping(value = "/updatePaymentDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest updatePaymentDetails(@RequestBody Map<String, String> requestParams) throws Throwable {
		logger.info("ENTRY:" + requestParams);                
		SubscriptionRequest response = subscriptionRequestManager
                        .updatePaymentDetails(requestParams);
		logger.info("EXIT:" + response.toString());
		return response;
	}        
        
	@RequestMapping(value = "/createSubscriptionFromRequest", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest createSubscriptionFromRequest(@RequestBody Map<String, Long> requestParams) throws Throwable {
		logger.info("ENTRY:" + requestParams);                
		SubscriptionRequest response = subscriptionRequestManager
                        .createSubscriptionFromRequest(requestParams.get("subscriptionRequestId"),
                                requestParams.get("hoursToSchedule"), requestParams.get("callingUserId"));
		logger.info("EXIT:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/get/{id}", method = RequestMethod.GET)
	@ResponseBody
	public SubscriptionRequest getSubscriptionRequest(@PathVariable("id") Long id,
			@RequestParam(value = "callingUserId", required = false) Long callingUserId) throws Throwable {
		logger.info("Request subscriptionRequestId: " + id);
		SubscriptionRequest response = subscriptionRequestManager.getSubscriptionRequest(id, callingUserId);
		logger.info("getSubscription Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = { "gets" }, method = RequestMethod.GET)
	@ResponseBody
	public GetSubscriptionRequestsResponse getSubscriptionRequests(GetSubscriptionRequestReq req) throws Throwable {
		GetSubscriptionRequestsResponse response = subscriptionRequestManager.getSubscriptionRequests(req);
		return response;
	}

	@RequestMapping(value = "/expire", method = RequestMethod.GET)
	@ResponseBody
	public GetSubscriptionRequestsResponse getExpiredSubscriptionRequests(@RequestParam(name = "callingUserId", required = true) Long callingUserId)throws Throwable {
		logger.info("Request received for getting SubscriptionRequests");
		GetSubscriptionRequestsResponse response = subscriptionRequestManager.expireSubscriptionRequests(callingUserId);
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/expiryReminder", method = RequestMethod.GET)
	@ResponseBody
	public GetSubscriptionRequestsResponse getExpiryReminderSubscriptionRequests() throws Throwable {
		logger.info("Request received for getting SubscriptionRequests");
		GetSubscriptionRequestsResponse response = subscriptionRequestManager.expiryReminderSubscriptionRequests();
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/activate", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest activateSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req)
			throws Throwable {
		logger.info("Request:" + req.toString());
		SubscriptionRequest response = subscriptionRequestManager.activateSubscriptionRequest(req);
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/cancel", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest cancelSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws Throwable {
		logger.info("Request:" + req.toString());
		SubscriptionRequest response = subscriptionRequestManager.cancelSubscriptionRequest(req);
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/reject", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest rejectSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws Throwable {
		logger.info("Request:" + req.toString());
		SubscriptionRequest response = subscriptionRequestManager.rejectSubscriptionRequest(req);
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/accept", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest accpetSubscriptionRequest(@RequestBody UpdateSubscriptionRequest req) throws Throwable {
		logger.info("Request:" + req.toString());
		SubscriptionRequest response = subscriptionRequestManager.acceptSubscriptionRequest(req);
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/updateSubState", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionRequest updateSubscriptionRequestSubState(@RequestBody UpdateSubscriptionRequestSubState req)
			throws Throwable {
		logger.info("Request:" + req.toString());
		SubscriptionRequest response = subscriptionRequestManager.updateSubscriptionRequestSubState(req.getId(),
				req.getSubState(), req.getCallingUserId());
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/getSlotConflicts", method = RequestMethod.GET)
	@ResponseBody
	public GetSlotConflictRes getSubscriptionRequestConflicts(@RequestParam(name = "id", required = true) Long id)
			throws Throwable {
		logger.info("Request:" + id);
		GetSlotConflictRes response = subscriptionRequestManager.getSubscriptionRequestConflicts(id);
		logger.info("Response:" + response.toString());
		return response;
	}

	@RequestMapping(value = "/resolveSlotConflicts", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResolveSlotConflictRes resolveSubscriptionRequestConflicts(@RequestBody ResolveSlotConflictReq req)
			throws Exception {
		logger.info("Request : " + req.toString());
		ResolveSlotConflictRes response = subscriptionRequestManager.resolveSubscriptionRequestConflicts(req);
		logger.info("Response : " + response.toString());
		return response;
	}

	@RequestMapping(value = "/markScheduled", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public SubscriptionRequest markScheduledSubState(
			@RequestParam(name = "subscriptionRequestId", required = true) Long id) throws Exception {
		logger.info("Request : " + id);
		SubscriptionRequest response = subscriptionRequestManager.markScheduledSubState(id);
		logger.info("Response : " + response.toString());
		return response;
	}

	@RequestMapping(value = "/getPendingRequests", method = RequestMethod.GET)
	@ResponseBody
	public GetSubscriptionRequestsResponse getPendingSubscriptionRequests(
			@RequestParam(name = "startTime", required = true) Long startTime,
			@RequestParam(name = "endTime", required = true) Long endTime) throws Throwable {
		logger.info("Request: startTime - " + startTime + " endTime - " + endTime);
		GetSubscriptionRequestsResponse response = subscriptionRequestManager.getPendingSubscriptionRequests(startTime,
				endTime);
		logger.info("Response:" + response.toString());
		return response;
	}
}
