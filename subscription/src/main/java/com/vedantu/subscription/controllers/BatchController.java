package com.vedantu.subscription.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.User.Role;
import com.vedantu.app.responses.BatchDetailsResponse;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.dinero.request.PayInstalmentReq;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.dinero.response.PayInstalmentRes;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.OTFSessionToolType;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BatchEnrolmentInfo;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.onetofew.pojo.BatchReq;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.pojo.GetBatchesRes;
import com.vedantu.onetofew.pojo.GetTeacherAndStudentIdsRes;
import com.vedantu.onetofew.pojo.LastUpdatedUsers;
import com.vedantu.onetofew.pojo.UpdateBatchToolTypeReq;
import com.vedantu.onetofew.request.GetBatchesReq;
import com.vedantu.onetofew.request.MarkBatchInactiveReq;
import com.vedantu.onetofew.request.PurchaseOTFReq;
import com.vedantu.scheduling.request.session.GetSessionPartnersReq;
import com.vedantu.scheduling.response.session.GetSessionPartnersRes;
import com.vedantu.session.pojo.EnrollmentPurchaseContext;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.managers.BatchManager;
import com.vedantu.subscription.managers.CourseManager;
import com.vedantu.subscription.managers.PaymentManager;
import com.vedantu.subscription.managers.SectionManager;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.subscription.request.BatchDetailInfoReq;
import com.vedantu.subscription.request.EarlyLearningBatchCreationRequest;
import com.vedantu.subscription.request.GetCourseIdByUsingUserIdBatchIdsReq;
import com.vedantu.subscription.request.ProcessOtfPostPaymentReq;
import com.vedantu.subscription.request.UserPremiumSubscriptionEnrollmentReq;
import com.vedantu.subscription.response.BatchDashboardInfo;
import com.vedantu.subscription.response.BatchesResponse;
import com.vedantu.subscription.response.CurriculumProgressInfo;
import com.vedantu.subscription.response.OTMSessionDashboardInfo;
import com.vedantu.subscription.response.StudentEnrolledInfo;
import com.vedantu.subscription.viewobject.response.GetBatchRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("batch")
public class BatchController {

    @Autowired
    private BatchManager batchManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private PaymentManager paymentManager;

    @Autowired
    private CourseManager courseManager;

    @Autowired
    private SectionManager sectionManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(BatchController.class);

    @RequestMapping(method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public BatchInfo addBatch(@RequestBody BatchReq batchReq) throws VException, CloneNotSupportedException {
        logger.info("Batch request :" + batchReq.toString());
        Batch batch = Batch.toBatch(batchReq);
        List<String> errors = validate(batch);
        if (!CollectionUtils.isEmpty(errors)) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Invalid fields " + errors);
        }

        BatchInfo batchInfo = batchManager.addBatch(batch, batchReq.getAgenda(), batchReq.getCallingUserId(),
                batchReq.getInstalmentDetails(),batchReq.getSections());
        logger.info("Batch result :" + batchInfo.toString());
        return batchInfo;
    }

    @RequestMapping(value = "/markBatchInactive", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public BatchInfo markBatchInactive(@RequestBody MarkBatchInactiveReq markBatchInactiveReq) throws VException {
        String batchId = markBatchInactiveReq.getBatchId();
        Batch batch = batchManager.markBatchInactive(batchId);
        BatchInfo batchInfo = batchManager.getBatchInfo(batch, true);
        batchManager.triggerSNSForBatch(batchInfo, EntityStatus.INACTIVE);
        return batchInfo;
    }

    @RequestMapping(value = "/batchDashboardInfo", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<BatchDashboardInfo> getBatchDashboardInfo(@RequestParam(name = "groupName", required = true) String groupName,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size) throws VException {
         sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
         return batchManager.getBatchDashboardInfos(groupName, start, size);
    }

    @RequestMapping(value = "/batchDashboardInfo/enrolledStudents", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StudentEnrolledInfo> getBatchEnrolledStudentInfo(@RequestParam(name = "batchId", required = true) String batchId,
                                                                 @RequestParam(name="start", required = true) int start,
                                                                 @RequestParam(name="size", required = true) int size) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return batchManager.getEnrolledStudentInfo(batchId,start,size);
    }

    @RequestMapping(value = "/batchDashboardInfo/activeStudents", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StudentEnrolledInfo> getBatchActiveEnrolledStudentInfo(@RequestParam(name = "batchId", required = true) String batchId,
                                                                       @RequestParam(name="start", required = true) int start,
                                                                       @RequestParam(name="size", required = true) int size) throws VException {
        return batchManager.getActiveEnrolledStudentInfo(batchId,start,size);
    }

    @RequestMapping(value = "/batchDashboardInfo/regularStudents", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StudentEnrolledInfo> getBatchRegularEnrolledStudentInfo(@RequestParam(name = "batchId", required = true) String batchId,
                                                                        @RequestParam(name="start", required = true) int start,
                                                                        @RequestParam(name="size", required = true) int size) throws VException {
        return batchManager.getRegularEnrolledStudentInfo(batchId,start,size);
    }

    @RequestMapping(value = "/batchDashboardInfo/topicsStats", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public CurriculumProgressInfo getTopicsStats(@RequestParam(name = "batchId", required = true) String batchId) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return batchManager.getTopicsStats(batchId);
    }

    @RequestMapping(value = "/otmSessionDashboardInfo", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<OTMSessionDashboardInfo> getOTMSessionDashboardInfo(@RequestParam(name = "groupName", required = true) String groupName,
            @RequestParam(name = "start", required = false) Integer start,
            @RequestParam(name = "size", required = false) Integer size) throws VException {
            sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        return batchManager.getOTMSessionDashboardInfo(groupName, start, size);
    }
    
    @RequestMapping(value = "/getBatchBasicInfosForGroupName", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody  
    public List<BatchBasicInfo> getBatchBasicInfosForGroupName(@RequestParam(name = "groupName", required = true) String groupName){
        return batchManager.getBatchBasicInfosForGroupName(groupName);
    }

    @RequestMapping(value = "/getBatchBasicInfosForMultipleGroupNames", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody  
    public List<BatchBasicInfo> getBatchBasicInfosForGroupName(@RequestParam(name = "groupNames", required = true) List<String> groupNames){
        return batchManager.getBatchBasicInfosForGroupNames(groupNames);
    }    
    
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BatchInfo getBatch(@PathVariable("id") String id,
            @RequestParam(name = "returnEnrollments", required = false) Boolean returnEnrollments,
            @RequestParam(name = "returnStudentInfo", required = false) Boolean returnStudentInfo,
            @RequestParam(name = "callingUserId", required = false) Long callingUserId) throws Exception {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        Batch batch = batchManager.getBatch(id);
        if (batch == null) {
            throw new NotFoundException(ErrorCode.NOT_FOUND_ERROR, "batch not found with id " + id);
        }
        Long cont = batchManager.getEnrollmentCountInBatch(id);
        Course course = courseManager.getCourse(batch.getCourseId());
        BatchInfo batchInfo = batchManager.getBatchInfo(batch, course, returnEnrollments, exposeEmail);
        if(cont!=null){
            batchInfo.setEnrollmentCount(cont.intValue());
        }
        return batchInfo;
    }

    @RequestMapping(value = "/basicInfo/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BatchBasicInfo getBatchBasicInfo(@PathVariable("id") String id) throws Exception {
        BatchBasicInfo batchInfo = batchManager.getBatchBasicInfo(id);
        return batchInfo;
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public GetBatchRes getBatches(GetBatchesReq req) {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        GetBatchRes batchesRes = batchManager.getBatches(req, exposeEmail);
        logger.info("get batches res count :" + batchesRes.getCount() + ", totalCount :" + batchesRes.getTotalCount());
        return batchesRes;
    }

    @RequestMapping(value = "/updateBatchToolType", method = RequestMethod.POST)
    @ResponseBody
    public PlatformBasicResponse updateBatchToolType(@RequestBody UpdateBatchToolTypeReq req)
            throws VException {
        batchManager.updateToolType(req);
        return new PlatformBasicResponse(true, null, null);
    }

    @RequestMapping(value = "/getSessionPartners", method = RequestMethod.GET)
    @ResponseBody
    public GetSessionPartnersRes getSessionPartners(GetSessionPartnersReq req) throws VException {
        req.verify();
        return batchManager.getSessionPartners(req);
    }

    @RequestMapping(value = "/getTeachersAndStudentsInBatch", method = RequestMethod.GET)
    @ResponseBody
    public GetTeacherAndStudentIdsRes getTeachersAndStudentsInBatch(@RequestParam(name = "batchId", required = true) String batchId) throws BadRequestException {
        return new GetTeacherAndStudentIdsRes();
    }

    @RequestMapping(value = "/getCourseTitleForBatchIds", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, String> getCourseTitleForBatchIds(@RequestParam(name = "batchIds", required = true) List<String> batchIds) throws BadRequestException {
        return batchManager.getCourseTitleForBatchIds(batchIds);
    }

    @RequestMapping(value = "/getBatchByGroupName", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getBatchByGroupName(@RequestParam(name = "groupName", required = true) List<String> groupName) throws BadRequestException {
        return batchManager.getBatchByGroupName(groupName);
    }

    @RequestMapping(value = "/getBatchBasicInfosFromCourse", method = RequestMethod.GET)
    @ResponseBody
    public GetBatchesRes getBatchBasicInfosFromCourse(@RequestParam(name = "batchId", required = false) String batchId,
            @RequestParam(name = "courseId", required = false) String courseId) {
        return batchManager.getBatchBasicInfosFromCourse(batchId, courseId);
    }

    @RequestMapping(value = "/getBatchPurchasePrice", method = RequestMethod.GET)
    @ResponseBody
    public PlatformBasicResponse getBatchPurchasePrice(@RequestParam(name = "batchId", required = true) String batchId) throws NotFoundException {
        BatchBasicInfo batchInfo = batchManager.getBatchBasicInfo(batchId);
        PlatformBasicResponse res = new PlatformBasicResponse();
        res.setResponse(String.valueOf(batchInfo.getPurchasePrice()));
        return res;
    }

    @RequestMapping(value = "/getBatchBasicInfoForBatchIds", method = RequestMethod.GET)
    @ResponseBody
    public Map<String, BatchBasicInfo> getBatchBasicInfoForBatchIds(@RequestParam(name = "batchIds", required = true) List<String> batchIds) throws BadRequestException {
        return batchManager.getBatchBasicInfoForBatchIds(batchIds);
    }

    //Used mainly for scheduling
    @RequestMapping(value = "/getBatchById/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BatchBasicInfo getBatchById(@PathVariable("id") String id) throws Exception {
        return batchManager.getBatchById(id);
    }

    @RequestMapping(value = "/getBatchByIdWithEnrollments/{id}", method = RequestMethod.GET)
    @ResponseBody
    public BatchInfo getBatchByIdWithEnrollments(@PathVariable("id") String id) throws Exception {
        return batchManager.getBatchByIdWithEnrollments(id);
    }

    @RequestMapping(value = "/getBatchIdsActiveEnrollments", method = RequestMethod.GET)
    @ResponseBody
    public List<EnrollmentPojo> getBatchIdsActiveEnrollments(@RequestParam(name = "batchIds", required = true) List<String> batchIds,
            @RequestParam(value = "returnTeacherEnrollments", required = false) boolean returnTeacherEnrollments) throws Exception {
        return batchManager.getBatchIdsActiveEnrollments(batchIds, returnTeacherEnrollments);
    }

    @RequestMapping(value = "/getBatchBasicInfosByIds", method = RequestMethod.POST)
    @ResponseBody
    public List<BatchBasicInfo> getBatchBasicInfosByIds(@RequestBody List<String> batchIds) throws Exception {
        return batchManager.getBatchBasicInfosByIds(batchIds);
    }

    @RequestMapping(value = "/getDetailedBatchInfo", method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BatchBasicInfo> getDetailedBatchInfo(@RequestBody BatchDetailInfoReq req) throws Exception {
        return batchManager.getDetailedBatchInfo(req);
    }

    @RequestMapping(value = "/getLastUpdatedUserIds", method = RequestMethod.GET)
    @ResponseBody
    public LastUpdatedUsers getLastUpdatedUserIds(LastUpdatedUsers req)
            throws InternalServerErrorException, BadRequestException, NotFoundException {
        return new LastUpdatedUsers();
    }

    public List<String> validate(Batch batch) {
        List<String> errors = new ArrayList<>();
        if (StringUtils.isEmpty(batch.getCourseId())) {
            errors.add(Batch.Constants.COURSE_ID);
        }
        if (batch.getStartTime() == null || batch.getEndTime() == null) {
            errors.add(Batch.Constants.START_TIME);
            errors.add(Batch.Constants.END_TIME);
        }
        if (batch.getPurchasePrice() < 0) {
            errors.add(Batch.Constants.PURCHASE_PRICE);
        }
        if (batch.getVisibilityState() == null) {
            errors.add(Batch.Constants.VISIBILITY_STATE);
        }
        if (batch.getDuration() == null) {
            errors.add("duration");
        }
        if (ArrayUtils.isEmpty(batch.getSearchTerms())) {
            errors.add(Batch.Constants.SEARCH_TERMS);
        }
//		if (StringUtils.isEmpty(batch.getPlanString())) {
//			errors.add(Batch.Constants.PLAN_STRING);
//		}
        if (batch.getSessionToolType() == null || OTFSessionToolType.GTM.equals(batch.getSessionToolType())) {
            errors.add("sessionToolType");
        }
        return errors;
    }

    @RequestMapping(value = "/purchaseAndRegisterBatch", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public OrderInfo purchaseAndRegisterBatch(@RequestBody PurchaseOTFReq req) throws VException, CloneNotSupportedException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return batchManager.purchaseAndRegisterBatch(req);
    }

    @RequestMapping(value = "/purchaseBatch", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public OrderInfo purchaseBatch(@RequestBody PurchaseOTFReq req) throws VException, CloneNotSupportedException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return batchManager.purchaseBatch(req);
    }

    @RequestMapping(value = "/processOTFRegFeePayment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void processOTFRegFeePayment(@RequestBody ProcessOtfPostPaymentReq req) throws VException, CloneNotSupportedException {

        batchManager.processOTFRegFeePayment(req.getOrderId(), req.getUserId(), req.getEntityId());
    }

    @RequestMapping(value = "/processBatchAfterPayment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void processBatchAfterPayment(@RequestBody ProcessOtfPostPaymentReq req) throws VException, CloneNotSupportedException {

        if(req.getPurchaseContextId()==null) {
            batchManager.processBatchAfterPayment(req.getOrderId(), req.getUserId(), req.getEntityId());
        }else{
            batchManager.processBatchAfterPayment(req.getOrderId(),req.getUserId(),batchManager.getBatch(req.getEntityId()), EnrollmentPurchaseContext.BUNDLE,req.getPurchaseContextId(), EnrollmentType.UPSELL);
        }
    }

    @RequestMapping(value = "/payInstalmentForBatch", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public PayInstalmentRes payInstalmentForBatch(@RequestBody PayInstalmentReq req) throws VException, IOException {

        sessionUtils.checkIfAllowedList(req.getUserId(), null, Boolean.TRUE);
        req.setNewFlow(Boolean.TRUE);
        return paymentManager.payInstalment(req);
    }

    @RequestMapping(value = "/processInstalmentAfterPayment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void processInstalmentAfterPayment(@RequestBody InstalmentInfo info) throws VException, CloneNotSupportedException {

        batchManager.processInstalmentAfterPayment(info);
    }

    @RequestMapping(value = "/addSnapshots", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public void updateSnapshots(@RequestBody List<SessionSnapshot> sessionSnapshots) {
        batchManager.updateSnapshots(sessionSnapshots);
    }

    @RequestMapping(value = "/updateSnapshot", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public void updateSnapshots(@RequestBody SessionSnapshot sessionSnapshot) {
        batchManager.updateSnapshot(sessionSnapshot);
    }

    @RequestMapping(value = "/endBatchConsumption", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void endBatchConsumption(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            batchManager.checkForEndBatchConsumptionAsync();
        }
    }

    @RequestMapping(value = "/contentBatchConsumption", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void contentBatchConsumption(@RequestHeader(value = "x-amz-sns-message-type") String messageType,
            @RequestBody String request) throws Exception {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            logger.info(subscriptionRequest.toString());
            batchManager.checkForContentOnlyBatchConsumptionAsync();
        }
    }

    @RequestMapping(value = "/getBatchIdsForUser", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getBatchIdsForUser(@RequestParam( name="userId", required=true) String userId){
        return batchManager.getBatchIdsForUser(userId);
    }
    
    @RequestMapping(value = "/getModularBatch", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getBatchBasicInfosByIds() throws Exception {
        return batchManager.getModularBatchIds();
    }    

    @RequestMapping(value = "/getBatchForAM", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getBatchForAM(@RequestParam(name="groupName", required=true)String groupName, @RequestParam(name="amId", required=false)String amId){
        return batchManager.getBatchIdsForAMandGroupName(groupName, amId);
    }      
    
    @RequestMapping(value = "/getNonTeachersIds/{id}", method = RequestMethod.GET)
    @ResponseBody
    public Set<Long> getNonTeachersIds(@PathVariable("id") String id) throws VException {
        return batchManager.getNonTeachersIds(id);
    }

    @RequestMapping(value = "/getEnrolmentsByBatchIds", method = RequestMethod.POST)
    @ResponseBody
    public List<BatchEnrolmentInfo> getEnrolmentsByBatchIds(@RequestBody List<String> batchIds) throws Exception {
        return batchManager.getEnrolmentsByBatchIds(batchIds);
    }


    @RequestMapping(value = "/autoEnrolToBatch", method = RequestMethod.POST, consumes = MediaType.TEXT_PLAIN_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void autoEnrolToBatch(@RequestHeader(value = "x-amz-sns-message-type") String messageType, @RequestBody String request) throws VException, CloneNotSupportedException, InterruptedException {
        Gson gson = new Gson();
        AWSSNSSubscriptionRequest subscriptionRequest = gson.fromJson(request, AWSSNSSubscriptionRequest.class);
        if (messageType.equals("SubscriptionConfirmation")) {
            String json = null;
            logger.info("SubscribeURL = " + subscriptionRequest.getSubscribeURL());
            ClientResponse resp = WebUtils.INSTANCE.doCall(subscriptionRequest.getSubscribeURL(), HttpMethod.GET, json, false);
            logger.info(resp.getEntity(String.class));
        } else if (messageType.equals("Notification")) {
            logger.info("Notification received - SNS");
            String batchId = subscriptionRequest.getMessage();
            batchManager.autoEnrolToBatch(batchId);
        }
    }

    @RequestMapping(value = "/getCourseIdByUsingUserIdBatchIds", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getCourseIdByUsingUserIdBatchIds(@RequestBody GetCourseIdByUsingUserIdBatchIdsReq req) throws BadRequestException {
        return batchManager.getCourseIdByUsingUserIdBatchIds(req);
    }

    @RequestMapping(value = "/getEarlyLearningBundleByBatch/{batchId}", method = RequestMethod.GET)
    @ResponseBody
    public Bundle getEarlyLearningBundleByBatch(@PathVariable("batchId") String batchId) throws BadRequestException {
        return batchManager.getEarlyLearningBundleByBatch(batchId);
    }

    @RequestMapping(value = "/createEarlyLearningBatchForEnrollment", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public BatchInfo createEarlyLearningBatchForEnrollment(@RequestBody EarlyLearningBatchCreationRequest req) throws VException, CloneNotSupportedException {
        req.verify();
        return batchManager.createEarlyLearningBatchForEnrollment(req);
    }

    @RequestMapping(value="/getSeatsRemainingForBatch", method = RequestMethod.GET)
    @ResponseBody
    public Long getSeatsRemainingForBatch(@RequestParam(value="batchId") String batchId,
                                          @RequestParam(value="callingUserId") Long callingUserId,
                                          @RequestParam(value="withBuffer") Boolean withBuffer) throws VException {
        sessionUtils.checkIfAllowedList(callingUserId,null,true);
        return sectionManager.getSeatsRemainingForBatch(batchId,withBuffer, Arrays.asList(EnrollmentState.REGULAR,EnrollmentState.TRIAL));
    }
    
    @RequestMapping(value = "/getBatchesForHomefeed", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<BatchesResponse> getBatchesForHomefeed(@RequestBody UserPremiumSubscriptionEnrollmentReq req) throws VException {

    		req.setUserId(sessionUtils.getCallingUserId().toString());

    		req.setUserRole(sessionUtils.getCallingUserRole());

    	return batchManager.getBatchesForHomeFeedCard(req);
    }
    
    @RequestMapping(value = "/getBatchDetailsForApp", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public BatchDetailsResponse getBatchDetailsForApp(@RequestParam(name="bundleId", required=true) String bundleId,
    		@RequestParam(name="userId", required=false) String userId) throws VException {
    	if(StringUtils.isEmpty(userId)) {
    		userId = sessionUtils.getCallingUserId().toString();
    	}
    	return batchManager.getBatchDetailsForApp(bundleId, userId);
    }
}
