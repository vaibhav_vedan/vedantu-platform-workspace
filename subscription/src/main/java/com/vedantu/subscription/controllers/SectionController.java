package com.vedantu.subscription.controllers;

import com.google.gson.Gson;
import com.sun.jersey.api.client.ClientResponse;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.VException;
import com.vedantu.exception.VExceptionFactory;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.subscription.managers.EnrollmentManager;
import com.vedantu.subscription.managers.SectionManager;
import com.vedantu.subscription.pojo.section.BatchSectionPojo;
import com.vedantu.subscription.request.section.*;
import com.vedantu.subscription.response.section.SectionInfo;
import com.vedantu.subscription.response.section.SectionMetaDataRes;
import com.vedantu.subscription.response.section.StudentSectionMappingRes;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.WebUtils;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/section")
public class SectionController{

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SectionController.class);

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private SectionManager sectionManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @RequestMapping(value="/getSections",method = RequestMethod.GET,produces = "application/json")
    @ResponseBody
    public List<SectionInfo> getSectionsForBatchId(@RequestParam(value="batchId") String batchId,
                                                   @RequestParam(value="callingUserId" ) Long callingUserId) throws VException {
        // API restriction
        httpSessionUtils.checkIfAllowedList(callingUserId,null,true);
        return sectionManager.getSectionsForBatchId(batchId,callingUserId);
    }

    @RequestMapping(value="/updateSections", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse updateSections(@RequestBody UpdateSectionsReq updateSectionsReq)throws VException{
        // API restriction
        httpSessionUtils.checkIfAllowedList(updateSectionsReq.getCallingUserId(),null,true);
        updateSectionsReq.verify();
        sectionManager.updateSections(updateSectionsReq);
        return new PlatformBasicResponse(true,"Success","");
    }

    // for testing purpose, not used any where as of now
    @RequestMapping(value="/getDeletedSections", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<Section> getDeletedSections(@RequestParam(value = "batchId") String batchId,
                                            @RequestParam(value="callingUserId") Long callingUserId) throws VException {
        httpSessionUtils.checkIfAllowedList(callingUserId,null,true);
        return sectionManager.getDeletedSectionsForBatchId(batchId);
    }

    // for testing purposes, not used any where as of now
    @RequestMapping(value="/getIdEnrollmentCount", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map<String,Long> getIdEnrollmentCount(@RequestParam(value = "batchId") String batchId) throws VException {
        return sectionManager.getEnrolledCountForSections(batchId);
    }

    @RequestMapping(value="/isTwoTeacherModelApplicable",method = RequestMethod.POST, produces = "application/json" , consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse isTwoTeacherModelApplicable(@RequestBody BatchSectionReq req){
        return sectionManager.isTwoTeacherModelApplicable(req.getBatchIds());
    }

    @RequestMapping(value="/isOtfSessionApplicable",method = RequestMethod.POST, produces = "application/json" , consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse isOtfSessionApplicable(@RequestBody BatchSectionReq req){
        return sectionManager.isOtfSessionApplicable(req.getBatchIds());
    }


    //---------------------- Section Management Interface APIs below --------------------------------------------

    // student-level view
    @RequestMapping(value="/getStudentMappings", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StudentSectionMappingRes> getStudentSectionMappings(@RequestParam(value = "batchId") String batchId,
                                                                    @RequestParam(value="userId") String userId,
                                                                    @RequestParam(value="callingUserId") Long callingUserId) throws VException {
        // api restriction
        httpSessionUtils.checkIfAllowedList(callingUserId,null,true);
        return sectionManager.getStudentSectionMappings(batchId,userId);
    }

    // paid-student level view
    @RequestMapping(value="/getPaidStudentMappings", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StudentSectionMappingRes> getPaidStudentSectionMappings(@RequestParam(value = "batchId", required = false) String batchId,
                                                                        @RequestParam(value="sectionId", required = false)String sectionId,
                                                                        @RequestParam(value="callingUserId") Long callingUserId) throws VException {
        // api restriction
        httpSessionUtils.checkIfAllowedList(callingUserId,null,true);
        return sectionManager.getPaidStudentSectionMappings(batchId,sectionId);
    }

    // trial-student level view
    @RequestMapping(value="/getTrialStudentMappings", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<StudentSectionMappingRes> getTrialStudentMappings(@RequestParam(value = "batchId", required = false) String batchId,
                                                                  @RequestParam(value="sectionId", required = false)String sectionId,
                                                                  @RequestParam(value="callingUserId") Long callingUserId) throws VException {
        // api restriction
        httpSessionUtils.checkIfAllowedList(callingUserId,null,true);
        return sectionManager.getTrialStudentSectionMappings(batchId,sectionId);
    }

    // handles update from both student-level + paid-student level view
    @RequestMapping(value="/updateSectionsFromSMI", method = RequestMethod.POST, produces = "application/json",consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse updateSectionsFromSMI(@Valid @RequestBody UpdateSectionsFromSMIReq updateSectionsFromSMIReq) throws VException {
        // api restriction
        httpSessionUtils.checkIfAllowedList(updateSectionsFromSMIReq.getCallingUserId(),null,true);
        return sectionManager.updateSectionsFromSMI(updateSectionsFromSMIReq);
    }

    // --------------------------- API used by node team ----------------------------

    @RequestMapping(value="/getSectionsInfoFromBatchIdsTAids", method = RequestMethod.POST, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public Map<String,SectionMetaDataRes> getSectionsInfoFromBatchIdsTAids(@RequestBody SectionMetaDataReq sectionMetaDataReq){
        return sectionManager.getSectionsInfoFromBatchIdsTAids(sectionMetaDataReq);
    }

    @RequestMapping(value="/isTeacherPartOfSection", method = RequestMethod.GET, produces = "application/json", consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse isTeacherPartOfSection(@ModelAttribute TeacherInSectionReq req) throws BadRequestException {
        req.verify();
        return sectionManager.isTeacherPartOfSection(req);
    }
    // --------------------------- END ----------------------------------------------


    @RequestMapping(value="/sectionBulkUpdate", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse sectionBulkUpdate(@RequestBody UpdateSectionsFromSMIReq updateSectionsFromSMIReq) throws VException {
        // api restriction
        httpSessionUtils.checkIfAllowedList(updateSectionsFromSMIReq.getCallingUserId(),null,true);
        return  sectionManager.sectionBulkUpdate(updateSectionsFromSMIReq);
    }

}
