package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import java.io.UnsupportedEncodingException;
import java.util.List;

import javax.ws.rs.core.MediaType;

import com.vedantu.subscription.entities.mongo.AcadMentorStudents;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.vedantu.User.User;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.pojo.InsertAcadMentorStudentsReq;
import com.vedantu.onetofew.pojo.ModifyAcadMentorOfStudentReq;
import com.vedantu.subscription.managers.AcadMentorStudentsManager;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("acadmentorstudents")
public class AcadMentorStudentsController {

    @Autowired
    private AcadMentorStudentsManager acadMentorStudentsManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AcadMentorStudentsController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/assignAcadMentorStudents", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse assignAcadMentorStudents(@RequestBody InsertAcadMentorStudentsReq acadMentorStudentsReq) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        acadMentorStudentsReq.verify();
        long noUpdated = acadMentorStudentsManager.assignAcadMentorStudents(acadMentorStudentsReq);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setResponse("" + noUpdated);
        return platformBasicResponse;
    }

    @RequestMapping(value = "/modifyAcadMentorStudents", method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public PlatformBasicResponse modifyAcadMentorStudents(@RequestBody ModifyAcadMentorOfStudentReq modifyAcadMentorOfStudentReq) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        modifyAcadMentorOfStudentReq.verify();
        long noUpdated = acadMentorStudentsManager.modifyAcadMentorStudents(modifyAcadMentorOfStudentReq);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setResponse("" + noUpdated);
        return platformBasicResponse;
    }

    @RequestMapping(value = "/removeStudents", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public PlatformBasicResponse removeStudents(@RequestBody InsertAcadMentorStudentsReq acadMentorStudentsReq) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        acadMentorStudentsReq.verify();
        long noUpdated = acadMentorStudentsManager.removeStudents(acadMentorStudentsReq);
        PlatformBasicResponse platformBasicResponse = new PlatformBasicResponse();
        platformBasicResponse.setResponse("" + noUpdated);
        return platformBasicResponse;
    }

    @RequestMapping(value = "/{acadMentorEmail}/getStudents", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> getStudents(@PathVariable("acadMentorEmail") String acadMentorEMail, @RequestParam("skip") Integer skip, @RequestParam("limit") Integer limit)
            throws VException, UnsupportedEncodingException {
//        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        return acadMentorStudentsManager.getStudentsByAcadMentor(acadMentorEMail, skip, limit);
    }

    @RequestMapping(value = "/getAcaMentors", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<User> getAcaMentors(@RequestParam("skip") int skip, @RequestParam("limit") Integer limit) throws VException, UnsupportedEncodingException {
        return acadMentorStudentsManager.getAcadMentors(skip,limit);
    }

    @RequestMapping(value = "/getAcaMentorsForStudents", method = RequestMethod.POST, produces = "application/json")
    @ResponseBody
    public List<AcadMentorStudents> getAcaMentors(@RequestBody List<Long> studentIds) throws VException, UnsupportedEncodingException {
        return acadMentorStudentsManager.getAcadMentors(studentIds);
    }


}
