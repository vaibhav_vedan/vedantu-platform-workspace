package com.vedantu.subscription.controllers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.onetofew.request.GetUserMessageReq;
import com.vedantu.subscription.entities.mongo.UserMessage;
import com.vedantu.subscription.managers.UserMessageManager;
import com.vedantu.subscription.viewobject.response.BasicRes;
import com.vedantu.subscription.viewobject.response.GetUserMessageRes;
import com.vedantu.util.LogFactory;

@RestController
@RequestMapping("userMessage")
public class UserMessageController {

	@Autowired
	private UserMessageManager userMessageManager;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(UserMessageController.class);

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public BasicRes userMessage(@RequestBody UserMessage userMessage) throws Exception {
		logger.info("Request:" + userMessage.toString());
		BasicRes res = new BasicRes();
		UserMessage result = userMessageManager.userMessage(userMessage);
		if (result == null) {
			res.setSuccess(false);
		}

		logger.info("Response:" + res);
		return res;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public UserMessage getUserMessageById(@PathVariable("id") String id) throws Exception {
		logger.info("Request:" + id);
		UserMessage result = userMessageManager.getUserMessage(id);
		logger.info("Response:" + result);
		return result;
	}

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public GetUserMessageRes getUserMessages(GetUserMessageReq req) {
		List<UserMessage> userMessages = userMessageManager.getUserMessages(req);

		GetUserMessageRes res = new GetUserMessageRes();
		if (userMessages != null && !userMessages.isEmpty()) {
			res.setList(userMessages);
		}
		return res;
	}

}
