package com.vedantu.subscription.controllers;

import java.util.ArrayList;
import java.util.List;

import com.vedantu.exception.BadRequestException;
import com.vedantu.subscription.viewobject.request.OrderedItemNameDetailsReq;
import com.vedantu.subscription.viewobject.response.OrderedItemNameDetailsRes;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.vedantu.subscription.entities.sql.SubscriptionDetails;
import com.vedantu.subscription.managers.SubscriptionDetailsManager;
import com.vedantu.util.LogFactory;


@RestController
@RequestMapping("subscriptionDetails")
public class SubscriptionDetailsController {

	@Autowired
	private SubscriptionDetailsManager subscriptionDetailsManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionDetailsController.class);
	
	@RequestMapping(value = "/addSubscriptionDetails", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionDetails addSubscriptionDetails(@RequestBody SubscriptionDetails subscriptionDetails) throws Exception {

		subscriptionDetails.setLastUpdatedTime(System.currentTimeMillis());
		subscriptionDetails.setEnabled(true);
		logger.info("Request:" + subscriptionDetails.toString());
		List<String> errors = validate(subscriptionDetails);
		SubscriptionDetails subscriptionDetailsRes;

		if (errors != null && !errors.isEmpty()) {
			logger.error("Illegal Arguments with fields " + errors);
			throw new IllegalArgumentException("Illegal Arguments with fields " + errors);
		} else {
			subscriptionDetailsRes = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetails,null,null);
		}

		logger.info("Response:" + subscriptionDetailsRes.toString());
		return subscriptionDetailsRes;
	}

	@RequestMapping(value = "getSubscriptionDetails/{id}", method = RequestMethod.GET)
	@ResponseBody
	public SubscriptionDetails getSubscriptionDetailsById(@PathVariable("id") Long id) throws Exception {
		logger.info("Request received for getting SubscriptionDetails for db_id: " + id);
		SubscriptionDetails subscriptionDetailsRes = subscriptionDetailsManager.getSubscriptionDetailsById(id,true,null);
		logger.info("Response:" + subscriptionDetailsRes.toString());
		return subscriptionDetailsRes;
	}

	@RequestMapping(value = "/updateSubscriptionDetails/{id}", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public SubscriptionDetails updateSubscriptionDetails(@RequestBody SubscriptionDetails subscriptionDetails,
			@PathVariable("id") Long id) throws Exception {
		subscriptionDetails.setLastUpdatedTime(System.currentTimeMillis());
		subscriptionDetails.setId(id);
		logger.info("Request:" + subscriptionDetails.toString());
		SubscriptionDetails subscriptionDetailsRes = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetails,null,null);
		logger.info("Response:" + subscriptionDetailsRes.toString());
		return subscriptionDetailsRes;
	}

	@RequestMapping(value = "deleteSubscriptionDetails/{id}", method = RequestMethod.DELETE)
	@ResponseBody
	public SubscriptionDetails deleteSubscriptionDetailsById(@PathVariable("id") Long id) throws Exception {
		logger.info("Request received for disabling SubscriptionDetails for id: " + id);
		SubscriptionDetails subscriptionDetailsRes = subscriptionDetailsManager.getSubscriptionDetailsById(id,true,null);
		if (subscriptionDetailsRes != null) {
			subscriptionDetailsRes.setEnabled(false);
			subscriptionDetailsRes = subscriptionDetailsManager.addOrUpdateSubscriptionDetails(subscriptionDetailsRes,true,null);
			logger.info("Response:" + subscriptionDetailsRes.toString());
		}
		return subscriptionDetailsRes;
	}

	@RequestMapping(value = "/getOrderedItemNames",method = RequestMethod.POST,consumes = MediaType.APPLICATION_JSON_VALUE,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public OrderedItemNameDetailsRes getOrderedItemNames(@RequestBody OrderedItemNameDetailsReq itemNameReq) throws BadRequestException {
		itemNameReq.verify();
		return subscriptionDetailsManager.getOrderedItemNames(itemNameReq);
	}

	public List<String> validate(SubscriptionDetails subscriptionDetails) {
		List<String> errors = new ArrayList<String>();

		if (subscriptionDetails == null) {
			errors.add("ID");
		}

		Long id = subscriptionDetails.getId();

		logger.info("ID: " + id);
		return errors;
	}
}
