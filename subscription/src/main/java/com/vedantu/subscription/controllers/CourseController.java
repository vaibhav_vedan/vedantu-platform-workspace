package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;

import java.util.*;

import com.vedantu.onetofew.pojo.*;
import com.vedantu.subscription.managers.EnrollmentManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.onetofew.request.GetBatchesReq;
import com.vedantu.onetofew.request.GetCoursesReq;
import com.vedantu.onetofew.request.PurchaseOTFReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.managers.CourseManager;
import com.vedantu.subscription.request.ProcessOtfPostPaymentReq;
import com.vedantu.subscription.viewobject.request.ChangeStudentSlotReq;
import com.vedantu.subscription.pojo.CourseMobileRes;
import com.vedantu.subscription.viewobject.response.GetCourseRes;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.FosUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;

import com.vedantu.util.request.GetEnrollmentsReq;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("course")
public class CourseController {

    @Autowired
    private FosUtils fosUtils;

    @Autowired
    private CourseManager courseManager;

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private HttpSessionUtils sessionUtils;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CourseController.class);

    private final String apigurusUrl = ConfigUtils.INSTANCE.getStringValue("ip_location.apigurus.api");

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public CourseInfo addCourse(@RequestBody Course course) throws Exception {
        logger.info("Request:" + course.toString());
        List<String> errors = validate(course);
        if (ArrayUtils.isEmpty(errors)) {
            Set<String> teacherIds = new HashSet<>();
            if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                for (BoardTeacherPair pair : course.getBoardTeacherPairs()) {
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null) {
                                teacherIds.add(teacher.getTeacherId().toString());
                            }
                        }
                    }
                }
            }
            if (ArrayUtils.isNotEmpty(course.getFeaturedTeachers())) {
                for (BoardTeacherPair pair : course.getFeaturedTeachers()) {
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null) {
                                teacherIds.add(teacher.getTeacherId().toString());
                            }
                        }
                    }
                }
            }
            course.setTeacherIds(teacherIds);
            course = courseManager.course(course);
        } else {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, errors.toString());
        }
        Map<String, UserBasicInfo> userBasicInfos = fosUtils.getUserBasicInfosMap(course.getTeacherIds(), true);
        CourseInfo courseInfo = course.toCourseInfo(course, userBasicInfos);
        logger.info("Response:" + courseInfo.toString());
        return courseInfo;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    @ResponseBody
    public CourseInfo getCourse(@PathVariable("id") String id) throws Exception {
        logger.info("Request receivde for getting Course id: " + id);
        Course course = courseManager.getCourse(id);
        if (course == null) {
            throw new NotFoundException(ErrorCode.COURSE_NOT_FOUND, "Course not found for id : " + id);
        }
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        Map<String, UserBasicInfo> userBasicInfos = fosUtils.getUserBasicInfosMap(course.getTeacherIds(), exposeEmail);
        CourseInfo courseInfo = course.toCourseInfo(course, userBasicInfos);
        logger.info("Response:" + courseInfo.toString());
        return courseInfo;
    }

    @RequestMapping(value = "/getCoursesBasicInfos", method = RequestMethod.GET)
    @ResponseBody
    public List<Course> getCoursesBasicInfos(@RequestParam(value = "ids", required = true) ArrayList<String> ids) {
        return courseManager.getCoursesBasicInfos(ids);
    }

    @RequestMapping(value = "/getCoursesByGrades", method = RequestMethod.GET)
    @ResponseBody
    public List<String> getCoursesByGrades(@RequestParam(value = "grades", required = true) ArrayList<String> grades) {
        return courseManager.getCoursesByGrades(grades);
    }

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public GetCourseRes getCourse(GetCoursesReq getCoursesReq) {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        GetCourseRes courseInfos = courseManager.getCourses(getCoursesReq, exposeEmail);
        return courseInfos;
    }

    @RequestMapping(value = "/updateStudentPreferenceSessionSlot", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public PlatformBasicResponse updateStudentPreferenceSessionSlot(@RequestBody StudentSlotPreferencePojo req) throws Exception {
        courseManager.updateStudentPreferenceSessionSlot(req);
        return new PlatformBasicResponse();
    }

    //Add check for admin/student
    @RequestMapping(value = "/fetchStudentSlotPreference", method = RequestMethod.GET)
    @ResponseBody
    public List<StudentSlotPreferencePojo> fetchStudentSlotPreference(GetBatchesReq req) {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        List<String> studentIds = new ArrayList<>();
        if (ArrayUtils.isNotEmpty(req.getStudentIds())) {
            studentIds.addAll(req.getStudentIds());
        }
        return courseManager.fetchStudentSlotPreference(req.getCourseId(), studentIds, req.getStart(), req.getSize(), exposeEmail);
    }

    @RequestMapping(value = "/getStudentSlotPreference", method = RequestMethod.GET)
    @ResponseBody
    public List<StudentSlotPreferencePojo> getStudentSlotPreference(GetEnrollmentsReq req) throws VException {
        boolean exposeEmail = sessionUtils.isAdminOrStudentCare(sessionUtils.getCurrentSessionData());
        if (req.getUserId() == null || StringUtils.isEmpty(req.getEntityId())) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "userId/entityId");
        }
        sessionUtils.checkIfAllowed(req.getUserId(), Role.STUDENT, Boolean.TRUE);
        return courseManager.fetchStudentSlotPreference(req.getEntityId(), Arrays.asList(req.getUserId().toString()), req.getStart(), req.getSize(), exposeEmail);
    }

    @RequestMapping(value = "/changeStudentPreferenceSessionSlot", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public StudentSlotPreferencePojo changeStudentPreferenceSessionSlot(@RequestBody ChangeStudentSlotReq req) throws Exception {

        sessionUtils.checkIfAllowed(req.getCallingUserId(), Role.STUDENT, Boolean.TRUE);
        return courseManager.changeStudentPreferenceSessionSlot(req);
    }

    public List<String> validate(Course course) {
        List<String> errors = new ArrayList<>();
        Long launchDate = course.getLaunchDate();
        if (launchDate == null || launchDate <= 0) {
            errors.add(Course.Constants.LAUNCH_DATE);
        }
        if (StringUtils.isEmpty(course.getTitle())) {
            errors.add(Course.Constants.TITLE);
        }
        Long duration = course.getDuration();
        if (duration == null || duration <= 0) {
            errors.add(Course.Constants.DURATION);
        }
        List<BatchTiming> batchTimings = course.getBatchTimings();
        if (batchTimings == null) {
            errors.add(Course.Constants.BATCH_TIMINGS);
        } else {
            Long startTime;
            Long endTime;
            for (BatchTiming batchTiming : batchTimings) {
                startTime = batchTiming.getStartTime();
                endTime = batchTiming.getEndTime();
                if (startTime == null || startTime <= 0 || endTime == null || endTime <= 0 || startTime >= endTime) {
                    errors.add(BatchTiming.Constants.START_TIME);
                    errors.add(BatchTiming.Constants.END_TIME);
                }
            }
        }
        List<FAQPojo> faqs = course.getFaq();
        if (faqs == null) {
            errors.add(Course.Constants.FAQ);
        } else {
            for (FAQPojo faq : faqs) {
                if (StringUtils.isEmpty(faq.getQuestion()) || StringUtils.isEmpty(faq.getAnswer())) {
                    errors.add(Course.Constants.FAQ);
                }
            }
        }
        Set<String> grades = course.getGrades();
        if (grades == null || grades.isEmpty()) {
            errors.add(Course.Constants.GRADES);
        }
        Set<String> targets = course.getNormalizeTargets();
        if (targets == null || targets.isEmpty()) {
            errors.add(Course.Constants.TARGETS);
        }
        if (course.getStartPrice() <= 0) {
            errors.add(Course.Constants.START_PRICE);
        }
        if (course.getExitDate() == null || course.getExitDate() <= 0) {
            errors.add(Course.Constants.EXIT_DATE);
        }
        if (ArrayUtils.isEmpty(course.getSearchTerms())) {
            errors.add(Course.Constants.SEARCH_TERMS);
        }
        return errors;
    }

    @RequestMapping(value = "/processCourseRegFeePayment", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void processCourseRegFeePayment(@RequestBody ProcessOtfPostPaymentReq req) throws VException, CloneNotSupportedException {

        courseManager.processCourseRegFeePayment(req.getOrderId(), req.getUserId(), req.getEntityId());
    }

    @RequestMapping(value = "/purchaseAndRegisterCourse", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public OrderInfo purchaseAndRegisterCourse(@RequestBody PurchaseOTFReq req) throws VException, CloneNotSupportedException {
        List<Role> roles = new ArrayList<>();
        roles.add(Role.STUDENT);
        sessionUtils.checkIfAllowedList(null, roles, Boolean.TRUE);
        return courseManager.purchaseAndRegisterCourse(req);
    }

    //for mobile only for now
    @RequestMapping(value = "/getallcourses", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<CourseMobileRes> getallcourses(@RequestParam(value = "subjects", required = false) Set<String> subjects,
            @RequestParam(value = "grade", required = false) String grade,
            @RequestParam(value = "target", required = false) String target,
            @RequestParam(value = "board", required = false) String board,
            @RequestParam(value = "type", required = false) EntityType type,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "free", required = false) Boolean free) throws VException, CloneNotSupportedException {
        //TODO product asked to use only grade and no board for this query
        return courseManager.getCourseMobileRes(null, grade, subjects, free);
    }

    //for mobile only for now
    @RequestMapping(value = "/getmycourses", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<CourseMobileRes> getmycourses(@RequestParam(value = "subjects", required = false) Set<String> subjects,
            @RequestParam(value = "grade", required = false) String grade,
            @RequestParam(value = "target", required = false) String target,
            @RequestParam(value = "board", required = false) String board,
            @RequestParam(value = "type", required = false) EntityType type,
            @RequestParam(value = "start", required = false) Integer start,
            @RequestParam(value = "size", required = false) Integer size,
            @RequestParam(value = "free", required = false) Boolean free) throws VException, CloneNotSupportedException {
        return courseManager.getCoursesForEnrollments(sessionUtils.getCallingUserId(), subjects, free);
    }

    @RequestMapping(value = "/getSubjectsForCourses", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<String> getSubjectForCourse() {
        return Arrays.asList("Physics", "Mathematics", "Chemistry");
    }

    @RequestMapping(value = "/getallcoursesforfeed", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<CourseMobileRes> getallcoursesForFeed(@RequestParam(value = "grade", required = false) String grade,
            @RequestParam(value = "board", required = false) String board,
            @RequestParam(value = "size", required = true) int size) throws VException, CloneNotSupportedException {
        //TODO product asked to use only grade and no board for this query
        return courseManager.getCoursesForFeed(board, grade, size);
    }

    @RequestMapping(value = "/getCourseName/{id}", method = RequestMethod.GET, produces = "application/json")
    public String getCourseName(@PathVariable(value = "id")String id) throws BadRequestException{
        Course course=courseManager.getCourse(id,Arrays.asList(Course.Constants.TITLE),Arrays.asList(Course.Constants._ID));
        if(course==null){
            throw new BadRequestException(ErrorCode.COURSE_NOT_FOUND,"Course not found for course id "+id);
        }
        return course.getTitle();
    }
    
}
