package com.vedantu.subscription.controllers;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vedantu.subscription.entities.sql.HourTransaction;
import com.vedantu.subscription.managers.HourTransactionManager;
import com.vedantu.subscription.viewobject.request.GetHourTransactionReq;
import com.vedantu.util.LogFactory;


@RestController
@RequestMapping("hourTransaction")
public class HourTransactionController {

	@Autowired
	private HourTransactionManager hourTransactionManager;

	@Autowired
	private LogFactory logFactory;
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(HourTransactionController.class);

	@RequestMapping(value = { "getHourTransactions" }, method = RequestMethod.GET)
	@ResponseBody
	public List<HourTransaction> getHourTransactions(GetHourTransactionReq req) throws Throwable {
		List<HourTransaction> response = hourTransactionManager.getHourTransactions(req);
		return response;
	}
}
