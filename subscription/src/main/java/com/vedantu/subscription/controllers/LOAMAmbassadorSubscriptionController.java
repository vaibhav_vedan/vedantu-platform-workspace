package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.User;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.User.enums.SubRole;
import com.vedantu.exception.*;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.managers.LOAMAmbassadorSubscriptionManager;
import com.vedantu.subscription.request.LOAMStudentsReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("loam")
public class LOAMAmbassadorSubscriptionController
{

    @Autowired
    private LOAMAmbassadorSubscriptionManager loamSubscriptionManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(LOAMAmbassadorSubscriptionController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    /*
    @Purpose : Get all the Student data mapped to a Acadmentor.
    */
    @RequestMapping(value = "/loam_getStudents", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public List<Long> loam_getStudents(@RequestBody LOAMStudentsReq loamStudentsReq) throws VException, UnsupportedEncodingException
    {
        logger.info("loam_getStudents caller ip :" + loamStudentsReq.getIpAddress());

        if(null != loamStudentsReq.getCalledFrom() && !loamStudentsReq.getCalledFrom().isEmpty())
        {
            logger.info("loam_getStudents called from :" + loamStudentsReq.getCalledFrom());
        }

        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        Set<SubRole> subRoles = sessionUtils.getCurrentSessionData().getSubRoles();


        if(subRoles==null || !(subRoles.contains(SubRole.ACADMENTOR) || subRoles.contains(SubRole.SUPERMENTOR) ))
        {
            throw new ForbiddenException(ErrorCode.FORBIDDEN_ERROR, "User other than Acedemic Mentor are forbidden to use this API");
        }

        return loamSubscriptionManager.loam_getStudentsByAcadMentor(loamStudentsReq.getAcadMentorEmail());
    }

    @RequestMapping(value = "/loam_getBatchData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Batch> loam_getCBatchData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                               @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return new ArrayList<>();
//        return loamSubscriptionManager.loam_getBatchData(fromTime, thruTime, start, size);

    }

    @RequestMapping(value = "/loam_getCourseData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Course> loam_getCourseData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                           @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {
        return new ArrayList<>();
//        return loamSubscriptionManager.loam_getCourseData(fromTime, thruTime, start, size);

    }

    @RequestMapping(value = "/loam_getEnrollmentData", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<Enrollment> loam_getEnrollmentData(@RequestParam(name = "fromTime", required = true) Long fromTime, @RequestParam(name = "thruTime", required = true) Long thruTime,
                                                   @RequestParam(name = "start", required = true) Integer start, @RequestParam(name = "size", required = true) Integer size) throws BadRequestException, ForbiddenException, NotFoundException, ConflictException, VException {

        return new ArrayList<>();
//        return loamSubscriptionManager.loam_getEnrollmentData(fromTime, thruTime, start, size);

    }

}
