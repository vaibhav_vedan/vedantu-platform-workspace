package com.vedantu.subscription.controllers;

import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.vedantu.User.Role;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.entities.mongo.AcadMentorIntervention;
import com.vedantu.subscription.managers.AcadMentorDashboardManager;
import com.vedantu.subscription.managers.AcadMentorInterventionManager;
import com.vedantu.subscription.viewobject.request.GeneralInterventionReq;
import com.vedantu.subscription.viewobject.response.amdashboard.AMDashboardStudentData;
import com.vedantu.util.LogFactory;
import com.vedantu.util.PlatformBasicResponse;
import com.vedantu.util.security.HttpSessionData;
import com.vedantu.util.security.HttpSessionUtils;
import java.util.Arrays;

@RestController
@RequestMapping("acadmentordashboard")
public class AcadMentorDashboardController {

    @Autowired
    private AcadMentorInterventionManager acadMentorInterventionManager;

    @Autowired
    private AcadMentorDashboardManager acadMentorDashboardManager;

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(AcadMentorDashboardController.class);

    @Autowired
    HttpSessionUtils sessionUtils;

    @RequestMapping(value = "/fetchDataByAcadMentor", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<AMDashboardStudentData> fetchData(@RequestParam(value = "acadMentorEmail") String acadMentorEmail, @RequestParam(value = "fromTime") Long fromTime, @RequestParam(value = "thruTime") Long thruTime) throws VException, UnsupportedEncodingException {
        logger.info("Fetching inclass, assignments and tests from " + fromTime + "to" + thruTime);
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        if (Role.TEACHER.equals(sessionData.getRole()) && !acadMentorEmail.equalsIgnoreCase(sessionData.getEmail())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "action not allowed for this user");
        }
        return acadMentorDashboardManager.fetchDataByAcadMentor(acadMentorEmail, fromTime, thruTime);
    }

    @RequestMapping(value = "/acadMentorInterventions/retrieveOpenInterventions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Map< Long, List<AcadMentorIntervention>> retrieveOpenInterventions(@RequestParam(value = "acadMentorEmail", required = true) String acadMentorEmail) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowedList(null, Arrays.asList(Role.ADMIN, Role.TEACHER), Boolean.TRUE);
        HttpSessionData sessionData = sessionUtils.getCurrentSessionData(true);
        if (Role.TEACHER.equals(sessionData.getRole()) && !acadMentorEmail.equals(sessionData.getEmail())) {
            throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "action not allowed for this u@PathVariable(\"acadMentorId\") Long acadMentorIdser");
        }
        return acadMentorInterventionManager.retrieveOpenInterventions(acadMentorEmail);
    }

    @RequestMapping(value = "/acadMentorInterventions/retrieveInterventionsOfStudent/{studentId}", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public List<AcadMentorIntervention> retrieveInterventionsByStudent(@PathVariable("studentId") Long studentId, @RequestParam(value = "start") Integer start,
            @RequestParam(value = "size") Integer size) throws VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        logger.info("ENTRY:  start" + start + ", size " + size);
        start = (start == null) ? 0 : start;
        size = (size != null && size != 0) ? size : 100;
        return acadMentorInterventionManager.retrieveInterventionsByStudent(studentId, start, size);
    }

    @RequestMapping(value = "/acadMentorInterventions/closeIntervention", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public void closeIntervention(@RequestBody AcadMentorIntervention acadMentorIntervention) throws VException,BadRequestException {
           sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        if (acadMentorIntervention.getStudentId() == null || acadMentorIntervention.getAcadMentorComment() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Either student Id or Acad mentor comment null");
        }
        acadMentorInterventionManager.closeIntervention(acadMentorIntervention);
    }

    @RequestMapping(value = "/acadMentorInterventions/generalIntervention", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    @ResponseBody
    public AcadMentorIntervention generalIntervention(@RequestBody GeneralInterventionReq generalInterventionReq) throws UnsupportedEncodingException, VException {
           sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
        if (generalInterventionReq.getAcadMentorEmail() == null || generalInterventionReq.getComment() == null || generalInterventionReq.getStudentId() == null) {
            throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Enter all the compulsory fields");
        }
        return acadMentorInterventionManager.generalIntervention(generalInterventionReq);
    }

    @Deprecated
    @RequestMapping(value = "/acadMentorInterventions/createInterventions", method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public PlatformBasicResponse createInterventions(@RequestParam(value = "fromTime", required = true) Long fromTime) throws VException, UnsupportedEncodingException {
        sessionUtils.checkIfAllowedList(null, null, Boolean.TRUE);
        return acadMentorInterventionManager.scanAndSaveInterventions(fromTime);
    }
}
