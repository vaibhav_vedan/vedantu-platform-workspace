package com.vedantu.subscription.controllers;

import com.vedantu.User.Role;
import com.vedantu.User.UserBasicInfo;

import java.util.*;

import com.vedantu.subscription.entities.mongo.CoursePlan;
import com.vedantu.subscription.request.GetCoursePlansReq;
import com.vedantu.util.ArrayUtils;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.ForbiddenException;
import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.lms.cmds.enums.EnumBasket;
import com.vedantu.lms.cmds.pojo.TestSectionResult;
import com.vedantu.lms.cmds.pojo.UserTestResultPojo;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.BatchDashboardInfo;
import com.vedantu.onetofew.pojo.EnrollmentPojo;
import com.vedantu.onetofew.request.GetBatchesForDashboardReq;
import com.vedantu.scheduling.pojo.UserSessionAttendance;
import com.vedantu.subscription.managers.DashboardManager;
import com.vedantu.subscription.pojo.TeacherCourseStateCount;
import com.vedantu.subscription.viewobject.response.StudentAcademicOTMInfo;
import com.vedantu.subscription.viewobject.response.DashboardInfoForAM;
import com.vedantu.subscription.viewobject.response.StudentAcademicOTOInfo;
import com.vedantu.subscription.viewobject.response.StudentAcademicOneCycleInfo;
import com.vedantu.subscription.viewobject.response.StudentCoursePlanInfo;
import com.vedantu.subscription.viewobject.response.StudentOTMEnrollmentInfo;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.security.HttpSessionUtils;

@RestController
@RequestMapping("dashboard")
public class DashboardController {

	@Autowired
	private DashboardManager dashboardManager;
        
        @Autowired
        private HttpSessionUtils sessionUtils;

	@Autowired
	private LogFactory logFactory;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(DashboardController.class);

	@RequestMapping(value = "/getBatches", method = RequestMethod.GET)
	@ResponseBody
	public List<BatchDashboardInfo> getBatches(GetBatchesForDashboardReq req) throws NotFoundException {
		return dashboardManager.getBatches(req);
	}

	@RequestMapping(value = "/getBatch", method = RequestMethod.GET)
	@ResponseBody
	public BatchDashboardInfo getBatch(@RequestParam(value = "batchId") String batchId) throws NotFoundException {
		return dashboardManager.getBatch(batchId);
	}

	@RequestMapping(value = "/getEnrollmentsByBatchIds", method = RequestMethod.GET)
	@ResponseBody
	public List<EnrollmentPojo> getEnrollmentsByBatchIds(@RequestParam(value = "batchIds") List<String> batchIds)
			throws BadRequestException, NotFoundException {
		return dashboardManager.getEnrollmentsByBatchIds(batchIds);
	}

	@RequestMapping(value = "/getTeacherEnrollmentCountMapping", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public Map<Long, TeacherCourseStateCount> getTeacherEnrollmentCountMapping(@RequestParam("startTime") Long startTime,
			@RequestParam("endTime") Long endTime) throws BadRequestException, NotFoundException {

		if(startTime == null || endTime == null){
			throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR,"start or endTime not given");
		}
		return dashboardManager.getTeacherEnrollmentCountMapping(startTime, endTime);
	}
        
	@RequestMapping(value = "/getStudentAcademicOTMInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<StudentAcademicOTMInfo> getStudentAcademicOTMInfos(@RequestParam(name = "userId", required = true) String userId) throws BadRequestException, NotFoundException, VException {
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                // Below code not used
                /*
                List<StudentAcademicOTMInfo> result = new ArrayList<>();

                StudentAcademicOTMInfo studentAcademicOTMInfo = new StudentAcademicOTMInfo();
                studentAcademicOTMInfo.setGroupName("abc");
                StudentAcademicOneCycleInfo studentAcademicOneCycleInfo = new StudentAcademicOneCycleInfo();
                studentAcademicOneCycleInfo.setAttendance(36.5f);
                UserTestResultPojo userTestResultPojo = new UserTestResultPojo();
                userTestResultPojo.setAttempted(false);
                userTestResultPojo.setTestName("Chapter test 1");
                studentAcademicOneCycleInfo.setPreMainTestResultObj(Arrays.asList(userTestResultPojo));
                userTestResultPojo = new UserTestResultPojo();
                userTestResultPojo.setAttempted(true);
                userTestResultPojo.setAttemptedQuestion(10);
                userTestResultPojo.setAverageMarks(50f);
                userTestResultPojo.setCorrect(5);
                userTestResultPojo.setInCorrect(5);
                userTestResultPojo.setMarksAchieved(50f);
                userTestResultPojo.setPercentile(50f);
                userTestResultPojo.setTag(EnumBasket.TestTagType.UNIT);
                userTestResultPojo.setTestName("Unit Test 1");
                userTestResultPojo.setTotalMarks(100f);
                userTestResultPojo.setTotalQuestions(10);
                userTestResultPojo.setUnAttemptedQuestion(0);
                userTestResultPojo.setzScore(70f);
                List<TestSectionResult> testSectionResults = new ArrayList<>();
                TestSectionResult testSectionResult = new TestSectionResult();
                testSectionResult.setAttemptedQuestion(5);
                testSectionResult.setCorrect(3);
                testSectionResult.setInCorrect(2);
                testSectionResult.setMarksAchieved(30f);
                testSectionResult.setSectionName("PHYSICS");
                testSectionResult.setTotalMarks(50f);
                testSectionResult.setTotalQuestions(5);
                testSectionResult.setUnAttemptedQuestion(0);
                testSectionResults.add(testSectionResult);
                testSectionResult.setSectionName("CHEMISTRY");
                testSectionResults.add(testSectionResult);
                userTestResultPojo.setTestSectionResults(testSectionResults);
                studentAcademicOneCycleInfo.setMainTestResultObj(userTestResultPojo);
                List<UserSessionAttendance> userSessionAttendances = new ArrayList<>();
                UserSessionAttendance userSessionAttendance = new UserSessionAttendance();
                userSessionAttendance.setAttended(true);
                userSessionAttendance.setContextIds(new HashSet<>(Arrays.asList("testId 1")));
                userSessionAttendance.setSessionTitle("Math 1");
                userSessionAttendance.setStartTime(System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY);
                userSessionAttendances.add(userSessionAttendance);
                studentAcademicOneCycleInfo.setSessionAttendances(userSessionAttendances);
                studentAcademicOTMInfo.setCycles(Arrays.asList(studentAcademicOneCycleInfo));
                result.add(studentAcademicOTMInfo);  */
                // return result;
                // return new ArrayList<>();

		return dashboardManager.getStudentAcademicOTMInfos(userId);
	}        

	@RequestMapping(value = "/getStudentOTMEnrollmentInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<StudentOTMEnrollmentInfo> getStudentOTMEnrollmentInfos(@RequestParam(name = "userId", required = true) String userId) throws BadRequestException, NotFoundException, VException {
        sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                // return new ArrayList<>();
		return dashboardManager.getStudentOTMEnrollments(userId);
	}
        
        @RequestMapping(value = "/getStudentCoursePlanInfos", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<StudentCoursePlanInfo> getStudentCoursePlanInfos(@RequestParam(name = "userId", required = true) String userId) throws BadRequestException, NotFoundException, VException {
            sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                // return new ArrayList<>();
		return dashboardManager.getStudentCoursePlanInfos(userId);
	}

    @RequestMapping(value = "/getStudentsCoursePlanInfos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<StudentCoursePlanInfo> getStudentsCoursePlanInfos(@RequestBody GetCoursePlansReq getCoursePlansReq) throws BadRequestException, NotFoundException, VException {
        return   dashboardManager.getStudentsCoursePlanInfos(getCoursePlansReq);


    }

    @RequestMapping(value = "/getStudentAcademicOTOInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<StudentAcademicOTOInfo> getStudentAcademicOTOInfos(@RequestParam(name = "userId", required = true) String userId) throws BadRequestException, NotFoundException, VException {
                sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
                List<StudentAcademicOTOInfo> studentAcademicOTOInfos = new ArrayList<>();
                StudentAcademicOTOInfo studentAcademicOTOInfo = new StudentAcademicOTOInfo();
                studentAcademicOTOInfo.setAttendance(50f);
                studentAcademicOTOInfo.setCoursePlanId("ide1");
                studentAcademicOTOInfo.setTitle("Course Plan Test");
                List<UserSessionAttendance> userSessionAttendances = new ArrayList<>();
                UserSessionAttendance userSessionAttendance = new UserSessionAttendance();
                userSessionAttendance.setAttended(true);
                userSessionAttendance.setContextIds(new HashSet<>(Arrays.asList("testId 1")));
                userSessionAttendance.setSessionTitle("Math 1");
                userSessionAttendance.setStartTime(System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY);
                userSessionAttendances.add(userSessionAttendance);
                studentAcademicOTOInfo.setSessionAttendances(userSessionAttendances);
                UserTestResultPojo userTestResultPojo = new UserTestResultPojo();
                userTestResultPojo.setAttempted(true);
                userTestResultPojo.setAttemptedQuestion(10);
                userTestResultPojo.setAverageMarks(50f);
                userTestResultPojo.setCorrect(5);
                userTestResultPojo.setInCorrect(5);
                userTestResultPojo.setMarksAchieved(50f);
                userTestResultPojo.setPercentile(50f);
                userTestResultPojo.setTag(EnumBasket.TestTagType.NORMAL);
                userTestResultPojo.setTestName("Unit Test 1");
                userTestResultPojo.setTotalMarks(100f);
                userTestResultPojo.setTotalQuestions(10);
                userTestResultPojo.setUnAttemptedQuestion(0);
                userTestResultPojo.setzScore(70f);
                List<TestSectionResult> testSectionResults = new ArrayList<>();
                TestSectionResult testSectionResult = new TestSectionResult();
                testSectionResult.setAttemptedQuestion(5);
                testSectionResult.setCorrect(3);
                testSectionResult.setInCorrect(2);
                testSectionResult.setMarksAchieved(30f);
                testSectionResult.setSectionName("PHYSICS");
                testSectionResult.setTotalMarks(50f);
                testSectionResult.setTotalQuestions(5);
                testSectionResult.setUnAttemptedQuestion(0);
                testSectionResults.add(testSectionResult);
                testSectionResult.setSectionName("CHEMISTRY");
                testSectionResults.add(testSectionResult);
                userTestResultPojo.setTestSectionResults(testSectionResults);
                List<UserTestResultPojo> userTestResultPojos = new ArrayList<>();
                userTestResultPojos.add(userTestResultPojo);
                studentAcademicOTOInfo.setUserTestResults(userTestResultPojos);
                studentAcademicOTOInfos.add(studentAcademicOTOInfo);
                // return studentAcademicOTOInfos;
		return dashboardManager.getStudentAcademinOTOInfos(userId);
	}
        
        
        @RequestMapping(value = "/getAMDashboardInfo", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody
        public List<DashboardInfoForAM> getAMDashboardInfo() throws VException,ForbiddenException{
            sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
            if(!(Role.ADMIN.equals(sessionUtils.getCallingUserRole()) || Role.STUDENT_CARE.equals(sessionUtils.getCallingUserRole()))){
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "only admin or student care or teacher role allowed");
            }
            
            Long userId = sessionUtils.getCallingUserId();
            List<DashboardInfoForAM> dashboardInfoForAMs = new ArrayList<>();
            DashboardInfoForAM dashboardInfoForAM = new DashboardInfoForAM();
            dashboardInfoForAM.setActiveStudents(50);
            dashboardInfoForAM.setTotalStudents(70);
            dashboardInfoForAM.setGroupName("test");
            dashboardInfoForAMs.add(dashboardInfoForAM);
            // return dashboardInfoForAMs;
            return dashboardManager.getDashboardInfosForAM(userId);
        }
        
        @RequestMapping(value = "/getStudentInfoForAM", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
        @ResponseBody        
        public List<UserBasicInfo> getUserBasicInfos(@RequestParam(name="groupName", required = true) String groupName, @RequestParam(name="status", required = false) EntityStatus entityStatus) throws VException,ForbiddenException{
            sessionUtils.checkIfAllowed(null, Role.ADMIN, Boolean.TRUE);
            
            if(!(Role.ADMIN.equals(sessionUtils.getCallingUserRole()) || Role.STUDENT_CARE.equals(sessionUtils.getCallingUserRole()))){
                throw new ForbiddenException(ErrorCode.ACTION_NOT_ALLOWED, "only admin or student care or teacher role allowed");
            }
            Long userId = sessionUtils.getCallingUserId();
            
            List<UserBasicInfo> userBasicInfos = new ArrayList<>();
            UserBasicInfo userBasicInfo = new UserBasicInfo();
            userBasicInfo.setContactNumber("9999999999");
            userBasicInfo.setEmail("abc@xyz.com");
            userBasicInfo.setFirstName("Bhavesh");
            userBasicInfos.add(userBasicInfo);
            // return userBasicInfos;
            
            return dashboardManager.getUserBasicInfosForAM(userId.toString() ,groupName, entityStatus);
        } 
        
}
