package com.vedantu.subscription.async;

import com.vedantu.async.AppContextManager;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.async.IAsyncQueueName;

public enum AsyncQueueName implements IAsyncQueueName {
    //EMAIL_QUEUE, LEADSQUARED_QUEUE, ES_QUEUE, SMS_QUEUE;
    
    DEFAULT_QUEUE(DefaultQueueExecutor.class);

    private final Class<? extends IAsyncQueueExecutor> asyncExecutorClass;
    
    
    private AsyncQueueName(Class<? extends IAsyncQueueExecutor> asyncQueueExecutor) {
        this.asyncExecutorClass = asyncQueueExecutor;
    }
    
    
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        //TODO: Change this, this is getting the interface proxy bean, hence using this temp approach
        //IAsyncQueueExecutor asyncQueueExecutor = AppContextManager.getAppContext().getBean(asyncExecutorClass);
        String beanName = Character.toLowerCase(asyncExecutorClass.getSimpleName().charAt(0)) + asyncExecutorClass.getSimpleName().substring(1);
        IAsyncQueueExecutor asyncQueueExecutor = (IAsyncQueueExecutor) AppContextManager.getAppContext().getBean(beanName);
        
        asyncQueueExecutor.execute(params);
    }
}

