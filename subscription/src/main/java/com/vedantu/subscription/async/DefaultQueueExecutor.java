package com.vedantu.subscription.async;

import com.google.gson.Gson;
import com.vedantu.User.UserInfo;
import com.vedantu.async.AsyncTaskParams;
import com.vedantu.async.IAsyncQueueExecutor;
import com.vedantu.dinero.enums.PaymentType;
import com.vedantu.dinero.response.InstalmentInfo;
import com.vedantu.dinero.response.OrderInfo;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.notification.enums.CommunicationType;
import com.vedantu.notification.requests.BulkTextSMSRequest;
import com.vedantu.notification.requests.EmailRequest;
import com.vedantu.onetofew.enums.BatchEventsOTF;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.OTFSessionPojoUtils;
import com.vedantu.subscription.entities.mongo.*;
import com.vedantu.subscription.managers.*;
import com.vedantu.subscription.pojo.BatchEnrollmentEmail;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.viewobject.request.AddBatchToBundleReq;
import com.vedantu.subscription.viewobject.request.EndBundleEnrollmentReq;
import com.vedantu.util.ConfigUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.dozer.DozerBeanMapper;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javax.mail.internet.InternetAddress;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * @author somil
 */
@Service
public class DefaultQueueExecutor implements IAsyncQueueExecutor {

    @Autowired
    public LogFactory logFactory;

    @Autowired
    public EnrollmentAsyncManager enrollmentAsyncManager;

    @Autowired
    private AwsSNSManager awsSNSManager;

    @Autowired
    private RegistrationManager registrationManager;

    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private BundleManager bundleManager;

    @Autowired
    private BatchManager batchManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    private AwsSQSManager awsSQSManager;


    @Autowired
    private AwsSQSManager sqsManager;

    @Autowired
    private CoursePlanManager coursePlanManager;

    @Autowired
    private SectionManager sectionManager;

    @Autowired
    private RedisDAO redisDAO;

    @SuppressWarnings("static-access")
    public Logger logger = logFactory.getLogger(DefaultQueueExecutor.class);

    @Autowired
    private DozerBeanMapper mapper;

    String NOTIFICATION_ENDPOINT = ConfigUtils.INSTANCE.getStringValue("NOTIFICATION_ENDPOINT");

    @Async("taskExecutor")
    @Retryable(maxAttempts = 1, backoff = @Backoff(delay = 100, maxDelay = 500))
    @Override
    public void execute(AsyncTaskParams params) throws Exception {
        if (!(params.getAsyncTaskName() instanceof AsyncTaskName)) {
            logger.error(params.getAsyncTaskName() + " not supported");
            return;
        }
        AsyncTaskName taskName = (AsyncTaskName) params.getAsyncTaskName();
        Map<String, Object> payload = params.getPayload();
        String message = null;
        String subject = null;
        switch (taskName) {
            case BATCH_EVENTS_TRIGGER:
                BatchInfo batchInfo = (BatchInfo) payload.get("batchInfo");
                BatchEventsOTF batchEvent = (BatchEventsOTF) payload.get("event");
                if (batchInfo != null) {
                    BatchInfo batch = mapper.map(batchInfo, BatchInfo.class);
                    batch.setAgenda(null);
                    message = new Gson().toJson(batch);
                }
                if (batchEvent != null) {
                    subject = batchEvent.name();
                }
                awsSNSManager.triggerSNS(SNSTopicOTF.BATCH_EVENTS_OTF, subject, message);
                break;
            case ENROLLMENT_EVENTS_TRIGGER:
                Enrollment enrollment = (Enrollment) payload.get("enrollment");
                BatchEventsOTF batchEvents = (BatchEventsOTF) payload.get("event");
                logger.info("ENROLLMENT_EVENTS_TRIGGER \n- enrollment - {} \nbatchEvent - {}",enrollment,batchEvents);
                if (enrollment != null) {
                    EnrollmentPojo enrollmentPojo = mapper.map(enrollment, EnrollmentPojo.class);
                    message = new Gson().toJson(enrollmentPojo);
                }
                if (batchEvents != null) {
                    subject = batchEvents.name();
                }
                awsSNSManager.triggerSNS(SNSTopicOTF.BATCH_EVENTS_OTF, subject, message);
                break;
            case BATCH_UPDATED:
                String batchId1 = (String) payload.get("batchId");
                String key = redisDAO.getBatchRedisKey(batchId1);
                enrollmentManager.deleteRedisKey(key);
                String mongoBatchKey = otfBundleManager.getCachedBatchKey(batchId1);
                enrollmentManager.deleteRedisKey(mongoBatchKey);
                break;
            case ADD_BATCH_TO_BUNDLE:
                AddBatchToBundleReq req = (AddBatchToBundleReq) payload.get("addBatchToBundleReq");
                bundleManager.addBatchToBundle(req);
                break;
            case ADD_PACKAGES_TO_BUNDLE:
                String bundleId = (String) payload.get("bundleId");
                bundleManager.addPackagesInBundle(bundleId);
                break;
            case COURSE_UPDATED:
                String courseId = (String) payload.get("courseId");
                String key1 = redisDAO.getCourseRedisKey(courseId);
                String mongoCourseKey = otfBundleManager.getCachedCourseKey(courseId);
                enrollmentManager.deleteRedisKey(key1);
                enrollmentManager.deleteRedisKey(mongoCourseKey);
                break;
            case UPDATE_ENTITY_TITLE_COURSE_ENROLLMENTS:
                String updatedTitleCourseId = (String) payload.get("courseId");
                String updatedEntityTitle = (String) payload.get("updatedTitle");
                enrollmentAsyncManager.updateEntityTitleForCourseEnrolments(updatedTitleCourseId, updatedEntityTitle);
                break;
            case SEND_EMAIL_TRIGGER:
                CSVEnrollmentPojo csvEnrollmentPojo2 = (CSVEnrollmentPojo) payload.get("csvEnrollmentPojo");
                UserInfo user2 = (UserInfo) payload.get("user");
                Map<String, String> batchCourseMap = (Map<String, String>) payload.get("batchCourseMap");
                Map<String, Course> courseMap = (Map<String, Course>) payload.get("courseMap");
                Map<String, Batch> batchMap = (Map<String, Batch>) payload.get("batchMap");
                String bundleName = (String) payload.get("bundleName");
                HashMap<String, Object> bodyScopes = new HashMap<>();
                bodyScopes.put("bundleName", bundleName);
                List<BatchEnrollmentEmail> courseList = new ArrayList<>();
                for (String batchId : csvEnrollmentPojo2.getBatchIds()) {
                    List<SessionPlanPojo> sessionPlan = batchMap.get(batchId).getSessionPlan();
                    courseList.add(new BatchEnrollmentEmail(courseMap.get(batchCourseMap.get(batchId)).getTitle(), sessionPlan));
                }
                List<OTFSessionPojoUtils> otfsessionlist = enrollmentAsyncManager.getLatestSession(csvEnrollmentPojo2.getBatchIds());
                Long startDate = 0l;
                String whatsAppUrl = null;
                if (otfsessionlist.size() > 0) {
                    startDate = DateTimeUtils.getISTDayStartTime(otfsessionlist.get(0).getStartTime());
                } else {
                    startDate = batchMap.get(csvEnrollmentPojo2.getBatchIds().get(0)).getStartTime();
                    for (String batchId : csvEnrollmentPojo2.getBatchIds()) {
                        if (startDate > batchMap.get(batchId).getStartTime()) {
                            startDate = batchMap.get(batchId).getStartTime();
                        }
                    }
                }
                for (String batchId : csvEnrollmentPojo2.getBatchIds()) {
                    if (StringUtils.isEmpty(whatsAppUrl)) {
                        whatsAppUrl = batchMap.get(batchId).getWhatsappJoinUrl();
                    }
                }

                if (StringUtils.isNotEmpty(whatsAppUrl)) {
                    bodyScopes.put("whatsAppUrl", whatsAppUrl);
                }

                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yy");
                dateFormat.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
                if (startDate != 0l) {
                    bodyScopes.put("startDate", dateFormat.format(new Date(startDate)));
                    bodyScopes.put("dateShow", true);
                } else {
                    bodyScopes.put("dateShow", false);
                }
                bodyScopes.put("subscriptionLink", ConfigUtils.INSTANCE.getStringValue("url.base") + "/mysubscription");
                bodyScopes.put("courseList", courseList);
                communicationManager.sendMailToOTMBundleEnrolledStudents(bodyScopes, csvEnrollmentPojo2.getEmail(), user2.getFullName());
                break;
            case BOOK_CALENDAR_EVENTS_TRIGGER:
                CSVEnrollmentPojo csvEnrollmentPojo = (CSVEnrollmentPojo) payload.get("csvEnrollmentPojo");
                UserInfo user = (UserInfo) payload.get("user");

                for (String batchId : csvEnrollmentPojo.getBatchIds()) {
                    enrollmentAsyncManager.updateCalendar(batchId, user.getUserId().toString());
                }
                break;
            case CREATE_GTT_ATTENDEES_NEW_ENROLLMENT:
                Enrollment enrollment1 = (Enrollment) payload.get("enrollment");
                enrollmentAsyncManager.createGttAttendeeDetails(enrollment1);
                break;
            case END_BATCHS_FOR_BUNDLE:
                List<Enrollment> batchEnrollments = (List<Enrollment>) payload.get("enrollment");
                for (Enrollment _enrollment : batchEnrollments) {
                    logger.info("sending end enrollment request to sqs fifo queue -- " + _enrollment.getId());
                    Map<String, Object> payload1 = new HashMap<>();
                    payload1.put("enrollment", _enrollment);
                    sqsManager.sendToSQS(SQSQueue.BUNDLE_OPS, SQSMessageType.END_BUNDLE_BATCH_ENROLLMENT, new Gson().toJson(payload1));
                }
                break;
            case SHARE_CURRICULUM_EVENTS_TRIGGER:
                CSVEnrollmentPojo csvEnrollmentPojo1 = (CSVEnrollmentPojo) payload.get("csvEnrollmentPojo");
                user = (UserInfo) payload.get("user");
                List<String> batchIds = csvEnrollmentPojo1.getBatchIds();
                Long currentTime = System.currentTimeMillis();
                for (String batchId : batchIds) {
                    ShareContentEnrollmentReq request = new ShareContentEnrollmentReq();
                    request.setContextId(batchId);
                    request.setStudentId(user.getUserId().toString());
                    // request.setStartTime(currentTime);
                    enrollmentAsyncManager.shareContentOnEnrollment(request);
                }
                break;
            case SHARE_CURRICULUM:
                ShareContentEnrollmentReq request = (ShareContentEnrollmentReq) payload.get("shareContentReq");
                enrollmentAsyncManager.shareContentOnEnrollment(request);
                break;
            case UPDATE_BATCH_SEAT:
                List<Enrollment> enrollments = (List<Enrollment>) payload.get("enrollments");
                enrollmentManager.updateEnrollments(enrollments);
                break;

            case END_BUNDLE_REFUND:
                JSONObject markOrderForfeitedReq = (JSONObject) payload.get("markOrderForfeitedReq");
                bundleManager.bundleRefund(markOrderForfeitedReq, (BundleEnrolment) payload.get("enrollments"), (EndBundleEnrollmentReq) payload.get("req"), (Long) payload.get("callingUserId"));
                break;

            case UPDATE_CALENDAR_FOR_ENROL_EVENT:
                EnrollmentUpdatePojo pojo = (EnrollmentUpdatePojo) payload.get("pojo");
                enrollmentAsyncManager.updateCalendarScheduling(pojo);
                break;
            case CHECK_REGISTRATION_FOR_ADVANCED_PAYMENT:
                CSVEnrollmentPojo csvEnrollmentPojo3 = (CSVEnrollmentPojo) payload.get("csvEnrollmentPojo");
                UserInfo user3 = (UserInfo) payload.get("user");
                registrationManager.checkAndUpdateAdvancedPaymentRegistration(EntityType.OTM_BUNDLE_REGISTRATION, csvEnrollmentPojo3.getbundleId(), user3.getUserId());
                break;
            case OTM_BUNDLE_ENROLLMENT_POST_BULK_OR_INSTALMENT_PAYMENT_EMAIL:
                Long biUserd = (Long) payload.get("userId");
                OTFBundleInfo biotfbundleInfo = (OTFBundleInfo) payload.get("otfBundleInfo");
                Integer amountPaid = (Integer) payload.get("amountPaid");
                boolean newEnrollmentsCreated = (boolean) payload.get("newEnrollmentsCreated");
                PaymentType paymentType = (PaymentType) payload.get("paymentType");
                String orderId = (String) payload.get("orderId");
                communicationManager.sendMailForOTMBundleEnrollmentAfterBulkOrInstalmentPayment(biUserd,
                        biotfbundleInfo, amountPaid, newEnrollmentsCreated, paymentType, orderId);
                break;
            case OTM_BUNDLE_FROM_2_ND_INSTALMENT_PAYMENT_EMAIL:
                Long instUserId = (Long) payload.get("userId");
                String instBundleId = (String) payload.get("otfBundleId");
                List<InstalmentInfo> instalments = (List<InstalmentInfo>) payload.get("instalments");
                communicationManager.sendInstalmentPaidEmailFron2ndInstalment(instUserId, instBundleId, instalments);
                break;
            case BUNDLE_FROM_2_ND_INSTALMENT_PAYMENT_EMAIL:
                Long _instUserId = (Long) payload.get("userId");
                String _instBundleId = (String) payload.get("bundleId");
                List<InstalmentInfo> _instalments = (List<InstalmentInfo>) payload.get("instalments");
                communicationManager.sendInstalmentPaidEmailFron2ndInstalmentForBundle(_instUserId, _instBundleId, _instalments);
                break;
            case PROCESS_INSTALMENT_PAYMENT:
                InstalmentInfo instalmentInfo = (InstalmentInfo) payload.get("instalmentInfo");
                switch (instalmentInfo.getContextType()) {
                    case OTF_BUNDLE:
                        otfBundleManager.processInstalmentAfterPayment(instalmentInfo);
                        break;
                    case BATCH:
                        batchManager.processInstalmentAfterPayment(instalmentInfo);
                        break;
                    case BUNDLE:
                        bundleManager.processInstalmentAfterPayment(instalmentInfo);
                        break;
                    default:
                        logger.error("process instalment payment not supported for : " + instalmentInfo.getContextType()
                                + " instalemntInfo : " + instalmentInfo);
                }

                break;
            case CHECK_TRIAL_ENROLLMENT_DUES:
                Integer start = (Integer) payload.get("start");
                Integer size = (Integer) payload.get("size");
                enrollmentAsyncManager.checkTrialEnrollments(start, size);
                break;
            case ENROLLMENT_REACTIVATED:
                Enrollment enrollment2 = (Enrollment) payload.get("enrollment");
                communicationManager.sendOTFReactivatedMail(enrollment2);
                break;
            case TRIGGER_ENROLLMENT_STATUS_EVENTS:
                Enrollment enrollment3 = (Enrollment) payload.get("enrollment");
                Map<String, Enrollment> newEnrollmentMap = new HashMap<>();
                newEnrollmentMap.put(enrollment3.getId(), enrollment3);
                otfBundleManager.enrollmentTriggers(newEnrollmentMap.keySet(), new HashMap<>(), newEnrollmentMap, false);
                break;
            case POST_OTM_BUNDLE_REGISTRATION_EMAIL:
                Long userId2 = (Long) payload.get("userId");
                OTFBundleInfo otfBundleInfo = (OTFBundleInfo) payload.get("otfBundleInfo");
                float paidAmount = (float) payload.get("paidAmount");
                CommunicationType type = (CommunicationType) payload.get("communicationType");
                communicationManager.sendOTMBundlePurchaseEmail(userId2, otfBundleInfo, paidAmount, type);
                break;
            case UPDATE_DELIVERABLE_ENTITY_ID_ON_ENROLL:
                String entityId = (String) payload.get("entityId");
                Long userId3 = (Long) payload.get("userId");
                List<Enrollment> enrollments2 = (List<Enrollment>) payload.get("enrollments");
                otfBundleManager.updateDeliverableEntityId(null, userId3, entityId, enrollments2);
                break;
            case UPDATE_DELIVERABLE_ENTITY_ID_ON_PAYMENT:
                String orderId3 = (String) payload.get("orderId");
                String entityId2 = (String) payload.get("entityId");
                Long userId4 = (Long) payload.get("userId");
                OTFBundleInfo bundleInfo = (OTFBundleInfo) payload.get("bundleInfo");
                otfBundleManager.updateDeliverableEntityId(orderId3, userId4, entityId2, bundleInfo);
                break;
            case EXPORT_TRIAL_ENROLLMENTS:
                String name = (String) payload.get("name");
                String email = (String) payload.get("email");
                List<String> ccList = (List<String>) payload.get("ccList");
                enrollmentAsyncManager.exportTrialEnrollments(email, name, ccList);
                break;
            case OTF_ENROLLMENT_EMAIL:
                Enrollment enrollment_new = (Enrollment) payload.get("enrollment");
                OrderInfo order = (OrderInfo) payload.get("orderInfo");
                Boolean newEnrollmentCreated = (Boolean) payload.get("newEnrollmentCreated");
                communicationManager.sendOTFEnrollmentEmail(enrollment_new, order, newEnrollmentCreated);
                break;
            case OTF_UPDATE_CALENDAR:
                String updateCalUserId = (String) payload.get("userId");
                String updateCalBatchId = (String) payload.get("batchId");
                EntityStatus status = (EntityStatus) payload.get("status");
                enrollmentAsyncManager.updateCalendar(updateCalUserId, updateCalBatchId, status);
                break;
            case OTF_UNMARK_CALENDAR:
                enrollmentAsyncManager.unmarkCalendar((String) payload.get("userId"), (String) payload.get("batchId"));
                break;

            case REMOVE_GTT:

                enrollmentAsyncManager.removeGTT((String) payload.get("userId"), (String) payload.get("batchId"),
                        new Gson().fromJson((String) payload.get("enrolmentInfo"), EnrollmentPojo.class));

                break;

            case PAY_INSTALMENT_OTF:
                Long otfUserId = (Long) payload.get("userId");
                String batchId = (String) payload.get("batchId");
                List<InstalmentInfo> instalmentInfos = (List<InstalmentInfo>) payload.get("instalments");
                communicationManager.sendInstalmentPaidEmailOTF(otfUserId, batchId, instalmentInfos);
                break;
            case TRIGGER_SQS:
                SQSQueue queue = (SQSQueue) payload.get("queue");
                SQSMessageType messageType = (SQSMessageType) payload.get("messageType");
                String sqsMessage = (String) payload.get("message");
                String groupId = (String) payload.get("groupId");

                awsSQSManager.sendToSQS(queue, messageType, sqsMessage, groupId);
                break;
            case END_BATCH_CONSUMPTION:
                batchManager.checkForEndBatchConsumption();
                break;
            case CONTENT_BATCH_CONSUMPTION:
                batchManager.checkForContentOnlyBatchConsumption();
                break;
            case PROCESS_COURSE_PLAN_HOURS:
                String id = (String) payload.get("id");
                coursePlanManager.checkCoursePlanHoursAboutToEnd(id);
                break;
            case AUTO_ENROLL_ON_COURSE_ADDITION_IN_BUNDLE:
                break;
            case REMOVE_ENROLL_ON_COURSE_REMOVAL_FROM_BUNDLE:
                String removeenrollbundleid = (String) payload.get("bundleId");
                String removeenrollotmbundleid = (String) payload.get("otmBundleId");
                bundleManager.removeEnrollForAllEnrolledUserIdsInBundle(removeenrollbundleid, removeenrollotmbundleid);
                break;
            case SET_UNLIMITED_DOUBTS_BUNDLE:
                String bundleid = (String) payload.get("bundleId");
                Boolean unlimiteDoubts = (Boolean) payload.get("unlimitedDoubts");
                Long callingUserId = (Long) payload.get("callingUserId");
                bundleManager.setUnlimitedDoubtsBundle(bundleid, unlimiteDoubts, callingUserId);

                break;
            case BUNDLE_AUTO_ENROLL:
                String bundle = (String) payload.get("entityId");
                String userId = (String) payload.get("userId");
                String enrollmentId = (String) payload.get("enrollmentId");
                bundleManager.autoEnrollInBundleCourses(bundle, userId, enrollmentId);

                break;

            case TERMINATE_FREE_PASS:
                bundleManager.terminateFreePass();
                break;
            case SEND_BULK_EMAIL:
                EmailRequest emailRequest = (EmailRequest) payload.get("emailRequest");
                List<InternetAddress> emailTo = (List<InternetAddress>) payload.get("emailTo");
                emailTo.forEach(to -> {
                    emailRequest.setTo(Arrays.asList(to));
                    communicationManager.sendBulkEmail(emailRequest);
                });
                break;

            case SEND_BULK_SMS:
                BulkTextSMSRequest bulkTextSMSRequest = (BulkTextSMSRequest) payload.get("bulkTextSMSRequest");
                communicationManager.sendBulkSMS(bulkTextSMSRequest);
                break;
            case SET_MAIN_TAGS_IN_BUNDLE:
                bundleManager.setMainTagInfoMap((Bundle) payload.get("bundle"), (Long) payload.get("userId"));
                break;
            case GET_DEMO_SESSION_SUPERKIDS:
                Map<String,String> userLeads= (Map<String, String>) payload.get("userLeads");
                batchManager.sendPaymentStartLeadSquareSuperKids(userLeads);
                break;
            case UPDATE_BATCH_TO_BUNDLE:
                bundleManager.editBatchInBundle((AddBatchToBundleReq) payload.get("addBatchToBundleReq"));
                break;
            case REMOVE_BATCH_FROM_BUNDLE:
                bundleManager.removeBatch((String) payload.get("batchId"));
                break;
            case ACADOPS_SECTION_FILLING_ALERTS:
                logger.info("IN_ASYNC_ACADOPS_SECTION_FILLING_ALERTS");
                String res = (String)payload.get("batchId");
                sectionManager.sendAlertsToAcadOps(res);
                break;
            default:
                logger.error("Logic not defined for task:" + taskName);
        }
    }

    @Recover
    @Override
    public void recover(Exception exception) {
        logger.error("exception thrown in async task" + Thread.currentThread().getName(), exception);
    }

}
