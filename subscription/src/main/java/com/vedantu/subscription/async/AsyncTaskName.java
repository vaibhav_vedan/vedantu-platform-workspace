package com.vedantu.subscription.async;

import com.vedantu.async.IAsyncTaskName;

/**
 * @author somil
 */
public enum AsyncTaskName implements IAsyncTaskName {

    BATCH_EVENTS_TRIGGER(AsyncQueueName.DEFAULT_QUEUE),
    ENROLLMENT_EVENTS_TRIGGER(AsyncQueueName.DEFAULT_QUEUE),
    BATCH_UPDATED(AsyncQueueName.DEFAULT_QUEUE),
    ADD_BATCH_TO_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    ADD_PACKAGES_TO_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_BATCH_TO_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    REMOVE_BATCH_FROM_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    COURSE_UPDATED(AsyncQueueName.DEFAULT_QUEUE),
    BOOK_CALENDAR_EVENTS_TRIGGER(AsyncQueueName.DEFAULT_QUEUE),
    SHARE_CURRICULUM_EVENTS_TRIGGER(AsyncQueueName.DEFAULT_QUEUE),
    SEND_EMAIL_TRIGGER(AsyncQueueName.DEFAULT_QUEUE),
    CHECK_REGISTRATION_FOR_ADVANCED_PAYMENT(AsyncQueueName.DEFAULT_QUEUE),
    SHARE_CURRICULUM(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_BATCH_SEAT(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_CALENDAR_FOR_ENROL_EVENT(AsyncQueueName.DEFAULT_QUEUE),
    OTM_BUNDLE_ENROLLMENT_POST_BULK_OR_INSTALMENT_PAYMENT_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    OTM_BUNDLE_FROM_2_ND_INSTALMENT_PAYMENT_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    PROCESS_INSTALMENT_PAYMENT(AsyncQueueName.DEFAULT_QUEUE),
    CHECK_TRIAL_ENROLLMENT_DUES(AsyncQueueName.DEFAULT_QUEUE),
    ENROLLMENT_REACTIVATED(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_ENROLLMENT_STATUS_EVENTS(AsyncQueueName.DEFAULT_QUEUE),
    POST_OTM_BUNDLE_REGISTRATION_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_DELIVERABLE_ENTITY_ID_ON_ENROLL(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_DELIVERABLE_ENTITY_ID_ON_PAYMENT(AsyncQueueName.DEFAULT_QUEUE),
    EXPORT_TRIAL_ENROLLMENTS(AsyncQueueName.DEFAULT_QUEUE),
    OTF_ENROLLMENT_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    OTF_UPDATE_CALENDAR(AsyncQueueName.DEFAULT_QUEUE),
    PAY_INSTALMENT_OTF(AsyncQueueName.DEFAULT_QUEUE),
    TRIGGER_SQS(AsyncQueueName.DEFAULT_QUEUE),
    END_BATCH_CONSUMPTION(AsyncQueueName.DEFAULT_QUEUE),
    CONTENT_BATCH_CONSUMPTION(AsyncQueueName.DEFAULT_QUEUE),
    CREATE_GTT_ATTENDEES_NEW_ENROLLMENT(AsyncQueueName.DEFAULT_QUEUE),
    PROCESS_COURSE_PLAN_HOURS(AsyncQueueName.DEFAULT_QUEUE),
    BUNDLE_FROM_2_ND_INSTALMENT_PAYMENT_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    AUTO_ENROLL_ON_COURSE_ADDITION_IN_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    REMOVE_ENROLL_ON_COURSE_REMOVAL_FROM_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    BUNDLE_AUTO_ENROLL(AsyncQueueName.DEFAULT_QUEUE),
    TERMINATE_FREE_PASS(AsyncQueueName.DEFAULT_QUEUE),
    SET_UNLIMITED_DOUBTS_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    SEND_BULK_EMAIL(AsyncQueueName.DEFAULT_QUEUE),
    SEND_BULK_SMS(AsyncQueueName.DEFAULT_QUEUE),
    OTF_UNMARK_CALENDAR(AsyncQueueName.DEFAULT_QUEUE),
    REMOVE_GTT(AsyncQueueName.DEFAULT_QUEUE),
    END_BATCHS_FOR_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    END_BUNDLE_REFUND(AsyncQueueName.DEFAULT_QUEUE),
    SET_MAIN_TAGS_IN_BUNDLE(AsyncQueueName.DEFAULT_QUEUE),
    GET_DEMO_SESSION_SUPERKIDS(AsyncQueueName.DEFAULT_QUEUE),
    ACADOPS_SECTION_FILLING_ALERTS(AsyncQueueName.DEFAULT_QUEUE),
    UPDATE_ENTITY_TITLE_COURSE_ENROLLMENTS(AsyncQueueName.DEFAULT_QUEUE);
    private AsyncQueueName queue;

    private AsyncTaskName(AsyncQueueName queue) {
        this.queue = queue;
    }

    private AsyncTaskName() {
        this.queue = AsyncQueueName.DEFAULT_QUEUE;
    }

    @Override
    public AsyncQueueName getQueue() {
        return queue;
    }

}
