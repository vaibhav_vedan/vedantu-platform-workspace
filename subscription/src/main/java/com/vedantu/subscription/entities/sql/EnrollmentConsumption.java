package com.vedantu.subscription.entities.sql;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.subscription.enums.RefundPolicyType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;

@Entity
@Table(name="EnrollmentConsumption", uniqueConstraints =
        { @UniqueConstraint(columnNames = { "enrollmentId" }) })
public class EnrollmentConsumption extends AbstractSqlLongIdEntity {

    private String enrollmentId;
    private String batchId;
    private String courseId;
    private String userId;
    private Long duration;
    private Long markTrailInactiveTime;
    private Integer regAmtPaid=0;
    private Integer regAmtPaidP=0;
    private Integer regAmtPaidNP=0;
    private Integer regAmtPaidFromDiscount=0;
    private Integer regAmtLeft=0;
    private Integer regAmtPLeft=0;
    private Integer regAmtNPLeft=0;
    private Integer regAmtLeftFromDiscount=0;
    private Integer amtToPay=0;
    private Integer amtPaid=0;
    private Integer amtPaidP=0;
    private Integer amtPaidNP=0;
    private Integer amtPaidFromDiscount=0;
    private Integer amtPaidLeft=0;
    private Integer amtPaidPLeft=0;
    private Integer amtPaidNPLeft=0;
    private Integer amtPaidFromDiscountLeft=0;
    private Float discountToAmtRatio;
    private Integer hourlyrate=0;//amount in paisa per hr
    private Integer negativeMargin=0;
    private boolean enrollmentEnded = false;
    private boolean batchConsumptionDone = false;
    @Enumerated(EnumType.STRING)
    private RefundPolicyType refundPolicyType;


    public String getEnrollmentId() {
        return enrollmentId;
    }

    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Long getMarkTrailInactiveTime() {
        return markTrailInactiveTime;
    }

    public void setMarkTrailInactiveTime(Long markTrailInactiveTime) {
        this.markTrailInactiveTime = markTrailInactiveTime;
    }

    public Integer getRegAmtLeft() {
        if(regAmtLeft==null){
            regAmtLeft=0;
        }
        return regAmtLeft;
    }

    public void setRegAmtLeft(Integer regAmtLeft) throws InternalServerErrorException{
        if(regAmtLeft != null && regAmtLeft < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtLeft can't be less than zero");
        }
        this.regAmtLeft = regAmtLeft;
    }

    public Integer getRegAmtPLeft() {
        if(regAmtPLeft==null){
            regAmtPLeft=0;
        }
        return regAmtPLeft;
    }

    public void setRegAmtPLeft(Integer regAmtPLeft) throws InternalServerErrorException{
        if(regAmtPLeft != null && regAmtPLeft < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtPLeft can't be less than zero");
        }
        this.regAmtPLeft = regAmtPLeft;
    }

    public Integer getRegAmtNPLeft() {
        if(regAmtNPLeft==null){
            regAmtNPLeft=0;
        }
        return regAmtNPLeft;
    }

    public void setRegAmtNPLeft(Integer regAmtNPLeft) throws InternalServerErrorException{
        if(regAmtNPLeft != null && regAmtNPLeft < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtNPLeft can't be less than zero");
        }
        this.regAmtNPLeft = regAmtNPLeft;
    }

    public Integer getRegAmtLeftFromDiscount() {
        if(regAmtLeftFromDiscount==null){
            regAmtLeftFromDiscount=0;
        }
        return regAmtLeftFromDiscount;
    }

    public void setRegAmtLeftFromDiscount(Integer regAmtLeftFromDiscount) throws InternalServerErrorException{
        if(regAmtLeftFromDiscount != null && regAmtLeftFromDiscount < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtLeftFromDiscount can't be less than zero");
        }
        this.regAmtLeftFromDiscount = regAmtLeftFromDiscount;
    }

    public Integer getRegAmtPaid() {
        if(regAmtPaid==null){
            regAmtPaid=0;
        }
        return regAmtPaid;
    }

    public void setRegAmtPaid(Integer regAmtPaid) throws InternalServerErrorException{
        if(regAmtPaid != null && regAmtPaid < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtPaid can't be less than zero");
        }
        this.regAmtPaid = regAmtPaid;
    }

    public Integer getRegAmtPaidP() {
        if(regAmtPaidP==null){
            regAmtPaidP=0;
        }
        return regAmtPaidP;
    }

    public void setRegAmtPaidP(Integer regAmtPaidP) throws InternalServerErrorException {
        if(regAmtPaidP != null && regAmtPaidP < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtPaidP can't be less than zero");
        }
        this.regAmtPaidP = regAmtPaidP;
    }

    public Integer getRegAmtPaidNP() {
        if(regAmtPaidNP==null){
            regAmtPaidNP=0;
        }
        return regAmtPaidNP;
    }

    public void setRegAmtPaidNP(Integer regAmtPaidNP) throws InternalServerErrorException{
        if(regAmtPaidNP != null && regAmtPaidNP < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtPaidNP can't be less than zero");
        }
        this.regAmtPaidNP = regAmtPaidNP;
    }

    public Integer getRegAmtPaidFromDiscount() {
        if(regAmtPaidFromDiscount==null){
            regAmtPaidFromDiscount=0;
        }
        return regAmtPaidFromDiscount;
    }

    public void setRegAmtPaidFromDiscount(Integer regAmtPaidFromDiscount) throws InternalServerErrorException{
        if(regAmtPaidFromDiscount != null && regAmtPaidFromDiscount < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "regAmtPaidFromDiscount can't be less than zero");
        }
        this.regAmtPaidFromDiscount = regAmtPaidFromDiscount;
    }

    public Integer getAmtToPay() {
        if(amtToPay==null){
            amtToPay=0;
        }
        return amtToPay;
    }

    public void setAmtToPay(Integer amtToPay) throws InternalServerErrorException {
        if(amtToPay != null && amtToPay < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtToPay can't be less than zero");
        }
        this.amtToPay = amtToPay;
    }

    public Integer getAmtPaid() {
        if(amtPaid==null){
            amtPaid=0;
        }
        return amtPaid;
    }

    public void setAmtPaid(Integer amtPaid) throws InternalServerErrorException {
        if(amtPaid != null && amtPaid < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaid can't be less than zero");
        }
        this.amtPaid = amtPaid;
    }

    public Integer getAmtPaidP() {
        if(amtPaidP==null){
            amtPaidP=0;
        }
        return amtPaidP;
    }

    public void setAmtPaidP(Integer amtPaidP) throws InternalServerErrorException {
        if(amtPaidP != null && amtPaidP < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaidP can't be less than zero");
        }
        this.amtPaidP = amtPaidP;
    }

    public Integer getAmtPaidNP() {
        if(amtPaidNP==null){
            amtPaidNP=0;
        }
        return amtPaidNP;
    }

    public void setAmtPaidNP(Integer amtPaidNP) throws InternalServerErrorException{
        if(amtPaidNP != null && amtPaidNP < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaidNP can't be less than zero");
        }
        this.amtPaidNP = amtPaidNP;
    }

    public Integer getAmtPaidFromDiscount() {
        if(amtPaidFromDiscount==null){
            amtPaidFromDiscount=0;
        }
        return amtPaidFromDiscount;
    }

    public void setAmtPaidFromDiscount(Integer amtPaidFromDiscount) throws InternalServerErrorException {
        if(amtPaidFromDiscount != null && amtPaidFromDiscount < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaidFromDiscount can't be less than zero");
        }
        this.amtPaidFromDiscount = amtPaidFromDiscount;
    }

    public Integer getAmtPaidLeft() {
        if(amtPaidLeft==null){
            amtPaidLeft=0;
        }
        return amtPaidLeft;
    }

    public void setAmtPaidLeft(Integer amtPaidLeft) throws InternalServerErrorException {
        if(amtPaidLeft != null && amtPaidLeft < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaidLeft can't be less than zero");
        }
        this.amtPaidLeft = amtPaidLeft;
    }

    public Integer getAmtPaidPLeft() {
        if(amtPaidPLeft==null){
            amtPaidPLeft=0;
        }
        return amtPaidPLeft;
    }

    public void setAmtPaidPLeft(Integer amtPaidPLeft) throws InternalServerErrorException {
        if(amtPaidPLeft != null && amtPaidPLeft < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaidPLeft can't be less than zero");
        }
        this.amtPaidPLeft = amtPaidPLeft;
    }

    public Integer getAmtPaidNPLeft() {
        if(amtPaidNPLeft==null){
            amtPaidNPLeft=0;
        }
        return amtPaidNPLeft;
    }

    public void setAmtPaidNPLeft(Integer amtPaidNPLeft) throws InternalServerErrorException {
        if(amtPaidNPLeft != null && amtPaidNPLeft < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaidNPLeft can't be less than zero");
        }
        this.amtPaidNPLeft = amtPaidNPLeft;
    }

    public Integer getAmtPaidFromDiscountLeft() {
        if(amtPaidFromDiscountLeft==null){
            amtPaidFromDiscountLeft=0;
        }
        return amtPaidFromDiscountLeft;
    }

    public void setAmtPaidFromDiscountLeft(Integer amtPaidFromDiscountLeft) throws InternalServerErrorException {
        if(amtPaidFromDiscountLeft != null && amtPaidFromDiscountLeft < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "amtPaidFromDiscountLeft can't be less than zero");
        }
        this.amtPaidFromDiscountLeft = amtPaidFromDiscountLeft;
    }

    public Float getDiscountToAmtRatio() {
        return discountToAmtRatio;
    }

    public void setDiscountToAmtRatio(Float discountToAmtRatio) {
        this.discountToAmtRatio = discountToAmtRatio;
    }

    public Integer getHourlyrate() {
        return hourlyrate;
    }

    public void setHourlyrate(Integer hourlyrate) {
        this.hourlyrate = hourlyrate;
    }

    public Integer getNegativeMargin() {
        if(negativeMargin==null){
            negativeMargin=0;
        }
        return negativeMargin;
    }

    public void setNegativeMargin(Integer negativeMargin) throws InternalServerErrorException {
        if(negativeMargin!=null && negativeMargin < 0){
            throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, "negativeMargin cannot be less than zero");
        }
        this.negativeMargin = negativeMargin;
    }

    public RefundPolicyType getRefundPolicyType() {
        return refundPolicyType;
    }

    public void setRefundPolicyType(RefundPolicyType refundPolicyType) {
        this.refundPolicyType = refundPolicyType;
    }

    /**
     * @return the courseId
     */
    public String getCourseId() {
        return courseId;
    }

    /**
     * @param courseId the courseId to set
     */
    public void setCourseId(String courseId) {
        this.courseId = courseId;
    }

    /**
     * @return the enrollmentEnded
     */
    public boolean isEnrollmentEnded() {
        return enrollmentEnded;
    }

    /**
     * @param enrollmentEnded the enrollmentEnded to set
     */
    public void setEnrollmentEnded(boolean enrollmentEnded) {
        this.enrollmentEnded = enrollmentEnded;
    }

    /**
     * @return the batchConsumptionDone
     */
    public boolean isBatchConsumptionDone() {
        return batchConsumptionDone;
    }

    /**
     * @param batchConsumptionDone the batchConsumptionDone to set
     */
    public void setBatchConsumptionDone(boolean batchConsumptionDone) {
        this.batchConsumptionDone = batchConsumptionDone;
    }
}
