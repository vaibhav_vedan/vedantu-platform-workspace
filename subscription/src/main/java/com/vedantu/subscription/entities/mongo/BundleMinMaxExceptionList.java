package com.vedantu.subscription.entities.mongo;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "BundleMinMaxExceptionList")
public class BundleMinMaxExceptionList extends AbstractMongoStringIdEntity {

    private Long userId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String USERID = "userId";
    }
}
