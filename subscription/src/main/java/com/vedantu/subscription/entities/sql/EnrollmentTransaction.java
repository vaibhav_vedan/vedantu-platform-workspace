package com.vedantu.subscription.entities.sql;

import com.vedantu.onetofew.enums.EntityStatus;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.enums.EnrollmentTransactionType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;

@Entity
@Table(name="EnrollmentTransaction", uniqueConstraints = {
    @UniqueConstraint(columnNames = { "enrollmentId", "enrollmentTransactionContextType", "enrollmentTransactionContextId"})})
public class EnrollmentTransaction extends AbstractSqlLongIdEntity {

    private Integer regAmtPaid=0;
    private Integer regAmtPaidP=0;
    private Integer regAmtPaidNP=0;
    private Integer regAmtPaidFromDiscount=0;
    private Integer regAmtLeft=0;
    private Integer regAmtLeftP=0;
    private Integer regAmtLeftNP=0;
    private Integer regAmtLeftFromDiscount=0;
    private Long enrollmentConsumptionId;
    private String enrollmentId;
    private Integer amtPaid=0;
    private Integer amtPaidP=0;
    private Integer amtPaidNP=0;
    private Integer amtPaidFromDiscount=0;
    private boolean negativeMargin;
    private Integer amtLeft=0;
    private Integer amtLeftP=0;
    private Integer amtLeftNP=0;
    private Integer amtLeftFromDiscount=0;
    private Integer negativeMarginLeft;
    @Enumerated(EnumType.STRING)
    private EnrollmentTransactionContextType enrollmentTransactionContextType;
    private String enrollmentTransactionContextId;
    @Enumerated(EnumType.STRING)
    private EntityStatus consumptionType;
    @Enumerated(EnumType.STRING)
    private EnrollmentTransactionType enrollmentTransactionType;
    private String remarks;

    public Integer getRegAmtPaid() {
        return regAmtPaid;
    }

    public void setRegAmtPaid(Integer regAmtPaid) {
        this.regAmtPaid = regAmtPaid;
    }

    public Integer getRegAmtPaidP() {
        return regAmtPaidP;
    }

    public void setRegAmtPaidP(Integer regAmtPaidP) {
        this.regAmtPaidP = regAmtPaidP;
    }

    public Integer getRegAmtPaidNP() {
        return regAmtPaidNP;
    }

    public void setRegAmtPaidNP(Integer regAmtPaidNP) {
        this.regAmtPaidNP = regAmtPaidNP;
    }

    public Integer getRegAmtPaidFromDiscount() {
        return regAmtPaidFromDiscount;
    }

    public void setRegAmtPaidFromDiscount(Integer regAmtPaidFromDiscount) {
        this.regAmtPaidFromDiscount = regAmtPaidFromDiscount;
    }

    public Integer getRegAmtLeft() {
        return regAmtLeft;
    }

    public void setRegAmtLeft(Integer regAmtLeft) {
        this.regAmtLeft = regAmtLeft;
    }

    public Integer getRegAmtLeftP() {
        return regAmtLeftP;
    }

    public void setRegAmtLeftP(Integer regAmtLeftP) {
        this.regAmtLeftP = regAmtLeftP;
    }

    public Integer getRegAmtLeftNP() {
        return regAmtLeftNP;
    }

    public void setRegAmtLeftNP(Integer regAmtLeftNP) {
        this.regAmtLeftNP = regAmtLeftNP;
    }

    public Long getEnrollmentConsumptionId() {
        return enrollmentConsumptionId;
    }

    public void setEnrollmentConsumptionId(Long enrollmentConsumptionId) {
        this.enrollmentConsumptionId = enrollmentConsumptionId;
    }

    public Integer getAmtPaid() {
        return amtPaid;
    }

    public void setAmtPaid(Integer amtPaid) {
        this.amtPaid = amtPaid;
    }

    public Integer getAmtPaidP() {
        return amtPaidP;
    }

    public void setAmtPaidP(Integer amtPaidP) {
        this.amtPaidP = amtPaidP;
    }

    public Integer getAmtPaidNP() {
        return amtPaidNP;
    }

    public void setAmtPaidNP(Integer amtPaidNP) {
        this.amtPaidNP = amtPaidNP;
    }

    public Integer getAmtPaidFromDiscount() {
        return amtPaidFromDiscount;
    }

    public void setAmtPaidFromDiscount(Integer amtPaidFromDiscount) {
        this.amtPaidFromDiscount = amtPaidFromDiscount;
    }

    public boolean isNegativeMargin() {
        return negativeMargin;
    }

    public void setNegativeMargin(boolean negativeMargin) {
        this.negativeMargin = negativeMargin;
    }

    public Integer getAmtLeft() {
        return amtLeft;
    }

    public void setAmtLeft(Integer amtLeft) {
        this.amtLeft = amtLeft;
    }

    public Integer getAmtLeftP() {
        return amtLeftP;
    }

    public void setAmtLeftP(Integer amtLeftP) {
        this.amtLeftP = amtLeftP;
    }

    public Integer getAmtLeftNP() {
        return amtLeftNP;
    }

    public void setAmtLeftNP(Integer amtLeftNP) {
        this.amtLeftNP = amtLeftNP;
    }

    public Integer getAmtLeftFromDiscount() {
        return amtLeftFromDiscount;
    }

    public void setAmtLeftFromDiscount(Integer amtLeftFromDiscount) {
        this.amtLeftFromDiscount = amtLeftFromDiscount;
    }

    public Integer getNegativeMarginLeft() {
        return negativeMarginLeft;
    }

    public void setNegativeMarginLeft(Integer negativeMarginLeft) {
        this.negativeMarginLeft = negativeMarginLeft;
    }

    public EnrollmentTransactionContextType getEnrollmentTransactionContextType() {
        return enrollmentTransactionContextType;
    }

    public void setEnrollmentTransactionContextType(EnrollmentTransactionContextType enrollmentTransactionContextType) {
        this.enrollmentTransactionContextType = enrollmentTransactionContextType;
    }

    public String getEnrollmentTransactionContextId() {
        return enrollmentTransactionContextId;
    }

    public void setEnrollmentTransactionContextId(String enrollmentTransactionContextId) {
        this.enrollmentTransactionContextId = enrollmentTransactionContextId;
    }

    public EntityStatus getConsumptionType() {
        return consumptionType;
    }

    public void setConsumptionType(EntityStatus consumptionType) {
        this.consumptionType = consumptionType;
    }

    public EnrollmentTransactionType getEnrollmentTransactionType() {
        return enrollmentTransactionType;
    }

    public void setEnrollmentTransactionType(EnrollmentTransactionType enrollmentTransactionType) {
        this.enrollmentTransactionType = enrollmentTransactionType;
    }

    public Integer getRegAmtLeftFromDiscount() {
        return regAmtLeftFromDiscount;
    }

    public void setRegAmtLeftFromDiscount(Integer regAmtLeftFromDiscount) {
        this.regAmtLeftFromDiscount = regAmtLeftFromDiscount;
    }

    /**
     * @return the enrollmentId
     */
    public String getEnrollmentId() {
        return enrollmentId;
    }

    /**
     * @param enrollmentId the enrollmentId to set
     */
    public void setEnrollmentId(String enrollmentId) {
        this.enrollmentId = enrollmentId;
    }

    /**
     * @return the remarks
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * @param remarks the remarks to set
     */
    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
