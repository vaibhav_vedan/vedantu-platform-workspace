package com.vedantu.subscription.entities.mongo;

import com.vedantu.subscription.pojo.RewardDistributionInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;


/**
 * @author mano
 */
@Getter
@Setter
@Document
public class GameRewardItem extends AbstractMongoStringIdEntity {
    @Indexed(unique = true)
    private String name;
    private Set<RewardDistributionInfo> distributionInfo;

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String NAME = "name";
    }
}
