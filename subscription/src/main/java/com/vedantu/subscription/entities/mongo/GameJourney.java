package com.vedantu.subscription.entities.mongo;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.subscription.enums.game.*;
import com.vedantu.subscription.managers.GameManager;
import com.vedantu.subscription.pojo.game.GameMetaData;
import com.vedantu.subscription.pojo.game.GameSubtask;
import com.vedantu.subscription.pojo.game.GameTask;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.logging.log4j.Level;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author anirudha
 */
@Data
@NoArgsConstructor
@Document(collection = "GameJourney")
@CompoundIndexes({
        @CompoundIndex(useGeneratedName = true, def = "{'userId':1,'gameId':1}", unique = true, background = true),
        @CompoundIndex(name = "taskCompletedTime", def = "{'gameTaskList.completedTime': 1}", background = true),
        @CompoundIndex(name = "taskStatus", def = "{'gameTaskList.status': 1}", background = true),
        @CompoundIndex(name = "rewardItem", def = "{'gameTaskList.gameRewardDetails.gameRewardItem': 1}", background = true),
        @CompoundIndex(name = "taskName", def = "{'gameTaskList.name': 1}", background = true)
})
public class GameJourney extends AbstractMongoStringIdEntity {
    private Long userId;
    private String gameId;
    @Indexed(background = true)
    private Eligibility eligibility;
    private Set<String> enrollmentIds;
    private Set<String> batchIds;
    private List<GameTask> gameTaskList;
    private EnrollmentState enrollmentType;

    public long getLastTaskCompleted() {

        long max = getCreationTime();
        for (GameTask gameTask : gameTaskList) {
            if (gameTask.getType() == GameTaskType.TASK) {
                if (gameTask.getCompletedTime() != null) {
                    max = Math.max(max, gameTask.getCompletedTime());
                }
            }
        }

        return max;
    }

    public long getMaxActivityCreation(GameEntity entity, Set<GameSubtaskName> names) {
        OptionalLong max = gameTaskList.stream().filter(Objects::nonNull)
                .map(GameTask::getSubtaskList).filter(Objects::nonNull)
                .flatMap(List::stream).filter(Objects::nonNull)
                .filter(e -> names == null || names.contains(e.getName()))
                .map(GameSubtask::getMeta).filter(Objects::nonNull)
                .flatMap(List::stream).filter(Objects::nonNull)
                .filter(e -> e.getEntity() == entity)
                .map(GameMetaData::getStartTime).filter(Objects::nonNull)
                .mapToLong(e -> e).max();
        return max.isPresent() ? max.getAsLong() : -1;
    }

    public GameTask getGameTaskForName(String activeTask) {
        Optional<GameTask> optionalGameTask = gameTaskList.stream()
                .filter(e -> e.getName().equals(activeTask))
                .findAny();
        if (optionalGameTask.isPresent()) {
            return gameTaskList.stream()
                    .filter(e -> e.getName().equals(activeTask) && e.getStatus() != GameTaskStatus.COMPLETE)
                    .findAny()
                    .orElse(null);
        }

        throw new VRuntimeException(ErrorCode.SERVICE_ERROR, String.format("Task - %s not found for game %s", activeTask, this.getId()));
    }

    public GameTask getNextActiveTask(String activeTask) {
        boolean nextTask = false;
        for (GameTask task : gameTaskList) {
            if (nextTask) {
                return task;
            }
            if (Objects.equals(task.getName(), activeTask)) {
                nextTask = true;
            }
        }
        return null;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String ENROLLMENT_IDS = "enrollmentIds";
        public static final String GAME_TASKS = "gameTaskList";
        public static final String GAME_ID = "gameId";
        public static final String ELIGIBILITY = "eligibility";

        // GameTaskConstants
        public static final String GAME_TASKS_NAME = "gameTaskList.name";
        public static final String GAME_TASKS_REWARDDETAILS = "gameTaskList.gameRewardDetails";
        public static final String GAME_TASKS_REWARDITEM = "gameTaskList.gameRewardDetails.gameRewardItem";
        public static final String GAME_TASKS_TYPE = "gameTaskList.type";
        public static final String GAME_TASKS_STATUS = "gameTaskList.status";
        public static final String GAME_TASKS_HISTORY = "gameTaskList.gameTaskHistoryList";
        public static final String GAME_TASKS_COMPLETEDTIME = "gameTaskList.completedTime";

        public static final String STATUS = "status";
        public static final String NAME = "name";
        public static final String COMPLETED_TIME = "completedTime";
        public static final String REWARD_ITEM = "gameRewardDetails.gameRewardItem";

    }

}
