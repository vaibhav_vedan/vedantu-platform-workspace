/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.entities.mongo;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.ArrayList;
import java.util.List;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "Registration")
public class Registration extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long userId;
    private EntityType entityType;    
    private String entityId;
    private String orderId;
    private Integer bulkPriceToPay;//this is the (bulk)price when reg was done,will be used for bulk payment
    private List<OTMBundleEntityInfo> entities;
    private List<BaseInstalmentInfo> instalmentDetails;//will be used to create instalments
    private RegistrationStatus status = RegistrationStatus.REGISTERED;
    private List<String> deliverableEntityIds = new ArrayList<>();
    private Integer duration;//will be used in case of batch/course registration and will be populated when registration reaches enrolled status
    private String advancePaymentOrderId;
    private boolean advancePaymentConsumed;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public Integer getBulkPriceToPay() {
        return bulkPriceToPay;
    }

    public void setBulkPriceToPay(Integer bulkPriceToPay) {
        this.bulkPriceToPay = bulkPriceToPay;
    }

    public List<OTMBundleEntityInfo> getEntities() {
        return entities;
    }

    public void setEntities(List<OTMBundleEntityInfo> entities) {
        this.entities = entities;
    }

    public List<BaseInstalmentInfo> getInstalmentDetails() {
        return instalmentDetails;
    }

    public void setInstalmentDetails(List<BaseInstalmentInfo> instalmentDetails) {
        this.instalmentDetails = instalmentDetails;
    }

    public RegistrationStatus getStatus() {
        return status;
    }

    public void setStatus(RegistrationStatus status) {
        this.status = status;
    }

    public List<String> getDeliverableEntityIds() {
        return deliverableEntityIds;
    }

    public void setDeliverableEntityIds(List<String> deliverableEntityIds) {
        this.deliverableEntityIds = deliverableEntityIds;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getAdvancePaymentOrderId() {
        return advancePaymentOrderId;
    }

    public void setAdvancePaymentOrderId(String advancePaymentOrderId) {
        this.advancePaymentOrderId = advancePaymentOrderId;
    }

    public boolean isAdvancePaymentConsumed() {
        return advancePaymentConsumed;
    }

    public void setAdvancePaymentConsumed(boolean advancePaymentConsumed) {
        this.advancePaymentConsumed = advancePaymentConsumed;
    }
    

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String STATUS = "status";
        public static final String ENTITY_ID = "entityId";
    }

}
