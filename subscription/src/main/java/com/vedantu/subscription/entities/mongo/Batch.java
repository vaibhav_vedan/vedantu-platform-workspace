package com.vedantu.subscription.entities.mongo;

import com.vedantu.User.UserBasicInfo;
import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.onetofew.enums.*;

import com.vedantu.onetofew.enums.CourseTerm;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import com.vedantu.onetofew.pojo.BatchBasicInfo;
import com.vedantu.onetofew.pojo.BatchInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.onetofew.pojo.Agenda;
import com.vedantu.onetofew.pojo.BatchReq;
import com.vedantu.onetofew.pojo.BoardTeacher;
import com.vedantu.onetofew.pojo.BoardTeacherPair;

import com.vedantu.onetofew.pojo.SessionPlanPojo;
import com.vedantu.util.ArrayUtils;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Map;

@Data
@NoArgsConstructor
public class Batch extends AbstractMongoStringIdEntity {

    private String courseId;
    private Long startTime;
    private Long endTime;
    @Getter(AccessLevel.NONE)
    private int maxEnrollment = 5;
    @Getter(AccessLevel.NONE)
    private int minEnrollment = 1;
    private Set<String> teacherIds;
    private String currency = "INR";
    private int purchasePrice;
    private int cutPrice = -1;
    private String planString;
    private VisibilityState visibilityState;
    private EntityStatus batchState;
    private String demoVideoUrl;
    private boolean isHalfScheduled;
    private OTFSessionToolType sessionToolType = OTFSessionToolType.VEDANTU_WAVE; // If
    private Integer registrationFee;
    private List<BoardTeacherPair> boardTeacherPairs;
    private Long duration;
    private String groupName;
    private String groupSlug;
    private String preferredSeat;
    private List<SessionPlanPojo> sessionPlan;
    private List<CourseTerm> searchTerms;
    private String whatsappJoinUrl;
    private boolean contentBatch = false;
    private boolean modularBatch = false;
    private String acadMentorId;
    private boolean recordedVideo = false;
    private Long lastPurchaseDate;
    private boolean earlyLearning = false;
    private String orgId;
    private Language language = Language.DEFAULT;
    private List<SubscriptionPackageType> packageTypes;
    private Long earlyLearningUserId;
    private List<EarlyLearningCourseType> earlyLearningCourses;
    private Boolean hasSections = Boolean.FALSE;

    public Batch(String id, String courseId, Long startTime, Long endTime, int maxEnrollment, int minEnrollment,
            Set<String> teacherIds, String currency, int purchasePrice, int cutPrice,
            String planString, VisibilityState visibilityState, EntityStatus batchState, String demoVideoUrl,
            boolean isHalfScheduled, List<Agenda> agenda,
            OTFSessionToolType sessionToolType,
            Integer registrationFee, List<BoardTeacherPair> boardTeacherPairs, String orgId) {
        super();
        if (sessionToolType != null && sessionToolType.isUnsupported()) {
            throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, "Can't use this tool type - " + sessionToolType);
        }
        super.setId(id);
        this.courseId = courseId;
        this.startTime = startTime;
        this.endTime = endTime;
        this.maxEnrollment = maxEnrollment;
        this.minEnrollment = minEnrollment;
        this.teacherIds = teacherIds;
        this.currency = currency;
        this.purchasePrice = purchasePrice;
        this.cutPrice = cutPrice;
        this.planString = planString;
        this.visibilityState = visibilityState;
        this.batchState = batchState;
        this.demoVideoUrl = demoVideoUrl;
        this.isHalfScheduled = isHalfScheduled;
        this.sessionToolType = sessionToolType == null ? OTFSessionToolType.VEDANTU_WAVE : sessionToolType;
        this.registrationFee = registrationFee;
        this.boardTeacherPairs = boardTeacherPairs;
    }

    public int getMaxEnrollment() {
        if (maxEnrollment <= 0) {
            return 5;
        }

        return maxEnrollment;
    }

    public int getMinEnrollment() {
        if (minEnrollment <= 0) {
            return 1;
        }

        return minEnrollment;
    }

    public void setSessionToolType(OTFSessionToolType sessionToolType) {
        if (sessionToolType != null && sessionToolType.isUnsupported()) {
            throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, "Can't use this tool type - " + sessionToolType);
        }
        this.sessionToolType = sessionToolType;
    }

    public BatchBasicInfo toBatchBasicInfo(Batch batch, Course course,
                                           Map<String, UserBasicInfo> userMap, Map<Long, String> boardSubjectMap) {
        BatchBasicInfo batchBasicInfo = new BatchBasicInfo();
        if (batch != null) {
            batchBasicInfo.setBatchId(batch.getId());
            batchBasicInfo.setVisibilityState(batch.getVisibilityState());
            batchBasicInfo.setPlanString(batch.getPlanString());
            batchBasicInfo.setPurchasePrice(batch.getPurchasePrice());
            batchBasicInfo.setCutPrice(batch.getCutPrice());
            batchBasicInfo.setStartTime(batch.getStartTime());
            batchBasicInfo.setEndTime(batch.getEndTime());
            batchBasicInfo.setMaxEnrollment(batch.getMaxEnrollment());
            batchBasicInfo.setMinEnrollment(batch.getMinEnrollment());
            batchBasicInfo.setHalfScheduled(batch.isHalfScheduled());
            batchBasicInfo.setCreationTime(batch.getCreationTime());
            batchBasicInfo.setLastUpdated(batch.getLastUpdated());
            batchBasicInfo.setBatchState(batch.getBatchState());
            batchBasicInfo.setTeacherIds(batch.getTeacherIds());
            batchBasicInfo.setSearchTerms(batch.getSearchTerms());
            batchBasicInfo.setContentBatch(batch.isContentBatch());
            batchBasicInfo.setModularBatch(batch.isModularBatch());
            batchBasicInfo.setAcadMentorId(batch.getAcadMentorId());
            batchBasicInfo.setCourseId(batch.getCourseId());
            batchBasicInfo.setRecordedVideo(batch.isRecordedVideo());
            batchBasicInfo.setEarlyLearning(batch.isEarlyLearning());
            if (userMap != null) {
                List<UserBasicInfo> _teachers = new ArrayList<>();
                Set<String> userIds = batch.getTeacherIds();
                if (userIds != null && !userIds.isEmpty()) {
                    for (String userId : userIds) {
                        if (userMap.containsKey(userId)) {
                            _teachers.add(userMap.get(userId));
                        }
                    }
                }
                batchBasicInfo.setTeachers(_teachers);
            }
            batchBasicInfo.setSessionToolType(batch.getSessionToolType());
            if (course != null) {
                batchBasicInfo.setCourseInfo(course.toCourseBasicInfo(course, userMap, null));
            }
            batchBasicInfo.setRegistrationFee(batch.getRegistrationFee());

            batchBasicInfo.setDuration(batch.getDuration());
            batchBasicInfo.setGroupName(batch.getGroupName());
            batchBasicInfo.setGroupSlug(batch.getGroupSlug());
            batchBasicInfo.setSessionPlan(batch.getSessionPlan());
            batchBasicInfo.setLastPurchaseDate(batch.getLastPurchaseDate());
            if (ArrayUtils.isNotEmpty(batch.getBoardTeacherPairs())) {
                for (BoardTeacherPair pair : batch.getBoardTeacherPairs()) {
                    if (boardSubjectMap != null && boardSubjectMap.containsKey(pair.getBoardId())) {
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null && userMap != null
                                    && userMap.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userMap.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            batchBasicInfo.setBoardTeacherPairs(batch.getBoardTeacherPairs());
            batchBasicInfo.setOrgId(batch.getOrgId());
            batchBasicInfo.setLanguage(batch.getLanguage());
            batchBasicInfo.setPackageTypes(batch.getPackageTypes());
            if(Objects.nonNull(batch.getHasSections()))
                batchBasicInfo.setHasSections(batch.getHasSections());
            else
                batchBasicInfo.setHasSections(Boolean.FALSE);
        }
        return batchBasicInfo;
    }

    public BatchInfo toBatchInfo(Batch batch, Map<String, UserBasicInfo> userMap,
            Map<Long, String> boardSubjectMap) {
        BatchInfo batchInfo = new BatchInfo();
        if (batch != null) {
            batchInfo.setBatchId(batch.getId());
            batchInfo.setVisibilityState(batch.getVisibilityState());
            batchInfo.setPlanString(batch.getPlanString());
            batchInfo.setPurchasePrice(batch.getPurchasePrice());
            batchInfo.setCutPrice(batch.getCutPrice());
            batchInfo.setStartTime(batch.getStartTime());
            batchInfo.setEndTime(batch.getEndTime());
            batchInfo.setMaxEnrollment(batch.getMaxEnrollment());
            batchInfo.setMinEnrollment(batch.getMinEnrollment());
            batchInfo.setHalfScheduled(batch.isHalfScheduled());
            batchInfo.setCreationTime(batch.getCreationTime());
            batchInfo.setLastUpdated(batch.getLastUpdated());
            batchInfo.setContentBatch(batch.isContentBatch());
            batchInfo.setModularBatch(batch.isModularBatch());
            batchInfo.setAcadMentorId(batch.getAcadMentorId());
            batchInfo.setWhatsappJoinUrl(batch.getWhatsappJoinUrl());
            batchInfo.setEarlyLearning(batch.isEarlyLearning());
            if(Objects.nonNull(batch.getEarlyLearningCourses())) {
                batchInfo.setEarlyLearningCourses(batch.getEarlyLearningCourses());
            }
            if (userMap != null) {
                List<UserBasicInfo> _teachers = new ArrayList<>();
                Set<String> userIds = batch.getTeacherIds();
                if (userIds != null && !userIds.isEmpty()) {
                    for (String userId : userIds) {
                        if (userMap.containsKey(userId)) {
                            _teachers.add(userMap.get(userId));
                        }
                    }
                }

                batchInfo.setTeachers(_teachers);
            }
            batchInfo.setSessionToolType(batch.getSessionToolType());
            batchInfo.setCurrency(batch.getCurrency());
            batchInfo.setBatchState(batch.getBatchState());
            batchInfo.setDemoVideoUrl(batch.getDemoVideoUrl());
            batchInfo.setRegistrationFee(batch.getRegistrationFee());
            batchInfo.setDuration(batch.getDuration());
            batchInfo.setGroupName(batch.getGroupName());
            batchInfo.setGroupSlug(batch.getGroupSlug());
            batchInfo.setSessionPlan(batch.getSessionPlan());
            batchInfo.setLastPurchaseDate(batch.getLastPurchaseDate());
            batchInfo.setSearchTerms(batch.getSearchTerms());
            if (ArrayUtils.isNotEmpty(batch.getBoardTeacherPairs())) {
                for (BoardTeacherPair pair : batch.getBoardTeacherPairs()) {
                    if (boardSubjectMap != null && boardSubjectMap.containsKey(pair.getBoardId())) {
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null && userMap != null
                                    && userMap.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userMap.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            batchInfo.setBoardTeacherPairs(batch.getBoardTeacherPairs());
            batchInfo.setRecordedVideo(batch.isRecordedVideo());
            batchInfo.setOrgId(batch.getOrgId());
            batchInfo.setLanguage(batch.getLanguage());
            batchInfo.setPackageTypes(batch.getPackageTypes());
            if(Objects.nonNull(batch.getHasSections()))
                batchInfo.setHasSections(batch.getHasSections());
            else
                batchInfo.setHasSections(Boolean.FALSE);
        }
        return batchInfo;
    }

    public static Batch toBatch(BatchReq req) {
        Batch batch = new Batch(req.getId(), req.getCourseId(), req.getStartTime(),
                req.getEndTime(), req.getMaxEnrollment(), req.getMinEnrollment(),
                req.getTeacherIds(), req.getCurrency(),
                req.getPurchasePrice(), req.getCutPrice(),
                req.getPlanString(), req.getVisibilityState(), req.getBatchState(), req.getDemoVideoUrl(),
                req.isHalfScheduled(), null, req.getSessionToolType(), req.getRegistrationFee(),
                req.getBoardTeacherPairs(), req.getOrgId());
        Set<String> teacherIds = new HashSet<>();
        if (ArrayUtils.isNotEmpty(req.getBoardTeacherPairs())) {
            for (BoardTeacherPair pair : req.getBoardTeacherPairs()) {
                if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                    for (BoardTeacher teacher : pair.getTeachers()) {
                        if (teacher.getTeacherId() != null) {
                            teacherIds.add(teacher.getTeacherId().toString());
                        }
                    }
                }
            }
        }
        batch.setModularBatch(req.isModularBatch());
        batch.setContentBatch(req.isContentBatch());
        batch.setAcadMentorId(req.getAcadMentorId());
        batch.setDuration(req.getDuration());
        batch.setGroupName(req.getGroupName());
        batch.setPreferredSeat(req.getPreferredSeat());
        batch.setTeacherIds(teacherIds);
        batch.setSessionPlan(req.getSessionPlan());
        batch.setSearchTerms(req.getSearchTerms());
        batch.setWhatsappJoinUrl(req.getWhatsappJoinUrl());
        batch.setRecordedVideo(req.isRecordedVideo());
        batch.setLastPurchaseDate(req.getLastPurchaseDate());
        batch.setOrgId(req.getOrgId());
        batch.setLanguage(req.getLanguage());
        batch.setPackageTypes(req.getPackageType());
        if (Objects.nonNull(req.getEarlyLearning())) {
            batch.setEarlyLearning(req.getEarlyLearning());
        }
        if (Objects.nonNull(req.getEarlyLearningCourses())){
            batch.setEarlyLearningCourses(req.getEarlyLearningCourses());
        }
        if(Objects.nonNull(req.getHasSections()))
            batch.setHasSections(req.getHasSections());
        else
            batch.setHasSections(Boolean.FALSE);

        return batch;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String TEACHER_IDS = "teacherIds";
        public static final String PURCHASE_PRICE = "purchasePrice";
        public static final String COURSE_ID = "courseId";
        public static final String VISIBILITY_STATE = "visibilityState";
        public static final String START_TIME = "startTime";
        public static final String BATCH_STATE = "batchState";
        public static final String END_TIME = "endTime";
        public static final String CUT_PRICE = "cutPrice";
        public static final String AGENDA = "agenda";
        public static final String PLAN_STRING = "planString";
        public static final String GROUP_NAME = "groupName";
        public static final String SEARCH_TERMS = "searchTerms";
        public static final String CONTENT_BATCH = "contentBatch";
        public static final String MODULAR_BATCH = "modularBatch";
        public static final String AM_ID = "acadMentorId";
        public static final String SESSION_PLAN = "sessionPlan";
        public static final String BOARD_TEACHER_PAIRS = "boardTeacherPairs";
        public static final String RECORDED_VIDEO = "recordedVideo";
        public static final String PACKAGE_TYPES = "packageTypes";
        public static final String ORG_ID = "orgId";
        public static final String HAS_SECTIONS = "hasSections";

    }
}
