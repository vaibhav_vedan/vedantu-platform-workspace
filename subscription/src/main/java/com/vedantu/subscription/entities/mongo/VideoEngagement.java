package com.vedantu.subscription.entities.mongo;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by darshit on 18/8/19.
 */
@Document(collection = "VideoEngagement")
public class VideoEngagement extends AbstractMongoStringIdEntity {
    String userId;
    String  bundleEnrollmentId;
    String  videoId;
    Long duration;


    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBundleEnrollmentId() {
        return bundleEnrollmentId;
    }

    public void setBundleEnrollmentId(String bundleEnrollmentId) {
        this.bundleEnrollmentId = bundleEnrollmentId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }


    public void addDuration(Long duration) {
        this.duration += duration;
    }
    public static class Constants extends AbstractMongoEntity.Constants {
    public static final String USERID= "userId";
        public static final String VIDEOID = "videoId";
        public static final String BUNDLEENROLLMENTID = "bundleEnrollmentId";

    }


}
