package com.vedantu.subscription.entities.sql;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.vedantu.subscription.enums.HourTransactionType;
import com.vedantu.subscription.enums.HourType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;

@Entity
@Table(name = "HourTransaction", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "creditTo", "transactionType", "transactionRefNoNew" }) })
public class HourTransaction extends AbstractSqlLongIdEntity {
	private Long subscriptionId;
	private Long subscriptionDetailsId;
	@Enumerated(EnumType.STRING)
	private HourType debitFrom;
	@Enumerated(EnumType.STRING)
	private HourType creditTo;
	private Long duration;// in milliseconds
	@Enumerated(EnumType.STRING)
	private HourTransactionType transactionType;
	private Long transactionRefNo;// sessionId or subscriptionDetailsId(in case
									// of create,ended,renewed subscription)
        private String transactionRefNoNew;

	public HourTransaction() {
		super();
	}

	public HourTransaction(Long subscriptionId, Long subscriptionDetailsId, HourType debitFrom, HourType creditTo,
			Long duration, HourTransactionType transactionType, String transactionRefNo) {
		super();
		this.subscriptionId = subscriptionId;
		this.subscriptionDetailsId = subscriptionDetailsId;
		this.debitFrom = debitFrom;
		this.creditTo = creditTo;
		this.duration = duration;
		this.transactionType = transactionType;
		this.transactionRefNoNew = transactionRefNo;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getSubscriptionDetailsId() {
		return subscriptionDetailsId;
	}

	public void setSubscriptionDetailsId(Long subscriptionDetailsId) {
		this.subscriptionDetailsId = subscriptionDetailsId;
	}

	public HourType getDebitFrom() {
		return debitFrom;
	}

	public void setDebitFrom(HourType debitFrom) {
		this.debitFrom = debitFrom;
	}

	public HourType getCreditTo() {
		return creditTo;
	}

	public void setCreditTo(HourType creditTo) {
		this.creditTo = creditTo;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public HourTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(HourTransactionType transactionType) {
		this.transactionType = transactionType;
	}

        public Long getTransactionRefNo() {
            return transactionRefNo;
        }

        public void setTransactionRefNo(Long transactionRefNo) {
            this.transactionRefNo = transactionRefNo;
        }

        public String getTransactionRefNoNew() {
            return transactionRefNoNew;
        }

        public void setTransactionRefNoNew(String transactionRefNoNew) {
            this.transactionRefNoNew = transactionRefNoNew;
        }
        
}