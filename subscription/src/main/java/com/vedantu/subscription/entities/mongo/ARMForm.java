package com.vedantu.subscription.entities.mongo;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.dinero.enums.DeliverableEntityType;
import com.vedantu.exception.VException;
import com.vedantu.subscription.enums.ModeOfLearning;
import com.vedantu.subscription.pojo.CoursePlanCompactSchedule;
import com.vedantu.subscription.request.AddARMFormReq;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;

@Document(collection = "ARMForm")
@CompoundIndexes({
    @CompoundIndex(name = "studentId_creationTime", def = "{'studentId': 1, 'creationTime': -1}", background = true)
})
public class ARMForm extends AbstractMongoStringIdEntity {

    private String school;
    private String city;
    private Long studentId;
    private String lookingFor;
    private String target;
    private Integer grade;
    private List<String> subject;
    private List<Long> boardId;
    private String schoolBooks;
    private Long startDate;
    private ModeOfLearning modeOfLearning;
    private String comment;
    private List<CoursePlanCompactSchedule> compactSchedule;
    private DeliverableEntityType deliverableEntityType;//CoursePlan
    private String deliverableEntityId;//CoursePlanID
    private String chapterSequence;
    private String marksScored;
    private String doubts;
    private Boolean registrationDone;
    private String description;
    private Long adminId;
    private Integer hours;
    private boolean isClosed = false;
    private String adminComment;

    public ARMForm() {
        super();
    }

    public ARMForm(AddARMFormReq req) throws VException {
        super();
        this.lookingFor = req.getLookingFor();
        this.target = req.getTarget();
        this.grade = req.getGrade();
        this.subject = req.getSubject();
        this.boardId = req.getBoardId();
        this.schoolBooks = req.getSchoolBooks();
        this.startDate = req.getStartDate();
        this.modeOfLearning = req.getModeOfLearning();
        this.comment = req.getComment();
        this.compactSchedule = req.getCompactSchedule();
        if (req.getId() != null) {
            this.setId(req.getId());
        }

        this.school = req.getSchool();
        this.city = req.getCity();
        this.studentId = req.getStudentId();
        this.adminId = req.getAdminId();
        this.chapterSequence = req.getChapterSequence();
        this.marksScored = req.getMarksScored();
        this.doubts = req.getDoubts();
        this.description = req.getDescription();
        this.registrationDone = req.getRegistrationDone();
        this.hours = req.getHours();
        this.isClosed = req.isIsClosed();
        if (StringUtils.isNotEmpty(req.getAdminComment())) {
            this.adminComment = req.getAdminComment();
        }
    }

    public String getLookingFor() {
        return lookingFor;
    }

    public void setLookingFor(String lookingFor) {
        this.lookingFor = lookingFor;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public List<String> getSubject() {
        return subject;
    }

    public void setSubject(List<String> subject) {
        this.subject = subject;
    }

    public List<Long> getBoardId() {
        return boardId;
    }

    public void setBoardId(List<Long> boardId) {
        this.boardId = boardId;
    }

    public String getSchoolBooks() {
        return schoolBooks;
    }

    public void setSchoolBooks(String schoolBooks) {
        this.schoolBooks = schoolBooks;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public ModeOfLearning getModeOfLearning() {
        return modeOfLearning;
    }

    public void setModeOfLearning(ModeOfLearning modeOfLearning) {
        this.modeOfLearning = modeOfLearning;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public List<CoursePlanCompactSchedule> getCompactSchedule() {
        return compactSchedule;
    }

    public void setCompactSchedule(List<CoursePlanCompactSchedule> compactSchedule) {
        this.compactSchedule = compactSchedule;
    }

    public DeliverableEntityType getDeliverableEntityType() {
        return deliverableEntityType;
    }

    public void setDeliverableEntityType(DeliverableEntityType deliverableEntityType) {
        this.deliverableEntityType = deliverableEntityType;
    }

    public String getDeliverableEntityId() {
        return deliverableEntityId;
    }

    public void setDeliverableEntityId(String deliverableEntityId) {
        this.deliverableEntityId = deliverableEntityId;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getChapterSequence() {
        return chapterSequence;
    }

    public void setChapterSequence(String chapterSequence) {
        this.chapterSequence = chapterSequence;
    }

    public String getMarksScored() {
        return marksScored;
    }

    public void setMarksScored(String marksScored) {
        this.marksScored = marksScored;
    }

    public String getDoubts() {
        return doubts;
    }

    public void setDoubts(String doubts) {
        this.doubts = doubts;
    }

    public Boolean getRegistrationDone() {
        return registrationDone;
    }

    public void setRegistrationDone(Boolean registrationDone) {
        this.registrationDone = registrationDone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getAdminId() {
        return adminId;
    }

    public void setAdminId(Long adminId) {
        this.adminId = adminId;
    }

    public Integer getHours() {
        return hours;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String STUDENT_ID = "studentId";
        public static final String ADMIN_ID = "adminId";
        public static final String STATE = "state";
        public static final String SUBJECT = "subject";
        public static final String TARGET = "target";
        public static final String GRADE = "grade";
        public static final String BOARD_ID = "boardId";
        public static final String TITLE = "title";
        public static final String COMPACT_SCHEDULE = "compactSchedule";
        public static final String DESCRIPTION = "description";
        public static final String STARTDATE = "startDate";
        public static final String IS_CLOSED = "isClosed";
    }

    /**
     * @return the isResolved
     */
    public boolean isIsClosed() {
        return isClosed;
    }

    /**
     * @param isClosed the isResolved to set
     */
    public void setIsClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    /**
     * @return the adminComment
     */
    public String getAdminComment() {
        return adminComment;
    }

    /**
     * @param adminComment the adminComment to set
     */
    public void setAdminComment(String adminComment) {
        this.adminComment = adminComment;
    }

}
