package com.vedantu.subscription.entities.mongo;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.BundleStateChangeTime;
import java.util.List;
import com.vedantu.onetofew.pojo.BundleValidTillChangeTime;
import com.vedantu.subscription.enums.BundleContentType;
import java.util.Map;

import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.DurationTime;
import com.vedantu.subscription.pojo.TestContentData;
import com.vedantu.subscription.pojo.Track;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.pojo.bundle.AioPackage;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import java.util.ArrayList;
import java.util.Set;

/**
 * Created by somil on 08/05/17.
 */
@Document(collection = "Bundle")
@Data
@CompoundIndexes({
        @CompoundIndex(name = "tests_children_grade_1_tests_children_subject_1", def = "{'tests.children.subject': 1, 'tests.children.grade': 1}", background = true),
        @CompoundIndex(name = "videos_children_grade_1_videos_children_subject_1", def = "{'videos.children.subject': 1, 'videos.children.grade': 1}", background = true),
        @CompoundIndex(name = "lastUpdated_Idx", def = "{'lastUpdated': -1}", background = true),
        @CompoundIndex(name = "creationTime_Idx", def = "{'creationTime': 1}", background = true),
        @CompoundIndex(name = "displayTagsSize_Idx", def = "{'bundleDetails.displayTagsSize': 1}", background = true),
})
public class Bundle extends AbstractMongoStringIdEntity {

    private List<Track> webinarCategories;
    private VideoContentData videos;
    private TestContentData tests;

    @Indexed(background = true)
    private String title;

    @Indexed(background = true)
    private List<String> subject;

    @Indexed(background = true)
    private List<String> target;

    @Indexed(background = true)
    private List<Integer> grade;

    private Long boardId;

    @Indexed(background = true)
    private BundleState state = BundleState.DRAFT;

    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private Long startTime;
    private List<String> tags;
    private List<String> teacherIds;
    private boolean featured;
    private Integer priority; // lowest is highest
    private String demoVideoUrl;
    private Integer noOfWebinars;
    private Integer noOfVideos;
    private Integer noOfTests;
    private List<String> courses;
    private Map<String, Object> metadata;
    private int trailAmount;
    private int trailAllowedDays;
    private Boolean trailIncluded;
    private Long validTill;
    private int validDays;
    private Boolean tabletIncluded;
    private Boolean amIncluded;
    private Boolean unLimitedDoubtsIncluded;
    private List<String> suggestionPackages;

    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    @Deprecated
    private List<AioPackage> packages;

    @Indexed(background = true)
    @Getter(AccessLevel.NONE)
    @Deprecated
    private int packagesSize;

    private Boolean isTrail;
    private List<CourseTerm> searchTerms;
    private Boolean isRankBooster;
    private Boolean isVassist;
    private BundleDetails bundleDetails;
    private Boolean isInstalmentVisible = Boolean.FALSE;

    @Indexed(background = true)
    @Getter(AccessLevel.NONE)
    private Boolean isSubscription = Boolean.FALSE ;

    private DurationTime durationtime;
    private Long lastPurchaseDate;
    private Integer minimumPurchasePrice;

    @Getter(AccessLevel.NONE)
    private List<BundleStateChangeTime> stateChangeTime;

    private List<BundleValidTillChangeTime> validTillChangeTimes;
    private Long windowRight;

    private Set<String> trialIds;
    private String orgId;
    private List<String> displaySubject;


    public Boolean getIsSubscription() {
        return isSubscription != null && isSubscription;
    }

    private boolean earlyLearning;

    public List<BundleStateChangeTime> getStateChangeTime() {
        if(stateChangeTime == null){
            stateChangeTime = new ArrayList<>();
        }
        return stateChangeTime;
    }

    @Deprecated
    public List<AioPackage> getPackages() {
        if (packages == null) {
            return new ArrayList<>();
        }
        this.packagesSize = packages.size();
        return packages;
    }

    @Deprecated
    public void setPackages(List<AioPackage> packages) {
        this.packages = packages;
        this.packagesSize = this.packages != null ? this.packages.size() : 0;
    }

    @Deprecated
    public int getPackagesSize() {
        this.packagesSize = this.packages != null ? this.packages.size() : 0;
        return this.packagesSize;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String VIDEOS_CHILDREN_GRADE = "videos.children.grade";
        public static final String VIDEOS_CHILDREN_SUBJECT = "videos.children.subject";
        public static final String TEST_CHILDREN_GRADE = "tests.children.grade";
        public static final String TEST_CHILDREN_SUBJECT = "tests.children.subject";
        public static final String TEST_CHILDREN_VALIDTILL = "tests.children.validTill";
        public static final String BUNDLE_STATE = "state";
        public static final String FEATURED = "featured";
        public static final String TITLE = "title";
        public static final String PRICE = "price";
        public static final String CUT_PRICE = "cutPrice";
        public static final String VIDEOS = "videos";
        public static final String VIDEOS_CHILDREN = "videos.children";
        public static final String TESTS = "tests";
        public static final String TESTS_CHILDREN = "tests.children";
        @Deprecated
        public static final String PACKAGES_ENTITYID = "packages.entityId";
        @Deprecated
        public static final String PACKAGES = "packages";
        @Deprecated
        public static final String PACKAGES_MAINTAGS = "packages.mainTags";
        @Deprecated
        public static final String PACKAGES_STARTTIME = "packages.startTime";
        @Deprecated
        public static final String PACKAGES_ENDTIME= "packages.endTime";
        @Deprecated
        public static final String PACKAGES_COURSETITLE = "packages.courseTitle";
        @Deprecated
        public static final String PACKAGES_ENTITY = "packages.packageType";
        public static final String GRADE = "grade";
        public static final String DURATION_TIME = "durationtime";
        public static final String SUBJECT = "subject";
        public static final String TARGET = "target";
        public static final String VALID_TILL = "validTill";
        public static final String VALID_DAYS = "validDays";
        public static final String WEBINAR_CATEGORIES = "webinarCategories";
        public static final String SEARCH_TERMS = "searchTerms";
        public static final String BUNDLE_DETAILS = "bundleDetails";
        public static final String BUNDLE_DETAILS_DISPLAY_TAG_SIZE = "bundleDetails.displayTagsSize";
        public static final String BUNDLE_PROMOTION_TAG = "bundleDetails.promotionTag";
        public static final String BUNDLE_DETAILS_COURSE_TYPE = "bundleDetails.courseType";
        public static final String BUNDLE_DETAILS_COURSE_START_TIME = "bundleDetails.courseStartTime";
        public static final String ENTITY_ID = "entityId";
        public static final String IS_SUBSCRIPTION = "isSubscription";
        public static final String START_TIME = "startTime";
        public static final String TEACHER_IDS = "teacherIds";
        public static final String BUNDLE_IMAGE = "bundleDetails.bundleImageUrl";
        public static final String STATE_CHANGE_TIME = "stateChangeTime";
        public static final String BUNDLE_DISPLAY_TAG = "bundleDetails.displayTags";
        public static final String WINDOW_RIGHT = "windowRight";
        public static final String TRIAL_IDS = "trialIds";
        public static final String TRIAL_AMOUNT = "trailAmount";
        public static final String TRIAL_ALLOWED_DAYS = "trailAllowedDays";
        public static final String VALID_TILL_CHANGE_TIME = "validTillChangeTimes";
        public static final String EARLY_LEARNING = "earlyLearning";
        public static final String IS_VASSIST = "isVassist";

        @Deprecated
        public static final String PACKAGES_SUBSCRIPTION_TYPE = "packages.subscriptionPackageTypes";

    }
}
