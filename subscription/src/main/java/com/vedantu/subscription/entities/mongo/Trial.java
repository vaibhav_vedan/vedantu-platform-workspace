package com.vedantu.subscription.entities.mongo;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;

@Data
@NoArgsConstructor
@CompoundIndexes({
        @CompoundIndex(name = "trialAmount.trialAllowedDays.contextId", def = "{'trialAmount': 1, 'trialAllowedDays': 1, 'contextId' : 1}", unique = true, background = true)
})
public class Trial extends AbstractMongoStringIdEntity {
    private Long trialAmount;
    private Long cutPrice;
    private Long trialAllowedDays;
    private int gracePeriod=7;
    private EntityType contextType;
    @Indexed(background = true)
    private String contextId;

    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String CONTEXT_ID = "contextId";
        public static final String TOTAL_AMOUNT = "trialAmount";

    }
}
