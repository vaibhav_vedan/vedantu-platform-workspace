package com.vedantu.subscription.entities.mongo;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.pojo.BundlePricingPojo;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.subscription.pojo.EntityInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.Set;

/**
 * Created by somil on 10/05/17.
 */
@Document(collection = "BundlePackage")
@CompoundIndexes({
    @CompoundIndex(name = "entityId_1", def = "{'entities.entityId': 1}", background = true)
})
public class BundlePackage extends AbstractMongoStringIdEntity {

    private List<EntityInfo> entities;

    private String title;
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private List<String> tags;
    private boolean featured;
    //lowest is highest
    private Integer priority;
    private List<BundlePricingPojo> pricing;
    private String referralCouponCode;
    private List<CourseTerm> searchTerms;
    private Long startTime;
    private VisibilityState visibilityState = VisibilityState.VISIBLE;
    private Set<Long> boardIds;
    private Integer duration;

    public Integer getCutPrice() {
        return cutPrice;
    }

    public void setCutPrice(Integer cutPrice) {
        this.cutPrice = cutPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<EntityInfo> getEntities() {
        return entities;
    }

    public void setEntities(List<EntityInfo> entities) {
        this.entities = entities;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String ENTITIES = "entities";
        public static final String TAGS = "tags";
        public static final String GRADE = "grade";
        public static final String TARGETS = "target";
        public static final String VISIBILITY_STATE = "visibilityState";
        public static final String START_TIME = "startTime";

    }

    public List<BundlePricingPojo> getPricing() {
        return pricing;
    }

    public void setPricing(List<BundlePricingPojo> pricing) {
        this.pricing = pricing;
    }

    public String getReferralCouponCode() {
        return referralCouponCode;
    }

    public void setReferralCouponCode(String referralCouponCode) {
        this.referralCouponCode = referralCouponCode;
    }

    public List<CourseTerm> getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(List<CourseTerm> searchTerms) {
        this.searchTerms = searchTerms;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the visibilityState
     */
    public VisibilityState getVisibilityState() {
        return visibilityState;
    }

    /**
     * @param visibilityState the visibilityState to set
     */
    public void setVisibilityState(VisibilityState visibilityState) {
        this.visibilityState = visibilityState;
    }

    /**
     * @return the boardIds
     */
    public Set<Long> getBoardIds() {
        return boardIds;
    }

    /**
     * @param boardIds the boardIds to set
     */
    public void setBoardIds(Set<Long> boardIds) {
        this.boardIds = boardIds;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

}
