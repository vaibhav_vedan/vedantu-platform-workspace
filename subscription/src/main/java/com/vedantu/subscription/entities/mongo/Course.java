package com.vedantu.subscription.entities.mongo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vedantu.User.UserBasicInfo;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.pojo.*;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.enums.EarlyLearningCourseType;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

@Data
@NoArgsConstructor
@Document(collection = "Course")
public class Course extends AbstractMongoStringIdEntity {

    private Long launchDate;
    private String title;
    private String description;
    @JsonProperty
    private List<ContentInfo> contents;
    private Long duration;
    @JsonProperty
    private Set<String> teacherIds;
    @JsonProperty
    private List<BatchTiming> batchTimings;
    @JsonProperty
    private List<CurriculumPojo> curriculum;
    private String targetAudience;
    @JsonProperty
    private List<SessionBluePrintEntry> sessionBluePrint;
    private VisibilityState visibilityState;
    @JsonProperty
    private List<FAQPojo> faq;
    @JsonProperty
    private Set<String> grades;
    private int liveBatches = 0;
    private int upcomingBatches = 0;
    private int totalBatches = 0;
    @JsonProperty
    private Set<String> targets;
    @JsonProperty
    private Set<String> normalizeTargets;
    @JsonProperty
    private Set<String> normalizeSubjects;
    private int priority = 0;
    private boolean featured = false;
    private Long studentsEnrolled = 0L;
    private Long totalSeats = 0L;
    private String parentCourseId = null;
    private String orgId;

    // grades, subjects, targets, course title will be added and removed by
    // default
    @JsonProperty
    private List<String> tags;
    private String demoVideoUrl;
    private Long exitDate;
    private int startPrice;
    @JsonProperty
    private List<PreRequisites> preRequisites;
    private Long batchStartTime;
    private Long batchEndTime;
    private Integer registrationFee;
    private List<BoardTeacherPair> boardTeacherPairs;
    private List<BoardTeacherPair> featuredTeachers;
    private List<CourseTerm> searchTerms;
    private Boolean earlyLearning=Boolean.FALSE;
    private List<EarlyLearningCourseType> earlyLearningCourses;

    public CourseBasicInfo toCourseBasicInfo(Course course,
                                             Map<String, UserBasicInfo> userBasicInfos, Map<Long, String> boardSubjectMap) {
        CourseBasicInfo courseBasicInfo = new CourseBasicInfo();
        if (course != null) {
            courseBasicInfo.setId(course.getId());
            courseBasicInfo.setTitle(course.getTitle());
            courseBasicInfo.setLiveBatches(course.getLiveBatches());
            courseBasicInfo.setUpcomingBatches(course.getUpcomingBatches());
            courseBasicInfo.setTargets(course.getTargets());
            courseBasicInfo.setNormalizeSubjects(course.getNormalizeSubjects());
            courseBasicInfo.setNormalizeTargets(course.getNormalizeTargets());
            courseBasicInfo.setGrades(course.getGrades());
            courseBasicInfo.setVisibilityState(course.getVisibilityState());
            courseBasicInfo.setPriority(course.getPriority());
            courseBasicInfo.setTags(course.getTags());
            courseBasicInfo.setLaunchDate(course.getLaunchDate());
            courseBasicInfo.setFeatured(course.isFeatured());
            courseBasicInfo.setDescription(course.getDescription());
            courseBasicInfo.setBatchStartTime(course.getBatchStartTime());
            courseBasicInfo.setBatchEndTime(course.getBatchEndTime());
            courseBasicInfo.setStudentsEnrolled(course.getStudentsEnrolled());
            courseBasicInfo.setTotalSeats(course.getTotalSeats());
            courseBasicInfo.setCreationTime(course.getCreationTime());
            courseBasicInfo.setLastUpdated(course.getLastUpdated());
            courseBasicInfo.setDuration(course.getDuration());
            courseBasicInfo.setEarlyLearning(course.getEarlyLearning());
            courseBasicInfo.setSearchTerms(course.getSearchTerms());
            if(!Objects.isNull(course.getEarlyLearningCourses())) {
                courseBasicInfo.setEarlyLearningCourses(course.getEarlyLearningCourses());
            }

            // populate teachers from userBasicinfos
            Set<String> _teacherIds = course.getTeacherIds();
            if (userBasicInfos != null && !userBasicInfos.isEmpty() && _teacherIds != null && !_teacherIds.isEmpty()) {
                List<UserBasicInfo> teachers = new ArrayList<>();
                for (String teacherId : _teacherIds) {
                    if (userBasicInfos.containsKey(teacherId)) {
                        teachers.add(userBasicInfos.get(teacherId));
                    }
                }
                courseBasicInfo.setTeachers(teachers);
            }
            courseBasicInfo.setRegistrationFee(course.getRegistrationFee());

            if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                for (BoardTeacherPair pair : course.getBoardTeacherPairs()) {
                    if (boardSubjectMap != null && boardSubjectMap.containsKey(pair.getBoardId())) {
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null && userBasicInfos != null
                                    && userBasicInfos.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userBasicInfos.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            courseBasicInfo.setBoardTeacherPairs(course.getBoardTeacherPairs());
            if (ArrayUtils.isNotEmpty(course.getFeaturedTeachers())) {
                for (BoardTeacherPair pair : course.getFeaturedTeachers()) {
                    if (boardSubjectMap != null && boardSubjectMap.containsKey(pair.getBoardId())) {
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null && userBasicInfos != null
                                    && userBasicInfos.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userBasicInfos.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            courseBasicInfo.setFeaturedTeachers(course.getFeaturedTeachers());
            courseBasicInfo.setOrgId(course.getOrgId());
        }
        return courseBasicInfo;
    }

    public CourseInfo toCourseInfo(Course course, Map<String, UserBasicInfo> userBasicInfos) {
        return toCourseInfo(course, userBasicInfos, null);
    }

    public CourseInfo toCourseInfo(Course course, Map<String, UserBasicInfo> userBasicInfos, Map<Long, String> boardSubjectMap) {
        CourseInfo courseInfo = new CourseInfo();
        if (course != null) {
            courseInfo.setId(course.getId());
            courseInfo.setTitle(course.getTitle());
            courseInfo.setLiveBatches(course.getLiveBatches());
            courseInfo.setUpcomingBatches(course.getUpcomingBatches());
            courseInfo.setTargets(course.getTargets());
            courseInfo.setNormalizeSubjects(course.getNormalizeSubjects());
            courseInfo.setNormalizeTargets(course.getNormalizeTargets());
            courseInfo.setGrades(course.getGrades());
            courseInfo.setVisibilityState(course.getVisibilityState());
            courseInfo.setPriority(course.getPriority());
            courseInfo.setTags(course.getTags());
            courseInfo.setLaunchDate(course.getLaunchDate());
            courseInfo.setFeatured(course.isFeatured());
            courseInfo.setDescription(course.getDescription());
            courseInfo.setBatchStartTime(course.getBatchStartTime());
            courseInfo.setBatchEndTime(course.getBatchEndTime());
            courseInfo.setStudentsEnrolled(course.getStudentsEnrolled());
            courseInfo.setTotalSeats(course.getTotalSeats());
            courseInfo.setCreationTime(course.getCreationTime());
            courseInfo.setLastUpdated(course.getLastUpdated());

            // populate teachers from userBasicinfos
            Set<String> _teacherIds = course.getTeacherIds();
            if (userBasicInfos != null && !userBasicInfos.isEmpty() && _teacherIds != null && !_teacherIds.isEmpty()) {
                List<UserBasicInfo> teachers = new ArrayList<>();
                for (String teacherId : _teacherIds) {
                    if (userBasicInfos.containsKey(teacherId)) {
                        teachers.add(userBasicInfos.get(teacherId));
                    }
                }
                courseInfo.setTeachers(teachers);
            }
            courseInfo.setRegistrationFee(course.getRegistrationFee());

            courseInfo.setContents(course.getContents());
            courseInfo.setDuration(course.getDuration());
            courseInfo.setBatchTimings(course.getBatchTimings());
            courseInfo.setCurriculum(course.getCurriculum());
            courseInfo.setTargetAudience(course.getTargetAudience());
            courseInfo.setSessionBluePrint(course.getSessionBluePrint());
            courseInfo.setFaq(course.getFaq());
            courseInfo.setDemoVideoUrl(course.getDemoVideoUrl());
            courseInfo.setExitDate(course.getExitDate());
            courseInfo.setStartPrice(course.getStartPrice());
            courseInfo.setPreRequisites(course.getPreRequisites());
            courseInfo.setParentCourseId(course.getParentCourseId());
            courseInfo.setEarlyLearning(course.getEarlyLearning());
            if(!Objects.isNull(course.getEarlyLearningCourses())) {
                courseInfo.setEarlyLearningCourses(course.getEarlyLearningCourses());
            }
            if (ArrayUtils.isNotEmpty(course.getBoardTeacherPairs())) {
                for (BoardTeacherPair pair : course.getBoardTeacherPairs()) {
                    if (boardSubjectMap != null && boardSubjectMap.containsKey(pair.getBoardId())) {
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null && userBasicInfos != null
                                    && userBasicInfos.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userBasicInfos.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            courseInfo.setBoardTeacherPairs(course.getBoardTeacherPairs());

            if (ArrayUtils.isNotEmpty(course.getFeaturedTeachers())) {
                for (BoardTeacherPair pair : course.getFeaturedTeachers()) {
                    if (boardSubjectMap != null && boardSubjectMap.containsKey(pair.getBoardId())) {
                        pair.setSubject(boardSubjectMap.get(pair.getBoardId()));
                    }
                    if (ArrayUtils.isNotEmpty(pair.getTeachers())) {
                        for (BoardTeacher teacher : pair.getTeachers()) {
                            if (teacher.getTeacherId() != null && userBasicInfos != null
                                    && userBasicInfos.containsKey(teacher.getTeacherId().toString())) {
                                teacher.setTeacherInfo(userBasicInfos.get(teacher.getTeacherId().toString()));
                            }
                        }
                    }
                }
            }
            courseInfo.setFeaturedTeachers(course.getFeaturedTeachers());
            courseInfo.setSearchTerms(course.getSearchTerms());
            courseInfo.setOrgId(course.getOrgId());
        }
        return courseInfo;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String TITLE = "title";
        public static final String SUBJECTS = "subjects";
        public static final String NORMALIZE_SUBJECTS = "normalizeSubjects";
        public static final String TARGETS = "targets";
        public static final String NORMALIZE_TARGETS = "normalizeTargets";
        public static final String GRADES = "grades";
        public static final String ID = "_id";
        public static final String VISIBILITY_STATE = "visibilityState";
        public static final String LAUNCH_DATE = "launchDate";
        public static final String EXIT_DATE = "exitDate";
        public static final String DURATION = "duration";
        public static final String FAQ = "faq";
        public static final String START_PRICE = "startPrice";
        public static final String BATCH_TIMINGS = "batchTimings";
        public static final String FEATURED = "featured";
        public static final String PARENT_COURSE = "parentCourseId";
        public static final String TEACHER_IDS = "teacherIds";
        public static final String PRIORITY = "priority";
        public static final String UPCOMING_BATCHES = "upcomingBatches";
        public static final String TOTAL_BATCHES = "totalBatches";
        public static final String BATCH_START_TIME = "batchStartTime";
        public static final String BATCH_END_TIME = "batchEndTime";
        public static final String SEARCH_TERMS = "searchTerms";
        public static final String BOARD_TEACHER_PAIRS = "boardTeacherPairs";
        public static final String CURRICULUM = "curriculum";
        public static final String EARLY_LEARNING = "earlyLearning";
    }
}
