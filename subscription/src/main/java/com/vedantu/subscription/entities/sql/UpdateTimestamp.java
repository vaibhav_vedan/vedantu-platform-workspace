package com.vedantu.subscription.entities.sql;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreRemove;
import javax.persistence.PreUpdate;

public class UpdateTimestamp {

	@PrePersist
	@PreRemove
	@PreUpdate
	public void update(Subscription subscription) {
		subscription.setLastUpdatedTime(new Date().getTime());
	}

}
