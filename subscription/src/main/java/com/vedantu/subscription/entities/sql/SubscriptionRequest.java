package com.vedantu.subscription.entities.sql;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Lob;
import javax.persistence.Table;

import com.google.gson.Gson;
import com.vedantu.User.Role;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.session.pojo.SessionSlot;
import com.vedantu.subscription.enums.SubModel;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import com.vedantu.util.enums.ScheduleType;
import com.vedantu.util.enums.SessionModel;
import com.vedantu.util.enums.SessionSource;
import com.vedantu.util.subscription.SubscriptionRequestAction;
import com.vedantu.util.subscription.SubscriptionRequestState;
import com.vedantu.util.subscription.SubscriptionRequestSubState;

@Entity
@Table(name = "SubscriptionRequest")
public class SubscriptionRequest extends AbstractSqlLongIdEntity {
	private Long teacherId;
	private Long studentId;
	private String title;
	private String planId;
	@Enumerated(EnumType.STRING)
	private SessionModel model = SessionModel.OTO;
	@Enumerated(EnumType.STRING)
	private SubModel subModel = SubModel.DEFAULT;
	@Enumerated(EnumType.STRING)
	private ScheduleType schedule;
	private Long offeringId;
	private Long totalHours;
	private String subject;
	private String target;
	private Integer grade;
	@Lob
	private String paymentDetails;
	private Long startDate;
	private Long endDate;
	private Long boardId;
	private Long noOfWeeks;
	private String reason; // SubscriptionRequest Reject/Cancel Reason
	private String note; // note while booking SubscriptionRequest
	private Long hourlyRate;
	@Enumerated(EnumType.STRING)
	private SubscriptionRequestState state = SubscriptionRequestState.DRAFT;
	@Enumerated(EnumType.STRING)
	private SubscriptionRequestAction action = SubscriptionRequestAction.PAYMENT_PENDING;
	@Enumerated(EnumType.STRING)
	private SubscriptionRequestSubState subState = SubscriptionRequestSubState.INIT;
	private Long actionPendingBy;
	private Long expiryTime;
	@Lob
	private String slots;
	private Long teacherViewedAt;
	private Long studentViewedAt;
	private Long activatedAt;
	@Enumerated(EnumType.STRING)
	private Role createdByRole;
	private Long amount;
	@Lob
	private String lockBalanceInfo;
	private Long subscriptionId;
	private Long subscriptionDetailsId;
	@Enumerated(EnumType.STRING)
	private SessionSource sessionSource;

	public SubscriptionRequest() {
		super();
	}

	public SubscriptionRequest(Long teacherId, Long studentId, String title, String planId, SessionModel model,
			SubModel subModel, ScheduleType schedule, Long offeringId, Long totalHours, String subject, String target,
			Integer grade, String paymentDetails, Long startDate, Long endDate, Long boardId, Long noOfWeeks,
			String reason, String note, Long hourlyRate, SubscriptionRequestState state,
			SubscriptionRequestAction action, Long actionPendingBy, Long expiryTime, String slots, Long teacherViewedAt,
			Long studentViewedAt, Long activatedAt, Role createdByRole) {
		super();
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.title = title;
		this.planId = planId;
		this.model = model;
		this.subModel = subModel;
		this.schedule = schedule;
		this.offeringId = offeringId;
		this.totalHours = totalHours;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.paymentDetails = paymentDetails;
		this.startDate = startDate;
		this.endDate = endDate;
		this.boardId = boardId;
		this.noOfWeeks = noOfWeeks;
		this.reason = reason;
		this.note = note;
		this.hourlyRate = hourlyRate;
		this.state = state;
		this.action = action;
		this.actionPendingBy = actionPendingBy;
		this.expiryTime = expiryTime;
		this.slots = slots;
		this.teacherViewedAt = teacherViewedAt;
		this.studentViewedAt = studentViewedAt;
		this.activatedAt = activatedAt;
		this.createdByRole = createdByRole;
	}

	public SubscriptionRequest(Long teacherId, Long studentId, String title, String planId, SessionModel model,
			SubModel subModel, ScheduleType schedule, Long offeringId, Long totalHours, String subject, String target,
			Integer grade, String paymentDetails, Long startDate, Long endDate, Long boardId, Long noOfWeeks,
			String reason, String note, Long hourlyRate, SubscriptionRequestState state,
			SubscriptionRequestAction action, Long actionPendingBy, Long expiryTime, String slots, Long teacherViewedAt,
			Long studentViewedAt, Long activatedAt, Role createdByRole, SessionSource sessionSource) {
		super();
		this.teacherId = teacherId;
		this.studentId = studentId;
		this.title = title;
		this.planId = planId;
		this.model = model;
		this.subModel = subModel;
		this.schedule = schedule;
		this.offeringId = offeringId;
		this.totalHours = totalHours;
		this.subject = subject;
		this.target = target;
		this.grade = grade;
		this.paymentDetails = paymentDetails;
		this.startDate = startDate;
		this.endDate = endDate;
		this.boardId = boardId;
		this.noOfWeeks = noOfWeeks;
		this.reason = reason;
		this.note = note;
		this.hourlyRate = hourlyRate;
		this.state = state;
		this.action = action;
		this.actionPendingBy = actionPendingBy;
		this.expiryTime = expiryTime;
		this.slots = slots;
		this.teacherViewedAt = teacherViewedAt;
		this.studentViewedAt = studentViewedAt;
		this.activatedAt = activatedAt;
		this.createdByRole = createdByRole;
		this.sessionSource = sessionSource;
	}

	
	public Long getTeacherId() {
		return teacherId;
	}

	public void setTeacherId(Long teacherId) {
		this.teacherId = teacherId;
	}

	public Long getStudentId() {
		return studentId;
	}

	public void setStudentId(Long studentId) {
		this.studentId = studentId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public SessionModel getModel() {
		return model;
	}

	public void setModel(SessionModel model) {
		this.model = model;
	}

	public ScheduleType getSchedule() {
		return schedule;
	}

	public void setSchedule(ScheduleType schedule) {
		this.schedule = schedule;
	}

	public Long getOfferingId() {
		return offeringId;
	}

	public void setOfferingId(Long offeringId) {
		this.offeringId = offeringId;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getPaymentDetails() {
		return paymentDetails;
	}

	public void setPaymentDetails(String paymentDetails) {
		this.paymentDetails = paymentDetails;
	}

	public Long getStartDate() {
		return startDate;
	}

	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}

	public Long getEndDate() {
		return endDate;
	}

	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}

	public Long getBoardId() {
		return boardId;
	}

	public void setBoardId(Long boardId) {
		this.boardId = boardId;
	}

	public Long getNoOfWeeks() {
		return noOfWeeks;
	}

	public void setNoOfWeeks(Long noOfWeeks) {
		this.noOfWeeks = noOfWeeks;
	}

	public String getReason() {
		return reason;
	}

	public void setReason(String reason) {
		this.reason = reason;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getHourlyRate() {
		return hourlyRate;
	}

	public void setHourlyRate(Long hourlyRate) {
		this.hourlyRate = hourlyRate;
	}

	public SubscriptionRequestState getState() {
		return state;
	}

	public void setState(SubscriptionRequestState state) {
		this.state = state;
	}

	public SubscriptionRequestAction getAction() {
		return action;
	}

	public void setAction(SubscriptionRequestAction action) {
		this.action = action;
	}

	public Long getActionPendingBy() {
		return actionPendingBy;
	}

	public void setActionPendingBy(Long actionPendingBy) {
		this.actionPendingBy = actionPendingBy;
	}

	public Long getExpiryTime() {
		return expiryTime;
	}

	public void setExpiryTime(Long expiryTime) {
		this.expiryTime = expiryTime;
	}

	public String getSlots() {
		return slots;
	}

	public void setSlots(String slots) {
		this.slots = slots;
	}

	public Long getTeacherViewedAt() {
		return teacherViewedAt;
	}

	public void setTeacherViewedAt(Long teacherViewedAt) {
		this.teacherViewedAt = teacherViewedAt;
	}

	public Long getStudentViewedAt() {
		return studentViewedAt;
	}

	public void setStudentViewedAt(Long studentViewedAt) {
		this.studentViewedAt = studentViewedAt;
	}

	public Long getActivatedAt() {
		return activatedAt;
	}

	public void setActivatedAt(Long activatedAt) {
		this.activatedAt = activatedAt;
	}

	public Role getCreatedByRole() {
		return createdByRole;
	}

	public void setCreatedByRole(Role createdByRole) {
		this.createdByRole = createdByRole;
	}

	public SubModel getSubModel() {
		return subModel;
	}

	public void setSubModel(SubModel subModel) {
		this.subModel = subModel;
	}

	public String getLockBalanceInfo() {
		return lockBalanceInfo;
	}

	public void setLockBalanceInfo(String lockBalanceInfo) {
		this.lockBalanceInfo = lockBalanceInfo;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public SubscriptionRequestSubState getSubState() {
		return subState;
	}

	public void setSubState(SubscriptionRequestSubState subState) {
		this.subState = subState;
	}

	public Long getSubscriptionDetailsId() {
		return subscriptionDetailsId;
	}

	public void setSubscriptionDetailsId(Long subscriptionDetailsId) {
		this.subscriptionDetailsId = subscriptionDetailsId;
	}

	public Long getAmount() {
		return amount;
	}

	public void setAmount(Long amount) {
		this.amount = amount;
	}

	public SessionSource getSessionSource() {
		return sessionSource;
	}

	public void setSessionSource(SessionSource sessionSource) {
		this.sessionSource = sessionSource;
	}

	public List<SessionSlot> fetchSubscriptionRequestSlots() {
		List<SessionSlot> sessionSlots = new ArrayList<SessionSlot>();
		SessionSchedule schedule = new Gson().fromJson(this.slots, SessionSchedule.class);
		if (schedule != null) {
			long[] slotBitSetLongArray = schedule.getSlotBitSet();
			if (slotBitSetLongArray == null) {
				// TODO : recalculate the slots
			}

			BitSet bitSet = BitSet.valueOf(slotBitSetLongArray);
			Long slotStartTime = null;
			Long startTime = schedule.getStartTime();
			for (int i = 0; i < bitSet.size(); i++) {
				if (bitSet.get(i)) {
					if (slotStartTime == null) {
						slotStartTime = startTime + i * com.vedantu.util.Constants.SLOT_LENGTH;
					}
				} else {
					if (slotStartTime != null) {
						sessionSlots.add(new SessionSlot(slotStartTime, startTime + i * com.vedantu.util.Constants.SLOT_LENGTH, null,
								null, null, false));
						slotStartTime = null;
					}
				}
			}
		}

		return sessionSlots;
	}

	@Override
	public String toString() {
		return "SubscriptionRequest [teacherId=" + teacherId + ", studentId="
				+ studentId + ", title=" + title + ", planId=" + planId
				+ ", model=" + model + ", subModel=" + subModel + ", schedule="
				+ schedule + ", offeringId=" + offeringId + ", totalHours="
				+ totalHours + ", subject=" + subject + ", target=" + target
				+ ", grade=" + grade + ", paymentDetails=" + paymentDetails
				+ ", startDate=" + startDate + ", endDate=" + endDate
				+ ", boardId=" + boardId + ", noOfWeeks=" + noOfWeeks
				+ ", reason=" + reason + ", note=" + note + ", hourlyRate="
				+ hourlyRate + ", state=" + state + ", action=" + action
				+ ", subState=" + subState + ", actionPendingBy="
				+ actionPendingBy + ", expiryTime=" + expiryTime + ", slots="
				+ slots + ", teacherViewedAt=" + teacherViewedAt
				+ ", studentViewedAt=" + studentViewedAt + ", activatedAt="
				+ activatedAt + ", createdByRole=" + createdByRole
				+ ", amount=" + amount + ", lockBalanceInfo=" + lockBalanceInfo
				+ ", subscriptionId=" + subscriptionId
				+ ", subscriptionDetailsId=" + subscriptionDetailsId
				+ ", sessionSource=" + sessionSource + "]";
	}
}