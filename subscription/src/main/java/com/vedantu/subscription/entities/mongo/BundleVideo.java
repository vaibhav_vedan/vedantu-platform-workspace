package com.vedantu.subscription.entities.mongo;

import com.vedantu.subscription.pojo.VideoBundleContentInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
@Document(collection = "BundleVideo")
public class BundleVideo
        extends AbstractMongoStringIdEntity  {

    private List<BundleVideo> children;
    private List<VideoBundleContentInfo> contents;
    @Indexed(background = true)
    private String title;
    @Indexed(background = true)
    private String subject;
    private String target;
    @Indexed(background = true)
    private Integer grade;
    private Long boardId;
    private String description;

    public List<BundleVideo> getChildren() {
        return children;
    }

    public void setChildren(List<BundleVideo> children) {
        this.children = children;
    }

    public List<VideoBundleContentInfo> getContents() {
        return contents;
    }

    public void setContents(List<VideoBundleContentInfo> contents) {
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String GRADE = "grade";
        public static final String SUBJECT = "subject";
        public static final String TITLE = "title";
        public static final String CHILDREN_TITLE="children.title";

    }

}
