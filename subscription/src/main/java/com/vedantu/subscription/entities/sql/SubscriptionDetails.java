package com.vedantu.subscription.entities.sql;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.vedantu.subscription.request.SubscriptionVO;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;

@Entity
@Table(name="SubscriptionDetails")
public class SubscriptionDetails extends AbstractSqlLongIdEntity {
	private Long subscriptionId;
	private Long totalHours;
	private Long consumedHours;
	private Long lockedHours;
	private Long remainingHours;
	private String teacherCouponId;
	private Long teacherDiscountAmount;
	private Long teacherHourlyRate;

	public SubscriptionDetails() {
		super();
	}

	public SubscriptionDetails(Long subscriptionId, Long totalHours, Long consumedHours, Long lockedHours,
			Long remainingHours, String teacherCouponId, Long teacherDiscountAmount, Long teacherHourlyRate) {
		super();
		this.subscriptionId = subscriptionId;
		this.totalHours = totalHours;
		this.consumedHours = consumedHours;
		this.lockedHours = lockedHours;
		this.remainingHours = remainingHours;
		this.teacherCouponId = teacherCouponId;
		this.teacherDiscountAmount = teacherDiscountAmount;
		this.teacherHourlyRate = teacherHourlyRate;
	}
	
	public SubscriptionDetails(SubscriptionVO subscriptionVO) {
		super();
		this.subscriptionId = subscriptionVO.getSubscriptionId();
		this.totalHours = subscriptionVO.getTotalHours();
		this.teacherCouponId = subscriptionVO.getTeacherCouponId();
		this.teacherDiscountAmount = subscriptionVO.getTeacherDiscountAmount();
		this.teacherHourlyRate = subscriptionVO.getTeacherHourlyRate();
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getTotalHours() {
		return totalHours;
	}

	public void setTotalHours(Long totalHours) {
		this.totalHours = totalHours;
	}

	public Long getConsumedHours() {
		return consumedHours;
	}

	public void setConsumedHours(Long consumedHours) {
		this.consumedHours = consumedHours;
	}

	public Long getLockedHours() {
		return lockedHours;
	}

	public void setLockedHours(Long lockedHours) {
		this.lockedHours = lockedHours;
	}

	public Long getRemainingHours() {
		return remainingHours;
	}

	public void setRemainingHours(Long remainingHours) {
		this.remainingHours = remainingHours;
	}

	public String getTeacherCouponId() {
		return teacherCouponId;
	}

	public void setTeacherCouponId(String teacherCouponId) {
		this.teacherCouponId = teacherCouponId;
	}

	public Long getTeacherDiscountAmount() {
		return teacherDiscountAmount;
	}

	public void setTeacherDiscountAmount(Long teacherDiscountAmount) {
		this.teacherDiscountAmount = teacherDiscountAmount;
	}

	public Long getTeacherHourlyRate() {
		return teacherHourlyRate;
	}

	public void setTeacherHourlyRate(Long teacherHourlyRate) {
		this.teacherHourlyRate = teacherHourlyRate;
	}

	@Override
	public String toString() {
		return super.toString()+" SubscriptionDetails [subscriptionId=" + subscriptionId + ", totalHours=" + totalHours + ", consumedHours="
				+ consumedHours + ",lockedHours =" + lockedHours + ", remainingHours=" + remainingHours
				+ ", teacherCouponId=" + teacherCouponId + ", teacherDiscountAmount=" + teacherDiscountAmount
				+ ", teacherHourlyRate=" + teacherHourlyRate + "]";
	}
}
