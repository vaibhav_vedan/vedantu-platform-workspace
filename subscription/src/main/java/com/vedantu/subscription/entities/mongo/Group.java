package com.vedantu.subscription.entities.mongo;

import java.util.Set;

import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Document(collection = "Group")
public class Group extends AbstractMongoStringIdEntity{

	private String name;
	@Indexed(background = true)
	private String slug;
	@Indexed(background = true)
	private Set<String> batchIds;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSlug() {
		return slug;
	}

	public void setSlug(String slug) {
		this.slug = slug;
	}

	public Set<String> getBatchIds() {
		return batchIds;
	}

	public void setBatchIds(Set<String> batchIds) {
		this.batchIds = batchIds;
	}
}
