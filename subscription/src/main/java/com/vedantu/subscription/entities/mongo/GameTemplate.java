package com.vedantu.subscription.entities.mongo;

import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.subscription.enums.game.GameTaskStatus;
import com.vedantu.subscription.enums.game.GameTaskType;
import com.vedantu.subscription.pojo.game.*;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;
import java.util.stream.Collectors;

/**
 * @author mano
 */
@Getter
@Setter
@ToString
@Document("GameTemplate")
public class GameTemplate extends AbstractMongoStringIdEntity {
    @Indexed(unique = true)
    private String name;
    private List<GameMetaData> eligibleEntities = new ArrayList<>();
    private List<GameTaskTemplate> taskTemplates = new ArrayList<>();
    private List<GameRewardTemplate> rewardTemplates = new ArrayList<>();


    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String NAME = "name";
    }

    public void verify() {
        Set<String> actualTasks = taskTemplates.stream()
                .map(GameTaskTemplate::getName)
                .collect(Collectors.toSet());

        if (actualTasks.isEmpty()) {
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "Game has 0 tasks");
        }

        taskTemplates.stream()
                .map(GameTaskTemplate::getName)
                .filter(StringUtils::isEmpty)
                .findAny()
                .ifPresent(e -> {
                    throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "Task names can't be empty");
                });

        Set<String> rewardsTasks = rewardTemplates.stream()
                .map(GameRewardTemplate::getTasksToComplete)
                .flatMap(List::stream)
                .collect(Collectors.toSet());

        if (!actualTasks.equals(rewardsTasks)) {
            throw new VRuntimeException(ErrorCode.SERVICE_ERROR, "Actual tasks list and rewards tasks list does not match");
        }
    }


    public List<GameTask> setupGameTask() {
        List<GameTask> tasks = new ArrayList<>();
        Map<String, GameTask> tasksMap = new HashMap<>();

        for (GameTaskTemplate taskTemplate : this.taskTemplates) {
            GameTask task = new GameTask();
            task.setName(taskTemplate.getName());
            task.setStatus(GameTaskStatus.INCOMPLETE);
            task.setType(GameTaskType.TASK);

            List<GameSubtaskTemplate> subtaskTemplates = taskTemplate.getSubtaskTemplates();
            List<GameSubtask> subtasks = new ArrayList<>();
            for (GameSubtaskTemplate subtaskTemplate : subtaskTemplates) {
                GameSubtask subtask = new GameSubtask();
                subtask.setName(subtaskTemplate.getName());
                subtask.setStatus(GameTaskStatus.INCOMPLETE);
                subtasks.add(subtask);
            }
            task.setSubtaskList(subtasks);
            tasksMap.put(task.getName(), task);
        }

        for (GameRewardTemplate rewardTemplate : rewardTemplates) {
            List<String> tasksToComplete = rewardTemplate.getTasksToComplete();
            for (String name : tasksToComplete) {
                GameTask task = tasksMap.get(name);
                if (task != null) {
                    tasks.add(task);
                } else {
                    throw new VRuntimeException(ErrorCode.SERVICE_ERROR, String.format("Task %s for game %s not found", name, this.getId()));
                }
            }
            GameTask task = new GameTask();
            task.setName(rewardTemplate.getName());
            task.setStatus(GameTaskStatus.DISABLED);
            task.setType(GameTaskType.REWARD);
            tasks.add(task);
        }
        return tasks;
    }

    public Map<String, Map<String, Integer>> getRewardRatio() {
        Map<String, Map<String, Integer>> ratio = new LinkedHashMap<>(rewardTemplates.size());
        for (GameRewardTemplate rewardTemplate : this.getRewardTemplates()) {
            Map<String, Integer> map = CollectionUtils.putIfAbsentAndGet(ratio, rewardTemplate.getName(), HashMap::new);
            List<GameRewardPojo> availableRewards = rewardTemplate.getAvailableRewards();
            for (GameRewardPojo availableReward : availableRewards) {
                map.putIfAbsent(availableReward.getName(), availableReward.getDistributionRatio());
            }
        }
        return ratio;
    }

    public Optional<GameTaskTemplate> gameTaskTemplateForTask(String task) {
        return taskTemplates.stream().filter(e -> e.getName().equals(task)).findAny();
    }
}
