package com.vedantu.subscription.entities.mongo;

import com.vedantu.subscription.pojo.TestBundleContentInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "BundleTest")
public class BundleTest
        extends AbstractMongoStringIdEntity {

    private List<BundleTest> children;
    private List<TestBundleContentInfo> contents;
    @Indexed(background = true)
    private String title;
    @Indexed(background = true)
    private String subject;
    @Indexed(background = true)
    private String target;
    private Integer grade;
    private Long boardId;
    private String description;
    private Long validFrom;
    private Long validTill;

    public List<BundleTest> getChildren() {
        return children;
    }

    public void setChildren(List<BundleTest> children) {
        this.children = children;
    }

    public List<TestBundleContentInfo> getContents() {
        return contents;
    }

    public void setContents(List<TestBundleContentInfo> contents) {
        this.contents = contents;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Long validFrom) {
        this.validFrom = validFrom;
    }

    public Long getValidTill() {
        return validTill;
    }

    public void setValidTill(Long validTill) {
        this.validTill = validTill;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String GRADE = "grade";
        public static final String SUBJECT = "subject";
        public static final String TITLE = "title";
        public static final String CHILDREN_TITLE="children.title";

    }

}
