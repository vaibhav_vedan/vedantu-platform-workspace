package com.vedantu.subscription.entities.mongo;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.subscription.response.section.SectionInfo;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import java.util.Objects;

/**
 * Created by vimal on 08/05/20.
 */
@Document(collection = "Section")
@Data
@CompoundIndexes({
        @CompoundIndex(name = "batchId_sectionName", def = "{'batchId': 1, 'sectionName': 1}", unique = true, background = true),
        @CompoundIndex(name = "batchId_1_sectionType_1_seatsVacant_-1", def = "{'batchId':1, 'sectionType':1, 'seatsVacant':-1}", background = true)
})
public class Section extends AbstractMongoStringIdEntity {

    // FIELDS
    private String batchId;
    private long size;
    private long seatsVacant;
    private String sectionName;
    private EnrollmentState sectionType;
    // First teacherId to be primary and rest all secondary
    private List<String> teacherIds;

    // CONSTRUCTORS
    public Section(){
        super();
    }

    // GETTERS SETTERS
    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }

    public EnrollmentState getSectionType() {
        return sectionType;
    }

    public void setSectionType(EnrollmentState sectionType) {
        this.sectionType = sectionType;
    }

    public List<String> getTeacherIds() {
        return teacherIds;
    }

    public void setTeacherIds(List<String> teacherIds) {
        this.teacherIds = teacherIds;
    }

    public SectionInfo toSectionInfo(Section section, List<String> teacherEmailIds, long enrollmentCount){
        SectionInfo sectionInfo = new SectionInfo();

        if(Objects.nonNull(section)){
            sectionInfo.setSectionId(section.getId());
            sectionInfo.setSectionName(section.getSectionName());
            sectionInfo.setSectionType(section.getSectionType());
            sectionInfo.setSize(section.getSize());
            sectionInfo.setEnrollmentCount(enrollmentCount);
            sectionInfo.setEntityState(section.getEntityState());
            if(Objects.nonNull(teacherEmailIds) && !teacherEmailIds.isEmpty())
                sectionInfo.setEmailIds(teacherEmailIds);
        }
        return sectionInfo;
    }


    public static class Constants extends AbstractMongoEntity.Constants{
        public static final String SEATS_VACANT = "seatsVacant";
        public static final String SECTION_SIZE = "size";
        public static final String SECTION_NAME = "sectionName";
        public static final String BATCH_ID = "batchId";
        public static final String SECTION_TYPE = "sectionType";
        public static final int MAX_TEACHER_EMAILS_ALLOWED = 10;
        public static final int SECTION_NAME_MAX_LENGTH = 20;
        public static final long MAX_REGULAR_SECTION_SIZE = 500;
        public static final long MAX_TRIAL_SECTION_SIZE = 130000;
        public static final long MIN_TEACHERS_NEEDED_IN_SECTION = 2;
        public static final long MINIMUM_SECTION_SIZE = 10;
        public static final long MINIMUM_SECTIONS_REQUIRED_IN_A_BATCH = 1;
        public static final long MINIMUM_HOURS_FOR_SECTION_UPDATE_BEFORE_SESSION_START = 2;
        public static final long SECTION_BUFFER = 20;
        public static final long SECTION_OFFSET_FOR_BUFFER = 100;
        public static final long ENROLLMENT_OFFSET = 10;
        public static final long MAX_UPDATE_SECTIONS_COUNT = 15000;
        public static final double SECTION_BUFFER_PERCENT = 0.1d;
        public static final String TEACHER_IDS = "teacherIds";

    }
}
