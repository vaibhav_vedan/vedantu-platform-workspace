package com.vedantu.subscription.entities.mongo;


import com.vedantu.subscription.enums.BatchSnapshotType;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;

@Document(collection = "BatchSnapshotOTM")
@CompoundIndexes({
    @CompoundIndex(name = "batchId_monthStartTime", def = "{'batchId': 1, 'monthStartTime': 1}", background = true, unique = true)
})
public class BatchSnapshotOTM extends AbstractMongoStringIdEntity {

    private String batchId;
    private List<SessionSnapshot> sessions;
    private BatchSnapshotType batchSnapshotType;
    private Long monthStartTime;

    public String getBatchId() {
        return batchId;
    }

    public void setBatchId(String batchId) {
        this.batchId = batchId;
    }

    public List<SessionSnapshot> getSessions() {
        return sessions;
    }

    public void setSessions(List<SessionSnapshot> sessions) {
        this.sessions = sessions;
    }

    public BatchSnapshotType getBatchSnapshotType() {
        return batchSnapshotType;
    }

    public void setBatchSnapshotType(BatchSnapshotType batchSnapshotType) {
        this.batchSnapshotType = batchSnapshotType;
    }

    public Long getMonthStartTime() {
        return monthStartTime;
    }

    public void setMonthStartTime(Long monthStartTime) {
        this.monthStartTime = monthStartTime;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String BATCH_ID = "batchId";
        public static final String MONTH_START_TIME = "monthStartTime";
        public static final String BATCH_SNAPSHOT_TYPE = "batchSnapshotType";

    }
}
