/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.entities.mongo;

import com.vedantu.dinero.pojo.BaseInstalmentInfo;
import com.vedantu.onetofew.pojo.FAQPojo;
import com.vedantu.session.pojo.SessionSchedule;
import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;
import com.vedantu.subscription.pojo.CoursePlanAdminData;
import com.vedantu.subscription.pojo.CoursePlanCompactSchedule;
import com.vedantu.subscription.pojo.CourseStateChangeTime;
import com.vedantu.subscription.pojo.OTOCurriculumPojo;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.enums.ScheduleType;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author ajith
 */
@Document(collection = "CoursePlan")
@CompoundIndexes({
    @CompoundIndex(name = "lastUpdated_-1", def = "{'lastUpdated' : -1}", background = true)
    ,
    @CompoundIndex(name = "creationTime_-1", def = "{'creationTime': -1}", background = true),})
public class CoursePlan extends AbstractMongoStringIdEntity {

    private String title;
    @Indexed(background = true)
    private Long teacherId;
    @Indexed(background = true)
    private Long studentId;
    private String subject;
    private String target;
    private Integer grade;
    private Long boardId;
    private Integer totalCourseHours;//will be used for bulk purchase of hrs without schedule and for front end display
    private List<CoursePlanCompactSchedule> compactSchedule;//only for display, no business logic
    private Integer noOfWeeks;
    private CoursePlanState state = CoursePlanState.DRAFT;

    private String description;
    private List<OTOCurriculumPojo> curriculum;
    private Set<Long> suggestedTeacherIds;
    private List<FAQPojo> faq;
    private String parentCourseId;
    private Boolean registeredForParentCourse = false;
    private List<String> tags;
    private Map<String, String> keyValues;
    private String demoVideoUrl;
    //money related
    private Integer price;//includes registration fee, user has to pay price-registration to get ENROLLED
    private SessionSchedule trialSessionSchedule;
    private SessionSchedule regularSessionSchedule;
    private List<BaseInstalmentInfo> instalmentsInfo;
    private ScheduleType scheduleType = ScheduleType.FS;
    private Integer teacherPayoutRate;
    private Integer trialRegistrationFee;

    private Long endTime; // CancelSubscriptionTime
    private String endReason; // Subscription End Reason
    private Long endedBy; // UserId which ended Subscription
    private CoursePlanEnums.CoursePlanEndType endType;
    private Long startDate;
    private Long regularSessionStartDate;
    private List<CoursePlanAdminData> adminData;
    private String armFormId;
    private List<CourseStateChangeTime> stateChangeTime;
    private Integer hourlyRate;//in paisa
    private Integer intendedHours;//millis
    private Long sessionEndDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Long teacherId) {
        this.teacherId = teacherId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Integer getTotalCourseHours() {
        return totalCourseHours;
    }

    public void setTotalCourseHours(Integer totalCourseHours) {
        this.totalCourseHours = totalCourseHours;
    }

    public List<CoursePlanCompactSchedule> getCompactSchedule() {
        return compactSchedule;
    }

    public void setCompactSchedule(List<CoursePlanCompactSchedule> compactSchedule) {
        this.compactSchedule = compactSchedule;
    }

    public Integer getNoOfWeeks() {
        return noOfWeeks;
    }

    public void setNoOfWeeks(Integer noOfWeeks) {
        this.noOfWeeks = noOfWeeks;
    }

    public CoursePlanState getState() {
        return state;
    }

    public void setState(CoursePlanState state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<OTOCurriculumPojo> getCurriculum() {
        return curriculum;
    }

    public void setCurriculum(List<OTOCurriculumPojo> curriculum) {
        this.curriculum = curriculum;
    }

    public Set<Long> getSuggestedTeacherIds() {
        return suggestedTeacherIds;
    }

    public void setSuggestedTeacherIds(Set<Long> suggestedTeacherIds) {
        this.suggestedTeacherIds = suggestedTeacherIds;
    }

    public List<FAQPojo> getFaq() {
        return faq;
    }

    public void setFaq(List<FAQPojo> faq) {
        this.faq = faq;
    }

    public String getParentCourseId() {
        return parentCourseId;
    }

    public void setParentCourseId(String parentCourseId) {
        this.parentCourseId = parentCourseId;
    }

    public Boolean getRegisteredForParentCourse() {
        return registeredForParentCourse;
    }

    public void setRegisteredForParentCourse(Boolean registeredForParentCourse) {
        this.registeredForParentCourse = registeredForParentCourse;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public String getDemoVideoUrl() {
        return demoVideoUrl;
    }

    public void setDemoVideoUrl(String demoVideoUrl) {
        this.demoVideoUrl = demoVideoUrl;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public SessionSchedule getRegularSessionSchedule() {
        return regularSessionSchedule;
    }

    public void setRegularSessionSchedule(SessionSchedule regularSessionSchedule) {
        this.regularSessionSchedule = regularSessionSchedule;
    }

    public ScheduleType getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(ScheduleType scheduleType) {
        this.scheduleType = scheduleType;
    }

    public Integer getTeacherPayoutRate() {
        return teacherPayoutRate;
    }

    public void setTeacherPayoutRate(Integer teacherPayoutRate) {
        this.teacherPayoutRate = teacherPayoutRate;
    }

    public Integer getTrialRegistrationFee() {
        return trialRegistrationFee;
    }

    public void setTrialRegistrationFee(Integer trialRegistrationFee) {
        this.trialRegistrationFee = trialRegistrationFee;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getEndReason() {
        return endReason;
    }

    public void setEndReason(String endReason) {
        this.endReason = endReason;
    }

    public Long getEndedBy() {
        return endedBy;
    }

    public void setEndedBy(Long endedBy) {
        this.endedBy = endedBy;
    }

    public CoursePlanEnums.CoursePlanEndType getEndType() {
        return endType;
    }

    public void setEndType(CoursePlanEnums.CoursePlanEndType endType) {
        this.endType = endType;
    }

    public List<BaseInstalmentInfo> getInstalmentsInfo() {
        return instalmentsInfo;
    }

    public void setInstalmentsInfo(List<BaseInstalmentInfo> instalmentsInfo) {
        this.instalmentsInfo = instalmentsInfo;
    }

    public SessionSchedule getTrialSessionSchedule() {
        return trialSessionSchedule;
    }

    public void setTrialSessionSchedule(SessionSchedule trialSessionSchedule) {
        this.trialSessionSchedule = trialSessionSchedule;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getRegularSessionStartDate() {
        return regularSessionStartDate;
    }

    public void setRegularSessionStartDate(Long regularSessionStartDate) {
        this.regularSessionStartDate = regularSessionStartDate;
    }

    public List<CoursePlanAdminData> getAdminData() {
        return adminData;
    }

    public void setAdminData(List<CoursePlanAdminData> adminData) {
        this.adminData = adminData;
    }

    public String getArmFormId() {
        return armFormId;
    }

    public void setArmFormId(String armFormId) {
        this.armFormId = armFormId;
    }

    public List<CourseStateChangeTime> getStateChangeTime() {
        return stateChangeTime;
    }

    public void setStateChangeTime(List<CourseStateChangeTime> stateChangeTime) {
        this.stateChangeTime = stateChangeTime;
    }

    public Integer getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(Integer hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public Integer getIntendedHours() {
        return intendedHours;
    }

    public void setIntendedHours(Integer intendedHours) {
        this.intendedHours = intendedHours;
    }

    public Long getSessionEndDate() {
        return sessionEndDate;
    }

    public void setSessionEndDate(Long sessionEndDate) {
        this.sessionEndDate = sessionEndDate;
    }
    
    
    

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String STUDENT_ID = "studentId";
        public static final String TEACHER_ID = "teacherId";
        public static final String STATE = "state";
        public static final String SUBJECT = "subject";
        public static final String TARGET = "target";
        public static final String GRADE = "grade";
        public static final String BOARD_ID = "boardId";
        public static final String TITLE = "title";
        public static final String TOTAL_COURSE_HOURS = "totalCourseHours";
        public static final String COMPACT_SCHEDULE = "compactSchedule";
        public static final String DESCRIPTION = "description";
        public static final String PARENT_COURSE_ID = "parentCourseId";
        public static final String PRICE = "price";
        public static final String TEACHER_PAYOUT_RATE = "teacherPayoutRate";
        public static final String TRIAL_REGISTRATION_FEE = "trialRegistrationFee";
        public static final String START_DATE = "startDate";
        public static final String REGULAR_SESSION_START_DATE = "regularSessionStartDate";
        public static final String FIRST_REGULAR_PAYMENT_DUE_DATE = "firstRegularPaymentDueDate";
        public static final String END_TIME = "endTime";
        public static final String END_REASON = "endReason";
        public static final String ENDED_BY = "endedBy";
        public static final String END_TYPE = "endType";
        public static final String STATE_CHANGE_TIME = "stateChangeTime";
        public static final String INSTALMENTS_INFO_DUETIME = "instalmentsInfo.0.dueTime";
        public static final String TRIAL_SESSION_START_TIME = "trialSessionSchedule.sessionSlots.startTime";
        public static final String INTENDED_HOURS="intendedHours";
    }
}
