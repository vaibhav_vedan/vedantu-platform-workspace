package com.vedantu.subscription.entities.mongo;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

public class UserMessage extends AbstractMongoStringIdEntity {
	private String userId;
	private String emailId;
	private String message;
	private String mobileNumber;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getEmailId() {
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public static class Constants {
		public static final String USER_ID = "userId";
		public static final String EMAIL_ID = "emailId";
		public static final String MOBILE_NUMBER = "mobileNumber";
	}

}
