package com.vedantu.subscription.entities.mongo;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;

@Document(collection = "AcadMentorStudents")
@CompoundIndexes({
    @CompoundIndex(name = "studentId", def = "{'studentId': -1}", background = true, unique = true)
})
public class AcadMentorStudents extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long acadMentorId;
    private Long studentId;

    public AcadMentorStudents() {
        super();
    }

    public AcadMentorStudents(Long acadMentorId, Long studentId) {
        super();
        this.acadMentorId = acadMentorId;
        this.studentId = studentId;
    }

    public Long getAcadMentorId() {
        return acadMentorId;
    }

    public void setAcadMentorId(Long acadMentorId) {
        this.acadMentorId = acadMentorId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public static AcadMentorStudents toAcadMentorStudents(Long acadMentorId, Long studentId) {
        AcadMentorStudents acadMentorStudents = new AcadMentorStudents();
        acadMentorStudents.setAcadMentorId(acadMentorId);
        acadMentorStudents.setStudentId(studentId);
        return acadMentorStudents;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String ACADMENTORID = "acadMentorId";
        public static final String STUDENTID = "studentId";
    }
}
