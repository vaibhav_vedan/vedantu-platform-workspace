/**
 * 
 */
package com.vedantu.subscription.entities.mongo;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.subscription.enums.CourseCategory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * @author subarna
 *
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "PremiumSubscription")
@EqualsAndHashCode(callSuper = true)
@ToString
@CompoundIndexes({
    @CompoundIndex(name = "grade_1_board_1_stream_1_target_1_validFrom_1_validTill_1", def = "{'grade': 1, 'board': 1,'stream' : 1, 'target' : 1,"
    		+ "'validFrom' : 1, 'validTill' : 1}", background = true),
    @CompoundIndex(name = "grade_1_target_1_validFrom_1_validTill_1", def = "{'grade': 1, 'target' : 1, 'validFrom' : 1, 'validTill' : 1}", background = true)
})
public class PremiumSubscription extends AbstractMongoStringIdEntity{

	private String title;
	
	private String grade;
	
	private String board;
	
	private String stream;
	
	private String target;
	
	private String medium;
	
	private CourseCategory courseCategory;
	
	private Long validFrom;
	
	private Long validTill;
	
	private Integer	noOfDaysOfFreeAccess;
	
	@Indexed(unique = true, background = true)
	private String bundleId;
	
	
	 public static class Constants extends AbstractMongoEntity.Constants {
		 
		 public static final String TITLE = "title";
		 public static final String GRADE = "grade";
		 public static final String BOARD = "board";
		 public static final String TARGET = "target";
		 public static final String STREAM = "stream";
		 public static final String MEDIUM = "medium";
		 public static final String COURSE_CATEGORY = "courseCategory";
		 public static final String VALID_FROM = "validFrom";
		 public static final String VALID_TILL = "validTill";
		 public static final String NO_OF_DAYS_OF_FREE_ACCESS = "noOfDaysOfFreeAccess";
		 public static final String BUNDLE_ID = "bundleId";
		 

	 }
	 
	
}
