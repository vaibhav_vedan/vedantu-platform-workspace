/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.entities.sql;

import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 *
 * @author ajith
 */
@Entity
@Table(name = "CoursePlanHours")
public class CoursePlanHours extends AbstractSqlLongIdEntity implements Serializable{

    private String coursePlanId;
    private Integer totalHours;//millis
    private Integer consumedHours;//millis
    private Integer lockedHours;//millis
    private Integer remainingHours;//millis

    @Version
    private int version;

    public CoursePlanHours() {
        super();
    }

    public CoursePlanHours(String coursePlanId, Integer totalHours, Integer consumedHours, Integer lockedHours, Integer remainingHours) {
        super();
        this.coursePlanId = coursePlanId;
        this.totalHours = totalHours;
        this.consumedHours = consumedHours;
        this.lockedHours = lockedHours;
        this.remainingHours = remainingHours;
    }
    
    public String getCoursePlanId() {
        return coursePlanId;
    }

    public void setCoursePlanId(String coursePlanId) {
        this.coursePlanId = coursePlanId;
    }

    public Integer getTotalHours() {
        return totalHours;
    }

    public void setTotalHours(Integer totalHours) {
        this.totalHours = totalHours;
    }

    public Integer getConsumedHours() {
        return consumedHours;
    }

    public void setConsumedHours(Integer consumedHours) {
        this.consumedHours = consumedHours;
    }

    public Integer getLockedHours() {
        return lockedHours;
    }

    public void setLockedHours(Integer lockedHours) {
        this.lockedHours = lockedHours;
    }

    public Integer getRemainingHours() {
        return remainingHours;
    }

    public void setRemainingHours(Integer remainingHours) {
        this.remainingHours = remainingHours;
    }
    
}
