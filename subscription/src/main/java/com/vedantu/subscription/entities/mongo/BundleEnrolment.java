package com.vedantu.subscription.entities.mongo;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.EnrollmentStateChangeTime;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by somil on 10/05/17.
 */
@Document(collection = "BundleEnrolment")
@Data
@NoArgsConstructor
@CompoundIndexes({
        @CompoundIndex(name = "userId_bundleId_randomBundleEnrollmentId", def = "{ 'userId': 1,'bundleId' : 1,'randomBundleEnrollmentId':1}", unique = true, background = true),
        @CompoundIndex(name = "bundleId_creationTime", def = "{ 'bundleId': 1,'creationTime' : -1}", background = true),
        @CompoundIndex(name = "userId_state", def = "{'userId': 1, 'state': 1}", background = true),
        @CompoundIndex(name = "userId_bundleId_status_id", def = "{'userId': 1, 'bundleId': 1, 'status': 1, '_id' : 1}", background = true)
})
public class BundleEnrolment extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private String bundleId;

    @Indexed(background = true)
    private String userId;

    @Indexed(background = true)
    private EntityStatus status;

    // Making sure unique entry to get rid of Multiple Active BundleEnrollments for BundleId and UserId
    // UNIQUE in case of active BundleEnrollment
    private String randomBundleEnrollmentId="UNIQUE";

    private EnrollmentState state;
    @Getter(AccessLevel.NONE)
    private List<StatusChangeTime> statusChangeTime;
    private List<EnrollmentStateChangeTime> stateChangeTime;
    private Long endedBy;
    private Long validTill;
    private Long doubtsValidTill;
    private Boolean isSubscriptionPlan;
    private String endReason;

    @Indexed(background = true)
    private Long freePassvalidTill;

    private boolean unlimitedDoubts = false;
    private String trialId;

    private List<CourseTerm> searchTerms;


    private String subscriptionUpdateId;


    public BundleEnrolment(String bundleId, String userId, EntityStatus status, EnrollmentState state) {
        this.bundleId = bundleId;
        this.userId = userId;
        this.status = status;
        this.state = state;
    }

    public List<StatusChangeTime> getStatusChangeTime() {
        if(statusChangeTime == null){
            statusChangeTime= new ArrayList<>();
        }
        return statusChangeTime;
    }


    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String USER_ID = "userId";
        public static final String BUNDLE_ID = "bundleId";
        public static final String STATUS = "status";
        public static final String STATE = "state";
        public static final String VALID_TILL = "validTill";
        public static final String FREE_PASS_VALID_TILL = "freePassvalidTill";
        public static final String CREATION_TIME = "creationTime";
        public static final String STATUS_CHANGE_TIME = "statusChangeTime";
        public static final String PREVIOUS_STATUS="statusChangeTime.previousStatus";
        public static final String NEW_STATUS="statusChangeTime.newStatus";
        public static final String TRIAL_ID="trialId";
        public static final String SEARCH_TERMS = "searchTerms";
        public static final String RANDOM_BUNDLE_ENROLLMENTID ="randomBundleEnrollmentId";
        public static final String IS_SUBSCRIPTION_PLAN = "isSubscriptionPlan";
    }
}
