package com.vedantu.subscription.entities.mongo;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.Language;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "BundleEntity")
@CompoundIndexes({
        @CompoundIndex(def = "{'bundleId': 1, 'packageType': 1}", background = true, useGeneratedName = true),
        @CompoundIndex(def = "{'mainTags': 1,'startTime': 1  ,'endTime': 1}", background = true, useGeneratedName = true),
        @CompoundIndex(def = "{ 'mainTags': 1,'startTime': 1 }", background = true, useGeneratedName = true)
})
public class BundleEntity extends AbstractMongoStringIdEntity {
    private Integer grade;
    @Indexed(background = true)
    private PackageType packageType;
    @Indexed(background = true)
    private String entityId;
    private EnrollmentType enrollmentType;
    private BatchDetails batchDetails;
    private List<String> subjects;
    private Set<String> targets;
    private String courseId;
    private Boolean recordedVideo;
    private Long startTime;
    private Long endTime;
    @Indexed(background = true)
    private String courseTitle;
    @Getter(AccessLevel.NONE)
    private List<CourseTerm> searchTerms;
    @Getter(AccessLevel.NONE)
    @Indexed(background = true)
    private List<String> mainTags;
    private Integer courseCount;
    private Language language = Language.DEFAULT;
    private List<SubscriptionPackageType> subscriptionPackageTypes;
    @Indexed(background = true)
    private String bundleId;
    private boolean isSubscription = false ;

    public List<CourseTerm> getSearchTerms() {
        if(ArrayUtils.isEmpty(searchTerms)){
            searchTerms = new ArrayList<>();
        }
        return searchTerms;
    }

    public List<String> getMainTags() {
        if(ArrayUtils.isEmpty(mainTags)){
            mainTags = new ArrayList<>();
        }
        return mainTags;
    }

    public BundleEntity populateRequiredBundleData(Bundle bundle) {
        this.bundleId = bundle.getId();
        this.isSubscription=bundle.getIsSubscription();
        return this;
    }
    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String BUNDLE_ID = "bundleId";
        public static final String GRADE = "grade";
        public static final String ENTITY_ID = "entityId";
        public static final String ENROLLMENT_TYPE = "enrollmentType";
        public static final String MAIN_TAGS = "mainTags";
        public static final String RECORDED_VIDEO = "recordedVideo";
        public static final String SUBJECTS = "subjects";
        public static final String COURSE_ID = "courseId";
        public static final String START_TIME = "startTime";
        public static final String END_TIME = "endTime";
        public static final String COURSE_TITLE = "courseTitle";
        public static final String SEARCH_TERMS = "searchTerms";
        public static final String IS_SUBSCRIPTION = "isSubscription";
        public static final String LANGUAGE = "language";
        public static final String SUBSCRIPTION_PACKAGES_TYPES = "subscriptionPackageTypes";
        public static final String TARGETS = "targets";
        public static final String PACKAGE_TYPE = "packageType";
        public static final String BATCH_DETAILS = "batchDetails";
        public static final String BATCH_DETAILS_COURSE_TYPE = "batchDetails.courseType";
        public static final String BATCH_DETAILS_TEACHER_INFO = "batchDetails.teacherInfo";
        public static final String BATCH_DETAILS_COURSE_START_TIME = "batchDetails.courseStartTime";

    }

    @Override
    public String toString() {
        return "BundleEntity{" +
                    " \n     grade=" + grade +
                    ",\n     packageType=" + packageType +
                    ",\n     entityId='" + entityId + '\'' +
                    ",\n     enrollmentType=" + enrollmentType +
                    ",\n     batchDetails=" + batchDetails +
                    ",\n     subjects=" + subjects +
                    ",\n     targets=" + targets +
                    ",\n     courseId='" + courseId + '\'' +
                    ",\n     recordedVideo=" + recordedVideo +
                    ",\n     startTime=" + startTime +
                    ",\n     endTime=" + endTime +
                    ",\n     courseTitle='" + courseTitle + '\'' +
                    ",\n     searchTerms=" + searchTerms +
                    ",\n     mainTags=" + mainTags +
                    ",\n     courseCount=" + courseCount +
                    ",\n     language=" + language +
                    ",\n     subscriptionPackageTypes=" + subscriptionPackageTypes +
                    ",\n     bundleId='" + bundleId + '\'' +
                    ",\n     isSubscription=" + isSubscription +
                '}';
    }
}
