/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.entities.sql;

import com.vedantu.subscription.enums.HourTransactionContext;
import com.vedantu.subscription.enums.HourTransactionType;
import com.vedantu.subscription.enums.HourType;
import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "CoursePlanHourTransaction", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "creditTo", "transactionType", "transactionRefNo" }) })
public class CoursePlanHourTransaction extends AbstractSqlLongIdEntity implements Serializable {
	private HourTransactionContext contextType;
        private String contextId;
	@Enumerated(EnumType.STRING)
	private HourType debitFrom;
	@Enumerated(EnumType.STRING)
	private HourType creditTo;
	private Integer duration;// in milliseconds
	@Enumerated(EnumType.STRING)
	private HourTransactionType transactionType;
	private String transactionRefNo;// sessionId or subscriptionDetailsId(in case
									// of create,ended,renewed subscription)
	private String remark;

	private Integer closingConsumedHours;//millis
	private Integer closingLockedHours;//millis
	private Integer closingRemainingHours;//millis


	public CoursePlanHourTransaction(HourTransactionContext contextType, String contextId, HourType debitFrom, HourType creditTo,
						   Integer duration, HourTransactionType transactionType, String transactionRefNo, Integer closingConsumedHours, Integer closingLockedHours, Integer closingRemainingHours) {
		super();
		this.contextType = contextType;
		this.contextId = contextId;
		this.debitFrom = debitFrom;
		this.creditTo = creditTo;
		this.duration = duration;
		this.transactionType = transactionType;
		this.transactionRefNo = transactionRefNo;
		this.closingConsumedHours = closingConsumedHours;
		this.closingLockedHours = closingLockedHours;
		this.closingRemainingHours = closingRemainingHours;
	}



        public HourTransactionContext getContextType() {
            return contextType;
        }

        public void setContextType(HourTransactionContext contextType) {
            this.contextType = contextType;
        }

        public String getContextId() {
            return contextId;
        }

        public void setContextId(String contextId) {
            this.contextId = contextId;
        }


	public HourType getDebitFrom() {
		return debitFrom;
	}

	public void setDebitFrom(HourType debitFrom) {
		this.debitFrom = debitFrom;
	}

	public HourType getCreditTo() {
		return creditTo;
	}

	public void setCreditTo(HourType creditTo) {
		this.creditTo = creditTo;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public HourTransactionType getTransactionType() {
		return transactionType;
	}

	public void setTransactionType(HourTransactionType transactionType) {
		this.transactionType = transactionType;
	}

	public String getTransactionRefNo() {
		return transactionRefNo;
	}

	public void setTransactionRefNo(String transactionRefNo) {
		this.transactionRefNo = transactionRefNo;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}


	public Integer getClosingConsumedHours() {
		return closingConsumedHours;
	}

	public void setClosingConsumedHours(Integer closingConsumedHours) {
		this.closingConsumedHours = closingConsumedHours;
	}

	public Integer getClosingLockedHours() {
		return closingLockedHours;
	}

	public void setClosingLockedHours(Integer closingLockedHours) {
		this.closingLockedHours = closingLockedHours;
	}

	public Integer getClosingRemainingHours() {
		return closingRemainingHours;
	}

	public void setClosingRemainingHours(Integer closingRemainingHours) {
		this.closingRemainingHours = closingRemainingHours;
	}
}