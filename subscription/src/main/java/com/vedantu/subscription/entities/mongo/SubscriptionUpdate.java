package com.vedantu.subscription.entities.mongo;

import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.EnrollmentStateChangeTime;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

/**
 * Created by Dpadhya
 */
@Document(collection = "SubscriptionUpdate")
@Data
@NoArgsConstructor
@AllArgsConstructor
@CompoundIndexes({
        @CompoundIndex(name = "bundleId_1_userId_1_enrollmentStartDate_1", def = "{'bundleId': 1, 'userId': 1,'enrollmentStartDate' : 1}", background = true)
})
public class SubscriptionUpdate extends AbstractMongoStringIdEntity {


    private String bundleId;
    private Long userId;
    @Indexed(background = true)
    private Long enrollmentStartDate;
    private Integer validMonths;
    @Indexed(background = true)
    private Boolean isEnrollmentCreated ;
    @Indexed(background = true)
    private String oldEnrollmentId ;
    private String orderId ;


    public static class Constants extends AbstractMongoStringIdEntity.Constants {


        public static final String OLD_ENROLLMENT_ID = "oldEnrollmentId";
        public static final String BUNDLE_ID = "bundleId";
        public static final String USER_ID = "userId";
        public static final String ENROLLMENT_START_DATE = "enrollmentStartDate";

        public static final String IS_ENROLLMENT_CREATED = "isEnrollmentCreated";

    }
}
