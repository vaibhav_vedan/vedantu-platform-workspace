package com.vedantu.subscription.entities.mongo;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "StudentAccountManagerStudents")
@CompoundIndexes({
        @CompoundIndex(name = "studentId", def = "{'studentId': -1}", background = true, unique = true)
})
public class StudentAccountManagerStudents extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long studentAccountManagerId;
    private Long studentId;

    public StudentAccountManagerStudents() {
        super();
    }

    public StudentAccountManagerStudents(Long studentAccountManagerId, Long studentId) {
        super();
        this.studentAccountManagerId = studentAccountManagerId;
        this.studentId = studentId;
    }

    public Long getStudentAccountManagerId() {
        return studentAccountManagerId;
    }

    public void setStudentAccountManagerId(Long studentAccountManagerId) {
        this.studentAccountManagerId = studentAccountManagerId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {

        public static final String STUDENTACCOUNTMANAGERID = "studentAccountManagerId";
        public static final String STUDENTID = "studentId";
    }
}