package com.vedantu.subscription.entities.mongo;

import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.Indexed;

@Document(collection = "AcadMentorIntervention")
public class AcadMentorIntervention extends AbstractMongoStringIdEntity {

    @Indexed(background = true)
    private Long acadMentorId;
    @Indexed(background = true)
    private Long studentId;
    private String interventionReason;
    private boolean isClosed = false;
    private String acadMentorComment;
    private Long acadMentorCommentTime;

    public Long getAcadMentorId() {
        return acadMentorId;
    }

    public void setAcadMentorId(Long acadMentorId) {
        this.acadMentorId = acadMentorId;
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public String getInterventionReason() {
        return interventionReason;
    }

    public void setInterventionReason(String interventionReason) {
        this.interventionReason = interventionReason;
    }

    public boolean isClosed() {
        return isClosed;
    }

    public void setClosed(boolean isClosed) {
        this.isClosed = isClosed;
    }

    public String getAcadMentorComment() {
        return acadMentorComment;
    }

    public void setAcadMentorComment(String acadMentorComment) {
        this.acadMentorComment = acadMentorComment;
    }

    public Long getAcadMentorCommentTime() {
        return acadMentorCommentTime;
    }

    public void setAcadMentorCommentTime(Long acadMentorCommentTime) {
        this.acadMentorCommentTime = acadMentorCommentTime;
    }
}
