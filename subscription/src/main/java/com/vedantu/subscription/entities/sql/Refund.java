package com.vedantu.subscription.entities.sql;

import com.vedantu.util.dbentities.mysql.AbstractSqlLongIdEntity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "Refund")
public class Refund extends AbstractSqlLongIdEntity {
	private Long subscriptionId;
	private Long refundAmount;
	private String refundReason;

	public Refund() {
		super();
	}

	public Refund(Long subscriptionId, Long refundAmount, String refundReason) {
		super();
		this.subscriptionId = subscriptionId;
		this.refundAmount = refundAmount;
		this.refundReason = refundReason;
	}

	public Long getSubscriptionId() {
		return subscriptionId;
	}

	public void setSubscriptionId(Long subscriptionId) {
		this.subscriptionId = subscriptionId;
	}

	public Long getRefundAmount() {
		return refundAmount;
	}

	public void setRefundAmount(Long refundAmount) {
		this.refundAmount = refundAmount;
	}

	public String getRefundReason() {
		return refundReason;
	}

	public void setRefundReason(String refundReason) {
		this.refundReason = refundReason;
	}

	@Override
	public String toString() {
		return "Subscription [subscriptionId=" + subscriptionId + ",refundAmount=" + refundAmount + ", refundReason=" + refundReason + "]";
	}
}