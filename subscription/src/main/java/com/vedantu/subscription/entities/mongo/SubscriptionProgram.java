package com.vedantu.subscription.entities.mongo;

import com.vedantu.subscription.pojo.bundle.SubscriptionMedia;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlan;
import com.vedantu.subscription.pojo.bundle.SubscriptionPlanHighlights;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by Dpadhya
 */
@Document(collection = "SubscriptionProgram")
@CompoundIndexes({
        @CompoundIndex(name = "grade_1_year_1_target_1", def = "{'grade': 1, 'year': 1,'target' : 1}", background = true, unique = true)
})
@Data
public class SubscriptionProgram extends AbstractMongoStringIdEntity {

    private List<SubscriptionPlan> subscriptionPlans;
    private String name;
    private Integer grade;
    private Integer year;
    private String target;
    private List<SubscriptionMedia> subscriptionMedias;
    private List<String> highLights;
    private List<String> functionality;
    private String offer;
    @Indexed(background = true)
    private Boolean isHidden;
    private List<SubscriptionPlanHighlights> subscriptionPlanHighlights;

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String GRADE = "grade";
        public static final String YEAR = "year";
        public static final String TARGET = "target";
        public static final String IS_HIDDEN = "isHidden";
    }

}
