package com.vedantu.subscription.entities.mongo;

import com.vedantu.lms.cmds.enums.VideoType;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Created by somil on 10/05/17.
 */

@Document(collection = "VideoAnalytics")
public class VideoAnalytics extends AbstractMongoStringIdEntity {
    private Long userId;
    private EntityType contextType;
    private String contextId;
    private String videoId;
    private VideoType videoType;

    public VideoAnalytics() {
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public EntityType getContextType() {
        return contextType;
    }

    public void setContextType(EntityType contextType) {
        this.contextType = contextType;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public VideoType getVideoType() {
        return videoType;
    }

    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }

    public static class Constants extends AbstractMongoStringIdEntity.Constants {
        public static final String USER_ID = "userId";
        public static final String CONTEXT_ID = "contextId";
        public static final String CONTEXT_TYPE = "contextType";
        public static final String VIDEO_ID = "videoId";
        public static final String VIDEO_TYPE = "videoType";

    }
}
