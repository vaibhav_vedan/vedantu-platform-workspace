package com.vedantu.subscription.entities.mongo;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author manishkumarsingh
 */
@Document(collection = "LoanAgreement")
public class LoanAgreement extends AbstractMongoStringIdEntity {
    private Long userId;
    private String entityId;
    private EntityType entityType;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public EntityType getEntityType() {
        return entityType;
    }

    public void setEntityType(EntityType entityType) {
        this.entityType = entityType;
    }
    
    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String USER_ID = "userId";
        public static final String ENTITY_ID = "entityId";
        public static final String ENTITY_TYPE = "entityType";
    }
    
}
