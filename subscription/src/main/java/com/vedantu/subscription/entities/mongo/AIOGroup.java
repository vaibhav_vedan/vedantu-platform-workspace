package com.vedantu.subscription.entities.mongo;

import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;


/**
 * Created by Dpadhya
 */
@Document(collection = "AIOGroup")
@CompoundIndexes({
    @CompoundIndex(name = "groupName_grade_unique",
            unique = true,
            def = "{'groupName' : 1, 'grade' : 1}",
            background = true)
})
public class AIOGroup extends AbstractMongoStringIdEntity {

    private List<String> bundleIds;
    private String groupName;
    private String heading;
    private Integer pageRank;
    private Integer grade;

    public List<String> getBundleIds() {
        return bundleIds;
    }

    public void setBundleIds(List<String> bundleIds) {
        this.bundleIds = bundleIds;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public Integer getPageRank() {
        return pageRank;
    }

    public void setPageRank(Integer pageRank) {
        this.pageRank = pageRank;
    }

    public static class Constants extends AbstractMongoEntity.Constants {
        public static final String GROUP_NAME= "groupName";
        public static final String BUNDLE_IDS = "bundleIds";
        public static final String GRADE = "grade";
        public static final String PAGERANK = "pageRank";
        public static final String HEADING = "heading";

    }

}
