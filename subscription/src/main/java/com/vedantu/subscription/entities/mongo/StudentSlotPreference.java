package com.vedantu.subscription.entities.mongo;

import java.util.List;

import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.onetofew.pojo.OTFSlot;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;

@Document(collection = "StudentSlotPreference")
public class StudentSlotPreference extends AbstractMongoStringIdEntity{

	private String userId;
	private String entityId;
	private EntityType entityType;
	private List<OTFSlot> preferredSlots;
	private Boolean postPayment;
	
	public StudentSlotPreference() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public StudentSlotPreference(String userId, String entityId, EntityType entityType,List<OTFSlot> preferredSlots) {
		super();
		this.userId = userId;
		this.entityId = entityId;
		this.entityType = entityType;
		this.preferredSlots = preferredSlots;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public List<OTFSlot> getPreferredSlots() {
		return preferredSlots;
	}

	public void setPreferredSlots(List<OTFSlot> preferredSlots) {
		this.preferredSlots = preferredSlots;
	}
	
	public String getEntityId() {
		return entityId;
	}

	public void setEntityId(String entityId) {
		this.entityId = entityId;
	}

	public EntityType getEntityType() {
		return entityType;
	}

	public void setEntityType(EntityType entityType) {
		this.entityType = entityType;
	}

	public Boolean getPostPayment() {
		return postPayment;
	}

	public void setPostPayment(Boolean postPayment) {
		this.postPayment = postPayment;
	}

	public static class Constants extends AbstractMongoStringIdEntity.Constants {
		public static final String USER_ID = "userId";
		public static final String ENTITY_ID = "entityId";
		public static final String ENTITY_TYPE = "entityType";
	}
}