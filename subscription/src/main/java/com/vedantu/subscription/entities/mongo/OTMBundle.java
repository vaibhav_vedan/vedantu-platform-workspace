package com.vedantu.subscription.entities.mongo;

import com.google.gson.Gson;
import com.vedantu.onetofew.enums.CourseTerm;
import java.util.List;
import java.util.Map;

import org.springframework.data.mongodb.core.index.CompoundIndex;
import org.springframework.data.mongodb.core.index.CompoundIndexes;
import org.springframework.data.mongodb.core.mapping.Document;

import com.vedantu.onetofew.pojo.OTFSlot;
import com.vedantu.onetofew.pojo.OTMBundleEntityInfo;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.util.ArrayUtils;
import com.vedantu.onetofew.request.AddEditOTFBundleReq;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import java.util.ArrayList;
import javax.persistence.Index;

@Document(collection = "OTMBundle")
@CompoundIndexes({
	@CompoundIndex(name = "entityId_1", def = "{'entities.entityId': 1}", background = true)
	 })
public class OTMBundle extends AbstractMongoStringIdEntity{

	private List<OTMBundleEntityInfo> entities;

    private String title;
    private String target;
    private Integer grade;
    private List<Long> boardIds;
    private String description;
    private Map<String, String> keyValues;
    private Integer price;
    private Integer cutPrice;
    private List<String> tags;
    private boolean featured;
    //lowest is highest
    private Integer priority;
    private String referralCouponCode;
    private Integer registrationFee;
    private List<OTFSlot> allowedSlots;
    private String groupName;
    private Long startTime;
    private VisibilityState visibilityState = VisibilityState.VISIBLE;
    private List<String> teacherInfos;
    private List<String> scheduleInfos;
    private Map<String, Object> bundleInfo;
    private List<String> courseInclusionInfos;
    private List<String> testimonials;
    private Integer registrationCutPrice;
    //teacherInfos
    //scheduleInfo
    //bundleInfo
    //courseInclusionInfo
    private List<CourseTerm> searchTerms;

    public OTMBundle() {
        super();
    }
    

    
    public OTMBundle(AddEditOTFBundleReq req) {
        super();
        this.entities = req.getEntities();
        this.title = req.getTitle();
        this.target = req.getTarget();
        this.grade = req.getGrade();
        this.boardIds = req.getBoardIds();
        this.description = req.getDescription();
        this.keyValues = req.getKeyValues();
        this.price = req.getPrice();
        this.cutPrice = req.getCutPrice();
        this.tags = req.getTags();
        this.featured = req.isFeatured();
        this.priority = req.getPriority();
        this.referralCouponCode = req.getReferralCouponCode();
        this.registrationFee = req.getRegistrationFee();
        this.allowedSlots = req.getAllowedSlots();
    }
    
    public Integer getCutPrice() {
        return cutPrice;
    }

    public void setCutPrice(Integer cutPrice) {
        this.cutPrice = cutPrice;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public Integer getGrade() {
        return grade;
    }

    public void setGrade(Integer grade) {
        this.grade = grade;
    }

    public List<Long> getBoardIds() {
        return boardIds;
    }

    public void setBoardIds(List<Long> boardIds) {
        this.boardIds = boardIds;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Map<String, String> getKeyValues() {
        return keyValues;
    }

    public void setKeyValues(Map<String, String> keyValues) {
        this.keyValues = keyValues;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public boolean isFeatured() {
        return featured;
    }

    public void setFeatured(boolean featured) {
        this.featured = featured;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public List<OTMBundleEntityInfo> getEntities() {
        return entities;
    }

    public void setEntities(List<OTMBundleEntityInfo> entities) {
        this.entities = entities;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public static class Constants extends AbstractMongoEntity.Constants {

        public static final String ENTITIES = "entities";
        public static final String TAGS = "tags";
        public static final String ENTITIES_ENTITY_TYPE = "entities.entityType";
        public static final String TITLE = "title";

    }

    public String getReferralCouponCode() {
        return referralCouponCode;
    }

    public void setReferralCouponCode(String referralCouponCode) {
        this.referralCouponCode = referralCouponCode;
    }

    public Integer getRegistrationFee() {
            return registrationFee;
    }

    public void setRegistrationFee(Integer registrationFee) {
            this.registrationFee = registrationFee;
    }

    public List<OTFSlot> getAllowedSlots() {
            return allowedSlots;
    }

    public void setAllowedSlots(List<OTFSlot> allowedSlots) {
            this.allowedSlots = allowedSlots;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the startTime
     */
    public Long getStartTime() {
        return startTime;
    }

    /**
     * @param startTime the startTime to set
     */
    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    /**
     * @return the visibilityState
     */
    public VisibilityState getVisibilityState() {
        return visibilityState;
    }

    /**
     * @param visibilityState the visibilityState to set
     */
    public void setVisibilityState(VisibilityState visibilityState) {
        this.visibilityState = visibilityState;
    }

    /**
     * @return the teacherInfos
     */
    public List<String> getTeacherInfos() {
        return teacherInfos;
    }

    /**
     * @param teacherInfos the teacherInfos to set
     */
    public void setTeacherInfos(List<Object> teacherInfos) {
        List<String> convertedTeacherInfos = new ArrayList<>();
        
        if(ArrayUtils.isNotEmpty(teacherInfos)){
            Gson gson = new Gson();
            for(Object obj : teacherInfos){
                convertedTeacherInfos.add(gson.toJson(obj));
            }
        }
        this.teacherInfos = convertedTeacherInfos;
    }

    /**
     * @return the scheduleInfo
     */
    public List<String> getScheduleInfos() {
        return scheduleInfos;
    }

    /**
     * @param scheduleInfos the scheduleInfo to set
     */
    public void setScheduleInfos(List<Object> scheduleInfos) {
        List<String> convertedSchedule = new ArrayList<>();
        
        if(ArrayUtils.isNotEmpty(scheduleInfos)){
            Gson gson = new Gson();
            for(Object obj : scheduleInfos){
                convertedSchedule.add(gson.toJson(obj));
            }
        }
        this.scheduleInfos = convertedSchedule;
    }

    /**
     * @return the bundleInfo
     */
    public Map<String, Object> getBundleInfo() {
        return bundleInfo;
    }

    /**
     * @param bundleInfo the bundleInfo to set
     */
    public void setBundleInfo(Map<String, Object> bundleInfo) {
        this.bundleInfo = bundleInfo;
    }

    /**
     * @return the courseInclusionInfos
     */
    public List<String> getCourseInclusionInfos() {
        return courseInclusionInfos;
    }

    /**
     * @param courseInclusionInfos the courseInclusionInfos to set
     */
    public void setCourseInclusionInfos(List<Object> courseInclusionInfos) {
        List<String> convertedInclusions = new ArrayList<>();
        if(ArrayUtils.isNotEmpty(courseInclusionInfos)){
            Gson gson = new Gson();
            for(Object obj : courseInclusionInfos){
                convertedInclusions.add(gson.toJson(obj));
            }
        }
        this.courseInclusionInfos = convertedInclusions;
    }

    /**
     * @return the testimonials
     */
    public List<String> getTestimonials() {
        return testimonials;
    }

    /**
     * @param testimonials the testimonials to set
     */
    public void setTestimonials(List<Object> testimonials) {
        List<String> convertedTestimonials = new ArrayList<>();
       
        if(ArrayUtils.isNotEmpty(testimonials)){
            Gson gson = new Gson();
            for(Object obj : testimonials){
                convertedTestimonials.add(gson.toJson(obj));
            }        
        }        
        this.testimonials = convertedTestimonials;
    }


    public List<CourseTerm> getSearchTerms() {
        return searchTerms;
    }

    public void setSearchTerms(List<CourseTerm> searchTerms) {
        this.searchTerms = searchTerms;
    }

    /**
     * @return the registrationCutPrice
     */
    public Integer getRegistrationCutPrice() {
        return registrationCutPrice;
    }

    /**
     * @param registrationCutPrice the registrationCutPrice to set
     */
    public void setRegistrationCutPrice(Integer registrationCutPrice) {
        this.registrationCutPrice = registrationCutPrice;
    }
        
}
