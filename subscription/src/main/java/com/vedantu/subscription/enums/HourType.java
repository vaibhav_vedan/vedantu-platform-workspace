package com.vedantu.subscription.enums;

public enum HourType {
	DEFAULT, LOCKED, REMAINING, CONSUMED, TOTAL
}
