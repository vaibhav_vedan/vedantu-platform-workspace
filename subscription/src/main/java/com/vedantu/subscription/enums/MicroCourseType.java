package com.vedantu.subscription.enums;

public enum MicroCourseType {
    COURSES, COLLECTIONS, SUBJECT
}
