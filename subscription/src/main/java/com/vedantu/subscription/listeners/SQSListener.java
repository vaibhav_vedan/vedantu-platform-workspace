/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.listeners;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;
import com.vedantu.aws.pojo.AWSSNSSubscriptionRequest;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.exception.VRuntimeException;
import com.vedantu.lms.cmds.pojo.LMSTestInfo;
import com.vedantu.lms.cmds.request.ShareContentEnrollmentReq;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.SNSTopicOTF;
import com.vedantu.onetofew.pojo.OTFBundleInfo;
import com.vedantu.onetofew.request.OTFEnrollmentReq;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.sql.EnrollmentConsumption;
import com.vedantu.subscription.entities.sql.EnrollmentConsumption;
import com.vedantu.subscription.entities.mongo.Section;
import com.vedantu.subscription.managers.*;
import com.vedantu.subscription.pojo.SessionSnapshot;
import com.vedantu.subscription.pojo.game.GameSessionActivity;
import com.vedantu.subscription.pojo.section.EnrollmentListPojo;
import com.vedantu.subscription.request.*;
import com.vedantu.subscription.request.section.*;
import com.vedantu.subscription.response.section.SectionInfoList;
import com.vedantu.subscription.viewobject.request.AddBatchToBundleReq;
import com.vedantu.subscription.viewobject.request.CompleteSessionRequest;
import com.vedantu.subscription.viewobject.request.GamificationToolFilterRequest;
import com.vedantu.subscription.viewobject.request.UnlockHoursRequest;
import com.vedantu.util.*;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Queue;
import javax.jms.TextMessage;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author parashar
 */
@Component
public class SQSListener implements MessageListener {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private EnrollmentConsumptionManager enrollmentConsumptionManager;

    @Autowired
    private BatchSnapshotOTMManager batchSnapshotOTMManager;

    @Autowired
    private AcadMentorInterventionManager acadMentorInterventionManager;

    @Autowired
    private BundleManager bundleManager;
    @Autowired
    private EnrollmentManager enrollmentManager;

    @Autowired
    private GameManager gameManager;

    @Autowired
    private EnrollmentAsyncManager enrollmentAsyncManager;

    @Autowired
    private SectionManager sectionManager;

    @Autowired
    private OTFBundleManager otfBundleManager;

    @Autowired
    private CommunicationManager communicationManager;

    @Autowired
    StatsdClient statsdClient;

    @Autowired
    private SubscriptionManager subscriptionManager;

    @Autowired
    private CoursePlanManager coursePlanManager;

    private static final Gson gson = new Gson();

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(SQSListener.class);

    private String env = ConfigUtils.INSTANCE.getStringValue("environment");
    private String arn = ConfigUtils.INSTANCE.getStringValue("aws.arn");

    @Override
    public void onMessage(Message msg) {
        TextMessage textMessage = (TextMessage) msg;

        try {
            logger.info("Full Message: " + textMessage.getJMSType());
            logger.info("JMS Destination: " + textMessage.getJMSDestination());
            logger.info("JMS reply " + textMessage.getJMSReplyTo());
            logger.info("MessageId: " + textMessage.getJMSMessageID());
            logger.info("Received message " + textMessage.getText());
            String queueName = ((Queue) textMessage.getJMSDestination()).getQueueName();
            Long startTime = System.currentTimeMillis();
            logger.info("Queue name: " + queueName);
            if (textMessage.getStringProperty("messageType") == null) {
                AWSSNSSubscriptionRequest aWSSNSSubscriptionRequest = new Gson().fromJson(textMessage.getText(), AWSSNSSubscriptionRequest.class);
                handleMessage(queueName, aWSSNSSubscriptionRequest);
            } else if (SQSQueue.COMMUNICATE_IN_BULK_QUEUE.getQueueName(env).equals(queueName)) {
                SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
                subscriptionManager.communicateInBulkFromSQS(sqsMessageType, textMessage.getText());
            } else {
                SQSMessageType sqsMessageType = SQSMessageType.valueOf(textMessage.getStringProperty("messageType"));
                handleMessage(sqsMessageType, textMessage.getText());
                if ("PROD".equals(ConfigUtils.INSTANCE.getStringValue("environment").toUpperCase())) {
                    String className = ConfigUtils.INSTANCE.properties.getProperty("application.name") + this.getClass().getSimpleName();
                    if(null != sqsMessageType) {
                        statsdClient.recordCount(className, sqsMessageType.name(), "apicount");
                        statsdClient.recordExecutionTime(System.currentTimeMillis() - startTime, className, sqsMessageType.name());
                    }
                }
            }

            msg.acknowledge();


        } catch (Exception e) {
            logger.error("Error processing message ", e);
        }

        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String getTopicNonEnvArn(String topic) {
        return "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic;
    }

    public String getTopicArn(String topic) {
        return "arn:aws:sns:ap-southeast-1:" + arn + ":" + topic + "_" + env.toUpperCase();
    }

    public void handleMessage(String queueName, AWSSNSSubscriptionRequest aWSSNSSubscriptionRequest) throws VException, ParseException, UnsupportedEncodingException {
        logger.info("Text message: " + aWSSNSSubscriptionRequest);
        if (SQSQueue.CONSUMPTION_QUEUE.getQueueName(env).equals(queueName)) {

            handleConsumptionMessage(aWSSNSSubscriptionRequest.getMessage());

        } else if (SQSQueue.TRIGGER_SESSION_CONSUMPTION_QUEUE.getQueueName(env).equals(queueName)) {

            handleTriggerConsumptionMessage(aWSSNSSubscriptionRequest);

        } else if (SQSQueue.ACAD_MENTOR_DASHBOARD_QUEUE.getQueueName(env).equals(queueName)) {

            //not making a check for Cron_chime_daily
            //for now only one cron chime daily event is pushed from SNS to SQS, so handling it as acadintervention cron for now
            //tomorrow if we add more subscriptions of SNS to SQS, then switch condition will be needed here
            acadMentorInterventionManager.scanAndSaveInterventions(System.currentTimeMillis() - DateTimeUtils.MILLIS_PER_DAY);
        }

    }

    public void handleMessage(SQSMessageType sqsMessageType, String text) throws Exception {

        switch (sqsMessageType) {
            case SCAN_FOR_AM_INTERVENTIONS:
                JsonObject json = gson.fromJson(text, JsonObject.class);
                if (json.get("acadMentorId") == null || json.get("fromTime") == null) {
                    logger.error("invalid message received in sqs listner for SCAN_FOR_AM_INTERVENTIONS " + text);
                    return;
                }
                Long acadMentorId = json.get("acadMentorId").getAsLong();
                Long fromTime = json.get("fromTime").getAsLong();
                acadMentorInterventionManager.scanForInterventionsForAMStudents(acadMentorId, fromTime);
                break;
            case AUTO_ENROLL_ON_COURSE_ADDITION_IN_BUNDLE:
                JsonObject autoEnrolJson = gson.fromJson(text, JsonObject.class);
                String bundleId = autoEnrolJson.get("bundleId").getAsString();
                Type aioPackageType = new TypeToken<BundleEntity>(){}.getType();
                BundleEntity autoenrollotmPackage = gson.fromJson(autoEnrolJson.get("bundleEntity"), aioPackageType);
                logger.info("Package for autoenrollment for bundle id {} with payload {}",bundleId,autoenrollotmPackage.toString());
                bundleManager.autoEnrollForAllEnrolledUserIdsInBundle(bundleId, autoenrollotmPackage);
                break;

            case AUTO_ENROLL_NEW_ENROLLMENT :
                JsonObject autoEnrollJson = gson.fromJson(text, JsonObject.class);
                String _bundleId = autoEnrollJson.get("entityId").getAsString();
                String userId = autoEnrollJson.get("userId").getAsString();
                String enrollmentId = autoEnrollJson.get("enrollmentId").getAsString();
                bundleManager.autoEnrollInBundleCourses(_bundleId,userId,enrollmentId);
                    break;

            case AUTO_ENROLL_BATCHES:
                JsonObject autoEnrollInBatches = gson.fromJson(text, JsonObject.class);
                String enrolledBundleId=autoEnrollInBatches.get("bundleId").getAsString();
                Type bundlePackageType = new TypeToken<BundleEntity>(){}.getType();
                BundleEntity bundleEntity=gson.fromJson(autoEnrollInBatches.get("bundleEntity"), bundlePackageType);
                Type entityStatusType = new TypeToken<EntityStatus>(){}.getType();
                EntityStatus enrollmentStatus=gson.fromJson(autoEnrollInBatches.get("enrollmentStatus"),entityStatusType);
                Type listType = new TypeToken<ArrayList<String>>(){}.getType();
                List<String> batchIds = gson.fromJson(autoEnrollInBatches.get("batchIds"), listType);
                Type oTFBundleInfoType = new TypeToken<OTFBundleInfo>(){}.getType();
                OTFBundleInfo oTFBundleInfo=gson.fromJson(autoEnrollInBatches.get("oTFBundleInfo"),oTFBundleInfoType);
                Type mapType = new TypeToken<HashMap<String, String>>(){}.getType();
                Map<String, String> batchIdCourseIdMap=gson.fromJson(autoEnrollInBatches.get("batchIdCourseIdMap"),mapType);
                Map<String,String> enrollmentIdUserIdMap=gson.fromJson(autoEnrollInBatches.get("enrollmentIdUserIdMap"),mapType);
                bundleManager._autoEnrollInBundleCoursesParallel(enrollmentIdUserIdMap,enrolledBundleId,bundleEntity,enrollmentStatus,batchIds,oTFBundleInfo,batchIdCourseIdMap);
                break;
            case AUTO_ENROLL_CALENDAR_POPULATION:
                JsonObject calendarPopulationObject = gson.fromJson(text, JsonObject.class);
                enrollmentAsyncManager.updateCalendar(
                        calendarPopulationObject.get("userId").getAsString(),
                        calendarPopulationObject.get("batchId").getAsString(),
                        gson.fromJson(calendarPopulationObject.get("enrollmentStatus"),new TypeToken<EntityStatus>(){}.getType())
                );
                break;
            case END_BUNDLE_BATCH_ENROLLMENT:
                JsonObject bundleBatchEnrollmentJson = new Gson().fromJson(text, JsonObject.class);
                Type enrollmentType = new TypeToken<Enrollment>() {
                }.getType();
                Enrollment bundleBatchEnrollment = new Gson().fromJson(bundleBatchEnrollmentJson.get("enrollment"), enrollmentType);
                logger.info("bundle batch enrollment to end -- " + bundleBatchEnrollment.toString());
                bundleManager.endBundleBatchEnrollment(bundleBatchEnrollment);
                break;
            case AUTO_ENROLL_ON_COURSE_ADDITION_IN_BUNDLE_2:
                JsonObject autoEnrolJson2 = gson.fromJson(text, JsonObject.class);
                String bundleId2 = autoEnrolJson2.get("bundleId").getAsString();
                Type aioPackageType2 = new TypeToken<BundleEntity>(){}.getType();
                BundleEntity autoenrollotmPackage2 = gson.fromJson(autoEnrolJson2.get("bundleEntity"), aioPackageType2);
                logger.info("Package for autoenrollment for bundle id {} with payload {}",bundleId2,autoenrollotmPackage2.toString());
                bundleManager.autoEnrollForAllEnrolledUserIdsInBundle(bundleId2, autoenrollotmPackage2);
                break;
            case AUTO_ENROLL_NEW_ENROLLMENT_2 :
                JsonObject autoEnrollJson2 = gson.fromJson(text, JsonObject.class);
                String _bundleId2= autoEnrollJson2.get("entityId").getAsString();
                String userId2 = autoEnrollJson2.get("userId").getAsString();
                String enrollmentId2 = autoEnrollJson2.get("enrollmentId").getAsString();
                bundleManager.autoEnrollInBundleCourses(_bundleId2,userId2,enrollmentId2);
                break;

            case END_BUNDLE_BATCH_ENROLLMENT_2:
                JsonObject bundleBatchEnrollmentJson2 = new Gson().fromJson(text, JsonObject.class);
                Type enrollmentType2 = new TypeToken<Enrollment>() {
                }.getType();
                Enrollment bundleBatchEnrollment2 = new Gson().fromJson(bundleBatchEnrollmentJson2.get("enrollment"), enrollmentType2);
                logger.info("bundle batch enrollment to end -- " + bundleBatchEnrollment2.toString());
                bundleManager.endBundleBatchEnrollment(bundleBatchEnrollment2);
                break;

            case GAME_ONBOARD: {
                GameSetupRequest request = new Gson().fromJson(text, GameSetupRequest.class);
                gameManager.gameOnboarding(request);
            }
            break;
            case GAME_SESSION_ACTIVITY_BATCH: {
                List<GameSessionActivity> activities = gson.fromJson(text, new TypeToken<List<GameSessionActivity>>(){}
                .getType());
                logger.info("Game Activities Size: " + activities.size());
                gameManager.processSessionActivityBatch(activities);
                break;
            }
            case GAME_SESSION_ACTIVITY:
            case GAME_SESSION_JOIN_ACTIVITY:
            case GAME_SESSION_DOWNLOAD_ACTIVITY: {
                logger.info("recieved type {} message {}", sqsMessageType, text);
                GameSessionActivity sessionActivity = gson.fromJson(text, GameSessionActivity.class);
                gameManager.sessionActivity(sessionActivity);
            }
            break;
            case GAME_TEST_ACTIVITY:
                logger.info("entered SQSListener for case: " + sqsMessageType);
                LMSTestInfo lmsTestInfo = new Gson().fromJson(text, LMSTestInfo.class);
                gameManager.checkValidTest(lmsTestInfo);
                break;
            case GAMIFICATION_TOOL_DOWNLOAD:
                logger.info("entered SQSListener for case: " + sqsMessageType);
                GamificationToolFilterRequest request = new Gson().fromJson(text, GamificationToolFilterRequest.class);
                gameManager.downloadClaimedUsersData(request);
                break;
            case ADD_BATCH_IN_SUBSCRIPTION:
                JsonObject addBatchToSubscriptionReq = new Gson().fromJson(text, JsonObject.class);
                Type AddBatchToBundleReqType = new TypeToken<AddBatchToBundleReq>() {
                }.getType();
                AddBatchToBundleReq addBatchToBundleReq = new Gson().fromJson(addBatchToSubscriptionReq, AddBatchToBundleReqType);
                bundleManager.addBatchToBundle(addBatchToBundleReq);
                break;
            case REMOVE_BATCH_FROM_SUBSCRIPTION:
                JsonObject removeBatchFromSubscription = gson.fromJson(text, JsonObject.class);
                bundleManager.removeBatch(removeBatchFromSubscription.get("batchId").getAsString());
                break;
            case ADD_PACKAGES_IN_SUBSCRIPTION:
                JsonObject addPackageInSubs = gson.fromJson(text, JsonObject.class);
                bundleManager.addPackagesInBundle(addPackageInSubs.get("bundleId").getAsString());
                break;
            case EARLY_LEARNING_BUNDLE_CREATION:
                AddEditBundleReq bundleReq = gson.fromJson(text, AddEditBundleReq.class);
                bundleManager.addEditBundle(bundleReq);
                break;
            case CREATE_SECTION:
                logger.info("IN_CONSUMER_CREATE_SECTION");
                AddSectionReq addSectionReq =  gson.fromJson(text,AddSectionReq.class);
                sectionManager.createSections(addSectionReq);
                break;
            case MARK_SECTION_DELETED:
                logger.info("IN_CONSUMER_MARK_SECTION_DELETED");
                UpdateSectionsReq updateSectionsReq = gson.fromJson(text,UpdateSectionsReq.class);
                sectionManager.markSectionsDeleted(updateSectionsReq);
                break;
            case POST_SECTION_CREATION_EMAIL:
                logger.info("IN_CONSUMER_POST_SECTION_CREATION_EMAIL");
                SectionInfoList sectionInfoList = gson.fromJson(text,SectionInfoList.class);
                communicationManager.sendEmailToTeacherForSectionAllotment(sectionInfoList);
                break;
            case ASSIGN_STUDENT_ENROLLMENT_TO_SECTION:
                logger.info("IN_CONSUMER_ASSIGN_STUDENT_ENROLLMENT_TO_SECTION");
                CreateSectionEnrollmentReq createSectionEnrollmentReq = gson.fromJson(text,CreateSectionEnrollmentReq.class);
//                sectionManager.setSectionToEnrollment(createSectionEnrollmentReq.getEnrollment(),createSectionEnrollmentReq.getVacantSection());
                sectionManager.assignStudentToSection(createSectionEnrollmentReq);
                break;
            case CHANGE_SECTION_ENROLLMENT_TO_REGULAR:
                logger.info("IN_CONSUMER_CHANGE_SECTION_ENROLLMENT_TO_REGULAR");
                UpdateSectionEnrollmentReq updateSectionEnrollmentReq = gson.fromJson(text,UpdateSectionEnrollmentReq.class);
//                sectionManager.setSectionToEnrollment(updateSectionEnrollmentReq.getEnrollment(),updateSectionEnrollmentReq.getVacantSection());
                sectionManager.changeSectionEnrollmentToRegular(updateSectionEnrollmentReq);
                break;
            case UPDATE_BULK_ENROLLMENTS:
                logger.info("IN_CONSUMER_UPDATE_BULK_ENROLLMENTS");
                EnrollmentListPojo enrollmentListPojo = gson.fromJson(text,EnrollmentListPojo.class);
                sectionManager.updateEnrollmentsInBulk(enrollmentListPojo);
                break;
            case UPDATE_BUNDLE_BATCH_ENROLLMENTS:
                logger.info("IN_UPDATE_BUNDLE_BATCH_ENROLLMENTS");
                UpdateBundleBatchEnrollmentsReq req = gson.fromJson(text,UpdateBundleBatchEnrollmentsReq.class);
                sectionManager.updateBundleBatchEnrollments(req);
                break;
            case ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE:
                logger.info("IN_ALERT_ACADOPS_FOR_ENROLLMENT_FAILURE");
                EnrollmentFailureReq _req = gson.fromJson(text,EnrollmentFailureReq.class);
                communicationManager.sendEmailAlertsForEnrollmentFailure(_req);
                break;
            case MARK_BUNDLE_ACTIVE_INAVTIVE:
                UpdateStatusReq updateStatusReq = gson.fromJson(text, UpdateStatusReq.class);
                bundleManager.markBundleActiveInActive(updateStatusReq);
                break;

            case MARK_TRIAL_BUNDLE_INAVTIVE:
                UpdateStatusReq _updateStatusReq = gson.fromJson(text, UpdateStatusReq.class);
                bundleManager.markTrialBundleInActive(_updateStatusReq);
                break;
            case MARK_SUBSCRIPTION_INAVTIVE:


                bundleManager.markSubscriptionPlanInActive();

                break;
            case CHECK_FOR_EXISTING_TRIAL:
                BundleEnrolment bundleEnrolment = gson.fromJson(text, BundleEnrolment.class);

                bundleManager.migrateForExistingTrial(bundleEnrolment);
                break;
            case MARK_ENROLLMENT_STATUS:
                OTFEnrollmentReq oTFEnrollmentReq = gson.fromJson(text, OTFEnrollmentReq.class);

                enrollmentManager.markStatus(oTFEnrollmentReq);
                break;
            case RENEW_SUBSCRIPTION:

                BundleEnrolment enrollment = gson.fromJson(text, BundleEnrolment.class);
                bundleManager.renewSubscriptionPlan(enrollment.getId());
//                enrollmentManager.markStatus(oTFEnrollmentReq);


                break;
            case COURSE_PLAN_TRANSFER_HOURS_PRIOR_SESSION_BOOK: {
                AddCoursePlanHoursReq sessionRequest = gson.fromJson(text, AddCoursePlanHoursReq.class);
                coursePlanManager.addCoursePlanHours(sessionRequest);
            }
                break;
            case COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_BOOK: {
                CoursePlanSessionRequest sessionRequest = gson.fromJson(text, CoursePlanSessionRequest.class);
                coursePlanManager.transferBookSessionHours(sessionRequest);
            }
                break;
            case COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_CANCEL: {
                TransferCancelSessionHoursReq cancelSessionHoursReq = gson.fromJson(text, TransferCancelSessionHoursReq.class);
                coursePlanManager.transferCancelSessionHours(cancelSessionHoursReq);
            }
                break;
            case COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_COMPLETE: {
                CompleteSessionRequest completeSessionRequest = gson.fromJson(text, CompleteSessionRequest.class);
                coursePlanManager.completeCoursePlanSession(completeSessionRequest);
            }
                break;
            case COURSE_PLAN_TRANSFER_HOURS_UPON_SESSION_UNLOCK: {
                UnlockHoursRequest unlockHoursRequest = gson.fromJson(text, UnlockHoursRequest.class);
                coursePlanManager.unlockHours(unlockHoursRequest);
            }
                break;


            case TRIGGER_ENROLLMENT_STATUS_EVENTS:
                Enrollment enrollment3 = gson.fromJson(text, Enrollment.class);
                Map<String, Enrollment> newEnrollmentMap = new HashMap<>();
                newEnrollmentMap.put(enrollment3.getId(), enrollment3);
                otfBundleManager.enrollmentTriggers(newEnrollmentMap.keySet(), new HashMap<>(), newEnrollmentMap, false);
                break;

            case VALIDATE_SECTION_REDIS_KEYS:
                BatchSectionReq _r = gson.fromJson(text,BatchSectionReq.class);
                sectionManager.validateSeatsInRedis(_r);
            case POST_ENROLLMENT_CHANGES_TASK:
                SectionEnrollmentReq sectionEnrollmentReq = gson.fromJson(text,SectionEnrollmentReq.class);
                bundleManager.handleSectionChange(sectionEnrollmentReq);
                break;
            case ACADOPS_SECTION_FILLING_ALERTS:
                SeatsFillingReq seatsFillingReq = gson.fromJson(text,SeatsFillingReq.class);
                sectionManager.sendAlertsToAcadOps(seatsFillingReq.getBatchId());
                break;
            default:
                throw new VRuntimeException(ErrorCode.BAD_REQUEST_ERROR, "invalid message received in sqs listner for  " + sqsMessageType.name() + ", " + text);
        }
        logger.info("Message handled");
    }

    public void handleConsumptionMessage(String message) {
        SessionSnapshot sessionSnapshot = new Gson().fromJson(message, SessionSnapshot.class);
        enrollmentConsumptionManager.performSessionConsumption(sessionSnapshot);
    }

    public void handleTriggerConsumptionMessage(AWSSNSSubscriptionRequest aWSSNSSubscriptionRequest) throws ParseException, VException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");

        if (getTopicNonEnvArn(SNSTopicOTF.CRON_CHIME_3HOURLY.toString()).equals(aWSSNSSubscriptionRequest.getTopicArn())) {
            batchSnapshotOTMManager.checkForOTFSessions(sdf.parse(aWSSNSSubscriptionRequest.getTimestamp()).getTime());
        } else if (getTopicNonEnvArn(SNSTopicOTF.CRON_CHIME_DAILY.toString()).equals(aWSSNSSubscriptionRequest.getTopicArn())) {
            batchSnapshotOTMManager.createSnapshots(sdf.parse(aWSSNSSubscriptionRequest.getTimestamp()).getTime());
        }
        logger.info("Message Handled");
    }

}
