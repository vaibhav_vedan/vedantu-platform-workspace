package com.vedantu.subscription.dao;

import com.vedantu.exception.NotFoundException;
import com.vedantu.exception.VException;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleVideo;
import com.vedantu.subscription.pojo.VideoContentData;
import com.vedantu.subscription.viewobject.request.GetBundleTestVideoReq;
import com.vedantu.subscription.viewobject.response.BundleVideoFilterRes;
import com.vedantu.subscription.viewobject.response.BundleVideoRes;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import org.bson.Document;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;


@Service
public class BundleVideoDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(BundleVideo.class);


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(BundleVideo p) {
        // try {
        if (p != null) {
            logger.info("Saving into db" + p);
            saveEntity(p);
        }
    }

    public List<BundleVideo> getVideo(GetBundleTestVideoReq req) {

        Query query = new Query();
        if (req.getGrade() != null) {
            query.addCriteria(Criteria.where(BundleVideo.Constants.GRADE).is(req.getGrade()));
        }
        if (req.getSubject() != null) {
            query.addCriteria(Criteria.where(BundleVideo.Constants.TITLE).is(req.getSubject()));
        }
        if (req.getTitle() != null) {
            int length = req.getTitle().length();
            int index = length < 30 ? length : 30;
            query.addCriteria(Criteria.where(BundleVideo.Constants.CHILDREN_TITLE).regex(req.getTitle().substring(0, index), "i"));
        }
        logger.info("entry 3: " + query);
        return runQuery(query, BundleVideo.class);
    }

    public BundleVideoRes getVideoByBundle(GetBundleTestVideoReq bundleVideo) throws NotFoundException, VException {

        Query query = new Query();
        List<AggregationOperation> aggregationOperation = new ArrayList<AggregationOperation>();

        aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).is(bundleVideo.getBundleId())));
        aggregationOperation.add(Aggregation.unwind(Bundle.Constants.VIDEOS_CHILDREN));

        if (bundleVideo.getGrade() != null) {

            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.VIDEOS_CHILDREN_GRADE).is(bundleVideo.getGrade())));

        }
        if (bundleVideo.getSubject() != null) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.VIDEOS_CHILDREN_SUBJECT).is(bundleVideo.getSubject())));

        }

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);


        logger.info("aggregation  " + aggregation);
        AggregationResults<BundleVideoFilterRes> results = getMongoOperations().aggregate(aggregation,
                Bundle.class, BundleVideoFilterRes.class);
        List<BundleVideoFilterRes> bundles = results.getMappedResults();
        if (bundles != null && !bundles.isEmpty()) {
            return bundles.get(0).getVideos();
        } else {
            return null;
        }
    }


    public VideoContentData getVideoByBundles(GetBundleTestVideoReq bundleVideo) throws NotFoundException, VException {

        Query query = new Query();
        List<AggregationOperation> aggregationOperation = new ArrayList<AggregationOperation>();
        if (bundleVideo.getBundleIds() != null && !bundleVideo.getBundleIds().isEmpty()) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).in(bundleVideo.getBundleIds())));
        } else {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).is(bundleVideo.getBundleId())));
        }
        aggregationOperation.add(Aggregation.unwind(Bundle.Constants.VIDEOS_CHILDREN));

        if (bundleVideo.getGrade() != null) {

            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.VIDEOS_CHILDREN_GRADE).is(bundleVideo.getGrade())));

        }
        if (bundleVideo.getSubject() != null) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.VIDEOS_CHILDREN_SUBJECT).is(bundleVideo.getSubject())));

        }

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);


        logger.info("aggregation  " + aggregation);
        AggregationResults<BundleVideoFilterRes> results = getMongoOperations().aggregate(aggregation,
                Bundle.class, BundleVideoFilterRes.class);
        List<BundleVideoFilterRes> bundles = results.getMappedResults();
        VideoContentData videoContentData = new VideoContentData();
        videoContentData.setChildren(new ArrayList<VideoContentData>());
        if (bundles != null && !bundles.isEmpty()) {
            for (BundleVideoFilterRes bundle : bundles) {
                videoContentData.getChildren().add(bundle.getVideos().getChildren());
            }
            return videoContentData;
        } else {
            return null;
        }
    }

    public List<BundleVideoFilterRes> getVideoDetailsByBundles(GetBundleTestVideoReq bundleVideo) {
        Query query = new Query();
        List<AggregationOperation> aggregationOperation = new ArrayList<AggregationOperation>();

        if (bundleVideo.getBundleIds() != null && !bundleVideo.getBundleIds().isEmpty()) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).in(bundleVideo.getBundleIds())));
        } else {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).is(bundleVideo.getBundleId())));
        }
        aggregationOperation.add(Aggregation.unwind(Bundle.Constants.VIDEOS_CHILDREN));


        if (bundleVideo.getGrade() != null) {

            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.VIDEOS_CHILDREN_GRADE).is(bundleVideo.getGrade())));

        }
        if (bundleVideo.getSubject() != null) {
            aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.VIDEOS_CHILDREN_SUBJECT).is(bundleVideo.getSubject())));

        }

        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);


        logger.info("aggregation  " + aggregation);
        AggregationResults<BundleVideoFilterRes> results = getMongoOperations().aggregate(aggregation,
                Bundle.class, BundleVideoFilterRes.class);
        List<BundleVideoFilterRes> bundles = results.getMappedResults();
        return bundles;
    }
}
