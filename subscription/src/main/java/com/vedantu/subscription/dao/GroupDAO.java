package com.vedantu.subscription.dao;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.mongo.Group;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class GroupDAO extends AbstractMongoDAO {

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public GroupDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public Group getById(String id) {
		Group schedule = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				schedule = (Group) getEntityById(id, Group.class);
			}
		} catch (Exception ex) {
			// log Exception
			schedule = null;
		}
		return schedule;
	}

	public void save(Group p) {
		if (p != null) {
			saveEntity(p);
		}
	}
	
	public void addBatch(String batchId,String slug,String name){
		Query query = new Query();
		query.addCriteria(Criteria.where("slug").is(slug));
		List<Group> groups = runQuery(query,Group.class);
		Group group = null;
		if(ArrayUtils.isNotEmpty(groups)){
			group = groups.get(0);
			Set<String> ids = group.getBatchIds();
			if(ArrayUtils.isEmpty(ids)){
				ids = new HashSet<>();
			}
			ids.add(batchId);
			group.setBatchIds(ids);
			
		}else {
			group = new Group();
			group.setName(name);
			group.setSlug(slug);
			group.setBatchIds(new HashSet<>(Arrays.asList(batchId)));
		}
		
		save(group);
	}
	
	public List<Group> getGroups(String slug,Integer start,Integer size){
		Query query = new Query();
		
		if(StringUtils.isNotEmpty(slug)){
			query.addCriteria(Criteria.where("slug").is(slug));
		}
		setFetchParameters(query, start,size);		
		return runQuery(query,Group.class);
	}
}
