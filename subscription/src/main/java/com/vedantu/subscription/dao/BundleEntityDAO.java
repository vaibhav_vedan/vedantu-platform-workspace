package com.vedantu.subscription.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.enums.EnrollmentType;
import com.vedantu.subscription.enums.bundle.CourseType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.subscription.pojo.bundle.GetBundlePackageReferenceRequest;
import com.vedantu.subscription.request.getMicroCoursesWithFilter;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.subscription.viewobject.request.BundleSearchReq;
import com.vedantu.subscription.viewobject.request.SubscriptionCoursesReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.security.HttpSessionUtils;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.newAggregation;

@Repository
public class BundleEntityDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private HttpSessionUtils httpSessionUtils;

    @Autowired
    private RedisDAO redisDAO;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    private final Logger logger = logFactory.getLogger(BundleEntityDAO.class);

    private static final String AIOPACKAGE_TAGS_LIVE ="LIVE";
    private static final String AIOPACKAGE_TAGS_RECORDED ="RECORDED";

    public BundleEntityDAO() {
        super();
    }

    private void setMandatoryUpdateFields(Update update){
        update.set(AbstractMongoEntity.Constants.LAST_UPDATED,System.currentTimeMillis());
        Long callingUserId = httpSessionUtils.getCallingUserId();
        if(Objects.nonNull(callingUserId)){
            update.set(AbstractMongoEntity.Constants.LAST_UPDATED_BY, callingUserId);
        }else{
            update.set(AbstractMongoEntity.Constants.LAST_UPDATED_BY, "SYSTEM");
        }
    }

    private Query getQueryForActiveData(){
        Query query=new Query();
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        return query;
    }

    public List<BundleEntity> getBundleEntityForEntityId(String bundleId, Collection<String> entityIds, PackageType packageType) {
        Query query = getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).in(entityIds));
        query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).is(packageType));
        return runQuery(query, BundleEntity.class);
    }

    public void insertAllEntities(List<BundleEntity> bundleEntities){
        if(ArrayUtils.isNotEmpty(bundleEntities)) {
            insertAllEntities(bundleEntities, BundleEntity.class.getSimpleName());
        }
    }

    public BundleEntity getBundleEntityByBundleIdAndEntityId(String bundleId, String entityId, Integer grade) throws BadRequestException {
        return getBundleEntityByBundleIdAndEntityId(bundleId,entityId,grade,null);
    }

    private BundleEntity getBundleEntityByBundleIdAndEntityId(String bundleId, String entityId, Integer grade, List<String> includeFields) throws BadRequestException {

        if(StringUtils.isEmpty(bundleId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"bundleId can't be null");
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.GRADE).is(grade));

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        return findOne(query, BundleEntity.class);
    }

    public void save(BundleEntity bundleEntity) {
        saveEntity(bundleEntity);
        logger.info("Bundle entity saved with details: \n"+bundleEntity);
    }

    public void updateBundleEntityData(String id, Update update) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants._ID).is(id));
        setMandatoryUpdateFields(update);
        logger.info("Updating bundle entity with entity id - {}\nquery - {}\nupdate - {}",id,query,update);
        int modifiedCount = updateFirst(query, update, BundleEntity.class);
        logger.info("Modified count for data is - {}", modifiedCount);
    }

    public void removeAllTheReferenceOfSubscriptionBundleEntity(String batchId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(batchId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.IS_SUBSCRIPTION).is(true));

        Update update=new Update();
        update.set(BundleEntity.Constants.ENTITY_STATE, EntityState.DELETED);
        setMandatoryUpdateFields(update);
        logger.info("removeAllTheReferenceOfSubscriptionBundleEntity for batch id - {} \nquery - {}\nupdate - {}",batchId,query,update);
        updateMulti(query,update, BundleEntity.class);
    }

    public List<BundleEntity> getAllBundleEntityByBundleIdAndEntityId(String bundleId, String batchId) {
        return getAllBundleEntityByBundleIdAndEntityId(bundleId,batchId,null);
    }
    public List<BundleEntity> getAllBundleEntityByBundleIdAndEntityId(String bundleId, String batchId, List<String> includeFields) {

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(batchId));

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        return runQuery(query, BundleEntity.class);
    }

    public void deleteMultipleEntitiesById(List<String> ids) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants._ID).in(ids));

        Update update=new Update();
        update.set(BundleEntity.Constants.ENTITY_STATE,EntityState.DELETED);
        setMandatoryUpdateFields(update);
        logger.info("Deleting multiple bundle entities by id having query - {}, update - {}",query,update);
        updateMulti(query,update, BundleEntity.class);
    }

    public void removeAllBundleIdEntityIdCombination(String bundleId, String batchId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(batchId));

        Update update=new Update();
        update.set(BundleEntity.Constants.ENTITY_STATE,EntityState.DELETED);
        setMandatoryUpdateFields(update);
        updateMulti(query,update, BundleEntity.class);    }

    public void removeAllBundleIdsEntityIdCombination(List<String> bundleIds, String batchId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(bundleIds));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(batchId));

        Update update=new Update();
        update.set(BundleEntity.Constants.ENTITY_STATE,EntityState.DELETED);
        setMandatoryUpdateFields(update);
        updateMulti(query,update, BundleEntity.class);    }

    public List<BundleEntity> getBundleEntitiesByBundleId(String bundleId, Integer start, Integer size) throws BadRequestException {
        return getBundleEntitiesByBundleId(bundleId,start,size,null);
    }
    public List<BundleEntity> getBundleEntitiesByBundleId(String bundleId, Integer start, Integer size, List<String> includeFields) throws BadRequestException {
        if(StringUtils.isEmpty(bundleId) || Objects.isNull(start) || Objects.isNull(size)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Invalid parameters present in getBundleEntitiesByBundleId");
        }
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        query.skip(start);
        query.limit(size);

        return runQuery(query, BundleEntity.class);
    }

    public List<String> getAllDistinctSubscriptionBundleIdByBatchId(String batchId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(batchId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.IS_SUBSCRIPTION).is(true));

        query.fields().include(BundleEntity.Constants.BUNDLE_ID);
        query.fields().exclude(BundleEntity.Constants._ID);

        return Optional.ofNullable(runQuery(query, BundleEntity.class))
                        .map(Collection::stream)
                        .orElseGet(Stream::empty)
                        .map(BundleEntity::getBundleId)
                        .distinct()
                        .collect(Collectors.toList());

    }
    public List<BundleEntity> getBatchesByBundles(SubscriptionCoursesReq req) {

        List<String> includeFields = new ArrayList<>();
        List<String> excludeFields = new ArrayList<>();

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(req.getBundleIds()));
        query.addCriteria(Criteria.where(BundleEntity.Constants.IS_SUBSCRIPTION).is(true));
        if (Objects.nonNull(req.getGrade())) {
            includeFields.add(req.getGrade().toString());
            logger.info("Adding grades");
        }
        if(SubscriptionPackageType.TRIAL.equals(req.getSubscriptionPackageType())) {
            logger.info("Setting up trial subscription package types");
            query.addCriteria(Criteria.where(BundleEntity.Constants.SUBSCRIPTION_PACKAGES_TYPES).in(req.getSubscriptionPackageType()));
        }
        if (StringUtils.isNotEmpty(req.getQuery())) {
            logger.info("Going for query...");
            int length = req.getQuery().length();
            int index = length < 40 ? length : 40;
            query.addCriteria(Criteria.where(BundleEntity.Constants.COURSE_TITLE).regex(req.getQuery().substring(0, index), "i"));
        } else {
            logger.info("Going for parameters...");
            if (Objects.nonNull(req.getSubject())) {
                includeFields.add(req.getSubject());
                logger.info("Adding subjects into includeFields");
            }

            if (Objects.nonNull(req.getRecordedVideo())) {
                includeFields.add(req.getRecordedVideo() ? AIOPACKAGE_TAGS_RECORDED : AIOPACKAGE_TAGS_LIVE);
                logger.info("Including for recorded video tag");
            }
            if (StringUtils.isNotEmpty(req.getCourseTime())) {
                Long currentMillis = System.currentTimeMillis();
                long dayInMillis = (long) DateTimeUtils.MILLIS_PER_DAY;
                switch (req.getCourseTime()) {
                    case "ONGOING":
                        logger.info("Searching for ONGOING");
                        query.addCriteria(Criteria.where(BundleEntity.Constants.START_TIME).lt(currentMillis + (dayInMillis * 2)));
                        query.addCriteria(Criteria.where(BundleEntity.Constants.END_TIME).gt(currentMillis));
                        break;

                    case "UPCOMING":
                        logger.info("Searching for UPCOMING");
                        query.addCriteria(Criteria.where(BundleEntity.Constants.START_TIME).gt(currentMillis));
                        break;

                    case "PAST":
                        logger.info("Searching for PAST");
                        query.addCriteria(Criteria.where(BundleEntity.Constants.END_TIME).lt(currentMillis));
                        break;
                }
                if(Objects.nonNull(req.getAscendingOrderStartTimeSort())){
                    if(req.getAscendingOrderStartTimeSort()){
                        query.with(Sort.by(Sort.Direction.ASC, BundleEntity.Constants.START_TIME));
                    }else{
                        query.with(Sort.by(Sort.Direction.DESC, BundleEntity.Constants.START_TIME));
                    }
                    logger.info("Adding query for sorting...");
                }
                logger.info("Adding query for course time");
            }

            if (Objects.nonNull(req.getSearchTerms())) {
                if (CourseTerm.MICRO_COURSES.equals(req.getSearchTerms())) {
                    excludeFields.add(CourseTerm.LONG_TERM.toString());
                    excludeFields.add(CourseTerm.SHORT_TERM.toString());
                    excludeFields.add(CourseTerm.TEST_SERIES.toString());
                    excludeFields.add(CourseTerm.CO_CURRICULAR.toString());
                    logger.info("In case of microcourse excluding certain fields");
                } else {
                    includeFields.add(req.getSearchTerms().toString());
                    logger.info("Including fields in case the thing is not a microcourse");
                }
                logger.info("Adding query for search term");
            }

            if (ArrayUtils.isNotEmpty(req.getExcludeCourseIds())) {
                excludeFields.addAll(req.getExcludeCourseIds());
                logger.info("Taking care of excluding course ids");
            }

            if (ArrayUtils.isNotEmpty(req.getIncludeCourseIds())) {
                includeFields.addAll(req.getIncludeCourseIds());
                logger.info("Taking care of including course ids");
            }

            if (ArrayUtils.isNotEmpty(req.getBatchIds())) {
                query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).in(req.getBatchIds()));
                logger.info("Taking care of including batch ids");
            }

            if (ArrayUtils.isNotEmpty(req.getExcludeBatchIds())) {
                query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).nin(req.getExcludeBatchIds()));
                logger.info("Taking care of excluding batch ids");
            }
        }

        if (ArrayUtils.isEmpty(excludeFields)) {
            query.addCriteria(Criteria.where(BundleEntity.Constants.MAIN_TAGS).all(includeFields));
            logger.info("Adding query for inclusion of fields");
        } else {
            query.addCriteria(new Criteria().andOperator(
                    Criteria.where(BundleEntity.Constants.MAIN_TAGS).all(includeFields),
                    Criteria.where(BundleEntity.Constants.MAIN_TAGS).nin(excludeFields)
            ));
            logger.info("Adding query for both inclusion and exclusion of fields");
        }
        if (Objects.nonNull(req.getStart()) && Objects.nonNull(req.getSize())) {
            query.skip(req.getStart());
            query.limit(req.getSize());
            logger.info("Taking user defined start and size");
        } else {
            query.skip(0);
            query.limit(100);
            logger.info("Taking default start and size as things are not defined properly");
        }
        logger.info("getBatchesByBundles - query - {}",query);
        return runQuery(query, BundleEntity.class);
    }

    public long getCountOfAutoEnrolledPackagesOfBundle(String bundleId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENROLLMENT_TYPE).is(EnrollmentType.AUTO_ENROLL));

        return queryCount(query, BundleEntity.class);
    }

    public long getCountOfBundleIdEntityIdCombination(String bundleId, String entityId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(entityId));

        return queryCount(query, BundleEntity.class);
    }

    public List<String> getAllDistinctBatchesOfBundle(String bundleId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).is(PackageType.OTF));

        query.fields().include(BundleEntity.Constants.ENTITY_ID);

        query.fields().exclude(BundleEntity.Constants._ID);

        logger.info("getAllDistinctBatchesOfBundle - query - {}",query);

        return Optional.ofNullable(runQuery(query, BundleEntity.class))
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(BundleEntity::getEntityId)
                .filter(StringUtils::isNotEmpty)
                .distinct()
                .peek(logger::info)
                .collect(Collectors.toList());
    }

    public List<BundleEntity> getAutoEnrollEntityOfBundle(String bundleId) throws BadRequestException {

        if(StringUtils.isEmpty(bundleId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"bundleId can't be null");
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENROLLMENT_TYPE).is(EnrollmentType.AUTO_ENROLL));

        return runQuery(query,BundleEntity.class);
    }

    public BundleEntity getMicroCourseEntity(String bundleId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        return findOne(query,BundleEntity.class);
    }

    public List<BundleEntity> getPackageDataForMultipleBundles(List<String> bundleIds, Integer start, Integer size,List<String> includeFields) throws BadRequestException {
        if(ArrayUtils.isEmpty(bundleIds) || Objects.isNull(start) || Objects.isNull(size)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Invalid parameters present in getBundleEntitiesByBundleId");
        }
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(bundleIds));

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        query.skip(start);
        query.limit(size);

        return runQuery(query, BundleEntity.class);    }

    public List<BundleEntity> getNonUpsellEntitiesForBundle(String bundleId, Integer start, Integer size) throws BadRequestException {
        if(StringUtils.isEmpty(bundleId) || Objects.isNull(start) || Objects.isNull(size)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"One or more parameter is empty it seems");
        }
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENROLLMENT_TYPE).ne(EnrollmentType.UPSELL));

        query.skip(start);
        query.limit(size);

        return runQuery(query,BundleEntity.class);
    }

    public List<BundleEntity> getAllMicroCourseEntities(Set<String> bundleIds, getMicroCoursesWithFilter req) {

        if(ArrayUtils.isEmpty(bundleIds)){
            return new ArrayList<>();
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(bundleIds));
        query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).ne(PackageType.OTM_BUNDLE));
        query.addCriteria(Criteria.where(BundleEntity.Constants.BATCH_DETAILS).exists(Boolean.TRUE).ne(null));



        query.skip(0);
        query.limit(req.getSize());

        logger.info("micro course bundle entity query {}", query);
        return runQuery(query,BundleEntity.class);
    }

    public boolean getIfEntitiesExistForBundle(String bundleId) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));

        query.fields().include(BundleEntity.Constants._ID);

        return Objects.nonNull(findOne(query,BundleEntity.class));
    }

    public Map<String,List<BundleEntity>> getBundleIdMapByBundleIds(List<String> bundleIds,List<String> includeFields){
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(bundleIds));
        query.with(Sort.by(Sort.Direction.ASC, BundleEntity.Constants.BUNDLE_ID));
        List<BundleEntity>  bundleEntities =  runQuery(query,BundleEntity.class,includeFields);
        Map<String,List<BundleEntity>> bundleEntityMap = new HashMap<>();
        bundleEntities.forEach(bundleEntity -> {
            if(bundleEntityMap.containsKey(bundleEntity.getBundleId())) {
                bundleEntityMap.get(bundleEntity.getBundleId()).add(bundleEntity);
            }
            else{
                bundleEntityMap.put(bundleEntity.getBundleId(),new ArrayList<>(Arrays.asList( bundleEntity)));
            }

        });
        return bundleEntityMap;
    }


    public List<BundleEntity> filterForBundleEntityDataForBundle(String bundleId, EnrollmentType enrollmentType,
                                                                 String entityId, PackageType courseType, Integer grade,
                                                                 String courseId, Integer start, Integer size, List<String> includeFields) throws BadRequestException {
        if(StringUtils.isEmpty(bundleId) && Objects.isNull(start) && Objects.isNull(size)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"All the required parameters are empty");
        }
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        if(Objects.nonNull(enrollmentType)){
            query.addCriteria(Criteria.where(BundleEntity.Constants.ENROLLMENT_TYPE).is(enrollmentType));
        }
        if(StringUtils.isNotEmpty(entityId)){
            query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(entityId));
        }
        if(Objects.nonNull(courseType)){
            query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).is(courseType));
        }
        if(Objects.nonNull(grade)){
            query.addCriteria(Criteria.where(BundleEntity.Constants.GRADE).is(grade));
        }
        if(StringUtils.isNotEmpty(courseId)){
            query.addCriteria(Criteria.where(BundleEntity.Constants.COURSE_ID).is(courseId));
        }

        setFetchParameters(query,start,size);

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        return runQuery(query,BundleEntity.class);
    }

    public BundleEntity getEntityById(String id) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants._ID).is(id));

        return findOne(query,BundleEntity.class);
    }

    public long getCountOfBundleIdCourseIdCombination(String bundleId, String courseId) throws BadRequestException {

        if(StringUtils.isEmpty(bundleId) || StringUtils.isEmpty(courseId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"All the required parameters are empty");
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.COURSE_ID).is(courseId));

        return queryCount(query, BundleEntity.class);
    }

    public List<BundleEntity> getAllOTMBundleEntities(String bundleId, int start, int size) {
        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).is(PackageType.OTM_BUNDLE));

        query.skip(start);
        query.limit(size);

        query.fields().include(BundleEntity.Constants.ENTITY_ID);

        return runQuery(query,BundleEntity.class);
    }

    public Set<String> getFilteredBundleIdForMicroCourse(Set<String> ids, BundleSearchReq req, boolean otmBundlePackage) {
        Query query = getQueryForActiveData();
        if (ids.size() > 1000) {
            logger.error("more than 1000 documents searched for micro course");
        }

        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(ids));
        query.addCriteria(Criteria.where(BundleEntity.Constants.BATCH_DETAILS).exists(Boolean.TRUE).ne(null));

        if (otmBundlePackage) {
            query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).is(PackageType.OTM_BUNDLE));
        } else {
            query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).ne(PackageType.OTM_BUNDLE));
        }

        CourseType courseType = req.getCourseType();
        if (courseType != null) {
            query.addCriteria(Criteria.where(BundleEntity.Constants.BATCH_DETAILS_COURSE_TYPE).is(courseType));
        }
        query.fields().include(BundleEntity.Constants.BUNDLE_ID);

        logger.info("query for microcourse bundle entity {}", query);
        List<String> bundleIds = super.findDistinct(query, BundleEntity.Constants.BUNDLE_ID, BundleEntity.class, String.class);
        return new HashSet<>(bundleIds);
    }

    public List<BundleEntity> getAllBundleIdEntityIdData(String bundleId, List<String> entityIds, List<String> includeFields) {

        if(ArrayUtils.isEmpty(entityIds)){
            return new ArrayList<>();
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).in(entityIds));

        Optional.ofNullable(includeFields).map(Collection::stream).orElseGet(Stream::empty).forEach(query.fields()::include);

        return runQuery(query,BundleEntity.class);
    }

    public List<BundleEntity> getByIds(List<String> ids) {

        if(ArrayUtils.isEmpty(ids)){
            return new ArrayList<>();
        }

        if(ArrayUtils.isNotEmpty(ids) && ids.size()>100){
            logger.error("Cann't fetch size more than 100");
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants._ID).in(ids));

        return runQuery(query,BundleEntity.class);
    }

    public long getCountOfBatchAndBundlesForSubscription(List<String> bundleIds, String batchId) throws BadRequestException {

        if(StringUtils.isEmpty(batchId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"[batchId] cann't be emtpy");
        }

        if(ArrayUtils.isEmpty(bundleIds)){
            return 0;
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(batchId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).is(PackageType.OTF));
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(bundleIds));
        query.addCriteria(Criteria.where(BundleEntity.Constants.IS_SUBSCRIPTION).is(true));

        return queryCount(query,BundleEntity.class);

    }

    public long getCountOfEntitiesWithPropertyExcludingOneIfPossible(String exclusionId, String entityId, Integer grade, String bundleId) throws BadRequestException {

        if(StringUtils.isEmpty(entityId) || StringUtils.isEmpty(bundleId) || Objects.isNull(grade)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Some of the parameters are invalid in the method getCountOfEntitiesWithPropertyExcludingOne");
        }

        Query query=getQueryForActiveData();
        if(StringUtils.isNotEmpty(exclusionId)) {
            query.addCriteria(Criteria.where(BundleEntity.Constants._ID).ne(exclusionId));
        }
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).is(entityId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.GRADE).is(grade));
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        return queryCount(query,BundleEntity.class);
    }


    public List<String> getBatchIdsForBundle(List<String> batchIds, String bundleId) throws BadRequestException {

        if(StringUtils.isEmpty(bundleId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"[bundleId] cann't be emtpy");
        }

        if(ArrayUtils.isEmpty(batchIds)){
            return batchIds;
        }

        Query query=getQueryForActiveData();
        query.addCriteria(Criteria.where(BundleEntity.Constants.ENTITY_ID).in(batchIds));
        query.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).is(PackageType.OTF));
        query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEntity.Constants.IS_SUBSCRIPTION).is(true));

        List<BundleEntity> bundleEntities = runQuery(query,BundleEntity.class);
        return Optional.ofNullable(bundleEntities)
                .map(Collection::stream)
                .orElseGet(Stream::empty).map(BundleEntity :: getEntityId).collect(Collectors.toList());


    }

    public Map<String, BatchDetails> getBatchDetails(List<String> bundleIds)
    {
        logger.info("Getting batch details for bundleIds : {}", bundleIds);
        Map<String, BatchDetails> map = new HashMap<>();
        if (bundleIds != null && !bundleIds.isEmpty())
        {
            AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();

            Criteria filter = new Criteria().andOperator(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(bundleIds),
                    Criteria.where(BundleEntity.Constants.BATCH_DETAILS).exists(true).ne(null),
                    Criteria.where(BundleEntity.Constants.BATCH_DETAILS_COURSE_TYPE).exists(true).ne(null),
                    Criteria.where(BundleEntity.Constants.BATCH_DETAILS_TEACHER_INFO).exists(true).ne(null));
            Aggregation agg = newAggregation(
                    Aggregation.match(filter),
                    Aggregation.group(BundleEntity.Constants.BUNDLE_ID)
                            .first(BundleEntity.Constants.BATCH_DETAILS).as(BundleEntity.Constants.BATCH_DETAILS)
                            .first(BundleEntity.Constants.BUNDLE_ID).as(BundleEntity.Constants.BUNDLE_ID))
                    .withOptions(aggregationOptions);
//            logger.info("Agrregation :"+agg);
            AggregationResults<BundleEntity> groupResults
                    = getMongoOperations().aggregate(agg, BundleEntity.class.getSimpleName(), BundleEntity.class);
            List<BundleEntity> result = groupResults.getMappedResults();
            logger.info("BundleEntities : {}",result);

            result.forEach(bundleEntity -> map.put(bundleEntity.getBundleId(), bundleEntity.getBatchDetails()));
//            logger.info("Batch details map : {}", map);
        }
        return map;
    }

    public List<BundleEntity> getBundleEntityDataForBundleForSalesConversion(GetBundlePackageReferenceRequest getBundlePackageReferenceRequest) {
        List<BundleEntity> bundleEntities = new ArrayList<>();
        if (StringUtils.isNotEmpty(getBundlePackageReferenceRequest.getBundleId())) {
            Query query=getQueryForActiveData();
            query.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).is(getBundlePackageReferenceRequest.getBundleId()));
            query.addCriteria(Criteria.where(BundleEntity.Constants.ENROLLMENT_TYPE).is(EnrollmentType.AUTO_ENROLL));
            query.fields().include(BundleEntity.Constants.ENTITY_ID);
            query.fields().include(BundleEntity.Constants.ENROLLMENT_TYPE);
            setFetchParameters(query, getBundlePackageReferenceRequest.getStart(), getBundlePackageReferenceRequest.getSize());
            bundleEntities = runQuery(query,BundleEntity.class);
        }
        return bundleEntities;
    }
}
