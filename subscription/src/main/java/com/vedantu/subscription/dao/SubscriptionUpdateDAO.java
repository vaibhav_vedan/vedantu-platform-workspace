package com.vedantu.subscription.dao;

import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.subscription.entities.mongo.SubscriptionProgram;
import com.vedantu.subscription.entities.mongo.SubscriptionUpdate;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.DateTimeUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Dpadhya
 */
@Service
public class SubscriptionUpdateDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(SubscriptionUpdateDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public SubscriptionUpdateDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(SubscriptionUpdate p, Long callingUserId) {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            if(StringUtils.isNotEmpty( p.getId()) ){
                SubscriptionUpdate	_p =  getEntityById(p.getId(), SubscriptionUpdate.class);
                if(_p != null) {
                    p.setCreationTime(_p.getCreationTime());
                    p.setCreatedBy(_p.getCreatedBy());
                }
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }
    public Long  getSubscriptionsExpiryDate(String enrollmentId ) {
        Query query = new Query();

        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.OLD_ENROLLMENT_ID).is(enrollmentId));

        query.with(Sort.by(Sort.Direction.DESC, SubscriptionUpdate.Constants.ENROLLMENT_START_DATE));

        logger.info("Query :" + query);
        SubscriptionUpdate subscriptionUpdate= findOne(query, SubscriptionUpdate.class);
        if(subscriptionUpdate == null){
            return 0L;
        }
        return ( subscriptionUpdate.getEnrollmentStartDate() + (subscriptionUpdate.getValidMonths() * 30L * DateTimeUtils.MILLIS_PER_DAY));

    }

    public SubscriptionUpdate  getExpiredSubscriptions(String enrollmentId ) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.IS_ENROLLMENT_CREATED).ne(true));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.OLD_ENROLLMENT_ID).is(enrollmentId));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.ENROLLMENT_START_DATE).lt(System.currentTimeMillis()));


        logger.info("Query :" + query);
        return findOne(query, SubscriptionUpdate.class);
    }

    public void   updateOldEnrollmetId(String enrollmentId , String newEnrollmentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.IS_ENROLLMENT_CREATED).ne(true));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.OLD_ENROLLMENT_ID).is(enrollmentId));

        Update update=new Update();
        update.set(SubscriptionUpdate.Constants.OLD_ENROLLMENT_ID,newEnrollmentId);

        updateMulti(query,update,SubscriptionUpdate.class);


    }



    public void  removeSubscriptionUpdate(String enrollmentId ) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.OLD_ENROLLMENT_ID).is(enrollmentId));

        Update update = new Update();

        update.set(SubscriptionUpdate.Constants.ENTITY_STATE, EntityState.DELETED);
        update.set(SubscriptionUpdate.Constants.LAST_UPDATED,System.currentTimeMillis());
        logger.info("update Query :" + query);
        updateMulti(query, update, SubscriptionUpdate.class);


    }


    public List<SubscriptionUpdate>  getUpdateSubscriptionsId(List<String> enrolmentIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.ENTITY_STATE).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.OLD_ENROLLMENT_ID).in(enrolmentIds));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.IS_ENROLLMENT_CREATED).ne(true));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.ENROLLMENT_START_DATE).lt(System.currentTimeMillis()));
        query.fields().include(SubscriptionUpdate.Constants.OLD_ENROLLMENT_ID);
        logger.info("Query :" + query);
        return runQuery(query, SubscriptionUpdate.class);
    }

    
    public List<SubscriptionUpdate> getSubscriptionsExpiryDateForUserBundle(String userId, String bundleId) {

        Query query = new Query();
        List<SubscriptionUpdate> subscriptionUpdates = new ArrayList<SubscriptionUpdate>();
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.USER_ID).is(Long.valueOf(userId)));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.IS_ENROLLMENT_CREATED).is(false));
        query.with(Sort.by(Sort.Direction.ASC, SubscriptionUpdate.Constants.ENROLLMENT_START_DATE));

        logger.debug("Query :" + query);
        subscriptionUpdates = runQuery(query, SubscriptionUpdate.class);
        return subscriptionUpdates;

    }

    public  List<SubscriptionUpdate>  getSubscriptionPlan(String bundleId,String userId ) {
        Query query = new Query();
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(SubscriptionUpdate.Constants.USER_ID).is(Long.parseLong( userId)));

        query.with(Sort.by(Sort.Direction.ASC, SubscriptionUpdate.Constants.ENROLLMENT_START_DATE));

        logger.info("Query :" + query);
        return runQuery(query, SubscriptionUpdate.class);

    }
}
