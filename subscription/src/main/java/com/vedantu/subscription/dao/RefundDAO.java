package com.vedantu.subscription.dao;

import java.util.List;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.util.dbutils.*;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.sql.Refund;
import com.vedantu.util.LogFactory;

@Service
public class RefundDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {
	
	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(RefundDAO.class);

	public RefundDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;


	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}


	public void create(Refund refund, Session session) {
		create(refund, session, null);
	}

	public void create(Refund refund, Session session, String callingUserId) {
		try {
			if (refund != null) {
				saveEntity(refund, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create Refund: "+ex.getMessage());
		}
	}

	public Refund getById(Long id, Boolean enabled, Session session) {
		Refund refund = null;
		try {
			if (id != null) {
				refund = (Refund) getEntityById(id, enabled, Refund.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById Refund: "+ex.getMessage());
			refund = null;
		}
		return refund;
	}

	public void update(Refund p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateEntity(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("update Refund: "+ex.getMessage());
		}
	}

	public void update(Refund refund, Session session) {
		update(refund, session, null);
	}


	public void updateAll(List<Refund> p, Session session) {
		updateAll(p, session, null);
	}

	public void updateAll(List<Refund> p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateAllEntities(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("updateAll Refund: "+ex.getMessage());
		}
	}
}
