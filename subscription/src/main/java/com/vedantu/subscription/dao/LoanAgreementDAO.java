/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.LoanAgreement;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

import java.util.List;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author manishkumarsingh
 */
@Service
public class LoanAgreementDAO extends AbstractMongoDAO {
    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(LoanAgreementDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public LoanAgreementDAO() {
        super();
    }
    
    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
    
    public void save(LoanAgreement agreement, String callingUserId) {
        saveEntity(agreement, callingUserId);
    }
    
    public LoanAgreement getLoanAgreementById(String id) {
        return getEntityById(id, LoanAgreement.class);
    }
    
    public LoanAgreement getLoanAgreementDetails(Long userId, String entityId) {
        LoanAgreement agreement = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(LoanAgreement.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(LoanAgreement.Constants.ENTITY_ID).is(entityId));
        logger.info("query " + query);
        List<LoanAgreement> results = runQuery(query, LoanAgreement.class);
        if (null != results && !results.isEmpty()) {
            agreement = results.get(0);
        }
        return agreement;
    }
}
