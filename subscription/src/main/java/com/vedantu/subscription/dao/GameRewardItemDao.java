package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.GameRewardItem;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 * @author mano
 */
@Service
public class GameRewardItemDao extends AbstractMongoDAO {


    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(GameRewardItem entity) {
        saveEntity(entity);
    }

    public GameRewardItem getItemByName(String itemName) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GameRewardItem.Constants.NAME).is(itemName));
        return findOne(query, GameRewardItem.class);
    }
}
