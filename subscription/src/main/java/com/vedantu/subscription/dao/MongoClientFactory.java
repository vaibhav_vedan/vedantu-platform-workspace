/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.dao;

import com.vedantu.util.ConfigUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoClientFactory;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */

@Service
public class MongoClientFactory extends AbstractMongoClientFactory {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(MongoClientFactory.class);

    private final String hosts = ConfigUtils.INSTANCE.getStringValue("MONGO_HOST");
    private final String port = ConfigUtils.INSTANCE.getStringValue("MONGO_PORT");
    private final boolean useAuthentication = ConfigUtils.INSTANCE.getBooleanValue("useAuthentication");
    private final String mongoUsername = ConfigUtils.INSTANCE.getStringValue("MONGO_USERNAME");
    private final String mongoPassword = ConfigUtils.INSTANCE.getStringValue("MONGO_PASSWD");
    private final String mongoDBName = ConfigUtils.INSTANCE.getStringValue("MONGO_DB_NAME");
    private final int connectionsPerHost = 50;
    private final String connectionStringType = ConfigUtils.INSTANCE.getStringValue("mongo.connection.string.type", ConnectionStringType.STANDARD_HOST_BASED.name());
    ConnectionStringType _connectionStringType = ConnectionStringType.valueOf(connectionStringType);    

    public MongoClientFactory() {
        super();
        initMongoOperations(hosts, port, useAuthentication, mongoUsername,
                mongoPassword, mongoDBName, connectionsPerHost,_connectionStringType);
        logger.info("Mongo connection created successfully, connections created: " + connectionsPerHost);
    }
}
