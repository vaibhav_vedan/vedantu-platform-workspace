/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.StructuredCourse;
import com.vedantu.subscription.request.GetStructuredCoursesReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.entities.mongo.StructuredCourse;
import com.vedantu.subscription.request.GetStructuredCoursesReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.List;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;


/**
 *
 * @author ajith
 */
@Service
public class StructuredCourseDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(StructuredCourseDAO.class);


    @Autowired
    private MongoClientFactory mongoClientFactory;

    public StructuredCourseDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(StructuredCourse p, Long callingUserId){
        //try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }

    public StructuredCourse getStructuredCourse(String id) {
        return getEntityById(id, StructuredCourse.class);
    }

    public List<StructuredCourse> getStructuredCourses(GetStructuredCoursesReq req) {
        Query query= new Query();
        if(ArrayUtils.isNotEmpty(req.getCourseIds())){
            query.addCriteria(Criteria.where(AbstractMongoEntity.Constants.ID).in(req.getCourseIds()));
        }
        setFetchParameters(query, req);
        return runQuery(query, StructuredCourse.class);
    }
}