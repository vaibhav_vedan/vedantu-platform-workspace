package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.AIOGroup;
import com.vedantu.subscription.entities.mongo.SubscriptionProgram;
import com.vedantu.subscription.request.GetAIOGroupReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

/**
 * Created by Dpadhya
 */
@Service
public class SubscriptionProgramDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	private final Logger logger = logFactory.getLogger(SubscriptionProgramDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public SubscriptionProgramDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void save(SubscriptionProgram p, Long callingUserId) {
		if (p != null) {
			String callingUserIdString = null;
			if (callingUserId != null) {
				callingUserIdString = callingUserId.toString();
			}
			if(StringUtils.isNotEmpty( p.getId()) ){
				SubscriptionProgram	_p =  getEntityById(p.getId(), SubscriptionProgram.class);
				if(_p != null) {
					p.setCreationTime(_p.getCreationTime());
					p.setCreatedBy(_p.getCreatedBy());
				}
			}
			logger.info("Saving into db" + p);
			saveEntity(p, callingUserIdString);
		}
	}

	public List<SubscriptionProgram> getPrograms(Integer grade , String target, Integer year, Boolean displayHidden ) {
		Query query = new Query();
		if(!displayHidden) {
			query.addCriteria(Criteria.where(SubscriptionProgram.Constants.IS_HIDDEN).ne(true));
		}
		if(!Objects.isNull(grade)) {
			query.addCriteria(Criteria.where(SubscriptionProgram.Constants.GRADE).is(grade));
		}
		if(!Objects.isNull(target)) {
			query.addCriteria(Criteria.where(SubscriptionProgram.Constants.TARGET).is(target));
		}
		if(!Objects.isNull(year)) {
			query.addCriteria(Criteria.where(SubscriptionProgram.Constants.YEAR).is(year));
		}

		return runQuery(query, SubscriptionProgram.class);

	}


}
