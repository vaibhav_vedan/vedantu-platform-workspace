package com.vedantu.subscription.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.lms.request.GetBundlesRequest;
import com.vedantu.lms.response.HomeFeedBundleResponse;
import com.vedantu.lms.response.MicroCourseCollectionResponse;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.enums.SubscriptionPackageType;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.entities.mongo.Bundle;
import com.vedantu.subscription.entities.mongo.BundleEnrolment;
import com.vedantu.subscription.entities.mongo.BundleEntity;
import com.vedantu.subscription.entities.mongo.BundleMinMaxExceptionList;
import com.vedantu.subscription.enums.BundleContentType;
import com.vedantu.subscription.enums.bundle.BundleState;
import com.vedantu.subscription.enums.bundle.CourseType;
import com.vedantu.subscription.enums.bundle.PackageType;
import com.vedantu.subscription.pojo.bundle.BatchDetails;
import com.vedantu.subscription.pojo.bundle.BundleDetails;
import com.vedantu.subscription.request.getMicroCoursesWithFilter;
import com.vedantu.subscription.viewobject.request.BundleSearchReq;
import com.vedantu.subscription.request.GetBundlesReq;
import com.vedantu.subscription.viewobject.request.SubscriptionCoursesReq;
import com.vedantu.subscription.viewobject.response.BundleBatchFilterRes;
import com.vedantu.util.*;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.*;
import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;

/**
 * Created by somil on 09/05/17.
 */
@Service
public class BundleDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	private final Logger logger = logFactory.getLogger(BundleDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public static final String AIOPACKAGE_TAGS_LIVE ="LIVE";
	public static final String AIOPACKAGE_TAGS_RECORDED ="RECORDED";

	public BundleDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void save(Bundle p, Long callingUserId) {
		// try {
		if (p != null) {
			String callingUserIdString = null;
			if (callingUserId != null) {
				callingUserIdString = callingUserId.toString();
			}
			logger.info("Saving into db" + p);
			saveEntity(p, callingUserIdString);
		}
	}

	public Bundle getBundleById(String id) {
		 return getEntityById(id, Bundle.class);
	}

	public List<Bundle> getBundles(GetBundlesReq req) {
		Query query = new Query();
		if (!CollectionUtils.isEmpty(req.getStates())) {
			query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_STATE).in(req.getStates()));
		}
		if (req.getFeatured() != null) {
			query.addCriteria(Criteria.where(Bundle.Constants.FEATURED).is(req.getFeatured()));
		}
		if (req.getBundleIds() != null && !req.getBundleIds().isEmpty())  {
			query.addCriteria(Criteria.where(Bundle.Constants._ID).in(req.getBundleIds()));
		}
		if (req.getBundleName() != null )  {
			int length = req.getBundleName().length();
			int index = length < 40 ? length : 40;
			query.addCriteria(Criteria.where(Bundle.Constants.TITLE).regex(req.getBundleName().substring(0, index)));
		}
//		if (!CollectionUtils.isEmpty(req.getContentPriceTypes())) {
//			query.addCriteria(Criteria.where("webinarCategories.webinars.type").in(req.getContentPriceTypes()));
//		}

		Bundle bundle = new Bundle();
		getProjectionQuery(query,req.getReturnContentTypes());
//		query.fields().include(Bundle.Constants.ID);
//		query.fields().include(Bundle.Constants.CREATED_BY);
//		query.fields().include(Bundle.Constants.CREATION_TIME);
//		query.fields().include(Bundle.Constants.LAST_UPDATED);
//		query.fields().include(Bundle.Constants.LAST_UPDATED_BY);


		setFetchParameters(query, req);
		query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
		logger.info("query Bundel is : "+ query.toString());
		return runQuery(query, Bundle.class);
	}
	public List<Bundle> getBundlesdetail(GetBundlesReq req) {
		Query query = new Query();
		if (!CollectionUtils.isEmpty(req.getStates())) {
			query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_STATE).in(req.getStates()));
		}
		if (req.getFeatured() != null) {
			query.addCriteria(Criteria.where(Bundle.Constants.FEATURED).is(req.getFeatured()));
		}
		if (req.getBundleIds() != null && !req.getBundleIds().isEmpty())  {
			query.addCriteria(Criteria.where(Bundle.Constants._ID).in(req.getBundleIds()));
		}
		if (req.getBundleName() != null )  {
			String regex=("^.*".concat(req.getBundleName())).concat(".*$");
			query.addCriteria(Criteria.where(Bundle.Constants.TITLE).regex(regex));
		}
		if (req.getStart() != null )  {

			query.skip(req.getStart());
		}
		if (req.getSize() != null )  {

			query.limit(req.getSize());
		}



		query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.CREATION_TIME));
		logger.info("query Bundel is : "+ query.toString());
		return runQuery(query, Bundle.class);
	}

	public List<Bundle> getBundlesByIds(List<String> bundleIds) {
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		return runQuery(query, Bundle.class);
	}
	public List<Bundle> getBundleById(String bundleId,List<String> includeFields) {
		Query query = new Query(Criteria.where(Bundle.Constants._ID).is(bundleId));
		return runQuery(query, Bundle.class,includeFields);
	}

	public List<Bundle> getBundlesByIds(List<String> bundleIds,List<String> includeFields) {
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		return runQuery(query, Bundle.class,includeFields);
	}

	public List<Bundle> getActiveValidBundlesByIds(List<String> bundleIds, List<String> includeFields) {
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_STATE).is(BundleState.ACTIVE));
		query.addCriteria(Criteria.where(Bundle.Constants.VALID_TILL).gt(System.currentTimeMillis()));
		return runQuery(query, Bundle.class,includeFields);
	}


	public List<Bundle> getBundlesByIdsFilterd(List<String> bundleIds) {
		Query query = new Query(Criteria.where("_id").in(bundleIds));
		query.fields().exclude(Bundle.Constants.VIDEOS);
		query.fields().exclude(Bundle.Constants.TESTS);


		return runQuery(query, Bundle.class);
	}


	public List<Bundle> getNonSubscriptionBundlesPackages(List<String> bundleIds) {
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(false));

		return runQuery(query, Bundle.class);
	}
	public List<Bundle> getSubscriptionBundlesFilterd(List<String> bundleIds) {
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(true));
		query.fields().exclude(Bundle.Constants.VIDEOS);
		query.fields().exclude(Bundle.Constants.TESTS);
		return runQuery(query, Bundle.class);
	}

	public List<Bundle> getBundleByIdAndOTMBundleId(String bundleId,String otfBundleId) {
		Query query=new Query();
		query.addCriteria(Criteria.where(Bundle.Constants._ID).is(bundleId));
		query.addCriteria(Criteria.where(Bundle.Constants.PACKAGES_ENTITYID).is(otfBundleId));
		query.addCriteria(Criteria.where(Bundle.Constants.PACKAGES_ENTITY).is(EntityType.OTM_BUNDLE));// Correction
		return runQuery(query, Bundle.class);
	}

	@Deprecated
	public List<Bundle> getBundleByIdAndOTFEntityId(String bundleId, String otfEntityId){
		Query query=new Query();
		query.addCriteria(Criteria.where(Bundle.Constants._ID).is(bundleId));
		query.addCriteria(Criteria.where(Bundle.Constants.PACKAGES_ENTITYID).is(otfEntityId));
		query.addCriteria(Criteria.where(Bundle.Constants.PACKAGES_ENTITY).is(EntityType.OTF));
		return runQuery(query, Bundle.class);
	}

	public void emptyDisplayTagsForMicroCourse() {
		Query query = new Query();
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DETAILS_DISPLAY_TAG_SIZE).gt(0));
		query.addCriteria(Criteria.where(Bundle.Constants.SEARCH_TERMS).in(CourseTerm.MICRO_COURSES));
		Update update = new Update();
		update.set("bundleDetails.displayTags", new HashSet<>());
		update.set("bundleDetails.displayTagsSize", 0);
		update.set("bundleDetails.displayTagsRank", 0);
		updateMulti(query, update, Bundle.class);
	}

	public void updateBundleDetails(Map<String, BundleDetails> bundleDetailsMap) throws BadRequestException {
		for (String id : bundleDetailsMap.keySet()) {
			Bundle bundle = getBundleById(id);
			if (bundle == null) {
				throw new BadRequestException(ErrorCode.BAD_REQUEST_ERROR, "Bundle not Found " + id );
			}

			{
				BundleDetails details = bundle.getBundleDetails();
				BundleDetails bundleDetails = bundleDetailsMap.get(id);
				if (details == null) {
					details = new BundleDetails();
				}

				String bundleImageUrl = bundleDetails.getBundleImageUrl();
				if (StringUtils.isNotEmpty(bundleImageUrl)) {
					details.setBundleImageUrl(bundleImageUrl);
				}

				LinkedHashSet<String> displayTags = bundleDetails.getDisplayTags();
				if (ArrayUtils.isNotEmpty(displayTags)) {
					details.setDisplayTags(displayTags);
					details.setDisplayTagsRanks(bundleDetails.getDisplayTagsRanks());
				}

				String promotionTag = bundleDetails.getPromotionTag();
				if (StringUtils.isNotEmpty(promotionTag)) {
					details.setPromotionTag(promotionTag.trim());
				}
				if (bundleDetails.getCourseType() != null) {
				details.setCourseType(bundleDetails.getCourseType());
					}

					details.setCourseStartTime(bundleDetails.getCourseStartTime());

				bundle.setBundleDetails(details);
			}
			save(bundle, null);
		}
	}

	public List<Bundle> searchBundle(BundleSearchReq req) {
		Query query = new Query();

		if(StringUtils.isNotEmpty(req.getPromotionTag())) {
			query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_PROMOTION_TAG).is(req.getPromotionTag()));
		}

		getBundleSearchQuery(req, query);
		setBundleProjection(query);

		query.with(Sort.by(Sort.Direction.ASC, AbstractMongoStringIdEntity.Constants.CREATION_TIME));

		setFetchParameters(query, req.getStart(), req.getLimit());
		logger.info("QUERY --->> "  + query);
		return runQuery(query, Bundle.class);
	}

	private void getBundleSearchQuery(BundleSearchReq req, Query query) {
		if (req == null) {
			return;
		}
		List<Integer> grades = req.getGrades();
		if (ArrayUtils.isNotEmpty(grades)) {
			query.addCriteria(Criteria.where(Bundle.Constants.GRADE).in(grades));
		}

		String subject = req.getSubject();
		if (StringUtils.isNotEmpty(subject)) {
			query.addCriteria(Criteria.where(Bundle.Constants.SUBJECT).in(subject));
		}

		List<String> targets = req.getTargets();
		if (ArrayUtils.isNotEmpty(targets)) {
			query.addCriteria(Criteria.where(Bundle.Constants.TARGET)
					.in(targets.stream().map(String::toLowerCase).collect(Collectors.toSet())));
		}

		List<CourseTerm> courseTerms = req.getCourseTerms();
		if (ArrayUtils.isNotEmpty(courseTerms)) {
			query.addCriteria(Criteria.where(Bundle.Constants.SEARCH_TERMS).in(courseTerms));
		}

		String searchTerm = req.getSearchTerm();
		if (StringUtils.isNotEmpty(searchTerm)) {
			String[] split = searchTerm.split("\\s+");
			searchTerm = Arrays.stream(split).map(Pattern::quote).collect(Collectors.joining("\\s+"));
			String regex = ".*" + (searchTerm.trim()) + ".*";
			logger.info("BUNDLE SEARCH REGEX = " + regex);
			query.addCriteria(Criteria.where(Bundle.Constants.TITLE).regex(Pattern.compile(regex, Pattern.CASE_INSENSITIVE)));
		}

		query.addCriteria(new Criteria().orOperator(
				Criteria.where(Bundle.Constants.VALID_TILL).gt(System.currentTimeMillis()),
				Criteria.where(Bundle.Constants.VALID_DAYS).gt(0)
		));
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DETAILS).exists(Boolean.TRUE).ne(null));
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_STATE).is(BundleState.ACTIVE));
	}

	public List<Bundle> groupBundlesByDisplayTags(BundleSearchReq req) {
		Query query = new Query();
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DETAILS_DISPLAY_TAG_SIZE).gt(0));

		getBundleSearchQuery(req, query);
		query.limit(500);
		setBundleProjection(query);

		query.with(Sort.by(Sort.Direction.DESC, Bundle.Constants.LAST_UPDATED));
		logger.info("BUNDLE GROUP DISPLAY TAGS QUERY = " + query);
		return runQuery(query, Bundle.class);
	}

	private void setBundleProjection(Query query) {
		query.fields().include("id")
				.include(Bundle.Constants.BUNDLE_STATE)
				.include(Bundle.Constants.FEATURED)
				.include(Bundle.Constants.TITLE)
				.include(Bundle.Constants.PACKAGES)
				.include(Bundle.Constants.BUNDLE_DETAILS)
				.include(Bundle.Constants.GRADE)
				.include(Bundle.Constants.SUBJECT)
				.include(Bundle.Constants.TARGET)
				.include(Bundle.Constants.VALID_TILL)
				.include(Bundle.Constants.VALID_DAYS)
				.include(Bundle.Constants.PRICE)
				.include(Bundle.Constants.CUT_PRICE)
				.include(Bundle.Constants.SEARCH_TERMS);
	}

	public List<Bundle> getMicroCourseSubscriptionsPackages(BundleSearchReq req) {
		Query query = new Query();
		getBundleSearchQuery(req, query);
		setBundleProjection(query);
		query.with(Sort.by(Sort.Direction.ASC, AbstractMongoStringIdEntity.Constants.CREATION_TIME));
		logger.info("MICRO COURSE SUBSCRIPTION PACKAGE QUERY= "  + query);
		return runQuery(query, Bundle.class);
	}

	public List<BundleEnrolment> getDuplicateEnrollment() throws VException {
		//AggregationOptions aggregationOptions= Aggregation.newAggregationOptions().cursor(new Document()).build();
		List<AggregationOperation> aggregationOperation = new ArrayList<>();
		aggregationOperation.add(Aggregation.match(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE)));
		aggregationOperation.add(Aggregation.match(Criteria.where(BundleEnrolment.Constants.CREATION_TIME).gt(1546353532000L)));
		aggregationOperation.add(Aggregation.group(BundleEnrolment.Constants.USER_ID,BundleEnrolment.Constants.BUNDLE_ID).count().as("count"));
		aggregationOperation.add(Aggregation.match(Criteria.where("count").gt(1)));
//		.withOptions(aggregationOptions)


		AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
		Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);
		AggregationResults<BundleEnrolment> results = getMongoOperations().aggregate(aggregation,
				BundleEnrolment.class, BundleEnrolment.class);
		return results.getMappedResults();

	}

//	@Deprecated
//	public List<BundleBatchFilterRes> getBachesByBundles(SubscriptionCoursesReq req) {
//		List<AggregationOperation> aggregationOperation = new ArrayList<>();
//		List<String> includedfields = new ArrayList<>();
//		List<String> excludedfields = new ArrayList<>();
//		aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants._ID).in(req.getBundleIds())));
//		aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(true)));
//		aggregationOperation.add(Aggregation.unwind(Bundle.Constants.PACKAGES));
//		if (req.getGrade() != null) {
//			includedfields.add(req.getGrade().toString());
////			aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_GRADE).is(req.getGrade())));
//		}
//		if(SubscriptionPackageType.TRIAL.equals( req.getSubscriptionPackageType() )) {
//			aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_SUBSCRIPTION_TYPE).in(SubscriptionPackageType.TRIAL)));
//		}
//		if (StringUtils.isNotEmpty(req.getQuery())) {
//			int length = req.getQuery().length();
//			int index = length < 40 ? length : 40;
//			aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_COURSETITLE).regex(req.getQuery().substring(0, index), "i")));
//		} else {
//			if (req.getSubject() != null) {
//				includedfields.add(req.getSubject());
//			}
//			if (StringUtils.isNotEmpty(req.getQuery())) {
//				int length = req.getQuery().length();
//				int index = length < 40 ? length : 40;
//				aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_COURSETITLE).regex(req.getQuery().substring(0, index), "i")));
//			}
//
//			if (req.getRecordedVideo() != null) {
//				includedfields.add(req.getRecordedVideo() ? AIOPACKAGE_TAGS_RECORDED : AIOPACKAGE_TAGS_LIVE);
//			}
//			if (StringUtils.isNotEmpty(req.getCourseTime())) {
//				Long currentMillis = System.currentTimeMillis();
//				Long dayInMillis = (long) DateTimeUtils.MILLIS_PER_DAY;
//				switch (req.getCourseTime()) {
//					case "ONGOING":
//						aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_STARTTIME).lt(currentMillis + (dayInMillis * 2))));
//						aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_ENDTIME).gt(currentMillis)));
//						break;
//
//					case "UPCOMING":
//						aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_STARTTIME).gt(currentMillis)));
//						break;
//
//					case "PAST":
//						aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_ENDTIME).lt(currentMillis)));
//
//						break;
//				}
//				if(Objects.nonNull(req.getAscendingOrderStartTimeSort())){
//					if(req.getAscendingOrderStartTimeSort()){
//						aggregationOperation.add(Aggregation.sort(Sort.Direction.ASC,Bundle.Constants.PACKAGES_STARTTIME));
//					}else{
//						aggregationOperation.add(Aggregation.sort(Sort.Direction.DESC,Bundle.Constants.PACKAGES_STARTTIME));
//					}
//				}
//
//			}
//
//			if (req.getSearchTerms() != null) {
//				if (CourseTerm.MICRO_COURSES.equals(req.getSearchTerms())) {
//					excludedfields.add(CourseTerm.LONG_TERM.toString());
//					excludedfields.add(CourseTerm.SHORT_TERM.toString());
//					excludedfields.add(CourseTerm.TEST_SERIES.toString());
//					excludedfields.add(CourseTerm.CO_CURRICULAR.toString());
//
//
//				} else {
//					includedfields.add(req.getSearchTerms().toString());
//				}
//			}
//
//			if (!ArrayUtils.isEmpty(req.getExcludeCourseIds())) {
//				excludedfields.addAll(req.getExcludeCourseIds());
//			}
//			if (!ArrayUtils.isEmpty(req.getExcludeBatchIds())) {
//				aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_ENTITYID).nin(req.getExcludeBatchIds())));
//			}
//
//			if (!ArrayUtils.isEmpty(req.getIncludeCourseIds())) {
//				includedfields.addAll(req.getIncludeCourseIds());
//			}
//			if (!ArrayUtils.isEmpty(req.getBatchIds())) {
//				aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_ENTITYID).in(req.getBatchIds())));
//			}
//		}
//
//		if (ArrayUtils.isEmpty(excludedfields)) {
//			aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_MAINTAGS).all(includedfields)));
//		} else {
//			aggregationOperation.add(Aggregation.match(Criteria.where(Bundle.Constants.PACKAGES_MAINTAGS).all(includedfields).andOperator(Criteria.where(Bundle.Constants.PACKAGES_MAINTAGS).nin(excludedfields))));
//		}
//		if (req.getStart() != null && req.getSize() != null) {
//			aggregationOperation.add(Aggregation.skip(req.getStart()));
//			aggregationOperation.add(Aggregation.limit(req.getSize()));
//		} else {
//			aggregationOperation.add(Aggregation.skip(0));
//			aggregationOperation.add(Aggregation.limit(100));
//		}
//		AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
//		Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);
//
//
//		logger.info("\n--------------------------aggregation query---------------------------------\n" + aggregation + "\n");
//		AggregationResults<BundleBatchFilterRes> results = getMongoOperations().aggregate(aggregation,
//				Bundle.class, BundleBatchFilterRes.class);
//		List<BundleBatchFilterRes> bundles = results.getMappedResults();
//		logger.info( "result bundles"+ bundles.size() );
//		return bundles;
//	}

	public List<Bundle> getBundleByTags(List<String> existingBundleIds, List<Integer> grades, List<String> targets, Set<String> subjects, List<String> includeFields,Boolean includeAssist ,  Integer start, Integer size) {
		Query query = new Query();
		if(ArrayUtils.isNotEmpty(existingBundleIds)) {
			query.addCriteria(Criteria.where(Bundle.Constants._ID).nin(existingBundleIds));
		}
		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(true));
		query.addCriteria(Criteria.where(Bundle.Constants.GRADE).in(grades));
		query.addCriteria(Criteria.where(Bundle.Constants.TARGET).in(targets));
		query.addCriteria(Criteria.where(Bundle.Constants.SUBJECT).in(subjects));
		if(!includeAssist) {
			query.addCriteria(Criteria.where(Bundle.Constants.IS_VASSIST).ne(true));
		}

		start = (start != null) ? start : 0;
		size = (size != null) ? size : 20;
		if(MAX_ALLOWED_FETCH_SIZE < size) {
			logger.error( "getBundleByTags crossed MAX_ALLOWED_FETCH_SIZE "+ size );
		}
		query.skip(start);
		query.limit(size);
		logger.info("getBundleByTags - query - {}",query);
		return runQuery(query, Bundle.class, includeFields);
	}

	public List<Bundle> getBundlesByPackageId(String packageId, List<String> includeFields, Integer start, Integer size) {
		Query query = new Query();
		query.addCriteria(Criteria.where(Bundle.Constants.PACKAGES_ENTITYID).is(packageId));
		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(true));
		start = (start != null) ? start : 0;
		size = (size != null) ? size : 20;
		if(MAX_ALLOWED_FETCH_SIZE < size) {
			logger.error( "getBundlesByPackageId crossed MAX_ALLOWED_FETCH_SIZE "+ size );
		}
		query.skip(start);
		query.limit(size);
		return runQuery(query, Bundle.class, includeFields);
	}

	public List<Bundle> getEarlyLearningBundlesByPackageId(String bundleId, List<String> includeFields, Integer start, Integer size) {
		Query query = new Query();
		query.addCriteria(Criteria.where(Bundle.Constants._ID).is(bundleId));
		query.addCriteria(Criteria.where(Bundle.Constants.EARLY_LEARNING).is(Boolean.TRUE));
		start = (start != null) ? start : 0;
		size = (size != null) ? size : 20;
		if(MAX_ALLOWED_FETCH_SIZE < size) {
			logger.error( "getBundlesByPackageId crossed MAX_ALLOWED_FETCH_SIZE "+ size );
		}
		query.skip(start);
		query.limit(size);
		return runQuery(query, Bundle.class, includeFields);
	}

	public void updateMultiple(Query query,Update update){

		updateMulti(query,update, Bundle.class);
	}

	public List<HomeFeedBundleResponse> getBundlesForMobileHomeFeed(GetBundlesRequest getBundlesRequest) throws VException
	{
		List<HomeFeedBundleResponse> homeFeedBundleResponses = new ArrayList<>();
		if (getBundlesRequest.getBundleIds() != null && !getBundlesRequest.getBundleIds().isEmpty())
		{
			logger.info("Getting Bundles for Mobile homefeed for bundleIds : {}", getBundlesRequest.getBundleIds());
//			Long currentTime = System.currentTimeMillis();
			Query query = new Query();
			query.addCriteria(Criteria.where(Bundle.Constants._ID).in(getBundlesRequest.getBundleIds()));
			query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_IMAGE).exists(true).ne(null));
			query.addCriteria(Criteria.where(Bundle.Constants.TITLE).exists(true).ne(null));
			query.addCriteria(Criteria.where(Bundle.Constants.SUBJECT).exists(true).ne(null));
			query.addCriteria(Criteria.where(Bundle.Constants.PRICE).exists(true).ne(null));
			query.addCriteria(Criteria.where(Bundle.Constants.CUT_PRICE).exists(true).ne(null));
			query.addCriteria(Criteria.where(Bundle.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
//			query.addCriteria(Criteria.where(Bundle.Constants.START_TIME).gte(currentTime));
			logger.info("Query for Bundle Request : {}", query);
			getProjectionQuery(query);
			logger.info("Projection query : {}", query);
			List<Bundle> bundles = runQuery(query, Bundle.class);
			logger.info("Bundles fetched : {}", bundles);
			homeFeedBundleResponses = bundleToBundleResponse(bundles);
		}
		return homeFeedBundleResponses;
	}

	private List<HomeFeedBundleResponse> bundleToBundleResponse(List<Bundle> bundles)
	{
		List<HomeFeedBundleResponse> homeFeedBundleResponses = new ArrayList<>();
		String bundleId = "";
		if (!bundles.isEmpty()) {
			try {
				for (Bundle bundle : bundles) {
					HomeFeedBundleResponse homeFeedBundleResponse = new HomeFeedBundleResponse();

					if (bundle.getId() == null)
						continue;
					else
						{
							homeFeedBundleResponse.setId(bundle.getId());
							bundleId = bundle.getId();
						}

					if (StringUtils.isEmpty(bundle.getTitle()))
						continue;
					else
						homeFeedBundleResponse.setTitle(bundle.getTitle());
					
					if(null == bundle.getSubject() || bundle.getSubject().isEmpty())
						continue;
					else
						homeFeedBundleResponse.setSubjects(bundle.getSubject());
					
					if (bundle.getBundleDetails() == null || StringUtils.isEmpty(bundle.getBundleDetails().getBundleImageUrl()))
						continue;
					else
						homeFeedBundleResponse.setBundleImageUrl(bundle.getBundleDetails().getBundleImageUrl());

					if (bundle.getPrice() == null)
						continue;
					else
						homeFeedBundleResponse.setPrice(bundle.getPrice());

					if (bundle.getCutPrice() == null)
						continue;
					else
						homeFeedBundleResponse.setCutPrice(bundle.getCutPrice());

					//Fetching batchDetails from BundleEntity now.

//					if (bundle.getPackages() == null)
//						continue;
//					else
//					{
//						homeFeedBundleResponse.setPackages(bundle.getPackages());
//						String teacherName = "";
//
//						if (bundle.getPackages().size() == 0 || bundle.getPackages().get(0).getBatchDetails() == null || bundle.getPackages().get(0).getBatchDetails().getTeacherInfo() == null)
//							continue;
//						else
//						{
//							teacherName = bundle.getPackages().get(0).getBatchDetails().getTeacherInfo().get("teacherName").toString();
//							if (StringUtils.isEmpty(teacherName))
//								continue;
//							else
//							{
//								homeFeedBundleResponse.setTeacherName(teacherName);
//							}
//						}
//					}

					homeFeedBundleResponses.add(homeFeedBundleResponse);
				}

				logger.info("HomeFeedBundle Response : {}", homeFeedBundleResponses);
			}
			catch (Exception ex) {
				logger.error("Error converting Bundle to BundleReposnse due to : {} for bundle  : {}", ex.getMessage(), bundleId);
			}
		}
		else {
			logger.info("Empty Bundle List");
		}

		return homeFeedBundleResponses;
	}

	public MicroCourseCollectionResponse getMicroCourseCollection(String displayTag, String grade)
	{
		logger.info("Getting Bundles for Mobile homefeed for displayTag : {}", displayTag);
		Query query = new Query();
		query.addCriteria(Criteria.where(Bundle.Constants.SEARCH_TERMS).is(CourseTerm.MICRO_COURSES));
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DISPLAY_TAG).is(displayTag));
		query.addCriteria(Criteria.where(Bundle.Constants.GRADE).is(Integer.parseInt(grade)));
		query.addCriteria(new Criteria().orOperator(
				Criteria.where(Bundle.Constants.VALID_TILL).gt(System.currentTimeMillis()),
				Criteria.where(Bundle.Constants.VALID_DAYS).gt(0)));
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DETAILS_DISPLAY_TAG_SIZE).gt(0));
		query.fields().include(AbstractMongoEntity.Constants._ID);
		logger.info("Bundle Query : {}", query);
		final List<Bundle> bundles = runQuery(query, Bundle.class);
		final Set<String> ids = bundles.stream().map(Bundle::getId).collect(Collectors.toSet());

		Query bundleEntityQuery = new Query();
		bundleEntityQuery.addCriteria(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(ids));
		bundleEntityQuery.addCriteria(Criteria.where(BundleEntity.Constants.PACKAGE_TYPE).ne(PackageType.OTM_BUNDLE));
		final List<String> distinctIds = super.findDistinct(bundleEntityQuery, BundleEntity.Constants.BUNDLE_ID, BundleEntity.class, String.class);
		long count = distinctIds.size();


		logger.info("{} Bundles fetched", count);
		MicroCourseCollectionResponse microCourseCollectionResponse = new MicroCourseCollectionResponse();
		microCourseCollectionResponse.setDisplayTag(displayTag);
		microCourseCollectionResponse.setSize(count);
		return microCourseCollectionResponse;
	}

	private void getProjectionQuery(Query query)
	{
		query.fields().include(Bundle.Constants.TITLE)
				.include(Bundle.Constants._ID)
				.include(Bundle.Constants.SUBJECT)
				.include(Bundle.Constants.PRICE)
				.include(Bundle.Constants.CUT_PRICE)
				.include(Bundle.Constants.PACKAGES)
				.include(Bundle.Constants.BUNDLE_IMAGE);
	}


	public List<Bundle> getMicroCoursesWithFilter(getMicroCoursesWithFilter req, List<String> includeFields) {
		Query query = new Query();

		query.addCriteria(Criteria.where(Bundle.Constants.SEARCH_TERMS).is(CourseTerm.MICRO_COURSES));
		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).ne(true));
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DETAILS).exists(Boolean.TRUE).ne(null));
		query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_STATE).is(BundleState.ACTIVE));
		query.addCriteria(Criteria.where(Bundle.Constants.VALID_TILL).gt(System.currentTimeMillis()));
		if(req.getGrade() != null) {
			query.addCriteria(Criteria.where(Bundle.Constants.GRADE).is(req.getGrade()));
		}

		if(StringUtils.isNotEmpty(req.getQuery()) && req.getSearchWithOnlyQuery() !=null &&  req.getSearchWithOnlyQuery() ){
			int length = req.getQuery().length();
			int index = length < 40 ? length : 40;
			query.addCriteria(Criteria.where(Bundle.Constants.TITLE).regex(req.getQuery().substring(0, index), "i"));
		}else {
			if(StringUtils.isNotEmpty(req.getQuery())){
				int length = req.getQuery().length();
				int index = length < 40 ? length : 40;
				query.addCriteria(Criteria.where(Bundle.Constants.TITLE).regex(req.getQuery().substring(0, index), "i"));
			}
			if (!ArrayUtils.isEmpty(req.getBundleIds())) {
				query.addCriteria(Criteria.where(AbstractMongoEntity.Constants._ID).in(req.getBundleIds()));
			}

			if (!ArrayUtils.isEmpty(req.getSubject())) {
				query.addCriteria(Criteria.where(Bundle.Constants.SUBJECT).in(req.getSubject()));
			}

			if (!ArrayUtils.isEmpty(req.getTarget())) {
				query.addCriteria(Criteria.where(Bundle.Constants.TARGET).in(req.getTarget()));
			}
		}
        if (req.getStart() != null )  {

            query.skip(req.getStart());
        }
        if (req.getSize() != null )  {

            query.limit(req.getSize() * 2 );
        }

		if (req.getSortAsc() != null )  {

			query.with(Sort.by(Boolean.TRUE.equals(req.getSortAsc()) ?  Sort.Direction.ASC :   Sort.Direction.DESC, Bundle.Constants.PRICE));
		}


		if (req.getCourseType() != null) {
			query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DETAILS_COURSE_TYPE).is(req.getCourseType()));
		}

		if (req.getFromTime() != null && req.getToTime() != null) {
			query.addCriteria(Criteria.where(Bundle.Constants.BUNDLE_DETAILS_COURSE_START_TIME).gt(req.getFromTime()).lt(req.getToTime()));
		}


		logger.info("micro cources query  {}", query);
		// Criteria.where(Bundle.Constants._ID).in("1")
		return runQuery(query, Bundle.class,includeFields);
	}

	public void createBundleMinMaxException(BundleMinMaxExceptionList p, Long callingUserId) {
		if (p != null) {
			String callingUserIdString = null;
			if (callingUserId != null) {
				callingUserIdString = callingUserId.toString();
			}
			logger.info("Saving into db" + p);
			saveEntity(p, callingUserIdString);
		}
	}

	public BundleMinMaxExceptionList getBundleMinMaxExceptionByUserId(Long userId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(BundleMinMaxExceptionList.Constants.USERID).is(userId));
		query.addCriteria(Criteria.where(BundleMinMaxExceptionList.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
		List<BundleMinMaxExceptionList> bundleMinMaxExceptions = runQuery(query, BundleMinMaxExceptionList.class);
		if(ArrayUtils.isNotEmpty(bundleMinMaxExceptions)){
			return bundleMinMaxExceptions.get(0);
		}
		return null;
	}

	public long getLongTermBundleCount(List<String> bundleIds) {
		Query query = new Query();
		query.addCriteria(Criteria.where(Bundle.Constants.ID).in(bundleIds));
		query.addCriteria(Criteria.where(Bundle.Constants.SEARCH_TERMS).ne(CourseTerm.MICRO_COURSES));
		logger.info("query " + query);
		long count = queryCount(query, Bundle.class);
		logger.info("count " + count);

		return count;
	}

	public Set<Integer> getGradesOfAllTheBundleIds(List<String> bundleIds){
		Query query=new Query();
		query.addCriteria(Criteria.where(Bundle.Constants._ID).in(bundleIds));

		query.fields().exclude(Bundle.Constants._ID);

		query.fields().include(Bundle.Constants.GRADE);

		List<Bundle> bundles = runQuery(query, Bundle.class);

		return Optional.ofNullable(bundles)
						.map(Collection::stream)
						.orElseGet(Stream::empty)
						.map(Bundle::getGrade)
						.filter(ArrayUtils::isNotEmpty)
						.map(grade->grade.get(0))
						.collect(Collectors.toSet());

	}

	public void getProjectionQuery(Query query, List<BundleContentType> returnTypes) {
		if (returnTypes == null) {
			returnTypes = new ArrayList<>();
		}

		if (!returnTypes.contains( BundleContentType.WEBINAR )) {
			query.fields().exclude( Bundle.Constants.WEBINAR_CATEGORIES );
		}
		if (!returnTypes.contains( BundleContentType.VIDEO )) {
			query.fields().exclude( Bundle.Constants.VIDEOS );
		}
		if (!returnTypes.contains( BundleContentType.TEST )) {
			query.fields().exclude( Bundle.Constants.TESTS );
		}
		if (!returnTypes.contains( BundleContentType.PACKAGES )) {
			query.fields().exclude( Bundle.Constants.PACKAGES );
		}
	}

	public List<Bundle> getBundlesForMigration(long skip, int limit) {
		Query query=new Query();
		query.skip(skip);
		query.limit(limit);
		query.addCriteria(Criteria.where("packageReferenceIds").is(null));
		return runQuery(query,Bundle.class);
	}

	public void updatePackageReferenceDetailsForBundle(String bundleId, Set<String> packageIds) {
		logger.info("updatePackageReferenceDetailsForBundle - bundleId - {} - packageIds - {}",bundleId,packageIds);
		Query query=new Query();
		query.addCriteria(Criteria.where(Bundle.Constants._ID).is(bundleId));
		query.addCriteria(Criteria.where("packageReferenceIds").is(null));

		Update update=new Update();
		update.set("packageReferenceIds",packageIds);

		updateFirst(query,update,Bundle.class);
	}

//	public long isBatchPartOfSubscriptionAIO(List <String> bundleIds,String batchId){
//		Query query = new Query();
//		query.addCriteria(Criteria.where(Bundle.Constants._ID).in(bundleIds));
//		query.addCriteria(Criteria.where(Bundle.Constants.PACKAGES_ENTITYID).is(batchId));
//		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(true));
//		long bundleCount = queryCount(query,Bundle.class);
//		return bundleCount;
//	}

	public List<Bundle> getLimitedDataForEnrolledNormalBundles(List<String> bundleIds, int skip, int limit){
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(false));
		query.fields().slice(Bundle.Constants.PACKAGES,0,20);
		query.skip(skip);
		query.limit(limit);
		return runQuery(query, Bundle.class);
	}

	public List<Bundle> getLimitedDataForSubscriptionBundles(List<String> bundleIds, int skip, int limit){
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		query.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(true));
		query.fields().slice(Bundle.Constants.PACKAGES,0,20);
		query.skip(skip);
		query.limit(limit);
		return runQuery(query, Bundle.class);
	}

	public List<Bundle> getLimitedDataForMicroCourseBundles(List<String> bundleIds, int skip, int limit){
		Query query = new Query(Criteria.where(Bundle.Constants._ID).in(bundleIds));
		query.addCriteria(Criteria.where(Bundle.Constants.SEARCH_TERMS).in(CourseTerm.MICRO_COURSES));
		query.fields().slice(Bundle.Constants.PACKAGES,0,20);
		query.skip(skip);
		query.limit(limit);
		return runQuery(query, Bundle.class);
	}

	public List<BundleEntity> getLongTermCoursesBySubscription(List<String> bundleIds) {

		List<AggregationOperation> aggregationOperation = new ArrayList<>();
		List<String> includedfields = new ArrayList<>();
		List<String> excludedfields = new ArrayList<>();
		aggregationOperation.add(Aggregation.match(Criteria.where(BundleEntity.Constants.BUNDLE_ID).in(bundleIds)));
		aggregationOperation.add(Aggregation.match(Criteria.where(BundleEntity.Constants.MAIN_TAGS).is(CourseTerm.LONG_TERM)));
		aggregationOperation.add(Aggregation.group(BundleEntity.Constants.COURSE_ID));

		aggregationOperation.add(Aggregation.skip(0));
		aggregationOperation.add(Aggregation.limit(10));

		AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
		Aggregation aggregation = Aggregation.newAggregation(aggregationOperation).withOptions(aggregationOptions);


		logger.info("\n--------------------------aggregation query---------------------------------\n" + aggregation + "\n");
		AggregationResults<BundleEntity> results = getMongoOperations().aggregate(aggregation,
				BundleEntity.class, BundleEntity.class);
		List<BundleEntity> bundles = results.getMappedResults();
		return bundles;


	}
    public List<Bundle> getBundlesByBatchIds(List<String> bundleIds, String batchId, List<String> includeFields,
                                             Integer start, Integer limit) {
        start = (start == null) ? 0 : start;
        limit = (limit == null) ? 20 : limit;
        Query query = new Query();
        query.addCriteria( Criteria.where( Bundle.Constants._ID ).in( bundleIds ) );
        query.addCriteria( Criteria.where( Bundle.Constants.PACKAGES_ENTITYID ).is( batchId ) );
        query.addCriteria( Criteria.where( Bundle.Constants.IS_SUBSCRIPTION ).is( true ) );
        query.skip( start );
        query.limit( limit );
        logger.info( "query " + query );
        return runQuery( query, Bundle.class, includeFields );
    }

	public Bundle isValidBundle(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where(Bundle.Constants._ID).is(id));
		query.fields().include(Bundle.Constants.ID);
		logger.info("isValidBundle id ->"+id+" query-> "+query);
		return findOne(query, Bundle.class);
	}

}
