package com.vedantu.subscription.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.InternalServerErrorException;
import com.vedantu.subscription.entities.sql.CoursePlanHours;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractSqlDAO;

/**
 * Created by somil on 28/03/17.
 */
@Service
public class CoursePlanHoursDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(CoursePlanHoursDAO.class);

    public CoursePlanHoursDAO() {
        super();
    }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void create(CoursePlanHours coursePlanHours, String callingUserId) {
        try {
            if (coursePlanHours != null) {
                saveEntity(coursePlanHours, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("Create coursePlanHours: " + ex.getMessage());
        }
    }

    public CoursePlanHours getByCoursePlanId(String coursePlanId) {
        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
        CoursePlanHours coursePlanHours = null;
        try {
            coursePlanHours = getByCoursePlanId(coursePlanId, session);
        } catch (Exception ex) {
            logger.error("getById Subscription: " + ex.getMessage());
            coursePlanHours = null;
        } finally {
            session.close();
        }
        return coursePlanHours;
    }

    public List<CoursePlanHours> getActiveCoursePlanHours(Set<String> coursePlanIds) {
    	// TODO: Need to consider start time and end time once it grows
		List<CoursePlanHours> results = new ArrayList<>();
    	if (CollectionUtils.isEmpty(coursePlanIds)) {
    		return results;
    	}

        SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
        Session session = sessionFactory.openSession();
		try {
			Criteria cr = session.createCriteria(CoursePlanHours.class);
			cr.add(Restrictions.in("coursePlanId", coursePlanIds));
			logger.info("cr --> "+cr);
			results = runQuery(session, cr, CoursePlanHours.class);
		} catch (Exception ex) {
			logger.error("getById SessionStateDetails : " + ex.getMessage());
		} finally {
            session.close();
        }
		logger.info("coursePlanIds --> "+coursePlanIds);
        logger.info("results --> "+results);
		return results;
    }

    public CoursePlanHours getByCoursePlanId(String coursePlanId, Session session) {
        CoursePlanHours coursePlanHours = null;
        if (coursePlanId != null) {
            Criteria cr = session.createCriteria(CoursePlanHours.class);
            cr.add(Restrictions.eq("coursePlanId", coursePlanId));
            List<CoursePlanHours> results = runQuery(session, cr, CoursePlanHours.class);
            if (!CollectionUtils.isEmpty(results)) {
                if (results.size() > 1) {
                    logger.error("Duplicate entries found for id : " + coursePlanId);
                }
                coursePlanHours = results.get(0);
            } else {
                logger.info("getById CoursePlanHours : no coursePlanHours found" + coursePlanId);
            }
        }
        return coursePlanHours;
    }

    public List<CoursePlanHours> getByCoursePlanIds(List<String> coursePlanIds) throws InternalServerErrorException {
        List<CoursePlanHours> coursePlanHours = null;
        if (ArrayUtils.isNotEmpty(coursePlanIds)) {
            SessionFactory sessionFactory = sqlSessionFactory.getSessionFactory();
            Session session = sessionFactory.openSession();
            try {
                Criteria cr = session.createCriteria(CoursePlanHours.class);
                cr.add(Restrictions.in("coursePlanId", coursePlanIds));
                coursePlanHours = runQuery(session, cr, CoursePlanHours.class);
            } catch (Exception e) {
                logger.error(e);
                throw new InternalServerErrorException(ErrorCode.SERVICE_ERROR, e.getMessage());
            } finally {
                session.close();
            }
        }
        return coursePlanHours;
    }
    
    public void update(CoursePlanHours p, Session session, String callingUserId) {
        if (p != null) {
            logger.info("update subscription: " + p.toString());
            updateEntity(p, session, callingUserId);
        }
    }

    public void updateAll(List<CoursePlanHours> p, Session session) {
        updateAll(p, session, null);
    }

    public void updateAll(List<CoursePlanHours> p, Session session, String callingUserId) {
        try {
            if (p != null) {
                updateAllEntities(p, session, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("updateAll CoursePlanHours: " + ex.getMessage());
        }
    }

}
