package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.BatchSnapshotOTM;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


@Service
public class BatchSnapshotOTMDAO extends AbstractMongoDAO{

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logfactory;

    @SuppressWarnings("static-access")
    private Logger logger = logfactory.getLogger(BatchSnapshotOTMDAO.class);

    public BatchSnapshotOTMDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void create(BatchSnapshotOTM p, Long callingUserId){
        String callingUserIdString = null;
        if(callingUserId!=null){
            callingUserIdString = callingUserId.toString();
        }
        if(p!=null){
            saveEntity(p, callingUserIdString);
        }
    }

    public void insertAll(List<BatchSnapshotOTM> p, Long callingUserId) {
        if(p!=null) {
            String callingUserIdString = null;
            if(callingUserId != null){
                callingUserIdString = callingUserId.toString();
            }
            insertAllEntities(p, BatchSnapshotOTM.class.getSimpleName(), callingUserIdString);
        }
    }

    public BatchSnapshotOTM getById(String id){
        BatchSnapshotOTM batchSnapshotOTM = null;

        if(!StringUtils.isEmpty(id)){
            batchSnapshotOTM = getEntityById(id, BatchSnapshotOTM.class);
        }
        return batchSnapshotOTM;
    }

    public void save(BatchSnapshotOTM p){
        if(p != null){
            saveEntity(p);
        }
    }

    public List<BatchSnapshotOTM> getBatchSnapshotsForBatchId(String batchId, Long monthStartTime){
        if(!StringUtils.isEmpty(batchId)){
            Query query = new Query();
            query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.BATCH_ID).is(batchId));
            query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.MONTH_START_TIME).is(monthStartTime));
            return runQuery(query, BatchSnapshotOTM.class);
        }
        return new ArrayList<>();
    }
    
    public BatchSnapshotOTM getBatchSnapshotOTM(String batchId, Long monthStartTime){
        Query query = new Query();
        query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.BATCH_ID).is(batchId));
        query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.MONTH_START_TIME).is(monthStartTime));
        return findOne(query, BatchSnapshotOTM.class);
        
    }
    
    public List<BatchSnapshotOTM> getPastBatchSnapshotsOTM(List<String> batchIds, Long currentTime, String sessionId){
        if(ArrayUtils.isNotEmpty(batchIds)){
            Query query = new Query();
            query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.BATCH_ID).in(batchIds));
            query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.MONTH_START_TIME).is(currentTime));
            query.addCriteria(Criteria.where("sessions.sessionId").is(sessionId));
            return runQuery(query, BatchSnapshotOTM.class);
        }
        return new ArrayList<>();
    }    
    
    public List<BatchSnapshotOTM> getPastBatchSnapshotsForBatch(String batchId, Long currentTime){
        if(!StringUtils.isEmpty(batchId)){
            Query query = new Query();
            query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.BATCH_ID).is(batchId));
            query.addCriteria(Criteria.where("sessions.startTime").lt(currentTime));
            return runQuery(query, BatchSnapshotOTM.class);
        }
        return new ArrayList<>();
    }
    
    public int getBatchSnapshotsCountForBatchId(String batchId, Long monthStartTime){
        if(!StringUtils.isEmpty(batchId)){
            Query query = new Query();
            query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.BATCH_ID).is(batchId));
            query.addCriteria(Criteria.where(BatchSnapshotOTM.Constants.MONTH_START_TIME).is(monthStartTime));
            return (int)queryCount(query, BatchSnapshotOTM.class);
        }
        return 0;
    }    

    public List<BatchSnapshotOTM> getBatchSnapshotWithSessionInTime(Long startTime, Long endTime){
        if(startTime!=null && endTime != null){
            Query query = new Query();
            query.addCriteria(Criteria.where("sessions.endTime").gte(startTime).lt(endTime));
            //query.addCriteria(Criteria.where("sessions.endTime").lte(endTime));
            return runQuery(query, BatchSnapshotOTM.class);
        }
        return new ArrayList<>();
        
    }
    
}
