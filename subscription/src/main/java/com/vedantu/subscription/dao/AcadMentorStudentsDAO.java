package com.vedantu.subscription.dao;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.exception.DuplicateEntryException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.subscription.entities.mongo.AcadMentorStudents;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class AcadMentorStudentsDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(AcadMentorStudentsDAO.class);

    public AcadMentorStudentsDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void assignAcadMentorStudents(final AcadMentorStudents p) {
        try {
            if (p != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(AcadMentorStudents.Constants.STUDENTID).is(p.getStudentId()));
                Update u = new Update();
                u.set(AcadMentorStudents.Constants.ACADMENTORID, p.getAcadMentorId());
                u.set(AcadMentorStudents.Constants.STUDENTID, p.getStudentId());
                upsertEntity(q, u, AcadMentorStudents.class);
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Adding new AcadMentorStudents save failed for: " + p);
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "A AcadMentorStudents failed.");
        }
    }

    public void insertAcadMentorStudentsList(final List<AcadMentorStudents> acadMentorStudentsList) throws DuplicateEntryException {
        try {
            insertAllEntities(acadMentorStudentsList, "AcadMentorStudents");
        } catch (Exception e) {
            throw new DuplicateEntryException(ErrorCode.DUPLICATE_ENTRY, "Student already has academic mentor assigned");
        }
    }

    public void upsertAcadMentorStudent(final AcadMentorStudents p, final Long requesterId) {
        try {
            if (p != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(AcadMentorStudents.Constants.STUDENTID).is(p.getStudentId()));
                List<AcadMentorStudents> queryRes = runQuery(q, AcadMentorStudents.class);
                if (ArrayUtils.isEmpty(queryRes)) {
                    saveEntity(p);
                } else {
                    Update u = new Update();
                    u.set(AcadMentorStudents.Constants.ACADMENTORID, p.getAcadMentorId());
                    u.set(AcadMentorStudents.Constants.LAST_UPDATED, System.currentTimeMillis());
                    u.set(AcadMentorStudents.Constants.LAST_UPDATED_BY, requesterId);
                    updateFirst(q, u, AcadMentorStudents.class);
                }
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Adding new AcadMentorStudents save failed for: " + p);
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "A AcadMentorStudents failed.");
        }
    }

    public int removeStudents(final AcadMentorStudents p) throws NotFoundException {
        try {
            if (p != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(AcadMentorStudents.Constants.STUDENTID).is(p.getStudentId()));
                q.addCriteria(Criteria.where(AcadMentorStudents.Constants.ACADMENTORID).is(p.getAcadMentorId()));
                return deleteEntities(q, AcadMentorStudents.class);
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Removing AcadMentorStudents failed for: " + p);
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Removing AcadMentorStudents failed.");
        }
        return 0;
    }

    public List<Long> getStudentsByAcadMentor(final Long acadMentorId) {
        return getStudentsByAcadMentor(acadMentorId, 0, 100);
    }

    public List<Long> getStudentsByAcadMentor(final Long acadMentorId, Integer skip, Integer limit) {
        try {
            if (acadMentorId != null) {
                if (limit == null || limit == 0) {
                    limit = 100;
                }
                Query q = new Query();
                q.addCriteria(Criteria.where(AcadMentorStudents.Constants.ACADMENTORID).is(acadMentorId));
                setFetchParameters(q, skip, limit);
                List<AcadMentorStudents> acadMentorStudentsList = runQuery(q, AcadMentorStudents.class);
                List<Long> studentIds = new ArrayList<>();
                for (AcadMentorStudents acadMentorStudents : acadMentorStudentsList) {
                    studentIds.add(acadMentorStudents.getStudentId());
                }
                return studentIds;
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Getting AcadMentorStudents failed.");
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Removing AcadMentorStudents failed.");
        }
        return null;
    }

    public List<Long> getAcadMentors(Integer skip, Integer limit) {
        try {

            if(limit==null || limit ==0)
                limit=100;
            Query q = new Query();
            q.limit(limit);
            q.skip(skip);
            q.fields().include(AcadMentorStudents.Constants.ACADMENTORID);
            List<AcadMentorStudents> acadMentorStudentsList = runQuery(q, AcadMentorStudents.class);
            List<Long> acadMentors = new ArrayList<>();
            for (AcadMentorStudents acadMentorStudents : acadMentorStudentsList) {
                acadMentors.add(acadMentorStudents.getAcadMentorId());
            }
            return acadMentors;
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Getting AcadMentorStudents failed.");
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Removing AcadMentorStudents failed.");
        }
        return null;
    }

    public List<AcadMentorStudents> getAcadMentors(List<Long> studentIds) {
        try {

            Query q = new Query();
            q.addCriteria(Criteria.where(AcadMentorStudents.Constants.STUDENTID).in(studentIds));
            List<AcadMentorStudents> acadMentorStudentsList = runQuery(q, AcadMentorStudents.class);
            return acadMentorStudentsList;
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Getting AcadMentorStudents failed.");
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Removing AcadMentorStudents failed.");
        }
        return null;
    }
}
