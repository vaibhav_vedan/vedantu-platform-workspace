/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.dao;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.entities.mongo.Registration;
import com.vedantu.subscription.enums.RegistrationStatus;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

/**
 *
 * @author ajith
 */
@Service
public class RegistrationDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public RegistrationDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public Registration getById(String id) {
        return getEntityById(id, Registration.class);
    }

    public void save(Registration p) {
        if (p != null) {
            saveEntity(p);
        }
    }

    public List<Registration> getRegistrations(EntityType type, String entityId, RegistrationStatus status,
            Long userId) {
        Query query = new Query();
        if (type != null) {
            query.addCriteria(Criteria.where("entityType").is(type));
        }
        if (!StringUtils.isEmpty(entityId)) {
            query.addCriteria(Criteria.where("entityId").is(entityId));
        }
        if (status != null) {
            query.addCriteria(Criteria.where("status").is(status));
        }
        if (userId != null) {
            query.addCriteria(Criteria.where("userId").is(userId));
        }
        return runQuery(query, Registration.class);
    }
    
    public Registration getRegistrationByOrderId(String orderId){
        Query query = new Query();
        query.addCriteria(Criteria.where("orderId").is(orderId));
        List<Registration> registrations = runQuery(query, Registration.class);
        if(ArrayUtils.isEmpty(registrations)){
            return null;
        }
        return registrations.get(0);
    }

    public List<Registration> getRegistrations(EntityType type, String entityId, RegistrationStatus status,
            Long userId, boolean advancedPaymentConsumed) {
        Query query = new Query();
        if (type != null) {
            query.addCriteria(Criteria.where("entityType").is(type));
        }
        if (!StringUtils.isEmpty(entityId)) {
            query.addCriteria(Criteria.where("entityId").is(entityId));
        }
        query.addCriteria(Criteria.where("advancePaymentConsumed").is(advancedPaymentConsumed));
        query.addCriteria(Criteria.where("advancePaymentOrderId").exists(true));
        if (status != null) {
            query.addCriteria(Criteria.where("status").is(status));
        }
        if (userId != null) {
            query.addCriteria(Criteria.where("userId").is(userId));
        }
        return runQuery(query, Registration.class);
    }

    public List<Registration> getRegistrations(String entityId,
            Long userId) {
        Query query = new Query();

        if (!StringUtils.isEmpty(entityId)) {
            query.addCriteria(Criteria.where("entityId").is(entityId));
        }
        query.addCriteria(Criteria.where("status").in(Arrays.asList(RegistrationStatus.REGISTERED, RegistrationStatus.ENROLLED)));

        if (userId != null) {
            query.addCriteria(Criteria.where("userId").is(userId));
        }
        return runQuery(query, Registration.class);
    }

    public List<Registration> getRegistrations(Set<String> entityIds,
            Long userId, List<RegistrationStatus> statuses) {
        Query query = new Query();

        if (ArrayUtils.isNotEmpty(entityIds)) {
            query.addCriteria(Criteria.where(Registration.Constants.ENTITY_ID).in(entityIds));
        }
        if (ArrayUtils.isNotEmpty(statuses)) {
            query.addCriteria(Criteria.where(Registration.Constants.STATUS).in(statuses));
        }
        if (userId != null) {
            query.addCriteria(Criteria.where(Registration.Constants.USER_ID).is(userId));
        }
        return runQuery(query, Registration.class);
    }
    
    public List<Registration> getRegistrationsMultipleStatus(EntityType type, String entityId, List<RegistrationStatus> statuses,
            Long userId) {
        Query query = new Query();
        if (type != null) {
            query.addCriteria(Criteria.where("entityType").is(type));
        }
        if (!StringUtils.isEmpty(entityId)) {
            query.addCriteria(Criteria.where("entityId").is(entityId));
        }
        if (ArrayUtils.isNotEmpty(statuses)) {
            query.addCriteria(Criteria.where("status").in(statuses));
        }
        if (userId != null) {
            query.addCriteria(Criteria.where("userId").is(userId));
        }
        return runQuery(query, Registration.class);
    }
}
