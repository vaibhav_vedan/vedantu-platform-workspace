package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.AIOGroup;
import com.vedantu.subscription.request.GetAIOGroupReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Dpadhya
 */
@Service
public class AIOGroupDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	private final Logger logger = logFactory.getLogger(AIOGroupDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public AIOGroupDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void save(AIOGroup p, Long callingUserId) {
		if (p != null) {
			String callingUserIdString = null;
			if (callingUserId != null) {
				callingUserIdString = callingUserId.toString();
			}
			logger.info("Saving into db" + p);
			saveEntity(p, callingUserIdString);
		}
	}
	public List<AIOGroup> getGroups(GetAIOGroupReq req ,Boolean getAllGroup) {
		Query query = new Query();
		if (req.getGrade() != null) {
			query.addCriteria(Criteria.where(AIOGroup.Constants.GRADE).is(req.getGrade()));
		}
		if (!ArrayUtils.isEmpty(req.getGroupName()) ) {
			query.addCriteria(Criteria.where(AIOGroup.Constants.GROUP_NAME).in(req.getGroupName()));
		}

		if (!ArrayUtils.isEmpty(req.getGroupIds()) ) {
			query.addCriteria(Criteria.where(AIOGroup.Constants.ID).in(req.getGroupIds()));
		}
		if(!getAllGroup) {
			query.addCriteria(Criteria.where(AIOGroup.Constants.PAGERANK).gt(0));
		}
		query.with(Sort.by(Sort.Direction.ASC, AIOGroup.Constants.PAGERANK));
		return runQuery(query, AIOGroup.class);
	}
	public List<AIOGroup> getGroups(GetAIOGroupReq req, List<String> includeFields) {
		Query query = new Query();

		if (req.getGrade() != null) {
			query.addCriteria(Criteria.where(AIOGroup.Constants.GRADE).is(req.getGrade()));
		}

		return runQuery(query, AIOGroup.class,includeFields);
	}

}
