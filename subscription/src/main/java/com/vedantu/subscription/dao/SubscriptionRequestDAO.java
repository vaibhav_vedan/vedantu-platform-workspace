package com.vedantu.subscription.dao;

import java.util.List;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.sql.SubscriptionRequest;
import com.vedantu.util.LogFactory;

@Service
public class SubscriptionRequestDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {
	
	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(SubscriptionRequestDAO.class);

	public SubscriptionRequestDAO() {
		super();
	}

	@Autowired
	SqlSessionFactory sqlSessionFactory;


	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}

	public SubscriptionRequest addOrUpdateSubscriptionRequest(SubscriptionRequest subscriptionRequest,
															  Session session) {
		return addOrUpdateSubscriptionRequest(subscriptionRequest, session, null);
	}


	public SubscriptionRequest addOrUpdateSubscriptionRequest(SubscriptionRequest subscriptionRequest,
															  Session session, Long callingUserId) {
		Long id = subscriptionRequest.getId();
		logger.info("id: " + id);
		String callingUserIdString = null;
		if(callingUserId!=null)
			callingUserIdString = callingUserId.toString();
		if (id == null) {
			logger.info("insert");
			create(subscriptionRequest, session, callingUserIdString);
			return subscriptionRequest;
		} else {
			if (subscriptionRequest != null) {
				logger.info("update");
				update(subscriptionRequest, session, callingUserIdString);
			}
		}
		return subscriptionRequest;
	}

	private void create(SubscriptionRequest subscriptionRequest, Session session, String callingUserId) {
		try {
			if (subscriptionRequest != null) {
				saveEntity(subscriptionRequest, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create SubscriptionVO: "+ex.getMessage());
		}
	}

	public SubscriptionRequest getById(Long id, Boolean enabled, Session session) {
		SubscriptionRequest subscriptionRequest = null;
		try {
			if (id != null) {
				subscriptionRequest = (SubscriptionRequest) getEntityById(id, enabled, SubscriptionRequest.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById SubscriptionVO: "+ ex.getMessage());
			subscriptionRequest = null;
		}
		return subscriptionRequest;
	}

	private void update(SubscriptionRequest p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateEntity(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Update SubscriptionVO: "+ex.getMessage());
		}
	}

	public void updateAll(List<SubscriptionRequest> p, Session session) {
		updateAll(p, session, null);
	}



	public void updateAll(List<SubscriptionRequest> p, Session session, Long callingUserId) {
		try {
			if (p != null) {
				String callingUserIdString = null;
				if(callingUserId!=null)
					callingUserIdString = callingUserId.toString();
				saveAllEntities(p, session, callingUserIdString);
			}
		} catch (Exception ex) {
			logger.error("updateAll SubscriptionVO: "+ex.getMessage());
		}
	}

}
