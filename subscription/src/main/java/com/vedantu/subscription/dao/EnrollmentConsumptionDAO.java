package com.vedantu.subscription.dao;

import java.util.List;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.subscription.entities.sql.EnrollmentConsumption;
import com.vedantu.util.dbutils.*;
import org.apache.logging.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.vedantu.util.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.Transaction;

@Service
public class EnrollmentConsumptionDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EnrollmentConsumptionDAO.class);

    public EnrollmentConsumptionDAO(){ super(); }

    @Autowired
    SqlSessionFactory sqlSessionFactory;

    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void create(EnrollmentConsumption enrollmentConsumption, Session session){
        create(enrollmentConsumption, session, null);
    }

    public void create(EnrollmentConsumption enrollmentConsumption, Session session, String callingUserId) {
        try {
            if(enrollmentConsumption != null) {
                saveEntity(enrollmentConsumption, session, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("Create Enrollment Consumption: "+ex.getMessage());
        }
    }

    public void insertAll(List<EnrollmentConsumption> enrollmentConsumptions, String callingUserId){
        try{
            if(enrollmentConsumptions != null){
                saveAllEntities(enrollmentConsumptions, callingUserId);
            }
        }catch (Exception ex){
            logger.error("insertAll Enrollment Consumption: " + ex.getMessage());
        }
    }
    
    public EnrollmentConsumption getById(Long id, Boolean enabled, Session session) {
        EnrollmentConsumption enrollmentConsumption = null;
        try {
            if(id != null) {
                enrollmentConsumption = (EnrollmentConsumption) getEntityById(id, enabled, EnrollmentConsumption.class, session);
            }
        }catch (Exception ex) {
            logger.error("getById EnrollmentConsumption: " + ex.getMessage());
            enrollmentConsumption = null;
        }
        return enrollmentConsumption;
    }

    public EnrollmentConsumption getByEnrollmentId(String id, Boolean enabled, Session session, LockMode lockMode) {
        EnrollmentConsumption enrollmentConsumption = null;
        try {
            if(id!=null){
                Criteria cr = session.createCriteria(EnrollmentConsumption.class);
                cr.add(Restrictions.eq("enrollmentId", id));
                if (enabled != null) {
			if (enabled.equals(true)) {
				cr.add(Restrictions.eq("enabled", true));
			} else {
				cr.add(Restrictions.eq("enabled", false));
			}
		}
                List<EnrollmentConsumption> result = runQuery(session, cr, EnrollmentConsumption.class, lockMode);
                if(result != null && !result.isEmpty()){
                    enrollmentConsumption = result.get(0);
                }
            }
        }catch (Exception ex){
            logger.error("getByEnrollmentId: "+ex.getMessage());
        }
        return enrollmentConsumption;
        
    }
    
    public void update(EnrollmentConsumption enrollmentConsumption, Session session) { update(enrollmentConsumption, session, null); }

    public void update(EnrollmentConsumption enrollmentConsumption, Session session, String callingUserId) {
        try {
            if(enrollmentConsumption != null){
                updateEntity(enrollmentConsumption, session, callingUserId);
            }
        } catch (Exception ex){
            logger.error("update EnrollmentConsumption: " + ex.getMessage());
        }
    }

    public void updateBatchId(String enrollmentId, String batchId){
        Session session = sqlSessionFactory.getSessionFactory().openSession();
        Transaction transaction = session.getTransaction();
        
        try{
            transaction.begin();
            String hqlQuery = "UPDATE EnrollmentConsumption a SET a.batchId = :batchId WHERE a.enrollmentId = :enrollmentId";
            int updatedEntites = session.createQuery(hqlQuery)
                                        .setString("batchId", batchId)
                                        .setString("enrollmentId", enrollmentId)
                                        .executeUpdate();
            transaction.commit();
            if(updatedEntites<1){
                logger.error("No entries found to update for enrollmentId: "+enrollmentId+" batchId: "+batchId);
            }
        }catch(Exception ex){
            session.getTransaction().rollback();
            logger.error("Error while updating enrollment consumption for : "+enrollmentId+" batchId : "+batchId+" error: "+ex.getMessage());
        }finally{
            session.close();
        }
        
        
    }
    
    public void updateAll(List<EnrollmentConsumption> enrollmentConsumptionList, Session session) { updateAll(enrollmentConsumptionList, session, null); }

    public void updateAll(List<EnrollmentConsumption> enrollmentConsumptionList, Session session, String callingUserId) {
        try {
            if(enrollmentConsumptionList != null) {
                updateAllEntities(enrollmentConsumptionList, session, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("updateAll EnrollmentConsumption: " + ex.getMessage());
        }
    }

    public List<EnrollmentConsumption> getByBatchId(List<String> batchIds, Session session){
        List<EnrollmentConsumption> enrollmentConsumptionList = null;
        if(session==null){
            session = sqlSessionFactory.getSessionFactory().openSession();
        }
        Transaction transaction = session.getTransaction();
        try{
            transaction.begin();
            if(batchIds!=null){
                Criteria cr = session.createCriteria(EnrollmentConsumption.class);
                cr.add(Restrictions.in("batchId", batchIds));
                logger.info("Query criteria: "+cr);
                enrollmentConsumptionList = runQuery(session, cr, EnrollmentConsumption.class);
            }
            transaction.commit();
        }catch (Exception ex){
            session.getTransaction().rollback();
            logger.error("getByBatchId EnrollmentConsumption: "+ex.getMessage());
        }finally{
            session.close();
        }
        
        return enrollmentConsumptionList;
    }
    
    public List<EnrollmentConsumption> getByBatchIdForContentOnly(List<String> batchIds, Long time, Session session){
        List<EnrollmentConsumption> enrollmentConsumptionList = null;
        if(session==null){
            session = sqlSessionFactory.getSessionFactory().openSession();
        }
        Transaction transaction = session.getTransaction();
        try{
            transaction.begin();
            if(batchIds!=null){
                Criteria cr = session.createCriteria(EnrollmentConsumption.class);
                cr.add(Restrictions.in("batchId", batchIds));
                cr.add(Restrictions.lt("creationTime", time));
                logger.info("Query criteria: "+cr);
                enrollmentConsumptionList = runQuery(session, cr, EnrollmentConsumption.class);
            }
            transaction.commit();
        }catch (Exception ex){
            session.getTransaction().rollback();
            logger.error("getByBatchId EnrollmentConsumption: "+ex.getMessage());
        }finally{
            session.close();
        }
        
        return enrollmentConsumptionList;
    }


}
