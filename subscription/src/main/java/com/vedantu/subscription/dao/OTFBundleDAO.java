package com.vedantu.subscription.dao;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.onetofew.request.GetOTFBundlesReq;
import com.vedantu.session.pojo.EntityType;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.entities.mongo.OTMBundle;
import com.vedantu.subscription.pojo.EntityInfo;
import com.vedantu.subscription.viewobject.request.GetCourseListingReq;
import com.vedantu.subscription.viewobject.response.AggregatedOTMBundles;
import com.vedantu.subscription.viewobject.response.AggregatedTargets;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.CollectionUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import java.util.ArrayList;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.bson.Document;

@Service
public class OTFBundleDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(OTFBundleDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public OTFBundleDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(OTMBundle p, Long callingUserId) {
        // try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }

    public OTMBundle getOTFBundleById(String id) {
        return getEntityById(id, OTMBundle.class);
    }

    public List<OTMBundle> getOTMBundlesByIds(List<String> bundleIds){
        return getOTMBundlesByIds(bundleIds,null);
    }

    public List<OTMBundle> getOTMBundlesByIds(List<String> bundleIds,List<String> includeFields){
        Query query = new Query();
        setFetchParameters(query, 0, 500);
        query.addCriteria(Criteria.where(OTMBundle.Constants.ID).in(bundleIds));

        return runQuery(query, OTMBundle.class,includeFields);
    }

    public List<OTMBundle> getOTFBundles(GetOTFBundlesReq req) {
        Query query = new Query();
        setFetchParameters(query, req);
        query.addCriteria(Criteria.where(OTMBundle.Constants.ENTITIES_ENTITY_TYPE).is(EntityType.OTF_COURSE));        
        if (!CollectionUtils.isEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(OTMBundle.Constants.ENTITIES)
                    .elemMatch(Criteria.where(EntityInfo.Constants.ENTITY_ID).in(req.getEntityId())));
        }
        if (!CollectionUtils.isEmpty(req.getTags())) {
            query.addCriteria(Criteria.where(OTMBundle.Constants.TAGS).in(req.getTags()));
        }
        if(!CollectionUtils.isEmpty(req.getOtfBundleIds())){
        	query.addCriteria(Criteria.where(OTMBundle.Constants.ID).in(req.getOtfBundleIds()));
        }
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        return runQuery(query, OTMBundle.class);
    }
    
    public List<OTMBundle> getOTFBundlesForBatch(GetOTFBundlesReq req) {
        Query query = new Query();
        setFetchParameters(query, req);
        query.addCriteria(Criteria.where(OTMBundle.Constants.ENTITIES_ENTITY_TYPE).is(EntityType.OTF));
        if (!CollectionUtils.isEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(OTMBundle.Constants.ENTITIES)
                    .elemMatch(Criteria.where(EntityInfo.Constants.ENTITY_ID).in(req.getEntityId())));
        }
        if (!CollectionUtils.isEmpty(req.getTags())) {
            query.addCriteria(Criteria.where(OTMBundle.Constants.TAGS).in(req.getTags()));
        }
        if(!CollectionUtils.isEmpty(req.getOtfBundleIds())){
        	query.addCriteria(Criteria.where(OTMBundle.Constants.ID).in(req.getOtfBundleIds()));
        }
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        return runQuery(query, OTMBundle.class);
    }
    
    public List<OTMBundle> getOTFBundlesForEntity(GetOTFBundlesReq req) {
        Query query = new Query();
        setFetchParameters(query, req);
        if (!CollectionUtils.isEmpty(req.getEntityId())) {
            query.addCriteria(Criteria.where(OTMBundle.Constants.ENTITIES)
                    .elemMatch(Criteria.where(EntityInfo.Constants.ENTITY_ID).in(req.getEntityId())));
        }

        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        return runQuery(query, OTMBundle.class);
    }
    
    public List<AggregatedOTMBundles> getOTMBundleForListing(GetCourseListingReq getCourseListingReq, List<Long> boardIds){
        
        List<Criteria> criterias = new ArrayList<>();
        
        criterias.add(Criteria.where("grade").is(Integer.parseInt(getCourseListingReq.getGrade())));
        
        if(StringUtils.isNotEmpty(getCourseListingReq.getTarget())){
            criterias.add(Criteria.where("target").is(getCourseListingReq.getTarget()));
        }
        
        if(ArrayUtils.isNotEmpty(boardIds)){
            criterias.add(Criteria.where("boardIds").in(boardIds));
        }
        
        if(getCourseListingReq.getCourseTerm()!=null){
            criterias.add(Criteria.where("searchTerms").is(getCourseListingReq.getCourseTerm()));            
        }
        
        criterias.add(Criteria.where("visibilityState").is(VisibilityState.VISIBLE));
        
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agr = Aggregation.newAggregation(
                                    Aggregation.match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))),
                                    Aggregation.group("groupName").push("$$ROOT").as("bundles").min("startTime").as("minStartTime").push("startTime").as("startTimes"),
                                    Aggregation.sort(Sort.Direction.ASC,"minStartTime"),
                                    Aggregation.match(Criteria.where("minStartTime").gte(getCourseListingReq.getLastStartTime())),
                                    Aggregation.limit(getCourseListingReq.getSize() + getCourseListingReq.getSkipCount())).withOptions(aggregationOptions);
        
        logger.info("aggregation: "+agr);
        //System.out.print("aggregation: "+agr);
        AggregationResults<AggregatedOTMBundles> results = getMongoOperations().aggregate(agr, OTMBundle.class, AggregatedOTMBundles.class);
        
        return results.getMappedResults();
        
    }

    public List<AggregatedOTMBundles> getPastOTMBundleForListing(GetCourseListingReq getCourseListingReq, List<Long> boardIds){
        
        List<Criteria> criterias = new ArrayList<>();
        
        Long currentTime = System.currentTimeMillis();
        int skipCount = 0;
        if(getCourseListingReq.getLastStartTime() < currentTime){
            currentTime = getCourseListingReq.getLastStartTime();
            skipCount = getCourseListingReq.getSkipCount();
        }
        
        criterias.add(Criteria.where("grade").is(Integer.parseInt(getCourseListingReq.getGrade())));
        
        if(StringUtils.isNotEmpty(getCourseListingReq.getTarget())){
            criterias.add(Criteria.where("target").is(getCourseListingReq.getTarget()));
        }
        
        if(ArrayUtils.isNotEmpty(boardIds)){
            criterias.add(Criteria.where("boardIds").in(boardIds));
        }
        
        if(getCourseListingReq.getCourseTerm()!=null){
            criterias.add(Criteria.where("searchTerms").is(getCourseListingReq.getCourseTerm()));            
        }
        
        criterias.add(Criteria.where("visibilityState").is(VisibilityState.VISIBLE));
        
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation agr = Aggregation.newAggregation(
                                    Aggregation.match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))),
                                    Aggregation.group("groupName").push("$$ROOT").as("bundles").min("startTime").as("minStartTime").push("startTime").as("startTimes"),
                                    Aggregation.sort(Sort.Direction.DESC,"minStartTime"),
                                    Aggregation.match(Criteria.where("minStartTime").lte(currentTime)),
                                    Aggregation.limit(getCourseListingReq.getSize() + skipCount)).withOptions(aggregationOptions);
        
        logger.info("aggregation: "+agr);
        //System.out.print("aggregation: "+agr);
        AggregationResults<AggregatedOTMBundles> results = getMongoOperations().aggregate(agr, OTMBundle.class, AggregatedOTMBundles.class);
        
        return results.getMappedResults();
        
    }
    
    
    public AggregatedTargets getAggregatedTargets(){
        AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(
                                            Aggregation.match(Criteria.where("visibilityState").is(VisibilityState.VISIBLE)),
                                            Aggregation.group("visibilityState").addToSet("target").as("aggTargets"),
                                            Aggregation.project("aggTargets").andExclude("_id")).withOptions(aggregationOptions);
        
        AggregationResults<AggregatedTargets> results = getMongoOperations().aggregate(aggregation, OTMBundle.class, AggregatedTargets.class);
        
        if(ArrayUtils.isEmpty(results.getMappedResults())){
            return null;
        }
        
        return results.getMappedResults().get(0);
    }
    
    public List<OTMBundle> getBundleByGroupName(String groupName){
        Query query = new Query();
        query.addCriteria(Criteria.where("groupName").is(groupName));
        return runQuery(query, OTMBundle.class);
    }
    
}