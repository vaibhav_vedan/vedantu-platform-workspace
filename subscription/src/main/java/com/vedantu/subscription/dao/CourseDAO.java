package com.vedantu.subscription.dao;

import com.vedantu.User.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.onetofew.request.GetCoursesReq;
import com.vedantu.session.pojo.VisibilityState;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.viewobject.request.GetCourseListingReq;
import com.vedantu.subscription.viewobject.response.AggregatedTargets;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

import java.lang.reflect.Array;
import java.util.*;

import org.apache.logging.log4j.Logger;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.bson.Document;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

@Service
public class CourseDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(CourseDAO.class);

    public CourseDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public Course getById(String id) {
        return getById(id,null,null);

    }
    public Course getById(String id,List<String> includeFields,List<String> excludeFields) {
        Course course = null;
        try {
            if (!StringUtils.isEmpty(id)) {
                Query query=new Query();
                query.addCriteria(Criteria.where(Course.Constants._ID).is(id));
                if(ArrayUtils.isNotEmpty(includeFields)){
                    includeFields.forEach(field->query.fields().include(field));
                }
                if(ArrayUtils.isNotEmpty(excludeFields)){
                    excludeFields.forEach(field->query.fields().exclude(field));
                }
                course = findOne(query,Course.class);
            }
        } catch (Exception ex) {
            // log Exception
            course = null;
        }
        return course;
    }

    public void save(Course p) {
        try {
            if (p != null) {
                if (ArrayUtils.isEmpty(p.getFeaturedTeachers()) && p.getId() != null) {
                    logger.info("course " + p.getId() + " has no featured teachers");
                }
                logger.info("Saving for couse with course price {}",p.getStartPrice());
                saveEntity(p);
            }
        } catch (Exception ex) {
            // throw Exception;
        }
    }

    public int deleteById(String id) {
        int result = 0;
        try {
            result = deleteEntityById(id, Course.class);
        } catch (Exception ex) {
            // throw Exception;
        }

        return result;
    }

    public List<String> getCoursesByGrades(List<String> grades) {
        Query query = new Query();
        if (ArrayUtils.isNotEmpty(grades)) {
            query.addCriteria(Criteria.where(Course.Constants.GRADES).in(grades));
        }
        List<Course> courses = runQuery(query, Course.class);
        List<String> courseIds = new ArrayList<>();
        for (Course c : courses) {
            courseIds.add(c.getId());
        }
        return courseIds;
    }

    public List<Course> getCoursesBasicInfos(List<String> ids) {
        return getCoursesBasicInfos(ids, null);
    }

    public List<Course> getCoursesBasicInfos(List<String> ids, List<String> includeFields) {

        if (ArrayUtils.isEmpty(ids)) {
            return new ArrayList<>();
        }

        Query query = new Query();
        query.addCriteria(Criteria.where(Course.Constants.ID).in(ids));
        if (ArrayUtils.isNotEmpty(includeFields)) {
            includeFields.forEach((fieldName) -> {
                query.fields().include(fieldName);
            });
        }
        logger.info("query " + query.toString());
        List<Course> courses = runQuery(query, Course.class);
        return courses;
    }

    public List<Course> getCourses(GetCoursesReq req) {

        Query query = getCoursesQuery(req);
        setFetchParameters(query, req);
        query.with(Sort.by(Sort.Direction.DESC, Course.Constants.FEATURED)
                .and(Sort.by(Sort.Direction.DESC, Course.Constants.PRIORITY))
                .and(Sort.by(Sort.Direction.ASC, Course.Constants.LAUNCH_DATE))
                .and(Sort.by(Sort.Direction.DESC, Course.Constants.LAST_UPDATED)));

        logger.info("query " + query.toString());

        List<Course> courses = runQuery(query, Course.class);
        return courses;
    }

    public Long getCoursesCount(GetCoursesReq req) {
        Query query = getCoursesQuery(req);
        return queryCount(query, Course.class);
    }

    private Query getCoursesQuery(GetCoursesReq req) {
        Query query = new Query();
        if (req.getTeacherIds() != null) {
            query.addCriteria(
                    Criteria.where(Course.Constants.TEACHER_IDS).in(new HashSet<>(req.getTeacherIds())));
        }
        if (req.getSubjects() != null) {
            query.addCriteria(Criteria.where(Course.Constants.SUBJECTS).in(new HashSet<>(req.getSubjects())));
        }
        if (req.getGrades() != null) {
            query.addCriteria(Criteria.where(Course.Constants.GRADES).in(new HashSet<>(req.getGrades())));
        }
        if (req.getTargets() != null) {
            query.addCriteria(Criteria.where(Course.Constants.TARGETS).in(new HashSet<>(req.getTargets())));
        }
        if (req.getTitle() != null) {
            int length = req.getTitle().length();
            int index = length < 40 ? length : 40;
            query.addCriteria(Criteria.where(Course.Constants.TITLE).regex(req.getTitle().substring(0, index), "i"));
        }
        if (req.getFeatured() != null) {
            query.addCriteria(Criteria.where(Course.Constants.FEATURED).is(req.getFeatured()));
        }
        if (req.getParentCourseId() != null) {
            if (0 == req.getParentCourseId().compareTo("none")) {
                query.addCriteria(Criteria.where(Course.Constants.PARENT_COURSE).is(null));
            } else {
                query.addCriteria(
                        new Criteria().orOperator(Criteria.where(Course.Constants.ID).is(req.getParentCourseId()),
                                Criteria.where(Course.Constants.PARENT_COURSE).is(req.getParentCourseId())));

            }
        }

        if (req.getCourseIds() != null && !req.getCourseIds().isEmpty()) {
            query.addCriteria(Criteria.where(Course.Constants.ID).in(new HashSet<>(req.getCourseIds())));
        }

        if (req.getStartPriceFrom() != null && req.getStartPriceTill() == null) {
            query.addCriteria(Criteria.where(Course.Constants.START_PRICE).gte(req.getStartPriceFrom()));
        }

        if (req.getStartPriceTill() != null && req.getStartPriceFrom() == null) {
            query.addCriteria(Criteria.where(Course.Constants.START_PRICE).lte(req.getStartPriceTill()));
        }

        if (req.getStartPriceFrom() != null && req.getStartPriceTill() != null) {
            query.addCriteria(new Criteria().andOperator(
                    Criteria.where(Course.Constants.START_PRICE).gte(req.getStartPriceFrom()),
                    Criteria.where(Course.Constants.START_PRICE).lte(req.getStartPriceTill())));
        }

        if (!Role.ADMIN.equals(req.getCallingUserRole())) {
            query.addCriteria(Criteria.where(Course.Constants.LAUNCH_DATE).lte(System.currentTimeMillis()));
            query.addCriteria(Criteria.where(Course.Constants.EXIT_DATE).gte(System.currentTimeMillis()));
            query.addCriteria(Criteria.where(Course.Constants.VISIBILITY_STATE).is(VisibilityState.VISIBLE));
            if (req.getSendNoTeachers() != null && req.getSendNoTeachers()) {
                query.addCriteria(Criteria.where(Course.Constants.TOTAL_BATCHES).gt(0));
            } else {
                query.addCriteria(Criteria.where(Course.Constants.UPCOMING_BATCHES).gt(0));
            }
        } else if (req.getVisibilityState() != null) {
            query.addCriteria(Criteria.where(Course.Constants.VISIBILITY_STATE).is(req.getVisibilityState()));
        }
        return query;
    }

    public List<Course> getCourseForListing(GetCourseListingReq getCourseListingReq, List<Long> boardIds) {

        Query query = new Query();
        query.addCriteria(Criteria.where("grades").is(getCourseListingReq.getGrade()));

        if (!StringUtils.isEmpty(getCourseListingReq.getTarget())) {
            query.addCriteria(Criteria.where("targets").is(getCourseListingReq.getTarget()));
        }

        if (ArrayUtils.isNotEmpty(boardIds)) {
            query.addCriteria(Criteria.where("boardTeacherPairs.boardId").in(boardIds));
        }

        if (getCourseListingReq.getCourseTerm() != null) {
            query.addCriteria(Criteria.where("searchTerms").is(getCourseListingReq.getCourseTerm()));
        }
        query.addCriteria(Criteria.where("exitDate").gte(System.currentTimeMillis()));
        query.addCriteria(Criteria.where("visibilityState").is(VisibilityState.VISIBLE));
        query.addCriteria(Criteria.where("batchStartTime").gte(getCourseListingReq.getLastStartTime()));
        query.with(Sort.by(Sort.Direction.ASC, "batchStartTime"));



        if(Objects.nonNull( getCourseListingReq.getSize() ) && Objects.nonNull( getCourseListingReq.getSkipCount() )) {
            query.limit(getCourseListingReq.getSize() + getCourseListingReq.getSkipCount());
        } else {
            query.limit(20);
        }

        List<Course> courses = runQuery(query, Course.class);
        return courses;

    }

    public List<Course> getPastCourseForListing(GetCourseListingReq getCourseListingReq, List<Long> boardIds) {

        Query query = new Query();
        query.addCriteria(Criteria.where("grades").is(getCourseListingReq.getGrade()));

        Long currentTime = System.currentTimeMillis();
        int skipCount = 0;
        if (getCourseListingReq.getLastStartTime() < currentTime) {
            currentTime = getCourseListingReq.getLastStartTime();
            skipCount = getCourseListingReq.getSkipCount();
        }

        if (!StringUtils.isEmpty(getCourseListingReq.getTarget())) {
            query.addCriteria(Criteria.where("targets").is(getCourseListingReq.getTarget()));
        }

        if (ArrayUtils.isNotEmpty(boardIds)) {
            query.addCriteria(Criteria.where("boardTeacherPairs.boardId").in(boardIds));
        }

        if (getCourseListingReq.getCourseTerm() != null) {
            query.addCriteria(Criteria.where("searchTerms").is(getCourseListingReq.getCourseTerm()));
        }
        query.addCriteria(Criteria.where("exitDate").gte(System.currentTimeMillis()));
        query.addCriteria(Criteria.where("visibilityState").is(VisibilityState.VISIBLE));
        query.addCriteria(Criteria.where("batchStartTime").lte(currentTime));
        query.with(Sort.by(Sort.Direction.DESC, "batchStartTime"));

        query.limit(getCourseListingReq.getSize() + skipCount);
        logger.info("Past Query: " + query);
        List<Course> courses = runQuery(query, Course.class);
        return courses;

    }

    public AggregatedTargets getAggregatedTargets() {

        List<Criteria> criterias = new ArrayList<>();

        criterias.add(Criteria.where("visibilityState").is(VisibilityState.VISIBLE));
        criterias.add(Criteria.where("exitDate").gte(System.currentTimeMillis()));

        AggregationOptions aggregationOptions = Aggregation.newAggregationOptions().cursor(new Document()).build();
        Aggregation aggregation = Aggregation.newAggregation(
                Aggregation.match(new Criteria().andOperator(criterias.toArray(new Criteria[criterias.size()]))),
                Aggregation.unwind("targets"),
                Aggregation.group("visibilityState").addToSet("targets").as("aggTargets"),
                Aggregation.project("aggTargets").andExclude("_id")).withOptions(aggregationOptions);

        logger.info(aggregation);
        AggregationResults<AggregatedTargets> results = getMongoOperations().aggregate(aggregation, Course.class, AggregatedTargets.class);

        logger.info(results);

        if (ArrayUtils.isEmpty(results.getMappedResults())) {
            return null;
        }

        return results.getMappedResults().get(0);
    }

    public List<Course> getFutureCourseForQuery(String board, String grade, int start, int size) {
        Long currentTime = System.currentTimeMillis();
        Query query = new Query();
        query.addCriteria(Criteria.where(Course.Constants.BATCH_START_TIME).gte(currentTime));
        query.addCriteria(Criteria.where(Course.Constants.VISIBILITY_STATE).is(VisibilityState.VISIBLE));
        query.addCriteria(Criteria.where(Course.Constants.GRADES).is(grade));
        if (!StringUtils.isEmpty(board)) {
            query.addCriteria(Criteria.where(Course.Constants.TARGETS).is(board.toLowerCase()));
        }
        setFetchParameters(query, start, size);
        return runQuery(query, Course.class);
    }

    public List<Course> getPastCourseForQuery(String board, String grade, int start, int size) {
        Long currentTime = System.currentTimeMillis();

        Query query = new Query();
        query.addCriteria(Criteria.where(Course.Constants.BATCH_START_TIME).lte(currentTime));
        query.addCriteria(Criteria.where(Course.Constants.EXIT_DATE).gte(currentTime));
        query.addCriteria(Criteria.where(Course.Constants.VISIBILITY_STATE).is(VisibilityState.VISIBLE));
        query.addCriteria(Criteria.where(Course.Constants.GRADES).is(grade));
        if (!StringUtils.isEmpty(board)) {
            query.addCriteria(Criteria.where(Course.Constants.TARGETS).is(board.toLowerCase()));
        }
        setFetchParameters(query, start, size);
        return runQuery(query, Course.class);
    }

    public List<Course> getCourseByIds(Set<String> courseIds, List<String> courseDataIncludeFields, List<String> courseDataExcludeFields){
        Query query = new Query();
        query.addCriteria(Criteria.where(Course.Constants.ID).in(courseIds));

        if(ArrayUtils.isNotEmpty(courseDataIncludeFields)){
            courseDataIncludeFields.forEach(field->query.fields().include(field));
        }

        if(ArrayUtils.isNotEmpty(courseDataExcludeFields)){
            courseDataExcludeFields.forEach(field->query.fields().exclude(field));
        }

        logger.info("getCourseByIds - "+query);
        return runQuery(query, Course.class);
    }

    public List<Course> getCoursesForSubscription(String grade, List<String> subjects, List<String> targets, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria( Criteria.where( Course.Constants.GRADES ).in( grade ) );
        query.addCriteria( Criteria.where( Course.Constants.NORMALIZE_SUBJECTS ).in( subjects ) );
        query.addCriteria( Criteria.where( Course.Constants.NORMALIZE_TARGETS ).in( targets ) );
        query.addCriteria( Criteria.where( Course.Constants.VISIBILITY_STATE ).is( VisibilityState.VISIBLE ) );
        query.addCriteria( Criteria.where(Course.Constants.EARLY_LEARNING).ne(true));
        query.fields().include( Course.Constants.GRADES );
        query.fields().include( Course.Constants.NORMALIZE_TARGETS );
        query.fields().include( Course.Constants.NORMALIZE_SUBJECTS );
        query.fields().include( Course.Constants.TITLE );
        start = (start != null) ? start : 0;
        size = (size != null) ? size : 20;
        if(MAX_ALLOWED_FETCH_SIZE < size) {
            logger.error( "getCourseForSubscription crossed MAX_ALLOWED_FETCH_SIZE "+ size );
        }
        query.skip(start);
        query.limit(size);
        logger.info("getCoursesForSubscription - query - {}",query);
        return runQuery(query, Course.class);
    }
    public List<Course> getCourseCurriculum(List<String> courseId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Course.Constants.ID).in(courseId));
        query.fields().include( Course.Constants.NORMALIZE_SUBJECTS );
        query.fields().include( Course.Constants.CURRICULUM );
        query.limit(20);
        query.skip(0);
        return runQuery(query, Course.class);
    }
}
