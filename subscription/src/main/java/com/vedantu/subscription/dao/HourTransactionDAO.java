package com.vedantu.subscription.dao;

import java.util.List;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.util.dbutils.*;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.sql.HourTransaction;
import com.vedantu.util.LogFactory;

@Service
public class HourTransactionDAO extends com.vedantu.util.dbutils.AbstractSqlDAO {
	
	@Autowired
	private LogFactory logFactory;
	
	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(HourTransactionDAO.class);

	public HourTransactionDAO() {
		super();
	}


	@Autowired
	SqlSessionFactory sqlSessionFactory;


	@Override
	protected SessionFactory getSessionFactory() {
		return sqlSessionFactory.getSessionFactory();
	}

	public void create(HourTransaction hourTransaction, Session session) {
		create(hourTransaction, session, null);
	}


	public void create(HourTransaction hourTransaction, Session session, String callingUserId) {
		try {
			if (hourTransaction != null) {
				saveEntity(hourTransaction, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("Create HourTransaction: "+ex.getMessage());
		}
	}

	public HourTransaction getById(Long id, Boolean enabled, Session session) {
		HourTransaction hourTransaction = null;
		try {
			if (id != null) {
				hourTransaction = (HourTransaction) getEntityById(id, enabled, HourTransaction.class, session);
			}
		} catch (Exception ex) {
			logger.error("getById HourTransaction: "+ex.getMessage());
			hourTransaction = null;
		}
		return hourTransaction;
	}

	public void update(HourTransaction p, Session session) {
		update(p, session, null);
	}

	public void update(HourTransaction p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateEntity(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("update HourTransaction: "+ex.getMessage());
		}
	}

	public void updateAll(List<HourTransaction> p, Session session) {
		updateAll(p, session, null);
	}


	public void updateAll(List<HourTransaction> p, Session session, String callingUserId) {
		try {
			if (p != null) {
				updateAllEntities(p, session, callingUserId);
			}
		} catch (Exception ex) {
			logger.error("updateAll HourTransaction: "+ex.getMessage());
		}
	}
}
