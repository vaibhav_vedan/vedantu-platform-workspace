package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.GameJourney;
import com.vedantu.subscription.enums.game.Eligibility;
import com.vedantu.subscription.enums.game.GameTaskStatus;
import com.vedantu.subscription.viewobject.request.GamificationToolFilterRequest;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GameJourneyDAO extends AbstractMongoDAO {

    private final Logger logger = LogFactory.getLogger(GameJourneyDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public void save(GameJourney gameJourney) {
        if (gameJourney != null) {
            logger.info("saving gameJourney into db: " + gameJourney);
            saveEntity(gameJourney);
        } else {
            logger.info("gameJourney is null");
        }
    }

    public GameJourney getJourneyForEligibleUser(Long userId, String activeGameId, String... fieldsToInclude) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GameJourney.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(GameJourney.Constants.ELIGIBILITY).is(Eligibility.ELIGIBLE));
        query.addCriteria(Criteria.where(GameJourney.Constants.GAME_ID).is(activeGameId));
        if (fieldsToInclude != null && fieldsToInclude.length != 0) {
            for (String field : fieldsToInclude) {
                query.fields().include(field);
            }
        }
        return findOne(query, GameJourney.class);
    }

    public GameJourney getJourneyUser(Long userId, String activeGameId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(GameJourney.Constants.USER_ID).is(userId));
        // TODO remove to include previous game users too for new game
        // query.addCriteria(Criteria.where(GameJourney.Constants.GAME_ID).is(activeGameId));
        return findOne(query, GameJourney.class);
    }

    public List<GameJourney> getClaimedUsers(GamificationToolFilterRequest request, String activeGameTemplateId) {

        Query query = new Query();

        query.addCriteria(Criteria.where(GameJourney.Constants.ELIGIBILITY).is(Eligibility.ELIGIBLE));

        // UserId filter
        if (request.getUserId() != null) {
            query.addCriteria(Criteria.where(GameJourney.Constants.USER_ID).is(request.getUserId()));
        }

        // TODO remove this to get all game data
        query.addCriteria(Criteria.where(GameJourney.Constants.GAME_ID).is(activeGameTemplateId));

        List<Criteria> criteriaList = new ArrayList<>();
        Criteria gameTaskListCriteria = new Criteria();
        // Status Filter like CLAIMED/SHIPPED
        if (request.getStatus() == null || request.getStatus().toString().equals("")) {
            Criteria claimedCriteria = Criteria.where(GameJourney.Constants.STATUS).is(GameTaskStatus.CLAIMED);
            Criteria shippedCriteria = Criteria.where(GameJourney.Constants.STATUS).is(GameTaskStatus.SHIPPED);
            Criteria orCriteria = new Criteria().orOperator(shippedCriteria, claimedCriteria);
            criteriaList.add(orCriteria);
            //query.addCriteria(orCriteria);
        } else {
            Criteria statusCriteria = Criteria.where(GameJourney.Constants.STATUS).is(request.getStatus());
            criteriaList.add(statusCriteria);
            //query.addCriteria(elementCriteria);
        }

        if (!StringUtils.isEmpty(request.getRewardName())) {
            Criteria rewardName = Criteria.where(GameJourney.Constants.NAME).is(request.getRewardName());
            criteriaList.add(rewardName);
        }

        // Start Time and End Time filters
        if (request.getStartTime() != null && request.getEndTime() != null) {
            Criteria claimedTimeCriteria = Criteria.where(GameJourney.Constants.COMPLETED_TIME).gte(request.getStartTime()).lte(request.getEndTime());
            criteriaList.add(claimedTimeCriteria);
            //query.addCriteria(claimedTimeCriteria);
        }

        // Reward Item Filter
        if (!StringUtils.isEmpty(request.getRewardItem())) {
            Criteria itemCriteria = Criteria.where(GameJourney.Constants.REWARD_ITEM).is(request.getRewardItem());
            criteriaList.add(itemCriteria);
            //query.addCriteria(itemCriteria);
        }

        logger.info("criterias: " + criteriaList.toArray(new Criteria[]{}));
        gameTaskListCriteria.andOperator(criteriaList.toArray(new Criteria[]{}));
        Criteria elementCriteria = Criteria.where(GameJourney.Constants.GAME_TASKS).elemMatch(gameTaskListCriteria);
        // Filter for Reward Name - REWARD1/REWARD2 not written since they will be available for all the users

        query.addCriteria(elementCriteria);
        //query.addCriteria(Criteria.where(GameJourney.Constants.GAME_TASKS_TYPE).is(GameTaskType.REWARD));
        query.fields().include(GameJourney.Constants.USER_ID);
        query.fields().include(GameJourney.Constants.GAME_ID);
        query.fields().include(GameJourney.Constants.GAME_TASKS_NAME);
        query.fields().include(GameJourney.Constants.GAME_TASKS_TYPE);
        query.fields().include(GameJourney.Constants.GAME_TASKS_STATUS);
        query.fields().include(GameJourney.Constants.GAME_TASKS_REWARDDETAILS);
        query.fields().include(GameJourney.Constants.GAME_TASKS_COMPLETEDTIME);
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoEntity.Constants.ID));
        setFetchParameters(query, request.getStart(), request.getSize());
        logger.info("query: " + query);
        return runQuery(query, GameJourney.class);
    }

    public void addEnrollmentIds(GameJourney journey) {
        Query query = new Query();
        Update update = new Update();
        query.addCriteria(Criteria.where(GameJourney.Constants._ID).is(journey.getId()));
        logger.info("setting enrollmentIds to db: " + journey.getEnrollmentIds());
        update.set(GameJourney.Constants.ENROLLMENT_IDS, journey.getEnrollmentIds());
        updateFirst(query, update, GameJourney.class);
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }
}
