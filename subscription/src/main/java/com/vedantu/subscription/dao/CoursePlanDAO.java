/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vedantu.subscription.dao;

import com.google.gson.Gson;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.vedantu.subscription.request.CoursePlanBasicInfoReq;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.vedantu.User.User;
import com.vedantu.subscription.entities.mongo.CoursePlan;
import com.vedantu.subscription.enums.CoursePlanEnums;
import com.vedantu.subscription.enums.CoursePlanEnums.CoursePlanState;
import com.vedantu.subscription.enums.ReminderTimeType;
import com.vedantu.subscription.managers.AwsSQSManager;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.subscription.request.GetCoursePlanReminderListReq;
import com.vedantu.subscription.request.GetCoursePlansReq;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoEntity;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.enums.SQSMessageType;
import com.vedantu.util.enums.SQSQueue;
import java.util.Arrays;

import static com.vedantu.util.dbentities.mongo.AbstractMongoEntity.Constants._ID;

/**
 *
 * @author ajith
 */
@Service
public class CoursePlanDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(CoursePlanDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;
    
    @Autowired
    private AwsSQSManager awsSQSManager;
    
    private final Gson gson = new Gson();

    public CoursePlanDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(CoursePlan p, Long callingUserId) {
        //try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            boolean newCP = StringUtils.isEmpty(p.getId());
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
            if(newCP){
                awsSQSManager.triggerAsyncSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.COURSEPLAN_NEW_LEADSQUARE, gson.toJson(p), p.getStudentId().toString()+"_CP");
                
            }else {
                awsSQSManager.triggerAsyncSQS(SQSQueue.LEADSQUARED_QUEUE, SQSMessageType.COURSEPLAN_UPDATED_LEADSQUARE, gson.toJson(p), p.getStudentId().toString()+"_CP");
            }
            
        }
    }

    public CoursePlan getCoursePlanById(String id) {
        return getEntityById(id, CoursePlan.class);
    }

    public CoursePlan getCoursePlanTitleById(String id){
        if(StringUtils.isNotEmpty(id)){
            Query query = new Query();
            query.addCriteria(Criteria.where(CoursePlan.Constants._ID).in(id));
            query.fields().include(AbstractMongoEntity.Constants.ID);
            query.fields().include(CoursePlan.Constants.TITLE);
            return findOne(query,CoursePlan.class);
        }
        return null;
    }

    public List<CoursePlan> getBasicInfosForHomePage(CoursePlanBasicInfoReq req){
        if(ArrayUtils.isNotEmpty(req.getCoursePlanIds())){
            Query query = new Query();
            query.addCriteria(Criteria.where(CoursePlan.Constants._ID).in(req.getCoursePlanIds()));
            query.fields().include(AbstractMongoEntity.Constants.ID);
            query.fields().include(CoursePlan.Constants.TITLE);
            return runQuery(query, CoursePlan.class);
        }
        return new ArrayList<>();
    }

    public List<CoursePlan> getNonDraftCoursePlans(String parentId, Long studentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CoursePlan.Constants.PARENT_COURSE_ID).is(parentId));
        query.addCriteria(Criteria.where(CoursePlan.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).ne(CoursePlanEnums.CoursePlanState.DRAFT));
        setFetchParameters(query, null, null);
        logger.info("query " + query);
        return runQuery(query, CoursePlan.class);
    }

    public List<CoursePlan> getPublishedCoursePlans(String parentId, Long studentId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(CoursePlan.Constants.PARENT_COURSE_ID).is(parentId));
        query.addCriteria(Criteria.where(CoursePlan.Constants.STUDENT_ID).is(studentId));
        query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).is(CoursePlanEnums.CoursePlanState.PUBLISHED));
        setFetchParameters(query, null, null);
        logger.info("query " + query);
        return runQuery(query, CoursePlan.class);
    }

    public List<CoursePlan> getCoursePlansByIds(List<String> ids) {
        return getCoursePlansByIds(ids,null);
    }

    public List<CoursePlan> getCoursePlansByIds(List<String> ids,List<String> includeFields) {
        if (ids == null) {
            return null;
        }
        Query query;
        query = new Query();
        query.addCriteria(Criteria.where(CoursePlan.Constants.ID).in(ids));
        return runQuery(query, CoursePlan.class,includeFields);
    }

    public List<CoursePlan> getCoursePlanBasicInfos(GetCoursePlansReq req) {
        Query query = new Query();
        if (req.getStudentId() != null) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.STUDENT_ID).is(req.getStudentId()));
        }
        if (req.getTeacherId() != null) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.TEACHER_ID).is(req.getTeacherId()));
        }
        if (req.getStates() != null && !req.getStates().isEmpty()) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).in(req.getStates()));
        }
        if (req.getTarget() != null) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.TARGET).is(req.getTarget()));
        }
        if (req.getGrade() != null) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.GRADE).is(req.getGrade()));
        }
        if (req.getSubject() != null) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.SUBJECT).is(req.getSubject()));
        }
        if (req.getBoardId() != null) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.BOARD_ID).is(req.getBoardId()));
        }

        if (req.getLastUpdated() != null) {
            query.addCriteria(Criteria.where(CoursePlan.Constants.LAST_UPDATED).gt(req.getLastUpdated()));
        }

        Criteria regularDateCriteria = null;
        if (req.getFromRegularSessionStartDate() != null) {
            regularDateCriteria = Criteria.where(CoursePlan.Constants.REGULAR_SESSION_START_DATE).gte(req.getFromRegularSessionStartDate());
        }
        if (req.getTillRegularSessionStartDate() != null) {
            if (regularDateCriteria != null) {
                regularDateCriteria = regularDateCriteria.andOperator(Criteria.where(CoursePlan.Constants.REGULAR_SESSION_START_DATE).lte(req.getTillRegularSessionStartDate()));
            } else {
                regularDateCriteria = Criteria.where(CoursePlan.Constants.REGULAR_SESSION_START_DATE).lte(req.getTillRegularSessionStartDate());
            }
        }
        if(regularDateCriteria!=null){
            query.addCriteria(regularDateCriteria);
        }
        

        Criteria startDateCriteria = null;
        if (req.getFromStartDate() != null) {
            startDateCriteria = Criteria.where(CoursePlan.Constants.START_DATE).gte(req.getFromStartDate());
        }
        if (req.getTillStartDate() != null) {
            if (startDateCriteria != null) {
                startDateCriteria = startDateCriteria.andOperator(Criteria.where(CoursePlan.Constants.START_DATE).lte(req.getTillStartDate()));
            } else {
                startDateCriteria = Criteria.where(CoursePlan.Constants.START_DATE).lte(req.getTillStartDate());
            }
        }
        if(startDateCriteria!=null){
            query.addCriteria(startDateCriteria);
        }

        setFetchParameters(query, req);
        if (req.getOnlyBasicDetails()) {
            query.fields().include(AbstractMongoEntity.Constants.ID);
            query.fields().include(CoursePlan.Constants.STUDENT_ID);
            query.fields().include(CoursePlan.Constants.TEACHER_ID);
            query.fields().include(CoursePlan.Constants.STATE);
            query.fields().include(CoursePlan.Constants.SUBJECT);
            query.fields().include(CoursePlan.Constants.TARGET);
            query.fields().include(CoursePlan.Constants.GRADE);
            query.fields().include(CoursePlan.Constants.BOARD_ID);
            query.fields().include(CoursePlan.Constants.TITLE);
            query.fields().include(CoursePlan.Constants.TOTAL_COURSE_HOURS);
            query.fields().include(CoursePlan.Constants.COMPACT_SCHEDULE);
            query.fields().include(CoursePlan.Constants.DESCRIPTION);
            query.fields().include(CoursePlan.Constants.PARENT_COURSE_ID);
            query.fields().include(CoursePlan.Constants.PRICE);
            query.fields().include(CoursePlan.Constants.TEACHER_PAYOUT_RATE);
            query.fields().include(CoursePlan.Constants.TRIAL_REGISTRATION_FEE);
            query.fields().include(CoursePlan.Constants.START_DATE);
            query.fields().include(CoursePlan.Constants.REGULAR_SESSION_START_DATE);
            query.fields().include(CoursePlan.Constants.FIRST_REGULAR_PAYMENT_DUE_DATE);
            query.fields().include(CoursePlan.Constants.END_TIME);
            query.fields().include(CoursePlan.Constants.END_REASON);
            query.fields().include(CoursePlan.Constants.ENDED_BY);
            query.fields().include(CoursePlan.Constants.END_TYPE);

            setQueryFieldsFromAbstractEntity(query);
        }
        query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        return runQuery(query, CoursePlan.class);
    }
    
    public List<CoursePlan> getCoursePlansForUser(Long userId){
        
        Query query = new Query();
        query.addCriteria(Criteria.where(CoursePlan.Constants.STUDENT_ID).is(userId));
        query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).is(CoursePlanEnums.CoursePlanState.ENROLLED));
        List<CoursePlan> result = new ArrayList<>();
        int start = 0;
        int size = 100;
        while(true){
            setFetchParameters(query, start, size);
            List<CoursePlan> coursePlans = runQuery(query, CoursePlan.class);
            
            if(ArrayUtils.isEmpty(coursePlans)){
                break;
            }
            
            result.addAll(coursePlans);
            
            if(coursePlans.size() < size){
                break;
            }
            start = start + size;
        }
        return result;
    }

    public List<CoursePlan> getByCreationTime(ExportCoursePlansReq req) {
        Long fromTime = req.getFromTime();
        Long tillTime = req.getTillTime();
        Query query = new Query();
        Criteria criteria = null;
        if (fromTime != null && fromTime > 0) {
            criteria = Criteria.where(CoursePlan.Constants.CREATION_TIME).gte(fromTime);
        }

        if (tillTime != null && tillTime > 0) {
            if (criteria != null) {
                criteria = criteria.andOperator(Criteria.where(CoursePlan.Constants.CREATION_TIME).lt(tillTime));
            } else {
                criteria = Criteria.where(CoursePlan.Constants.CREATION_TIME).lt(tillTime);
            }
        }
        query.addCriteria(criteria);
        query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.CREATION_TIME));
        setFetchParameters(query, req.getStart(), req.getSize());
        return runQuery(query, CoursePlan.class);
    }
    
    public List<CoursePlan> getByLastUpdated(Long startTime,Long endTime){
    	Query query = new Query();
    	query.addCriteria(Criteria.where(CoursePlan.Constants.LAST_UPDATED).gte(startTime).lt(endTime));
    	
    	query.fields().include(CoursePlan.Constants.TEACHER_ID);
    	query.fields().include(CoursePlan.Constants.STUDENT_ID);
    	query.fields().include(CoursePlan.Constants.END_TIME);
    	query.fields().include(CoursePlan.Constants.STATE);
    	query.fields().include(CoursePlan.Constants.STATE_CHANGE_TIME);
    	query.fields().include(CoursePlan.Constants._ID);
    	query.fields().include(CoursePlan.Constants.ID);
    	
    	query.with(Sort.by(Sort.Direction.ASC,
                AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
    	
    	return runQuery(query, CoursePlan.class);
    }

    public Set<String> getActiveCoursePlans(int start, int size) {
    	Query query = new Query();
    	query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).ne(CoursePlanState.ENDED));
    	query.fields().include(CoursePlan.Constants.ID);
    	query.skip(start);
    	query.limit(size);

    	query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.CREATION_TIME));
    	List<CoursePlan> coursePlans = runQuery(query, CoursePlan.class);
    	Set<String> results = new HashSet<>();
    	if (!CollectionUtils.isEmpty(coursePlans)) {
    		results = coursePlans.stream().map(c -> c.getId()).collect(Collectors.toSet());
    	}
    	return results;
    }
    //check for sessions in 
    public List<CoursePlan> getCoursePlanReminderList(GetCoursePlanReminderListReq req){
    	Query query = new Query();
    	if(req.getTimeType() != null && ReminderTimeType.INSTALMENT.equals(req.getTimeType())){
    		query.addCriteria(Criteria.where(CoursePlan.Constants.INSTALMENTS_INFO_DUETIME).gte(req.getStartTime()).lt(req.getEndTime()));
    		List<CoursePlanState> states = new ArrayList<>();
    		if(ArrayUtils.isNotEmpty(req.getState())){
    			states.addAll(req.getState());
    		}else{
    			states.add(CoursePlanState.PUBLISHED);
    			states.add(CoursePlanState.TRIAL_PAYMENT_DONE);
    			states.add(CoursePlanState.TRIAL_SESSIONS_DONE);
    		}
    		query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).in(states));
    		
    	}else if(req.getTimeType() != null && ReminderTimeType.TRIAL.equals(req.getTimeType())){
    		query.addCriteria(Criteria.where(CoursePlan.Constants.TRIAL_SESSION_START_TIME).gte(req.getStartTime()).lt(req.getEndTime()));
    		if(ArrayUtils.isNotEmpty(req.getState())){
    			query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).in(req.getState()));
    		}else {
    			query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).is(CoursePlanState.PUBLISHED));
    		}    		
    	}
    	query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
    	setFetchParameters(query,req);
    	List<CoursePlan> coursePlans = runQuery(query,CoursePlan.class);
    	
    	return coursePlans;
    }
    
    public List<CoursePlan> getCoursePlansByIds(GetCoursePlanReminderListReq req){
    	List<CoursePlan> courses = new ArrayList<>();
    	if(ArrayUtils.isEmpty(req.getCoursePlanIds())){
    		return courses;
    	}
    	Query query = new Query();
    	query.addCriteria(Criteria.where(CoursePlan.Constants._ID).in(req.getCoursePlanIds()));
    	if(ArrayUtils.isNotEmpty(req.getState())){
    		query.addCriteria(Criteria.where(CoursePlan.Constants.STATE).in(req.getState()));
    	}
    	return runQuery(query,CoursePlan.class);
    }

    public List<CoursePlan> getUserCoursePlans(Long userId, List<String> includeFields) {

        Query query = new Query();
        query.addCriteria(Criteria.where(CoursePlan.Constants.STUDENT_ID).is(userId));
        query.limit(50);
        query.with(Sort.by(Sort.Direction.DESC, _ID));
        return runQuery(query, CoursePlan.class, includeFields);
    }

}
