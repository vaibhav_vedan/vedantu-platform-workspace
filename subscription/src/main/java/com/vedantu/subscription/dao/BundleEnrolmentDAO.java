package com.vedantu.subscription.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.vedantu.subscription.entities.mongo.*;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.exception.ConflictException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.onetofew.enums.CourseTerm;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.onetofew.enums.EntityStatus;
import com.vedantu.onetofew.pojo.StatusChangeTime;
import com.vedantu.subscription.pojo.EnrolledBundlesRes;
import com.vedantu.subscription.request.CheckBundleEnrollmentReq;
import com.vedantu.subscription.request.GetBundleEnrolmentsReq;
import com.vedantu.subscription.response.TrialExpirationResp;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import com.vedantu.util.fos.request.AbstractFrontEndListReq;

/**
 * Created by somil on 09/05/17.
 */
@Service
public class BundleEnrolmentDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
	private final Logger logger = logFactory.getLogger(BundleEnrolmentDAO.class);

    @Autowired
    private MongoClientFactory mongoClientFactory;

    public BundleEnrolmentDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(BundleEnrolment p, Long callingUserId) throws ConflictException {
        // try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            if(EntityStatus.ENDED.equals(p.getStatus())){
               p.setRandomBundleEnrollmentId(UUID.randomUUID().toString());
            }
            logger.info("Saving into db" + p);
            try {
                saveEntity(p, callingUserIdString);
            }catch(org.springframework.dao.DuplicateKeyException | com.mongodb.DuplicateKeyException e){
                logger.warn("BundleEnrolment already exists for the user [" + p.getUserId() + "]" + " and BundleId [" + p.getBundleId() + "]");
                throw new ConflictException(ErrorCode.BUNDLE_ENROLLMENT_ALREADY_EXIST,"BundleEnrolment already exists for the user [" + p.getUserId() + "]"
                        + " and BundleId [" + p.getBundleId() + "]");
            }
        }
    }

    public BundleEnrolment getBundleEnrolmentById(String id) {
        return getEntityById(id, BundleEnrolment.class);
    }

    public List<BundleEnrolment> getBundles(AbstractFrontEndListReq req) {
        Query query = new Query();
        setFetchParameters(query, req);
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));
        return runQuery(query, BundleEnrolment.class);
    }

    public BundleEnrolment getBundleEnrolment(String userId, String bundleId) {
        BundleEnrolment bundleEnrolment = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(bundleId));
        logger.info("query " + query);
        List<BundleEnrolment> results = runQuery(query, BundleEnrolment.class);
        if (null != results && !results.isEmpty()) {
            bundleEnrolment = results.get(0);
        }
        return bundleEnrolment;
    }

    public BundleEnrolment getNonEndedBundleEnrolment(String userId, String bundleId) {
        BundleEnrolment bundleEnrolment = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).ne(EntityStatus.ENDED));
        logger.info("query " + query);
        List<BundleEnrolment> results = runQuery(query, BundleEnrolment.class);
        if (null != results && !results.isEmpty()) {
            bundleEnrolment = results.get(0);
        }
        return bundleEnrolment;
    }

    public List<BundleEnrolment> getBundleEnrolments(String userId, List<String> bundleIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).in(bundleIds));
        logger.info("query " + query);
        return runQuery(query, BundleEnrolment.class);
    }

    public List<BundleEnrolment> getUsersActiveEnrolment(String userId) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        logger.info("query " + query);
        List<BundleEnrolment> results = runQuery(query, BundleEnrolment.class);
        return results;
    }

    public List<BundleEnrolment> getRegularActiveEnrolments(String userId, Integer start, Integer limit) {
        start = (start == null) ? 0 : start;
        limit = (limit == null) ? 20 : limit;
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.REGULAR));
        query.skip( start );
        query.limit( limit );
        logger.info("query " + query);
        List<BundleEnrolment> results = runQuery(query, BundleEnrolment.class);
        return results;
    }

    public List<BundleEnrolment> getUsersNonEndedEnrolment(String userId,List<String> includeFields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).ne(EntityStatus.ENDED));
        logger.info("query " + query);
        List<BundleEnrolment> results = runQuery(query, BundleEnrolment.class,includeFields);
        logger.info("results " + results);
        return results;
    }

    public List<BundleEnrolment> getBundleEnrolmentsFromReq(GetBundleEnrolmentsReq req) {
        return getBundleEnrolmentsFromReq(req,null,null);
    }
    public List<BundleEnrolment> getBundleEnrolmentsFromReq(GetBundleEnrolmentsReq req,List<String> includeFields,List<String> excludeFields) {
        Query query = new Query();
        if (req.getStatus() != null) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(req.getStatus()));
        } else if (req.getStatusList() != null) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(req.getStatusList()));
        }
        if (req.getBundleId() != null) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(req.getBundleId()));
        }
        if (req.getUserId() != null) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(req.getUserId()));
        }
        if (req.getLastUpdated() != null) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.LAST_UPDATED).gt(req.getLastUpdated()));
        }
        if (req.getCreatedBefore() != null) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.CREATION_TIME).lt(req.getCreatedBefore()));
        }
        if(ArrayUtils.isNotEmpty(includeFields)){
            includeFields.forEach(query.fields()::include);
        }
        if(ArrayUtils.isNotEmpty(excludeFields)){
            excludeFields.forEach(query.fields()::exclude);
        }
        setFetchParameters(query, req);
        query.with(Sort.by(Sort.Direction.DESC, BundleEnrolment.Constants.CREATION_TIME));
        logger.info("query - getBundleEnrolmentsFromReq - {}",query);
        List<BundleEnrolment> results = runQuery(query, BundleEnrolment.class);
        return results;
    }

    public List<BundleEnrolment> getBundleEnrolmentsFromReq(CheckBundleEnrollmentReq req) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(req.getBundleId()));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(req.getUserId()));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(EntityStatus.ACTIVE, EntityStatus.INACTIVE));

        if (req.getState() != null) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(req.getState()));
        }


        List<BundleEnrolment> enrolments = runQuery(query, BundleEnrolment.class);
        return enrolments;
    }

    public List<BundleEnrolment> getActiveTrialEnrolmentsByIds(List<String> enrolmentIds, EnrollmentState trial) {
        Query query = new Query();
        query.addCriteria(Criteria.where("_id").in(enrolmentIds));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.TRIAL));

        List<BundleEnrolment> enrolments = runQuery(query, BundleEnrolment.class);
        return enrolments;
    }

    public List<BundleEnrolment> getActiveBundleEnrolmentsByBundleIds(List<String> bundleIds) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).in(bundleIds));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE)));
        return runQuery(query, BundleEnrolment.class);
    }

    public List<BundleEnrolment> getUsersActiveAndInactiveEnrollment(String userId) {
        if (StringUtils.isEmpty(userId)) {
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        query.with(Sort.by(Sort.Direction.DESC, BundleEnrolment.Constants.STATUS));
        query.fields().include(BundleEnrolment.Constants.STATUS);
        query.fields().include(BundleEnrolment.Constants.STATE);
        query.fields().include(BundleEnrolment.Constants.BUNDLE_ID);
        logger.info("getUsersActiveAndInactiveEnrollment - query - {}", query);
        return runQuery(query, BundleEnrolment.class);
    }

    public List<BundleEnrolment>  getBundleEnrollmentsToBeEnded() {
        Long currentMillis = System.currentTimeMillis();
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.IS_SUBSCRIPTION_PLAN).ne(true));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.VALID_TILL).lt(currentMillis));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        List<BundleEnrolment> bundleEnrolments=runQuery(query,BundleEnrolment.class);

        return bundleEnrolments;
    }

    public void endBundleEnrolmentByCron(StatusChangeTime statusChangeTime,String bundleEnrollmentId){

        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants._ID).is(bundleEnrollmentId));
        Update update = new Update();
        update.set(BundleEnrolment.Constants.RANDOM_BUNDLE_ENROLLMENTID,UUID.randomUUID().toString());
        update.set(BundleEnrolment.Constants.STATUS, EntityStatus.ENDED);
        update.addToSet(BundleEnrolment.Constants.STATUS_CHANGE_TIME, statusChangeTime);
        updateFirst(query,update,BundleEnrolment.class);
    }

    public void updateValidTillOfChangedBundle(String bundleId, Long validTill) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));

        Update update = new Update();
        update.set(BundleEnrolment.Constants.VALID_TILL, validTill);

        updateMulti(query, update, BundleEnrolment.class);
    }

    public List<BundleEnrolment> getUsersActiveEnrolments(GetBundleEnrolmentsReq req) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).in(req.getUserIdList()));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(req.getStatusList()));
        query.fields().include(BundleEnrolment.Constants.USER_ID);
        query.fields().include(BundleEnrolment.Constants.BUNDLE_ID);
        logger.info("query " + query);
        List<BundleEnrolment> results = runQuery(query, BundleEnrolment.class);
        return results;
    }

    public EnrolledBundlesRes getEnrolledBundlesHomeFeed(List<String> bundleIds, String userId) {
        logger.info("Getting Enrolled Bundles for userId : " + userId);
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId))
                .addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).in(bundleIds))
                .addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));

        query.fields().include(BundleEnrolment.Constants.BUNDLE_ID);

        logger.info("Projection query : {} ", query);

        List<BundleEnrolment> bundleEnrolments = runQuery(query, BundleEnrolment.class);

        EnrolledBundlesRes enrolledBundlesRes = new EnrolledBundlesRes();

        if (!bundleEnrolments.isEmpty()) {

            Set<String> enrolledBundleIds = new HashSet<>();

            for (BundleEnrolment bundleEnrolment : bundleEnrolments)
                enrolledBundleIds.add(bundleEnrolment.getBundleId());

            logger.info("Enrolled Bundles : {}", enrolledBundleIds);

            enrolledBundlesRes.setEnrolledBundles(enrolledBundleIds);
        }

        return enrolledBundlesRes;
    }

    public List<String> getBundleIdsOfAllKindOfTrialEnrollment(String userId) {

        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.TRIAL_ID).ne(null));

        query.fields().include(BundleEnrolment.Constants.BUNDLE_ID);

        query.fields().exclude(BundleEnrolment.Constants._ID);

        List<BundleEnrolment> bundleEnrolments = runQuery(query, BundleEnrolment.class);

        return Optional.ofNullable(bundleEnrolments)
                .map(Collection::stream)
                .orElseGet(Stream::empty)
                .map(BundleEnrolment::getBundleId)
                .collect(Collectors.toList());

    }
    public List<BundleEnrolment> getEnrollmentsOfAllKindOfTrialEnrollment(String userId,List<String> includeFields) {

        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.TRIAL_ID).ne(null));


        List<BundleEnrolment> bundleEnrolments = runQuery(query, BundleEnrolment.class,includeFields);

        return bundleEnrolments;
    }
    /*
    public void getAllTrialEnrollmentAndMigrateThem(String bundleId,String trialId) {

        // Getting all the possibility of trial enrollment happened before migration
        Criteria orCriteria=new Criteria().orOperator(
                Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.TRIAL),
                Criteria.where(BundleEnrolment.Constants.PREVIOUS_STATUS).is(EnrollmentState.TRIAL),
                Criteria.where(BundleEnrolment.Constants.NEW_STATUS).is(EnrollmentState.TRIAL)
        );

        Query query=new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.TRIAL_ID).is(null));
        query.addCriteria(orCriteria);

        Update update=new Update();
        update.set(BundleEnrolment.Constants.TRIAL_ID,trialId);

        updateMulti(query,update,BundleEnrolment.class);

    }
    */


    public List<BundleEnrolment> getMicrocoursesStudent(int start, int size) {
        logger.info("Query for Microcourse Bundle Enrolment");
        Query query = new Query();
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(BundleEnrolment.Constants.SEARCH_TERMS).in(CourseTerm.MICRO_COURSES), Criteria.where(BundleEnrolment.Constants.SEARCH_TERMS).nin(CourseTerm.LONG_TERM));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).ne(EnrollmentState.TRIAL));
        query.addCriteria(andCriteria);
        query.fields().include(BundleEnrolment.Constants.USER_ID);
        setFetchParameters(query, start, size);
        logger.info("Query :" + query);
        return runQuery(query, BundleEnrolment.class);
    }

    public boolean isValidEnrolmentForOnboarding(String userId, String bundleId, Long creationTime) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.REGULAR));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.CREATION_TIME).gt(creationTime));
        logger.info("Query :" + query);
        return getMongoOperations().exists(query, BundleEnrolment.class);
    }


    public Long getActiveEnrollmentCount(String userId , List<String> bundleIds) {
        if(StringUtils.isEmpty(userId) && ArrayUtils.isEmpty(bundleIds)){
            return 0l;
        }


        Query query = new Query();

        //query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.REGULAR));
        if(StringUtils.isNotEmpty(userId )) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        }
        if(ArrayUtils.isNotEmpty(bundleIds )) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).in(bundleIds));
        }

        logger.info("Query :" + query);
        return queryCount(query, BundleEnrolment.class);
    }
    public String  getTrailEnrollment(String userId,List<String> trialIds) {

        Query query = new Query();

        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).ne(EntityStatus.ENDED));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.TRIAL));
        if(StringUtils.isNotEmpty(userId )) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        }
        if(ArrayUtils.isNotEmpty(trialIds )) {
            query.addCriteria(Criteria.where(BundleEnrolment.Constants.TRIAL_ID).in(trialIds));
        }
        query.fields().include(BundleEnrolment.Constants.TRIAL_ID);
            BundleEnrolment bundleEnrolment =findOne(query, BundleEnrolment.class);
        if(bundleEnrolment == null){
            return  "";
        }
            return bundleEnrolment.getTrialId();
    }
    public List<BundleEnrolment>  getExpiredSubscriptions() {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.ENTITY_STATE).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.IS_SUBSCRIPTION_PLAN).is(true));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.VALID_TILL).lt(System.currentTimeMillis()));
        query.fields().include(BundleEnrolment.Constants.ID);
        query.fields().include(BundleEnrolment.Constants.BUNDLE_ID);
        query.fields().include(BundleEnrolment.Constants.USER_ID);
        logger.info("Query :" + query);
        return runQuery(query, BundleEnrolment.class);
    }

    public void  markExpiredSubscriptionsInactive(StatusChangeTime statusChangeTime, List<String> enrollmentIds ) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants._ID).in(enrollmentIds));

        Update update = new Update();

        update.set(BundleEnrolment.Constants.STATUS,EntityStatus.INACTIVE );
        update.addToSet(BundleEnrolment.Constants.STATUS_CHANGE_TIME,statusChangeTime);
        logger.info("update Query :" + query);
        updateMulti(query, update, BundleEnrolment.class);



    }



	public TrialExpirationResp getTrialEnrollmentData(String userId) {

		TrialExpirationResp trialExpirationResp = new TrialExpirationResp();
		trialExpirationResp.setUserId(userId);
		Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.TRIAL_ID).exists(true));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.TRIAL));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).ne(EntityStatus.ENDED));
        query.fields().include(BundleEnrolment.Constants.USER_ID);
        query.fields().include(BundleEnrolment.Constants.BUNDLE_ID);
        query.fields().include(BundleEnrolment.Constants.STATUS);
        query.fields().include(BundleEnrolment.Constants.STATE);
        query.fields().include(BundleEnrolment.Constants.TRIAL_ID);
        query.fields().include(BundleEnrolment.Constants.FREE_PASS_VALID_TILL);
        query.with(Sort.by(Sort.Direction.DESC, AbstractMongoStringIdEntity.Constants.LAST_UPDATED));

        logger.debug("Query :" + query);
        BundleEnrolment bundleEnrolment = findOne(query, BundleEnrolment.class);
        if(null != bundleEnrolment) {
        	trialExpirationResp.setBundleId(bundleEnrolment.getBundleId());
        	trialExpirationResp.setState(bundleEnrolment.getState());
        	trialExpirationResp.setTrialId(bundleEnrolment.getTrialId());
        	trialExpirationResp.setStatus(bundleEnrolment.getStatus());
        	trialExpirationResp.setValidTill(bundleEnrolment.getFreePassvalidTill());
        	trialExpirationResp.setIsTrialUser(true);
        }
		return trialExpirationResp;
	}

    public List<String> getBundleIdsOfAllActiveAndInactiveEnrollmentOfUser(String userId) {
        if (StringUtils.isEmpty(userId)) {
            return new ArrayList<>();
        }
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE, EntityStatus.INACTIVE)));
        logger.info("getUsersActiveAndInactiveEnrollment - query - {}", query);
        return findDistinct(query,BundleEnrolment.Constants.BUNDLE_ID,BundleEnrolment.class,String.class);
    }
    public List<BundleEnrolment> getActiveBundleEnrolmentsByBundleIds(List<String> bundleIds, List<String> includeFields, Integer start, Integer size) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).in(bundleIds));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).is(EntityStatus.ACTIVE));
        if (ArrayUtils.isNotEmpty(includeFields)) {
            includeFields.forEach(field -> query.fields().include(field));
        }
        setFetchParameters(query, start, size);
        return runQuery(query, BundleEnrolment.class);
    }

    public BundleEnrolment getTrialEnrollmentForSameGrade(BundleEnrolment bundleEnrolment) {
        Query query = new Query();
        query.addCriteria(Criteria.where(Bundle.Constants.ID).is(bundleEnrolment.getBundleId()));

        Bundle bundle =   runQuery(query, Bundle.class,Arrays.asList(Bundle.Constants.GRADE,Bundle.Constants.TARGET)).get(0) ;


        Set <Integer> grades = new HashSet<>(bundle.getGrade());
        Set <String> targets = new HashSet<>(bundle.getTarget());
        Query bundleQuery = new Query();
        bundleQuery.addCriteria(Criteria.where(Bundle.Constants.GRADE).size(grades.size()).all(grades));
        bundleQuery.addCriteria(Criteria.where(Bundle.Constants.TARGET).size(targets.size()).all(targets));
        bundleQuery.addCriteria(Criteria.where(Bundle.Constants.ID).ne(bundleEnrolment.getId()));
        bundleQuery.addCriteria(Criteria.where(Bundle.Constants.IS_SUBSCRIPTION).is(true));
        List<Bundle> bundleList =   runQuery(bundleQuery, Bundle.class,Arrays.asList(Bundle.Constants.ID)) ;
        List<String> bundleIds = new ArrayList<>();

        if(ArrayUtils.isEmpty(bundleList)){
            return null;
        }

        bundleList.forEach(_bundle -> {
            bundleIds.add(_bundle.getId());
        } ) ;

        Query trialQuery = new Query();
        trialQuery.addCriteria(Criteria.where(Trial.Constants.CONTEXT_ID).in(bundleIds));
        trialQuery.addCriteria(Criteria.where(Trial.Constants.TOTAL_AMOUNT).is(0));

        List<Trial> trialList =   runQuery(trialQuery, Trial.class,Arrays.asList(Trial.Constants.ID)) ;
        if(ArrayUtils.isEmpty(trialList)){
            return null;
        }


        List<String> trialIds = new ArrayList<>();

        trialList.forEach(trial -> {
            trialIds.add(trial.getId());
        } ) ;

        Query enrollmentQuery = new Query();
        enrollmentQuery.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(bundleEnrolment.getUserId()));
        enrollmentQuery.addCriteria(Criteria.where(BundleEnrolment.Constants.TRIAL_ID).in(trialIds));
        enrollmentQuery.addCriteria(Criteria.where(BundleEnrolment.Constants.STATE).is(EnrollmentState.TRIAL));
        enrollmentQuery.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(Arrays.asList(EntityStatus.ACTIVE,EntityStatus.INACTIVE)));


        return  findOne(enrollmentQuery, BundleEnrolment.class) ;

    }
    public BundleEnrolment getBundleEnrollmentById(String bundleEnrollmentId, List<String> includeFields){
        BundleEnrolment bundleEnrollment = null;
        if(bundleEnrollmentId == null || bundleEnrollmentId.isEmpty())
            return null;

        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.ID).is(bundleEnrollmentId));

        for(String field : includeFields)
            query.fields().include(field);

        List<BundleEnrolment> bundleEnrollments = runQuery(query,BundleEnrolment.class);
        if(bundleEnrollments != null && !bundleEnrollments.isEmpty())
            bundleEnrollment = bundleEnrollments.get(0);
        return bundleEnrollment;
    }
    
    public List<BundleEnrolment> getBundleEnrolments(String userId, List<String> bundleIds, List<String> includeFields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).in(bundleIds));
        logger.info("query " + query);
        return runQuery(query, BundleEnrolment.class, includeFields);
    }
    
    public List<BundleEnrolment> getActiveBundleEnrolments(String userId, List<String> bundleIds, List<String> includeFields) {
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).in(bundleIds));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).in(EntityStatus.ACTIVE, EntityStatus.INACTIVE));
        logger.info("query " + query);
        return runQuery(query, BundleEnrolment.class, includeFields);
    }
    
    public BundleEnrolment getSortedNonEndedBundleEnrolment(String userId, String bundleId) {
        BundleEnrolment bundleEnrolment = null;
        Query query = new Query();
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.USER_ID).is(userId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.BUNDLE_ID).is(bundleId));
        query.addCriteria(Criteria.where(BundleEnrolment.Constants.STATUS).ne(EntityStatus.ENDED));
        query.with(Sort.by(Sort.Direction.DESC, BundleEnrolment.Constants._ID));
        logger.info("query " + query);
        bundleEnrolment = findOne(query, BundleEnrolment.class);
        return bundleEnrolment;
    }
}
