package com.vedantu.subscription.dao;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.mongo.AcadMentorIntervention;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class AcadMentorInterventionDAO extends AbstractMongoDAO {

    @Autowired
    private MongoClientFactory mongoClientFactory;

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(AcadMentorStudentsDAO.class);

    public AcadMentorInterventionDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(List<AcadMentorIntervention> acadMentorInterventions) {
        insertAllEntities(acadMentorInterventions, "AcadMentorIntervention");
    }
    
    public void saveOne(AcadMentorIntervention acadMentorIntervention) {
    	saveEntity(acadMentorIntervention);    	
    }

    public List<AcadMentorIntervention> retrieveOpenInterventions(Long amId) {
        Query q = new Query();
        q.addCriteria(Criteria.where("acadMentorId").is(amId));
        q.addCriteria(Criteria.where("isClosed").is(false));
        logger.info("Logger info: "+ q);
        return runQuery(q, AcadMentorIntervention.class);
    }

    public List<AcadMentorIntervention> retrieveInterventionsByStudent(Long studentId, Integer start, Integer size) {
        Query q = new Query();
        q.addCriteria(Criteria.where("studentId").is(studentId));
        setFetchParameters(q, start, size);
        return runQuery(q, AcadMentorIntervention.class);
    }

    public void closeIntervention(AcadMentorIntervention acadMentorIntervention) {
        Query q = new Query();
        q.addCriteria(Criteria.where("studentId").is(acadMentorIntervention.getStudentId()));
        q.addCriteria(Criteria.where("isClosed").is(false));

        Update u = new Update();
        u.set("isClosed", true);
        u.set("acadMentorComment", acadMentorIntervention.getAcadMentorComment());

        updateMulti(q, u, AcadMentorIntervention.class);
    }

}
