package com.vedantu.subscription.dao;

import java.util.List;

import com.vedantu.dbs.sessionfactory.SqlSessionFactory;
import com.vedantu.onetofew.enums.EnrollmentState;
import com.vedantu.subscription.entities.sql.EnrollmentConsumption;
import com.vedantu.subscription.entities.sql.EnrollmentTransaction;
import com.vedantu.subscription.enums.EnrollmentTransactionContextType;
import com.vedantu.subscription.enums.EnrollmentTransactionType;
import com.vedantu.util.dbutils.*;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.vedantu.util.LogFactory;
import java.util.ArrayList;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

@Service
public class EnrollmentTransactionDAO extends AbstractSqlDAO {

    @Autowired
    private LogFactory logFactory;

    @SuppressWarnings("static-access")
    private Logger logger = logFactory.getLogger(EnrollmentTransactionDAO.class);

    @Autowired
    SqlSessionFactory sqlSessionFactory;


    @Override
    protected SessionFactory getSessionFactory() {
        return sqlSessionFactory.getSessionFactory();
    }

    public void create(EnrollmentTransaction p, Session session) {
        create(p, session, null);
    }

    public void create(EnrollmentTransaction p, Session session, String callingUserId){
        try{
            if(p != null){
                saveEntity(p, session, callingUserId);
            }
        } catch(Exception ex) {
            logger.error("Create EnrollmentTransaction: " + ex.getMessage());
        }
    }
    
    public void save(EnrollmentTransaction p, Session session, String callingUserId){
        saveEntity(p, session, callingUserId);
    }
    
    public void insertAll(List<EnrollmentTransaction> enrollmentTransactions, String callingUserId){
        try{
            if(enrollmentTransactions != null){
                saveAllEntities(enrollmentTransactions, callingUserId);
            }
        }catch (Exception ex){
            logger.error("insertAll Enrollment Transaction: " + ex.getMessage());
        }
    }

    
    
    public EnrollmentTransaction getById(Long id, Boolean enabled, Session session) {
        EnrollmentTransaction enrollmentTransaction = null;
        try{
            if(id != null){
                enrollmentTransaction = (EnrollmentTransaction) getEntityById(id, enabled, EnrollmentTransaction.class, session);
            }
        } catch(Exception ex) {
            logger.error("getById EnrollmentTransaction: " + ex.getMessage());
        }
        return enrollmentTransaction;
    }

    public List<EnrollmentTransaction> getForRefundCheck(String enrollmentId, Long maxTime, Session session, EnrollmentState enrollmentState){
        List<EnrollmentTransaction> enrollmentTransactions = new ArrayList();
        
        try{
            Criteria cr = session.createCriteria(EnrollmentTransaction.class);
            cr.add(Restrictions.eq("enrollmentId", enrollmentId));
            
            if(EnrollmentState.TRIAL.equals(enrollmentState)){
                cr.add(Restrictions.eq("enrollmentTransactionContextType", EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION));
            }else{
                
                cr.add(Restrictions.gt(EnrollmentTransaction.Constants.CREATION_TIME, maxTime));
                cr.add(Restrictions.ne("enrollmentTransactionContextType", EnrollmentTransactionContextType.REGISTRATION_CONSUMPTION));
            }
                        
            cr.add(Restrictions.eq("enrollmentTransactionType", EnrollmentTransactionType.DEBIT));
            
            enrollmentTransactions = cr.list();
        }catch(Exception ex){
            logger.info("Error fetching enrollmentTransactions: "+ex.getMessage());
        }
        return enrollmentTransactions;
    }
    
    public void update(EnrollmentTransaction p, Session session) { update(p, session, null); }

    public void update(EnrollmentTransaction p, Session session, String callingUserId) {
        try {
            if(p!=null){
                updateEntity(p, session, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("update EnrollmentConsumption: " + ex.getMessage());
        }
    }

    public void updateAll(List<EnrollmentTransaction> p, Session session){
        updateAll(p, session, null);
    }

    public void updateAll(List<EnrollmentTransaction> p, Session session, String callingUserId) {
        try {
            if(p != null){
                updateAllEntities(p, session, callingUserId);
            }
        } catch (Exception ex) {
            logger.error("updateAll EnrollmentTransaction: " + ex.getMessage());
        }
    }
    
}
