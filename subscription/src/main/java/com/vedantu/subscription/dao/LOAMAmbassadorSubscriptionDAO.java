package com.vedantu.subscription.dao;

import com.vedantu.subscription.entities.mongo.AcadMentorStudents;
import com.vedantu.subscription.entities.mongo.Batch;
import com.vedantu.subscription.entities.mongo.Course;
import com.vedantu.subscription.entities.mongo.Enrollment;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class LOAMAmbassadorSubscriptionDAO extends AbstractMongoDAO
{
    @Autowired
    private LogFactory logFactory;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    private final Logger logger = logFactory.getLogger(LOAMAmbassadorSubscriptionDAO.class);

    public List<Long> loam_getStudentsByAcadMentor(final Long acadMentorId, Set<String> includeKeySet, boolean isInclude)
    {
        try
        {
            if (acadMentorId != null)
            {
                Query q = new Query();
                q.addCriteria(Criteria.where(AcadMentorStudents.Constants.ACADMENTORID).is(acadMentorId));

                // add Projection to get just the student Id.
                loam_manageFieldsInclude(q, includeKeySet, true);

                List<AcadMentorStudents> acadMentorStudentsList = runQuery(q, AcadMentorStudents.class);
                List<Long> studentIds = new ArrayList<>();

                for (AcadMentorStudents acadMentorStudents : acadMentorStudentsList)
                {
                    studentIds.add(acadMentorStudents.getStudentId());
                }

                return studentIds;
            }
        }
        catch (Exception ex)
        {
            // throw Exception;
            logger.error("Getting AcadMentorStudents failed.");
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Removing AcadMentorStudents failed.");
        }
        return null;
    }

    private void loam_manageFieldsInclude(Query q, Set<String> includeKeySet, boolean isInclude)
    {
        if(includeKeySet !=null && !includeKeySet.isEmpty())
        {
            if(isInclude)
                includeKeySet.parallelStream().forEach(includeKey -> q.fields().include(includeKey));
            else
                includeKeySet.parallelStream().forEach(includeKey -> q.fields().exclude(includeKey));
        }
    }

    @Override
    protected MongoOperations getMongoOperations()
    {
        return mongoClientFactory.getMongoOperations();
    }

    public List<Batch> loam_getBatchData(Long fromTime, Long thruTime, Set<String> requiredFields, boolean b, Integer start, Integer size)
    {
        Query query = new Query();
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(Batch.Constants.LAST_UPDATED).gte(fromTime),
                Criteria.where(Batch.Constants.LAST_UPDATED).lte(thruTime));
        query.addCriteria(andCriteria);
        /*loam_manageFieldsInclude(query, keySet, isInclude);*/
        query.with(Sort.by(Sort.Direction.DESC, Batch.Constants.LAST_UPDATED));
        setFetchParameters(query, start, size);

        return runQuery(query, Batch.class);
    }

    public List<Course> loam_getCourseData(Long fromTime, Long thruTime, Set<String> requiredFields, boolean b, Integer start, Integer size)
    {
        Query query = new Query();
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(Course.Constants.LAST_UPDATED).gte(fromTime),
                Criteria.where(Course.Constants.LAST_UPDATED).lte(thruTime));
        query.addCriteria(andCriteria);
        /*loam_manageFieldsInclude(query, keySet, isInclude);*/
        query.with(Sort.by(Sort.Direction.DESC, Course.Constants.LAST_UPDATED));
        setFetchParameters(query, start, size);

        return runQuery(query, Course.class);
    }

    public List<Enrollment> loam_getEnrollmentData(Long fromTime, Long thruTime, Set<String> requiredFields, boolean b, Integer start, Integer size)
    {
        Query query = new Query();
        Criteria andCriteria = new Criteria().andOperator(Criteria.where(Enrollment.Constants.LAST_UPDATED).gte(fromTime),
                Criteria.where(Enrollment.Constants.LAST_UPDATED).lte(thruTime));
        query.addCriteria(andCriteria);
        /*loam_manageFieldsInclude(query, keySet, isInclude);*/
        query.with(Sort.by(Sort.Direction.DESC, Enrollment.Constants.LAST_UPDATED));
        setFetchParameters(query, start, size);

        return runQuery(query, Enrollment.class);
    }
}
