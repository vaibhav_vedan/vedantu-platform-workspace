package com.vedantu.subscription.dao;

import com.vedantu.session.pojo.EntityType;
import com.vedantu.subscription.entities.mongo.VideoAnalytics;
import com.vedantu.subscription.response.VideoInfo;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.data.mongodb.core.aggregation.Aggregation.project;
import org.springframework.data.mongodb.core.aggregation.AggregationOptions;
import org.bson.Document;

/**
 * Created by somil on 09/05/17.
 */
@Service
public class VideoAnalyticsDAO extends AbstractMongoDAO {

	@Autowired
	private LogFactory logFactory;

	private final Logger logger = logFactory.getLogger(VideoAnalyticsDAO.class);

	@Autowired
	private MongoClientFactory mongoClientFactory;

	public VideoAnalyticsDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void save(VideoAnalytics p, Long callingUserId) {
		// try {
		if (p != null) {
			String callingUserIdString = null;
			if (callingUserId != null) {
				callingUserIdString = callingUserId.toString();
			}
			logger.info("Saving into db" + p);
			saveEntity(p, callingUserIdString);
		}
	}

	public VideoAnalytics getById(String id) {
		return getEntityById(id, VideoAnalytics.class);
	}

	public List<VideoAnalytics> getAnalyticsByUserAndContext(Long userId, EntityType contextType, String contextId) {
		Query query = new Query();
		query.addCriteria(Criteria.where(VideoAnalytics.Constants.USER_ID).is(userId));
		query.addCriteria(Criteria.where(VideoAnalytics.Constants.CONTEXT_TYPE).is(contextType));
		query.addCriteria(Criteria.where(VideoAnalytics.Constants.CONTEXT_ID).is(contextId));
		return runQuery(query, VideoAnalytics.class);
	}

    public List<VideoInfo> getVideoViewInfoFromAnalytics(List<VideoInfo> videoInfosReq) {
		List<String> videoIds = new ArrayList<>();
		List<String> videoTypes = new ArrayList<>();

		for(VideoInfo videoInfo: videoInfosReq) {
			videoIds.add(videoInfo.getVideoId());
			videoTypes.add(videoInfo.getVideoType().toString());
		}

		Criteria criteria = new Criteria();
		Criteria videoIdCriteria = Criteria.where(VideoAnalytics.Constants.VIDEO_ID).in(videoIds);
		Criteria videoTypeCriteria = Criteria.where(VideoAnalytics.Constants.VIDEO_TYPE).in(videoTypes);
		criteria.andOperator(videoIdCriteria, videoTypeCriteria);
                AggregationOptions aggregationOptions=Aggregation.newAggregationOptions().cursor(new Document()).build();
		Aggregation agg = Aggregation.newAggregation(Aggregation.match(criteria),
				Aggregation.group(VideoAnalytics.Constants.VIDEO_ID, VideoAnalytics.Constants.VIDEO_TYPE).count().as("viewCount"),
				project("viewCount").and("_id."+VideoAnalytics.Constants.VIDEO_ID)
                                        .as(VideoAnalytics.Constants.VIDEO_ID).and("_id."+VideoAnalytics.Constants.VIDEO_TYPE).as(VideoAnalytics.Constants.VIDEO_TYPE).andExclude("_id")).withOptions(aggregationOptions);
		logger.info(agg);
		AggregationResults<VideoInfo> groupResults = getMongoOperations().aggregate(agg,
				VideoAnalytics.class.getSimpleName(), VideoInfo.class);
		return groupResults.getMappedResults();


    }
}
