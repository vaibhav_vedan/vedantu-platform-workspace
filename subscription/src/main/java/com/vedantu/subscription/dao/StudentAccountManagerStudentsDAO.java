package com.vedantu.subscription.dao;

import com.vedantu.exception.DuplicateEntryException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.NotFoundException;
import com.vedantu.subscription.entities.mongo.StudentAccountManagerStudents;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StudentAccountManagerStudentsDAO extends AbstractMongoDAO {

    private final Logger logger = LogFactory.getLogger(StudentAccountManagerStudentsDAO.class);
    @Autowired
    private MongoClientFactory mongoClientFactory;
    @Autowired
    private LogFactory logFactory;

    public StudentAccountManagerStudentsDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void insertStudentAccountManagerStudentsList(final List<StudentAccountManagerStudents> studentAccountManagerStudentsList) throws DuplicateEntryException {
        try {
            insertAllEntities(studentAccountManagerStudentsList, "StudentAccountManagerStudents");
        } catch (Exception e) {
            throw new DuplicateEntryException(ErrorCode.DUPLICATE_ENTRY, "Students already has assigned to Student account manager");
        }
    }

    public void upsertStudentAccountManagerStudent(final StudentAccountManagerStudents p, final Long requesterId) {
        try {
            if (p != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(StudentAccountManagerStudents.Constants.STUDENTID).is(p.getStudentId()));
                List<StudentAccountManagerStudents> queryRes = runQuery(q, StudentAccountManagerStudents.class);
                if (ArrayUtils.isEmpty(queryRes)) {
                    saveEntity(p);
                } else {
                    Update u = new Update();
                    u.set(StudentAccountManagerStudents.Constants.STUDENTACCOUNTMANAGERID, p.getStudentAccountManagerId());
                    u.set(StudentAccountManagerStudents.Constants.LAST_UPDATED, System.currentTimeMillis());
                    u.set(StudentAccountManagerStudents.Constants.LAST_UPDATED_BY, requesterId);
                    updateFirst(q, u, StudentAccountManagerStudents.class);
                }
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Adding new SAM save failed for: " + p);
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "A AcadMentorStudents failed.");
        }
    }

    public int removeStudents(final StudentAccountManagerStudents p) throws NotFoundException {
        try {
            if (p != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(StudentAccountManagerStudents.Constants.STUDENTID).is(p.getStudentId()));
                q.addCriteria(Criteria.where(StudentAccountManagerStudents.Constants.STUDENTACCOUNTMANAGERID).is(p.getStudentAccountManagerId()));
                return deleteEntities(q, StudentAccountManagerStudents.class);
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Removing SAMStudents failed for: " + p);
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Removing AcadMentorStudents failed.");
        }
        return 0;
    }

    public List<Long> getStudentsByStudentAccountManager(final Long studentAccountManagerId) {
        try {
            if (studentAccountManagerId != null) {
                Query q = new Query();
                q.addCriteria(Criteria.where(StudentAccountManagerStudents.Constants.STUDENTACCOUNTMANAGERID).is(studentAccountManagerId));
                List<StudentAccountManagerStudents> studentAccountManagerStudentsList = runQuery(q, StudentAccountManagerStudents.class);
                List<Long> studentIds = new ArrayList<>();
                for (StudentAccountManagerStudents studentAccountManagerStudents : studentAccountManagerStudentsList) {
                    studentIds.add(studentAccountManagerStudents.getStudentId());
                }
                return studentIds;
            }
        } catch (Exception ex) {
            // throw Exception;
            logger.error("Getting SAM failed.");
            // throw new VException(ErrorCode.BAD_REQUEST_ERROR, "Removing AcadMentorStudents failed.");
        }
        return null;
    }

    public List<StudentAccountManagerStudents> getSAMByStudentsId(List<String> studentIdList) {

        Query q = new Query();
        q.addCriteria(Criteria.where(StudentAccountManagerStudents.Constants.STUDENTID).in(studentIdList));
        return runQuery(q, StudentAccountManagerStudents.class);
    }
}
