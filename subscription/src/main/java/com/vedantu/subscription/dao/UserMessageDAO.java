package com.vedantu.subscription.dao;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.vedantu.subscription.entities.mongo.UserMessage;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;

@Service
public class UserMessageDAO extends AbstractMongoDAO {
    
        @Autowired
        private MongoClientFactory mongoClientFactory;    


        public UserMessageDAO() {
            super();
        }

        @Override
        protected MongoOperations getMongoOperations() {
            return mongoClientFactory.getMongoOperations();
        }
    
	public UserMessage create(UserMessage userMessage) {
		// Always an insert operation. Update is not supported.
		try {
			if (userMessage != null) {
				saveEntity(userMessage);
			}
		} catch (Exception ex) {
			// throw Exception
			return null;
		}

		return userMessage;
	}

	public UserMessage getById(String id) {
		UserMessage userMessage = null;
		try {
			if (!StringUtils.isEmpty(id)) {
				userMessage = (UserMessage) getEntityById(id, UserMessage.class);
			}
		} catch (Exception ex) {
			// log Exception
			userMessage = null;
		}
		return userMessage;
	}
}
