package com.vedantu.subscription.dao;

import com.vedantu.exception.BadRequestException;
import com.vedantu.exception.ErrorCode;
import com.vedantu.subscription.entities.mongo.Trial;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbentitybeans.mongo.EntityState;
import com.vedantu.util.dbutils.AbstractMongoDAO;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class TrialDAO extends AbstractMongoDAO {
    @Autowired
    private LogFactory logFactory;

    @Autowired
    private MongoClientFactory mongoClientFactory;

    private final Logger logger = logFactory.getLogger(TrialDAO.class);

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void insertAllEntities(List<Trial> trials) {
        if(ArrayUtils.isNotEmpty(trials)) {
            insertAllEntities(trials, Trial.class.getSimpleName());
        }
    }

    public List<Trial> getByIds(Set<String> trialIds) {
        return getByIds(trialIds,null);
    }
    public List<Trial> getByIds(Set<String> trialIds,List<String> includeFields) {
        if(ArrayUtils.isEmpty(trialIds)){
            return new ArrayList<>();
        }

        Query query=new Query();
        query.addCriteria(Criteria.where(Trial.Constants._ID).in(trialIds));
        logger.info("TrialDAO - getByIds - query - {}",query);
        return runQuery(query,Trial.class);
    }

    public void updateContextId(Set<String> trialIds, String contextId) {
        if(ArrayUtils.isEmpty(trialIds) || StringUtils.isEmpty(contextId)){
            return;
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(Trial.Constants._ID).in(trialIds));

        Update update=new Update();
        update.set(Trial.Constants.CONTEXT_ID,contextId);

        updateMulti(query,update,Trial.class);
    }

    public void markTrialIdDeleted(List<String> trialIdsForRemoval) {
        if(ArrayUtils.isNotEmpty(trialIdsForRemoval)){
            Query query=new Query();
            query.addCriteria(Criteria.where(Trial.Constants._ID).in(trialIdsForRemoval));

            Update update=new Update();
            update.set(Trial.Constants.ENTITY_STATE, EntityState.DELETED);

            updateMulti(query,update,Trial.class);
        }
    }

    public Trial getById(String trialId) {
        if(StringUtils.isNotEmpty(trialId)){
            return getEntityById(trialId,Trial.class);
        }
        return null;
    }

    public void save(Trial trial) {
        if(Objects.nonNull(trial)){
            saveEntity(trial);
        }
    }
    public List<Trial> getAllActiveTrialsForBundle( List<String> bundleId) throws BadRequestException {
        Query query=new Query();
        query.addCriteria(Criteria.where(Trial.Constants.CONTEXT_ID).in(bundleId));
        query.addCriteria(Criteria.where(Trial.Constants.ENTITY_STATE).is(EntityState.ACTIVE));
        query.limit(20);
        query.skip(0);
        return runQuery(query,Trial.class);
    }
    public List<Trial> getAllActiveTrialsForBundle(Set<String> trialIds, String bundleId) throws BadRequestException {
        if(ArrayUtils.isEmpty(trialIds) || StringUtils.isEmpty(bundleId)){
            throw new BadRequestException(ErrorCode.INVALID_PARAMETER,"Neither [trialIds] nor [bundleId] could be null");
        }
        Criteria orCriteria=new Criteria();
        orCriteria.orOperator(
                Criteria.where(Trial.Constants._ID).in(trialIds),
                Criteria.where(Trial.Constants.CONTEXT_ID).is(bundleId)
        );
        Query query=new Query();
        query.addCriteria(orCriteria);
        query.addCriteria(Criteria.where(Trial.Constants.ENTITY_STATE).is(EntityState.ACTIVE));

        return runQuery(query,Trial.class);
    }
    public List<Trial> getByContexId(String contexId) {
        if(StringUtils.isEmpty(contexId)){
            return new ArrayList<>();
        }
        Query query=new Query();
        query.addCriteria(Criteria.where(Trial.Constants.CONTEXT_ID).in(contexId));
        logger.info("TrialDAO - getByContexId - query - {}",query);
        return runQuery(query,Trial.class);
    }

}
