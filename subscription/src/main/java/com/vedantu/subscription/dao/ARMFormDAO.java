package com.vedantu.subscription.dao;

import java.util.List;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.vedantu.subscription.entities.mongo.ARMForm;
import com.vedantu.subscription.entities.mongo.CoursePlan;
import com.vedantu.subscription.request.ExportCoursePlansReq;
import com.vedantu.util.LogFactory;
import com.vedantu.util.dbentities.mongo.AbstractMongoStringIdEntity;
import com.vedantu.util.dbutils.AbstractMongoDAO;

@Service
public class ARMFormDAO extends AbstractMongoDAO {

    @Autowired
    private LogFactory logFactory;

    private final Logger logger = logFactory.getLogger(ARMFormDAO.class);


    @Autowired
    private MongoClientFactory mongoClientFactory;

    public ARMFormDAO() {
        super();
    }

    @Override
    protected MongoOperations getMongoOperations() {
        return mongoClientFactory.getMongoOperations();
    }

    public void save(ARMForm p, Long callingUserId){
        //try {
        if (p != null) {
            String callingUserIdString = null;
            if (callingUserId != null) {
                callingUserIdString = callingUserId.toString();
            }
            logger.info("Saving into db" + p);
            saveEntity(p, callingUserIdString);
        }
    }

    public ARMForm getARMFormById(String id) {
        return getEntityById(id, ARMForm.class);
    }
    
    public List<ARMForm> getARMForms(ExportCoursePlansReq req) {
        Long fromTime = req.getFromTime();
        Long tillTime = req.getTillTime();
        Query query = new Query();
        Criteria criteria = null;
        if (fromTime != null && fromTime > 0) {
            criteria = Criteria.where(ARMForm.Constants.CREATION_TIME).gte(fromTime);
        }

        if (tillTime != null && tillTime > 0) {
            if (criteria != null) {
                criteria = criteria.andOperator(Criteria.where(ARMForm.Constants.CREATION_TIME).lt(tillTime));
            } else {
                criteria = Criteria.where(ARMForm.Constants.CREATION_TIME).lt(tillTime);
            }
        }
        if(criteria!=null){
            query.addCriteria(criteria);
        }
        if(req.getAdminId() != null){
            query.addCriteria(Criteria.where(ARMForm.Constants.ADMIN_ID).is(req.getAdminId()));
        }
        
        if(req.getStudentId() != null){
            query.addCriteria(Criteria.where(ARMForm.Constants.STUDENT_ID).is(req.getStudentId()));
        }
        
        if(req.getStartDate() != null){
            query.addCriteria(Criteria.where(ARMForm.Constants.STARTDATE).is(req.getStartDate()));
        }
        
        if(req.isIsClosed() != null){
            query.addCriteria(Criteria.where(ARMForm.Constants.IS_CLOSED).is(req.isIsClosed()));
        }
        
        query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.CREATION_TIME));
        setFetchParameters(query, req.getStart(), req.getSize());
        return runQuery(query, ARMForm.class);
    }
    
    public List<ARMForm> getARMFormByAdminCreationTime(Long adminId) {
     
        Query query = new Query();
    
        query.addCriteria(Criteria.where(ARMForm.Constants.ADMIN_ID).is(adminId));
        query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.CREATION_TIME));
//        setFetchParameters(query,0,20);
        return runQuery(query, ARMForm.class);
    }
    
    public List<ARMForm> getARMFormByStudentId(Long studentId) {
    	Query query = new Query();
    	query.addCriteria(Criteria.where(ARMForm.Constants.STUDENT_ID).is(studentId));
    	query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.CREATION_TIME));
    	return runQuery(query, ARMForm.class);
    }
    
    public List<ARMForm> getARMFormByStartDate(Long startDate){
    	Query query = new Query();
    	query.addCriteria(Criteria.where(ARMForm.Constants.STARTDATE).is(startDate));
    	query.with(Sort.by(Sort.Direction.DESC,
                AbstractMongoStringIdEntity.Constants.CREATION_TIME));
    	return runQuery(query,ARMForm.class);
    }
    
}
