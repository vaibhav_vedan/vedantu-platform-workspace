/**
 * 
 */
package com.vedantu.subscription.dao;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.mongodb.DuplicateKeyException;
import com.mongodb.MongoWriteException;
import com.mysql.fabric.xmlrpc.base.Array;
import com.vedantu.exception.ErrorCode;
import com.vedantu.exception.VException;
import com.vedantu.subscription.entities.mongo.PremiumSubscription;
import com.vedantu.subscription.request.PremiumSubscriptionRequest;
import com.vedantu.subscription.util.RedisDAO;
import com.vedantu.util.ArrayUtils;
import com.vedantu.util.LogFactory;
import com.vedantu.util.StringUtils;
import com.vedantu.util.dbutils.AbstractMongoDAO;

/**
 * @author subarna
 *
 */

@Service
public class PremiumSubscriptionDAO extends AbstractMongoDAO{

	@Autowired
	private MongoClientFactory mongoClientFactory;

	@Autowired
	private LogFactory logFactory;

	@Autowired
	private RedisDAO redisDAO;

	@SuppressWarnings("static-access")
	private Logger logger = logFactory.getLogger(PremiumSubscriptionDAO.class);

	public PremiumSubscriptionDAO() {
		super();
	}

	@Override
	protected MongoOperations getMongoOperations() {
		return mongoClientFactory.getMongoOperations();
	}

	public void saveOrUpdate(PremiumSubscription s, String callingUserId) throws VException{
		PremiumSubscription p = new PremiumSubscription();

		if(StringUtils.isNotEmpty(s.getId()) ){
			p =  getEntityById(s.getId(), PremiumSubscription.class);
		}

		if(null != p && null != p.getId()) {
			if(StringUtils.isNotEmpty(s.getTitle()))
				p.setTitle(s.getTitle());

			if(StringUtils.isNotEmpty(s.getGrade()))
				p.setGrade(s.getGrade());

			if(StringUtils.isNotEmpty(s.getBoard()))
				p.setBoard(s.getBoard());

			if(StringUtils.isNotEmpty(s.getTarget()))
				p.setTarget(s.getTarget());

			if(StringUtils.isNotEmpty(s.getStream()))
				p.setStream(s.getStream());

			if(StringUtils.isNotEmpty(s.getMedium()))
				p.setMedium(s.getMedium());

			if(null != s.getCourseCategory())
				p.setCourseCategory(s.getCourseCategory());

			if(null != s.getValidFrom())
				p.setValidFrom(s.getValidFrom());

			if(null != s.getValidTill())
				p.setValidTill(s.getValidTill());

			if(null != s.getNoOfDaysOfFreeAccess())
				p.setNoOfDaysOfFreeAccess(s.getNoOfDaysOfFreeAccess());

			if(StringUtils.isNotEmpty(s.getBundleId()))
				p.setBundleId(s.getBundleId());

			p.setCreationTime(p.getCreationTime());
			p.setCreatedBy(p.getCreatedBy());

			logger.info("Updating PremiumSubscription for id -> " + s.getId() + " into db" + p);
			saveEntity(p, callingUserId);

		}else {
			try {
				logger.info("Saving PremiumSubscription into db" + s);
				saveEntity(s, callingUserId);
			}catch(DuplicateKeyException | org.springframework.dao.DuplicateKeyException | MongoWriteException e) {
				if (e.getMessage().contains("duplicate key error")) {
					throw new VException(ErrorCode.DUPLICATE_ENTRY, "Bundle for the Combination of Grade, Board, Stream, Target &"
							+ " CourseCategory already exists! Duplicate Entry of same bundle not allowed across any other combination .");
				}else {
					throw e;
				}
			}

		}
	}

	public PremiumSubscription findById(String id) throws VException{
		logger.info("PremiumSubscriptionDAO findById id -> " + id);
		if (StringUtils.isNotEmpty(id)){
			Query query = new Query();
			query.addCriteria(Criteria.where(PremiumSubscription.Constants._ID).is(id));
			return findOne(query, PremiumSubscription.class);
		}
		return null;
	}


	public void deleteById(String id) throws VException{
		logger.info("PremiumSubscriptionDAO deleteById id -> " + id);
		if (StringUtils.isNotEmpty(id))
			deleteEntityById(id, PremiumSubscription.class);
	}

	public List<PremiumSubscription> viewPremiumSubscription(
			PremiumSubscriptionRequest premiumSubscriptionReq) {

		Query query = new Query();

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getGrade()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.GRADE).is(premiumSubscriptionReq.getGrade()));

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getTarget()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.TARGET).is(premiumSubscriptionReq.getTarget()));

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getBoard()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.BOARD).is(premiumSubscriptionReq.getBoard()));

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getStream()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.STREAM).is(premiumSubscriptionReq.getStream()));

		if(null != premiumSubscriptionReq.getCourseCategory())
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.COURSE_CATEGORY).is(premiumSubscriptionReq.getCourseCategory()));

		if(null != premiumSubscriptionReq.getMedium())
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.MEDIUM).is(premiumSubscriptionReq.getMedium()));

		logger.info("PremiumSubscriptionDAO viewPremiumSubscription Query -> "+query);

		return runQuery(query, PremiumSubscription.class);
	}

	public List<PremiumSubscription> getPremiumBundlesForEnrollment(
			PremiumSubscriptionRequest premiumSubscriptionReq, List<String> includeFields) {
		Long currentMillis = System.currentTimeMillis();
		Query query = new Query();

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getGrade()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.GRADE).is(premiumSubscriptionReq.getGrade()));

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getTarget()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.TARGET).is(premiumSubscriptionReq.getTarget()));

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getBoard()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.BOARD).is(premiumSubscriptionReq.getBoard()));

		if(StringUtils.isNotEmpty(premiumSubscriptionReq.getStream()))
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.STREAM).is(premiumSubscriptionReq.getStream()));

		if(null != premiumSubscriptionReq.getCourseCategory())
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.COURSE_CATEGORY).is(premiumSubscriptionReq.getCourseCategory()));

		if(null != premiumSubscriptionReq.getMedium())
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.MEDIUM).is(premiumSubscriptionReq.getMedium()));

		query.addCriteria(Criteria.where(PremiumSubscription.Constants.VALID_FROM).lte(currentMillis));

		query.addCriteria(Criteria.where(PremiumSubscription.Constants.VALID_TILL).gt(currentMillis));

		logger.info("PremiumSubscriptionDAO getPremiumBundlesForEnrollment Query -> "+query+ "includeFields-> "+includeFields);

		return runQuery(query, PremiumSubscription.class, includeFields);
	}

	public PremiumSubscription getPremiumBundleDetailsByBundleId(String bundleId) {

		PremiumSubscription premiumSubscription = new PremiumSubscription();
		Query query = new Query();
		if(StringUtils.isNotEmpty(bundleId)) {
			query.addCriteria(Criteria.where(PremiumSubscription.Constants.BUNDLE_ID).is(bundleId));
			premiumSubscription = findOne(query, PremiumSubscription.class);
		}
		return premiumSubscription;
	}

	public List<PremiumSubscription> getPremiumBundlesByFilter(
			PremiumSubscriptionRequest premiumSubscriptionReq) {

		List<PremiumSubscription> premiumSubscriptions = new ArrayList<PremiumSubscription>();
		Query query = new Query();
		StringJoiner keyJoiner = new StringJoiner("_", "PS_FOR_", "");
		String resString = "";

		Type type = new TypeToken<List<PremiumSubscription>>() {
		}.getType();

		if(null != premiumSubscriptionReq) {
			if(StringUtils.isNotEmpty(premiumSubscriptionReq.getGrade())) {
				keyJoiner.add(premiumSubscriptionReq.getGrade());
				query.addCriteria(Criteria.where(PremiumSubscription.Constants.GRADE).is(premiumSubscriptionReq.getGrade()));

			}
			if(StringUtils.isNotEmpty(premiumSubscriptionReq.getBoard())) {
				keyJoiner.add(premiumSubscriptionReq.getBoard().toUpperCase());
				query.addCriteria(Criteria.where(PremiumSubscription.Constants.BOARD).is(premiumSubscriptionReq.getBoard().toUpperCase()));
			}
			if(StringUtils.isNotEmpty(premiumSubscriptionReq.getTarget())) {
				keyJoiner.add(premiumSubscriptionReq.getTarget().toUpperCase());
				query.addCriteria(Criteria.where(PremiumSubscription.Constants.TARGET).is(premiumSubscriptionReq.getTarget()));
			}
			if(StringUtils.isNotEmpty(premiumSubscriptionReq.getStream())) {
				keyJoiner.add(premiumSubscriptionReq.getStream().toUpperCase());
				query.addCriteria(Criteria.where(PremiumSubscription.Constants.STREAM).is(premiumSubscriptionReq.getStream()));
			}
			if(StringUtils.isNotEmpty(premiumSubscriptionReq.getMedium())) {
				keyJoiner.add(premiumSubscriptionReq.getMedium().toUpperCase());
				query.addCriteria(Criteria.where(PremiumSubscription.Constants.MEDIUM).is(premiumSubscriptionReq.getMedium().toUpperCase()));
			}
			if(null != premiumSubscriptionReq.getCourseCategory()) {
				keyJoiner.add(premiumSubscriptionReq.getCourseCategory().toString());
				query.addCriteria(Criteria.where(PremiumSubscription.Constants.COURSE_CATEGORY).is(premiumSubscriptionReq.getCourseCategory()));
			}
		}
		logger.debug("keyJoiner ->"+keyJoiner);
		try {
			resString = redisDAO.get(keyJoiner.toString());
		} catch (Exception e) {
			logger.error("ERROR Occured while fetching data from Cache for key = "+keyJoiner.toString()+" due to -> " + e.getMessage());
		}

		if(StringUtils.isNotEmpty(resString)) {

			premiumSubscriptions = new Gson().fromJson(resString, type);

		} else {

			query.with(Sort.by(Direction.ASC, PremiumSubscription.Constants.VALID_FROM));

			logger.info("PremiumSubscriptionDAO viewPremiumSubscription Query -> "+query);

			premiumSubscriptions = runQuery(query, PremiumSubscription.class);

			if(ArrayUtils.isNotEmpty(premiumSubscriptions)) {
				resString = new Gson().toJson(premiumSubscriptions);
				try {
					redisDAO.setex(keyJoiner.toString(), resString, 600);
				} catch (Exception e) {
					logger.error("Error Occured while saving data of getPremiumBundlesByFilter to Cache for key = " +keyJoiner.toString()+" due to -> " + e.getMessage());
				}
			}

		}

		return premiumSubscriptions;
	}
}